#!/usr/bin/env bash
export NODE_OPTIONS=--max-old-space-size=8192
set -x #echo on

rm -rf dist/
#ng build --prod --configuration=test

ng build --configuration=local_java --watch
