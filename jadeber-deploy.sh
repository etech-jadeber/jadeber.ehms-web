#!/usr/bin/env bash
#set -x #echo on
export NODE_OPTIONS=--max-old-space-size=8192

rm -rf dist/
ng build --configuration=jadeber &&
ssh jadeber@192.168.1.65 'rm -rf /home/jadeber/jadeber-web/*' &&
scp -r dist/ehms-web/* jadeber@192.168.1.65:/home/jadeber/jadeber-web