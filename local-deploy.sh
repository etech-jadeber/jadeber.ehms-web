#!/usr/bin/env bash
#set -x #echo on
export NODE_OPTIONS=--max-old-space-size=8192
rm -rf dist/
ng build --configuration=local_server &&
ssh etech2030@10.10.20.13 'rm -rf /home/etech2030/jadeber-ehms/*' &&
scp -r dist/ehms-web/* etech2030@10.10.20.13:/home/etech2030/jadeber-ehms