#!/usr/bin/env bash
#set -x #echo on
export NODE_OPTIONS=--max-old-space-size=8192
rm -rf dist/
ng build --configuration=local_server &&
ssh -p 55 etech2030@et1.etechsc.com 'rm -rf /home/etech2030/web-ehms/*' &&
scp -P 55 -r dist/ehms-web/* etech2030@et1.etechsc.com:/home/etech2030/web-ehms