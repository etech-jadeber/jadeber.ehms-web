import {TCId} from "../tc/models";export class ANCInitialEvaluationSummary extends TCId {
    jaundice:boolean;
    genera:string;
    palor:boolean;
    hiv_test_result:number;
    heart_abnormality:boolean;
    chest_abnormality:boolean;
    cervica_lesion:boolean;
    danger_signs_in_pregnancy_and_delivery_adviced:boolean;
    vaginal_discharge:boolean;
    vulvar_ulcer:boolean;
    counseled_on_infant_feeding:boolean;
    pelvic_mass:boolean;
    partner_hiv_test_result:number;
    referred_for_care_treatment_and_support:boolean;
    uterine_size_wks:string;
    mother_hiv_test_result:boolean;
    hiv_test_result_recieved_with_post_test_counseling:boolean;
    birth_preparedness_adviced:boolean;
    patient_id:string;
    encounter_id:string;
    anc_history:string;
     
    }
    export class ANCInitialEvaluationSummaryPartialList {
      data: ANCInitialEvaluationSummary[];
      total: number;
    }
    export class ANCInitialEvaluationDetail extends ANCInitialEvaluationSummary {
    }
    
    export class ANCInitialEvaluationDashboard {
      total: number;
    }