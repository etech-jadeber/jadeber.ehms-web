import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {ANCInitialEvaluationEditComponent} from "./anc-initial-evaluation-edit/anc-initial-evaluation-edit.component";
import {ANCInitialEvaluationPickComponent} from "./anc-initial-evaluation-pick/anc-initial-evaluation-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ANCInitialEvaluationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  aNCInitialEvaluationsUrl(): string {
    return "/ANC_Initial_Evaluations";
  }

  aNCInitialEvaluationUrl(id: string): string {
    return "/ANC_Initial_Evaluations/" + id;
  }

  viewANCInitialEvaluations(): void {
    this.router.navigateByUrl(this.aNCInitialEvaluationsUrl());
  }

  viewANCInitialEvaluation(id: string): void {
    this.router.navigateByUrl(this.aNCInitialEvaluationUrl(id));
  }

  editANCInitialEvaluation(id: string): MatDialogRef<ANCInitialEvaluationEditComponent> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = TCModalModes.EDIT;
    const dialogRef = this.dialog.open(ANCInitialEvaluationEditComponent, {
      data: new TCParentChildIds(null, id,params),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addANCInitialEvaluation(parentId:string): MatDialogRef<ANCInitialEvaluationEditComponent> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = TCModalModes.NEW;
    const dialogRef = this.dialog.open(ANCInitialEvaluationEditComponent, {
          data: new TCParentChildIds(parentId,null, params),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickANCInitialEvaluations(selectOne: boolean=false): MatDialogRef<ANCInitialEvaluationPickComponent> {
      const dialogRef = this.dialog.open(ANCInitialEvaluationPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}