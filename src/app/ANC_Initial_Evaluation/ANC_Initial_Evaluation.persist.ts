import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  ANCInitialEvaluationDashboard,
  ANCInitialEvaluationDetail,
  ANCInitialEvaluationSummaryPartialList,
} from './ANC_Initial_Evaluation.model';
import { HIVTestResultStatus } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class ANCInitialEvaluationPersist {
  hivTestResultFilter: number;

  HivTestResult: TCEnum[] = [
    new TCEnum(HIVTestResultStatus.R, 'R'),
    new TCEnum(HIVTestResultStatus.NR, 'NR'),
    new TCEnum(HIVTestResultStatus.I, 'I'),
  ];

  aNCInitialEvaluationSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.aNCInitialEvaluationSearchText;
    //add custom filters
    return fltrs;
  }

  searchANCInitialEvaluation(
    parentId:string,
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<ANCInitialEvaluationSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'anc_history/'+parentId+'/ANC_Initial_Evaluation',
      this.aNCInitialEvaluationSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<ANCInitialEvaluationSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  aNCInitialEvaluationDashboard(): Observable<ANCInitialEvaluationDashboard> {
    return this.http.get<ANCInitialEvaluationDashboard>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/dashboard'
    );
  }

  getANCInitialEvaluation(id: string): Observable<ANCInitialEvaluationDetail> {
    return this.http.get<ANCInitialEvaluationDetail>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/' + id
    );
  }
  addANCInitialEvaluation(item: ANCInitialEvaluationDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/',
      item
    );
  }

  updateANCInitialEvaluation(
    item: ANCInitialEvaluationDetail
  ): Observable<ANCInitialEvaluationDetail> {
    return this.http.patch<ANCInitialEvaluationDetail>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/' + item.id,
      item
    );
  }

  deleteANCInitialEvaluation(id: string): Observable<{}> {
    return this.http.delete(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/' + id
    );
  }
  aNCInitialEvaluationsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/do',
      new TCDoParam(method, payload)
    );
  }

  aNCInitialEvaluationDo(
    id: string,
    method: string,
    payload: any
  ): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluations/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'ANC_Initial_Evaluation/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
