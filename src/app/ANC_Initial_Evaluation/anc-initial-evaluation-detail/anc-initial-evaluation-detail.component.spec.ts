import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ANCInitialEvaluationDetailComponent } from './anc-initial-evaluation-detail.component';

describe('ANCInitialEvaluationDetailComponent', () => {
  let component: ANCInitialEvaluationDetailComponent;
  let fixture: ComponentFixture<ANCInitialEvaluationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ANCInitialEvaluationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ANCInitialEvaluationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
