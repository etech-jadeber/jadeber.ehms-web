import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ANCInitialEvaluationDetail} from "../ANC_Initial_Evaluation.model";
import {ANCInitialEvaluationPersist} from "../ANC_Initial_Evaluation.persist";
import {ANCInitialEvaluationNavigator} from "../ANC_Initial_Evaluation.navigator";

export enum ANCInitialEvaluationTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-anc_initial_evaluation-detail',
  templateUrl: './anc-initial-evaluation-detail.component.html',
  styleUrls: ['./anc-initial-evaluation-detail.component.css']
})
export class ANCInitialEvaluationDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  aNCInitialEvaluationIsLoading:boolean = false;
  
  ANCInitialEvaluationTabs: typeof ANCInitialEvaluationTabs = ANCInitialEvaluationTabs;
  activeTab: ANCInitialEvaluationTabs = ANCInitialEvaluationTabs.overview;
  //basics
  aNCInitialEvaluationDetail: ANCInitialEvaluationDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public aNCInitialEvaluationNavigator: ANCInitialEvaluationNavigator,
              public  aNCInitialEvaluationPersist: ANCInitialEvaluationPersist) {
    this.tcAuthorization.requireRead("anc_initial_evaluations");
    this.aNCInitialEvaluationDetail = new ANCInitialEvaluationDetail();
  } ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.aNCInitialEvaluationIsLoading = true;
    this.aNCInitialEvaluationPersist.getANCInitialEvaluation(id).subscribe(aNCInitialEvaluationDetail => {
          this.aNCInitialEvaluationDetail = aNCInitialEvaluationDetail;
          this.aNCInitialEvaluationIsLoading = false;
        }, error => {
          console.error(error);
          this.aNCInitialEvaluationIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.aNCInitialEvaluationDetail.id);
  }
  editANCInitialEvaluation(): void {
    let modalRef = this.aNCInitialEvaluationNavigator.editANCInitialEvaluation(this.aNCInitialEvaluationDetail.id);
    modalRef.afterClosed().subscribe(aNCInitialEvaluationDetail => {
      TCUtilsAngular.assign(this.aNCInitialEvaluationDetail, aNCInitialEvaluationDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.aNCInitialEvaluationPersist.print(this.aNCInitialEvaluationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print ANC_Initial_Evaluation", true);
      });
    }

  back():void{
      this.location.back();
    }

}