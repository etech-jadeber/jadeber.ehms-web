import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ANCInitialEvaluationEditComponent } from './anc-initial-evaluation-edit.component';

describe('ANCInitialEvaluationEditComponent', () => {
  let component: ANCInitialEvaluationEditComponent;
  let fixture: ComponentFixture<ANCInitialEvaluationEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ANCInitialEvaluationEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ANCInitialEvaluationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
