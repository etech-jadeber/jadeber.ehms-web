import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes, TCParentChildIds } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ANCInitialEvaluationDetail } from '../ANC_Initial_Evaluation.model';
import { ANCInitialEvaluationPersist } from '../ANC_Initial_Evaluation.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
@Component({
  selector: 'app-anc_initial_evaluation-edit',
  templateUrl: './anc-initial-evaluation-edit.component.html',
  styleUrls: ['./anc-initial-evaluation-edit.component.css'],
})
export class ANCInitialEvaluationEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  aNCInitialEvaluationDetail: ANCInitialEvaluationDetail;

  Jaundice: string;
  Palor: string;
  chest_abnormality: string;
  heart_abnormality: string;
  referred_for_care_treatment_and_support: string;
  vulvar_ulcer: string;
  vaginal_discharge: string;
  pelvic_mass: string;
  cervica_lesion: string;
  danger_signs_in_pregnancy_and_delivery_adviced: string;
  birth_preparedness_adviced: string;
  mother_HIV_test_result: string;
  HIV_test_result_recieved_with_post_test_counseling: string;
  counseled_on_infant_feeding: string;
  hiv_test_result: number;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<ANCInitialEvaluationEditComponent>,
    public persist: ANCInitialEvaluationPersist,
    private tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.context['mode'] == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('anc_initial_evaluations');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'ANC Initial Evaluation';
      this.aNCInitialEvaluationDetail = new ANCInitialEvaluationDetail();

      let parentId = this.idMode.parentId;
      this.aNCInitialEvaluationDetail.anc_history = parentId;
      
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('anc_initial_evaluations');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'ANC Initial Evaluation';
      this.isLoadingResults = true;
      this.persist.getANCInitialEvaluation(this.idMode.childId).subscribe(
        (aNCInitialEvaluationDetail) => {
          this.aNCInitialEvaluationDetail = aNCInitialEvaluationDetail;
          this.mother_HIV_test_result =
            this.aNCInitialEvaluationDetail.mother_hiv_test_result?.toString();
          this.referred_for_care_treatment_and_support =
            this.aNCInitialEvaluationDetail.referred_for_care_treatment_and_support?.toString();
          this.Palor = this.aNCInitialEvaluationDetail.palor?.toString();
          this.Jaundice = this.aNCInitialEvaluationDetail.jaundice?.toString();
          this.chest_abnormality =
            this.aNCInitialEvaluationDetail.chest_abnormality?.toString();
          this.heart_abnormality =
            this.aNCInitialEvaluationDetail.heart_abnormality?.toString();
          this.vulvar_ulcer =
            this.aNCInitialEvaluationDetail.vulvar_ulcer?.toString();
          this.vaginal_discharge =
            this.aNCInitialEvaluationDetail.vaginal_discharge?.toString();
          this.pelvic_mass =
            this.aNCInitialEvaluationDetail.pelvic_mass?.toString();
          this.cervica_lesion =
            this.aNCInitialEvaluationDetail.cervica_lesion?.toString();
          this.danger_signs_in_pregnancy_and_delivery_adviced =
            this.aNCInitialEvaluationDetail.danger_signs_in_pregnancy_and_delivery_adviced?.toString();
          this.birth_preparedness_adviced =
            this.aNCInitialEvaluationDetail.birth_preparedness_adviced?.toString();
          this.HIV_test_result_recieved_with_post_test_counseling =
            this.aNCInitialEvaluationDetail.hiv_test_result_recieved_with_post_test_counseling?.toString();
          this.counseled_on_infant_feeding =
            this.aNCInitialEvaluationDetail.counseled_on_infant_feeding?.toString();

          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist
      .addANCInitialEvaluation(this.aNCInitialEvaluationDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('aNCInitialEvaluation added');
          this.aNCInitialEvaluationDetail.id = value.id;
          this.dialogRef.close(this.aNCInitialEvaluationDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }

  onUpdate(): void {
    this.isLoadingResults = false;

    this.persist
      .updateANCInitialEvaluation(this.aNCInitialEvaluationDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('ANC_Initial_Evaluation updated');
          this.dialogRef.close(this.aNCInitialEvaluationDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }
  canSubmit(): boolean {
    if (this.aNCInitialEvaluationDetail == null) {
      return false;
    }

    if (this.aNCInitialEvaluationDetail.jaundice == null) {
      return false;
    }
    if (
      this.aNCInitialEvaluationDetail.genera == null ||
      this.aNCInitialEvaluationDetail.genera == ''
    ) {
      return false;
    }
    if (this.aNCInitialEvaluationDetail.palor == null) {
      return false;
    }
    if (this.aNCInitialEvaluationDetail.heart_abnormality == null) {
      return false;
    }
    if (this.aNCInitialEvaluationDetail.chest_abnormality == null) {
      return false;
    }
    return true;
  }

  changeValue(
    element: ANCInitialEvaluationDetail,
    field: string,
    event: string
  ) {
    element[field] = event == 'true' ? true : false;
  }
}
