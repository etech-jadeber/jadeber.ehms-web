import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ANCInitialEvaluationListComponent } from './anc-initial-evaluation-list.component';

describe('ANCInitialEvaluationListComponent', () => {
  let component: ANCInitialEvaluationListComponent;
  let fixture: ComponentFixture<ANCInitialEvaluationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ANCInitialEvaluationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ANCInitialEvaluationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
