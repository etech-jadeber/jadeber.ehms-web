import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  ANCInitialEvaluationSummary,
  ANCInitialEvaluationSummaryPartialList,
} from '../ANC_Initial_Evaluation.model';
import { ANCInitialEvaluationPersist } from '../ANC_Initial_Evaluation.persist';
import { ANCInitialEvaluationNavigator } from '../ANC_Initial_Evaluation.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-anc_initial_evaluation-list',
  templateUrl: './anc-initial-evaluation-list.component.html',
  styleUrls: ['./anc-initial-evaluation-list.component.css'],
})
export class ANCInitialEvaluationListComponent implements OnInit {
  aNCInitialEvaluationsData: ANCInitialEvaluationSummary[] = [];
  aNCInitialEvaluationsTotalCount: number = 0;
  aNCInitialEvaluationSelectAll: boolean = false;
  aNCInitialEvaluationSelection: ANCInitialEvaluationSummary[] = [];

  aNCInitialEvaluationsDisplayedColumns: string[] = [
    'action',
    'Genera',
    'Jaundice',
    'Palor',
    'heart_abnormality',
    'chest_abnormality',
  ];

  @Input() ancHistoryId: any;
  @Output() onResult = new EventEmitter<number>();
  
  aNCInitialEvaluationSearchTextBox: FormControl = new FormControl();
  aNCInitialEvaluationIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  aNCInitialEvaluationsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) aNCInitialEvaluationsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public aNCInitialEvaluationPersist: ANCInitialEvaluationPersist,
    public aNCInitialEvaluationNavigator: ANCInitialEvaluationNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob
  ) {
    this.tcAuthorization.requireRead('anc_initial_evaluations');
    this.aNCInitialEvaluationSearchTextBox.setValue(
      aNCInitialEvaluationPersist.aNCInitialEvaluationSearchText
    );
    //delay subsequent keyup events
    this.aNCInitialEvaluationSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.aNCInitialEvaluationPersist.aNCInitialEvaluationSearchText = value;
        this.searchANCInitialEvaluations();
      });
  }
  ngOnInit() {
    this.searchANCInitialEvaluations();
  }

  searchANCInitialEvaluations(isPagination: boolean = false): void {
    // let paginator = this.aNCInitialEvaluationsPaginator;
    // let sorter = this.aNCInitialEvaluationsSort;

    this.aNCInitialEvaluationIsLoading = true;
    this.aNCInitialEvaluationSelection = [];

    this.aNCInitialEvaluationPersist
      .searchANCInitialEvaluation(this.ancHistoryId,10, 0, 'id', 'asc')
      .subscribe(
        (partialList: ANCInitialEvaluationSummaryPartialList) => {
          this.aNCInitialEvaluationsData = partialList.data;
          if (partialList.total != -1) {
            this.aNCInitialEvaluationsTotalCount = partialList.total;
          }
          this.aNCInitialEvaluationIsLoading = false;
        },
        (error) => {
          this.aNCInitialEvaluationIsLoading = false;
        }
      );
  }
  downloadANCInitialEvaluations(): void {
    if (this.aNCInitialEvaluationSelectAll) {
      this.aNCInitialEvaluationPersist
        .downloadAll()
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download ANC_Initial_Evaluation',
            true
          );
        });
    } else {
      this.aNCInitialEvaluationPersist
        .download(this.tcUtilsArray.idsList(this.aNCInitialEvaluationSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download ANC_Initial_Evaluation',
            true
          );
        });
    }
  }
  addANCInitialEvaluation(): void {
    let dialogRef =
      this.aNCInitialEvaluationNavigator.addANCInitialEvaluation(this.ancHistoryId);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchANCInitialEvaluations();
      }
    });
  }

  editANCInitialEvaluation(item: ANCInitialEvaluationSummary) {
    let dialogRef = this.aNCInitialEvaluationNavigator.editANCInitialEvaluation(
      item.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteANCInitialEvaluation(item: ANCInitialEvaluationSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('ANC_Initial_Evaluation');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.aNCInitialEvaluationPersist
          .deleteANCInitialEvaluation(item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('ANC_Initial_Evaluation deleted');
              this.searchANCInitialEvaluations();
            },
            (error) => {}
          );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
