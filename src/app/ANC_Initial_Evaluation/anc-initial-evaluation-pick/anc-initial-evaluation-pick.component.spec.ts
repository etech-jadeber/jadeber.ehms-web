import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ANCInitialEvaluationPickComponent } from './anc-initial-evaluation-pick.component';

describe('ANCInitialEvaluationPickComponent', () => {
  let component: ANCInitialEvaluationPickComponent;
  let fixture: ComponentFixture<ANCInitialEvaluationPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ANCInitialEvaluationPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ANCInitialEvaluationPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
