import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ANCInitialEvaluationSummary, ANCInitialEvaluationSummaryPartialList } from '../ANC_Initial_Evaluation.model';
import { ANCInitialEvaluationPersist } from '../ANC_Initial_Evaluation.persist';
import { ANCInitialEvaluationNavigator } from '../ANC_Initial_Evaluation.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-anc_initial_evaluation-pick',
  templateUrl: './anc-initial-evaluation-pick.component.html',
  styleUrls: ['./anc-initial-evaluation-pick.component.css']
})export class ANCInitialEvaluationPickComponent implements OnInit {
  aNCInitialEvaluationsData: ANCInitialEvaluationSummary[] = [];
  aNCInitialEvaluationsTotalCount: number = 0;
  aNCInitialEvaluationSelectAll:boolean = false;
  aNCInitialEvaluationSelection: ANCInitialEvaluationSummary[] = [];

 aNCInitialEvaluationsDisplayedColumns: string[] = ["select", ,"Pallor","Jaundice","Genera","Palor","HIV test result","hiv_test_result","heart_abnormality","Heart Abnormality","Chest Abn.","chest_abnormality","Cervica Lesion","cervica_lesion","Danger signs in pregnancy & delivery adviced","danger_signs_in_pregnancy_and_delivery_adviced","Vaginal Discharge","vaginal_discharge","Vulvar Ulcer","vulvar_ulcer","Counseled on infant feeding","counseled_on_infant_feeding","Pelvic Mass","pelvic_mass","partner_HIV_test_result","Referred for Care, Treatment and Support","referred_for_care_treatment_and_support","Birth Preparedness Adviced","uterine_size_wks","Partner HIV test result","Uterine Size(wks)","Mother HIV test result","mother_HIV_test_result","HIV test result recieved with post test counseling","HIV_test_result_recieved_with_post_test_counseling","birth_preparedness_adviced" ];
  aNCInitialEvaluationSearchTextBox: FormControl = new FormControl();
  aNCInitialEvaluationIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) aNCInitialEvaluationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) aNCInitialEvaluationsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public aNCInitialEvaluationPersist: ANCInitialEvaluationPersist,
                public aNCInitialEvaluationNavigator: ANCInitialEvaluationNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ANCInitialEvaluationPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("anc_initial_evaluations");
       this.aNCInitialEvaluationSearchTextBox.setValue(aNCInitialEvaluationPersist.aNCInitialEvaluationSearchText);
      //delay subsequent keyup events
      this.aNCInitialEvaluationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.aNCInitialEvaluationPersist.aNCInitialEvaluationSearchText = value;
        this.searchANC_Initial_Evaluations();
      });
    } ngOnInit() {
   
      this.aNCInitialEvaluationsSort.sortChange.subscribe(() => {
        this.aNCInitialEvaluationsPaginator.pageIndex = 0;
        this.searchANC_Initial_Evaluations(true);
      });

      this.aNCInitialEvaluationsPaginator.page.subscribe(() => {
        this.searchANC_Initial_Evaluations(true);
      });
      //start by loading items
      this.searchANC_Initial_Evaluations();
    }

  searchANC_Initial_Evaluations(isPagination:boolean = false): void {


    let paginator = this.aNCInitialEvaluationsPaginator;
    let sorter = this.aNCInitialEvaluationsSort;

    this.aNCInitialEvaluationIsLoading = true;
    this.aNCInitialEvaluationSelection = [];

    this.aNCInitialEvaluationPersist.searchANCInitialEvaluation(TCUtilsString.getInvalidId(), paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ANCInitialEvaluationSummaryPartialList) => {
      this.aNCInitialEvaluationsData = partialList.data;
      if (partialList.total != -1) {
        this.aNCInitialEvaluationsTotalCount = partialList.total;
      }
      this.aNCInitialEvaluationIsLoading = false;
    }, error => {
      this.aNCInitialEvaluationIsLoading = false;
    });

  }
  markOneItem(item: ANCInitialEvaluationSummary) {
    if(!this.tcUtilsArray.containsId(this.aNCInitialEvaluationSelection,item.id)){
          this.aNCInitialEvaluationSelection = [];
          this.aNCInitialEvaluationSelection.push(item);
        }
        else{
          this.aNCInitialEvaluationSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.aNCInitialEvaluationSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }