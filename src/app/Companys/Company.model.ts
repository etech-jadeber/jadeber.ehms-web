import {TCId} from "../tc/models";

export class CompanySummary extends TCId {
  name : string;
tin_no : string;
max_payment : number;
phone_no : string;
address : string;
duration : number;
percentage : number;
insurance_id : string;
status : number
}

export class CompanySummaryPartialList {
  data: CompanySummary[];
  total: number;
}

export class CompanyDetail extends CompanySummary {
  name : string;
tin_no : string;
max_payment : number;
phone_no : string;
address : string;
duration : number;
percentage : number;
insurance_id : string;
}

export class CompanyDashboard {
  total: number;
}
