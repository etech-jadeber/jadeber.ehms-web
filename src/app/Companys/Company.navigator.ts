import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CompanyEditComponent} from "./company-edit/company-edit.component";
import {CompanyPickComponent} from "./company-pick/company-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CompanyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  CompanysUrl(): string {
    return "/companys";
  }

  CompanyUrl(id: string): string {
    return "/companys/" + id;
  }

  viewCompanys(): void {
    this.router.navigateByUrl(this.CompanysUrl());
  }

  viewCompany(id: string): void {
    this.router.navigateByUrl(this.CompanyUrl(id));
  }

  editCompany(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(CompanyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCompany(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(CompanyEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickCompanys(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(CompanyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
