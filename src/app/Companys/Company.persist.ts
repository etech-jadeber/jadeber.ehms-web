import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation, TCEnum} from "../tc/models";
import {CompanyDashboard, CompanyDetail, CompanySummaryPartialList} from "./Company.model";
import { ActivenessStatus, company_insurance_payment_types, CreditStatus } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class CompanyPersist {

  CompanySearchText: string = "";

  company_insurance_payment_typeId: number ;

  companyStatusFilter: number  = -1;

    Activeness_Status: TCEnum[] = [
     new TCEnum( ActivenessStatus.active, 'active'),
  new TCEnum( ActivenessStatus.freeze, 'freeze'),

  ];
    
  company_insurance_payment_types: TCEnumTranslation[] = [
  new TCEnumTranslation(company_insurance_payment_types.yearly, this.appTranslation.getKey('general', 'yearly')),
  new TCEnumTranslation(company_insurance_payment_types.montly, this.appTranslation.getKey('general', 'montly')),
];

  constructor(
      private http: HttpClient,
      private appTranslation: AppTranslation,
      ) {
  }

  searchCompany(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CompanySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("Companys", this.CompanySearchText, pageSize, pageIndex, sort, order);
    if (this.companyStatusFilter != -1){
      url = TCUtilsString.appendUrlParameter(url, "status", this.companyStatusFilter.toString())
    }
    return this.http.get<CompanySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.CompanySearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "Companys/do", new TCDoParam("download_all", this.filters()));
  }

  CompanyDashboard(): Observable<CompanyDashboard> {
    return this.http.get<CompanyDashboard>(environment.tcApiBaseUri + "Companys/dashboard");
  }

  getCompany(id: string): Observable<CompanyDetail> {
    return this.http.get<CompanyDetail>(environment.tcApiBaseUri + "Companys/" + id);
  }

  addCompany(item: CompanyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "Companys/", item);
  }

  updateCompany(item: CompanyDetail): Observable<CompanyDetail> {
    return this.http.patch<CompanyDetail>(environment.tcApiBaseUri + "Companys/" + item.id, item);
  }

  deleteCompany(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "Companys/" + id);
  }

  CompanysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "Companys/do", new TCDoParam(method, payload));
  }

  CompanyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "Companys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "Companys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "Companys/" + id + "/do", new TCDoParam("print", {}));
  }


}
