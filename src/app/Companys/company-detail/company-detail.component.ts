import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {CompanyDetail} from "../Company.model";
import {CompanyPersist} from "../Company.persist";
import {CompanyNavigator} from "../Company.navigator";
import { MenuState } from 'src/app/global-state-manager/global-state';
import { tabs } from 'src/app/app.enums';

export enum CompanyTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-Company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.css']
})
export class CompanyDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  CompanyLoading:boolean = false;
  
  CompanyTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  companyDetail: CompanyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  InsuredEmployeeTotalCount: number = 0;
  CreditHistoryTotalCount: number = 0;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public CompanyNavigator: CompanyNavigator,
              public  CompanyPersist: CompanyPersist,
              public menuState: MenuState,) {
    this.tcAuthorization.requireRead("companys");
    this.companyDetail = new CompanyDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("companys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.CompanyLoading = true;
    this.CompanyPersist.getCompany(id).subscribe(CompanyDetail => {
          this.companyDetail = CompanyDetail;
          this.menuState.getDataResponse(this.companyDetail);
          this.CompanyLoading = false;
        }, error => {
          console.error(error);
          this.CompanyLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.companyDetail.id);
  }

  editCompany(): void {
    let modalRef = this.CompanyNavigator.editCompany(this.companyDetail.id);
    modalRef.afterClosed().subscribe(modifiedCompanyDetail => {
      TCUtilsAngular.assign(this.companyDetail, modifiedCompanyDetail);
    }, error => {
      console.error(error);
    });
  }

   printCompany():void{
      this.CompanyPersist.print(this.companyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print Company", true);
      });
    }

  back():void{
      this.location.back();
    }

    onInsuredEmployeesResult(count: number){
      this.InsuredEmployeeTotalCount = count
    }

    onCreditHistoryResult(count: number){
      this.CreditHistoryTotalCount = count
    }

}
