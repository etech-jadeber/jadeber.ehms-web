import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {CompanyDetail} from "../Company.model";
import {CompanyPersist} from "../Company.persist";
import { Insurance_CompanyPersist } from 'src/app/insurance_companys/ insurance_company.persist';
import { Insurance_CompanySummary, Insurance_CompanySummaryPartialList } from 'src/app/insurance_companys/insurance_company.model';


@Component({
  selector: 'app-Company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.css']
})
export class CompanyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  CompanyDetail: CompanyDetail;
  insuranceCompanies: Insurance_CompanySummary[] = [];

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<CompanyEditComponent>,
              public  persist: CompanyPersist,
              public insuranceCompanyPersist: Insurance_CompanyPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    this.getInsuranceCompanies();
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("companys");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("inventory","company");
      this.CompanyDetail = new CompanyDetail();
      this.CompanyDetail.duration = -1;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("companys");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("inventory","company");
      this.isLoadingResults = true;
      this.persist.getCompany(this.idMode.id).subscribe(CompanyDetail => {
        this.CompanyDetail = CompanyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addCompany(this.CompanyDetail).subscribe(value => {
      this.tcNotification.success("Company added");
      this.CompanyDetail.id = value.id;
      this.dialogRef.close(this.CompanyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateCompany(this.CompanyDetail).subscribe(value => {
      this.tcNotification.success("Company updated");
      this.dialogRef.close(this.CompanyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  onContinue(): void {
    this.isLoadingResults = true;
    this.persist.addCompany(this.CompanyDetail).subscribe(value => {
    this.CompanyDetail.id = value.id
    this.isLoadingResults = false;
        this.dialogRef.close({...this.CompanyDetail, next: true});
    }, error => {
      this.isLoadingResults = false;
      console.log(error)
    })
  }

  getInsuranceCompanies():void{
    this.insuranceCompanyPersist.searchInsurance_Company(50, 0, "", "").subscribe(
      (partialList:Insurance_CompanySummaryPartialList)=>{
        this.insuranceCompanies =  partialList.data;      
      }
    )
  }

  canSubmit():boolean{
        if (this.CompanyDetail == null){
            return false;
          }

        if (this.CompanyDetail.name == null || this.CompanyDetail.name  == "") {
                      return false;
                    }

if (this.CompanyDetail.tin_no == null || this.CompanyDetail.tin_no  == "") {
                      return false;
                    }

if (this.CompanyDetail.phone_no == null || this.CompanyDetail.phone_no  == "") {
                      return false;
                    }

if (this.CompanyDetail.address == null || this.CompanyDetail.address  == "") {
                      return false;
                    }
                    if (this.CompanyDetail.duration == -1) {
                      return false;
                    }


        return true;
      }


}
