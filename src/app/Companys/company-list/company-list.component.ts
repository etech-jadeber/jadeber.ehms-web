import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {CompanyPersist} from "../Company.persist";
import {CompanyNavigator} from "../Company.navigator";
import {CompanyDetail, CompanySummary, CompanySummaryPartialList} from "../Company.model";
import { Insurance_CompanyPersist } from 'src/app/insurance_companys/ insurance_company.persist';
import { Insurance_CompanySummary, Insurance_CompanySummaryPartialList } from 'src/app/insurance_companys/insurance_company.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { ActivenessStatus } from 'src/app/app.enums';
import { CompanyCreditServiceNavigator } from 'src/app/company_credit_service/company_credit_service.navigator';
import { CompanyCreditServicePackageNavigator } from 'src/app/company_credit_service_package/companyCreditServicePackage.navigator';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-Company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {

  CompanysData: CompanySummary[] = [];
  CompanysTotalCount: number = 0;
  CompanysSelectAll:boolean = false;
  CompanysSelection: CompanySummary[] = [];

  CompanysDisplayedColumns: string[] = ["select","action", "name","tin_no","max_payment","phone_no","address","duration","percentage","insurance_id", "status"];
  CompanysSearchTextBox: FormControl = new FormControl();
  CompanysIsLoading: boolean = false;

  insuranceCompanies: Insurance_CompanySummary[] = [];
  company_status = ActivenessStatus

  @ViewChild(MatPaginator, {static: true}) CompanysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) CompanysSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public CompanyPersist: CompanyPersist,
                public CompanyNavigator: CompanyNavigator,
                public jobPersist: JobPersist,
                public insuranceCompanyPersist: Insurance_CompanyPersist,
                public patientNavigator: PatientNavigator,
                public companyCreditServiceNavigator: CompanyCreditServiceNavigator,
                public companyCreditServicePackage: CompanyCreditServicePackageNavigator,
    ) {

        this.tcAuthorization.requireRead("companys");
       this.CompanysSearchTextBox.setValue(CompanyPersist.CompanySearchText);
      //delay subsequent keyup events
      this.CompanysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.CompanyPersist.CompanySearchText = value;
        this.searchCompanys();
      });
    }

    ngOnInit() {
      this.getInsuranceCompanies();
      this.CompanysSort.sortChange.subscribe(() => {
        this.CompanysPaginator.pageIndex = 0;
        this.searchCompanys(true);
      });

      this.CompanysPaginator.page.subscribe(() => {
        this.searchCompanys(true);
      });
      //start by loading items
      this.searchCompanys();
    }

  searchCompanys(isPagination:boolean = false): void {


    let paginator = this.CompanysPaginator;
    let sorter = this.CompanysSort;

    this.CompanysIsLoading = true;
    this.CompanysSelection = [];

    this.CompanyPersist.searchCompany(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CompanySummaryPartialList) => {
      this.CompanysData = partialList.data;
      if (partialList.total != -1) {
        this.CompanysTotalCount = partialList.total;
      }
      this.CompanysIsLoading = false;
    }, error => {
      this.CompanysIsLoading = false;
    });

  }

  downloadCompanys(): void {
    if(this.CompanysSelectAll){
         this.CompanyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download Companys", true);
      });
    }
    else{
        this.CompanyPersist.download(this.tcUtilsArray.idsList(this.CompanysSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download Companys",true);
            });
        }
  }


  activeCompany(company: CompanyDetail): void {
    this.CompanyPersist.CompanyDo(company.id, "active", {}).subscribe(result => {
      company.status = this.company_status.active
    })
  }

  freezeCompany(company: CompanyDetail): void {
    this.CompanyPersist.CompanyDo(company.id, "freeze", {}).subscribe(result => {
      company.status = this.company_status.freeze
    })
  }

  addCompanyCredit(dialogRef: MatDialogRef<unknown, any>, companyId: string, id: string): void {
    dialogRef = this.companyCreditServiceNavigator.addCompanyCreditService(id)
    dialogRef.afterClosed().subscribe(
      result => {
        if (result && result.next) {
          this.addCompanyPackage(dialogRef, companyId)
        }
      }
    )
  }

  addCompanyPackage(dialogRef: MatDialogRef<unknown, any>, id: string): void {
    dialogRef = this.companyCreditServicePackage.addCompanyCreditServicePackage(id)
    dialogRef.afterClosed().subscribe(
      result => {
        if (result && result.next) {
          this.addCompanyCredit(dialogRef, id, result.id)
        }
      }
    )
  }


  addCompany(): void {
    let dialogRef = this.CompanyNavigator.addCompany();
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.next) {
        this.addCompanyPackage(dialogRef, result.id)
        this.searchCompanys()
      }
      else if (result){
        this.searchCompanys();
      }
    });
  }

  editCompany(item: CompanySummary) {
    let dialogRef = this.CompanyNavigator.editCompany(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  getInsuranceCompanies():void{
    this.insuranceCompanyPersist.searchInsurance_Company(50, 0, "", "").subscribe(
      (partialList:Insurance_CompanySummaryPartialList)=>{
        this.insuranceCompanies =  partialList.data;      
      }
    )
  }


  deleteCompany(item: CompanySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Company");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.CompanyPersist.deleteCompany(item.id).subscribe(response => {
          this.tcNotification.success("Company deleted");
          this.searchCompanys();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
