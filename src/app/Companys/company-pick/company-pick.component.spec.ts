import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompanyPickComponent } from './company-pick.component';

describe('CompanyPickComponent', () => {
  let component: CompanyPickComponent;
  let fixture: ComponentFixture<CompanyPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
