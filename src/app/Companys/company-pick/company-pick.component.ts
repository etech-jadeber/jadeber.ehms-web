import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {CompanyDetail, CompanySummary, CompanySummaryPartialList} from "../Company.model";
import {CompanyPersist} from "../Company.persist";


@Component({
  selector: 'app-Company-pick',
  templateUrl: './company-pick.component.html',
  styleUrls: ['./company-pick.component.css']
})
export class CompanyPickComponent implements OnInit {

  CompanysData: CompanySummary[] = [];
  CompanysTotalCount: number = 0;
  CompanysSelection: CompanySummary[] = [];
  CompanysDisplayedColumns: string[] = ["select", 'name','tin_no','max_payment','phone_no','address','duration','percentage','insurance_id' ];

  CompanysSearchTextBox: FormControl = new FormControl();
  CompanysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) CompanysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) CompanysSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public CompanyPersist: CompanyPersist,
              public dialogRef: MatDialogRef<CompanyPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("companys");
    this.CompanysSearchTextBox.setValue(CompanyPersist.CompanySearchText);
    //delay subsequent keyup events
    this.CompanysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.CompanyPersist.CompanySearchText = value;
      this.searchCompanys();
    });
  }

  ngOnInit() {

    this.CompanysSort.sortChange.subscribe(() => {
      this.CompanysPaginator.pageIndex = 0;
      this.searchCompanys();
    });

    this.CompanysPaginator.page.subscribe(() => {
      this.searchCompanys();
    });

    //set initial picker list to 5
    this.CompanysPaginator.pageSize = 5;

    //start by loading items
    this.searchCompanys();
  }

  searchCompanys(): void {
    this.CompanysIsLoading = true;
    this.CompanysSelection = [];

    this.CompanyPersist.searchCompany(this.CompanysPaginator.pageSize,
        this.CompanysPaginator.pageIndex,
        this.CompanysSort.active,
        this.CompanysSort.direction).subscribe((partialList: CompanySummaryPartialList) => {
      this.CompanysData = partialList.data;
      if (partialList.total != -1) {
        this.CompanysTotalCount = partialList.total;
      }
      this.CompanysIsLoading = false;
    }, error => {
      this.CompanysIsLoading = false;
    });

  }

  markOneItem(item: CompanySummary) {
    if(!this.tcUtilsArray.containsId(this.CompanysSelection,item.id)){
          this.CompanysSelection = [];
          this.CompanysSelection.push(item);
        }
        else{
          this.CompanysSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.CompanysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
