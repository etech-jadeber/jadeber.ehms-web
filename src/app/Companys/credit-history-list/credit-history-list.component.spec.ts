import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreditHistoryListComponent } from './credit-history-list.component';

describe('CreditHistoryListComponent', () => {
  let component: CreditHistoryListComponent;
  let fixture: ComponentFixture<CreditHistoryListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditHistoryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
