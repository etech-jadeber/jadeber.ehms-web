import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";

import { CommentDetail, CommentSummary, TCId } from 'src/app/tc/models';
import { FormControl } from '@angular/forms';
import { TCAuthentication } from 'src/app/tc/authentication';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { CreditStatus, PaymentType, payment_status, payment_type, tabs, vat_status } from 'src/app/app.enums';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PaymentDetail, PaymentSummary, TotalPaymentSummery } from '../../payments/payment.model';
import { PaymentPersist } from '../../payments/payment.persist';
import { FeePersist } from 'src/app/fees/fee.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { CompanyPersist } from '../Company.persist';
import { FeeDetail } from 'src/app/fees/fee.model';


export enum PaginatorIndexes {
  covered,
  uncovered,
}

export enum PatientTabs {
  overview
}


@Component({
  selector: 'app-credit-history-list',
  templateUrl: './credit-history-list.component.html',
  styleUrls: ['./credit-history-list.component.css']
})
export class CreditHistoryListComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  creditHistoryLoading: boolean = false;
  totalPaymentPrice: TotalPaymentSummery = { total_price: 0, total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_patient_price: 0 };
  //basics
  coveredData: PaymentSummary[] = [];
  uncoveredData: PaymentSummary[] = [];
  coveredSelectAll: boolean = false;
  uncoveredSelectAll: boolean = false;
  coveredSelection: PaymentSummary[] = [];
  uncoveredSelection: PaymentDetail[] = [];
  coveredTotalCount: number = 0;
  uncoveredTotalCount: number = 0;
  coveredDisplayColumns: String[] = ["select", "patient_id", "crv", "fee_id", "payment_date", "employee", "company", "price"];
  uncoveredDisplayColumns: String[] = ["select", "patient_id", "crv", "fee_id", "payment_date", "employee", "company", "price"]
  creditHistorySearchTextBox: FormControl = new FormControl();
  creditHistory_type: number = PaymentType.CreditByEmployee;
  selectedTabIndex: number = 0;

  tabs: typeof tabs = tabs;
  activeTab: number;

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  @Input() companyId: string;
  @Output() onResult = new EventEmitter<number>();

  start_date: FormControl = new FormControl();
  end_date: FormControl = new FormControl();
  fees: {[id: string]: FeeDetail} = {};
  patients: {[id: string]: PatientDetail} = {}
  downloadPayments(){
    
  }
  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public userPersist: UserPersist,
    public tcAuthentication: TCAuthentication,
    public appTranslation: AppTranslation,
    public tcUtilsString: TCUtilsString,
    public creditHistoryPersist: PaymentPersist,
    public menuState: MenuState,
    public feePersist: FeePersist,
    public doctorPersist: DoctorPersist,
    public patientPersist: PatientPersist,) {
    this.tcAuthorization.requireRead("employee_credit_historys");
    // creditHistory filter
    // this.creditHistorySearchTextBox.setValue(creditHistoryPersist.creditHistorySearchText);
    // this.creditHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
    //   this.creditHistoryPersist.creditHistorySearchText = value.trim();
    //   this.searchPayment();
    // });

    //tab state manager

  }

  update(uncoveredSelection:any, row:any){
    this.tcUtilsArray.toggleSelection(uncoveredSelection,row) ; this.updateSummery()
  }
  ngOnInit() {
    this.tcAuthorization.requireRead("employee_credit_historys");
    this.activeTab = tabs.overview;
    this.creditHistoryPersist.creditPaymentSearchHistory.company_id = this.companyId;
    this.creditHistoryPersist.creditPaymentSearchHistory.credit_status = CreditStatus.not_covered
  }

  ngOnDestroy(): void {
    // this.creditHistoryPersist.start_date = null;
    // this.creditHistoryPersist.end_date = null;
    // this.creditHistoryPersist.company_id = null
    // this.creditHistoryPersist.creditStatusFilter = null;
    // if (this.paramsSubscription) {
    //   this.paramsSubscription.unsubscribe();
    //   this.creditHistoryPersist.paymentTypeFilter = -1;
    // }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      // this.creditHistoryPersist.patient_id = id;
      // this.menuState.changeMenuState(id);
    });
    // covered sorter
    this.sorters
      .toArray()
    [PaginatorIndexes.covered].sortChange.subscribe(() => {
      this.paginators.toArray()[PaginatorIndexes.covered].pageIndex = 0;
      this.searchPayment(true);
    });
    // covered paginator
    this.paginators
      .toArray()
    [PaginatorIndexes.covered].page.subscribe(() => {
      this.searchPayment(true);
    });

    //  uncovered sorter
    this.sorters
      .toArray()
    [PaginatorIndexes.uncovered].sortChange.subscribe(() => {
      this.paginators.toArray()[PaginatorIndexes.uncovered].pageIndex = 0;
      this.searchPayment(true);
    });
    // uncovered paginator
    this.paginators
      .toArray()
    [PaginatorIndexes.uncovered].page.subscribe(() => {
      this.searchPayment(true);
    });

    this.start_date.valueChanges.subscribe(value => {
      this.creditHistoryPersist.creditPaymentSearchHistory.start_date = this.tcUtilsDate.toTimeStamp(new Date(value._d))
      this.searchPayment()
    })
    this.end_date.valueChanges.subscribe(value => {
      this.creditHistoryPersist.creditPaymentSearchHistory.end_date = this.tcUtilsDate.toTimeStamp(new Date(value._d))
      this.searchPayment()
    })
    this.searchPayment()
  }

  back(): void {
    this.location.back();
  }

  //notes methods
  searchPayment(isPagination: boolean = false): void {
    let paginator = this.paginators.toArray()[this.selectedTabIndex];
    let sorter = this.sorters.toArray()[this.selectedTabIndex];
    this.coveredData = [];
    this.uncoveredData = [];
    this.creditHistoryLoading = true;
    this.totalPaymentPrice = { total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_price: 0, total_company_price: 0 }
    this.creditHistoryPersist.searchPayment(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
      this.selectedTabIndex ? this.coveredData = response.data : this.uncoveredData = response.data;
      response.data.forEach(resp => {
        if(!this.fees[resp.fee_id]){
          this.fees[resp.fee_id] = new FeeDetail()
        this.feePersist.getFee(resp.fee_id).subscribe(result => {
          this.fees[resp.fee_id] = result
        })}
        if(!this.patients[resp.patient_id]){
          this.patients[resp.patient_id] = new PatientDetail()
          this.patientPersist.getPatient(resp.patient_id).subscribe(patient => {
            this.patients[resp.patient_id] = patient
          })
        } 
        // this.doctorPersist.getDoctor(resp.provider_id).subscribe(result => {
        //   resp.provider_name = result.first_name + " " + result.last_name;
        // }),
        // resp.cashier_id && this.userPersist.getUser(resp.cashier_id).subscribe(result => {
        //   resp.cashier_name = result.name
        // })
      })
      if (response.total != -1) {
        this.selectedTabIndex ? this.coveredTotalCount = response.total : this.uncoveredTotalCount = response.total;
      }
      this.creditHistoryLoading = false;
    }, error => {
      this.creditHistoryLoading = false;
    });
  }


  addPayments(): void {
    this.uncoveredSelection.forEach(selection => {
      selection.payment_type = this.creditHistory_type
      this.creditHistoryPersist.paymentDo(selection.id, "cover", {}).subscribe(result => {
            this.searchPayment()
            this.uncoveredSelection = []
            this.uncoveredSelectAll = false;
      }
      )
    })
  }

  editcovered(item: CommentDetail): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), item.comment_text, true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        // this.patientPersist.noteDo(this.patientDetail.id, item.id, "edit", { "note": note }).subscribe(response => {
        //   this.searchNotes();
        // });
      }
    });
  }

  deletecovered(item: CommentDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        // this.patientPersist.deleteNote(this.patientDetail.id, item.id).subscribe(response => {
        //   this.tcNotification.success("note deleted");
        //   this.searchNotes();
        // }, error => {
        // });
      }

    });
  }

  updateSummery() {
    this.totalPaymentPrice = { total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_price: 0, total_patient_price: 0, total_company_price: 0}
    this.uncoveredSelection.forEach((selection) => {
      this.totalPaymentPrice.total_price += selection.price;
      this.totalPaymentPrice.total_additional_payment += selection.additional_payment;
      this.totalPaymentPrice.total_discount_payment += selection.discount_payment;
      this.totalPaymentPrice.total_net_price += this.getNetPayment(selection);
      this.totalPaymentPrice.total_company_price = parseFloat((this.totalPaymentPrice.total_company_price + selection.company_payment).toFixed(2))
    })
  }

  updateTab(){
   if(this.selectedTabIndex){
    this.creditHistoryPersist.creditPaymentSearchHistory.credit_status = CreditStatus.covered
     this.searchPayment(false)
   }else {
    this.creditHistoryPersist.creditPaymentSearchHistory.credit_status = CreditStatus.not_covered
     this.searchPayment()
   }
  }

  canSubmit():boolean{
    if (!this.totalPaymentPrice.total_net_price){
      return false;
    }
    return true;
  }

  getNetPayment(element: PaymentDetail){
    return parseFloat((element.price + (element.additional_payment || 0) - (element.discount_payment || 0)).toFixed(2))
  }

  getFeeName(id: string){
    if (this.fees[id]){
      return this.fees[id].name
    }
    return ""
  }

  getPatientName(id: string){
    if (this.patients[id]){
      return `${this.patients[id].fname} ${this.patients[id].mname} ${this.patients[id].lname}`
    }
    return ""
  }

}
