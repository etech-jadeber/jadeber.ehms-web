import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {AdditionalServiceDetail} from "../additional_service.model";
import {AdditionalServicePersist} from "../additional_service.persist";
import {AdditionalServiceNavigator} from "../additional_service.navigator";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';

export enum AdditionalServiceTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-additional-service-detail',
  templateUrl: './additional-service-detail.component.html',
  styleUrls: ['./additional-service-detail.component.scss']
})
export class AdditionalServiceDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  additionalServiceIsLoading:boolean = false;
  
  AdditionalServiceTabs: typeof AdditionalServiceTabs = AdditionalServiceTabs;
  activeTab: AdditionalServiceTabs = AdditionalServiceTabs.overview;
  //basics
  additionalServiceDetail: AdditionalServiceDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public form_encounterPersist: Form_EncounterPersist,
              public additionalServiceNavigator: AdditionalServiceNavigator,
              public  additionalServicePersist: AdditionalServicePersist) {
    this.tcAuthorization.requireRead("additional_services");
    this.additionalServiceDetail = new AdditionalServiceDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("additional_services");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.additionalServiceIsLoading = true;
    this.additionalServicePersist.getAdditionalService(id).subscribe(additionalServiceDetail => {
          this.additionalServiceDetail = additionalServiceDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(additionalServiceDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.additionalServiceIsLoading = false;
        }, error => {
          console.error(error);
          this.additionalServiceIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.additionalServiceDetail.id);
  }
  editAdditionalService(): void {
    let modalRef = this.additionalServiceNavigator.editAdditionalService(this.additionalServiceDetail.id);
    modalRef.afterClosed().subscribe(additionalServiceDetail => {
      TCUtilsAngular.assign(this.additionalServiceDetail, additionalServiceDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.additionalServicePersist.print(this.additionalServiceDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print additional_service", true);
      });
    }

  back():void{
      this.location.back();
    }

}