import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AdditionalServiceDetail, AmbulanceService, MealService, OxygenService } from '../additional_service.model';import { AdditionalServicePersist } from '../additional_service.persist';import { AdditionalServiceType } from 'src/app/app.enums';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-additional_service-edit',
  templateUrl: './additional-service-edit.component.html',
  styleUrls: ['./additional-service-edit.component.scss']
})

export class AdditionalServiceEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  additionalServiceDetail: AdditionalServiceDetail;
  oxygenService: OxygenService;
  ambulanceService: AmbulanceService;
  mealService: MealService;
  AdditionalServiceType = AdditionalServiceType
  future_date: FormControl = new FormControl({disabled: true, value: new Date()})
  end_date: FormControl = new FormControl({disabled: true, value: new Date()})
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<AdditionalServiceEditComponent>,
              public  persist: AdditionalServicePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
          this.future_date.valueChanges.subscribe((value) => {
            this.additionalServiceDetail.future_date = tcUtilsDate.toTimeStamp(value._d)
            this.oxygenService.start_date = this.additionalServiceDetail.future_date
            this.mealService.start_date = this.additionalServiceDetail.future_date
            this.ambulanceService.left_date = this.additionalServiceDetail.future_date
          })
          this.end_date.valueChanges.subscribe((value) => {
            this.oxygenService.end_date = tcUtilsDate.toTimeStamp(value._d)
            this.mealService.end_date = tcUtilsDate.toTimeStamp(value._d)
          })
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("additional_services");
      this.title = this.appTranslation.getText("general","new") +  " " + "additional_service";
      this.additionalServiceDetail = new AdditionalServiceDetail();
      this.oxygenService = new OxygenService();
      this.mealService = new MealService();
      this.ambulanceService = new AmbulanceService()
      this.oxygenService.start_date = this.tcUtilsDate.toTimeStamp(this.future_date.value);
      this.oxygenService.end_date = this.tcUtilsDate.toTimeStamp(this.end_date.value);
      this.mealService.start_date = this.oxygenService.start_date;
      this.mealService.end_date = this.oxygenService.end_date ;
      this.ambulanceService.left_date = this.oxygenService.start_date;
      this.additionalServiceDetail.future_date = this.oxygenService.start_date;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("additional_services");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "additional_service";
      this.isLoadingResults = true;
      this.persist.getAdditionalService(this.idMode.id).subscribe(additionalServiceDetail => {
        this.additionalServiceDetail = additionalServiceDetail;
        this.addTargetObject(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } 
  
  
  onAdd(): void {
    this.isLoadingResults = true;
    this.addTargetObject()
    this.persist.addAdditionalService(this.idMode.id, this.additionalServiceDetail).subscribe(value => {
      this.tcNotification.success("additionalService added");
      this.additionalServiceDetail.id = value.id;
      this.dialogRef.close(this.additionalServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateAdditionalService(this.additionalServiceDetail).subscribe(value => {
      this.tcNotification.success("additional_service updated");
      this.dialogRef.close(this.additionalServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  

  addTargetObject(toTarget: boolean = true){
    if (this.additionalServiceDetail.type == AdditionalServiceType.ambulance){
      toTarget ? this.additionalServiceDetail.target_obj = this.ambulanceService : this.ambulanceService = this.additionalServiceDetail.target_obj as AmbulanceService
    } 
    if (this.additionalServiceDetail.type == AdditionalServiceType.meal){
      toTarget ? this.additionalServiceDetail.target_obj = this.mealService : this.mealService = this.additionalServiceDetail.target_obj as MealService
    }
    if (this.additionalServiceDetail.type == AdditionalServiceType.oxygen){
      toTarget ? this.additionalServiceDetail.target_obj = this.oxygenService : this.oxygenService = this.additionalServiceDetail.target_obj as OxygenService

    }
  }
  
  canSubmit():boolean{
        if (this.additionalServiceDetail == null){
            return false;
          }
        if (this.additionalServiceDetail.future_date == null) {
            return false;
        }
        if (this.additionalServiceDetail.type == null || this.additionalServiceDetail.type == -1 ) {
            return false;
        }
        else {
          if (this.additionalServiceDetail.type == AdditionalServiceType.ambulance){
            return this.canSubmitAmbulance()
          } 
          if (this.additionalServiceDetail.type == AdditionalServiceType.meal){
            return this.canSubmitMeal()
          }
          if (this.additionalServiceDetail.type == AdditionalServiceType.oxygen){
            return this.canSubmitOxygen()
          }
        }
 return true;

 }

 canSubmitOxygen(): boolean {
  if (this.oxygenService.start_date == null){
    return false;
  }
  if (this.oxygenService.oxygen_id == null || this.oxygenService.oxygen_id == ""){
    return false;
  }
  if (this.oxygenService.quantity == null || this.oxygenService.quantity <= 0){
    return false;
  }
  if (this.oxygenService.end_date == null) {
    return false;
  }
  return true;
 }

 canSubmitAmbulance(): boolean {
  if (this.ambulanceService.left_date == null){
    return false;
  }
  if (this.ambulanceService.service_type == null || this.ambulanceService.service_type == -1){
    return false;
  }
  if (this.ambulanceService.pick_location == null || this.ambulanceService.pick_location == ""){
    return false;
  }
  if (this.ambulanceService.trip_type == null || this.ambulanceService.trip_type == -1) {
    return false;
  }
  return true;
 }

 canSubmitMeal(): boolean {
  if (this.mealService.start_date == null){
    return false;
  }
  if (this.mealService.meal_id == null || this.mealService.meal_id == ""){
    return false;
  }
  if (this.mealService.frequency == null || this.mealService.frequency == -1){
    return false;
  }
  if (this.mealService.end_date == null) {
    return false;
  }
  return true;
 }
 }