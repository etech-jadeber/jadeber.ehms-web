import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { AdditionalServiceSummary, AdditionalServiceSummaryPartialList } from '../additional_service.model';
import { AdditionalServicePersist } from '../additional_service.persist';
import { AdditionalServiceNavigator } from '../additional_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-additional-service-list',
  templateUrl: './additional-service-list.component.html',
  styleUrls: ['./additional-service-list.component.scss']
})

export class AdditionalServiceListComponent implements OnInit {
  additionalServicesData: AdditionalServiceSummary[] = [];
  additionalServicesTotalCount: number = 0;
  additionalServiceSelectAll:boolean = false;
  additionalServiceSelection: AdditionalServiceSummary[] = [];

 additionalServicesDisplayedColumns: string[] = ["select","action","type","ordered_date","future_date","status" ];
  additionalServiceSearchTextBox: FormControl = new FormControl();
  additionalServiceIsLoading: boolean = false;  
  

  @Input() encounterId: string;
  @Input() patientId: string;
  @ViewChild(MatPaginator, {static: true}) additionalServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) additionalServicesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public additionalServicePersist: AdditionalServicePersist,
                public additionalServiceNavigator: AdditionalServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public form_encounterPersist: Form_EncounterPersist,

    ) {

        this.tcAuthorization.requireRead("additional_services");
       this.additionalServiceSearchTextBox.setValue(additionalServicePersist.additionalServiceSearchText);
      //delay subsequent keyup events
      this.additionalServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.additionalServicePersist.additionalServiceSearchText = value;
        this.searchAdditionalServices();
      });
    } ngOnInit() {
      this.additionalServicePersist.encounterId = this.encounterId
this.additionalServicePersist.patientId = this.patientId
      this.additionalServicesSort.sortChange.subscribe(() => {
        this.additionalServicesPaginator.pageIndex = 0;
        this.searchAdditionalServices(true);
      });

      this.additionalServicesPaginator.page.subscribe(() => {
        this.searchAdditionalServices(true);
      });
      //start by loading items
      this.searchAdditionalServices();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.additionalServicePersist.encounterId = this.encounterId;
      } else {
      this.additionalServicePersist.encounterId = null;
      }
      this.searchAdditionalServices()
      }

  searchAdditionalServices(isPagination:boolean = false): void {


    let paginator = this.additionalServicesPaginator;
    let sorter = this.additionalServicesSort;

    this.additionalServiceIsLoading = true;
    this.additionalServiceSelection = [];

    this.additionalServicePersist.searchAdditionalService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: AdditionalServiceSummaryPartialList) => {
      this.additionalServicesData = partialList.data;
      if (partialList.total != -1) {
        this.additionalServicesTotalCount = partialList.total;
      }
      this.additionalServiceIsLoading = false;
    }, error => {
      this.additionalServiceIsLoading = false;
    });

  } downloadAdditionalServices(): void {
    if(this.additionalServiceSelectAll){
         this.additionalServicePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download additional_service", true);
      });
    }
    else{
        this.additionalServicePersist.download(this.tcUtilsArray.idsList(this.additionalServiceSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download additional_service",true);
            });
        }
  }
addAdditionalService(): void {
    let dialogRef = this.additionalServiceNavigator.addAdditionalService(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAdditionalServices();
      }
    });
  }

  editAdditionalService(item: AdditionalServiceSummary) {
    let dialogRef = this.additionalServiceNavigator.editAdditionalService(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteAdditionalService(item: AdditionalServiceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("additional_service");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.additionalServicePersist.deleteAdditionalService(item.id).subscribe(response => {
          this.tcNotification.success("additional_service deleted");
          this.searchAdditionalServices();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}