import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalServicePickComponent } from './additional-service-pick.component';

describe('AdditionalServicePickComponent', () => {
  let component: AdditionalServicePickComponent;
  let fixture: ComponentFixture<AdditionalServicePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalServicePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalServicePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
