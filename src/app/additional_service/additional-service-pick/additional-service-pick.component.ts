import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { AdditionalServiceSummary, AdditionalServiceSummaryPartialList } from '../additional_service.model';
import { AdditionalServicePersist } from '../additional_service.persist';
import { AdditionalServiceNavigator } from '../additional_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-additional_service-pick',
  templateUrl: './additional-service-pick.component.html',
  styleUrls: ['./additional-service-pick.component.scss']
})export class AdditionalServicePickComponent implements OnInit {
  additionalServicesData: AdditionalServiceSummary[] = [];
  additionalServicesTotalCount: number = 0;
  additionalServiceSelectAll:boolean = false;
  additionalServiceSelection: AdditionalServiceSummary[] = [];

 additionalServicesDisplayedColumns: string[] = ["select", ,"encounter_id","ordered_date","future_date","type","target_id" ];
  additionalServiceSearchTextBox: FormControl = new FormControl();
  additionalServiceIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) additionalServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) additionalServicesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public additionalServicePersist: AdditionalServicePersist,
                public additionalServiceNavigator: AdditionalServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<AdditionalServicePickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, parentId: string}

    ) {

        this.tcAuthorization.requireRead("additional_service");
       this.additionalServiceSearchTextBox.setValue(additionalServicePersist.additionalServiceSearchText);
      //delay subsequent keyup events
      this.additionalServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.additionalServicePersist.additionalServiceSearchText = value;
        this.searchAdditional_services();
      });
    } ngOnInit() {
   
      this.additionalServicesSort.sortChange.subscribe(() => {
        this.additionalServicesPaginator.pageIndex = 0;
        this.searchAdditional_services(true);
      });

      this.additionalServicesPaginator.page.subscribe(() => {
        this.searchAdditional_services(true);
      });
      //start by loading items
      this.searchAdditional_services();
    }

  searchAdditional_services(isPagination:boolean = false): void {


    let paginator = this.additionalServicesPaginator;
    let sorter = this.additionalServicesSort;

    this.additionalServiceIsLoading = true;
    this.additionalServiceSelection = [];

    this.additionalServicePersist.searchAdditionalService( paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: AdditionalServiceSummaryPartialList) => {
      this.additionalServicesData = partialList.data;
      if (partialList.total != -1) {
        this.additionalServicesTotalCount = partialList.total;
      }
      this.additionalServiceIsLoading = false;
    }, error => {
      this.additionalServiceIsLoading = false;
    });

  }
  markOneItem(item: AdditionalServiceSummary) {
    if(!this.tcUtilsArray.containsId(this.additionalServiceSelection,item.id)){
          this.additionalServiceSelection = [];
          this.additionalServiceSelection.push(item);
        }
        else{
          this.additionalServiceSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.additionalServiceSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }