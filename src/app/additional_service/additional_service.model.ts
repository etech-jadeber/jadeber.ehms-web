import {TCId} from "../tc/models";


export class AdditionalServiceSummary extends TCId {
    encounter_id:string;
    ordered_date:number;
    future_date:number;
    type:number;
    target_id:string;
    target_obj: AmbulanceService | OxygenService | MealService;
    status: number;
    }

export class AdditionalServiceSummaryPartialList {
      data: AdditionalServiceSummary[];
      total: number;
    }
    
export class AdditionalServiceDetail extends AdditionalServiceSummary {
    }
    
export class AdditionalServiceDashboard {
      total: number;
    }

export class OxygenService{
    oxygen_id: string;
    quantity: number;
    start_date: number;
    end_date: number;
}

export class AmbulanceService{
    service_type: number;
    pick_location: string;
    destination_location: string
    trip_type: number;
    left_date: number;
}

export class MealService {
    meal_id: string;
    frequency: number;
    start_date: number;
    end_date: number;
}