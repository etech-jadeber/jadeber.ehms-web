import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {AdditionalServiceEditComponent} from "./additional-service-edit/additional-service-edit.component";
import {AdditionalServicePickComponent} from "./additional-service-pick/additional-service-pick.component";


@Injectable({
  providedIn: 'root'
})

export class AdditionalServiceNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  additionalServicesUrl(): string {
    return "/additional_services";
  }

  additionalServiceUrl(id: string): string {
    return "/additional_services/" + id;
  }

  viewAdditionalServices(): void {
    this.router.navigateByUrl(this.additionalServicesUrl());
  }

  viewAdditionalService(id: string): void {
    this.router.navigateByUrl(this.additionalServiceUrl(id));
  }

  editAdditionalService(id: string): MatDialogRef<AdditionalServiceEditComponent> {
    const dialogRef = this.dialog.open(AdditionalServiceEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addAdditionalService(parentId: string): MatDialogRef<AdditionalServiceEditComponent> {
    const dialogRef = this.dialog.open(AdditionalServiceEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickAdditionalServices(parentId: string, selectOne: boolean=false): MatDialogRef<AdditionalServicePickComponent> {
      const dialogRef = this.dialog.open(AdditionalServicePickComponent, {
        data: {parentId, selectOne},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}