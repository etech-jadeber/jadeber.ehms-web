import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {AdditionalServiceDashboard, AdditionalServiceDetail, AdditionalServiceSummaryPartialList} from "./additional_service.model";
import { AdditionalServiceType, AmbulanceServiceType, MealFrequency, TripType } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class AdditionalServicePersist {
 additionalServiceSearchText: string = "";
 encounterId: string;
 patientId: string;

 additionalServiceTypeFilter: number ;

  AdditionalServiceType: TCEnum[] = [
  new TCEnum( AdditionalServiceType.ambulance, 'Ambulance'),
  new TCEnum( AdditionalServiceType.meal, 'Meal'),
  new TCEnum( AdditionalServiceType.oxygen, 'Oxygen'),

  ];


  ambulanceServiceTypeFilter: number ;

    AmbulanceServiceType: TCEnum[] = [
     new TCEnum( AmbulanceServiceType.advanced_life_support, 'Advanced Life Support'),
  new TCEnum( AmbulanceServiceType.basic_life_support, 'Basic Life Support'),
  new TCEnum( AmbulanceServiceType.patient_transport, 'Patient Transport'),

  ];

  tripTypeFilter: number ;

    TripType: TCEnum[] = [
     new TCEnum( TripType.from_hospital, 'From Hospital'),
  new TCEnum( TripType.to_hosptial, 'To Hospital'),

  ];

  mealFrequencyFilter: number ;

    MealFrequency: TCEnum[] = [
     new TCEnum( MealFrequency.once_a_day, 'Once a Day'),
  new TCEnum( MealFrequency.twice_a_day, 'Twice a Day'),
  new TCEnum( MealFrequency.three_time_a_day, 'Three time a Day'),

  ];



 
 constructor(private http: HttpClient) {
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.additionalServiceSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchAdditionalService( pageSize: number, pageIndex: number, sort: string, order: string,): Observable<AdditionalServiceSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("additional_service", this.additionalServiceSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<AdditionalServiceSummaryPartialList>(url);

  } 
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "additional_service/do", new TCDoParam("download_all", this.filters()));
  }

  additionalServiceDashboard(): Observable<AdditionalServiceDashboard> {
    return this.http.get<AdditionalServiceDashboard>(environment.tcApiBaseUri + "additional_service/dashboard");
  }

  getAdditionalService(id: string): Observable<AdditionalServiceDetail> {
    return this.http.get<AdditionalServiceDetail>(environment.tcApiBaseUri + "additional_service/" + id);
  }
  
  addAdditionalService(parentId: string, item: AdditionalServiceDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/additional_service", item);
  }

  updateAdditionalService(item: AdditionalServiceDetail): Observable<AdditionalServiceDetail> {
    return this.http.patch<AdditionalServiceDetail>(environment.tcApiBaseUri + "additional_service/" + item.id, item);
  }

  deleteAdditionalService(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "additional_service/" + id);
  }
 
  additionalServicesDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/additional_service/do", new TCDoParam(method, payload));
  }

  additionalServiceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "additional_services/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "additional_service/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "additional_service/" + id + "/do", new TCDoParam("print", {}));
  }


}