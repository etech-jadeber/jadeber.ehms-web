import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AleryFormListComponent } from './alery-form-list.component';

describe('AleryFormListComponent', () => {
  let component: AleryFormListComponent;
  let fixture: ComponentFixture<AleryFormListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AleryFormListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AleryFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
