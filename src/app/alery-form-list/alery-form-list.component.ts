import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import {
  AllergyFormDetail,
  AllergyFormSummary,
  AllergySummary,
} from '../allergy/allergy.model';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { TCAuthorization } from '../tc/authorization';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import { Location } from '@angular/common';
import { AllergyPersist } from '../allergy/allergy.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-alery-form-list',
  templateUrl: './alery-form-list.component.html',
  styleUrls: ['./alery-form-list.component.css'],
})
export class AleryFormListComponent implements OnInit {
  allergyFormsData: AllergyFormSummary[] = [];
  allergyFormsTotalCount: number = 0;
  allergyFormSelectAll: boolean = false;
  allergyFormSelection: AllergyFormSummary[] = [];
  allergyFormsDisplayedColumns: string[] = [
    'select',
    'action',
    'allergy_id',
    'remark',
  ];
  allergyFormSearchTextBox: FormControl = new FormControl();
  allergyFormIsLoading: boolean = false;

  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sorter: MatSort;
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public allergyPersist:AllergyPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray
  ) {
    //form_vitalss filter
    this.tcAuthorization.requireRead("allergy_forms");
    this.allergyFormSearchTextBox.setValue(
      form_encounterPersist.allergyFormSearchText
    );
    this.allergyFormSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.form_vitalsSearchText = value.trim();
        this.searchAllergyForm();
      });
  }

  ngOnInit(): void {
      this.form_encounterPersist.allergyFormPatientId = this.patientId 
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchAllergyForm(true);
    });
    // form_vitalss paginator
    this.paginator.page.subscribe(() => {
      this.searchAllergyForm(true);
    });
    this.searchAllergyForm(true);
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.form_encounterPersist.allergyFormEncounterId = this.encounterId;
    } else {
      this.form_encounterPersist.allergyFormEncounterId = null;
    }
    this.searchAllergyForm()
  }

  searchAllergyForm(isPagination: boolean = false): void {
    this.tcAuthorization.canRead("allergy_forms");
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.allergyFormSelection = [];
    this.allergyFormIsLoading = true;
    this.form_encounterPersist
      .searchAllergyForm(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.allergyFormsData = response.data;
          this.allergyFormsData.forEach(
            data => {
              this.searchAllergy(data)
            }
          )
          if (response.total != -1) {
            this.allergyFormsTotalCount = response.total;
          }
          this.allergyFormIsLoading = false;
        },
        (error) => {
          this.allergyFormIsLoading = false;
        }
      );
  }

  searchAllergy(data: AllergyFormDetail):void{
    this.allergyPersist.getAllergy(data.allergy_id).subscribe((result)=>{
     data.allergy_name = result.name
    })

  }

  deleteAllergyForm(item: AllergyFormDetail): void {
    this.tcAuthorization.canDelete("allergy_forms");
    let dialogRef = this.tcNavigator.confirmDeletion('allergy_form');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteAllergyForm(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('allergy_form deleted');
              this.searchAllergyForm();
            },
            (error) => {}
          );
      }
    });
  }
  addAllergyForm(): void {
    this.tcAuthorization.canCreate("allergy_forms");
    let dialogRef = this.form_encounterNavigator.addAllergyForm(
      this.encounterId
    );
    dialogRef.afterClosed().subscribe((res) => {
      this.searchAllergyForm();
    });
  }

  editAllergyForm(item: AllergyFormDetail): void {
    this.tcAuthorization.canUpdate("allergy_forms");
    let dialogRef = this.form_encounterNavigator.editAllergyForm(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((res) => {
      if (res) {
        this.searchAllergyForm();
      }
    });
  }
  downloadAllergyForms(): void {
    this.tcNotification.info('Download allergy_form');
  }

  back(): void {
    this.location.back();
  }
}
