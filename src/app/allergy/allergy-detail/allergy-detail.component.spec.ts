import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AllergyDetailComponent } from './allergy-detail.component';

describe('AllergyDetailComponent', () => {
  let component: AllergyDetailComponent;
  let fixture: ComponentFixture<AllergyDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
