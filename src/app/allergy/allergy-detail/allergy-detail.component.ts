import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {AllergyDetail} from "../allergy.model";
import {AllergyPersist} from "../allergy.persist";
import {AllergyNavigator} from "../allergy.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export enum AllergyTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-allergy-detail',
  templateUrl: './allergy-detail.component.html',
  styleUrls: ['./allergy-detail.component.css']
})
export class AllergyDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  allergyIsLoading:boolean = false;
  
  AllergyTabs: typeof AllergyTabs = AllergyTabs;
  activeTab: AllergyTabs = AllergyTabs.overview;
  //basics
  allergyDetail: AllergyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public allergyNavigator: AllergyNavigator,
              public  allergyPersist: AllergyPersist) {
                this.tcAuthorization.requireRead("allergys");
    this.allergyDetail = new AllergyDetail();
  } ngOnInit() {
    
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.allergyIsLoading = true;
    this.allergyPersist.getAllergy(id).subscribe(allergyDetail => {
          this.allergyDetail = allergyDetail;
          this.allergyIsLoading = false;
        }, error => {
          console.error(error);
          this.allergyIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.allergyDetail.id);
  }
  editAllergy(): void {
    let modalRef = this.allergyNavigator.editAllergy(this.allergyDetail.id);
    modalRef.afterClosed().subscribe(allergyDetail => {
      TCUtilsAngular.assign(this.allergyDetail, allergyDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.allergyPersist.print(this.allergyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print allergy", true);
      });
    }

  back():void{
      this.location.back();
    }

}