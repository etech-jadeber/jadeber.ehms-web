import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AllergyDetail } from '../allergy.model';import { AllergyPersist } from '../allergy.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-allergy-edit',
  templateUrl: './allergy-edit.component.html',
  styleUrls: ['./allergy-edit.component.css']
})export class AllergyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  allergyDetail: AllergyDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<AllergyEditComponent>,
              public  persist: AllergyPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("allergys");
      this.title = this.appTranslation.getText("general","new") +  " " + "allergy";
      this.allergyDetail = new AllergyDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("allergy");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "allergy";
      this.isLoadingResults = true;
      this.persist.getAllergy(this.idMode.id).subscribe(allergyDetail => {
        this.allergyDetail = allergyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addAllergy(this.allergyDetail).subscribe(value => {
      this.tcNotification.success("allergy added");
      this.allergyDetail.id = value.id;
      this.dialogRef.close(this.allergyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateAllergy(this.allergyDetail).subscribe(value => {
      this.tcNotification.success("allergy updated");
      this.dialogRef.close(this.allergyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.allergyDetail == null){
            return false;
          }

if (this.allergyDetail.type == null) {
            return false;
        }
if (this.allergyDetail.name == null || this.allergyDetail.name  == "") {
            return false;
        } 
 return true;
if (this.allergyDetail.medicine == null || this.allergyDetail.medicine  == "") {
            return false;
        } 
 return true;

 }
 }