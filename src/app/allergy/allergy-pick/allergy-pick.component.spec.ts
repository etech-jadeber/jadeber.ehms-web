import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AllergyPickComponent } from './allergy-pick.component';

describe('AllergyPickComponent', () => {
  let component: AllergyPickComponent;
  let fixture: ComponentFixture<AllergyPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergyPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
