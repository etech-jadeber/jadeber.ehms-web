import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { AllergySummary, AllergySummaryPartialList } from '../allergy.model';
import { AllergyPersist } from '../allergy.persist';
import { AllergyNavigator } from '../allergy.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-allergy-pick',
  templateUrl: './allergy-pick.component.html',
  styleUrls: ['./allergy-pick.component.css']
})export class AllergyPickComponent implements OnInit {
  allergysData: AllergySummary[] = [];
  allergysTotalCount: number = 0;
  allergySelectAll:boolean = false;
  allergySelection: AllergySummary[] = [];

 allergysDisplayedColumns: string[] = ["select","type","name","medicine" ];
  allergySearchTextBox: FormControl = new FormControl();
  allergyIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) allergysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) allergysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public allergyPersist: AllergyPersist,
          
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
              public dialogRef: MatDialogRef<AllergyPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("allergys");
       this.allergySearchTextBox.setValue(allergyPersist.allergySearchText);
      //delay subsequent keyup events
      this.allergySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.allergyPersist.allergySearchText = value;
        this.searchAllergys();
      });
    } ngOnInit() {
   
      this.allergysSort.sortChange.subscribe(() => {
        this.allergysPaginator.pageIndex = 0;
        this.searchAllergys(true);
      });

      this.allergysPaginator.page.subscribe(() => {
        this.searchAllergys(true);
      });
      //start by loading items
      this.searchAllergys();
    }

  searchAllergys(isPagination:boolean = false): void {


    let paginator = this.allergysPaginator;
    let sorter = this.allergysSort;

    this.allergyIsLoading = true;
    this.allergySelection = [];

    this.allergyPersist.searchAllergy(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: AllergySummaryPartialList) => {
      this.allergysData = partialList.data;
      if (partialList.total != -1) {
        this.allergysTotalCount = partialList.total;
      }
      this.allergyIsLoading = false;
    }, error => {
      this.allergyIsLoading = false;
    });

  }
  markOneItem(item: AllergySummary) {
    if(!this.tcUtilsArray.containsId(this.allergySelection,item.id)){
          this.allergySelection = [];
          this.allergySelection.push(item);
        }
        else{
          this.allergySelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.allergySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }