import {TCId} from "../tc/models";export class AllergySummary extends TCId {
    type:number;
    name:string;
    medicine:string;
     
    }
    export class AllergySummaryPartialList {
      data: AllergySummary[];
      total: number;
    }
    export class AllergyDetail extends AllergySummary {
    }
    
    export class AllergyDashboard {
      total: number;
    }

    export class AllergyFormSummary extends TCId {
      encounter_id:string;
      allergy_id:string;
      remark:string;
       allergy_name: string;
      }
      export class AllergyFormSummaryPartialList {
        data: AllergyFormSummary[];
        total: number;
      }
      export class AllergyFormDetail extends AllergyFormSummary {
      }
      
      export class AllergyFormDashboard {
        total: number;
      }