import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {AllergyEditComponent} from "./allergy-edit/allergy-edit.component";
import {AllergyPickComponent} from "./allergy-pick/allergy-pick.component";
import { AllergyFormEditComponent } from "../form_encounters/allergy-form-edit/allergy-form-edit.component";


@Injectable({
  providedIn: 'root'
})

export class AllergyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  allergysUrl(): string {
    return "/allergys";
  }

  allergyUrl(id: string): string {
    return "/allergys/" + id;
  }

  viewAllergys(): void {
    this.router.navigateByUrl(this.allergysUrl());
  }

  viewAllergy(id: string): void {
    this.router.navigateByUrl(this.allergyUrl(id));
  }

  editAllergy(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AllergyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addAllergy(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AllergyEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickAllergys(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(AllergyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }

}