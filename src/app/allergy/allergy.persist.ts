import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {AllergyDashboard, AllergyDetail, AllergySummaryPartialList} from "./allergy.model";
import { AllergyType } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class AllergyPersist {
 allergySearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.allergySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchAllergy(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<AllergySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("allergy", this.allergySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<AllergySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "allergy/do", new TCDoParam("download_all", this.filters()));
  }

  allergyDashboard(): Observable<AllergyDashboard> {
    return this.http.get<AllergyDashboard>(environment.tcApiBaseUri + "allergy/dashboard");
  }

  getAllergy(id: string): Observable<AllergyDetail> {
    return this.http.get<AllergyDetail>(environment.tcApiBaseUri + "allergy/" + id);
  } addAllergy(item: AllergyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "allergy/", item);
  }

  updateAllergy(item: AllergyDetail): Observable<AllergyDetail> {
    return this.http.patch<AllergyDetail>(environment.tcApiBaseUri + "allergy/" + item.id, item);
  }

  deleteAllergy(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "allergy/" + id);
  }
 allergysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "allergy/do", new TCDoParam(method, payload));
  }

  allergyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "allergys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "allergy/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "allergy/" + id + "/do", new TCDoParam("print", {}));
  }
  allergyTypeFilter: number ;

  AllergyType: TCEnum[] = [
   new TCEnum( AllergyType.medication, 'medication'),
new TCEnum( AllergyType.food, 'food'),
new TCEnum( AllergyType.seasonal, 'seasonal'),
new TCEnum( AllergyType.animal, 'animal'),
new TCEnum( AllergyType.other, 'other'),

];



}