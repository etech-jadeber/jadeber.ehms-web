import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AncHistoryDetailComponent } from './anc-history-detail.component';

describe('AncHistoryDetailComponent', () => {
  let component: AncHistoryDetailComponent;
  let fixture: ComponentFixture<AncHistoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AncHistoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AncHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
