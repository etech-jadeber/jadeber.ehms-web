import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {AncHistoryDetail} from "../anc_history.model";
import {AncHistoryPersist} from "../anc_history.persist";
import {AncHistoryNavigator} from "../anc_history.navigator";
import { MatTabChangeEvent } from '@angular/material/tabs';

export enum AncHistoryTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-anc_history-detail',
  templateUrl: './anc-history-detail.component.html',
  styleUrls: ['./anc-history-detail.component.css']
})
export class AncHistoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  ancHistoryIsLoading:boolean = false;
  selectedIndex: number = 0;
  
  AncHistoryTabs: typeof AncHistoryTabs = AncHistoryTabs;
  activeTab: AncHistoryTabs = AncHistoryTabs.overview;
  //basics
  ancHistoryDetail: AncHistoryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  form_postNatalCareTotalCount: number;
  form_ancInitialEvaluationTotalCount: number;
  from_PresentPregnancyTotalCount: number;
  form_IntrapartumCareTotaCount: number;
  form_DeliverySummaryTotalCount: number;
  encounterId: string;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public ancHistoryNavigator: AncHistoryNavigator,
              public  ancHistoryPersist: AncHistoryPersist) {
    this.tcAuthorization.requireRead("anc_historys");
    this.ancHistoryDetail = new AncHistoryDetail();
  } ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.ancHistoryPersist.selectedIndex=matTab.index;
  }
   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.ancHistoryIsLoading = true;
    this.ancHistoryPersist.getAncHistory(id).subscribe(ancHistoryDetail => {
          this.ancHistoryDetail = ancHistoryDetail;
          this.ancHistoryIsLoading = false;
          this.selectedIndex = this.ancHistoryPersist.selectedIndex
        }, error => {
          console.error(error);
          this.ancHistoryIsLoading = false;
        });
  }

  OnPostNatalCare(count: number) {
    this.form_postNatalCareTotalCount = count;
  }
  OnAncInitialEvaluation(count: number) {
    this.form_ancInitialEvaluationTotalCount = count;
  }
  OnPresentPregenancyResult(count: number) {
    this.from_PresentPregnancyTotalCount = count
  }
  OnIntrapartumCareResult(count: number) {
    this.form_IntrapartumCareTotaCount = count
  }
  OnDeliverySummaryResult(count: number) {
    this.form_DeliverySummaryTotalCount = count
  }

  reload(){
    this.loadDetails(this.ancHistoryDetail.id);
  }
  editAncHistory(): void {
    let modalRef = this.ancHistoryNavigator.editAncHistory(this.ancHistoryDetail.id);
    modalRef.afterClosed().subscribe(ancHistoryDetail => {
      TCUtilsAngular.assign(this.ancHistoryDetail, ancHistoryDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.ancHistoryPersist.print(this.ancHistoryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print anc_history", true);
      });
    }

  back():void{
      this.location.back();
    }

}
