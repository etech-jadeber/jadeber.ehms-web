import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AncHistoryEditComponent } from './anc-history-edit.component';

describe('AncHistoryEditComponent', () => {
  let component: AncHistoryEditComponent;
  let fixture: ComponentFixture<AncHistoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AncHistoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AncHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
