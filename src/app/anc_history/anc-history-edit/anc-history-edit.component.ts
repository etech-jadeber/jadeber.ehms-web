import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AncHistoryDetail } from '../anc_history.model';
import { AncHistoryPersist } from '../anc_history.persist';
@Component({
  selector: 'app-anc_history-edit',
  templateUrl: './anc-history-edit.component.html',
  styleUrls: ['./anc-history-edit.component.css'],
})
export class AncHistoryEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  ancHistoryDetail: AncHistoryDetail;

  previous_stillbirth: string;
  history_of_three_consecutive_abortions: string;
  birth_weight_last_baby_small: string;
  birth_weight_last_baby_big: string;
  last_pregnancy_admission_for_hypertension: string;
  previous_surgery_on_reproductive_tract: string;
  diagnosed_multiple_pregnancy: string;
  age_less_than_sixteen: string;
  age_more_than_fourty: string;
  isoimmunization_in_previous_pregnancy: string;
  vaginal_bleeding: string;
  pelvic_mass: string;
  diastolic_bp_ninety_mm: string;
  diabetes_mellitus: string;
  renal_disease: string;
  cardiac_disease: string;
  chronic_hypertension: string;
  known_substance_abuse: string;
  other_sever_desease: string;
  marital_status: string;

  dateTime: string;
  lmpDateTime: string;
  eddDateTime: string;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<AncHistoryEditComponent>,
    public persist: AncHistoryPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  dateAndTime(timedate: Date): [string, string] {
    const date = `${timedate.getFullYear()}-${(
      '0' +
      (timedate.getMonth() + 1)
    ).slice(-2)}-${('0' + timedate.getDate()).slice(-2)}`;
    const time = `${('0' + timedate.getHours()).slice(-2)}:${(
      '0' + timedate.getMinutes()
    ).slice(-2)}`;
    return [date, time];
  }

  ngOnInit() {
    const today = new Date();
    const [date, time] = this.dateAndTime(today);
    this.dateTime = `${date}`;
    // this.lmpDateTime = `${date}`;
    // this.eddDateTime = `${date}`;
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('anc_historys');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'ANC History';
      this.ancHistoryDetail = new AncHistoryDetail();
      this.ancHistoryDetail.encounter_id = this.idMode.id;
      this.ancHistoryDetail.major_finding = "";
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('anc_historys');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'ANC History';
      this.isLoadingResults = true;
      this.transformDates(false);
      this.persist.getAncHistory(this.idMode.id).subscribe(
        (ancHistoryDetail) => {
          this.ancHistoryDetail = ancHistoryDetail;
          const [date, time] = this.dateAndTime(
            new Date(this.ancHistoryDetail.date_value * 1000)
          );
          const [lmpDate, lmptime] = this.dateAndTime(
            new Date(this.ancHistoryDetail.lmp_value * 1000)
          );

          const [eddDate, eddtime] = this.dateAndTime(
            new Date(this.ancHistoryDetail.edd_value * 1000)
          );
          this.dateTime = `${date}`;
          this.lmpDateTime = this.ancHistoryDetail.lmp_value ? `${lmpDate}`: null;
          this.eddDateTime = this.ancHistoryDetail.edd_value ? `${eddDate}`: null;

          this.previous_stillbirth =
            this.ancHistoryDetail.previous_stillbirth?.toString();
          this.history_of_three_consecutive_abortions =
            this.ancHistoryDetail.history_of_three_consecutive_abortions?.toString();
          this.birth_weight_last_baby_small =
            this.ancHistoryDetail.birth_weight_last_baby_small?.toString();
          this.birth_weight_last_baby_big =
            this.ancHistoryDetail.birth_weight_last_baby_big?.toString();
          this.last_pregnancy_admission_for_hypertension =
            this.ancHistoryDetail.last_pregnancy_admission_for_hypertension?.toString();
          this.previous_surgery_on_reproductive_tract =
            this.ancHistoryDetail.previous_surgery_on_reproductive_tract?.toString();
          this.diagnosed_multiple_pregnancy =
            this.ancHistoryDetail.diagnosed_multiple_pregnancy?.toString();
          this.age_less_than_sixteen =
            this.ancHistoryDetail.age_less_than_sixteen?.toString();
          this.age_more_than_fourty =
            this.ancHistoryDetail.age_more_than_fourty?.toString();
          this.isoimmunization_in_previous_pregnancy =
            this.ancHistoryDetail.isoimmunization_in_previous_pregnancy?.toString();
          this.vaginal_bleeding =
            this.ancHistoryDetail.vaginal_bleeding?.toString();
          this.pelvic_mass = this.ancHistoryDetail.pelvic_mass?.toString();
          this.diastolic_bp_ninety_mm =
            this.ancHistoryDetail.diastolic_bp_ninety_mm?.toString();
          this.diabetes_mellitus =
            this.ancHistoryDetail.diabetes_mellitus?.toString();
          this.renal_disease = this.ancHistoryDetail.renal_disease?.toString();
          this.cardiac_disease =
            this.ancHistoryDetail.cardiac_disease?.toString();
          this.chronic_hypertension =
            this.ancHistoryDetail.chronic_hypertension?.toString();
          this.known_substance_abuse =
            this.ancHistoryDetail.known_substance_abuse?.toString();
          this.other_sever_desease =
            this.ancHistoryDetail.other_sever_desease?.toString();

          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addAncHistory(this.ancHistoryDetail).subscribe(
      (value) => {
        this.tcNotification.success('ancHistory added');
        this.ancHistoryDetail.id = value.id;
        this.dialogRef.close(this.ancHistoryDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateAncHistory(this.ancHistoryDetail).subscribe(
      (value) => {
        this.tcNotification.success('anc_history updated');
        this.dialogRef.close(this.ancHistoryDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.ancHistoryDetail.date_value =
        new Date(this.dateTime).getTime() / 1000;
      this.ancHistoryDetail.lmp_value = this.lmpDateTime ?
        new Date(this.lmpDateTime).getTime() / 1000 : null;
      this.ancHistoryDetail.edd_value = this.eddDateTime ?
        new Date(this.eddDateTime).getTime() / 1000 :null;
    } else {
    }
  }

  canSubmit(): boolean {
    if (this.ancHistoryDetail == null) {
      return false;
    }

    if (this.ancHistoryDetail.gravida == null) {
      return false;
    }
    if (this.ancHistoryDetail.para == null) {
      return false;
    }
    if (this.ancHistoryDetail.number_of_children_alive == null) {
      return false;
    }
    if (this.ancHistoryDetail.marital_status == null) {
      return false;
    }

    return true;
  }

  changeValue(element: AncHistoryDetail, field: string, event: string) {
    element[field] = event == 'true' ? true : false;
  }
}
