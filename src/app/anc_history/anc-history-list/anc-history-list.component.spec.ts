import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AncHistoryListComponent } from './anc-history-list.component';

describe('AncHistoryListComponent', () => {
  let component: AncHistoryListComponent;
  let fixture: ComponentFixture<AncHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AncHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AncHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
