import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

import { AppTranslation } from '../../app.translation';
import {
  AncHistorySummary,
  AncHistorySummaryPartialList,
} from '../anc_history.model';
import { AncHistoryPersist } from '../anc_history.persist';
import { AncHistoryNavigator } from '../anc_history.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-anc_history-list',
  templateUrl: './anc-history-list.component.html',
  styleUrls: ['./anc-history-list.component.css'],
})
export class AncHistoryListComponent implements OnInit {
  ancHistorysData: AncHistorySummary[] = [];
  ancHistorysTotalCount: number = 0;
  ancHistorySelectAll: boolean = false;
  ancHistorySelection: AncHistorySummary[] = [];

  ancHistorysDisplayedColumns: string[] = [
    // 'select',
    'action',
    // 'link',
    'date_value',
    'acn_reg_no',
    'edd_value',
    'lmp_value',
    'gravida',
    'para',
    'number_of_children_alive',
    'marital_status',
  ];

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  ancHistorySearchTextBox: FormControl = new FormControl();
  ancHistoryIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true }) ancHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) ancHistorysSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public ancHistoryPersist: AncHistoryPersist,
    public ancHistoryNavigator: AncHistoryNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob
  ) {
    this.tcAuthorization.requireRead('anc_historys');
    this.ancHistorySearchTextBox.setValue(
      ancHistoryPersist.ancHistorySearchText
    );
    //delay subsequent keyup events
    this.ancHistorySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.ancHistoryPersist.ancHistorySearchText = value;
        this.searchAncHistorys();
      });
  }
  ngOnInit() {
this.ancHistoryPersist.patientId = this.patientId
    this.ancHistorysSort.sortChange.subscribe(() => {
      this.ancHistorysPaginator.pageIndex = 0;
      this.searchAncHistorys(true);
    });

    this.ancHistorysPaginator.page.subscribe(() => {
      this.searchAncHistorys(true);
    });
    //start by loading items
    this.searchAncHistorys();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.ancHistoryPersist.encounterId = this.encounterId;
    } else {
    this.ancHistoryPersist.encounterId = null;
    }
    this.searchAncHistorys()
    }

  searchAncHistorys(isPagination: boolean = false): void {
    let paginator = this.ancHistorysPaginator;
    let sorter = this.ancHistorysSort;

    this.ancHistoryIsLoading = true;
    this.ancHistorySelection = [];

    this.ancHistoryPersist
      .searchAncHistory(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: AncHistorySummaryPartialList) => {
          this.ancHistorysData = partialList.data;
          if (partialList.total != -1) {
            this.ancHistorysTotalCount = partialList.total;
          }
          this.ancHistoryIsLoading = false;
        },
        (error) => {
          this.ancHistoryIsLoading = false;
        }
      );
  }
  downloadAncHistorys(): void {
    if (this.ancHistorySelectAll) {
      this.ancHistoryPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download anc_history', true);
      });
    } else {
      this.ancHistoryPersist
        .download(this.tcUtilsArray.idsList(this.ancHistorySelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download anc_history',
            true
          );
        });
    }
  }
  addAncHistory(): void {
    let dialogRef = this.ancHistoryNavigator.addAncHistory(this.encounterId);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchAncHistorys();
      }
    });
  }

  editAncHistory(item: AncHistorySummary) {
    let dialogRef = this.ancHistoryNavigator.editAncHistory(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteAncHistory(item: AncHistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('anc_history');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.ancHistoryPersist.deleteAncHistory(item.id).subscribe(
          (response) => {
            this.tcNotification.success('anc_history deleted');
            this.searchAncHistorys();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
