import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AncHistoryPickComponent } from './anc-history-pick.component';

describe('AncHistoryPickComponent', () => {
  let component: AncHistoryPickComponent;
  let fixture: ComponentFixture<AncHistoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AncHistoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AncHistoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
