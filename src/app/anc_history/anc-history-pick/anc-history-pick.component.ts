import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  AncHistorySummary,
  AncHistorySummaryPartialList,
} from '../anc_history.model';
import { AncHistoryPersist } from '../anc_history.persist';
import { AncHistoryNavigator } from '../anc_history.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-anc_history-pick',
  templateUrl: './anc-history-pick.component.html',
  styleUrls: ['./anc-history-pick.component.css'],
})
export class AncHistoryPickComponent implements OnInit {
  ancHistorysData: AncHistorySummary[] = [];
  ancHistorysTotalCount: number = 0;
  ancHistorySelectAll: boolean = false;
  ancHistorySelection: AncHistorySummary[] = [];

  ancHistorysDisplayedColumns: string[] = [
    'select',
    ,
    'date_value',
    'acn_reg_no',
    'lmp',
    'edd_value',
    'lmp_value',
    'gravida',
    'para',
    'number_of_children_alive',
    'marital_status',
    'previous_stillbirth',
    'history_of_three_consecutive_abortions',
    'birth_weight_last_baby_small',
    'birth_weight_last_baby_big',
    'last_pregnancy_admission_for_hypertension',
    'previous_surgery_on_reproductive_tract',
    'diagnosed_multiple_pregnancy',
    'age_less_than_sixteen',
    'age_more_than_fourty',
    'isoimmunization_in_previous_pregnancy',
    'vaginal_bleeding',
    'pelvic_mass',
    'diastolic_bp_ninety_mm',
    'diabetes_mellitus',
    'renal_disease',
    'cardiac_disease',
    'chronic_hypertension',
    'known_substance_abuse',
    'other_sever_desease',
  ];
  ancHistorySearchTextBox: FormControl = new FormControl();
  ancHistoryIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true }) ancHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) ancHistorysSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public ancHistoryPersist: AncHistoryPersist,
    public ancHistoryNavigator: AncHistoryNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<AncHistoryPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('anc_historys');
    this.ancHistorySearchTextBox.setValue(
      ancHistoryPersist.ancHistorySearchText
    );
    //delay subsequent keyup events
    this.ancHistorySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.ancHistoryPersist.ancHistorySearchText = value;
        this.searchAnc_historys();
      });
  }
  ngOnInit() {
    this.ancHistorysSort.sortChange.subscribe(() => {
      this.ancHistorysPaginator.pageIndex = 0;
      this.searchAnc_historys(true);
    });

    this.ancHistorysPaginator.page.subscribe(() => {
      this.searchAnc_historys(true);
    });
    //start by loading items
    this.searchAnc_historys();
  }

  searchAnc_historys(isPagination: boolean = false): void {
    let paginator = this.ancHistorysPaginator;
    let sorter = this.ancHistorysSort;

    this.ancHistoryIsLoading = true;
    this.ancHistorySelection = [];

    this.ancHistoryPersist
      .searchAncHistory(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: AncHistorySummaryPartialList) => {
          this.ancHistorysData = partialList.data;
          if (partialList.total != -1) {
            this.ancHistorysTotalCount = partialList.total;
          }
          this.ancHistoryIsLoading = false;
        },
        (error) => {
          this.ancHistoryIsLoading = false;
        }
      );
  }
  markOneItem(item: AncHistorySummary) {
    if (!this.tcUtilsArray.containsId(this.ancHistorySelection, item.id)) {
      this.ancHistorySelection = [];
      this.ancHistorySelection.push(item);
    } else {
      this.ancHistorySelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.ancHistorySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
