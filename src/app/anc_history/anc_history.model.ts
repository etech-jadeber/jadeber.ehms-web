import {TCId} from "../tc/models";export class AncHistorySummary extends TCId {
    date_value:number;
    acn_reg_no:number;
    edd_value:number;
    lmp_value:number;
    gravida:number;
    para:number;
    number_of_children_alive:number;
    marital_status:number;
    previous_stillbirth:boolean;
    history_of_three_consecutive_abortions:boolean;
    birth_weight_last_baby_small:boolean;
    birth_weight_last_baby_big:boolean;
    last_pregnancy_admission_for_hypertension:boolean;
    previous_surgery_on_reproductive_tract:boolean;
    diagnosed_multiple_pregnancy:boolean;
    age_less_than_sixteen:boolean;
    age_more_than_fourty:boolean;
    isoimmunization_in_previous_pregnancy:boolean;
    vaginal_bleeding:boolean;
    pelvic_mass:boolean;
    diastolic_bp_ninety_mm:boolean;
    diabetes_mellitus:boolean;
    renal_disease:boolean;
    cardiac_disease:boolean;
    chronic_hypertension:boolean;
    known_substance_abuse:boolean;
    other_sever_desease:boolean;
    major_finding: string;
    encounter_id:string;
     
    }
    export class AncHistorySummaryPartialList {
      data: AncHistorySummary[];
      total: number;
    }
    export class AncHistoryDetail extends AncHistorySummary {

    }
    
    export class AncHistoryDashboard {
      total: number;
    }
    