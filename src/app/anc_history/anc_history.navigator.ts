import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { AncHistoryEditComponent } from './anc-history-edit/anc-history-edit.component';
import { AncHistoryPickComponent } from './anc-history-pick/anc-history-pick.component';

@Injectable({
  providedIn: 'root',
})
export class AncHistoryNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  ancHistorysUrl(): string {
    return '/anc_historys';
  }

  ancHistoryUrl(id: string): string {
    return '/anc_historys/' + id;
  }

  viewAncHistorys(): void {
    this.router.navigateByUrl(this.ancHistorysUrl());
  }

  viewAncHistory(id: string): void {
    this.router.navigateByUrl(this.ancHistoryUrl(id));
  }

  editAncHistory(id: string): MatDialogRef<AncHistoryEditComponent> {
    const dialogRef = this.dialog.open(AncHistoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  addAncHistory(parentId: string): MatDialogRef<AncHistoryEditComponent> {
    const dialogRef = this.dialog.open(AncHistoryEditComponent, {
      data: new TCIdMode(parentId, TCModalModes.NEW),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  pickAncHistorys(
    selectOne: boolean = false
  ): MatDialogRef<AncHistoryPickComponent> {
    const dialogRef = this.dialog.open(AncHistoryPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
