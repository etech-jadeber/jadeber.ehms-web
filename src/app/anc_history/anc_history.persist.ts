import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {AncHistoryDashboard, AncHistoryDetail, AncHistorySummaryPartialList} from "./anc_history.model";
import { MaritalStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class AncHistoryPersist {
  selectedIndex:number=0;
  encounterId: string;
  patientId: string;

  MaritalStatus: TCEnum[] = [
    new TCEnum(MaritalStatus.single, 'Single'),
    new TCEnum(MaritalStatus.married, 'Married'),
    new TCEnum(MaritalStatus.widowed, 'Widowed'),
    new TCEnum(MaritalStatus.divorced, 'Divorced'),
    new TCEnum(MaritalStatus.separated, 'Separated'),
  ];

 ancHistorySearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.ancHistorySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchAncHistory( pageSize: number, pageIndex: number, sort: string, order: string,): Observable<AncHistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("anc_history", this.ancHistorySearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<AncHistorySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "anc_history/do", new TCDoParam("download_all", this.filters()));
  }

  ancHistoryDashboard(): Observable<AncHistoryDashboard> {
    return this.http.get<AncHistoryDashboard>(environment.tcApiBaseUri + "anc_history/dashboard");
  }

  getAncHistory(id: string): Observable<AncHistoryDetail> {
    return this.http.get<AncHistoryDetail>(environment.tcApiBaseUri + "anc_history/" + id);
  } addAncHistory(item: AncHistoryDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "anc_history/", item);
  }

  updateAncHistory(item: AncHistoryDetail): Observable<AncHistoryDetail> {
    return this.http.patch<AncHistoryDetail>(environment.tcApiBaseUri + "anc_history/" + item.id, item);
  }

  deleteAncHistory(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "anc_history/" + id);
  }
 ancHistorysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "anc_history/do", new TCDoParam(method, payload));
  }

  ancHistoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "anc_historys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "anc_history/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "anc_history/" + id + "/do", new TCDoParam("print", {}));
  }


}
