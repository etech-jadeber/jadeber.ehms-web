import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import { DrugtypeDetailComponent } from './drugtypes/drugtype-detail/drugtype-detail.component';
import { DrugtypeListComponent } from './drugtypes/drugtype-list/drugtype-list.component';
import { LoginComponent } from './login/login.component';
import { LostDamageDetailComponent } from './lostDamages/lost-damage-detail/lost-damage-detail.component';
import { LostDamageListComponent } from './lostDamages/lost-damage-list/lost-damage-list.component';
import { AboutComponent } from './pages/about/about.component';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { InvalidOperationComponent } from './pages/invalid-operation/invalid-operation.component';
import { MandatoryComponent } from './pages/mandatory/mandatory.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { TosComponent } from './pages/tos/tos.component';
import { PatientDetailComponent } from './patients/patient-detail/patient-detail.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { PatientsHomeComponent } from './patients/patients-home/patients-home.component';
import { ProfileComponent } from './profile/profile.component';
import { RequestDetailComponent } from './requests/request-detail/request-detail.component';
import { RequestListComponent } from './requests/request-list/request-list.component';
import { SupplierDetailComponent } from './suppliers/supplier-detail/supplier-detail.component';
import { SupplierListComponent } from './suppliers/supplier-list/supplier-list.component';
import { StoreDetailComponent } from './stores/store-detail/store-detail.component';
import { StoreListComponent } from './stores/store-list/store-list.component';
import { CaseDetailComponent } from './tc/cases/case-detail/case-detail.component';
import { CaseListComponent } from './tc/cases/case-list/case-list.component';
import { EventlogDetailComponent } from './tc/eventlogs/eventlog-detail/eventlog-detail.component';
import { EventlogListComponent } from './tc/eventlogs/eventlog-list/eventlog-list.component';
import { FileDetailComponent } from './tc/files/file-detail/file-detail.component';
import { FileListComponent } from './tc/files/file-list/file-list.component';
import { GroupDetailComponent } from './tc/groups/group-detail/group-detail.component';
import { GroupListComponent } from './tc/groups/group-list/group-list.component';
import { JobDetailComponent } from './tc/jobs/job-detail/job-detail.component';
import { JobListComponent } from './tc/jobs/job-list/job-list.component';
import { LockDetailComponent } from './tc/locks/lock-detail/lock-detail.component';
import { LockListComponent } from './tc/locks/lock-list/lock-list.component';
import { MailDetailComponent } from './tc/mails/mail-detail/mail-detail.component';
import { MailListComponent } from './tc/mails/mail-list/mail-list.component';
import { ResourceDetailComponent } from './tc/resources/resource-detail/resource-detail.component';
import { ResourceListComponent } from './tc/resources/resource-list/resource-list.component';
import { SettingDetailComponent } from './tc/settings/setting-detail/setting-detail.component';
import { SettingListComponent } from './tc/settings/setting-list/setting-list.component';
import { StatDetailComponent } from './tc/stats/stat-detail/stat-detail.component';
import { StatListComponent } from './tc/stats/stat-list/stat-list.component';
import { TgidentityDetailComponent } from './tc/tgidentitys/tgidentity-detail/tgidentity-detail.component';
import { TgidentityListComponent } from './tc/tgidentitys/tgidentity-list/tgidentity-list.component';
import { UserDetailComponent } from './tc/users/user-detail/user-detail.component';
import { UserListComponent } from './tc/users/user-list/user-list.component';
import { WaitinglistDetailComponent } from './bed/waitinglists/waitinglist-detail/waitinglist-detail.component';
import { WaitinglistListComponent } from './bed/waitinglists/waitinglist-list/waitinglist-list.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Item_Ledger_EntryDetailComponent } from './item_ledger_entrys/item-ledger-entry-detail/item-ledger-entry-detail.component';
import { EmployeeListComponent } from './storeemployees/employee-list/employee-list.component';
import { EmployeeDetailComponent } from './storeemployees/employee-detail/employee-detail.component';

import { EmployeeHomeComponent } from './storeemployees/employee-home/employee-home.component';

import { TransferListComponent } from './transfers/transfer-list/transfer-list.component';
import { TransferDetailComponent } from './transfers/transfer-detail/transfer-detail.component';
import { Item_Ledger_EntryListComponent } from './item_ledger_entrys/item-ledger-entry-list/item-ledger-entry-list.component';
import { PurchaseDetailComponent } from './purchases/purchase-detail/purchase-detail.component';
import { PurchaseListComponent } from './purchases/purchase-list/purchase-list.component';
import { PaymentDetailComponent } from './payments/payment-detail/payment-detail.component';
import { PaymentListComponent } from './payments/payment-list/payment-list.component';
import { AppointmentDetailComponent } from './appointments/appointment-detail/appointment-detail.component';
import { AppointmentListComponent } from './appointments/appointment-list/appointment-list.component';
import { AvailabilityListComponent } from './availabilitys/availability-list/availability-list.component';
import { AvailabilityDetailComponent } from './availabilitys/availability-detail/availability-detail.component';
import { ReceptionDetailComponent } from './receptions/reception-detail/reception-detail.component';
import { ReceptionListComponent } from './receptions/reception-list/reception-list.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { DoctorDetailComponent } from './doctors/doctor-detail/doctor-detail.component';
import { DoctorSelfComponent } from './doctors/doctor-self/doctor-self.component';
import { ReceptionSelfComponent } from './receptions/reception-self/reception-self.component';
import { ReportDetailComponent } from './reports/report-detail/report-detail.component';
import { ReportListComponent } from './reports/report-list/report-list.component';
import { FeeDetailComponent } from './fees/fee-detail/fee-detail.component';
import { FeeListComponent } from './fees/fee-list/fee-list.component';
import { Fee_TypeDetailComponent } from './feetypes/feetype-detail/feetype-detail.component';
import { Fee_TypeListComponent } from './feetypes/feetype-list/feetype-list.component';
import { Item_In_StoreDetailComponent } from './item_in_stores/item-in-store-detail/item-in-store-detail.component';
import { Item_In_StoreListComponent } from './item_in_stores/item-in-store-list/item-in-store-list.component';
import { Surgery_TypeDetailComponent } from './surgery_types/surgery-type-detail/surgery-type-detail.component';
import { Surgery_TypeListComponent } from './surgery_types/surgery-type-list/surgery-type-list.component';
import { Surgery_AppointmentDetailComponent } from './surgery_appointments/surgery-appointment-detail/surgery-appointment-detail.component';
import { Surgery_AppointmentListComponent } from './surgery_appointments/surgery-appointment-list/surgery-appointment-list.component';
import { DepositDetailComponent } from './deposits/deposit-detail/deposit-detail.component';
import { DepositListComponent } from './deposits/deposit-list/deposit-list.component';
import { VitalDetailComponent } from './vitals/vital-detail/vital-detail.component';
import { VitalListComponent } from './vitals/vital-list/vital-list.component';
import { Form_EncounterDetailComponent } from './form_encounters/form-encounter-detail/form-encounter-detail.component';
import { Form_EncounterListComponent } from './form_encounters/form-encounter-list/form-encounter-list.component';
import { LabSelfComponent } from './doctors/lab-self/lab-self.component';
import { NurseSelfComponent } from './doctors/nurse-self/nurse-self.component';
import { Procedure_OrderDetailComponent } from './form_encounters/procedure_orders/procedure-order-detail/procedure-order-detail.component';
import { Procedure_ProviderDetailComponent } from './procedure_providers/procedure-provider-detail/procedure-provider-detail.component';
import { Procedure_ProviderListComponent } from './procedure_providers/procedure-provider-list/procedure-provider-list.component';
import { MessagesListComponent } from './messagess/messages-list/messages-list.component';
import { MessagesDetailComponent } from './messagess/messages-detail/messages-detail.component';

import { Procedure_TestsListComponent } from './procedure_testss/procedure-tests-list/procedure-tests-list.component';
import { ProcedureTestsDetailComponent } from './procedure_testss/procedure-tests-detail/procedure-tests-detail.component';
import { Procedure_Payment_DueListComponent } from './procedure_payment_dues/procedure-payment-due-list/procedure-payment-due-list.component';
import { ProcedurePaymentDueDetailComponent } from './procedure_payment_dues/procedure-payment-due-detail/procedure-payment-due-detail.component';
import { CompanyDetailComponent } from './Companys/company-detail/company-detail.component';
import { CompanyListComponent } from './Companys/company-list/company-list.component';
import { InsuredEmployeeDetailComponent } from './insured_employees/insured-employee-detail/insured-employee-detail.component';
import { InsuredEmployeeListComponent } from './insured_employees/insured-employee-list/insured-employee-list.component';
import { PaymentDueDetailComponent } from './payment_dues/payment-due-detail/payment-due-detail.component';
import { PaymentDueListComponent } from './payment_dues/payment-due-list/payment-due-list.component';
import { Case_TransferDetailComponent } from './form_encounters/case-transfer/case-transfer-detail/case-transfer-detail.component';
import { Case_TransferListComponent } from './form_encounters/case-transfer/case-transfer-list/case-transfer-list.component';
import { MedicalReportChartsComponent } from './medical-report-charts/medical-report-charts.component';
import { InsuranceCompanyDetailComponent } from './insurance_companys/insurance-company-detail/insurance-company-detail.component';
import { InsuranceCompanyListComponent } from './insurance_companys/insurance-company-list/insurance-company-list.component';
import { Review_Of_System_OptionsDetailComponent } from './review_of_system_optionss/review-of-system-options-detail/review-of-system-options-detail.component';
import { Review_Of_System_OptionsListComponent } from './review_of_system_optionss/review-of-system-options-list/review-of-system-options-list.component';
import { OrdersDetailComponent } from './form_encounters/orderss/orders-detail/orders-detail.component';
import { OrdersListComponent } from './form_encounters/orderss/orders-list/orders-list.component';
import { Lab_ResultDetailComponent } from './form_encounters/orderss/lab_results/lab-result-detail/lab-result-detail.component';
import { Lab_ResultListComponent } from './form_encounters/orderss/lab_results/lab-result-list/lab-result-list.component';

import { Lab_PanelDetailComponent } from './lab_panels/lab-panel-detail/lab-panel-detail.component';
import { Lab_PanelListComponent } from './lab_panels/lab-panel-list/lab-panel-list.component';
import { Lab_TestDetailComponent } from './lab_tests/lab-test-detail/lab-test-detail.component';
import { Lab_TestListComponent } from './lab_tests/lab-test-list/lab-test-list.component';
import { Lab_OrderDetailComponent } from './lab_orders/lab-order-detail/lab-order-detail.component';
import { Lab_OrderListComponent } from './lab_orders/lab-order-list/lab-order-list.component';
// import {Lab_OrderDetailComponent} from "./lab_orders/lab-order-detail/lab-order-detail.component"
import { DiagnosisDetailComponent } from './form_encounters/diagnosiss/diagnosis-detail/diagnosis-detail.component';
import { DiagnosisListComponent } from './form_encounters/diagnosiss/diagnosis-list/diagnosis-list.component';
import { LabListComponent } from './orderss/lab-list/lab-list.component';
import { RadListComponent } from './orderss/rad-list/rad-list.component';
import { LabDetailComponent } from './orderss/lab-detail/lab-detail.component';
import { RadDetailComponent } from './orderss/rad-detail/rad-detail.component';

import { Physiotherapy_TypeDetailComponent } from './physiotherapy_types/physiotherapy-type-detail/physiotherapy-type-detail.component';
import { Physiotherapy_TypeListComponent } from './physiotherapy_types/physiotherapy-type-list/physiotherapy-type-list.component';
import { DepartmentDetailComponent } from './departments/department-detail/department-detail.component';
import { DepartmentListComponent } from './departments/department-list/department-list.component';
import { Consent_FormListComponent } from './consent_forms/consent-form-list/consent-form-list.component';
import { Consent_FormDetailComponent } from './consent_forms/consent-form-detail/consent-form-detail.component';
import { BirthDetailComponent } from './births/birth-detail/birth-detail.component';
import { BirthListComponent } from './births/birth-list/birth-list.component';
import { VaccinationDetailComponent } from './vaccinations/vaccination-detail/vaccination-detail.component';
import { VaccinationListComponent } from './vaccinations/vaccination-list/vaccination-list.component';
import { VaccinatedDetailComponent } from './vaccinateds/vaccinated-detail/vaccinated-detail.component';
import { VaccinatedListComponent } from './vaccinateds/vaccinated-list/vaccinated-list.component';
import { DeathDetailComponent } from './deaths/death-detail/death-detail.component';
import { DeathListComponent } from './deaths/death-list/death-list.component';
import { ProcedureTypeDetailComponent } from './procedure_types/procedure-type-detail/procedure-type-detail.component';
import { ProcedureTypeListComponent } from './procedure_types/procedure-type-list/procedure-type-list.component';
import { ProcedureDetailComponent } from './procedures/procedure-detail/procedure-detail.component';
import { ProcedureListComponent } from './procedures/procedure-list/procedure-list.component';
import { OperationNoteDetailComponent } from './operation_note/operation-note-detail/operation-note-detail.component';
import { OperationNoteListComponent } from './operation_note/operation-note-list/operation-note-list.component';
import { DriverMedicalCertificateDetailComponent } from './driver_medical_certificate/driver-medical-certificate-detail/driver-medical-certificate-detail.component';
import { DriverMedicalCertificateListComponent } from './driver_medical_certificate/driver-medical-certificate-list/driver-medical-certificate-list.component';
import { PharmacyDashboardComponent } from './pharmacy-dashboard/pharmacy-dashboard.component';

import { Cancelled_AppointmentListComponent } from './cancelled_appointments/cancelled-appointment-list/cancelled-appointment-list.component';
import { Cancelled_AppointmentDetailComponent } from './cancelled_appointments/cancelled-appointment-detail/cancelled-appointment-detail.component';
import { PatientProcedureNotesComponent } from './form_encounters/patient-procedure-notes/patient-procedure-notes.component';
import { Outside_OrdersListComponent } from './outside_orderss/outside-orders-list/outside-orders-list.component';
import { Outside_OrdersDetailComponent } from './outside_orderss/outside-orders-detail/outside-orders-detail.component';
import { MedicalCertificateEmployeeDetailComponent } from './medical_certificate_employee/medical-certificate-employee-detail/medical-certificate-employee-detail.component';
import { MedicalCertificateEmployeeListComponent } from './medical_certificate_employee/medical-certificate-employee-list/medical-certificate-employee-list.component';
import { BiopsyRequestDetail } from './biopsy_request/biopsy_request.model';
import { BiopsyRequestListComponent } from './biopsy_request/biopsy-request-list/biopsy-request-list.component';
import { BiopsyRequestDetailComponent } from './biopsy_request/biopsy-request-detail/biopsy-request-detail.component';
import { MedicationAdminstrationChartDetailComponent } from './medication_adminstration_chart/medication-adminstration-chart-detail/medication-adminstration-chart-detail.component';
import { MedicationAdminstrationChartListComponent } from './medication_adminstration_chart/medication-adminstration-chart-list/medication-adminstration-chart-list.component';
import { AllergyListComponent } from './allergy/allergy-list/allergy-list.component';
import { AllergyDetailComponent } from './allergy/allergy-detail/allergy-detail.component';
import { DeliverySummaryListComponent } from './delivery_summary/delivery-summary-list/delivery-summary-list.component';
import { DeliverySummaryDetailComponent } from './delivery_summary/delivery-summary-detail/delivery-summary-detail.component';

import { NurseRecordListComponent } from './nurse_record/nurse-record-list/nurse-record-list.component';
import { NurseRecordDetailComponent } from './nurse_record/nurse-record-detail/nurse-record-detail.component';
import { ANCInitialEvaluationDetailComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-detail/anc-initial-evaluation-detail.component';
import { ANCInitialEvaluationListComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-list/anc-initial-evaluation-list.component';
import { PostNatalCareDetailComponent } from './post_natal_care/post-natal-care-detail/post-natal-care-detail.component';
import { PostNatalCareListComponent } from './post_natal_care/post-natal-care-list/post-natal-care-list.component';
import { EquipmentDataDetailComponent } from './equipment_data/equipment-data-detail/equipment-data-detail.component';
import { EquipmentDataListComponent } from './equipment_data/equipment-data-list/equipment-data-list.component';
import { PresentPregnancyFollowUpDetailComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-detail/present-pregnancy-follow-up-detail.component';
import { PresentPregnancyFollowUpListComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-list/present-pregnancy-follow-up-list.component';
import { IntrapartumCareDetailComponent } from './intrapartum_care/intrapartum-care-detail/intrapartum-care-detail.component';
import { IntrapartumCareListComponent } from './intrapartum_care/intrapartum-care-list/intrapartum-care-list.component';
import { SampleCollectionListComponent } from './sample_collection/sample-collection-list/sample-collection-list.component';
import { SampleCollectionDetailComponent } from './sample_collection/sample-collection-detail/sample-collection-detail.component';
import { RadResultApprovalListComponent } from './orderss/rad-result-approval-list/rad-result-approval-list.component';
import { LabResultApprovalListComponent } from './orderss/lab-result-approval-list/lab-result-approval-list.component';
import { DoctorConsultationDetailComponent } from './doctor_dashboard/doctor-consultation-detail/doctor-consultation-detail.component';
import { DoctorConsultationListComponent } from './doctor_dashboard/doctor-consultation-list/doctor-consultation-list.component';
import { ConsultingDetailComponent } from './form_encounters/consulting-detail/consulting-detail.component';
import { FormVitalsListComponent } from './form-vitals-list/form-vitals-list.component';
import { AncHistoryListComponent } from './anc_history/anc-history-list/anc-history-list.component';
import { AncHistoryDetailComponent } from './anc_history/anc-history-detail/anc-history-detail.component';
import { PathologyResultDetailComponent } from './pathology_result/pathology-result-detail/pathology-result-detail.component';
import { PathologyResultListComponent } from './pathology_result/pathology-result-list/pathology-result-list.component';
import { PatientDischargeListComponent } from './patient_discharge/patient-discharge-list/patient-discharge-list.component';
import { PatientDischargeDetailComponent } from './patient_discharge/patient-discharge-detail/patient-discharge-detail.component';
import { PatientReferralFormDetailComponent } from './patient_referral_form/patient-referral-form-detail/patient-referral-form-detail.component';
import { PatientReferralFormListComponent } from './patient_referral_form/patient-referral-form-list/patient-referral-form-list.component';
import { OpdReportListComponent } from './patient_report/opd-report-list/opd-report-list.component';
import { IpdReportListComponent } from './patient_report/ipd-report-list/ipd-report-list.component';
import { EdReportListComponent } from './patient_report/ed-report-list/ed-report-list.component';
import { Department_SpecialtyListComponent } from './department_specialtys/department-specialty-list/department-specialty-list.component';
import { Department_SpecialtyDetailComponent } from './department_specialtys/department-specialty-detail/department-specialty-detail.component';
import { ProcedureReportListComponent } from './reports/procedure-report-list/procedure-report-list.component';
import { LabReportListComponent } from './lab_report/lab-report-list/lab-report-list.component';
import { EncounterReportListComponent } from './reports/encounter-report-list/encounter-report-list.component';
import { PatientReportList } from './reports/patient-report-list/patient-report-list.component';
import { PrescriptionAndDispensationReportListComponent } from './reports/prescription-and-dispensation-report-list/prescription-and-dispensation-report-list.component';
import { ReferralsReportListComponent } from './reports/referrals-report-list/referrals-report-list.component';
import { DispenseHistoryDetailComponent } from './dispense_history/dispense-history-detail/dispense-history-detail.component';
import { DispenseHistoryListComponent } from './dispense_history/dispense-history-list/dispense-history-list.component';
import { HemodialysisScheduledPateintDetailComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-detail/hemodialysis-scheduled-pateint-detail.component';
import { HemodialysisScheduledPateintListComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-list/hemodialysis-scheduled-pateint-list.component';
import { DialysisMachineListComponent } from './dialysis_machine/dialysis-machine-list/dialysis-machine-list.component';
import { DialysisMachineDetailComponent } from './dialysis_machine/dialysis-machine-detail/dialysis-machine-detail.component';
import { SurgeryRoomListComponent } from './surgery_room/surgery-room-list/surgery-room-list.component';
import { SurgeryRoomDetailComponent } from './surgery_room/surgery-room-detail/surgery-room-detail.component';
import { ResultApprovalListComponent } from './orderss/result-approval-list/result-approval-list.component';
import { OverviewComponent } from './form_encounters/overview/overview.component';
import { OtherServicesDetailComponent } from './other_services/other-services-detail/other-services-detail.component';
import { OtherServicesListComponent } from './other_services/other-services-list/other-services-list.component';
import { OrderedOtheServiceDetailComponent } from './ordered_othe_service/ordered-othe-service-detail/ordered-othe-service-detail.component';
import { OrderedOtheServiceListComponent } from './ordered_othe_service/ordered-othe-service-list/ordered-othe-service-list.component';
import { ResultProgressListComponent } from './result-progress-list/result-progress-list.component';
import { PhysicalExaminationOptionsDetailComponent } from './physical_examination_options/physical-examination-options-detail/physical-examination-options-detail.component';
import { PhysicalExaminationOptionsListComponent } from './physical_examination_options/physical-examination-options-list/physical-examination-options-list.component';
import { VitalsComponent } from './vitals/vitals.component';

import { RoomDetailComponent } from './room/room-detail/room-detail.component';
import { RoomListComponent } from './room/room-list/room-list.component';
import { QueueDetailComponent } from './queue/queue-detail/queue-detail.component';
import { QueueListComponent } from './queue/queue-list/queue-list.component';

import { PaymentReportListComponent } from './reports/payment-report-list/payment-report-list.component';
import { DialysisListComponent } from './dialysis/dialysis-list/dialysis-list.component';
import { DialysisDetailComponent } from './dialysis/dialysis-detail/dialysis-detail.component';
import { DialysisSessionDetailComponent } from './dialysis_session/dialysis-session-detail/dialysis-session-detail.component';
import { DialysisSessionListComponent } from './dialysis_session/dialysis-session-list/dialysis-session-list.component';
import { DialysisOrderComponent } from './dialysis-order/dialysis-order.component';
import { PatientIntakeFormDetailComponent } from './patient_intake_form/patient-intake-form-detail/patient-intake-form-detail.component';
import { AdditionalServiceDetailComponent } from './additional_service/additional-service-detail/additional-service-detail.component';
import { AdditionalServiceListComponent } from './additional_service/additional-service-list/additional-service-list.component';
import { PhysiotherapyComponent } from './physiotherapy/physiotherapy.component';
import { PhysiotherapySessionDetailComponent } from './physiotherapy_session/physiotherapy-session-detail/physiotherapy-session-detail.component';
import { PhysiotherapySessionListComponent } from './physiotherapy_session/physiotherapy-session-list/physiotherapy-session-list.component';
import { RehabilitationOrderDetailComponent } from './rehabilitation-order-detail/rehabilitation-order-detail.component';
import { ProceduresComponent } from './form_encounters/procedures/procedures.component';
import { Procedure_OrderListComponent } from './form_encounters/procedure_orders/procedure-order-list/procedure-order-list.component';
import { PatientProcedureListComponent } from './patient-procedure-list/patient-procedure-list.component';
import { ItemCategoryDetailComponent } from './item_category/item-category-detail/item-category-detail.component';
import { ItemCategoryListComponent } from './item_category/item-category-list/item-category-list.component';
import { PurchaseRequestDetailComponent } from './purchase_request/purchase-request-detail/purchase-request-detail.component';
import { PurchaseRequestListComponent } from './purchase_request/purchase-request-list/purchase-request-list.component';
import { ItemReceiveDetailComponent } from './item_receive/item-receive-detail/item-receive-detail.component';
import { ItemReceiveListComponent } from './item_receive/item-receive-list/item-receive-list.component';
import { InterchangeableItemDetailComponent } from './interchangeable_item/interchangeable-item-detail/interchangeable-item-detail.component';
import { InterchangeableItemListComponent } from './interchangeable_item/interchangeable-item-list/interchangeable-item-list.component';
import { StoreStockLevelDetailComponent } from './store_stock_level/store-stock-level-detail/store-stock-level-detail.component';
import { StoreStockLevelListComponent } from './store_stock_level/store-stock-level-list/store-stock-level-list.component';
import { PrescriptionRequestDetailComponent } from './prescription_request/prescription-request-detail/prescription-request-detail.component';
import { PrescriptionRequestListComponent } from './prescription_request/prescription-request-list/prescription-request-list.component';
import { PrescriptionRequestByPatientListComponent } from './prescription_request/prescription-request-by-patient-list/prescription-request-by-patient-list.component';
import { PrescriptionRequestByPatientDetailComponent } from './prescription_request/prescription-request-by-patient-detail/prescription-request-by-patient-detail.component';
import { PrescriptionsDetailComponent } from './form_encounters/prescriptions-detail/prescriptions-detail.component';
import { ItemDispatchDetailComponent } from './item_dispatch/item-dispatch-detail/item-dispatch-detail.component';
import { ItemDispatchListComponent } from './item_dispatch/item-dispatch-list/item-dispatch-list.component';
import { ItemReturnListComponent } from './item_return/item-return-list/item-return-list.component';
import { ItemReturnDetailComponent } from './item_return/item-return-detail/item-return-detail.component';
import { ApprovedRequestsComponent } from './requests/approved-requests/approved-requests.component';
import { ItemReturnGroupListComponent } from './item_return/item-return-group-list/item-return-group-list.component';
import { SellReportComponent } from './reports/sell-report/sell-report.component';
import { ItemExpireDateReportListComponent } from './reports/item-expire-date-report-list/item-expire-date-report-list.component';
import { RadiologyResultSettingsDetailComponent } from './radiology_result_settings/radiology-result-settings-detail/radiology-result-settings-detail.component';
import { RadiologyResultSettingsListComponent } from './radiology_result_settings/radiology-result-settings-list/radiology-result-settings-list.component';
import { RadiologyResultDetailComponent } from './radiology_result/radiology-result-detail/radiology-result-detail.component';
import { RadiologyResultListComponent } from './radiology_result/radiology-result-list/radiology-result-list.component';
import { ChiefCompliantOptionDetailComponent } from './chief_compliant_option/chief-compliant-option-detail/chief-compliant-option-detail.component';
import { ChiefCompliantOptionListComponent } from './chief_compliant_option/chief-compliant-option-list/chief-compliant-option-list.component';
import { RiskFactorsOptionsDetailComponent } from './risk_factors_options/risk-factors-options-detail/risk-factors-options-detail.component';
import { RiskFactorsOptionsListComponent } from './risk_factors_options/risk-factors-options-list/risk-factors-options-list.component';
import { FinalAssessmentDetailComponent } from './final_assessment/final-assessment-detail/final-assessment-detail.component';
import { FinalAssessmentListComponent } from './final_assessment/final-assessment-list/final-assessment-list.component';
import { ReviewOfSystemListComponent } from './review-of-system-list/review-of-system-list.component';
import { ReviewOfSystemDetailComponent } from './review-of-system-detail/review-of-system-detail.component';
import { HomeComponent } from './home/home.component';
import { TopTenDiseasListComponent } from './top-ten-diseas-list/top-ten-diseas-list.component';
import { FormClinicalInstructionDetail } from './form_clinical_instruction/form_clinical_instruction.model';
import { FormClinicalInstructionDetailComponent } from './form_clinical_instruction/form-clinical-instruction-detail/form-clinical-instruction-detail.component';
import { RiskFactorDetailComponent } from './risk_factor/risk-factor-detail/risk-factor-detail.component';
import { RiskFactorListComponent } from './risk_factor/risk-factor-list/risk-factor-list.component';
import { NewBornNatalCareListComponent } from './new_born_natal_care/new-born-natal-care-list/new-born-natal-care-list.component';
import { NewBornNatalCareDetailComponent } from './new_born_natal_care/new-born-natal-care-detail/new-born-natal-care-detail.component';
import { FormSoapDetailComponent } from './form_encounters/form-soap-detail/form-soap-detail.component';
import { FormVitalsDetailComponent } from './form_encounters/form-vitals-detail/form-vitals-detail.component';
import { SurgeryAppointmentsComponent } from './surgery_appointments/surgery-appointments/surgery-appointments.component';
import { PaymentDiscountRequestDetailComponent } from './payment_discount_request/payment-discount-request-detail/payment-discount-request-detail.component';
import { PaymentDiscountRequestListComponent } from './payment_discount_request/payment-discount-request-list/payment-discount-request-list.component';
import { MedicalCheckupPackageDetailComponent } from './medical_checkup_package/medical-checkup-package-detail/medical-checkup-package-detail.component';
import { MedicalCheckupPackageListComponent } from './medical_checkup_package/medical-checkup-package-list/medical-checkup-package-list.component';
import { MedicalCheckupServiceDetailComponent } from './medical_checkup_service/medical-checkup-service-detail/medical-checkup-service-detail.component';
import { MedicalCheckupServiceListComponent } from './medical_checkup_service/medical-checkup-service-list/medical-checkup-service-list.component';
import { DepositOptionDetailComponent } from './deposit_option/deposit-option-detail/deposit-option-detail.component';
import { DepositOptionListComponent } from './deposit_option/deposit-option-list/deposit-option-list.component';
import { CreditEmployeeFamilyDetailComponent } from './credit_employee_family/credit-employee-family-detail/credit-employee-family-detail.component';
import { CreditEmployeeFamilyListComponent } from './credit_employee_family/credit-employee-family-list/credit-employee-family-list.component';
import { MarkupPriceListComponent } from './markup_price/markup-price-list/markup-price-list.component';
import { MarkupPriceDetailComponent } from './markup_price/markup-price-detail/markup-price-detail.component';
import { HolidayDetailComponent } from './holiday/holiday-detail/holiday-detail.component';
import { HolidayListComponent } from './holiday/holiday-list/holiday-list.component';
import { CompanyCreditServicePackageDetailComponent } from './company_credit_service_package/company-credit-service-package-detail/company-credit-service-package-detail.component';
import { CompanyCreditServicePackageListComponent } from './company_credit_service_package/company-credit-service-package-list/company-credit-service-package-list.component';
import { NurseHistoryDetailComponent } from './nurse_history/nurse-history-detail/nurse-history-detail.component';
import { NurseHistoryListComponent } from './nurse_history/nurse-history-list/nurse-history-list.component';
import { PlanOfCareDetailComponent } from './plan_of_care/plan-of-care-detail/plan-of-care-detail.component';
import { PlanOfCareListComponent } from './plan_of_care/plan-of-care-list/plan-of-care-list.component';
import { NursePlanOfCareDetailComponent } from './nurse_plan_of_care/nurse-plan-of-care-detail/nurse-plan-of-care-detail.component';
import { NursePlanOfCareListComponent } from './nurse_plan_of_care/nurse-plan-of-care-list/nurse-plan-of-care-list.component';
import { TatReportListComponent } from './reports/tat-report-list/tat-report-list.component';
import { BeforeInductionOfAnaesthesiaDetailComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-detail/before-induction-of-anaesthesia-detail.component';
import { BeforeInductionOfAnaesthesiaListComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-list/before-induction-of-anaesthesia-list.component';
import { BeforePatientLeavesOperatingRoomDetailComponent } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-detail/before-patient-leaves-operating-room-detail.component';
import { BeforePatientLeavesOperatingRoomListComponent } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-list/before-patient-leaves-operating-room-list.component';
import { BeforeSkinIncisionDetailComponent } from './before_skin_incision/before-skin-incision-detail/before-skin-incision-detail.component';
import { BeforeSkinIncisionListComponent } from './before_skin_incision/before-skin-incision-list/before-skin-incision-list.component';
import { SurgicalSafetyChecklistComponent } from './surgical-safety-checklist/surgical-safety-checklist.component';
import { EmergencyObservationDetailComponent } from './emergency_observation/emergency-observation-detail/emergency-observation-detail.component';
import { EmergencyObservationListComponent } from './emergency_observation/emergency-observation-list/emergency-observation-list.component';
import { lab_order_type } from './app.enums';
import { ItemRequestDetailComponent } from './item_request/item-request-detail/item-request-detail.component';
import { ItemRequestListComponent } from './item_request/item-request-list/item-request-list.component';
import { EmployeeRequestComponent } from './employee_request/employee-request/employee-request.component';
import { EmployeeRequestDetailComponent } from './employee_request/employee-request-detail/employee-request-detail.component';
import { OutsidePrescriptionDetailComponent } from './outside_prescription/outside-prescription-detail/outside-prescription-detail.component';
import { OutsidePrescriptionListComponent } from './outside_prescription/outside-prescription-list/outside-prescription-list.component';
import { MeasuringUnitListComponent } from './measuring_unit/measuring-unit-list/measuring-unit-list.component';
import { MeasuringUnitDetailComponent } from './measuring_unit/measuring-unit-detail/measuring-unit-detail.component';
import { BankDetailComponent } from './bank/bank-detail/bank-detail.component';
import { BankListComponent } from './bank/bank-list/bank-list.component';
import { OpdObservationDetailComponent } from './opd_observation/opd-observation-detail/opd-observation-detail.component';
import { OpdObservationListComponent } from './opd_observation/opd-observation-list/opd-observation-list.component';
import { IcuPhysicalAssessmentDetailComponent } from './icu_physical_assessment/icu-physical-assessment-detail/icu-physical-assessment-detail.component';
import { IcuPhysicalAssessmentListComponent } from './icu_physical_assessment/icu-physical-assessment-list/icu-physical-assessment-list.component';
import { ProcedureTemplateDetailComponent } from './procedure_template/procedure_template-detail/prcedure-template-detail.component';
import { OutsourcingCompanyDetailComponent } from './outsourcing_companys/outsourcing_company-detail/outsourcing_company-detail.component';
import { OutsourcingCompanyListComponent } from './outsourcing_companys/outsourcing_companys-list/outsourcing_company-list.component';
import { CorelatedFeeDetailComponent } from './corelated_fee/corelated-fee-detail/corelated-fee-detail.component';
import { CorelatedFeeListComponent } from './corelated_fee/corelated-fee-list/corelated-fee-list.component';
import { PosMachineDetailComponent } from './pos_machine/pos-machine-detail/pos-machine-detail.component';
import { PosMachineListComponent } from './pos_machine/pos-machine-list/pos-machine-list.component';
import { PosMachineUserDetailComponent } from './pos_machine_user/pos-machine-user-detail/pos-machine-user-detail.component';
import { PosMachineUserListComponent } from './pos_machine_user/pos-machine-user-list/pos-machine-user-list.component';
import { PreAnsthesiaEvaluationDetailComponent } from './pre_ansthesia_evaluation/pre-ansthesia-evaluation-detail/pre-ansthesia-evaluation-detail.component';
import { FlowSheetDetailComponent } from './flow_sheet/flow-sheet-detail/flow-sheet-detail.component';
import { FlowSheetListComponent } from './flow_sheet/flow-sheet-list/flow-sheet-list.component';
import { SessionTherapyDetailComponent } from './session_therapy/session-therapy-detail/session-therapy-detail.component';
import { SessionTherapyListComponent } from './session_therapy/session-therapy-list/session-therapy-list.component';
import { GroupMemberListComponent } from './group_member/group-member-list/group-member-list.component';
import { GroupMemberDetailComponent } from './group_member/group-member-detail/group-member-detail.component';
import { PrescriptionListComponent } from './prescription-list/prescription-list.component';
import { NotificationComponent } from './notification/notification.component';
import { WardReportDetailComponent } from './ward_report/ward-report-detail/ward-report-detail.component';
import { WardReportListComponent } from './ward_report/ward-report-list/ward-report-list.component';
import { PatientWardReportDetailComponent } from './patient_ward_report/patient-ward-report-detail/patient-ward-report-detail.component';
import { PatientWardReportListComponent } from './patient_ward_report/patient-ward-report-list/patient-ward-report-list.component';
import { MachinesDetailComponent } from './machines/machines-detail/machines-detail.component'
import { MachinesListComponent } from './machines/machines-list/machines-list.component';
import { MachineTestListComponent } from './machines/machine_test/machine-test-list/machine-test-list.component';
import { MachineTestDetailComponent } from './machines/machine_test/machine-test-detail/machine-test-detail.component';
import { DailyReportListComponent } from './daily_report/daily-report-list/daily-report-list.component';
import { DailyReportDetailComponent } from './daily_report/daily-report-detail/daily-report-detail.component';
import { PsychoterapyProgressNoteDetailComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-detail/psychoterapy-progress-note-detail.component';
import { PsychoterapyProgressNoteListComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-list/psychoterapy-progress-note-list.component';
import { QueuesListComponent } from './queues/queues-list/queues-list.component';
import { QueuesDetailComponent } from './queues/queues-detail/queues-detail.component';
import {CreditPaymentListComponent} from "./credit_payment/credit-payment-list/credit-payment-list.component";
import {CreditPaymentDetailComponent} from "./credit_payment/credit-payment-detail/credit-payment-detail.component";
import {
  CreditPaymentHistoryDetailComponent
} from "./credit_payment_history/credit-payment-history-detail/credit-payment-history-detail.component";
import {
  CreditPaymentHistoryListComponent
} from "./credit_payment_history/credit-payment-history-list/credit-payment-history-list.component";



const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: 'invalid', component: InvalidOperationComponent },
  { path: 'tos', component: TosComponent },
  { path: 'mandatory', component: MandatoryComponent },

  { path: 'messages/:id', component: MessagesDetailComponent },
  { path: 'messages', component: MessagesListComponent },

  { path: 'tgidentitys/:id', component: TgidentityDetailComponent },
  { path: 'tgidentitys', component: TgidentityListComponent },

  { path: 'patients/:id', component: PatientDetailComponent },
  { path: 'patients', component: PatientListComponent },

  { path: 'patient-home', component: PatientsHomeComponent },

  { path: 'lost_damages/:id', component: LostDamageDetailComponent },
  { path: 'lost_damages', component: LostDamageListComponent },
  { path: 'items/:id', component: ItemDetailComponent },
  { path: 'items', component: ItemListComponent },
  { path: 'stores/:id', component: StoreDetailComponent },
  { path: 'stores', component: StoreListComponent },

  { path: 'allergys/:id', component: AllergyDetailComponent },
  { path: 'allergys', component: AllergyListComponent },

  {
    path: 'item_ledger_entrys/:id',
    component: Item_Ledger_EntryDetailComponent,
  },
  { path: 'suppliers/:id', component: SupplierDetailComponent },
  { path: 'suppliers', component: SupplierListComponent },
  { path: 'drugtypes/:id', component: DrugtypeDetailComponent },
  { path: 'drugtypes', component: DrugtypeListComponent },
  { path: 'item_ledger_entrys', component: Item_Ledger_EntryListComponent },
  { path: 'transfers/:id', component: TransferDetailComponent },
  { path: 'transfers', component: TransferListComponent },
  { path: 'purchases/:id', component: PurchaseDetailComponent },
  { path: 'purchases', component: PurchaseListComponent },

  { path: 'payments/:id', component: PaymentDetailComponent },
  { path: 'payments', component: PaymentListComponent },

  { path: 'appointments/:id', component: AppointmentDetailComponent },
  { path: 'appointments', component: AppointmentListComponent },

  { path: 'availabilitys/:id', component: AvailabilityDetailComponent },
  { path: 'availabilitys', component: AvailabilityListComponent },
  { path: 'transfers', component: TransferListComponent },

  { path: 'employees/:id', component: EmployeeDetailComponent },
  { path: 'employees', component: EmployeeListComponent },

  { path: 'employee-home', component: EmployeeHomeComponent },

  { path: 'doctor-home', component: DoctorSelfComponent },
  { path: 'reception-home', component: ReceptionSelfComponent },

  { path: 'lab-home', component: LabSelfComponent },

  { path: 'receptions/:id', component: ReceptionDetailComponent },
  { path: 'receptions', component: ReceptionListComponent },

  { path: 'medical_practitioners/:id', component: DoctorDetailComponent },
  { path: 'medical_practitioners', component: DoctorListComponent },
  { path: 'inventory_reports/:id', component: ReportDetailComponent },
  { path: 'inventory_reports', component: ReportListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'item_in_stores/:id', component: Item_In_StoreDetailComponent },
  { path: 'item_in_stores', component: Item_In_StoreListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },
  { path: 'surgery_types/:id', component: Surgery_TypeDetailComponent },
  { path: 'surgery_types', component: Surgery_TypeListComponent },
  {
    path: 'surgery_appointments/:id',
    component: Surgery_AppointmentDetailComponent,
  },
  { path: 'surgery_appointments', component: Surgery_AppointmentListComponent },
  { path: 'deposits/:id', component: DepositDetailComponent },
  { path: 'deposits', component: DepositListComponent },
  { path: '', component: WelcomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: 'invalid', component: InvalidOperationComponent },
  { path: 'tos', component: TosComponent },
  { path: 'mandatory', component: MandatoryComponent },

  { path: 'users/:id', component: UserDetailComponent },
  { path: 'users', component: UserListComponent },
  { path: 'groups/:id', component: GroupDetailComponent },
  { path: 'groups', component: GroupListComponent },
  { path: 'resources/:id', component: ResourceDetailComponent },
  { path: 'resources', component: ResourceListComponent },
  { path: 'eventlogs/:id', component: EventlogDetailComponent },
  { path: 'eventlogs', component: EventlogListComponent },
  { path: 'settings/:id', component: SettingDetailComponent },
  { path: 'settings', component: SettingListComponent },
  { path: 'files/:id', component: FileDetailComponent },
  { path: 'files', component: FileListComponent },
  { path: 'mails/:id', component: MailDetailComponent },
  { path: 'mails', component: MailListComponent },
  { path: 'locks/:id', component: LockDetailComponent },
  { path: 'locks', component: LockListComponent },
  { path: 'cases/:id', component: CaseDetailComponent },
  { path: 'cases', component: CaseListComponent },

  { path: 'messagess/:id', component: MessagesDetailComponent },
  { path: 'messagess', component: MessagesListComponent },
  { path: 'stats/:id', component: StatDetailComponent },
  { path: 'stats', component: StatListComponent },
  { path: 'jobs/:id', component: JobDetailComponent },
  { path: 'jobs', component: JobListComponent },
  { path: 'tgidentitys/:id', component: TgidentityDetailComponent },
  { path: 'tgidentitys', component: TgidentityListComponent },

  { path: 'patients/:id', component: PatientDetailComponent },
  { path: 'patients', component: PatientListComponent },

  { path: 'patient-home', component: PatientsHomeComponent },

  { path: 'lostDamages/:id', component: LostDamageDetailComponent },
  { path: 'lostDamages', component: LostDamageListComponent },
  { path: 'items/:id', component: ItemDetailComponent },
  { path: 'items', component: ItemListComponent },
  { path: 'stores/:id', component: StoreDetailComponent },
  { path: 'stores', component: StoreListComponent },
  { path: 'allergys/:id', component: AllergyDetailComponent },
  { path: 'allergyszz', component: AllergyListComponent },

  {
    path: 'item_ledger_entrys/:id',
    component: Item_Ledger_EntryDetailComponent,
  },
  { path: 'suppliers/:id', component: SupplierDetailComponent },
  { path: 'suppliers', component: SupplierListComponent },
  { path: 'drugtypes/:id', component: DrugtypeDetailComponent },
  { path: 'drugtypes', component: DrugtypeListComponent },
  { path: 'item_ledger_entrys', component: Item_Ledger_EntryListComponent },
  { path: 'transfers/:id', component: TransferDetailComponent },
  { path: 'transfers', component: TransferListComponent },

  { path: 'payments/:id', component: PaymentDetailComponent },
  { path: 'payments', component: PaymentListComponent },

  { path: 'appointments/:id', component: AppointmentDetailComponent },
  { path: 'appointments', component: AppointmentListComponent },

  { path: 'availabilitys/:id', component: AvailabilityDetailComponent },
  { path: 'availabilitys', component: AvailabilityListComponent },
  { path: 'transfers', component: TransferListComponent },

  { path: 'store_employees/:id', component: EmployeeDetailComponent },
  { path: 'store_employees', component: EmployeeListComponent },

  { path: 'employee-home', component: EmployeeHomeComponent },

  { path: 'doctor-home', component: DoctorSelfComponent },
  { path: 'reception-home', component: ReceptionSelfComponent },

  { path: 'lab-home', component: LabSelfComponent },

  { path: 'receptions/:id', component: ReceptionDetailComponent },
  { path: 'receptions', component: ReceptionListComponent },

  { path: 'doctors/:id', component: DoctorDetailComponent },
  { path: 'doctors', component: DoctorListComponent },
  { path: 'reports/:id', component: ReportDetailComponent },
  { path: 'reports', component: ReportListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'item_in_stores/:id', component: Item_In_StoreDetailComponent },
  { path: 'item_in_stores', component: Item_In_StoreListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },


  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'vitals/:id', component: VitalDetailComponent },
  { path: 'vitals', component: VitalListComponent },
  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },
  { path: 'surgery_types/:id', component: Surgery_TypeDetailComponent },
  { path: 'surgery_types', component: Surgery_TypeListComponent },
  { path: 'employee_item_requests', component: EmployeeRequestComponent },
  { path: 'employee_item_requests/:id', component: EmployeeRequestDetailComponent },
  {
    path: 'surgery_appointments/:id',
    component: Surgery_AppointmentDetailComponent,
  },
  { path: 'surgery_appointments', component: Surgery_AppointmentListComponent },
  { path: 'deposits/:id', component: DepositDetailComponent },
  { path: 'deposits', component: DepositListComponent },
  { path: '', component: WelcomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: 'invalid', component: InvalidOperationComponent },
  { path: 'tos', component: TosComponent },
  { path: 'mandatory', component: MandatoryComponent },

  { path: 'users/:id', component: UserDetailComponent },
  { path: 'users', component: UserListComponent },
  { path: 'groups/:id', component: GroupDetailComponent },
  { path: 'groups', component: GroupListComponent },
  { path: 'resources/:id', component: ResourceDetailComponent },
  { path: 'resources', component: ResourceListComponent },
  { path: 'eventlogs/:id', component: EventlogDetailComponent },
  { path: 'eventlogs', component: EventlogListComponent },
  { path: 'settings/:id', component: SettingDetailComponent },
  { path: 'settings', component: SettingListComponent },
  { path: 'files/:id', component: FileDetailComponent },
  { path: 'files', component: FileListComponent },
  { path: 'mails/:id', component: MailDetailComponent },
  { path: 'mails', component: MailListComponent },
  { path: 'locks/:id', component: LockDetailComponent },
  { path: 'locks', component: LockListComponent },
  { path: 'cases/:id', component: CaseDetailComponent },
  { path: 'cases', component: CaseListComponent },

  { path: 'messagess/:id', component: MessagesDetailComponent },
  { path: 'messagess', component: MessagesListComponent },
  { path: 'stats/:id', component: StatDetailComponent },
  { path: 'stats', component: StatListComponent },
  { path: 'jobs/:id', component: JobDetailComponent },
  { path: 'jobs', component: JobListComponent },
  { path: 'tgidentitys/:id', component: TgidentityDetailComponent },
  { path: 'tgidentitys', component: TgidentityListComponent },

  { path: 'patients/:id', component: PatientDetailComponent },
  { path: 'patients', component: PatientListComponent },

  { path: 'patient-home', component: PatientsHomeComponent },
  { path: 'dialysis_machines/:id', component: DialysisMachineDetailComponent },
  { path: 'dialysis_machines', component: DialysisMachineListComponent },

  { path: 'hemodialysis_scheduled_pateints/:id', component: HemodialysisScheduledPateintDetailComponent },
  { path: 'hemodialysis_scheduled_pateints', component: HemodialysisScheduledPateintListComponent },


  { path: 'lostDamages/:id', component: LostDamageDetailComponent },
  { path: 'lostDamages', component: LostDamageListComponent },
  { path: 'items/:id', component: ItemDetailComponent },
  { path: 'items', component: ItemListComponent },
  { path: 'stores/:id', component: StoreDetailComponent },
  { path: 'stores', component: StoreListComponent },

  { path: 'requests/:id', component: RequestDetailComponent },
  { path: 'requests', component: ApprovedRequestsComponent },



  {
    path: 'item_ledger_entrys/:id',
    component: Item_Ledger_EntryDetailComponent,
  },
  { path: 'suppliers/:id', component: SupplierDetailComponent },
  { path: 'suppliers', component: SupplierListComponent },
  { path: 'drugtypes/:id', component: DrugtypeDetailComponent },
  { path: 'drugtypes', component: DrugtypeListComponent },
  { path: 'item_ledger_entrys', component: Item_Ledger_EntryListComponent },
  { path: 'transfers/:id', component: TransferDetailComponent },
  { path: 'transfers', component: TransferListComponent },

  { path: 'payments/:id', component: PaymentDetailComponent },
  { path: 'payments', component: PaymentListComponent },

  { path: 'appointments/:id', component: AppointmentDetailComponent },
  { path: 'appointments', component: AppointmentListComponent },

  { path: 'availabilitys/:id', component: AvailabilityDetailComponent },
  { path: 'availabilitys', component: AvailabilityListComponent },
  { path: 'transfers', component: TransferListComponent },

  { path: 'employees/:id', component: EmployeeDetailComponent },
  { path: 'employees', component: EmployeeListComponent },

  { path: 'employee-home', component: EmployeeHomeComponent },

  { path: 'doctor-home', component: DoctorSelfComponent },
  { path: 'reception-home', component: ReceptionSelfComponent },

  { path: 'nurse-home', component: NurseSelfComponent },
  { path: 'lab-home', component: LabSelfComponent },


  // { path: 'queues/:id', component: QueueDetailComponent },
  // { path: 'queues', component: QueueListComponent },

  { path: 'ordered_othe_services/:id', component: OrderedOtheServiceDetailComponent },
  { path: 'ordered_othe_services', component: OrderedOtheServiceListComponent },

  { path: 'other_servicess/:id', component: OtherServicesDetailComponent },
  { path: 'other_servicess', component: OtherServicesListComponent },

  { path: 'receptions/:id', component: ReceptionDetailComponent },
  { path: 'receptions', component: ReceptionListComponent },

  { path: 'doctors/:id', component: DoctorDetailComponent },
  { path: 'doctors', component: DoctorListComponent },
  { path: 'reports/:id', component: ReportDetailComponent },
  { path: 'reports', component: ReportListComponent },

  { path: 'rooms/:id', component: RoomDetailComponent },
  { path: 'rooms', component: RoomListComponent },

  { path: 'vitals_nurse/:id', component: VitalsComponent },

  { path: 'procedures/:id', component: ProceduresComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'item_in_stores/:id', component: Item_In_StoreDetailComponent },
  { path: 'item_in_stores', component: Item_In_StoreListComponent },

  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },


  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },

  { path: 'vitals/:id', component: VitalDetailComponent },
  { path: 'vitals', component: FormVitalsListComponent },
  { path: 'fees/:id', component: FeeDetailComponent },
  { path: 'fees', component: FeeListComponent },
  { path: 'fee_types/:id', component: Fee_TypeDetailComponent },
  { path: 'fee_types', component: Fee_TypeListComponent },
  { path: 'surgery_types/:id', component: Surgery_TypeDetailComponent },
  { path: 'surgery_types', component: Surgery_TypeListComponent },
  {
    path: 'surgery_appointments/:id',
    component: Surgery_AppointmentDetailComponent,
  },
  { path: 'surgery_appointments', component: Surgery_AppointmentListComponent },
  { path: 'deposits/:id', component: DepositDetailComponent },
  { path: 'deposits', component: DepositListComponent },

  { path: 'vitals/:id', component: VitalDetailComponent },
  { path: 'vitals', component: VitalListComponent },
  { path: 'form_encounters/:id', component: Form_EncounterDetailComponent },
  { path: 'form_encounters', component: Form_EncounterListComponent },

  { path: 'procedure_orders/:id', component: Procedure_OrderDetailComponent },

  {
    path: 'procedure_providers/:id',
    component: Procedure_ProviderDetailComponent,
  },
  { path: 'procedure_providers', component: Procedure_ProviderListComponent },

  { path: 'case_transfers/:id', component: Case_TransferDetailComponent },
  { path: 'case_transfers', component: Case_TransferListComponent },
  { path: 'procedure_testss/:id', component: ProcedureTestsDetailComponent },
  { path: 'procedure_testss', component: Procedure_TestsListComponent },

  {
    path: 'procedure_payment_dues/:id',
    component: ProcedurePaymentDueDetailComponent,
  },
  {
    path: 'procedure_payment_dues',
    component: Procedure_Payment_DueListComponent,
  },
  { path: 'companys/:id', component: CompanyDetailComponent },
  { path: 'companys', component: CompanyListComponent },
  { path: 'outsourcing_companys/:id', component: OutsourcingCompanyDetailComponent },
  { path: 'outsourcing_companys', component: OutsourcingCompanyListComponent },
  { path: 'insured_employees/:id', component: InsuredEmployeeDetailComponent },
  { path: 'insured_employees', component: InsuredEmployeeListComponent },
  { path: 'payment_dues/:id', component: PaymentDueDetailComponent },
  { path: 'payment_dues', component: PaymentDueListComponent },

  { path: 'medical_report_charts', component: MedicalReportChartsComponent },
  {
    path: 'insurance_companys/:id',
    component: InsuranceCompanyDetailComponent,
  },
  { path: 'laboratory_reports', component: LabReportListComponent, data: { diagnosis_type: lab_order_type.lab } },
  { path: 'radiology_reports', component: LabReportListComponent, data: { diagnosis_type: lab_order_type.imaging } },
  { path: 'pathology_reports', component: LabReportListComponent, data: { diagnosis_type: lab_order_type.pathology } },
  { path: 'insurance_companys', component: InsuranceCompanyListComponent },

  { path: 'order_givens/:id', component: Lab_OrderDetailComponent },
  { path: 'order_givens', component: Lab_OrderListComponent },

  { path: 'orders/:id', component: OrdersDetailComponent },
  { path: 'orders', component: OrdersListComponent },

  { path: 'lab_panels/:id', component: Lab_PanelDetailComponent },
  { path: 'lab_panels', component: Lab_PanelListComponent },

  { path: 'patient_referral_forms/:id', component: PatientReferralFormDetailComponent },
  { path: 'patient_referral_forms', component: PatientReferralFormListComponent },

  { path: 'patient_discharges/:id', component: PatientDischargeDetailComponent },
  { path: 'patient_discharges', component: PatientDischargeListComponent },

  { path: 'lab_tests/:id', component: Lab_TestDetailComponent },
  { path: 'lab_tests', component: Lab_TestListComponent },
  {
    path: 'review_of_system_options/:id',
    component: Review_Of_System_OptionsDetailComponent,
  },
  {
    path: 'review_of_system_options',
    component: Review_Of_System_OptionsListComponent,
  },
  {
    path: 'review_of_systems/:id',
    component: ReviewOfSystemDetailComponent,
  },

  { path: 'diagnosiss/:id', component: DiagnosisDetailComponent },
  { path: 'diagnosiss', component: DiagnosisListComponent },
  { path: 'lab_results/:id', component: Lab_ResultDetailComponent },
  { path: 'lab_results', component: Lab_ResultListComponent },

  { path: 'radiology_orders', component: RadListComponent },
  { path: 'laboratory_orders', component: LabListComponent },

  { path: 'consent_forms/:id', component: Consent_FormDetailComponent },
  { path: 'consent_forms', component: Consent_FormListComponent },
  { path: 'outside_orders/:id', component: Outside_OrdersDetailComponent },
  { path: 'outside_orders', component: Outside_OrdersListComponent },

  { path: 'radiology_orders/:id', component: RadDetailComponent },
  { path: 'laboratory_orders/:id', component: LabDetailComponent },

  {
    path: 'physiotherapy_types/:id',
    component: Physiotherapy_TypeDetailComponent,
  },
  { path: 'physiotherapy_types', component: Physiotherapy_TypeListComponent },

  { path: 'departments/:id', component: DepartmentDetailComponent },
  { path: 'departments', component: DepartmentListComponent },
  { path: 'births/:id', component: BirthDetailComponent },
  { path: 'births', component: BirthListComponent },
  { path: 'vaccinations/:id', component: VaccinationDetailComponent },
  { path: 'vaccinations', component: VaccinationListComponent },
  { path: 'vaccinateds/:id', component: VaccinatedDetailComponent },
  { path: 'vaccinateds', component: VaccinatedListComponent },
  { path: 'deaths/:id', component: DeathDetailComponent },
  { path: 'deaths', component: DeathListComponent },
  { path: 'procedure_types/:id', component: ProcedureTypeDetailComponent },
  { path: 'procedure_types', component: ProcedureTypeListComponent },
  { path: 'procedures/:id', component: ProcedureDetailComponent },
  { path: 'procedures', component: ProcedureListComponent },
  {
    path: 'patient_procedure_notes/:id',
    component: PatientProcedureNotesComponent,
  },

  { path: 'patient_precedure', component: PatientProcedureListComponent },
  {
    path: 'cancelled_appointments/:id',
    component: Cancelled_AppointmentDetailComponent,
  },
  {
    path: 'cancelled_appointments',
    component: Cancelled_AppointmentListComponent,
  },

  { path: 'operation_notes/:id', component: OperationNoteDetailComponent },
  { path: 'operation_notes', component: OperationNoteListComponent },
  {
    path: 'driver_medical_certificates/:id',
    component: DriverMedicalCertificateDetailComponent,
  },
  {
    path: 'driver_medical_certificates',
    component: DriverMedicalCertificateListComponent,
  },

  { path: 'pharmacy-dashboard', component: PharmacyDashboardComponent },

  {
    path: 'medical_certificate_employees/:id',
    component: MedicalCertificateEmployeeDetailComponent,
  },
  {
    path: 'medical_certificate_employees',
    component: MedicalCertificateEmployeeListComponent,
  },
  {
    path: 'pathology_orders/:id',
    component: LabDetailComponent,
    data: { isPat: true }
  },
  {
    path: 'pathology_orders',
    component: BiopsyRequestListComponent,
  },
  {
    path: 'medication_adminstration_charts/:id',
    component: MedicationAdminstrationChartDetailComponent,
  },
  { path: 'item_request/:id', component: ItemRequestDetailComponent },
  { path: 'item_requests', component: ItemRequestListComponent },
  {
    path: 'medication_adminstration_charts',
    component: MedicationAdminstrationChartListComponent,
  },
  { path: 'nurse_record', component: NurseRecordListComponent },
  { path: 'nurse_record/:id', component: NurseRecordDetailComponent },

  {
    path: 'medication_adminstration_charts/:id',
    component: MedicationAdminstrationChartDetailComponent,
  },
  {
    path: 'medication_adminstration_charts',
    component: MedicationAdminstrationChartListComponent,
  },

  { path: 'delivery_summarys/:id', component: DeliverySummaryDetailComponent },
  { path: 'delivery_summarys', component: DeliverySummaryListComponent },

  { path: 'intrapartum_cares/:id', component: IntrapartumCareDetailComponent },
  { path: 'intrapartum_cares', component: IntrapartumCareListComponent },


  { path: 'emergency_observations/:id', component: EmergencyObservationDetailComponent },
  { path: 'emergency_observations', component: EmergencyObservationListComponent },
  { path: 'opd_observations/:id', component: OpdObservationDetailComponent },
  { path: 'opd_observations', component: OpdObservationListComponent },

  {
    path: 'ANC_Initial_Evaluations/:id',
    component: ANCInitialEvaluationDetailComponent,
  },
  {
    path: 'ANC_Initial_Evaluations',
    component: ANCInitialEvaluationListComponent,
  },

  { path: 'post_natal_cares/:id', component: PostNatalCareDetailComponent },
  { path: 'post_natal_cares', component: PostNatalCareListComponent },

  { path: 'new_born_natal_cares/:id', component: NewBornNatalCareDetailComponent },
  { path: 'new_born_natal_cares', component: NewBornNatalCareListComponent },

  { path: 'anc_historys/:id', component: AncHistoryDetailComponent },
  { path: 'anc_historys', component: AncHistoryListComponent },

  { path: 'equipment_datas/:id', component: EquipmentDataDetailComponent },
  { path: 'equipment_datas', component: EquipmentDataListComponent },
  {
    path: '',
    loadChildren: () => import('./tc/tc.module').then((m) => m.TcModule),
  },
  {
    path: '',
    loadChildren: () => import('./bed/bed.module').then((m) => m.BedModule),
  },
  {
    path: 'present_pregnancy_follow_ups/:id',
    component: PresentPregnancyFollowUpDetailComponent,
  },
  {
    path: 'present_pregnancy_follow_ups',
    component: PresentPregnancyFollowUpListComponent,
  },

  {
    path: 'sample_collections/:id',
    component: SampleCollectionDetailComponent,
  },
  { path: 'sample_collections', component: SampleCollectionListComponent },
  // { path: 'radiology_result_approvals', component: RadResultApprovalListComponent },
  // { path: 'laboratory_result_approvals', component: LabResultApprovalListComponent },
  { path: 'consultations/:id', component: DoctorConsultationDetailComponent },
  { path: 'consultations', component: DoctorConsultationListComponent },
  { path: 'consultings/:id', component: ConsultingDetailComponent },

  { path: 'overview/:id', component: OverviewComponent },
  { path: 'pathology_results/:id', component: PathologyResultDetailComponent },
  { path: 'pathology_results', component: PathologyResultListComponent },
  { path: 'report/opd_report', component: OpdReportListComponent },
  { path: 'report/ipd_report', component: IpdReportListComponent },
  { path: 'report/ed_report', component: EdReportListComponent },
  { path: 'department_specialtys', component: Department_SpecialtyListComponent },
  { path: 'department_specialtys/:id', component: Department_SpecialtyDetailComponent },
  { path: 'procedure_reports', component: ProcedureReportListComponent },
  { path: 'encounter_reports', component: EncounterReportListComponent },
  { path: 'patient_list_reports', component: PatientReportList },
  { path: 'prescription_and_dispensation_reports', component: PrescriptionAndDispensationReportListComponent },
  { path: 'referral_reports', component: ReferralsReportListComponent },
  { path: 'dispense_historys/:id', component: DispenseHistoryDetailComponent },
  { path: 'dispense_historys', component: DispenseHistoryListComponent },
  { path: 'laboratory_result_approvals', component: ResultApprovalListComponent },
  { path: 'radiology_result_approvals', component: ResultApprovalListComponent },
  { path: 'pathology_result_approvals', component: ResultApprovalListComponent },
  { path: 'surgery_rooms/:id', component: SurgeryRoomDetailComponent },
  { path: 'surgery_rooms', component: SurgeryRoomListComponent },
  { path: 'result_progress', component: ResultProgressListComponent },
  { path: 'physical_examination_options/:id', component: PhysicalExaminationOptionsDetailComponent },
  { path: 'physical_examination_options', component: PhysicalExaminationOptionsListComponent },
  { path: 'payment_reports', component: PaymentReportListComponent },
  { path: 'dialysiss/:id', component: DialysisDetailComponent },
  { path: 'dialysiss', component: DialysisListComponent },
  { path: 'dialysis_sessions/:id', component: DialysisSessionDetailComponent },
  { path: 'dialysis_sessions', component: DialysisSessionListComponent },
  { path: 'dialysis_order', component: DialysisOrderComponent },
  { path: 'patient_intake_forms/:id', component: PatientIntakeFormDetailComponent },
  { path: 'additional_services/:id', component: AdditionalServiceDetailComponent },
  { path: 'additional_services', component: AdditionalServiceListComponent },
  { path: 'physiotherapys', component: PhysiotherapyComponent },
  { path: 'physiotherapy_sessions/:id', component: PhysiotherapySessionDetailComponent },
  { path: 'physiotherapy_sessions', component: PhysiotherapySessionListComponent },
  { path: 'rehabilitation_order/:id', component: RehabilitationOrderDetailComponent },
  { path: 'item_categorys/:id', component: ItemCategoryDetailComponent },
  { path: 'item_categorys', component: ItemCategoryListComponent },
  { path: 'purchase_requests/:id', component: PurchaseRequestDetailComponent },
  { path: 'purchase_requests', component: PurchaseRequestListComponent },
  { path: 'item_receives/:id', component: ItemReceiveDetailComponent },
  { path: 'item_receives', component: ItemReceiveListComponent },
  { path: 'interchangeable_items/:id', component: InterchangeableItemDetailComponent },
  { path: 'interchangeable_items', component: InterchangeableItemListComponent },
  { path: 'store_stock_levels/:id', component: StoreStockLevelDetailComponent },
  { path: 'store_stock_levels', component: StoreStockLevelListComponent },
  { path: 'prescription_requests/:id', component: PrescriptionRequestDetailComponent },
  { path: 'prescription_requests', component: PrescriptionRequestListComponent },
  { path: 'prescription_requests_by_patient', component: PrescriptionRequestByPatientListComponent },
  { path: 'prescription_requests_by_patient/:id', component: PrescriptionRequestByPatientDetailComponent },
  { path: 'prescriptionss/:id', component: PrescriptionsDetailComponent },
  { path: 'prescriptionss', component: PrescriptionListComponent },
  { path: 'item_dispatchs/:id', component: ItemDispatchDetailComponent },
  { path: 'item_dispatchs', component: ItemDispatchListComponent },
  { path: 'item_returns/:id', component: ItemReturnDetailComponent },
  { path: 'item_returns', component: ItemReturnGroupListComponent },
  { path: 'sell_reports', component: SellReportComponent },
  { path: 'item_expire_date_reports', component: ItemExpireDateReportListComponent },
  { path: 'radiology_result_settingss/:id', component: RadiologyResultSettingsDetailComponent },
  { path: 'radiology_result_settingss', component: RadiologyResultSettingsListComponent },
  { path: 'radiology_results/:id', component: RadiologyResultDetailComponent },
  { path: 'radiology_results', component: RadiologyResultListComponent },
  { path: 'chief_compliant_options/:id', component: ChiefCompliantOptionDetailComponent },
  { path: 'chief_compliant_options', component: ChiefCompliantOptionListComponent },
  { path: 'risk_factors_optionss/:id', component: RiskFactorsOptionsDetailComponent },
  { path: 'risk_factors_optionss', component: RiskFactorsOptionsListComponent },
  { path: 'final_assessments/:id', component: FinalAssessmentDetailComponent },
  { path: 'final_assessments', component: FinalAssessmentListComponent },
  { path: 'diagnosis_report', component: TopTenDiseasListComponent },
  { path: 'form_clinical_instructions/:id', component: FormClinicalInstructionDetailComponent },
  { path: 'risk_factors/:id', component: RiskFactorDetailComponent },
  { path: 'risk_factors', component: RiskFactorListComponent },
  { path: 'form_soaps/:id', component: FormSoapDetailComponent },
  { path: "form_vitals/:id", component: FormVitalsDetailComponent },
  { path: 'surgery_appointment', component: SurgeryAppointmentsComponent },

  { path: 'payment_discount_requests/:id', component: PaymentDiscountRequestDetailComponent },
  { path: 'payment_discount_requests', component: PaymentDiscountRequestListComponent },
  { path: 'medical_checkup_packages/:id', component: MedicalCheckupPackageDetailComponent },
  { path: 'medical_checkup_packages', component: MedicalCheckupPackageListComponent },
  { path: 'medical_checkup_services/:id', component: MedicalCheckupServiceDetailComponent },
  { path: 'medical_checkup_services', component: MedicalCheckupServiceListComponent },
  { path: 'deposit_options/:id', component: DepositOptionDetailComponent },
  { path: 'deposit_options', component: DepositOptionListComponent },
  { path: 'credit_employee_familys/:id', component: CreditEmployeeFamilyDetailComponent },
  { path: 'credit_employee_familys', component: CreditEmployeeFamilyListComponent },
  { path: 'markup_prices/:id', component: MarkupPriceDetailComponent },
  { path: 'markup_prices', component: MarkupPriceListComponent },
  { path: 'nurse_history/:id', component: NurseHistoryDetailComponent },
  { path: 'nurse_historys', component: NurseHistoryListComponent },
  { path: 'holidays/:id', component: HolidayDetailComponent },
  { path: 'holidays', component: HolidayListComponent },
  { path: 'plan_of_care/:id', component: PlanOfCareDetailComponent },
  { path: 'plan_of_cares', component: PlanOfCareListComponent },
  { path: 'nurse_plan_of_care/:id', component: NursePlanOfCareDetailComponent },
  { path: 'nurse_plan_of_cares', component: NursePlanOfCareListComponent },
  { path: 'company_credit_service_packages/:id', component: CompanyCreditServicePackageDetailComponent },
  { path: 'company_credit_service_packages', component: CompanyCreditServicePackageListComponent },
  { path: 'tat_reports', component: TatReportListComponent },

  { path: 'before_induction_of_anaesthesia/:id', component: BeforeInductionOfAnaesthesiaDetailComponent },
  { path: 'before_induction_of_anaesthesias', component: BeforeInductionOfAnaesthesiaListComponent },
  { path: 'before_patient_leaves_operating_room/:id', component: BeforePatientLeavesOperatingRoomDetailComponent },
  { path: 'before_patient_leaves_operating_rooms', component: BeforePatientLeavesOperatingRoomListComponent },
  { path: 'before_skin_incision/:id', component: BeforeSkinIncisionDetailComponent },
  { path: 'before_skin_incisions', component: BeforeSkinIncisionListComponent },
  { path: 'surgical_safety_checklist/:id', component: SurgicalSafetyChecklistComponent },
  { path: 'outside_prescriptions', component: OutsidePrescriptionListComponent },
  { path: 'outside_prescriptions/:id', component: OutsidePrescriptionDetailComponent },
  { path: 'measuring_unit/:id', component: MeasuringUnitDetailComponent },
  { path: 'measuring_units', component: MeasuringUnitListComponent },
  { path: 'bank/:id', component: BankDetailComponent },
  { path: 'bank', component: BankListComponent },
  { path: 'icu_physical_assessment/:id', component: IcuPhysicalAssessmentDetailComponent },
  { path: 'icu_physical_assessments', component: IcuPhysicalAssessmentListComponent },
  { path: 'procedure-templates/:id', component: ProcedureTemplateDetailComponent },
  { path: 'corelated_fee/:id', component: CorelatedFeeDetailComponent },
  { path: 'corelated_fees', component: CorelatedFeeListComponent },
  { path: 'pos_machine/:id', component: PosMachineDetailComponent },
  { path: 'pos_machines', component: PosMachineListComponent },
  { path: 'pos_machine_user/:id', component: PosMachineUserDetailComponent },
  { path: 'pos_machine_users', component: PosMachineUserListComponent },
  { path: 'pre_ansthesia_evaluations/:id', component: PreAnsthesiaEvaluationDetailComponent },
  { path: 'flow_sheet/:id', component: FlowSheetDetailComponent },
  { path: 'flow_sheets', component: FlowSheetListComponent },
  { path: 'session_therapys/:id', component: GroupMemberListComponent },
  { path: 'session_therapys', component: SessionTherapyListComponent },
  { path: 'group_member/:id', component: GroupMemberDetailComponent },
  { path: 'group_members', component: GroupMemberListComponent },
  { path: 'notification', component: NotificationComponent },

  { path: 'ward_reports/:id', component: WardReportDetailComponent },
  { path: 'ward_reports', component: WardReportListComponent },

  { path: 'patient_ward_reports/:id', component: PatientWardReportDetailComponent },
  { path: 'patient_ward_reports', component: PatientWardReportListComponent },
  { path: 'machines/:id', component: MachinesDetailComponent },
  { path: 'machiness', component: MachinesListComponent },
  { path: 'machine_test/:id', component: MachineTestDetailComponent },
  { path: 'machine_tests', component: MachineTestListComponent },
  { path: 'daily_report/:id', component: DailyReportDetailComponent },
  { path: 'daily_reports', component: DailyReportListComponent },
  { path: 'psychoterapy_progress_note/:id', component: PsychoterapyProgressNoteDetailComponent },
  { path: 'psychoterapy_progress_notes', component: PsychoterapyProgressNoteListComponent },
  {path: 'queues/:id', component: QueuesDetailComponent},
  {path: 'queues', component: QueuesListComponent},
  {path: 'credit_payments/:id', component: CreditPaymentDetailComponent},
  {path: 'credit_payments', component: CreditPaymentListComponent},
  {path: 'credit_payment_historys/:id', component: CreditPaymentHistoryDetailComponent},
  {path: 'credit_payment_historys', component: CreditPaymentHistoryListComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
