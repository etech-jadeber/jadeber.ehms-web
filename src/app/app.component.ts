import { HttpClient } from '@angular/common/http';
import { Component, HostListener, Inject, Injectable, OnInit } from '@angular/core';

import { NavigationEnd, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AppTranslation } from './app.translation';
import { BedassignmentNavigator } from './bed/bedassignments/bedassignment.navigator';
import {Title} from "@angular/platform-browser";
import { LostDamageNavigator } from './lostDamages/lostDamage.navigator';
import { ItemNavigator } from './items/item.navigator';
import { DrugtypeNavigator } from './drugtypes/drug_type.navigator';
import { PatientNavigator } from './patients/patients.navigator';
import { PatientPersist } from './patients/patients.persist';
import { RequestNavigator } from './requests/request.navigator';
import { SupplierNavigator } from './suppliers/supplier.navigator';
import { StoreNavigator } from './stores/store.navigator';
import { TCAsyncJob } from './tc/asyncjob';
import { TCAuthentication } from './tc/authentication';
import { TCAuthorization } from './tc/authorization';
import { CaseNavigator } from './tc/cases/case.navigator';
import { EventlogNavigator } from './tc/eventlogs/eventlog.navigator';
import { FileNavigator } from './tc/files/file.navigator';
import { GroupNavigator } from './tc/groups/group.navigator';
import { JobNavigator } from './tc/jobs/job.navigator';
import { JobPersist } from './tc/jobs/job.persist';
import { LockNavigator } from './tc/locks/lock.navigator';
import { MailNavigator } from './tc/mails/mail.navigator';
import { TCNavigator } from './tc/navigator';
import { TCNotification } from './tc/notification';
import { ResourceNavigator } from './tc/resources/resource.navigator';
import { SettingNavigator } from './tc/settings/setting.navigator';
import { StatNavigator } from './tc/stats/stat.navigator';
import { TgidentityNavigator } from './tc/tgidentitys/tgidentity.navigator';
import { UserNavigator } from './tc/users/user.navigator';
import { UserPersist } from './tc/users/user.persist';
import { TCUtilsAngular } from './tc/utils-angular';
import { TCUtilsString } from './tc/utils-string';
import { WaitinglistNavigator } from './bed/waitinglists/waitinglist.navigator';
import { TransferNavigator } from './transfers/transfer.navigator';
import { Item_Ledger_EntryNavigator } from './item_ledger_entrys/item_ledger_entry.navigator';
import { PurchaseNavigator } from './purchases/purchase.navigator';
import { PaymentNavigator } from './payments/payment.navigator';
import { AvailabilityNavigator } from './availabilitys/availability.navigator';
import { AppointmentNavigator } from './appointments/appointment.navigator';
import { EmployeeNavigator } from './storeemployees/employee.navigator';
import { EmployeeSelfPersist } from './storeemployees/employee-self.persist';
import { DoctorNavigator } from './doctors/doctor.navigator';
import { ReceptionNavigator } from './receptions/reception.navigator';
import { DoctorPersist } from './doctors/doctor.persist';
import { ReceptionPersist } from './receptions/reception.persist';
import { ReportNavigator } from './reports/report.navigator';
import { FeeNavigator } from './fees/fee.navigator';
import { Fee_TypeNavigator } from './feetypes/fee_type.navigator';
import { Surgery_TypeNavigator } from './surgery_types/surgery_type.navigator';
import { Surgery_AppointmentNavigator } from './surgery_appointments/surgery_appointment.navigator';
import { DepositNavigator } from './deposits/deposit.navigator';
import { Form_EncounterNavigator } from './form_encounters/form_encounter.navigator';
import { Admission_FormNavigator } from './form_encounters/admission_forms/admission_form.navigator';
import { MenuState } from './global-state-manager/global-state';
import { DOCUMENT, Location } from '@angular/common';
import { DiagnosisTypes, lab_order_type, tabs, user_context } from './app.enums';
import { Procedure_ProviderNavigator } from './procedure_providers/procedure_provider.navigator';
import { Item_In_StoreNavigator } from './item_in_stores/item_in_store.navigator';
import { MessagesNavigator } from './messagess/messages.navigator';
import { Procedure_Payment_DueNavigator } from './procedure_payment_dues/procedure_payment_due.navigator';
import { Procedure_TestsNavigator } from './procedure_testss/procedure_tests.navigator';
import { CompanyNavigator } from './Companys/Company.navigator';
import { OutsourcingCompanyNavigator } from './outsourcing_companys/outsourcing_company.navigator';
import { Insured_EmployeeNavigator } from './insured_employees/insured_employee.navigator';
import { Payment_DueNavigator } from './payment_dues/payment_due.navigator';
import { MedicalReportChartsNavigator } from './medical-report-charts/medical-report-charts.navigator';
import { TCAppInit } from './tc/app-init';
import { Insurance_CompanyNavigator } from './insurance_companys/insurance_company.navigator';
import { Lab_PanelNavigator } from './lab_panels/lab_panel.navigator';
import { Lab_TestNavigator } from './lab_tests/lab_test.navigator';
import { Lab_OrderNavigator } from './lab_orders/lab_order.navigator';
import { Review_Of_System_OptionsNavigator } from './review_of_system_optionss/review_of_system_options.navigator';
import { Physiotherapy_TypeNavigator } from './physiotherapy_types/physiotherapy_type.navigator';
import { Test_PanelNavigator } from './lab_panels/test-panel.navigator';
import { DepartmentNavigator } from './departments/department.navigator';
import { Consent_FormNavigator } from './consent_forms/consent_form.navigator';
import { BirthNavigator } from './births/birth.navigator';
import { VaccinatedNavigator } from './vaccinateds/vaccinated.navigator';
import { VaccinationNavigator } from './vaccinations/vaccination.navigator';
import { DeathNavigator } from './deaths/death.navigator';
import { Procedure_TypeNavigator } from './procedure_types/procedure_type.navigator';
import { ProcedureNavigator } from './procedures/procedure.navigator';
import { Cancelled_AppointmentNavigator } from './cancelled_appointments/cancelled_appointment.navigator';
import { Outside_OrdersNavigator } from './outside_orderss/outside_orders.navigator';
import { MedicalCertificateEmployeeNavigator } from './medical_certificate_employee/medical_certificate_employee.navigator';
import { BiopsyRequestNavigator } from './biopsy_request/biopsy_request.navigator';
import { Medication_adminstration_chartNavigator } from './medication_adminstration_chart/medication_adminstration_chart.navigator';
import { DRIVER_MEDICAL_CERTIFICATENavigator } from './driver_medical_certificate/driver_medical_certificate.navigator';
import { AllergyNavigator } from './allergy/allergy.navigator';

import { Nurse_RecordNavigator } from './nurse_record/nurse_Record_Navigator';
import { BedroomlevelNavigator } from './bed/bedroomlevels/bedroomlevel.navigator';
import { BedroomtypeNavigator } from './bed/bedroomtypes/bedroomtype.navigator';
import { BedNavigator } from './bed/beds/bed.navigator';
import { BedroomNavigator } from './bed/bedrooms/bedroom.navigator';
import { ANCInitialEvaluationNavigator } from './ANC_Initial_Evaluation/ANC_Initial_Evaluation.navigator';
import { MenuItem } from 'primeng/api';
import { PresentPregnancyFollowUpNavigator } from './present_pregnancy_follow_up/present_pregnancy_follow_up.navigator';
import { ApprovedRequestNavigator } from './requests/approved-requests/approved-requests.navigator';
import { PatientDischargeNavigator } from './patient_discharge/patient_discharge.navigator';
import { ColorService } from './colorService';
import { PatientReportNavigator } from './patient_report/report_navigator';
import { Department_SpecialtyNavigator } from './department_specialtys/department_specialty.navigator';
import { HemodialysisScheduledPateintNavigator } from './hemodialysis_scheduled_pateint/hemodialysis.navigator';
import { DialysisMachineNavigator } from './dialysis_machine/dialysis_machine.navigator';
import { SurgeryRoomNavigator } from './surgery_room/surgery_room.navigator';
import { OtherServicesNavigator } from './other_services/other-services.navigator';
import { OrderedOtheServiceNavigator } from './ordered_othe_service/ordered_othe_service.navigator';
import { Lab_ResultNavigator } from './form_encounters/orderss/lab_results/lab_result.navigator';
import { PhysicalExaminationOptionsNavigator } from './physical_examination_options/physical_examination_options.navigator';
import { VitalNavigator } from './vitals/vital.navigator';

import { RoomNavigator } from './room/room.navigator';
import { QueueNavigator } from './queue/queue.navigator';

import { DialysisSessionNavigator } from './dialysis_session/dialysis_session.navigator';
import { Physiotherapy } from './physiotherapy/physiotherapy';
import { ItemCategoryNavigator } from './item_category/item_category.navigator';
import { PurchaseRequestNavigator } from './purchase_request/purchase_request.navigator';
import { ItemReceiveNavigator } from './item_receive/item_receive.navigator';
import { PrescriptionRequestNavigator } from './prescription_request/prescription_request.navigator';
import { ItemDispatchNavigator } from './item_dispatch/item_dispatch.navigator';
import { ItemReturnNavigator } from './item_return/item_return.navigator';
import { RadiologyResultSettingsNavigator } from './radiology_result_settings/radiology_result_settings.navigator';
import { ChiefCompliantOptionNavigator } from './chief_compliant_option/chief_compliant_option.navigator';
import { RiskFactorsOptionsNavigator } from './risk_factors_options/risk_factors_options.navigator';
import { PaymentDiscountRequestNavigator } from './payment_discount_request/payment_discount_request.navigator';
import { MedicalCheckupPackageNavigator } from './medical_checkup_package/medical_checkup_package.navigator';
import { DepositOptionNavigator } from './deposit_option/deposit_option.navigator';
import { MarkupPriceNavigator } from './markup_price/markup_price.navigator';
import { HolidayNavigator } from './holiday/holiday.navigator';
import { OrdersNavigator } from './form_encounters/orderss/orders.navigator';
import { PlanOfCareNavigator } from './plan_of_care/plan_of_care.navigator';
import { EmergencyObservationNavigator } from './emergency_observation/emergency_observation.navigator';
import { ItemRequestNavigator } from './item_request/item_request.navigator';
import { OutsidePrescriptionNavigator } from './outside_prescription/outside_prescription.navigator';
import { MeasuringUnitNavigator } from './measuring_unit/measuringUnit.navigator';
import { BankNavigator } from './bank/bank.navigator';
import { OpdObservationNavigator } from './opd_observation/opd_observation.navigator';
import { ProcedureTemplateNavigator } from './procedure_template/procedure_template.navigator';
import { CorelatedFeeNavigator } from './corelated_fee/corelated_fee.navigator';
import { PosMachineNavigator } from './pos_machine/pos-machine.navigator';
import { PosMachineUserNavigator } from './pos_machine_user/pos-machine-user.navigator';
import { DoctorConsultationNavigator } from './doctor_dashboard/doctor_consultation.navigator';
import { SessionTherapyNavigator } from './session_therapy/session_therapy.navigator';
// import { SocketService } from './socketService';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotificationNavigator } from './notification/notification.navigator';
import { TCUtilsHttp } from './tc/utils-http';
import { NotificationPersist } from './notification/notification.persist';
import { AppPersist } from './app.persist';
import { MachinesNavigator } from './machines/machines.navigator';
import {DailyReportNavigator} from "./daily_report/daily-report.navigator";
import { QueuesNavigator } from './queues/queues.navigator';
import {CreditPaymentPersist} from "./credit_payment/credit_payment.persist";
import {CreditPaymentNavigator} from "./credit_payment/credit_payment.navigator";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  @HostListener('window:onbeforeunload', ['$event'])
  clearLocalStorage(event) {
    localStorage.clear();
  }
  logo = "";
  title = 'Jadeber Psychiatric and Rehab Center';
  core_emr_url = environment.core_emr_url;
  stateValues = '';
  tabs: typeof tabs = tabs;
  activeTab = tabs.overview;
  stateResponseData: any;
  subtitle: string = '';
  onDetail: boolean = false;
  previousUrl = '';
  pathUrl = [];
  TCAppInit: typeof TCAppInit = TCAppInit;
  pid = '';
  patientProfile="";
  patientProfileLoaded:boolean=false;
  constructor(
    public tcUtilsAngular: TCUtilsAngular,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    public router: Router,
    public nurse_recordNavigator: Nurse_RecordNavigator,
    private location: Location,
    public tcNavigator: TCNavigator,
    public tcAuthentication: TCAuthentication,
    public transferNavigator: TransferNavigator,
    public item_ledger_entryNavigator: Item_Ledger_EntryNavigator,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public cancelled_appointmentNavigator: Cancelled_AppointmentNavigator,
    public resourceNavigator: ResourceNavigator,
    public userNavigator: UserNavigator,
    public groupNavigator: GroupNavigator,
    public eventlogNavigator: EventlogNavigator,
    public settingNavigator: SettingNavigator,
    public appointmentNavigator: AppointmentNavigator,
    public fileNavigator: FileNavigator,
    public mailNavigator: MailNavigator,
    public doctorNavigator: DoctorNavigator,
    private dialysisMachineNavigator:DialysisMachineNavigator,
    public receptionNavigator: ReceptionNavigator,
    public doctorPersist: DoctorPersist,
    public outside_ordersNavigator: Outside_OrdersNavigator,
    public dialysisSessionNavigator: DialysisSessionNavigator,
    public vaccinatedNavigator: VaccinatedNavigator,
    public receptionPersist: ReceptionPersist,
    public lockNavigator: LockNavigator,
    public caseNavigator: CaseNavigator,
    public roomNavigator:RoomNavigator,
    public statNavigator: StatNavigator,
    public jobNavigator: JobNavigator,
    public appTranslation: AppTranslation,
    public availabilityNavigator: AvailabilityNavigator,
    public procedure_providerNavigator: Procedure_ProviderNavigator,
    public paymentNavigator: PaymentNavigator,
    public jobPersist: JobPersist,
    public tcAsyncJob: TCAsyncJob,
    public bedroomlevelNavigator: BedroomlevelNavigator,
    public tgidentityNavigator: TgidentityNavigator,
    public titleService: Title,
    public item_in_storeNavigator: Item_In_StoreNavigator,
    public userPersist: UserPersist,
    private http: HttpClient,
    public patientNavigator: PatientNavigator,
    public patientPersisit: PatientPersist,
    public tcUtilsString: TCUtilsString,
    public bedroomtypeNavigator: BedroomtypeNavigator,
    public bedNavigator: BedNavigator,
    public bedassignmentNavigator: BedassignmentNavigator,
    public waitinglistNavigator: WaitinglistNavigator,
    public bedroomNavigator: BedroomNavigator,
    public requestNavigator: RequestNavigator,
    public vitalNavvitalNav:VitalNavigator,
    private otherSerNav:OtherServicesNavigator,
    private colorService:ColorService,
    public machineNavigator: MachinesNavigator,
    public lostDamageNavigator: LostDamageNavigator,
    public itemNavigator: ItemNavigator,
    public supplierNavigator: SupplierNavigator,
    public drug_typeNavigator: DrugtypeNavigator,
    public storeNavigator: StoreNavigator,
    private queueNavigator:QueueNavigator,
    public purchaseNavigator: PurchaseNavigator,
    public employeeNavigator: EmployeeNavigator,
    public messagesNavigator: MessagesNavigator,
    public employeePersist: EmployeeSelfPersist,
    public procedure_payment_dueNavigator: Procedure_Payment_DueNavigator,
    public procedure_testsNavigator: Procedure_TestsNavigator,
    public reportNavigator: ReportNavigator,
    public feeNavigator: FeeNavigator,
    public fee_typeNavigator: Fee_TypeNavigator,
    public allergyNavigator: AllergyNavigator,
    public surgery_typeNavigator: Surgery_TypeNavigator,
    public surgery_appointmentNavigator: Surgery_AppointmentNavigator,
    public depositNavigator: DepositNavigator,
    public consent_formNavigator: Consent_FormNavigator,
    public form_encounterNavigator: Form_EncounterNavigator,
    public dailyReportNavigator: DailyReportNavigator,
    public nursePersist: DoctorPersist,
    public admission_formNavigator: Admission_FormNavigator,
    public menuState: MenuState,
    public CompanyNavigator: CompanyNavigator,
    public OutsourcingCompanyNavigator: OutsourcingCompanyNavigator,
    public insured_employeeNavigator: Insured_EmployeeNavigator,
    public payment_dueNavigator: Payment_DueNavigator,
    public medicalReportChartsNavigator: MedicalReportChartsNavigator,
    public insurance_companyNavigator: Insurance_CompanyNavigator,
    public lab_panelNavigator: Lab_PanelNavigator,
    public lab_testNavigator: Lab_TestNavigator,
    public otherNavigator:OtherServicesNavigator,
    public orderNavigator: OrdersNavigator,
    public lab_orderNavigator: Lab_OrderNavigator,
    public review_of_system_optionsNavigator: Review_Of_System_OptionsNavigator,
    public test_panelNavigator: Test_PanelNavigator,
    public physiotherapy_typeNavigator: Physiotherapy_TypeNavigator,
    public departmentSpecialityNavigator: Department_SpecialtyNavigator,
    public birthNavigator: BirthNavigator,
    public vaccinationNavigator: VaccinationNavigator,
    public deathNavigator: DeathNavigator,
    public procedure_typeNavigator: Procedure_TypeNavigator,
    public hemoNav:HemodialysisScheduledPateintNavigator,
    public outsidePrescriptionNavigator: OutsidePrescriptionNavigator,
    public procedureNavigator: ProcedureNavigator,
    public medicationAdminstrationChartNavigator: Medication_adminstration_chartNavigator,
    public driverMedicalCertificateNavigator: DRIVER_MEDICAL_CERTIFICATENavigator,
    public medicalCertificateEmployeeNavigator: MedicalCertificateEmployeeNavigator,
    public biopsyRequestNavigator: BiopsyRequestNavigator,
    public aNCInitialEvaluationNavigator: ANCInitialEvaluationNavigator,
    public presentPregnancyFollowUpNavigator: PresentPregnancyFollowUpNavigator,
    public patient_discharge_navigator: PatientDischargeNavigator,
    public patientReportNavigator: PatientReportNavigator,
    public surgeryRoomNavigator: SurgeryRoomNavigator,
    public lab_resultNavigator: Lab_ResultNavigator,
    public physicalExaminationOptionNavigator: PhysicalExaminationOptionsNavigator,
    public physiotherapyNavigator: Physiotherapy,
    public itemCategoryNavigator: ItemCategoryNavigator,
    public purchaseRequestNavigator: PurchaseRequestNavigator,
    public itemReceiveNavigator: ItemReceiveNavigator,
    public prescriptionRequestNavigator: PrescriptionRequestNavigator,
    public itemDispatchNavigator: ItemDispatchNavigator,
    public itemReturnNavigator: ItemReturnNavigator,
    public itemRequestNavigator: ItemRequestNavigator,
    public radiologyResultSettingNavigator: RadiologyResultSettingsNavigator,
    public procedureTemplateNavigator: ProcedureTemplateNavigator,
    public chief_compliant_optionNavigator: ChiefCompliantOptionNavigator,
    public risk_factor_optionsNavigator: RiskFactorsOptionsNavigator,
    public paymentdiscountNavigator: PaymentDiscountRequestNavigator,
    public medicalCheckupPackageNavigator: MedicalCheckupPackageNavigator,
    public depositOptionNavigator : DepositOptionNavigator,
    public planeOfCareNavigator: PlanOfCareNavigator,
    public markupPriceNavigator : MarkupPriceNavigator,
    public opdObservationNavigator: OpdObservationNavigator,
    public emergencyObservationNavigator: EmergencyObservationNavigator,
    public holidayNavigator : HolidayNavigator,
    public doctorConsultationNavigator: DoctorConsultationNavigator,
    public measuringUnitNavigator: MeasuringUnitNavigator,
    public bankNavigator: BankNavigator,
    public corelatedFeeNavigator: CorelatedFeeNavigator,
    public posMachineNavigator: PosMachineNavigator,
    public posMachineUserNavigator: PosMachineUserNavigator,
    public sessionTherapyNavigator: SessionTherapyNavigator,
    public queuesNavigator: QueuesNavigator,

    // public socketService: SocketService,
    private _snackBar: MatSnackBar,
    public notificationNavigator: NotificationNavigator,
    public notificationPersist: NotificationPersist,

    public tCUtilsHttp:TCUtilsHttp,
    public appPersit:AppPersist,
    public creditPaymentNavigator: CreditPaymentNavigator,





  ) {

    this.router.events.subscribe((val)=>{
      TCAppInit.appInitialized$.subscribe((res) => {
        if(res){
          this.loadMenu();
        }
      });
    })

    this.onDetail = false;
    menuState.currentMenuState.subscribe((value) => {


      if (value != null) {
        this.stateValues = value;
        this.onDetail = true;
      } else {
        this.onDetail = false;
      }
    });
    menuState.currentDataResponseState.subscribe((res) => {



      if (res != null) {
        this.stateResponseData = res;
        this.patientProfile=`${environment.tcApiBaseUri}tc/files/download?fk=${this.stateResponseData['profile_pic']}`;
        this.patientProfileLoaded=true;
      } else {
        this.patientProfileLoaded=false;
      }
      this.onDetail=this.router.url.split("/").length==3;

      this.pathUrl=this.router.url.split("/");

    });

  }

  items: MenuItem[];

  loadMenu(): void {
    this.items = [
      {
        label: this.appTranslation.getText('general', 'dashboard'),
        icon: 'fa fa-home',
        routerLink:['/dashboard'],
      },
      {
        label: this.appTranslation.getText('patient', 'Patients'),
        icon: 'fa fa-users',
        items: [
          {
            label: this.appTranslation.getText('patient', 'Patients'),
            icon: 'fa fa-user',
            routerLink:[this.patientNavigator.patientsUrl()],
            visible: this.tcAuthorization.canRead('patients'),
          },
          {
            label: this.appTranslation.getText("patient", "assignments"),
            icon: 'fa fa-calendar-alt',
            routerLink:[this.appointmentNavigator.appointmentsUrl()],
            visible: this.tcAuthorization.canCreate('appointments'),
          },
          {
            label: this.appTranslation.getText(
              'schedule',
              'cancelled_appointment'
            ),
            icon: 'fa fa-ban',
            routerLink:[this.cancelled_appointmentNavigator.cancelled_appointmentsUrl()],
            visible: this.tcAuthorization.canRead('cancelled_appointments'),
          },

        ],
      },
      {

        label: this.appTranslation.getText('patient', 'visits'),
        icon: 'fa fa-check-double',
        items: [
          {
            routerLink: [this.form_encounterNavigator.form_encountersUrl()],
            label: this.appTranslation.getText('patient', 'visits'),
            icon: 'fa fa-check-double',
            visible: this.tcAuthorization.canCreate('form_encounters'),
          },
          {
            routerLink: [this.dailyReportNavigator.daily_reportsUrl()],
            label: "Daily Reports",
            icon: 'fa fa-check-double',
            visible: this.tcAuthorization.canCreate('daily_reports'),
          },

          {
            routerLink: [this.doctorConsultationNavigator.consultationsUrl()],
            label: 'Consultation',
            icon: 'fa fa-check-double',
            visible: this.tcAuthorization.canCreate('consultings'),
          },
          {
            label: "Queues",
            icon: 'fa fa-bandcamp',
            routerLink:[this.queueNavigator.queuesUrl()],
            visible:this.tcAuthorization.canRead('queues'),
          },



          // {
          //   label: "Queues",medi
          //   icon: 'fa fa-bandcamp',
          //   routerLink:[this.queueNavigator.queuesUrl()],
          //   visible:this.tcAuthorization.canRead('queues'),
          // },


          {
            label: this.appTranslation.getText('procedure', 'result_progress'),
            icon: 'fa fa-bars-progress',
            routerLink:[this.lab_resultNavigator.resultProgressUrl()],
            visible: this.tcAuthorization.canRead('result_progress'),
          },

          {
            label:'OPD Observation',
            icon: 'fa fa-user-nurse',
            routerLink:[this.opdObservationNavigator.opd_observationsUrl()],
            visible: this.tcAuthorization.canRead("opd_observations"),
          },

          {
            label:'Emergency Observation',
            icon: 'fa fa-user-nurse',
            routerLink:[this.emergencyObservationNavigator.emergency_observationsUrl()],
            visible: this.tcAuthorization.canRead("emergency_observations"),
          },
          {
            label: this.appTranslation.getText('general', 'nurse_record'),
            icon: 'fa fa-user-nurse',
            routerLink:[this.nurse_recordNavigator.nurseNavigatorUrl()],
            visible: this.tcAuthorization.canRead('nurse_records'),
          },
          // {
          //   label: this.appTranslation.getText('patient', 'consent_form'),
          //   icon: 'fa fa-signature',
          //   routerLink: [this.consent_formNavigator.consent_formsUrl()],
          //   visible: this.tcAuthorization.canRead('consent_forms'),
          // },
          // {
          //   label: this.appTranslation.getText('patient', 'vaccinateds'),
          //   icon: 'fa fa-syringe',
          //   routerLink: [this.vaccinatedNavigator.vaccinatedsUrl()],
          //   visible: this.tcAuthorization.canCreate('vaccinateds'),
          // },
        ],
      },

      {
        label: this.appTranslation.getText('bed', 'bed_management'),
        icon: 'fa fa-bed',
        items: [
          {
            label: this.appTranslation.getText('bed', 'bed_assignment'),
            icon: 'fa fa-gavel',
            routerLink:[this.bedassignmentNavigator.bedassignmentsUrl()],
            visible: this.tcAuthorization.canRead('bed_assignments'),
          },
          {
            label: this.appTranslation.getText(
              'patient',
              'discharge'
            ),
            icon: 'fa fa-ban',
            routerLink:[this.patient_discharge_navigator.patientDischargesUrl()],
            visible: this.tcAuthorization.canRead('patient_discharges'),
          },
          {
            label: this.appTranslation.getText('patient', 'admission_form'),
            icon: 'fa fa-user-friends',
            routerLink:[this.admission_formNavigator.admission_formsUrl()],
            visible: this.tcAuthorization.canRead('admission_forms'),
          },
          // {
          //   label: this.appTranslation.getText('patient', 'waiting_lists'),
          //   icon: 'fa fa-user-friends',
          //   routerLink:[this.waitinglistNavigator.waitinglistsUrl()],
          //   visible: this.tcAuthorization.canRead('waiting_lists'),
          // },

        ],
      },

      {
        label: this.appTranslation.getText('laboratory', 'diagnostic'),
        icon: 'fa fa-flask',

        items: [
          {
            label: 'Lab Order',
            icon: 'fa fa-flask',
            routerLink:[this.orderNavigator.labOrderssUrl()],
            visible: this.tcAuthorization.canCreate('laboratory_results'),
          },
          {
            label: 'Lab Result Approval',
            icon: 'fa fa-person-circle-check',
            routerLink: [this.orderNavigator.labResultApprovalUrl()],
            visible: this.tcAuthorization.canCreate('laboratory_result_approvals'),
          },
          {
            label: this.appTranslation.getText('procedure', 'pathology'),
            icon: 'fa fa-flask',
            routerLink: [this.biopsyRequestNavigator.biopsy_requestsUrl()],
            visible: this.tcAuthorization.canCreate('pathology_results'),
          },
          {
            label: this.appTranslation.getText('procedure', 'pathology_results_approvals'),
            icon: 'fa fa-flask',
            routerLink: [this.orderNavigator.patResultApprovalUrl()],
            visible: this.tcAuthorization.canCreate('pathology_result_approvals'),

          },
          {
            label: 'Rad Order',
            icon: 'fa fa-flask',
            routerLink: [this.orderNavigator.radOrderssUrl()],
            visible: this.tcAuthorization.canCreate('radiology_results'),
          },
          {
            label: 'Rad Result Approval',
            icon: 'fa fa-person-circle-check',
            routerLink:[this.orderNavigator.radResultApprovalUrl()],
            visible: this.tcAuthorization.canCreate('radiology_result_approvals'),
          },
        ],
      },
      // {
      //   label: "Dialysis",
      //   icon: 'fa fa-stethoscope',
      //   routerLink:[this.dialysisSessionNavigator.dialysisOrderUrl()],
      //   visible:this.tcAuthorization.canRead('dialysiss'),
      // },
      {

        label: "Prescriptions",
        icon: 'fa fa-user-injured',
        routerLink:[this.form_encounterNavigator.prescriptionssUrl()],
        visible:this.tcAuthorization.canRead('prescriptions'),
      },
      {
        label: "Physiotherapy",
        icon: 'fa fa-user-injured',
        routerLink:[this.physiotherapyNavigator.physiotherapysUrl()],
        visible:this.tcAuthorization.canRead('rehabilitation_orders'),
      },
      {
        label: "Procedure",
        icon: 'fa fa-stethoscope',
        routerLink:[this.form_encounterNavigator.patientProceduresUrl()],
        visible:this.tcAuthorization.canRead('procedure_orders'),
      },
      {
        label: "Group therapy",
        icon: 'fa-sharp fa-solid fa-head-side-brain',
        routerLink:[this.sessionTherapyNavigator.session_therapysUrl()],
        visible:this.tcAuthorization.canRead('session_therapys'),
      },

      // {
      //   label: this.appTranslation.getText('patient', 'request_mgt'),
      //   icon: 'fa fa-question',

      //   items: [
          // {
          //   label: this.appTranslation.getText(
          //     'patient',
          //     'surgery_appointment'
          //   ),
          //   icon: 'fa fa-calendar-alt',
          //   routerLink: [this.surgery_appointmentNavigator.surgeryAppointemntUrl()],
          //   visible: this.tcAuthorization.canRead('surgery_appointments'),
          // },
          {
            label: this.appTranslation.getText('patient', 'outside_order'),
            icon: 'fa fa-arrow-up-right-dots',
            routerLink:[this.outside_ordersNavigator.outside_orderssUrl()],
            visible: this.tcAuthorization.canCreate('outside_orders'),
          },


      //   ],
      // },
      {
        label: this.appTranslation.getText('financial', 'financial'),
        icon: 'fa fa-coins',
        items: [
          {
            label: this.appTranslation.getText('financial', 'payment'),
            icon: 'fa fa-money-bill-wave-alt',
            routerLink:[this.paymentNavigator.paymentsUrl()],
            visible: this.tcAuthorization.canCreate('payments'),
          },
          {
            label: this.appTranslation.getText('financial', 'fees'),
            icon: 'fa fa-money-bill',
            routerLink:[this.feeNavigator.feesUrl()],
            visible: this.tcAuthorization.canRead('fees'),
          },
          // {
          //   label:'POS Users',
          //   icon: 'fa fa-cash-register',
          //   routerLink:[this.posMachineUserNavigator.pos_machine_usersUrl()],
          //   visible: this.tcAuthorization.canRead('pos_machine_users'),
          // },
          {
            label:'Corelated Fee',
            icon: 'fa fa-money-check',
            routerLink:[this.corelatedFeeNavigator.corelated_feesUrl()],
            visible: this.tcAuthorization.canRead('corelated_fees'),
          },
          {
            label: this.appTranslation.getText('financial', 'deposit'),
            icon: 'fa fa-wallet',
            routerLink: [this.depositNavigator.depositsUrl()],
            visible: this.tcAuthorization.canCreate('deposits'),
          },
          {
            label: 'Credit',
            icon: 'fa fa-wallet',
            routerLink: [this.creditPaymentNavigator.credit_paymentsUrl()],
            visible: this.tcAuthorization.canCreate('credit_payments'),
          },
          {
            label: "Medical Checkup Package",
            icon: 'fa fa-box-open',
            routerLink: [this.medicalCheckupPackageNavigator.medicalCheckupPackagesUrl()],
            visible: this.tcAuthorization.canCreate('medical_checkup_packages'),
          },
          {
            label: "Discount Request",
            icon: 'fa fa-percent',
            routerLink: [this.paymentdiscountNavigator.paymentDiscountRequestsUrl()],
            visible: this.tcAuthorization.canRead('payment_discount_requests'),
          },
        ],
      },
      {
        label: this.appTranslation.getText('general', 'human_resource'),
        icon: 'fa fa-people-group',
        items: [
          {
            label: this.appTranslation.getText('employee', 'physicians'),
            icon: 'pi pi-fw pi-trash',
            routerLink:[this.doctorNavigator.doctorsUrl()],
            visible: this.tcAuthorization.canCreate('medical_practitioners'),
          },
          // {
          //   label: this.appTranslation.getText('employee', 'store_employee'),
          //   icon: 'pi pi-fw pi-trash',
          //   routerLink:[this.employeeNavigator.employeesUrl()],
          //   visible: this.tcAuthorization.canCreate('store_employees'),
          // },

          {
            label: this.appTranslation.getText('employee', 'receptionist'),
            icon: 'pi pi-fw pi-trash',
            routerLink:[this.receptionNavigator.receptionsUrl()],
            visible: this.tcAuthorization.canCreate('receptions'),
          },
          // {
          //   label: this.appTranslation.getText('schedule', 'availability'),
          //   icon: 'fa fa-calendar',
          //   routerLink:[this.availabilityNavigator.availabilitysUrl()],
          //   visible: this.tcAuthorization.canCreate('availabilitys'),
          // },
          {
            label: this.appTranslation.getText('schedule', 'messages'),
            icon: 'fa fa-comment',
            routerLink: [this.messagesNavigator.messagessUrl()],
            visible: this.tcAuthorization.canCreate('messages'),
          },
        ],
      },

      // {
      //   label: this.appTranslation.getText('inventory', 'inventory'),
      //   icon: 'fa fa-building',
      //   items: [
      //     {
      //       label: this.appTranslation.getText('inventory', 'stores'),
      //       icon: 'fa fa-warehouse',
      //       routerLink:[this.storeNavigator.storesUrl()],
      //       visible: this.tcAuthorization.canRead('stores'),
      //     },
      //     {
      //       routerLink:[this.itemNavigator.itemsUrl()],
      //       label: this.appTranslation.getText('inventory', 'items'),
      //       icon: 'fa fa-boxes',
      //       visible: this.tcAuthorization.canCreate('items'),
      //     },

      //     {
      //       label: this.appTranslation.getText('inventory', 'items_in_store'),
      //       icon: 'fa fa-border-all',
      //       routerLink:[this.item_in_storeNavigator.item_in_storesUrl()],
      //       visible: this.tcAuthorization.canRead('item_in_stores'),
      //     },
      //     {
      //       label: this.appTranslation.getText('inventory', 'lost_damage'),
      //       icon: 'fa fa-circle-exclamation',
      //       routerLink:[this.lostDamageNavigator.lostDamagesUrl()],
      //       visible: this.tcAuthorization.canCreate('lost_damages'),
      //     },
      //     {
      //       label: this.appTranslation.getText('inventory', 'request'),
      //       icon: 'fa fa-question',
      //       routerLink:[this.requestNavigator.requestsUrl()],
      //       visible: this.tcAuthorization.canRead('requests'),
      //     },
      //     {
      //       label: this.appTranslation.getText('inventory', 'transfer'),
      //       icon: 'fa fa-exchange-alt',
      //       routerLink:[this.transferNavigator.transfersUrl()],
      //       visible: this.tcAuthorization.canRead('transfers'),
      //     },

      //     {
      //       label: "Outside Prescription",
      //       icon: 'fa fa-prescription-bottle',
      //       routerLink:[this.outsidePrescriptionNavigator.outside_prescriptionsUrl()],
      //       visible: this.tcAuthorization.canRead('outside_prescriptions'),
      //     },
      //     {
      //       label: "Prescription Request",
      //       icon: 'fa fa-prescription-bottle',
      //       routerLink:[this.prescriptionRequestNavigator.prescriptionRequestByPatientsUrl()],
      //       visible: this.tcAuthorization.canRead('prescription_requests'),
      //     },
      //     {
      //       label: "Employee Item Request",
      //       icon: 'fa fa-prescription-bottle',
      //       routerLink:[this.itemRequestNavigator.employee_item_requestsUrl()],
      //       visible: this.tcAuthorization.canRead('employee_item_requests'),
      //     },
      //     {
      //       label: "Item Request",
      //       icon: 'fa fa-prescription-bottle',
      //       routerLink:[this.itemRequestNavigator.item_requestsUrl()],
      //       visible: this.tcAuthorization.canCreate('item_requests'),
      //     },

      //     {
      //       label: this.appTranslation.getText('inventory', 'ledger_entry'),
      //       icon: 'fa fa-book-open',
      //       routerLink:[this.item_ledger_entryNavigator.item_ledger_entrysUrl()],
      //       visible: this.tcAuthorization.canRead('item_ledger_entrys'),
      //     },

      //     {
      //       label: this.appTranslation.getText('inventory', 'purchases'),
      //       icon: 'fa fa-money-bill',
      //       routerLink:[this.purchaseNavigator.purchasesUrl()],
      //       visible: this.tcAuthorization.canRead('purchases'),
      //     },
      //     {
      //       label: "Request Purchase",
      //       icon: 'fa fa-cart-arrow-down',
      //       routerLink:[this.purchaseRequestNavigator.purchaseRequestsUrl()],
      //       visible: this.tcAuthorization.canRead('purchase_requests'),
      //     },
      //     {
      //       label: "Item Receive",
      //       icon: 'fa fa-dolly',
      //       routerLink:[this.itemReceiveNavigator.itemReceivesUrl()],
      //       visible: this.tcAuthorization.canRead('item_receives'),
      //     },
      //     {
      //       label: "Item dispached",
      //       icon: 'fa fa-trowel-bricks',
      //       routerLink:[this.itemDispatchNavigator.itemDispatchsUrl()],
      //       visible: this.tcAuthorization.canRead('item_dispatchs'),
      //     },
      //     {
      //       label: "Item returned",
      //       icon: 'fa fa-rotate-left',
      //       routerLink:[this.itemReturnNavigator.itemReturnsUrl()],
      //       visible: this.tcAuthorization.canRead('item_returns'),
      //     }

      //   ],
      // },

      // {
      //   label: this.appTranslation.getText('inventory', 'insurance'),
      //   icon: 'fa fa-building',
      //   items: [
      //     {
      //       label: this.appTranslation.getText(
      //         'inventory',
      //         'insurance_company'
      //       ),
      //       icon: 'fa fa-building',
      //       routerLink:[this.insurance_companyNavigator.insurance_companysUrl()],
      //       visible: this.tcAuthorization.canCreate('insurance_companys'),
      //     },
      //     {
      //       label: this.appTranslation.getText('inventory', 'company'),
      //       icon: 'fa fa-industry',
      //       routerLink:[this.CompanyNavigator.CompanysUrl()],
      //       visible: this.tcAuthorization.canRead('companys'),
      //     },

      //   ],
      // },



      {
        label: 'Reports',
        icon: 'fa fa-book',
        items: [
          {
            label: "payment report",
            icon: 'fa fa-user-md',
            routerLink:[this.reportNavigator.paymentReportUrl()],
            visible: this.tcAuthorization.canRead('payment_reports'),
          },
          {
            label: "TAT Report",
            icon: 'fa fa-user-md',
            routerLink:[this.reportNavigator.tatReportUril()],
            visible: this.tcAuthorization.canRead('order_tests'),
          },
          {
            label: this.appTranslation.getText('laboratory', 'lab_report'),
            icon: 'fa fa-user-md',
            routerLink:[this.reportNavigator.labReportsUrl()],
            visible: this.tcAuthorization.canRead('laboratory_reports'),
          },
          {
            label: this.appTranslation.getText('laboratory', 'rad_report'),
            icon: 'fa fa-user-md',
            routerLink:[this.reportNavigator.radReportsUrl()],
            visible: this.tcAuthorization.canRead('radiology_reports'),
          },
          {
            label: this.appTranslation.getText('laboratory', 'path_report'),
            icon: 'fa fa-user-md',
            routerLink:[this.reportNavigator.patReportsUrl()],
            visible: this.tcAuthorization.canRead('pathology_reports'),
          },
          // {
          //   label: this.appTranslation.getText('inventory', 'inventory'),
          //   icon: 'fa fa-book',
          //   routerLink:[this.reportNavigator.reportsUrl()],
          //   visible: this.tcAuthorization.canRead('inventory_reports'),
          // },
          // {
          //   label:"sell report",
          //   icon: 'fa fa-book',
          //   routerLink:[this.reportNavigator.sellReportsUrl()],
          //   visible: this.tcAuthorization.canRead('sale_reports'),
          // },
          {
            label: "Disease Report",
            icon: 'fa fa-user',
            routerLink:[this.patientNavigator.diagnosisReportUrl()],
            visible: this.tcAuthorization.canRead('diagnosis_reports'),
          },
          {
            label: "OPD",
            icon: 'fa fa-user',
            routerLink:[this.patientReportNavigator.view_opd_report_url()],
            visible: this.tcAuthorization.canRead('opd_reports'),
          },
          {
            label: "IPD",
            icon: 'fa fa-user',
            routerLink:[this.patientReportNavigator.view_ipd_report_url()],
            visible: this.tcAuthorization.canRead('ipd_reports'),
          },
          {
          label: "ED",
          icon: 'fa fa-user',
          routerLink: [this.patientReportNavigator.view_ed_report_url()],
          visible: this.tcAuthorization.canRead('ed_reports'),
          },
            {
              label: this.appTranslation.getText('procedure', 'procedure'),
            icon: 'fa fa-book',
            routerLink:[this.reportNavigator.procedureReportsUrl()],
            visible: this.tcAuthorization.canRead('procedure_reports'),
          },
          {
            label: this.appTranslation.getText('patient', 'visits')+ ' Report',
            icon: 'fa fa-book',
            routerLink:[this.reportNavigator.encounterReportsUrl()],
            visible: this.tcAuthorization.canRead('encounter_reports'),
          },
          {
            label: this.appTranslation.getText('patient', 'patient'),
            icon: 'fa fa-book',
            routerLink:[this.reportNavigator.patientListReportsUrl()],
            visible: this.tcAuthorization.canRead('patient_list_reports'),
          },
          // {
          //   label: this.appTranslation.getText('patient', 'prescription'),
          //   icon: 'fa fa-book',
          //   routerLink:[this.reportNavigator.prescriptionAndDispensationReportsUrl()],
          //   visible: this.tcAuthorization.canRead('prescription_and_dispensation_reports'),
          // },
          {
            label: this.appTranslation.getText('financial', 'referral'),
            icon: 'fa fa-book',
            routerLink:[this.reportNavigator.refferalReportsUrl()],
            visible: this.tcAuthorization.canRead('referral_reports'),
          },
          // {
          //   label: "Item Expire",
          //   icon: 'fa fa-book',
          //   routerLink:[this.reportNavigator.itemExpireReportUrl()],
          //   visible: this.tcAuthorization.canRead('item_expire_reports'),
          // },
          // {
          //   label: this.appTranslation.getText('patient', 'certificates'),
          //   icon: 'fa fa-bed',
          //   items: [
          //     {
          //       label: this.appTranslation.getText('general', 'birth'),
          //       icon: 'fa fa-baby',
          //       routerLink:[this.birthNavigator.birthsUrl()],
          //       visible: this.tcAuthorization.canRead('births'),
          //     },
          //     {
          //       label: this.appTranslation.getText('patient', 'deaths'),
          //       icon: 'fa fa-book-dead',
          //       routerLink: [this.deathNavigator.deathsUrl()],
          //       visible: this.tcAuthorization.canRead('deaths'),
          //     },
          //     {
          //       label: this.appTranslation.getText(
          //         'patient',
          //         'driver_medical_certificate'
          //       ),
          //       icon: 'fa fa-id-badge',
          //       routerLink:[this.driverMedicalCertificateNavigator.driver_medical_certificatesUrl()],
          //       visible: this.tcAuthorization.canRead(
          //         'driver_medical_certificates'
          //       ),
          //     },
          //   ],
          // },
        ],
      },

      {
        label: 'Settings',
        icon: 'fa fa-list',
        items: [
          // {
          //   label: "Rooms",
          //   icon: 'fa fa-joomla',
          //   routerLink:[this.roomNavigator.roomsUrl()],
          //   visible: this.tcAuthorization.canCreate('rooms'),
          // },

          {
            label: this.appTranslation.getText('patient', 'orders_given'),
            icon: 'fa fa-flask',
            routerLink:[this.lab_orderNavigator.lab_ordersUrl()],
            visible: this.tcAuthorization.canCreate('order_givens') || this.tcAuthorization.canCreate('radiology_result_settings'),
          },
          // {
          //   label: "Dialysis Machine",
          //   icon: 'fa fa-machine',
          //   routerLink:[this.dialysisMachineNavigator.dialysisMachinesUrl()],
          //   visible: this.tcAuthorization.canCreate('dialysis_machines'),
          // },

               {
            label: "Machines",
            icon: 'fa fa-joomla',
            routerLink:[this.machineNavigator.machinessUrl()],
            visible: this.tcAuthorization.canCreate('machiness'),
          },

          {
            label: "Other Services",
            icon: 'fa fa-medkit',
            routerLink:[this.otherNavigator.otherServicessUrl()],
            visible: this.tcAuthorization.canCreate('other_services'),
          },
          // {
          //   label: "Radiology Result Settings",
          //   icon: 'fa fa-wrench',
          //   routerLink:[this.radiologyResultSettingNavigator.radiologyResultSettingssUrl()],
          //   visible: this.tcAuthorization.canCreate('radiology_result_settings'),
          // },
          {
            label: this.appTranslation.getText('patient', 'allergy'),
            icon: 'fa fa-users',
            routerLink:[this.allergyNavigator.allergysUrl()],
            visible: this.tcAuthorization.canCreate('allergys'),
          },
          {
            label: "Measuring Units",
            icon: 'fa fa-wrench',
            routerLink:[this.measuringUnitNavigator.measuring_unitsUrl()],
            visible: this.tcAuthorization.canCreate('measuring_units'),
          },
          {
            label: "Item List",
            icon: 'fa fa-wrench',
            routerLink:[this.itemNavigator.itemsUrl()],
            visible: this.tcAuthorization.canCreate('item_list'),
          },
          {
            label: "Banks",
            icon: 'fa fa-building-columns',
            routerLink:[this.bankNavigator.banksUrl()],
            visible: this.tcAuthorization.canCreate('banks'),
          },
          // {
          //   label: "Pos Machine",
          //   icon: 'fa fa-cash-register',
          //   routerLink:[this.posMachineNavigator.pos_machinesUrl()],
          //   visible: this.tcAuthorization.canCreate('pos_machines'),
          // },
          // {
          //   label: this.appTranslation.getText('patient', 'vaccinations'),
          //   icon: 'fa fa-tasks',
          //   routerLink:[this.vaccinationNavigator.vaccinationsUrl()],
          //   visible: this.tcAuthorization.canCreate('vaccinations'),
          // },
          // {
          //   label: this.appTranslation.getText(
          //     'patient',
          //     'review_of_system_options'
          //   ),
          //   icon: 'fa fa-list-alt',
          //   routerLink:[this.review_of_system_optionsNavigator.review_of_system_optionssUrl()],
          //   visible: this.tcAuthorization.canCreate('review_of_system_options'),
          // },
          // {
          //   label: this.appTranslation.getText(
          //     'patient',
          //     'physical_examination_options'
          //   ),
          //   icon: 'fa fa-filter',
          //   routerLink:[this.physicalExaminationOptionNavigator.physicalExaminationOptionssUrl()],
          //   visible: this.tcAuthorization.canCreate('physical_examination_options'),
          // },
          {
            label: this.appTranslation.getText('patient', 'procedure_types'),
            icon: 'fa fa-tasks',
            routerLink:[this.procedure_typeNavigator.procedure_typesUrl()],
            visible: this.tcAuthorization.canCreate('procedure_types'),          },
          {
            label: this.appTranslation.getText('procedure', 'Procedures'),
            icon: 'fa fa-tasks',
            routerLink:[this.procedureNavigator.proceduresUrl()],
            visible: this.tcAuthorization.canCreate('procedures'),
          },
          // {
          //   label: this.appTranslation.getText('inventory', 'supplier'),
          //   icon: 'fa fa-address-book',
          //   routerLink:[this.supplierNavigator.suppliersUrl()],
          //   visible: this.tcAuthorization.canCreate('suppliers'),
          // },
          // {
          //   label: this.appTranslation.getText('patient', 'surgery_types'),
          //   icon: 'fa fa-cut',
          //   routerLink:[this.surgery_typeNavigator.surgery_typesUrl()],
          //   visible: this.tcAuthorization.canCreate('surgery_types'),
          // },
          // {
          //   label: "Item Category",
          //   icon: 'fa fa-joomla',
          //   routerLink:[this.itemCategoryNavigator.itemCategorysUrl()],
          //   visible: this.tcAuthorization.canCreate('item_categorys'),
          // },
          {
            label: this.appTranslation.getText('patient', 'physiotherapy_type'),
            icon: 'fa fa-user-injured',
            routerLink:[this.physiotherapy_typeNavigator.physiotherapy_typesUrl()],
            visible: this.tcAuthorization.canCreate('physiotherapy_types'),
          },
          {
            label: this.appTranslation.getText('employee', 'department_specialty'),
            icon: 'fa fa-hotel',
            routerLink:[this.departmentSpecialityNavigator.department_specialtysUrl()],
            visible: this.tcAuthorization.canCreate('department_specialtys'),
          },
          {
            label: this.appTranslation.getText('bed', 'bed_room_level'),
            icon: 'fa fa-door-open',
            routerLink:[this.bedroomlevelNavigator.bedroomlevelsUrl()],
            visible: this.tcAuthorization.canCreate('bed_room_levels'),
          },
          // {
          //   label: "Surgery Room",
          //   icon: 'fa fa-door-open',
          //   routerLink: [this.surgeryRoomNavigator.surgeryRoomsUrl()],
          //   visible: this.tcAuthorization.canCreate('surgery_rooms'),
          // },
          {
            label: 'Ward',
            icon: 'fa fa-level-up-alt',
            routerLink:[this.bedroomtypeNavigator.bedroomtypesUrl()],
            visible: this.tcAuthorization.canCreate('wards'),
          },
          // {
          //   label: "Markup Price",
          //   icon: 'fa fa-money-check',
          //   routerLink:[this.markupPriceNavigator.markupPricesUrl()],
          //   visible: this.tcAuthorization.canCreate('markup_prices'),
          // },
          {
            label: 'Deposit options',
            icon: 'fa fa-tasks',
            routerLink:[this.depositOptionNavigator.depositOptionsUrl()],
            visible: this.tcAuthorization.canCreate('deposit_options'),
          },
          // {
          //   label: 'Chief Compliant options',
          //   icon: 'fa fa-tasks',
          //   routerLink:[this.chief_compliant_optionNavigator.chiefCompliantOptionsUrl()],
          //   visible: this.tcAuthorization.canCreate('chief_compliant_options'),
          // },
          // {
          //   label: 'Risk Factor Options',
          //   icon: 'fa fa-tasks',
          //   routerLink:[this.risk_factor_optionsNavigator.riskFactorsOptionssUrl()],
          //   visible: this.tcAuthorization.canCreate('risk_factors_options'),
          // },
          {
            label: "Outsourcing Company",
            icon: 'fa fa-industry',
            routerLink:[this.OutsourcingCompanyNavigator.OutsourcingCompanysUrl()],
            visible: this.tcAuthorization.canRead('outsourcing_companys'),
          },
          // {
          //   label: "Plane of Care",
          //   icon: 'fa fa-tree-christmas',
          //   routerLink:[this.planeOfCareNavigator.plan_of_caresUrl()],
          //   visible: this.tcAuthorization.canCreate('plan_of_cares'),
          // },

          {
            label: "Holiday",
            icon: 'fa fa-gift',
            routerLink:[this.holidayNavigator.holidaysUrl()],
            visible: this.tcAuthorization.canCreate('holidays'),
          },
        ],
      },
      {
        label: this.appTranslation.getText('telegram', 'telegram'),
        icon: 'fa fa-paper-plane',
        visible: this.tcAuthorization.canRead('tgidentitys'),
        routerLink:[this.tgidentityNavigator.tgidentitysUrl()],
      },
    ];
    this.items = this.shownTab(this.items)
  }

  shownTab(items: typeof this.items){
    for(let item of items){
      if (item.items){
        this.shownTab(item.items)
        item.expanded=item.items.some(value=> value.expanded ||(value.routerLink && this.isActiveTab(value.routerLink[0])))
        item.visible = item.items.some(value => value.visible)
      }
    }
    return items;
  }


  ngOnInit(): void {
    // ********************************************** socket notification ******************************
    // this.socketService.startListening();


    // this.router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {
    //     console.log('Location changed:', event.url);
    //     if(event.url != "/notification"){
    //       this.socketService.socket.on('updateCount',()=>{
    //         this.appPersit.notificationCount++;
    //         this.appPersit.notificationCountHidden=false;
    //       })
    //       this.socketService.socket.on('msg', (data) => {
    //         console.log('Received message from server:', data);

    //           this._snackBar.open(data.msg, 'Ok', {
    //             horizontalPosition:'right',
    //             verticalPosition: 'top',
    //             duration: 3 * 1000,
    //           });

    //       });
    //     }else{
    //       this.socketService.socket.off('updateCount');
    //       this.socketService.socket.off('msg');

    //     }

    //   }
    // });


    // ********************************************** socket notification ******************************



    this.onDetail = false;
    this.activeTab = tabs.overview;


    TCAppInit.appInitialized$.subscribe(async(res) => {
      if(res){
        console.log(this.tcAuthentication)



         if(TCAppInit.userInfo.user_id){
          this.notificationPersist.getNotificationCount(TCAppInit.userInfo.user_id).subscribe(res=>{
            console.log("getNotificationCount=====",res)
            this.appPersit.notificationCount = res.count.count
            if(res.count.count==0){
              this.appPersit.notificationCountHidden= true
            }
          })
    // ********************************************** socket notification ******************************


          // this.http.get(`http://localhost:5000/users/check/${TCAppInit.userInfo.user_id}`)
          // .subscribe((res:{role:number,user:{},status:string})=>{
          //   this.tCUtilsHttp.roleNumber = res.role;
          //   this.socketService.joinRoom("all")
          //   if(user_context.doctor==res.role){
          //       this.socketService.joinRoom("doctor")
          //   }
          //   if(user_context.nurse==res.role){
          //     this.socketService.joinRoom("nurs")
          // }
          //   console.log("====================",res)

          // },(error)=>{
          //   console.log("pppppppppp")
          // })

    // ********************************************** socket notification ******************************


         }

        this.loadMenu();
      this.intAppBehaviour();
      }
    });
  }


  languageChanged(): void {
    this.userPersist
      .updateLanguage(TCAppInit.userInfo.lang)
      .subscribe((result) => {
        this.tcAuthentication.reloadUserInfo(() => {
          this.tcNotification.success('Language profile updated');
          this.loadMenu();
        });
      });
  }

  updateNotificationCount(){
    this.notificationPersist.updateNotificationCount().subscribe(res=>{
      this.appPersit.notificationCount = 0
      this.appPersit.notificationCountHidden=true
      console.log("updateddddddddddddddddddddddddddddddddddddd!!!!!!")
    })
  }

  isActiveTab(routeStart: string): boolean {
    return this.router.url.startsWith(routeStart);
  }

  logout(): void {
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }

  redirectToCore() {
    window.location.replace(environment.core_emr_url);
  }

  returnToMainApp() {
    this.previousUrl = this.router.url;
    this.pathUrl = this.previousUrl.split('/');
    this.stateValues = null;
    this.activeTab = 0;
    this.onDetail = false;
  }

  front() {
    if (this.pathUrl.length == 3) {
      this.onDetail = true;
    }
    this.router.navigateByUrl(this.previousUrl);
  }

  listenTab(tab) {
    this.menuState.sendTabMenuResponse(tab);
  }

  tabItemData(tab, icon, title) {
    return { tab: tab, icon: icon, title: title };
  }

  commonTab = {
    tab: tabs.overview,
    icon: 'fa-info-circle',
    title: this.appTranslation.getText('general', 'overview'),
  };
  tabItems = { subtitle: '', icon: '', tabs: [] };

  patientTabs = {
    subtitle: this.appTranslation.getText('patient', 'patient'),
    icon: 'fa-user',
    tabs: [this.commonTab],
  };

  paymentTabs = {
    subtitle: this.appTranslation.getText('financial', 'payment'),
    icon: 'fa-user',
    tabs: [this.commonTab],
  };

  employeeTabs = {
    subtitle: this.appTranslation.getText('employee', 'store_employee'),
    icon: 'fa-store',
    tabs: [this.commonTab],
  };

  doctorTabs = {
    subtitle: this.appTranslation.getText('employee', 'physicians'),
    icon: 'fa-user-md',
    tabs: [this.commonTab],
  };

  receptionistTabs = {
    subtitle: this.appTranslation.getText('employee', 'receptionist'),
    icon: 'fa-concierge-bell',
    tabs: [this.commonTab],
  };

  procedureProviderTab = {
    subtitle: this.appTranslation.getText('procedure', 'procedure_providers'),
    icon: 'fa-flask',
    tabs: [
      this.commonTab,
      {
        tab: tabs.pending_review,
        icon: 'fa-spinner',
        title: this.appTranslation.getText('procedure', 'pending_review'),
      },
    ],
  };

  ordersTab = {
    subtitle: this.appTranslation.getText('procedure', 'order'),
    icon: 'fa-clipboard-list',
    tabs: [
      this.commonTab,
      {
        tab: tabs.lab_result,
        icon: 'fa-poll-h',
        title: this.appTranslation.getText('procedure', 'lab_results'),
      },
    ],
  };

  labListTab = {
    subtitle: this.appTranslation.getText('procedure', 'order'),
    icon: 'fa-flask',
    tabs: [
      this.commonTab,
      {
        tab: tabs.selected_orderss,
        icon: 'fa-poll-h',
        title: this.appTranslation.getText('procedure', 'selected_orders'),
      },
    ],
  };

  companyTab = {
    subtitle: this.appTranslation.getText('inventory', 'company'),
    icon: 'fa-industry',
    tabs: [
      this.commonTab,
      {
        tab: tabs.insured_employees,
        icon: 'fa-users',
        title: this.appTranslation.getText('inventory', 'insured_employees'),
      },
      {
        tab: tabs.credit_history,
        icon: 'fa-credit-card',
        title: this.appTranslation.getText('financial', 'credit_history'),
      },
    ],
  };

  radListTab = {
    subtitle: this.appTranslation.getText('procedure', 'order'),
    icon: 'fa-flask',
    tabs: [
      this.commonTab,
      {
        tab: tabs.rad_selected_orderss,
        icon: 'fa-poll-h',
        title: this.appTranslation.getText('procedure', 'selected_orders'),
      },
    ],
  };

  messageTabs = {
    subtitle: this.appTranslation.getText('general', 'message'),
    icon: 'fa-message',
    tabs: [this.commonTab],
  };
  encounterTabs = {
    subtitle: this.appTranslation.getText('patient', 'visits'),
    icon: 'fa-check-double',
    tabs: [
      this.commonTab,
      {
        tab: tabs.form_vitalss,
        icon: 'fa-heartbeat',
        title: this.appTranslation.getText('patient', 'form_vitals'),
      },

      {
        tab: tabs.physical_examinations,
        icon: 'fa-user-md',
        title: this.appTranslation.getText('patient', 'physical_examination'),
      },
      {
        tab: tabs.history_datas,
        icon: 'fa-users',
        title: this.appTranslation.getText('patient', 'family_history'),
      },
      {
        tab: tabs.diagnosis,
        icon: 'fa-diagnoses',
        title: this.appTranslation.getText('patient', 'diagnosis'),
      },
      {
        tab: tabs.form_soaps,
        icon: 'fa-bullseye',
        title: this.appTranslation.getText('patient', 'psoap'),
      },
      {
        tab: tabs.form_clinical_instruction,
        icon: 'fa-sticky-note',
        title: this.appTranslation.getText('patient', 'clinical_instruction'),
      },
      {
        tab: tabs.prescriptionss,
        icon: 'fa-file-medical',
        title: this.appTranslation.getText('patient', 'prescription'),
      },
      {
        tab: tabs.medical_adminstration_chart,
        icon: 'fa-calendar-check',
        title: this.appTranslation.getText(
          'patient',
          'medication_adminstration_chart'
        ),
      },
      {
        tab: tabs.notes,
        icon: 'fa-sticky-note',
        title: this.appTranslation.getText('notes', 'notes'),
      },

      {
        tab: tabs.allery_form,
        icon: 'fa-sticky-note',
        title: 'Allergy form',
      },

      {
        tab: tabs.consultings,
        icon: 'fa-handshake',
        title: this.appTranslation.getText('patient', 'consulting'),
      },

      {
        tab: tabs.lab_orders,
        icon: 'fa-flask',
        title: this.appTranslation.getText('patient', 'Lab_orders'),
      },

      {
        tab: tabs.bed_assignment,
        icon: 'fa-gavel',
        title: this.appTranslation.getText('bed', 'bed_assignment'),
      },

      {
        tab: tabs.orders,
        icon: 'fa-clipboard-list',
        title: this.appTranslation.getText('procedure', 'result'),
      },
      {
        tab: tabs.surgery_appointments,
        icon: 'fa-calendar-check',
        title: this.appTranslation.getText('patient', 'surgery_appointments'),
      },
      {
        tab: tabs.review_of_systems,
        icon: 'fa-stethoscope',
        title: this.appTranslation.getText('patient', 'review_of_system'),
      },
      {
        tab: tabs.form_observations,
        icon: 'fa-eye',
        title: this.appTranslation.getText('patient', 'observation'),
      },
      {
        tab: tabs.case_transfer,
        icon: 'fa-retweet',
        title: this.appTranslation.getText('patient', 'case_transfer'),
      },
      {
        tab: tabs.doctor_private_info,
        icon: 'fa-info',
        title: this.appTranslation.getText('patient', 'private_info'),
      },
      {
        tab: tabs.medical_certificates,
        icon: 'fa-certificate',
        title: this.appTranslation.getText('patient', 'medical_certificate'),
      },
      {
        tab: tabs.patient_procedures,
        icon: 'fa-procedures',
        title: this.appTranslation.getText('procedure', 'patient_procedures'),
      },
      {
        tab: tabs.medical_certificate_employee,
        icon: 'fa-certificate',
        title: this.appTranslation.getText(
          'patient',
          'employee_medical_certificate'
        ),
      },

      {
        tab: tabs.encounter_imagess,
        icon: 'fa-image',
        title: this.appTranslation.getText('patient', 'images'),
      },
      {
        tab: tabs.follow_up_note,
        icon: 'fa-sticky-note',
        title: this.appTranslation.getText('patient', 'follow_up_note'),
      },
      {
        tab: tabs.rehabilitation_orders,
        icon: 'fa-user-injured',
        title: this.appTranslation.getText('patient', 'physiotherapy'),
      },
      {
        tab: tabs.other_encounters,
        icon: 'fa-check-double',
        title: 'Other Encounters',
      },
      {
        tab: tabs.delivery_summary,
        icon: 'fa-baby',
        title: 'Delivery Summary',
      },
      {
        tab: tabs.radiology_data,
        icon: 'fa-diagnoses',
        title: 'Radiology Order',
      },
      {
        tab: tabs.present_pregenancy_follow_up,
        icon: 'fa-users',
        title: 'Pregenancy Follow Up',
      },
      {
        tab: tabs.intrapartum_care,
        icon: 'fa-users',
        title: 'Intrapartum Care',
      },
    ],
  };

  admissionTabs = {
    subtitle: this.appTranslation.getText('patient', 'admission_forms'),
    icon: 'fa-book',
    tabs: [this.commonTab],
  };

  storeTabs = {
    subtitle: this.appTranslation.getText('inventory', 'stores'),
    icon: 'fa-warehouse',
    tabs: [this.commonTab],
  };

  itemTabs = {
    subtitle: this.appTranslation.getText('inventory', 'items'),
    icon: 'fa-boxes',
    tabs: [this.commonTab],
  };


  deliverySummaryTabs = {
    subtitle: 'Delivery Summary',
    icon: 'fa-baby',
    tabs: [this.commonTab],
  };

  depositTabs = {
    subtitle: this.appTranslation.getText('financial', 'deposit'),
    icon: 'fa-wallet',
    tabs: [
      this.commonTab,
      {
        tab: tabs.deposit_historys,
        icon: 'fa-wallet',
        title: this.appTranslation.getText('financial', 'deposit'),
      },
    ],
  };

  driverLicenseMedicationTabs = {
    subtitle: this.appTranslation.getText(
      'patient',
      'driver_medical_certificate'
    ),
    icon: 'fa-user',
    tabs: [this.commonTab],
  };

  tabActionHandler() {
    if (this.isActiveTab('/patients')) {
      this.subtitle =
        this.stateResponseData.fname + ' ' + this.stateResponseData.lname;
      this.tabItems = this.patientTabs;
    } else if (this.isActiveTab('/form_encounters')) {
      this.subtitle = this.stateResponseData.full_name;
      this.pid = this.stateResponseData.pid;
      this.tabItems = this.encounterTabs;
    } else if (this.isActiveTab('/doctors')) {
      this.subtitle =
        this.stateResponseData.first_name +
        ' ' +
        this.stateResponseData.last_name;
      this.tabItems = this.doctorTabs;
    } else if (this.isActiveTab('/employees')) {
      this.subtitle =
        this.stateResponseData.first_name +
        ' ' +
        this.stateResponseData.last_name;
      this.tabItems = this.employeeTabs;
    } else if (this.isActiveTab('/receptions')) {
      this.subtitle =
        this.stateResponseData.first_name +
        ' ' +
        this.stateResponseData.last_name;
      this.tabItems = this.receptionistTabs;
    } else if (this.isActiveTab('/admission_forms')) {
      this.subtitle =
        this.stateResponseData.serial_id +
        ' ' +
        this.stateResponseData.encounter_id;
      this.tabItems = this.admissionTabs;
    } else if (this.isActiveTab('/stores')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.storeTabs;
    } else if (this.isActiveTab('/items')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.itemTabs;
    } else if (this.isActiveTab('/deposits')) {
      this.subtitle = this.stateResponseData.current_amount;
      this.tabItems = this.depositTabs;
    } else if (this.isActiveTab('/procedure_providers')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.procedureProviderTab;
    } else if (this.isActiveTab('/messagess')) {
      this.tabItems = this.messageTabs;
    } else if (this.isActiveTab('/orderss')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.ordersTab;
    } else if (this.isActiveTab('/lab-orders')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.labListTab;
    } else if (this.isActiveTab('/rad-orders')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.radListTab;
    } else if (this.isActiveTab('/driver_medical_certificates')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.driverLicenseMedicationTabs;
    } else if (this.isActiveTab('/delivery_summary')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.deliverySummaryTabs;
    } else if (this.isActiveTab('/payments')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.paymentTabs;
    } else if (this.isActiveTab('/Companys')) {
      this.subtitle = this.stateResponseData.name;
      this.tabItems = this.companyTab;
    }
    return this.tabItems;
  }



  intAppBehaviour(){
    this.colorService.searchProperties().subscribe(
      {
        next:res=>{

            // title
            if(res.title != null){
              this.titleService.setTitle(res.title);
            }
            else{
              this.titleService.setTitle("eHealth");
            }

          //set logo
          if(res.app_icon != null){
            this.logo = res.app_icon ;
          }
          else{
            this.logo = "assets/images/logo.png";
          }
          //set icon

          if(res.fav_icon != null){
            this._document.getElementById('appIcon').setAttribute('href', res.fav_icon);

          }
          else{
            this._document.getElementById('appIcon').setAttribute('href', "assets/images/favicon.ico");
          }

           //set base color
           if(res.base_color != null){
            this._document.documentElement.style.setProperty('--base-color', res.base_color);
          }
          else{
            this._document.documentElement.style.setProperty('--base-color', "#132321");
          }
          // set secondary
          if(res.secondary_color != null){
            this._document.documentElement.style.setProperty('--secondary-color',res.secondary_color);
          }
          else{
            this._document.documentElement.style.setProperty('--secondary-color', "#323232");
          }

          // set third
          if(res.third_color != null){
            this._document.documentElement.style.setProperty('--third-color',res.third_color);
          }
          else{
            this._document.documentElement.style.setProperty('--third-color', "#276898");
          }

          // set fourth
          if(res.fourth_color != null){
            this._document.documentElement.style.setProperty('--fourth-color', res.fourth_color);
          }
          else{
            this._document.documentElement.style.setProperty('--fourth-color', "#1a73e8");
          }

          // set fifth

          if(res.fith_color != null){
            this._document.documentElement.style.setProperty('--fith-color',res.fith_color );
          }
          else{
            this._document.documentElement.style.setProperty('--fith-color', "#6c757d");
          }

          // set sixth

          if(res.sixth_color != null){
            this._document.documentElement.style.setProperty('--sixth-color', res.sixth_color )
          }
          else{
            this._document.documentElement.style.setProperty('--sixth-color', "#495057")
          }

          // set seventh
          if(res.seventh_color != null){
            this._document.documentElement.style.setProperty('--seventh-color',res.seventh_color);
          }
          else{
            this._document.documentElement.style.setProperty('--seventh-color', "#307cb3ab");
          }

          // set eigth

          if(res.eight_color != null){
            this._document.documentElement.style.setProperty('--eight-color', res.eight_color );
          }
          else{
            this._document.documentElement.style.setProperty('--eight-color', "#777");
          }

            // set ninth

            if(res.ninth_color != null){
              this._document.documentElement.style.setProperty('--ninth-color',res.ninth_color );
            }
            else{
              this._document.documentElement.style.setProperty('--ninth-color', "#d0eaff");
            }

              // set text

          if(res.text_color != null){
            this._document.documentElement.style.setProperty('--text-color', res.text_color);
          }
          else{
            this._document.documentElement.style.setProperty('--text-color', "white");
          }



        }
      }
    );

  }
}
