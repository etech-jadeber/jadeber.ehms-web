//enums

import {TCMandatoryTypes} from "./tc/models";

export class AppEventTypes {
}

export class AppMandatoryTypes extends TCMandatoryTypes {
}


export class Event_Type {
  static app_state_event = 1;
  static identity_management = 11;
  static user_management = 12;
  static user_login_success = 13;
  static user_login_failed = 14;
  static user_password_changed = 15;
  static case_management = 21;
  static case_state_change = 22;
  static data_add_event = 31;
  static data_update_event = 32;
  static data_delete_event = 33;
  static job_event = 41;
  static file_event = 51;
  static file_download_event = 52;
}

export class UserContexts {

}

//Job States
export class Job_States {
  static queued = 1;
  static running = 2;
  static success = 3;
  static failure = 4;
  static aborted = 5;
}

//Job Types
export class Job_Types {
  static test = -1;
  static tc_download = 1;
  static tc_print = 2;
}

export class Setting_Type {
  static user_pwd_reset = 1;
  static user_language = 2;
  static user_activation = 3;
  static user_otp = 4;
  static user_otp_csrf = 5;
  static telegram_login = 6;
}


export class bed_status {
  static maintainance: number = 100;
  static occupied: number = 102;
  static available: number = 101;
}

export class patient_priority {
  static low: number = 100;
  static medium: number = 101;
  static high: number = 102;
}

export class room_status {
  static active: number = 100;
  static inactive: number = 101;
}


export class bed_assignment_status {
  static active: number = 100;
  static inactive: number = 101;
}

export class waiting_status {
  static waiting: number = 100;
  static assigned: number = 101;
  static canceled: number = 102;
}

export class service_type {
  static other_service: number = 100;
  static medication: number = 101;
  static guard: number = 102;
}

export class item_type {
  static drug: number = 101;
  static equipment: number = 102;
  static consumable: number = 100;
}

export class product_category {
  static Ointment: number = 100;
  static Cream: number = 101;
  static Injection: number = 102;
  static Suspension: number = 103;
  static Capsule: number = 104;
  static Inhaler: number = 105;
  static Lotion: number = 106;
  static OralRinse: number = 107;
  static Drops: number = 108;
  static Solution: number = 109;
  static MouthPaint: number = 110;
  static Pessary: number = 111;
  static Gel: number = 112;
  static OralSolution: number = 113;
  static Powder: number = 114;
  static NasalSolution: number = 115;
  static Rotacap: number = 116;
  static Syrup: number = 117;
}

export class request_status {
  static firstrejected: number = 102;
  static submitted: number = 100;
  static approved: number = 101;
  static dispatched: number = 103;
  static secondrejected: number = 104;
}

export class units {
  static piece: number = 100;
  static box: number = 101;
  static pack: number = 102;
  static vial: number = 103;
}

export class entry_type {
  static sales: number = 102;
  static purchase: number = 100;
  static transfer: number = 101;
  static lost_damage: number = 103;
}

export class report_type {
  static sales: number = 102;
  static purchase: number = 100;
  static transfer: number = 101;
  static all: number = 103;
  static doctor_encounter: number = 104;
}


export class Purchase_type {
  static annual: number = 100;
  static stock_out: number = 101;
  static other: number = 102;
}

export class payment_type {
  static card_fee: number = 100;
  static laboratory: number = 101;
  static radiology: number = 102;
}

export class transfer_type {
  static in: number = 100;
  static out: number = 101;
}

export class other_type {
  static no_type: number = 100;
  static vat: number = 101;
  static tot: number = 102;
}

export class schedule_type {

  static new_patient: number = 100;
  static revisit: number = 101;
  static follow_up: number = 102;
  static no_show: number = 103;
  static repeat: number = 104;
  static repayment: number = 105;
}


export class repetition {
  static once: number = 100;
  static every_day: number = 101;
  static once_a_week: number = 102;
  static once_a_month: number = 103;
}


export class day_of_the_week {
  static Monday: number = 100;
  static Tuesday: number = 101;
  static Wednesday: number = 102;
  static Thursday: number = 103;
  static Friday: number = 104;
  static Saturday: number = 105;
  static Sunday: number = 106;
}

export class appointment_type {
  static in_patient: number = 100;
  static out_patient: number = 101;
  static emergency: number = 102;
}


export class user_context {
  static employee: number = 103;
  static patient: number = 104;
  static front_desk: number = 105;
  static doctor: number = 106;
  static nurse: number = 107;
  static lab : number = 108;
  static radiology : number = 109;
  static physiotherapy : number = 110;
  // static midwives : number = 111;
  // static driver: number = 112;
  static mental_health_professional: number = 113;
  static psychatric_nurse: number = 114;
  static pharmacist: number = 115;
  static reception: number = 116;

}

export class physican_type {
  static doctor: number = 106;
  static nurse: number = 107;
  static lab: number = 108;
  static radiology: number = 109;
  static physiotherapy: number = 110;
  static mental_health_professional: number = 113;
  static psychatric_nurse: number = 114;
  static pharmacist: number = 115;
  // static midwives: number = 111;
}





export class store_type {
  static main: number = 100;
  static outpatient: number = 101;
  static inpatient: number = 102;
  static emergency: number = 103;
  static child: number = 104;
  static ward_store:number = 106;
  static lab_store:number = 107;
  static rad_store:number = 108
  static anesthesia_store:number = 109;
  static or_store:number = 110;
  static opd_store: number = 111;
  static endoscopy_store: number = 112;

}

export class physician_type {
  static cardiologist: number = 101;
  static neurologist: number = 100;
  static general: number = 102;
}

export class consultationStatus {
  static no_response: number = 100;
  static response_given: number = 101;
}


export class surgery_status {
  static sent: number = 100;
  static appointed: number = 101;
  static on_surgery: number = 102;
  static cancelled: number = 103;
  static completed: number = 104;
  static rejected: number = 105;
}
export class deposit_history_type {
  static additional_deposit: number = 102;
  static initial_deposit: number = 100;
  static medical_expense: number = 101;
  static take_out: number = 103;
}

export class tabs {
  static overview: number = 100;
  static test: number = 101;
  static form_soaps = 102;
  static form_vitalss = 103;
  static prescriptionss = 104;
  static admission_forms = 105;
  static procedure_order = 106;
  static history_datas = 107;
  static notes = 108;
  static form_clinical_instruction = 109;
  static bed_assignment = 110;
  static surgery_appointments = 111;
  static form_observations = 112;
  static review_of_systems = 113;
  static deposit_historys = 114;
  static doctor_private_info = 116;
  static medical_certificates = 117;
  static encounter_imagess = 118;
  static case_transfer = 119;
  static pending_review = 120;
  static physical_examinations = 131;
  static history = 121;
  static diagnosis = 130;
  static orders = 124;
  static lab_result = 123;
  static lab_orders = 135;
  static follow_up_note = 125;
  static selected_orderss = 126;
  static rad_selected_orderss = 127;
  static rehabilitation_orders = 128;
  static other_encounters = 158;
  static patient_procedures = 159;
  static selected_orderss_to_approve = 160;
  static consultings = 164;
  static medical_certificate_employee = 161;
  static biopsy_requests = 162;
  static medical_adminstration_chart = 163;
  static delivery_summary = 184;
  static insured_employees = 164;
  static credit_history = 165;
  static allery_form = 190;
  static nurse_record = 160;
  static nurse_record_detail = 161;
  static radiology_data = 181;
  static present_pregenancy_follow_up = 182;
  static intrapartum_care=93;
}

export class discharge_condition {
  static treated: number = 100;
  static worsen: number = 101;
  static died: number = 102;
  static no_change: number = 103;
}

export class message_type {
  static pharmacy: number = 100;
static bill_collect: number = 101;
static priority: number = 102;
static lab_result: number = 103;
}

export class SurgeryNoteWriters{
  static surgeon: number = 100;
  static assistant: number =101;
  static anesthesis: number = 102;
  static scrub: number = 103;
  static runner_nurse: number = 104;
}


export class message_status {
  static new: number = 102;
static done: number = 100;
static read: number = 103;
static forwarded: number = 101;
}

export class encounter_status {
  static opened: number = 100;
  static closed: number = 101;
}

export class deposit_status {
  static opened: number = 100;
  static closed: number = 101;
}


export class procedure_status {
  static free: number = 104;
static paid: number = 100;
static not_paid: number = 101;
static credit_by_employee: number = 102;
static credit_by_family: number = 105
static referal: number = 103;
}
export class company_insurance_payment_types {
  static yearly: number = 100;
  static montly: number = 101;
}

export class payment_due_status {
  static covered: number = 100;
  static pending: number = 101;
}
export class drug_form{
  static suspension: number = 1;
  static tablet: number = 2;
  static capsule: number = 3;
  static solution: number = 4;
  static tsp: number = 5;
  static ml: number = 6;
  static units: number = 7;
  static inhalations: number = 8;
  static drops: number = 9;
  static cream: number = 10;
  static ointment: number = 11;
  static puff: number = 12;
  static kit: number = 13;
  static piece: number = 15;
  static sachet:number =16;
  static tin: number = 17;
  static jar: number = 18;
  static box: number = 19;
  static cannister: number = 20;
  static  vial: number = 21;
  static syrup: number = 22;
  static injection: number = 23;
  static suppository: number = 24;
  static ampule: number = 25;
  static bottel: number = 26;
}
export class drug_unit{
    static mg: number = 1;
    static mg1cc: number = 2;
    static mg2cc: number = 3;
    static mg3cc: number = 4;
    static mg4cc: number = 5;
    static mg5cc: number = 6;
    static mcg: number = 7;
    static grams: number = 8;
    static mL: number = 9;

}

export class dosage_unit {
  static mg : number = 100;
  static gm:number = 101;
  static ml:number = 102;
  static l :number = 103;
  static iu:number = 104;
  static kit: number = 13;
  static pack: number =14;
  static piece: number = 15;
  static sachet:number =16;
  static roll: number = 17;
  static bag: number = 10;
  static percent: number = 11;
  static pair : number = 12;
  static mg_ml:number = 18;
  static mg_2ml: number = 19;
  static mg_3ml: number = 20;
  static mg_5ml: number = 21;
  static mg_10ml: number = 22;

}

export class encounter_type {
  static normal: number = 100;
static referred: number = 101;
static uncouncious: number = 102;
}


export class appointment_patient_status {
  static alert: number = 100;
static uncouncious: number = 101;
static verbal: number = 103;
static pain: number = 104;
}
export class session_therapy_session_type {
  static group_therapy: number = 100;
// static uncouncious: number = 101;

}
export class session_therapy_status {
  static active: number = 100;
static deactive: number = 101;

}
export class group_member_status {
  static active: number = 100;
static deactive: number = 101;

}
export class sensetivity {
static urgent: number = 101;
static non_urgent: number = 100;
}
export class temp_method {
  static oral: number = 100;
  static rectal: number = 101;
  static axillary: number = 102;
  static tympanic: number = 103;
  static infrared : number = 104;
}
export class payment_season {
  static holiday: number = 103;
static weekend: number = 101;
static night_shift: number = 102;
static normal: number = 100;
}
export class Condition_status {
  static cured: number = 100;
static worsened: number = 101;
static no_change: number = 102;
static died: number = 103;
static improved: number = 104;
static against_medical: number = 105;
}
export class Discharge_status {
  static ordered: number = 100;
  static discharged: number = 101;

}

export class fee_type {
  static bed: number = 102;
	static item: number = 103;
	static card: number = 100;
	static procedure: number = 104;
	static lab_test: number = 101;
	static lab_panel: number = 105;
	static dialysis: number = 109;
	static physio_theraphy: number = 108;
	static rad_test: number = 107;
	static rad_panel: number = 106;
  static other_service:number = 110;
  static pat_test: number = 111;
  static pat_panel: number = 112;
  static medical_package: number = 113;
  static ambulance_in_km: number = 114;
  static ambulance_in_waiting: number = 115;
  static doctor: number = 118;
  static operation_room_fee: number = 119;
  static oxygen_service: number = 121;
  static guard: number = 150;


}
export class lab_order_type {
  static imaging: number = 100;
static lab: number = 101;
static pathology: number = 102;
}
export class lab_urgency {
  static normal: number = 100;
static urgent: number = 101;
static critical: number = 102;
}

export class diagnosis_order {
  static primary: number = 100;
static secondary: number = 101;
}

export class Admission_status {
  static admitted: number = 100;
  static readmitted: number = 101;
}

export class Occurance {
  static new_occurance: number = 100;
  static repeated: number = 101;
  static new_repeat: number = 102;
}

export class diagnosis_certainty {
  static confirmed: number = 100;
static presumed: number = 101;
}

export class diagnosis_status {
  static active: number = 100;
static inactive: number = 101;
}

export class lab_order_status {
  static ordered: number = 100;
static started: number = 101;
static done: number = 102;
}
export class vaccinated_patient_type {
  static child: number = 100;
static patient: number = 101;
}

export class death_condition {vital_status_with_age
  static Antepartum: number = 100;
static Intrapartum: number = 101;
static Postpartum: number = 102;
}
export class procedure_type {
  static major: number = 100;
  static minor: number = 101;
}
export class  admission_form_status{
  static sent: number = 100;
  static bed_assigned: number = 101;
  static nurse_assigned: number = 102;
  static discharged: number = 103;
}


export class appointment_status {
  static pending: number = 100;
static started: number = 101;
static patient_cancelled: number = 103;
static no_show_up: number = 102;
static doctor_cancelled: number = 104;
static appointment_ended:number =105;
}
export class selected_orders_status{
    static submitted: number = 100
    static  ready_to_approve: number = 101;
    static  approved: number = 102;
    static  rejected: number = 103;
}


export class payment_status {
static paid: number = 100;
static not_paid: number = 101;
static covered: number = 102;
  static Cancelled: number = 103;
  static Refunded: number = 104;
}

export class DriverLicenseType{
  static granted: number = 100;
  static granted_with_restriction: number = 101;
  static refused: number = 102;
}

export class MedicationRoute{
  static Intramuscular: number = 100;
  static Intravenous: number = 101;
  static Oral: number = 102;
  static Subcutaneous: number = 103;
  static Per_Rectum: number = 104;
  static Per_Vaginal: number = 105;
  static Sub_Lingual: number = 106;
  static Nasogastric: number = 107;
  static Intradermal: number = 108;
  static Intraperitoneal: number = 109;
  static Intrathecal: number = 110;
  static Intraosseous: number = 111;
  static Topical: number = 112;
  static Nasal: number = 113;
  static Inhalation: number = 114;
}

export class PainStatus {
  static No_Pain: number = 100;
  static Mild_Pain: number = 101;
  static Moderate_Pain: number = 102;
  static Severe_Pain: number = 103;
  static Worst_Pain: number = 104;
}

export class SurgeryType{
  static Elective: number = 100;
  static Emergency: number = 101
}

export class vat_status{
  static no_vat: number = 100;
  static vat: number = 101;
}

export class PaymentType{
  static CreditByEmployee: number = 100;
  static Cash: number = 101;
  static Deposit: number = 102;
  static Card: number = 103;
  static Free: number = 104;
  static CreditByFamily: number = 105;
  static transfer: number = 106;
}

export class AllergyType{
  static medication:number  = 100;
  static food :number  =101;static seasonal :number  =102; static animal :number  =103; static other :number  =104 ;
}

export class AdmissionStatus{
  static general:number  = 100;
  static accident:number  = 101;
  static surgery:number  = 102;
  static observation:number  = 103;
}

export class HIVTestResultStatus {
  static R:number = 100;
  static NR:number = 101;
  static I:number = 102;
}

export class PostNatalVisit {
  static First_Visit: number = 100;
  static Second_Visit: number = 101;
  static Third_Visit: number = 102;
}

export class PostNatalCareStatus {
  static completed: number = 100;
  static incomplete: number = 101;
}
export class equipment_type {
  static medical_equipment: number = 100;
  static consumables: number = 101;
  static spare_part: number = 102;
}

export class current_condition {
  static operable_and_in_service: number = 100;
  static operable_and_outof_service: number = 101;
  static requires_maintainance: number = 102;
  static not_repairable: number = 103;
}

export class available_manuals {
  static user_manual: number = 100;
  static service_manual: number = 101;
  static other: number = 102;
}

export class equipment_user {
  static doctor: number = 100;
  static nurse: number = 101;
  static lab: number = 102;
  static rad: number = 103;
  static resident: number = 104;
}

export class power_requirement {
  static small: number = 110;
  static large: number = 220;
  static neutral: number = -1;
}

export class BedAssignmentStatus{
  static urgent:number  = 100;
  static normal:number  = 101;
}
export class State{
  static addisAbaba:number = 100;
  static tigray:number = 101;
  static amhara:number = 102;
  static oromia:number = 103;
  static benishangul:number = 104;
  static afar:number = 105;
  static somalia:number = 106;
  static harar:number = 107;
  static diredawa:number = 108;
  static south:number = 109;
  static sidama:number = 110;
  static south_west:number = 111;
  static gambella:number = 112;
}


export class SubmissionStatus {
  static submitted:number = 100;
  static ordered: number = 101;
}

export class PatientEducationalStatus{
  static uneducated:number = 100;
  static elementary1:number = 101;
  static elementary2:number = 102;
  static highschool:number = 103;
  static preparatory:number = 104;
  static tvet:number = 105;
  static university:number = 106;
  static degree:number  = 107;
  static master:number = 108;
  static phd : number = 109;
}

export class MaritalStatus{
  static single:number =100;
  static married:number =101;
  static widowed:number =102;
  static divorced:number=103;
  static separated:number=104;

}

export class VisitStatus{
  static firstVisit:number=100;
  static secondVisit:number =101;
  static thirdVisit:number =102;
  static fourthVisit:number=103;
}

export class Status{
  static incompleted:number=100;
  static completed:number =101;
}

export class SampleType{
  static blood: number = 100;
  static urine: number = 101;
  static serum: number = 102;
  static semen: number = 103;
  static stool: number = 104;
  static csf: number = 105;
  static tissue: number = 106;
  static sputum: number = 107;
  static viginal_fluid: number = 110;
  static pleural_fluid: number = 111;
  static other_body_fluid: number = 112;
  static knee_joint_fluid: number = 108;
  static peritoneal_fluid: number = 109;
  static pus: number = 113;
  static others: number = 114;
}

export class MeasuringUnit{
  static "X10^3/MM^3": number = 102;
  static PG: number = 104;
  static "U/L": number = 106;
  static "G/DL": number = 100;
  static "%": number = 101;
  static FL: number = 103;
  static "X10^6/UL": number = 105;
  static "NG/DL": number = 108;
  static "ML/MIN/1.73M^2": number = 111;
  static "PG/ML" : number = 107;
  static "MICRO IU/ML": number = 109;
  static "CELLS/HPF": number = 110;
  static HPF: number = 112;
  static IPF: number = 113;
}

export class AnatomicCite{
  static "Arm": number = 100;
  static "Buttock": number = 101;
  static "Other": number = 102;
}
export class MeansOfArrival{
  static ambulance:number = 100;
  static walk_in:number = 101;
  static public_service:number = 102;

}


export class Severity{
  static rescuitation:number = 100;
  static emergent:number = 101;
  static urgent:number = 102;
  static less_urgent:number = 103;
  static non_urgent:number = 104;
}

export class ServiceType{
  static inpatient:number = 102;
  static outpatient:number = 100;
  static emergency:number = 101;
  // static referral: number = 103;
  static  outsideOrder: number = 104;
  static referral: number = 103;
  static inhouse_patient: number = 106
  static all: number = 90;
}

export class PrescriptionStatus{
  static prescribed: number = 100;
  static dispatched: number = 101;
  static void: number = 102;
}

export class OutsideOrderType {
 static prescription: number = 103;
 static lab_order: number = 101;
 static rad_order: number = 100;
 static pat_order: number = 102;
 static procedure: number = 104;
 static other_service: number = 105;
 static physiotherapy: number = 106;
 }

export class OrderType extends OutsideOrderType{
  static both_lab_rad_order: number = 103;
  static lab_rad_pat_order: number = 104;
}
 export class PatientType{
   static outPatient: number = 100;
   static inPatient: number = 101;
   static referral : number = 102;
   static outsideOrder: number = 104;
 }

 export class ResultType {
  static lab: number = 100;
  static pat: number = 101;
  static img: number = 102;
}
export class DiagnosisTypes {
  static lab: string = "lab";
  static rad: string = "rad";
  static pat: string = "pat";
}


export class DialysisType{
  static hemodialysis:number = 100;
  static peritoneal:number = 101;
}

export class DialysisSubTypes{
  static intravenous:number = 100;
  static fistula:number = 101;
  static APD:number = 102;
  static CAPD:number = 103;
}

export class FeePaymentType {
  static once: number = 100;
 static recurring: number = 101;

 }

 export class OrderStatus {
  static completed: number = 100;
  static not_completed: number = 101;
 }

 export class Department {
  static outpatient: number = 100;
  static emergency: number = 101;
  static inpatient: number = 102;
 }
 export class InsuredEmployeeStatus {
  static covered: number = 100;
 static terminated: number = 101;
 }

 export class ActivenessStatus {
  static active: number = 100;
 static freeze: number = 101;
 }

 export class RoomStatus{
  static empty:number = 100;
  static occupied:number  = 101;
 }


 export class DoctorInRoomStatus{
  static empty:number = 100;
  static occupied:number  = 101;
 }


 export class QueueType{
  static doctor:number = 100;
  static nurse:number = 101;
  static lab:number = 102;
  static rad:number = 103;
  static procedure:number = 104;
 }

 export class PhysicalExaminationOptions {
  static ent : number = 101;
  static eye : number = 102;
  static head_and_neck : number = 103;
  static glands : number = 104;
  static chests : number = 105;
  static cardiovascular : number = 106;
  static abdominal : number = 107;
  static genitourinary : number = 108;
  static musculoskeletal : number = 109;
  static integument : number = 110;
  static neurology : number = 111;
 }

 export class PhysicalExaminationState {
  static normal: number = 100;
 static abnormal: number = 101;
 }

 export class indication_for_homodialysis{
  static uremia : string ='Uremia with encephalopathy';
  static excessive : string ='Excessive fluid retantion refractory to diuresis';
  static electrolyte : string ='Electrolyte Imbalance';
  static pericardits_pleurisy : string ='Pericardits and or pleurisy';
  static nephrologist : string ='Nephrologist recommendation';
  static uremic : string ='Uremic Encephalopathy';
  static pericardits : string ='Pericarditis';
  static bleeding : string ='Bleeding associated with Uremia';
  static fluid : string ='Fluid overload unresponsive';
  static electrolyte_imbalance : string ='Electrolyte imbalance (Severe and intractable Hyperkalemia';
  static acid_base : string ='Acid Base imbalance (Severe Metabolic Acidosis';
  static accelerated : string ='Accelerated hypertension poorly responsive to antihypertensives';
 }
 export class serology{
  static positive:number=100;
  static negative:number=101;
 }

 export class other_reason{
  static poisoning:number=100;
  static drug_overdose:number=101;
  static other:number=102;
 }

 export class relevant_investigation{
  static hgb:number=100;
  static calcium:number=101;
  static po:number=102;
  static pth:number=103;
 }

 export class dialysis_acess_data_inserted{
  static temporary_catheter:number=100;
  static permanent_catheter:number=101;
  static AV_fistula:number=102;
  static AV_graft:number=103;
 }

 export class renal_failure{
  static AKI:number=100;
  static CKD:number=100;
  static AKI_on_CKD:number=100;
 }
export class underlying_causes{
  static hypertension :string="Hypertension";
  static lupus_nephritis :string="Lupus nephritis";
  static diabetes_mellitus :string="Diabetes mellitus";
  static drug_induced :string="Drug Induced";
}

export class dialysis_service_type{
  static dialysis_service:number=100;
}

export class session_status{
  static started:number =100;
  static completed:number =101;
}

export class AdditionalServiceType {
  static ambulance: number = 100;
 static meal: number = 101;
 static oxygen: number = 102;

 }

 export class AmbulanceServiceType {
  static advanced_life_support: number = 100;
 static basic_life_support: number = 101;
 static patient_transport: number = 102;

 }

 export class TripType {
  static to_hosptial: number = 100;
 static from_hospital: number = 101;
 }

 export class MealFrequency {
  static once_a_day: number = 100;
 static twice_a_day: number = 101;
 static three_time_a_day: number = 102;
 }
export class TreatmentPerWeek{
  static daily :number = 100;
  static _6xWeekly :number = 101;
  static _5xWeekly :number = 102;
  static _4xWeekly :number = 103;
  static _3xWeekly : number = 104;
  static _2xWeekly :number = 105;
  static _1xWeekly :number = 106;
}

export class durationUnit {
  static minute:number = 100;
  static hour :number = 101;
  static day :number = 102;
  static week :number = 103;
  static month :number = 104;
  static year :number = 105;
}

export class PatientCondition {
  static stable:number = 100;
  static critical:number = 101;
}

export class GraphBy{
  static patient: number = 100;
  static doctor: number = 101;
  static fee: number = 102;
  static fee_type: number = 103;
  static cashier: number = 104;
  static company: number = 105;
  static payment: number = 106;
}

export class patient_procedure_status{
  static ordered:number =100;
  static completed: number =101;
}

export class PurchaseRequestStatus {
  static submitted: number = 100;
 static accepted: number = 101;
 static rejected: number = 102;
 static purchased: number = 103;
 static dispatched: number = 103;
 static onprogress: number = 104;

 }

 export class PurchaseStatus {
  static purchased: number = 100;
 static sent: number = 101;
//  static received: number = 102;
//  static rejected: number = 103;
 static onprogress: number = 104;

 }

 export class ReceiveStatus {
  static sent: number = 100;
 static received: number = 101;
 static rejected: number = 102;

 }

 export class LostDamageStatus {
  static lost: number = 100;
 static damage: number = 101;
 static dispose: number = 102;
 }

 export class PrescriptionRequestStatus {
  static requested: number = 100;
 static accept: number = 101;
 static reject: number = 102;
 static void: number = 103;
 static cancel: number = 104;
 static dispatch: number = 105;

 }


 export class PrintType {
  static summary_sheet: number = 100;
 static medical_certificate: number = 101;
 static employee_medical_certificate: number = 102;
 static laboratory: number = 103;
 static pathology: number = 104;
 static radiology: number = 105;
 static prescription: number = 106;
 static refferal: number = 107;
 static death: number = 108;
 static driver_medical_certificate = 109;

 }

 export class Frequency {
 static immidiately: number = 100;
static once_a_day: number = 101;
static none:number=112;
static prn: number =123;
// static twice_a_day: number = 102;
// static thrice_a_day: number = 103;
// static four_times_a_day: number = 104;
static every_3_hours: number = 105;
static every_8_hours: number = 110;
static five_days_a_week: number = 120;
static on_alternate_days: number = 122;
static every_hour: number = 107;
static every_2_hours: number = 106;
static every_4_hours: number = 108;
static every_6_hours: number = 109;
static every_12_hours: number = 111;
static onece_a_week: number = 113;
static twice_a_week: number = 114;
static thrice_a_week: number = 115;
static every_2_weeks: number = 116;
static every_3_weeks: number = 117;
static five_times_a_day: number = 118;
static four_days_week: number = 119;
static six_days_a_week: number = 121;

}

export class HowToUse {
  static before_meals: number = 100;
 static empty_stomach: number = 101;
 static after_meals: number = 102;
 static in_the_morning: number = 103;
 static in_the_evening: number = 104;
 static at_bedtime: number = 105;
 static immediately: number = 106;
 static as_directed: number = 107;

 }

export class PhysiotherapyStatus {
 static waiting: number = 100;
static inprogress: number = 101;
static completed: number = 102;

}

export class AdmissionType {
  static emergency: number = 100;
  static elective: number = 101;
 }
export class MedicalEmploymentStatus {
  static fit: number = 100;
 static unfit: number = 101;

 }

 export class ModificationStatus {
  static modifiable: number = 100;
 static non_modifiable: number = 101;
 static medical_conditions_contribute_to_cad: number = 102;

 }

 export class InstructionType {
  static vital: number = 100;
//  static observation: number = 101;
 static medication: number = 102;
 static patient_procedure: number = 101;
 static physiotherapy: number = 103;
 static oxygentherapy: number = 104;
 static lab_order: number = 105;
 static pat_order: number = 106;
 static rad_order: number = 107;
 static treatment: number = 108;

 }

 export class InstructionSubType {
  static panel: number = 100;
  static test: number = 101;
 }

 export class InstructionStatus {
  static open: number = 100;
 static start: number = 101;
 static close: number = 102;

 }

 export class FeeServiceType {
  static all: number = 100;
  static outpatient: number = 101;
  static inpatient: number = 102;
  static emergency: number = 103;
 }

 export class CreditStatus {
  static covered: number = 100;
  static not_covered: number = 101;
 }

 export class DiscountPaymentType {
  static cash: number = 100;
 static credit: number = 101;

 }

 export class AdditionalPaymentType {
  static vat: number = 100;

 }

 export class CreditFamilyRelationship {
  static partner: number = 100;
 static child: number = 101;

 }

 export class CreditServiceFor {
  static all: number = 100;
  static employee: number = 101;
  static family: number = 102;
 }

 export class  FeeFrequency{
  static once: number = 100;
  static minute: number = 101;
  static hour: number = 102;
  static day: number = 103;
  static km: number = 104;
}

export class TimeOption{
  static Default: number = 100;
  static Min: number = 101;
  static Hr: number = 102;
  static Day: number = 103;
  static km: number = 104;
  static price: number = 105;
}
 export class dispatch_status {
  static accepted : number =100;
  static dispatched: number = 101;
  static cancelled: number = 102;
 }

export class item_units {
  static bottle : number = 100
  static sachet : number = 101
  static vial : number = 102
  static tab : number = 103
  static amp : number = 104
  static supp : number = 105
  static each  : number = 106
  static cap : number = 107
  static jar : number = 108
 }

export class sex {
  static all : number = 100;
  static male : number = 101;
  static female : number = 102;
}

export class store_service_type {
  static pharmacy :number = 100;
  static logistic: number = 101

}

export class return_from {
  static store :number = 100;
  static maintenance: number = 101
}

export class writtenBy {
  static doctor:number = 100;
  static nurse:number = 101;
}

export class category {
  static drug:number = 100;
  static supply: number = 101;
  static spare_part: number = 102;
  static consumable:number = 103;
}

export class plan_status {
  static done: number = 100;
  static undone:number = 101;
}
export class payment_report_group_by {
  static date :number = 100;
  static patient: number = 101;
  static doctor: number = 102;
  static cashier: number = 103;
  static company: number = 104;
  static fee: number = 105;
  static payment_type: number = 106;
  static payment_status: number = 107;
  static fee_type: number = 108;
  static ward: number = 109;

}

export class added_by {
  static doctor: number = 100;
  static nurse: number = 101;
}

export class vital_sign_with_age{
  static pulse_rate: number = 100;
  static respiratory_rate:number = 101;
  static systolic_bp: number = 102;
  static diastolic_bp: number = 103;
}

export class vital_sign {
  static temprature : number = 100;
  static oxygen_saturation: number = 101;
  static glascow_coma_scale: number = 102;
  static bmi: number = 103;
  static head_circumference: number = 104;
}


export class admission_reason {
  static physical_management: number = 100;
  static observation: number = 101;
  static surgical_management: number = 102;
  static labory_and_delivery_management: number = 103;
}

export class prescription_type {
  static doctor_prescription = 100;
  static nurse_prescription = 101;
  static pharmacist_prescription = 102;
  static opd_prescription = 103;
}

export class oxygen_type {
  static all: number = 100;
  static less_than_3: number = 101;
  static great_than_3: number = 102;
}

export class operation_room_type {
  static all: number = 100;
  static minor: number = 101;
  static major: number = 102;
}

export class progress_status {
  static start: number = 100;
  static end: number = 101;
}

export class procedure_for {
  static diagnostic: number = 100;
  static therapeutic: number = 101;
}

export class deposit_share_status {
  static shared: number = 100;
  static self: number = 101;
}

export class deposit_patient_type {
  static outside_patient: number = 100;
  static normal_patient: number = 101;
}

export class deposit_override_status {
  static allowed_override: number = 100;
  static forbiden_override: number = 101;
}

export class openness_status {
  static open: number = 100;
  static closed: number = 101;
}

export class credit_payment_history_type {
  static credit: number = 100;
  static debit: number = 101;
}
