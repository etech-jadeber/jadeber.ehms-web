import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FullCalendarModule } from '@fullcalendar/angular';
import {FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatCommonModule, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import {SlideMenuModule} from 'primeng/slidemenu';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import {PanelMenuModule} from 'primeng/panelmenu';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AppointmentDetailComponent } from './appointments/appointment-detail/appointment-detail.component';
import { AppointmentEditComponent } from './appointments/appointment-edit/appointment-edit.component';
import { AppointmentListComponent } from './appointments/appointment-list/appointment-list.component';
import { AppointmentPickComponent } from './appointments/appointment-pick/appointment-pick.component';
import { AvailabilityDetailComponent } from './availabilitys/availability-detail/availability-detail.component';
import { AvailabilityEditComponent } from './availabilitys/availability-edit/availability-edit.component';
import { AvailabilityListComponent } from './availabilitys/availability-list/availability-list.component';
import { AvailabilityPickComponent } from './availabilitys/availability-pick/availability-pick.component';


import { GeneralCaseComponent } from './cases/general-case/general-case.component';
import { PatientAppointmentCaseComponent } from './cases/patient-appointment-case/patient-appointment-case.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DepositDetailComponent } from './deposits/deposit-detail/deposit-detail.component';
import { DepositEditComponent } from './deposits/deposit-edit/deposit-edit.component';
import { Deposit_HistoryEditComponent } from './deposits/deposit-history-edit/deposit-history-edit.component';
import { DepositListComponent } from './deposits/deposit-list/deposit-list.component';
import { DepositPickComponent } from './deposits/deposit-pick/deposit-pick.component';
import { DoctorDetailComponent } from './doctors/doctor-detail/doctor-detail.component';
import { DoctorEditComponent } from './doctors/doctor-edit/doctor-edit.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { DoctorPickComponent } from './doctors/doctor-pick/doctor-pick.component';
import { DoctorSelfComponent } from './doctors/doctor-self/doctor-self.component';
import { LabSelfComponent } from './doctors/lab-self/lab-self.component';
import { NurseSelfComponent } from './doctors/nurse-self/nurse-self.component';
import { DoctorSurgery_AppointmentEditComponent } from './doctors/surgery-appointment-edit/surgery-appointment-edit.component';
import { DrugtypeDetailComponent } from './drugtypes/drugtype-detail/drugtype-detail.component';
import { DrugtypeEditComponent } from './drugtypes/drugtype-edit/drugtype-edit.component';
import { DrugtypeListComponent } from './drugtypes/drugtype-list/drugtype-list.component';
import { DrugtypePickComponent } from './drugtypes/drugtype-pick/drugtype-pick.component';
import { FeeDetailComponent } from './fees/fee-detail/fee-detail.component';
import { FeeEditComponent } from './fees/fee-edit/fee-edit.component';
import { FeeListComponent } from './fees/fee-list/fee-list.component';
import { FeePickComponent } from './fees/fee-pick/fee-pick.component';
import { Fee_TypeDetailComponent } from './feetypes/feetype-detail/feetype-detail.component';
import { Fee_TypeEditComponent } from './feetypes/feetype-edit/feetype-edit.component';
import { Fee_TypeListComponent } from './feetypes/feetype-list/feetype-list.component';
import { Fee_TypePickComponent } from './feetypes/feetype-pick/feetype-pick.component';
import { Form_EncounterDetailComponent } from './form_encounters/form-encounter-detail/form-encounter-detail.component';
import { Form_EncounterEditComponent } from './form_encounters/form-encounter-edit/form-encounter-edit.component';
import { Form_EncounterListComponent } from './form_encounters/form-encounter-list/form-encounter-list.component';
import { Form_EncounterPickComponent } from './form_encounters/form-encounter-pick/form-encounter-pick.component';
import { Form_ObservationEditComponent } from './form_encounters/form-observation-edit/form-observation-edit.component';
import { Form_SoapEditComponent } from './form_encounters/form-soap-edit/form-soap-edit.component';
import { Form_VitalsEditComponent } from './form_encounters/form-vitals-edit/form-vitals-edit.component';
import { PrescriptionsComponent, PrescriptionsEditComponent, PrescriptionsEditListComponent } from "./form_encounters/prescriptions-edit/prescriptions-edit.component";
import { Procedure_OrderDetailComponent } from "./form_encounters/procedure_orders/procedure-order-detail/procedure-order-detail.component";
import { Procedure_OrderEditComponent } from './form_encounters/procedure_orders/procedure-order-edit/procedure-order-edit.component';
import { Procedure_OrderListComponent } from "./form_encounters/procedure_orders/procedure-order-list/procedure-order-list.component";
import { Procedure_OrderPickComponent } from './form_encounters/procedure_orders/procedure-order-pick/procedure-order-pick.component';
import { ProcedureOrderCodeEditComponent } from './form_encounters/procedure_order_codes/procedure-order-code-edit/procedure-order-code-edit.component';
import { Form_Encounter_Surgery_AppointmentEditComponent } from './form_encounters/surgery-appointment-edit/surgery-appointment-edit.component';
import { ItemDetailComponent } from './items/item-detail/item-detail.component';
import { ItemEditComponent } from './items/item-edit/item-edit.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import { ItemPickComponent } from './items/item-pick/item-pick.component';
import { Item_Ledger_EntryDetailComponent } from './item_ledger_entrys/item-ledger-entry-detail/item-ledger-entry-detail.component';
import { Item_Ledger_EntryEditComponent } from './item_ledger_entrys/item-ledger-entry-edit/item-ledger-entry-edit.component';
import { Item_Ledger_EntryListComponent } from './item_ledger_entrys/item-ledger-entry-list/item-ledger-entry-list.component';
import { Item_Ledger_EntryPickComponent } from "./item_ledger_entrys/item-ledger-entry-pick/item-ledger-entry-pick.component";
import { LoginComponent } from './login/login.component';
import { LostDamageDetailComponent } from './lostDamages/lost-damage-detail/lost-damage-detail.component';
import { LostDamageEditComponent } from './lostDamages/lost-damage-edit/lost-damage-edit.component';
import { LostDamageListComponent } from './lostDamages/lost-damage-list/lost-damage-list.component';
import { LostDamagePickComponent } from './lostDamages/lost-damage-pick/lost-damage-pick.component';
import { Openemr_UserPickComponent } from './openemr_users/openemr-user-pick/openemr-user-pick.component';
import { AboutComponent } from './pages/about/about.component';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { InvalidOperationComponent } from './pages/invalid-operation/invalid-operation.component';
import { MandatoryComponent } from './pages/mandatory/mandatory.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { TosComponent } from './pages/tos/tos.component';
import { WebcamModule } from 'ngx-webcam';
import { History_DataEditComponent } from './patients/history-data-edit/history-data-edit.component';
import { PatientAppointmentEditComponent } from './patients/patient-appointment-edit/patient-appointment-edit.component';
import { PatientDetailComponent } from './patients/patient-detail/patient-detail.component';
import { PatientEditComponent } from './patients/patient-edit/patient-edit.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { PatientPickComponent } from './patients/patient-pick/patient-pick.component';
import { PatientsHomeComponent } from './patients/patients-home/patients-home.component';
import { PaymentDetailComponent } from './payments/payment-detail/payment-detail.component';
import { PaymentEditComponent } from './payments/payment-edit/payment-edit.component';
import { PaymentListComponent } from './payments/payment-list/payment-list.component';
import { PaymentPickComponent } from './payments/payment-pick/payment-pick.component';
import { PasswordChangeComponent } from './profile/password-change/password-change.component';
import { ProfileComponent } from './profile/profile.component';
import { PurchaseDetailComponent } from './purchases/purchase-detail/purchase-detail.component';
import { PurchaseEditComponent } from './purchases/purchase-edit/purchase-edit.component';
import { PurchaseListComponent } from './purchases/purchase-list/purchase-list.component';
import { PurchasePickComponent } from './purchases/purchase-pick/purchase-pick.component';
import { ReceptionDetailComponent } from './receptions/reception-detail/reception-detail.component';
import { ReceptionEditComponent } from './receptions/reception-edit/reception-edit.component';
import { ReceptionListComponent } from './receptions/reception-list/reception-list.component';
import { ReceptionPickComponent } from './receptions/reception-pick/reception-pick.component';
import { ReceptionSelfComponent } from './receptions/reception-self/reception-self.component';
import { ReportDetailComponent } from './reports/report-detail/report-detail.component';
import { ReportEditComponent } from './reports/report-edit/report-edit.component';
import { ReportListComponent } from './reports/report-list/report-list.component';
import { ReportPickComponent } from './reports/report-pick/report-pick.component';
import { RequestDetailComponent } from './requests/request-detail/request-detail.component';
import { RequestEditComponent } from './requests/request-edit/request-edit.component';
import { RequestListComponent } from './requests/request-list/request-list.component';
import { RequestPickComponent } from './requests/request-pick/request-pick.component';
import { EmployeeDetailComponent } from './storeemployees/employee-detail/employee-detail.component';
import { EmployeeEditComponent } from './storeemployees/employee-edit/employee-edit.component';
import { EmployeeHomeLedgerComponent } from './storeemployees/employee-home-ledger/employee-home-ledger.component';
import { EmployeeHomeRequestEditComponent } from './storeemployees/employee-home-request-edit/employee-home-request-edit.component';
import { EmployeeHomeRequestListComponent } from './storeemployees/employee-home-request-list/employee-home-request-list.component';
import { EmployeeHomeResponseApproveComponent } from './storeemployees/employee-home-response-approve/employee-home-response-approve.component';
import { EmployeeHomeTransferComponent } from './storeemployees/employee-home-transfer/employee-home-transfer.component';
import { EmployeeHomeComponent } from './storeemployees/employee-home/employee-home.component';
import { EmployeeListComponent } from './storeemployees/employee-list/employee-list.component';
import { EmployeePickComponent } from './storeemployees/employee-pick/employee-pick.component';
import { StoreDetailComponent } from './stores/store-detail/store-detail.component';
import { StoreEditComponent } from './stores/store-edit/store-edit.component';
import { StoreListComponent } from './stores/store-list/store-list.component';
import { StorePickComponent } from './stores/store-pick/store-pick.component';
import { SupplierDetailComponent } from './suppliers/supplier-detail/supplier-detail.component';
import { SupplierEditComponent } from './suppliers/supplier-edit/supplier-edit.component';
import { SupplierListComponent } from './suppliers/supplier-list/supplier-list.component';
import { SupplierPickComponent } from './suppliers/supplier-pick/supplier-pick.component';
import { Surgery_AppointmentDetailComponent } from './surgery_appointments/surgery-appointment-detail/surgery-appointment-detail.component';
import { Surgery_AppointmentEditComponent } from './surgery_appointments/surgery-appointment-edit/surgery-appointment-edit.component';
import { Surgery_AppointmentListComponent } from './surgery_appointments/surgery-appointment-list/surgery-appointment-list.component';
import { Surgery_AppointmentPickComponent } from './surgery_appointments/surgery-appointment-pick/surgery-appointment-pick.component';
import { Surgery_TypeDetailComponent } from './surgery_types/surgery-type-detail/surgery-type-detail.component';
import { Surgery_TypeEditComponent } from './surgery_types/surgery-type-edit/surgery-type-edit.component';
import { Surgery_TypeListComponent } from './surgery_types/surgery-type-list/surgery-type-list.component';
import { Surgery_TypePickComponent } from './surgery_types/surgery-type-pick/surgery-type-pick.component';
import { AsyncJobsComponent } from './tc/async-jobs/async-jobs.component';
import { CaseDetailComponent } from './tc/cases/case-detail/case-detail.component';
import { CaseEditComponent } from './tc/cases/case-edit/case-edit.component';
import { CaseListComponent } from './tc/cases/case-list/case-list.component';
import { CasePickComponent } from './tc/cases/case-pick/case-pick.component';
import { ConfirmComponent } from './tc/confirm/confirm.component';
import { DateinputComponent } from './tc/dateinput/dateinput.component';
import { EventlogDetailComponent } from './tc/eventlogs/eventlog-detail/eventlog-detail.component';
import { EventlogEditComponent } from './tc/eventlogs/eventlog-edit/eventlog-edit.component';
import { EventlogListComponent } from './tc/eventlogs/eventlog-list/eventlog-list.component';
import { EventlogPickComponent } from './tc/eventlogs/eventlog-pick/eventlog-pick.component';
import { FileDetailComponent } from './tc/files/file-detail/file-detail.component';
import { FileEditComponent } from './tc/files/file-edit/file-edit.component';
import { FileListComponent } from './tc/files/file-list/file-list.component';
import { FilePickComponent } from './tc/files/file-pick/file-pick.component';
import { GroupDetailComponent } from './tc/groups/group-detail/group-detail.component';
import { GroupEditComponent } from './tc/groups/group-edit/group-edit.component';
import { GroupListComponent } from './tc/groups/group-list/group-list.component';
import { GroupPickComponent } from './tc/groups/group-pick/group-pick.component';
import { httpInterceptorProviders } from './tc/http';
import { JobDetailComponent } from './tc/jobs/job-detail/job-detail.component';
import { JobEditComponent } from './tc/jobs/job-edit/job-edit.component';
import { JobListComponent } from './tc/jobs/job-list/job-list.component';
import { JobPickComponent } from './tc/jobs/job-pick/job-pick.component';
import { LockDetailComponent } from './tc/locks/lock-detail/lock-detail.component';
import { LockEditComponent } from './tc/locks/lock-edit/lock-edit.component';
import { LockListComponent } from './tc/locks/lock-list/lock-list.component';
import { LockPickComponent } from './tc/locks/lock-pick/lock-pick.component';
import { MailDetailComponent } from './tc/mails/mail-detail/mail-detail.component';
import { MailEditComponent } from './tc/mails/mail-edit/mail-edit.component';
import { MailListComponent } from './tc/mails/mail-list/mail-list.component';
import { MailPickComponent } from './tc/mails/mail-pick/mail-pick.component';
import { MenuChildComponent } from './tc/menu/menu-child/menu-child.component';
import { MenuParentComponent } from './tc/menu/menu-parent/menu-parent.component';
import { ResourceDetailComponent } from './tc/resources/resource-detail/resource-detail.component';
import { ResourceEditComponent } from './tc/resources/resource-edit/resource-edit.component';
import { ResourceListComponent } from './tc/resources/resource-list/resource-list.component';
import { ResourcePickComponent } from './tc/resources/resource-pick/resource-pick.component';
import { TcaclEditComponent } from './tc/resources/tcacl-edit/tcacl-edit.component';
import { SettingDetailComponent } from './tc/settings/setting-detail/setting-detail.component';
import { SettingEditComponent } from './tc/settings/setting-edit/setting-edit.component';
import { SettingListComponent } from './tc/settings/setting-list/setting-list.component';
import { SettingPickComponent } from './tc/settings/setting-pick/setting-pick.component';
import { StatDetailComponent } from './tc/stats/stat-detail/stat-detail.component';
import { StatEditComponent } from './tc/stats/stat-edit/stat-edit.component';
import { StatListComponent } from './tc/stats/stat-list/stat-list.component';
import { StatPickComponent } from './tc/stats/stat-pick/stat-pick.component';
import { TextinputComponent } from './tc/textinput/textinput.component';
import { TgidentityDetailComponent } from './tc/tgidentitys/tgidentity-detail/tgidentity-detail.component';
import { TgidentityEditComponent } from './tc/tgidentitys/tgidentity-edit/tgidentity-edit.component';
import { TgidentityListComponent } from './tc/tgidentitys/tgidentity-list/tgidentity-list.component';
import { TgidentityPickComponent } from './tc/tgidentitys/tgidentity-pick/tgidentity-pick.component';
import { UserDetailComponent } from './tc/users/user-detail/user-detail.component';
import { UserEditComponent } from './tc/users/user-edit/user-edit.component';
import { UserListComponent } from './tc/users/user-list/user-list.component';
import { UserPickComponent } from './tc/users/user-pick/user-pick.component';
import { TC_DATE_FORMATS } from './tc/utils-date';
import { ValidationMessagesComponent } from './tc/validation-messages/validation-messages.component';
import { TransferDetailComponent } from './transfers/transfer-detail/transfer-detail.component';
import { TransferEditComponent } from './transfers/transfer-edit/transfer-edit.component';
import { TransferListComponent } from './transfers/transfer-list/transfer-list.component';
import { TransferPickComponent } from './transfers/transfer-pick/transfer-pick.component';
import { VitalDetailComponent } from './vitals/vital-detail/vital-detail.component';
import { VitalEditComponent } from './vitals/vital-edit/vital-edit.component';
import { VitalListComponent } from './vitals/vital-list/vital-list.component';
import { VitalPickComponent } from './vitals/vital-pick/vital-pick.component';
import { Item_In_StoreListComponent } from "./item_in_stores/item-in-store-list/item-in-store-list.component";
import { Item_In_StoreEditComponent } from "./item_in_stores/item-in-store-edit/item-in-store-edit.component";
import { Item_In_StorePickComponent } from "./item_in_stores/item-in-store-pick/item-in-store-pick.component";
import { Item_In_StoreDetailComponent } from "./item_in_stores/item-in-store-detail/item-in-store-detail.component";

import { WaitinglistDetailComponent } from './bed/waitinglists/waitinglist-detail/waitinglist-detail.component';
import { WaitinglistEditComponent } from './bed/waitinglists/waitinglist-edit/waitinglist-edit.component';
import { WaitinglistListComponent } from './bed/waitinglists/waitinglist-list/waitinglist-list.component';
import { WaitinglistPickComponent } from './bed/waitinglists/waitinglist-pick/waitinglist-pick.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { Procedure_ProviderEditComponent } from "./procedure_providers/procedure-provider-edit/procedure-provider-edit.component";
import { Procedure_ProviderPickComponent } from "./procedure_providers/procedure-provider-pick/procedure-provider-pick.component";
import { Procedure_ProviderListComponent } from "./procedure_providers/procedure-provider-list/procedure-provider-list.component";
import { Procedure_ProviderDetailComponent } from "./procedure_providers/procedure-provider-detail/procedure-provider-detail.component";
import { PendingReviewComponent } from './procedure_providers/pending-review/pending-review.component';
import { PendingReviewResultComponent } from './procedure_providers/pending-review-result/pending-review-result.component';
import { ProcedureOrderOverviewComponent } from './form_encounters/procedure_orders/procedure-order-overview/procedure-order-overview.component';
import { ProcedureOrderResultsComponent } from './form_encounters/procedure_orders/procedure-order-results/procedure-order-results.component';
import { ProcedureOrderResultComponent } from './form_encounters/procedure_orders/procedure-order-result/procedure-order-result.component';
import { PatientDispatchComponent } from './form_encounters/admission_forms/patient-dispatch/patient-dispatch.component';
import { MenuState } from './global-state-manager/global-state';
import { FormDrugPickComponent } from './form_encounters/form-drug-pick/form-drug-pick.component';
import { Doctor_Private_InfoEditComponent } from './patients/doctor-private-info-edit/doctor-private-info-edit.component';
import { Medical_CertificateEditComponent } from './form_encounters/medical-certificate-edit/medical-certificate-edit.component';
import { Encounter_ImagesEditComponent } from './form_encounters/encounter-images-edit/encounter-images-edit.component';
import { MessagesListComponent } from './messagess/messages-list/messages-list.component';
import { MessagesDetailComponent } from './messagess/messages-detail/messages-detail.component';
import { MessagesEditComponent } from './messagess/messages-edit/messages-edit.component';
import { MessagesPickComponent } from './messagess/messages-pick/messages-pick.component';

import { Case_TransferEditComponent } from './form_encounters/case-transfer/case-transfer-edit/case-transfer-edit.component';
import { Case_TransferDetailComponent } from './form_encounters/case-transfer/case-transfer-detail/case-transfer-detail.component';
import { Case_TransferPickComponent } from './form_encounters/case-transfer/case-transfer-pick/case-transfer-pick.component';
import { Case_TransferListComponent } from './form_encounters/case-transfer/case-transfer-list/case-transfer-list.component';
import { DiagnosisCodePickComponent } from './form_encounters/procedure_order_codes/diagnosis-code-pick/diagnosis-code-pick.component';
import { CompanyListComponent } from './Companys/company-list/company-list.component';
import { CompanyDetailComponent } from './Companys/company-detail/company-detail.component';
import { CompanyEditComponent } from './Companys/company-edit/company-edit.component';
import { CompanyPickComponent } from './Companys/company-pick/company-pick.component';
import { InsuredEmployeeListComponent } from './insured_employees/insured-employee-list/insured-employee-list.component';
import { InsuredEmployeeDetailComponent } from './insured_employees/insured-employee-detail/insured-employee-detail.component';
import { InsuredEmployeeEditComponent } from './insured_employees/insured-employee-edit/insured-employee-edit.component';
import { InsuredEmployeePickComponent } from './insured_employees/insured-employee-pick/insured-employee-pick.component';
import { PaymentDueListComponent } from './payment_dues/payment-due-list/payment-due-list.component';
import { PaymentDueDetailComponent } from './payment_dues/payment-due-detail/payment-due-detail.component';
import { PaymentDueEditComponent } from './payment_dues/payment-due-edit/payment-due-edit.component';
import { PaymentDuePickComponent } from './payment_dues/payment-due-pick/payment-due-pick.component';
import { MedicalReportChartsComponent } from './medical-report-charts/medical-report-charts.component';

import { ProcedureTestsDetailComponent } from './procedure_testss/procedure-tests-detail/procedure-tests-detail.component';
import { Procedure_TestsEditComponent } from './procedure_testss/procedure-tests-edit/procedure-tests-edit.component';

import { Procedure_TestsListComponent } from './procedure_testss/procedure-tests-list/procedure-tests-list.component';

import { ProcedurePaymentDueDetailComponent } from './procedure_payment_dues/procedure-payment-due-detail/procedure-payment-due-detail.component';

import { ProcedurePaymentDuePickComponent } from './procedure_payment_dues/procedure-payment-due-pick/procedure-payment-due-pick.component';
import { Procedure_Payment_DueListComponent } from './procedure_payment_dues/procedure-payment-due-list/procedure-payment-due-list.component';
import { Procedure_Payment_DueEditComponent } from './procedure_payment_dues/procedure-payment-due-edit/procedure-payment-due-edit.component';
import { Procedure_TestsPickComponent } from './procedure_testss/procedure-tests-pick/procedure-tests-pick.component';
import {
  Review_Of_System_OptionsListComponent
} from './review_of_system_optionss/review-of-system-options-list/review-of-system-options-list.component';
import {
  Review_Of_System_OptionsDetailComponent
} from './review_of_system_optionss/review-of-system-options-detail/review-of-system-options-detail.component';
import {
  Review_Of_System_OptionsEditComponent
} from './review_of_system_optionss/review-of-system-options-edit/review-of-system-options-edit.component';
import {
  Review_Of_System_OptionsPickComponent
} from './review_of_system_optionss/review-of-system-options-pick/review-of-system-options-pick.component';
import {Review_Of_SystemEditComponent, Review_Of_SystemEditListComponent} from './form_encounters/review-of-system-edit/review-of-system-edit.component';

import {
  InsuranceCompanyDetailComponent
} from './insurance_companys/insurance-company-detail/insurance-company-detail.component';
import {
  InsuranceCompanyListComponent
} from './insurance_companys/insurance-company-list/insurance-company-list.component';

import {
  Physical_ExaminationEditComponent, Physical_ExaminationEditListComponent
} from './form_encounters/physical-examination-edit/physical-examination-edit.component';
import {
  InsuranceCompanyEditComponent
} from './insurance_companys/insurance-company-edit/insurance-company-edit.component';
import {
  InsuranceCompanyPickComponent
} from './insurance_companys/insurance-company-pick/insurance-company-pick.component';
import {HistoryEditComponent, HistoryEditListComponent} from './historys/history-edit/history-edit.component';
import {Lab_PanelEditComponent} from './lab_panels/lab-panel-edit/lab-panel-edit.component';
import {Lab_PanelPickComponent} from './lab_panels/lab-panel-pick/lab-panel-pick.component';
import {Lab_PanelDetailComponent} from './lab_panels/lab-panel-detail/lab-panel-detail.component';
import {Lab_PanelListComponent} from './lab_panels/lab-panel-list/lab-panel-list.component';
import {Lab_TestListComponent} from './lab_tests/lab-test-list/lab-test-list.component';
import {Lab_TestDetailComponent} from './lab_tests/lab-test-detail/lab-test-detail.component';
import {Lab_TestEditComponent} from './lab_tests/lab-test-edit/lab-test-edit.component';
import {Lab_TestPickComponent} from './lab_tests/lab-test-pick/lab-test-pick.component';
import {Lab_OrderEditComponent} from './lab_orders/lab-order-edit/lab-order-edit.component';
import {Lab_OrderPickComponent} from './lab_orders/lab-order-pick/lab-order-pick.component';
import {Lab_OrderListComponent} from './lab_orders/lab-order-list/lab-order-list.component';
import {Lab_OrderDetailComponent} from './lab_orders/lab-order-detail/lab-order-detail.component';
import {DiagnosisListComponent} from './form_encounters/diagnosiss/diagnosis-list/diagnosis-list.component';
import {DiagnosisDetailComponent} from './form_encounters/diagnosiss/diagnosis-detail/diagnosis-detail.component';
import {DiagnosisEditComponent, DiagnosisEDitListComponent} from './form_encounters/diagnosiss/diagnosis-edit/diagnosis-edit.component';
import {DiagnosisPickComponent} from './form_encounters/diagnosiss/diagnosis-pick/diagnosis-pick.component';

import {OrdersEditComponent} from './form_encounters/orderss/orders-edit/orders-edit.component';
import {OrdersPickComponent} from './form_encounters/orderss/orders-pick/orders-pick.component';


import {Lab_ResultListComponent} from './form_encounters/orderss/lab_results/lab-result-list/lab-result-list.component';
import {Lab_ResultEditComponent} from './form_encounters/orderss/lab_results/lab-result-edit/lab-result-edit.component';
import {
  Lab_ResultDetailComponent
} from './form_encounters/orderss/lab_results/lab-result-detail/lab-result-detail.component';
import {Lab_ResultPickComponent} from './form_encounters/orderss/lab_results/lab-result-pick/lab-result-pick.component';
import {OrdersListComponent} from './form_encounters/orderss/orders-list/orders-list.component';
import {OrdersDetailComponent} from './form_encounters/orderss/orders-detail/orders-detail.component';
import {
  Physiotherapy_TypeListComponent
} from './physiotherapy_types/physiotherapy-type-list/physiotherapy-type-list.component';
import {
  Physiotherapy_TypeDetailComponent
} from './physiotherapy_types/physiotherapy-type-detail/physiotherapy-type-detail.component';
import {
  Physiotherapy_TypePickComponent
} from './physiotherapy_types/physiotherapy-type-pick/physiotherapy-type-pick.component';
import {
  Physiotherapy_TypeEditComponent
} from './physiotherapy_types/physiotherapy-type-edit/physiotherapy-type-edit.component';
import {Test_PanelEditComponent} from './lab_panels/test-panel-edit/test-panel-edit.component';
import { RadListComponent } from './orderss/rad-list/rad-list.component';
import { LabListComponent } from './orderss/lab-list/lab-list.component';
import { LabDetailComponent } from './orderss/lab-detail/lab-detail.component';
import { RadDetailComponent } from './orderss/rad-detail/rad-detail.component';
import { LabEditComponent } from './form_encounters/lab-edit/lab-edit.component';
import {AvatarModule} from 'primeng/avatar';
import {
  Rehabilitation_OrderEditComponent
} from './form_encounters/rehabilitation-order-edit/rehabilitation-order-edit.component';
import {DepartmentListComponent} from './departments/department-list/department-list.component';
import {DepartmentDetailComponent} from './departments/department-detail/department-detail.component';
import {DepartmentEditComponent} from './departments/department-edit/department-edit.component';
import {DepartmentPickComponent} from './departments/department-pick/department-pick.component';
import {
  Department_SpecialtyListComponent
} from "./department_specialtys/department-specialty-list/department-specialty-list.component";
import {
  Department_SpecialtyDetailComponent
} from "./department_specialtys/department-specialty-detail/department-specialty-detail.component";
import {
  Department_SpecialtyEditComponent
} from "./department_specialtys/department-specialty-edit/department-specialty-edit.component";
import {
  Department_SpecialtyPickComponent
} from "./department_specialtys/department-specialty-pick/department-specialty-pick.component";
import { Consent_FormListComponent } from './consent_forms/consent-form-list/consent-form-list.component';
import { Consent_FormDetailComponent } from './consent_forms/consent-form-detail/consent-form-detail.component';
import { Consent_FormEditComponent } from './consent_forms/consent-form-edit/consent-form-edit.component';
import { Consent_FormPickComponent } from './consent_forms/consent-form-pick/consent-form-pick.component';

import { BirthListComponent } from './births/birth-list/birth-list.component';
import { BirthDetailComponent } from './births/birth-detail/birth-detail.component';
import { BirthEditComponent } from './births/birth-edit/birth-edit.component';
import { BirthPickComponent } from './births/birth-pick/birth-pick.component';
import { VaccinationListComponent } from './vaccinations/vaccination-list/vaccination-list.component';
import { VaccinationDetailComponent } from './vaccinations/vaccination-detail/vaccination-detail.component';
import { VaccinationEditComponent } from './vaccinations/vaccination-edit/vaccination-edit.component';
import { VaccinationPickComponent } from './vaccinations/vaccination-pick/vaccination-pick.component';
import { VaccinatedListComponent } from './vaccinateds/vaccinated-list/vaccinated-list.component';
import { VaccinatedDetailComponent } from './vaccinateds/vaccinated-detail/vaccinated-detail.component';
import { VaccinatedEditComponent } from './vaccinateds/vaccinated-edit/vaccinated-edit.component';
import { VaccinatedPickComponent } from './vaccinateds/vaccinated-pick/vaccinated-pick.component';
import { AvailabilityCalenderViewComponent } from './availabilitys/availability-calender-view/availability-calender-view.component';
import { DeathListComponent } from './deaths/death-list/death-list.component';
import { DeathDetailComponent } from './deaths/death-detail/death-detail.component';
import { DeathEditComponent } from './deaths/death-edit/death-edit.component';
import { DeathPickComponent } from './deaths/death-pick/death-pick.component';
import { ProcedureTypeListComponent } from './procedure_types/procedure-type-list/procedure-type-list.component';
import { ProcedureTypeDetailComponent } from './procedure_types/procedure-type-detail/procedure-type-detail.component';
import { ProcedureTypeEditComponent } from './procedure_types/procedure-type-edit/procedure-type-edit.component';
import { ProcedureTypePickComponent } from './procedure_types/procedure-type-pick/procedure-type-pick.component';
import { ProcedureListComponent } from './procedures/procedure-list/procedure-list.component';
import { ProcedureDetailComponent } from './procedures/procedure-detail/procedure-detail.component';
import { ProcedureEditComponent } from './procedures/procedure-edit/procedure-edit.component';
import { ProcedurePickComponent } from './procedures/procedure-pick/procedure-pick.component';
import { AdmissionOrderComponent } from './form_encounters/admission_forms/admission-order/admission-order.component';
import { PatientProcedureEditComponent } from './form_encounters/patient-procedure-edit/patient-procedure-edit.component';
import { Cancelled_AppointmentListComponent } from './cancelled_appointments/cancelled-appointment-list/cancelled-appointment-list.component';
import { Cancelled_AppointmentDetailComponent } from './cancelled_appointments/cancelled-appointment-detail/cancelled-appointment-detail.component';
import { Cancelled_AppointmentEditComponent } from './cancelled_appointments/cancelled-appointment-edit/cancelled-appointment-edit.component';
import { Cancelled_AppointmentPickComponent } from './cancelled_appointments/cancelled-appointment-pick/cancelled-appointment-pick.component';
import { PatientProcedureNotesComponent } from './form_encounters/patient-procedure-notes/patient-procedure-notes.component';
import { Outside_OrdersListComponent } from './outside_orderss/outside-orders-list/outside-orders-list.component';
import { Outside_OrdersDetailComponent } from './outside_orderss/outside-orders-detail/outside-orders-detail.component';
import { Outside_OrdersPickComponent } from './outside_orderss/outside-orders-pick/outside-orders-pick.component';
import { Outside_OrdersEditComponent } from './outside_orderss/outside-orders-edit/outside-orders-edit.component';
import { LabOrderComponent } from './outside_orderss/lab-order/lab-order.component';
import { ConsultingEditComponent } from './form_encounters/consulting-edit/consulting-edit.component';
import { MedicalCertificateEmployeeListComponent } from './medical_certificate_employee/medical-certificate-employee-list/medical-certificate-employee-list.component';
import { MedicalCertificateEmployeeDetailComponent } from './medical_certificate_employee/medical-certificate-employee-detail/medical-certificate-employee-detail.component';
import { MedicalCertificateEmployeeEditComponent } from './medical_certificate_employee/medical-certificate-employee-edit/medical-certificate-employee-edit.component';
import { MedicalCertificateEmployeePickComponent } from './medical_certificate_employee/medical-certificate-employee-pick/medical-certificate-employee-pick.component';
import { BiopsyRequestListComponent } from './biopsy_request/biopsy-request-list/biopsy-request-list.component';
import { BiopsyRequestDetailComponent } from './biopsy_request/biopsy-request-detail/biopsy-request-detail.component';
import { BiopsyRequestEditComponent } from './biopsy_request/biopsy-request-edit/biopsy-request-edit.component';
import { BiopsyRequestPickComponent } from './biopsy_request/biopsy-request-pick/biopsy-request-pick.component';


import { MedicationAdminstrationChartListComponent } from './medication_adminstration_chart/medication-adminstration-chart-list/medication-adminstration-chart-list.component';
import { MedicationAdminstrationChartDetailComponent } from './medication_adminstration_chart/medication-adminstration-chart-detail/medication-adminstration-chart-detail.component';
import { MedicationAdminstrationChartEditComponent } from './medication_adminstration_chart/medication-adminstration-chart-edit/medication-adminstration-chart-edit.component';
import { MedicationAdminstrationChartPickComponent } from './medication_adminstration_chart/medication-adminstration-chart-pick/medication-adminstration-chart-pick.component';
import { OperationNoteListComponent } from './operation_note/operation-note-list/operation-note-list.component';
import { OperationNoteDetailComponent } from './operation_note/operation-note-detail/operation-note-detail.component';
import { OperationNoteEditComponent } from './operation_note/operation-note-edit/operation-note-edit.component';
import { OperationNotePickComponent } from './operation_note/operation-note-pick/operation-note-pick.component';
import { DriverMedicalCertificateListComponent } from './driver_medical_certificate/driver-medical-certificate-list/driver-medical-certificate-list.component';
import { DriverMedicalCertificateDetailComponent } from './driver_medical_certificate/driver-medical-certificate-detail/driver-medical-certificate-detail.component';
import { DriverMedicalCertificateEditComponent } from './driver_medical_certificate/driver-medical-certificate-edit/driver-medical-certificate-edit.component';
import { DriverMedicalCertificatePickComponent } from './driver_medical_certificate/driver-medical-certificate-pick/driver-medical-certificate-pick.component';
import { PharmacyDashboardComponent } from './pharmacy-dashboard/pharmacy-dashboard.component';
import { DeliverySummaryListComponent } from './delivery_summary/delivery-summary-list/delivery-summary-list.component';
import { DeliverySummaryDetailComponent } from './delivery_summary/delivery-summary-detail/delivery-summary-detail.component';
import { DeliverySummaryEditComponent } from './delivery_summary/delivery-summary-edit/delivery-summary-edit.component';
import { DeliverySummaryPickComponent } from './delivery_summary/delivery-summary-pick/delivery-summary-pick.component';
import { CreditHistoryListComponent } from './Companys/credit-history-list/credit-history-list.component';
import { AllergyListComponent } from './allergy/allergy-list/allergy-list.component';
import { AllergyDetailComponent } from './allergy/allergy-detail/allergy-detail.component';
import { AllergyEditComponent } from './allergy/allergy-edit/allergy-edit.component';
import { AllergyPickComponent } from './allergy/allergy-pick/allergy-pick.component';
import { AllergyFormEditComponent } from './form_encounters/allergy-form-edit/allergy-form-edit.component';

import { NurseRecordListComponent } from './nurse_record/nurse-record-list/nurse-record-list.component';
import { NurseRecordDetailComponent } from './nurse_record/nurse-record-detail/nurse-record-detail.component';
import { PatientTreatmentListComponent } from './patient_treatments/patient-treatment-list/patient-treatment-list.component';
import { PatientTreatmentDetailComponent } from './patient_treatments/patient-treatment-detail/patient-treatment-detail.component';
import { Patient_TreatmentEditComponent } from './patient_treatments/patient_treatment-edit/patient-treatment-edit.component';
import { Patient_TreatmentPickComponent } from './patient_treatments/patient_treatment-pick/patient_treatment-pick.component';
import { PatientRecordListComponent } from './patient_records/patient-record-list/patient-record-list.component';
import { PatientRecordDetailComponent } from './patient_records/patient-record-detail/patient-record-detail.component';
import { Patient_RecordEditComponent } from './patient_records/patient-record-edit/patient-record-edit.component';
import { Patient_RecordPickComponent } from './patient_records/patient-record-pick/patient-record-pick.component';
import { CalendarModule } from 'angular-calendar';
//
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FormVitalsListComponent } from './form-vitals-list/form-vitals-list.component';
import { AleryFormListComponent } from './alery-form-list/alery-form-list.component';
import { FormSoapsListComponent } from './form-soaps-list/form-soaps-list.component';
import { HistoryListComponent } from './history-list/history-list.component';
import { PrescriptionListComponent } from './prescription-list/prescription-list.component';
import { FormClinicalInstructionListComponent } from './form_clinical_instruction/form-clinical-instruction-list/form-clinical-instruction-list.component';
import { HistoryDataLisComponent } from './history-data-lis/history-data-lis.component';
import { NotesListComponent } from './notes-list/notes-list.component';
import { FormObservationsListComponent } from './form-observations-list/form-observations-list.component';
import { SurgeryAppointmentListComponent } from './surgery-appointment-list/surgery-appointment-list.component';
import { ReviewOfSystemListComponent } from './review-of-system-list/review-of-system-list.component';
import { ConsultingListComponent } from './consulting-list/consulting-list.component';
import { PatientProcedureListComponent } from './patient-procedure-list/patient-procedure-list.component';
import { DoctorPrivateInfoListComponent } from './doctor-private-info-list/doctor-private-info-list.component';
import { MedicalCertificateListComponent } from './medical-certificate-list/medical-certificate-list.component';
import { EncounterImagesListComponent } from './encounter-images-list/encounter-images-list.component';
import { FollowupNoteListComponent } from './followup-note-list/followup-note-list.component';
import { RehabilitationOrderListComponent } from './rehabilitation-order-list/rehabilitation-order-list.component';
import { PhysicalExaminationListComponent } from './physical-examination-list/physical-examination-list.component';
import { FormEncountersListComponent } from './form-encounters-list/form-encounters-list.component';
import { LabOrdersListComponent } from './lab-orders-list/lab-orders-list.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { OverviewComponent } from './form_encounters/overview/overview.component';
import { FormVitalsComponent } from './form_encounters/form-vitals/form-vitals.component';
import { OtherEncounterComponent } from './form_encounters/other-encounter/other-encounter.component';
import { RadiologyDataListComponent } from './radiology_data/radiology-data-list/radiology-data-list.component';
import { RadiologyDataDetailComponent } from './radiology_data/radiology-data-detail/radiology-data-detail.component';
import { RadiologyDataEditComponent } from './radiology_data/radiology-data-edit/radiology-data-edit.component';
import { RadiologyDataPickComponent } from './radiology_data/radiology-data-pick/radiology-data-pick.component';
import { EquipmentDataListComponent } from './equipment_data/equipment-data-list/equipment-data-list.component';
import { EquipmentDataDetailComponent } from './equipment_data/equipment-data-detail/equipment-data-detail.component';
import { EquipmentDataEditComponent } from './equipment_data/equipment-data-edit/equipment-data-edit.component';
import { EquipmentDataPickComponent } from './equipment_data/equipment-data-pick/equipment-data-pick.component';
import { ResultDescriptionEditComponent } from './orderss/result-description-edit/result-description-edit.component';
import { ANCInitialEvaluationListComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-list/anc-initial-evaluation-list.component';
import { ANCInitialEvaluationDetailComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-detail/anc-initial-evaluation-detail.component';
import { ANCInitialEvaluationEditComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-edit/anc-initial-evaluation-edit.component';
import { ANCInitialEvaluationPickComponent } from './ANC_Initial_Evaluation/anc-initial-evaluation-pick/anc-initial-evaluation-pick.component';
import { PostNatalCareListComponent } from './post_natal_care/post-natal-care-list/post-natal-care-list.component';
import { PostNatalCareDetailComponent } from './post_natal_care/post-natal-care-detail/post-natal-care-detail.component';
import { PostNatalCareEditComponent } from './post_natal_care/post-natal-care-edit/post-natal-care-edit.component';
import { PostNatalCarePickComponent } from './post_natal_care/post-natal-care-pick/post-natal-care-pick.component';
import { PresentPregnancyFollowUpListComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-list/present-pregnancy-follow-up-list.component';
import { PresentPregnancyFollowUpDetailComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-detail/present-pregnancy-follow-up-detail.component';
import { PresentPregnancyFollowUpEditComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-edit/present-pregnancy-follow-up-edit.component';
import { PresentPregnancyFollowUpPickComponent } from './present_pregnancy_follow_up/present-pregnancy-follow-up-pick/present-pregnancy-follow-up-pick.component';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
// import interactionPlugin from '@fullcalendar/interaction';
import { ApprovedRequestsComponent } from './requests/approved-requests/approved-requests.component';
import { DispatchOrRejectListComponent } from './dispatch_or_reject/dispatch-or-reject-list/dispatch-or-reject-list.component';
import { DispatchOrRejectDetailComponent } from './dispatch_or_reject/dispatch-or-reject-detail/dispatch-or-reject-detail.component';
import { DispatchOrRejectEditComponent } from './dispatch_or_reject/dispatch-or-reject-edit/dispatch-or-reject-edit.component';
import { DispatchOrRejectPickComponent } from './dispatch_or_reject/dispatch-or-reject-pick/dispatch-or-reject-pick.component'; // a plugin!
import { IntrapartumCarePickComponent } from './intrapartum_care/intrapartum-care-pick/intrapartum-care-pick.component'; // a plugin!
import { IntrapartumCareListComponent } from './intrapartum_care/intrapartum-care-list/intrapartum-care-list.component';
import { IntrapartumCareDetailComponent } from './intrapartum_care/intrapartum-care-detail/intrapartum-care-detail.component';
import { IntrapartumCareEditComponent } from './intrapartum_care/intrapartum-care-edit/intrapartum-care-edit.component';
import { SampleCollectionListComponent } from './sample_collection/sample-collection-list/sample-collection-list.component';
import { SampleCollectionDetailComponent } from './sample_collection/sample-collection-detail/sample-collection-detail.component';
import { SampleCollectionEditComponent } from './sample_collection/sample-collection-edit/sample-collection-edit.component';
import { SampleCollectionPickComponent } from './sample_collection/sample-collection-pick/sample-collection-pick.component';
import { LabResultApprovalListComponent } from './orderss/lab-result-approval-list/lab-result-approval-list.component';
import { RadResultApprovalListComponent } from './orderss/rad-result-approval-list/rad-result-approval-list.component'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!
import timeGridPlugin from '@fullcalendar/timegrid';
import { CameraTakeComponent } from './patients/camera-take/camera-take.component';
import { DoctorConsultationListComponent } from './doctor_dashboard/doctor-consultation-list/doctor-consultation-list.component';
import { DoctorConsultationResponseComponent } from './doctor_dashboard/doctor-consultation-response/doctor-consultation-response.component';
import { DoctorConsultationDetailComponent } from './doctor_dashboard/doctor-consultation-detail/doctor-consultation-detail.component';
import { ConsultingDetailComponent } from './form_encounters/consulting-detail/consulting-detail.component';
import { AncHistoryListComponent } from './anc_history/anc-history-list/anc-history-list.component';
import { AncHistoryDetailComponent } from './anc_history/anc-history-detail/anc-history-detail.component';
import { AncHistoryEditComponent } from './anc_history/anc-history-edit/anc-history-edit.component';
import { AncHistoryPickComponent } from './anc_history/anc-history-pick/anc-history-pick.component';
import { PathologyResultListComponent } from './pathology_result/pathology-result-list/pathology-result-list.component';
import { PathologyResultDetailComponent } from './pathology_result/pathology-result-detail/pathology-result-detail.component';
import { PathologyResultEditComponent } from './pathology_result/pathology-result-edit/pathology-result-edit.component';
import { PathologyResultPickComponent } from './pathology_result/pathology-result-pick/pathology-result-pick.component';
import { PatientDischargeListComponent } from './patient_discharge/patient-discharge-list/patient-discharge-list.component';
import { PatientDischargeDetailComponent } from './patient_discharge/patient-discharge-detail/patient-discharge-detail.component';
import { PatientDischargeEditComponent } from './patient_discharge/patient-discharge-edit/patient-discharge-edit.component';
import { PatientDischargePickComponent } from './patient_discharge/patient-discharge-pick/patient-discharge-pick.component';
import { PatientReferralFormListComponent } from './patient_referral_form/patient-referral-form-list/patient-referral-form-list.component';
import { PatientReferralFormDetailComponent } from './patient_referral_form/patient-referral-form-detail/patient-referral-form-detail.component';
import { PatientReferralFormEditComponent } from './patient_referral_form/patient-referral-form-edit/patient-referral-form-edit.component';
import { PatientReferralFormPickComponent } from './patient_referral_form/patient-referral-form-pick/patient-referral-form-pick.component';
import { OpdReportListComponent } from './patient_report/opd-report-list/opd-report-list.component';
import { IpdReportListComponent } from './patient_report/ipd-report-list/ipd-report-list.component';
import { EdReportListComponent } from './patient_report/ed-report-list/ed-report-list.component';
import { ProcedureReportListComponent } from './reports/procedure-report-list/procedure-report-list.component';
import { LabReportListComponent } from './lab_report/lab-report-list/lab-report-list.component';
import { EncounterReportListComponent } from './reports/encounter-report-list/encounter-report-list.component';
import { PrescriptionAndDispensationReportListComponent } from './reports/prescription-and-dispensation-report-list/prescription-and-dispensation-report-list.component';
import { ReferralsReportListComponent } from './reports/referrals-report-list/referrals-report-list.component';
import { PatientReportList } from './reports/patient-report-list/patient-report-list.component';
import { DispenseHistoryListComponent } from './dispense_history/dispense-history-list/dispense-history-list.component';
import { DispenseHistoryDetailComponent } from './dispense_history/dispense-history-detail/dispense-history-detail.component';
import { DispenseHistoryEditComponent } from './dispense_history/dispense-history-edit/dispense-history-edit.component';


import { OrderDialysisListComponent } from './form_encounter/order-dialysis-list/order-dialysis-list.component'
import { OrderDialysisEditComponent } from './form_encounter/order-dialysis-edit/order-dialysis-edit.component';
import { HemodialysisScheduledPateintListComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-list/hemodialysis-scheduled-pateint-list.component';
import { HemodialysisScheduledPateintDetailComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-detail/hemodialysis-scheduled-pateint-detail.component';
import { HemodialysisScheduledPateintEditComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-edit/hemodialysis-scheduled-pateint-edit.component';
import { HemodialysisScheduledPateintPickComponent } from './hemodialysis_scheduled_pateint/hemodialysis-scheduled-pateint-pick/hemodialysis-scheduled-pateint-pick.component';
import { OrderDialysisPickComponent } from './form_encounter/order-dialysis-pick/order-dialysis-pick.component';
import { HemoPerformedEditComponent } from './form_encounter/hemo-performed-edit/hemo-performed-edit.component';
import { HemoPerformedListComponent } from './form_encounter/hemo-performed-list/hemo-performed-list.component';
import { DialysisMachineListComponent } from './dialysis_machine/dialysis-machine-list/dialysis-machine-list.component';
import { DialysisMachineDetailComponent } from './dialysis_machine/dialysis-machine-detail/dialysis-machine-detail.component';
import { DialysisMachineEditComponent } from './dialysis_machine/dialysis-machine-edit/dialysis-machine-edit.component';
import { DialysisMachinePickComponent } from './dialysis_machine/dialysis-machine-pick/dialysis-machine-pick.component';
import { SurgeryRoomListComponent } from './surgery_room/surgery-room-list/surgery-room-list.component';
import { SurgeryRoomDetailComponent } from './surgery_room/surgery-room-detail/surgery-room-detail.component';
import { SurgeryRoomEditComponent } from './surgery_room/surgery-room-edit/surgery-room-edit.component';
import { SurgeryRoomPickComponent } from './surgery_room/surgery-room-pick/surgery-room-pick.component'

import { ResultApprovalListComponent } from './orderss/result-approval-list/result-approval-list.component';
import { BedModule } from './bed/bed.module';
import { TcModule } from './tc/tc.module';
import { DispenseHistoryPickComponent } from './dispense_history/dispense-history-pick/dispense-history-pick.component';
import { OtherServicesListComponent } from './other_services/other-services-list/other-services-list.component';
import { OtherServicesDetailComponent } from './other_services/other-services-detail/other-services-detail.component';
import { OtherServicesEditComponent } from './other_services/other-services-edit/other-services-edit.component';
import { OtherServicesPickComponent } from './other_services/other-services-pick/other-services-pick.component';
import { OrderedOtheServiceListComponent } from './ordered_othe_service/ordered-othe-service-list/ordered-othe-service-list.component';
import { OrderedOtheServiceDetailComponent } from './ordered_othe_service/ordered-othe-service-detail/ordered-othe-service-detail.component';
import { OrderedOtheServiceEditComponent } from './ordered_othe_service/ordered-othe-service-edit/ordered-othe-service-edit.component';
import { OrderedOtheServicePickComponent } from './ordered_othe_service/ordered-othe-service-pick/ordered-othe-service-pick.component';
import { ResultProgressListComponent } from './result-progress-list/result-progress-list.component';
import { PhysicalExaminationOptionsListComponent } from './physical_examination_options/physical-examination-options-list/physical-examination-options-list.component';
import { PhysicalExaminationOptionsDetailComponent } from './physical_examination_options/physical-examination-options-detail/physical-examination-options-detail.component';
import { PhysicalExaminationOptionsEditComponent } from './physical_examination_options/physical-examination-options-edit/physical-examination-options-edit.component';
import { PhysicalExaminationOptionsPickComponent } from './physical_examination_options/physical-examination-options-pick/physical-examination-options-pick.component';
import { VitalsComponent } from './vitals/vitals.component';

import { RoomListComponent } from './room/room-list/room-list.component';
import { RoomDetailComponent } from './room/room-detail/room-detail.component';
import { RoomEditComponent } from './room/room-edit/room-edit.component';
import { RoomPickComponent } from './room/room-pick/room-pick.component';
import { DoctorInRoomEditComponent } from './room/doctor-in-room-edit/doctor-in-room-edit.component';
import { QueueListComponent } from './queue/queue-list/queue-list.component';
import { QueueDetailComponent } from './queue/queue-detail/queue-detail.component';
import { QueueEditComponent } from './queue/queue-edit/queue-edit.component';
import { QueuePickComponent } from './queue/queue-pick/queue-pick.component';

import { PaymentReportListComponent } from './reports/payment-report-list/payment-report-list.component';
import { DialysisListComponent } from './dialysis/dialysis-list/dialysis-list.component';
import { DialysisDetailComponent } from './dialysis/dialysis-detail/dialysis-detail.component';
import { DialysisEditComponent } from './dialysis/dialysis-edit/dialysis-edit.component';
import { DialysisPickComponent } from './dialysis/dialysis-pick/dialysis-pick.component';
import { DialysisSessionListComponent } from './dialysis_session/dialysis-session-list/dialysis-session-list.component';
import { DialysisSessionDetailComponent } from './dialysis_session/dialysis-session-detail/dialysis-session-detail.component';
import { DialysisSessionEditComponent } from './dialysis_session/dialysis-session-edit/dialysis-session-edit.component';
import { DialysisSessionPickComponent } from './dialysis_session/dialysis-session-pick/dialysis-session-pick.component';
import { DialysisOrderComponent } from './dialysis-order/dialysis-order.component';
import { PatientIntakeFormEditComponent } from './patient_intake_form/patient-intake-form-edit/patient-intake-form-edit.component';
import { PatientIntakeFormDetailComponent } from './patient_intake_form/patient-intake-form-detail/patient-intake-form-detail.component';
import { AdditionalServiceListComponent } from './additional_service/additional-service-list/additional-service-list.component';
import { AdditionalServiceDetailComponent } from './additional_service/additional-service-detail/additional-service-detail.component';
import { AdditionalServiceEditComponent } from './additional_service/additional-service-edit/additional-service-edit.component';
import { AdditionalServicePickComponent } from './additional_service/additional-service-pick/additional-service-pick.component';
import { OtherFormEncountersListComponent } from './form_encounters/other-form-encounters-list/other-form-encounters-list.component';
import { PhysiotherapyComponent } from './physiotherapy/physiotherapy.component';
import { PhysiotherapySessionListComponent } from './physiotherapy_session/physiotherapy-session-list/physiotherapy-session-list.component';
import { PhysiotherapySessionDetailComponent } from './physiotherapy_session/physiotherapy-session-detail/physiotherapy-session-detail.component';
import { RehabilitationOrderDetailComponent } from './rehabilitation-order-detail/rehabilitation-order-detail.component';
import { PhysiotherapyNoteEditComponent } from './physiotherapy_note/physiotherapy-note-edit/physiotherapy-note-edit.component';
import { PhysiotherapyNoteDetailComponent } from './physiotherapy_note/physiotherapy-note-detail/physiotherapy-note-detail.component';
import { ProceduresComponent } from './form_encounters/procedures/procedures.component';
import { SelectedOrdersComponent } from './orderss/selected-orders-detail/selected-orders.component';
import { PreviousResultListComponent } from './form_encounters/orderss/lab_results/previous-result-list/previous-result-list.component';
import { PanelResultListComponent } from './form_encounters/orderss/lab_results/panel-result-list/panel-result-list.component';
import { Icd11PickComponent } from './icd_11/icd11-pick/icd11-pick.component';
import { ItemCategoryListComponent } from './item_category/item-category-list/item-category-list.component';
import { ItemCategoryDetailComponent } from './item_category/item-category-detail/item-category-detail.component';
import { ItemCategoryEditComponent } from './item_category/item-category-edit/item-category-edit.component';
import { ItemCategoryPickComponent } from './item_category/item-category-pick/item-category-pick.component';
import { PurchaseRequestListComponent } from './purchase_request/purchase-request-list/purchase-request-list.component';
import { PurchaseRequestDetailComponent } from './purchase_request/purchase-request-detail/purchase-request-detail.component';
import { PurchaseRequestEditComponent } from './purchase_request/purchase-request-edit/purchase-request-edit.component';
import { PurchaseRequestPickComponent } from './purchase_request/purchase-request-pick/purchase-request-pick.component';
import { ItemReceiveListComponent } from './item_receive/item-receive-list/item-receive-list.component';
import { ItemReceiveDetailComponent } from './item_receive/item-receive-detail/item-receive-detail.component';
import { ItemReceiveEditComponent } from './item_receive/item-receive-edit/item-receive-edit.component';
import { ItemReceivePickComponent } from './item_receive/item-receive-pick/item-receive-pick.component';
import { InterchangeableItemListComponent } from './interchangeable_item/interchangeable-item-list/interchangeable-item-list.component';
import { InterchangeableItemDetailComponent } from './interchangeable_item/interchangeable-item-detail/interchangeable-item-detail.component';
import { InterchangeableItemEditComponent } from './interchangeable_item/interchangeable-item-edit/interchangeable-item-edit.component';
import { InterchangeableItemPickComponent } from './interchangeable_item/interchangeable-item-pick/interchangeable-item-pick.component';
import { StoreStockLevelListComponent } from './store_stock_level/store-stock-level-list/store-stock-level-list.component';
import { StoreStockLevelDetailComponent } from './store_stock_level/store-stock-level-detail/store-stock-level-detail.component';
import { StoreStockLevelEditComponent } from './store_stock_level/store-stock-level-edit/store-stock-level-edit.component';
import { StoreStockLevelPickComponent } from './store_stock_level/store-stock-level-pick/store-stock-level-pick.component';
import { PrescriptionRequestListComponent } from './prescription_request/prescription-request-list/prescription-request-list.component';
import { PrescriptionRequestDetailComponent } from './prescription_request/prescription-request-detail/prescription-request-detail.component';
import { PrescriptionRequestEditComponent } from './prescription_request/prescription-request-edit/prescription-request-edit.component';
import { PrescriptionRequestPickComponent } from './prescription_request/prescription-request-pick/prescription-request-pick.component';
import { PrescriptionRequestByPatientListComponent } from './prescription_request/prescription-request-by-patient-list/prescription-request-by-patient-list.component';
import { PrescriptionRequestByPatientDetailComponent } from './prescription_request/prescription-request-by-patient-detail/prescription-request-by-patient-detail.component';
import { PrescriptionsDetailComponent } from './form_encounters/prescriptions-detail/prescriptions-detail.component';
import { ItemDispatchListComponent } from './item_dispatch/item-dispatch-list/item-dispatch-list.component';
import { ItemDispatchDetailComponent } from './item_dispatch/item-dispatch-detail/item-dispatch-detail.component';
import { ItemDispatchEditChildComponent, ItemDispatchEditComponent } from './item_dispatch/item-dispatch-edit/item-dispatch-edit.component';
import { ItemDispatchPickComponent } from './item_dispatch/item-dispatch-pick/item-dispatch-pick.component';
import { ItemReturnListComponent } from './item_return/item-return-list/item-return-list.component';
import { ItemReturnDetailComponent } from './item_return/item-return-detail/item-return-detail.component';
import { ItemReturnEditComponent } from './item_return/item-return-edit/item-return-edit.component';
import { ItemReturnPickComponent } from './item_return/item-return-pick/item-return-pick.component';
import { RejectReasonEditComponent } from './requests/reject-reason-edit/reject-reason-edit.component';
import { ItemReturnGroupListComponent } from './item_return/item-return-group-list/item-return-group-list.component';
import { SellReportComponent } from './reports/sell-report/sell-report.component';
import { ItemExpireDateReportListComponent } from './reports/item-expire-date-report-list/item-expire-date-report-list.component';
import { RadiologyResultDetailComponent } from './radiology_result/radiology-result-detail/radiology-result-detail.component';
import { RadiologyResultEditComponent } from './radiology_result/radiology-result-edit/radiology-result-edit.component';
import { RadiologyResultListComponent } from './radiology_result/radiology-result-list/radiology-result-list.component';
import { RadiologyResultPickComponent } from './radiology_result/radiology-result-pick/radiology-result-pick.component';
import { RadiologyResultSettingsDetailComponent } from './radiology_result_settings/radiology-result-settings-detail/radiology-result-settings-detail.component';
import { RadiologyResultSettingsEditComponent } from './radiology_result_settings/radiology-result-settings-edit/radiology-result-settings-edit.component';
import { RadiologyResultSettingsListComponent } from './radiology_result_settings/radiology-result-settings-list/radiology-result-settings-list.component';
import { RadiologyResultSettingsPickComponent } from './radiology_result_settings/radiology-result-settings-pick/radiology-result-settings-pick.component';
import { PatientVisitFormComponent } from './patient-visit-form/patient-visit-form.component';
import { ChiefCompliantOptionListComponent } from './chief_compliant_option/chief-compliant-option-list/chief-compliant-option-list.component';
import { ChiefCompliantOptionDetailComponent } from './chief_compliant_option/chief-compliant-option-detail/chief-compliant-option-detail.component';
import { ChiefCompliantOptionEditComponent } from './chief_compliant_option/chief-compliant-option-edit/chief-compliant-option-edit.component';
import { ChiefCompliantOptionPickComponent } from './chief_compliant_option/chief-compliant-option-pick/chief-compliant-option-pick.component';
import { RiskFactorsOptionsListComponent } from './risk_factors_options/risk-factors-options-list/risk-factors-options-list.component';
import { RiskFactorsOptionsDetailComponent } from './risk_factors_options/risk-factors-options-detail/risk-factors-options-detail.component';
import { RiskFactorsOptionsEditComponent } from './risk_factors_options/risk-factors-options-edit/risk-factors-options-edit.component';
import { RiskFactorsOptionsPickComponent } from './risk_factors_options/risk-factors-options-pick/risk-factors-options-pick.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { FinalAssessmentListComponent } from './final_assessment/final-assessment-list/final-assessment-list.component';
import { FinalAssessmentDetailComponent } from './final_assessment/final-assessment-detail/final-assessment-detail.component';
import { FinalAssessmentEditComponent, FinalAssessmentEditListComponent } from './final_assessment/final-assessment-edit/final-assessment-edit.component';
import { FinalAssessmentPickComponent } from './final_assessment/final-assessment-pick/final-assessment-pick.component';
import { ReviewOfSystemDetailComponent } from './review-of-system-detail/review-of-system-detail.component';
import { HomeComponent } from './home/home.component';
import { TopTenDiseasListComponent } from './top-ten-diseas-list/top-ten-diseas-list.component';
import { FormClinicalInstructionDetailComponent } from './form_clinical_instruction/form-clinical-instruction-detail/form-clinical-instruction-detail.component';
import { FormClinicalInstructionPickComponent } from './form_clinical_instruction/form-clinical-instruction-pick/form-clinical-instruction-pick.component';
import { FormClinicalInstructionEditComponent } from './form_clinical_instruction/form-clinical-instruction-edit/form-clinical-instruction-edit.component';
import { ClinicalInstructionResponseListComponent } from './clinical_instruction_response/clinical-instruction-response-list/clinical-instruction-response-list.component';
import { ClinicalInstructionResponseDetailComponent } from './clinical_instruction_response/clinical-instruction-response-detail/clinical-instruction-response-detail.component';
import { ClinicalInstructionResponseEditComponent } from './clinical_instruction_response/clinical-instruction-response-edit/clinical-instruction-response-edit.component';
import { ClinicalInstructionResponsePickComponent } from './clinical_instruction_response/clinical-instruction-response-pick/clinical-instruction-response-pick.component';
import { RiskFactorListComponent } from './risk_factor/risk-factor-list/risk-factor-list.component';
import { RiskFactorDetailComponent } from './risk_factor/risk-factor-detail/risk-factor-detail.component';
import { RiskFactorEditComponent, RiskFactorEditListComponent } from './risk_factor/risk-factor-edit/risk-factor-edit.component';
import { RiskFactorPickComponent } from './risk_factor/risk-factor-pick/risk-factor-pick.component';
import { NewBornNatalCareListComponent } from './new_born_natal_care/new-born-natal-care-list/new-born-natal-care-list.component';
import { NewBornNatalCareDetailComponent } from './new_born_natal_care/new-born-natal-care-detail/new-born-natal-care-detail.component';
import { NewBornNatalCareEditComponent } from './new_born_natal_care/new-born-natal-care-edit/new-born-natal-care-edit.component';
import { NewBornNatalCarePickComponent } from './new_born_natal_care/new-born-natal-care-pick/new-born-natal-care-pick.component';
import { FormSoapDetailComponent } from './form_encounters/form-soap-detail/form-soap-detail.component';
import { FormVitalsDetailComponent } from './form_encounters/form-vitals-detail/form-vitals-detail.component';
import { SurgeryAppointmentsComponent } from './surgery_appointments/surgery-appointments/surgery-appointments.component';
import { PaymentDiscountRequestListComponent } from './payment_discount_request/payment-discount-request-list/payment-discount-request-list.component';
import { PaymentDiscountRequestDetailComponent } from './payment_discount_request/payment-discount-request-detail/payment-discount-request-detail.component';
import { PaymentDiscountRequestEditComponent } from './payment_discount_request/payment-discount-request-edit/payment-discount-request-edit.component';
import { PaymentDiscountRequestPickComponent } from './payment_discount_request/payment-discount-request-pick/payment-discount-request-pick.component';
import { DepositOptionListComponent } from './deposit_option/deposit-option-list/deposit-option-list.component';
import { DepositOptionDetailComponent } from './deposit_option/deposit-option-detail/deposit-option-detail.component';
import { DepositOptionEditComponent } from './deposit_option/deposit-option-edit/deposit-option-edit.component';
import { DepositOptionPickComponent } from './deposit_option/deposit-option-pick/deposit-option-pick.component';
import { CompanyCreditServiceListComponent } from './company_credit_service/company-credit-service-list/company-credit-service-list.component';
import { CompanyCreditServiceDetailComponent } from './company_credit_service/company-credit-service-detail/company-credit-service-detail.component';
import { CompanyCreditServiceEditComponent } from './company_credit_service/company-credit-service-edit/company-credit-service-edit.component';
import { CompanyCreditServicePickComponent } from './company_credit_service/company-credit-service-pick/company-credit-service-pick.component';
import { MarkupPriceListComponent } from './markup_price/markup-price-list/markup-price-list.component';
import { MarkupPriceDetailComponent } from './markup_price/markup-price-detail/markup-price-detail.component';
import { MarkupPriceEditComponent } from './markup_price/markup-price-edit/markup-price-edit.component';
import { MarkupPricePickComponent } from './markup_price/markup-price-pick/markup-price-pick.component';
import { MedicalCheckupPackageListComponent } from './medical_checkup_package/medical-checkup-package-list/medical-checkup-package-list.component';
import { MedicalCheckupPackageDetailComponent } from './medical_checkup_package/medical-checkup-package-detail/medical-checkup-package-detail.component';
import { MedicalCheckupPackageEditComponent } from './medical_checkup_package/medical-checkup-package-edit/medical-checkup-package-edit.component';
import { MedicalCheckupPackagePickComponent } from './medical_checkup_package/medical-checkup-package-pick/medical-checkup-package-pick.component';
import { MedicalCheckupServiceListComponent } from './medical_checkup_service/medical-checkup-service-list/medical-checkup-service-list.component';
import { MedicalCheckupServiceDetailComponent } from './medical_checkup_service/medical-checkup-service-detail/medical-checkup-service-detail.component';
import { MedicalCheckupServiceEditComponent } from './medical_checkup_service/medical-checkup-service-edit/medical-checkup-service-edit.component';
import { MedicalCheckupServicePickComponent } from './medical_checkup_service/medical-checkup-service-pick/medical-checkup-service-pick.component';
import { CreditEmployeeFamilyListComponent } from './credit_employee_family/credit-employee-family-list/credit-employee-family-list.component';
import { CreditEmployeeFamilyDetailComponent } from './credit_employee_family/credit-employee-family-detail/credit-employee-family-detail.component';
import { CreditEmployeeFamilyEditComponent } from './credit_employee_family/credit-employee-family-edit/credit-employee-family-edit.component';
import { CreditEmployeeFamilyPickComponent } from './credit_employee_family/credit-employee-family-pick/credit-employee-family-pick.component';
import { HolidayDetailComponent } from './holiday/holiday-detail/holiday-detail.component';
import { HolidayEditComponent } from './holiday/holiday-edit/holiday-edit.component';
import { HolidayListComponent } from './holiday/holiday-list/holiday-list.component';
import { HolidayPickComponent } from './holiday/holiday-pick/holiday-pick.component';
import { LabTestNormalRangeDetailComponent } from './lab-test-normal-range/lab-test-normal-range-detail/lab-test-normal-range-detail.component';
import { LabTestNormalRangeEditComponent } from './lab-test-normal-range/lab-test-normal-range-edit/lab-test-normal-range-edit.component';
import { LabTestNormalRangeListComponent } from './lab-test-normal-range/lab-test-normal-range-list/lab-test-normal-range-list.component';
import { LabTestNormalRangePickComponent } from './lab-test-normal-range/lab-test-normal-range-pick/lab-test-normal-range-pick.component';
import { Admission_FormEditListComponent } from './form_encounters/admission_forms/admission-form-edit/admission-form-edit.component';
import { CompanyCreditServicePackageListComponent } from './company_credit_service_package/company-credit-service-package-list/company-credit-service-package-list.component';
import { CompanyCreditServicePackageDetailComponent } from './company_credit_service_package/company-credit-service-package-detail/company-credit-service-package-detail.component';
import { CompanyCreditServicePackageEditComponent } from './company_credit_service_package/company-credit-service-package-edit/company-credit-service-package-edit.component';
import { CompanyCreditServicePackagePickComponent } from './company_credit_service_package/company-credit-service-package-pick/company-credit-service-package-pick.component';
import { NurseHistoryListComponent } from './nurse_history/nurse-history-list/nurse-history-list.component';
import { NurseHistoryDetailComponent } from './nurse_history/nurse-history-detail/nurse-history-detail.component';
import { NurseHistoryEditComponent } from './nurse_history/nurse-history-edit/nurse-history-edit.component';
import { NurseHistoryPickComponent } from './nurse_history/nurse-history-pick/nurse-history-pick.component';
import { NutritionalAssesmentListComponent } from './nutritional_assesment/nutritional-assesment-list/nutritional-assesment-list.component';
import { NutritionalAssesmentDetailComponent } from './nutritional_assesment/nutritional-assesment-detail/nutritional-assesment-detail.component';
import { NutritionalAssesmentEditComponent } from './nutritional_assesment/nutritional-assesment-edit/nutritional-assesment-edit.component';
import { NutritionalAssesmentPickComponent } from './nutritional_assesment/nutritional-assesment-pick/nutritional-assesment-pick.component';
import { PlanOfCareListComponent } from './plan_of_care/plan-of-care-list/plan-of-care-list.component';
import { PlanOfCareDetailComponent } from './plan_of_care/plan-of-care-detail/plan-of-care-detail.component';
import { PlanOfCareEditComponent } from './plan_of_care/plan-of-care-edit/plan-of-care-edit.component';
import { PlanOfCarePickComponent } from './plan_of_care/plan-of-care-pick/plan-of-care-pick.component';
import { NursePlanOfCareListComponent } from './nurse_plan_of_care/nurse-plan-of-care-list/nurse-plan-of-care-list.component';
import { NursePlanOfCareDetailComponent } from './nurse_plan_of_care/nurse-plan-of-care-detail/nurse-plan-of-care-detail.component';
import { NursePlanOfCareEditComponent } from './nurse_plan_of_care/nurse-plan-of-care-edit/nurse-plan-of-care-edit.component';
import { NursePlanOfCarePickComponent } from './nurse_plan_of_care/nurse-plan-of-care-pick/nurse-plan-of-care-pick.component';
import { SelectedOrdersListComponent } from './orderss/selected-orders-list/selected-orders-list.component';
import { PhysicalExaminationEditListComponent } from './form_encounters/physical-examination-edit-list/physical-examination-edit-list.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TestRadTemplatesEditComponent } from './test-rad-templates/test-rad-templates-edit/test-rad-templates-edit.component';
import { TestRadTemplatesDetailComponent } from './test-rad-templates/test-rad-templates-detail/test-rad-templates-detail.component';
import { TestRadTemplatesListComponent } from './test-rad-templates/test-rad-templates-list/test-rad-templates-list.component';
import { TestRadTemplatesPickComponent } from './test-rad-templates/test-rad-templates-pick/test-rad-templates-pick.component';
import { TatReportListComponent } from './reports/tat-report-list/tat-report-list.component';
import { OxygenServiceComponent } from './other_services/oxygen-service/oxygen-service.component';
import { BeforeInductionOfAnaesthesiaListComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-list/before-induction-of-anaesthesia-list.component';
import { BeforeInductionOfAnaesthesiaDetailComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-detail/before-induction-of-anaesthesia-detail.component';
import { BeforeInductionOfAnaesthesiaEditComponent, BeforeInductionOfAnaesthesiaEditSecondComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-edit/before-induction-of-anaesthesia-edit.component';
import { BeforeInductionOfAnaesthesiaPickComponent } from './before_induction_of_anaesthesia/before-induction-of-anaesthesia-pick/before-induction-of-anaesthesia-pick.component';
import { BeforePatientLeavesOperatingRoomListComponent } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-list/before-patient-leaves-operating-room-list.component';
import { BeforePatientLeavesOperatingRoomDetailComponent } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-detail/before-patient-leaves-operating-room-detail.component';
import { BeforePatientLeavesOperatingRoomEditComponent, BeforePatientLeavesOperatingRoomEditComponentSecond } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-edit/before-patient-leaves-operating-room-edit.component';
import { BeforePatientLeavesOperatingRoomPickComponent } from './before_patient_leaves_operating_room/before-patient-leaves-operating-room-pick/before-patient-leaves-operating-room-pick.component';
import { BeforeSkinIncisionListComponent } from './before_skin_incision/before-skin-incision-list/before-skin-incision-list.component';
import { BeforeSkinIncisionDetailComponent } from './before_skin_incision/before-skin-incision-detail/before-skin-incision-detail.component';
import { BeforeSkinIncisionEditComponent, BeforeSkinIncisionEditSecondComponent } from './before_skin_incision/before-skin-incision-edit/before-skin-incision-edit.component';
import { BeforeSkinIncisionPickComponent } from './before_skin_incision/before-skin-incision-pick/before-skin-incision-pick.component';
import { SurgicalSafetyChecklistComponent } from './surgical-safety-checklist/surgical-safety-checklist.component';
import { EmergencyObservationListComponent } from './emergency_observation/emergency-observation-list/emergency-observation-list.component';
import { EmergencyObservationDetailComponent } from './emergency_observation/emergency-observation-detail/emergency-observation-detail.component';
import { EmployeeRequestComponent } from './employee_request/employee-request/employee-request.component';
import { ItemRequestListComponent } from './item_request/item-request-list/item-request-list.component';
import { ItemRequestDetailComponent } from './item_request/item-request-detail/item-request-detail.component';
import { ItemRequestEditComponent } from './item_request/item-request-edit/item-request-edit.component';
import { ItemRequestPickComponent } from './item_request/item-request-pick/item-request-pick.component';
import { EmployeeRequestDetailComponent } from './employee_request/employee-request-detail/employee-request-detail.component';
import { TestPanelListComponent } from './lab_panels/test-panel-list/test-panel-list.component';
import { PreviousOrderListComponent } from './previous-order-list/previous-order-list.component';
import { OutsidePrescriptionListComponent } from './outside_prescription/outside-prescription-list/outside-prescription-list.component';
import { OutsidePrescriptionDetailComponent } from './outside_prescription/outside-prescription-detail/outside-prescription-detail.component';
import { OutsidePrescriptionEditComponent } from './outside_prescription/outside-prescription-edit/outside-prescription-edit.component';
import { OutsidePrescriptionPickComponent } from './outside_prescription/outside-prescription-pick/outside-prescription-pick.component';
import { MeasuringUnitListComponent } from './measuring_unit/measuring-unit-list/measuring-unit-list.component';
import { MeasuringUnitDetailComponent } from './measuring_unit/measuring-unit-detail/measuring-unit-detail.component';
import { MeasuringUnitEditComponent } from './measuring_unit/measuring-unit-edit/measuring-unit-edit.component';
import { MeasuringUnitPickComponent } from './measuring_unit/measuring-unit-pick/measuring-unit-pick.component';
import { BankListComponent } from './bank/bank-list/bank-list.component';
import { BankEditComponent } from './bank/bank-edit/bank-edit.component';
import { BankPickComponent } from './bank/bank-pick/bank-pick.component';
import { BankDetailComponent } from './bank/bank-detail/bank-detail.component';
import { BankSummaryComponent } from './bank-summary/bank-summary.component';
import { OpdObservationListComponent } from './opd_observation/opd-observation-list/opd-observation-list.component';
import { OpdObservationDetailComponent } from './opd_observation/opd-observation-detail/opd-observation-detail.component';
import { BinCardComponent } from './bin_card/bin-card/bin-card.component';
import { PreAnsthesiaEvaluationListComponent } from './pre_ansthesia_evaluation/pre-ansthesia-evaluation-list/pre-ansthesia-evaluation-list.component';
import { PreAnsthesiaEvaluationDetailComponent } from './pre_ansthesia_evaluation/pre-ansthesia-evaluation-detail/pre-ansthesia-evaluation-detail.component';
import { PreAnsthesiaEvaluationEditComponent, PreAnsthesiaEvaluationEditListComponent } from './pre_ansthesia_evaluation/pre-ansthesia-evaluation-edit/pre-ansthesia-evaluation-edit.component';
import { PreAnsthesiaEvaluationPickComponent } from './pre_ansthesia_evaluation/pre-ansthesia-evaluation-pick/pre-ansthesia-evaluation-pick.component';
import { IcuPhysicalAssessmentListComponent } from './icu_physical_assessment/icu-physical-assessment-list/icu-physical-assessment-list.component';
import { IcuPhysicalAssessmentDetailComponent } from './icu_physical_assessment/icu-physical-assessment-detail/icu-physical-assessment-detail.component';
import { IcuPhysicalAssessmentEditComponent } from './icu_physical_assessment/icu-physical-assessment-edit/icu-physical-assessment-edit.component';
import { IcuPhysicalAssessmentPickComponent } from './icu_physical_assessment/icu-physical-assessment-pick/icu-physical-assessment-pick.component';
import { ProcedureTemplateEditComponent } from './procedure_template/procedure_template-edit/procedure-template-edit.component';
import { ProcedureTemplateListComponent } from './procedure_template/procedure_template-list/procedure-template-list.component';
import { ProcedureTemplatePickComponent } from './procedure_template/procedure_template-pick/procedure-template-pick.component';
import { ProcedureTemplateDetailComponent } from './procedure_template/procedure_template-detail/prcedure-template-detail.component';
import { BehavioralPainScaleListComponent } from './behavioral_pain_scale/behavioral-pain-scale-list/behavioral-pain-scale-list.component';
import { BehavioralPainScaleDetailComponent } from './behavioral_pain_scale/behavioral-pain-scale-detail/behavioral-pain-scale-detail.component';
import { BehavioralPainScaleEditComponent } from './behavioral_pain_scale/behavioral-pain-scale-edit/behavioral-pain-scale-edit.component';
import { BehavioralPainScalePickComponent } from './behavioral_pain_scale/behavioral-pain-scale-pick/behavioral-pain-scale-pick.component';
import { OutsourcingCompanyEditComponent } from './outsourcing_companys/oursourcing_companys-edit/outsourcing_companys-edit.component';
import { OutsourcingCompanyListComponent } from './outsourcing_companys/outsourcing_companys-list/outsourcing_company-list.component';
import { OutsourcingCompanyPickComponent } from './outsourcing_companys/outsourcing_companys-pick/outsourcing_company-pick.component';
import { OutsourcingCompanyDetailComponent } from './outsourcing_companys/outsourcing_company-detail/outsourcing_company-detail.component';
import { CorelatedFeeListComponent } from './corelated_fee/corelated-fee-list/corelated-fee-list.component';
import { CorelatedFeeDetailComponent } from './corelated_fee/corelated-fee-detail/corelated-fee-detail.component';
import { CorelatedFeeEditComponent } from './corelated_fee/corelated-fee-edit/corelated-fee-edit.component';
import { CorelatedFeePickComponent } from './corelated_fee/corelated-fee-pick/corelated-fee-pick.component';
import { PosMachineListComponent } from './pos_machine/pos-machine-list/pos-machine-list.component';
import { PosMachineDetailComponent } from './pos_machine/pos-machine-detail/pos-machine-detail.component';
import { PosMachineEditComponent } from './pos_machine/pos-machine-edit/pos-machine-edit.component';
import { PosMachinePickComponent } from './pos_machine/pos-machine-pick/pos-machine-pick.component';
import { PosMachineUserListComponent } from './pos_machine_user/pos-machine-user-list/pos-machine-user-list.component';
import { PosMachineUserDetailComponent } from './pos_machine_user/pos-machine-user-detail/pos-machine-user-detail.component';
import { PosMachineUserEditComponent } from './pos_machine_user/pos-machine-user-edit/pos-machine-user-edit.component';
import { PosMachineUserPickComponent } from './pos_machine_user/pos-machine-user-pick/pos-machine-user-pick.component';
import { FlowSheetListComponent } from './flow_sheet/flow-sheet-list/flow-sheet-list.component';
import { FlowSheetDetailComponent } from './flow_sheet/flow-sheet-detail/flow-sheet-detail.component';
import { FlowSheetEditComponent } from './flow_sheet/flow-sheet-edit/flow-sheet-edit.component';
import { FlowSheetPickComponent } from './flow_sheet/flow-sheet-pick/flow-sheet-pick.component';
import { PatientHistoryDetailComponent } from './patient_history/patient-history-detail/patient-history-detail.component';
import { PatientHistoryEditListComponent,PatientHistoryEditComponent } from './patient_history/patient-history-edit/patient-history-edit.component';
import { PatientHistoryListComponent } from './patient_history/patient-history-list/patient-history-list.component';
import { PatientHistoryPickComponent } from './patient_history/patient-history-pick/patient-history-pick.component';
import { SessionTherapyListComponent } from './session_therapy/session-therapy-list/session-therapy-list.component';
import { SessionTherapyDetailComponent } from './session_therapy/session-therapy-detail/session-therapy-detail.component';
import { SessionTherapyEditComponent } from './session_therapy/session-therapy-edit/session-therapy-edit.component';
import { SessionTherapyPickComponent } from './session_therapy/session-therapy-pick/session-therapy-pick.component';
import { GroupMemberListComponent } from './group_member/group-member-list/group-member-list.component';
import { GroupMemberDetailComponent } from './group_member/group-member-detail/group-member-detail.component';
import { GroupMemberEditComponent } from './group_member/group-member-edit/group-member-edit.component';
import { GroupMemberPickComponent } from './group_member/group-member-pick/group-member-pick.component';
import { NotificationComponent } from './notification/notification.component';
import { WardReportListComponent } from './ward_report/ward-report-list/ward-report-list.component';
import { WardReportDetailComponent } from './ward_report/ward-report-detail/ward-report-detail.component';
import { WardReportEditComponent } from './ward_report/ward-report-edit/ward-report-edit.component';
import { WardReportPickComponent } from './ward_report/ward-report-pick/ward-report-pick.component';
import { PatientWardReportListComponent } from './patient_ward_report/patient-ward-report-list/patient-ward-report-list.component';
import { PatientWardReportDetailComponent } from './patient_ward_report/patient-ward-report-detail/patient-ward-report-detail.component';
import { PatientWardReportEditComponent } from './patient_ward_report/patient-ward-report-edit/patient-ward-report-edit.component';
import { PatientWardReportPickComponent } from './patient_ward_report/patient-ward-report-pick/patient-ward-report-pick.component';
import { MachinesListComponent } from './machines/machines-list/machines-list.component';
import { MachinesDetailComponent } from './machines/machines-detail/machines-detail.component';
import { MachinesEditComponent } from './machines/machines-edit/machines-edit.component';
import { MachinesPickComponent } from './machines/machines-pick/machines-pick.component';
import { MachineTestListComponent } from './machines/machine_test/machine-test-list/machine-test-list.component';
import { MachineTestDetailComponent } from './machines/machine_test/machine-test-detail/machine-test-detail.component';
import { MachineTestEditComponent } from './machines/machine_test/machine-test-edit/machine-test-edit.component';
import { MachineTestPickComponent } from './machines/machine_test/machine-test-pick/machine-test-pick.component';
import { DailyReportListComponent } from './daily_report/daily-report-list/daily-report-list.component';
import { DailyReportDetailComponent } from './daily_report/daily-report-detail/daily-report-detail.component';
import { DailyReportEditComponent } from './daily_report/daily-report-edit/daily-report-edit.component';
import { DailyReportPickComponent } from './daily_report/daily-report-pick/daily-report-pick.component';
import { AngularSignaturePadModule } from '@almothafar/angular-signature-pad';
import { QueueBarModule } from 'ngx-queue-bar';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { EthiopianBirrFormatPipe } from './tc/ethiopian-birr-format';

import { PsychoterapyProgressNoteListComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-list/psychoterapy-progress-note-list.component';
import { PsychoterapyProgressNoteDetailComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-detail/psychoterapy-progress-note-detail.component';
import { PsychoterapyProgressNoteEditComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-edit/psychoterapy-progress-note-edit.component';
import { PsychoterapyProgressNotePickComponent } from './psychoterapy_progress_note/psychoterapy-progress-note-pick/psychoterapy-progress-note-pick.component';
import { QueuesListComponent } from './queues/queues-list/queues-list.component';
import { QueuesDetailComponent } from './queues/queues-detail/queues-detail.component';
import { QueuesEditComponent } from './queues/queues-edit/queues-edit.component';
import { QueuesPickComponent } from './queues/queues-pick/queues-pick.component';
import { CreditPaymentListComponent } from './credit_payment/credit-payment-list/credit-payment-list.component';
import { CreditPaymentDetailComponent } from './credit_payment/credit-payment-detail/credit-payment-detail.component';
import { CreditPaymentEditComponent } from './credit_payment/credit-payment-edit/credit-payment-edit.component';
import { CreditPaymentPickComponent } from './credit_payment/credit-payment-pick/credit-payment-pick.component';
import { CreditPaymentHistoryListComponent } from './credit_payment_history/credit-payment-history-list/credit-payment-history-list.component';
import { CreditPaymentHistoryDetailComponent } from './credit_payment_history/credit-payment-history-detail/credit-payment-history-detail.component';
import { CreditPaymentHistoryEditComponent } from './credit_payment_history/credit-payment-history-edit/credit-payment-history-edit.component';
import { CreditPaymentHistoryPickComponent } from './credit_payment_history/credit-payment-history-pick/credit-payment-history-pick.component';
import { CustomDatepickerDirective } from './custom-datepicker.directive';
import { CustomDatetimepickerDirective } from './custom-datetimepicker.directive';
import { CustomFormValidationDirective } from './custom-form-validation.directive';
import { TextareaAutoresizeDirective } from './textarea-autoresize.directive';
import { CustomDatePipe } from './full_date_format';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,timeGridPlugin,
  interactionPlugin
]);


@NgModule({
    declarations: [
        TextareaAutoresizeDirective,
        CustomDatepickerDirective,
        CustomDatePipe,
        CustomFormValidationDirective,
        CustomDatetimepickerDirective,
        AppComponent,
        EthiopianBirrFormatPipe,
        WelcomeComponent,
        DashboardComponent,
        LoginComponent,
        ProfileComponent,
        PasswordChangeComponent,
        AboutComponent,
        AccessDeniedComponent,
        InvalidOperationComponent,
        NotFoundComponent,
        TosComponent,
        MandatoryComponent,
        ConfirmComponent,
        ValidationMessagesComponent,
        TextinputComponent,
        AsyncJobsComponent,
        DateinputComponent,

        DeliverySummaryDetailComponent,
        DeliverySummaryEditComponent,
        DeliverySummaryPickComponent,
        DeliverySummaryListComponent,

        FileListComponent,
        Physical_ExaminationEditComponent,
        InsuranceCompanyEditComponent,
        InsuranceCompanyPickComponent,
        HistoryEditComponent,

        Medical_CertificateEditComponent,

        Test_PanelEditComponent,



        Item_Ledger_EntryDetailComponent,

        TgidentityListComponent,
        TgidentityDetailComponent,
        OrdersPickComponent,
        TgidentityEditComponent,
        TgidentityPickComponent,
        PatientListComponent,
        PatientDetailComponent,
        PatientEditComponent,
        PatientPickComponent,
        PatientsHomeComponent,
        GeneralCaseComponent,
        MenuParentComponent,
        MenuChildComponent,

        OrdersEditComponent,

        SettingListComponent,
        SettingDetailComponent,
        SettingEditComponent,
        SettingPickComponent,

        ValidationMessagesComponent,
        RequestListComponent,
        RequestDetailComponent,
        RequestEditComponent,
        RequestPickComponent,
        LostDamageDetailComponent,
        LostDamageEditComponent,
        LostDamagePickComponent,
        LostDamageListComponent,
        LostDamageDetailComponent,
        LostDamageEditComponent,
        LostDamagePickComponent,
        ItemListComponent,
        ItemDetailComponent,
        ItemEditComponent,
        ItemPickComponent,
        SupplierListComponent,
        SupplierDetailComponent,
        SupplierEditComponent,
        SupplierPickComponent,
        DrugtypeListComponent,
        DrugtypeDetailComponent,
        DrugtypeEditComponent,
        DrugtypePickComponent,
        StoreListComponent,
        StoreDetailComponent,
        StoreEditComponent,
        StorePickComponent,
        TransferListComponent,
        TransferDetailComponent,
        TransferEditComponent,
        TransferPickComponent,
        Item_Ledger_EntryEditComponent,
        Item_Ledger_EntryListComponent,
        Item_Ledger_EntryPickComponent,
        StorePickComponent,
        PurchaseListComponent,
        PurchaseDetailComponent,
        PurchaseEditComponent,
        PurchasePickComponent,
        EmployeeListComponent,
        EmployeeDetailComponent,
        EmployeeEditComponent,
        EmployeePickComponent,
        EmployeeHomeComponent,
        EmployeeHomeRequestListComponent,
        EmployeeHomeRequestEditComponent,
        EmployeeHomeResponseApproveComponent,
        EmployeeHomeTransferComponent,
        EmployeeHomeLedgerComponent,
        PaymentEditComponent,
        AvailabilityEditComponent,
        AvailabilityPickComponent,
        AppointmentEditComponent,
        AppointmentPickComponent,
        AppointmentEditComponent,
        DoctorEditComponent,
        DoctorPickComponent,
        ReceptionEditComponent,
        ReceptionPickComponent,
        PaymentDetailComponent,
        PaymentListComponent,
        AppointmentDetailComponent,
        AppointmentListComponent,
        AvailabilityListComponent,
        AvailabilityDetailComponent,
        ReceptionDetailComponent,
        ReceptionListComponent,
        DoctorListComponent,
        DoctorDetailComponent,
        DoctorEditComponent,
        DoctorPickComponent,
        DoctorSelfComponent,
        ReceptionSelfComponent,
        PaymentPickComponent,
        InsuranceCompanyDetailComponent,
        InsuranceCompanyListComponent,
        Openemr_UserPickComponent,
        ReportListComponent,
        ReportDetailComponent,
        ReportEditComponent,
        ReportPickComponent,
        PatientAppointmentEditComponent,
        PatientAppointmentCaseComponent,
        Openemr_UserPickComponent,
        FeeListComponent,
        FeeDetailComponent,
        FeeEditComponent,
        FeePickComponent,
        Fee_TypeListComponent,
        Fee_TypePickComponent,
        Fee_TypeEditComponent,
        Fee_TypeDetailComponent,
        Item_In_StoreListComponent,
        Item_In_StoreDetailComponent,
        Item_In_StoreEditComponent,
        Item_In_StorePickComponent,
        Surgery_TypeListComponent,
        Surgery_TypeDetailComponent,
        Surgery_TypeEditComponent,
        Surgery_TypePickComponent,
        Surgery_AppointmentListComponent,
        Surgery_AppointmentDetailComponent,
        Surgery_AppointmentEditComponent,
        Surgery_AppointmentPickComponent,
        DoctorSurgery_AppointmentEditComponent,
        DepositListComponent,
        DepositDetailComponent,
        DepositEditComponent,
        DepositPickComponent,
        Deposit_HistoryEditComponent,
        Fee_TypeDetailComponent,
        VitalListComponent,
        VitalDetailComponent,
        VitalEditComponent,
        VitalPickComponent,
        LabSelfComponent,
        PrescriptionsEditComponent,
        Form_EncounterListComponent,
        Form_EncounterDetailComponent,
        Form_EncounterEditComponent,
        Form_EncounterPickComponent,
        Form_VitalsEditComponent,
        Procedure_OrderListComponent,
        Procedure_OrderDetailComponent,
        Procedure_OrderEditComponent,
        Procedure_OrderPickComponent,
        Form_SoapEditComponent,
        History_DataEditComponent,
        Form_Encounter_Surgery_AppointmentEditComponent,
        Form_ObservationEditComponent,
        ProcedureOrderCodeEditComponent,
        Procedure_ProviderListComponent,
        Procedure_ProviderDetailComponent,
        Procedure_ProviderEditComponent,
        Procedure_ProviderPickComponent,
        PendingReviewComponent,
        PendingReviewResultComponent,
        ProcedureOrderOverviewComponent,
        ProcedureOrderResultsComponent,
        ProcedureOrderResultComponent,
        PatientDispatchComponent,
        FormDrugPickComponent,
        Doctor_Private_InfoEditComponent,
        Encounter_ImagesEditComponent,
        MessagesListComponent,
        MessagesDetailComponent,
        MessagesEditComponent,
        MessagesPickComponent,
        Case_TransferDetailComponent,
        Case_TransferPickComponent,
        Case_TransferListComponent,
        Case_TransferEditComponent,
        DiagnosisCodePickComponent,
        Procedure_TestsListComponent,
        ProcedureTestsDetailComponent,
        Procedure_TestsEditComponent,
        Procedure_TestsPickComponent,
        Procedure_Payment_DueListComponent,
        ProcedurePaymentDueDetailComponent,
        Procedure_Payment_DueEditComponent,
        ProcedurePaymentDuePickComponent,
        CompanyListComponent,
        CompanyDetailComponent,
        CompanyEditComponent,
        CompanyPickComponent,
        InsuredEmployeeListComponent,
        InsuredEmployeeDetailComponent,
        InsuredEmployeeEditComponent,
        InsuredEmployeePickComponent,
        PaymentDueListComponent,
        PaymentDueDetailComponent,
        PaymentDueEditComponent,
        PaymentDuePickComponent,
        MedicalReportChartsComponent,
        InsuranceCompanyDetailComponent,
        InsuranceCompanyListComponent,
        OrdersListComponent,
        OrdersDetailComponent,
        OrdersEditComponent,
        OrdersPickComponent,
        Lab_PanelDetailComponent,
        Lab_PanelListComponent,
        Lab_PanelEditComponent,
        Lab_PanelPickComponent,
        Lab_TestListComponent,
        Lab_TestDetailComponent,
        Lab_TestEditComponent,
        Lab_TestPickComponent,
        Lab_TestEditComponent,
        Lab_TestPickComponent,
        Lab_OrderListComponent,
        Lab_OrderDetailComponent,
        Lab_OrderEditComponent,
        Lab_OrderPickComponent,
        HistoryEditComponent,
        Review_Of_System_OptionsListComponent,
        Review_Of_System_OptionsDetailComponent,
        Review_Of_System_OptionsEditComponent,
        Review_Of_System_OptionsPickComponent,
        Review_Of_SystemEditComponent,
        DiagnosisListComponent,
        DiagnosisDetailComponent,
        DiagnosisEditComponent,
        DiagnosisPickComponent,
        Lab_ResultListComponent,
        Lab_ResultEditComponent,
        Lab_ResultDetailComponent,
        Lab_ResultPickComponent,
        OrdersListComponent,
        OrdersDetailComponent,
        OrdersEditComponent,
        OrdersPickComponent,
        RadListComponent,
        LabListComponent,
        LabDetailComponent,
        RadDetailComponent,
        RadDetailComponent,
        LabEditComponent,
        Test_PanelEditComponent,
        Physiotherapy_TypeListComponent,
        Physiotherapy_TypeDetailComponent,
        Physiotherapy_TypePickComponent,
        Physiotherapy_TypeEditComponent,
        Rehabilitation_OrderEditComponent,
        DepartmentListComponent,
        DepartmentDetailComponent,
        DepartmentEditComponent,
        DepartmentPickComponent,
        Department_SpecialtyListComponent,
        Department_SpecialtyDetailComponent,
        Department_SpecialtyEditComponent,
        Department_SpecialtyPickComponent,
        Consent_FormListComponent,
        Consent_FormDetailComponent,
        Consent_FormEditComponent,
        Consent_FormPickComponent,
        BirthListComponent,
        BirthDetailComponent,
        BirthEditComponent,
        BirthPickComponent,
        AvailabilityCalenderViewComponent,
        VaccinationListComponent,
        VaccinationDetailComponent,
        VaccinationEditComponent,
        VaccinationPickComponent,
        VaccinatedListComponent,
        VaccinatedDetailComponent,
        VaccinatedEditComponent,
        VaccinatedPickComponent,
        DeathListComponent,
        DeathDetailComponent,
        DeathEditComponent,
        DeathPickComponent,
        ProcedureTypeListComponent,
        ProcedureTypeDetailComponent,
        ProcedureTypeEditComponent,
        ProcedureTypePickComponent,
        ProcedureListComponent,
        ProcedureDetailComponent,
        ProcedureEditComponent,
        ProcedurePickComponent,
        AdmissionOrderComponent,
        PatientProcedureEditComponent,
        Cancelled_AppointmentListComponent,
        Cancelled_AppointmentDetailComponent,
        Cancelled_AppointmentEditComponent,
        Cancelled_AppointmentPickComponent,
        PatientProcedureNotesComponent,
        Outside_OrdersListComponent,
        Outside_OrdersDetailComponent,
        Outside_OrdersEditComponent,
        Outside_OrdersPickComponent,
        LabOrderComponent,
        ConsultingEditComponent,
        MedicalCertificateEmployeeListComponent,
        MedicalCertificateEmployeeDetailComponent,
        MedicalCertificateEmployeeEditComponent,
        MedicalCertificateEmployeePickComponent,
        BiopsyRequestListComponent,
        BiopsyRequestDetailComponent,
        BiopsyRequestEditComponent,
        BiopsyRequestPickComponent,
        MedicationAdminstrationChartListComponent,
        MedicationAdminstrationChartDetailComponent,
        MedicationAdminstrationChartEditComponent,
        MedicationAdminstrationChartPickComponent,
        OperationNoteListComponent,
        OperationNoteDetailComponent,
        OperationNoteEditComponent,
        OperationNotePickComponent,
        DriverMedicalCertificateListComponent,
        DriverMedicalCertificateDetailComponent,
        DriverMedicalCertificateEditComponent,
        DriverMedicalCertificatePickComponent,
        PharmacyDashboardComponent,
        CreditHistoryListComponent,
        AllergyListComponent,
        AllergyDetailComponent,
        AllergyEditComponent,
        AllergyPickComponent,
        AllergyFormEditComponent,
        NurseRecordListComponent,
        NurseRecordDetailComponent,
        PatientTreatmentListComponent,
        PatientTreatmentDetailComponent,
        Patient_TreatmentEditComponent,
        Patient_TreatmentPickComponent,
        PatientRecordListComponent,
        PatientRecordDetailComponent,
        Patient_RecordEditComponent,
        Patient_RecordPickComponent,
        FormVitalsListComponent,
        AleryFormListComponent,
        FormSoapsListComponent,
        HistoryListComponent,
        PrescriptionListComponent,
        FormClinicalInstructionListComponent,
        HistoryDataLisComponent,
        NotesListComponent,
        FormObservationsListComponent,
        SurgeryAppointmentListComponent,
        ReviewOfSystemListComponent,
        ConsultingListComponent,
        PatientProcedureListComponent,
        DoctorPrivateInfoListComponent,
        MedicalCertificateListComponent,
        EncounterImagesListComponent,
        FollowupNoteListComponent,
        RehabilitationOrderListComponent,
        PhysicalExaminationListComponent,
        FormEncountersListComponent,
        LabOrdersListComponent,
        OverviewComponent,
        CameraTakeComponent,
        FormVitalsComponent,
        OtherEncounterComponent,
        RadiologyDataListComponent,
        RadiologyDataDetailComponent,
        RadiologyDataEditComponent,
        RadiologyDataPickComponent,
        EquipmentDataListComponent,
        EquipmentDataDetailComponent,
        EquipmentDataEditComponent,
        EquipmentDataPickComponent,
        ResultDescriptionEditComponent,
        ANCInitialEvaluationListComponent,
        ANCInitialEvaluationDetailComponent,
        ANCInitialEvaluationEditComponent,
        ANCInitialEvaluationPickComponent,
        PostNatalCareListComponent,
        PostNatalCareDetailComponent,
        PostNatalCareEditComponent,
        PostNatalCarePickComponent,
        PresentPregnancyFollowUpListComponent,
        PresentPregnancyFollowUpDetailComponent,
        PresentPregnancyFollowUpEditComponent,
        PresentPregnancyFollowUpPickComponent,
        ApprovedRequestsComponent,
        DispatchOrRejectListComponent,
        DispatchOrRejectDetailComponent,
        DispatchOrRejectEditComponent,
        DispatchOrRejectPickComponent,
        DoctorConsultationListComponent,
        DoctorConsultationDetailComponent,
        DoctorConsultationResponseComponent,
        IntrapartumCarePickComponent,
        IntrapartumCareListComponent,
        IntrapartumCareDetailComponent,
        IntrapartumCareEditComponent,

        SampleCollectionListComponent,
        SampleCollectionDetailComponent,
        SampleCollectionEditComponent,
        SampleCollectionPickComponent,
        LabResultApprovalListComponent,
        RadResultApprovalListComponent,
        ConsultingDetailComponent,
        AncHistoryListComponent,
        AncHistoryDetailComponent,
        AncHistoryEditComponent,
        AncHistoryPickComponent,
        PathologyResultListComponent,
        PathologyResultDetailComponent,
        PathologyResultEditComponent,
        PathologyResultPickComponent,
        PatientDischargeListComponent,
        PatientDischargeDetailComponent,
        PatientDischargeEditComponent,
        PatientDischargePickComponent,
        PatientReferralFormListComponent,
        PatientReferralFormDetailComponent,
        PatientReferralFormEditComponent,
        OrderDialysisPickComponent,
        PatientReferralFormPickComponent,
        OpdReportListComponent,
        IpdReportListComponent,
        EdReportListComponent,
        ProcedureReportListComponent,
        LabReportListComponent,
        EncounterReportListComponent,
        PrescriptionAndDispensationReportListComponent,
        ReferralsReportListComponent,
        PatientReportList,
        DispenseHistoryListComponent,
        DispenseHistoryDetailComponent,
        DispenseHistoryEditComponent,
        DispenseHistoryPickComponent,
        OrderDialysisListComponent,
        OrderDialysisEditComponent,
        HemodialysisScheduledPateintListComponent,
        HemodialysisScheduledPateintDetailComponent,
        HemodialysisScheduledPateintEditComponent,
        HemodialysisScheduledPateintPickComponent,
        OrderDialysisPickComponent,
        HemoPerformedEditComponent,
        HemoPerformedListComponent,
        DialysisMachineListComponent,
        DialysisMachineDetailComponent,
        DialysisMachineEditComponent,
        DialysisMachinePickComponent,
        SurgeryRoomListComponent,
        SurgeryRoomDetailComponent,
        SurgeryRoomEditComponent, 
        SurgeryRoomPickComponent,
        ResultApprovalListComponent,
        OtherServicesListComponent,
        OtherServicesDetailComponent,
        OtherServicesEditComponent,
        OtherServicesPickComponent,
        OrderedOtheServiceListComponent,
        OrderedOtheServiceDetailComponent,
        OrderedOtheServiceEditComponent,
        OrderedOtheServicePickComponent,
        ResultProgressListComponent,
        PhysicalExaminationOptionsListComponent,
        PhysicalExaminationOptionsDetailComponent,
        PhysicalExaminationOptionsEditComponent,
        PhysicalExaminationOptionsPickComponent,
        VitalsComponent,

        RoomListComponent,
        RoomDetailComponent,
        RoomEditComponent,
        RoomPickComponent,
        DoctorInRoomEditComponent,
        QueueListComponent,
        QueueDetailComponent,
        QueueEditComponent,
        QueuePickComponent,

        PaymentReportListComponent,
        DialysisListComponent,
        DialysisDetailComponent,
        DialysisEditComponent,
        DialysisPickComponent,
        DialysisSessionListComponent,
        DialysisSessionDetailComponent,
        DialysisSessionEditComponent,
        DialysisSessionPickComponent,
        DialysisOrderComponent,
        PatientIntakeFormEditComponent,
        PatientIntakeFormDetailComponent,
        AdditionalServiceListComponent,
        AdditionalServiceDetailComponent,
        AdditionalServiceEditComponent,
        AdditionalServicePickComponent,
        OtherFormEncountersListComponent,
        PhysiotherapyComponent,
        PhysiotherapySessionListComponent,
        PhysiotherapySessionDetailComponent,
        RehabilitationOrderDetailComponent,
        PhysiotherapyNoteEditComponent,
        PhysiotherapyNoteDetailComponent,
        ProceduresComponent,
        SelectedOrdersComponent,
        PreviousResultListComponent,
        PanelResultListComponent,
        Icd11PickComponent,
        PurchaseRequestDetailComponent,
        PurchaseRequestPickComponent,
        PurchaseRequestListComponent,
        PurchaseRequestEditComponent,
        ItemReceiveListComponent,
        ItemReceiveDetailComponent,
        ItemReceiveEditComponent,
        ItemReceivePickComponent,
        InterchangeableItemListComponent,
        InterchangeableItemDetailComponent,
        InterchangeableItemEditComponent,
        InterchangeableItemPickComponent,
        StoreStockLevelListComponent,
        StoreStockLevelDetailComponent,
        StoreStockLevelEditComponent,
        StoreStockLevelPickComponent,
        PrescriptionRequestListComponent,
        PrescriptionRequestDetailComponent,
        PrescriptionRequestEditComponent,
        PrescriptionRequestPickComponent,
        PrescriptionRequestByPatientListComponent,
        PrescriptionRequestByPatientDetailComponent,
        PrescriptionsDetailComponent,
        ItemDispatchListComponent,
        ItemDispatchDetailComponent,
        ItemDispatchEditComponent,
        ItemDispatchPickComponent,
        ItemReturnListComponent,
        ItemReturnDetailComponent,
        ItemReturnEditComponent,
        ItemReturnPickComponent,
        PurchaseRequestDetailComponent,
        PurchaseRequestEditComponent,
        ItemCategoryPickComponent,
        ItemCategoryEditComponent,
        ItemCategoryDetailComponent,
        ItemCategoryListComponent,
        ItemCategoryPickComponent,
        RejectReasonEditComponent,
        ItemReturnGroupListComponent,
        SellReportComponent,
        ItemExpireDateReportListComponent,
        RadiologyResultSettingsListComponent,
        RadiologyResultSettingsDetailComponent,
        RadiologyResultSettingsEditComponent,
        RadiologyResultSettingsPickComponent,
        RadiologyResultListComponent,
        RadiologyResultDetailComponent,
        RadiologyResultEditComponent,
        RadiologyResultPickComponent,
        PatientVisitFormComponent,
        HistoryEditListComponent,
        ChiefCompliantOptionListComponent,
        ChiefCompliantOptionDetailComponent,
        ChiefCompliantOptionEditComponent,
        ChiefCompliantOptionPickComponent,
        RiskFactorsOptionsListComponent,
        RiskFactorsOptionsDetailComponent,
        RiskFactorsOptionsEditComponent,
        RiskFactorsOptionsPickComponent,
        DiagnosisEDitListComponent,
        FinalAssessmentListComponent,
        FinalAssessmentDetailComponent,
        FinalAssessmentEditComponent,
        FinalAssessmentPickComponent,
        FinalAssessmentEditListComponent,
        ReviewOfSystemDetailComponent,
        Review_Of_SystemEditListComponent,
        Physical_ExaminationEditListComponent,
        HomeComponent,
        TopTenDiseasListComponent,
        FormClinicalInstructionDetailComponent,
        FormClinicalInstructionPickComponent,
        FormClinicalInstructionEditComponent,
        ClinicalInstructionResponseListComponent,
        ClinicalInstructionResponseDetailComponent,
        ClinicalInstructionResponseEditComponent,
        ClinicalInstructionResponsePickComponent,
        RiskFactorListComponent,
        RiskFactorDetailComponent,
        RiskFactorEditComponent,
        RiskFactorPickComponent,
        RiskFactorEditListComponent,
        NewBornNatalCareListComponent,
        NewBornNatalCareDetailComponent,
        NewBornNatalCareEditComponent,
        NewBornNatalCarePickComponent,
        FormSoapDetailComponent,
        FormVitalsDetailComponent,
        SurgeryAppointmentsComponent,
        PaymentDiscountRequestListComponent,
        PaymentDiscountRequestDetailComponent,
        PaymentDiscountRequestEditComponent,
        PaymentDiscountRequestPickComponent,
        DepositOptionListComponent,
        DepositOptionDetailComponent,
        DepositOptionEditComponent,
        DepositOptionPickComponent,
        CompanyCreditServiceListComponent,
        CompanyCreditServiceDetailComponent,
        CompanyCreditServiceEditComponent,
        CompanyCreditServicePickComponent,
        MarkupPriceListComponent,
        MarkupPriceDetailComponent,
        MarkupPriceEditComponent,
        MarkupPricePickComponent,
        MedicalCheckupPackageListComponent,
        MedicalCheckupPackageDetailComponent,
        MedicalCheckupPackageEditComponent,
        MedicalCheckupPackagePickComponent,
        MedicalCheckupServiceListComponent,
        MedicalCheckupServiceDetailComponent,
        MedicalCheckupServiceEditComponent,
        MedicalCheckupServicePickComponent,
        CreditEmployeeFamilyListComponent,
        CreditEmployeeFamilyDetailComponent,
        CreditEmployeeFamilyEditComponent,
        CreditEmployeeFamilyPickComponent,
        HolidayListComponent,
        HolidayDetailComponent,
        HolidayEditComponent,
        HolidayPickComponent,
        LabTestNormalRangeListComponent,
        LabTestNormalRangeDetailComponent,
        LabTestNormalRangeEditComponent,
        LabTestNormalRangePickComponent,
        Admission_FormEditListComponent,
        CompanyCreditServicePackageListComponent,
        CompanyCreditServicePackageDetailComponent,
        CompanyCreditServicePackageEditComponent,
        CompanyCreditServicePackagePickComponent,
        NurseHistoryListComponent,
        NurseHistoryDetailComponent,
        NurseHistoryEditComponent,
        NurseHistoryPickComponent,
        NutritionalAssesmentListComponent,
        NutritionalAssesmentDetailComponent,
        NutritionalAssesmentEditComponent,
        NutritionalAssesmentPickComponent,
        PlanOfCareListComponent,
        PlanOfCareDetailComponent,
        PlanOfCareEditComponent,
        PlanOfCarePickComponent,
        NursePlanOfCareListComponent,
        NursePlanOfCareDetailComponent,
        NursePlanOfCareEditComponent,
        NursePlanOfCarePickComponent,
        SelectedOrdersListComponent,
        PhysicalExaminationEditListComponent,
        TestRadTemplatesEditComponent,
        TestRadTemplatesDetailComponent,
        TestRadTemplatesListComponent,
        TestRadTemplatesPickComponent,
        PrescriptionsComponent,
        PrescriptionsEditListComponent,
        BankDetailComponent,
        TatReportListComponent,
        OxygenServiceComponent,
        BeforePatientLeavesOperatingRoomEditComponentSecond,
        BeforeInductionOfAnaesthesiaEditSecondComponent,
        BeforeSkinIncisionEditSecondComponent,
        BeforeInductionOfAnaesthesiaListComponent,
        BeforeInductionOfAnaesthesiaDetailComponent,
        BeforeInductionOfAnaesthesiaEditComponent,
        BeforeInductionOfAnaesthesiaPickComponent,
        BeforePatientLeavesOperatingRoomListComponent,
        BeforePatientLeavesOperatingRoomDetailComponent,
        BeforePatientLeavesOperatingRoomEditComponent,
        BeforePatientLeavesOperatingRoomPickComponent,
        BeforeSkinIncisionListComponent,
        BeforeSkinIncisionDetailComponent,
        BeforeSkinIncisionEditComponent,
        BeforeSkinIncisionPickComponent,
        SurgicalSafetyChecklistComponent,
        EmergencyObservationListComponent,
        EmergencyObservationDetailComponent,
        EmployeeRequestComponent,
        ItemRequestListComponent,
        ItemRequestDetailComponent,
        ItemRequestEditComponent,
        ItemRequestPickComponent,
        EmployeeRequestDetailComponent,
        TestPanelListComponent,
        PreviousOrderListComponent,
        OutsidePrescriptionListComponent,
        OutsidePrescriptionDetailComponent,
        OutsidePrescriptionEditComponent,
        ItemDispatchEditChildComponent,
        OutsidePrescriptionPickComponent,
        MeasuringUnitListComponent,
        MeasuringUnitDetailComponent,
        MeasuringUnitEditComponent,
        MeasuringUnitPickComponent,
        BankListComponent,
        BankEditComponent,
        BankPickComponent,
        BankDetailComponent,
        BankSummaryComponent,
        OpdObservationListComponent,
        OpdObservationDetailComponent,
        BinCardComponent,
        PreAnsthesiaEvaluationEditListComponent,
        PreAnsthesiaEvaluationListComponent,
        PreAnsthesiaEvaluationDetailComponent,
        PreAnsthesiaEvaluationEditComponent,
        PreAnsthesiaEvaluationPickComponent,
        IcuPhysicalAssessmentListComponent,
        IcuPhysicalAssessmentDetailComponent,
        IcuPhysicalAssessmentEditComponent,
        IcuPhysicalAssessmentPickComponent,
        ProcedureTemplateEditComponent,
        ProcedureTemplateListComponent,
        ProcedureTemplatePickComponent,
        ProcedureTemplateDetailComponent,
        BehavioralPainScaleListComponent,
        BehavioralPainScaleDetailComponent,
        BehavioralPainScaleEditComponent,
        BehavioralPainScalePickComponent,
        OutsourcingCompanyEditComponent,
        OutsourcingCompanyListComponent,
        OutsourcingCompanyPickComponent,
        OutsourcingCompanyDetailComponent,
        CorelatedFeeListComponent,
        CorelatedFeeDetailComponent,
        CorelatedFeeEditComponent,
        CorelatedFeePickComponent,
        PosMachineListComponent,
        PosMachineDetailComponent,
        PosMachineEditComponent,
        PosMachinePickComponent,
        PosMachineUserListComponent,
        PosMachineUserDetailComponent,
        PosMachineUserEditComponent,
        PosMachineUserPickComponent,
        FlowSheetListComponent,
        FlowSheetDetailComponent,
        FlowSheetEditComponent,
        FlowSheetPickComponent,
        PatientHistoryDetailComponent,
        PatientHistoryEditListComponent,
        PatientHistoryEditComponent,
        PatientHistoryListComponent,
        PatientHistoryPickComponent,
        SessionTherapyListComponent,
        SessionTherapyDetailComponent,
        SessionTherapyEditComponent,
        SessionTherapyPickComponent,
        GroupMemberListComponent,
        GroupMemberDetailComponent,
        GroupMemberEditComponent,
        GroupMemberPickComponent,
        NotificationComponent,
        WardReportListComponent,
        WardReportDetailComponent,
        WardReportEditComponent,
        WardReportPickComponent,
        PatientWardReportListComponent,
        PatientWardReportDetailComponent,
        PatientWardReportEditComponent,
        PatientWardReportPickComponent,
        MachinesListComponent,
        MachinesDetailComponent,
        MachinesEditComponent,
        MachinesPickComponent,
        MachineTestListComponent,
        MachineTestDetailComponent,
        MachineTestEditComponent,
        MachineTestPickComponent,
        DailyReportListComponent,
        DailyReportDetailComponent,
        DailyReportEditComponent,
        DailyReportPickComponent,
        PsychoterapyProgressNoteListComponent,
        PsychoterapyProgressNoteDetailComponent,
        PsychoterapyProgressNoteEditComponent,
        PsychoterapyProgressNotePickComponent,
        QueuesListComponent,
        QueuesDetailComponent,
        QueuesEditComponent,
        QueuesPickComponent,
        CreditPaymentListComponent,
        CreditPaymentDetailComponent,
        CreditPaymentEditComponent,
        CreditPaymentPickComponent,
        CreditPaymentHistoryListComponent,
        CreditPaymentHistoryDetailComponent,
        CreditPaymentHistoryEditComponent,
        CreditPaymentHistoryPickComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        FormsModule,
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatToolbarModule,
        MatListModule,
        MatRippleModule,
        MatTableModule,
        MatCommonModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatSidenavModule,
        MatSnackBarModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatRadioModule,
        MatExpansionModule,
        MatSelectModule,
        MatTabsModule,
        MatTooltipModule,
        MatDatepickerModule,SlideMenuModule,
        MatChipsModule,
        MatNativeDateModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatSortModule,
        MatAutocompleteModule,
        PanelMenuModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        HttpClientModule,
        WebcamModule,
        AvatarModule,
        NgxChartsModule,
        FullCalendarModule,
        MatBadgeModule,
        NgSelectModule,
        NgxIntlTelInputModule,
        QueueBarModule,
        CKEditorModule,
        AngularSignaturePadModule,
        CalendarModule.forRoot({
          provide: DateAdapter,
          useFactory: adapterFactory
        }),
        ServiceWorkerModule.register('ngsw-worker.js', {
          enabled: environment.production,
          // Register the ServiceWorker as soon as the application is stable
          // or after 30 seconds (whichever comes first).
          registrationStrategy: 'registerWhenStable:30000'
        }),
        BedModule,
        TcModule
    ],
    providers: [

        httpInterceptorProviders,
        MenuState,
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
        {
            provide: DateAdapter,
            useClass: MomentDateAdapter,
            deps: [MAT_DATE_LOCALE],
        },
        { provide: MAT_DATE_FORMATS, useValue: TC_DATE_FORMATS },
        {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {disableClose: false}}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
