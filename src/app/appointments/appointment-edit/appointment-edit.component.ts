import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {DoctorSummary} from 'src/app/doctors/doctor.model';
import {DoctorNavigator} from 'src/app/doctors/doctor.navigator';
import {PatientSummary} from 'src/app/patients/patients.model';
import {PatientNavigator} from 'src/app/patients/patients.navigator';
import {PatientPersist} from 'src/app/patients/patients.persist';
import {UserPersist} from 'src/app/tc/users/user.persist';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {AppTranslation} from "../../app.translation";
import {TCAuthorization} from "../../tc/authorization";
import {TCEnum, TCModalModes, TCParentChildIds, TCWizardProgress, TCWizardProgressModes} from "../../tc/models";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppointmentDetail} from "../appointment.model";
import {AppointmentPersist} from "../appointment.persist";
import {DepartmentSummary} from "../../departments/department.model";
import {Department_SpecialtySummary} from "../../department_specialtys/department_specialty.model";
import {DepartmentNavigator} from "../../departments/department.navigator";
import {Department_SpecialtyNavigator} from "../../department_specialtys/department_specialty.navigator";
import {appointment_patient_status, appointment_status, appointment_type, physican_type, schedule_type, ServiceType} from "../../app.enums";
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { MedicalCheckupPackagePersist } from 'src/app/medical_checkup_package/medical_checkup_package.persist';
import { MedicalCheckupPackageNavigator } from 'src/app/medical_checkup_package/medical_checkup_package.navigator';
import { MedicalCheckupPackageDetail } from 'src/app/medical_checkup_package/medical_checkup_package.model';



@Component({
  selector: 'app-appointment-edit',
  templateUrl: './appointment-edit.component.html',
  styleUrls: ['./appointment-edit.component.css']
})
export class AppointmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  appointmentDetail: AppointmentDetail;
  start_time: string;
  patientFullName: string = "";
  userFullName: string = "";
  end_time: Date = new Date();
  departmentName: string = "";
  departmentSpeciality: string = "";
  serviceType = ServiceType;
  packageName: string;
  department: string;
  scheduleTypeList: TCEnum[] = []
  minDate:Date;


  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<AppointmentEditComponent>,
              public persist: AppointmentPersist,
              private patientNavigator: PatientNavigator,
              private userPersisit: UserPersist,
              private departmentNavigator: DepartmentNavigator,
              private departmentSpecialityNavigator: Department_SpecialtyNavigator,
              private departmentSpecialityPersist: Department_SpecialtyPersist,
              private doctorNavigator: DoctorNavigator,
              private patientPersist: PatientPersist,
              private tCUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public doctorPersist: DoctorPersist,
              public medicalPackageNavigator: MedicalCheckupPackageNavigator,
   

              @Inject(MAT_DIALOG_DATA) public ids: { id: string, patientId: string, department_id: string, doctor_id: string, mode: string, start_date: Date, canRepayment: boolean, followup_encounter: string}) {

                this.scheduleTypeList = this.persist.schedule_type;
                this.minDate = new Date();
  }


  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.appointmentDetail.scheduled_user_id = result[0].id;
      }
    });
  }

  getPatientName(patientId: string) {
    this.patientPersist.getPatient(patientId).subscribe((
        patient
      ) => {
        this.patientFullName = `${patient.fname} ${patient.mname} ${patient.lname}`;


      }
    )
  }

  getProviderName(provider_id: string) {
    this.doctorPersist.getDoctor(provider_id).subscribe((result)=>{
      if(result){
        this.userFullName = `${result.first_name} ${result.middle_name} ${result.last_name}`;
        this.getDepartment(result.department_speciality_id)
      }
    })
  }

  getDepartment(department_id: string, isParent: boolean = false){
    this.departmentSpecialityPersist.getDepartment_Specialty(department_id).subscribe((result)=>{
      if(result){
        if (!isParent){
          this.appointmentDetail.department_id = result.id;
          this.departmentSpeciality = result.name;
          this.getDepartment(result.parent_department_id, true)
        } else {
          this.appointmentDetail.parent_department_id = result.id;
          this.department = result.name;
        }
      }
    })
  }


  // we add physical type to search with exact physican type
  searchDoctor() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor, this.appointmentDetail.parent_department_id, this.appointmentDetail.parent_department_id == this.appointmentDetail.department_id ? this.tcUtilsString.invalid_id : this.appointmentDetail.department_id );
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.userFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.appointmentDetail.scheduler_user_id = result[0].id;
      }
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isWizard(): boolean {
    return this.ids.mode == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.ids.mode == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.mode == TCModalModes.EDIT;

  }

  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }

  ngOnInit() {

    if (this.isNew() || this.isWizard()){
      this.appointmentDetail = new AppointmentDetail();
      this.appointmentDetail.referred_from = "";
      this.appointmentDetail.referred_by = "";
      this.appointmentDetail.is_referal = false;
      this.appointmentDetail.is_result = false;

      this.appointmentDetail.is_package = false;
      this.appointmentDetail.patient_status = appointment_patient_status.alert;
      this.start_time = this.getStartTime(this.ids.start_date);
      this.appointmentDetail.scheduled_user_id = this.ids.patientId;
      this.ids.patientId && this.getPatientName(this.ids.patientId);
      this.appointmentDetail.status = appointment_status.pending;
      this.appointmentDetail.department_id = this.ids.department_id;
      this.ids.department_id && this.getDepartment(this.ids.department_id)
      this.appointmentDetail.scheduler_user_id = this.ids.doctor_id;
      this.ids.doctor_id && this.getProviderName(this.ids.doctor_id);
      this.appointmentDetail.followup_encounter = this.ids.followup_encounter
      if (this.ids.canRepayment){
        this.appointmentDetail.service_type = ServiceType.outpatient;
        this.appointmentDetail.schedule_type = schedule_type.follow_up;
        this.appointmentDetail.patient_status = appointment_patient_status.alert;
        this.scheduleTypeList = [...this.scheduleTypeList, new TCEnum(schedule_type.repayment, this.appTranslation.getKey("schedule", "repayment"))]
      }
    }
    if (this.isNew()) {
      this.tcAuthorization.requireCreate("appointments");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("patient", "waiting");
      //set necessary defaults

    } else if (this.isWizard()) {
      this.tcAuthorization.requireCreate("appointments");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("patient", "waiting");
    } else {
      this.tcAuthorization.requireUpdate("appointments");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("patient", "waiting");
      this.isLoadingResults = true;
      this.persist.getAppointment(this.ids.id).subscribe(appointmentDetail => {
        this.appointmentDetail = appointmentDetail;
        this.getProviderName(this.appointmentDetail.scheduler_user_id);
        this.getPatientName(this.appointmentDetail.scheduled_user_id);

        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addAppointment(this.appointmentDetail).subscribe(value => {
      this.tcNotification.success("Appointment added");
      this.appointmentDetail.id = value.id;
      this.dialogRef.close(this.appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  ngOnDestroy(): void {
    this.departmentSpecialityPersist.parent_department_id = null;
  }

  onContinue(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.addAppointment(this.appointmentDetail).subscribe(
      (value) => {
        this.tcNotification.success("Appointment added");
        this.dialogRef.close(
          new TCWizardProgress(TCWizardProgressModes.NEXT, value.id));
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }


  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateAppointment(this.appointmentDetail).subscribe(value => {
      this.tcNotification.success("Appointment updated");
      this.dialogRef.close(this.appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  canSubmit(): boolean {
    if (this.appointmentDetail == null) {
      return false;
    }


    // if (!this.appointmentDetail.schedule_type) {
    //   return false;
    // }
    if(this.appointmentDetail.service_type == null ||this.appointmentDetail.service_type == -1){
      return false;
    }
    if (this.appointmentDetail.scheduled_user_id == null) {
      return false;
    }

    if (this.appointmentDetail.scheduler_user_id == null) {
      return false;
    }
    if (this.appointmentDetail.patient_status == null||this.appointmentDetail.patient_status == -1) {
      return false;
    }

    if (this.appointmentDetail.is_package && !this.tcUtilsString.isValidId(this.appointmentDetail.package_id)){
      return false;
    }

    if (this.appointmentDetail.is_referal){
      if (!this.appointmentDetail.referred_from){
        return false;
      }
      if (!this.appointmentDetail.referred_by){
        return false;
      }
    }
    return true;
  }

  transformDates(todate: boolean = true) {
    this.appointmentDetail.service_type = parseInt(this.appointmentDetail.service_type.toString());
  
    if (todate) {
      this.appointmentDetail.schedule_start_time = new Date(this.start_time).getTime() / 1000;
      this.appointmentDetail.schedule_end_time = new Date(this.end_time).getTime() / 1000;

    } else {
      this.start_time=this.getStartTime(this.tCUtilsDate.toDate(this.appointmentDetail.schedule_start_time));
      this.end_time = this.tCUtilsDate.toDate(this.appointmentDetail.schedule_end_time);
    }
  }

  onServiceTypeChange():void{
    this.appointmentDetail.department_id=null;
    this.appointmentDetail.scheduler_user_id=null;
    this.userFullName=null;
    this.departmentSpeciality=null;
  }

  searchDepartment() {
    let dialogRef = this.departmentNavigator.pickDepartments(true);
    dialogRef.afterClosed().subscribe((result: DepartmentSummary[]) => {
      if (result) {
        // this.department_id = result[0].id;
        this.departmentName = result[0].name
      }
    });
  }

  onIsReferralChange():void{
    if(this.appointmentDetail.is_referal){
        this.appointmentDetail.referred_by = "";
        this.appointmentDetail.referred_from = "";
        this.appointmentDetail.is_package = false;
    }
  }

  onIsPackageChange():void{
    if(this.appointmentDetail.is_package){
        this.appointmentDetail.package_id = null;
        this.appointmentDetail.is_referal = false;
    }
  }

  searchDepartmentSpeciality(parent_id: string) {
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, parent_id);
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        if(parent_id == this.tcUtilsString.invalid_id){
        this.appointmentDetail.parent_department_id = result[0].id;
        this.appointmentDetail.department_id = result[0].id;
        this.department = result[0].name
        this.userFullName=null;
        this.appointmentDetail.scheduler_user_id=null;
      this.appointmentDetail.department_id = result[0].id;
        this.departmentSpeciality = null
        } else {
        this.appointmentDetail.department_id = result[0].id;
        this.departmentSpeciality = result[0].name
        this.userFullName=null;
        this.appointmentDetail.scheduler_user_id=null;
      }
      if (this.ids.canRepayment){
        this.appointmentDetail.schedule_type = this.ids.department_id != result[0].id ? schedule_type.repayment : schedule_type.repeat
        this.appointmentDetail.followup_encounter = this.ids.department_id != result[0].id ? this.tcUtilsString.invalid_id : this.ids.followup_encounter
      }
    }
    });
  }

  searchPackages(){
    let dialogRef = this.medicalPackageNavigator.pickMedicalCheckupPackages(true)
    dialogRef.afterClosed().subscribe(
      (data: MedicalCheckupPackageDetail[]) => {
        if(data){
          this.packageName = data[0].name;
          this.appointmentDetail.package_id = data[0].id;
        }
      }
    )
  }

}
