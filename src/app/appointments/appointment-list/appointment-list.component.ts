import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {AppointmentPersist} from "../appointment.persist";
import {AppointmentNavigator} from "../appointment.navigator";
import {
  AppointmentDetail,
  AppointmentSummary,
  AppointmentSummaryPartialList, PrefixedAppointmentSummary,
  PrefixedAppointmentSummaryPartialList
} from "../appointment.model";
import {PatientPersist} from 'src/app/patients/patients.persist';
import {
  PatientSummary,
  PatientSummaryPartialList,
  patientFilterColumn,
  getPrefixPatientName,
  prefixPatientFilterColumn
} from 'src/app/patients/patients.model';
import {UserPersist} from 'src/app/tc/users/user.persist';
import {DoctorPersist} from 'src/app/doctors/doctor.persist';
import {DoctorDetail, DoctorSummary, getPrefixPhysicianName} from 'src/app/doctors/doctor.model';
import {Form_EncounterNavigator} from 'src/app/form_encounters/form_encounter.navigator';
import {TCAuthentication} from "../../tc/authentication";
import {TCUtilsString} from 'src/app/tc/utils-string';
import {PaymentNavigator} from 'src/app/payments/payment.navigator';
import * as moment from "moment";
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { PaymentDetail } from 'src/app/payments/payment.model';
import { appointment_status, deposit_history_type, other_type, payment_status } from 'src/app/app.enums';
import { FeeSummary } from 'src/app/fees/fee.model';
import { physican_type } from 'src/app/app.enums';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { CalendarOptions, EventSourceInput } from '@fullcalendar/core';
import { Form_EncounterDetail } from 'src/app/form_encounters/form_encounter.model';
import { MatTabChangeEvent } from '@angular/material/tabs';
import {getPrefixedUserName, UserDetail} from "../../tc/users/user.model";


@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.css']
})
export class AppointmentListComponent implements OnInit, OnDestroy {
  events = [];
  appointmentFilterColumn = prefixPatientFilterColumn

  appointmentsData: PrefixedAppointmentSummary[] = [];
  appointmentsTotalCount: number = 0;
  appointmentsSelectAll: boolean = false;
  appointmentsSelection: AppointmentSummary[] = [];
  patients: PatientSummary[] = [];
  doctorSummary: DoctorSummary [] = [];
  appointmentStatus=appointment_status;
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    events:this.events,

    initialView: 'timeGridDay',
    // dateClick: this.addNewEvent.bind(this),
    datesSet: this.reloadData.bind(this),
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: false,
    dayMaxEvents: true,
    nowIndicator: true,
    // allDaySlot: false,
    slotDuration: '00:15:00',
    slotLabelInterval: 15,
    snapDuration: '00:30:00',
  };



  appointmentsDisplayedColumns: string[] = ["select", "action", 'patient.pid', "patient_fname", "schedule_start_time", "physician_first_name",
  "service_type",
  "slot_time","schedule_end_time", "status","patient_status","schedule_type", "et_user_name" ];
  appointmentsSearchTextBox: FormControl = new FormControl(this.appointmentPersist.appointmentSearchHistory.search_text);
  appointmentsIsLoading: boolean = false;
  dateTextField: FormControl = new FormControl({disabled:true , value:this.tcUtilsDate.toFormDate(this.appointmentPersist.appointmentSearchHistory.appoint_date)});
  other_type = other_type;
  paymentDetail:PaymentDetail;
  // patientFullName:string = "";
  // doctorFullName:string;
  doctors: {[id: string]: DoctorDetail} = {}
  users: {[id: string]: UserDetail} = {}
  tab_index: number = 0;
  startDate: Date;
  endDate: Date
  getPrefixPatientName = getPrefixPatientName;
  getPrefixPhysicianName = getPrefixPhysicianName;
  getPrefixedUserName = getPrefixedUserName;

  @ViewChild(MatPaginator, {static: true}) appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) appointmentsSort: MatSort;

  // calendarPlugins = [timeGridPlugin, interactionPlugin];

  constructor(private router: Router,
              private location: Location,
              public tcUtilString:TCUtilsString,
              public tcAuthorization: TCAuthorization,
              public tcAuthentication: TCAuthentication,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public doctorPersist: DoctorPersist,
              public appointmentPersist: AppointmentPersist,
              public appointmentNavigator: AppointmentNavigator,
              public paymentNavigator: PaymentNavigator,
              public patientPersist: PatientPersist,
              public userPersist: UserPersist,
              public jobPersist: JobPersist,
              public paymentPersist: PaymentPersist,
              private patientNavigator:PatientNavigator,
              private doctorNavigator:DoctorNavigator,
              public encounterNav: Form_EncounterNavigator,
              public persist:PaymentPersist,
  ) {


    this.tcAuthorization.requireRead("appointments");
    this.appointmentsSearchTextBox.setValue(appointmentPersist.appointmentSearchHistory.search_text);
    //delay subsequent keyup events
    this.appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.appointmentPersist.appointmentSearchHistory.search_text = value;
      this.searchAppointments();
    });

    // this.appointmentPersist.appointmentSearchHistory.schedule_start_date = Math.round(new Date().setHours(0, 0, 0) / 1000);
      // this.appointmentPersist.appointmentSearchHistory.schedule_end_date = this.appointmentPersist.appointmentSearchHistory.schedule_start_date + 24 * 60 * 60;

    this.dateTextField.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.appointmentPersist.appointmentSearchHistory.appoint_date = this.tcUtilsDate.FromFormToTimeStamp(value);
      this.searchAppointments();
    });

  }

  ngOnInit() {
      this.appointmentsSort.sortChange.subscribe(() => {
        this.appointmentsPaginator.pageIndex = 0;
        this.searchAppointments(true);
      });

      this.appointmentsPaginator.page.subscribe(() => {
        this.searchAppointments(true);
      });
      this.appointmentPersist.appointmentSearchHistory.isAll = false;
    this.searchAppointments();
  }


  headerInfo = {
    left: 'prev,next, today',
    center: 'title',
    right: 'timeGridWeek, timeGridDay'
  };

  getColorForEvent(event: PrefixedAppointmentSummary) {
    if (event.closed) {
      return "#545151";
    } else if (event.payment_payment_status == payment_status.paid) {
      return "#6eb950";
    } else if (event.payment_payment_status != payment_status.paid) {
      return "#c23c4c";
    }
    return "#175D91";
  }

  getPatientStatus(appointment: PrefixedAppointmentSummary){
    if (appointment.payment_payment_status != payment_status.paid ){
      return 'red'
    }else {
      return 'black'
    }
  }

  reloadData($event:any=null) {
    if($event){
      this.startDate = $event.view.currentStart;
      this.endDate = $event.view.currentEnd;
    }
    this.appointmentPersist.appointmentSearchHistory.isAll = true;
    this.appointmentPersist.appointmentSearchHistory.appoint_date = undefined;
    this.appointmentPersist.appointmentsForCalendar(this.tcUtilsDate.toTimeStamp(this.startDate), this.tcUtilsDate.toTimeStamp(this.endDate)).subscribe(
      (result: PrefixedAppointmentSummaryPartialList) => {
        this.appointmentsData = result.data
        this.loadCalendar(this.appointmentsData)
        // this.loadAdditional(this.appointmentsData, true)

      },
      error => {
        console.error(error);
      }
    )


  }


  loadCalendar(data: PrefixedAppointmentSummary[]){
    let newEvents:  EventSourceInput = [];
    for (const event of data) {
      const provider_name = getPrefixPhysicianName(event)
      newEvents.push(
        {
          title: `${provider_name}\n scheduled ${getPrefixPatientName(event)}`,
          start: this.tcUtilsDate.toDate(event.schedule_start_time),
          end: this.tcUtilsDate.toDate(event.schedule_end_time),
          id: event.id,
          color: this.getColorForEvent(event),
          description: "This is description",
          overlap: true
        }
      );

    }
    this.events = newEvents;


    this.calendarOptions = {
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      events:this.events,

      initialView: 'timeGridDay',
      // dateClick: this.addNewEvent.bind(this),
      datesSet: this.reloadData.bind(this),
      weekends: true,
      selectable: true,
      selectMirror: false,
      dayMaxEvents: true,
      nowIndicator: true,
      // allDaySlot: false,
      slotDuration: '00:15:00',
      slotLabelInterval: 15,
      snapDuration: '00:30:00',
    };
  }


  loadAdditional(data: PrefixedAppointmentSummary[], isCalendar = false) {
    if (!data.length){
      this.loadCalendar(data)
    }
    data.forEach((d, idx) => {
      if (!this.doctors[d.scheduler_user_id]){
        this.doctors[d.scheduler_user_id] = new DoctorDetail()
        this.doctorPersist.getDoctor(d.scheduler_user_id).subscribe(
          doctor => this.doctors[d.scheduler_user_id] = doctor
        )
      }
      this.paymentPersist.getPayment(d.card_payment_id).subscribe((payment) => {
        d.payment_status = payment.payment_status
        if(idx == data.length - 1 && isCalendar){
          this.loadCalendar(data)
        }
      })
    })
  }

  editEvent($event) {
    const appointment = new AppointmentDetail();
    appointment.id = $event.event.id;
    this.editAppointment(appointment);
  }

  addNewEvent($event) {
    this.addAppointment(new Date($event.dateStr));
  }

  searchAppointments(isPagination: boolean = false): void {
    if(this.tab_index){
      return this.reloadData()
    }
    let paginator = this.appointmentsPaginator;
    let sorter = this.appointmentsSort;

    this.appointmentsIsLoading = true;
    this.appointmentsSelection = [];

    this.appointmentPersist.searchAppointment(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: PrefixedAppointmentSummaryPartialList) => {
      this.appointmentsData = partialList.data;

      // this.loadAdditional(this.appointmentsData)

      if (partialList.total != -1) {
        this.appointmentsTotalCount = partialList.total;
      }
      this.appointmentsIsLoading = false;
    }, error => {
      this.appointmentsIsLoading = false;
    });

  }

  downloadAppointments(): void {
    if (this.appointmentsSelectAll) {
      this.appointmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download appointments", true);
      });
    } else {
      this.appointmentPersist.download(this.tcUtilsArray.idsList(this.appointmentsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download appointments", true);
      });
    }
  }


  addAppointment(startDate?: Date): void {
    let dialogRef = this.appointmentNavigator.addAppointment(null, null, null, startDate);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAppointments();
      }
    });
  }

  editAppointment(item: AppointmentSummary) {
    let dialogRef = this.appointmentNavigator.editAppointment(item.id, null);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteAppointment(item: AppointmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Appointment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.appointmentPersist.deleteAppointment(item.id).subscribe(response => {
          this.tcNotification.success("Appointment deleted");
          this.searchAppointments();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

  updateStatusToClosed(appintment: AppointmentSummary): void {
    this.appointmentPersist.appointmentDo(appintment.id, "update_status_to_closed", {}).subscribe(
      res => {
        this.tcNotification.success("Sucessfully updated");
        this.searchAppointments();
      }
    )
  }

  createEncounter(appointment: PrefixedAppointmentSummary) {
    this.appointmentsIsLoading = true;
    if (appointment.card_payment_id != this.tcUtilsString.invalid_id){
      // this.paymentPersist.getPayment(appointment.card_payment_id).subscribe(
      //   data => {
          if (appointment.payment_payment_status != payment_status.paid){
            this.tcNotification.error("Card Fee is not paid")
            this.appointmentsIsLoading = false
            return
          }
          // else if(appointment.is_package){
          //   this.paymentPersist.getPayment(appointment.package_payment_id).subscribe(
          //     packagePayment => {
          //       if (packagePayment.payment_status != payment_status.paid){
          //         this.tcNotification.error("Package Fee is not paid")
          //         this.appointmentsIsLoading = false
          //         return;
          //       } else {
          //         this.appointmentsIsLoading = false
          //         return this.addEncounter(appointment);
          //       }
          //     }
          //   )
          // }
           else {
            this.appointmentsIsLoading = false
         return this.addEncounter(appointment);
          }
      //   }
      // )
    }else {
      this.paymentPersist.getPayment(appointment.package_payment_id).subscribe(
        packagePament => {
          if (packagePament.payment_status != payment_status.paid){
            this.appointmentsIsLoading = false
            this.tcNotification.error("Package Fee is not paid")
            return;
          } else {
            this.appointmentsIsLoading = false
            return this.addEncounter(appointment);
          }
        }
      )
    }
  }

  addEncounter(appointment: AppointmentSummary){
    let dialogRef = this.encounterNav.addForm_Encounter(true, appointment);
    dialogRef.afterClosed().subscribe(
      (encounter: Form_EncounterDetail) => {


        this.encounterNav.viewForm_Encounter(encounter.id);
      }
      ,
      err => {

      }
    )
  }

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary[]) => {
      if (result) {
        this.appointmentPersist.appointmentSearchHistory.patientFullName = result[0].fname + ' ' + result[0].mname + result[0].lname;
        this.appointmentPersist.appointmentSearchHistory.patientId = result[0].id;
        this.searchAppointments();
      }
    });
  }


  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(false, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.appointmentPersist.appointmentSearchHistory.doctorFullName = result.map((value) => `${value.first_name} ${value.middle_name} ${value.last_name}`).join(' , ');
        this.appointmentPersist.appointmentSearchHistory.doctorId = result.map((value) => value.id).toString();
        this.searchAppointments();
      }
    });
  }


  ngOnDestroy(): void {
    // this.appointmentPersist.clearFilters();
  }

  payScheduleFee(appointment: AppointmentDetail) {
    let dialogRef = this.paymentNavigator.addAppointmentPayment(appointment.id);


  }

  onTabChanged(event: MatTabChangeEvent){
    if (! event.index){
      this.appointmentPersist.appointmentSearchHistory.isAll = false;
      this.appointmentPersist.appointmentSearchHistory.schedule_start_date = undefined;
      this.appointmentPersist.appointmentSearchHistory.schedule_end_date = undefined;
      this.appointmentPersist.appointmentSearchHistory.appoint_date = this.tcUtilsDate.toTimestamp()
      this.searchAppointments()
    }
  }

  getDoctor(id: string){
    if(this.doctors[id]){
      const {first_name, middle_name, last_name} = this.doctors[id]
    return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
    }
  }

  getUser(id: string){
    if(this.users[id]){
      const {name} = this.users[id]
      return name ? name : ""
    }
  }
}
