import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AppointmentPickComponent } from './appointment-pick.component';

describe('AppointmentPickComponent', () => {
  let component: AppointmentPickComponent;
  let fixture: ComponentFixture<AppointmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AppointmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppointmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
