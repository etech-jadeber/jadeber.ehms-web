import { PrefixedDoctor } from "../doctors/doctor.model";
import { PrefixedPatient } from "../patients/patients.model";
import { PrefixedPayment } from "../payments/payment.model";
import {TCId} from "../tc/models";
import { PrefixedUser } from "../tc/users/user.model";

export type PrefixedAppointmentSummary = PrefixedPatient & PrefixedDoctor & PrefixedUser & PrefixedPayment & AppointmentSummary;
export type PrefixedTriageSummary = PrefixedPatient & PrefixedDoctor & PrefixedUser & PrefixedPayment & AppointmentSummary;

export class AppointmentSummary extends TCId {
  schedule_type : number;
scheduled_user_id : string;
service_type:number;
scheduler_user_id : string;
schedule_start_time : number;
schedule_end_time : number;
status : number;
encounter : string;
patient_status : number;
departmentType:number;
paid:boolean;
closed: boolean;
referred_from: string;
referred_by: string;
department_id: string;
is_referal : boolean;
is_package: boolean;
card_payment_id: string;
package_payment_id: string;
package_id: string;
parent_department_id: string
provider_name: string;
patient_name: string;
payment_status: number;
followup_encounter: string;
is_result:boolean;

}

export class AppointmentSummaryPartialList {
  data: AppointmentSummary[];
  total: number;
}

export class PrefixedAppointmentSummaryPartialList {
  data: PrefixedAppointmentSummary[];
  total: number;
}

export class AppointmentDetail extends AppointmentSummary {
  schedule_type : number;
scheduled_user_id : string;
scheduler_user_id : string;
schedule_start_time : number;
schedule_end_time : number;
status : number;
encounter : string;
patient_status : number;
closed: boolean;
referred_from: string;
referred_by: string;

}

export class AppointmentDashboard {
  total: number;
}
