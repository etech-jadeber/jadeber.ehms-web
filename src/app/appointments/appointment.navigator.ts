import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCModalModes, TCParentChildIds} from "../tc/models";

import {AppointmentEditComponent} from "./appointment-edit/appointment-edit.component";
import {AppointmentPickComponent} from "./appointment-pick/appointment-pick.component";
import { TCUtilsString } from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})

export class AppointmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog,
              public tcUtilsString: TCUtilsString,) {
  }

  appointmentsUrl(): string {
    return "/appointments";
  }

  appointmentUrl(id: string): string {
    return "/appointments/" + id;
  }

  viewAppointments(): void {
    this.router.navigateByUrl(this.appointmentsUrl());
  }

  viewAppointment(id: string): void {
    this.router.navigateByUrl(this.appointmentUrl(id));
  }

  editAppointment(id: string, patientId: string, department_id: string = null, doctor_id: string = null, mode: string = TCModalModes.EDIT, start_date: Date = new Date(), canRepayment: boolean = false, followup_encounter: string = this.tcUtilsString.invalid_id): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AppointmentEditComponent, {
      data: {id, patientId, department_id, doctor_id, mode, start_date, canRepayment, followup_encounter},
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }




  
  addAppointment(patientId: string, department_id: string = null, doctor_id = null, start_date: Date = new Date(), canRepayment: boolean = false, followup_encounter: string = this.tcUtilsString.invalid_id): MatDialogRef<unknown,any> {
    return this.editAppointment(null, patientId, department_id, doctor_id, TCModalModes.NEW, start_date, canRepayment, followup_encounter);
  }

  wizardAppointment(patientId: string, department_id: string = null, doctor_id = null, start_date: Date = new Date()): MatDialogRef<unknown,any> {
    return this.editAppointment(null, patientId, department_id, doctor_id, TCModalModes.WIZARD, start_date);
  }

  pickAppointments(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AppointmentPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
