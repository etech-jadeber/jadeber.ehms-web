import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TCEnum, TCEnumTranslation, TCId} from "../tc/models";
import {AppointmentDashboard, AppointmentDetail, AppointmentSummary, AppointmentSummaryPartialList} from "./appointment.model";
import {appointment_patient_status, appointment_status, appointment_type, schedule_type, ServiceType} from '../app.enums';
import {TCUtilsString} from '../tc/utils-string';
import {AppTranslation} from '../app.translation';
import {TCUtilsDate} from "../tc/utils-date";
import { PaymentDetail } from '../payments/payment.model';


@Injectable({
  providedIn: 'root'
})
export class AppointmentPersist {
  // appointmentSearchText: string = "";
  // schedule_start_date: number;
  // schedule_end_date: number;
  // schedule_typeId: number;
  closed: number = 1;
  prevAppointmentDetails: AppointmentDetail;
  // appointment_typeId: number;
  // doctorsId:string[] = [];
  // patientId:string = "";

  // appointment_patient_statuId: number ;
  // appointment_statuId: number ;
  // isAll: boolean = false;

  serviceType: TCEnumTranslation[] = [
    new TCEnumTranslation(ServiceType.outpatient, 'OutPatient'),
    new TCEnumTranslation(ServiceType.emergency, 'Emergency'),

    // new TCEnumTranslation(ServiceType.referral, 'Refferal')
  ];



  schedule_type: TCEnum[] = [
    new TCEnum(schedule_type.new_patient, this.appTranslation.getKey("schedule", "new_patient")),
    new TCEnum(schedule_type.revisit, this.appTranslation.getKey("schedule", "revisit")),
    // new TCEnum(schedule_type.repayment, this.appTranslation.getKey("schedule", "repayment")),
    new TCEnum(schedule_type.repeat, this.appTranslation.getKey("schedule", "repeat")),
    new TCEnum(schedule_type.follow_up, this.appTranslation.getKey("schedule", "follow_up")),
  ];

  appointment_type: TCEnum[] = [
    new TCEnum(appointment_type.in_patient, 'in_patient'),
    new TCEnum(appointment_type.out_patient, 'out_patient'),
    new TCEnum(appointment_type.emergency, 'emergency'),
  ];

  constructor(
    private http: HttpClient,
    private appTranslation: AppTranslation,
    private tcDateUtil: TCUtilsDate
  ) {
  }

  appointmentSearchHistory = {
      "schedule_start_date": undefined,
      "schedule_end_date": undefined,
      "schedule_type": undefined,
      "appointment_patient_statuId": undefined,
      "appointment_statuId": undefined,
      "doctorId": "",
      "patientId": "",
      "isAll": false,
      "search_text": "",
      "search_column": "",
      "patientFullName": "",
      "doctorFullName": "",
      "appoint_date": this.tcDateUtil.toTimestamp(),
  }

  defaultAppointmentSearchHistory = {...this.appointmentSearchHistory}

  resetAppointmentSearchHistory(){
    this.appointmentSearchHistory = {...this.defaultAppointmentSearchHistory}
  }

  triageSearchHistory = {...this.appointmentSearchHistory, service_type: undefined, vital_status: false, payment_status: undefined}
  defaultTriageSearchHistory = {...this.triageSearchHistory}

  resetTriageSearchHistory(){
    this.triageSearchHistory = {...this.defaultTriageSearchHistory}
  }

  appointmentsForCalendar(startTimeStamp: number, endTimeStamp: number, searchHistory = this.appointmentSearchHistory): Observable<AppointmentSummaryPartialList> {
    let url = environment.tcApiBaseUri + "appointments";
    const newSearchHistory = {...searchHistory,"schedule_start_date": startTimeStamp.toString(), "schedule_end_date": endTimeStamp.toString() }
    url = TCUtilsString.appendUrlParameters(url, newSearchHistory);
    // url = TCUtilsString.appendUrlParameter(url, "isAll", 'true')
    // if (this.schedule_typeId) {
    //   url = TCUtilsString.appendUrlParameter(url, "schedule_type", this.schedule_typeId.toString());
    // }

    // if (this.appointment_patient_statuId) {
    //   url = TCUtilsString.appendUrlParameter(url, "appointment_patient_statuId", this.appointment_patient_statuId.toString());
    // }

    // if (this.appointment_statuId) {
    //   url = TCUtilsString.appendUrlParameter(url, "appointment_statuId", this.appointment_statuId.toString());
    // }

    // if (this.doctorsId) {
    //   url = TCUtilsString.appendUrlParameter(url, "doctorId", this.doctorsId.toString());
    // }

    // if (this.patientId) {
    //   url = TCUtilsString.appendUrlParameter(url, "patientId", this.patientId.toString());
    // }
    return this.http.get<AppointmentSummaryPartialList>(url);
  }

  searchAppointment(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.appointmentSearchHistory): Observable<AppointmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchWithColumnUrl("appointments", searchHistory.search_text, pageSize, pageIndex, sort, order, searchHistory.search_column);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);

    // if (this.schedule_start_date) {
    //   url = TCUtilsString.appendUrlParameter(url, "schedule_start_date", this.schedule_start_date.toString());
    // }
    // if (this.schedule_end_date) {
    //   url = TCUtilsString.appendUrlParameter(url, "schedule_end_date", this.schedule_end_date.toString());
    // }
    // if (this.schedule_typeId) {
    //   url = TCUtilsString.appendUrlParameter(url, "schedule_type", this.schedule_typeId.toString());
    // }

    // if (this.appointment_patient_statuId) {
    //   url = TCUtilsString.appendUrlParameter(url, "appointment_patient_statuId", this.appointment_patient_statuId.toString());
    // }

    // if (this.appointment_statuId) {
    //   url = TCUtilsString.appendUrlParameter(url, "appointment_statuId", this.appointment_statuId.toString());
    // }

    // if (this.doctorsId) {
    //   url = TCUtilsString.appendUrlParameter(url, "doctorId", this.doctorsId.toString());
    // }

    // if (this.patientId) {
    //   url = TCUtilsString.appendUrlParameter(url, "patientId", this.patientId.toString());
    // }
    // url = TCUtilsString.appendUrlParameter(url, "isAll", this.isAll.toString());
    return this.http.get<AppointmentSummaryPartialList>(url);

  }

  filters(searchHistory = this.appointmentSearchHistory): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "appointments/do", new TCDoParam("download_all", this.filters()));
  }

  appointmentDashboard(): Observable<AppointmentDashboard> {
    return this.http.get<AppointmentDashboard>(environment.tcApiBaseUri + "appointments/dashboard");
  }

  getAppointment(id: string): Observable<AppointmentDetail> {
    return this.http.get<AppointmentDetail>(environment.tcApiBaseUri + "appointments/" + id);
  }

  addAppointment(item: AppointmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "appointments/", item);
  }

  proceedAppointment(): Observable<TCId> {
    let url = environment.tcApiBaseUri + "appointments/";
    url = TCUtilsString.appendUrlParameter(url, "type", "proceed");
    return this.http.post<TCId>(url, this.prevAppointmentDetails);
  }

  updateAppointment(item: AppointmentDetail): Observable<AppointmentDetail> {
    return this.http.patch<AppointmentDetail>(environment.tcApiBaseUri + "appointments/" + item.id, item);
  }

  deleteAppointment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "appointments/" + id);
  }

  appointmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "appointments/do", new TCDoParam(method, payload));
  }

  appointmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "appointments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "appointments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "appointments/" + id + "/do", new TCDoParam("print", {}));
  }

  // clearFilters() {
  //   this.schedule_typeId = null;
  //   this.appointmentSearchText = "";
  //   this.schedule_typeId = null;
  //   this.closed = null;
  // }



  appointment_patient_status: TCEnumTranslation[] = [
  new TCEnumTranslation(appointment_patient_status.alert, this.appTranslation.getKey('patient', 'alert')),
  new TCEnumTranslation(appointment_patient_status.verbal, this.appTranslation.getKey('patient', 'verbal')),
  new TCEnumTranslation(appointment_patient_status.pain, this.appTranslation.getKey('patient', 'pain')),
  new TCEnumTranslation(appointment_patient_status.uncouncious, this.appTranslation.getKey('patient', 'unconscious')),
];



appointment_status: TCEnumTranslation[] = [
  new TCEnumTranslation(appointment_status.pending, this.appTranslation.getKey('patient', 'pending')),
  new TCEnumTranslation(appointment_status.started, this.appTranslation.getKey('patient',"visiting")),
  new TCEnumTranslation(appointment_status.no_show_up, this.appTranslation.getKey('patient', 'no_show_up')),
  new TCEnumTranslation(appointment_status.patient_cancelled, this.appTranslation.getKey('schedule', 'patient_cancelled')),
  new TCEnumTranslation(appointment_status.doctor_cancelled, this.appTranslation.getKey('schedule', 'doctor_cancelled')),
  new TCEnumTranslation(appointment_status.appointment_ended, this.appTranslation.getKey('patient','appointment_ended')),
];


}
