import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AvailabilityCalenderViewComponent } from './availability-calender-view.component';

describe('AvailabilityCalenderViewComponent', () => {
  let component: AvailabilityCalenderViewComponent;
  let fixture: ComponentFixture<AvailabilityCalenderViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityCalenderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityCalenderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
