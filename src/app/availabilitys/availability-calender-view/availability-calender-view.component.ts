import {Component, OnInit, ViewChild} from '@angular/core';
import {Calendar, CalendarOptions, DatesSetArg, EventSourceInput} from '@fullcalendar/core';
import {TCAuthorization} from "../../tc/authorization";
import {AppTranslation} from "../../app.translation";
import {DoctorDetail, DoctorSummary} from "../../doctors/doctor.model";
import {DoctorNavigator} from "../../doctors/doctor.navigator";
import {TCAuthentication} from "../../tc/authentication";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import {AvailabilityPersist} from "../availability.persist";
import {physican_type, repetition} from "../../app.enums";
import {AvailabilityDetail} from "../availability.model";
import {TCUtilsDate} from "../../tc/utils-date";
import * as moment from 'moment';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-availability-calender-view',
  templateUrl: './availability-calender-view.component.html',
  styleUrls: ['./availability-calender-view.component.css']
})
export class AvailabilityCalenderViewComponent implements OnInit {
  userFullName: string = "";

  events = [];

  headerInfo = {
    left: 'prev,next, today',
    center: 'title',
    right: 'timeGridWeek, timeGridDay'
  };

  eventColor = ["#175D91", "#3f47aa", "#179188", '#917281'];

  calendarPlugins = [timeGridPlugin, interactionPlugin];
  doctorsTotalCount: number;

  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    events:this.events,

    initialView: 'timeGridDay',
    datesSet: this.reloadData.bind(this),
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: false,
    dayMaxEvents: true,
    nowIndicator: true,
    allDaySlot: false,
    
  };

  @ViewChild(MatPaginator, {static: true}) availabilityPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) availabilitysSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public appTranslation: AppTranslation,
              public tcUtilsDate: TCUtilsDate,
              public availabilityPersist: AvailabilityPersist,
              public tcAuthentication: TCAuthentication,
              private doctorNavigator: DoctorNavigator,
              private doctorPersist: DoctorPersist,
              public tcUtilsArray: TCUtilsArray) {
    this.tcAuthorization.requireRead("availabilitys");
    const name = Calendar.name
  }

  getDoctorAvailability(isPagination = false) {
    // this.availabilityPersist.userId = doctor.id;
    const paginator = this.availabilityPaginator;
    this.availabilityPersist.searchAvailability(paginator.pageSize ,isPagination ? paginator.pageIndex : 0 , "doctor_name", "asc").subscribe(
      result => {
        this.events = [];
        const availability = result.data;
        if(result.total != -1){
          this.doctorsTotalCount = result.total;
        }
        for (const availabilitySummary of availability) {
          this.addAvailabilityToEven(availabilitySummary);
        }
        this.calendarOptions.events = this.events;
      },
      error => {
        console.error(error);
      }
    )
  }

  reloadData(dates: DatesSetArg){
    this.availabilityPersist.start_date = dates.start.getTime() / 1000
    this.availabilityPersist.end_date = dates.end.getTime() / 1000
    this.getDoctorAvailability()
  }

  addAvailabilityToEven(availability: AvailabilityDetail) {
    switch (availability.repetition) {
      case repetition.once:
        let dateNow = new Date(new Date(availability.date*1000))
        let oneDate = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate())
        this.events = [...this.events, {
          title: availability.doctor_name,
          startTime: availability.start_time,
          endTime: availability.end_time,
          startRecur: oneDate,
          endRecur: new Date(new Date(oneDate).setDate(new Date(oneDate).getDate() + 1)), 
          color: this.eventColor[0]
        }];
        break;
      case repetition.every_day:
        this.events = [...this.events, {
          title: availability.doctor_name,
          startTime: availability.start_time,
          endTime: availability.end_time,
          startRecur: new Date(availability.start_date),
          endRecur: new Date(availability.end_date).setDate(new Date(availability.end_date).getDate() + 1),
          color: this.eventColor[1]
        }];
        break;
      case repetition.once_a_week:
        this.events = [...this.events, {
          title: availability.doctor_name,
          daysOfWeek: [availability.day_of_the_week - 99],
          startTime: availability.start_time,
          endTime: availability.end_time,
          startRecur: new Date(availability.start_date),
          endRecur: new Date(availability.end_date).setDate(new Date(availability.end_date).getDate() + 1),
          color: this.eventColor[2]
        }];
        break;
      case repetition.once_a_month:
        //TODO: this approach has a known issue: more exploration is needed https://fullcalendar.io/docs/v4/rrule-plugin
        let date: Date = new Date(new Date(this.availabilityPersist.start_date * 1000).setDate(availability.day_of_the_month));
        const noMonths = Math.abs(new Date(this.availabilityPersist.end_date * 1000).getMonth() - new Date(this.availabilityPersist.start_date * 1000).getMonth())
        for (let i=0; i <= noMonths; i++){
          this.events = [...this.events, {
            title: availability.doctor_name,
            startTime: availability.start_time,
            endTime: availability.end_time,
            startRecur: date,
            endRecur: new Date(date).setDate(date.getDate() + 1),
            color: this.eventColor[3],
          }];
          date = new Date(new Date(date).setMonth(date.getMonth() + 1))
        }
        break;
    }
  }

  ngOnInit() {
    
    this.availabilityPaginator.page.subscribe(() => {
      this.getDoctorAvailability(true)
    });
    this.availabilityPaginator.pageSize = 5;
    this.availabilityPersist.start_date = Math.round(new Date().getTime() / 1000);
    this.getDoctorAvailability()
  }

  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.events = []
        this.userFullName = result[0].first_name + " " + result[0].last_name
        this.availabilityPersist.userId = result[0].id;
        this.getDoctorAvailability()
      }
    });
  }

  ngOnDestroy(){
    this.availabilityPersist.start_date = null;
    this.availabilityPersist.end_date = null;
  }
}
