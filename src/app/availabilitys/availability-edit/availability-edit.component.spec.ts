import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AvailabilityEditComponent } from './availability-edit.component';

describe('AvailabilityEditComponent', () => {
  let component: AvailabilityEditComponent;
  let fixture: ComponentFixture<AvailabilityEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
