import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {AvailabilityDetail} from "../availability.model";
import {AvailabilityPersist} from "../availability.persist";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {day_of_the_week, repetition} from 'src/app/app.enums';
import {UserSummary} from 'src/app/tc/users/user.model';
import {UserNavigator} from 'src/app/tc/users/user.navigator';
import {DoctorNavigator} from "../../doctors/doctor.navigator";
import {DoctorSummary} from "../../doctors/doctor.model";


@Component({
  selector: 'app-availability-edit',
  templateUrl: './availability-edit.component.html',
  styleUrls: ['./availability-edit.component.css']
})
export class AvailabilityEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  userFullName: string = "";
  title: string;
  availabilityDetail: AvailabilityDetail;
  date: Date = new Date();
  start_time: string;
  end_time:  string;
  repetition = repetition;
  day_of_the_week = day_of_the_week;
  start_date: Date =  new Date();
  end_date: Date = new Date();

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              private tCUtilsDate: TCUtilsDate,
              private doctorNavigator: DoctorNavigator,
              public dialogRef: MatDialogRef<AvailabilityEditComponent>,
              public persist: AvailabilityPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("availabilitys");
      this.title = this.appTranslation.getText("general", "new") + ' ' + this.appTranslation.getText('schedule', 'availability');
      this.availabilityDetail = new AvailabilityDetail();
      this.availabilityDetail.repetition = -1;
      this.availabilityDetail.day_of_the_month = -1;
      this.availabilityDetail.day_of_the_week = -1;
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("availabilitys");
      this.title = this.appTranslation.getText("general", "edit") + ' ' + this.appTranslation.getText('schedule', 'availability')
      ;
      this.isLoadingResults = true;
      this.persist.getAvailability(this.idMode.id).subscribe(availabilityDetail => {
        this.availabilityDetail = availabilityDetail;
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addAvailability(this.availabilityDetail).subscribe(value => {
      this.tcNotification.success("Availability added");
      this.availabilityDetail.id = value.id;
      this.dialogRef.close(this.availabilityDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateAvailability(this.availabilityDetail).subscribe(value => {
      this.tcNotification.success("Availability updated");
      this.dialogRef.close(this.availabilityDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.availabilityDetail == null) {
      return false;
    }

    if (this.availabilityDetail.user_id   == null
      ) {
       return false;
     }


    if (this.start_time   == null
      ) {
       return false;
     }


    if (this.end_time   == null
      ) {
       return false;
     }
     if (this.end_time <= this.start_time){
      return false;
     }
    if (this.availabilityDetail.repetition   == -1
      ) {
       return false;
     }

    
    if (this.availabilityDetail.repetition  < 100 && this.availabilityDetail.repetition > 103
     ) {
      return false;
    }
    if (this.availabilityDetail.repetition == repetition.once_a_month && 
      (this.availabilityDetail.day_of_the_month < 1 || this.availabilityDetail.day_of_the_month > 30)) {
      return false;
    }
    if (this.availabilityDetail.repetition == repetition.once_a_week && 
      (this.availabilityDetail.day_of_the_week < 100 || this.availabilityDetail.day_of_the_week > 106)) {
      return false;
    }

    return true;
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      let ed = new Date();
      let end_times:string[] = this.end_time.split(":");
      ed.setHours(parseInt( end_times[0]))
      ed.setMinutes(parseInt( end_times[1]))
      let sd = new Date();
      let start_times:string[] = this.start_time.split(":");
      sd.setHours(parseInt( start_times[0]))
      sd.setMinutes(parseInt( start_times[1]))

      this.availabilityDetail.date = this.tCUtilsDate.toTimeStamp(this.date);
      this.availabilityDetail.end_time = this.tCUtilsDate.toTimeStamp(ed);
      this.availabilityDetail.start_time = this.tCUtilsDate.toTimeStamp(sd);
      
      this.availabilityDetail.end_date = this.tCUtilsDate.toTimeStamp(this.end_date);
      this.availabilityDetail.start_date = this.tCUtilsDate.toTimeStamp(this.start_date);
    
    } else {

    }
  }


  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.userFullName = `${result[0].first_name} ${result[0].last_name}`;
        this.availabilityDetail.user_id = result[0].id;
      }
    });
  }
}
