import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {AvailabilityPersist} from "../availability.persist";
import {AvailabilityNavigator} from "../availability.navigator";
import {AvailabilitySummary, AvailabilitySummaryPartialList} from "../availability.model";
import {repetition} from 'src/app/app.enums';
import {DoctorDetail} from "../../doctors/doctor.model";
import {DoctorPersist} from "../../doctors/doctor.persist";
import { Cancelled_AppointmentNavigator } from 'src/app/cancelled_appointments/cancelled_appointment.navigator';
import { AppointmentSummary } from 'src/app/appointments/appointment.model';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { AvailabilityCalenderViewComponent } from '../availability-calender-view/availability-calender-view.component';


@Component({
  selector: 'app-availability-list',
  templateUrl: './availability-list.component.html',
  styleUrls: ['./availability-list.component.css']
})
export class AvailabilityListComponent implements OnInit {

  availabilitysData: AvailabilitySummary[] = [];
  availabilitysTotalCount: number = 0;
  availabilitysSelectAll: boolean = false;
  availabilitysSelection: AvailabilitySummary[] = [];
  repetition = repetition;

  availabilitysDisplayedColumns: string[] = ["select", "action", "user_id", "start_time", "end_time", "day_of_the_week", "day_of_the_month", "date", "repetition","start_date","end_date","max_patient"];
  availabilitysSearchTextBox: FormControl = new FormControl();
  availabilitysIsLoading: boolean = false;
  doctors: DoctorDetail[] = [];
  showAvailabilityCalender: boolean = true;
  selectedTabIndex: number;

  @ViewChild(MatPaginator, {static: true}) availabilitysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) availabilitysSort: MatSort;
  @ViewChild('calendar') calendar_view: AvailabilityCalenderViewComponent;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              private cancelled_appointmentNavigator:Cancelled_AppointmentNavigator,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public availabilityPersist: AvailabilityPersist,
              public availabilityNavigator: AvailabilityNavigator,
              public doctorsPersist: DoctorPersist,
              public jobPersist: JobPersist,
  ) {

    this.tcAuthorization.requireRead("availabilitys");
    this.availabilitysSearchTextBox.setValue(availabilityPersist.availabilitySearchText);
    //delay subsequent keyup events
    this.availabilitysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.availabilityPersist.availabilitySearchText = value;
      this.searchAvailabilitys();
    });
  }

  ngOnInit() {
    this.availabilitysSort.sortChange.subscribe(() => {
      this.availabilitysPaginator.pageIndex = 0;
      this.searchAvailabilitys(true);
    });

    this.availabilitysPaginator.page.subscribe(() => {
      this.searchAvailabilitys(true);
    });
    //start by loading items
    this.searchAvailabilitys();

  }

  ngOnDestroy(){
    this.selectedTabIndex = 0;
  }

  searchAvailabilitys(isPagination: boolean = false): void {
    if (this.selectedTabIndex == 1){
      this.calendar_view.getDoctorAvailability()
      return;
    }

    let paginator = this.availabilitysPaginator;
    let sorter = this.availabilitysSort;

    this.availabilitysIsLoading = true;
    this.availabilitysSelection = [];

    this.availabilityPersist.searchAvailability(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: AvailabilitySummaryPartialList) => {
      this.availabilitysData = partialList.data;

      this.getDoctorsDetail(partialList.data);

      if (partialList.total != -1) {
        this.availabilitysTotalCount = partialList.total;
      }
      this.availabilitysIsLoading = false;
    }, error => {
      this.availabilitysIsLoading = false;
    });

  }

  getDoctorsDetail(partialList: AvailabilitySummary[]) {
    for (let av of partialList) {
      this.doctorsPersist.getDoctor(av.user_id).subscribe(
        result => {
          this.doctors = [...this.doctors, result]
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  downloadAvailabilitys(): void {
    if (this.availabilitysSelectAll) {
      this.availabilityPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download availabilitys", true);
      });
    } else {
      this.availabilityPersist.download(this.tcUtilsArray.idsList(this.availabilitysSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download availabilitys", true);
      });
    }
  }


  addAvailability(): void {
    let dialogRef = this.availabilityNavigator.addAvailability();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAvailabilitys();
      }
    });
  }

  editAvailability(item: AvailabilitySummary) {
    let dialogRef = this.availabilityNavigator.editAvailability(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteAvailability(item: AvailabilitySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Availability");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.availabilityPersist.deleteAvailability(item.id).subscribe(response => {
          this.tcNotification.success("Availability deleted");
          this.searchAvailabilitys();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }


  getDoctorFullName(doctorId: string) {
    const doctor = this.tcUtilsArray.getById(this.doctors, doctorId);
    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`;
    }
    return "";
  }

  addCancelled_Appointment(appointment:AppointmentSummary): void {
    let dialogRef = this.cancelled_appointmentNavigator.addCancelled_Appointment(appointment.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAvailabilitys();
      }
    });
  }

  updateState(event: MatTabChangeEvent){
    this.availabilityPersist.userId = null;
    this.selectedTabIndex = event.index
    // this.searchAvailabilitys()
  }

}
