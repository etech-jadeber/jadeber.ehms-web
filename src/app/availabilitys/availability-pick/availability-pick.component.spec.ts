import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AvailabilityPickComponent } from './availability-pick.component';

describe('AvailabilityPickComponent', () => {
  let component: AvailabilityPickComponent;
  let fixture: ComponentFixture<AvailabilityPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilityPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilityPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
