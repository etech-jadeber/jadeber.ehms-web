import {TCId} from "../tc/models";

export class AvailabilitySummary extends TCId {
  user_id : string;
start_time : number;
end_time : number;
day_of_the_week : number;
day_of_the_month : number;
date : number;
repetition : number;
closed : boolean;
end_date : number;
start_date : number;
max_patient : number;
doctor_name : string;
}

export class AvailabilitySummaryPartialList {
  data: AvailabilitySummary[];
  total: number;
}

export class AvailabilityDetail extends AvailabilitySummary {
}

export class AvailabilityDashboard {
  total: number;
}