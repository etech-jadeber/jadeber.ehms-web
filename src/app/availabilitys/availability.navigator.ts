import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {AvailabilityEditComponent} from "./availability-edit/availability-edit.component";
import {AvailabilityPickComponent} from "./availability-pick/availability-pick.component";


@Injectable({
  providedIn: 'root'
})

export class AvailabilityNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  availabilitysUrl(): string {
    return "/availabilitys";
  }

  availabilityUrl(id: string): string {
    return "/availabilitys/" + id;
  }

  viewAvailabilitys(): void {
    this.router.navigateByUrl(this.availabilitysUrl());
  }

  viewAvailability(id: string): void {
    this.router.navigateByUrl(this.availabilityUrl(id));
  }

  editAvailability(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AvailabilityEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addAvailability(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AvailabilityEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickAvailabilitys(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(AvailabilityPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}