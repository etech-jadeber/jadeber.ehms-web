import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {AvailabilityDashboard, AvailabilityDetail, AvailabilitySummaryPartialList} from "./availability.model";
import { day_of_the_week, repetition } from '../app.enums';
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class AvailabilityPersist {

  availabilitySearchText: string = "";
  userId: string;
  start_date: number;
  end_date: number;

  constructor(private http: HttpClient) {
  }

  searchAvailability(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<AvailabilitySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("availabilitys", this.availabilitySearchText, pageSize, pageIndex, sort, order);
    if (this.userId){
      url = TCUtilsString.appendUrlParameter(url, "user_id", this.userId);
    }
    if (this.start_date){
      url = TCUtilsString.appendUrlParameter(url, "start_date", this.start_date.toString());
    }
    if (this.end_date && (new Date(this.end_date * 1000).getTime() - new Date(this.start_date * 1000).getTime() > 1000 * 60 * 60 * 24)){
      url = TCUtilsString.appendUrlParameter(url, "end_date", this.end_date.toString());
    }
    
    return this.http.get<AvailabilitySummaryPartialList>(url);
  }

  clearFilters(){
    this.userId = null;
  }

  filters(): any {
    let fltrs =  {};
    fltrs[TCUrlParams.searchText] = this.availabilitySearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "availabilitys/do", new TCDoParam("download_all", this.filters()));
  }

  availabilityDashboard(): Observable<AvailabilityDashboard> {
    return this.http.get<AvailabilityDashboard>(environment.tcApiBaseUri + "availabilitys/dashboard");
  }

  getAvailability(id: string): Observable<AvailabilityDetail> {
    return this.http.get<AvailabilityDetail>(environment.tcApiBaseUri + "availabilitys/" + id);
  }

  addAvailability(item: AvailabilityDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "availabilitys/", item);
  }

  updateAvailability(item: AvailabilityDetail): Observable<AvailabilityDetail> {
    return this.http.patch<AvailabilityDetail>(environment.tcApiBaseUri + "availabilitys/" + item.id, item);
  }

  deleteAvailability(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "availabilitys/" + id);
  }

  availabilitysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "availabilitys/do", new TCDoParam(method, payload));
  }

  availabilityDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "availabilitys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "availabilitys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "availabilitys/" + id + "/do", new TCDoParam("print", {}));
  }

  availableForCalendar(startTimeStamp: number, endTimeStamp: number): Observable<AvailabilitySummaryPartialList> {
    let url = environment.tcApiBaseUri + "availableForCalendar";
    url = TCUtilsString.appendUrlParameter(url, "startDate", startTimeStamp.toString());
    url = TCUtilsString.appendUrlParameter(url, "endDate", endTimeStamp.toString());
    return this.http.get<AvailabilitySummaryPartialList>(url);
  }

  repetitionId: number ;

  repetition: TCEnum[] = [
  new TCEnum(repetition.once, 'once'),
  new TCEnum(repetition.every_day, 'every_day'),
  new TCEnum(repetition.once_a_week, 'once_a_week'),
  new TCEnum(repetition.once_a_month, 'once_a_month'),
];

day_of_the_weekId: number ;

day_of_the_week: TCEnum[] = [
new TCEnum(day_of_the_week.Monday, 'monday'),
new TCEnum(day_of_the_week.Tuesday, 'tuesday'),
new TCEnum(day_of_the_week.Wednesday, 'wednesday'),
new TCEnum(day_of_the_week.Thursday, 'thursday'),
new TCEnum(day_of_the_week.Friday, 'friday'),
new TCEnum(day_of_the_week.Saturday, 'saturday'),
new TCEnum(day_of_the_week.Sunday, 'sunday'),
];



}
