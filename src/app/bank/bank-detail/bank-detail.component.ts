import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {BankDetail} from "../bank.model";
import {BankPersist} from "../bank.persist";
import {BankNavigator} from "../bank.navigator";

export enum BankTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-bank-detail',
  templateUrl: './bank-detail.component.html',
  styleUrls: ['./bank-detail.component.scss']
})
export class BankDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  bankIsLoading:boolean = false;
  
  BankTabs: typeof BankTabs = BankTabs;
  activeTab: BankTabs = BankTabs.overview;
  //basics
  bankDetail: BankDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public bankNavigator: BankNavigator,
              public  bankPersist: BankPersist) {
    this.tcAuthorization.requireRead("banks");
    this.bankDetail = new BankDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("banks");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.bankIsLoading = true;
    this.bankPersist.getBank(id).subscribe(bankDetail => {
          this.bankDetail = bankDetail;
          this.bankIsLoading = false;
        }, error => {
          console.error(error);
          this.bankIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.bankDetail.id);
  }
  editBank(): void {
    let modalRef = this.bankNavigator.editBank(this.bankDetail.id);
    modalRef.afterClosed().subscribe(bankDetail => {
      TCUtilsAngular.assign(this.bankDetail, bankDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.bankPersist.print(this.bankDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print bank", true);
      });
    }

  back():void{
      this.location.back();
    }

}