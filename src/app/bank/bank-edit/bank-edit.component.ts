import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BankDetail } from '../bank.model';import { BankPersist } from '../bank.persist';@Component({
  selector: 'app-bank-edit',
  templateUrl: './bank-edit.component.html',
  styleUrls: ['./bank-edit.component.scss']
})export class BankEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  bankDetail: BankDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BankEditComponent>,
              public  persist: BankPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("banks");
      this.title = this.appTranslation.getText("general","new") +  " " + "bank";
      this.bankDetail = new BankDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("banks");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "bank";
      this.isLoadingResults = true;
      this.persist.getBank(this.idMode.id).subscribe(bankDetail => {
        this.bankDetail = bankDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addBank(this.bankDetail).subscribe(value => {
      this.tcNotification.success("bank added");
      this.bankDetail.id = value;
      this.dialogRef.close(this.bankDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateBank(this.bankDetail).subscribe(value => {
      this.tcNotification.success("bank updated");
      this.dialogRef.close(this.bankDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.bankDetail == null){
            return false;
          }
        if (!this.bankDetail.name) {
          return false;
        }
        return true;

 }
 }