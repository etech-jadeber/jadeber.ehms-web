import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { BankPersist } from '../bank.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { BankSummary, BankSummaryPartialList } from '../bank.model';
import { BankNavigator } from '../bank.navigator';
@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.scss']
})export class BankListComponent implements OnInit {
  banksData: BankSummary[] = [];
  banksTotalCount: number = 0;
  bankSelectAll:boolean = false;
  bankSelection: BankSummary[] = [];

 banksDisplayedColumns: string[] = ["select","action", "name"];
  bankSearchTextBox: FormControl = new FormControl();
  bankIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) banksPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) banksSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public bankPersist: BankPersist,
                public bankNavigator: BankNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("banks");
       this.bankSearchTextBox.setValue(bankPersist.bankSearchText);
      //delay subsequent keyup events
      this.bankSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.bankPersist.bankSearchText = value;
        this.searchBanks();
      });
    } ngOnInit() {
   
      this.banksSort.sortChange.subscribe(() => {
        this.banksPaginator.pageIndex = 0;
        this.searchBanks(true);
      });

      this.banksPaginator.page.subscribe(() => {
        this.searchBanks(true);
      });
      //start by loading items
      this.searchBanks();
    }

  searchBanks(isPagination:boolean = false): void {


    let paginator = this.banksPaginator;
    let sorter = this.banksSort;

    this.bankIsLoading = true;
    this.bankSelection = [];

    this.bankPersist.searchBank(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BankSummaryPartialList) => {
      this.banksData = partialList.data;
      if (partialList.total != -1) {
        this.banksTotalCount = partialList.total;
      }
      this.bankIsLoading = false;
    }, error => {
      this.bankIsLoading = false;
    });

  } downloadBanks(): void {
    if(this.bankSelectAll){
         this.bankPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download bank", true);
      });
    }
    else{
        this.bankPersist.download(this.tcUtilsArray.idsList(this.bankSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download bank",true);
            });
        }
  }
addBank(): void {
    let dialogRef = this.bankNavigator.addBank();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBanks();
      }
    });
  }

  editBank(item: BankSummary) {
    let dialogRef = this.bankNavigator.editBank(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBank(item: BankSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("bank");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.bankPersist.deleteBank(item.id).subscribe(response => {
          this.tcNotification.success("bank deleted");
          this.searchBanks();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}