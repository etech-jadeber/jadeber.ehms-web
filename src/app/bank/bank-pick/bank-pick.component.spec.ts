import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BankPickComponent } from './bank-pick.component';

describe('BankPickComponent', () => {
  let component: BankPickComponent;
  let fixture: ComponentFixture<BankPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BankPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BankPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
