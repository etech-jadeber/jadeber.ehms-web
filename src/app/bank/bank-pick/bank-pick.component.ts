import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { BankPersist } from '../bank.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BankSummary, BankSummaryPartialList } from '../bank.model';
@Component({
  selector: 'app-bank-pick',
  templateUrl: './bank-pick.component.html',
  styleUrls: ['./bank-pick.component.scss']
})export class BankPickComponent implements OnInit {
  banksData: BankSummary[] = [];
  banksTotalCount: number = 0;
  bankSelectAll:boolean = false;
  bankSelection: BankSummary[] = [];

 banksDisplayedColumns: string[] = ["select","action","name" ];
  bankSearchTextBox: FormControl = new FormControl();
  bankIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) banksPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) banksSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public bankPersist: BankPersist,
                public dialogRef: MatDialogRef<BankPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("banks");
       this.bankSearchTextBox.setValue(bankPersist.bankSearchText);
      //delay subsequent keyup events
      this.bankSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.bankPersist.bankSearchText = value;
        this.searchBanks();
      });
    } ngOnInit() {
   
      this.banksSort.sortChange.subscribe(() => {
        this.banksPaginator.pageIndex = 0;
        this.searchBanks(true);
      });

      this.banksPaginator.page.subscribe(() => {
        this.searchBanks(true);
      });
      //start by loading items
      this.searchBanks();
    }

  searchBanks(isPagination:boolean = false): void {


    let paginator = this.banksPaginator;
    let sorter = this.banksSort;

    this.bankIsLoading = true;
    this.bankSelection = [];

    this.bankPersist.searchBank(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BankSummaryPartialList) => {
      this.banksData = partialList.data;
      if (partialList.total != -1) {
        this.banksTotalCount = partialList.total;
      }
      this.bankIsLoading = false;
    }, error => {
      this.bankIsLoading = false;
    });

  }
  markOneItem(item: BankSummary) {
    if(!this.tcUtilsArray.containsId(this.bankSelection,item.id)){
          this.bankSelection = [];
          this.bankSelection.push(item);
        }
        else{
          this.bankSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.bankSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }