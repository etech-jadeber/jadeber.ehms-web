import {TCId} from "../tc/models";export class BankSummary extends TCId {
    name:string;
     
    }
    export class BankSummaryPartialList {
      data: BankSummary[];
      total: number;
    }
    export class BankDetail extends BankSummary {
    }
    
    export class BankDashboard {
      total: number;
    }