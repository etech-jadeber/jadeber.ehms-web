import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {BankEditComponent} from "./bank-edit/bank-edit.component";
import {BankPickComponent} from "./bank-pick/bank-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BankNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  banksUrl(): string {
    return "/bank";
  }

  bankUrl(id: string): string {
    return "/bank/" + id;
  }

  viewBanks(): void {
    this.router.navigateByUrl(this.banksUrl());
  }

  viewBank(id: string): void {
    this.router.navigateByUrl(this.bankUrl(id));
  }

  editBank(id: string): MatDialogRef<BankEditComponent> {
    const dialogRef = this.dialog.open(BankEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBank(): MatDialogRef<BankEditComponent> {
    const dialogRef = this.dialog.open(BankEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBanks(selectOne: boolean=false): MatDialogRef<BankPickComponent> {
      const dialogRef = this.dialog.open(BankPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}