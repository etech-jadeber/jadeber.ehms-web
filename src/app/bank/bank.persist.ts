import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {BankDashboard, BankDetail, BankSummaryPartialList} from "./bank.model";


@Injectable({
  providedIn: 'root'
})
export class BankPersist {

 bankSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bank/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.bankSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchBank(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BankSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("bank", this.bankSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<BankSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bank/do", new TCDoParam("download_all", this.filters()));
  }

  bankDashboard(): Observable<BankDashboard> {
    return this.http.get<BankDashboard>(environment.tcApiBaseUri + "bank/dashboard");
  }

  getBank(id: string): Observable<BankDetail> {
    return this.http.get<BankDetail>(environment.tcApiBaseUri + "bank/" + id);
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bank/do", new TCDoParam("download", ids));
}

banksDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bank/do", new TCDoParam(method, payload));
  }

  bankDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bank/" + id + "/do", new TCDoParam(method, payload));
  }

  addBank(item: BankDetail): Observable<string> {
    return this.http.post<string>(environment.tcApiBaseUri + "bank", item);
  }

  updateBank(item: BankDetail): Observable<BankDetail> {
    return this.http.patch<BankDetail>(environment.tcApiBaseUri + "bank/" + item.id, item);
  }

  deleteBank(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "bank/" + id);
  }
 }