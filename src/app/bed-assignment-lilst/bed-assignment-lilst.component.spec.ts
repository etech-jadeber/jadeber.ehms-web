import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BedAssignmentLilstComponent } from './bed-assignment-lilst.component';

describe('BedAssignmentLilstComponent', () => {
  let component: BedAssignmentLilstComponent;
  let fixture: ComponentFixture<BedAssignmentLilstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BedAssignmentLilstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BedAssignmentLilstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
