import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';

import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsAngular } from '../tc/utils-angular';


import { PatientPersist } from '../patients/patients.persist';
import { PatientSummary } from '../patients/patients.model';
import { BedassignmentSummary } from '../bed/bedassignments/bedassignment.model';
import { BedroomSummary, BedroomSummaryPartialList } from '../bed/bedrooms/bedroom.model';
import { BedSummary, BedSummaryPartialList } from '../bed/beds/bed.model';
import { BedassignmentPersist } from '../bed/bedassignments/bedassignment.persist';
import { BedassignmentNavigator } from '../bed/bedassignments/bedassignment.navigator';
import { BedroomPersist } from '../bed/bedrooms/bedroom.persist';
import { BedPersist } from '../bed/beds/bed.persist';

@Component({
  selector: 'app-bed-assignment-lilst',
  templateUrl: './bed-assignment-lilst.component.html',
  styleUrls: ['./bed-assignment-lilst.component.css']
})
export class BedAssignmentLilstComponent implements OnInit {
  patients: PatientSummary[] = [];
  changeBedRoom: boolean = false;
  bedassignmentsSelection: BedassignmentSummary[] = [];
  bedassignmentsData: BedassignmentSummary[] = [];
  bedassignmentsTotalCount: number = 0;
  bedassignmentsSelectAll: boolean = false;
  bedassignmentsDisplayedColumns: string[] = [
    'select',
    'action',
    'patient_id',
    'bed_id',
    'bed_room_id',
    'start_date',
    'end_date',
  ];

  bedRoom: BedroomSummary[] = [];
  beds: BedSummary[] = [];

  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public bedAssignmentPersist: BedassignmentPersist,
    public bedassignmentNavigator: BedassignmentNavigator,
    public bedRoomPersist: BedroomPersist,
    public bedPersist: BedPersist,
    public patientPersist: PatientPersist,
  ) { 
    this.tcAuthorization.requireRead('bed_assignments')
  }

  ngOnInit(): void {
    this.loadBedRooms();
    this.loadBeds();
    this.getPatients();
  }

  changeBed(item: BedassignmentSummary) {
    this.changeBedRoom = true;
    let dialogRef = this.bedassignmentNavigator.changeBed(
      item.id,
      this.changeBedRoom
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchBedAssignment();
      }
    });
  }

  searchBedAssignment(): void {
    this.bedAssignmentPersist
      .bedassignmentDo(
        TCUtilsString.getInvalidId(),
        'get_assigned_bed_by_encounter',
        { encounter: this.encounterId}
      )
      .subscribe((res: BedassignmentSummary[]) => {
        this.bedassignmentsData = res;
      });
  }

  loadBedRooms() {
    this.bedRoomPersist.bedroomsDo('get_all_rooms', {}).subscribe((result) => {
      this.bedRoom = (result as BedroomSummaryPartialList).data;
    });
  }

  loadBeds() {
    this.bedPersist.bedsDo('get_all_beds', {}).subscribe((result) => {
      this.beds = (result as BedSummaryPartialList).data;
    });
  }

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(
      (value) => value.uuidd == patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
    return '';
  }

  getPatients() {
    this.patientPersist.searchPatient(50, 0, '', 'asc').subscribe((result) => {
      this.patients = result.data;
    });
  }
  
  back(): void {
    this.location.back();
  } 

}
