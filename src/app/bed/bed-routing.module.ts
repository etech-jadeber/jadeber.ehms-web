import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NurseSelfComponent } from '../doctors/nurse-self/nurse-self.component';
import { Admission_FormDetailComponent } from '../form_encounters/admission_forms/admission-form-detail/admission-form-detail.component';
import { Admission_FormListComponent } from '../form_encounters/admission_forms/admission-form-list/admission-form-list.component';
import { BedassignmentDetailComponent } from './bedassignments/bedassignment-detail/bedassignment-detail.component';
import { BedassignmentListComponent } from './bedassignments/bedassignment-list/bedassignment-list.component';
import { BedroomlevelDetailComponent } from './bedroomlevels/bedroomlevel-detail/bedroomlevel-detail.component';
import { BedroomlevelListComponent } from './bedroomlevels/bedroomlevel-list/bedroomlevel-list.component';
import { BedroomDetailComponent } from './bedrooms/bedroom-detail/bedroom-detail.component';
import { BedroomListComponent } from './bedrooms/bedroom-list/bedroom-list.component';
import { BedroomtypeDetailComponent } from './bedroomtypes/bedroomtype-detail/bedroomtype-detail.component';
import { BedroomtypeListComponent } from './bedroomtypes/bedroomtype-list/bedroomtype-list.component';
import { BedDetailComponent } from './beds/bed-detail/bed-detail.component';
import { BedListComponent } from './beds/bed-list/bed-list.component';
import { WaitinglistDetailComponent } from './waitinglists/waitinglist-detail/waitinglist-detail.component';
import { WaitinglistListComponent } from './waitinglists/waitinglist-list/waitinglist-list.component';


const routes: Routes = [
  {path: 'bedroomtypes/:id', component: BedroomtypeDetailComponent},
  {path: 'bedroomtypes', component: BedroomtypeListComponent},
  {path: 'waitinglists/:id', component: WaitinglistDetailComponent},
  {path: 'waitinglists', component: WaitinglistListComponent},
  {path: 'nurse-home', component: NurseSelfComponent},

  {path: 'beds/:id', component: BedDetailComponent},
  {path: 'beds', component: BedListComponent},

  {path: 'bedroomlevels/:id', component: BedroomlevelDetailComponent},
  {path: 'bedroomlevels', component: BedroomlevelListComponent},
  {path: 'bedrooms/:id', component: BedroomDetailComponent},
  {path: 'bedrooms', component: BedroomListComponent},

  {path: 'bedassignments/:id', component: BedassignmentDetailComponent},
  {path: 'bedassignments', component: BedassignmentListComponent},

  // { path: 'bedroomtypes/:id', component: BedroomtypeDetailComponent },
  // { path: 'bedroomtypes', component: BedroomtypeListComponent },
  { path: 'waitinglists/:id', component: WaitinglistDetailComponent },
  { path: 'waitinglists', component: WaitinglistListComponent },

  { path: 'bedroomlevels/:id', component: BedroomlevelDetailComponent },
  { path: 'bedroomlevels', component: BedroomlevelListComponent },
  { path: 'bedrooms/:id', component: BedroomDetailComponent },
  { path: 'bedrooms', component: BedroomListComponent },

  { path: 'bedassignments/:id', component: BedassignmentDetailComponent },
  { path: 'bedassignments', component: BedassignmentListComponent },
  { path: 'admission_forms/:id', component: Admission_FormDetailComponent },
  { path: 'admission_forms', component: Admission_FormListComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BedRoutingModule { }
