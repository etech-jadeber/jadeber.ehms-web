import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BedRoutingModule } from './bed-routing.module';
import { WaitinglistDetailComponent } from './waitinglists/waitinglist-detail/waitinglist-detail.component';
import { WaitinglistListComponent } from './waitinglists/waitinglist-list/waitinglist-list.component';
import { WaitinglistEditComponent } from './waitinglists/waitinglist-edit/waitinglist-edit.component';
import { WaitinglistPickComponent } from './waitinglists/waitinglist-pick/waitinglist-pick.component';
import { BedassignmentPickComponent } from './bedassignments/bedassignment-pick/bedassignment-pick.component';
import { BedassignmentEditComponent, BedassignmentEditListComponent } from './bedassignments/bedassignment-edit/bedassignment-edit.component';
import { BedListComponent } from './beds/bed-list/bed-list.component';
import { BedDetailComponent } from './beds/bed-detail/bed-detail.component';
import { BedEditComponent } from './beds/bed-edit/bed-edit.component';
import { BedPickComponent } from './beds/bed-pick/bed-pick.component';
import { BedroomtypeListComponent } from './bedroomtypes/bedroomtype-list/bedroomtype-list.component';
import { BedroomtypeDetailComponent } from './bedroomtypes/bedroomtype-detail/bedroomtype-detail.component';
import { BedroomtypeEditComponent } from './bedroomtypes/bedroomtype-edit/bedroomtype-edit.component';
import { BedroomtypePickComponent } from './bedroomtypes/bedroomtype-pick/bedroomtype-pick.component';
import { BedroomlevelListComponent } from './bedroomlevels/bedroomlevel-list/bedroomlevel-list.component';
import { BedroomlevelDetailComponent } from './bedroomlevels/bedroomlevel-detail/bedroomlevel-detail.component';
import { BedroomlevelEditComponent } from './bedroomlevels/bedroomlevel-edit/bedroomlevel-edit.component';
import { BedroomlevelPickComponent } from './bedroomlevels/bedroomlevel-pick/bedroomlevel-pick.component';
import { BedroomListComponent } from './bedrooms/bedroom-list/bedroom-list.component';
import { BedroomEditComponent } from './bedrooms/bedroom-edit/bedroom-edit.component';
import { BedroomDetailComponent } from './bedrooms/bedroom-detail/bedroom-detail.component';
import { BedassignmentListComponent } from './bedassignments/bedassignment-list/bedassignment-list.component';
import { BedassignmentDetailComponent } from './bedassignments/bedassignment-detail/bedassignment-detail.component';
import { BedroomPickComponent } from './bedrooms/bedroom-pick/bedroom-pick.component';
import { NurseSelfComponent } from '../doctors/nurse-self/nurse-self.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCommonModule, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatChipsModule } from '@angular/material/chips';
import { BedassignmentGraphComponent } from './bedassignments/bedassignment-graph/bedassignment-graph.component';
import { Admission_FormDetailComponent } from '../form_encounters/admission_forms/admission-form-detail/admission-form-detail.component';
import { Admission_FormEditComponent } from '../form_encounters/admission_forms/admission-form-edit/admission-form-edit.component';
import { Admission_FormListComponent } from '../form_encounters/admission_forms/admission-form-list/admission-form-list.component';
import { Admission_FormPickComponent } from '../form_encounters/admission_forms/admission-form-pick/admission-form-pick.component';


@NgModule({
  declarations: [ WaitinglistDetailComponent,
    WaitinglistEditComponent,
    WaitinglistPickComponent,
    WaitinglistListComponent,
    BedListComponent,
    BedDetailComponent,
    BedEditComponent,
    BedPickComponent,
    BedroomtypeListComponent,
    BedroomtypeDetailComponent,
    BedroomtypeEditComponent,
    NurseSelfComponent,
    BedroomtypePickComponent,
    BedroomlevelListComponent,
    BedroomlevelDetailComponent,
    BedroomlevelEditComponent,
    BedroomlevelPickComponent,
    BedroomListComponent,
    BedroomDetailComponent,
    BedroomEditComponent,
    BedroomPickComponent,
    BedassignmentListComponent,
    BedassignmentDetailComponent,
    BedassignmentEditComponent,
    BedassignmentPickComponent,
    Admission_FormListComponent,
    Admission_FormDetailComponent,
    Admission_FormEditComponent,
    Admission_FormPickComponent,
    BedassignmentEditListComponent,
    BedassignmentGraphComponent
  ],
  imports: [
    CommonModule,
    BedRoutingModule,
    FullCalendarModule,
    FormsModule,
    MatBadgeModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatListModule,
    MatRippleModule,
    MatTableModule,
    MatCommonModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatSelectModule,
    MatTabsModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatChipsModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatSortModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    NgxChartsModule,
  ]
})
export class BedModule { }
