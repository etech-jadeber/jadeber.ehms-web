import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedassignmentDetailComponent } from './bedassignment-detail.component';

describe('BedassignmentDetailComponent', () => {
  let component: BedassignmentDetailComponent;
  let fixture: ComponentFixture<BedassignmentDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedassignmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedassignmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
