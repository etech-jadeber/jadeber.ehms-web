import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedassignmentEditComponent } from './bedassignment-edit.component';

describe('BedassignmentEditComponent', () => {
  let component: BedassignmentEditComponent;
  let fixture: ComponentFixture<BedassignmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedassignmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedassignmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
