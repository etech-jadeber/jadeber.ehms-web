import { Component, Inject, Injectable, OnInit, ViewChild } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

import { TCAuthorization } from "../../../tc/authorization";
import { TCNotification } from "../../../tc/notification";
import { TCUtilsString } from "../../../tc/utils-string";
import { AppTranslation } from "../../../app.translation";

import { TCModalModes, TCParentChildIds } from "../../../tc/models";


import { BedassignmentDetail, BedassignmentSummary, ExtendedBedassignmentDetail } from "../bedassignment.model";
import { BedassignmentPersist } from "../bedassignment.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Admission_FormPersist } from "../../../form_encounters/admission_forms/admission_form.persist";
import { DepositPersist } from 'src/app/deposits/deposit.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import {Location} from '@angular/common';
import { BedassignmentGraphComponent } from '../bedassignment-graph/bedassignment-graph.component';
import {BedroomlevelPersist} from "../../bedroomlevels/bedroomlevel.persist";

@Injectable()
abstract class BedassignmentComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  bedassignmentDetail: ExtendedBedassignmentDetail;
  @ViewChild('graph', {static: false}) graph: BedassignmentGraphComponent;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public persist: BedassignmentPersist,
    public tcUtilsArray: TCUtilsArray,
    public depositPersist: DepositPersist,
    public admissionFormPersist: Admission_FormPersist,
    public bedRoomLevelPersist: BedroomlevelPersist,
    public ids: TCParentChildIds,
    public isDialog: Boolean) {
  }


  isWizard(): boolean {
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] == TCModalModes.EDIT;

  }


  ngOnInit() {

    if (this.isNew()) {
      this.tcAuthorization.requireCreate('bed_assignments')
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("bed", "bed_assignment");
      this.bedassignmentDetail = new ExtendedBedassignmentDetail();
      this.bedassignmentDetail.can_wait = false;
      this.bedassignmentDetail.remark = "";
      this.bedassignmentDetail.admission_form_id = this.ids.parentId;
      this.bedassignmentDetail.ward_id = this.ids.childId;
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate('bed_assignments')
      // this.title = !this.changeBedRoom ? this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("bed", "bed_assignment") : this.appTranslation.getText("bed", "change_bed");
      this.bedassignmentDetail = new ExtendedBedassignmentDetail();
      this.isLoadingResults = true;

      this.persist.getBedassignment(this.ids.childId).subscribe(bedassignmentDetail => {
        this.bedassignmentDetail = bedassignmentDetail;
        this.bedRoomLevelPersist.getBedroomlevel(this.bedassignmentDetail.assigned_bed_room_level_id).subscribe(value => {
          this.bedassignmentDetail.assigned_bed_room_level_name = value.name
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }

  }

  action(bed_assignmentDetail: BedassignmentDetail) { }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addBedassignment(this.bedassignmentDetail).subscribe(value => {
      if (value) {
        this.tcNotification.success("bed is assigned");
      }
      this.action(this.bedassignmentDetail)
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateBedassignment(this.bedassignmentDetail).subscribe(value => {
      this.tcNotification.success("Bedassignment updated");
      this.action(this.bedassignmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  dispatchPatient(item: BedassignmentSummary): void {
    item.end_date = this.tcUtilsDate.toTimeStamp(new Date());
    //just update the status to assigned and load the bedassignments
    this.persist.bedassignmentDo(item.id, "dispatch", item).subscribe(value => {
      this.tcNotification.success("Patient dispatched");
    }, error => {
      // this.isLoadingResults = false;
    })
  }


  canSubmit(): boolean {
    if (this.bedassignmentDetail == null) {
      return false;
    }

    if (this.bedassignmentDetail.bed_room_id == null) {
      return false;
    }
    if (this.bedassignmentDetail.bed_id == null) {
      return false;
    }

    if (this.bedassignmentDetail.ward_id == null) {
      return false;
    }

    return true;
  }

  back(): void {
  }

  onCancel(): void {}

}


@Component({
  selector: 'app-bedassignment-edit',
  templateUrl: './bedassignment-edit.component.html',
  styleUrls: ['./bedassignment-edit.component.css']
})
export class BedassignmentEditComponent extends BedassignmentComponent {
  constructor(
    public dialogRef: MatDialogRef<BedassignmentEditComponent>,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public persist: BedassignmentPersist,
    public tcUtilsArray: TCUtilsArray,
    public depositPersist: DepositPersist,
    public admissionFormPersist: Admission_FormPersist,
    public bedRoomLevelPersist: BedroomlevelPersist,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {
    super(tcAuthorization, tcNotification, tcUtilsString, tcUtilsDate, appTranslation, persist, tcUtilsArray, depositPersist, admissionFormPersist, bedRoomLevelPersist, ids, true)
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(review: BedassignmentDetail) {
    this.dialogRef.close(review)
  }

}


@Component({
  selector: 'app-bedassignment-edit-list',
  templateUrl: './bedassignment-edit.component.html',
  styleUrls: ['./bedassignment-edit.component.css']
})
export class BedassignmentEditListComponent extends BedassignmentComponent {

  constructor(
    public location: Location,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public persist: BedassignmentPersist,
    public tcUtilsArray: TCUtilsArray,
    public depositPersist: DepositPersist,
    public admissionFormPersist: Admission_FormPersist,
    public bedRoomLevelPersist: BedroomlevelPersist,
    ) {
    super(tcAuthorization, tcNotification, tcUtilsString, tcUtilsDate, appTranslation, persist, tcUtilsArray, depositPersist, admissionFormPersist, bedRoomLevelPersist, {
      parentId: null,
      childId: null,
      context: null,
    }, false)
  }

  isNew(): boolean {
    return true
  }

  back(): void {
    this.location.back();
  }

}
