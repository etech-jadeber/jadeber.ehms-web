import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BedassignmentGraphComponent } from './bedassignment-graph.component';

describe('BedassignmentGraphComponent', () => {
  let component: BedassignmentGraphComponent;
  let fixture: ComponentFixture<BedassignmentGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BedassignmentGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BedassignmentGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
