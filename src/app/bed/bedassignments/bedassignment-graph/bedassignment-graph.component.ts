import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs';
import { bed_status } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { BedroomDetail, BedroomSummary } from '../../bedrooms/bedroom.model';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { BedSummary, BedDetail } from '../../beds/bed.model';
import { BedPersist } from '../../beds/bed.persist';
import { ExtendedBedassignmentDetail } from '../bedassignment.model';
import { BedassignmentPersist } from '../bedassignment.persist';
import { BedroomlevelNavigator } from '../../bedroomlevels/bedroomlevel.navigator';
import { BedroomlevelDetail } from '../../bedroomlevels/bedroomlevel.model';

@Component({
  selector: 'app-bedassignment-graph',
  templateUrl: './bedassignment-graph.component.html',
  styleUrls: ['./bedassignment-graph.component.scss']
})
export class BedassignmentGraphComponent implements OnInit {

  bedroomtypeSearchTextBox: FormControl = new FormControl();
  bedroomSearchTextBox: FormControl = new FormControl();
  bedRoomTypeTotalCount = 0;
  bedRoomTotalCount = 0;
  ward_id: string;
  isLoadingResults: boolean = false;
  bedroomTypes: BedroomtypeSummary[] = [];
  bedrooms: BedroomSummary[] = [];
  changeBedRoom: boolean = false;
  beds: BedSummary[] = [];
  bed_status = bed_status;
  bed_room_level: string

  @Input() bedassignmentDetail: ExtendedBedassignmentDetail = new ExtendedBedassignmentDetail()
  @ViewChildren(MatPaginator) paginator = new QueryList<MatPaginator>();
  constructor(
    public bedroomTypePersis: BedroomtypePersist,
    public bedroomPersist: BedroomPersist,
    public bedPersist: BedPersist,
    public persist: BedassignmentPersist,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public tcUtilsArray: TCUtilsArray,
    public bedRoomLevelNavigator: BedroomlevelNavigator,
  ) {
    this.bedroomtypeSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.bedroomTypePersis.bedroomtypeSearchText = value;
      this.loadBedroomTypes();
    });
    this.bedroomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.bedroomPersist.bedroomSearchText = value;
      this.loadBedrooms();
    });
  }



  ngOnInit(): void {
    this.ward_id = this.bedassignmentDetail.ward_id;
    this.loadBedrooms();
  }

  ngAfterViewInit() {
    this.paginator.toArray()[0].page.subscribe(() => {
      this.loadBedroomTypes(true);
    });
    this.paginator.toArray()[1].page.subscribe(() => {
      this.loadBedrooms(true);
    });
    this.paginator.toArray()[0].pageSize = 6;
    this.paginator.toArray()[1].pageSize = 2;
    this.loadBedroomTypes();
    if (this.bedassignmentDetail.ward_id){
      this.ward_id = this.bedassignmentDetail.ward_id
      this.loadBedrooms()
    }
  }

  loadBedroomTypes(isPagination: boolean = false) {
    let paginator = this.paginator.toArray()[0];
    this.isLoadingResults = true;
    this.bedroomTypePersis.searchBedroomtype(paginator.pageSize, isPagination ? paginator.pageIndex : 0, 'available', 'desc').subscribe(
      (result) => {
        if (result) {
          this.bedroomTypes = result.data;
          if (result.total != -1) {
            this.bedRoomTypeTotalCount = result.total
          }
          this.isLoadingResults = false
        }
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  loadBedrooms(isPagination: boolean = false) {
    let paginator = this.paginator.toArray()[1];
    this.bedroomPersist.ward_id = this.ward_id;
    this.isLoadingResults = true;
    this.bedroomPersist.searchBedroom(paginator.pageSize, isPagination ? paginator.pageIndex : 0, 'id', 'desc').subscribe(
      (bedroomParital) => {
        this.bedrooms = bedroomParital.data;
        this.bedrooms.forEach(bedroom => {
          this.persist.bedassignmentsDo("bed_information", { ward_id: this.ward_id, bed_room_id: bedroom.id }).subscribe(
            (bed: BedDetail[]) => {
              bedroom.beds = bed
            }
          )
        })
        if (bedroomParital.total != -1) {
          this.bedRoomTotalCount = bedroomParital.total
        }
        this.isLoadingResults = false
      },
      error => {
        this.isLoadingResults = false;
      }
    )
  }

  assignPatient(bed: BedDetail, bed_room: BedroomDetail) {
    if (bed.status == bed_status.available) {
      this.bedassignmentDetail.bed_id = bed.id;
      this.bedassignmentDetail.bed_room_id = bed_room.id;
      this.bedassignmentDetail.ward_id = bed_room.bed_room_type_id
    }
  }

  searchBedroomLevel(isAssigned: boolean = false){
    let dialogRef = this.bedRoomLevelNavigator.pickBedroomlevels(true)
    dialogRef.afterClosed().subscribe(
      (bed_room_level: BedroomlevelDetail[]) => {
        if(isAssigned) {
          this.bedroomPersist.bed_room_level = bed_room_level[0].id
          this.bed_room_level = bed_room_level[0].name
          this.loadBedrooms()
        } else {
          this.bedassignmentDetail.assigned_bed_room_level_id = bed_room_level[0].id
          this.bedassignmentDetail.assigned_bed_room_level_name = bed_room_level[0].name
        }
      }
    )
  }

}
