import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedassignmentListComponent } from './bedassignment-list.component';

describe('BedassignmentListComponent', () => {
  let component: BedassignmentListComponent;
  let fixture: ComponentFixture<BedassignmentListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedassignmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedassignmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
