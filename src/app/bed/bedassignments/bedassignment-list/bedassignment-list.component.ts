import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from 'rxjs/operators';
import {Location} from '@angular/common';

import {TCAuthorization} from '../../../tc/authorization';
import {TCUtilsAngular} from '../../../tc/utils-angular';
import {TCUtilsArray} from '../../../tc/utils-array';
import {TCUtilsDate} from '../../../tc/utils-date';
import {TCNotification} from '../../../tc/notification';
import {TCNavigator} from '../../../tc/navigator';
import {JobPersist} from '../../../tc/jobs/job.persist';

import {AppTranslation} from '../../../app.translation';

import {BedassignmentPersist} from '../bedassignment.persist';
import {BedassignmentNavigator} from '../bedassignment.navigator';
import {
  BedassignmentDetail, bedAssignmentFilterColumn,
  BedassignmentSummary,
  BedassignmentSummaryPartialList,
  PrefixedBedAssignmentPartialList, PrefixedBedAssignmentSummary,
} from '../bedassignment.model';
import {
  getPrefixPatientName,
  PatientDetail,
  PatientSummary,
  PatientSummaryPartialList,
} from 'src/app/patients/patients.model';
import {PatientPersist} from 'src/app/patients/patients.persist';

import {PatientNavigator} from 'src/app/patients/patients.navigator';
import {Admission_FormSummary} from "../../../form_encounters/admission_forms/admission_form.model";
import {Admission_FormPersist} from "../../../form_encounters/admission_forms/admission_form.persist";
import {Admission_FormNavigator} from "../../../form_encounters/admission_forms/admission_form.navigator";
import { AdmissionStatus, admission_form_status, Admission_status, bed_assignment_status, physican_type } from 'src/app/app.enums';
import { BedDetail, BedSummary, BedSummaryPartialList } from '../../beds/bed.model';
import { BedroomDetail, BedroomSummary, BedroomSummaryPartialList } from '../../bedrooms/bedroom.model';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';
import { BedPersist } from '../../beds/bed.persist';
import { WaitinglistNavigator } from '../../waitinglists/waitinglist.navigator';
import { encounterFilterColumn } from 'src/app/form_encounters/form_encounter.model';
import { BedroomtypeDetail } from '../../bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { getPrefixPhysicianName} from "../../../doctors/doctor.model";
@Component({
  selector: 'app-bedassignment-list',
  templateUrl: './bedassignment-list.component.html',
  styleUrls: ['./bedassignment-list.component.css'],
})
export class BedassignmentListComponent implements OnInit {
  bedassignmentsData: PrefixedBedAssignmentSummary[] = [];
  bedassignmentsTotalCount: number = 0;
  bedassignmentsSelectAll: boolean = false;
  bedassignmentsSelection: PrefixedBedAssignmentSummary[] = [];
  changeBedRoom:boolean=false;
  bedAssignmentFilterColumn = bedAssignmentFilterColumn;
  getPrefixPatientName = getPrefixPatientName;
  getPrefixPhysicianName = getPrefixPhysicianName;

  bedassignmentsDisplayedColumns: string[] = [
    'select',
    'action',
    'patient.pid',
    'patient_fname',
    'bed_room_type_name',
    'bed_serial_id',
    'bed_room_name',
    'physician_first_name',
    'admission_form_admission_diagnosis',
    'start_date',
    'end_date',
  ];
  bedassignmentsSearchTextBox: FormControl = new FormControl();
  bedassignmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true})
  bedassignmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedassignmentsSort: MatSort;

  admissionForms: Admission_FormSummary[] = [];
  AdmissionStatus = admission_form_status
  beds: {[id: string] : BedDetail} = {}
  wards: { [id: string]: BedroomtypeDetail } = {}
  bedrooms : {[id: string] : BedroomDetail} = {}
  patients : {[id: string] : PatientDetail} = {}
  @Input() role;
  physicianType = physican_type;

  constructor(
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public bedassignmentPersist: BedassignmentPersist,
    public bedassignmentNavigator: BedassignmentNavigator,
    public nurseAdmissionNavigator: Admission_FormNavigator,
    public jobPersist: JobPersist,
    public patientPersist: PatientPersist,
    private bedRoomPersist: BedroomPersist,
    private bedPersist: BedPersist,
    private wardPersist: BedroomtypePersist,
    public admissionFormNavigator: Admission_FormNavigator,
    private admissionFormPersist: Admission_FormPersist,
    public patientNavigator: PatientNavigator,
    public waitinglistNavigator: WaitinglistNavigator,
  ) {
    this.tcAuthorization.requireRead('bed_assignments')
    this.bedassignmentsSearchTextBox.setValue(
      bedassignmentPersist.bedAssignmentSearchHistory.search_text
    );
    //delay subsequent keyup events
    this.bedassignmentsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.bedassignmentPersist.bedAssignmentSearchHistory.search_text = value;
        this.searchBedassignments();
      });
      bedassignmentPersist.bedAssignmentSearchHistory.status = bed_assignment_status.active;
  }

  ngOnInit() {
    this.bedassignmentsSort.sortChange.subscribe(() => {
      this.bedassignmentsPaginator.pageIndex = 0;
      this.searchBedassignments(true);
    });

    this.bedassignmentsPaginator.page.subscribe(() => {
      this.searchBedassignments(true);
    });
    //start by loading items
    this.searchBedassignments();
    // this.loadBedRooms();
    // this.loadBeds();
  }

  getAdmissionForm(value: BedassignmentSummary) {
    this.admissionFormPersist.getAdmission_Form(value.admission_form_id).subscribe(result => {
      value.admission_status = result.admission_status;
      value.admission_process_status = result.status
      value.serial_id = result.serial_id
    }, error => {
      console.error(error);
    });
  }

  searchBedassignments(isPagination: boolean = false): void {
    let paginator = this.bedassignmentsPaginator;
    let sorter = this.bedassignmentsSort;

    this.bedassignmentsIsLoading = true;
    this.bedassignmentsSelection = [];

    this.bedassignmentPersist
      .searchBedassignment(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: PrefixedBedAssignmentPartialList) => {
          this.bedassignmentsData = partialList.data;
          // this.bedassignmentsData.forEach(value => {
            // if (!this.beds[value.bed_id]){
            //   this.beds[value.bed_id] = new BedDetail()
            //   this.bedPersist.getBed(value.bed_id).subscribe(
            //     bed => this.beds[value.bed_id] = bed
            //   )
            // }
            // if (!this.wards[value.ward_id]) {
            //   this.wards[value.ward_id] = new BedroomtypeDetail()
            //   this.wardPersist.getBedroomtype(value.ward_id).subscribe(
            //     ward => this.wards[value.ward_id] = ward
            //   )
            // }
            // if (!this.beds[value.bed_room_id]){
            //   this.bedrooms[value.bed_room_id] = new BedroomDetail()
            //   this.bedRoomPersist.getBedroom(value.bed_room_id).subscribe(
            //     room => this.bedrooms[value.bed_room_id] = room
            //   )
            // }
            // if (!this.beds[value.patient_id]){
            //   this.patients[value.patient_id] = new PatientDetail()
            //   this.patientPersist.getPatient(value.patient_id).subscribe(
            //     patient => this.patients[value.patient_id] = patient
            //   )
            // }
          // })
          if (partialList.total != -1) {
            this.bedassignmentsTotalCount = partialList.total;
          }
          this.bedassignmentsIsLoading = false;
        },
        (error) => {
          this.bedassignmentsIsLoading = false;
        }
      );
  }

  downloadBedassignments(): void {
    if (this.bedassignmentsSelectAll) {
      this.bedassignmentPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download bedassignments',
          true
        );
      });
    } else {
      this.bedassignmentPersist
        .download(this.tcUtilsArray.idsList(this.bedassignmentsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download bedassignments',
            true
          );
        });
    }
  }

  // addBedassignment(): void {
  //   let dialogRef = this.bedassignmentNavigator.addBedassignment();
  //   dialogRef.afterClosed().subscribe((result) => {
  //     if (result) {
  //       this.searchBedassignments();
  //     }
  //   });
  // }

  sendPayment(bedAssignmentDetail: BedassignmentDetail){
    let dialogRef = this.tcNavigator.dateInput("Date Range", null, "Save", "cancel", true)
    dialogRef.afterClosed().subscribe((value) => {
      if(value){
        this.bedassignmentPersist.bedassignmentDo(bedAssignmentDetail.id, "send_bed_payment", {start_date: value.start, end_date: value.end}).subscribe(
          (value) => {
            this.tcNotification.success("The payment is successfuly added")
          }
        )
      }
    })
  }

  assignNurse(bedAssignmentDetail: BedassignmentDetail): void {
    let dialogRef = this.nurseAdmissionNavigator.addAdmission_Form(bedAssignmentDetail.admission_form_id);
    dialogRef.afterClosed().subscribe((result) => {
    if (result) {
      this.searchBedassignments();
    }
    })
  }

  // loadPatients() {
  //   this.patientPersist
  //     .patientsDo('get_all_patients', '')
  //     .subscribe((result) => {
  //       this.patients = (result as PatientSummaryPartialList).data;
  //     });
  // }

  editBedassignment(item: BedassignmentSummary) {
    let dialogRef = this.bedassignmentNavigator.editBedassignment(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }
  changeBed(item: BedassignmentSummary) {
    this.changeBedRoom=true;
    let dialogRef = this.bedassignmentNavigator.changeBed(item.id,this.changeBedRoom);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchBedassignments()
      }
    });
    // let dialogRef = this.waitinglistNavigator.editWaitinglist(item.admission_form_id, true);
    // dialogRef.afterClosed().subscribe((result: WaitinglistSummary) => {
    //   if (result) {
    //     this.searchBedassignments();
    //   }
    // });
  }

  deleteBedassignment(item: BedassignmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Bedassignment');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.bedassignmentPersist.deleteBedassignment(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Bedassignment deleted');
            this.searchBedassignments();
          },
          (error) => {
          }
        );
      }
    });
  }

  dispatchPatient(item: BedassignmentSummary): void {
    item.end_date = this.tcUtilsDate.toTimeStamp(new Date());
    //just update the status to assigned and load the bedassignments
    this.bedassignmentPersist.bedassignmentDo(item.id, "dispatch", item).subscribe(value => {
      this.tcNotification.success("Patient dispatched");
      this.searchBedassignments();
    }, error => {
      // this.isLoadingResults = false;
    })
  }

  back(): void {
    this.location.back();
  }

  getPatientFullName(patientId: string): string {
    const patient = this.patients[patientId]
    return patient.fname ? `${patient.fname} ${patient.mname} ${patient.lname}` : ''
  }

  getBedRoom(room_id: string): string {
    const room = this.bedrooms[room_id]
    return room.name ? `${room.name}` : ''
  }

  getBed(bed_id: string): string {
    const bed = this.beds[bed_id]
    return bed.serial_id ? `${bed.serial_id}` : ''
  }

  getWard(ward_id: string): string {
    const ward = this.wards[ward_id]
    return ward.name ? `${ward.name}` : ''
  }


  // loadBedRooms() {
  //   this.bedRoomPersist.bedroomsDo('get_all_rooms', {}).subscribe((result) => {
  //     this.bedRoom = (result as BedroomSummaryPartialList).data;
  //   });
  // }

  // loadBeds() {
  //   this.bedPersist.bedsDo('get_all_beds', {}).subscribe((result) => {
  //     this.beds = (result as BedSummaryPartialList).data;
  //   });
  // }
}
