import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedassignmentPickComponent } from './bedassignment-pick.component';

describe('BedassignmentPickComponent', () => {
  let component: BedassignmentPickComponent;
  let fixture: ComponentFixture<BedassignmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedassignmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedassignmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
