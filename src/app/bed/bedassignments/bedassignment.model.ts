import { PrefixedDoctor } from "src/app/doctors/doctor.model";
import { PrefixedPatient } from "src/app/patients/patients.model";
import {TCId, TCString} from "../../tc/models";
import { WaitinglistDetail } from "../waitinglists/waitinglist.model";
import { PrefixedAdmission } from "src/app/form_encounters/admission_forms/admission_form.model";
import { PrefixedBedRoom } from "../bedrooms/bedroom.model";
import { PrefixedBedRoomType } from "../bedroomtypes/bedroomtype.model";
import { PrefixedBed } from "../beds/bed.model";

export type PrefixedBedAssignmentSummary = PrefixedPatient & PrefixedDoctor  & PrefixedAdmission & PrefixedBed & PrefixedBedRoom & PrefixedBedRoomType;


export class PrefixedBedAssignmentPartialList {
  data: PrefixedBedAssignmentSummary[]
  total: number;
}

export const bedAssignmentFilterColumn: TCString[] = [
  new TCString("patient.pid", 'MRN'),
  new TCString("patient.fname", 'First Name'),
  new TCString("patient.mname", 'Middle Name'),
  new TCString("patient.lname", 'Last Name'),
  new TCString("patient.phone_cell", 'Phone Number'),
];



export class BedassignmentSummary extends TCId {
  patient_id: string;
  bed_id: string;
  bed_room_id: string;
  start_date: number;
  end_date: number;
  admission_form_id: string;
  status:number;
  admission_status: number;
  admission_process_status?: number;
  serial_id?: number;
}

export class BedassignmentSummaryPartialList {
  data: BedassignmentSummary[];
  total: number;
}

export class BedassignmentDetail extends BedassignmentSummary {
  patient_id: string;
  bed_id: string;
  bed_room_id: string;
  start_date: number;
  end_date: number;
  admission_form_id: string;
  status:number;
  admission_status: number;
}

export class ExtendedBedassignmentDetail extends BedassignmentDetail {
  priority: number;
  remark: string;
  can_wait: boolean;
  ward_id: string;
  bed_room_level_id: string;
  admission_form_id: string;
  assigned_bed_room_level_id?: string;
  assigned_bed_room_level_name?: string;
  status:number;
  waitingList: WaitinglistDetail;
}

export class BedassignmentDashboard {
  total: number;
}

export class BedData extends TCId {
  assigned: boolean;
}
