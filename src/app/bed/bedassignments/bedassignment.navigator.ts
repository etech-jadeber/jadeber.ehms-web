import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";

import {BedassignmentEditComponent} from "./bedassignment-edit/bedassignment-edit.component";
import {BedassignmentPickComponent} from "./bedassignment-pick/bedassignment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BedassignmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  bedassignmentsUrl(): string {
    return "/bedassignments";
  }

  bedassignmentUrl(id: string): string {
    return "/bedassignments/" + id;
  }

  viewBedassignments(): void {
    this.router.navigateByUrl(this.bedassignmentsUrl());
  }

  viewBedassignment(id: string): void {
    this.router.navigateByUrl(this.bedassignmentUrl(id));
  }

  editBedassignment(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedassignmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }
  changeBed(id: string,change:boolean): MatDialogRef<unknown,any> {
    let params: TcDictionary<string> = new TcDictionary();
    params['mode'] =TCModalModes.EDIT;
    params['change'] = "true";
    const dialogRef = this.dialog.open(BedassignmentEditComponent, {
      data: new TCParentChildIds(null, id, params),
      width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }
  addBedassignment(parentId:string = null,ward_id:string,isWizard:boolean = false,id:string =  null): MatDialogRef<unknown,any> {
    let params: TcDictionary<string> = new TcDictionary();
    params['mode'] = isWizard ? TCModalModes.WIZARD : (id == null ? TCModalModes.NEW : TCModalModes.EDIT);
    const dialogRef = this.dialog.open(BedassignmentEditComponent, {
      data: new TCParentChildIds(parentId, ward_id,params),
      width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }
  
   pickBedassignments(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(BedassignmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}