import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../tc/utils-http";
import {TCId, TcDictionary, TCEnum, TCEnumTranslation} from "../../tc/models";
import {
  BedassignmentDashboard,
  BedassignmentDetail,
  BedassignmentSummaryPartialList,
  BedData,
  ExtendedBedassignmentDetail, PrefixedBedAssignmentPartialList,
  PrefixedBedAssignmentSummary
} from "./bedassignment.model";
import { Admission_status, bed_assignment_status } from '../../app.enums';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from 'src/app/app.translation';


@Injectable({
  providedIn: 'root'
})
export class BedassignmentPersist {

  // bedassignmentSearchText: string = "";

  admissionStatuses: TCEnumTranslation[] = [
    new TCEnumTranslation(Admission_status.admitted, this.appTranslation.getKey('bed', 'admitted')),
    new TCEnumTranslation(Admission_status.readmitted, this.appTranslation.getKey('bed', 'readmitted')),
  ];

  constructor(
    private http: HttpClient,
    private appTranslation: AppTranslation) {
  }

  bedAssignmentSearchHistory = {
    "status": undefined,
    "search_text": "",
    "search_column": "",
}

defaultBedAssignmentSearchHistory = {...this.bedAssignmentSearchHistory}

resetBedAssignmentSearchHistory(){
  this.bedAssignmentSearchHistory = {...this.defaultBedAssignmentSearchHistory}
}

  searchBedassignment(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.bedAssignmentSearchHistory): Observable<BedassignmentSummaryPartialList | PrefixedBedAssignmentPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("bedassignments", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);
    return this.http.get<BedassignmentSummaryPartialList | PrefixedBedAssignmentPartialList>(url);
  }

  filters(searchHistory = this.bedAssignmentSearchHistory): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    fltrs = TCUtilsString.applyFilterParameter(fltrs, searchHistory)
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bedassignments/do", new TCDoParam("download_all", this.filters()));
  }

  bedassignmentDashboard(): Observable<BedassignmentDashboard> {
    return this.http.get<BedassignmentDashboard>(environment.tcApiBaseUri + "bedassignments/dashboard");
  }

  getBedassignment(id: string): Observable<ExtendedBedassignmentDetail> {
    return this.http.get<ExtendedBedassignmentDetail>(environment.tcApiBaseUri + "bedassignments/" + id);
  }

  getBedassignmentByPatient(id: string): Observable<BedassignmentSummaryPartialList> {
    return this.http.get<BedassignmentSummaryPartialList>(environment.tcApiBaseUri + "bedassignments/patient/" + id);
  }

  addBedassignment(item: BedassignmentDetail): Observable<string> {
    return this.http.post<string>(environment.tcApiBaseUri + "bedassignments", item);
  }

  updateBedassignment(item: BedassignmentDetail): Observable<BedassignmentDetail> {
    return this.http.patch<BedassignmentDetail>(environment.tcApiBaseUri + "bedassignments/" + item.id, item);
  }

  deleteBedassignment(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "bedassignments/" + id);
  }

  bedassignmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedassignments/do", new TCDoParam(method, payload));
  }

  bedassignmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedassignments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "bedassignments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "bedassignments/" + id + "/do", new TCDoParam("print", {}));
  }

  // bed_assignment_statuId: number = -1 ;

  bed_assignment_status: TCEnum[] = [
  new TCEnum(bed_assignment_status.active, 'active'),
  new TCEnum(bed_assignment_status.inactive, 'inactive'),
];


}
