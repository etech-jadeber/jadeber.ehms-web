import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomlevelDetailComponent } from './bedroomlevel-detail.component';

describe('BedroomlevelDetailComponent', () => {
  let component: BedroomlevelDetailComponent;
  let fixture: ComponentFixture<BedroomlevelDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomlevelDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomlevelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
