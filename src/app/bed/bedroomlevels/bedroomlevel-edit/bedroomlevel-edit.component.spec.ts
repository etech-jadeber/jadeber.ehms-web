import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomlevelEditComponent } from './bedroomlevel-edit.component';

describe('BedroomlevelEditComponent', () => {
  let component: BedroomlevelEditComponent;
  let fixture: ComponentFixture<BedroomlevelEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomlevelEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomlevelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
