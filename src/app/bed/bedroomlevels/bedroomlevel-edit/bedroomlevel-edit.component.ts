import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';


import { BedroomlevelDetail } from "../bedroomlevel.model";
import { BedroomlevelPersist } from "../bedroomlevel.persist";
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';


@Component({
  selector: 'app-bedroomlevel-edit',
  templateUrl: './bedroomlevel-edit.component.html',
  styleUrls: ['./bedroomlevel-edit.component.css']
})
export class BedroomlevelEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  bedroomlevelDetail: BedroomlevelDetail;
  isPriceValidTemplate: boolean = false

  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<BedroomlevelEditComponent>,
    public persist: BedroomlevelPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("bed_room_levels");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("bed", "bed_room_level");
      this.bedroomlevelDetail = new BedroomlevelDetail();
      this.bedroomlevelDetail.remark = "";
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("bed_room_levels");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("bed", "bed_room_level");
      this.isLoadingResults = true;
      this.persist.getBedroomlevel(this.idMode.id).subscribe(bedroomlevelDetail => {
        this.bedroomlevelDetail = bedroomlevelDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addBedroomlevel(this.bedroomlevelDetail).subscribe(value => {
      this.tcNotification.success("Bedroomlevel added");
      this.bedroomlevelDetail.id = value.id;
      this.dialogRef.close(this.bedroomlevelDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateBedroomlevel(this.bedroomlevelDetail).subscribe(value => {
      this.tcNotification.success("Bedroomlevel updated");
      this.dialogRef.close(this.bedroomlevelDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (!this.isPriceValid(this.bedroomlevelDetail.price_per_day)) {
      this.isPriceValidTemplate = false
      return false;
    }
    if (this.bedroomlevelDetail == null) {
      return false;
    }

    if (this.bedroomlevelDetail.name == null || this.bedroomlevelDetail.name == "") {
      return false;
    }

    if (this.bedroomlevelDetail.remark == null) {
      return false;
    }


    return true;
  }

  isPriceValid(pricePerDay: number) {
    if (isNaN(pricePerDay)) {
      return false;
    }
    return pricePerDay >= 0;
  }


}
