import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomlevelListComponent } from './bedroomlevel-list.component';

describe('BedroomlevelListComponent', () => {
  let component: BedroomlevelListComponent;
  let fixture: ComponentFixture<BedroomlevelListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomlevelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomlevelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
