import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';

import {BedroomlevelPersist} from "../bedroomlevel.persist";
import {BedroomlevelNavigator} from "../bedroomlevel.navigator";

import {BedroomlevelDetail, BedroomlevelSummary, BedroomlevelSummaryPartialList} from "../bedroomlevel.model";
import { TCUtilsNumber } from 'src/app/tc/utils-number';

@Component({
  selector: 'app-bedroomlevel-list',
  templateUrl: './bedroomlevel-list.component.html',
  styleUrls: ['./bedroomlevel-list.component.css']
})
export class BedroomlevelListComponent implements OnInit {

  bedroomlevelsData: BedroomlevelSummary[] = [];
  bedroomlevelsTotalCount: number = 0;
  bedroomlevelsSelectAll:boolean = false;
  bedroomlevelsSelection: BedroomlevelSummary[] = [];

  bedroomlevelsDisplayedColumns: string[] = ["select","action", "name", 'price_per_day',"remark"];
  bedroomlevelsSearchTextBox: FormControl = new FormControl();
  bedroomlevelsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) bedroomlevelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedroomlevelsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public bedroomlevelPersist: BedroomlevelPersist,
                public bedroomlevelNavigator: BedroomlevelNavigator,
                public jobPersist: JobPersist,
                public tcUtilsNumber: TCUtilsNumber,
    ) {

        this.tcAuthorization.requireRead("bed_room_levels");
       this.bedroomlevelsSearchTextBox.setValue(bedroomlevelPersist.bedroomlevelSearchText);
      //delay subsequent keyup events
      this.bedroomlevelsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.bedroomlevelPersist.bedroomlevelSearchText = value;
        this.searchBedroomlevels();
      });
    }

    ngOnInit() {

      this.bedroomlevelsSort.sortChange.subscribe(() => {
        this.bedroomlevelsPaginator.pageIndex = 0;
        this.searchBedroomlevels(true);
      });

      this.bedroomlevelsPaginator.page.subscribe(() => {
        this.searchBedroomlevels(true);
      });
      //start by loading items
      this.searchBedroomlevels();
    }

  searchBedroomlevels(isPagination:boolean = false): void {


    let paginator = this.bedroomlevelsPaginator;
    let sorter = this.bedroomlevelsSort;

    this.bedroomlevelsIsLoading = true;
    this.bedroomlevelsSelection = [];

    this.bedroomlevelPersist.searchBedroomlevel(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BedroomlevelSummaryPartialList) => {
      this.bedroomlevelsData = partialList.data;
      if (partialList.total != -1) {
        this.bedroomlevelsTotalCount = partialList.total;
      }
      this.bedroomlevelsIsLoading = false;
    }, error => {
      this.bedroomlevelsIsLoading = false;
    });

  }

  downloadBedroomlevels(): void {
    if(this.bedroomlevelsSelectAll){
         this.bedroomlevelPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download bedroomlevels", true);
      });
    }
    else{
        this.bedroomlevelPersist.download(this.tcUtilsArray.idsList(this.bedroomlevelsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download bedroomlevels",true);
            });
        }
  }



  addBedroomlevel(): void {
    let dialogRef = this.bedroomlevelNavigator.addBedroomlevel();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBedroomlevels();
      }
    });
  }

  editBedroomlevel(item: BedroomlevelSummary) {
    let dialogRef = this.bedroomlevelNavigator.editBedroomlevel(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBedroomlevel(item: BedroomlevelSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Bedroomlevel");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.bedroomlevelPersist.deleteBedroomlevel(item.id).subscribe(response => {
          this.tcNotification.success("Bedroomlevel deleted");
          this.searchBedroomlevels();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
