import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomlevelPickComponent } from './bedroomlevel-pick.component';

describe('BedroomlevelPickComponent', () => {
  let component: BedroomlevelPickComponent;
  let fixture: ComponentFixture<BedroomlevelPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomlevelPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomlevelPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
