import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import {BedroomlevelDetail, BedroomlevelSummary, BedroomlevelSummaryPartialList} from "../bedroomlevel.model";
import { BedroomlevelPersist } from  "../bedroomlevel.persist";
import { bed_status } from 'src/app/app.enums';

@Component({
  selector: 'app-bedroomlevel-pick',
  templateUrl: './bedroomlevel-pick.component.html',
  styleUrls: ['./bedroomlevel-pick.component.css']
})
export class BedroomlevelPickComponent implements OnInit {

  bedroomLevelsData: BedroomlevelSummary[] = [];
  bedroomLevelsTotalCount: number = 0;
  bedroomLevelsSelection: BedroomlevelSummary[] = [];
  bedroomLevelsDisplayedColumns: string[] = ["select",'name', 'remark'];

  bedroomLevelsSearchTextBox: FormControl = new FormControl();
  bedroomLevelsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) bedroomLevelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedroomLevelsSort: MatSort;

  constructor(
    public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public bedroomLevelPersist: BedroomlevelPersist,
              public dialogRef: MatDialogRef<BedroomlevelPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("bed_room_levels");
    this.bedroomLevelsSearchTextBox.setValue(bedroomLevelPersist.bedroomlevelSearchText);
    //delay subsequent keyup events
    this.bedroomLevelsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.bedroomLevelPersist.bedroomlevelSearchText = value;
      this.searchBedrooms();
    });
    // if (this.pickerData.bedStatus){
    //   this.bedroomLevelPersist.bed_room_status = this.pickerData.bedStatus
    // }
   }

   ngOnInit() {

    this.bedroomLevelsSort.sortChange.subscribe(() => {
      this.bedroomLevelsPaginator.pageIndex = 0;
      this.searchBedrooms();
    });

    this.bedroomLevelsPaginator.page.subscribe(() => {
      this.searchBedrooms();
    });

    //set initial picker list to 5
    this.bedroomLevelsPaginator.pageSize = 5;

    //start by loading items
    this.searchBedrooms();
  }

  searchBedrooms(): void {
    this.bedroomLevelsIsLoading = true;
    this.bedroomLevelsSelection = [];

    this.bedroomLevelPersist.searchBedroomlevel(this.bedroomLevelsPaginator.pageSize,
        this.bedroomLevelsPaginator.pageIndex,
        this.bedroomLevelsSort.active,
        this.bedroomLevelsSort.direction).subscribe((partialList: BedroomlevelSummaryPartialList) => {
      this.bedroomLevelsData = partialList.data;
      if (partialList.total != -1) {
        this.bedroomLevelsTotalCount = partialList.total;
      }
      this.bedroomLevelsIsLoading = false;
    }, error => {
      this.bedroomLevelsIsLoading = false;
    });

  }

  markOneItem(item: BedroomlevelSummary) {
    if(!this.tcUtilsArray.containsId(this.bedroomLevelsSelection,item.id)){
          this.bedroomLevelsSelection = [];
          this.bedroomLevelsSelection.push(item);
        }
        else{
          this.bedroomLevelsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.bedroomLevelsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
