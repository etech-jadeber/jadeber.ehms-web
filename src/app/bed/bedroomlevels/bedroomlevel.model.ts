import { TCId } from "src/app/tc/models";


export class BedroomlevelSummary extends TCId {
  name : string;
remark : string;
price_per_day: number;
nurse_kit_price: number;
}

export class BedroomlevelSummaryPartialList {
  data: BedroomlevelSummary[];
  total: number;
}

export class BedroomlevelDetail extends BedroomlevelSummary {
  name : string;
remark : string;
}

export class BedroomlevelDashboard {
  total: number;
}
