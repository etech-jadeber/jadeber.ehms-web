import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { TCIdMode, TCModalModes } from "src/app/tc/models";
import { TCModalWidths } from "src/app/tc/utils-angular";


import {BedroomlevelEditComponent} from "./bedroomlevel-edit/bedroomlevel-edit.component";
import {BedroomlevelPickComponent} from "./bedroomlevel-pick/bedroomlevel-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BedroomlevelNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  bedroomlevelsUrl(): string {
    return "/bedroomlevels";
  }

  bedroomlevelUrl(id: string): string {
    return "/bedroomlevels/" + id;
  }

  viewBedroomlevels(): void {
    this.router.navigateByUrl(this.bedroomlevelsUrl());
  }

  viewBedroomlevel(id: string): void {
    this.router.navigateByUrl(this.bedroomlevelUrl(id));
  }

  editBedroomlevel(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomlevelEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBedroomlevel(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomlevelEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBedroomlevels(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(BedroomlevelPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}