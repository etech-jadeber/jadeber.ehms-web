import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';

import {BedroomlevelDashboard, BedroomlevelDetail, BedroomlevelSummaryPartialList} from "./bedroomlevel.model";
import { TCDoParam, TCUrlParams, TCUtilsHttp } from 'src/app/tc/utils-http';
import { environment } from 'src/environments/environment';
import { TCId } from 'src/app/tc/models';


@Injectable({
  providedIn: 'root'
})
export class BedroomlevelPersist {

  bedroomlevelSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchBedroomlevel(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BedroomlevelSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("bedroomlevels", this.bedroomlevelSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<BedroomlevelSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = this.bedroomlevelSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bedroomlevels/do", new TCDoParam("download_all", this.filters()));
  }

  bedroomlevelDashboard(): Observable<BedroomlevelDashboard> {
    return this.http.get<BedroomlevelDashboard>(environment.tcApiBaseUri + "bedroomlevels/dashboard");
  }

  getBedroomlevel(id: string): Observable<BedroomlevelDetail> {
    return this.http.get<BedroomlevelDetail>(environment.tcApiBaseUri + "bedroomlevels/" + id);
  }

  addBedroomlevel(item: BedroomlevelDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bedroomlevels/", item);
  }

  updateBedroomlevel(item: BedroomlevelDetail): Observable<BedroomlevelDetail> {
    return this.http.patch<BedroomlevelDetail>(environment.tcApiBaseUri + "bedroomlevels/" + item.id, item);
  }

  deleteBedroomlevel(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "bedroomlevels/" + id);
  }

  bedroomlevelsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedroomlevels/do", new TCDoParam(method, payload));
  }

  bedroomlevelDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedroomlevels/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "bedroomlevels/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "bedroomlevels/" + id + "/do", new TCDoParam("print", {}));
  }


}