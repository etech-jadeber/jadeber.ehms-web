import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';

import {BedroomDetail} from "../bedroom.model";
import {BedroomPersist} from "../bedroom.persist";
import {BedroomNavigator} from "../bedroom.navigator";


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-bedroom-detail',
  templateUrl: './bedroom-detail.component.html',
  styleUrls: ['./bedroom-detail.component.css']
})
export class BedroomDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  bedroomLoading:boolean = false;
  //basics
  bedroomDetail: BedroomDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public bedroomNavigator: BedroomNavigator,
              public  bedroomPersist: BedroomPersist) {
    this.tcAuthorization.requireRead("bed_rooms");
    this.bedroomDetail = new BedroomDetail();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.bedroomLoading = true;
    this.bedroomPersist.getBedroom(id).subscribe(bedroomDetail => {
          this.bedroomDetail = bedroomDetail;
          this.bedroomLoading = false;
        }, error => {
          console.error(error);
          this.bedroomLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.bedroomDetail.id);
  }

  editBedroom(): void {
    let modalRef = this.bedroomNavigator.editBedroom(this.bedroomDetail.id);
    modalRef.afterClosed().subscribe(modifiedBedroomDetail => {
      TCUtilsAngular.assign(this.bedroomDetail, modifiedBedroomDetail);
    }, error => {
      console.error(error);
    });
  }

   printBedroom():void{
      this.bedroomPersist.print(this.bedroomDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print bedroom", true);
      });
    }

  back():void{
      this.location.back();
    }

}