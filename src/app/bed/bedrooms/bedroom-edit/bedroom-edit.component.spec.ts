import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomEditComponent } from './bedroom-edit.component';

describe('BedroomEditComponent', () => {
  let component: BedroomEditComponent;
  let fixture: ComponentFixture<BedroomEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
