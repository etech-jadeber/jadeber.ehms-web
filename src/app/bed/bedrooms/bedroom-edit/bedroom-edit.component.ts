import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { BedroomDetail } from '../bedroom.model';
import { BedroomPersist } from '../bedroom.persist';
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { BedroomlevelSummary } from '../../bedroomlevels/bedroomlevel.model';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { BedroomlevelPersist } from '../../bedroomlevels/bedroomlevel.persist';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-bedroom-edit',
  templateUrl: './bedroom-edit.component.html',
  styleUrls: ['./bedroom-edit.component.css'],
})
export class BedroomEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  bedroomDetail: BedroomDetail = new BedroomDetail();
  bedroomTypes: BedroomtypeSummary[] = [];
  bedroomLevels: BedroomlevelSummary[] = [];
  isPriceValidTemplate = true;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<BedroomEditComponent>,
    public persist: BedroomPersist,
    public bedroomTypePersis: BedroomtypePersist,
    public bedroomLevelsPersis: BedroomlevelPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    this.loadBedroomTypes();
    this.loadBedroomLevels();
    if (this.idMode.mode == TCModalModes.NEW) {
      this.bedroomDetail.status = -1;
      this.tcAuthorization.requireCreate('bed_rooms');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        this.appTranslation.getText('bed', 'bedrooms');
      this.bedroomDetail = new BedroomDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('bed_rooms');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        this.appTranslation.getText('bed', 'bedrooms');
      this.isLoadingResults = true;
      this.persist.getBedroom(this.idMode.id).subscribe(
        (bedroomDetail) => {
          this.bedroomDetail = bedroomDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.bedroomDetail.bed_room_type_id=this.idMode.id;
    this.persist.addBedroom(this.bedroomDetail).subscribe(
      (value) => {
        this.tcNotification.success('Bedroom added');
        this.bedroomDetail.id = value.id;
        this.dialogRef.close(this.bedroomDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateBedroom(this.bedroomDetail).subscribe(
      (value) => {
        this.tcNotification.success('Bedroom updated');
        this.dialogRef.close(this.bedroomDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.bedroomDetail.status === -1) {
      return false;
    }
    if (this.bedroomDetail == null) {
      return false;
    }

    return true;
  }

  isPriceValid(pricePerDay: number) {
    if (isNaN(pricePerDay)) {
      return false;
    }
    return pricePerDay >= 0;
  }

  loadBedroomTypes() {
    this.bedroomTypePersis.searchBedroomtype(50, 0, '', 'asc').subscribe(
      (result) => {
        if (result) {
          this.bedroomTypes = result.data;
        }
      },
      (error) => {}
    );
  }

  loadBedroomLevels() {
    this.bedroomLevelsPersis.searchBedroomlevel(50, 0, '', 'asc').subscribe(
      (result) => {
        if (result) {
          this.bedroomLevels = result.data;
        }
      },
      (error) => {}
    );
  }
}
