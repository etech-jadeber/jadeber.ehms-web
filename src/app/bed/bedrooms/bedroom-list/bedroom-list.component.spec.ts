import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomListComponent } from './bedroom-list.component';

describe('BedroomListComponent', () => {
  let component: BedroomListComponent;
  let fixture: ComponentFixture<BedroomListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
