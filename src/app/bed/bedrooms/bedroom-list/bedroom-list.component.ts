import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';

import { BedroomPersist } from '../bedroom.persist';
import { BedroomNavigator } from '../bedroom.navigator';
import {
  BedroomDetail,
  BedroomSummary,
  BedroomSummaryPartialList,
} from '../bedroom.model';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { BedroomlevelSummary } from '../../bedroomlevels/bedroomlevel.model';
import { BedroomlevelPersist } from '../../bedroomlevels/bedroomlevel.persist';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { BedDetail, BedSummary, BedSummaryPartialList } from '../../beds/bed.model';
import { BedPersist } from '../../beds/bed.persist';
import { BedNavigator } from '../../beds/bed.navigator';
import { bed_status } from 'src/app/app.enums';
import { TCUtilsNumber } from 'src/app/tc/utils-number';

@Component({
  selector: 'app-bedroom-list',
  templateUrl: './bedroom-list.component.html',
  styleUrls: ['./bedroom-list.component.css'],
})
export class BedroomListComponent implements OnInit {
  bedroomsData: BedroomSummary[] = [];
  bedroomsTotalCount: number = 0;
  bedroomsSelectAll: boolean = false;
  bedroomsSelection: BedroomSummary[] = [];
  bedId: string;

  @Input() ward_id:string = "";
  bedroomsDisplayedColumns: string[] = [
    'select',
    'action',
    "no",
    "name",
    'bed_room_level_id',
    'status',
  ];
  bedroomsSearchTextBox: FormControl = new FormControl();
  bedroomsIsLoading: boolean = false;
  bedroomTypes: BedroomtypeSummary[] = [];
  bedroomLevels: BedroomlevelSummary[] = [];

  @ViewChild(MatPaginator, { static: true }) bedroomsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) bedroomsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public bedroomPersist: BedroomPersist,
    public bedroomNavigator: BedroomNavigator,
    public jobPersist: JobPersist,

    public bedPersist: BedPersist,
    public tcUtilsNumber: TCUtilsNumber,
    public bedroomTypePersis: BedroomtypePersist,
    public bedroomLevelPersist: BedroomlevelPersist
  ) {
    this.tcAuthorization.requireRead('bed_rooms');
    this.bedroomsSearchTextBox.setValue(bedroomPersist.bedroomSearchText);
    //delay subsequent keyup events
    this.bedroomsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.bedroomPersist.bedroomSearchText = value;
        this.searchBedrooms();

      });
  }

  ngOnInit() {
    this.loadBedroomTypes();
    this.loadBedroomLevel();
    this.bedroomsSort.sortChange.subscribe(() => {
      this.bedroomsPaginator.pageIndex = 0;
      this.searchBedrooms(true);
    });

    this.bedroomsPaginator.page.subscribe(() => {
      this.searchBedrooms(true);
    });
    //start by loading items
    this.searchBedrooms();

  }

  searchBedrooms(isPagination: boolean = false): void {
    let paginator = this.bedroomsPaginator;
    let sorter = this.bedroomsSort;

    this.bedroomsIsLoading = true;
    this.bedroomsSelection = [];

    this.bedroomPersist.ward_id=this.ward_id;
    this.bedroomPersist
      .searchBedroom(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: BedroomSummaryPartialList) => {
          this.bedroomsData = partialList.data;
          if (partialList.total != -1) {
            this.bedroomsTotalCount = partialList.total;
          }
          this.bedroomsIsLoading = false;
        },
        (error) => {
          this.bedroomsIsLoading = false;
        }
      );
  }


  onBedAdd( bed_room_id:string, serial_id): void {
    let bedDetail:BedDetail=new BedDetail();
    bedDetail.status=bed_status.available;
    bedDetail.bed_room_id=bed_room_id;
    bedDetail.ward_id = this.ward_id;
    bedDetail.serial_id = serial_id;
    this.bedPersist.addBed(bedDetail).subscribe(value => {
      this.tcNotification.success("Bed added");
      this.searchBedrooms();
    }, error => {
    })
  }


  downloadBedrooms(): void {
    if (this.bedroomsSelectAll) {
      this.bedroomPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download bedrooms', true);
      });
    } else {
      this.bedroomPersist
        .download(this.tcUtilsArray.idsList(this.bedroomsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'download bedrooms', true);
        });
    }
  }

  addBedroom(): void {
    let dialogRef = this.bedroomNavigator.addBedroom(this.ward_id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchBedrooms();
      }
    });
  }

  editBedroom(item: BedroomSummary) {
    let dialogRef = this.bedroomNavigator.editBedroom(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteBedroom(item: BedroomSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Bedroom');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.bedroomPersist.deleteBedroom(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Bedroom deleted');
            this.searchBedrooms();
          },
          (error) => {}
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }

  loadBedroomTypes() {
    this.bedroomTypePersis.searchBedroomtype(50, 0, '', 'asc').subscribe(
      (result) => {
        if (result) {
          this.bedroomTypes = result.data;
        }
      },
      (error) => {}
    );
  }

  loadBedroomLevel() {
    this.bedroomLevelPersist.searchBedroomlevel(50, 0, '', 'asc').subscribe(
      (result) => {
        if (result) {
          this.bedroomLevels = result.data;
        }
      },
      (error) => {}
    );
  }

  getBedroomLevelName(bedLevelTypeId: string) {
    const bedroomLevel: BedroomlevelSummary = this.tcUtilsArray.getById(
      this.bedroomLevels,
      bedLevelTypeId
    );
    if (bedroomLevel) {
      return bedroomLevel.name;
    }
    return '';
  }

  getBedroomTypeName(bedRoomTypeId: string) {
    const bedRoomType: BedroomtypeSummary = this.tcUtilsArray.getById(
      this.bedroomTypes,
      bedRoomTypeId
    );
    if (bedRoomType) {
      return bedRoomType.name;
    }
    return '';
  }


}
