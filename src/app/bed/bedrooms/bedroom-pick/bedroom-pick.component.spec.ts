import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomPickComponent } from './bedroom-pick.component';

describe('BedroomPickComponent', () => {
  let component: BedroomPickComponent;
  let fixture: ComponentFixture<BedroomPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
