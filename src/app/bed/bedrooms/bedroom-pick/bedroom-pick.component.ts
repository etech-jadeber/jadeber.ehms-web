import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import {BedroomDetail, BedroomSummary, BedroomSummaryPartialList} from "../bedroom.model";
import {BedroomPersist} from "../bedroom.persist";
import { bed_status } from 'src/app/app.enums';


@Component({
  selector: 'app-bedroom-pick',
  templateUrl: './bedroom-pick.component.html',
  styleUrls: ['./bedroom-pick.component.css']
})
export class BedroomPickComponent implements OnInit {

  bedroomsData: BedroomSummary[] = [];
  bedroomsTotalCount: number = 0;
  bedroomsSelection: BedroomSummary[] = [];
  bedroomsDisplayedColumns: string[] = ["select",'name', 'price_per_day','status','no' ];

  bedroomsSearchTextBox: FormControl = new FormControl();
  bedroomsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) bedroomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedroomsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public bedroomPersist: BedroomPersist,
              public dialogRef: MatDialogRef<BedroomPickComponent>,
              @Inject(MAT_DIALOG_DATA) public pickerData: {selectOne: boolean, wardId: string, levelId: string, bedStatus: number}
  ) {
    this.bedroomPersist.ward_id = this.pickerData.wardId;
    this.tcAuthorization.requireRead("bed_rooms");
    this.bedroomsSearchTextBox.setValue(bedroomPersist.bedroomSearchText);
    //delay subsequent keyup events
    this.bedroomsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.bedroomPersist.bedroomSearchText = value;
      this.searchBedrooms();
    });
    if (this.pickerData.bedStatus){
      this.bedroomPersist.bed_room_status = this.pickerData.bedStatus
      this.bedroomPersist.bed_room_level = this.pickerData.levelId
    }
  }

  ngOnInit() {

    this.bedroomsSort.sortChange.subscribe(() => {
      this.bedroomsPaginator.pageIndex = 0;
      this.searchBedrooms();
    });

    this.bedroomsPaginator.page.subscribe(() => {
      this.searchBedrooms();
    });

    //set initial picker list to 5
    this.bedroomsPaginator.pageSize = 5;

    //start by loading items
    this.searchBedrooms();
  }

  searchBedrooms(): void {
    this.bedroomsIsLoading = true;
    this.bedroomsSelection = [];

    this.bedroomPersist.searchBedroom(this.bedroomsPaginator.pageSize,
        this.bedroomsPaginator.pageIndex,
        this.bedroomsSort.active,
        this.bedroomsSort.direction).subscribe((partialList: BedroomSummaryPartialList) => {
      this.bedroomsData = partialList.data;
      if (partialList.total != -1) {
        this.bedroomsTotalCount = partialList.total;
      }
      this.bedroomsIsLoading = false;
    }, error => {
      this.bedroomsIsLoading = false;
    });

  }

  markOneItem(item: BedroomSummary) {
    if(!this.tcUtilsArray.containsId(this.bedroomsSelection,item.id)){
          this.bedroomsSelection = [];
          this.bedroomsSelection.push(item);
        }
        else{
          this.bedroomsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.bedroomsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}