import { TCId } from "src/app/tc/models";
import { BedDetail } from "../beds/bed.model";

export type Prefixed<T> = {
  [K in keyof T as `bed_room_${string & K}`]: T[K];
};

export type PrefixedBedRoom = Prefixed<BedroomDetail>;

export class BedroomSummary extends TCId {
  id:string;
  bed_room_type_id : string;
bed_room_level_id : string;
status : number;
name : string;
no : number;
beds?: BedDetail[]
}

export class BedroomSummaryPartialList {
  data: BedroomSummary[];
  total: number;
}

export class BedroomDetail extends BedroomSummary {
  bed_room_type_id : string;
bed_room_level_id : string;
status : number;
}

export class BedroomDashboard {
  total: number;
}
