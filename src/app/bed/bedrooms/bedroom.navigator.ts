import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { TCIdMode, TCModalModes } from "src/app/tc/models";
import { TCModalWidths } from "src/app/tc/utils-angular";



import {BedroomEditComponent} from "./bedroom-edit/bedroom-edit.component";
import {BedroomPickComponent} from "./bedroom-pick/bedroom-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BedroomNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  bedroomsUrl(): string {
    return "/bedrooms";
  }

  bedroomUrl(id: string): string {
    return "/bedrooms/" + id;
  }

  viewBedrooms(): void {
    this.router.navigateByUrl(this.bedroomsUrl());
  }

  viewBedroom(id: string): void {
    this.router.navigateByUrl(this.bedroomUrl(id));
  }

  editBedroom(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBedroom(ward_id:string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomEditComponent, {
          data: new TCIdMode(ward_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBedrooms(selectOne: boolean=false, wardId: string = "", levelId: string = "",  bedStatus: number = -1): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(BedroomPickComponent, {
        data: {selectOne, wardId, levelId, bedStatus},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}