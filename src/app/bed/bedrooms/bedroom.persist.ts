import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';


import {BedroomDashboard, BedroomDetail, BedroomSummaryPartialList} from "./bedroom.model";
import { room_status } from 'src/app/app.enums';
import { TcDictionary, TCEnum, TCId } from 'src/app/tc/models';
import { TCDoParam, TCUrlParams, TCUtilsHttp } from 'src/app/tc/utils-http';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BedroomPersist {

  bedroomSearchText: string = "";
  ward_id:string="";
  room_statusId: number;
  bed_room_status: number = -1;
  bed_room_level: string = "";
  room_status: TCEnum[] = [
    new TCEnum(room_status.active, 'active'),
    new TCEnum(room_status.inactive, 'inactive'),
  ];
  statusFilter: string = "-1";


  constructor(private http: HttpClient) {
  }

  searchBedroom(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BedroomSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("bedrooms", this.bedroomSearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "status", this.statusFilter.toString());
    url = TCUtilsString.appendUrlParameter(url, "bed_status", this.bed_room_status.toString())
    url = TCUtilsString.appendUrlParameter(url, "bed_room_type_id", this.ward_id);
    url = TCUtilsString.appendUrlParameter(url, "bed_room_level_id", this.bed_room_level);
    return this.http.get<BedroomSummaryPartialList>(url);

  }

  clearFilters(): void {
    this.bedroomSearchText = "";
    this.statusFilter = "-1";
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.bedroomSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bedrooms/do", new TCDoParam("download_all", this.filters()));
  }

  bedroomDashboard(): Observable<BedroomDashboard> {
    return this.http.get<BedroomDashboard>(environment.tcApiBaseUri + "bedrooms/dashboard");
  }

  getBedroom(id: string): Observable<BedroomDetail> {
    return this.http.get<BedroomDetail>(environment.tcApiBaseUri + "bedrooms/" + id);
  }
  getBedroomBylevelAndType(bed_room_type_id: string,bed_room_level_id:string): Observable<BedroomSummaryPartialList> {
    return this.http.get<BedroomSummaryPartialList>(environment.tcApiBaseUri + "bedrooms/by_type_and_level/" + bed_room_type_id+"/"+bed_room_level_id);
  }
  addBedroom(item: BedroomDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "bedrooms/", item);
  }

  updateBedroom(item: BedroomDetail): Observable<BedroomDetail> {
    return this.http.patch<BedroomDetail>(environment.tcApiBaseUri + "bedrooms/" + item.id, item);
  }

  deleteBedroom(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "bedrooms/" + id);
  }

  bedroomsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedrooms/do", new TCDoParam(method, payload));
  }

  bedroomDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "bedrooms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "bedrooms/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "bedrooms/" + id + "/do", new TCDoParam("print", {}));
  }


}