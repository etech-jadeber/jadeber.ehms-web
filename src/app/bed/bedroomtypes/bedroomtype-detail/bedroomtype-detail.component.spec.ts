import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomtypeDetailComponent } from './bedroomtype-detail.component';

describe('BedroomtypeDetailComponent', () => {
  let component: BedroomtypeDetailComponent;
  let fixture: ComponentFixture<BedroomtypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomtypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomtypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
