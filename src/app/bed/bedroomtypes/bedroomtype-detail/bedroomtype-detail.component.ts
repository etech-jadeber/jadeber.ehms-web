import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';


import { BedroomtypeDetail } from '../bedroomtype.model';
import { BedroomtypePersist } from '../bedroomtype.persist';
import { BedroomtypeNavigator } from '../bedroomtype.navigator';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCNotification } from 'src/app/tc/notification';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';

export enum PaginatorIndexes {}

@Component({
  selector: 'app-bedroomtype-detail',
  templateUrl: './bedroomtype-detail.component.html',
  styleUrls: ['./bedroomtype-detail.component.css'],
})
export class BedroomtypeDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  bedroomtypeLoading: boolean = false;
  //basics
  bedroomtypeDetail: BedroomtypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public bedroomtypeNavigator: BedroomtypeNavigator,
    public bedroomtypePersist: BedroomtypePersist
  ) {
    this.tcAuthorization.requireRead('wards');
    this.bedroomtypeDetail = new BedroomtypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead('wards');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.bedroomtypeLoading = true;
    this.bedroomtypePersist.getBedroomtype(id).subscribe(
      (bedroomtypeDetail) => {
        this.bedroomtypeDetail = bedroomtypeDetail;
        this.bedroomtypeLoading = false;
      },
      (error) => {
        console.error(error);
        this.bedroomtypeLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.bedroomtypeDetail.id);
  }

  editBedroomtype(): void {
    let modalRef = this.bedroomtypeNavigator.editBedroomtype(
      this.bedroomtypeDetail.id
    );
    modalRef.afterClosed().subscribe(
      (modifiedBedroomtypeDetail) => {
        TCUtilsAngular.assign(
          this.bedroomtypeDetail,
          modifiedBedroomtypeDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBedroomtype(): void {
    this.bedroomtypePersist
      .print(this.bedroomtypeDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print bedroomtype', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
