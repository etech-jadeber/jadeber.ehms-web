import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomtypeEditComponent } from './bedroomtype-edit.component';

describe('BedroomtypeEditComponent', () => {
  let component: BedroomtypeEditComponent;
  let fixture: ComponentFixture<BedroomtypeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomtypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomtypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
