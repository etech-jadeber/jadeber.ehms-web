import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import { BedroomtypeDetail } from '../bedroomtype.model';
import { BedroomtypePersist } from '../bedroomtype.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { AppTranslation } from 'src/app/app.translation';
import { TCNotification } from 'src/app/tc/notification';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { StoreSummary } from 'src/app/stores/store.model';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StorePersist } from 'src/app/stores/store.persist';
import { store_type } from 'src/app/app.enums';

@Component({
  selector: 'app-bedroomtype-edit',
  templateUrl: './bedroomtype-edit.component.html',
  styleUrls: ['./bedroomtype-edit.component.css'],
})
export class BedroomtypeEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  bedroomtypeDetail: BedroomtypeDetail;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public storeNavigator: StoreNavigator,
    public dialogRef: MatDialogRef<BedroomtypeEditComponent>,
    public persist: BedroomtypePersist,
    public storePersist: StorePersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('wards');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' +'Ward';
      this.bedroomtypeDetail = new BedroomtypeDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('wards');
      this.title =
        this.appTranslation.getText('general', 'edit') + ' ' + "Ward"
      this.isLoadingResults = true;
      this.persist.getBedroomtype(this.idMode.id).subscribe(
        (bedroomtypeDetail) => {
          this.bedroomtypeDetail = bedroomtypeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addBedroomtype(this.bedroomtypeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Ward added');
        this.bedroomtypeDetail.id = value.id;
        this.dialogRef.close(this.bedroomtypeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateBedroomtype(this.bedroomtypeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Ward updated');
        this.dialogRef.close(this.bedroomtypeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.bedroomtypeDetail == null) {
      return false;
    }

    if (
      this.bedroomtypeDetail.name == null ||
      this.bedroomtypeDetail.name == ''
    ) {
      return false;
    }

    if (
      this.bedroomtypeDetail.remark == null ||
      this.bedroomtypeDetail.remark == ''
    ) {
      return false;
    }

    return true;
  }



  searchStore() {
    this.storePersist.store_typeId = store_type.ward_store;
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      this.storePersist.store_typeId = undefined;
      if (result) {
        this.bedroomtypeDetail.store_name = result[0].name;
        this.bedroomtypeDetail.store_id = result[0].id;
      }
    });
  }
}
