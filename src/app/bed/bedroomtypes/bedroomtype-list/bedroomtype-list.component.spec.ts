import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomtypeListComponent } from './bedroomtype-list.component';

describe('BedroomtypeListComponent', () => {
  let component: BedroomtypeListComponent;
  let fixture: ComponentFixture<BedroomtypeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomtypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomtypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
