import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';



import { BedroomtypePersist } from '../bedroomtype.persist';
import { BedroomtypeNavigator } from '../bedroomtype.navigator';
import {
  BedroomtypeDetail,
  BedroomtypeSummary,
  BedroomtypeSummaryPartialList,
} from '../bedroomtype.model';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';

@Component({
  selector: 'app-bedroomtype-list',
  templateUrl: './bedroomtype-list.component.html',
  styleUrls: ['./bedroomtype-list.component.css'],
})
export class BedroomtypeListComponent implements OnInit {
  bedroomtypesData: BedroomtypeSummary[] = [];
  bedroomtypesTotalCount: number = 0;
  bedroomtypesSelectAll: boolean = false;
  bedroomtypesSelection: BedroomtypeSummary[] = [];

  bedroomtypesDisplayedColumns: string[] = [
    'select',
    'action',
    'name',
    // 'store_name',
    'remark',
  ];
  bedroomtypesSearchTextBox: FormControl = new FormControl();
  bedroomtypesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true })
  bedroomtypesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) bedroomtypesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public bedroomtypePersist: BedroomtypePersist,
    public bedroomtypeNavigator: BedroomtypeNavigator,
    public jobPersist: JobPersist
  ) {
    this.tcAuthorization.requireRead('wards');
    this.bedroomtypesSearchTextBox.setValue(
      bedroomtypePersist.bedroomtypeSearchText
    );
    //delay subsequent keyup events
    this.bedroomtypesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.bedroomtypePersist.bedroomtypeSearchText = value;
        this.searchBedroomtypes();
      });
  }

  ngOnInit() {
    this.bedroomtypesSort.sortChange.subscribe(() => {
      this.bedroomtypesPaginator.pageIndex = 0;
      this.searchBedroomtypes(true);
    });

    this.bedroomtypesPaginator.page.subscribe(() => {
      this.searchBedroomtypes(true);
    });
    //start by loading items
    this.searchBedroomtypes();
  }

  searchBedroomtypes(isPagination: boolean = false): void {
    let paginator = this.bedroomtypesPaginator;
    let sorter = this.bedroomtypesSort;

    this.bedroomtypesIsLoading = true;
    this.bedroomtypesSelection = [];

    this.bedroomtypePersist
      .searchBedroomtype(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: BedroomtypeSummaryPartialList) => {
          this.bedroomtypesData = partialList.data;
          if (partialList.total != -1) {
            this.bedroomtypesTotalCount = partialList.total;
          }
          this.bedroomtypesIsLoading = false;
        },
        (error) => {
          this.bedroomtypesIsLoading = false;
        }
      );
  }

  downloadBedroomtypes(): void {
    if (this.bedroomtypesSelectAll) {
      this.bedroomtypePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download bedroomtypes',
          true
        );
      });
    } else {
      this.bedroomtypePersist
        .download(this.tcUtilsArray.idsList(this.bedroomtypesSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download bedroomtypes',
            true
          );
        });
    }
  }

  addBedroomtype(): void {
    let dialogRef = this.bedroomtypeNavigator.addBedroomtype();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchBedroomtypes();
      }
    });
  }

  editBedroomtype(item: BedroomtypeSummary) {
    let dialogRef = this.bedroomtypeNavigator.editBedroomtype(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteBedroomtype(item: BedroomtypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Bedroomtype');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.bedroomtypePersist.deleteBedroomtype(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Bedroomtype deleted');
            this.searchBedroomtypes();
          },
          (error) => {}
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }
}
