import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedroomtypePickComponent } from './bedroomtype-pick.component';

describe('BedroomtypePickComponent', () => {
  let component: BedroomtypePickComponent;
  let fixture: ComponentFixture<BedroomtypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedroomtypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedroomtypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
