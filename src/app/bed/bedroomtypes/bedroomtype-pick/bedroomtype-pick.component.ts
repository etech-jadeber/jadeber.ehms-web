import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { debounceTime } from 'rxjs/operators';

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import {
  BedroomtypeDetail,
  BedroomtypeSummary,
  BedroomtypeSummaryPartialList,
} from '../bedroomtype.model';
import { BedroomtypePersist } from '../bedroomtype.persist';

@Component({
  selector: 'app-bedroomtype-pick',
  templateUrl: './bedroomtype-pick.component.html',
  styleUrls: ['./bedroomtype-pick.component.css'],
})
export class BedroomtypePickComponent implements OnInit {
  bedroomtypesData: BedroomtypeSummary[] = [];
  bedroomtypesTotalCount: number = 0;
  bedroomtypesSelection: BedroomtypeSummary[] = [];
  bedroomtypesDisplayedColumns: string[] = ['select', 'name', 'remark'];

  bedroomtypesSearchTextBox: FormControl = new FormControl();
  bedroomtypesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) bedroomtypesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) bedroomtypesSort: MatSort;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public bedroomtypePersist: BedroomtypePersist,
    public dialogRef: MatDialogRef<BedroomtypePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('wards');
    this.bedroomtypesSearchTextBox.setValue(
      bedroomtypePersist.bedroomtypeSearchText
    );
    //delay subsequent keyup events
    this.bedroomtypesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.bedroomtypePersist.bedroomtypeSearchText = value;
        this.searchBedroomtypes();
      });
  }

  ngOnInit() {
    this.bedroomtypesSort.sortChange.subscribe(() => {
      this.bedroomtypesPaginator.pageIndex = 0;
      this.searchBedroomtypes();
    });

    this.bedroomtypesPaginator.page.subscribe(() => {
      this.searchBedroomtypes();
    });

    //set initial picker list to 5
    this.bedroomtypesPaginator.pageSize = 5;

    //start by loading items
    this.searchBedroomtypes();
  }

  searchBedroomtypes(): void {
    this.bedroomtypesIsLoading = true;
    this.bedroomtypesSelection = [];

    this.bedroomtypePersist
      .searchBedroomtype(
        this.bedroomtypesPaginator.pageSize,
        this.bedroomtypesPaginator.pageIndex,
        this.bedroomtypesSort.active,
        this.bedroomtypesSort.direction
      )
      .subscribe(
        (partialList: BedroomtypeSummaryPartialList) => {
          this.bedroomtypesData = partialList.data;
          if (partialList.total != -1) {
            this.bedroomtypesTotalCount = partialList.total;
          }
          this.bedroomtypesIsLoading = false;
        },
        (error) => {
          this.bedroomtypesIsLoading = false;
        }
      );
  }

  markOneItem(item: BedroomtypeSummary) {
    if (!this.tcUtilsArray.containsId(this.bedroomtypesSelection, item.id)) {
      this.bedroomtypesSelection = [];
      this.bedroomtypesSelection.push(item);
    } else {
      this.bedroomtypesSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.bedroomtypesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
