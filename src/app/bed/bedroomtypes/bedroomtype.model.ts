import { TCId } from "src/app/tc/models";

export type Prefixed<T> = {
  [K in keyof T as `bed_room_type_${string & K}`]: T[K];
};

export type PrefixedBedRoomType = Prefixed<BedroomtypeDetail>;

export class BedroomtypeSummary extends TCId {
  name: string;
  store_id: string;
  store_name: string;
  remark: string;
}

export class BedroomtypeSummaryPartialList {
  data: BedroomtypeSummary[];
  total: number;
}

export class BedroomtypeDetail extends BedroomtypeSummary {
  name: string;
  remark: string;
}

export class BedroomtypeDashboard {
  total: number;
}
