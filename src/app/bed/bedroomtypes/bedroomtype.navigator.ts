import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCIdMode, TCModalModes } from "src/app/tc/models";
import { TCModalWidths } from "src/app/tc/utils-angular";


import { BedroomtypeEditComponent } from './bedroomtype-edit/bedroomtype-edit.component';
import { BedroomtypePickComponent } from './bedroomtype-pick/bedroomtype-pick.component';

@Injectable({
  providedIn: 'root',
})
export class BedroomtypeNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}

  bedroomtypesUrl(): string {
    return '/bedroomtypes';
  }

  bedroomtypeUrl(id: string): string {
    return '/bedroomtypes/' + id;
  }

  viewBedroomtypes(): void {
    this.router.navigateByUrl(this.bedroomtypesUrl());
  }

  viewBedroomtype(id: string): void {
    this.router.navigateByUrl(this.bedroomtypeUrl(id));
  }

  editBedroomtype(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomtypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addBedroomtype(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomtypeEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickBedroomtypes(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedroomtypePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
