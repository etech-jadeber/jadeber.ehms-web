import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  BedroomtypeDashboard,
  BedroomtypeDetail,
  BedroomtypeSummaryPartialList,
} from './bedroomtype.model';
import { TCDoParam, TCUrlParams, TCUtilsHttp } from 'src/app/tc/utils-http';
import { TcDictionary, TCId } from 'src/app/tc/models';
import { environment } from 'src/environments/environment';
import { TCUtilsString } from 'src/app/tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class BedroomtypePersist {
  bedroomtypeSearchText: string = '';
  bed_room_level_id: string;

  constructor(private http: HttpClient) {}

  searchBedroomtype(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<BedroomtypeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'bedroomtypes',
      this.bedroomtypeSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if (this.bed_room_level_id){
      url = TCUtilsString.appendUrlParameter(url, 'bed_room_level_id', this.bed_room_level_id)
    }
    return this.http.get<BedroomtypeSummaryPartialList>(url);
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.bedroomtypeSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'bedroomtypes/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  bedroomtypeDashboard(): Observable<BedroomtypeDashboard> {
    return this.http.get<BedroomtypeDashboard>(
      environment.tcApiBaseUri + 'bedroomtypes/dashboard'
    );
  }

  getBedroomtype(id: string): Observable<BedroomtypeDetail> {
    return this.http.get<BedroomtypeDetail>(
      environment.tcApiBaseUri + 'bedroomtypes/' + id
    );
  }

  addBedroomtype(item: BedroomtypeDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'bedroomtypes/',
      item
    );
  }

  updateBedroomtype(item: BedroomtypeDetail): Observable<BedroomtypeDetail> {
    return this.http.patch<BedroomtypeDetail>(
      environment.tcApiBaseUri + 'bedroomtypes/' + item.id,
      item
    );
  }

  deleteBedroomtype(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'bedroomtypes/' + id);
  }

  bedroomtypesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'bedroomtypes/do',
      new TCDoParam(method, payload)
    );
  }

  bedroomtypeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'bedroomtypes/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'bedroomtypes/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'bedroomtypes/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
