import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedDetailComponent } from './bed-detail.component';

describe('BedDetailComponent', () => {
  let component: BedDetailComponent;
  let fixture: ComponentFixture<BedDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
