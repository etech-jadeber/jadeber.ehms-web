import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";




import {BedDetail} from "../bed.model";
import {BedPersist} from "../bed.persist";
import {BedNavigator} from "../bed.navigator";
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { AppTranslation } from 'src/app/app.translation';


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-bed-detail',
  templateUrl: './bed-detail.component.html',
  styleUrls: ['./bed-detail.component.css']
})
export class BedDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  bedLoading:boolean = false;
  //basics
  bedDetail: BedDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public bedNavigator: BedNavigator,
              public  bedPersist: BedPersist) {
    this.tcAuthorization.requireRead("beds");
    this.bedDetail = new BedDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("beds");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.bedLoading = true;
    this.bedPersist.getBed(id).subscribe(bedDetail => {
          this.bedDetail = bedDetail;
          this.bedLoading = false;
        }, error => {
          console.error(error);
          this.bedLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.bedDetail.id);
  }

  editBed(): void {
    let modalRef = this.bedNavigator.editBed(this.bedDetail.id);
    modalRef.afterClosed().subscribe(modifiedBedDetail => {
      TCUtilsAngular.assign(this.bedDetail, modifiedBedDetail);
    }, error => {
      console.error(error);
    });
  }

   printBed():void{
      this.bedPersist.print(this.bedDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print bed", true);
      });
    }

  back():void{
      this.location.back();
    }

}
