import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedEditComponent } from './bed-edit.component';

describe('BedEditComponent', () => {
  let component: BedEditComponent;
  let fixture: ComponentFixture<BedEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
