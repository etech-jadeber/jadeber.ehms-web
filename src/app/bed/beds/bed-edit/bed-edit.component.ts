import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { AppTranslation } from 'src/app/app.translation';

import { BedDetail } from "../bed.model";
import { BedPersist } from "../bed.persist";

import { bed_status } from 'src/app/app.enums';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { BedroomNavigator } from '../../bedrooms/bedroom.navigator';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { BedroomSummary } from '../../bedrooms/bedroom.model';


@Component({
  selector: 'app-bed-edit',
  templateUrl: './bed-edit.component.html',
  styleUrls: ['./bed-edit.component.css']
})
export class BedEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  bedDetail: BedDetail;
  bedRoomName:string ="";
  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<BedEditComponent>,
    public persist: BedPersist,
    private bedRoomNavigator:BedroomNavigator,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("beds");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("bed", "beds");
      this.bedDetail = new BedDetail();
      this.bedDetail.status = bed_status.available;
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("beds");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("bed", "beds");
      this.isLoadingResults = true;
      this.persist.getBed(this.idMode.id).subscribe(bedDetail => {
        this.bedDetail = bedDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addBed(this.bedDetail).subscribe(value => {
      this.tcNotification.success("Bed added");
      this.bedDetail.id = value.id;
      this.dialogRef.close(this.bedDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  searchBedroom(){
    let dialogRef = this.bedRoomNavigator.pickBedrooms(true);
    dialogRef.afterClosed().subscribe((result: BedroomSummary[]) => {
      if (result) {
        this.bedRoomName = result[0].name;
        this.bedDetail.bed_room_id = result[0].id;
      }
    });
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateBed(this.bedDetail).subscribe(value => {
      this.tcNotification.success("Bed updated");
      this.dialogRef.close(this.bedDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.bedDetail == null|| this.bedDetail.status == -1 ) {
      return false;
    }

    return true;
  }




}
