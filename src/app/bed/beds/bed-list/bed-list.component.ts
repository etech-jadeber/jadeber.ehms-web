import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';



import {BedPersist} from "../bed.persist";
import {BedNavigator} from "../bed.navigator";
import {BedDetail, BedSummary, BedSummaryPartialList} from "../bed.model";
import { BedroomSummary, BedroomSummaryPartialList } from '../../bedrooms/bedroom.model';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';
import { BedroomtypeNavigator } from '../../bedroomtypes/bedroomtype.navigator';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-bed-list',
  templateUrl: './bed-list.component.html',
  styleUrls: ['./bed-list.component.css']
})
export class BedListComponent implements OnInit {

  bedroomsData: BedroomSummary[] = [];
  bedsData: BedSummary[] = [];
  wardName:string = "";
  bedsTotalCount: number = 0;
  bedsSelectAll:boolean = false;
  bedsSelection: BedSummary[] = [];
  bedRoom: BedroomSummary [] = [];
  @Input() bed_room_id:string='';


  bedsDisplayedColumns: string[] = ["select","action", "serial_id","status",];
  bedsSearchTextBox: FormControl = new FormControl();
  bedsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) bedsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                private bedRoomPersist:BedroomPersist,
                public bedPersist: BedPersist,
                private bedRoomType:BedroomtypeNavigator,
                public bedNavigator: BedNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("beds");
       this.bedsSearchTextBox.setValue(bedPersist.bedSearchHistory.search_text);
      //delay subsequent keyup events
      this.bedsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.bedPersist.bedSearchHistory.search_text = value;
        this.searchBeds();
      });
    }

    ngOnInit() {

      this.bedsSort.sortChange.subscribe(() => {
        this.bedsPaginator.pageIndex = 0;
        this.searchBeds(true);
      });

      this.bedsPaginator.page.subscribe(() => {
        this.searchBeds(true);
      });
      //start by loading items
      this.searchBeds();
      this.loadBedRooms();
      console.log("....",this.bed_room_id);
    }

  searchBeds(isPagination:boolean = false): void {


    let paginator = this.bedsPaginator;
    let sorter = this.bedsSort;

    this.bedsIsLoading = true;
    this.bedsSelection = [];
    this.bedPersist.bedSearchHistory.bed_room_id=this.bed_room_id;
    this.bedPersist.searchBed(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BedSummaryPartialList) => {
      this.bedsData = partialList.data;
      console.log("####",this.bedsData)
      if (partialList.total != -1) {
        this.bedsTotalCount = partialList.total;
      }
      this.bedsIsLoading = false;
    }, error => {
      this.bedsIsLoading = false;
    });

  }

  downloadBeds(): void {
    if(this.bedsSelectAll){
         this.bedPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download beds", true);
      });
    }
    else{
        this.bedPersist.download(this.tcUtilsArray.idsList(this.bedsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download beds",true);
            });
        }
  }



  addBed(): void {
    let dialogRef = this.bedNavigator.addBed();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBeds();
      }
    });
  }

  editBed(item: BedSummary) {
    let dialogRef = this.bedNavigator.editBed(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBed(item: BedSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Bed");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.bedPersist.deleteBed(item.id).subscribe(response => {
          this.tcNotification.success("Bed deleted");
          this.searchBeds();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }


    loadBedRooms() {
      this.bedRoomPersist
        .bedroomsDo("get_all_rooms",{})
        .subscribe((result) => {
          this.bedRoom =  (result as BedroomSummaryPartialList).data;
        });
    }

    searchBedType():void{
      this.bedRoomType.pickBedroomtypes(true).afterClosed().subscribe(
        (bedRoomTypes:BedroomtypeSummary[])=>{
          this.bedPersist.bedSearchHistory.ward_id = bedRoomTypes[0].id;
          this.searchBeds();
          this.wardName = bedRoomTypes[0].name;
        }
      )
    }

}
