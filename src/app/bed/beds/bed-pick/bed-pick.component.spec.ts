import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BedPickComponent } from './bed-pick.component';

describe('BedPickComponent', () => {
  let component: BedPickComponent;
  let fixture: ComponentFixture<BedPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BedPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
