import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";


import {BedDetail, BedSummary, BedSummaryPartialList} from "../bed.model";
import {BedPersist} from "../bed.persist";
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';


@Component({
  selector: 'app-bed-pick',
  templateUrl: './bed-pick.component.html',
  styleUrls: ['./bed-pick.component.css']
})
export class BedPickComponent implements OnInit {

  bedsData: BedSummary[] = [];
  bedsTotalCount: number = 0;
  bedsSelection: BedSummary[] = [];
  bedsDisplayedColumns: string[] = ["select", 'serial_id','bed_room_id','status' ];

  bedsSearchTextBox: FormControl = new FormControl();
  bedsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) bedsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bedsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public bedPersist: BedPersist,
              public bedRoomPersist: BedroomPersist,
              public dialogRef: MatDialogRef<BedPickComponent>,
              @Inject(MAT_DIALOG_DATA) public pickerData: {selectOne: boolean, bedroom_id: string, bedStatus: number}
  ) {
    this.tcAuthorization.requireRead("beds");
    this.bedsSearchTextBox.setValue(bedPersist.bedSearchHistory.search_text);
    //delay subsequent keyup events
    this.bedsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.bedPersist.bedSearchHistory.search_text = value;
      this.searchBeds();
    });
    if (pickerData.bedStatus){
      this.bedPersist.bedSearchHistory.bed_room_id = pickerData.bedroom_id;
      this.bedPersist.bedSearchHistory.status = pickerData.bedStatus.toString();
    }
  }

  ngOnInit() {

    this.bedsSort.sortChange.subscribe(() => {
      this.bedsPaginator.pageIndex = 0;
      this.searchBeds();
    });

    this.bedsPaginator.page.subscribe(() => {
      this.searchBeds();
    });

    //set initial picker list to 5
    this.bedsPaginator.pageSize = 5;

    //start by loading items
    this.searchBeds();
  }

  searchBeds(): void {
    this.bedsIsLoading = true;
    this.bedsSelection = [];

    this.bedPersist.searchBed(
      this.bedsPaginator.pageSize,
        this.bedsPaginator.pageIndex,
        this.bedsSort.active,
        this.bedsSort.direction).subscribe((partialList: BedSummaryPartialList) => {
      this.bedsData = partialList.data;
      this.bedsData.forEach(value => {
        this.bedRoomPersist.getBedroom(value.bed_room_id).subscribe(bedroom => {
          value.bed_room_name = bedroom.name
        })
      })
      if (partialList.total != -1) {
        this.bedsTotalCount = partialList.total;
      }
      this.bedsIsLoading = false;
    }, error => {
      this.bedsIsLoading = false;
    });

  }

  markOneItem(item: BedSummary) {
    if(!this.tcUtilsArray.containsId(this.bedsSelection,item.id)){
          this.bedsSelection = [];
          this.bedsSelection.push(item);
        }
        else{
          this.bedsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.bedsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
