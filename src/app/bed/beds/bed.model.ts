import { TCId } from "src/app/tc/models";

export type Prefixed<T> = {
  [K in keyof T as `bed_${string & K}`]: T[K];
};

export type PrefixedBed = Prefixed<BedDetail>;

export class BedSummary extends TCId {
  serial_id : number;
bed_room_id : string;
bed_room_name : string;
status : number;
ward:string;
ward_id: string;
}

export class BedSummaryPartialList {
  data: BedSummary[];
  total: number;
}

export class BedDetail extends BedSummary {
  ward_id: string;
  bed_room_id : string;
  status : number;
}

export class BedDashboard {
  total: number;
  available_beds:number;
  total_beds:number;

}