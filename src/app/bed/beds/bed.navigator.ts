import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {BedEditComponent} from "./bed-edit/bed-edit.component";
import {BedPickComponent} from "./bed-pick/bed-pick.component";
import { TCIdMode, TCModalModes } from "src/app/tc/models";
import { TCModalWidths } from "src/app/tc/utils-angular";


@Injectable({
  providedIn: 'root'
})

export class BedNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  bedsUrl(): string {
    return "/beds";
  }

  bedUrl(id: string): string {
    return "/beds/" + id;
  }

  viewBeds(): void {
    this.router.navigateByUrl(this.bedsUrl());
  }

  viewBed(id: string): void {
    this.router.navigateByUrl(this.bedUrl(id));
  }

  editBed(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBed(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BedEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBeds(selectOne: boolean=false, bedroom_id: string = "", bedStatus: number = -1): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(BedPickComponent, {
        data: {selectOne, bedroom_id, bedStatus},
        width: TCModalWidths.medium
      });
      return dialogRef;
    }
}