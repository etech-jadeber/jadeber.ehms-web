import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';



import {BedDashboard, BedDetail, BedSummaryPartialList} from "./bed.model";

import { BedroomtypeSummaryPartialList } from '../bedroomtypes/bedroomtype.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { AppTranslation } from 'src/app/app.translation';
import { TCDoParam, TCUrlParams, TCUtilsHttp } from 'src/app/tc/utils-http';
import { environment } from 'src/environments/environment';
import { TCEnumTranslation, TCId } from 'src/app/tc/models';
import {bed_assignment_status, bed_status} from 'src/app/app.enums';


@Injectable({
  providedIn: 'root'
})
export class BedPersist {
  // ward_id:string = TCUtilsString.getInvalidId();
  // bedSearchText: string = "";
  // statusFilter: string = "-1";
  // bed_room_id:string=TCUtilsString.getInvalidId();

  constructor(private http: HttpClient,
    public appTranslation: AppTranslation) {
  }

  bedSearchHistory = {
    "ward_id": "",
    "bed_room_id": "",
    "status": "",
    "wardName": "",
    "search_text": "",
    "search_column": "",
}

defaultBedSearchHistory = {...this.bedSearchHistory}

resetBedSearchHistory(){
  this.bedSearchHistory = {...this.defaultBedSearchHistory}
}

  searchBed(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.bedSearchHistory): Observable<BedSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("beds", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url,searchHistory);
    return this.http.get<BedSummaryPartialList>(url);

  }

  allBeds(): Observable<BedSummaryPartialList> {
    return this.http.get<BedSummaryPartialList>(environment.tcApiBaseUri + "beds");

  }

  clearFilters(): void {
    // this.bedSearchText = "";
    // this.statusFilter = "-1";
  }

  filters(searchHistory = this.bedSearchHistory): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "beds/do", new TCDoParam("download_all", this.filters()));
  }

  bedDashboard(): Observable<BedDashboard> {
    return this.http.get<BedDashboard>(environment.tcApiBaseUri + "beds/dashboard");
  }

  getBed(id: string): Observable<BedDetail> {
    return this.http.get<BedDetail>(environment.tcApiBaseUri + "beds/" + id);
  }

  getBedByRoom(room_id: string): Observable<BedSummaryPartialList> {
    return this.http.get<BedSummaryPartialList>(environment.tcApiBaseUri + "beds/by_room/" + room_id);
  }

  addBed(item: BedDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "beds/", item);
  }

  updateBed(item: BedDetail): Observable<BedDetail> {
    return this.http.patch<BedDetail>(environment.tcApiBaseUri + "beds/" + item.id, item);
  }

  deleteBed(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "beds/" + id);
  }

  nurseRecordListSearchHistory = {
    "ward_id": "",
    "wardName": "",
    "search_text": "",
    "search_column": "",
    "status": bed_assignment_status.active,
}

defaultNurseRecordListSearchHistory = {...this.nurseRecordListSearchHistory}

resetNurseRecordListSearchHistory(){
  this.nurseRecordListSearchHistory = {...this.defaultNurseRecordListSearchHistory}
}

  bedsDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.nurseRecordListSearchHistory): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("beds/do", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  bedDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "beds/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "beds/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "beds/" + id + "/do", new TCDoParam("print", {}));
  }


  bed_statuId: number ;

  bed_status: TCEnumTranslation[] = [
    new TCEnumTranslation(bed_status.maintainance, this.appTranslation.getKey('bed', 'maintenance')),
    new TCEnumTranslation(bed_status.occupied, this.appTranslation.getKey('bed', 'occupied')),
    new TCEnumTranslation(bed_status.available, this.appTranslation.getKey('bed', 'available')),
  ];

}
