import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WaitinglistDetailComponent } from './waitinglist-detail.component';

describe('WaitinglistDetailComponent', () => {
  let component: WaitinglistDetailComponent;
  let fixture: ComponentFixture<WaitinglistDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitinglistDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitinglistDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
