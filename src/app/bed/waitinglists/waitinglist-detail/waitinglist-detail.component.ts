import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../../tc/authorization';
import { TCUtilsAngular } from '../../../tc/utils-angular';
import { TCNotification } from '../../../tc/notification';
import { TCUtilsArray } from '../../../tc/utils-array';
import { TCNavigator } from '../../../tc/navigator';
import { JobPersist } from '../../../tc/jobs/job.persist';
import { AppTranslation } from '../../../app.translation';

import { WaitinglistDetail } from '../waitinglist.model';
import { WaitinglistPersist } from '../waitinglist.persist';
import { WaitinglistNavigator } from '../waitinglist.navigator';

export enum PaginatorIndexes {}

@Component({
  selector: 'app-waitinglist-detail',
  templateUrl: './waitinglist-detail.component.html',
  styleUrls: ['./waitinglist-detail.component.css'],
})
export class WaitinglistDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  waitinglistLoading: boolean = false;
  //basics
  waitinglistDetail: WaitinglistDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public waitinglistNavigator: WaitinglistNavigator,
    public waitinglistPersist: WaitinglistPersist
  ) {
    this.tcAuthorization.requireRead('waiting_lists');
    this.waitinglistDetail = new WaitinglistDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead('waiting_lists');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.waitinglistLoading = true;
    this.waitinglistPersist.getWaitinglist(id).subscribe(
      (waitinglistDetail) => {
        this.waitinglistDetail = waitinglistDetail;
        this.waitinglistLoading = false;
      },
      (error) => {
        console.error(error);
        this.waitinglistLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.waitinglistDetail.id);
  }

  editWaitinglist(): void {
    let modalRef = this.waitinglistNavigator.editWaitinglist(
      this.waitinglistDetail.id
    );
    modalRef.afterClosed().subscribe(
      (modifiedWaitinglistDetail) => {
        TCUtilsAngular.assign(
          this.waitinglistDetail,
          modifiedWaitinglistDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printWaitinglist(): void {
    this.waitinglistPersist
      .print(this.waitinglistDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print waitinglist', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
