import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WaitinglistEditComponent } from './waitinglist-edit.component';

describe('WaitinglistEditComponent', () => {
  let component: WaitinglistEditComponent;
  let fixture: ComponentFixture<WaitinglistEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitinglistEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitinglistEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
