import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from '../../../tc/authorization';
import {TCNotification} from '../../../tc/notification';
import {TCUtilsString} from '../../../tc/utils-string';
import {AppTranslation} from '../../../app.translation';

import {TCIdMode, TCModalModes} from '../../../tc/models';

import {WaitinglistDetail} from '../waitinglist.model';
import {WaitinglistPersist} from '../waitinglist.persist';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {PatientPersist} from 'src/app/patients/patients.persist';
import {PatientSummary} from 'src/app/patients/patients.model';
import {isMoment, Moment} from 'moment';
import {PatientNavigator} from 'src/app/patients/patients.navigator';

import {Admission_FormNavigator} from "../../../form_encounters/admission_forms/admission_form.navigator";
import {Admission_FormSummary} from "../../../form_encounters/admission_forms/admission_form.model";
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { BedroomlevelSummary } from '../../bedroomlevels/bedroomlevel.model';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { BedroomlevelPersist } from '../../bedroomlevels/bedroomlevel.persist';
import { BedroomNavigator } from '../../bedrooms/bedroom.navigator';
import { BedroomSummary } from '../../bedrooms/bedroom.model';
import { bed_status, waiting_status } from 'src/app/app.enums';
import { BedNavigator } from '../../beds/bed.navigator';
import { BedSummary } from '../../beds/bed.model';
import { BedroomtypeNavigator } from '../../bedroomtypes/bedroomtype.navigator';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';
import { BedPersist } from '../../beds/bed.persist';
import { BedroomlevelNavigator } from '../../bedroomlevels/bedroomlevel.navigator';
import { ExtendedBedassignmentDetail } from '../../bedassignments/bedassignment.model';
import { BedassignmentPersist } from '../../bedassignments/bedassignment.persist';

@Component({
  selector: 'app-waitinglist-edit',
  templateUrl: './waitinglist-edit.component.html',
  styleUrls: ['./waitinglist-edit.component.css'],
})
export class WaitinglistEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  waitinglistDetail: WaitinglistDetail = new WaitinglistDetail();
  start_date: Date = new Date();
  end_date: Date = new Date();
  patientFullName = '';
  bedroomTypes: BedroomtypeSummary[] = [];
  bedroomLevels: BedroomlevelSummary[] = [];
  admissionSerialNumber: string;
  bedroom: string;
  bed: number;
  ward: string;
  isWaiting: boolean = false;
  level: string;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<WaitinglistEditComponent>,
    public persist: WaitinglistPersist,
    public patientNavigator: PatientNavigator,
    public bedroomTypePersis: BedroomtypePersist,
    public bedroomTypeNavigator: BedroomtypeNavigator,
    public bedroomLevelsPersist: BedroomlevelPersist,
    public bedroomLevelsNavigator: BedroomlevelNavigator,
    public admissionFromNavigator: Admission_FormNavigator,
    public bedroomNavigator: BedroomNavigator,
    public bedNavigator: BedNavigator,
    public bedPersist: BedPersist,
    public bedroomPersist: BedroomPersist,
    public bedAssignmentPersist: BedassignmentPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: {id: string, mode: string, isAdmission: boolean},
    public patientPersist: PatientPersist
  ) {
    this.persist.isAdmissionForm = this.idMode.isAdmission;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.waitinglistDetail.priority = -1;
      this.tcAuthorization.requireCreate('waiting_lists');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        this.appTranslation.getText('patient', 'waiting_lists');
      this.waitinglistDetail = new WaitinglistDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('waiting_lists');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        this.appTranslation.getText('patient', 'waiting_lists');
      this.isLoadingResults = true;
      this.persist.getWaitinglist(this.idMode.id).subscribe(
        (waitinglistDetail) => {
          this.waitinglistDetail = waitinglistDetail;
          this.waitinglistDetail.status = waiting_status.assigned;
          this.waitinglistDetail.patient_id != this.tcUtilsString.invalid_id && this.patientPersist.getPatient(this.waitinglistDetail.patient_id).subscribe(patient => {
            this.patientFullName = patient.fname + " " + patient.mname + " " + patient.lname
          })
          this.waitinglistDetail.bed_room_level_id != this.tcUtilsString.invalid_id && this.bedroomLevelsPersist.getBedroomlevel(this.waitinglistDetail.bed_room_level_id).subscribe(bedLevel => {
            this.level = bedLevel.name
          })
          this.waitinglistDetail.bed_room_type_id != this.tcUtilsString.invalid_id && this.bedroomTypePersis.getBedroomtype(waitinglistDetail.bed_room_type_id).subscribe(
            result => this.ward = result.name
          )
          this.waitinglistDetail.bed_room_id != this.tcUtilsString.invalid_id && this.bedroomPersist.getBedroom(waitinglistDetail.bed_room_id).subscribe(
            result => this.bedroom = result.name
          )
          this.waitinglistDetail.bed_id != this.tcUtilsString.invalid_id && this.bedPersist.getBed(waitinglistDetail.bed_id).subscribe(
            result => this.bed = result.serial_id
          )
          this.transformDates(false);
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  ngOnDestroy(){
    this.persist.isAdmissionForm = false;
  }

  searchWard(): void {
    let dialogRef = this.bedroomTypeNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
      if (result){
        if (result[0].name !== this.ward){
          this.bedroom = "";
          this.waitinglistDetail.bed_room_id = this.tcUtilsString.invalid_id;
          this.bed = undefined;
          this.waitinglistDetail.bed_id = this.tcUtilsString.invalid_id;
        }
        this.ward = result[0].name;
        this.waitinglistDetail.bed_room_type_id = result[0].id;
      }
    }) 
  }

  searchBedroom(): void {
    let dialogRef = this.bedroomNavigator.pickBedrooms(true, this.waitinglistDetail.bed_room_type_id, this.waitinglistDetail.bed_room_level_id, this.isWaiting ? -1 : bed_status.available);
    dialogRef.afterClosed().subscribe((result: BedroomSummary[]) => {
      if (result) {
        if (result[0].name !== this.bedroom){
          this.bed = undefined;
          this.waitinglistDetail.bed_id = this.tcUtilsString.invalid_id;
        }
        this.bedroom = result[0].name;
        this.waitinglistDetail.bed_room_id = result[0].id;
      }
    });
  }

  searchBed(): void {
    let dialogRef = this.bedNavigator.pickBeds(true, this.waitinglistDetail.bed_room_id, bed_status.available);
    dialogRef.afterClosed().subscribe((result: BedSummary[]) => {
      if (result) {
        this.bed = result[0].serial_id;
        this.waitinglistDetail.bed_id = result[0].id;
      }
    });
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.addWaitinglist(this.waitinglistDetail).subscribe(
      (value) => {
        this.waitinglistDetail.id = value.id;
        if (this.waitinglistDetail.status == waiting_status.assigned){
          this.assignBed()
        } else {
          this.tcNotification.success('Waitinglist added');
          this.dialogRef.close(this.waitinglistDetail)
        }
      },
      (error) => {
        this.isLoadingResults = false;
        this.dialogRef.close(this.waitinglistDetail)
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    // this.persist.updateWaitinglist(this.waitinglistDetail).subscribe(
    //   (value) => {
    //     if (this.waitinglistDetail.status == waiting_status.assigned){
    //       this.assignBed()
    //     } else {
    //       this.tcNotification.success('Waitinglist updated');
    //       this.dialogRef.close(this.waitinglistDetail)
    //     }
    //   },
    //   (error) => {
    //     this.isLoadingResults = false;
    //     this.dialogRef.close(this.waitinglistDetail)
    //   }
    // );
    this.assignBed()
  }

  transformDates(todate: boolean) {
    if (todate) {
      this.waitinglistDetail.start_date = this.tcUtilsDate.toTimeStamp(
        this.start_date
      );
      this.waitinglistDetail.end_date = this.tcUtilsDate.toTimeStamp(
        this.end_date
      );
    } else {
      this.start_date = this.tcUtilsDate.toDate(
        this.waitinglistDetail.start_date
      );
      this.end_date = this.tcUtilsDate.toDate(this.waitinglistDetail.end_date);
    }
  }

  isDateGreater(one: Date | Moment, two: Date | Moment): boolean {
    // moment is thrown when user clicks the time picker, need to change to date to compare.
    if (!(one instanceof Date)) {
      one = one.toDate();
    }
    if (!(two instanceof Date)) {
      two = two.toDate();
    }
    one.setHours(0, 0, 0, 0);
    two.setHours(0, 0, 0, 0);
    return one > two;
  }

  canSubmit(): boolean {
    if (this.isNew() && this.tcUtilsDate.isPast(this.start_date)) {
      return false;
    }
    if (this.waitinglistDetail == null) {
      return false;
    }
    if (
      this.waitinglistDetail.bed_room_type_id == undefined ||
      this.waitinglistDetail.bed_room_type_id == this.tcUtilsString.invalid_id ||
      this.waitinglistDetail.bed_room_id == this.tcUtilsString.invalid_id ||
      this.waitinglistDetail.bed_room_id == undefined ||
      this.waitinglistDetail.bed_room_level_id == this.tcUtilsString.invalid_id ||
      this.waitinglistDetail.bed_room_level_id == undefined
    ) {
      return false;
    }
    if (this.isDateGreater(this.start_date, this.end_date)) {
      return false;
    }
    if (this.isWaiting){
      if (this.waitinglistDetail.priority == -1) {
        return false;
      }
    } else {
      if (this.waitinglistDetail.bed_id == undefined || this.waitinglistDetail.bed_id == this.tcUtilsString.invalid_id){
        return false;
      }
    }
    if (
      this.waitinglistDetail.remark == null ||
      this.waitinglistDetail.remark == ''
    ) {
      return false;
    }

    return true;
  }

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.waitinglistDetail.patient_id = result[0].uuidd;
      }
    });
  }

  searchAdmission() {
    let dialogRef = this.admissionFromNavigator.pickAdmission_Forms(true);
    dialogRef.afterClosed().subscribe((result: Admission_FormSummary) => {
      if (result) {
        this.admissionSerialNumber = result[0].serial_id.toString();
        this.waitinglistDetail.admission_form_id = result[0].id;
      }
    });
  }

  cancelWaiting(){
    this.waitinglistDetail.status = waiting_status.canceled;
    this.waitinglistDetail.bed_room_id = this.tcUtilsString.invalid_id;
    this.waitinglistDetail.bed_id = this.tcUtilsString.invalid_id;
    this.onUpdate()
  }

  updateWaiting() {
    if(this.isWaiting){
      this.waitinglistDetail.bed_id = this.tcUtilsString.invalid_id;
      this.waitinglistDetail.status = waiting_status.waiting
    } else {
      this.waitinglistDetail.status = waiting_status.assigned
    }
  }

  searchLevel() {
    let dialogRef = this.bedroomLevelsNavigator.pickBedroomlevels(true);
    dialogRef.afterClosed().subscribe((result: BedroomlevelSummary[]) => {
      if (result) {
        if (result[0].name !== this.level){
          this.bedroom = "";
          this.waitinglistDetail.bed_room_id = this.tcUtilsString.invalid_id;
          this.bed = undefined;
          this.waitinglistDetail.bed_id = this.tcUtilsString.invalid_id;
        }
        this.level = result[0].name;
        this.waitinglistDetail.bed_room_level_id = result[0].id
      }
    });
  }

  assignBed(): void {
    const bedassignmentDetail: ExtendedBedassignmentDetail = {...this.waitinglistDetail, can_wait : false, admission_status: undefined, waitingList: this.waitinglistDetail};
    this.bedAssignmentPersist.addBedassignment(bedassignmentDetail).subscribe(value => {
      if (value){
        this.tcNotification.success(value)
      }
      // if (value.assigned == true) {
      //   this.tcNotification.success("Bedassignment added");

      // } else if (value.id != null) {
      //   this.tcNotification.success("bed unavalaible");
      // } else {
      //   this.tcNotification.success("waiting room assigned");
      // }
      this.dialogRef.close(this.waitinglistDetail);
    }, (error) => {
      this.isLoadingResults = false;
    })
  }
}
