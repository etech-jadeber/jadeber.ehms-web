import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WaitinglistListComponent } from './waitinglist-list.component';

describe('WaitinglistListComponent', () => {
  let component: WaitinglistListComponent;
  let fixture: ComponentFixture<WaitinglistListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitinglistListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitinglistListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
