import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';

import {debounceTime} from 'rxjs/operators';
import {Location} from '@angular/common';

import {TCAuthorization} from '../../../tc/authorization';
import {TCUtilsAngular} from '../../../tc/utils-angular';
import {TCUtilsArray} from '../../../tc/utils-array';
import {TCUtilsDate} from '../../../tc/utils-date';
import {TCNotification} from '../../../tc/notification';
import {TCNavigator} from '../../../tc/navigator';
import {JobPersist} from '../../../tc/jobs/job.persist';

import {AppTranslation} from '../../../app.translation';

import {WaitinglistPersist} from '../waitinglist.persist';
import {WaitinglistNavigator} from '../waitinglist.navigator';
import {
  WaitinglistDetail,
  WaitinglistSummary,
  WaitinglistSummaryPartialList,
} from '../waitinglist.model';
import {PatientPersist} from 'src/app/patients/patients.persist';
import {
  PatientSummary,
  PatientSummaryPartialList,
} from 'src/app/patients/patients.model';
import { BedroomtypeSummary } from '../../bedroomtypes/bedroomtype.model';
import { BedroomlevelSummary } from '../../bedroomlevels/bedroomlevel.model';
import { BedroomtypePersist } from '../../bedroomtypes/bedroomtype.persist';
import { BedroomlevelPersist } from '../../bedroomlevels/bedroomlevel.persist';
;
import {PatientNavigator} from 'src/app/patients/patients.navigator';
import {waiting_status} from 'src/app/app.enums';
import {Admission_FormPersist} from "../../../form_encounters/admission_forms/admission_form.persist";
import {Admission_FormSummary} from "../../../form_encounters/admission_forms/admission_form.model";
import {Admission_FormNavigator} from "../../../form_encounters/admission_forms/admission_form.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ExtendedBedassignmentDetail } from '../../bedassignments/bedassignment.model';
import { BedassignmentPersist } from '../../bedassignments/bedassignment.persist';
import { BedPersist } from '../../beds/bed.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { BedroomPersist } from '../../bedrooms/bedroom.persist';

@Component({
  selector: 'app-waitinglist-list',
  templateUrl: './waitinglist-list.component.html',
  styleUrls: ['./waitinglist-list.component.css'],
})
export class WaitinglistListComponent implements OnInit {
  waitinglistsData: WaitinglistSummary[] = [];
  waitinglistsTotalCount: number = 0;
  waitinglistsSelectAll: boolean = false;
  waitinglistsSelection: WaitinglistSummary[] = [];
  patients: PatientSummary[] = [];
  bedroomTypes: BedroomtypeSummary[] = [];
  bedroomLevels: BedroomlevelSummary[] = [];
  waitingStatus = waiting_status;
  waitinglistsDisplayedColumns: string[] = [
    'select',
    'action',
    'patient_id',
    'start_date',
    'end_date',
    'bed_room_type_id',
    "bed_room_level",
    'bed_room_id',
    'bed_id',
    'priority',
    'remark',
  ];
  waitinglistsSearchTextBox: FormControl = new FormControl();
  waitinglistsIsLoading: boolean = false;


  @ViewChild(MatPaginator, {static: true})
  waitinglistsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) waitinglistsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public waitinglistPersist: WaitinglistPersist,
    public waitinglistNavigator: WaitinglistNavigator,
    public bedAssignmentPersist: BedassignmentPersist,
    public jobPersist: JobPersist,
    public patientPersist: PatientPersist,
    public patientNavigator: PatientNavigator,
    public admissionFormNavigator: Admission_FormNavigator,
    private admissionFormPersist: Admission_FormPersist,
    public bedroomTypePersis: BedroomtypePersist,
    public bedroomPersist: BedroomPersist,
    public bedPersist: BedPersist,
    public tcUtilsString: TCUtilsString,
    public bedroomLevelPersist: BedroomlevelPersist,
  ) {
    this.tcAuthorization.requireRead('waiting_lists');
    this.waitinglistsSearchTextBox.setValue(
      waitinglistPersist.waitinglistSearchText
    );
    //delay subsequent keyup events
    this.waitinglistsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.waitinglistPersist.waitinglistSearchText = value;
        this.searchWaitinglists();
      });
  }

  ngOnInit() {
    this.waitinglistsSort.sortChange.subscribe(() => {
      this.waitinglistsPaginator.pageIndex = 0;
      this.searchWaitinglists(true);
    });
    this.waitinglistsPaginator.page.subscribe(() => {
      this.searchWaitinglists(true);
    });
    //start by loading items
    this.searchWaitinglists();
    // this.loadPatients();
  }

  getAdmissionForm(value: WaitinglistSummary) {
    this.admissionFormPersist.getAdmission_Form(value.admission_form_id).subscribe(result => {
      value.serial_id = result.serial_id
    }, error => {
      console.error(error);
    });
  }

  getBedroomLevel(value: WaitinglistSummary) {
    this.bedroomLevelPersist.getBedroomlevel(value.bed_room_level_id).subscribe(result => {
      value.bed_room_level_name = result.name
    }, error => {
      console.error(error);
    });
  }

  searchWaitinglists(isPagination: boolean = false): void {
    let paginator = this.waitinglistsPaginator;
    let sorter = this.waitinglistsSort;

    this.waitinglistsIsLoading = true;
    this.waitinglistsSelection = [];

    this.waitinglistPersist
      .searchWaitinglist(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: WaitinglistSummaryPartialList) => {
          this.waitinglistsData = partialList.data;

          this.waitinglistsData.forEach(value => {
            // this.getAdmissionForm(value);
            this.getBedroomLevel(value)
            this.getPatientFullName(value);
            this.getBedroomTypeName(value);
            this.getBedroomName(value)
            this.getBedName(value);
          })

          if (partialList.total != -1) {
            this.waitinglistsTotalCount = partialList.total;
          }
          this.waitinglistsIsLoading = false;
        },
        (error) => {
          this.waitinglistsIsLoading = false;
        }
      );
  }

  downloadWaitinglists(): void {
    if (this.waitinglistsSelectAll) {
      this.waitinglistPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download waitinglists',
          true
        );
      });
    } else {
      this.waitinglistPersist
        .download(this.tcUtilsArray.idsList(this.waitinglistsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download waitinglists',
            true
          );
        });
    }
  }

  // addWaitinglist(): void {
  //   let dialogRef = this.waitinglistNavigator.addWaitinglist();
  //   dialogRef.afterClosed().subscribe((result) => {
  //     if (result) {
  //       this.searchWaitinglists();
  //     }
  //   });
  // }

  editWaitinglist(item: WaitinglistSummary) {

    let dialogRef = this.waitinglistNavigator.editWaitinglist(item.id);
    dialogRef.afterClosed().subscribe((result: WaitinglistSummary) => {
      if (result) {
        this.searchWaitinglists();
      }
    });
  }

  deleteWaitinglist(item: WaitinglistSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Waitinglist');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.waitinglistPersist.deleteWaitinglist(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Waitinglist deleted');
            this.searchWaitinglists();
          },
          (error) => {
          }
        );
      }
    });
  }

  // assignBed(item: WaitinglistSummary): void {
  //   const bedassignmentDetail: ExtendedBedassignmentDetail = new ExtendedBedassignmentDetail();
  //   bedassignmentDetail.priority = item.priority;
  //   bedassignmentDetail.remark = item.remark;
  //   bedassignmentDetail.can_wait = false;
  //   bedassignmentDetail.bed_room_type_id = item.bed_room_type_id;
  //   bedassignmentDetail.bed_room_id = item.bed_room_id;
  //   bedassignmentDetail.admission_form_id = item.admission_form_id;
  //   bedassignmentDetail.status = item.status;

  //   this.bedAssignmentPersist.addBedassignment(bedassignmentDetail).subscribe(value => {
  //     if (value.assigned == true) {
  //       this.tcNotification.success("Bedassignment added");

  //     } else if (value.id != null) {
  //       this.tcNotification.success("bed unavalaible");
  //     } else {
  //       this.tcNotification.success("waiting room assigned");
  //     }
  //     this.searchWaitinglists();
  //     bedassignmentDetail.id = value.id;
  //   }, error => {
  //     this.searchWaitinglists();
  //   })

  //   // this.waitinglistPersist.waitinglistDo(item.id, "assign", item).subscribe(value => {
  //   //   this.tcNotification.success("Patient dispatched");
  //   //   this.searchWaitinglists();
  //   // }, error => {
  //   //   console.error(error);
  //   // })
  // }

  cancelBed(item: WaitinglistSummary): void {
    this.waitinglistPersist.waitinglistDo(item.id, "cancel", item).subscribe(value => {
      this.tcNotification.success("Patient status updated to canceled");
      this.searchWaitinglists();
    }, error => {
      console.error(error);
    })
  }

  back(): void {
    this.location.back();
  }

  getPatientFullName(value: WaitinglistSummary) {
    this.patientPersist.getPatient(value.patient_id).subscribe(result => {
      value.patient_name = result.fname + ' ' + result.mname + ' ' + result.lname;
    }, error => {
      console.error(error);
    });
  }

  getBedName(value: WaitinglistSummary) {
    value.bed_id !== this.tcUtilsString.invalid_id && this.bedPersist.getBed(value.bed_id).subscribe((result) => {
      value.bed_no = result.serial_id;
    }, error => {
      console.log(error)
    })
  }

  getBedroomTypeName(value: WaitinglistSummary) {
    this.bedroomTypePersis.getBedroomtype(value.bed_room_type_id).subscribe((result) => {
      value.bed_room_type_name = result.name;
    }, error => {
      console.log(error)
    })
 }
 getBedroomName(value: WaitinglistSummary){
  value.bed_id !== this.tcUtilsString.invalid_id && this.bedroomPersist.getBedroom(value.bed_room_id).subscribe(result => {
    value.bed_room_name = result.name;
  }, error => {
    console.log(error)
  })
 }
}
