import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WaitinglistPickComponent } from './waitinglist-pick.component';

describe('WaitinglistPickComponent', () => {
  let component: WaitinglistPickComponent;
  let fixture: ComponentFixture<WaitinglistPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitinglistPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitinglistPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
