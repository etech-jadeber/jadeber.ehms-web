import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {WaitinglistDetail, WaitinglistSummary, WaitinglistSummaryPartialList} from "../waitinglist.model";
import {WaitinglistPersist} from "../waitinglist.persist";


@Component({
  selector: 'app-waitinglist-pick',
  templateUrl: './waitinglist-pick.component.html',
  styleUrls: ['./waitinglist-pick.component.css']
})
export class WaitinglistPickComponent implements OnInit {

  waitinglistsData: WaitinglistSummary[] = [];
  waitinglistsTotalCount: number = 0;
  waitinglistsSelection: WaitinglistSummary[] = [];
  waitinglistsDisplayedColumns: string[] = ["select", 'patient_id','start_date','end_date','bed_room_type_id','bed_room_level_id','priority','remark' ];

  waitinglistsSearchTextBox: FormControl = new FormControl();
  waitinglistsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) waitinglistsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) waitinglistsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public waitinglistPersist: WaitinglistPersist,
              public dialogRef: MatDialogRef<WaitinglistPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("waiting_lists");
    this.waitinglistsSearchTextBox.setValue(waitinglistPersist.waitinglistSearchText);
    //delay subsequent keyup events
    this.waitinglistsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.waitinglistPersist.waitinglistSearchText = value;
      this.searchWaitinglists();
    });
  }

  ngOnInit() {

    this.waitinglistsSort.sortChange.subscribe(() => {
      this.waitinglistsPaginator.pageIndex = 0;
      this.searchWaitinglists();
    });

    this.waitinglistsPaginator.page.subscribe(() => {
      this.searchWaitinglists();
    });

    //set initial picker list to 5
    this.waitinglistsPaginator.pageSize = 5;

    //start by loading items
    this.searchWaitinglists();
  }

  searchWaitinglists(): void {
    this.waitinglistsIsLoading = true;
    this.waitinglistsSelection = [];

    this.waitinglistPersist.searchWaitinglist(this.waitinglistsPaginator.pageSize,
        this.waitinglistsPaginator.pageIndex,
        this.waitinglistsSort.active,
        this.waitinglistsSort.direction).subscribe((partialList: WaitinglistSummaryPartialList) => {
      this.waitinglistsData = partialList.data;
      if (partialList.total != -1) {
        this.waitinglistsTotalCount = partialList.total;
      }
      this.waitinglistsIsLoading = false;
    }, error => {
      this.waitinglistsIsLoading = false;
    });

  }

  markOneItem(item: WaitinglistSummary) {
    if(!this.tcUtilsArray.containsId(this.waitinglistsSelection,item.id)){
          this.waitinglistsSelection = [];
          this.waitinglistsSelection.push(item);
        }
        else{
          this.waitinglistsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.waitinglistsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}