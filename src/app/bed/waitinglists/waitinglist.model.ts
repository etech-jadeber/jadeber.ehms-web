import {TCId} from "../../tc/models";

export class WaitinglistSummary extends TCId {
  patient_id: string;
  start_date: number;
  end_date: number;
  bed_room_type_id: string;
  bed_room_id: string;
  bed_id: string;
  priority: number;
  remark: string;
  status: number;
  admission_form_id: string;
  patient_name: string;
  serial_id: number;
  bed_room_type_name: string;
  bed_no: number;
  bed_room_name: string;
  bed_room_level_id: string;
  bed_room_level_name: string;
}

export class WaitinglistSummaryPartialList {
  data: WaitinglistSummary[];
  total: number;
}

export class WaitinglistDetail extends WaitinglistSummary {
  patient_id: string;
  start_date: number;
  end_date: number;
  bed_room_type_id: string;
  ward_id: string;
  priority: number;
  remark: string;
  admission_form_id : string;
}

export class WaitinglistDashboard {
  total: number;
  available_beds: number
}
