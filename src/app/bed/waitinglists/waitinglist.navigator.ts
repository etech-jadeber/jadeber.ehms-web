import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../../tc/models";

import {WaitinglistEditComponent} from "./waitinglist-edit/waitinglist-edit.component";
import {WaitinglistPickComponent} from "./waitinglist-pick/waitinglist-pick.component";


@Injectable({
  providedIn: 'root'
})

export class WaitinglistNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  waitinglistsUrl(): string {
    return "/waitinglists";
  }

  waitinglistUrl(id: string): string {
    return "/waitinglists/" + id;
  }

  viewWaitinglists(): void {
    this.router.navigateByUrl(this.waitinglistsUrl());
  }

  viewWaitinglist(id: string): void {
    this.router.navigateByUrl(this.waitinglistUrl(id));
  }

  editWaitinglist(id: string, isAdmission: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(WaitinglistEditComponent, {
      data: {id: id, mode : TCModalModes.EDIT, isAdmission},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addWaitinglist(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(WaitinglistEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickWaitinglists(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(WaitinglistPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}