import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../../tc/models";
import {WaitinglistDashboard, WaitinglistDetail, WaitinglistSummaryPartialList} from "./waitinglist.model";
import { patient_priority, waiting_status } from '../../app.enums';
import { TCUtilsString } from '../../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class WaitinglistPersist {

  waitinglistSearchText: string = "";
  patient_priorityId: number;
  isAdmissionForm: boolean;
  patient_priority: TCEnum[] = [
    new TCEnum(patient_priority.low, 'low'),
    new TCEnum(patient_priority.medium, 'medium'),
    new TCEnum(patient_priority.high, 'high'),
  ];
  priorityFilter: string = "-1";
  waiting_statusId: number ;
  waiting_status: TCEnum[] = [
    new TCEnum(waiting_status.waiting, 'waiting'),
    new TCEnum(waiting_status.assigned, 'assigned'),
    new TCEnum(waiting_status.canceled, 'canceled'),
  ];
  statusFilter: number = 100;

  constructor(private http: HttpClient) {
  }

  searchWaitinglist(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<WaitinglistSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("waitinglists", this.waitinglistSearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "priority", this.priorityFilter.toString());
    url = TCUtilsString.appendUrlParameter(url, "status", this.statusFilter.toString());
    return this.http.get<WaitinglistSummaryPartialList>(url);

  }

  clearFilters(): void {
    this.waitinglistSearchText = "";
    this.priorityFilter = "-1";
    this.statusFilter = 100;
  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.waitinglistSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "waitinglists/do", new TCDoParam("download_all", this.filters()));
  }

  waitinglistDashboard(): Observable<WaitinglistDashboard> {
    return this.http.get<WaitinglistDashboard>(environment.tcApiBaseUri + "waitinglists/dashboard");
  }

  getWaitinglist(id: string): Observable<WaitinglistDetail> {
    let url = environment.tcApiBaseUri + "waitinglists/" + id;
    url = TCUtilsString.appendUrlParameter(url, "isAdmission", this.isAdmissionForm.toString());
    return this.http.get<WaitinglistDetail>(url);
  }

  addWaitinglist(item: WaitinglistDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "waitinglists/", item);
  }

  updateWaitinglist(item: WaitinglistDetail): Observable<WaitinglistDetail> {
    return this.http.patch<WaitinglistDetail>(environment.tcApiBaseUri + "waitinglists/" + item.id, item);
  }

  deleteWaitinglist(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "waitinglists/" + id);
  }

  waitinglistsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "waitinglists/do", new TCDoParam(method, payload));
  }

  waitinglistDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "waitinglists/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "waitinglists/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "waitinglists/" + id + "/do", new TCDoParam("print", {}));
  }


}