import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeInductionOfAnaesthesiaDetailComponent } from './before-induction-of-anaesthesia-detail.component';

describe('BeforeInductionOfAnaesthesiaDetailComponent', () => {
  let component: BeforeInductionOfAnaesthesiaDetailComponent;
  let fixture: ComponentFixture<BeforeInductionOfAnaesthesiaDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeInductionOfAnaesthesiaDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeInductionOfAnaesthesiaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
