import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator, } from "@angular/material/paginator";
import { MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { BeforeInductionOfAnaesthesiaDetail } from '../before_induction_of_anaesthesia.model';
import { BeforeInductionOfAnaesthesiaPersist } from '../before_induction_of_anaesthesia.persist';
import { BeforeInductionOfAnaesthesiaNavigator } from '../before_induction_of_anaesthesia.navigator';

export enum BeforeInductionOfAnaesthesiaTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-before-induction-of-anaesthesia-detail',
  templateUrl: './before-induction-of-anaesthesia-detail.component.html',
  styleUrls: ['./before-induction-of-anaesthesia-detail.component.scss']
})
export class BeforeInductionOfAnaesthesiaDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  beforeInductionOfAnaesthesiaIsLoading:boolean = false;
  
  BeforeInductionOfAnaesthesiaTabs: typeof BeforeInductionOfAnaesthesiaTabs = BeforeInductionOfAnaesthesiaTabs;
  activeTab: BeforeInductionOfAnaesthesiaTabs = BeforeInductionOfAnaesthesiaTabs.overview;
  //basics
  beforeInductionOfAnaesthesiaDetail: BeforeInductionOfAnaesthesiaDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public beforeInductionOfAnaesthesiaNavigator: BeforeInductionOfAnaesthesiaNavigator,
              public  beforeInductionOfAnaesthesiaPersist: BeforeInductionOfAnaesthesiaPersist) {
    this.tcAuthorization.requireRead("before_induction_of_anaesthesias");
    this.beforeInductionOfAnaesthesiaDetail = new BeforeInductionOfAnaesthesiaDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("before_induction_of_anaesthesias");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.beforeInductionOfAnaesthesiaIsLoading = true;
    this.beforeInductionOfAnaesthesiaPersist.getBeforeInductionOfAnaesthesia(id).subscribe(beforeInductionOfAnaesthesiaDetail => {
          this.beforeInductionOfAnaesthesiaDetail = beforeInductionOfAnaesthesiaDetail;
          console.log(beforeInductionOfAnaesthesiaDetail)

          this.beforeInductionOfAnaesthesiaIsLoading = false;
        }, error => {
          console.error(error);
          this.beforeInductionOfAnaesthesiaIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.beforeInductionOfAnaesthesiaDetail.id);
  }
  editBeforeInductionOfAnaesthesia(): void {
    let modalRef = this.beforeInductionOfAnaesthesiaNavigator.editBeforeInductionOfAnaesthesia(this.beforeInductionOfAnaesthesiaDetail.id);
    modalRef.afterClosed().subscribe(beforeInductionOfAnaesthesiaDetail => {
      TCUtilsAngular.assign(this.beforeInductionOfAnaesthesiaDetail, beforeInductionOfAnaesthesiaDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.beforeInductionOfAnaesthesiaPersist.print(this.beforeInductionOfAnaesthesiaDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print before_induction_of_anaesthesia", true);
      });
    }

  back():void{
      this.location.back();
    }

}