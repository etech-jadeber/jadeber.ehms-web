import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeInductionOfAnaesthesiaEditComponent } from './before-induction-of-anaesthesia-edit.component';

describe('BeforeInductionOfAnaesthesiaEditComponent', () => {
  let component: BeforeInductionOfAnaesthesiaEditComponent;
  let fixture: ComponentFixture<BeforeInductionOfAnaesthesiaEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeInductionOfAnaesthesiaEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeInductionOfAnaesthesiaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
