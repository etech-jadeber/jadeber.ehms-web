import {Component, OnInit, Inject, Injectable, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BeforeInductionOfAnaesthesiaDetail } from '../before_induction_of_anaesthesia.model';
import { BeforeInductionOfAnaesthesiaPersist } from '../before_induction_of_anaesthesia.persist';


@Injectable()
abstract class  BeforeInductionOfAnaesthesiaEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;

  id:string;

  beforeInductionOfAnaesthesiaDetail: BeforeInductionOfAnaesthesiaDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforeInductionOfAnaesthesiaPersist,
              public tcUtilsDate: TCUtilsDate,
              public idMode: TCIdMode) {

  }


  action():void{

  }


  onCancel(): void {
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("before_induction_of_anaesthesias");
      this.title = this.appTranslation.getText("general","new") +  " " + "before_induction_of_anaesthesia";
      this.beforeInductionOfAnaesthesiaDetail = new BeforeInductionOfAnaesthesiaDetail();
      //set necessary defaults

    } else {
      this.idMode.id = this.id ? this.id : this.idMode.id; 
     this.tcAuthorization.requireUpdate("before_induction_of_anaesthesias");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "before_induction_of_anaesthesia";
      this.isLoadingResults = true;
      this.persist.getBeforeInductionOfAnaesthesia(this.idMode.id).subscribe(beforeInductionOfAnaesthesiaDetail => {
        this.beforeInductionOfAnaesthesiaDetail = beforeInductionOfAnaesthesiaDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addBeforeInductionOfAnaesthesia("",this.beforeInductionOfAnaesthesiaDetail).subscribe(value => {
      this.tcNotification.success("beforeInductionOfAnaesthesia added");
      this.beforeInductionOfAnaesthesiaDetail.id = value.id;
      this.action()
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateBeforeInductionOfAnaesthesia(this.beforeInductionOfAnaesthesiaDetail).subscribe(value => {
      this.tcNotification.success("before_induction_of_anaesthesia updated");
      this.action()
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.beforeInductionOfAnaesthesiaDetail == null){
            return false;
          }
        if (this.beforeInductionOfAnaesthesiaDetail.is_the_pulse_oximeter_on_the_patient_and_functioning == null || this.beforeInductionOfAnaesthesiaDetail.is_the_pulse_oximeter_on_the_patient_and_functioning == ""){
          return false;
        }
        if (this.beforeInductionOfAnaesthesiaDetail.anaesthesia_machine_checkup == null || this.beforeInductionOfAnaesthesiaDetail.anaesthesia_machine_checkup == ""){
          return false;
        }
        if (this.beforeInductionOfAnaesthesiaDetail.difficult_airway_or_aspiration_risk == null || this.beforeInductionOfAnaesthesiaDetail.difficult_airway_or_aspiration_risk == ""){
          return false;
        }
        if (this.beforeInductionOfAnaesthesiaDetail.is_the_site_marked == null || this.beforeInductionOfAnaesthesiaDetail.is_the_site_marked == ""){
          return false;
        }
        if (this.beforeInductionOfAnaesthesiaDetail.known_allergy == null || this.beforeInductionOfAnaesthesiaDetail.known_allergy == ""){
          return false;
        }if (this.beforeInductionOfAnaesthesiaDetail.patient_confirmed == null || this.beforeInductionOfAnaesthesiaDetail.patient_confirmed == ""){
          return false;
        }if (this.beforeInductionOfAnaesthesiaDetail.risk_of_500ml_blood_loss == null || this.beforeInductionOfAnaesthesiaDetail.risk_of_500ml_blood_loss == ""){
          return false;
        }

        return true;

 }
 }


 @Component({
  selector: 'app-before-induction-of-anaesthesia-edit',
  templateUrl: './before-induction-of-anaesthesia-edit.component.html',
  styleUrls: ['./before-induction-of-anaesthesia-edit.component.scss']
})export class BeforeInductionOfAnaesthesiaEditComponent extends BeforeInductionOfAnaesthesiaEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  beforeInductionOfAnaesthesiaDetail: BeforeInductionOfAnaesthesiaDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BeforeInductionOfAnaesthesiaEditComponent>,
              public  persist: BeforeInductionOfAnaesthesiaPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,idMode)

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(){
    this.dialogRef.close(this.beforeInductionOfAnaesthesiaDetail);
  }



}



@Component({
  selector: 'app-before-induction-of-anaesthesia-edit-second',
  templateUrl: './before-induction-of-anaesthesia-edit.component.html',
  styleUrls: ['./before-induction-of-anaesthesia-edit.component.scss']
})export class BeforeInductionOfAnaesthesiaEditSecondComponent extends BeforeInductionOfAnaesthesiaEditMainComponent{

  isLoadingResults: boolean = false;
  title: string;

  @Input() id:string;
  beforeInductionOfAnaesthesiaDetail: BeforeInductionOfAnaesthesiaDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforeInductionOfAnaesthesiaPersist,
              
              public tcUtilsDate: TCUtilsDate,) {

                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,{id:null, mode: TCModalModes.EDIT})

  }

 action(){
    this.isLoadingResults=false;
  }

}