import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeInductionOfAnaesthesiaListComponent } from './before-induction-of-anaesthesia-list.component';

describe('BeforeInductionOfAnaesthesiaListComponent', () => {
  let component: BeforeInductionOfAnaesthesiaListComponent;
  let fixture: ComponentFixture<BeforeInductionOfAnaesthesiaListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeInductionOfAnaesthesiaListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeInductionOfAnaesthesiaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
