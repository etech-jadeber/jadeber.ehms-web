import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator, } from "@angular/material/paginator";
import { MatSort} from "@angular/material/sort";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { BeforeInductionOfAnaesthesiaSummary, BeforeInductionOfAnaesthesiaSummaryPartialList } from '../before_induction_of_anaesthesia.model';
import { BeforeInductionOfAnaesthesiaNavigator } from '../before_induction_of_anaesthesia.navigator';
import { BeforeInductionOfAnaesthesiaPersist } from '../before_induction_of_anaesthesia.persist';
@Component({
  selector: 'app-before-induction-of-anaesthesia-list',
  templateUrl: './before-induction-of-anaesthesia-list.component.html',
  styleUrls: ['./before-induction-of-anaesthesia-list.component.scss']
})export class BeforeInductionOfAnaesthesiaListComponent implements OnInit {
  beforeInductionOfAnaesthesiasData: BeforeInductionOfAnaesthesiaSummary[] = [];
  beforeInductionOfAnaesthesiasTotalCount: number = 0;
  beforeInductionOfAnaesthesiaSelectAll:boolean = false;
  beforeInductionOfAnaesthesiaSelection: BeforeInductionOfAnaesthesiaSummary[] = [];

 beforeInductionOfAnaesthesiasDisplayedColumns: string[] = ["select","action","anaesthesia_machine_checkup" ];
  beforeInductionOfAnaesthesiaSearchTextBox: FormControl = new FormControl();
  beforeInductionOfAnaesthesiaIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) beforeInductionOfAnaesthesiasPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforeInductionOfAnaesthesiasSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public beforeInductionOfAnaesthesiaPersist: BeforeInductionOfAnaesthesiaPersist,
                public beforeInductionOfAnaesthesiaNavigator: BeforeInductionOfAnaesthesiaNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("before_induction_of_anaesthesias");
       this.beforeInductionOfAnaesthesiaSearchTextBox.setValue(beforeInductionOfAnaesthesiaPersist.beforeInductionOfAnaesthesiaSearchText);
      //delay subsequent keyup events
      this.beforeInductionOfAnaesthesiaSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforeInductionOfAnaesthesiaPersist.beforeInductionOfAnaesthesiaSearchText = value;
        this.searchBefore_induction_of_anaesthesias();
      });
    } ngOnInit() {
   
      this.beforeInductionOfAnaesthesiasSort.sortChange.subscribe(() => {
        this.beforeInductionOfAnaesthesiasPaginator.pageIndex = 0;
        this.searchBefore_induction_of_anaesthesias(true);
      });

      this.beforeInductionOfAnaesthesiasPaginator.page.subscribe(() => {
        this.searchBefore_induction_of_anaesthesias(true);
      });
      //start by loading items
      this.searchBefore_induction_of_anaesthesias();
    }

  searchBefore_induction_of_anaesthesias(isPagination:boolean = false): void {


    let paginator = this.beforeInductionOfAnaesthesiasPaginator;
    let sorter = this.beforeInductionOfAnaesthesiasSort;

    this.beforeInductionOfAnaesthesiaIsLoading = true;
    this.beforeInductionOfAnaesthesiaSelection = [];

    this.beforeInductionOfAnaesthesiaPersist.searchBeforeInductionOfAnaesthesia(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforeInductionOfAnaesthesiaSummaryPartialList) => {
      this.beforeInductionOfAnaesthesiasData = partialList.data;
      if (partialList.total != -1) {
        this.beforeInductionOfAnaesthesiasTotalCount = partialList.total;
      }
      this.beforeInductionOfAnaesthesiaIsLoading = false;
    }, error => {
      this.beforeInductionOfAnaesthesiaIsLoading = false;
    });

  } downloadBeforeInductionOfAnaesthesias(): void {
    if(this.beforeInductionOfAnaesthesiaSelectAll){
         this.beforeInductionOfAnaesthesiaPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download before_induction_of_anaesthesia", true);
      });
    }
    else{
        this.beforeInductionOfAnaesthesiaPersist.download(this.tcUtilsArray.idsList(this.beforeInductionOfAnaesthesiaSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download before_induction_of_anaesthesia",true);
            });
        }
  }
addBefore_induction_of_anaesthesia(): void {
    let dialogRef = this.beforeInductionOfAnaesthesiaNavigator.addBeforeInductionOfAnaesthesia();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBefore_induction_of_anaesthesias();
      }
    });
  }

  editBeforeInductionOfAnaesthesia(item: BeforeInductionOfAnaesthesiaSummary) {
    let dialogRef = this.beforeInductionOfAnaesthesiaNavigator.editBeforeInductionOfAnaesthesia(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBeforeInductionOfAnaesthesia(item: BeforeInductionOfAnaesthesiaSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("before_induction_of_anaesthesia");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.beforeInductionOfAnaesthesiaPersist.deleteBeforeInductionOfAnaesthesia(item.id).subscribe(response => {
          this.tcNotification.success("before_induction_of_anaesthesia deleted");
          this.searchBefore_induction_of_anaesthesias();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}