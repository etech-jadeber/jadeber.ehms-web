import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeInductionOfAnaesthesiaPickComponent } from './before-induction-of-anaesthesia-pick.component';

describe('BeforeInductionOfAnaesthesiaPickComponent', () => {
  let component: BeforeInductionOfAnaesthesiaPickComponent;
  let fixture: ComponentFixture<BeforeInductionOfAnaesthesiaPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeInductionOfAnaesthesiaPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeInductionOfAnaesthesiaPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
