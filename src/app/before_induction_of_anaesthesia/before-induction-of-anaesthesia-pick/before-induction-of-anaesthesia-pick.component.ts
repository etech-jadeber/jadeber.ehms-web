import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator, } from "@angular/material/paginator";
import { MatSort} from "@angular/material/sort";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BeforeInductionOfAnaesthesiaSummary, BeforeInductionOfAnaesthesiaSummaryPartialList } from '../before_induction_of_anaesthesia.model';
import { BeforeInductionOfAnaesthesiaPersist } from '../before_induction_of_anaesthesia.persist';
@Component({
  selector: 'app-before-induction-of-anaesthesia-pick',
  templateUrl: './before-induction-of-anaesthesia-pick.component.html',
  styleUrls: ['./before-induction-of-anaesthesia-pick.component.scss']
})export class BeforeInductionOfAnaesthesiaPickComponent implements OnInit {
  beforeInductionOfAnaesthesiasData: BeforeInductionOfAnaesthesiaSummary[] = [];
  beforeInductionOfAnaesthesiasTotalCount: number = 0;
  beforeInductionOfAnaesthesiaSelectAll:boolean = false;
  beforeInductionOfAnaesthesiaSelection: BeforeInductionOfAnaesthesiaSummary[] = [];

 beforeInductionOfAnaesthesiasDisplayedColumns: string[] = ["select","action" ,"anaesthesia_machine_checkup" ];
  beforeInductionOfAnaesthesiaSearchTextBox: FormControl = new FormControl();
  beforeInductionOfAnaesthesiaIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) beforeInductionOfAnaesthesiasPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforeInductionOfAnaesthesiasSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public dialogRef: MatDialogRef<BeforeInductionOfAnaesthesiaPickComponent>,
                public appTranslation:AppTranslation,
                public beforeInductionOfAnaesthesiaPersist: BeforeInductionOfAnaesthesiaPersist,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("before_induction_of_anaesthesias");
       this.beforeInductionOfAnaesthesiaSearchTextBox.setValue(beforeInductionOfAnaesthesiaPersist.beforeInductionOfAnaesthesiaSearchText);
      //delay subsequent keyup events
      this.beforeInductionOfAnaesthesiaSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforeInductionOfAnaesthesiaPersist.beforeInductionOfAnaesthesiaSearchText = value;
        this.searchBefore_induction_of_anaesthesias();
      });
    } ngOnInit() {
   
      this.beforeInductionOfAnaesthesiasSort.sortChange.subscribe(() => {
        this.beforeInductionOfAnaesthesiasPaginator.pageIndex = 0;
        this.searchBefore_induction_of_anaesthesias(true);
      });

      this.beforeInductionOfAnaesthesiasPaginator.page.subscribe(() => {
        this.searchBefore_induction_of_anaesthesias(true);
      });
      //start by loading items
      this.searchBefore_induction_of_anaesthesias();
    }

  searchBefore_induction_of_anaesthesias(isPagination:boolean = false): void {


    let paginator = this.beforeInductionOfAnaesthesiasPaginator;
    let sorter = this.beforeInductionOfAnaesthesiasSort;

    this.beforeInductionOfAnaesthesiaIsLoading = true;
    this.beforeInductionOfAnaesthesiaSelection = [];

    this.beforeInductionOfAnaesthesiaPersist.searchBeforeInductionOfAnaesthesia(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforeInductionOfAnaesthesiaSummaryPartialList) => {
      this.beforeInductionOfAnaesthesiasData = partialList.data;
      if (partialList.total != -1) {
        this.beforeInductionOfAnaesthesiasTotalCount = partialList.total;
      }
      this.beforeInductionOfAnaesthesiaIsLoading = false;
    }, error => {
      this.beforeInductionOfAnaesthesiaIsLoading = false;
    });

  }
  markOneItem(item: BeforeInductionOfAnaesthesiaSummary) {
    if(!this.tcUtilsArray.containsId(this.beforeInductionOfAnaesthesiaSelection,item.id)){
          this.beforeInductionOfAnaesthesiaSelection = [];
          this.beforeInductionOfAnaesthesiaSelection.push(item);
        }
        else{
          this.beforeInductionOfAnaesthesiaSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.beforeInductionOfAnaesthesiaSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }