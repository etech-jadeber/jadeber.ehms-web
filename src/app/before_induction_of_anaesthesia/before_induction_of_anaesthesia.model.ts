import {TCId} from "../tc/models";
export class BeforeInductionOfAnaesthesiaSummary extends TCId {
    patient_confirmed:string;
    is_the_site_marked:string;
    is_the_pulse_oximeter_on_the_patient_and_functioning:string;
    known_allergy:string;
    difficult_airway_or_aspiration_risk:string;
    risk_of_500ml_blood_loss:string;
    anaesthesia_machine_checkup:string;
     
    }
    export class BeforeInductionOfAnaesthesiaSummaryPartialList {
      data: BeforeInductionOfAnaesthesiaSummary[];
      total: number;
    }
    export class BeforeInductionOfAnaesthesiaDetail extends BeforeInductionOfAnaesthesiaSummary {
    }
    
    export class BeforeInductionOfAnaesthesiaDashboard {
      total: number;
    }