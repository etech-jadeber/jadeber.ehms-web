import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { BeforeInductionOfAnaesthesiaEditComponent } from "./before-induction-of-anaesthesia-edit/before-induction-of-anaesthesia-edit.component";
import { BeforeInductionOfAnaesthesiaPickComponent } from "./before-induction-of-anaesthesia-pick/before-induction-of-anaesthesia-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BeforeInductionOfAnaesthesiaNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  before_induction_of_anaesthesiasUrl(): string {
    return "/before_induction_of_anaesthesias";
  }

  before_induction_of_anaesthesiaUrl(id: string): string {
    return "/before_induction_of_anaesthesias/" + id;
  }

  viewBeforeInductionOfAnaesthesias(): void {
    this.router.navigateByUrl(this.before_induction_of_anaesthesiasUrl());
  }

  viewBeforeInductionOfAnaesthesia(id: string): void {
    this.router.navigateByUrl(this.before_induction_of_anaesthesiaUrl(id));
  }

  editBeforeInductionOfAnaesthesia(id: string): MatDialogRef<BeforeInductionOfAnaesthesiaEditComponent> {
    const dialogRef = this.dialog.open(BeforeInductionOfAnaesthesiaEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBeforeInductionOfAnaesthesia(): MatDialogRef<BeforeInductionOfAnaesthesiaEditComponent> {
    const dialogRef = this.dialog.open(BeforeInductionOfAnaesthesiaEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBeforeInductionOfAnaesthesias(selectOne: boolean=false): MatDialogRef<BeforeInductionOfAnaesthesiaPickComponent> {
      const dialogRef = this.dialog.open(BeforeInductionOfAnaesthesiaPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}