import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {BeforeInductionOfAnaesthesiaDashboard, BeforeInductionOfAnaesthesiaDetail, BeforeInductionOfAnaesthesiaSummaryPartialList} from "./before_induction_of_anaesthesia.model";


@Injectable({
  providedIn: 'root'
})
export class BeforeInductionOfAnaesthesiaPersist {
 beforeInductionOfAnaesthesiaSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.beforeInductionOfAnaesthesiaSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchBeforeInductionOfAnaesthesia(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BeforeInductionOfAnaesthesiaSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("before_induction_of_anaesthesia", this.beforeInductionOfAnaesthesiaSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<BeforeInductionOfAnaesthesiaSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/do", new TCDoParam("download_all", this.filters()));
  }

  beforeInductionOfAnaesthesiaDashboard(): Observable<BeforeInductionOfAnaesthesiaDashboard> {
    return this.http.get<BeforeInductionOfAnaesthesiaDashboard>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/dashboard");
  }

  getBeforeInductionOfAnaesthesia(id: string): Observable<BeforeInductionOfAnaesthesiaDetail> {
    return this.http.get<BeforeInductionOfAnaesthesiaDetail>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/" + id);
  }
  addBeforeInductionOfAnaesthesia(parentId:string,item: BeforeInductionOfAnaesthesiaDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/before_induction_of_anaesthesia/", item);
  }

  updateBeforeInductionOfAnaesthesia(item: BeforeInductionOfAnaesthesiaDetail): Observable<BeforeInductionOfAnaesthesiaDetail> {
    return this.http.patch<BeforeInductionOfAnaesthesiaDetail>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/" + item.id, item);
  }

  deleteBeforeInductionOfAnaesthesia(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "before_induction_of_anaesthesia/" + id);
  }
 beforeInductionOfAnaesthesiasDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/do", new TCDoParam(method, payload));
  }
  beforeInductionOfAnaesthesiaDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/" + id + "/do", new TCDoParam(method, payload));
  }
  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/do", new TCDoParam("download", ids));
}

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_induction_of_anaesthesia/" + id + "/do", new TCDoParam("print", {}));
  }
 }