import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforePatientLeavesOperatingRoomDetailComponent } from './before-patient-leaves-operating-room-detail.component';

describe('BeforePatientLeavesOperatingRoomDetailComponent', () => {
  let component: BeforePatientLeavesOperatingRoomDetailComponent;
  let fixture: ComponentFixture<BeforePatientLeavesOperatingRoomDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforePatientLeavesOperatingRoomDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforePatientLeavesOperatingRoomDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
