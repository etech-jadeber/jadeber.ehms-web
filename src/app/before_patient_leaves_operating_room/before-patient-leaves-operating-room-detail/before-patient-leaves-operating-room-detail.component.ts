import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { BeforePatientLeavesOperatingRoomDetail } from '../before_patient_leaves_operating_room.model';
import { BeforePatientLeavesOperatingRoomPersist } from '../before_patient_leaves_operating_room.persist';
import { BeforePatientLeavesOperatingRoomNavigator } from '../before_patient_leaves_operating_room.navigator';

export enum BeforePatientLeavesOperatingRoomTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-before-patient-leaves-operating-room-detail',
  templateUrl: './before-patient-leaves-operating-room-detail.component.html',
  styleUrls: ['./before-patient-leaves-operating-room-detail.component.scss']
})
export class BeforePatientLeavesOperatingRoomDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  beforePatientLeavesOperatingRoomIsLoading:boolean = false;
  
  BeforePatientLeavesOperatingRoomTabs: typeof BeforePatientLeavesOperatingRoomTabs = BeforePatientLeavesOperatingRoomTabs;
  activeTab: BeforePatientLeavesOperatingRoomTabs = BeforePatientLeavesOperatingRoomTabs.overview;
  //basics
  beforePatientLeavesOperatingRoomDetail: BeforePatientLeavesOperatingRoomDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public beforePatientLeavesOperatingRoomNavigator: BeforePatientLeavesOperatingRoomNavigator,
              public  beforePatientLeavesOperatingRoomPersist: BeforePatientLeavesOperatingRoomPersist) {
    this.tcAuthorization.requireRead("before_patient_leaves_operating_rooms");
    this.beforePatientLeavesOperatingRoomDetail = new BeforePatientLeavesOperatingRoomDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("before_patient_leaves_operating_rooms");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.beforePatientLeavesOperatingRoomIsLoading = true;
    this.beforePatientLeavesOperatingRoomPersist.getBeforePatientLeavesOperatingRoom(id).subscribe(beforePatientLeavesOperatingRoomDetail => {
          this.beforePatientLeavesOperatingRoomDetail = beforePatientLeavesOperatingRoomDetail;
          this.beforePatientLeavesOperatingRoomIsLoading = false;
        }, error => {
          console.error(error);
          this.beforePatientLeavesOperatingRoomIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.beforePatientLeavesOperatingRoomDetail.id);
  }
  editBeforePatientLeavesOperatingRoom(): void {
    let modalRef = this.beforePatientLeavesOperatingRoomNavigator.editBeforePatientLeavesOperatingRoom(this.beforePatientLeavesOperatingRoomDetail.id);
    modalRef.afterClosed().subscribe(beforePatientLeavesOperatingRoomDetail => {
      TCUtilsAngular.assign(this.beforePatientLeavesOperatingRoomDetail, beforePatientLeavesOperatingRoomDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.beforePatientLeavesOperatingRoomPersist.print(this.beforePatientLeavesOperatingRoomDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print before_patient_leaves_operating_room", true);
      });
    }

  back():void{
      this.location.back();
    }

}