import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforePatientLeavesOperatingRoomEditComponent } from './before-patient-leaves-operating-room-edit.component';

describe('BeforePatientLeavesOperatingRoomEditComponent', () => {
  let component: BeforePatientLeavesOperatingRoomEditComponent;
  let fixture: ComponentFixture<BeforePatientLeavesOperatingRoomEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforePatientLeavesOperatingRoomEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforePatientLeavesOperatingRoomEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
