import {Component, OnInit, Inject, Input, Injectable} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BeforePatientLeavesOperatingRoomDetail } from '../before_patient_leaves_operating_room.model';
import { BeforePatientLeavesOperatingRoomPersist } from '../before_patient_leaves_operating_room.persist';



@Injectable()
abstract class BeforePatientLeavesOperatingRoomEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  id:string;

  beforePatientLeavesOperatingRoomDetail: BeforePatientLeavesOperatingRoomDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforePatientLeavesOperatingRoomPersist,
              
              public tcUtilsDate: TCUtilsDate,
              public idMode: TCIdMode) {

  }



  action(beforePatientLeavesOperatingRoomDetail : BeforePatientLeavesOperatingRoomDetail):void{

  }

  onCancel():void{

  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("before_patient_leaves_operating_rooms");
      this.title = this.appTranslation.getText("general","new") +  " " + "before_patient_leaves_operating_room";
      this.beforePatientLeavesOperatingRoomDetail = new BeforePatientLeavesOperatingRoomDetail();
      //set necessary defaults

    } else {

      this.idMode.id = this.id ? this.id : this.idMode.id; 
     this.tcAuthorization.requireUpdate("before_patient_leaves_operating_rooms");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "before_patient_leaves_operating_room";
      this.isLoadingResults = true;
      this.persist.getBeforePatientLeavesOperatingRoom(this.idMode.id).subscribe(beforePatientLeavesOperatingRoomDetail => {
        this.beforePatientLeavesOperatingRoomDetail = beforePatientLeavesOperatingRoomDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }    
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addBeforePatientLeavesOperatingRoom("",this.beforePatientLeavesOperatingRoomDetail).subscribe(value => {
      this.tcNotification.success("beforePatientLeavesOperatingRoom added");
      this.beforePatientLeavesOperatingRoomDetail.id = value.id;
      this.action(this.beforePatientLeavesOperatingRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateBeforePatientLeavesOperatingRoom(this.beforePatientLeavesOperatingRoomDetail).subscribe(value => {
      this.tcNotification.success("before_patient_leaves_operating_room updated");
      this.action(this.beforePatientLeavesOperatingRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.beforePatientLeavesOperatingRoomDetail == null){
            return false;
          }

        if(this.beforePatientLeavesOperatingRoomDetail.completion_of_instrument_sponge_and_needle_counts == null || this.beforePatientLeavesOperatingRoomDetail.completion_of_instrument_sponge_and_needle_counts == ""){
          return false
        }

        if(this.beforePatientLeavesOperatingRoomDetail.name_of_the_procedure == null || this.beforePatientLeavesOperatingRoomDetail.name_of_the_procedure == ""){
          return false
        }
        if(this.beforePatientLeavesOperatingRoomDetail.specimen_labelling == null || this.beforePatientLeavesOperatingRoomDetail.specimen_labelling == ""){
          return false
        }
        if(this.beforePatientLeavesOperatingRoomDetail.equipment_problems_to_be_addressed == null || this.beforePatientLeavesOperatingRoomDetail.equipment_problems_to_be_addressed == ""){
          return false
        }
        if(this.beforePatientLeavesOperatingRoomDetail.key_concerns_for_recovery == null || this.beforePatientLeavesOperatingRoomDetail.key_concerns_for_recovery == ""){
          return false
        }

        return true;

 }

}

@Component({
  selector: 'app-before-patient-leaves-operating-room-edit',
  templateUrl: './before-patient-leaves-operating-room-edit.component.html',
  styleUrls: ['./before-patient-leaves-operating-room-edit.component.scss']
})export class BeforePatientLeavesOperatingRoomEditComponent extends BeforePatientLeavesOperatingRoomEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;

  beforePatientLeavesOperatingRoomDetail: BeforePatientLeavesOperatingRoomDetail;
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BeforePatientLeavesOperatingRoomEditComponent>,
              public  persist: BeforePatientLeavesOperatingRoomPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,idMode)

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(beforePatientLeavesOperatingRoomDetail : BeforePatientLeavesOperatingRoomDetail){
    this.dialogRef.close(beforePatientLeavesOperatingRoomDetail);
  }

 
 }


 @Component({
  selector: 'app-before-patient-leaves-operating-room-edit-second',
  templateUrl: './before-patient-leaves-operating-room-edit.component.html',
  styleUrls: ['./before-patient-leaves-operating-room-edit.component.scss']
})export class BeforePatientLeavesOperatingRoomEditComponentSecond extends BeforePatientLeavesOperatingRoomEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;

  @Input() id:string;
  beforePatientLeavesOperatingRoomDetail: BeforePatientLeavesOperatingRoomDetail;
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforePatientLeavesOperatingRoomPersist,
              
              public tcUtilsDate: TCUtilsDate,
              ) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,{id:null, mode: TCModalModes.EDIT})

  }

  action(beforePatientLeavesOperatingRoomDetail : BeforePatientLeavesOperatingRoomDetail){
    this.isLoadingResults=false;
  }

 }