import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforePatientLeavesOperatingRoomListComponent } from './before-patient-leaves-operating-room-list.component';

describe('BeforePatientLeavesOperatingRoomListComponent', () => {
  let component: BeforePatientLeavesOperatingRoomListComponent;
  let fixture: ComponentFixture<BeforePatientLeavesOperatingRoomListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforePatientLeavesOperatingRoomListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforePatientLeavesOperatingRoomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
