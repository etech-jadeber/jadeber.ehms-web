import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { BeforePatientLeavesOperatingRoomSummary, BeforePatientLeavesOperatingRoomSummaryPartialList } from '../before_patient_leaves_operating_room.model';
import { BeforePatientLeavesOperatingRoomNavigator } from '../before_patient_leaves_operating_room.navigator';
import { BeforePatientLeavesOperatingRoomPersist } from '../before_patient_leaves_operating_room.persist';
@Component({
  selector: 'app-before-patient-leaves-operating-room-list',
  templateUrl: './before-patient-leaves-operating-room-list.component.html',
  styleUrls: ['./before-patient-leaves-operating-room-list.component.scss']
})export class BeforePatientLeavesOperatingRoomListComponent implements OnInit {
  beforePatientLeavesOperatingRoomsData: BeforePatientLeavesOperatingRoomSummary[] = [];
  beforePatientLeavesOperatingRoomsTotalCount: number = 0;
  beforePatientLeavesOperatingRoomSelectAll:boolean = false;
  beforePatientLeavesOperatingRoomSelection: BeforePatientLeavesOperatingRoomSummary[] = [];

 beforePatientLeavesOperatingRoomsDisplayedColumns: string[] = ["select","action" ," key_concerns_for_recovery" ];
  beforePatientLeavesOperatingRoomSearchTextBox: FormControl = new FormControl();
  beforePatientLeavesOperatingRoomIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) beforePatientLeavesOperatingRoomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforePatientLeavesOperatingRoomsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public beforePatientLeavesOperatingRoomPersist: BeforePatientLeavesOperatingRoomPersist,
                public beforePatientLeavesOperatingRoomNavigator: BeforePatientLeavesOperatingRoomNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("before_patient_leaves_operating_rooms");
       this.beforePatientLeavesOperatingRoomSearchTextBox.setValue(beforePatientLeavesOperatingRoomPersist.beforePatientLeavesOperatingRoomSearchText);
      //delay subsequent keyup events
      this.beforePatientLeavesOperatingRoomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforePatientLeavesOperatingRoomPersist.beforePatientLeavesOperatingRoomSearchText = value;
        this.searchBefore_patient_leaves_operating_rooms();
      });
    } ngOnInit() {
   
      this.beforePatientLeavesOperatingRoomsSort.sortChange.subscribe(() => {
        this.beforePatientLeavesOperatingRoomsPaginator.pageIndex = 0;
        this.searchBefore_patient_leaves_operating_rooms(true);
      });

      this.beforePatientLeavesOperatingRoomsPaginator.page.subscribe(() => {
        this.searchBefore_patient_leaves_operating_rooms(true);
      });
      //start by loading items
      this.searchBefore_patient_leaves_operating_rooms();
    }

  searchBefore_patient_leaves_operating_rooms(isPagination:boolean = false): void {


    let paginator = this.beforePatientLeavesOperatingRoomsPaginator;
    let sorter = this.beforePatientLeavesOperatingRoomsSort;

    this.beforePatientLeavesOperatingRoomIsLoading = true;
    this.beforePatientLeavesOperatingRoomSelection = [];

    this.beforePatientLeavesOperatingRoomPersist.searchBeforePatientLeavesOperatingRoom(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforePatientLeavesOperatingRoomSummaryPartialList) => {
      this.beforePatientLeavesOperatingRoomsData = partialList.data;
      if (partialList.total != -1) {
        this.beforePatientLeavesOperatingRoomsTotalCount = partialList.total;
      }
      this.beforePatientLeavesOperatingRoomIsLoading = false;
    }, error => {
      this.beforePatientLeavesOperatingRoomIsLoading = false;
    });

  } downloadBeforePatientLeavesOperatingRooms(): void {
    if(this.beforePatientLeavesOperatingRoomSelectAll){
         this.beforePatientLeavesOperatingRoomPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download before_patient_leaves_operating_room", true);
      });
    }
    else{
        this.beforePatientLeavesOperatingRoomPersist.download(this.tcUtilsArray.idsList(this.beforePatientLeavesOperatingRoomSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download before_patient_leaves_operating_room",true);
            });
        }
  }
addBefore_patient_leaves_operating_room(): void {
    let dialogRef = this.beforePatientLeavesOperatingRoomNavigator.addBeforePatientLeavesOperatingRoom();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBefore_patient_leaves_operating_rooms();
      }
    });
  }

  editBeforePatientLeavesOperatingRoom(item: BeforePatientLeavesOperatingRoomSummary) {
    let dialogRef = this.beforePatientLeavesOperatingRoomNavigator.editBeforePatientLeavesOperatingRoom(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBeforePatientLeavesOperatingRoom(item: BeforePatientLeavesOperatingRoomSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("before_patient_leaves_operating_room");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.beforePatientLeavesOperatingRoomPersist.deleteBeforePatientLeavesOperatingRoom(item.id).subscribe(response => {
          this.tcNotification.success("before_patient_leaves_operating_room deleted");
          this.searchBefore_patient_leaves_operating_rooms();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}