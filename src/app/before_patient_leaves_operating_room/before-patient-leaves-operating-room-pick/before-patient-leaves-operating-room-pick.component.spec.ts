import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforePatientLeavesOperatingRoomPickComponent } from './before-patient-leaves-operating-room-pick.component';

describe('BeforePatientLeavesOperatingRoomPickComponent', () => {
  let component: BeforePatientLeavesOperatingRoomPickComponent;
  let fixture: ComponentFixture<BeforePatientLeavesOperatingRoomPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforePatientLeavesOperatingRoomPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforePatientLeavesOperatingRoomPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
