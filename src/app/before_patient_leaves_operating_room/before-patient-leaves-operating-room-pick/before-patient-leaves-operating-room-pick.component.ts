import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BeforePatientLeavesOperatingRoomSummary, BeforePatientLeavesOperatingRoomSummaryPartialList } from '../before_patient_leaves_operating_room.model';
import { BeforePatientLeavesOperatingRoomPersist } from '../before_patient_leaves_operating_room.persist';
@Component({
  selector: 'app-before-patient-leaves-operating-room-pick',
  templateUrl: './before-patient-leaves-operating-room-pick.component.html',
  styleUrls: ['./before-patient-leaves-operating-room-pick.component.scss']
})export class BeforePatientLeavesOperatingRoomPickComponent implements OnInit {
  beforePatientLeavesOperatingRoomsData: BeforePatientLeavesOperatingRoomSummary[] = [];
  beforePatientLeavesOperatingRoomsTotalCount: number = 0;
  beforePatientLeavesOperatingRoomSelectAll:boolean = false;
  beforePatientLeavesOperatingRoomSelection: BeforePatientLeavesOperatingRoomSummary[] = [];

 beforePatientLeavesOperatingRoomsDisplayedColumns: string[] = ["select","action"," key_concerns_for_recovery" ];
  beforePatientLeavesOperatingRoomSearchTextBox: FormControl = new FormControl();
  beforePatientLeavesOperatingRoomIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) beforePatientLeavesOperatingRoomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforePatientLeavesOperatingRoomsSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialogRef: MatDialogRef<BeforePatientLeavesOperatingRoomPickComponent>,
                public beforePatientLeavesOperatingRoomPersist: BeforePatientLeavesOperatingRoomPersist,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("before_patient_leaves_operating_rooms");
       this.beforePatientLeavesOperatingRoomSearchTextBox.setValue(beforePatientLeavesOperatingRoomPersist.beforePatientLeavesOperatingRoomSearchText);
      //delay subsequent keyup events
      this.beforePatientLeavesOperatingRoomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforePatientLeavesOperatingRoomPersist.beforePatientLeavesOperatingRoomSearchText = value;
        this.searchBefore_patient_leaves_operating_rooms();
      });
    } ngOnInit() {
   
      this.beforePatientLeavesOperatingRoomsSort.sortChange.subscribe(() => {
        this.beforePatientLeavesOperatingRoomsPaginator.pageIndex = 0;
        this.searchBefore_patient_leaves_operating_rooms(true);
      });

      this.beforePatientLeavesOperatingRoomsPaginator.page.subscribe(() => {
        this.searchBefore_patient_leaves_operating_rooms(true);
      });
      //start by loading items
      this.searchBefore_patient_leaves_operating_rooms();
    }

  searchBefore_patient_leaves_operating_rooms(isPagination:boolean = false): void {


    let paginator = this.beforePatientLeavesOperatingRoomsPaginator;
    let sorter = this.beforePatientLeavesOperatingRoomsSort;

    this.beforePatientLeavesOperatingRoomIsLoading = true;
    this.beforePatientLeavesOperatingRoomSelection = [];

    this.beforePatientLeavesOperatingRoomPersist.searchBeforePatientLeavesOperatingRoom(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforePatientLeavesOperatingRoomSummaryPartialList) => {
      this.beforePatientLeavesOperatingRoomsData = partialList.data;
      if (partialList.total != -1) {
        this.beforePatientLeavesOperatingRoomsTotalCount = partialList.total;
      }
      this.beforePatientLeavesOperatingRoomIsLoading = false;
    }, error => {
      this.beforePatientLeavesOperatingRoomIsLoading = false;
    });

  }
  markOneItem(item: BeforePatientLeavesOperatingRoomSummary) {
    if(!this.tcUtilsArray.containsId(this.beforePatientLeavesOperatingRoomSelection,item.id)){
          this.beforePatientLeavesOperatingRoomSelection = [];
          this.beforePatientLeavesOperatingRoomSelection.push(item);
        }
        else{
          this.beforePatientLeavesOperatingRoomSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.beforePatientLeavesOperatingRoomSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }