import {TCId} from "../tc/models";
export class BeforePatientLeavesOperatingRoomSummary extends TCId {
    name_of_the_procedure:string;
    completion_of_instrument_sponge_and_needle_counts:string;
    specimen_labelling:string;
    equipment_problems_to_be_addressed:string;
     key_concerns_for_recovery:string;
     
    }
    export class BeforePatientLeavesOperatingRoomSummaryPartialList {
      data: BeforePatientLeavesOperatingRoomSummary[];
      total: number;
    }
    export class BeforePatientLeavesOperatingRoomDetail extends BeforePatientLeavesOperatingRoomSummary {
    }
    
    export class BeforePatientLeavesOperatingRoomDashboard {
      total: number;
    }