import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { BeforePatientLeavesOperatingRoomEditComponent } from "./before-patient-leaves-operating-room-edit/before-patient-leaves-operating-room-edit.component";
import { BeforePatientLeavesOperatingRoomPickComponent } from "./before-patient-leaves-operating-room-pick/before-patient-leaves-operating-room-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BeforePatientLeavesOperatingRoomNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  before_patient_leaves_operating_roomsUrl(): string {
    return "/before_patient_leaves_operating_rooms";
  }

  before_patient_leaves_operating_roomUrl(id: string): string {
    return "/before_patient_leaves_operating_rooms/" + id;
  }

  viewBeforePatientLeavesOperatingRooms(): void {
    this.router.navigateByUrl(this.before_patient_leaves_operating_roomsUrl());
  }

  viewBeforePatientLeavesOperatingRoom(id: string): void {
    this.router.navigateByUrl(this.before_patient_leaves_operating_roomUrl(id));
  }

  editBeforePatientLeavesOperatingRoom(id: string): MatDialogRef<BeforePatientLeavesOperatingRoomEditComponent> {
    const dialogRef = this.dialog.open(BeforePatientLeavesOperatingRoomEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBeforePatientLeavesOperatingRoom(): MatDialogRef<BeforePatientLeavesOperatingRoomEditComponent> {
    const dialogRef = this.dialog.open(BeforePatientLeavesOperatingRoomEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBeforePatientLeavesOperatingRooms(selectOne: boolean=false): MatDialogRef<BeforePatientLeavesOperatingRoomPickComponent> {
      const dialogRef = this.dialog.open(BeforePatientLeavesOperatingRoomPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}