import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {BeforePatientLeavesOperatingRoomDashboard, BeforePatientLeavesOperatingRoomDetail, BeforePatientLeavesOperatingRoomSummaryPartialList} from "./before_patient_leaves_operating_room.model";


@Injectable({
  providedIn: 'root'
})
export class BeforePatientLeavesOperatingRoomPersist {
 beforePatientLeavesOperatingRoomSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.beforePatientLeavesOperatingRoomSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchBeforePatientLeavesOperatingRoom(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BeforePatientLeavesOperatingRoomSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("before_patient_leaves_operating_room", this.beforePatientLeavesOperatingRoomSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<BeforePatientLeavesOperatingRoomSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/do", new TCDoParam("download_all", this.filters()));
  }

  beforePatientLeavesOperatingRoomDashboard(): Observable<BeforePatientLeavesOperatingRoomDashboard> {
    return this.http.get<BeforePatientLeavesOperatingRoomDashboard>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/dashboard");
  }

  getBeforePatientLeavesOperatingRoom(id: string): Observable<BeforePatientLeavesOperatingRoomDetail> {
    return this.http.get<BeforePatientLeavesOperatingRoomDetail>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/" + id);
  }

  addBeforePatientLeavesOperatingRoom(parentId:string,item: BeforePatientLeavesOperatingRoomDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/before_patient_leaves_operating_room/", item);
  }

  updateBeforePatientLeavesOperatingRoom(item: BeforePatientLeavesOperatingRoomDetail): Observable<BeforePatientLeavesOperatingRoomDetail> {
    return this.http.patch<BeforePatientLeavesOperatingRoomDetail>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/" + item.id, item);
  }

  deleteBeforePatientLeavesOperatingRoom(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "before_patient_leaves_operating_room/" + id);
  }
 beforePatientLeavesOperatingRoomsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/do", new TCDoParam(method, payload));
  }
  beforePatientLeavesOperatingRoomDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_patient_leaves_operating_room/do", new TCDoParam("download", ids));
}


 }