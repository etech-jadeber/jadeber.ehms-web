import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeSkinIncisionDetailComponent } from './before-skin-incision-detail.component';

describe('BeforeSkinIncisionDetailComponent', () => {
  let component: BeforeSkinIncisionDetailComponent;
  let fixture: ComponentFixture<BeforeSkinIncisionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeSkinIncisionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeSkinIncisionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
