import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { BeforeSkinIncisionDetail } from '../before_skin_incision.model';
import { BeforeSkinIncisionNavigator } from '../before_skin_incision.navigator';
import { BeforeSkinIncisionPersist } from '../before_skin_incision.persist';

export enum BeforeSkinIncisionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-before-skin-incision-detail',
  templateUrl: './before-skin-incision-detail.component.html',
  styleUrls: ['./before-skin-incision-detail.component.scss']
})
export class BeforeSkinIncisionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  beforeSkinIncisionIsLoading:boolean = false;
  
  BeforeSkinIncisionTabs: typeof BeforeSkinIncisionTabs = BeforeSkinIncisionTabs;
  activeTab: BeforeSkinIncisionTabs = BeforeSkinIncisionTabs.overview;
  //basics
  beforeSkinIncisionDetail: BeforeSkinIncisionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public beforeSkinIncisionNavigator: BeforeSkinIncisionNavigator,
              public  beforeSkinIncisionPersist: BeforeSkinIncisionPersist) {
    this.tcAuthorization.requireRead("before_skin_incisions");
    this.beforeSkinIncisionDetail = new BeforeSkinIncisionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("before_skin_incisions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.beforeSkinIncisionIsLoading = true;
    this.beforeSkinIncisionPersist.getBeforeSkinIncision(id).subscribe(beforeSkinIncisionDetail => {
          this.beforeSkinIncisionDetail = beforeSkinIncisionDetail;
          this.beforeSkinIncisionIsLoading = false;
        }, error => {
          console.error(error);
          this.beforeSkinIncisionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.beforeSkinIncisionDetail.id);
  }
  editBeforeSkinIncision(): void {
    let modalRef = this.beforeSkinIncisionNavigator.editBeforeSkinIncision(this.beforeSkinIncisionDetail.id);
    modalRef.afterClosed().subscribe(beforeSkinIncisionDetail => {
      TCUtilsAngular.assign(this.beforeSkinIncisionDetail, beforeSkinIncisionDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.beforeSkinIncisionPersist.print(this.beforeSkinIncisionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print before_skin_incision", true);
      });
    }

  back():void{
      this.location.back();
    }

}