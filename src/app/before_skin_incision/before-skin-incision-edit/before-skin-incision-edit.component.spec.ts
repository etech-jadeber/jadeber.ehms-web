import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeSkinIncisionEditComponent } from './before-skin-incision-edit.component';

describe('BeforeSkinIncisionEditComponent', () => {
  let component: BeforeSkinIncisionEditComponent;
  let fixture: ComponentFixture<BeforeSkinIncisionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeSkinIncisionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeSkinIncisionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
