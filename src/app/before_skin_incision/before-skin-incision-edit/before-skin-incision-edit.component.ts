import {Component, OnInit, Inject, Injectable, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BeforeSkinIncisionDetail } from '../before_skin_incision.model';import { BeforeSkinIncisionPersist } from '../before_skin_incision.persist';

@Injectable()
abstract class  BeforeSkinIncisionEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  id:string;
  beforeSkinIncisionDetail: BeforeSkinIncisionDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforeSkinIncisionPersist,
              public tcUtilsDate: TCUtilsDate,
              public idMode: TCIdMode) {

  }

  onCancel(): void {
  }
  action():void{

  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("before_skin_incisions");
      this.title = this.appTranslation.getText("general","new") +  " " + "before_skin_incision";
      this.beforeSkinIncisionDetail = new BeforeSkinIncisionDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("before_skin_incisions");
     this.idMode.id = this.id ? this.id : this.idMode.id;
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "before_skin_incision";
      this.isLoadingResults = true;
      this.persist.getBeforeSkinIncision(this.idMode.id).subscribe(beforeSkinIncisionDetail => {
        this.beforeSkinIncisionDetail = beforeSkinIncisionDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addBeforeSkinIncision("",this.beforeSkinIncisionDetail).subscribe(value => {
      this.tcNotification.success("beforeSkinIncision added");
      this.beforeSkinIncisionDetail.id = value.id;
      this.action()
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateBeforeSkinIncision(this.beforeSkinIncisionDetail).subscribe(value => {
      this.tcNotification.success("before_skin_incision updated");
     this.action()
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.beforeSkinIncisionDetail == null){
            return false;
          }

        if(this.beforeSkinIncisionDetail.anticipated_blood_loss==null|| this.beforeSkinIncisionDetail.anticipated_blood_loss ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.case_take==null|| this.beforeSkinIncisionDetail.case_take ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.critical_or_non_routine_steps==null|| this.beforeSkinIncisionDetail.critical_or_non_routine_steps ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.equipment_issues==null|| this.beforeSkinIncisionDetail.equipment_issues ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.essential_imaging_displayed==null|| this.beforeSkinIncisionDetail.essential_imaging_displayed ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.has_antibiotic_prophylaxis==null|| this.beforeSkinIncisionDetail.has_antibiotic_prophylaxis ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.has_sterility==null|| this.beforeSkinIncisionDetail.has_sterility ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.induction==null|| this.beforeSkinIncisionDetail.induction ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.patient_name_procedure_confirmation==null|| this.beforeSkinIncisionDetail.patient_name_procedure_confirmation ==""){
          return false;
        }
        if(this.beforeSkinIncisionDetail.patient_specific_concerns==null|| this.beforeSkinIncisionDetail.patient_specific_concerns ==""){
          return false;
        }
        return true;
 }
 }

 @Component({
  selector: 'app-before-skin-incision-edit',
  templateUrl: './before-skin-incision-edit.component.html',
  styleUrls: ['./before-skin-incision-edit.component.scss']
})export class BeforeSkinIncisionEditComponent extends BeforeSkinIncisionEditMainComponent {

  title: string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BeforeSkinIncisionEditComponent>,
              public  persist: BeforeSkinIncisionPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,idMode)

              }
            
      onCancel(): void {
        this.dialogRef.close();
      }
    
      action(){
        this.dialogRef.close(this.beforeSkinIncisionDetail);
      }
            
            
}


@Component({
  selector: 'app-before-skin-incision-edit-second',
  templateUrl: './before-skin-incision-edit.component.html',
  styleUrls: ['./before-skin-incision-edit.component.scss']
})
export class BeforeSkinIncisionEditSecondComponent extends BeforeSkinIncisionEditMainComponent {

  @Input() id:string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: BeforeSkinIncisionPersist,
              
              public tcUtilsDate: TCUtilsDate) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,tcUtilsDate,{id:null, mode: TCModalModes.EDIT})

  }

 action(){
    this.isLoadingResults=false;
  }
}
