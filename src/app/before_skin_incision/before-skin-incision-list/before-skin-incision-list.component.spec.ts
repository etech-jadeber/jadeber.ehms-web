import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeSkinIncisionListComponent } from './before-skin-incision-list.component';

describe('BeforeSkinIncisionListComponent', () => {
  let component: BeforeSkinIncisionListComponent;
  let fixture: ComponentFixture<BeforeSkinIncisionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeSkinIncisionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeSkinIncisionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
