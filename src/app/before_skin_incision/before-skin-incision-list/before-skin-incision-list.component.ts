import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { BeforeSkinIncisionSummary, BeforeSkinIncisionSummaryPartialList } from '../before_skin_incision.model';
import { BeforeSkinIncisionNavigator } from '../before_skin_incision.navigator';
import { BeforeSkinIncisionPersist } from '../before_skin_incision.persist';
@Component({
  selector: 'app-before_skin_incision-list',
  templateUrl: './before-skin-incision-list.component.html',
  styleUrls: ['./before-skin-incision-list.component.scss']
})export class BeforeSkinIncisionListComponent implements OnInit {
  beforeSkinIncisionsData: BeforeSkinIncisionSummary[] = [];
  beforeSkinIncisionsTotalCount: number = 0;
  beforeSkinIncisionSelectAll:boolean = false;
  beforeSkinIncisionSelection: BeforeSkinIncisionSummary[] = [];

 beforeSkinIncisionsDisplayedColumns: string[] = ["select","action" ," essential_imaging_displayed" ];
  beforeSkinIncisionSearchTextBox: FormControl = new FormControl();
  beforeSkinIncisionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) beforeSkinIncisionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforeSkinIncisionsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public beforeSkinIncisionPersist: BeforeSkinIncisionPersist,
                public beforeSkinIncisionNavigator: BeforeSkinIncisionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("before_skin_incisions");
       this.beforeSkinIncisionSearchTextBox.setValue(beforeSkinIncisionPersist.beforeSkinIncisionSearchText);
      //delay subsequent keyup events
      this.beforeSkinIncisionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforeSkinIncisionPersist.beforeSkinIncisionSearchText = value;
        this.searchBefore_skin_incisions();
      });
    } ngOnInit() {
   
      this.beforeSkinIncisionsSort.sortChange.subscribe(() => {
        this.beforeSkinIncisionsPaginator.pageIndex = 0;
        this.searchBefore_skin_incisions(true);
      });

      this.beforeSkinIncisionsPaginator.page.subscribe(() => {
        this.searchBefore_skin_incisions(true);
      });
      //start by loading items
      this.searchBefore_skin_incisions();
    }

  searchBefore_skin_incisions(isPagination:boolean = false): void {


    let paginator = this.beforeSkinIncisionsPaginator;
    let sorter = this.beforeSkinIncisionsSort;

    this.beforeSkinIncisionIsLoading = true;
    this.beforeSkinIncisionSelection = [];

    this.beforeSkinIncisionPersist.searchBeforeSkinIncision(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforeSkinIncisionSummaryPartialList) => {
      this.beforeSkinIncisionsData = partialList.data;
      if (partialList.total != -1) {
        this.beforeSkinIncisionsTotalCount = partialList.total;
      }
      this.beforeSkinIncisionIsLoading = false;
    }, error => {
      this.beforeSkinIncisionIsLoading = false;
    });

  } downloadBeforeSkinIncisions(): void {
    if(this.beforeSkinIncisionSelectAll){
         this.beforeSkinIncisionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download before_skin_incision", true);
      });
    }
    else{
        this.beforeSkinIncisionPersist.download(this.tcUtilsArray.idsList(this.beforeSkinIncisionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download before_skin_incision",true);
            });
        }
  }
addBefore_skin_incision(): void {
    let dialogRef = this.beforeSkinIncisionNavigator.addBeforeSkinIncision();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBefore_skin_incisions();
      }
    });
  }

  editBeforeSkinIncision(item: BeforeSkinIncisionSummary) {
    let dialogRef = this.beforeSkinIncisionNavigator.editBeforeSkinIncision(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBeforeSkinIncision(item: BeforeSkinIncisionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("before_skin_incision");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.beforeSkinIncisionPersist.deleteBeforeSkinIncision(item.id).subscribe(response => {
          this.tcNotification.success("before_skin_incision deleted");
          this.searchBefore_skin_incisions();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}