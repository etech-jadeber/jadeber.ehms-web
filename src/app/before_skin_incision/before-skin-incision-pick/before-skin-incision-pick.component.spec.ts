import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeSkinIncisionPickComponent } from './before-skin-incision-pick.component';

describe('BeforeSkinIncisionPickComponent', () => {
  let component: BeforeSkinIncisionPickComponent;
  let fixture: ComponentFixture<BeforeSkinIncisionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeSkinIncisionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeSkinIncisionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
