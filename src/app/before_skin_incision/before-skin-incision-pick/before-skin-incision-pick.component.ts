import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BeforeSkinIncisionSummary, BeforeSkinIncisionSummaryPartialList } from '../before_skin_incision.model';
import { BeforeSkinIncisionPersist } from '../before_skin_incision.persist';
@Component({
  selector: 'app-before-skin-incision-pick',
  templateUrl: './before-skin-incision-pick.component.html',
  styleUrls: ['./before-skin-incision-pick.component.scss']
})export class BeforeSkinIncisionPickComponent implements OnInit {
  beforeSkinIncisionsData: BeforeSkinIncisionSummary[] = [];
  beforeSkinIncisionsTotalCount: number = 0;
  beforeSkinIncisionSelectAll:boolean = false;
  beforeSkinIncisionSelection: BeforeSkinIncisionSummary[] = [];

 beforeSkinIncisionsDisplayedColumns: string[] = ["select","action"," essential_imaging_displayed" ];
  beforeSkinIncisionSearchTextBox: FormControl = new FormControl();
  beforeSkinIncisionIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) beforeSkinIncisionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) beforeSkinIncisionsSort: MatSort;  
  constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public dialogRef: MatDialogRef<BeforeSkinIncisionPickComponent>,
                public appTranslation:AppTranslation,
                public beforeSkinIncisionPersist: BeforeSkinIncisionPersist,
                @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("before_skin_incisions");
       this.beforeSkinIncisionSearchTextBox.setValue(beforeSkinIncisionPersist.beforeSkinIncisionSearchText);
      //delay subsequent keyup events
      this.beforeSkinIncisionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.beforeSkinIncisionPersist.beforeSkinIncisionSearchText = value;
        this.searchBefore_skin_incisions();
      });
    } ngOnInit() {
   
      this.beforeSkinIncisionsSort.sortChange.subscribe(() => {
        this.beforeSkinIncisionsPaginator.pageIndex = 0;
        this.searchBefore_skin_incisions(true);
      });

      this.beforeSkinIncisionsPaginator.page.subscribe(() => {
        this.searchBefore_skin_incisions(true);
      });
      //start by loading items
      this.searchBefore_skin_incisions();
    }

  searchBefore_skin_incisions(isPagination:boolean = false): void {


    let paginator = this.beforeSkinIncisionsPaginator;
    let sorter = this.beforeSkinIncisionsSort;

    this.beforeSkinIncisionIsLoading = true;
    this.beforeSkinIncisionSelection = [];

    this.beforeSkinIncisionPersist.searchBeforeSkinIncision(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BeforeSkinIncisionSummaryPartialList) => {
      this.beforeSkinIncisionsData = partialList.data;
      if (partialList.total != -1) {
        this.beforeSkinIncisionsTotalCount = partialList.total;
      }
      this.beforeSkinIncisionIsLoading = false;
    }, error => {
      this.beforeSkinIncisionIsLoading = false;
    });

  }
  markOneItem(item: BeforeSkinIncisionSummary) {
    if(!this.tcUtilsArray.containsId(this.beforeSkinIncisionSelection,item.id)){
          this.beforeSkinIncisionSelection = [];
          this.beforeSkinIncisionSelection.push(item);
        }
        else{
          this.beforeSkinIncisionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.beforeSkinIncisionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }