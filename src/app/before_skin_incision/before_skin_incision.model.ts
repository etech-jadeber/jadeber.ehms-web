import {TCId} from "../tc/models";

export class BeforeSkinIncisionSummary extends TCId {
    induction:string;
    patient_name_procedure_confirmation:string;
    has_antibiotic_prophylaxis:string;
    critical_or_non_routine_steps:string;
    case_take:string;
    anticipated_blood_loss:string;
    patient_specific_concerns:string;
    has_sterility:string;
    equipment_issues:string;
    essential_imaging_displayed:string;
     
    }
    export class BeforeSkinIncisionSummaryPartialList {
      data: BeforeSkinIncisionSummary[];
      total: number;
    }
    export class BeforeSkinIncisionDetail extends BeforeSkinIncisionSummary {
    }
    
    export class BeforeSkinIncisionDashboard {
      total: number;
    }