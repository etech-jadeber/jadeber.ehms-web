import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { BeforeSkinIncisionEditComponent } from "./before-skin-incision-edit/before-skin-incision-edit.component";
import { BeforeSkinIncisionPickComponent } from "./before-skin-incision-pick/before-skin-incision-pick.component";

@Injectable({
  providedIn: 'root'
})

export class BeforeSkinIncisionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  before_skin_incisionsUrl(): string {
    return "/before_skin_incisions";
  }

  before_skin_incisionUrl(id: string): string {
    return "/before_skin_incisions/" + id;
  }

  viewBeforeSkinIncisions(): void {
    this.router.navigateByUrl(this.before_skin_incisionsUrl());
  }

  viewBeforeSkinIncision(id: string): void {
    this.router.navigateByUrl(this.before_skin_incisionUrl(id));
  }

  editBeforeSkinIncision(id: string): MatDialogRef<BeforeSkinIncisionEditComponent> {
    const dialogRef = this.dialog.open(BeforeSkinIncisionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBeforeSkinIncision(): MatDialogRef<BeforeSkinIncisionEditComponent> {
    const dialogRef = this.dialog.open(BeforeSkinIncisionEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBeforeSkinIncisions(selectOne: boolean=false): MatDialogRef<BeforeSkinIncisionPickComponent> {
      const dialogRef = this.dialog.open(BeforeSkinIncisionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}