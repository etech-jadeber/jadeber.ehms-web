import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {BeforeSkinIncisionDashboard, BeforeSkinIncisionDetail, BeforeSkinIncisionSummaryPartialList} from "./before_skin_incision.model";


@Injectable({
  providedIn: 'root'
})
export class BeforeSkinIncisionPersist {
 beforeSkinIncisionSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_skin_incision/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.beforeSkinIncisionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchBeforeSkinIncision(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BeforeSkinIncisionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("before_skin_incision", this.beforeSkinIncisionSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<BeforeSkinIncisionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_skin_incision/do", new TCDoParam("download_all", this.filters()));
  }

  beforeSkinIncisionDashboard(): Observable<BeforeSkinIncisionDashboard> {
    return this.http.get<BeforeSkinIncisionDashboard>(environment.tcApiBaseUri + "before_skin_incision/dashboard");
  }

  getBeforeSkinIncision(id: string): Observable<BeforeSkinIncisionDetail> {
    return this.http.get<BeforeSkinIncisionDetail>(environment.tcApiBaseUri + "before_skin_incision/" + id);
  }

  addBeforeSkinIncision(parentId:string,item: BeforeSkinIncisionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/before_skin_incision/", item);
  }

  updateBeforeSkinIncision(item: BeforeSkinIncisionDetail): Observable<BeforeSkinIncisionDetail> {
    return this.http.patch<BeforeSkinIncisionDetail>(environment.tcApiBaseUri + "before_skin_incision/" + item.id, item);
  }

  deleteBeforeSkinIncision(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "before_skin_incision/" + id);
  }
 beforeSkinIncisionDetailsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_skin_incision/do", new TCDoParam(method, payload));
  }
  beforeSkinIncisionDetailDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "before_skin_incision/" + id + "/do", new TCDoParam(method, payload));
  }
  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "before_skin_incision/do", new TCDoParam("download", ids));
}
  
 }