import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehavioralPainScaleDetailComponent } from './behavioral-pain-scale-detail.component';

describe('BehavioralPainScaleDetailComponent', () => {
  let component: BehavioralPainScaleDetailComponent;
  let fixture: ComponentFixture<BehavioralPainScaleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BehavioralPainScaleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehavioralPainScaleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
