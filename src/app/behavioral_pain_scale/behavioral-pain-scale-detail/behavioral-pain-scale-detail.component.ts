import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';
import { BehavioralPainScaleDetail } from '../behavioral_pain_scale.model';
import { BehavioralPainScaleNavigator } from '../behavioral_pain_scale.navigator';
import { BehavioralPainScalePersist } from '../behavioral_pain_scale.persist';

export enum BehavioralPainScaleTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-behavioral-pain-scale-detail',
  templateUrl: './behavioral-pain-scale-detail.component.html',
  styleUrls: ['./behavioral-pain-scale-detail.component.scss'],
})
export class BehavioralPainScaleDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  behavioralPainScaleIsLoading: boolean = false;

  BehavioralPainScaleTabs: typeof BehavioralPainScaleTabs =
    BehavioralPainScaleTabs;
  activeTab: BehavioralPainScaleTabs = BehavioralPainScaleTabs.overview;
  //basics
  behavioralPainScaleDetail: BehavioralPainScaleDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public behavioralPainScaleNavigator: BehavioralPainScaleNavigator,
    public behavioralPainScalePersist: BehavioralPainScalePersist
  ) {
    this.tcAuthorization.requireRead('behavioral_pain_scales');
    this.behavioralPainScaleDetail = new BehavioralPainScaleDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('behavioral_pain_scales');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.behavioralPainScaleIsLoading = true;
    this.behavioralPainScalePersist.getBehavioralPainScale(id).subscribe(
      (behavioralPainScaleDetail) => {
        this.behavioralPainScaleDetail = behavioralPainScaleDetail;
        this.behavioralPainScaleIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.behavioralPainScaleIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.behavioralPainScaleDetail.id);
  }
  editBehavioralPainScale(): void {
    let modalRef = this.behavioralPainScaleNavigator.editBehavioralPainScale(
      this.behavioralPainScaleDetail.id
    );
    modalRef.afterClosed().subscribe(
      (behavioralPainScaleDetail) => {
        TCUtilsAngular.assign(
          this.behavioralPainScaleDetail,
          behavioralPainScaleDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.behavioralPainScalePersist
      .print(this.behavioralPainScaleDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(
          printJob.id,
          'print behavioral_pain_scale',
          true
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
