import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehavioralPainScaleEditComponent } from './behavioral-pain-scale-edit.component';

describe('BehavioralPainScaleEditComponent', () => {
  let component: BehavioralPainScaleEditComponent;
  let fixture: ComponentFixture<BehavioralPainScaleEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BehavioralPainScaleEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehavioralPainScaleEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
