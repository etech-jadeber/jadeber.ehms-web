

import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BehavioralPainScaleDetail } from '../behavioral_pain_scale.model';
import { BehavioralPainScalePersist } from '../behavioral_pain_scale.persist';
@Component({
  selector: 'app-behavioral-pain-scale-edit',
  templateUrl: './behavioral-pain-scale-edit.component.html',
  styleUrls: ['./behavioral-pain-scale-edit.component.scss']
})
export class BehavioralPainScaleEditComponent implements OnInit {

  isLoadingResults: boolean = false;


  title: string;
  behavioralPainScaleDetail: BehavioralPainScaleDetail;
  total: number;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BehavioralPainScaleEditComponent>,
              public  persist: BehavioralPainScalePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("behavioral_pain_scales");
      this.title = this.appTranslation.getText("general","new") +  " " + "Behavioral Pain Scale (BPS) Tool";
      this.behavioralPainScaleDetail = new BehavioralPainScaleDetail();
      this.behavioralPainScaleDetail.encounter_id = this.idMode.id;
      
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("behavioral_pain_scales");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "Behavioral Pain Scale (BPS) Tool";
      this.isLoadingResults = true;
      this.persist.getBehavioralPainScale(this.idMode.id).subscribe(behavioralPainScaleDetail => {
        this.behavioralPainScaleDetail = behavioralPainScaleDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addBehavioralPainScale(this.behavioralPainScaleDetail).subscribe(value => {
      this.tcNotification.success("behavioralPainScale added");
      this.behavioralPainScaleDetail.id = value.id;
      this.dialogRef.close(this.behavioralPainScaleDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateBehavioralPainScale(this.behavioralPainScaleDetail).subscribe(value => {
      this.tcNotification.success("behavioral_pain_scale updated");
      this.dialogRef.close(this.behavioralPainScaleDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.behavioralPainScaleDetail == null){
            return false;
          }

          return true;

 }



 isDisabled():boolean{
  return false;
}
 }