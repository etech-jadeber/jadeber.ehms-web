import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehavioralPainScaleListComponent } from './behavioral-pain-scale-list.component';

describe('BehavioralPainScaleListComponent', () => {
  let component: BehavioralPainScaleListComponent;
  let fixture: ComponentFixture<BehavioralPainScaleListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BehavioralPainScaleListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehavioralPainScaleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
