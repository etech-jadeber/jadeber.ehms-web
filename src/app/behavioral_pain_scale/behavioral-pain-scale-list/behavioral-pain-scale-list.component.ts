import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { BehavioralPainScaleSummary, BehavioralPainScaleSummaryPartialList } from '../behavioral_pain_scale.model';
import { BehavioralPainScaleNavigator } from '../behavioral_pain_scale.navigator';
import { BehavioralPainScalePersist } from '../behavioral_pain_scale.persist';
@Component({
  selector: 'app-behavioral-pain-scale-list',
  templateUrl: './behavioral-pain-scale-list.component.html',
  styleUrls: ['./behavioral-pain-scale-list.component.scss']
})

export class BehavioralPainScaleListComponent implements OnInit {
  behavioralPainScalesData: BehavioralPainScaleSummary[] = [];
  behavioralPainScalesTotalCount: number = 0;
  behavioralPainScaleSelectAll:boolean = false;
  behavioralPainScaleSelection: BehavioralPainScaleSummary[] = [];

 behavioralPainScalesDisplayedColumns: string[] = ["select","action" ,"date","facial_expression","upper_limbs","compliance_with_ventilation","pain_rank" ];
  behavioralPainScaleSearchTextBox: FormControl = new FormControl();
  behavioralPainScaleIsLoading: boolean = false;  
  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) behavioralPainScalesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) behavioralPainScalesSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public behavioralPainScalePersist: BehavioralPainScalePersist,
                public behavioralPainScaleNavigator: BehavioralPainScaleNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("behavioral_pain_scales");
       this.behavioralPainScaleSearchTextBox.setValue(behavioralPainScalePersist.behavioralPainScaleSearchText);
      //delay subsequent keyup events
      this.behavioralPainScaleSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.behavioralPainScalePersist.behavioralPainScaleSearchText = value;
        this.searchBehavioral_pain_scales();
      });
    } ngOnInit() {
   
      this.behavioralPainScalesSort.sortChange.subscribe(() => {
        this.behavioralPainScalesPaginator.pageIndex = 0;
        this.searchBehavioral_pain_scales(true);
      });

      this.behavioralPainScalesPaginator.page.subscribe(() => {
        this.searchBehavioral_pain_scales(true);
      });

      this.behavioralPainScalePersist.encounter_id = this.encounterId;
      //start by loading items
      this.searchBehavioral_pain_scales();
    }

  searchBehavioral_pain_scales(isPagination:boolean = false): void {


    let paginator = this.behavioralPainScalesPaginator;
    let sorter = this.behavioralPainScalesSort;

    this.behavioralPainScaleIsLoading = true;
    this.behavioralPainScaleSelection = [];

    this.behavioralPainScalePersist.searchBehavioralPainScale(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BehavioralPainScaleSummaryPartialList) => {
      this.behavioralPainScalesData = partialList.data;
      if (partialList.total != -1) {
        this.behavioralPainScalesTotalCount = partialList.total;
      }
      this.behavioralPainScaleIsLoading = false;
    }, error => {
      this.behavioralPainScaleIsLoading = false;
    });

  } downloadBehavioralPainScales(): void {
    if(this.behavioralPainScaleSelectAll){
         this.behavioralPainScalePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download behavioral_pain_scale", true);
      });
    }
    else{
        this.behavioralPainScalePersist.download(this.tcUtilsArray.idsList(this.behavioralPainScaleSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download behavioral_pain_scale",true);
            });
        }
  }
addBehavioral_pain_scale(): void {
    let dialogRef = this.behavioralPainScaleNavigator.addBehavioralPainScale(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBehavioral_pain_scales();
      }
    });
  }

  editBehavioralPainScale(item: BehavioralPainScaleSummary) {
    let dialogRef = this.behavioralPainScaleNavigator.editBehavioralPainScale(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBehavioralPainScale(item: BehavioralPainScaleSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("behavioral_pain_scale");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.behavioralPainScalePersist.deleteBehavioralPainScale(item.id).subscribe(response => {
          this.tcNotification.success("behavioral_pain_scale deleted");
          this.searchBehavioral_pain_scales();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }


    ngOnDestroy():void {
      this.behavioralPainScalePersist.encounter_id = null;
    }
}