import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BehavioralPainScalePickComponent } from './behavioral-pain-scale-pick.component';

describe('BehavioralPainScalePickComponent', () => {
  let component: BehavioralPainScalePickComponent;
  let fixture: ComponentFixture<BehavioralPainScalePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BehavioralPainScalePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BehavioralPainScalePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
