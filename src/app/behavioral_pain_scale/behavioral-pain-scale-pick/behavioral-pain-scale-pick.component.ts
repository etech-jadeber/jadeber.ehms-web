import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { BehavioralPainScaleSummary, BehavioralPainScaleSummaryPartialList } from '../behavioral_pain_scale.model';
import { BehavioralPainScalePersist } from '../behavioral_pain_scale.persist';
@Component({
  selector: 'app-behavioral-pain-scale-pick',
  templateUrl: './behavioral-pain-scale-pick.component.html',
  styleUrls: ['./behavioral-pain-scale-pick.component.scss']
})export class BehavioralPainScalePickComponent implements OnInit {
  behavioralPainScalesData: BehavioralPainScaleSummary[] = [];
  behavioralPainScalesTotalCount: number = 0;
  behavioralPainScaleSelectAll:boolean = false;
  behavioralPainScaleSelection: BehavioralPainScaleSummary[] = [];

 behavioralPainScalesDisplayedColumns: string[] = ["select","action" ,"patient_id" ];
  behavioralPainScaleSearchTextBox: FormControl = new FormControl();
  behavioralPainScaleIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) behavioralPainScalesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) behavioralPainScalesSort: MatSort;  
  constructor(               
     public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public dialogRef : MatDialogRef<BehavioralPainScalePickComponent>,
                public appTranslation:AppTranslation,
                public behavioralPainScalePersist: BehavioralPainScalePersist,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("behavioral_pain_scales");
       this.behavioralPainScaleSearchTextBox.setValue(behavioralPainScalePersist.behavioralPainScaleSearchText);
      //delay subsequent keyup events
      this.behavioralPainScaleSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.behavioralPainScalePersist.behavioralPainScaleSearchText = value;
        this.searchBehavioral_pain_scales();
      });
    } ngOnInit() {
   
      this.behavioralPainScalesSort.sortChange.subscribe(() => {
        this.behavioralPainScalesPaginator.pageIndex = 0;
        this.searchBehavioral_pain_scales(true);
      });

      this.behavioralPainScalesPaginator.page.subscribe(() => {
        this.searchBehavioral_pain_scales(true);
      });
      //start by loading items
      this.searchBehavioral_pain_scales();
    }

  searchBehavioral_pain_scales(isPagination:boolean = false): void {


    let paginator = this.behavioralPainScalesPaginator;
    let sorter = this.behavioralPainScalesSort;

    this.behavioralPainScaleIsLoading = true;
    this.behavioralPainScaleSelection = [];

    this.behavioralPainScalePersist.searchBehavioralPainScale(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BehavioralPainScaleSummaryPartialList) => {
      this.behavioralPainScalesData = partialList.data;
      if (partialList.total != -1) {
        this.behavioralPainScalesTotalCount = partialList.total;
      }
      this.behavioralPainScaleIsLoading = false;
    }, error => {
      this.behavioralPainScaleIsLoading = false;
    });

  }
  markOneItem(item: BehavioralPainScaleSummary) {
    if(!this.tcUtilsArray.containsId(this.behavioralPainScaleSelection,item.id)){
          this.behavioralPainScaleSelection = [];
          this.behavioralPainScaleSelection.push(item);
        }
        else{
          this.behavioralPainScaleSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.behavioralPainScaleSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }