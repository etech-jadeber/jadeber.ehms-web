import { TCId } from '../tc/models';
export class BehavioralPainScaleSummary extends TCId {
  facial_expression: string;
  upper_limbs: string;
  compliance_with_ventilation: string;
  encounter_id: string;
  patient_id: string;
  date: number;
}
export class BehavioralPainScaleSummaryPartialList {
  data: BehavioralPainScaleSummary[];
  total: number;
}
export class BehavioralPainScaleDetail extends BehavioralPainScaleSummary {}

export class BehavioralPainScaleDashboard {
  total: number;
}
