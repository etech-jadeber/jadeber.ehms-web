import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { BehavioralPainScaleEditComponent } from './behavioral-pain-scale-edit/behavioral-pain-scale-edit.component';
import { BehavioralPainScalePickComponent } from './behavioral-pain-scale-pick/behavioral-pain-scale-pick.component';

@Injectable({
  providedIn: 'root',
})
export class BehavioralPainScaleNavigator {

  constructor(private router: Router, public dialog: MatDialog) {}
  behavioral_pain_scalesUrl(): string {
    return '/behavioral_pain_scales';
  }

  behavioralPainScaleUrl(id: string): string {
    return '/behavioral_pain_scales/' + id;
  }

  viewBehavioralPainScales(): void {
    this.router.navigateByUrl(this.behavioral_pain_scalesUrl());
  }

  viewBehavioralPainScale(id: string): void {
    this.router.navigateByUrl(this.behavioralPainScaleUrl(id));
  }

  editBehavioralPainScale(
    id: string
  ): MatDialogRef<BehavioralPainScaleEditComponent> {
    const dialogRef = this.dialog.open(BehavioralPainScaleEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addBehavioralPainScale(encounterId: string): MatDialogRef<BehavioralPainScaleEditComponent> {
    const dialogRef = this.dialog.open(BehavioralPainScaleEditComponent, {
      data: new TCIdMode(encounterId, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickBehavioralPainScales(
    selectOne: boolean = false
  ): MatDialogRef<BehavioralPainScalePickComponent> {
    const dialogRef = this.dialog.open(BehavioralPainScalePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
