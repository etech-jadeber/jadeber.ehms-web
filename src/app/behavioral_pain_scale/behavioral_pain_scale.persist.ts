import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  BehavioralPainScaleDashboard,
  BehavioralPainScaleDetail,
  BehavioralPainScaleSummaryPartialList,
} from './behavioral_pain_scale.model';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class BehavioralPainScalePersist {
  behavioralPainScaleSearchText: string = '';
  encounter_id: string;

  behavior = [
    {id:"Relaxed(score = 1)" ,value:1},
    {id:"Partially tightend(e.g., brow lowering)(score = 2)" ,value:2},
    {id:"Fully tightend(e.g., eyelid closing)(score = 3)" ,value:3},
    {id:"Grimacing(score = 4)" ,value:4},
    {id:"No movement(score = 1)" ,value:1},
    {id:"Partially bent(score = 2)" ,value:2},
    {id:"Fully bent with finger flexion(score = 3)" ,value:3},
    {id:"Permanently retracted(score = 4)" ,value:4},
    {id:"Tolerating movement(score = 1)" ,value:1},
    {id:"Coughing with mevement(score = 2)" ,value:2},
    {id:"Fighting ventilator(score = 3)" ,value:3},
    {id:"Unable to control ventilation(score = 4)" ,value:4}
  ]

  painRank = [
    {id:3, value: "Painless"},
    {id:4, value: "Mild"},
    {id:5, value: "Mild"},
    {id:6, value: "Mild"},
    {id:7, value: "Moderate"},
    {id:8, value: "Moderate"},
    {id:9, value: "Moderate"},
    {id:10, value: "Sever pain"},
    {id:11, value: "Sever pain"},
    {id:12, value: "Sever pain"},
  ]
  
  constructor(private http: HttpClient) {}

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.behavioralPainScaleSearchText;
    //add custom filters
    return fltrs;
  }

  searchBehavioralPainScale(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<BehavioralPainScaleSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'behavioral_pain_scale',
      this.behavioralPainScaleSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );

    if(this.encounter_id){
        url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounter_id)
      }
    return this.http.get<BehavioralPainScaleSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  calcPain (total: number): string {
    return this.painRank.find(value => value.id == total)?.value || "";
   }
  
   calculateTotal(behavioralPainScaleDetail : BehavioralPainScaleDetail): number {      
    return (this.behavior.find(value => value.id ==behavioralPainScaleDetail.facial_expression)?.value || 0) + (this.behavior.find(value => value.id ==behavioralPainScaleDetail.upper_limbs)?.value || 0)  + (this.behavior.find(value => value.id ==behavioralPainScaleDetail.compliance_with_ventilation)?.value || 0) 
   }

  behavioralPainScaleDashboard(): Observable<BehavioralPainScaleDashboard> {
    return this.http.get<BehavioralPainScaleDashboard>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/dashboard'
    );
  }

  getBehavioralPainScale(id: string): Observable<BehavioralPainScaleDetail> {
    return this.http.get<BehavioralPainScaleDetail>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/' + id
    );
  }

  addBehavioralPainScale(item: BehavioralPainScaleDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "behavioral_pain_scale",
      item
    );
  }

  updateBehavioralPainScale(item: BehavioralPainScaleDetail): Observable<BehavioralPainScaleDetail> {
    return this.http.patch<BehavioralPainScaleDetail>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/' + item.id,
      item
    );
  }

  deleteBehavioralPainScale(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'behavioral_pain_scale/' + id);
  }
  behavioralPainScalesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/do',
      new TCDoParam(method, payload)
    );
  }

  behavioralPainScaleDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'behavioral_pain_scale/' + id + '/do',
      new TCDoParam('print', {})
    );
  }

}
