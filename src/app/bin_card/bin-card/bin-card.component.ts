import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {ItemDetail, ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {ItemNavigator} from "../../items/item.navigator";
import {StoreNavigator} from "../../stores/store.navigator";
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { Item_In_StorePersist } from 'src/app/item_in_stores/item_in_store.persist';
import { Bin_cardSummary, Bin_cardSummaryPartialList } from 'src/app/item_in_stores/item_in_store.model';
import { Item_Ledger_EntryPersist } from 'src/app/item_ledger_entrys/item_ledger_entry.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';



@Component({
  selector: 'app-bin-card',
  templateUrl: './bin-card.component.html',
  styleUrls: ['./bin-card.component.scss']
})

export class BinCardComponent implements OnInit {

  bin_cardsData: Bin_cardSummary[] = [];
  bin_cardsTotalCount: number = 0;
  bin_cardsSelectAll: boolean = false;
  bin_cardsSelection: Bin_cardSummary[] = [];
  itemDetail: ItemDetail;
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
  // bin_cardsDisplayedColumns: string[] = ["select", "name", "entry_type", "quantity", "from","to", "batch_no", "date"];

  bin_cardsDisplayedColumns: string[] = ["select","date", "batch_no", "entry_type", "in","out", "balance"];

  bin_cardSearchTextBox: FormControl = new FormControl();
  bin_cardsIsLoading: boolean = false;
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""}) 
  @Input() item_id: string;

  @ViewChild(MatPaginator, {static: true}) bin_cardsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) bin_cardsSort: MatSort;
  bin_cardPersist: any;
  balance: number;
  bin_cardNavigator: any;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public itemPersist: ItemPersist,
              public itemNavigator: ItemNavigator,
              public formEncounterPersist: Form_EncounterPersist,
              public storeNavigator: StoreNavigator,
              public appTranslation: AppTranslation,
              public itemInStorePersist: Item_In_StorePersist,
              public itemLedgeryPersist: Item_Ledger_EntryPersist,
              public jobPersist: JobPersist,
              public itemsPersist: ItemPersist,
              public storePersist: StorePersist,
              public categoryPersist: ItemCategoryPersist,
              public categoryNavigator: ItemCategoryNavigator,
              public tcUtilsString: TCUtilsString,
  ) {

    this.tcAuthorization.requireRead("bin_cards");
    this.bin_cardSearchTextBox.setValue(itemInStorePersist.bin_cardSearchText);
    //delay subsequent keyup events
    this.bin_cardSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.itemInStorePersist.bin_cardSearchText = value;
      this.searchBin_cards();
    });
  }

  ngOnInit() {

    this.bin_cardsSort.sortChange.subscribe(() => {
      this.bin_cardsPaginator.pageIndex = 0;
      this.searchBin_cards(true);
    });

    this.bin_cardsPaginator.page.subscribe(() => {
      this.searchBin_cards(true);
    });
    this.from_date.valueChanges.pipe().subscribe(value => {
      this.searchBin_cards();
    });

    this.to_date.valueChanges.pipe().subscribe(value => {
      this.searchBin_cards();
    });
    //start by loading items
    this.loadItem();
    this.searchBin_cards();
    // this.loadItems();
  }


  loadItem(): void {
    this.itemPersist.getItem(this.item_id).subscribe((item)=>{
      if(item){
        this.itemDetail = item;
      }
    })
  }

  searchBin_cards(isPagination: boolean = false): void {

    let paginator = this.bin_cardsPaginator;
    let sorter = this.bin_cardsSort;

    this.bin_cardsIsLoading = true;
    this.bin_cardsSelection = [];
    this.itemInStorePersist.binCardSearch(this.item_id, paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction, this.from_date.value, this.to_date.value).subscribe((partialList: Bin_cardSummaryPartialList) => {
      this.bin_cardsData = partialList.data;
      if (partialList.total != -1) {
        this.bin_cardsTotalCount = partialList.total;
        this.balance = partialList.balance;
      }
      this.bin_cardsIsLoading = false;
    }, error => {
      this.bin_cardsIsLoading = false;
    });

  }

  downloadBin_cards(): void {
    if (this.bin_cardsSelectAll) {
      this.bin_cardPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download bin_cards", true);
      });
    } else {
      this.bin_cardPersist.download(this.tcUtilsArray.idsList(this.bin_cardsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download bin_cards", true);
      });
    }
  }


  addBin_card(): void {
    let dialogRef = this.bin_cardNavigator.addBin_card();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBin_cards();
      }
    });
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.bin_cardPersist.store_name = result[0].name;
        this.bin_cardPersist.store_id = result[0].id;
        this.searchBin_cards();
      }
    });
  }
 
  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.bin_cardPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.bin_cardPersist.categoryId = result[0].id;
      }
      this.searchBin_cards()
    })
  }
  
  editBin_card(item: Bin_cardSummary) {
    let dialogRef = this.bin_cardNavigator.editBin_card(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchBin_cards();
      }

    });
  }

  deleteBin_card(item: Bin_cardSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Item Ledger Entry");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.bin_cardPersist.deleteBin_card(item.id).subscribe(response => {
          this.tcNotification.success("Bin_card deleted");
          this.searchBin_cards();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

  // loadItems() {
  //   this.itemsPersist.itemsDo('get_all_items', '').subscribe((result) => {
  //     this.items = (result as ItemSummaryPartialList).data;
  //   });
  // }


}
