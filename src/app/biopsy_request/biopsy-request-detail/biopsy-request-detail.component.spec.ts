import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BiopsyRequestDetailComponent } from './biopsy-request-detail.component';

describe('BiopsyRequestDetailComponent', () => {
  let component: BiopsyRequestDetailComponent;
  let fixture: ComponentFixture<BiopsyRequestDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BiopsyRequestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiopsyRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
