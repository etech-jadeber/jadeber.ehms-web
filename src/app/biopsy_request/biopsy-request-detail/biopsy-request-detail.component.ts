import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { BiopsyRequestDetail } from '../biopsy_request.model';
import { BiopsyRequestPersist } from '../biopsy_request.persist';
import { BiopsyRequestNavigator } from '../biopsy_request.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';


export enum BiopsyRequestTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-biopsy_request-detail',
  templateUrl: './biopsy-request-detail.component.html',
  styleUrls: ['./biopsy-request-detail.component.css'],
})
export class BiopsyRequestDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  biopsyRequestIsLoading: boolean = false;

  BiopsyRequestTabs: typeof BiopsyRequestTabs = BiopsyRequestTabs;
  activeTab: BiopsyRequestTabs = BiopsyRequestTabs.overview;
  //basics
  biopsyRequestDetail: BiopsyRequestDetail;
  doctorName: string;
  patientName: string;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public biopsyRequestNavigator: BiopsyRequestNavigator,
    public biopsyRequestPersist: BiopsyRequestPersist,
    public doctorPersist: DoctorPersist,
    public patientPersist: PatientPersist,
    public form_EncounterPersist: Form_EncounterPersist
  ) {
    this.tcAuthorization.requireRead('biopsy_request');
    this.biopsyRequestDetail = new BiopsyRequestDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('biopsy_request');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.biopsyRequestIsLoading = true;
    this.biopsyRequestPersist.getBiopsyRequest(id).subscribe(
      (biopsyRequestDetail) => {
        this.biopsyRequestDetail = biopsyRequestDetail;
        this.doctorPersist
          .getDoctor(biopsyRequestDetail.doctor_id)
          .subscribe((result) => {
            this.doctorName =
              result.first_name +
              ' ' +
              result.middle_name +
              ' ' +
              result.last_name;
          });

        this.form_EncounterPersist.getForm_Encounter(biopsyRequestDetail.encounter_id).subscribe((enc_result) => {
          this.patientPersist
            .getPatient(enc_result.pid)
            .subscribe((patient_result) => {
              this.patientName = patient_result.fname + " "+ patient_result.mname + " " + patient_result.lname
            });
        })
        this.biopsyRequestIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.biopsyRequestIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.biopsyRequestDetail.id);
  }
  editBiopsyRequest(): void {
    let modalRef = this.biopsyRequestNavigator.editBiopsyRequest(
      this.biopsyRequestDetail.id
    );
    modalRef.afterClosed().subscribe(
      (biopsyRequestDetail) => {
        TCUtilsAngular.assign(this.biopsyRequestDetail, biopsyRequestDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.biopsyRequestPersist
      .print(this.biopsyRequestDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print biopsy_request', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
