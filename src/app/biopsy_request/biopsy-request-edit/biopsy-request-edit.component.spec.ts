import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BiopsyRequestEditComponent } from './biopsy-request-edit.component';

describe('BiopsyRequestEditComponent', () => {
  let component: BiopsyRequestEditComponent;
  let fixture: ComponentFixture<BiopsyRequestEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BiopsyRequestEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiopsyRequestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
