import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BiopsyRequestDetail } from '../biopsy_request.model';
import { BiopsyRequestPersist } from '../biopsy_request.persist';
@Component({
  selector: 'app-biopsy_request-edit',
  templateUrl: './biopsy-request-edit.component.html',
  styleUrls: ['./biopsy-request-edit.component.css'],
})
export class BiopsyRequestEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  biopsyRequestDetail: BiopsyRequestDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<BiopsyRequestEditComponent>,
    public persist: BiopsyRequestPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('biopsy_request');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'biopsy_request';
      this.biopsyRequestDetail = new BiopsyRequestDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('biopsy_request');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'biopsy_request';
      this.isLoadingResults = true;
      this.persist.getBiopsyRequest(this.idMode.id).subscribe(
        (biopsyRequestDetail) => {
          this.biopsyRequestDetail = biopsyRequestDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addBiopsyRequest(this.idMode.id, this.biopsyRequestDetail).subscribe(
      (value) => {
        this.tcNotification.success('biopsyRequest added');
        this.biopsyRequestDetail.id = value.id;
        this.dialogRef.close(this.biopsyRequestDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateBiopsyRequest(this.biopsyRequestDetail).subscribe(
      (value) => {
        this.tcNotification.success('biopsy_request updated');
        this.dialogRef.close(this.biopsyRequestDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.biopsyRequestDetail == null) {
      return false;
    }
    if (
      this.biopsyRequestDetail.hospital_submitting == null ||
      this.biopsyRequestDetail.hospital_submitting == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.ward == null ||
      this.biopsyRequestDetail.ward == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.class == null ||
      this.biopsyRequestDetail.class == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.ethic_group == null ||
      this.biopsyRequestDetail.ethic_group == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.previous_biopsy_no == null ||
      this.biopsyRequestDetail.previous_biopsy_no == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.nature_of_specimen == null ||
      this.biopsyRequestDetail.nature_of_specimen == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.duration_of_lession == null ||
      this.biopsyRequestDetail.duration_of_lession == 0
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.sign_of_infiltration == null ||
      this.biopsyRequestDetail.sign_of_infiltration == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.symptoms_and_signs == null ||
      this.biopsyRequestDetail.symptoms_and_signs == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.lmp == null ||
      this.biopsyRequestDetail.lmp == ''
    ) {
      return false;
    }
    if (
      this.biopsyRequestDetail.relevant_lab_data == null ||
      this.biopsyRequestDetail.relevant_lab_data == ''
    ) {
      return false;
    }
    return true;
  }
}
