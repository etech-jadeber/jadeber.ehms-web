import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BiopsyRequestListComponent } from './biopsy-request-list.component';

describe('BiopsyRequestListComponent', () => {
  let component: BiopsyRequestListComponent;
  let fixture: ComponentFixture<BiopsyRequestListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BiopsyRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiopsyRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
