import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { TCId, TCIdMode, TCModalModes } from '../../tc/models';
import { Form_EncounterPersist } from '../../form_encounters/form_encounter.persist'
import { PatientPersist } from '../../patients/patients.persist';

import { AppTranslation } from '../../app.translation';
import {
  BiopsyRequestSummary,
  BiopsyRequestSummaryPartialList,
} from '../biopsy_request.model';
import { BiopsyRequestPersist } from '../biopsy_request.persist';
import { BiopsyRequestNavigator } from '../biopsy_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { orderFilterColumn, OrdersDetail, OrdersSummary, OrdersSummaryPartialList } from '../../form_encounters/orderss/orders.model';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { lab_order_type, lab_urgency, OrderStatus, ResultType } from 'src/app/app.enums';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
@Component({
  selector: 'app-biopsy_request-list',
  templateUrl: './biopsy-request-list.component.html',
  styleUrls: ['./biopsy-request-list.component.css'],
})
export class BiopsyRequestListComponent implements OnInit {
  biopsyRequestsData: BiopsyRequestSummary[] = [];
  biopsyRequestsTotalCount: number = 0;
  biopsyRequestSelectAll: boolean = false;
  biopsyRequestSelection: BiopsyRequestSummary[] = [];
  patOrderFilterColumn = orderFilterColumn;

  orderssData: OrdersSummary[] = [];
  orderssTotalCount: number = 0;
  orderssSelectAll:boolean = false;
  orderssSelection: OrdersSummary[] = [];

  orderssIsLoading: boolean = false;

  orderssSearchTextBox: FormControl = new FormControl();

  biopsyRequestsDisplayedColumns: string[] = [
    'select',
    'action',
    'link',
    'hospital_submitting',
    'ward',
    'class',
    'ethic_group',
    'nature_of_specimen',
    'duration_of_lession',
    'sign_of_infiltration',
    'symptoms_and_signs',
    'lmp',
    'relevant_lab_data',
  ];
  orderssDisplayedColumns: string[] = ["select", "action", 'p.pid', "patient_id", "provider_id", "generated_sample_id", "future_date" ,"total_order","completed", "rejected", "requested", "patient_type" ];
  biopsyRequestSearchTextBox: FormControl = new FormControl();
  biopsyRequestIsLoading: boolean = false;
  doctors: {[id: string]: DoctorDetail} = {}
  lab_order_id: string;
  lab_order_name: string;
  startDateTextField: FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  endDateTextField: FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  @ViewChild(MatPaginator, { static: true })
  biopsyRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) biopsyRequestsSort: MatSort;
  @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public biopsyRequestPersist: BiopsyRequestPersist,
    public biopsyRequestNavigator: BiopsyRequestNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public formEncounterPersist: Form_EncounterPersist,
    public patientPersist: PatientPersist,
    public ordersPersist: OrdersPersist,
    public outsidePersist: Outside_OrdersPersist,
    public tcUtilsString: TCUtilsString,
    public encounterPersist: Form_EncounterPersist,
    public doctorPersist: DoctorPersist,
    public ordersNavigator: OrdersNavigator,
    public labOrderNavigator: Lab_OrderNavigator

  ) {
    this.tcAuthorization.requireRead('pathology_orders');
    this.biopsyRequestSearchTextBox.setValue(
      biopsyRequestPersist.biopsyRequestSearchText
    );
    this.orderssSearchTextBox.setValue(ordersPersist.patOrderSearchHistory.search_text);
      //delay subsequent keyup events
      this.orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.patOrderSearchHistory.search_text = value;
        this.searchOrderss();
      });
    //delay subsequent keyup events
    this.biopsyRequestSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.biopsyRequestPersist.biopsyRequestSearchText = value;
        this.searchOrderss();
      });

      this.ordersPersist.patOrderSearchHistory.ordered_start_date = this.tcUtilsDate.toTimeStamp(new Date())
      this.startDateTextField.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.patOrderSearchHistory.ordered_start_date  = value._d ? tcUtilsDate.toTimeStamp(value._d) : null
        this.searchOrderss();
      });
      this.endDateTextField.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.patOrderSearchHistory.ordered_end_date = value._d ? tcUtilsDate.toTimeStamp(value._d) : null
        this.searchOrderss();
      });
  }

  ngOnDestroy() {
    // this.ordersPersist.orderStatusFilter = null;
    //   this.ordersPersist.lab_order_id = null;
    //   this.ordersPersist.orderedStartDate = null;
    //   this.ordersPersist.orderedEndDate = null;
    //   this.ordersPersist.lab_order_statuId = null;
    //   this.ordersPersist.lab_urgencyId = null;
    //   this.ordersPersist.resultDate = null;
    //   this.ordersPersist.show_only_mine = null;
  }

  ngOnInit() {
    // this.ordersPersist.orderStatusFilter = OrderStatus.not_completed
    this.biopsyRequestsSort.sortChange.subscribe(() => {
      this.biopsyRequestsPaginator.pageIndex = 0;
      this.searchOrderss(true);
    });

    this.biopsyRequestsPaginator.page.subscribe(() => {
      this.searchOrderss(true);
    });
    //start by loading items
    this.searchOrderss();
  }

  pringOrder(order: OrdersDetail){
    this.ordersPersist.ordersDo(order.id, "print_order", {}).subscribe(
      (downloadJob: TCId) => {
        this.jobPersist.followJob(downloadJob.id, 'printing Order', true);
        let success_state = 3;
        this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
          if (job.job_state == success_state) {
          }
        });
      },
      (error) => {}
    )
  }

  searchBiopsy_requests(isPagination: boolean = false): void {
    let paginator = this.biopsyRequestsPaginator;
    let sorter = this.biopsyRequestsSort;

    this.biopsyRequestIsLoading = true;
    this.biopsyRequestSelection = [];

    this.biopsyRequestPersist
      .searchBiopsyRequest(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: BiopsyRequestSummaryPartialList) => {
          this.biopsyRequestsData = partialList.data;
          this.biopsyRequestsData.forEach(biopsy => {
            this.formEncounterPersist
              .getForm_Encounter(biopsy.encounter_id)
              .subscribe((enc_result) => {
                this.patientPersist
                  .getPatient(enc_result.pid)
                  .subscribe((patient_result) => {
                    biopsy.patient_name =
                      patient_result.fname +
                      ' ' +
                      patient_result.mname +
                      ' ' +
                      patient_result.lname;
                  });
              });
          })
          if (partialList.total != -1) {
            this.biopsyRequestsTotalCount = partialList.total;
          }
          this.biopsyRequestIsLoading = false;
        },
        (error) => {
          this.biopsyRequestIsLoading = false;
        }
      );
  }

  downloadOrderss(){
    // TODO implement here
  }
  searchLabOrder(){
    const dialogRef = this.labOrderNavigator.pickLab_Orders(true, lab_order_type.pathology);
    dialogRef.afterClosed().subscribe(
      (order: Lab_OrderDetail[]) => {
        if (order.length){
          this.lab_order_id = order[0].id
          this.lab_order_name = order[0].name
          this.searchOrderss()
        }
      }
    )
  }

  searchOrderss(isPagination:boolean = false): void {
    let paginator = this.biopsyRequestsPaginator;
    let sorter = this.biopsyRequestsSort;
    this.orderssIsLoading = true;
    this.ordersPersist.orderssDo("get_pat_order",{lab_order_id: this.lab_order_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc', this.ordersPersist.patOrderSearchHistory).subscribe(
      (labs:OrdersSummaryPartialList)=>{
        this.orderssData = labs.data;
        this.orderssData.forEach(
          orders => {
            if (!this.doctors[orders.provider_id]){
              this.doctors[orders.provider_id] = new DoctorDetail()
              orders.provider_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(orders.provider_id).subscribe(
                doctor => {
                  this.doctors[orders.provider_id] = doctor;
                }
              )
            }
            this.ordersPersist.ordersDo(orders.id, "has_paid_order", {}).subscribe((value: {is_paid: boolean}) => {
              orders.is_paid = value.is_paid
             })
          }
        )
        if (labs.total != -1) {
          this.orderssTotalCount = labs.total;
        }
        this.orderssIsLoading = false
      }, error => {
        this.orderssIsLoading = false
      }
    )


  }

  loadOrderDetail(orders: OrdersDetail): void {
    if(!orders.generated_sample_id){
      this.tcNotification.error("Sample ID is not generated")
    }
    else{
      this.router.navigate([this.biopsyRequestNavigator.biopsy_requestUrl(orders.id)]);
    }
  }

  // getPatientStatus(order: OrdersDetail){
  //   if (order.urgency == lab_urgency.critical){
  //     return 'red'
  //   }
  //   if (order.urgency = lab_urgency.urgent){
  //     return '#1A73E8'
  //   }else {
  //     return 'green'
  //   }
  // }

  getPatientStatus(order: OrdersDetail){
    if (order.urgency == lab_urgency.critical || !order.is_paid){
      return 'red'
    }
    if (order.urgency = lab_urgency.urgent){
      return '#1A73E8'
    }else {
      return 'green'
    }
  }

  generateSampleId(order: OrdersDetail) {
    this.ordersPersist.ordersDo(order.id, "generate_sample_id", {}).subscribe((result: Lab_OrderDetail) => {
      this.searchOrderss();
    })
  }

  downloadBiopsyRequests(): void {
    if (this.biopsyRequestSelectAll) {
      this.biopsyRequestPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download biopsy_request',
          true
        );
      });
    } else {
      this.biopsyRequestPersist
        .download(this.tcUtilsArray.idsList(this.biopsyRequestSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download biopsy_request',
            true
          );
        });
    }
  }
  addBiopsy_request(): void {
    let dialogRef = this.biopsyRequestNavigator.addBiopsyRequest(
      this.idMode.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchBiopsy_requests();
      }
    });
  }

  editBiopsyRequest(item: BiopsyRequestSummary) {
    let dialogRef = this.biopsyRequestNavigator.editBiopsyRequest(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteBiopsyRequest(item: BiopsyRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('biopsy_request');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.biopsyRequestPersist.deleteBiopsyRequest(item.id).subscribe(
          (response) => {
            this.tcNotification.success('biopsy_request deleted');
            this.searchBiopsy_requests();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }

  getDoctor(id: string){
    const {first_name, middle_name, last_name} = this.doctors[id]
    return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
  }
}
