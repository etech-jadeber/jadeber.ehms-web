import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BiopsyRequestPickComponent } from './biopsy-request-pick.component';

describe('BiopsyRequestPickComponent', () => {
  let component: BiopsyRequestPickComponent;
  let fixture: ComponentFixture<BiopsyRequestPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BiopsyRequestPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiopsyRequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
