import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
;
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  BiopsyRequestSummary,
  BiopsyRequestSummaryPartialList,
} from '../biopsy_request.model';
import { BiopsyRequestPersist } from '../biopsy_request.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-biopsy_request-pick',
  templateUrl: './biopsy-request-pick.component.html',
  styleUrls: ['./biopsy-request-pick.component.css'],
})
export class BiopsyRequestPickComponent implements OnInit {
  biopsyRequestsData: BiopsyRequestSummary[] = [];
  biopsyRequestsTotalCount: number = 0;
  biopsyRequestSelectAll: boolean = false;
  biopsyRequestSelection: BiopsyRequestSummary[] = [];

  biopsyRequestsDisplayedColumns: string[] = [
    'select',
    ,
    'patient_id',
    'hospital_submitting',
    'ward',
    'class',
    'ethic_group',
    'previous_biopsy_no',
    'nature_of_specimen',
    'duration_of_lession',
    'sign_of_infiltration',
    'symptoms_and_signs',
    'lmp',
    'relevant_lab_data',
    'doctor_name',
    'doctor_id',
    'patient_name',
  ];
  biopsyRequestSearchTextBox: FormControl = new FormControl();
  biopsyRequestIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  biopsyRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) biopsyRequestsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public biopsyRequestPersist: BiopsyRequestPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<BiopsyRequestPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('biopsy_request');
    this.biopsyRequestSearchTextBox.setValue(
      biopsyRequestPersist.biopsyRequestSearchText
    );
    //delay subsequent keyup events
    this.biopsyRequestSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.biopsyRequestPersist.biopsyRequestSearchText = value;
        this.searchBiopsy_requests();
      });
  }
  ngOnInit() {
    this.biopsyRequestsSort.sortChange.subscribe(() => {
      this.biopsyRequestsPaginator.pageIndex = 0;
      this.searchBiopsy_requests(true);
    });

    this.biopsyRequestsPaginator.page.subscribe(() => {
      this.searchBiopsy_requests(true);
    });
    //start by loading items
    this.searchBiopsy_requests();
  }

  searchBiopsy_requests(isPagination: boolean = false): void {
    let paginator = this.biopsyRequestsPaginator;
    let sorter = this.biopsyRequestsSort;

    this.biopsyRequestIsLoading = true;
    this.biopsyRequestSelection = [];

    this.biopsyRequestPersist
      .searchBiopsyRequest(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: BiopsyRequestSummaryPartialList) => {
          this.biopsyRequestsData = partialList.data;
          if (partialList.total != -1) {
            this.biopsyRequestsTotalCount = partialList.total;
          }
          this.biopsyRequestIsLoading = false;
        },
        (error) => {
          this.biopsyRequestIsLoading = false;
        }
      );
  }
  markOneItem(item: BiopsyRequestSummary) {
    if (!this.tcUtilsArray.containsId(this.biopsyRequestSelection, item.id)) {
      this.biopsyRequestSelection = [];
      this.biopsyRequestSelection.push(item);
    } else {
      this.biopsyRequestSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.biopsyRequestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
