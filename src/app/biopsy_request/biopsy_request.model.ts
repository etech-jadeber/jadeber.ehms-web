import { TCId } from '../tc/models';
export class BiopsyRequestSummary extends TCId {
  encounter_id: string;
  hospital_submitting: string;
  ward: string;
  class: string;
  ethic_group: string;
  previous_biopsy_no: string;
  nature_of_specimen: string;
  duration_of_lession: number;
  sign_of_infiltration: string;
  symptoms_and_signs: string;
  lmp: string;
  relevant_lab_data: string;
  doctor_id: string;
  patient_name: string;
}
export class BiopsyRequestSummaryPartialList {
  data: BiopsyRequestSummary[];
  total: number;
}
export class BiopsyRequestDetail extends BiopsyRequestSummary {}

export class BiopsyRequestDashboard {
  total: number;
}
