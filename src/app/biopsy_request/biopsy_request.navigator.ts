import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { BiopsyRequestEditComponent } from './biopsy-request-edit/biopsy-request-edit.component';
import { BiopsyRequestPickComponent } from './biopsy-request-pick/biopsy-request-pick.component';

@Injectable({
  providedIn: 'root',
})
export class BiopsyRequestNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  biopsy_requestsUrl(): string {
    return '/pathology_orders';
  }

  biopsy_requestUrl(id: string): string {
    return '/pathology_orders/' + id;
  }

  viewBiopsyRequests(): void {
    this.router.navigateByUrl(this.biopsy_requestsUrl());
  }

  viewBiopsyRequest(id: string): void {
    this.router.navigateByUrl(this.biopsy_requestUrl(id));
  }

  editBiopsyRequest(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BiopsyRequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addBiopsyRequest(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BiopsyRequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickBiopsyRequests(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BiopsyRequestPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
