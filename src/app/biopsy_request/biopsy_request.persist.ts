import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  BiopsyRequestDashboard,
  BiopsyRequestDetail,
  BiopsyRequestSummaryPartialList,
} from './biopsy_request.model';

@Injectable({
  providedIn: 'root',
})
export class BiopsyRequestPersist {
  biopsyRequestSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.biopsyRequestSearchText;
    //add custom filters
    return fltrs;
  }

  searchBiopsyRequest(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<BiopsyRequestSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'biopsy_request',
      this.biopsyRequestSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<BiopsyRequestSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'biopsy_request/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  biopsyRequestDashboard(): Observable<BiopsyRequestDashboard> {
    return this.http.get<BiopsyRequestDashboard>(
      environment.tcApiBaseUri + 'biopsy_request/dashboard'
    );
  }

  getBiopsyRequest(id: string): Observable<BiopsyRequestDetail> {
    return this.http.get<BiopsyRequestDetail>(
      environment.tcApiBaseUri + 'biopsy_request/' + id
    );
  }
  addBiopsyRequest(id: string, item: BiopsyRequestDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'form_encounter/' + id + '/biopsy_request',
      item
    );
  }

  updateBiopsyRequest(
    item: BiopsyRequestDetail
  ): Observable<BiopsyRequestDetail> {
    return this.http.patch<BiopsyRequestDetail>(
      environment.tcApiBaseUri + 'biopsy_request/' + item.id,
      item
    );
  }

  deleteBiopsyRequest(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'biopsy_request/' + id);
  }
  biopsyRequestsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'biopsy_request/do',
      new TCDoParam(method, payload)
    );
  }

  biopsyRequestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'biopsy_requests/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'biopsy_request/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'biopsy_request/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
