import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BirthDetailComponent } from './birth-detail.component';

describe('BirthDetailComponent', () => {
  let component: BirthDetailComponent;
  let fixture: ComponentFixture<BirthDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
