import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {BirthDetail} from "../birth.model";
import {BirthPersist} from "../birth.persist";
import {BirthNavigator} from "../birth.navigator";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';

export enum BirthTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-birth-detail',
  templateUrl: './birth-detail.component.html',
  styleUrls: ['./birth-detail.component.css']
})
export class BirthDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  birthLoading:boolean = false;
  
  BirthTabs: typeof BirthTabs = BirthTabs;
  activeTab: BirthTabs = BirthTabs.overview;
  //basics
  birthDetail: BirthDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public form_encounterPersist: Form_EncounterPersist,
              public birthNavigator: BirthNavigator,
              public  birthPersist: BirthPersist) {
    this.tcAuthorization.requireRead("births");
    this.birthDetail = new BirthDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("births");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.birthLoading = true;
    this.birthPersist.getBirth(id).subscribe(birthDetail => {
          this.birthDetail = birthDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(birthDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.birthLoading = false;
        }, error => {
          console.error(error);
          this.birthLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.birthDetail.id);
  }

  editBirth(): void {
    let modalRef = this.birthNavigator.editBirth(this.birthDetail.id);
    modalRef.afterClosed().subscribe(modifiedBirthDetail => {
      TCUtilsAngular.assign(this.birthDetail, modifiedBirthDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.birthPersist.print(this.birthDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print birth", true);
      });
    }

  back():void{
      this.location.back();
    }

}
