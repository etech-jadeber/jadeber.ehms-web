import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BirthEditComponent } from './birth-edit.component';

describe('BirthEditComponent', () => {
  let component: BirthEditComponent;
  let fixture: ComponentFixture<BirthEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
