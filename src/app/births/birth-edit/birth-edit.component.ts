import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {BirthDetail} from "../birth.model";
import {BirthPersist} from "../birth.persist";
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';


@Component({
  selector: 'app-birth-edit',
  templateUrl: './birth-edit.component.html',
  styleUrls: ['./birth-edit.component.css']
})
export class BirthEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  birthDetail: BirthDetail;
  birth_time:Date = new Date();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<BirthEditComponent>,
              public  persist: BirthPersist,
              public patientNavigator: PatientNavigator,
              public patientPersist: PatientPersist,
              public doctorNavigator: DoctorNavigator,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("births");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("general", "birth");
      this.birthDetail = new BirthDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("births");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("general", "birth");
      this.isLoadingResults = true;
      this.persist.getBirth(this.idMode.id).subscribe(birthDetail => {
        this.birthDetail = birthDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.birthDetail.birth_time = this.tcUtilsDate.toTimeStamp(this.birth_time);
    this.persist.addBirth(this.idMode.id,this.birthDetail).subscribe(value => {
      this.tcNotification.success("Birth added");
      this.birthDetail.id = value.id;
      this.dialogRef.close(this.birthDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.birthDetail.birth_time = this.tcUtilsDate.toTimeStamp(this.birth_time);

    this.persist.updateBirth(this.birthDetail).subscribe(value => {
      this.tcNotification.success("Birth updated");
      this.dialogRef.close(this.birthDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.birthDetail == null){
            return false;
          }

        if (this.birthDetail.mothername == null || this.birthDetail.mothername  == "") {
                      return false;
                    }

if (this.birthDetail.fathers_name == null || this.birthDetail.fathers_name  == "") {
                      return false;
                    }

if (this.birthDetail.given_name == null || this.birthDetail.given_name  == "") {
                      return false;
                    }


        return true;
      }
  
  patientFullName:string
  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.birthDetail.mothername = result[0].id;
      }
    });
  }

  getPatientName(patientId: string) {
    this.patientPersist.getPatient(patientId).subscribe((
        patient
      ) => {
        this.patientFullName = `${patient.fname} ${patient.mname} ${patient.lname}`;
      }
    )
  }

  userFullName:string;
  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.userFullName = `${result[0].first_name} ${result[0].last_name}`;
        this.birthDetail.doctors_id = result[0].id;
      }
    });
  }
}
