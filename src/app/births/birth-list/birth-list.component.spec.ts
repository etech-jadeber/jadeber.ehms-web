import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BirthListComponent } from './birth-list.component';

describe('BirthListComponent', () => {
  let component: BirthListComponent;
  let fixture: ComponentFixture<BirthListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
