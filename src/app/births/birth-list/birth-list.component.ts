import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {BirthPersist} from "../birth.persist";
import {BirthNavigator} from "../birth.navigator";
import {BirthDetail, BirthSummary, BirthSummaryPartialList} from "../birth.model";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { interval } from 'rxjs';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';


@Component({
  selector: 'app-birth-list',
  templateUrl: './birth-list.component.html',
  styleUrls: ['./birth-list.component.css']
})
export class BirthListComponent implements OnInit {

  birthsData: BirthSummary[] = [];
  birthsTotalCount: number = 0;
  birthsSelectAll:boolean = false;
  birthsSelection: BirthSummary[] = [];

  birthsDisplayedColumns: string[] = ["select","action", "mothername","fathers_name","given_name","birth_time","weight","height","doctors_id" ];
  birthsSearchTextBox: FormControl = new FormControl();
  birthsIsLoading: boolean = false;
//job states
unknown_state = -1;
queued_state = 1;
running_state = 2;
success_state = 3;
failed_state = 4;
aborted_state = 5;
message:string = '';

  @Input() encounterId :string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();
  @ViewChild(MatPaginator, {static: true}) birthsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) birthsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public birthPersist: BirthPersist,
                public birthNavigator: BirthNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public form_encounterPersist: Form_EncounterPersist,
                public doctorPersist: DoctorPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("births");
       this.birthsSearchTextBox.setValue(birthPersist.birthSearchText);
      //delay subsequent keyup events
      this.birthsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.birthPersist.birthSearchText = value;
        this.searchBirths();
      });
    }

    ngOnInit() {
      this.birthPersist.patientId = this.patientId
      this.getAllDoctors();
      this.loadPatients();
      this.birthsSort.sortChange.subscribe(() => {
        this.birthsPaginator.pageIndex = 0;
        this.searchBirths(true);
      });

      this.birthsPaginator.page.subscribe(() => {
        this.searchBirths(true);
      });
      //start by loading items
      this.searchBirths();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.birthPersist.encounterId = this.encounterId;
      } else {
      this.birthPersist.encounterId = null;
      }
      this.searchBirths()
      }

  searchBirths(isPagination:boolean = false): void {


    let paginator = this.birthsPaginator;
    let sorter = this.birthsSort;

    this.birthsIsLoading = true;
    this.birthsSelection = [];

    this.birthPersist.searchBirth(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: BirthSummaryPartialList) => {
      this.birthsData = partialList.data;
      if (partialList.total != -1) {
        this.birthsTotalCount = partialList.total;
      }
      this.birthsIsLoading = false;
    }, error => {
      this.birthsIsLoading = false;
    });

  }

  downloadBirths(): void {
    if(this.birthsSelectAll){
         this.birthPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download births", true);
      });
    }
    else{
        this.birthPersist.download(this.tcUtilsArray.idsList(this.birthsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download births",true);
            });
        }
  }



  addBirth(): void {
    let dialogRef = this.birthNavigator.addBirth(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchBirths();
      }
    });
  }

  editBirth(item: BirthSummary) {
    let dialogRef = this.birthNavigator.editBirth(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteBirth(item: BirthSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Birth");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.birthPersist.deleteBirth(item.id).subscribe(response => {
          this.tcNotification.success("Birth deleted");
          this.searchBirths();
        }, error => {
        });
      }

    });
  }
  patientFullName:string;
  patients: PatientSummary[] = [];
  doctorSummary: DoctorSummary[] = [];
  loadPatients() {
    this.patientPersist
      .patientsDo("get_all_patients", "")
      .subscribe((result) => {
        this.patients = (result as PatientSummaryPartialList).data;
      });
  }

  getPatientName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patients,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }


  getDoctorName(doctorId: string): string {
    let doctors: DoctorSummary = this.tcUtilsArray.getById(
      this.doctorSummary,
      doctorId
    );
    if (doctors) {
      return `${doctors.first_name} ${doctors.last_name}`;
    }

  }

  getAllDoctors(): void {
    this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
      this.doctorSummary = doctors;
    });
  }

  printBirth(item: BirthSummary){

    this.birthPersist.birthDo(item.id, "print_birth", item).subscribe(downloadJob => {

      this.jobPersist.followJob(downloadJob.id, "printing birth", true);
      let success_state = 3;
      this.jobPersist.getJob(downloadJob.id).subscribe(
        job => {
          if (job.job_state == success_state) {
          }
        }

      )
    }, error => {
    })
  }
  followJob(id: string, descrption: string, is_download_job: boolean = true): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }


    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe(n => {

      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

            if (is_download_job) {
              this.jobPersist.downloadFileKey(id).subscribe(jobFile => {
                this.tcNavigator.openUrl(this.filePersist.downloadLink(jobFile.download_key))
              });
            }

        }
        else{
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);
          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }

  back():void{
      this.location.back();
    }

}
