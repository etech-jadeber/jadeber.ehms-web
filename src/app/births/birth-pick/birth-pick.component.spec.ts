import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BirthPickComponent } from './birth-pick.component';

describe('BirthPickComponent', () => {
  let component: BirthPickComponent;
  let fixture: ComponentFixture<BirthPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
