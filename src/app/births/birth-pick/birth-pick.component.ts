import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {BirthDetail, BirthSummary, BirthSummaryPartialList} from "../birth.model";
import {BirthPersist} from "../birth.persist";


@Component({
  selector: 'app-birth-pick',
  templateUrl: './birth-pick.component.html',
  styleUrls: ['./birth-pick.component.css']
})
export class BirthPickComponent implements OnInit {

  birthsData: BirthSummary[] = [];
  birthsTotalCount: number = 0;
  birthsSelection: BirthSummary[] = [];
  birthsDisplayedColumns: string[] = ["select", 'mothername','fathers_name','given_name','birth_time','weight','height','doctors_id','patient_id' ];

  birthsSearchTextBox: FormControl = new FormControl();
  birthsIsLoading: boolean = false;
  encounterId:string
  @ViewChild(MatPaginator, {static: true}) birthsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) birthsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public birthPersist: BirthPersist,
              public dialogRef: MatDialogRef<BirthPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("births");
    this.birthsSearchTextBox.setValue(birthPersist.birthSearchText);
    //delay subsequent keyup events
    this.birthsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.birthPersist.birthSearchText = value;
      this.searchBirths();
    });
  }

  ngOnInit() {

    this.birthsSort.sortChange.subscribe(() => {
      this.birthsPaginator.pageIndex = 0;
      this.searchBirths();
    });

    this.birthsPaginator.page.subscribe(() => {
      this.searchBirths();
    });

    //set initial picker list to 5
    this.birthsPaginator.pageSize = 5;

    //start by loading items
    this.searchBirths();
  }

  searchBirths(): void {
    this.birthsIsLoading = true;
    this.birthsSelection = [];

    this.birthPersist.searchBirth(this.birthsPaginator.pageSize,
        this.birthsPaginator.pageIndex,
        this.birthsSort.active,
        this.birthsSort.direction).subscribe((partialList: BirthSummaryPartialList) => {
      this.birthsData = partialList.data;
      if (partialList.total != -1) {
        this.birthsTotalCount = partialList.total;
      }
      this.birthsIsLoading = false;
    }, error => {
      this.birthsIsLoading = false;
    });

  }

  markOneItem(item: BirthSummary) {
    if(!this.tcUtilsArray.containsId(this.birthsSelection,item.id)){
          this.birthsSelection = [];
          this.birthsSelection.push(item);
        }
        else{
          this.birthsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.birthsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
