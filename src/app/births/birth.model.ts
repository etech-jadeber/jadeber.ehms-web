import {TCId} from "../tc/models";

export class BirthSummary extends TCId {
  mothername : string;
fathers_name : string;
given_name : string;
birth_time : number;
weight : number;
height : number;
doctors_id : string;
patient_id : string;
encounter_id:string;
}

export class BirthSummaryPartialList {
  data: BirthSummary[];
  total: number;
}

export class BirthDetail extends BirthSummary {
  mothername : string;
fathers_name : string;
given_name : string;
birth_time : number;
weight : number;
height : number;
doctors_id : string;
patient_id : string;
}

export class BirthDashboard {
  total: number;
}
