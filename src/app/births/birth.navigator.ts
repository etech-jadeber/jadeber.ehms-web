import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {BirthEditComponent} from "./birth-edit/birth-edit.component";
import {BirthPickComponent} from "./birth-pick/birth-pick.component";


@Injectable({
  providedIn: 'root'
})

export class BirthNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  birthsUrl(): string {
    return "/births";
  }

  birthUrl(id: string): string {
    return "/births/" + id;
  }

  viewBirths(): void {
    this.router.navigateByUrl(this.birthsUrl());
  }

  viewBirth(id: string): void {
    this.router.navigateByUrl(this.birthUrl(id));
  }

  editBirth(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BirthEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addBirth(encounter_id:string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(BirthEditComponent, {
          data: new TCIdMode(encounter_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickBirths(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(BirthPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
