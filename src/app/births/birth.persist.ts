import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {BirthDashboard, BirthDetail, BirthSummaryPartialList} from "./birth.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class BirthPersist {

  birthSearchText: string = "";
  encounterId: string;
  patientId: string;

  constructor(private http: HttpClient) {
  }

  searchBirth(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<BirthSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("births", this.birthSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<BirthSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.birthSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "births/do", new TCDoParam("download_all", this.filters()));
  }

  birthDashboard(): Observable<BirthDashboard> {
    return this.http.get<BirthDashboard>(environment.tcApiBaseUri + "births/dashboard");
  }

  getBirth(id: string): Observable<BirthDetail> {
    return this.http.get<BirthDetail>(environment.tcApiBaseUri + "births/" + id);
  }

  addBirth(parent_id:string, item: BirthDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + 'form_encounter/' +parent_id + "/births", item);
  }

  updateBirth(item: BirthDetail): Observable<BirthDetail> {
    return this.http.patch<BirthDetail>(environment.tcApiBaseUri + "births/" + item.id, item);
  }

  deleteBirth(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "births/" + id);
  }

  birthsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "births/do", new TCDoParam(method, payload));
  }

  birthDo(id: string, method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "births/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "births/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "births/" + id + "/do", new TCDoParam("print", {}));
  }


}
