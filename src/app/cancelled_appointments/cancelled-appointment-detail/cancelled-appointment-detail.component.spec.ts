import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CancelledAppointmentDetailComponent } from './cancelled-appointment-detail.component';

describe('CancelledAppointmentDetailComponent', () => {
  let component: CancelledAppointmentDetailComponent;
  let fixture: ComponentFixture<CancelledAppointmentDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledAppointmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledAppointmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
