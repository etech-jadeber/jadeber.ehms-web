import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Cancelled_AppointmentDetail} from "../cancelled_appointment.model";
import {Cancelled_AppointmentPersist} from "../cancelled_appointment.persist";
import {Cancelled_AppointmentNavigator} from "../cancelled_appointment.navigator";

export enum Cancelled_AppointmentTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-cancelled_appointment-detail',
  templateUrl: './cancelled-appointment-detail.component.html',
  styleUrls: ['./cancelled-appointment-detail.component.css']
})
export class Cancelled_AppointmentDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  cancelled_appointmentLoading:boolean = false;
  
  Cancelled_AppointmentTabs: typeof Cancelled_AppointmentTabs = Cancelled_AppointmentTabs;
  activeTab: Cancelled_AppointmentTabs = Cancelled_AppointmentTabs.overview;
  //basics
  cancelled_appointmentDetail: Cancelled_AppointmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public cancelled_appointmentNavigator: Cancelled_AppointmentNavigator,
              public  cancelled_appointmentPersist: Cancelled_AppointmentPersist) {
    this.tcAuthorization.requireRead("cancelled_appointments");
    this.cancelled_appointmentDetail = new Cancelled_AppointmentDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("cancelled_appointments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.cancelled_appointmentLoading = true;
    this.cancelled_appointmentPersist.getCancelled_Appointment(id).subscribe(cancelled_appointmentDetail => {
          this.cancelled_appointmentDetail = cancelled_appointmentDetail;
          this.cancelled_appointmentLoading = false;
        }, error => {
          console.error(error);
          this.cancelled_appointmentLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.cancelled_appointmentDetail.id);
  }

  editCancelled_Appointment(): void {
    let modalRef = this.cancelled_appointmentNavigator.editCancelled_Appointment(this.cancelled_appointmentDetail.id);
    modalRef.afterClosed().subscribe(modifiedCancelled_AppointmentDetail => {
      TCUtilsAngular.assign(this.cancelled_appointmentDetail, modifiedCancelled_AppointmentDetail);
    }, error => {
      console.error(error);
    });
  }

   printCancelled_Appointment():void{
      this.cancelled_appointmentPersist.print(this.cancelled_appointmentDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print cancelled_appointment", true);
      });
    }

  back():void{
      this.location.back();
    }

}
