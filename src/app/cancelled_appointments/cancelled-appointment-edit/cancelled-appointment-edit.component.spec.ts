import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CancelledAppointmentEditComponent } from './cancelled-appointment-edit.component';

describe('CancelledAppointmentEditComponent', () => {
  let component: CancelledAppointmentEditComponent;
  let fixture: ComponentFixture<CancelledAppointmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledAppointmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledAppointmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
