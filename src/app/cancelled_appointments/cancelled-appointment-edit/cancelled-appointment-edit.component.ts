import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";


import {Cancelled_AppointmentDetail} from "../cancelled_appointment.model";
import {Cancelled_AppointmentPersist} from "../cancelled_appointment.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AvailabilityPersist } from 'src/app/availabilitys/availability.persist';
import { AvailabilitySummary } from 'src/app/availabilitys/availability.model';
import { repetition } from 'src/app/app.enums';


@Component({
  selector: 'app-cancelled_appointment-edit',
  templateUrl: './cancelled-appointment-edit.component.html',
  styleUrls: ['./cancelled-appointment-edit.component.css']
})
export class Cancelled_AppointmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  cancelled_appointmentDetail: Cancelled_AppointmentDetail;

  repetition = repetition;

  startDate:Date = new Date();
  endDate:Date = new Date();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public availabilityPersist:AvailabilityPersist,
              private tCUtilsDate:TCUtilsDate,
              public dialogRef: MatDialogRef<Cancelled_AppointmentEditComponent>,
              public  persist: Cancelled_AppointmentPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("cancelled_appointments");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("schedule", "cancelled_appointment");
      this.cancelled_appointmentDetail = new Cancelled_AppointmentDetail();
      let availId = this.idMode.context['appointmentId'];
      this.cancelled_appointmentDetail.day_of_the_month = -1;
      this.cancelled_appointmentDetail.day_of_the_week = -1;
      this.cancelled_appointmentDetail.repetition = -1;

      if(availId != null){
        this.availabilityPersist.getAvailability(availId).subscribe(
          (avail:AvailabilitySummary)=>{
            this.cancelled_appointmentDetail.availability_id = avail.id;
            this.cancelled_appointmentDetail.doctor_id = avail.user_id;
          }
        )
      }
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("cancelled_appointments");
      this.title = this.appTranslation.getText("general","edit") +  " "+ this.appTranslation.getText("schedule", "cancelled_appointment");
      this.isLoadingResults = true;
      this.persist.getCancelled_Appointment(this.idMode.childId).subscribe(cancelled_appointmentDetail => {
        this.cancelled_appointmentDetail = cancelled_appointmentDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.transformDates(true);
    this.isLoadingResults = true;
    this.persist.addCancelled_Appointment(this.cancelled_appointmentDetail).subscribe(value => {
      this.tcNotification.success("Cancelled_Appointment added");
      this.cancelled_appointmentDetail.id = value.id;
      this.dialogRef.close(this.cancelled_appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateCancelled_Appointment(this.cancelled_appointmentDetail).subscribe(value => {
      this.tcNotification.success("Cancelled_Appointment updated");
      this.dialogRef.close(this.cancelled_appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.cancelled_appointmentDetail == null){
            return false;
          }

        if (this.cancelled_appointmentDetail.availability_id == null || this.cancelled_appointmentDetail.availability_id  == "") {
                      return false;
        }
        if (this.cancelled_appointmentDetail.doctor_id == null || this.cancelled_appointmentDetail.doctor_id  == "") {
          return false;
        }
        if (this.cancelled_appointmentDetail.repetition == null ) {
          return false;
        }
        if (this.cancelled_appointmentDetail.day_of_the_month == null ) {
          return false;
        }
        if (this.cancelled_appointmentDetail.day_of_the_week == null) {
          return false;
        }

       

                    


        return true;
      }

      transformDates(todate: boolean = true) {
        if (todate) {
         
        
          this.cancelled_appointmentDetail.cancelled_end_date = this.tCUtilsDate.toTimeStamp(this.endDate);
          this.cancelled_appointmentDetail.cancelled_start_date = this.tCUtilsDate.toTimeStamp(this.startDate);
        
        } else {
    
        }
      }
    

}
