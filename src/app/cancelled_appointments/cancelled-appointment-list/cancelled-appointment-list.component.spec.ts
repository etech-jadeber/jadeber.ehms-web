import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CancelledAppointmentListComponent } from './cancelled-appointment-list.component';

describe('CancelledAppointmentListComponent', () => {
  let component: CancelledAppointmentListComponent;
  let fixture: ComponentFixture<CancelledAppointmentListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledAppointmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledAppointmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
