import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Cancelled_AppointmentPersist} from "../cancelled_appointment.persist";
import {Cancelled_AppointmentNavigator} from "../cancelled_appointment.navigator";
import {Cancelled_AppointmentDetail, Cancelled_AppointmentSummary, Cancelled_AppointmentSummaryPartialList} from "../cancelled_appointment.model";


@Component({
  selector: 'app-cancelled_appointment-list',
  templateUrl: './cancelled-appointment-list.component.html',
  styleUrls: ['./cancelled-appointment-list.component.css']
})
export class Cancelled_AppointmentListComponent implements OnInit {

  cancelled_appointmentsData: Cancelled_AppointmentSummary[] = [];
  cancelled_appointmentsTotalCount: number = 0;
  cancelled_appointmentsSelectAll:boolean = false;
  cancelled_appointmentsSelection: Cancelled_AppointmentSummary[] = [];

  cancelled_appointmentsDisplayedColumns: string[] = ["select", "doctor_id","cancelled_start_date","cancelled_end_date","no_of_patient", ];
  cancelled_appointmentsSearchTextBox: FormControl = new FormControl();
  cancelled_appointmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) cancelled_appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) cancelled_appointmentsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public cancelled_appointmentPersist: Cancelled_AppointmentPersist,
                public cancelled_appointmentNavigator: Cancelled_AppointmentNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("cancelled_appointments");
       this.cancelled_appointmentsSearchTextBox.setValue(cancelled_appointmentPersist.cancelled_appointmentSearchText);
      //delay subsequent keyup events
      this.cancelled_appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.cancelled_appointmentPersist.cancelled_appointmentSearchText = value;
        this.searchCancelled_Appointments();
      });
    }

    ngOnInit() {

      this.cancelled_appointmentsSort.sortChange.subscribe(() => {
        this.cancelled_appointmentsPaginator.pageIndex = 0;
        this.searchCancelled_Appointments(true);
      });

      this.cancelled_appointmentsPaginator.page.subscribe(() => {
        this.searchCancelled_Appointments(true);
      });
      //start by loading items
      this.searchCancelled_Appointments();
    }

  searchCancelled_Appointments(isPagination:boolean = false): void {


    let paginator = this.cancelled_appointmentsPaginator;
    let sorter = this.cancelled_appointmentsSort;

    this.cancelled_appointmentsIsLoading = true;
    this.cancelled_appointmentsSelection = [];

    this.cancelled_appointmentPersist.searchCancelled_Appointment(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Cancelled_AppointmentSummaryPartialList) => {
      this.cancelled_appointmentsData = partialList.data;
      if (partialList.total != -1) {
        this.cancelled_appointmentsTotalCount = partialList.total;
      }
      this.cancelled_appointmentsIsLoading = false;
    }, error => {
      this.cancelled_appointmentsIsLoading = false;
    });

  }

  downloadCancelled_Appointments(): void {
    if(this.cancelled_appointmentsSelectAll){
         this.cancelled_appointmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download cancelled_appointments", true);
      });
    }
    else{
        this.cancelled_appointmentPersist.download(this.tcUtilsArray.idsList(this.cancelled_appointmentsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download cancelled_appointments",true);
            });
        }
  }




  editCancelled_Appointment(item: Cancelled_AppointmentSummary) {
    let dialogRef = this.cancelled_appointmentNavigator.editCancelled_Appointment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCancelled_Appointment(item: Cancelled_AppointmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Cancelled_Appointment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.cancelled_appointmentPersist.deleteCancelled_Appointment(item.id).subscribe(response => {
          this.tcNotification.success("Cancelled_Appointment deleted");
          this.searchCancelled_Appointments();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
