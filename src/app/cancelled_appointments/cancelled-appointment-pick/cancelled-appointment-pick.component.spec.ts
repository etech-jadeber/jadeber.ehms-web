import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CancelledAppointmentPickComponent } from './cancelled-appointment-pick.component';

describe('CancelledAppointmentPickComponent', () => {
  let component: CancelledAppointmentPickComponent;
  let fixture: ComponentFixture<CancelledAppointmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelledAppointmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelledAppointmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
