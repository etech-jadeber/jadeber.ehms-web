import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Cancelled_AppointmentDetail, Cancelled_AppointmentSummary, Cancelled_AppointmentSummaryPartialList} from "../cancelled_appointment.model";
import {Cancelled_AppointmentPersist} from "../cancelled_appointment.persist";


@Component({
  selector: 'app-cancelled_appointment-pick',
  templateUrl: './cancelled-appointment-pick.component.html',
  styleUrls: ['./cancelled-appointment-pick.component.css']
})
export class Cancelled_AppointmentPickComponent implements OnInit {

  cancelled_appointmentsData: Cancelled_AppointmentSummary[] = [];
  cancelled_appointmentsTotalCount: number = 0;
  cancelled_appointmentsSelection: Cancelled_AppointmentSummary[] = [];
  cancelled_appointmentsDisplayedColumns: string[] = ["select", 'doctor_id','availability_id','cancelled_start_date','cancelled_end_date','day_of_the_week','day_of_the_month','repetition','reg_date','no_of_patient','reason' ];

  cancelled_appointmentsSearchTextBox: FormControl = new FormControl();
  cancelled_appointmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) cancelled_appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) cancelled_appointmentsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public cancelled_appointmentPersist: Cancelled_AppointmentPersist,
              public dialogRef: MatDialogRef<Cancelled_AppointmentPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("cancelled_appointments");
    this.cancelled_appointmentsSearchTextBox.setValue(cancelled_appointmentPersist.cancelled_appointmentSearchText);
    //delay subsequent keyup events
    this.cancelled_appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.cancelled_appointmentPersist.cancelled_appointmentSearchText = value;
      this.searchCancelled_Appointments();
    });
  }

  ngOnInit() {

    this.cancelled_appointmentsSort.sortChange.subscribe(() => {
      this.cancelled_appointmentsPaginator.pageIndex = 0;
      this.searchCancelled_Appointments();
    });

    this.cancelled_appointmentsPaginator.page.subscribe(() => {
      this.searchCancelled_Appointments();
    });

    //set initial picker list to 5
    this.cancelled_appointmentsPaginator.pageSize = 5;

    //start by loading items
    this.searchCancelled_Appointments();
  }

  searchCancelled_Appointments(): void {
    this.cancelled_appointmentsIsLoading = true;
    this.cancelled_appointmentsSelection = [];

    this.cancelled_appointmentPersist.searchCancelled_Appointment(this.cancelled_appointmentsPaginator.pageSize,
        this.cancelled_appointmentsPaginator.pageIndex,
        this.cancelled_appointmentsSort.active,
        this.cancelled_appointmentsSort.direction).subscribe((partialList: Cancelled_AppointmentSummaryPartialList) => {
      this.cancelled_appointmentsData = partialList.data;
      if (partialList.total != -1) {
        this.cancelled_appointmentsTotalCount = partialList.total;
      }
      this.cancelled_appointmentsIsLoading = false;
    }, error => {
      this.cancelled_appointmentsIsLoading = false;
    });

  }

  markOneItem(item: Cancelled_AppointmentSummary) {
    if(!this.tcUtilsArray.containsId(this.cancelled_appointmentsSelection,item.id)){
          this.cancelled_appointmentsSelection = [];
          this.cancelled_appointmentsSelection.push(item);
        }
        else{
          this.cancelled_appointmentsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.cancelled_appointmentsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
