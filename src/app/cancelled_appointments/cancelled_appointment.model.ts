import {TCId} from "../tc/models";

export class Cancelled_AppointmentSummary extends TCId {
  doctor_id : string;
availability_id : string;
cancelled_start_date : number;
cancelled_end_date : number;
day_of_the_week : number;
day_of_the_month : number;
repetition : number;
reg_date : number;
doc_name:string;
no_of_patient : number;
reason : string;
}

export class Cancelled_AppointmentSummaryPartialList {
  data: Cancelled_AppointmentSummary[];
  total: number;
}

export class Cancelled_AppointmentDetail extends Cancelled_AppointmentSummary {
  doctor_id : string;
  
availability_id : string;
cancelled_start_date : number;
cancelled_end_date : number;
day_of_the_week : number;
day_of_the_month : number;
repetition : number;
reg_date : number;
no_of_patient : number;
reason : string;
}

export class Cancelled_AppointmentDashboard {
  total: number;
}
