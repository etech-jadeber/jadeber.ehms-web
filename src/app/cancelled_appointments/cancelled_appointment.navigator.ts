import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import { Cancelled_AppointmentEditComponent } from "./cancelled-appointment-edit/cancelled-appointment-edit.component";
import { Cancelled_AppointmentPickComponent } from "./cancelled-appointment-pick/cancelled-appointment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Cancelled_AppointmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  cancelled_appointmentsUrl(): string {
    return "/cancelled_appointments";
  }

  cancelled_appointmentUrl(id: string): string {
    return "/cancelled_appointments/" + id;
  }

  viewCancelled_Appointments(): void {
    this.router.navigateByUrl(this.cancelled_appointmentsUrl());
  }

  viewCancelled_Appointment(id: string): void {
    this.router.navigateByUrl(this.cancelled_appointmentUrl(id));
  }

  editCancelled_Appointment(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Cancelled_AppointmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCancelled_Appointment(appointmentId:string, parentId= null, id = null, isWizard:boolean = false): MatDialogRef<unknown,any> {

    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = isWizard
      ? TCModalModes.WIZARD
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
    params["appointmentId"] = appointmentId;
    const dialogRef = this.dialog.open(Cancelled_AppointmentEditComponent, {
      data: new TCParentChildIds(parentId, id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;


  }
  
   pickCancelled_Appointments(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Cancelled_AppointmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
