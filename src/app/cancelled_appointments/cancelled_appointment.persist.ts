import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Cancelled_AppointmentDashboard, Cancelled_AppointmentDetail, Cancelled_AppointmentSummaryPartialList} from "./cancelled_appointment.model";


@Injectable({
  providedIn: 'root'
})
export class Cancelled_AppointmentPersist {

  cancelled_appointmentSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchCancelled_Appointment(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Cancelled_AppointmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("cancelled_appointments", this.cancelled_appointmentSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Cancelled_AppointmentSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.cancelled_appointmentSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "cancelled_appointments/do", new TCDoParam("download_all", this.filters()));
  }

  cancelled_appointmentDashboard(): Observable<Cancelled_AppointmentDashboard> {
    return this.http.get<Cancelled_AppointmentDashboard>(environment.tcApiBaseUri + "cancelled_appointments/dashboard");
  }

  getCancelled_Appointment(id: string): Observable<Cancelled_AppointmentDetail> {
    return this.http.get<Cancelled_AppointmentDetail>(environment.tcApiBaseUri + "cancelled_appointments/" + id);
  }

  addCancelled_Appointment(item: Cancelled_AppointmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "cancelled_appointments/", item);
  }

  updateCancelled_Appointment(item: Cancelled_AppointmentDetail): Observable<Cancelled_AppointmentDetail> {
    return this.http.patch<Cancelled_AppointmentDetail>(environment.tcApiBaseUri + "cancelled_appointments/" + item.id, item);
  }

  deleteCancelled_Appointment(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "cancelled_appointments/" + id);
  }

  cancelled_appointmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "cancelled_appointments/do", new TCDoParam(method, payload));
  }

  cancelled_appointmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "cancelled_appointments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "cancelled_appointments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "cancelled_appointments/" + id + "/do", new TCDoParam("print", {}));
  }


}
