import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GeneralCaseComponent } from './general-case.component';

describe('GeneralCaseComponent', () => {
  let component: GeneralCaseComponent;
  let fixture: ComponentFixture<GeneralCaseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
