import {Component, Inject, OnInit} from '@angular/core';
import {TCNavigator} from "../../tc/navigator";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserPersist} from "../../tc/users/user.persist";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNotification} from "../../tc/notification";
import {AppTranslation} from "../../app.translation";

import {CaseDetail} from "../../tc/cases/case.model";
import { PatientDetail } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientCasePersisit } from 'src/app/patients/PatientCase.persisit';

@Component({
  selector: 'app-general-case',
  templateUrl: './general-case.component.html',
  styleUrls: ['./general-case.component.css']
})
export class GeneralCaseComponent implements OnInit {

  isLoadingResults: boolean = false;
  caseDetail: CaseDetail;
  msg: string = "";
  patientDetail: PatientDetail;

  isSelf: boolean = false;

  constructor(public tcNavigator: TCNavigator,
              public tcAuthorization: TCAuthorization,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<GeneralCaseComponent>,
              public patientPersist: PatientPersist,
              public userPersist: UserPersist,
              public patientCasePersisit: PatientCasePersisit,
              @Inject(MAT_DIALOG_DATA) public id: string) {
  }

  ngOnInit() {
    if (this.id == null) {
      this.tcNavigator.invalidOperation();
    }
    if (!this.tcAuthorization.canUpdate("cases")) {
      this.isSelf = this.patientPersist.myPatientId != null;
    }
    this.loadDetails();
  }

  loadnPatient(): void {
    this.patientPersist.getPatient(this.caseDetail.target_id).subscribe(memberDetail => {
      this.patientDetail = memberDetail;
      this.isLoadingResults = false;
    });
  }

  loadDetails(): void {
    this.isLoadingResults = true;
    this.patientCasePersisit.getCase(this.id).subscribe(caseDetail => {
      this.caseDetail = caseDetail;
      this.loadnPatient();
      this.userPersist.loadUsersOfComments(this.caseDetail.comments);
      this.isLoadingResults = false;
    });
  }

  fullName(): string {
    if (!this.patientDetail) {
      return "";
    }
    return this.patientDetail.fname + " " + this.patientDetail.mname + " " + this.patientDetail.lname;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  hasMessage(): boolean {
    return this.msg.trim() != "";
  }

  onClose(): void {
    this.patientCasePersisit.caseDo(this.caseDetail.id, "close", {"remark": this.msg}).subscribe(response => {
      this.tcNotification.success("case closed");
      this.dialogRef.close(false);
    });
  }

}
