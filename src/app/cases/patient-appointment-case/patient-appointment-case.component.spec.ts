import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientAppointmentCaseComponent } from './patient-appointment-case.component';

describe('PatientAppointmentCaseComponent', () => {
  let component: PatientAppointmentCaseComponent;
  let fixture: ComponentFixture<PatientAppointmentCaseComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientAppointmentCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAppointmentCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
