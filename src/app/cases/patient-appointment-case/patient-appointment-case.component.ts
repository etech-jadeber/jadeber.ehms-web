import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PatientCasePersisit} from "../../patients/PatientCase.persisit";
import {AppointmentSummary} from "../../appointments/appointment.model";
import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsDate} from "../../tc/utils-date";
import {PatientSummary, PatientSummaryPartialList} from "../../patients/patients.model";
import {DoctorSummary} from "../../doctors/doctor.model";
import {TCUtilsArray} from "../../tc/utils-array";
import {DoctorPersist} from "../../doctors/doctor.persist";
import {PatientPersist} from "../../patients/patients.persist";
import {AppTranslation} from "../../app.translation";
import {AppointmentPersist} from "../../appointments/appointment.persist";
import {TCNotification} from "../../tc/notification";

@Component({
  selector: 'app-patient-appointment-case',
  templateUrl: './patient-appointment-case.component.html',
  styleUrls: ['./patient-appointment-case.component.css']
})
export class PatientAppointmentCaseComponent implements OnInit {

  appointmentDetail: AppointmentSummary;
  appointmentLoading: boolean = true;
  patients: PatientSummary[] = [];
  doctorSummary: DoctorSummary [] = [];
  remark: string = "";

  constructor(@Inject(MAT_DIALOG_DATA) public caseId: string,
              public tcAuthorization: TCAuthorization,
              public tcUtilDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public doctorPersist: DoctorPersist,
              public patientPersist: PatientPersist,
              public appTranslation: AppTranslation,
              public appointmentPersist: AppointmentPersist,
              public dialogRef: MatDialogRef<PatientAppointmentCaseComponent>,
              public tcNotification: TCNotification,
              private patientCasePersist: PatientCasePersisit,) {
  }

  ngOnInit() {
    this.loadPatients();
    this.getAllDoctors();

    this.patientCasePersist.getCase(this.caseId).subscribe(caseDetail => {
      const appointmentData: AppointmentSummary = caseDetail.payload["request"] as AppointmentSummary;
      appointmentData.scheduled_user_id = caseDetail.target_id;
      this.appointmentDetail = appointmentData;
      this.appointmentLoading = false;
    }, error => {
      this.appointmentLoading = false;
    });
  }


  loadPatients() {
    this.patientPersist
      .patientsDo("get_all_patients", "")
      .subscribe((result) => {
        this.patients = (result as PatientSummaryPartialList).data;
      });
  }

  getPatientFullName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patients,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }


  getDoctorFullName(doctorId: string): string {
    let doctors: DoctorSummary = this.tcUtilsArray.getById(
      this.doctorSummary,
      doctorId
    );
    if (doctors) {
      return `${doctors.first_name} ${doctors.middle_name} ${doctors.last_name}`;
    }

  }

  getAllDoctors(): void {
    this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
      this.doctorSummary = doctors;
    });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onAddAppointment() {
    this.appointmentPersist.addAppointment(this.appointmentDetail).subscribe(result => {
      this.tcNotification.success("appointment has been accepted");
      this.dialogRef.close(this.appointmentDetail);
    }, error => {
      console.error(error);
    });
  }

  onAccept(): void {
    this.patientCasePersist.caseDo(this.caseId, "close", {remark: this.remark}).subscribe(response => {
      this.onAddAppointment();
    }, error => {
      console.error(error);
    });
  }

  onReject() {
    this.patientCasePersist.caseDo(this.caseId, "reject", {remark: this.remark}).subscribe(response => {
      this.tcNotification.success("appointment has been rejected");
      this.dialogRef.close(this.appointmentDetail);
    }, error => {
      console.error(error);
    });
  }
}
