import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiefCompliantOptionDetailComponent } from './chief-compliant-option-detail.component';

describe('ChiefCompliantOptionDetailComponent', () => {
  let component: ChiefCompliantOptionDetailComponent;
  let fixture: ComponentFixture<ChiefCompliantOptionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChiefCompliantOptionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiefCompliantOptionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
