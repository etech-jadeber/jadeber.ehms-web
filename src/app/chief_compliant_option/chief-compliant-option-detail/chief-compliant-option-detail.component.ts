import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ChiefCompliantOptionDetail} from "../chief_compliant_option.model";
import {ChiefCompliantOptionPersist} from "../chief_compliant_option.persist";
import {ChiefCompliantOptionNavigator} from "../chief_compliant_option.navigator";

export enum ChiefCompliantOptionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-chief_compliant_option-detail',
  templateUrl: './chief-compliant-option-detail.component.html',
  styleUrls: ['./chief-compliant-option-detail.component.scss']
})
export class ChiefCompliantOptionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  chiefCompliantOptionIsLoading:boolean = false;
  
  ChiefCompliantOptionTabs: typeof ChiefCompliantOptionTabs = ChiefCompliantOptionTabs;
  activeTab: ChiefCompliantOptionTabs = ChiefCompliantOptionTabs.overview;
  //basics
  chiefCompliantOptionDetail: ChiefCompliantOptionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public chiefCompliantOptionNavigator: ChiefCompliantOptionNavigator,
              public  chiefCompliantOptionPersist: ChiefCompliantOptionPersist) {
    this.tcAuthorization.requireRead("chief_compliant_options");
    this.chiefCompliantOptionDetail = new ChiefCompliantOptionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("chief_compliant_options");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.chiefCompliantOptionIsLoading = true;
    this.chiefCompliantOptionPersist.getChiefCompliantOption(id).subscribe(chiefCompliantOptionDetail => {
          this.chiefCompliantOptionDetail = chiefCompliantOptionDetail;
          this.chiefCompliantOptionIsLoading = false;
        }, error => {
          console.error(error);
          this.chiefCompliantOptionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.chiefCompliantOptionDetail.id);
  }
  editChiefCompliantOption(): void {
    let modalRef = this.chiefCompliantOptionNavigator.editChiefCompliantOption(this.chiefCompliantOptionDetail.id);
    modalRef.afterClosed().subscribe(chiefCompliantOptionDetail => {
      TCUtilsAngular.assign(this.chiefCompliantOptionDetail, chiefCompliantOptionDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.chiefCompliantOptionPersist.print(this.chiefCompliantOptionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print chief_compliant_option", true);
      });
    }

  back():void{
      this.location.back();
    }

}