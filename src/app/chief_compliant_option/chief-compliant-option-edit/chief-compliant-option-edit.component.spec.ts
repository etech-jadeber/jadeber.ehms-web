import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiefCompliantOptionEditComponent } from './chief-compliant-option-edit.component';

describe('ChiefCompliantOptionEditComponent', () => {
  let component: ChiefCompliantOptionEditComponent;
  let fixture: ComponentFixture<ChiefCompliantOptionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChiefCompliantOptionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiefCompliantOptionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
