import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ChiefCompliantOptionDetail } from '../chief_compliant_option.model';import { ChiefCompliantOptionPersist } from '../chief_compliant_option.persist';@Component({
  selector: 'app-chief_compliant_option-edit',
  templateUrl: './chief-compliant-option-edit.component.html',
  styleUrls: ['./chief-compliant-option-edit.component.scss']
})export class ChiefCompliantOptionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  chiefCompliantOptionDetail: ChiefCompliantOptionDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ChiefCompliantOptionEditComponent>,
              public  persist: ChiefCompliantOptionPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("chief_compliant_options");
      this.title = this.appTranslation.getText("general","new") +  " " + "chief_compliant_option";
      this.chiefCompliantOptionDetail = new ChiefCompliantOptionDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("chief_compliant_options");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "chief_compliant_option";
      this.isLoadingResults = true;
      this.persist.getChiefCompliantOption(this.idMode.id).subscribe(chiefCompliantOptionDetail => {
        this.chiefCompliantOptionDetail = chiefCompliantOptionDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addChiefCompliantOption(this.chiefCompliantOptionDetail).subscribe(value => {
      this.tcNotification.success("chiefCompliantOption added");
      this.chiefCompliantOptionDetail.id = value.id;
      this.dialogRef.close(this.chiefCompliantOptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateChiefCompliantOption(this.chiefCompliantOptionDetail).subscribe(value => {
      this.tcNotification.success("chief_compliant_option updated");
      this.dialogRef.close(this.chiefCompliantOptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.chiefCompliantOptionDetail == null){
            return false;
          }

if (this.chiefCompliantOptionDetail.name == null || this.chiefCompliantOptionDetail.name  == "") {
            return false;
        } 
 return true;

 }
 }