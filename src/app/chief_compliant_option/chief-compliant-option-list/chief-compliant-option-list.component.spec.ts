import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiefCompliantOptionListComponent } from './chief-compliant-option-list.component';

describe('ChiefCompliantOptionListComponent', () => {
  let component: ChiefCompliantOptionListComponent;
  let fixture: ComponentFixture<ChiefCompliantOptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChiefCompliantOptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiefCompliantOptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
