import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiefCompliantOptionPickComponent } from './chief-compliant-option-pick.component';

describe('ChiefCompliantOptionPickComponent', () => {
  let component: ChiefCompliantOptionPickComponent;
  let fixture: ComponentFixture<ChiefCompliantOptionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChiefCompliantOptionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiefCompliantOptionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
