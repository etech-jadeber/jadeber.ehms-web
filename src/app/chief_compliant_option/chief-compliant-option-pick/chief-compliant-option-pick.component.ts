import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ChiefCompliantOptionSummary, ChiefCompliantOptionSummaryPartialList } from '../chief_compliant_option.model';
import { ChiefCompliantOptionPersist } from '../chief_compliant_option.persist';
import { ChiefCompliantOptionNavigator } from '../chief_compliant_option.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-chief_compliant_option-pick',
  templateUrl: './chief-compliant-option-pick.component.html',
  styleUrls: ['./chief-compliant-option-pick.component.scss']
})export class ChiefCompliantOptionPickComponent implements OnInit {
  chiefCompliantOptionsData: ChiefCompliantOptionSummary[] = [];
  chiefCompliantOptionsTotalCount: number = 0;
  chiefCompliantOptionSelectAll:boolean = false;
  chiefCompliantOptionSelection: ChiefCompliantOptionSummary[] = [];

 chiefCompliantOptionsDisplayedColumns: string[] = ["select", ,"name" ];
  chiefCompliantOptionSearchTextBox: FormControl = new FormControl();
  chiefCompliantOptionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) chiefCompliantOptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) chiefCompliantOptionsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public chiefCompliantOptionPersist: ChiefCompliantOptionPersist,
                public chiefCompliantOptionNavigator: ChiefCompliantOptionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ChiefCompliantOptionPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("chief_compliant_options");
       this.chiefCompliantOptionSearchTextBox.setValue(chiefCompliantOptionPersist.chiefCompliantOptionSearchText);
      //delay subsequent keyup events
      this.chiefCompliantOptionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.chiefCompliantOptionPersist.chiefCompliantOptionSearchText = value;
        this.searchChief_compliant_options();
      });
    } ngOnInit() {
   
      this.chiefCompliantOptionsSort.sortChange.subscribe(() => {
        this.chiefCompliantOptionsPaginator.pageIndex = 0;
        this.searchChief_compliant_options(true);
      });

      this.chiefCompliantOptionsPaginator.page.subscribe(() => {
        this.searchChief_compliant_options(true);
      });
      //start by loading items
      this.searchChief_compliant_options();
    }

  searchChief_compliant_options(isPagination:boolean = false): void {


    let paginator = this.chiefCompliantOptionsPaginator;
    let sorter = this.chiefCompliantOptionsSort;

    this.chiefCompliantOptionIsLoading = true;
    this.chiefCompliantOptionSelection = [];

    this.chiefCompliantOptionPersist.searchChiefCompliantOption(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ChiefCompliantOptionSummaryPartialList) => {
      this.chiefCompliantOptionsData = partialList.data;
      if (partialList.total != -1) {
        this.chiefCompliantOptionsTotalCount = partialList.total;
      }
      this.chiefCompliantOptionIsLoading = false;
    }, error => {
      this.chiefCompliantOptionIsLoading = false;
    });

  }
  markOneItem(item: ChiefCompliantOptionSummary) {
    if(!this.tcUtilsArray.containsId(this.chiefCompliantOptionSelection,item.id)){
          this.chiefCompliantOptionSelection = [];
          this.chiefCompliantOptionSelection.push(item);
        }
        else{
          this.chiefCompliantOptionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.chiefCompliantOptionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }