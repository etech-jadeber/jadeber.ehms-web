import {TCId} from "../tc/models";
export class ChiefCompliantOptionSummary extends TCId {
    name:string;
     
    }
    export class ChiefCompliantOptionSummaryPartialList {
      data: ChiefCompliantOptionSummary[];
      total: number;
    }
    export class ChiefCompliantOptionDetail extends ChiefCompliantOptionSummary {
    }
    
    export class ChiefCompliantOptionDashboard {
      total: number;
    }