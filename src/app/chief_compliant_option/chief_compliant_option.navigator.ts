import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ChiefCompliantOptionEditComponent} from "./chief-compliant-option-edit/chief-compliant-option-edit.component";
import {ChiefCompliantOptionPickComponent} from "./chief-compliant-option-pick/chief-compliant-option-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ChiefCompliantOptionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  chiefCompliantOptionsUrl(): string {
    return "/chief_compliant_options";
  }

  chiefCompliantOptionUrl(id: string): string {
    return "/chief_compliant_options/" + id;
  }

  viewChiefCompliantOptions(): void {
    this.router.navigateByUrl(this.chiefCompliantOptionsUrl());
  }

  viewChiefCompliantOption(id: string): void {
    this.router.navigateByUrl(this.chiefCompliantOptionUrl(id));
  }

  editChiefCompliantOption(id: string): MatDialogRef<ChiefCompliantOptionEditComponent> {
    const dialogRef = this.dialog.open(ChiefCompliantOptionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addChiefCompliantOption(): MatDialogRef<ChiefCompliantOptionEditComponent> {
    const dialogRef = this.dialog.open(ChiefCompliantOptionEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickChiefCompliantOptions(selectOne: boolean=false): MatDialogRef<ChiefCompliantOptionPickComponent> {
      const dialogRef = this.dialog.open(ChiefCompliantOptionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}