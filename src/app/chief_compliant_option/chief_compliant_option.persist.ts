import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ChiefCompliantOptionDashboard, ChiefCompliantOptionDetail, ChiefCompliantOptionSummaryPartialList} from "./chief_compliant_option.model";


@Injectable({
  providedIn: 'root'
})
export class ChiefCompliantOptionPersist {
 chiefCompliantOptionSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.chiefCompliantOptionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchChiefCompliantOption(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ChiefCompliantOptionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("chief_compliant_option", this.chiefCompliantOptionSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<ChiefCompliantOptionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "chief_compliant_option/do", new TCDoParam("download_all", this.filters()));
  }

  chiefCompliantOptionDashboard(): Observable<ChiefCompliantOptionDashboard> {
    return this.http.get<ChiefCompliantOptionDashboard>(environment.tcApiBaseUri + "chief_compliant_option/dashboard");
  }

  getChiefCompliantOption(id: string): Observable<ChiefCompliantOptionDetail> {
    return this.http.get<ChiefCompliantOptionDetail>(environment.tcApiBaseUri + "chief_compliant_option/" + id);
  } addChiefCompliantOption(item: ChiefCompliantOptionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "chief_compliant_option/", item);
  }

  updateChiefCompliantOption(item: ChiefCompliantOptionDetail): Observable<ChiefCompliantOptionDetail> {
    return this.http.patch<ChiefCompliantOptionDetail>(environment.tcApiBaseUri + "chief_compliant_option/" + item.id, item);
  }

  deleteChiefCompliantOption(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "chief_compliant_option/" + id);
  }
 chiefCompliantOptionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "chief_compliant_option/do", new TCDoParam(method, payload));
  }

  chiefCompliantOptionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "chief_compliant_options/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "chief_compliant_option/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "chief_compliant_option/" + id + "/do", new TCDoParam("print", {}));
  }


}