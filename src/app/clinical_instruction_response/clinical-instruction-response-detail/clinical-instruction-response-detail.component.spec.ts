import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalInstructionResponseDetailComponent } from './clinical-instruction-response-detail.component';

describe('ClinicalInstructionResponseDetailComponent', () => {
  let component: ClinicalInstructionResponseDetailComponent;
  let fixture: ComponentFixture<ClinicalInstructionResponseDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicalInstructionResponseDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalInstructionResponseDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
