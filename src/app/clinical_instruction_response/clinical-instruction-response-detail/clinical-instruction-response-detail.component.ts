import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ClinicalInstructionResponseDetail} from "../clinical_instruction_response.model";
import {ClinicalInstructionResponsePersist} from "../clinical_instruction_response.persist";
import {ClinicalInstructionResponseNavigator} from "../clinical_instruction_response.navigator";

export enum ClinicalInstructionResponseTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-clinical_instruction_response-detail',
  templateUrl: './clinical-instruction-response-detail.component.html',
  styleUrls: ['./clinical-instruction-response-detail.component.scss']
})
export class ClinicalInstructionResponseDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  clinicalInstructionResponseIsLoading:boolean = false;
  
  ClinicalInstructionResponseTabs: typeof ClinicalInstructionResponseTabs = ClinicalInstructionResponseTabs;
  activeTab: ClinicalInstructionResponseTabs = ClinicalInstructionResponseTabs.overview;
  //basics
  clinicalInstructionResponseDetail: ClinicalInstructionResponseDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public clinicalInstructionResponseNavigator: ClinicalInstructionResponseNavigator,
              public  clinicalInstructionResponsePersist: ClinicalInstructionResponsePersist) {
    this.tcAuthorization.requireRead("clinical_instruction_responses");
    this.clinicalInstructionResponseDetail = new ClinicalInstructionResponseDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("clinical_instruction_responses");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.clinicalInstructionResponseIsLoading = true;
    this.clinicalInstructionResponsePersist.getClinicalInstructionResponse(id).subscribe(clinicalInstructionResponseDetail => {
          this.clinicalInstructionResponseDetail = clinicalInstructionResponseDetail;
          this.clinicalInstructionResponseIsLoading = false;
        }, error => {
          console.error(error);
          this.clinicalInstructionResponseIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.clinicalInstructionResponseDetail.id);
  }
  editClinicalInstructionResponse(): void {
    let modalRef = this.clinicalInstructionResponseNavigator.editClinicalInstructionResponse(this.clinicalInstructionResponseDetail.id);
    modalRef.afterClosed().subscribe(clinicalInstructionResponseDetail => {
      TCUtilsAngular.assign(this.clinicalInstructionResponseDetail, clinicalInstructionResponseDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.clinicalInstructionResponsePersist.print(this.clinicalInstructionResponseDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print clinical_instruction_response", true);
      });
    }

  back():void{
      this.location.back();
    }

}
