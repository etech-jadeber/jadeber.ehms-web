import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalInstructionResponseEditComponent } from './clinical-instruction-response-edit.component';

describe('ClinicalInstructionResponseEditComponent', () => {
  let component: ClinicalInstructionResponseEditComponent;
  let fixture: ComponentFixture<ClinicalInstructionResponseEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicalInstructionResponseEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalInstructionResponseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
