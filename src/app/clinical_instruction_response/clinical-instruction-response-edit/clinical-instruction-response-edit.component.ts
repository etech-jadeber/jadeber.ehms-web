import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ClinicalInstructionResponseDetail } from '../clinical_instruction_response.model';import { ClinicalInstructionResponsePersist } from '../clinical_instruction_response.persist';import { Form_VitalsDetail } from 'src/app/form_encounters/form_encounter.model';
import { MedicationAdminstrationChartDetail } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.model';
import { ProcedureNoteSummary } from 'src/app/procedures/procedure.model';
@Component({
  selector: 'app-clinical_instruction_response-edit',
  templateUrl: './clinical-instruction-response-edit.component.html',
  styleUrls: ['./clinical-instruction-response-edit.component.scss']
})

export class ClinicalInstructionResponseEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  clinicalInstructionResponseDetail: ClinicalInstructionResponseDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ClinicalInstructionResponseEditComponent>,
              public  persist: ClinicalInstructionResponsePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: {id: string, response:MedicationAdminstrationChartDetail | Form_VitalsDetail | ProcedureNoteSummary, mode: string, patient_id: string}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("clinical_instruction_responses");
      this.title = this.appTranslation.getText("general","new") +  " " + "clinical_instruction_response";
      this.clinicalInstructionResponseDetail = new ClinicalInstructionResponseDetail();
      //set necessary defaults
      this.clinicalInstructionResponseDetail.patient_id = this.idMode.patient_id,
      this.clinicalInstructionResponseDetail.clinical_instruction_id = this.idMode.id
      this.clinicalInstructionResponseDetail.is_done = true;
      this.clinicalInstructionResponseDetail.complication = "";
      this.clinicalInstructionResponseDetail.remark = "";

    } else {
     this.tcAuthorization.requireUpdate("clinical_instruction_responses");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "clinical_instruction_response";
      this.isLoadingResults = true;
      this.persist.getClinicalInstructionResponse(this.idMode.id).subscribe(clinicalInstructionResponseDetail => {
        this.clinicalInstructionResponseDetail = clinicalInstructionResponseDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addClinicalInstructionResponse({item: this.clinicalInstructionResponseDetail, response: this.idMode.response}).subscribe(value => {
      this.tcNotification.success("clinicalInstructionResponse added");
      this.clinicalInstructionResponseDetail.id = value.id;
      this.dialogRef.close(this.clinicalInstructionResponseDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateClinicalInstructionResponse(this.clinicalInstructionResponseDetail).subscribe(value => {
      this.tcNotification.success("clinical_instruction_response updated");
      this.dialogRef.close(this.clinicalInstructionResponseDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.clinicalInstructionResponseDetail == null){
            return false;
          }

if (this.clinicalInstructionResponseDetail.clinical_instruction_id == null || this.clinicalInstructionResponseDetail.clinical_instruction_id  == "") {
            return false;
        } 
// if (this.clinicalInstructionResponseDetail.is_done == null) {
//             return false;
//         }
if (this.clinicalInstructionResponseDetail.patient_id == null || this.clinicalInstructionResponseDetail.patient_id  == "") {
            return false;
        }
        return true;

 }
 }
