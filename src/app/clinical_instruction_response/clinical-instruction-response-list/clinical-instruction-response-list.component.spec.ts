import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalInstructionResponseListComponent } from './clinical-instruction-response-list.component';

describe('ClinicalInstructionResponseListComponent', () => {
  let component: ClinicalInstructionResponseListComponent;
  let fixture: ComponentFixture<ClinicalInstructionResponseListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicalInstructionResponseListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalInstructionResponseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
