import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ClinicalInstructionResponseDetail, ClinicalInstructionResponseSummary, ClinicalInstructionResponseSummaryPartialList } from '../clinical_instruction_response.model';
import { ClinicalInstructionResponsePersist } from '../clinical_instruction_response.persist';
import { ClinicalInstructionResponseNavigator } from '../clinical_instruction_response.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FormClinicalInstructionDetail } from 'src/app/form_clinical_instruction/form_clinical_instruction.model';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { encounter_status, InstructionType, payment_status, ServiceType } from 'src/app/app.enums';
import { VitalNavigator } from 'src/app/vitals/vital.navigator';
import { Medication_adminstration_chartNavigator } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { MedicationAdminstrationChartDetail } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.model';
import { Form_VitalsDetail, Patient_ProcedureSummary } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { ProcedureNoteSummary } from 'src/app/procedures/procedure.model';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
@Component({
  selector: 'app-clinical_instruction_response-list',
  templateUrl: './clinical-instruction-response-list.component.html',
  styleUrls: ['./clinical-instruction-response-list.component.scss']
})
export class ClinicalInstructionResponseListComponent implements OnInit {
  clinicalInstructionResponsesData: ClinicalInstructionResponseSummary[] = [];
  clinicalInstructionResponsesTotalCount: number = 0;
  clinicalInstructionResponseSelectAll:boolean = false;
  clinicalInstructionResponseSelection: ClinicalInstructionResponseSummary[] = [];

  clinicalInstructionResponsesDisplayedColumns: string[] = ["select","action","is_done","response_date","nurse_id","complication","remark" ];
  clinicalInstructionResponseSearchTextBox: FormControl = new FormControl();
  clinicalInstructionResponseIsLoading: boolean = false; 
  nurses: {[id: string]: UserDetail } = {}
  @Input() clinicalInstruction: FormClinicalInstructionDetail
  @Input() showBar: boolean = true;
  @ViewChild(MatPaginator, {static: true}) clinicalInstructionResponsesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) clinicalInstructionResponsesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public clinicalInstructionResponsePersist: ClinicalInstructionResponsePersist,
                public clinicalInstructionResponseNavigator: ClinicalInstructionResponseNavigator,
                public form_encounterPersist: Form_EncounterPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public userPersist: UserPersist,
                public vitalNavigator: Form_EncounterNavigator,
                public procedurePersist: ProcedurePersist,
                public procedureNavigator: ProcedureNavigator,
                public paymentPersist: PaymentPersist,
                public medicationTreatmentNavigator: Medication_adminstration_chartNavigator,

    ) {

       this.tcAuthorization.requireRead("clinical_instruction_responses");
       this.clinicalInstructionResponseSearchTextBox.setValue(clinicalInstructionResponsePersist.clinicalInstructionResponseSearchText);
      //delay subsequent keyup events
      this.clinicalInstructionResponseSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.clinicalInstructionResponsePersist.clinicalInstructionResponseSearchText = value;
        this.searchClinicalInstructionResponses();
      });
    } ngOnInit() {
   
      this.clinicalInstructionResponsesSort.sortChange.subscribe(() => {
        this.clinicalInstructionResponsesPaginator.pageIndex = 0;
        this.searchClinicalInstructionResponses(true);
      });

      this.clinicalInstructionResponsesPaginator.page.subscribe(() => {
        this.searchClinicalInstructionResponses(true);
      });
      //start by loading items
      this.searchClinicalInstructionResponses();
    }

  searchClinicalInstructionResponses(isPagination:boolean = false): void {


    let paginator = this.clinicalInstructionResponsesPaginator;
    let sorter = this.clinicalInstructionResponsesSort;

    this.clinicalInstructionResponseIsLoading = true;
    this.clinicalInstructionResponseSelection = [];

    this.clinicalInstructionResponsePersist.searchClinicalInstructionResponse(this.clinicalInstruction.id, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ClinicalInstructionResponseSummaryPartialList) => {
      this.clinicalInstructionResponsesData = partialList.data;
      this.clinicalInstructionResponsesData.forEach(
        instruction => {
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(instruction.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          if (!this.nurses[instruction.nurse_id]){
            this.nurses[instruction.nurse_id] = new UserDetail()
            this.userPersist.getUser(instruction.nurse_id).subscribe(
              nurse => {
                this.nurses[instruction.nurse_id] = nurse;
              }
            )
          }
        }
      )
      if (partialList.total != -1) {
        this.clinicalInstructionResponsesTotalCount = partialList.total;
      }
      this.clinicalInstructionResponseIsLoading = false;
    }, error => {
      this.clinicalInstructionResponseIsLoading = false;
    });

  } downloadClinicalInstructionResponses(): void {
    if(this.clinicalInstructionResponseSelectAll){
         this.clinicalInstructionResponsePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download clinical_instruction_response", true);
      });
    }
    else{
        this.clinicalInstructionResponsePersist.download(this.tcUtilsArray.idsList(this.clinicalInstructionResponseSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download clinical_instruction_response",true);
            });
        }
  }

  addVital(){
    let dialogRef = this.vitalNavigator.wizardForm_Vitals(this.clinicalInstruction.encounter_id, ServiceType.inpatient);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addResponse(result)
      }
    });
  }

  addTreatment(){
    let dialogRef = this.medicationTreatmentNavigator.wizardMedication_adminstration_chart(this.clinicalInstruction.id, this.clinicalInstruction.target_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addResponse(result);
      }
    });
  }


addClinicalInstructionResponse(): void {
    if(this.clinicalInstruction.type == InstructionType.vital){
      this.addVital()
    }
    else if (this.clinicalInstruction.type == InstructionType.medication){
      this.addTreatment()
    }
    else if (this.clinicalInstruction.type == InstructionType.patient_procedure){
      this.goToNotes()
    }
    else{
      this.addResponse(null);
    }
  }

  goToNotes(): void {
    this.procedurePersist.procedureDo(this.clinicalInstruction.temp_id, "validate_consent",{encounter_id: this.clinicalInstruction.encounter_id, id: this.clinicalInstruction.target_id})
        .subscribe((response) => {
          if (!response.can_see_notes){
            this.tcNotification.error("The patient is not consent for this procedure.");
          } else {

            this.addNote()
            // element.payment_id ? this.paymentPersist.getPayment(element.payment_id).subscribe((result)=> {
            //   if(result){
            //     if(result.payment_status == payment_status.paid){
                   
            //     }
            //     else {
            //       this.tcNotification.error("Payment for this procedure is not paid")
            //     }
            //   }
            // }): this.tcNotification.error("payment id is not added");;
          }
        })
  }
  addNote(): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), "", true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const procedureNote = new ProcedureNoteSummary()
        procedureNote.patient_id = this.clinicalInstruction.patient_id;
        procedureNote.encounter_id = this.clinicalInstruction.encounter_id;
        procedureNote.note = note;
        procedureNote.patient_procedure_id = this.clinicalInstruction.target_id;

        this.addResponse(procedureNote);
       
      }
    });
  }

  addResponse(result: MedicationAdminstrationChartDetail | Form_VitalsDetail | ProcedureNoteSummary){
    let dialogRef = this.clinicalInstructionResponseNavigator.addClinicalInstructionResponse(this.clinicalInstruction.id, this.clinicalInstruction.patient_id, result);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchClinicalInstructionResponses();
      }
    });
  }

  editClinicalInstructionResponse(item: ClinicalInstructionResponseSummary) {
    let dialogRef = this.clinicalInstructionResponseNavigator.editClinicalInstructionResponse(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteClinicalInstructionResponse(item: ClinicalInstructionResponseSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("clinical_instruction_response");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.clinicalInstructionResponsePersist.deleteClinicalInstructionResponse(item.id).subscribe(response => {
          this.tcNotification.success("clinical_instruction_response deleted");
          this.searchClinicalInstructionResponses();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getNurse(id: string){
      return this.nurses[id] ? `${this.nurses[id]?.name}` : ""
    }

    getDetailUrl(element: ClinicalInstructionResponseDetail){
      if(element.type == InstructionType.medication){
        return this.medicationTreatmentNavigator.medication_adminstration_chartUrl(element.target_id)
      } 
      
      else if(element.type == InstructionType.vital) {
        return this.vitalNavigator.form_VitalUrl(element.target_id)
      }
      else if(element.type == InstructionType.patient_procedure) {
        return this.procedureNavigator.procedureNoteUrl(element.target_id)
      }
      return;
    }
}
