import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicalInstructionResponsePickComponent } from './clinical-instruction-response-pick.component';

describe('ClinicalInstructionResponsePickComponent', () => {
  let component: ClinicalInstructionResponsePickComponent;
  let fixture: ComponentFixture<ClinicalInstructionResponsePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicalInstructionResponsePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicalInstructionResponsePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
