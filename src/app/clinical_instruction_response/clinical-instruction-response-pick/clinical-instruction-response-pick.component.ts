import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ClinicalInstructionResponseSummary, ClinicalInstructionResponseSummaryPartialList } from '../clinical_instruction_response.model';
import { ClinicalInstructionResponsePersist } from '../clinical_instruction_response.persist';
import { ClinicalInstructionResponseNavigator } from '../clinical_instruction_response.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-clinical_instruction_response-pick',
  templateUrl: './clinical-instruction-response-pick.component.html',
  styleUrls: ['./clinical-instruction-response-pick.component.scss']
})export class ClinicalInstructionResponsePickComponent implements OnInit {
  clinicalInstructionResponsesData: ClinicalInstructionResponseSummary[] = [];
  clinicalInstructionResponsesTotalCount: number = 0;
  clinicalInstructionResponseSelectAll:boolean = false;
  clinicalInstructionResponseSelection: ClinicalInstructionResponseSummary[] = [];

 clinicalInstructionResponsesDisplayedColumns: string[] = ["select", ,"clinical_instruction_id","complication","is_done","nurse_id","patient_id","remark","response_date" ];
  clinicalInstructionResponseSearchTextBox: FormControl = new FormControl();
  clinicalInstructionResponseIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) clinicalInstructionResponsesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) clinicalInstructionResponsesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public clinicalInstructionResponsePersist: ClinicalInstructionResponsePersist,
                public clinicalInstructionResponseNavigator: ClinicalInstructionResponseNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ClinicalInstructionResponsePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("clinical_instruction_responses");
       this.clinicalInstructionResponseSearchTextBox.setValue(clinicalInstructionResponsePersist.clinicalInstructionResponseSearchText);
      //delay subsequent keyup events
      this.clinicalInstructionResponseSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.clinicalInstructionResponsePersist.clinicalInstructionResponseSearchText = value;
        this.searchClinical_instruction_responses();
      });
    } ngOnInit() {
   
      this.clinicalInstructionResponsesSort.sortChange.subscribe(() => {
        this.clinicalInstructionResponsesPaginator.pageIndex = 0;
        this.searchClinical_instruction_responses(true);
      });

      this.clinicalInstructionResponsesPaginator.page.subscribe(() => {
        this.searchClinical_instruction_responses(true);
      });
      //start by loading items
      this.searchClinical_instruction_responses();
    }

  searchClinical_instruction_responses(isPagination:boolean = false): void {


    let paginator = this.clinicalInstructionResponsesPaginator;
    let sorter = this.clinicalInstructionResponsesSort;

    this.clinicalInstructionResponseIsLoading = true;
    this.clinicalInstructionResponseSelection = [];

    this.clinicalInstructionResponsePersist.searchClinicalInstructionResponse('', paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ClinicalInstructionResponseSummaryPartialList) => {
      this.clinicalInstructionResponsesData = partialList.data;
      if (partialList.total != -1) {
        this.clinicalInstructionResponsesTotalCount = partialList.total;
      }
      this.clinicalInstructionResponseIsLoading = false;
    }, error => {
      this.clinicalInstructionResponseIsLoading = false;
    });

  }
  markOneItem(item: ClinicalInstructionResponseSummary) {
    if(!this.tcUtilsArray.containsId(this.clinicalInstructionResponseSelection,item.id)){
          this.clinicalInstructionResponseSelection = [];
          this.clinicalInstructionResponseSelection.push(item);
        }
        else{
          this.clinicalInstructionResponseSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.clinicalInstructionResponseSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
