import {TCId} from "../tc/models";export class ClinicalInstructionResponseSummary extends TCId {
    clinical_instruction_id:string;
    complication:string;
    is_done:boolean;
    nurse_id:string;
    patient_id:string;
    remark:string;
    response_date:number;
    type: number;
    target_id: string;
    encounter_id: string;
     
    }
    export class ClinicalInstructionResponseSummaryPartialList {
      data: ClinicalInstructionResponseSummary[];
      total: number;
    }
    export class ClinicalInstructionResponseDetail extends ClinicalInstructionResponseSummary {
    }
    
    export class ClinicalInstructionResponseDashboard {
      total: number;
    }
    