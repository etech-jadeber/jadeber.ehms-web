import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ClinicalInstructionResponseEditComponent} from "./clinical-instruction-response-edit/clinical-instruction-response-edit.component";
import {ClinicalInstructionResponsePickComponent} from "./clinical-instruction-response-pick/clinical-instruction-response-pick.component";
import { Form_VitalsDetail } from "../form_encounters/form_encounter.model";
import { MedicationAdminstrationChartDetail } from "../medication_adminstration_chart/medication_adminstration_chart.model";
import { ProcedureNoteSummary } from "../procedures/procedure.model";


@Injectable({
  providedIn: 'root'
})

export class ClinicalInstructionResponseNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  clinicalInstructionResponsesUrl(): string {
    return "/clinical_instruction_responses";
  }

  clinicalInstructionResponseUrl(id: string): string {
    return "/clinical_instruction_responses/" + id;
  }

  viewClinicalInstructionResponses(): void {
    this.router.navigateByUrl(this.clinicalInstructionResponsesUrl());
  }

  viewClinicalInstructionResponse(id: string): void {
    this.router.navigateByUrl(this.clinicalInstructionResponseUrl(id));
  }

  editClinicalInstructionResponse(id: string): MatDialogRef<ClinicalInstructionResponseEditComponent> {
    const dialogRef = this.dialog.open(ClinicalInstructionResponseEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addClinicalInstructionResponse(parent_id: string, patient_id: string, response: MedicationAdminstrationChartDetail | Form_VitalsDetail | ProcedureNoteSummary): MatDialogRef<ClinicalInstructionResponseEditComponent> {
    const dialogRef = this.dialog.open(ClinicalInstructionResponseEditComponent, {
          data: {
            id: parent_id,
            patient_id,
            response,
            mode: TCModalModes.NEW
          },
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickClinicalInstructionResponses(selectOne: boolean=false): MatDialogRef<ClinicalInstructionResponsePickComponent> {
      const dialogRef = this.dialog.open(ClinicalInstructionResponsePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
