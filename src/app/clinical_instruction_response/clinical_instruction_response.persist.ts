import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ClinicalInstructionResponseDashboard, ClinicalInstructionResponseDetail, ClinicalInstructionResponseSummaryPartialList} from "./clinical_instruction_response.model";
import { Form_VitalsDetail } from '../form_encounters/form_encounter.model';
import { MedicationAdminstrationChartDetail } from '../medication_adminstration_chart/medication_adminstration_chart.model';
import { ProcedureNoteSummary } from '../procedures/procedure.model';


@Injectable({
  providedIn: 'root'
})
export class ClinicalInstructionResponsePersist {
 clinicalInstructionResponseSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.clinicalInstructionResponseSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchClinicalInstructionResponse(parent_id:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ClinicalInstructionResponseSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("clinical_instruction/" + parent_id + "/clinical_instruction_response", this.clinicalInstructionResponseSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<ClinicalInstructionResponseSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "clinical_instruction_response/do", new TCDoParam("download_all", this.filters()));
  }

  clinicalInstructionResponseDashboard(): Observable<ClinicalInstructionResponseDashboard> {
    return this.http.get<ClinicalInstructionResponseDashboard>(environment.tcApiBaseUri + "clinical_instruction_response/dashboard");
  }

  getClinicalInstructionResponse(id: string): Observable<ClinicalInstructionResponseDetail> {
    return this.http.get<ClinicalInstructionResponseDetail>(environment.tcApiBaseUri + "clinical_instruction_response/" + id);
  } addClinicalInstructionResponse(data: {item: ClinicalInstructionResponseDetail, response: MedicationAdminstrationChartDetail | Form_VitalsDetail | ProcedureNoteSummary}): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "clinical_instruction_response/", data);
  }

  updateClinicalInstructionResponse(item: ClinicalInstructionResponseDetail): Observable<ClinicalInstructionResponseDetail> {
    return this.http.patch<ClinicalInstructionResponseDetail>(environment.tcApiBaseUri + "clinical_instruction_response/" + item.id, item);
  }

  deleteClinicalInstructionResponse(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "clinical_instruction_response/" + id);
  }
 clinicalInstructionResponsesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "clinical_instruction_response/do", new TCDoParam(method, payload));
  }

  clinicalInstructionResponseDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "clinical_instruction_responses/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "clinical_instruction_response/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "clinical_instruction_response/" + id + "/do", new TCDoParam("print", {}));
  }


}
