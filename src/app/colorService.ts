import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


class ColorSettingSummary {
    app_icon:string;
    fav_icon:string;
    base_color:string;
    secondary_color:string;
    third_color:string;
    fourth_color:string;
    fith_color:string;
    sixth_color:string;
    seventh_color:string;
    eight_color:string;
    ninth_color:string;
    text_color:string;
    title:string;
     
}

@Injectable({
    providedIn: 'root'
  })
  export class ColorService {

    constructor(private http:HttpClient){

    }
    searchProperties(): Observable<ColorSettingSummary> {

     
        return this.http.get<ColorSettingSummary>(environment.tcApiBaseUri + "css");
    
      }



  }