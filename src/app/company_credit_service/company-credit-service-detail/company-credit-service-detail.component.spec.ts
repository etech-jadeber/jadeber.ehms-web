import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServiceDetailComponent } from './company-credit-service-detail.component';

describe('CompanyCreditServiceDetailComponent', () => {
  let component: CompanyCreditServiceDetailComponent;
  let fixture: ComponentFixture<CompanyCreditServiceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServiceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
