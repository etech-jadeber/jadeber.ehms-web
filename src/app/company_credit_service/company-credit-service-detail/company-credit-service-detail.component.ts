import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {CompanyCreditServiceDetail} from "../company_credit_service.model";
import {CompanyCreditServicePersist} from "../company_credit_service.persist";
import {CompanyCreditServiceNavigator} from "../company_credit_service.navigator";

export enum CompanyCreditServiceTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-company_credit_service-detail',
  templateUrl: './company-credit-service-detail.component.html',
  styleUrls: ['./company-credit-service-detail.component.scss']
})
export class CompanyCreditServiceDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  companyCreditServiceIsLoading:boolean = false;
  
  CompanyCreditServiceTabs: typeof CompanyCreditServiceTabs = CompanyCreditServiceTabs;
  activeTab: CompanyCreditServiceTabs = CompanyCreditServiceTabs.overview;
  //basics
  companyCreditServiceDetail: CompanyCreditServiceDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public companyCreditServiceNavigator: CompanyCreditServiceNavigator,
              public  companyCreditServicePersist: CompanyCreditServicePersist) {
    this.tcAuthorization.requireRead("company_credit_services");
    this.companyCreditServiceDetail = new CompanyCreditServiceDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("company_credit_services");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.companyCreditServiceIsLoading = true;
    this.companyCreditServicePersist.getCompanyCreditService(id).subscribe(companyCreditServiceDetail => {
          this.companyCreditServiceDetail = companyCreditServiceDetail;
          this.companyCreditServiceIsLoading = false;
        }, error => {
          console.error(error);
          this.companyCreditServiceIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.companyCreditServiceDetail.id);
  }
  editCompanyCreditService(): void {
    let modalRef = this.companyCreditServiceNavigator.editCompanyCreditService(this.companyCreditServiceDetail.id);
    modalRef.afterClosed().subscribe(companyCreditServiceDetail => {
      TCUtilsAngular.assign(this.companyCreditServiceDetail, companyCreditServiceDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.companyCreditServicePersist.print(this.companyCreditServiceDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print company_credit_service", true);
      });
    }

  back():void{
      this.location.back();
    }

}
