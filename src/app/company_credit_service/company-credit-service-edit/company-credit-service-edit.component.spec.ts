import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServiceEditComponent } from './company-credit-service-edit.component';

describe('CompanyCreditServiceEditComponent', () => {
  let component: CompanyCreditServiceEditComponent;
  let fixture: ComponentFixture<CompanyCreditServiceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServiceEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
