import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CompanyCreditServiceDetail, CompanyCreditServiceSummary } from '../company_credit_service.model';
import { CompanyCreditServicePersist } from '../company_credit_service.persist';
import { FeePersist } from 'src/app/fees/fee.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { MatCheckboxChange } from '@angular/material/checkbox';


@Component({
  selector: 'app-company_credit_service-edit',
  templateUrl: './company-credit-service-edit.component.html',
  styleUrls: ['./company-credit-service-edit.component.scss']
})export class CompanyCreditServiceEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  service_for: number[] = []
  companyCreditServicesData: CompanyCreditServiceSummary[] = [];
  companyCreditServicesDisplayedColumns: string[] = ['action', 'service_for', 'service_type', 'discount_percent', 'service_percent'];
  companyCreditServiceDetail: CompanyCreditServiceDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<CompanyCreditServiceEditComponent>,
              public  persist: CompanyCreditServicePersist,
              public feePersist: FeePersist,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("company_credit_services");
      this.title = this.appTranslation.getText("general","new") +  " " + "company_credit_service";
      this.companyCreditServiceDetail = new CompanyCreditServiceDetail();
      //set necessary defaults
      this.companyCreditServiceDetail.package_id = this.idMode.id;

    } else {
     this.tcAuthorization.requireUpdate("company_credit_services");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "company_credit_service";
      this.isLoadingResults = true;
      this.persist.getCompanyCreditService(this.idMode.id).subscribe(companyCreditServiceDetail => {
        this.companyCreditServiceDetail = companyCreditServiceDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addCompanyCreditService(this.companyCreditServiceDetail).subscribe(value => {
      this.tcNotification.success("companyCreditService added");
      this.companyCreditServiceDetail.id = value.id;
      this.dialogRef.close(this.companyCreditServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  saveList(): void {
    this.isLoadingResults = true;
    const total = this.companyCreditServicesData.length - 1
    this.companyCreditServicesData.forEach((companyCreditService, idx) => {
      this.persist.addCompanyCreditService(companyCreditService).subscribe(value => {
        companyCreditService.id = value.id
        if (total == idx){
          this.tcNotification.success("comapnny credit service is added");
          this.dialogRef.close(this.companyCreditServicesData)
        }
      },error => {
        this.isLoadingResults = false;
      })
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateCompanyCreditService(this.companyCreditServiceDetail).subscribe(value => {
      this.tcNotification.success("company_credit_service updated");
      this.dialogRef.close(this.companyCreditServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  onContinue(): void {
    this.isLoadingResults = true;
    const total = this.companyCreditServicesData.length - 1
    this.companyCreditServicesData.forEach((companyCreditService, idx) => {
      this.persist.addCompanyCreditService(companyCreditService).subscribe(value => {
        companyCreditService.id = value.id
        if (total == idx){
          this.tcNotification.success("comapnny credit service is added");
          this.dialogRef.close({...this.companyCreditServicesData, next: true})
        }
      },error => {
        this.isLoadingResults = false;
      })
    })
  }
  
  canSubmit():boolean{
        if (this.companyCreditServiceDetail == null){
            return false;
          }

if (this.companyCreditServiceDetail.package_id == null || this.companyCreditServiceDetail.package_id  == "") {
            return false;
        } 
        if (this.service_for == null) {
          return false;
      }
      if (this.companyCreditServiceDetail.credit_service_for == null) {
        return false;
    }
if (this.companyCreditServiceDetail.service_percent == null || this.companyCreditServiceDetail.service_percent < 0 || this.companyCreditServiceDetail.service_percent > 100) {
          return false;
      }
      if (this.companyCreditServiceDetail.discount_percent == null || this.companyCreditServiceDetail.discount_percent < 0 || this.companyCreditServiceDetail.discount_percent > 100) {
        return false;
    }
 return true;

 }

 handleChange(change: MatCheckboxChange){
  if (change.checked){
    // this.service_for = this.feePersist.fee_type.map(val => val.id)
  } else {
    this.service_for = []
  }
 }

 isAllSelected(){
  // return this.feePersist.fee_type.every((val) => this.service_for.some((v) => v == val.id))
 }

 addToList() {
  if (this.isServiceExists()){
    this.tcNotification.error("The service is already registed")
    return;
  }
  this.companyCreditServicesData = [...this.service_for.map((value) => ({...this.companyCreditServiceDetail, service_type: value})), ...this.companyCreditServicesData];
  this.companyCreditServiceDetail = new CompanyCreditServiceDetail();
  this.companyCreditServiceDetail.package_id = this.idMode.id
}

isServiceExists() {
  return this.companyCreditServicesData.find((value) => value.service_type == this.companyCreditServiceDetail.service_type)
}

removeCompanyCreditServiceFromList(companyCreditService: CompanyCreditServiceDetail) {

  this.companyCreditServicesData = this.companyCreditServicesData.filter(it => it.service_type != companyCreditService.service_type);
}
 }
