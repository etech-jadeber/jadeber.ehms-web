import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServiceListComponent } from './company-credit-service-list.component';

describe('CompanyCreditServiceListComponent', () => {
  let component: CompanyCreditServiceListComponent;
  let fixture: ComponentFixture<CompanyCreditServiceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServiceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
