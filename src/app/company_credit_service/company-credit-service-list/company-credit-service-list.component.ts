import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CompanyCreditServiceSummary, CompanyCreditServiceSummaryPartialList } from '../company_credit_service.model';
import { CompanyCreditServicePersist } from '../company_credit_service.persist';
import { CompanyCreditServiceNavigator } from '../company_credit_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FeePersist } from 'src/app/fees/fee.persist';
@Component({
  selector: 'app-company_credit_service-list',
  templateUrl: './company-credit-service-list.component.html',
  styleUrls: ['./company-credit-service-list.component.scss']
})
export class CompanyCreditServiceListComponent implements OnInit {
  companyCreditServicesData: CompanyCreditServiceSummary[] = [];
  companyCreditServicesTotalCount: number = 0;
  companyCreditServiceSelectAll:boolean = false;
  companyCreditServiceSelection: CompanyCreditServiceSummary[] = [];

 companyCreditServicesDisplayedColumns: string[] = ["select","action", "credit_service_for", "service_type", "discount_percent","service_percent" ];
  companyCreditServiceSearchTextBox: FormControl = new FormControl();
  companyCreditServiceIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) companyCreditServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) companyCreditServicesSort: MatSort;
  @Input() packageId: string;
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public companyCreditServicePersist: CompanyCreditServicePersist,
                public companyCreditServiceNavigator: CompanyCreditServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public feePersist: FeePersist

    ) {

        this.tcAuthorization.requireRead("company_credit_services");
       this.companyCreditServiceSearchTextBox.setValue(companyCreditServicePersist.companyCreditServiceSearchText);
      //delay subsequent keyup events
      this.companyCreditServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.companyCreditServicePersist.companyCreditServiceSearchText = value;
        this.searchCompanyCreditServices();
      });
    }
     ngOnInit() {
      this.companyCreditServicePersist.packageId = this.packageId
      this.companyCreditServicesSort.sortChange.subscribe(() => {
        this.companyCreditServicesPaginator.pageIndex = 0;
        this.searchCompanyCreditServices(true);
      });

      this.companyCreditServicesPaginator.page.subscribe(() => {
        this.searchCompanyCreditServices(true);
      });
      //start by loading items
      this.searchCompanyCreditServices();
    }

  searchCompanyCreditServices(isPagination:boolean = false): void {


    let paginator = this.companyCreditServicesPaginator;
    let sorter = this.companyCreditServicesSort;

    this.companyCreditServiceIsLoading = true;
    this.companyCreditServiceSelection = [];

    this.companyCreditServicePersist.searchCompanyCreditService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CompanyCreditServiceSummaryPartialList) => {
      this.companyCreditServicesData = partialList.data;
      if (partialList.total != -1) {
        this.companyCreditServicesTotalCount = partialList.total;
      }
      this.companyCreditServiceIsLoading = false;
    }, error => {
      this.companyCreditServiceIsLoading = false;
    });

  } downloadCompanyCreditServices(): void {
    if(this.companyCreditServiceSelectAll){
         this.companyCreditServicePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download company_credit_service", true);
      });
    }
    else{
        this.companyCreditServicePersist.download(this.tcUtilsArray.idsList(this.companyCreditServiceSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download company_credit_service",true);
            });
        }
  }
addCompanyCreditService(): void {
    let dialogRef = this.companyCreditServiceNavigator.addCompanyCreditService(this.packageId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCompanyCreditServices();
      }
    });
  }

  editCompanyCreditService(item: CompanyCreditServiceSummary) {
    let dialogRef = this.companyCreditServiceNavigator.editCompanyCreditService(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCompanyCreditService(item: CompanyCreditServiceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("company_credit_service");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.companyCreditServicePersist.deleteCompanyCreditService(item.id).subscribe(response => {
          this.tcNotification.success("company_credit_service deleted");
          this.searchCompanyCreditServices();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
