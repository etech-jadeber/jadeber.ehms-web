import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServicePickComponent } from './company-credit-service-pick.component';

describe('CompanyCreditServicePickComponent', () => {
  let component: CompanyCreditServicePickComponent;
  let fixture: ComponentFixture<CompanyCreditServicePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServicePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServicePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
