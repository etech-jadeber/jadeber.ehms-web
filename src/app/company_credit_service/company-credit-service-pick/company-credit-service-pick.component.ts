import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CompanyCreditServiceSummary, CompanyCreditServiceSummaryPartialList } from '../company_credit_service.model';
import { CompanyCreditServicePersist } from '../company_credit_service.persist';
import { CompanyCreditServiceNavigator } from '../company_credit_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';


@Component({
  selector: 'app-company_credit_service-pick',
  templateUrl: './company-credit-service-pick.component.html',
  styleUrls: ['./company-credit-service-pick.component.scss']
})
export class CompanyCreditServicePickComponent implements OnInit {
  companyCreditServicesData: CompanyCreditServiceSummary[] = [];
  companyCreditServicesTotalCount: number = 0;
  companyCreditServiceSelectAll:boolean = false;
  companyCreditServiceSelection: CompanyCreditServiceSummary[] = [];

 companyCreditServicesDisplayedColumns: string[] = ["select", ,"company_id","service_type","service_percent" ];
  companyCreditServiceSearchTextBox: FormControl = new FormControl();
  companyCreditServiceIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) companyCreditServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) companyCreditServicesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public companyCreditServicePersist: CompanyCreditServicePersist,
                public companyCreditServiceNavigator: CompanyCreditServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<CompanyCreditServicePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("company_credit_services");
       this.companyCreditServiceSearchTextBox.setValue(companyCreditServicePersist.companyCreditServiceSearchText);
      //delay subsequent keyup events
      this.companyCreditServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.companyCreditServicePersist.companyCreditServiceSearchText = value;
        this.searchCompany_credit_services();
      });
    } ngOnInit() {
   
      this.companyCreditServicesSort.sortChange.subscribe(() => {
        this.companyCreditServicesPaginator.pageIndex = 0;
        this.searchCompany_credit_services(true);
      });

      this.companyCreditServicesPaginator.page.subscribe(() => {
        this.searchCompany_credit_services(true);
      });
      //start by loading items
      this.searchCompany_credit_services();
    }

  searchCompany_credit_services(isPagination:boolean = false): void {


    let paginator = this.companyCreditServicesPaginator;
    let sorter = this.companyCreditServicesSort;

    this.companyCreditServiceIsLoading = true;
    this.companyCreditServiceSelection = [];

    this.companyCreditServicePersist.searchCompanyCreditService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CompanyCreditServiceSummaryPartialList) => {
      this.companyCreditServicesData = partialList.data;
      if (partialList.total != -1) {
        this.companyCreditServicesTotalCount = partialList.total;
      }
      this.companyCreditServiceIsLoading = false;
    }, error => {
      this.companyCreditServiceIsLoading = false;
    });

  }
  markOneItem(item: CompanyCreditServiceSummary) {
    if(!this.tcUtilsArray.containsId(this.companyCreditServiceSelection,item.id)){
          this.companyCreditServiceSelection = [];
          this.companyCreditServiceSelection.push(item);
        }
        else{
          this.companyCreditServiceSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.companyCreditServiceSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
