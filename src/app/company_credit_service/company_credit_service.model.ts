import {TCId} from "../tc/models";

export class CompanyCreditServiceSummary extends TCId {
    company_id:string;
    service_type:number;
    discount_percent: number;
    service_percent:number;
    credit_service_for: number;
    package_id:string;
     
    }
    
export class CompanyCreditServiceSummaryPartialList {
      data: CompanyCreditServiceSummary[];
      total: number;
    }
    
export class CompanyCreditServiceDetail extends CompanyCreditServiceSummary {
    }
    
export class CompanyCreditServiceDashboard {
      total: number;
    }
    