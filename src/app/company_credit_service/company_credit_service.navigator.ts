import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CompanyCreditServiceEditComponent} from "./company-credit-service-edit/company-credit-service-edit.component";
import {CompanyCreditServicePickComponent} from "./company-credit-service-pick/company-credit-service-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CompanyCreditServiceNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  companyCreditServicesUrl(): string {
    return "/company_credit_services";
  }

  companyCreditServiceUrl(id: string): string {
    return "/company_credit_services/" + id;
  }

  viewCompanyCreditServices(): void {
    this.router.navigateByUrl(this.companyCreditServicesUrl());
  }

  viewCompanyCreditService(id: string): void {
    this.router.navigateByUrl(this.companyCreditServiceUrl(id));
  }

  editCompanyCreditService(id: string): MatDialogRef<CompanyCreditServiceEditComponent> {
    const dialogRef = this.dialog.open(CompanyCreditServiceEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCompanyCreditService(packageId: string): MatDialogRef<CompanyCreditServiceEditComponent> {
    const dialogRef = this.dialog.open(CompanyCreditServiceEditComponent, {
          data: new TCIdMode(packageId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickCompanyCreditServices(selectOne: boolean=false): MatDialogRef<CompanyCreditServicePickComponent> {
      const dialogRef = this.dialog.open(CompanyCreditServicePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
