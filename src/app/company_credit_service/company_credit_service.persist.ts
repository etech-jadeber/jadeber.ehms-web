import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {CompanyCreditServiceDashboard, CompanyCreditServiceDetail, CompanyCreditServiceSummaryPartialList} from "./company_credit_service.model";
import { TCUtilsString } from '../tc/utils-string';
import { CreditServiceFor } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class CompanyCreditServicePersist {
  companyId: string;
  packageId: string;
  creditServiceForFilter: number;
  CreditServiceFor: TCEnum[] = [
    new TCEnum( CreditServiceFor.all, 'All'),
 new TCEnum( CreditServiceFor.employee, 'Employee'),
 new TCEnum( CreditServiceFor.family, 'Family'),

 ];
 companyCreditServiceSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.companyCreditServiceSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchCompanyCreditService(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CompanyCreditServiceSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("company_credit_service", this.companyCreditServiceSearchText, pageSize, pageIndex, sort, order);
    if (this.companyId){
      url = TCUtilsString.appendUrlParameter(url, "company_id", this.companyId);
    }
    if (this.packageId){
      url = TCUtilsString.appendUrlParameter(url, "package_id", this.packageId);
    }
    if (this.creditServiceForFilter){
      url = TCUtilsString.appendUrlParameter(url, "credit_service_for", this.creditServiceForFilter.toString())
    }
    return this.http.get<CompanyCreditServiceSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service/do", new TCDoParam("download_all", this.filters()));
  }

  companyCreditServiceDashboard(): Observable<CompanyCreditServiceDashboard> {
    return this.http.get<CompanyCreditServiceDashboard>(environment.tcApiBaseUri + "company_credit_service/dashboard");
  }

  getCompanyCreditService(id: string): Observable<CompanyCreditServiceDetail> {
    return this.http.get<CompanyCreditServiceDetail>(environment.tcApiBaseUri + "company_credit_service/" + id);
  } addCompanyCreditService(item: CompanyCreditServiceDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service/", item);
  }

  updateCompanyCreditService(item: CompanyCreditServiceDetail): Observable<CompanyCreditServiceDetail> {
    return this.http.patch<CompanyCreditServiceDetail>(environment.tcApiBaseUri + "company_credit_service/" + item.id, item);
  }

  deleteCompanyCreditService(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "company_credit_service/" + id);
  }
 companyCreditServicesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "company_credit_service/do", new TCDoParam(method, payload));
  }

  companyCreditServiceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "company_credit_services/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service/" + id + "/do", new TCDoParam("print", {}));
  }


}
