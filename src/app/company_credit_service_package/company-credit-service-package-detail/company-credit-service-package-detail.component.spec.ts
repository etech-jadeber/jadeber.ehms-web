import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServicePackageDetailComponent } from './company-credit-service-package-detail.component';

describe('CompanyCreditServicePackageDetailComponent', () => {
  let component: CompanyCreditServicePackageDetailComponent;
  let fixture: ComponentFixture<CompanyCreditServicePackageDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServicePackageDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServicePackageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
