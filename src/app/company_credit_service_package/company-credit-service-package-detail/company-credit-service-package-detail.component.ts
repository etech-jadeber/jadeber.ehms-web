import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {CompanyCreditServicePackageDetail} from "../companyCreditServicePackage.model";
import {CompanyCreditServicePackagePersist} from "../companyCreditServicePackage.persist";
import {CompanyCreditServicePackageNavigator} from "../companyCreditServicePackage.navigator";

export enum CompanyCreditServicePackageTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-company_credit_service_package-detail',
  templateUrl: './company-credit-service-package-detail.component.html',
  styleUrls: ['./company-credit-service-package-detail.component.scss']
})
export class CompanyCreditServicePackageDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  companyCreditServicePackageIsLoading:boolean = false;
  
  CompanyCreditServicePackageTabs: typeof CompanyCreditServicePackageTabs = CompanyCreditServicePackageTabs;
  activeTab: CompanyCreditServicePackageTabs = CompanyCreditServicePackageTabs.overview;
  //basics
  companyCreditServicePackageDetail: CompanyCreditServicePackageDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public companyCreditServicePackageNavigator: CompanyCreditServicePackageNavigator,
              public  companyCreditServicePackagePersist: CompanyCreditServicePackagePersist) {
    this.tcAuthorization.requireRead("company_credit_service_packages");
    this.companyCreditServicePackageDetail = new CompanyCreditServicePackageDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("company_credit_service_packages");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.companyCreditServicePackageIsLoading = true;
    this.companyCreditServicePackagePersist.getCompanyCreditServicePackage(id).subscribe(companyCreditServicePackageDetail => {
          this.companyCreditServicePackageDetail = companyCreditServicePackageDetail;
          this.companyCreditServicePackageIsLoading = false;
        }, error => {
          console.error(error);
          this.companyCreditServicePackageIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.companyCreditServicePackageDetail.id);
  }
  editCompanyCreditServicePackage(): void {
    let modalRef = this.companyCreditServicePackageNavigator.editCompanyCreditServicePackage(this.companyCreditServicePackageDetail.id);
    modalRef.afterClosed().subscribe(companyCreditServicePackageDetail => {
      TCUtilsAngular.assign(this.companyCreditServicePackageDetail, companyCreditServicePackageDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.companyCreditServicePackagePersist.print(this.companyCreditServicePackageDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print company_credit_service_package", true);
      });
    }

  back():void{
      this.location.back();
    }

}