import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServicePackageEditComponent } from './company-credit-service-package-edit.component';

describe('CompanyCreditServicePackageEditComponent', () => {
  let component: CompanyCreditServicePackageEditComponent;
  let fixture: ComponentFixture<CompanyCreditServicePackageEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServicePackageEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServicePackageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
