import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CompanyCreditServicePackageDetail } from '../companyCreditServicePackage.model';
import { CompanyCreditServicePackagePersist } from '../companyCreditServicePackage.persist';

@Component({
  selector: 'app-company_credit_service_package-edit',
  templateUrl: './company-credit-service-package-edit.component.html',
  styleUrls: ['./company-credit-service-package-edit.component.scss']
})export class CompanyCreditServicePackageEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  companyCreditServicePackageDetail: CompanyCreditServicePackageDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<CompanyCreditServicePackageEditComponent>,
              public  persist: CompanyCreditServicePackagePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("company_credit_service_packages");
      this.title = this.appTranslation.getText("general","new") +  " " + "company_credit_service_package";
      this.companyCreditServicePackageDetail = new CompanyCreditServicePackageDetail();
      this.companyCreditServicePackageDetail.company_id = this.idMode.id;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("company_credit_service_packages");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "company_credit_service_package";
      this.isLoadingResults = true;
      this.persist.getCompanyCreditServicePackage(this.idMode.id).subscribe(companyCreditServicePackageDetail => {
        this.companyCreditServicePackageDetail = companyCreditServicePackageDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addCompanyCreditServicePackage(this.companyCreditServicePackageDetail).subscribe(value => {
      this.tcNotification.success("companyCreditServicePackage added");
      this.companyCreditServicePackageDetail.id = value.id;
      this.dialogRef.close(this.companyCreditServicePackageDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onContinue(): void {
    this.isLoadingResults = true;
    this.persist.addCompanyCreditServicePackage(this.companyCreditServicePackageDetail).subscribe(value => {
    this.companyCreditServicePackageDetail.id = value.id
    this.isLoadingResults = false;
        this.dialogRef.close({...this.companyCreditServicePackageDetail, next: true});
    }, error => {
      this.isLoadingResults = false;
      console.log(error)
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateCompanyCreditServicePackage(this.companyCreditServicePackageDetail).subscribe(value => {
      this.tcNotification.success("company_credit_service_package updated");
      this.dialogRef.close(this.companyCreditServicePackageDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.companyCreditServicePackageDetail == null){
            return false;
          }
          if (!this.tcUtilsString.isValidId(this.companyCreditServicePackageDetail.company_id)){
            return false;
          }
          if (!this.companyCreditServicePackageDetail.name){
            return false;
          }
          return true
 }
 }