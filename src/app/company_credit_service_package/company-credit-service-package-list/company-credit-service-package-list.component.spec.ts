import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServicePackageListComponent } from './company-credit-service-package-list.component';

describe('CompanyCreditServicePackageListComponent', () => {
  let component: CompanyCreditServicePackageListComponent;
  let fixture: ComponentFixture<CompanyCreditServicePackageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServicePackageListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServicePackageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
