import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CompanyCreditServicePackageSummary, CompanyCreditServicePackageSummaryPartialList } from '../companyCreditServicePackage.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { CompanyCreditServicePackageNavigator } from '../companyCreditServicePackage.navigator';
import { CompanyCreditServicePackagePersist } from '../companyCreditServicePackage.persist';
@Component({
  selector: 'app-company_credit_service_package-list',
  templateUrl: './company-credit-service-package-list.component.html',
  styleUrls: ['./company-credit-service-package-list.component.scss']
})export class CompanyCreditServicePackageListComponent implements OnInit {
  companyCreditServicePackagesData: CompanyCreditServicePackageSummary[] = [];
  companyCreditServicePackagesTotalCount: number = 0;
  companyCreditServicePackageSelectAll:boolean = false;
  companyCreditServicePackagesSelection: CompanyCreditServicePackageSummary[] = [];

 companyCreditServicePackagesDisplayedColumns: string[] = ["select","action","name" ,"covered_amount"];
  companyCreditServicePackageSearchTextBox: FormControl = new FormControl();
  companyCreditServicePackageIsLoading: boolean = false;  
  
  @Input() companyId: string;
  @ViewChild(MatPaginator, {static: true}) companyCreditServicePackagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) companyCreditServicePackagesSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public companyCreditServicePackagePersist: CompanyCreditServicePackagePersist,
                public companyCreditServicePackageNavigator: CompanyCreditServicePackageNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("company_credit_service_packages");
       this.companyCreditServicePackageSearchTextBox.setValue(companyCreditServicePackagePersist.companyCreditServicePackageSearchText);
      //delay subsequent keyup events
      this.companyCreditServicePackageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.companyCreditServicePackagePersist.companyCreditServicePackageSearchText = value;
        this.searchCompany_credit_service_packages();
      });
    } ngOnInit() {
      this.companyCreditServicePackagePersist.companyId = this.companyId;
      this.companyCreditServicePackagesSort.sortChange.subscribe(() => {
        this.companyCreditServicePackagesPaginator.pageIndex = 0;
        this.searchCompany_credit_service_packages(true);
      });

      this.companyCreditServicePackagesPaginator.page.subscribe(() => {
        this.searchCompany_credit_service_packages(true);
      });
      //start by loading items
      this.searchCompany_credit_service_packages();
    }

  searchCompany_credit_service_packages(isPagination:boolean = false): void {


    let paginator = this.companyCreditServicePackagesPaginator;
    let sorter = this.companyCreditServicePackagesSort;

    this.companyCreditServicePackageIsLoading = true;
    this.companyCreditServicePackagesSelection = [];

    this.companyCreditServicePackagePersist.searchCompanyCreditServicePackage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CompanyCreditServicePackageSummaryPartialList) => {
      this.companyCreditServicePackagesData = partialList.data;
      if (partialList.total != -1) {
        this.companyCreditServicePackagesTotalCount = partialList.total;
      }
      this.companyCreditServicePackageIsLoading = false;
    }, error => {
      this.companyCreditServicePackageIsLoading = false;
    });

  }
  
  downloadCompanyCreditServicePackages(): void {
    if(this.companyCreditServicePackageSelectAll){
         this.companyCreditServicePackagePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download company_credit_service_package", true);
      });
    }
    else{
        this.companyCreditServicePackagePersist.download(this.tcUtilsArray.idsList(this.companyCreditServicePackagesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download company_credit_service_package",true);
            });
        }
  }
addCompany_credit_service_package(): void {
    let dialogRef = this.companyCreditServicePackageNavigator.addCompanyCreditServicePackage(this.companyId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCompany_credit_service_packages();
      }
    });
  }

  editCompanyCreditServicePackage(item: CompanyCreditServicePackageSummary) {
    let dialogRef = this.companyCreditServicePackageNavigator.editCompanyCreditServicePackage(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCompanyCreditServicePackage(item: CompanyCreditServicePackageSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("company_credit_service_package");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.companyCreditServicePackagePersist.deleteCompanyCreditServicePackage(item.id).subscribe(response => {
          this.tcNotification.success("company_credit_service_package deleted");
          this.searchCompany_credit_service_packages();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}