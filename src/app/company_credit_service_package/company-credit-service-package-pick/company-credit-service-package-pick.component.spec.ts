import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreditServicePackagePickComponent } from './company-credit-service-package-pick.component';

describe('CompanyCreditServicePackagePickComponent', () => {
  let component: CompanyCreditServicePackagePickComponent;
  let fixture: ComponentFixture<CompanyCreditServicePackagePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyCreditServicePackagePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreditServicePackagePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
