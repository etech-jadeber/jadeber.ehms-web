import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CompanyCreditServicePackageSummary, CompanyCreditServicePackageSummaryPartialList } from '../companyCreditServicePackage.model';
import { CompanyCreditServicePackagePersist } from '../companyCreditServicePackage.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-company_credit_service_package-pick',
  templateUrl: './company-credit-service-package-pick.component.html',
  styleUrls: ['./company-credit-service-package-pick.component.scss']
})export class CompanyCreditServicePackagePickComponent implements OnInit {
  companyCreditServicePackagesData: CompanyCreditServicePackageSummary[] = [];
  companyCreditServicePackagesTotalCount: number = 0;
  companyCreditServicePackageSelectAll:boolean = false;
  companyCreditServicePackageSelection: CompanyCreditServicePackageSummary[] = [];

 companyCreditServicePackagesDisplayedColumns: string[] = ["select","action","name" ];
  companyCreditServicePackageSearchTextBox: FormControl = new FormControl();
  companyCreditServicePackageIsLoading: boolean = false;
  @ViewChild(MatPaginator, {static: true}) companyCreditServicePackagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) companyCreditServicePackagesSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public companyCreditServicePackagePersist: CompanyCreditServicePackagePersist,
                public dialogRef: MatDialogRef<CompanyCreditServicePackagePickComponent>,
 @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, company_id: string}
    ) {
        this.companyCreditServicePackagePersist.companyId = this.data.company_id
        this.tcAuthorization.requireRead("company_credit_service_packages");
       this.companyCreditServicePackageSearchTextBox.setValue(companyCreditServicePackagePersist.companyCreditServicePackageSearchText);
      //delay subsequent keyup events
      this.companyCreditServicePackageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.companyCreditServicePackagePersist.companyCreditServicePackageSearchText = value;
        this.searchCompany_credit_service_packages();
      });
    } ngOnInit() {

      this.companyCreditServicePackagesSort.sortChange.subscribe(() => {
        this.companyCreditServicePackagesPaginator.pageIndex = 0;
        this.searchCompany_credit_service_packages(true);
      });

      this.companyCreditServicePackagesPaginator.page.subscribe(() => {
        this.searchCompany_credit_service_packages(true);
      });
      //start by loading items
      this.searchCompany_credit_service_packages();
    }

  searchCompany_credit_service_packages(isPagination:boolean = false): void {


    let paginator = this.companyCreditServicePackagesPaginator;
    let sorter = this.companyCreditServicePackagesSort;

    this.companyCreditServicePackageIsLoading = true;
    this.companyCreditServicePackageSelection = [];

    this.companyCreditServicePackagePersist.searchCompanyCreditServicePackage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CompanyCreditServicePackageSummaryPartialList) => {
      this.companyCreditServicePackagesData = partialList.data;
      if (partialList.total != -1) {
        this.companyCreditServicePackagesTotalCount = partialList.total;
      }
      this.companyCreditServicePackageIsLoading = false;
    }, error => {
      this.companyCreditServicePackageIsLoading = false;
    });

  }
  markOneItem(item: CompanyCreditServicePackageSummary) {
    if(!this.tcUtilsArray.containsId(this.companyCreditServicePackageSelection,item.id)){
          this.companyCreditServicePackageSelection = [];
          this.companyCreditServicePackageSelection.push(item);
        }
        else{
          this.companyCreditServicePackageSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.companyCreditServicePackageSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
