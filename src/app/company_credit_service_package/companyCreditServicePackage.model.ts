import {TCId} from "../tc/models";export class CompanyCreditServicePackageSummary extends TCId {
    company_id:string;
    name:string;
    covered_amount: number;
     
    }
    export class CompanyCreditServicePackageSummaryPartialList {
      data: CompanyCreditServicePackageSummary[];
      total: number;
    }
    export class CompanyCreditServicePackageDetail extends CompanyCreditServicePackageSummary {
    }
    
    export class CompanyCreditServicePackageDashboard {
      total: number;
    }