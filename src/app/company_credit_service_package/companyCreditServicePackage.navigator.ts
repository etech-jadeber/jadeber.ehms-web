import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CompanyCreditServicePackageEditComponent} from "./company-credit-service-package-edit/company-credit-service-package-edit.component";
import {CompanyCreditServicePackagePickComponent} from "./company-credit-service-package-pick/company-credit-service-package-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CompanyCreditServicePackageNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  company_credit_service_packagesUrl(): string {
    return "/company_credit_service_packages";
  }

  company_credit_service_packageUrl(id: string): string {
    return "/company_credit_service_packages/" + id;
  }

  viewCompanyCreditServicePackages(): void {
    this.router.navigateByUrl(this.company_credit_service_packagesUrl());
  }

  viewCompanyCreditServicePackage(id: string): void {
    this.router.navigateByUrl(this.company_credit_service_packageUrl(id));
  }

  editCompanyCreditServicePackage(id: string): MatDialogRef<CompanyCreditServicePackageEditComponent> {
    const dialogRef = this.dialog.open(CompanyCreditServicePackageEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCompanyCreditServicePackage(companyId: string): MatDialogRef<CompanyCreditServicePackageEditComponent> {
    const dialogRef = this.dialog.open(CompanyCreditServicePackageEditComponent, {
          data: new TCIdMode(companyId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickCompanyCreditServicePackages(company_id: string, selectOne: boolean=false): MatDialogRef<CompanyCreditServicePackagePickComponent> {
      const dialogRef = this.dialog.open(CompanyCreditServicePackagePickComponent, {
        data: {company_id, selectOne},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
