import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {CompanyCreditServicePackageDashboard, CompanyCreditServicePackageDetail, CompanyCreditServicePackageSummaryPartialList} from "./companyCreditServicePackage.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class CompanyCreditServicePackagePersist {
  companyId: string;
 companyCreditServicePackageSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service_package/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.companyCreditServicePackageSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchCompanyCreditServicePackage(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CompanyCreditServicePackageSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("company_credit_service_package", this.companyCreditServicePackageSearchText, pageSize, pageIndex, sort, order);
    if(this.companyId){
      url = TCUtilsString.appendUrlParameter(url, "company_id", this.companyId)
    }
    return this.http.get<CompanyCreditServicePackageSummaryPartialList>(url);

  } 
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service_package/do", new TCDoParam("download_all", this.filters()));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service_package/do", new TCDoParam("download", ids));
}

  companyCreditServicePackageDashboard(): Observable<CompanyCreditServicePackageDashboard> {
    return this.http.get<CompanyCreditServicePackageDashboard>(environment.tcApiBaseUri + "company_credit_service_package/dashboard");
  }

  addCompanyCreditServicePackage(item: CompanyCreditServicePackageDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company_credit_service_package/", item);
  }

  updateCompanyCreditServicePackage(item: CompanyCreditServicePackageDetail): Observable<CompanyCreditServicePackageDetail> {
    return this.http.patch<CompanyCreditServicePackageDetail>(environment.tcApiBaseUri + "company_credit_service_package/" + item.id, item);
  }

  getCompanyCreditServicePackage(id: string): Observable<CompanyCreditServicePackageDetail> {
    return this.http.get<CompanyCreditServicePackageDetail>(environment.tcApiBaseUri + "company_credit_service_package/" + id);
  }

  deleteCompanyCreditServicePackage(id: string): Observable<CompanyCreditServicePackageDetail> {
    return this.http.delete<CompanyCreditServicePackageDetail>(environment.tcApiBaseUri + "company_credit_service_package/" + id);
  }
 }