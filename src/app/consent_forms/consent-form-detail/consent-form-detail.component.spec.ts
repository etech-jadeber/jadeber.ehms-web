import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConsentFormDetailComponent } from './consent-form-detail.component';

describe('ConsentFormDetailComponent', () => {
  let component: ConsentFormDetailComponent;
  let fixture: ComponentFixture<ConsentFormDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentFormDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentFormDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
