import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Consent_FormDetail} from "../consent_form.model";
import {Consent_FormPersist} from "../consent_form.persist";
import {Consent_FormNavigator} from "../consent_form.navigator";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';

export enum Consent_FormTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-consent_form-detail',
  templateUrl: './consent-form-detail.component.html',
  styleUrls: ['./consent-form-detail.component.css']
})
export class Consent_FormDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  consent_formLoading:boolean = false;
  
  Consent_FormTabs: typeof Consent_FormTabs = Consent_FormTabs;
  activeTab: Consent_FormTabs = Consent_FormTabs.overview;
  //basics
  consent_formDetail: Consent_FormDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public consent_formNavigator: Consent_FormNavigator,
              public formEncounterPersist: Form_EncounterPersist,
              public tcUtilsDate: TCUtilsDate,
              public  consent_formPersist: Consent_FormPersist) {
    this.tcAuthorization.requireRead("consent_forms");
    this.consent_formDetail = new Consent_FormDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("consent_forms");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.consent_formLoading = true;
    this.consent_formPersist.getConsent_Form(id).subscribe(consent_formDetail => {
          this.consent_formDetail = consent_formDetail;
          this.consent_formLoading = false;
        }, error => {
          console.error(error);
          this.consent_formLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.consent_formDetail.id);
  }

  editConsent_Form(): void {
    let modalRef = this.consent_formNavigator.editConsent_Form(this.consent_formDetail.id);
    modalRef.afterClosed().subscribe(modifiedConsent_FormDetail => {
      TCUtilsAngular.assign(this.consent_formDetail, modifiedConsent_FormDetail);
    }, error => {
      console.error(error);
    });
  }

   printConsent_Form():void{
      this.consent_formPersist.print(this.consent_formDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print consent_form", true);
      });
    }

  back():void{
      this.location.back();
    }

}
