import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConsentFormEditComponent } from './consent-form-edit.component';

describe('ConsentFormEditComponent', () => {
  let component: ConsentFormEditComponent;
  let fixture: ComponentFixture<ConsentFormEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentFormEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
