import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";


import {Consent_FormDetail} from "../consent_form.model";
import {Consent_FormPersist} from "../consent_form.persist";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { ProcedureDetail } from 'src/app/procedures/procedure.model';


@Component({
  selector: 'app-consent_form-edit',
  templateUrl: './consent-form-edit.component.html',
  styleUrls: ['./consent-form-edit.component.css']
})
export class Consent_FormEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  consent_formDetail: Consent_FormDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              private encounterPersist:Form_EncounterPersist,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Consent_FormEditComponent>,
              public  persist: Consent_FormPersist,
              public filePersist:FilePersist,
              public procedurePersist: ProcedurePersist,
              private http:HttpClient,
              @Inject(MAT_DIALOG_DATA) public idMode: any) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }


  
  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.context['mode'] === TCModalModes.WIZARD;
  }


  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("consent_forms");
      this.title = this.appTranslation.getText("general","new") + " "+this.appTranslation.getText("patient", "consent_form");
      this.consent_formDetail = new Consent_FormDetail();
      this.consent_formDetail.file_id = this.tcUtilsString.invalid_id;
      this.getProcedure( this.idMode.childId);
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("consent_forms");
      this.title = this.appTranslation.getText("general","edit") + " "+this.appTranslation.getText("patient", "consent_form");
      this.isLoadingResults = true;
      this.persist.getConsent_Form(this.idMode.childId).subscribe(consent_formDetail => {
        this.consent_formDetail = consent_formDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addConsent_Form(this.consent_formDetail).subscribe(value => {
      this.tcNotification.success("Consent_Form added");
      this.consent_formDetail.id = value.id;
      this.dialogRef.close(this.consent_formDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateConsent_Form(this.consent_formDetail).subscribe(value => {
      this.tcNotification.success("Consent_Form updated");
      this.dialogRef.close(this.consent_formDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.consent_formDetail == null){
            return false;
          }
          if(this.consent_formDetail.procedure_id == null || this.consent_formDetail.procedure_id == "" ){
            return false;
          }

          if(this.consent_formDetail.file_id == null || this.consent_formDetail.file_id == ""  || 
          this.consent_formDetail.file_id == this.tcUtilsString.invalid_id){
            return false;
          }


          if(this.consent_formDetail.doctor_id == null || this.consent_formDetail.doctor_id == "" ){
            return false;
          }


          if(this.consent_formDetail.patient_id == null || this.consent_formDetail.patient_id == "" ){
            return false;
          }        


          if(this.consent_formDetail.encounter_id == null || this.consent_formDetail.encounter_id == "" ){
            return false;
          }        


        return true;
      }



      searchProcedure(){

      }

      getProcedure(id:string){
        // get procedure with real procedure 
        this.encounterPersist.getPatient_Procedure(id).subscribe(
          (patientProcedure)=>{
            this.consent_formDetail.doctor_id = patientProcedure.doctor_id;
            this.consent_formDetail.patient_id = patientProcedure.patient_id;
            this.consent_formDetail.encounter_id = patientProcedure.encounter_id;
            this.consent_formDetail.procedure_id = patientProcedure.procedure_id;
            
          }
        )

      }



      documentUpload(fileInputEvent: any): void {

        if (fileInputEvent.target.files[0].size > 20000000) {
          this.tcNotification.error("File too big");
          return;
        }
    
        if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
          this.tcNotification.error("Unsupported file type");
          return;
        }
    
        let fd: FormData = new FormData();
        fd.append('file', fileInputEvent.target.files[0]);
        this.http.post(this.persist.documentUploadUri() + this.idMode.childId, fd).subscribe(response => {
          this.consent_formDetail.file_id = (<TCId>response).id;
        });
      }

}
