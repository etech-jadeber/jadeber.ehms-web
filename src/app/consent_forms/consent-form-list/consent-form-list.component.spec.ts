import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConsentFormListComponent } from './consent-form-list.component';

describe('ConsentFormListComponent', () => {
  let component: ConsentFormListComponent;
  let fixture: ComponentFixture<ConsentFormListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentFormListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
