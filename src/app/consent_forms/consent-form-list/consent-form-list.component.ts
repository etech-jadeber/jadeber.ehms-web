import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Consent_FormPersist} from "../consent_form.persist";
import {Consent_FormNavigator} from "../consent_form.navigator";
import {Consent_FormDetail, Consent_FormSummary, Consent_FormSummaryPartialList} from "../consent_form.model";


@Component({
  selector: 'app-consent_form-list',
  templateUrl: './consent-form-list.component.html',
  styleUrls: ['./consent-form-list.component.css']
})
export class Consent_FormListComponent implements OnInit {

  consent_formsData: Consent_FormSummary[] = [];
  consent_formsTotalCount: number = 0;
  consent_formsSelectAll:boolean = false;
  consent_formsSelection: Consent_FormSummary[] = [];

  consent_formsDisplayedColumns: string[] = ["select","action", "patient_id","encounter_id","doctor_id","procedure_id","created_at", ];
  consent_formsSearchTextBox: FormControl = new FormControl();
  consent_formsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) consent_formsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) consent_formsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public consent_formPersist: Consent_FormPersist,
                public consent_formNavigator: Consent_FormNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("consent_forms");
       this.consent_formsSearchTextBox.setValue(consent_formPersist.consent_formSearchText);
      //delay subsequent keyup events
      this.consent_formsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.consent_formPersist.consent_formSearchText = value;
        this.searchConsent_Forms();
      });
    }

    ngOnInit() {

      this.consent_formsSort.sortChange.subscribe(() => {
        this.consent_formsPaginator.pageIndex = 0;
        this.searchConsent_Forms(true);
      });

      this.consent_formsPaginator.page.subscribe(() => {
        this.searchConsent_Forms(true);
      });
      //start by loading items
      this.searchConsent_Forms();
    }

  searchConsent_Forms(isPagination:boolean = false): void {


    let paginator = this.consent_formsPaginator;
    let sorter = this.consent_formsSort;

    this.consent_formsIsLoading = true;
    this.consent_formsSelection = [];

    this.consent_formPersist.searchConsent_Form(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Consent_FormSummaryPartialList) => {
      this.consent_formsData = partialList.data;
      if (partialList.total != -1) {
        this.consent_formsTotalCount = partialList.total;
      }
      this.consent_formsIsLoading = false;
    }, error => {
      this.consent_formsIsLoading = false;
    });

  }

  downloadConsent_Forms(): void {
    if(this.consent_formsSelectAll){
         this.consent_formPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download consent_forms", true);
      });
    }
    else{
        this.consent_formPersist.download(this.tcUtilsArray.idsList(this.consent_formsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download consent_forms",true);
            });
        }
  }



  addConsent_Form(): void {
    let dialogRef = this.consent_formNavigator.addConsent_Form();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchConsent_Forms();
      }
    });
  }

  editConsent_Form(item: Consent_FormSummary) {
    let dialogRef = this.consent_formNavigator.editConsent_Form(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteConsent_Form(item: Consent_FormSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Consent_Form");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.consent_formPersist.deleteConsent_Form(item.id).subscribe(response => {
          this.tcNotification.success("Consent_Form deleted");
          this.searchConsent_Forms();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
