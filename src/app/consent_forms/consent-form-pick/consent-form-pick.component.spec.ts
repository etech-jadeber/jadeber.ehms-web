import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConsentFormPickComponent } from './consent-form-pick.component';

describe('ConsentFormPickComponent', () => {
  let component: ConsentFormPickComponent;
  let fixture: ComponentFixture<ConsentFormPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsentFormPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentFormPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
