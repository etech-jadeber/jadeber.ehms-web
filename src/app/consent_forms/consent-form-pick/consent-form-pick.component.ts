import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Consent_FormDetail, Consent_FormSummary, Consent_FormSummaryPartialList} from "../consent_form.model";
import {Consent_FormPersist} from "../consent_form.persist";


@Component({
  selector: 'app-consent_form-pick',
  templateUrl: './consent-form-pick.component.html',
  styleUrls: ['./consent-form-pick.component.css']
})
export class Consent_FormPickComponent implements OnInit {

  consent_formsData: Consent_FormSummary[] = [];
  consent_formsTotalCount: number = 0;
  consent_formsSelection: Consent_FormSummary[] = [];
  consent_formsDisplayedColumns: string[] = ["select", 'patient_id','encounter_id','doctor_id','file_id','procedure_id','created_at' ];

  consent_formsSearchTextBox: FormControl = new FormControl();
  consent_formsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) consent_formsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) consent_formsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public consent_formPersist: Consent_FormPersist,
              public dialogRef: MatDialogRef<Consent_FormPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("consent_forms");
    this.consent_formsSearchTextBox.setValue(consent_formPersist.consent_formSearchText);
    //delay subsequent keyup events
    this.consent_formsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.consent_formPersist.consent_formSearchText = value;
      this.searchConsent_Forms();
    });
  }

  ngOnInit() {

    this.consent_formsSort.sortChange.subscribe(() => {
      this.consent_formsPaginator.pageIndex = 0;
      this.searchConsent_Forms();
    });

    this.consent_formsPaginator.page.subscribe(() => {
      this.searchConsent_Forms();
    });

    //set initial picker list to 5
    this.consent_formsPaginator.pageSize = 5;

    //start by loading items
    this.searchConsent_Forms();
  }

  searchConsent_Forms(): void {
    this.consent_formsIsLoading = true;
    this.consent_formsSelection = [];

    this.consent_formPersist.searchConsent_Form(this.consent_formsPaginator.pageSize,
        this.consent_formsPaginator.pageIndex,
        this.consent_formsSort.active,
        this.consent_formsSort.direction).subscribe((partialList: Consent_FormSummaryPartialList) => {
      this.consent_formsData = partialList.data;
      if (partialList.total != -1) {
        this.consent_formsTotalCount = partialList.total;
      }
      this.consent_formsIsLoading = false;
    }, error => {
      this.consent_formsIsLoading = false;
    });

  }

  markOneItem(item: Consent_FormSummary) {
    if(!this.tcUtilsArray.containsId(this.consent_formsSelection,item.id)){
          this.consent_formsSelection = [];
          this.consent_formsSelection.push(item);
        }
        else{
          this.consent_formsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.consent_formsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
