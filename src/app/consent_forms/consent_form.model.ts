import {TCId} from "../tc/models";

export class Consent_FormSummary extends TCId {
  patient_id : string;
encounter_id : string;
doctor_id : string;
file_id : string;
procedure_id : string;
created_at : number;
patient_name:string;
encounter_date:number;
doctor_name:string;
procedure_name:string;
download_key: string;
patient_procedure_id : string;
}

export class Consent_FormSummaryPartialList {
  data: Consent_FormSummary[];
  total: number;
}

export class Consent_FormDetail extends Consent_FormSummary {
  patient_id : string;
encounter_id : string;
doctor_id : string;
file_id : string;
procedure_id : string;
created_at : number;
}

export class Consent_FormDashboard {
  total: number;
}
