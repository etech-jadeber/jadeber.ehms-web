import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import { Consent_FormEditComponent } from "./consent-form-edit/consent-form-edit.component";
import { Consent_FormPickComponent } from "./consent-form-pick/consent-form-pick.component";



@Injectable({
  providedIn: 'root'
})

export class Consent_FormNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  consent_formsUrl(): string {
    return "/consent_forms";
  }

  consent_formUrl(id: string): string {
    return "/consent_forms/" + id;
  }

  viewConsent_Forms(): void {
    this.router.navigateByUrl(this.consent_formsUrl());
  }

  viewConsent_Form(id: string): void {
    this.router.navigateByUrl(this.consent_formUrl(id));
  }

  editConsent_Form(id: string,parentId:string = null, isWizard:boolean = false, isNew:boolean = false): MatDialogRef<unknown,any> {

    let params: TcDictionary<string> = new TcDictionary();
    params['mode'] = isWizard
      ? TCModalModes.WIZARD
      : isNew
        ? TCModalModes.NEW
        : TCModalModes.EDIT;


        const dialogRef = this.dialog.open(Consent_FormEditComponent, {
          data: new TCParentChildIds(parentId, id, params),
          width: TCModalWidths.medium,
        });
    return dialogRef;
  }

  addConsent_Form(id:string = null, parentId:string = null ): MatDialogRef<unknown,any> {
    return this.editConsent_Form(id, parentId, false, true);
  }
  
   pickConsent_Forms(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Consent_FormPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
