import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Consent_FormDashboard, Consent_FormDetail, Consent_FormSummaryPartialList} from "./consent_form.model";


@Injectable({
  providedIn: 'root'
})
export class Consent_FormPersist {

  consent_formSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchConsent_Form(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Consent_FormSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("consent_forms", this.consent_formSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Consent_FormSummaryPartialList>(url);

  }


  documentUploadUri() {
    return environment.tcApiBaseUri + 'consent_forms/file-upload/';
  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.consent_formSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "consent_forms/do", new TCDoParam("download_all", this.filters()));
  }

  consent_formDashboard(): Observable<Consent_FormDashboard> {
    return this.http.get<Consent_FormDashboard>(environment.tcApiBaseUri + "consent_forms/dashboard");
  }

  getConsent_Form(id: string): Observable<Consent_FormDetail> {
    return this.http.get<Consent_FormDetail>(environment.tcApiBaseUri + "consent_forms/" + id);
  }

  addConsent_Form(item: Consent_FormDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "consent_forms/", item);
  }

  updateConsent_Form(item: Consent_FormDetail): Observable<Consent_FormDetail> {
    return this.http.patch<Consent_FormDetail>(environment.tcApiBaseUri + "consent_forms/" + item.id, item);
  }

  deleteConsent_Form(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "consent_forms/" + id);
  }

  consent_formsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "consent_forms/do", new TCDoParam(method, payload));
  }

  consent_formDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "consent_forms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "consent_forms/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "consent_forms/" + id + "/do", new TCDoParam("print", {}));
  }


}
