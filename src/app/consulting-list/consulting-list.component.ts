import { ConsultingSummary, ConsultingSummaryPartialList, Review_Of_SystemSummary, Review_Of_SystemSummaryPartialList } from '../form_encounters/form_encounter.model';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList } from '../form_encounters/form_encounter.model';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCUtilsAngular } from '../tc/utils-angular';
import { DepartmentPersist } from '../departments/department.persist';
import { DepartmentSummary } from '../departments/department.model';
import { DoctorPersist } from '../doctors/doctor.persist';
import { DoctorDetail, DoctorSummary } from '../doctors/doctor.model';
import { Department_SpecialtySummary } from '../department_specialtys/department_specialty.model';
import { Department_SpecialtyPersist } from '../department_specialtys/department_specialty.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-consulting-list',
  templateUrl: './consulting-list.component.html',
  styleUrls: ['./consulting-list.component.css']
})
export class ConsultingListComponent implements OnInit {

  //consulting
consultingsData: ConsultingSummary[] = [];
consultingsTotalCount: number = 0;
consultingSelectAll:boolean = false;
consultingIsLoading: boolean = false;
consultingSelection: ConsultingSummary[] = [];
consultingsDisplayedColumns: string[] = ["select","action" ,"link","consulting_dept_id","consulting_doctor_id","urgent","patient_convinience","brief_history","date" ];
consultingSearchTextBox: FormControl = new FormControl();

departments: DepartmentSummary[] = [];
departmentsSpeciality: Department_SpecialtySummary[] = [];
doctors: DoctorSummary[] = [];

@Input() encounterId: string;
@Input() patientId: string;
@Output() onResult = new EventEmitter<number>();

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public departmentPersist: DepartmentPersist, 
    public department_specialtyPersist: Department_SpecialtyPersist, 
    public doctorPersist: DoctorPersist,
  ) { 

  }

  ngOnInit(): void {
    // consultings sorter
    this.form_encounterPersist.consultingPatientId = this.patientId
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchConsultings(true);
    });
    // consultings paginator
    this.paginator.page.subscribe(() => {
      this.searchConsultings(true);
    });
    this.searchConsultings();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.form_encounterPersist.consultigEncounterId = this.encounterId;
    } else {
    this.form_encounterPersist.consultigEncounterId = null;
    }
    this.searchConsultings()
    }


  searchConsultings(isPagination:boolean = false): void {
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.consultingIsLoading = true;
    this.consultingSelection = [];
    this.form_encounterPersist.searchConsulting(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ConsultingSummaryPartialList) => {
      this.consultingsData = partialList.data;
      if (partialList.total != -1) {
        this.consultingsTotalCount = partialList.total;
        this.consultingsData.forEach((consulting)=>{
          this.department_specialtyPersist.getDepartment_Specialty(consulting.consulting_dept_id).subscribe((result)=>{
            if(result){
              this.departmentsSpeciality.push(result);
            }
          })
          this.doctorPersist.getDoctor(consulting.consulting_doctor_id).subscribe((result)=>{
            if(result){
              this.doctors.push(result)
            }
          })
        })
      }
      this.consultingIsLoading = false;
    }, error => {
      this.consultingIsLoading = false;
    });

  } 


  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(
      this.doctors,
      doctorId
    );

    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`;
    }
    return '';
  }

  addConsulting(): void {
    let dialogRef = this.form_encounterNavigator.addConsulting(this.encounterId);
    dialogRef.afterClosed().subscribe(newConsulting => {
      if (newConsulting) {
        this.searchConsultings();
      }
    });
  }
  
  editConsulting(item: ConsultingSummary) {
    let dialogRef = this.form_encounterNavigator.editConsulting(this.encounterId, item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchConsultings();
      }
  
    });
  }
  
  deleteConsulting(item: ConsultingSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("consulting");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.form_encounterPersist.deleteConsulting(item.id).subscribe(response => {
          this.tcNotification.success("consulting deleted");
          this.searchConsultings();
        }, error => {
        });
      }
  
    });
  }

  downloadConsultings(){

  }

  back(): void {
    this.location.back();
  } 

}
