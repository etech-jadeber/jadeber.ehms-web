import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorelatedFeeDetailComponent } from './corelated-fee-detail.component';

describe('CorelatedFeeDetailComponent', () => {
  let component: CorelatedFeeDetailComponent;
  let fixture: ComponentFixture<CorelatedFeeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorelatedFeeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorelatedFeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
