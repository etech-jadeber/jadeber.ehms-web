import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { CorelatedFeeDetail } from '../corelated_fee.model';
import { CorelatedFeeNavigator } from '../corelated_fee.navigator';
import { CorelatedFeePersist } from '../corelated_fee.persist';


export enum CorelatedFeeTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-corelated-fee-detail',
  templateUrl: './corelated-fee-detail.component.html',
  styleUrls: ['./corelated-fee-detail.component.scss']
})
export class CorelatedFeeDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  corelatedFeeIsLoading:boolean = false;
  
  CorelatedFeeTabs: typeof CorelatedFeeTabs = CorelatedFeeTabs;
  activeTab: CorelatedFeeTabs = CorelatedFeeTabs.overview;
  //basics
  corelatedFeeDetail: CorelatedFeeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public corelatedFeeNavigator: CorelatedFeeNavigator,
              public  corelatedFeePersist: CorelatedFeePersist) {
    this.tcAuthorization.requireRead("corelated_fees");
    this.corelatedFeeDetail = new CorelatedFeeDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("corelated_fees");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.corelatedFeeIsLoading = true;
    this.corelatedFeePersist.getCorelatedFee(id).subscribe(corelatedFeeDetail => {
          this.corelatedFeeDetail = corelatedFeeDetail;
          this.corelatedFeeIsLoading = false;
        }, error => {
          console.error(error);
          this.corelatedFeeIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.corelatedFeeDetail.id);
  }
  editCorelatedFee(): void {
    let modalRef = this.corelatedFeeNavigator.editCorelatedFee(this.corelatedFeeDetail.id);
    modalRef.afterClosed().subscribe(corelatedFeeDetail => {
      TCUtilsAngular.assign(this.corelatedFeeDetail, corelatedFeeDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.corelatedFeePersist.print(this.corelatedFeeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print corelated_fee", true);
      });
    }

  back():void{
      this.location.back();
    }

}