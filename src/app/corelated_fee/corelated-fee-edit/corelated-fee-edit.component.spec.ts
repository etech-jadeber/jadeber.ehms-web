import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorelatedFeeEditComponent } from './corelated-fee-edit.component';

describe('CorelatedFeeEditComponent', () => {
  let component: CorelatedFeeEditComponent;
  let fixture: ComponentFixture<CorelatedFeeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorelatedFeeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorelatedFeeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
