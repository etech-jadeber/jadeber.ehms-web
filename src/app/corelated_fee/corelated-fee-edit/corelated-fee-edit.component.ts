import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CorelatedFeeDetail } from '../corelated_fee.model';import { CorelatedFeePersist } from '../corelated_fee.persist';import { FeePersist } from 'src/app/fees/fee.persist';
import { FeeNavigator } from 'src/app/fees/fee.navigator';
import { FeeDetail } from 'src/app/fees/fee.model';
import { fee_type } from 'src/app/app.enums';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
@Component({
  selector: 'app-corelated-fee-edit',
  templateUrl: './corelated-fee-edit.component.html',
  styleUrls: ['./corelated-fee-edit.component.scss']
})export class CorelatedFeeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  Name: string;
  TargetName: string;
  fee_type = fee_type;
  corelatedFeeDetail: CorelatedFeeDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<CorelatedFeeEditComponent>,
              public  persist: CorelatedFeePersist,
              public feePersist: FeePersist,
              public tcUtilsDate: TCUtilsDate,
              public feeNavigator: FeeNavigator,
              public wardNavigator: BedroomtypeNavigator,
              public wardPersist: BedroomtypePersist,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("corelated_fees");
      this.title = this.appTranslation.getText("general","new") +  " " + "corelated_fee";
      this.corelatedFeeDetail = new CorelatedFeeDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("corelated_fees");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "corelated_fee";
      this.isLoadingResults = true;
      this.persist.getCorelatedFee(this.idMode.id).subscribe(corelatedFeeDetail => {
        this.corelatedFeeDetail = corelatedFeeDetail;
        this.feePersist.getFee(this.corelatedFeeDetail.fee_id).subscribe((fee) => {
          this.Name = fee.name
        })
        if (corelatedFeeDetail.fee_type == fee_type.bed){
          this.wardPersist.getBedroomtype(this.corelatedFeeDetail.target_id).subscribe((target) => {
            this.TargetName = target.name
          })
        }
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addCorelatedFee(this.corelatedFeeDetail).subscribe(value => {
      this.tcNotification.success("corelatedFee added");
      this.corelatedFeeDetail.id = value.id;
      this.dialogRef.close(this.corelatedFeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  searchFee() {
    let dialogRef = this.feeNavigator.pickFees(true);
    dialogRef.afterClosed().subscribe((result: FeeDetail[]) => {
      if (result) {
        this.Name = result[0].name ;
        this.corelatedFeeDetail.fee_id = result[0].id;
      }
    });
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateCorelatedFee(this.corelatedFeeDetail).subscribe(value => {
      this.tcNotification.success("corelated_fee updated");
      this.dialogRef.close(this.corelatedFeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.corelatedFeeDetail == null){
            return false;
          }
          if (!this.corelatedFeeDetail.fee_type){
            return false;
          }
          if (this.persist.feeTargetType[this.corelatedFeeDetail.fee_type] && !this.corelatedFeeDetail.sub_fee_type){
            return false;
          }
          if (this.corelatedFeeDetail.fee_type == fee_type.bed && !this.corelatedFeeDetail.target_id){
            return false;
          }
          if (!this.corelatedFeeDetail.fee_id){
            return false;
          }
          return true;

 }

 searchWards() {
  let dialogRef = this.wardNavigator.pickBedroomtypes(true);
  dialogRef.afterClosed().subscribe((result: BedroomtypeDetail[]) => {
    if (result) {
      this.TargetName = result[0].name ;
      this.corelatedFeeDetail.target_id = result[0].id;
    }
  });
}
 }