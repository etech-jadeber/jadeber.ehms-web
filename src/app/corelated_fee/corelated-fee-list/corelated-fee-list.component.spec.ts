import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorelatedFeeListComponent } from './corelated-fee-list.component';

describe('CorelatedFeeListComponent', () => {
  let component: CorelatedFeeListComponent;
  let fixture: ComponentFixture<CorelatedFeeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorelatedFeeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorelatedFeeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
