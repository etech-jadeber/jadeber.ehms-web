import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CorelatedFeeDetail, CorelatedFeeSummary, CorelatedFeeSummaryPartialList } from '../corelated_fee.model';
import { CorelatedFeePersist } from '../corelated_fee.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { CorelatedFeeNavigator } from '../corelated_fee.navigator';
import { FeeDetail } from 'src/app/fees/fee.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { fee_type } from 'src/app/app.enums';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
@Component({
  selector: 'app-corelated-fee-list',
  templateUrl: './corelated-fee-list.component.html',
  styleUrls: ['./corelated-fee-list.component.scss']
})export class CorelatedFeeListComponent implements OnInit {
  corelatedFeesData: CorelatedFeeSummary[] = [];
  corelatedFeesTotalCount: number = 0;
  corelatedFeeSelectAll:boolean = false;
  corelatedFeeSelection: CorelatedFeeSummary[] = [];
  fees: {[id: string]: FeeDetail} = {}
  wards: {[id: string]: BedroomtypeDetail} = {}

 corelatedFeesDisplayedColumns: string[] = ["select","action", "fee_type", "sub_fee_type", 'target_id', "fee_id" ];
  corelatedFeeSearchTextBox: FormControl = new FormControl();
  corelatedFeeIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) corelatedFeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) corelatedFeesSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public corelatedFeePersist: CorelatedFeePersist,
                public corelatedFeeNavigator: CorelatedFeeNavigator,
                public feePersist: FeePersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public wardPersist: BedroomtypePersist,

    ) {

        this.tcAuthorization.requireRead("corelated_fees");
       this.corelatedFeeSearchTextBox.setValue(corelatedFeePersist.corelatedFeeSearchText);
      //delay subsequent keyup events
      this.corelatedFeeSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.corelatedFeePersist.corelatedFeeSearchText = value;
        this.searchCorelated_fees();
      });
    } ngOnInit() {
   
      this.corelatedFeesSort.sortChange.subscribe(() => {
        this.corelatedFeesPaginator.pageIndex = 0;
        this.searchCorelated_fees(true);
      });

      this.corelatedFeesPaginator.page.subscribe(() => {
        this.searchCorelated_fees(true);
      });
      //start by loading items
      this.searchCorelated_fees();
    }

  searchCorelated_fees(isPagination:boolean = false): void {


    let paginator = this.corelatedFeesPaginator;
    let sorter = this.corelatedFeesSort;

    this.corelatedFeeIsLoading = true;
    this.corelatedFeeSelection = [];

    this.corelatedFeePersist.searchCorelatedFee(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CorelatedFeeSummaryPartialList) => {
      this.corelatedFeesData = partialList.data;
      this.corelatedFeesData.forEach(data => {
        if (!this.fees[data.fee_id]) {
          this.fees[data.fee_id] = new FeeDetail()
          this.feePersist.getFee(data.fee_id).subscribe(result => {
            this.fees[data.fee_id] = result
          })
        }
        if (data.fee_type == fee_type.bed && !this.wards[data.target_id]) {
          this.wards[data.target_id] = new BedroomtypeDetail()
          this.wardPersist.getBedroomtype(data.target_id).subscribe(result => {
            this.wards[data.target_id] = result
          })
        }
      })
      if (partialList.total != -1) {
        this.corelatedFeesTotalCount = partialList.total;
      }
      this.corelatedFeeIsLoading = false;
    }, error => {
      this.corelatedFeeIsLoading = false;
    });

  } downloadCorelatedFees(): void {
    if(this.corelatedFeeSelectAll){
         this.corelatedFeePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download corelated_fee", true);
      });
    }
    else{
        this.corelatedFeePersist.download(this.tcUtilsArray.idsList(this.corelatedFeeSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download corelated_fee",true);
            });
        }
  }
addCorelated_fee(): void {
    let dialogRef = this.corelatedFeeNavigator.addCorelatedFee();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCorelated_fees();
      }
    });
  }

  editCorelatedFee(item: CorelatedFeeSummary) {
    let dialogRef = this.corelatedFeeNavigator.editCorelatedFee(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCorelatedFee(item: CorelatedFeeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("corelated_fees");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.corelatedFeePersist.deleteCorelatedFee(item.id).subscribe(response => {
          this.tcNotification.success("corelated_fee deleted");
          this.searchCorelated_fees();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getFeeName(id: string){
      if (this.fees[id]){
        return this.fees[id].name
      }
      return ""
    }

    getTargetName(detail: CorelatedFeeDetail){
      if (detail.fee_type == fee_type.bed && this.wards[detail.target_id]){
        return this.wards[detail.target_id]?.name
      }
      return ""
    }
}