import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CorelatedFeePickComponent } from './corelated-fee-pick.component';

describe('CorelatedFeePickComponent', () => {
  let component: CorelatedFeePickComponent;
  let fixture: ComponentFixture<CorelatedFeePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CorelatedFeePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CorelatedFeePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
