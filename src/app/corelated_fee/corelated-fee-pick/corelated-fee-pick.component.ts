import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CorelatedFeeSummary, CorelatedFeeSummaryPartialList } from '../corelated_fee.model';
import { CorelatedFeePersist } from '../corelated_fee.persist';
@Component({
  selector: 'app-corelated_fee-pick',
  templateUrl: './corelated-fee-pick.component.html',
  styleUrls: ['./corelated-fee-pick.component.scss']
})export class CorelatedFeePickComponent implements OnInit {
  corelatedFeesData: CorelatedFeeSummary[] = [];
  corelatedFeesTotalCount: number = 0;
  corelatedFeeSelectAll:boolean = false;
  corelatedFeeSelection: CorelatedFeeSummary[] = [];

 corelatedFeesDisplayedColumns: string[] = ["select","action", 'fee_type', 'fee_sub_type',"fee_id" ];
  corelatedFeeSearchTextBox: FormControl = new FormControl();
  corelatedFeeIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) corelatedFeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) corelatedFeesSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public corelatedFeePersist: CorelatedFeePersist,
                public dialogRef: MatDialogRef<CorelatedFeePickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("corelated_fees");
       this.corelatedFeeSearchTextBox.setValue(corelatedFeePersist.corelatedFeeSearchText);
      //delay subsequent keyup events
      this.corelatedFeeSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.corelatedFeePersist.corelatedFeeSearchText = value;
        this.searchCorelated_fees();
      });
    } ngOnInit() {
   
      this.corelatedFeesSort.sortChange.subscribe(() => {
        this.corelatedFeesPaginator.pageIndex = 0;
        this.searchCorelated_fees(true);
      });

      this.corelatedFeesPaginator.page.subscribe(() => {
        this.searchCorelated_fees(true);
      });
      //start by loading items
      this.searchCorelated_fees();
    }

  searchCorelated_fees(isPagination:boolean = false): void {


    let paginator = this.corelatedFeesPaginator;
    let sorter = this.corelatedFeesSort;

    this.corelatedFeeIsLoading = true;
    this.corelatedFeeSelection = [];

    this.corelatedFeePersist.searchCorelatedFee(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CorelatedFeeSummaryPartialList) => {
      this.corelatedFeesData = partialList.data;
      if (partialList.total != -1) {
        this.corelatedFeesTotalCount = partialList.total;
      }
      this.corelatedFeeIsLoading = false;
    }, error => {
      this.corelatedFeeIsLoading = false;
    });

  }
  markOneItem(item: CorelatedFeeSummary) {
    if(!this.tcUtilsArray.containsId(this.corelatedFeeSelection,item.id)){
          this.corelatedFeeSelection = [];
          this.corelatedFeeSelection.push(item);
        }
        else{
          this.corelatedFeeSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.corelatedFeeSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }