import {TCId} from "../tc/models";

export class CorelatedFeeSummary extends TCId {
    fee_type:number;
    sub_fee_type:number;
    fee_id:string;
    target_id: string;
     
    }
    export class CorelatedFeeSummaryPartialList {
      data: CorelatedFeeSummary[];
      total: number;
    }
    export class CorelatedFeeDetail extends CorelatedFeeSummary {
    }
    
    export class CorelatedFeeDashboard {
      total: number;
    }