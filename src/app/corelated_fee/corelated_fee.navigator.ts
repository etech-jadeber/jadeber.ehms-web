import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CorelatedFeeEditComponent} from "./corelated-fee-edit/corelated-fee-edit.component";
import {CorelatedFeePickComponent} from "./corelated-fee-pick/corelated-fee-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CorelatedFeeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  corelated_feesUrl(): string {
    return "/corelated_fees";
  }

  corelated_feeUrl(id: string): string {
    return "/corelated_fees/" + id;
  }

  viewCorelatedFees(): void {
    this.router.navigateByUrl(this.corelated_feesUrl());
  }

  viewCorelatedFee(id: string): void {
    this.router.navigateByUrl(this.corelated_feeUrl(id));
  }

  editCorelatedFee(id: string): MatDialogRef<CorelatedFeeEditComponent> {
    const dialogRef = this.dialog.open(CorelatedFeeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCorelatedFee(): MatDialogRef<CorelatedFeeEditComponent> {
    const dialogRef = this.dialog.open(CorelatedFeeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickCorelatedFees(selectOne: boolean=false): MatDialogRef<CorelatedFeePickComponent> {
      const dialogRef = this.dialog.open(CorelatedFeePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}