import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCEnum, TCEnumTranslation, TCId, TcDictionary} from "../tc/models";
import {CorelatedFeeDashboard, CorelatedFeeDetail, CorelatedFeeSummaryPartialList} from "./corelated_fee.model";
import { fee_type, operation_room_type } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class CorelatedFeePersist {
  fee_type_filter: number;
  sub_type_filter: number;

 corelatedFeeSearchText: string = "";constructor(private http: HttpClient, public appTranslation:AppTranslation,
) {
  }

  fee_type: TCEnumTranslation[] = [
    new TCEnumTranslation(fee_type.bed, this.appTranslation.getKey('bed', 'bed')),
    new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure'))
  ];
  
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "corelated_fee/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.corelatedFeeSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchCorelatedFee(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CorelatedFeeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("corelated_fee", this.corelatedFeeSearchText, pageSize, pageIndex, sort, order);
    if (this.fee_type_filter){
      url = TCUtilsString.appendUrlParameter(url, "fee_type", this.fee_type_filter.toString());
    }
    if (this.sub_type_filter){
      url = TCUtilsString.appendUrlParameter(url, "sub_fee_type", this.sub_type_filter.toString());
    }
    return this.http.get<CorelatedFeeSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "corelated_fee/do", new TCDoParam("download_all", this.filters()));
  }

  corelatedFeeDashboard(): Observable<CorelatedFeeDashboard> {
    return this.http.get<CorelatedFeeDashboard>(environment.tcApiBaseUri + "corelated_fee/dashboard");
  }

  getCorelatedFee(id: string): Observable<CorelatedFeeDetail> {
    return this.http.get<CorelatedFeeDetail>(environment.tcApiBaseUri + "corelated_fee/" + id);
  }
  addCorelatedFee(item: CorelatedFeeDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'corelated_fee/',
      item
    );
  }

  updateCorelatedFee(item: CorelatedFeeDetail): Observable<CorelatedFeeDetail> {
    return this.http.patch<CorelatedFeeDetail>(
      environment.tcApiBaseUri + 'corelated_fee/' + item.id,
      item
    );
  }

  deleteCorelatedFee(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'corelated_fee/' + id);
  }
  corelatedFeeDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'corelated_fee/do',
      new TCDoParam(method, payload)
    );
  }

  corelatedFeesDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'corelated_fee/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'holiday/do',
      new TCDoParam('download', ids)
    );
  }

  OperationRoomType: TCEnum[] = [
    new TCEnum( operation_room_type.all, 'All'),
  new TCEnum( operation_room_type.minor, 'Minor'),
  new TCEnum( operation_room_type.major, 'Major'),
  ]

  feeTargetType = {
    [fee_type.procedure]: this.OperationRoomType
  }
 }