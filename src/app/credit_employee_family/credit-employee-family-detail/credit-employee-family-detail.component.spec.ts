import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditEmployeeFamilyDetailComponent } from './credit-employee-family-detail.component';

describe('CreditEmployeeFamilyDetailComponent', () => {
  let component: CreditEmployeeFamilyDetailComponent;
  let fixture: ComponentFixture<CreditEmployeeFamilyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditEmployeeFamilyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditEmployeeFamilyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
