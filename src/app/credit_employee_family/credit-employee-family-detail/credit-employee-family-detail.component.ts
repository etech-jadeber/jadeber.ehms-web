import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {CreditEmployeeFamilyDetail} from "../credit_employee_family.model";
import {CreditEmployeeFamilyPersist} from "../credit_employee_family.persist";
import {CreditEmployeeFamilyNavigator} from "../credit_employee_family.navigator";

export enum CreditEmployeeFamilyTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-credit_employee_family-detail',
  templateUrl: './credit-employee-family-detail.component.html',
  styleUrls: ['./credit-employee-family-detail.component.scss']
})
export class CreditEmployeeFamilyDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  creditEmployeeFamilyIsLoading:boolean = false;
  
  CreditEmployeeFamilyTabs: typeof CreditEmployeeFamilyTabs = CreditEmployeeFamilyTabs;
  activeTab: CreditEmployeeFamilyTabs = CreditEmployeeFamilyTabs.overview;
  //basics
  creditEmployeeFamilyDetail: CreditEmployeeFamilyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public creditEmployeeFamilyNavigator: CreditEmployeeFamilyNavigator,
              public  creditEmployeeFamilyPersist: CreditEmployeeFamilyPersist) {
    this.tcAuthorization.requireRead("credit_employee_familys");
    this.creditEmployeeFamilyDetail = new CreditEmployeeFamilyDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("credit_employee_familys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.creditEmployeeFamilyIsLoading = true;
    this.creditEmployeeFamilyPersist.getCreditEmployeeFamily(id).subscribe(creditEmployeeFamilyDetail => {
          this.creditEmployeeFamilyDetail = creditEmployeeFamilyDetail;
          this.creditEmployeeFamilyIsLoading = false;
        }, error => {
          console.error(error);
          this.creditEmployeeFamilyIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.creditEmployeeFamilyDetail.id);
  }
  editCreditEmployeeFamily(): void {
    let modalRef = this.creditEmployeeFamilyNavigator.editCreditEmployeeFamily(this.creditEmployeeFamilyDetail.id);
    modalRef.afterClosed().subscribe(creditEmployeeFamilyDetail => {
      TCUtilsAngular.assign(this.creditEmployeeFamilyDetail, creditEmployeeFamilyDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.creditEmployeeFamilyPersist.print(this.creditEmployeeFamilyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print credit_employee_family", true);
      });
    }

  back():void{
      this.location.back();
    }

}