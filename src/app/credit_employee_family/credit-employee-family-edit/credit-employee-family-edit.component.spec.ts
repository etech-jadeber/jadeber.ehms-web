import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditEmployeeFamilyEditComponent } from './credit-employee-family-edit.component';

describe('CreditEmployeeFamilyEditComponent', () => {
  let component: CreditEmployeeFamilyEditComponent;
  let fixture: ComponentFixture<CreditEmployeeFamilyEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditEmployeeFamilyEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditEmployeeFamilyEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
