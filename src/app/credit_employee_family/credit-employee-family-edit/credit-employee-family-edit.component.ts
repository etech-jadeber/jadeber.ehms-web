import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CreditEmployeeFamilyDetail } from '../credit_employee_family.model';import { CreditEmployeeFamilyPersist } from '../credit_employee_family.persist';import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientDetail } from 'src/app/patients/patients.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { WebcamImage } from 'ngx-webcam';
import { CameraNavigator } from 'src/app/patients/camera.navigator';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-credit_employee_family-edit',
  templateUrl: './credit-employee-family-edit.component.html',
  styleUrls: ['./credit-employee-family-edit.component.scss']
})export class CreditEmployeeFamilyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patient_name: string;
  creditEmployeeFamilyDetail: CreditEmployeeFamilyDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<CreditEmployeeFamilyEditComponent>,
              public  persist: CreditEmployeeFamilyPersist,
              public patienNavigator: PatientNavigator,
              public tcUtilsDate: TCUtilsDate,
              public filePersist: FilePersist,
              public cameraNav: CameraNavigator,
              public http: HttpClient,
              @Inject(MAT_DIALOG_DATA) public idMode: {id: string, company_id: string, employee_id: string, mode: string}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("credit_employee_family");
      this.title = this.appTranslation.getText("general","new") +  " " + "credit_employee_family";
      this.creditEmployeeFamilyDetail = new CreditEmployeeFamilyDetail();
      //set necessary defaults
      this.creditEmployeeFamilyDetail.company_id = this.idMode.company_id;
      this.creditEmployeeFamilyDetail.employee_id = this.idMode.employee_id;

    } else {
     this.tcAuthorization.requireUpdate("credit_employee_family");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "credit_employee_family";
      this.isLoadingResults = true;
      this.persist.getCreditEmployeeFamily(this.idMode.id).subscribe(creditEmployeeFamilyDetail => {
        this.creditEmployeeFamilyDetail = creditEmployeeFamilyDetail;
        this.creditEmployeeFamilyDetail.patient_id != this.tcUtilsString.invalid_id ? this.patient_name = this.creditEmployeeFamilyDetail.family_name : null
        this.creditEmployeeFamilyDetail.image_id != this.tcUtilsString.invalid_id && this.filePersist.getDownloadKey(this.creditEmployeeFamilyDetail.image_id).subscribe(
          (data: any) => {
            this.creditEmployeeFamilyDetail.image_url = this.filePersist.downloadLink(data.id)
          }
        )
        this.creditEmployeeFamilyDetail.documnet_id != this.tcUtilsString.invalid_id && this.filePersist.getDownloadKey(this.creditEmployeeFamilyDetail.documnet_id).subscribe(
          (data: any) => {
            this.creditEmployeeFamilyDetail.document_url = this.filePersist.downloadLink(data.id)
          }
        )
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addCreditEmployeeFamily(this.creditEmployeeFamilyDetail).subscribe(value => {
      this.tcNotification.success("creditEmployeeFamily added");
      this.creditEmployeeFamilyDetail.id = value.id;
      this.dialogRef.close(this.creditEmployeeFamilyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  searchPatient(){
    let dialogRef = this.patienNavigator.pickPatients(true)
    dialogRef.afterClosed().subscribe((result: PatientDetail[]) => {
      if(result){
        this.patient_name = result[0].fname + " " + result[0].mname + " " + result[0].lname
        this.creditEmployeeFamilyDetail.family_name = this.patient_name
      this.creditEmployeeFamilyDetail.patient_id = result[0].id;
      }
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateCreditEmployeeFamily(this.creditEmployeeFamilyDetail).subscribe(value => {
      this.tcNotification.success("credit_employee_family updated");
      this.dialogRef.close(this.creditEmployeeFamilyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.creditEmployeeFamilyDetail == null){
            return false;
          }

if (this.creditEmployeeFamilyDetail.company_id == null || this.creditEmployeeFamilyDetail.company_id  == "") {
            return false;
        } 
if (this.creditEmployeeFamilyDetail.employee_id == null || this.creditEmployeeFamilyDetail.employee_id  == "") {
            return false;
        } 
if (this.creditEmployeeFamilyDetail.family_name == null || this.creditEmployeeFamilyDetail.family_name  == "") {
            return false;
        }
if (this.creditEmployeeFamilyDetail.relationship == null) {
            return false;
        }
if (this.creditEmployeeFamilyDetail.covered_percent == null) {
            return false;
        }
if (this.creditEmployeeFamilyDetail.discount_percent == null) {
            return false;
        }
        return true;
 }

 takePic(key: string, pic: string){
  this.cameraNav.takePic().afterClosed().subscribe(
    {
      next:(res:WebcamImage)=>{

       let fd: FormData = new FormData();
       const rawData = atob(res.imageAsBase64);
       const bytes = new Array(rawData.length);
       for (var x = 0; x < rawData.length; x++) {
         bytes[x] = rawData.charCodeAt(x);
        }
        const arr = new Uint8Array(bytes);
        const blob = new Blob([arr], {type: 'image/png'});


       fd.append('file', blob);

       this.http.post(this.persist.documentUploadUri(), fd).subscribe({
        next: (response) => {
          this.creditEmployeeFamilyDetail[key] = (<TCId>response).id;
         this.filePersist.getDownloadKey(this.creditEmployeeFamilyDetail[key]).subscribe(
            (res:any)=>{
              this.creditEmployeeFamilyDetail[pic] = this.filePersist.downloadLink(res.id);
            }
          );
          
          
        },
  
        error:(err)=>{
          console.log(err);
          
        }
      });
        
      },
      error:(err)=>{
        
      }
    }
  )
}



documentUpload(fileInputEvent: any, key: string, pic: string): void {

  if (fileInputEvent.target.files[0].size > 20000000) {
    this.tcNotification.error("File too big");
    return;
  }

  if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
    this.tcNotification.error("Unsupported file type");
    return;
  }

  let fd: FormData = new FormData();
  fd.append('file', fileInputEvent.target.files[0]);
  this.http.post(this.persist.documentUploadUri(), fd).subscribe({
    next: (response) => {
      this.creditEmployeeFamilyDetail[key] = (<TCId>response).id;
     this.filePersist.getDownloadKey(this.creditEmployeeFamilyDetail[key]).subscribe(
        (res:any)=>{
          
          this.creditEmployeeFamilyDetail[pic] = this.filePersist.downloadLink(res.id);
          
        }
      );
      
      
    },

    error:(err)=>{
      console.log(err);
      
    }
  }
  

  );
}
 }