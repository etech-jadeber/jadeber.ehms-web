import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditEmployeeFamilyListComponent } from './credit-employee-family-list.component';

describe('CreditEmployeeFamilyListComponent', () => {
  let component: CreditEmployeeFamilyListComponent;
  let fixture: ComponentFixture<CreditEmployeeFamilyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditEmployeeFamilyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditEmployeeFamilyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
