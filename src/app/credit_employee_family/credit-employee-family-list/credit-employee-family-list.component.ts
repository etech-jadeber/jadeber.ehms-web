import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CreditEmployeeFamilyDetail, CreditEmployeeFamilySummary, CreditEmployeeFamilySummaryPartialList } from '../credit_employee_family.model';
import { CreditEmployeeFamilyPersist } from '../credit_employee_family.persist';
import { CreditEmployeeFamilyNavigator } from '../credit_employee_family.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { InsuredEmployeeStatus } from 'src/app/app.enums';
@Component({
  selector: 'app-credit_employee_family-list',
  templateUrl: './credit-employee-family-list.component.html',
  styleUrls: ['./credit-employee-family-list.component.scss']
})export class CreditEmployeeFamilyListComponent implements OnInit {
  creditEmployeeFamilysData: CreditEmployeeFamilySummary[] = [];
  creditEmployeeFamilysTotalCount: number = 0;
  creditEmployeeFamilySelectAll:boolean = false;
  creditEmployeeFamilySelection: CreditEmployeeFamilySummary[] = [];

 creditEmployeeFamilysDisplayedColumns: string[] = ["select","action","family_name","image_id","documnet_id","relationship","covered_percent","discount_percent" ];
  creditEmployeeFamilySearchTextBox: FormControl = new FormControl();
  creditEmployeeFamilyIsLoading: boolean = false; 
  insured_employeeStatus = InsuredEmployeeStatus
  @Input() companyId: string
  @Input() employeeId: string
  @ViewChild(MatPaginator, {static: true}) creditEmployeeFamilysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) creditEmployeeFamilysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public creditEmployeeFamilyPersist: CreditEmployeeFamilyPersist,
                public creditEmployeeFamilyNavigator: CreditEmployeeFamilyNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
                public patientNavigator: PatientNavigator,

    ) {

        this.tcAuthorization.requireRead("credit_employee_familys");
       this.creditEmployeeFamilySearchTextBox.setValue(creditEmployeeFamilyPersist.creditEmployeeFamilySearchText);
      //delay subsequent keyup events
      this.creditEmployeeFamilySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.creditEmployeeFamilyPersist.creditEmployeeFamilySearchText = value;
        this.searchCreditEmployeeFamilys();
      });
    } ngOnInit() {
   
      this.creditEmployeeFamilyPersist.companyId = this.companyId
      this.creditEmployeeFamilyPersist.employeeId = this.employeeId
      this.creditEmployeeFamilysSort.sortChange.subscribe(() => {
        this.creditEmployeeFamilysPaginator.pageIndex = 0;
        this.searchCreditEmployeeFamilys(true);
      });

      this.creditEmployeeFamilysPaginator.page.subscribe(() => {
        this.searchCreditEmployeeFamilys(true);
      });
      //start by loading items
      this.searchCreditEmployeeFamilys();
    }

  searchCreditEmployeeFamilys(isPagination:boolean = false): void {


    let paginator = this.creditEmployeeFamilysPaginator;
    let sorter = this.creditEmployeeFamilysSort;

    this.creditEmployeeFamilyIsLoading = true;
    this.creditEmployeeFamilySelection = [];

    this.creditEmployeeFamilyPersist.searchCreditEmployeeFamily(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditEmployeeFamilySummaryPartialList) => {
      this.creditEmployeeFamilysData = partialList.data;
      if (partialList.total != -1) {
        this.creditEmployeeFamilysTotalCount = partialList.total;
      }
      this.creditEmployeeFamilyIsLoading = false;
    }, error => {
      this.creditEmployeeFamilyIsLoading = false;
    });

  } downloadCreditEmployeeFamilys(): void {
    if(this.creditEmployeeFamilySelectAll){
         this.creditEmployeeFamilyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download credit_employee_family", true);
      });
    }
    else{
        this.creditEmployeeFamilyPersist.download(this.tcUtilsArray.idsList(this.creditEmployeeFamilySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download credit_employee_family",true);
            });
        }
  }
addCreditEmployeeFamily(): void {
    let dialogRef = this.creditEmployeeFamilyNavigator.addCreditEmployeeFamily(this.companyId, this.employeeId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCreditEmployeeFamilys();
      }
    });
  }

  viewImage(fileId: string) {
    this.filePersist.getDownloadKey(fileId).subscribe(value => {
      this.tcNavigator.openUrl(this.filePersist.downloadLink(value['id']), true)
    })
  }

  connectPatient(family: CreditEmployeeFamilyDetail) {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.creditEmployeeFamilyIsLoading = true
        this.creditEmployeeFamilyPersist.creditEmployeeFamilyDo(family.id, "connect_patient", {patient_id:result[0].id})
        .subscribe(response => {
          if (response) {
            this.creditEmployeeFamilyIsLoading = false;
            this.tcNotification.success("Insured Employee connected.");
            this.searchCreditEmployeeFamilys();
          }
        }, error => {
          this.creditEmployeeFamilyIsLoading = false;
          console.log(error)
        }
        )
      }
    });
  }

  terminateEmployee(family: CreditEmployeeFamilyDetail){
    this.creditEmployeeFamilyIsLoading = true
    this.creditEmployeeFamilyPersist.creditEmployeeFamilyDo(family.id, "terminate_patient", {}).subscribe(
      response => {
        if(response) {
          this.creditEmployeeFamilyIsLoading = false;
          this.tcNotification.success("The employee is terminated.");
          this.searchCreditEmployeeFamilys();
        }
      }, error => {
        this.creditEmployeeFamilyIsLoading = false;
        console.log(error)
      }
    )
  }

  editCreditEmployeeFamily(item: CreditEmployeeFamilySummary) {
    let dialogRef = this.creditEmployeeFamilyNavigator.editCreditEmployeeFamily(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCreditEmployeeFamily(item: CreditEmployeeFamilySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("credit_employee_family");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.creditEmployeeFamilyPersist.deleteCreditEmployeeFamily(item.id).subscribe(response => {
          this.tcNotification.success("credit_employee_family deleted");
          this.searchCreditEmployeeFamilys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}