import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditEmployeeFamilyPickComponent } from './credit-employee-family-pick.component';

describe('CreditEmployeeFamilyPickComponent', () => {
  let component: CreditEmployeeFamilyPickComponent;
  let fixture: ComponentFixture<CreditEmployeeFamilyPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditEmployeeFamilyPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditEmployeeFamilyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
