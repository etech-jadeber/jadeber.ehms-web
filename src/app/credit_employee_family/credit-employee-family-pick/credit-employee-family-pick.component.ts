import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { CreditEmployeeFamilySummary, CreditEmployeeFamilySummaryPartialList } from '../credit_employee_family.model';
import { CreditEmployeeFamilyPersist } from '../credit_employee_family.persist';
import { CreditEmployeeFamilyNavigator } from '../credit_employee_family.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-credit-employee-family-pick',
  templateUrl: './credit-employee-family-pick.component.html',
  styleUrls: ['./credit-employee-family-pick.component.scss']
})
export class CreditEmployeeFamilyPickComponent implements OnInit {

  creditEmployeeFamilysData: CreditEmployeeFamilySummary[] = [];
  creditEmployeeFamilysTotalCount: number = 0;
  creditEmployeeFamilySelectAll:boolean = false;
  creditEmployeeFamilySelection: CreditEmployeeFamilySummary[] = [];

  creditEmployeeFamilysDisplayedColumns: string[] = ["select", ,"company_id","service_type","service_percent" ];
  creditEmployeeFamilySearchTextBox: FormControl = new FormControl();
  creditEmployeeFamilyIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) creditEmployeeFamilyPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) creditEmployeeFamilySort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public creditEmployeeFamilyPersist: CreditEmployeeFamilyPersist,
                public companyCreditServiceNavigator: CreditEmployeeFamilyNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<CreditEmployeeFamilyPersist>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("company_credit_services");
       this.creditEmployeeFamilySearchTextBox.setValue(creditEmployeeFamilyPersist.creditEmployeeFamilySearchText);
      //delay subsequent keyup events
      this.creditEmployeeFamilySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.creditEmployeeFamilyPersist.creditEmployeeFamilySearchText = value;
        this.searchCompany_credit_services();
      });
    } ngOnInit() {
   
      this.creditEmployeeFamilySort.sortChange.subscribe(() => {
        this.creditEmployeeFamilyPaginator.pageIndex = 0;
        this.searchCompany_credit_services(true);
      });

      this.creditEmployeeFamilyPaginator.page.subscribe(() => {
        this.searchCompany_credit_services(true);
      });
      //start by loading items
      this.searchCompany_credit_services();
    }

  searchCompany_credit_services(isPagination:boolean = false): void {


    let paginator = this.creditEmployeeFamilyPaginator;
    let sorter = this.creditEmployeeFamilySort;

    this.creditEmployeeFamilyIsLoading = true;
    this.creditEmployeeFamilySelection = [];

    this.creditEmployeeFamilyPersist.searchCreditEmployeeFamily(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditEmployeeFamilySummaryPartialList) => {
      this.creditEmployeeFamilysData = partialList.data;
      if (partialList.total != -1) {
        this.creditEmployeeFamilysTotalCount = partialList.total;
      }
      this.creditEmployeeFamilyIsLoading = false;
    }, error => {
      this.creditEmployeeFamilyIsLoading = false;
    });

  }
  markOneItem(item: CreditEmployeeFamilySummary) {
    if(!this.tcUtilsArray.containsId(this.creditEmployeeFamilySelection,item.id)){
          this.creditEmployeeFamilySelection = [];
          this.creditEmployeeFamilySelection.push(item);
        }
        else{
          this.creditEmployeeFamilySelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.creditEmployeeFamilySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
