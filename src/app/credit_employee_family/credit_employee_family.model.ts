import {TCId} from "../tc/models";

export class CreditEmployeeFamilySummary extends TCId {
    company_id:string;
    employee_id:string;
    patient_id:string;
    family_name:string;
    image_id:string;
    documnet_id:string;
    relationship:number;
    covered_percent:number;
    discount_percent:number;
    image_url:string;
    document_url: string
     
    }
    export class CreditEmployeeFamilySummaryPartialList {
      data: CreditEmployeeFamilySummary[];
      total: number;
    }
    export class CreditEmployeeFamilyDetail extends CreditEmployeeFamilySummary {
    }
    
    export class CreditEmployeeFamilyDashboard {
      total: number;
    }