import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CreditEmployeeFamilyEditComponent} from "./credit-employee-family-edit/credit-employee-family-edit.component";
import {CreditEmployeeFamilyPickComponent} from "./credit-employee-family-pick/credit-employee-family-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CreditEmployeeFamilyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  creditEmployeeFamilysUrl(): string {
    return "/credit_employee_familys";
  }

  creditEmployeeFamilyUrl(id: string): string {
    return "/credit_employee_familys/" + id;
  }

  viewCreditEmployeeFamilys(): void {
    this.router.navigateByUrl(this.creditEmployeeFamilysUrl());
  }

  viewCreditEmployeeFamily(id: string): void {
    this.router.navigateByUrl(this.creditEmployeeFamilyUrl(id));
  }

  editCreditEmployeeFamily(id: string): MatDialogRef<CreditEmployeeFamilyEditComponent> {
    const dialogRef = this.dialog.open(CreditEmployeeFamilyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCreditEmployeeFamily(company_id: string, employee_id: string): MatDialogRef<CreditEmployeeFamilyEditComponent> {
    const dialogRef = this.dialog.open(CreditEmployeeFamilyEditComponent, {
          data: {company_id, employee_id, mode: TCModalModes.NEW},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickCreditEmployeeFamilys(selectOne: boolean=false): MatDialogRef<CreditEmployeeFamilyPickComponent> {
      const dialogRef = this.dialog.open(CreditEmployeeFamilyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}