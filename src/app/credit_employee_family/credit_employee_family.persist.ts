import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {CreditEmployeeFamilyDashboard, CreditEmployeeFamilyDetail, CreditEmployeeFamilySummaryPartialList} from "./credit_employee_family.model";
import { CreditFamilyRelationship } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class CreditEmployeeFamilyPersist {
  CreditFamilyRelationshipFilter: number  = -1;
  companyId: string;
  employeeId: string;

    CreditFamilyRelationship: TCEnum[] = [
     new TCEnum( CreditFamilyRelationship.partner, 'Partner'),
  new TCEnum( CreditFamilyRelationship.child, 'Child'),

  ];
  
 creditEmployeeFamilySearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.creditEmployeeFamilySearchText;
    //add custom filters
    return fltrs;
  }

  documentUploadUri() {
    return environment.tcApiBaseUri + 'patients/upload-image/';
  }
 
  searchCreditEmployeeFamily(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CreditEmployeeFamilySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("credit_employee_family", this.creditEmployeeFamilySearchText, pageSize, pageIndex, sort, order);
    if (this.companyId){
      url = TCUtilsString.appendUrlParameter(url, "company_id", this.companyId)
    }
    if (this.employeeId){
      url = TCUtilsString.appendUrlParameter(url, "employee_id", this.employeeId)
    }
    return this.http.get<CreditEmployeeFamilySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_employee_family/do", new TCDoParam("download_all", this.filters()));
  }

  creditEmployeeFamilyDashboard(): Observable<CreditEmployeeFamilyDashboard> {
    return this.http.get<CreditEmployeeFamilyDashboard>(environment.tcApiBaseUri + "credit_employee_family/dashboard");
  }

  getCreditEmployeeFamily(id: string): Observable<CreditEmployeeFamilyDetail> {
    return this.http.get<CreditEmployeeFamilyDetail>(environment.tcApiBaseUri + "credit_employee_family/" + id);
  } addCreditEmployeeFamily(item: CreditEmployeeFamilyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_employee_family/", item);
  }

  updateCreditEmployeeFamily(item: CreditEmployeeFamilyDetail): Observable<CreditEmployeeFamilyDetail> {
    return this.http.patch<CreditEmployeeFamilyDetail>(environment.tcApiBaseUri + "credit_employee_family/" + item.id, item);
  }

  deleteCreditEmployeeFamily(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "credit_employee_family/" + id);
  }
 creditEmployeeFamilysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "credit_employee_family/do", new TCDoParam(method, payload));
  }

  creditEmployeeFamilyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "credit_employee_family/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "credit_employee_family/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "credit_employee_family/" + id + "/do", new TCDoParam("print", {}));
  }


}