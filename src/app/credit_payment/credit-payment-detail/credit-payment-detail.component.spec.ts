import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentDetailComponent } from './credit-payment-detail.component';

describe('CreditPaymentDetailComponent', () => {
  let component: CreditPaymentDetailComponent;
  let fixture: ComponentFixture<CreditPaymentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
