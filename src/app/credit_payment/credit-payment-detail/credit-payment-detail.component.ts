import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {CreditPaymentDetail} from "../credit_payment.model";
import {CreditPaymentPersist} from "../credit_payment.persist";
import {CreditPaymentNavigator} from "../credit_payment.navigator";

export enum CreditPaymentTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-credit-payment-detail',
  templateUrl: './credit-payment-detail.component.html',
  styleUrls: ['./credit-payment-detail.component.scss']
})
export class CreditPaymentDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  creditPaymentIsLoading:boolean = false;

  CreditPaymentTabs: typeof CreditPaymentTabs = CreditPaymentTabs;
  activeTab: CreditPaymentTabs = CreditPaymentTabs.overview;
  //basics
  creditPaymentDetail: CreditPaymentDetail;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public creditPaymentNavigator: CreditPaymentNavigator,
              public  creditPaymentPersist: CreditPaymentPersist) {
    this.tcAuthorization.requireRead("credit_payments");
    this.creditPaymentDetail = new CreditPaymentDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("credit_payments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.creditPaymentIsLoading = true;
    this.creditPaymentPersist.getCreditPayment(id).subscribe(creditPaymentDetail => {
      this.creditPaymentDetail = creditPaymentDetail;
      this.creditPaymentIsLoading = false;
    }, error => {
      console.error(error);
      this.creditPaymentIsLoading = false;
    });
  }

  reload(){
    this.loadDetails(this.creditPaymentDetail.id);
  }
  editCreditPayment(): void {
    let modalRef = this.creditPaymentNavigator.editCreditPayment(this.creditPaymentDetail.id);
    modalRef.afterClosed().subscribe(creditPaymentDetail => {
      TCUtilsAngular.assign(this.creditPaymentDetail, creditPaymentDetail);
    }, error => {
      console.error(error);
    });
  }

  printBirth():void{
    this.creditPaymentPersist.print(this.creditPaymentDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print credit_payment", true);
    });
  }

  back():void{
    this.location.back();
  }

}
