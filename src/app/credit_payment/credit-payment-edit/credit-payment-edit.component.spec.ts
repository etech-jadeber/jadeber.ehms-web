import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentEditComponent } from './credit-payment-edit.component';

describe('CreditPaymentEditComponent', () => {
  let component: CreditPaymentEditComponent;
  let fixture: ComponentFixture<CreditPaymentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
