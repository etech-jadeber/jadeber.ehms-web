import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CreditPaymentDetail } from '../credit_payment.model';import { CreditPaymentPersist } from '../credit_payment.persist';
import {deposit_patient_type} from "../../app.enums";
import {PatientSummary} from "../../patients/patients.model";
import {PatientNavigator} from "../../patients/patients.navigator";

@Component({
  selector: 'app-credit-payment-edit',
  templateUrl: './credit-payment-edit.component.html',
  styleUrls: ['./credit-payment-edit.component.scss']
})export class CreditPaymentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientFullName = '';
  creditPaymentDetail: CreditPaymentDetail;constructor(public tcAuthorization:TCAuthorization,
                                                       public tcNotification: TCNotification,
                                                       public tcUtilsString:TCUtilsString,
                                                       public appTranslation:AppTranslation,
                                                       public dialogRef: MatDialogRef<CreditPaymentEditComponent>,
                                                       public  persist: CreditPaymentPersist,
                                                       public patientNavigator: PatientNavigator,
                                                       public tcUtilsDate: TCUtilsDate,

                                                       @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("credit_payments");
      this.title = this.appTranslation.getText("general","new") +  " " + "credit_payment";
      this.creditPaymentDetail = new CreditPaymentDetail();
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("credit_payments");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "credit_payment";
      this.isLoadingResults = true;
      this.persist.getCreditPayment(this.idMode.id).subscribe(creditPaymentDetail => {
        this.creditPaymentDetail = creditPaymentDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addCreditPayment(this.creditPaymentDetail).subscribe(value => {
      this.tcNotification.success("creditPayment added");
      this.creditPaymentDetail.id = value.id;
      this.dialogRef.close(this.creditPaymentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateCreditPayment(this.creditPaymentDetail).subscribe(value => {
      this.tcNotification.success("credit_payment updated");
      this.dialogRef.close(this.creditPaymentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
    if (this.creditPaymentDetail == null){
      return false;
    }
    return true;

  }

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);

    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.creditPaymentDetail.patient_id = result[0].id;
      }
    });
  }
}
