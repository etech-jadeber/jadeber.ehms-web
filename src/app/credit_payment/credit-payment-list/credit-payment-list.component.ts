import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from "@angular/material/sort";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {CreditPaymentDetail, CreditPaymentSummary, CreditPaymentSummaryPartialList} from "../credit_payment.model";
import {CreditPaymentNavigator} from "../credit_payment.navigator";
import {CreditPaymentPersist} from "../credit_payment.persist";
import {JobData} from "../../tc/jobs/job.model";
import {FilePersist} from "../../tc/files/file.persist";
import {TCAsyncJob} from "../../tc/asyncjob";
import {openness_status} from "../../app.enums";
@Component({
  selector: 'app-credit-payment-list',
  templateUrl: './credit-payment-list.component.html',
  styleUrls: ['./credit-payment-list.component.scss']
})export class CreditPaymentListComponent implements OnInit {
  creditPaymentsData: CreditPaymentSummary[] = [];
  creditPaymentsTotalCount: number = 0;
  creditPaymentSelectAll:boolean = false;
  creditPaymentSelection: CreditPaymentSummary[] = [];
  openness_status = openness_status;


  creditPaymentsDisplayedColumns: string[] = ["select","action", "pid", "patient_id", "telephone", "current_amount", "start_date", "end_date","status" ];
  creditPaymentSearchTextBox: FormControl = new FormControl();
  creditPaymentIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) creditPaymentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) creditPaymentsSort: MatSort;  constructor(private router: Router,
                                                                                private location: Location,
                                                                                public tcAuthorization:TCAuthorization,
                                                                                public tcUtilsAngular: TCUtilsAngular,
                                                                                public tcUtilsArray: TCUtilsArray,
                                                                                public tcUtilsDate:TCUtilsDate,
                                                                                public tcNotification: TCNotification,
                                                                                public tcNavigator: TCNavigator,
                                                                                public appTranslation:AppTranslation,
                                                                                public creditPaymentPersist: CreditPaymentPersist,
                                                                                public creditPaymentNavigator: CreditPaymentNavigator,
                                                                                public jobPersist: JobPersist,
                                                                                public  jobData: JobData,
                                                                                public filePersist: FilePersist,
                                                                                public tcAsyncJob: TCAsyncJob,

  ) {

    this.tcAuthorization.requireRead("credit_payments");
    this.creditPaymentSearchTextBox.setValue(creditPaymentPersist.creditPaymentSearchText);
    //delay subsequent keyup events
    this.creditPaymentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.creditPaymentPersist.creditPaymentSearchText = value;
      this.searchCredit_payments();
    });
  } ngOnInit() {

    this.creditPaymentsSort.sortChange.subscribe(() => {
      this.creditPaymentsPaginator.pageIndex = 0;
      this.searchCredit_payments(true);
    });

    this.creditPaymentsPaginator.page.subscribe(() => {
      this.searchCredit_payments(true);
    });
    //start by loading items
    this.searchCredit_payments();
  }

  searchCredit_payments(isPagination:boolean = false): void {


    let paginator = this.creditPaymentsPaginator;
    let sorter = this.creditPaymentsSort;

    this.creditPaymentIsLoading = true;
    this.creditPaymentSelection = [];

    this.creditPaymentPersist.searchCreditPayment(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditPaymentSummaryPartialList) => {
      this.creditPaymentsData = partialList.data;
      if (partialList.total != -1) {
        this.creditPaymentsTotalCount = partialList.total;
      }
      this.creditPaymentIsLoading = false;
    }, error => {
      this.creditPaymentIsLoading = false;
    });

  } downloadCreditPayments(): void {
    if(this.creditPaymentSelectAll){
      this.creditPaymentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download credit_payment", true);
      });
    }
    else{
      this.creditPaymentPersist.download(this.tcUtilsArray.idsList(this.creditPaymentSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download credit_payment",true);
      });
    }
  }
  addCredit_payment(): void {
    let dialogRef = this.creditPaymentNavigator.addCreditPayment();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCredit_payments();
      }
    });
  }

  editCreditPayment(item: CreditPaymentSummary) {
    let dialogRef = this.creditPaymentNavigator.editCreditPayment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCreditPayment(item: CreditPaymentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("credit_payment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.creditPaymentPersist.deleteCreditPayment(item.id).subscribe(response => {
          this.tcNotification.success("credit_payment deleted");
          this.searchCredit_payments();
        }, error => {
        });
      }

    });
  }  back():void{
    this.location.back();
  }

  activation(detail: CreditPaymentDetail, isOpen: boolean){
    this.creditPaymentPersist.creditPaymentDo(detail.id, isOpen ? "open" : "close", {}).subscribe(
      value => {
        this.searchCredit_payments();
      }
    )
  }

}
