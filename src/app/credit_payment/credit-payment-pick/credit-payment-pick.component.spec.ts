import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentPickComponent } from './credit-payment-pick.component';

describe('CreditPaymentPickComponent', () => {
  let component: CreditPaymentPickComponent;
  let fixture: ComponentFixture<CreditPaymentPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
