import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {CreditPaymentSummary, CreditPaymentSummaryPartialList} from "../credit_payment.model";
import {CreditPaymentPersist} from "../credit_payment.persist";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
@Component({
  selector: 'app-credit-payment-pick',
  templateUrl: './credit-payment-pick.component.html',
  styleUrls: ['./credit-payment-pick.component.scss']
})export class CreditPaymentPickComponent implements OnInit {
  creditPaymentsData: CreditPaymentSummary[] = [];
  creditPaymentsTotalCount: number = 0;
  creditPaymentSelectAll:boolean = false;
  creditPaymentSelection: CreditPaymentSummary[] = [];

  creditPaymentsDisplayedColumns: string[] = ["select","action", "pid", "patient_id", "telephone", "current_amount", "start_date", "end_date","status" ];
  creditPaymentSearchTextBox: FormControl = new FormControl();
  creditPaymentIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) creditPaymentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) creditPaymentsSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                                                                                                public tcUtilsAngular: TCUtilsAngular,
                                                                                                public tcUtilsArray: TCUtilsArray,
                                                                                                public tcUtilsDate:TCUtilsDate,
                                                                                                public tcNotification: TCNotification,
                                                                                                public tcNavigator: TCNavigator,
                                                                                                public appTranslation:AppTranslation,
                                                                                                public creditPaymentPersist: CreditPaymentPersist,
                                                                                                public dialogRef: MatDialogRef<CreditPaymentPickComponent>,
                                                                                                @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    this.tcAuthorization.requireRead("credit_payment");
    this.creditPaymentSearchTextBox.setValue(creditPaymentPersist.creditPaymentSearchText);
    //delay subsequent keyup events
    this.creditPaymentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.creditPaymentPersist.creditPaymentSearchText = value;
      this.searchCredit_payments();
    });
  } ngOnInit() {

    this.creditPaymentsSort.sortChange.subscribe(() => {
      this.creditPaymentsPaginator.pageIndex = 0;
      this.searchCredit_payments(true);
    });

    this.creditPaymentsPaginator.page.subscribe(() => {
      this.searchCredit_payments(true);
    });
    //start by loading items
    this.searchCredit_payments();
  }

  searchCredit_payments(isPagination:boolean = false): void {


    let paginator = this.creditPaymentsPaginator;
    let sorter = this.creditPaymentsSort;

    this.creditPaymentIsLoading = true;
    this.creditPaymentSelection = [];

    this.creditPaymentPersist.searchCreditPayment(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditPaymentSummaryPartialList) => {
      this.creditPaymentsData = partialList.data;
      if (partialList.total != -1) {
        this.creditPaymentsTotalCount = partialList.total;
      }
      this.creditPaymentIsLoading = false;
    }, error => {
      this.creditPaymentIsLoading = false;
    });

  }
  markOneItem(item: CreditPaymentSummary) {
    if(!this.tcUtilsArray.containsId(this.creditPaymentSelection,item.id)){
      this.creditPaymentSelection = [];
      this.creditPaymentSelection.push(item);
    }
    else{
      this.creditPaymentSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.creditPaymentSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
