import {TCId} from "../tc/models";
export class CreditPaymentSummary extends TCId {
  patient_id:string;
  user_id:string;
  start_date:number;
  end_date:number;
  amount:number;
  status:number;
  current_amount: number;

}
export class CreditPaymentSummaryPartialList {
  data: CreditPaymentSummary[];
  total: number;
}
export class CreditPaymentDetail extends CreditPaymentSummary {
}

export class CreditPaymentDashboard {
  total: number;
}
