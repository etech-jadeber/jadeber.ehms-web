import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CreditPaymentEditComponent} from "./credit-payment-edit/credit-payment-edit.component";
import {CreditPaymentPickComponent} from "./credit-payment-pick/credit-payment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CreditPaymentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  credit_paymentsUrl(): string {
    return "/credit_payments";
  }

  credit_paymentUrl(id: string): string {
    return "/credit_payments/" + id;
  }

  viewCreditPayments(): void {
    this.router.navigateByUrl(this.credit_paymentsUrl());
  }

  viewCreditPayment(id: string): void {
    this.router.navigateByUrl(this.credit_paymentUrl(id));
  }

  editCreditPayment(id: string): MatDialogRef<CreditPaymentEditComponent> {
    const dialogRef = this.dialog.open(CreditPaymentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCreditPayment(): MatDialogRef<CreditPaymentEditComponent> {
    const dialogRef = this.dialog.open(CreditPaymentEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickCreditPayments(selectOne: boolean=false): MatDialogRef<CreditPaymentPickComponent> {
    const dialogRef = this.dialog.open(CreditPaymentPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
