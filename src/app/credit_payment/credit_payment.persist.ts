import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {CreditPaymentDashboard, CreditPaymentDetail, CreditPaymentSummaryPartialList} from "./credit_payment.model";
import {openness_status} from "../app.enums";
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class CreditPaymentPersist {
  creditPaymentSearchText: string = "";constructor(private http: HttpClient) {
  }
  status: number;
  patient_id: string;

  opennessStatus: TCEnum[] = [
    new TCEnum(openness_status.open, "Open"),
    new TCEnum(openness_status.closed, "Closed"),
  ]
  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_payment/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.creditPaymentSearchText;
    //add custom filters
    return fltrs;
  }

  searchCreditPayment(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CreditPaymentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("credit_payment", this.creditPaymentSearchText, pageSize, pageIndex, sort, order);
    if(this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    if(this.patient_id){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patient_id.toString())
    }
    return this.http.get<CreditPaymentSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_payment/do", new TCDoParam("download_all", this.filters()));
  }

  creditPaymentDashboard(): Observable<CreditPaymentDashboard> {
    return this.http.get<CreditPaymentDashboard>(environment.tcApiBaseUri + "credit_payment/dashboard");
  }

  getCreditPayment(id: string): Observable<CreditPaymentDetail> {
    return this.http.get<CreditPaymentDetail>(environment.tcApiBaseUri + "credit_payment/" + id);
  }

  addCreditPayment(item: CreditPaymentDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'credit_payment/',
      item
    );
  }

  updateCreditPayment(item: CreditPaymentDetail): Observable<CreditPaymentDetail> {
    return this.http.patch<CreditPaymentDetail>(
      environment.tcApiBaseUri + 'credit_payment/' + item.id,
      item
    );
  }

  deleteCreditPayment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'credit_payment/' + id);
  }
  creditPaymentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'credit_payment/do',
      new TCDoParam(method, payload)
    );
  }

  creditPaymentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'credit_payment/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'credit_payment/do',
      new TCDoParam('download', ids)
    );
  }
}
