import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentHistoryDetailComponent } from './credit-payment-history-detail.component';

describe('CreditPaymentHistoryDetailComponent', () => {
  let component: CreditPaymentHistoryDetailComponent;
  let fixture: ComponentFixture<CreditPaymentHistoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentHistoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
