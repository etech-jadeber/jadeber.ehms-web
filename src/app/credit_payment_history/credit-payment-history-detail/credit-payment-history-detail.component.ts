import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import {CreditPaymentHistoryDetail} from "../credit_payment_history.model";
import {CreditPaymentHistoryNavigator} from "../credit_payment_history.navigator";
import {CreditPaymentHistoryPersist} from "../credit_payment_history.persist";

export enum CreditPaymentHistoryTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-credit-payment-history-detail',
  templateUrl: './credit-payment-history-detail.component.html',
  styleUrls: ['./credit-payment-history-detail.component.scss']
})
export class CreditPaymentHistoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  creditPaymentHistoryIsLoading:boolean = false;

  CreditPaymentHistoryTabs: typeof CreditPaymentHistoryTabs = CreditPaymentHistoryTabs;
  activeTab: CreditPaymentHistoryTabs = CreditPaymentHistoryTabs.overview;
  //basics
  creditPaymentHistoryDetail: CreditPaymentHistoryDetail;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public creditPaymentHistoryNavigator: CreditPaymentHistoryNavigator,
              public  creditPaymentHistoryPersist: CreditPaymentHistoryPersist) {
    this.tcAuthorization.requireRead("credit_payment_historys");
    this.creditPaymentHistoryDetail = new CreditPaymentHistoryDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("credit_payment_historys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.creditPaymentHistoryIsLoading = true;
    this.creditPaymentHistoryPersist.getCreditPaymentHistory(id).subscribe(creditPaymentHistoryDetail => {
      this.creditPaymentHistoryDetail = creditPaymentHistoryDetail;
      this.creditPaymentHistoryIsLoading = false;
    }, error => {
      console.error(error);
      this.creditPaymentHistoryIsLoading = false;
    });
  }

  reload(){
    this.loadDetails(this.creditPaymentHistoryDetail.id);
  }
  editCreditPaymentHistory(): void {
    let modalRef = this.creditPaymentHistoryNavigator.editCreditPaymentHistory(this.creditPaymentHistoryDetail.id);
    modalRef.afterClosed().subscribe(creditPaymentHistoryDetail => {
      TCUtilsAngular.assign(this.creditPaymentHistoryDetail, creditPaymentHistoryDetail);
    }, error => {
      console.error(error);
    });
  }

  printBirth():void{
    this.creditPaymentHistoryPersist.print(this.creditPaymentHistoryDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print credit_payment_history", true);
    });
  }

  back():void{
    this.location.back();
  }

}
