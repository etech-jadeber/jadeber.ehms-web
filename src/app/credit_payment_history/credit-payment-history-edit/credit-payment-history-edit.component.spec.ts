import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentHistoryEditComponent } from './credit-payment-history-edit.component';

describe('CreditPaymentHistoryEditComponent', () => {
  let component: CreditPaymentHistoryEditComponent;
  let fixture: ComponentFixture<CreditPaymentHistoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentHistoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
