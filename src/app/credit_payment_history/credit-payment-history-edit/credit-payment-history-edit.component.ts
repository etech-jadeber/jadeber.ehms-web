import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { CreditPaymentHistoryDetail } from '../credit_payment_history.model';import { CreditPaymentHistoryPersist } from '../credit_payment_history.persist';@Component({
  selector: 'app-credit-payment-history-edit',
  templateUrl: './credit-payment-history-edit.component.html',
  styleUrls: ['./credit-payment-history-edit.component.scss']
})export class CreditPaymentHistoryEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  creditPaymentHistoryDetail: CreditPaymentHistoryDetail;constructor(public tcAuthorization:TCAuthorization,
                                                                     public tcNotification: TCNotification,
                                                                     public tcUtilsString:TCUtilsString,
                                                                     public appTranslation:AppTranslation,
                                                                     public dialogRef: MatDialogRef<CreditPaymentHistoryEditComponent>,
                                                                     public  persist: CreditPaymentHistoryPersist,

                                                                     public tcUtilsDate: TCUtilsDate,

                                                                     @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("credit_payment_historys");
      this.title = this.appTranslation.getText("general","new") +  " " + "credit_payment_history";
      this.creditPaymentHistoryDetail = new CreditPaymentHistoryDetail();
      //set necessary defaults
      this.creditPaymentHistoryDetail.credit_payment_id = this.idMode.id;

    } else {
      this.tcAuthorization.requireUpdate("credit_payment_historys");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "credit_payment_history";
      this.isLoadingResults = true;
      this.persist.getCreditPaymentHistory(this.idMode.id).subscribe(creditPaymentHistoryDetail => {
        this.creditPaymentHistoryDetail = creditPaymentHistoryDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addCreditPaymentHistory(this.creditPaymentHistoryDetail).subscribe(value => {
      this.tcNotification.success("creditPaymentHistory added");
      this.creditPaymentHistoryDetail.id = value.id;
      this.dialogRef.close(this.creditPaymentHistoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateCreditPaymentHistory(this.creditPaymentHistoryDetail).subscribe(value => {
      this.tcNotification.success("credit_payment_history updated");
      this.dialogRef.close(this.creditPaymentHistoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
    if (this.creditPaymentHistoryDetail == null){
      return false;
    }
    if (!this.creditPaymentHistoryDetail.type){
      return false;
    }
    if (this.creditPaymentHistoryDetail.amount <= 0){
      return false;
    }
    return true;
  }
}
