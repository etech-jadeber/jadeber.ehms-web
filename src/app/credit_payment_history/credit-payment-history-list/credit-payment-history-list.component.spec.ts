import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentHistoryListComponent } from './credit-payment-history-list.component';

describe('CreditPaymentHistoryListComponent', () => {
  let component: CreditPaymentHistoryListComponent;
  let fixture: ComponentFixture<CreditPaymentHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
