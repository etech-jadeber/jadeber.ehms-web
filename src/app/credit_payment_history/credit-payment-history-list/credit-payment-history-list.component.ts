import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from "@angular/material/sort";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {CreditPaymentHistorySummary, CreditPaymentHistorySummaryPartialList} from "../credit_payment_history.model";
import {CreditPaymentHistoryPersist} from "../credit_payment_history.persist";
import {CreditPaymentHistoryNavigator} from "../credit_payment_history.navigator";
import {JobData} from "../../tc/jobs/job.model";
import {FilePersist} from "../../tc/files/file.persist";
import {TCAsyncJob} from "../../tc/asyncjob";
import {UserPersist} from "../../tc/users/user.persist";
import {UserDetail} from "../../tc/users/user.model";
@Component({
  selector: 'app-credit-payment-history-list',
  templateUrl: './credit-payment-history-list.component.html',
  styleUrls: ['./credit-payment-history-list.component.scss']
})export class CreditPaymentHistoryListComponent implements OnInit {
  creditPaymentHistorysData: CreditPaymentHistorySummary[] = [];
  creditPaymentHistorysTotalCount: number = 0;
  creditPaymentHistorySelectAll:boolean = false;
  creditPaymentHistorySelection: CreditPaymentHistorySummary[] = [];
  user : {[id:string]:UserDetail} = {}

  creditPaymentHistorysDisplayedColumns: string[] = ["select","action", "type", "amount","date", "user_id" ];
  creditPaymentHistorySearchTextBox: FormControl = new FormControl();
  creditPaymentHistoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) creditPaymentHistorysPaginator: MatPaginator;
  @Input() credit_payment_id: string;
  @ViewChild(MatSort, {static: true}) creditPaymentHistorysSort: MatSort;  constructor(private router: Router,
                                                                                       private location: Location,
                                                                                       public tcAuthorization:TCAuthorization,
                                                                                       public tcUtilsAngular: TCUtilsAngular,
                                                                                       public tcUtilsArray: TCUtilsArray,
                                                                                       public tcUtilsDate:TCUtilsDate,
                                                                                       public tcNotification: TCNotification,
                                                                                       public tcNavigator: TCNavigator,
                                                                                       public appTranslation:AppTranslation,
                                                                                       public creditPaymentHistoryPersist: CreditPaymentHistoryPersist,
                                                                                       public creditPaymentHistoryNavigator: CreditPaymentHistoryNavigator,
                                                                                       public jobPersist: JobPersist,
                                                                                       public  jobData: JobData,
                                                                                       public filePersist: FilePersist,
                                                                                       public tcAsyncJob: TCAsyncJob,
                                                                                       public userPersist: UserPersist,

  ) {

    this.tcAuthorization.requireRead("credit_payment_historys");
    this.creditPaymentHistorySearchTextBox.setValue(creditPaymentHistoryPersist.creditPaymentHistorySearchText);
    //delay subsequent keyup events
    this.creditPaymentHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.creditPaymentHistoryPersist.creditPaymentHistorySearchText = value;
      this.searchCredit_payment_historys();
    });
  } ngOnInit() {

    this.creditPaymentHistoryPersist.credit_payment_id = this.credit_payment_id;
    this.creditPaymentHistorysSort.sortChange.subscribe(() => {
      this.creditPaymentHistorysPaginator.pageIndex = 0;
      this.searchCredit_payment_historys(true);
    });

    this.creditPaymentHistorysPaginator.page.subscribe(() => {
      this.searchCredit_payment_historys(true);
    });
    //start by loading items
    this.searchCredit_payment_historys();
  }

  searchCredit_payment_historys(isPagination:boolean = false): void {


    let paginator = this.creditPaymentHistorysPaginator;
    let sorter = this.creditPaymentHistorysSort;

    this.creditPaymentHistoryIsLoading = true;
    this.creditPaymentHistorySelection = [];

    this.creditPaymentHistoryPersist.searchCreditPaymentHistory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditPaymentHistorySummaryPartialList) => {
      this.creditPaymentHistorysData = partialList.data;
      this.creditPaymentHistorysData.forEach(data =>{
        if(data.user_id && !this.user[data.user_id]){
          this.userPersist.getUser(data.user_id).subscribe((_user)=>{
            if(_user){
              this.user[data.user_id] = _user;
            }
          })
        }
      })
      if (partialList.total != -1) {
        this.creditPaymentHistorysTotalCount = partialList.total;
      }
      this.creditPaymentHistoryIsLoading = false;
    }, error => {
      this.creditPaymentHistoryIsLoading = false;
    });

  } downloadCreditPaymentHistorys(): void {
    if(this.creditPaymentHistorySelectAll){
      this.creditPaymentHistoryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download credit_payment_history", true);
      });
    }
    else{
      this.creditPaymentHistoryPersist.download(this.tcUtilsArray.idsList(this.creditPaymentHistorySelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download credit_payment_history",true);
      });
    }
  }
  addCredit_payment_history(): void {
    let dialogRef = this.creditPaymentHistoryNavigator.addCreditPaymentHistory(this.credit_payment_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCredit_payment_historys();
      }
    });
  }

  editCreditPaymentHistory(item: CreditPaymentHistorySummary) {
    let dialogRef = this.creditPaymentHistoryNavigator.editCreditPaymentHistory(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCreditPaymentHistory(item: CreditPaymentHistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("credit_payment_history");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.creditPaymentHistoryPersist.deleteCreditPaymentHistory(item.id).subscribe(response => {
          this.tcNotification.success("credit_payment_history deleted");
          this.searchCredit_payment_historys();
        }, error => {
        });
      }

    });
  }  back():void{
    this.location.back();
  }
}
