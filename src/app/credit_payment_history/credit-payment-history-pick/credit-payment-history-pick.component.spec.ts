import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditPaymentHistoryPickComponent } from './credit-payment-history-pick.component';

describe('CreditPaymentHistoryPickComponent', () => {
  let component: CreditPaymentHistoryPickComponent;
  let fixture: ComponentFixture<CreditPaymentHistoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditPaymentHistoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditPaymentHistoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
