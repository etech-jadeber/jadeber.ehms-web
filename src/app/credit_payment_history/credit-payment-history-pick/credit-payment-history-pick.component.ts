import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from "@angular/material/sort";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {CreditPaymentHistorySummary, CreditPaymentHistorySummaryPartialList} from "../credit_payment_history.model";
import {CreditPaymentHistoryPersist} from "../credit_payment_history.persist";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
@Component({
  selector: 'app-credit-payment-history-pick',
  templateUrl: './credit-payment-history-pick.component.html',
  styleUrls: ['./credit-payment-history-pick.component.scss']
})export class CreditPaymentHistoryPickComponent implements OnInit {
  creditPaymentHistorysData: CreditPaymentHistorySummary[] = [];
  creditPaymentHistorysTotalCount: number = 0;
  creditPaymentHistorySelectAll:boolean = false;
  creditPaymentHistorySelection: CreditPaymentHistorySummary[] = [];

  creditPaymentHistorysDisplayedColumns: string[] = ["select","action", "type", "amount","date" ];
  creditPaymentHistorySearchTextBox: FormControl = new FormControl();
  creditPaymentHistoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) creditPaymentHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) creditPaymentHistorysSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                                                                                                       public tcUtilsAngular: TCUtilsAngular,
                                                                                                       public tcUtilsArray: TCUtilsArray,
                                                                                                       public tcUtilsDate:TCUtilsDate,
                                                                                                       public tcNotification: TCNotification,
                                                                                                       public tcNavigator: TCNavigator,
                                                                                                       public appTranslation:AppTranslation,
                                                                                                       public creditPaymentHistoryPersist: CreditPaymentHistoryPersist,
                                                                                                       public dialogRef: MatDialogRef<CreditPaymentHistoryPickComponent>,
                                                                                                       @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    this.tcAuthorization.requireRead("credit_payment_history");
    this.creditPaymentHistorySearchTextBox.setValue(creditPaymentHistoryPersist.creditPaymentHistorySearchText);
    //delay subsequent keyup events
    this.creditPaymentHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.creditPaymentHistoryPersist.creditPaymentHistorySearchText = value;
      this.searchCredit_payment_historys();
    });
  } ngOnInit() {

    this.creditPaymentHistorysSort.sortChange.subscribe(() => {
      this.creditPaymentHistorysPaginator.pageIndex = 0;
      this.searchCredit_payment_historys(true);
    });

    this.creditPaymentHistorysPaginator.page.subscribe(() => {
      this.searchCredit_payment_historys(true);
    });
    //start by loading items
    this.searchCredit_payment_historys();
  }

  searchCredit_payment_historys(isPagination:boolean = false): void {


    let paginator = this.creditPaymentHistorysPaginator;
    let sorter = this.creditPaymentHistorysSort;

    this.creditPaymentHistoryIsLoading = true;
    this.creditPaymentHistorySelection = [];

    this.creditPaymentHistoryPersist.searchCreditPaymentHistory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: CreditPaymentHistorySummaryPartialList) => {
      this.creditPaymentHistorysData = partialList.data;
      if (partialList.total != -1) {
        this.creditPaymentHistorysTotalCount = partialList.total;
      }
      this.creditPaymentHistoryIsLoading = false;
    }, error => {
      this.creditPaymentHistoryIsLoading = false;
    });

  }
  markOneItem(item: CreditPaymentHistorySummary) {
    if(!this.tcUtilsArray.containsId(this.creditPaymentHistorySelection,item.id)){
      this.creditPaymentHistorySelection = [];
      this.creditPaymentHistorySelection.push(item);
    }
    else{
      this.creditPaymentHistorySelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.creditPaymentHistorySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
