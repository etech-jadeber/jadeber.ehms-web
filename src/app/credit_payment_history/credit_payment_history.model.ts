import {TCId} from "../tc/models";

export class CreditPaymentHistorySummary extends TCId {
  patient_id:string;
  user_id:string;
  credit_payment_id:string;
  type:number;
  amount:number;
  date:number;

}
export class CreditPaymentHistorySummaryPartialList {
  data: CreditPaymentHistorySummary[];
  total: number;
}
export class CreditPaymentHistoryDetail extends CreditPaymentHistorySummary {
}

export class CreditPaymentHistoryDashboard {
  total: number;
}
