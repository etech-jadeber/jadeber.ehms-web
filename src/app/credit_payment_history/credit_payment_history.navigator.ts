import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {CreditPaymentHistoryEditComponent} from "./credit-payment-history-edit/credit-payment-history-edit.component";
import {CreditPaymentHistoryPickComponent} from "./credit-payment-history-pick/credit-payment-history-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CreditPaymentHistoryNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  credit_payment_historysUrl(): string {
    return "/credit_payment_historys";
  }

  credit_payment_historyUrl(id: string): string {
    return "/credit_payment_historys/" + id;
  }

  viewCreditPaymentHistorys(): void {
    this.router.navigateByUrl(this.credit_payment_historysUrl());
  }

  viewCreditPaymentHistory(id: string): void {
    this.router.navigateByUrl(this.credit_payment_historyUrl(id));
  }

  editCreditPaymentHistory(id: string): MatDialogRef<CreditPaymentHistoryEditComponent> {
    const dialogRef = this.dialog.open(CreditPaymentHistoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCreditPaymentHistory(credit_payment_id: string): MatDialogRef<CreditPaymentHistoryEditComponent> {
    const dialogRef = this.dialog.open(CreditPaymentHistoryEditComponent, {
      data: new TCIdMode(credit_payment_id, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickCreditPaymentHistorys(selectOne: boolean=false): MatDialogRef<CreditPaymentHistoryPickComponent> {
    const dialogRef = this.dialog.open(CreditPaymentHistoryPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
