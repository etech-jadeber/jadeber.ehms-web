import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {CreditPaymentHistoryDashboard, CreditPaymentHistoryDetail, CreditPaymentHistorySummaryPartialList} from "./credit_payment_history.model";
import {credit_payment_history_type, openness_status} from "../app.enums";
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class CreditPaymentHistoryPersist {
  credit_payment_id: string;
  creditPaymentHistorySearchText: string = "";constructor(private http: HttpClient) {
  }

  creditPaymentHistoryType: TCEnum[] = [
    new TCEnum(credit_payment_history_type.credit, "Credit"),
    new TCEnum(credit_payment_history_type.debit, "Debit"),
  ]
  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_payment_history/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.creditPaymentHistorySearchText;
    //add custom filters
    return fltrs;
  }

  searchCreditPaymentHistory(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CreditPaymentHistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("credit_payment_history", this.creditPaymentHistorySearchText, pageSize, pageIndex, sort, order);
    if(this.credit_payment_id){
      url = TCUtilsString.appendUrlParameter(url, "credit_payment_id", this.credit_payment_id);
    }
    return this.http.get<CreditPaymentHistorySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "credit_payment_history/do", new TCDoParam("download_all", this.filters()));
  }

  creditPaymentHistoryDashboard(): Observable<CreditPaymentHistoryDashboard> {
    return this.http.get<CreditPaymentHistoryDashboard>(environment.tcApiBaseUri + "credit_payment_history/dashboard");
  }

  getCreditPaymentHistory(id: string): Observable<CreditPaymentHistoryDetail> {
    return this.http.get<CreditPaymentHistoryDetail>(environment.tcApiBaseUri + "credit_payment_history/" + id);
  }

  addCreditPaymentHistory(item: CreditPaymentHistoryDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'credit_payment_history/',
      item
    );
  }

  updateCreditPaymentHistory(item: CreditPaymentHistoryDetail): Observable<CreditPaymentHistoryDetail> {
    return this.http.patch<CreditPaymentHistoryDetail>(
      environment.tcApiBaseUri + 'credit_payment_history/' + item.id,
      item
    );
  }

  deleteCreditPaymentHistory(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'credit_payment_history/' + id);
  }
  creditPaymentHistorysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'credit_payment_history/do',
      new TCDoParam(method, payload)
    );
  }

  creditPaymentHistoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'credit_payment_history/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'credit_payment_history/do',
      new TCDoParam('download', ids)
    );
  }
}
