import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Input, OnChanges,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { TCUtilsDate } from './tc/utils-date'; // Ensure correct path to TCUtilsDate

@Directive({
  selector: '[appCustomDatepicker][dateModel]'
})
export class CustomDatepickerDirective implements OnInit, OnChanges, AfterViewInit{
  @Input() dateModel: number;
  @Input() type: string;
  @Input() matDatepicker: any
  @Input() ngxMatDatetimePicker: any
  @Output() dateModelChange: EventEmitter<number> = new EventEmitter<number>();
  constructor(public tcUtilsDate: TCUtilsDate, private renderer: Renderer2,
              private elementRef: ElementRef) {
  }

  updateDate(epoch: number){
    const formattedDate = this.type == 'datetime-local' ? this.tcUtilsDate.toNativeFormDateAndTime(epoch) : this.type == 'datetime' ? this.tcUtilsDate.formatDate(epoch) : this.type == 'date' ? this.tcUtilsDate.toNativeFormDate(epoch) : this.tcUtilsDate.formatDateFromEpoch(epoch);
    this.renderer.setProperty(this.elementRef.nativeElement, 'value', formattedDate);
    this.type != 'datetime-local' && this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', true)

  }

  ngOnChanges(changes: SimpleChanges) {
    if ('dateModel' in changes) {
      if(this.dateModel == null){
        this.matDatepicker?.select(null);
        this.ngxMatDatetimePicker?.select(null);
      } else {
        this.ngxMatDatetimePicker?.select(this.tcUtilsDate.toMomentDate(this.dateModel))
      }
      this.dateModelChange.emit(this.dateModel)
      this.updateDate(this.dateModel);
    }
    this.type != 'datetime-local' && this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', true)
  }

  ngAfterViewInit() {
    this.type != 'datetime-local' && this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', true)
  }

  ngOnInit() {
    // Set initial input value when dateModel is provided
    this.renderer.setProperty(this.elementRef.nativeElement, 'disabled', true)
    if (this.dateModel) {
      this.updateDate(this.dateModel)
      console.log(this.dateModel);
      this.ngxMatDatetimePicker?.select(this.tcUtilsDate.toMomentDate(this.dateModel))
    }
  }



  @HostListener('input', ['$event'])
  onInput(event: any) {
    const selectedDate = event.target.value;
    const epochTime = this.tcUtilsDate.toTimeStampFromString(selectedDate);
    this.dateModelChange.emit(epochTime)
    this.updateDate(epochTime)
  }

  @HostListener('dateInput', ['$event'])
  onDateInput(event: any) {
    const selectedDate = event.target.value;
    const epochTime = this.tcUtilsDate.toTimeStamp(selectedDate);
    this.dateModelChange.emit(epochTime)
    this.updateDate(epochTime)
  }
}
