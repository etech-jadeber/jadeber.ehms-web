import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appCustomDatetimepicker]'
})
export class CustomDatetimepickerDirective {

  constructor(private elementRef: ElementRef<HTMLInputElement>) {}

  @HostListener('input') onInput() {
    const inputValue = this.elementRef.nativeElement.value;
    // Example: Disable dates based on specific conditions (e.g., specific days)
    if (this.isDisabledDate(inputValue)) {
      this.elementRef.nativeElement.value = ''; // Clear input value if date is disabled
    }
  }

  private isDisabledDate(value: string): boolean {
    if (!value) {
      return false;
    }

    // Example condition: Disable dates based on day of the month
    const selectedDate = new Date(value);
    const day = selectedDate.getDate();
    const disabledDays = [1, 7, 15]; // Example disabled days
    return disabledDays.includes(day);
  }

}
