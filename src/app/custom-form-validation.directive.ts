import {AfterViewInit, Directive, ElementRef, Input, Renderer2} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, Validator} from "@angular/forms";

@Directive({
  selector: '[appCustomFormValidation]',
  providers: [{provide: NG_VALIDATORS, useExisting: CustomFormValidationDirective, multi: true}]
})
export class CustomFormValidationDirective implements Validator, AfterViewInit {
  private errorElement: HTMLElement;
  @Input() sequence: number;
  @Input() min: number;
  @Input() max: number;

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }

  ngAfterViewInit() {
    this.errorElement = this.renderer.createElement('mat-error');
    // this.errorElement.setAttribute('style', 'white-space:nowrap;direction:rtl');
    this.renderer.addClass(this.errorElement, 'mat-error');
    this.errorElement.textContent = "message"
    this.renderer.setStyle(this.errorElement, 'display', 'none');
    const element = this.el.nativeElement.parentNode.parentNode.parentNode.querySelector('div.mat-form-field-subscript-wrapper');
    this.renderer.appendChild(element, this.errorElement);
  }

  validate(control: AbstractControl): { [key: string]: any } | null {
    const value = control.value;

    if (value && this.sequence) {
      if (value < this.min) {
        this.showError(`Input must be greater than (${this.min})`);
        return {'invalidInput': true};
      }
      if (value > this.max) {
        this.showError(`Input must be less than (${this.max})`);
        return {'invalidInput': true};
      }
      if (value % this.sequence !== 0) {
        this.showError(`Input must be sequence of (${this.sequence})`);
        return {'invalidInput': true};
      }
    }
    this.removeError();
    return null;
  }

  private showError(message: string): void {
    if (this.errorElement) {
      this.errorElement.textContent = message
      this.renderer.setStyle(this.errorElement, 'display', 'block');
    }
  }

  private removeError(): void {
    if (this.errorElement) {
      this.renderer.setStyle(this.errorElement, 'display', 'none');
    }
  }
  isDisabled(){
    
  }
}
