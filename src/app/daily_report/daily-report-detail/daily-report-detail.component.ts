import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { DailyReportDetail } from "../daily-report.model";
import { DailyReportPersist } from "../daily-report.persist";
import { DailyReportNavigator } from "../daily-report.navigator";

export enum DailyReportTabs {
  overview,
}

export enum PaginatorIndexes {

} @Component({
  selector: 'app-daily_report-detail',
  templateUrl: './daily-report-detail.component.html',
  styleUrls: ['./daily-report-detail.component.css']
})
export class DailyReportDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  dailyReportIsLoading: boolean = false;

  DailyReportTabs: typeof DailyReportTabs = DailyReportTabs;
  activeTab: DailyReportTabs = DailyReportTabs.overview;
  //basics
  dailyReportDetail: DailyReportDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public dailyReportNavigator: DailyReportNavigator,
    public dailyReportPersist: DailyReportPersist) {
    this.tcAuthorization.requireRead("daily_reports");
    this.dailyReportDetail = new DailyReportDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("daily_reports");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.dailyReportIsLoading = true;
    this.dailyReportPersist.getDailyReport(id).subscribe(dailyReportDetail => {
      this.dailyReportDetail = dailyReportDetail;
      this.dailyReportIsLoading = false;
    }, error => {
      console.error(error);
      this.dailyReportIsLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.dailyReportDetail.id);
  }
  editDailyReport(): void {
    let modalRef = this.dailyReportNavigator.editDailyReport(this.dailyReportDetail.id);
    modalRef.afterClosed().subscribe(dailyReportDetail => {
      TCUtilsAngular.assign(this.dailyReportDetail, dailyReportDetail);
    }, error => {
      console.error(error);
    });
  }

  printBirth(): void {
    this.dailyReportPersist.print(this.dailyReportDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print daily_report", true);
    });
  }

  back(): void {
    this.location.back();
  }

}
