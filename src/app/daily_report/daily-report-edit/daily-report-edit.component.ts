import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCIdMode, TCModalModes } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DailyReportDetail } from '../daily-report.model';
import { DailyReportPersist } from '../daily-report.persist';

@Component({
  selector: 'app-daily_report-edit',
  templateUrl: './daily-report-edit.component.html',
  styleUrls: ['./daily-report-edit.component.css']
}) export class DailyReportEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  date: string;
  dailyReportDetail: DailyReportDetail; constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<DailyReportEditComponent>,
    public persist: DailyReportPersist,

    public tcUtilsDate: TCUtilsDate,
    @Inject(MAT_DIALOG_DATA) public data: DailyReportEditComponent,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  } ngOnInit() {
    this.date = this.getStartTime(new Date());
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("daily_reports");
      this.title = this.appTranslation.getText("general", "new") + " " + "daily_report";
      this.dailyReportDetail = new DailyReportDetail();
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("daily_reports");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "daily_report";
      this.isLoadingResults = true;
      this.persist.getDailyReport(this.idMode.id).subscribe(dailyReportDetail => {
        this.dailyReportDetail = dailyReportDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }


  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }


  onAdd(): void {
    this.isLoadingResults = true;
    this.dailyReportDetail.date = this.tcUtilsDate.toTimeStamp(new Date())
    console.log(this.dailyReportDetail)
    this.persist.add_daily_report(this.dailyReportDetail).subscribe(value => {
      this.tcNotification.success("dailyReport added");
      this.dailyReportDetail.id = value.id;
      this.dialogRef.close(this.dailyReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {

    this.isLoadingResults = true;


    this.persist.updateDailyReport(this.dailyReportDetail).subscribe(value => {
      this.tcNotification.success("daily_report updated");
      this.dialogRef.close(this.dailyReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  } canSubmit(): boolean {
    if (this.dailyReportDetail == null) {
      return false;
    }

    return true

  }
}
