import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { DailyReportSummary, DailyReportSummaryPartialList } from '../daily-report.model';
import { DailyReportNavigator } from '../daily-report.navigator';
import { DailyReportPersist } from '../daily-report.persist';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { UserDetail } from 'src/app/tc/users/user.model';
import { report } from 'process';
import { UserPersist } from 'src/app/tc/users/user.persist';
@Component({
  selector: 'app-daily_report-list',
  templateUrl: './daily-report-list.component.html',
  styleUrls: ['./daily-report-list.component.css']
}) export class DailyReportListComponent implements OnInit {
  dailyReportsData: DailyReportSummary[] = [];
  dailyReportsTotalCount: number = 0;
  dailyReportSelectAll: boolean = false;
  dailyReportSelection: DailyReportSummary[] = [];
  users: { [id: string]: UserDetail } = {};


  dailyReportsDisplayedColumns: string[] = ["select", "action" , "report", "date"];
  dailyReportSearchTextBox: FormControl = new FormControl();
  dailyReportIsLoading: boolean = false; @ViewChild(MatPaginator, { static: true }) dailyReportsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) dailyReportsSort: MatSort; constructor(private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dailyReportPersist: DailyReportPersist,
    public dailyReportNavigator: DailyReportNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public userPersist: UserPersist,

  ) {

    this.tcAuthorization.requireRead("daily_reports");
    this.dailyReportSearchTextBox.setValue(dailyReportPersist.dailyReportSearchText);
    //delay subsequent keyup events
    this.dailyReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.dailyReportPersist.dailyReportSearchText = value;
      this.searchDaily_reports();
    });
  } ngOnInit() {

    this.dailyReportsSort.sortChange.subscribe(() => {
      this.dailyReportsPaginator.pageIndex = 0;
      this.searchDaily_reports(true);
    });

    this.dailyReportsPaginator.page.subscribe(() => {
      this.searchDaily_reports(true);
    });
    this.dailyReportsPaginator.pageSize = 5;
    //start by loading items
    this.searchDaily_reports();
  }

  searchDaily_reports(isPagination: boolean = false): void {


    let paginator = this.dailyReportsPaginator;
    let sorter = this.dailyReportsSort;

    this.dailyReportIsLoading = true;
    this.dailyReportSelection = [];

    this.dailyReportPersist.searchDailyReport(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active || "date", sorter.direction || "desc").subscribe((partialList: DailyReportSummaryPartialList) => {
      this.dailyReportsData = partialList.data;
      this.dailyReportsData.forEach(report => {
        if (!this.users[report.provider]) {
          this.users[report.provider] = new UserDetail()
          this.userPersist.getUser(report.provider).subscribe(
            user => {
              this.users[report.provider] = user;
            }
          )
        }
      })
      if (partialList.total != -1) {
        this.dailyReportsTotalCount = partialList.total;
      }
      this.dailyReportIsLoading = false;
    }, error => {
      this.dailyReportIsLoading = false;
    });

  } downloadDailyReports(): void {
    if (this.dailyReportSelectAll) {
      this.dailyReportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download daily_report", true);
      });
    }
    else {
      this.dailyReportPersist.download(this.tcUtilsArray.idsList(this.dailyReportSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download daily_report", true);
      });
    }
  }
  addDaily_report(): void {
    let dialogRef = this.dailyReportNavigator.addDailyReport();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDaily_reports();
      }
    });
  }

  editDailyReport(item: DailyReportSummary) {
    let dialogRef = this.dailyReportNavigator.editDailyReport(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }




  deleteDailyReport(item: DailyReportSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("daily_report");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.dailyReportPersist.deleteDailyReport(item.id).subscribe(response => {
          this.tcNotification.success("daily_report deleted");
          this.searchDaily_reports();
        }, error => {
        });
      }

    });
  } back(): void {
    this.location.back();
  }

  getUser(id: string): string {
    return (this.users[id]?.name || "Unknown User")
  }
}
