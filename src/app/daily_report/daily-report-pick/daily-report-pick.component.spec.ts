import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyReportPickComponent } from './daily-report-pick.component';

describe('DailyReportPickComponent', () => {
  let component: DailyReportPickComponent;
  let fixture: ComponentFixture<DailyReportPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyReportPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyReportPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
