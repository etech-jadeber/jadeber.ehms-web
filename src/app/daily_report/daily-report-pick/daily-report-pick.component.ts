import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'; 
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { DailyReportSummary, DailyReportSummaryPartialList } from '../daily-report.model';
import { DailyReportPersist } from '../daily-report.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DailyReportNavigator } from '../daily-report.navigator';


@Component({
  selector: 'app-daily_report-list',
  templateUrl: './daily-report-pick.component.html',
  styleUrls: ['./daily-report-pick.component.css']
}) export class DailyReportPickComponent implements OnInit {
  dailyReportsData: DailyReportSummary[] = [];
  dailyReportsTotalCount: number = 0;
  dailyReportSelectAll: boolean = false;
  dailyReportSelection: DailyReportSummary[] = [];

  dailyReportsDisplayedColumns: string[] = ["select", "action", "provider", "report", "date"];
  dailyReportSearchTextBox: FormControl = new FormControl();
  dailyReportIsLoading: boolean = false; @ViewChild(MatPaginator, { static: true }) dailyReportsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) dailyReportsSort: MatSort; constructor(public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dailyReportPersist: DailyReportPersist,
    public dailyReportNavigator: DailyReportNavigator,
    public dialogRef: MatDialogRef<DailyReportPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    this.tcAuthorization.requireRead("daily_report");
    this.dailyReportSearchTextBox.setValue(dailyReportPersist.dailyReportSearchText);
    //delay subsequent keyup events
    this.dailyReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.dailyReportPersist.dailyReportSearchText = value;
      this.searchDaily_reports();
    });
  } ngOnInit() {

    this.dailyReportsSort.sortChange.subscribe(() => {
      this.dailyReportsPaginator.pageIndex = 0;
      this.searchDaily_reports(true);
    });

    this.dailyReportsPaginator.page.subscribe(() => {
      this.searchDaily_reports(true);
    });
    //start by loading items
    this.searchDaily_reports();
  }

  searchDaily_reports(isPagination: boolean = false): void {


    let paginator = this.dailyReportsPaginator;
    let sorter = this.dailyReportsSort;

    this.dailyReportIsLoading = true;
    this.dailyReportSelection = [];

    this.dailyReportPersist.searchDailyReport(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: DailyReportSummaryPartialList) => {
      this.dailyReportsData = partialList.data;
      if (partialList.total != -1) {
        this.dailyReportsTotalCount = partialList.total;
      }
      this.dailyReportIsLoading = false;
    }, error => {
      this.dailyReportIsLoading = false;
    });

  }
  markOneItem(item: DailyReportSummary) {
    if (!this.tcUtilsArray.containsId(this.dailyReportSelection, item.id)) {
      this.dailyReportSelection = [];
      this.dailyReportSelection.push(item);
    }
    else {
      this.dailyReportSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.dailyReportSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}