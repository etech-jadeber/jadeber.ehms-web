import { TCId } from "../tc/models";



export class DailyReportSummary extends TCId {
    provider: string;
    report: string;
    date: number;

}
export class DailyReportSummaryPartialList {
    data: DailyReportSummary[];
    total: number;
}
export class DailyReportDetail extends DailyReportSummary {
}

export class DailyReportDashboard {
    total: number;
}