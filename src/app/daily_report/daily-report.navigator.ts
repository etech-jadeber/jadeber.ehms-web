import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from "../tc/utils-angular";
import { TCIdMode, TCModalModes } from "../tc/models";

import { DailyReportEditComponent } from "./daily-report-edit/daily-report-edit.component";
import { DailyReportPickComponent } from "./daily-report-pick/daily-report-pick.component";


@Injectable({
    providedIn: 'root'
})

export class DailyReportNavigator {

    constructor(private router: Router,
        public dialog: MatDialog) {
    }
    daily_reportsUrl(): string {
        return "/daily_reports";
    }

    daily_reportUrl(id: string): string {
        return "/daily_reports/" + id;
    }

    viewDailyReports(): void {
        this.router.navigateByUrl(this.daily_reportsUrl());
    }

    viewDailyReport(id: string): void {
        this.router.navigateByUrl(this.daily_reportUrl(id));
    }

    editDailyReport(id: string): MatDialogRef<DailyReportEditComponent> {
        const dialogRef = this.dialog.open(DailyReportEditComponent, {
            data: new TCIdMode(id, TCModalModes.EDIT),
            width: TCModalWidths.medium
        });
        return dialogRef;
    }

    addDailyReport(): MatDialogRef<DailyReportEditComponent> {
        const dialogRef = this.dialog.open(DailyReportEditComponent, {
            data: new TCIdMode(null, TCModalModes.NEW),
            width: TCModalWidths.medium
        });
        return dialogRef;
    }

    pickDailyReports(selectOne: boolean = false): MatDialogRef<DailyReportPickComponent> {
        const dialogRef = this.dialog.open(DailyReportPickComponent, {
            data: selectOne,
            width: TCModalWidths.large
        });
        return dialogRef;
    }
}