import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";


import { TCDoParam, TCUrlParams, TCUtilsHttp } from "../tc/utils-http";
import { TCId, TcDictionary } from "../tc/models";
import { DailyReportDashboard, DailyReportDetail, DailyReportSummaryPartialList } from "./daily-report.model";


@Injectable({
    providedIn: 'root'
})
export class DailyReportPersist {
    dailyReportSearchText: string = ""; constructor(private http: HttpClient) {
    }
    print(id: string): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "daily_report/" + id + "/do", new TCDoParam("print", {}));
    } filters(): any {
        let fltrs: TcDictionary<string> = new TcDictionary<string>();
        fltrs[TCUrlParams.searchText] = this.dailyReportSearchText;
        //add custom filters
        return fltrs;
    }

    searchDailyReport(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DailyReportSummaryPartialList> {

        let url = TCUtilsHttp.buildSearchUrl("daily_report", this.dailyReportSearchText, pageSize, pageIndex, sort, order);
        return this.http.get<DailyReportSummaryPartialList>(url);

    } downloadAll(): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "daily_report/do", new TCDoParam("download_all", this.filters()));
    }

    dailyReportDashboard(): Observable<DailyReportDashboard> {
        return this.http.get<DailyReportDashboard>(environment.tcApiBaseUri + "daily_report/dashboard");
    }

    getDailyReport(id: string): Observable<DailyReportDetail> {
        return this.http.get<DailyReportDetail>(environment.tcApiBaseUri + "daily_report/" + id);
    }

    download(ids: string[]): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "daily_report/do", new TCDoParam("download", ids));
    }

    deleteDailyReport(id: string): Observable<{}> {
        return this.http.delete(environment.tcApiBaseUri + "daily_report/" + id);
    }
   
    add_daily_report(item: DailyReportDetail): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "daily_report/", item);
    }

    updateDailyReport(item: DailyReportDetail): Observable<DailyReportDetail> {
        return this.http.patch<DailyReportDetail>(environment.tcApiBaseUri + "daily_report/" + item.id, item);
    }
}