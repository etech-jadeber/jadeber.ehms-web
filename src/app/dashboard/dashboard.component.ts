import {Component, OnInit} from '@angular/core';
import {AppTranslation} from '../app.translation';

import {PatientCasePersisit} from '../patients/PatientCase.persisit';
import {PatientDashboard, PatientDetail, PatientSummary, PatientSummaryPartialList,PatientStatistics, ICD10_report} from '../patients/patients.model';
import {PatientNavigator} from '../patients/patients.navigator';
import {PatientPersist} from '../patients/patients.persist';
import {AppCaseTypes, UserContexts} from '../tc/app.enums';
import {TCAuthorization} from '../tc/authorization';
import {CaseDashboard, CaseSummary, CaseSummaryPartialList} from '../tc/cases/case.model';
import {TCNavigator} from '../tc/navigator';
import {TCNotification} from '../tc/notification';
import {TCUtilsArray} from '../tc/utils-array';
import {TCUtilsDate} from '../tc/utils-date';
import {WaitinglistPersist} from '../bed/waitinglists/waitinglist.persist';
import {ItemSummaryPartialList} from "../items/item.model";
import {ItemPersist} from "../items/item.persist";
import {EmployeeNavigator} from "../storeemployees/employee.navigator";
import {ReceptionNavigator} from '../receptions/reception.navigator';
import {DoctorNavigator} from '../doctors/doctor.navigator';
import {PurchaseSummaryPartialList} from "../purchases/purchase.model";
import {PurchasePersist} from "../purchases/purchase.persist";
import { AdmitByWardReport } from '../form_encounters/admission_forms/admission_form.model';
import { Admission_FormPersist } from '../form_encounters/admission_forms/admission_form.persist';
import { OrdersDashboard, SelectedOrdersDashboardInfo, Selected_OrdersDetail } from '../form_encounters/orderss/orders.model';
import { PaymentPersist } from '../payments/payment.persist';
import { PaymentDashboard } from '../payments/payment.model';
import { BedPersist } from '../bed/beds/bed.persist';
import { BedDashboard } from '../bed/beds/bed.model';
import { I } from '@angular/cdk/keycodes';
import { DoctorConsultationNavigator } from '../doctor_dashboard/doctor_consultation.navigator';
import { DoctorConsultationDashboard, DoctorConsultationSummary } from '../doctor_dashboard/doctor_consultation.model';
import { DoctorConsultationPersist } from '../doctor_dashboard/doctor_consultation.persist';
import {  SampleCollectionDashboardInfo } from '../sample_collection/sample_collection.model';
import { SampleCollectionPersist } from '../sample_collection/sample_collection.persist';
import { OrdersPersist } from '../form_encounters/orderss/orders.persist';
import { user_context } from '../app.enums';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  //coop dashboard

  UserContexts = UserContexts

  bedTypes = []
  itemsAboutToExpire = [];
  patientDashboard: PatientDashboard;
  lab_order_dashboard: OrdersDashboard[] = [];
  paymentDasboard: PaymentDashboard;
  consultation:DoctorConsultationSummary;
  caseDashboard: CaseDashboard;
  bedDashboard: BedDashboard;
  doctorConsultationDashboard: DoctorConsultationDashboard;
  itemsToReorder = [];
  isConsultationDashBoardLoading: boolean=false;
  isConsultationDashBoardLoaded: boolean=false;
  isPatientDashBoardLoading: boolean = false;
  isPatientDashBoardLoaded: boolean = false;
  isLab_order_dashboardLoading: boolean = false;
  isLab_order_dashboardLoaded: boolean = false;
  isPaymentDashboardLoading: boolean = false;
  isPaymentDashboardLoaded: boolean = false;
  casesByState = [];
  patientByState = [];
  patientCasesGeneralIsLoading: boolean = false;
  patientCasesGeneralData: CaseSummary[] = [];
  patientOfCase: PatientSummary[] = [];
  patientCasesGeneralTotalCount: number = 0
  chartColorScheme = {
    domain: ['#175D91', '#F79A20']
  };

  secondChartColorScheme = {
    domain: ['#F79A20']
  };
  CasesGeneralDisplayedColumns: string[] = [
    "action",
    "target_id"
  ];
  waitingListData = [];
  availableBedsForWaitingPeople: boolean = false;
  patientAppointmentCaseData: CaseSummary[];
  patientAppointmentCaseTotalCount: number = 0;
  ICD10_reportDisplayedColumns: String[] = ["name","infant", "child", "young", "old", "total"];
  ICD10_reportData: ICD10_report[] = [];
  ICD10_reportTotalCount: number = 0;
  admitByWardDisplayedColumns: String[] = ["name", "male", "female", "total"];
  admitByWardData: AdmitByWardReport[] = [];
  admitByWardTotalCount: number = 0;

  patientStatistics: PatientStatistics ;
  ageRangeDisplayColumn: string[] = ["sex","infant","child", "teen","young","old"];
  patientStatisticsBySex: PatientStatistics;

  sampleCollectionDashboardData: SampleCollectionDashboardInfo[] = [];
  isSampleCollectionLoading: boolean = false;

  selectedOrdersDashboardData: SelectedOrdersDashboardInfo[] = [];
  isSelectedOrdersLoading: boolean = false;


  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public tcUtilsArray: TCUtilsArray,
    public appTranslation: AppTranslation,
    private patientPersisit: PatientPersist,
    public casePersist: PatientCasePersisit,
    public patientNavigator: PatientNavigator,
    public employeeNavigator: EmployeeNavigator,
    public doctorNavigator: DoctorNavigator,
    public receptionNavigator: ReceptionNavigator,
    public doctorConsultationNavigator: DoctorConsultationNavigator,
    private bedPersist: BedPersist,
    private purchasePersist: PurchasePersist,
    private itemPersist: ItemPersist,
    private waitingListPersist: WaitinglistPersist,
    private admissionPersist: Admission_FormPersist,
    private lab_orders_persist: OrdersPersist,
    private paymentPersist: PaymentPersist,
    private doctorConsultingPersist:DoctorConsultationPersist,
    private sampleCollectionPersist: SampleCollectionPersist,
    private selectedOrdersPersist: OrdersPersist,
    public ordersPersist: OrdersPersist,
    public soketService: OrdersPersist,

  ) {
    this.patientDashboard = new PatientDashboard();
    this.patientStatistics = new PatientStatistics();
    this.lab_order_dashboard =[];
    this.paymentDasboard = new PaymentDashboard();
    this.doctorConsultationDashboard=new DoctorConsultationDashboard();

  }


  ngOnInit() {
    //TO DO
    // needs to updated with real value
    this.percentageCalculator(200, 200);

    this.tcAuthorization.requireLogin();

    if (this.tcAuthorization.hasMandatorys()) {
      this.tcNavigator.mandatory();
      return;
    }
    if (
      this.tcAuthorization.getUserContexts(user_context.patient).length > 0
    ) {
      this.patientNavigator.goHome();
      return;
    }


    // if (
    //   this.tcAuthorization.getUserContexts(user_context.employee).length > 0
    // ) {
    //   this.employeeNavigator.goHome();
    //   return;
    // }



    // if (
    //   this.tcAuthorization.getUserContexts(user_context.nurse).length > 0
    // ) {
    //   this.doctorNavigator.goNurseHome();
    //   return;
    // }



    // if (
    //   this.tcAuthorization.getUserContexts(user_context.lab).length > 0
    // ) {
    //   this.doctorNavigator.goLabHome();
    //   return;
    // }


    // if (
    //   this.tcAuthorization.getUserContexts(user_context.front_desk).length > 0
    // ) {
    //   this.receptionNavigator.goHome();
    //   return;
    // }

    

  


    this.bedPersist.bedDashboard().subscribe(
      (res) => {
        this.bedTypes = [
          {
            name: this.appTranslation.getText("bed", "taken"),
            value: res.total_beds - res.available_beds,
          },
          {
            name: this.appTranslation.getText("bed", "free"),
            value: res.available_beds,
          },
        ];
      }
    )

    this.waitingListPersist.waitinglistDashboard().subscribe(
      (res) => {
        this.availableBedsForWaitingPeople = res.total > 0 && res.available_beds > 0;
        this.waitingListData = [
          {
            name: this.appTranslation.getText("patient", "people_waiting"),
            value: res.total,
          },
          {
            name: this.appTranslation.getText("bed", "available_beds_to_be_assigned"),
            value: Math.min(res.total, res.available_beds),
          },
        ];
      }
    );
    if(this.tcAuthorization.canRead('payments')){
      this.loadPaymentDashboardInfo();
    }

    if((this.tcAuthorization.canRead('laboratory_orders') || this.tcAuthorization.canRead('radiology_orders') || this.tcAuthorization.canRead('pathology_orders'))){
      this.loadLabOrderDashboardInfo();
    }

    if(this.tcAuthorization.canRead('cases')){
      this.loadMemberCaseDashboardInfo();
    }

    if(this.tcAuthorization.canRead('sample_collections')){
      this.loadSampleCollectionDashboardInfo();
    }

    if(this.tcAuthorization.canRead('diagnostic_reports')){
      this.loadSelectedOrdersDashBoardInfo();
    }
    
    
    this.loadMemberDashboardInfo();

    if(this.tcAuthorization.getUserContexts(UserContexts.doctor).length){
      this.loadConsultingDashboardInfo();
    }
    

    if(this.tcAuthorization.canRead('patients')){
      this.loadPatients();
      this.loadICD10_report();
      this.loadPatientStatistics();
      this.searchCasesPatientGeneral();
      this.loadMemberDashboardInfo();
    }

    if(this.tcAuthorization.canRead('items')){
      this.loadCloseExpireDate();
      this.loadToReorderItems();
    }
    
    
    
    this.searchCasePatientAppointment();
    

    if(this.tcAuthorization.canRead('admission_forms')){
      this.admitByWardReport();
    }
    
    

  }

  loadMemberDashboardInfo() {
    if (this.tcAuthorization.canRead("patients")) {
      this.isPatientDashBoardLoading = true;
      this.patientPersisit.patientDashboard().subscribe(
        (data) => {
          this.patientDashboard = data;
          this.isPatientDashBoardLoaded = true;
          this.isPatientDashBoardLoading = false;


        },
        (error) => {
          this.isPatientDashBoardLoading = false;
        }
      );
      
    }
  }

  loadLabOrderDashboardInfo() {
      this.isLab_order_dashboardLoading = true;
      this.lab_orders_persist.ordersDashboard().subscribe(
        (data) => {
          this.lab_order_dashboard = data;
          this.isLab_order_dashboardLoading = false;


        },
        (error) => {
          this.isLab_order_dashboardLoading = false;
        }
      );
      
  }  

  loadSampleCollectionDashboardInfo(){
    this.isSampleCollectionLoading = true;
    this.sampleCollectionPersist.sampleCollectionDashboard().subscribe(
      data => {
        this.sampleCollectionDashboardData = data;
        this.isSampleCollectionLoading = false;
      }, error => {
        this.isSampleCollectionLoading = false;
        console.error(error)
      })
  }

  loadSelectedOrdersDashBoardInfo(){
    this.isSelectedOrdersLoading = true;
    this.selectedOrdersPersist.selectedOrdersDashboard().subscribe(
      data => {
        this.selectedOrdersDashboardData = data;
        this.isSelectedOrdersLoading = false;
      }, error => {
        this.isSelectedOrdersLoading = false;
        console.error(error)
      }
    )
  }

  loadPaymentDashboardInfo() {
    this.isPaymentDashboardLoading = true;
    this.paymentPersist.paymentDashboard().subscribe(
      (data) => {
        this.paymentDasboard = data;
        this.isPaymentDashboardLoaded = true;
        this.isPatientDashBoardLoading = false;


      },
      (error) => {
        this.isPatientDashBoardLoading = false;
      }
    );
    
}
loadConsultingDashboardInfo() {
  this.isConsultationDashBoardLoading = true;
  this.doctorConsultingPersist.DoctorconsultationDashboard().subscribe(
    (data) => {
      this.doctorConsultationDashboard = data;
      this.isConsultationDashBoardLoaded = true;
      this.isConsultationDashBoardLoading = false;


    },
    (error) => {
      this.isPatientDashBoardLoading = false;
    }
  );
  
}

getSelectedOnOrderType(order_type: number){
 return this.selectedOrdersDashboardData.filter(order => order.order_type == order_type)
}

  loadMemberCaseDashboardInfo() {
    if (this.tcAuthorization.canRead("cases")) {
      this.casePersist.caseDashboard().subscribe(
        (data) => {
          this.caseDashboard = data;
          this.casesByState = [
            {
              name: this.appTranslation.getText("case", "open"),
              value: this.caseDashboard.open,
            },
            {
              name: this.appTranslation.getText("case", "closed"),
              value: this.caseDashboard.total - this.caseDashboard.open,
            },
          ];
        },
        (error) => {
        }
      );
    }
  }

  searchCasePatientAppointment(): void {
    if (!this.tcAuthorization.canRead("cases")) {
      return;
    }

    this.casePersist.searchCase(AppCaseTypes.patient_appointment, 5, 0, "creation_time", "asc").subscribe(
      (result: CaseSummaryPartialList) => {
        this.patientAppointmentCaseData = result.data;
        this.patientAppointmentCaseTotalCount = result.total;
        this.fetchMembersOfCaseGeneral(result.data);
      }
    )

  }

  searchCasesPatientGeneral(): void {
    if (!this.tcAuthorization.canRead("cases")) {
      return;
    }

    this.patientCasesGeneralIsLoading = true;
    this.casePersist
      .searchCase(AppCaseTypes.patient_general, 5, 0, "creation_time", "asc")
      .subscribe(
        (partialList: CaseSummaryPartialList) => {
          this.patientCasesGeneralData = partialList.data;

          if (partialList.total != -1) {
            this.patientCasesGeneralTotalCount = partialList.total;
          }
          this.fetchMembersOfCaseGeneral(partialList.data);
          this.patientCasesGeneralIsLoading = false;
        },
        (error) => {
          this.patientCasesGeneralIsLoading = false;
        }
      );
  }

  fetchMembersOfCaseGeneral(patientCasesGeneralData): void {

    for (let cs of patientCasesGeneralData) {

      if (!this.tcUtilsArray.containsId(this.patientOfCase, cs.target_id)) {
        this.patientPersisit.getPatient(cs.target_id).subscribe(
          (patient) => {
            this.patientOfCase.push(patient);
          },
          err => {

          });
      }

    }
  }

  processPatientAppointmentCase(caseDetail: CaseSummary): void {
    let modalRef = this.patientNavigator.processAppointmentCase(caseDetail.id);
    modalRef.afterClosed().subscribe(
      (response) => {
        if (response != null) {
          this.searchCasePatientAppointment();
          this.loadMemberCaseDashboardInfo();
        }
      },
      (error) => {
        console.error(error);
      }
    );


  }


  processPatientGeneralCase(caseDetail: CaseSummary): void {
    let modalRef = this.patientNavigator.processCaseGeneral(caseDetail.id);
    modalRef.afterClosed().subscribe(
      (response) => {
        if (response != null) {
          this.searchCasesPatientGeneral();
          this.loadMemberCaseDashboardInfo();
        }
      },
      (error) => {
        console.error(error);
      }
    );

  }

  reloadCases(): void {
    this.searchCasesPatientGeneral();
  }


  fullPatientName(patientId: string): string {

    let patient: PatientDetail = this.tcUtilsArray.getById(
      this.patientOfCase,
      patientId
    );
    return patient == undefined
      ? ""
      : patient.fname + " " + patient.mname + " " + patient.lname;
  }

  perctageFormat(value: number): string {
    const str = value.toFixed(2);
    return str;
  }


  loadPatients() {
    this.patientPersisit
      .patientsDo("get_all_patients", "")
      .subscribe((result) => {
        this.patientOfCase = (result as PatientSummaryPartialList).data;
        this.patientByState = [
          {
            name: this.appTranslation.getText("patient", "total_patient"),
            value: this.patientOfCase.length
          }
        ]
      });
  }

  getPatientFullName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patientOfCase,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }

  loadCloseExpireDate() {

    this.purchasePersist.purchasesDo("get_items_with_close_expire_date", {}).subscribe(
      (result: PurchaseSummaryPartialList) => {
        this.itemsAboutToExpire = [
          {
            name: this.appTranslation.getText("inventory", "items_about_to_expire"),
            value: result.total
          }
        ];
      }, error => {
        console.error(error);
      }
    );
  }

  loadToReorderItems() {

    this.itemPersist.itemsDo("get_to_reorder_item", '').subscribe((result: ItemSummaryPartialList) => {
      this.itemsToReorder = [
        {
          name: this.appTranslation.getText("inventory", "items_to_reorder"),
          value: result.total,
        }
      ];
    }, error => {
      console.error(error)
    });
  }

  loadICD10_report(){
    this.patientPersisit.patientsDo("ICD10_report", {infant: this.patientPersisit.infant, child: this.patientPersisit.child, teen: this.patientPersisit.teen}).subscribe(
      (result: any) => {
        if (result){
        this.ICD10_reportData = result.data;
        if (result.total != -1) {
          this.ICD10_reportTotalCount = result.total;
        }
      }
    }, error => {
        console.error(error)
      }
    );
  }

  loadPatientStatistics(){
    this.patientPersisit.patientsDo("get_statistics", {}).subscribe((result:any) => {
      this.patientStatisticsBySex = result;
    });
  }

  admitByWardReport(){
    this.tcAuthorization.canRead('admission_forms')
    this.admissionPersist.admission_formsDo("admit_by_ward_report", {}).subscribe(
      (result: any) => {
        if (result){
        this.admitByWardData = result.data;
        if (result.total != -1){
          this.admitByWardTotalCount = result.total;
        }
      }
    }, error => {
        console.error(error);
      }
    )
  }

  percentageCalculator(value1: number, value2: number): [string, string, string] {
    const total = value1 + value2;
    const value1_percentage = (value1 / total) * 100;
    const value2_percentage = 100 - value1_percentage;
    return [this.setValue(total.toString()), this.setValue(value1_percentage.toFixed(2)), this.setValue(value2_percentage.toFixed(2))];
  }

  setValue(value) {
    return value == "NaN" ? 0 : value;
  }
}
