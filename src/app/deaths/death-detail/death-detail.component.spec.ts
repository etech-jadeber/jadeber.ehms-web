import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeathDetailComponent } from './death-detail.component';

describe('DeathDetailComponent', () => {
  let component: DeathDetailComponent;
  let fixture: ComponentFixture<DeathDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeathDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeathDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
