import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DeathDetail} from "../death.model";
import {DeathPersist} from "../death.persist";
import {DeathNavigator} from "../death.navigator";
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';

export enum DeathTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-death-detail',
  templateUrl: './death-detail.component.html',
  styleUrls: ['./death-detail.component.css']
})
export class DeathDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  deathLoading:boolean = false;
  
  DeathTabs: typeof DeathTabs = DeathTabs;
  activeTab: DeathTabs = DeathTabs.overview;
  //basics
  deathDetail: DeathDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcUtilsDate: TCUtilsDate,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public deathNavigator: DeathNavigator,
              public  deathPersist: DeathPersist,
              public patientPersist: PatientPersist,
              public doctorPersist: DoctorPersist,
              public form_encounterPersist: Form_EncounterPersist,
              ) {
    this.tcAuthorization.requireRead("deaths");
    this.deathDetail = new DeathDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("deaths");
    this.loadPatients();
    this.getAllDoctors();
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.deathLoading = true;
    this.deathPersist.getDeath(id).subscribe(deathDetail => {
          this.deathDetail = deathDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(deathDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.deathLoading = false;
        }, error => {
          console.error(error);
          this.deathLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.deathDetail.id);
  }

  editDeath(): void {
    let modalRef = this.deathNavigator.editDeath(this.deathDetail.id);
    modalRef.afterClosed().subscribe(modifiedDeathDetail => {
      TCUtilsAngular.assign(this.deathDetail, modifiedDeathDetail);
    }, error => {
      console.error(error);
    });
  }

   printDeath():void{
      this.deathPersist.print(this.deathDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print death", true);
      });
    }

  back():void{
      this.location.back();
    }
    patientFullName:string;
    patients: PatientSummary[] = [];
    doctorSummary: DoctorSummary[] = [];
    loadPatients() {
      this.patientPersist
        .patientsDo("get_all_patients", "")
        .subscribe((result) => {
          this.patients = (result as PatientSummaryPartialList).data;
        });
    }
  
    getPatientName(patientId): string {
      let patient: PatientSummary = this.tcUtilsArray.getById(
        this.patients,
        patientId
      );
      if (patient) {
        return `${patient.fname} ${patient.lname}`;
      }
    }
  
  
    getDoctorName(doctorId: string): string {
      let doctors: DoctorSummary = this.tcUtilsArray.getById(
        this.doctorSummary,
        doctorId
      );
      if (doctors) {
        return `${doctors.first_name} ${doctors.last_name}`;
      }
  
    }
  
    getAllDoctors(): void {
      this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
        this.doctorSummary = doctors;
      });
    }
}
