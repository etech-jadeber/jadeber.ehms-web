import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeathEditComponent } from './death-edit.component';

describe('DeathEditComponent', () => {
  let component: DeathEditComponent;
  let fixture: ComponentFixture<DeathEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeathEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeathEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
