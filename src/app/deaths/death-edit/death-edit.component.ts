import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {DeathDetail} from "../death.model";
import {DeathPersist} from "../death.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { DoctorSummary } from 'src/app/doctors/doctor.model';


@Component({
  selector: 'app-death-edit',
  templateUrl: './death-edit.component.html',
  styleUrls: ['./death-edit.component.css']
})
export class DeathEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  deathDetail: DeathDetail;
  date_of_death:string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DeathEditComponent>,
              public  persist: DeathPersist,
              public tcUtilsDate:TCUtilsDate,
              public patientNavigator: PatientNavigator,
              public patientPersist: PatientPersist,
              public doctorNavigator: DoctorNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("deaths");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "deaths");
      this.deathDetail = new DeathDetail();
      //set necessary defaults
      this.deathDetail.is_maternal = false;
      this.deathDetail.is_brought_dead = false;
      this.deathDetail.is_familly_aware = false;
      this.deathDetail.is_post_operation = false;
      this.transformDates(false);
      
    } else {
     this.tcAuthorization.requireUpdate("deaths");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "deaths");
      this.isLoadingResults = true;
      this.persist.getDeath(this.idMode.id).subscribe(deathDetail => {
        this.deathDetail = deathDetail;
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }
  transformDates(todate: boolean = true) {
    if (todate) {
      this.deathDetail.date_of_death = new Date(this.date_of_death).getTime() / 1000;
    } else {
      this.date_of_death=this.getStartTime(this.tcUtilsDate.toDate(this.deathDetail.date_of_death));
    }
  }
  changeReason():void{
    if( this.deathDetail.is_post_operation){
      this.deathDetail.is_brought_dead = false;
    }
  }
  changeDeathCondtition():void{
    if(!this.deathDetail.is_maternal){
      this.deathDetail.death_condition = undefined;
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
   
    if(!this.deathDetail.is_maternal){
      this.deathDetail.is_maternal = false;
    }
   
    this.persist.addDeath(this.idMode.id,this.deathDetail).subscribe(value => {
      this.tcNotification.success("Death added");
      this.deathDetail.id = value.id;
      this.dialogRef.close(this.deathDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    if(!this.deathDetail.is_maternal){
      this.deathDetail.is_maternal = false;
    }
    this.persist.updateDeath(this.deathDetail).subscribe(value => {
      this.tcNotification.success("Death updated");
      this.dialogRef.close(this.deathDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.deathDetail == null){
            return false;
          }
        if (this.deathDetail.primay_cause_of_death == null || this.deathDetail.primay_cause_of_death  == "") {
                      return false;
                    }

        return true;
      }


      patientFullName:string
      searchPatient() {
        let dialogRef = this.patientNavigator.pickPatients(true);
        dialogRef.afterClosed().subscribe((result: PatientSummary) => {
          if (result) {
            this.patientFullName = result[0].fname + ' ' + result[0].lname;
            this.deathDetail.patient_id = result[0].id;
          }
        });
      }
    
      getPatientName(patientId: string) {
        this.patientPersist.getPatient(patientId).subscribe((
            patient
          ) => {
            this.patientFullName = `${patient.fname} ${patient.mname} ${patient.lname}`;
          }
        )
      }
    
      userFullName:string;
      searchDoctors() {
        let dialogRef = this.doctorNavigator.pickDoctors(true);
        dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
          if (result) {
            this.userFullName = `${result[0].first_name} ${result[0].last_name}`;
            this.deathDetail.doctor_id = result[0].id;
          }
        });
      }
}
