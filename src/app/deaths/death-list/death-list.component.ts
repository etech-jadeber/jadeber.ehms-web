import {Component, Input, OnInit, Output, ViewChild,EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCModalWidths, TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {DeathPersist} from "../death.persist";
import {DeathNavigator} from "../death.navigator";
import {DeathDetail, DeathSummary, DeathSummaryPartialList} from "../death.model";
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';


@Component({
  selector: 'app-death-list',
  templateUrl: './death-list.component.html',
  styleUrls: ['./death-list.component.css']
})
export class DeathListComponent implements OnInit {

  deathsData: DeathSummary[] = [];
  deathsTotalCount: number = 0;
  deathsSelectAll:boolean = false;
  deathsSelection: DeathSummary[] = [];

  deathsDisplayedColumns: string[] = ["select","action","primay_cause_of_death","patient_id","date_of_death","is_maternal", "is_post_operation","doctor_id"];
  deathsSearchTextBox: FormControl = new FormControl();
  deathsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) deathsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) deathsSort: MatSort;

  @Input() encounterId :string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public deathPersist: DeathPersist,
                public deathNavigator: DeathNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public doctorPersist: DoctorPersist,
                public form_encounterPersist: Form_EncounterPersist,
    ) {

        this.tcAuthorization.requireRead("deaths");
       this.deathsSearchTextBox.setValue(deathPersist.deathSearchText);
      //delay subsequent keyup events
      this.deathsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.deathPersist.deathSearchText = value;
        this.searchDeaths();
      });
    }

    ngOnInit() {
this.deathPersist.patientId = this.patientId
      this.deathsSort.sortChange.subscribe(() => {
        this.deathsPaginator.pageIndex = 0;
        this.searchDeaths(true);
      });

      this.deathsPaginator.page.subscribe(() => {
        this.searchDeaths(true);
      });
      //start by loading items
      this.searchDeaths();
      this.loadPatients();
      this.getAllDoctors();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.deathPersist.encounterId = this.encounterId;
      } else {
      this.deathPersist.encounterId = null;
      }
      this.searchDeaths()
      }

  searchDeaths(isPagination:boolean = false): void {


    let paginator = this.deathsPaginator;
    let sorter = this.deathsSort;

    this.deathsIsLoading = true;
    this.deathsSelection = [];

    this.deathPersist.searchDeath(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DeathSummaryPartialList) => {
      this.deathsData = partialList.data;
      if (partialList.total != -1) {
        this.deathsTotalCount = partialList.total;
      }
      this.deathsIsLoading = false;
    }, error => {
      this.deathsIsLoading = false;
    });

  }

  downloadDeaths(): void {
    if(this.deathsSelectAll){
         this.deathPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download deaths", true);
      });
    }
    else{
        this.deathPersist.download(this.tcUtilsArray.idsList(this.deathsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download deaths",true);
            });
        }
  }



  addDeath(): void {
    let dialogRef = this.deathNavigator.addDeath(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDeaths();
      }
    });
  }

  editDeath(item: DeathSummary) {
    let dialogRef = this.deathNavigator.editDeath(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDeath(item: DeathSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Death");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.deathPersist.deleteDeath(item.id).subscribe(response => {
          this.tcNotification.success("Death deleted");
          this.searchDeaths();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }
    patientFullName:string;
    patients: PatientSummary[] = [];
    doctorSummary: DoctorSummary[] = [];
    loadPatients() {
      this.patientPersist
        .patientsDo("get_all_patients", "")
        .subscribe((result) => {
          this.patients = (result as PatientSummaryPartialList).data;
        });
    }
  
    getPatientName(patientId): string {
      let patient: PatientSummary = this.tcUtilsArray.getById(
        this.patients,
        patientId
      );
      if (patient) {
        return `${patient.fname} ${patient.lname}`;
      }
    }
  
  
    getDoctorName(doctorId: string): string {
      let doctors: DoctorSummary = this.tcUtilsArray.getById(
        this.doctorSummary,
        doctorId
      );
      if (doctors) {
        return `${doctors.first_name} ${doctors.last_name}`;
      }
  
    }
  
    getAllDoctors(): void {
      this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
        this.doctorSummary = doctors;
      });
    }
}
