import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DeathPickComponent } from './death-pick.component';

describe('DeathPickComponent', () => {
  let component: DeathPickComponent;
  let fixture: ComponentFixture<DeathPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DeathPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeathPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
