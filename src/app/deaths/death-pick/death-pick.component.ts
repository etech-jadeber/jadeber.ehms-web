import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {DeathDetail, DeathSummary, DeathSummaryPartialList} from "../death.model";
import {DeathPersist} from "../death.persist";
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-death-pick',
  templateUrl: './death-pick.component.html',
  styleUrls: ['./death-pick.component.css']
})
export class DeathPickComponent implements OnInit {

  deathsData: DeathSummary[] = [];
  deathsTotalCount: number = 0;
  deathsSelection: DeathSummary[] = [];
  deathsDisplayedColumns: string[] = ["select", 'patient_id','date_of_death','is_maternal','death_condition','is_post_operation','is_familly_aware','is_brought_dead','doctor_id','primay_cause_of_death','secondary_cause_of_death','other_co_morbidities' ];

  deathsSearchTextBox: FormControl = new FormControl();
  deathsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) deathsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) deathsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public deathPersist: DeathPersist,
              public dialogRef: MatDialogRef<DeathPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("deaths");
    this.deathsSearchTextBox.setValue(deathPersist.deathSearchText);
    //delay subsequent keyup events
    this.deathsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.deathPersist.deathSearchText = value;
      this.searchDeaths();
    });
  }

  ngOnInit() {

    this.deathsSort.sortChange.subscribe(() => {
      this.deathsPaginator.pageIndex = 0;
      this.searchDeaths();
    });

    this.deathsPaginator.page.subscribe(() => {
      this.searchDeaths();
    });

    //set initial picker list to 5
    this.deathsPaginator.pageSize = 5;

    //start by loading items
    this.searchDeaths();
  }

  searchDeaths(): void {
    this.deathsIsLoading = true;
    this.deathsSelection = [];

    this.deathPersist.searchDeath(this.deathsPaginator.pageSize,
        this.deathsPaginator.pageIndex,
        this.deathsSort.active, 
        this.deathsSort.direction).subscribe((partialList: DeathSummaryPartialList) => {
      this.deathsData = partialList.data;
      if (partialList.total != -1) {
        this.deathsTotalCount = partialList.total;
      }
      this.deathsIsLoading = false;
    }, error => {
      this.deathsIsLoading = false;
    });

  }

  markOneItem(item: DeathSummary) {
    if(!this.tcUtilsArray.containsId(this.deathsSelection,item.id)){
          this.deathsSelection = [];
          this.deathsSelection.push(item);
        }
        else{
          this.deathsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.deathsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
  