import {TCId} from "../tc/models";

export class DeathSummary extends TCId {
  patient_id : string;
date_of_death : number;
is_maternal : boolean;
death_condition : number;
is_post_operation : boolean;
is_familly_aware : boolean;
is_brought_dead : boolean;
doctor_id : string;
primay_cause_of_death : string;
secondary_cause_of_death : string;
other_co_morbidities : string;
encounter_id:string;
}

export class DeathSummaryPartialList {
  data: DeathSummary[];
  total: number;
}

export class DeathDetail extends DeathSummary {
  patient_id : string;
date_of_death : number;
is_maternal : boolean;
death_condition : number;
is_post_operation : boolean;
is_familly_aware : boolean;
is_brought_dead : boolean;
doctor_id : string;
primay_cause_of_death : string;
secondary_cause_of_death : string;
other_co_morbidities : string;
}

export class DeathDashboard {
  total: number;
}
