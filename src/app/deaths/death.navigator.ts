import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DeathEditComponent} from "./death-edit/death-edit.component";
import {DeathPickComponent} from "./death-pick/death-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DeathNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  deathsUrl(): string {
    return "/deaths";
  }

  deathUrl(id: string): string {
    return "/deaths/" + id;
  }

  viewDeaths(): void {
    this.router.navigateByUrl(this.deathsUrl());
  }

  viewDeath(id: string): void {
    this.router.navigateByUrl(this.deathUrl(id));
  }

  editDeath(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DeathEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addDeath(encounter_id :string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DeathEditComponent, {
          data: new TCIdMode(encounter_id, TCModalModes.NEW),
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickDeaths(selectOne: boolean=false): MatDialogRef<unknown, any> {
      const dialogRef = this.dialog.open(DeathPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
