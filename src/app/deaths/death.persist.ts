import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum, TCEnumTranslation} from "../tc/models";
import {DeathDashboard, DeathDetail, DeathSummaryPartialList} from "./death.model";
import { death_condition } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DeathPersist {

  deathSearchText: string = "";
  death_conditionId: number ;
  encounterId: string;
  patientId: string;

  constructor(private http: HttpClient, public appTranslation: AppTranslation) {
  }

  death_condition: TCEnum[] = [
  new TCEnum(death_condition.Antepartum, this.appTranslation.getKey("patient", "antepartum")),
  new TCEnum(death_condition.Intrapartum, this.appTranslation.getKey("patient", "intrapartum")),
  new TCEnum(death_condition.Postpartum,this.appTranslation.getKey("patient", "postpartum")),
];
  searchDeath(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DeathSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("deaths", this.deathSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<DeathSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.deathSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "deaths/do", new TCDoParam("download_all", this.filters()));
  }

  deathDashboard(): Observable<DeathDashboard> {
    return this.http.get<DeathDashboard>(environment.tcApiBaseUri + "deaths/dashboard");
  }

  getDeath(id: string): Observable<DeathDetail> {
    return this.http.get<DeathDetail>(environment.tcApiBaseUri + "deaths/" + id);
  }

  addDeath(parent_id :string,item: DeathDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri +'form_encounter/' +
    parent_id + "/deaths", item);
  }

  updateDeath(item: DeathDetail): Observable<DeathDetail> {
    return this.http.patch<DeathDetail>(environment.tcApiBaseUri + "deaths/" + item.id, item);
  }

  deleteDeath(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "deaths/" + id);
  }

  deathsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "deaths/do", new TCDoParam(method, payload));
  }

  deathDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "deaths/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "deaths/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "deaths/" + id + "/do", new TCDoParam("print", {}));
  }


}
