import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverySummaryDetailComponent } from './delivery-summary-detail.component';

describe('DeliverySummaryDetailComponent', () => {
  let component: DeliverySummaryDetailComponent;
  let fixture: ComponentFixture<DeliverySummaryDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverySummaryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverySummaryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
