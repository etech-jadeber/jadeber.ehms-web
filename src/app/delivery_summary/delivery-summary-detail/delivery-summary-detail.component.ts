import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { DeliverySummaryDetail } from '../delivery_summary.model';
import { DeliverySummaryPersist } from '../delivery_summary.persist';
import { DeliverySummaryNavigator } from '../delivery_summary.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { encounter_status, tabs } from 'src/app/app.enums';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';

export enum DeliverySummaryTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-delivery_summary-detail',
  templateUrl: './delivery-summary-detail.component.html',
  styleUrls: ['./delivery-summary-detail.component.css'],
})
export class DeliverySummaryDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  deliverySummaryIsLoading: boolean = false;

  // DeliverySummaryTabs: typeof DeliverySummaryTabs = DeliverySummaryTabs;
  // activeTab: DeliverySummaryTabs = DeliverySummaryTabs.overview;
  //basics
  DeliverySummaryTabs: typeof DeliverySummaryTabs = DeliverySummaryTabs;
  activeTab: DeliverySummaryTabs = DeliverySummaryTabs.overview;

  deliverySummaryDetail: DeliverySummaryDetail;
  doctorName: string;
  canEdit:boolean;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public form_encounterPersist: Form_EncounterPersist,
    public deliverySummaryNavigator: DeliverySummaryNavigator,
    public deliverySummaryPersist: DeliverySummaryPersist,
    public doctorPersist: DoctorPersist,
    public tcUtilsDate: TCUtilsDate,
    public menuState: MenuState
  ) {
    this.tcAuthorization.requireRead('delivery_summarys');
    this.deliverySummaryDetail = new DeliverySummaryDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('delivery_summarys');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.deliverySummaryIsLoading = true;
    this.deliverySummaryPersist.getDeliverySummary(id).subscribe(
      (deliverySummaryDetail) => {
        this.deliverySummaryDetail = deliverySummaryDetail;
          if(this.deliverySummaryDetail.anc_history)
          this.form_encounterPersist.getForm_Encounter(deliverySummaryDetail.anc_history).subscribe((encounter)=>{
            if(encounter){
              this.canEdit = encounter.status == encounter_status.opened;
            }
          })
        this.menuState.getDataResponse(this.deliverySummaryDetail);
        this.doctorPersist
          .getDoctor(deliverySummaryDetail.delivered_by)
          .subscribe((doctor) => {
            console.log('the doctor detail is ', doctor);
            this.doctorName = doctor.first_name + ' ' + doctor.last_name;
          });
        this.deliverySummaryIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.deliverySummaryIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.deliverySummaryDetail.id);
  }
  editDeliverySummary(): void {
    let modalRef = this.deliverySummaryNavigator.editDeliverySummary(
      this.deliverySummaryDetail.id
    );
    modalRef.afterClosed().subscribe(
      (deliverySummaryDetail) => {
        TCUtilsAngular.assign(
          this.deliverySummaryDetail,
          deliverySummaryDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.deliverySummaryPersist
      .print(this.deliverySummaryDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print delivery_summary', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
