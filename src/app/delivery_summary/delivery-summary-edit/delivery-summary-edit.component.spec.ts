import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverySummaryEditComponent } from './delivery-summary-edit.component';

describe('DeliverySummaryEditComponent', () => {
  let component: DeliverySummaryEditComponent;
  let fixture: ComponentFixture<DeliverySummaryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverySummaryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverySummaryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
