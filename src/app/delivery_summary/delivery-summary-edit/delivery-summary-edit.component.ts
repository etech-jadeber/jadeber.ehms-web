import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';
import { MatRadioModule } from '@angular/material/radio';
import { TCIdMode, TCModalModes, TCParentChildIds } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DeliverySummaryDetail } from '../delivery_summary.model';
import { DeliverySummaryPersist } from '../delivery_summary.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { HeatMapCellComponent } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-delivery_summary-edit',
  templateUrl: './delivery-summary-edit.component.html',
  styleUrls: ['./delivery-summary-edit.component.css'],
})
export class DeliverySummaryEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  deliverySummaryDetail: DeliverySummaryDetail;
  deliveryWay: string;
  amtsl: string;
  placenta: string;
  laceration_rep: string;
  newborn: string;
  sb: string;
  eclampsia: string;
  aph: string;
  pph: string;
  prom_sepsis: string;
  obst_prolg_labor: string;
  ruptured_ux: string;
  hiv_couns: boolean;
  hiv_result: boolean;
  hiv_test_accepted: boolean;
  mother_referred: boolean;
  date: Date = new Date();

  delivery_time: string;
  bcg: string;
  polio: string;
  vit_k: string;
  ttc: string;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<DeliverySummaryEditComponent>,
    public persist: DeliverySummaryPersist,
    public form_EncounterPersist: Form_EncounterPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('delivery_summarys');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'delivery_summary';
      this.deliverySummaryDetail = new DeliverySummaryDetail();
      //set necessary defaults
        this.delivery_time = this.getStartTime(new Date());

    } else {
      this.tcAuthorization.requireUpdate('delivery_summarys');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'delivery_summary';
      this.isLoadingResults = true;
      this.persist.getDeliverySummary(this.ids.childId).subscribe(
        (deliverySummaryDetail) => {
          this.deliverySummaryDetail = deliverySummaryDetail;
          this.isLoadingResults = false;
          this.transformDates(false);
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onSbChange():void{
    if(this.deliverySummaryDetail.sb){
      this.deliverySummaryDetail.baby_mother_bonding = false;
    }

  }

  onBabyMotherCahngeChange():void{
    if(!this.deliverySummaryDetail.baby_mother_bonding){
      this.deliverySummaryDetail.sb = "";
    }
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.deliverySummaryDetail.delivery_time = new Date(this.delivery_time).getTime() / 1000;
      this.deliverySummaryDetail.bcg = this.bcg ? new Date(this.bcg).getTime() / 1000 : null; 
      this.deliverySummaryDetail.polio = this.polio ? new Date(this.polio).getTime() / 1000 : null; 
      this.deliverySummaryDetail.vit_k = this.vit_k ? new Date(this.vit_k).getTime() / 1000 : null; 
      this.deliverySummaryDetail.ttc = this.ttc ? new Date(this.ttc).getTime() / 1000 : null;
    } else {
      this.delivery_time=this.getStartTime(this.tcUtilsDate.toDate( this.deliverySummaryDetail.delivery_time));
      this.bcg = this.deliverySummaryDetail.bcg && this.getDate(this.tcUtilsDate.toDate( this.deliverySummaryDetail.bcg));
      this.polio = this.deliverySummaryDetail.polio && this.getDate(this.tcUtilsDate.toDate( this.deliverySummaryDetail.polio));
      this.vit_k = this.deliverySummaryDetail.vit_k &&  this.getDate(this.tcUtilsDate.toDate( this.deliverySummaryDetail.vit_k));
      this.ttc = this.deliverySummaryDetail.ttc && this.getDate(this.tcUtilsDate.toDate( this.deliverySummaryDetail.ttc));
    }
  }


  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }
  getDate(date:Date){
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}`;
  }
  onAdd(): void {
    this.transformDates(true);

    this.isLoadingResults = true;
    this.persist.addDeliverySummary(this.idMode.id, this.deliverySummaryDetail).subscribe(
      (value) => {
        this.tcNotification.success('deliverySummary added');
        this.deliverySummaryDetail.id = value.id;
        this.dialogRef.close(this.deliverySummaryDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateDeliverySummary(this.deliverySummaryDetail).subscribe(
      (value) => {
        this.tcNotification.success('delivery_summary updated');
        this.dialogRef.close(this.deliverySummaryDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.deliverySummaryDetail == null) {
      return false;
    }

    if(this.deliverySummaryDetail.svd == null && this.deliverySummaryDetail.c_seaction == null && this.deliverySummaryDetail.episiotomy == null && this.deliverySummaryDetail.vacuum_forceps == null){
        return false;
    }
    if(this.deliverySummaryDetail.ergometrine == null && this.deliverySummaryDetail.oxytocine == null && this.deliverySummaryDetail.misoprostol == null && this.deliverySummaryDetail.other == null){
      return false;
    }


    if(this.deliverySummaryDetail.completed == null && this.deliverySummaryDetail.cct == null && this.deliverySummaryDetail. mrp == null && this.deliverySummaryDetail.incomplete == null){
      return false;
    }

    // if ( this.deliverySummaryDetail.delivery_way == null || this.deliverySummaryDetail.delivery_way == '') {
    //   return false;
    // }
    // if ( this.deliverySummaryDetail.amtsl == null || this.deliverySummaryDetail.amtsl == '') {
    //   return false;
    // }
    // if ( this.deliverySummaryDetail.placenta == null || this.deliverySummaryDetail.placenta == '') {
    //   return false;
    // }
    // if ( this.deliverySummaryDetail.laceration_rep == null || this.deliverySummaryDetail.laceration_rep == '') {
    //   return false;
    // }
    if ( this.deliverySummaryDetail.newborn == null || this.deliverySummaryDetail.newborn == '') {
      return false;
    }
    if( this.deliverySummaryDetail.is_alive == null || this.deliverySummaryDetail.is_alive == ''){
      return false;
    }
    return true;
  }

}

