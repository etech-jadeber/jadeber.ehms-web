import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverySummaryListComponent } from './delivery-summary-list.component';

describe('DeliverySummaryListComponent', () => {
  let component: DeliverySummaryListComponent;
  let fixture: ComponentFixture<DeliverySummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverySummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverySummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
