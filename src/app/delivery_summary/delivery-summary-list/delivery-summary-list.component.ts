import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DeliverySummarySummary,
  DeliverySummarySummaryPartialList,
} from '../delivery_summary.model';
import { DeliverySummaryPersist } from '../delivery_summary.persist';
import { DeliverySummaryNavigator } from '../delivery_summary.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-delivery_summary-list',
  templateUrl: './delivery-summary-list.component.html',
  styleUrls: ['./delivery-summary-list.component.css'],
})
export class DeliverySummaryListComponent implements OnInit {
  deliverySummarysData: DeliverySummarySummary[] = [];
  deliverySummarysTotalCount: number = 0;
  deliverySummarySelectAll: boolean = false;
  deliverySummarySelection: DeliverySummarySummary[] = [];
  doctor_name: string;
  deliverySummarysDisplayedColumns: string[] = [
    'select',
    'action',
    // 'link',
    'delivery_time',
    'delivery_way',
    'amtsl',
    'laceration_rep',
    'newborn',
  ];
  deliverySummarySearchTextBox: FormControl = new FormControl();
  deliverySummaryIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  deliverySummarysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) deliverySummarysSort: MatSort;
  @Input() ancHistoryId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>()
  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public deliverySummaryPersist: DeliverySummaryPersist,
    public deliverySummaryNavigator: DeliverySummaryNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public form_encounterPersist: Form_EncounterPersist,
    public doctorPersist: DoctorPersist
  ) {
    this.tcAuthorization.requireRead('delivery_summarys');
    this.deliverySummarySearchTextBox.setValue(
      deliverySummaryPersist.deliverySummarySearchText
    );
    //delay subsequent keyup events
    this.deliverySummarySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.deliverySummaryPersist.deliverySummarySearchText = value;
        this.searchDelivery_summarys();
      });
  }
  ngOnInit() {
    if (this.patientId){
      this.deliverySummaryPersist.patientId = this.patientId
    } else {
      this.deliverySummaryPersist.encounterId = this.ancHistoryId
    }
    this.deliverySummarysSort.sortChange.subscribe(() => {
      this.deliverySummarysPaginator.pageIndex = 0;
      this.searchDelivery_summarys(true);
    });

    this.deliverySummarysPaginator.page.subscribe(() => {
      this.searchDelivery_summarys(true);
    });
    //start by loading items
    this.searchDelivery_summarys();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.deliverySummaryPersist.encounterId = this.ancHistoryId;
    } else {
    this.deliverySummaryPersist.encounterId = null;
    }
    this.searchDelivery_summarys()
    }

  searchDelivery_summarys(isPagination: boolean = false): void {
    let paginator = this.deliverySummarysPaginator;
    let sorter = this.deliverySummarysSort;

    this.deliverySummaryIsLoading = true;
    this.deliverySummarySelection = [];

    this.deliverySummaryPersist
      .searchDeliverySummary(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DeliverySummarySummaryPartialList) => {
          this.deliverySummarysData = partialList.data;
          this.deliverySummarysData.forEach(delivery => {
            this.doctorPersist.getDoctor(delivery.delivered_by).subscribe(
              doctor => delivery.delivery_name = doctor.first_name + " " + doctor.last_name
            )
          })
          if (partialList.total != -1) {
            this.deliverySummarysTotalCount = partialList.total;
          }
          this.deliverySummaryIsLoading = false;
        },
        (error) => {
          this.deliverySummaryIsLoading = false;
        }
      );
  }
  downloadDeliverySummarys(): void {
    if (this.deliverySummarySelectAll) {
      this.deliverySummaryPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download delivery_summary',
          true
        );
      });
    } else {
      this.deliverySummaryPersist
        .download(this.tcUtilsArray.idsList(this.deliverySummarySelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download delivery_summary',
            true
          );
        });
    }
  }
  addDelivery_summary(): void {
    let dialogRef = this.deliverySummaryNavigator.addDeliverySummary(
      this.ancHistoryId
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchDelivery_summarys();
      }
    });
  }

  editDeliverySummary(item: DeliverySummarySummary) {
    let dialogRef = this.deliverySummaryNavigator.editDeliverySummary(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }


  isActive():boolean{
    return true;
  }


  deleteDeliverySummary(item: DeliverySummarySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('delivery_summary');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.deliverySummaryPersist.deleteDeliverySummary(item.id).subscribe(
          (response) => {
            this.tcNotification.success('delivery_summary deleted');
            this.searchDelivery_summarys();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
