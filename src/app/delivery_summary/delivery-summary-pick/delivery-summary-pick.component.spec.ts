import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverySummaryPickComponent } from './delivery-summary-pick.component';

describe('DeliverySummaryPickComponent', () => {
  let component: DeliverySummaryPickComponent;
  let fixture: ComponentFixture<DeliverySummaryPickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverySummaryPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverySummaryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
