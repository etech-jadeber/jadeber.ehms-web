import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DeliverySummarySummary,
  DeliverySummarySummaryPartialList,
} from '../delivery_summary.model';
import { DeliverySummaryPersist } from '../delivery_summary.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { encounter_status } from 'src/app/app.enums';
@Component({
  selector: 'app-delivery_summary-pick',
  templateUrl: './delivery-summary-pick.component.html',
  styleUrls: ['./delivery-summary-pick.component.css'],
})
export class DeliverySummaryPickComponent implements OnInit {
  deliverySummarysData: DeliverySummarySummary[] = [];
  deliverySummarysTotalCount: number = 0;
  deliverySummarySelectAll: boolean = false;
  deliverySummarySelection: DeliverySummarySummary[] = [];

  deliverySummarysDisplayedColumns: string[] = [
    'select',
    ,
    'delivery_time',
    'delivery_way',
    'AMTSL',
    'placenta',
    'laceration_rep',
    'newborn',
    'apgar_accre',
    'sb',
    'bcg',
    'polio',
    'vit_k',
    'ttc',
    'eclampsia',
    'aph',
    'pph',
    'prom_sepsis',
    'obst_prolg_labor',
    'hiv_couns',
    'hiv_result',
    'hiv_test_accepted',
    'arv_px_mother',
    'arv_px_nb',
    'feeding_option_ebf',
    'rf',
    'mother_referred',
    'remark',
    'delivered_by',
  ];
  deliverySummarySearchTextBox: FormControl = new FormControl();
  deliverySummaryIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  deliverySummarysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) deliverySummarysSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public deliverySummaryPersist: DeliverySummaryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<DeliverySummaryPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('delivery_summarys');
    this.deliverySummarySearchTextBox.setValue(
      deliverySummaryPersist.deliverySummarySearchText
    );
    //delay subsequent keyup events
    this.deliverySummarySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.deliverySummaryPersist.deliverySummarySearchText = value;
        this.searchDelivery_summarys();
      });
  }
  ngOnInit() {
    this.deliverySummarysSort.sortChange.subscribe(() => {
      this.deliverySummarysPaginator.pageIndex = 0;
      this.searchDelivery_summarys(true);
    });

    this.deliverySummarysPaginator.page.subscribe(() => {
      this.searchDelivery_summarys(true);
    });
    //start by loading items
    this.searchDelivery_summarys();
  }

  searchDelivery_summarys(isPagination: boolean = false): void {
    let paginator = this.deliverySummarysPaginator;
    let sorter = this.deliverySummarysSort;

    this.deliverySummaryIsLoading = true;
    this.deliverySummarySelection = [];
    let encounterId ="";
    this.deliverySummaryPersist
      .searchDeliverySummary(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DeliverySummarySummaryPartialList) => {
          this.deliverySummarysData = partialList.data;
          if (partialList.total != -1) {
            this.deliverySummarysTotalCount = partialList.total;
          }
          this.deliverySummaryIsLoading = false;
        },
        (error) => {
          this.deliverySummaryIsLoading = false;
        }
      );
  }
  markOneItem(item: DeliverySummarySummary) {
    if (!this.tcUtilsArray.containsId(this.deliverySummarySelection, item.id)) {
      this.deliverySummarySelection = [];
      this.deliverySummarySelection.push(item);
    } else {
      this.deliverySummarySelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.deliverySummarySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
