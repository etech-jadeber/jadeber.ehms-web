import { TCId } from '../tc/models';
export class DeliverySummarySummary extends TCId {
  delivery_time: number;
  delivery_way: string;
  amtsl: string;
  placenta: string;
  laceration_rep: string;
  newborn: string;
  is_alive: string;
  apgar_accre: string;
  sb: string;
  bcg: number;
  polio: number;
  vit_k: number;
  ttc: number;
  eclampsia: string;
  aph: string;
  pph: string;
  prom_sepsis: string;
  obst_prolg_labor: string;
  hiv_couns: boolean;
  hiv_result: boolean;
  hiv_test_accepted: boolean;
  arv_px_mother: string;
  arv_px_nb: string;
  feeding_option_ebf: string;
  rf: string;
  mother_referred: boolean;
  remark: string;
  delivered_by: string;
  ruptured_ux: string;
  delivery_name: string;
  encounter_id: string;
  anc_history: string;
  baby_mother_bonding: boolean;
  svd: boolean;
  c_seaction: boolean;
  episiotomy: boolean;
  vacuum_forceps: boolean;
  ergometrine: boolean;
  oxytocine: boolean;
  misoprostol: boolean;
  other: boolean;
  completed: boolean;
  cct: boolean;
  mrp: boolean;
  incomplete: boolean;
  fresh: boolean;
  term: boolean;
  mac: boolean;
  preterm: boolean;
  

}
export class DeliverySummarySummaryPartialList {
  data: DeliverySummarySummary[];
  total: number;
}
export class DeliverySummaryDetail extends DeliverySummarySummary {
}

export class DeliverySummaryDashboard {
  total: number;
}
