import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TcDictionary, TCIdMode, TCModalModes, TCParentChildIds } from '../tc/models';

import { DeliverySummaryEditComponent } from './delivery-summary-edit/delivery-summary-edit.component';
import { DeliverySummaryPickComponent } from './delivery-summary-pick/delivery-summary-pick.component';

@Injectable({
  providedIn: 'root',
})
export class DeliverySummaryNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  delivery_summarysUrl(): string {
    return '/delivery_summarys';
  }

  delivery_summaryUrl(id: string): string {
    return '/delivery_summarys/' + id;
  }

  viewDeliverySummarys(): void {
    this.router.navigateByUrl(this.delivery_summarysUrl());
  }

  viewDeliverySummary(id: string): void {
    this.router.navigateByUrl(this.delivery_summaryUrl(id));
  }

  editDeliverySummary(
    id: string,
    parentId: string = null,
    isWizard: boolean = false,
    delivery_time: Date = new Date(),
    bcg: Date = new Date(),
    polio: Date = new Date(),
    vit_k: Date = new Date(),
    ttc: Date = new Date()
  ): MatDialogRef<unknown, any> {
    let params: TcDictionary<string | Date> = new TcDictionary();

    params['mode'] = isWizard
      ? TCModalModes.WIZARD
      : id == null
      ? TCModalModes.NEW
      : TCModalModes.EDIT;
    params['startDate'] = delivery_time;
    params['bcg'] = bcg;
    params['polio'] = polio;
    params['vit_k'] = vit_k;
    params['ttc'] = ttc;
    const dialogRef = this.dialog.open(DeliverySummaryEditComponent, {
      data: new TCParentChildIds(parentId, id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addDeliverySummary(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DeliverySummaryEditComponent, {
      data: new TCIdMode(id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickDeliverySummarys(
    selectOne: boolean = false
  ): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DeliverySummaryPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
