import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  DeliverySummaryDashboard,
  DeliverySummaryDetail,
  DeliverySummarySummaryPartialList,
} from './delivery_summary.model';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class DeliverySummaryPersist {
  deliverySummarySearchText: string = '';
  encounterId: string;
  patientId: string;
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.deliverySummarySearchText;
    //add custom filters
    return fltrs;
  }

  searchDeliverySummary(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<DeliverySummarySummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("delivery_summary",
      this.deliverySummarySearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<DeliverySummarySummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'delivery_summary/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  deliverySummaryDashboard(): Observable<DeliverySummaryDashboard> {
    return this.http.get<DeliverySummaryDashboard>(
      environment.tcApiBaseUri + 'delivery_summary/dashboard'
    );
  }

  getDeliverySummary(id: string): Observable<DeliverySummaryDetail> {
    return this.http.get<DeliverySummaryDetail>(
      environment.tcApiBaseUri + 'delivery_summary/' + id
    );
  }
  addDeliverySummary(parentId: string,item: DeliverySummaryDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'form_encounter/' + parentId + '/delivery_summary',
      item
    );
  }

  updateDeliverySummary(
    item: DeliverySummaryDetail
  ): Observable<DeliverySummaryDetail> {
    return this.http.patch<DeliverySummaryDetail>(
      environment.tcApiBaseUri + 'delivery_summary/' + item.id,
      item
    );
  }

  deleteDeliverySummary(id: string): Observable<{}> {
    return this.http.delete(
      environment.tcApiBaseUri + 'delivery_summary/' + id
    );
  }
  deliverySummarysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'delivery_summary/do',
      new TCDoParam(method, payload)
    );
  }

  deliverySummaryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'delivery_summarys/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'delivery_summary/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'delivery_summary/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
