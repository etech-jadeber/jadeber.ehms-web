import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepartmentSpecialtyDetailComponent } from './department-specialty-detail.component';

describe('DepartmentSpecialtyDetailComponent', () => {
  let component: DepartmentSpecialtyDetailComponent;
  let fixture: ComponentFixture<DepartmentSpecialtyDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentSpecialtyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentSpecialtyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
