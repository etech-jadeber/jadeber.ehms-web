import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Department_SpecialtyDetail} from "../department_specialty.model";
import {Department_SpecialtyPersist} from "../department_specialty.persist";
import {Department_SpecialtyNavigator} from "../department_specialty.navigator";

export enum Department_SpecialtyTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-department_specialty-detail',
  templateUrl: './department-specialty-detail.component.html',
  styleUrls: ['./department-specialty-detail.component.css']
})
export class Department_SpecialtyDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  department_specialtyLoading: boolean = false;

  Department_SpecialtyTabs: typeof Department_SpecialtyTabs = Department_SpecialtyTabs;
  activeTab: Department_SpecialtyTabs = Department_SpecialtyTabs.overview;
  //basics
  department_specialtyDetail: Department_SpecialtyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public jobPersist: JobPersist,
              public appTranslation: AppTranslation,
              public department_specialtyNavigator: Department_SpecialtyNavigator,
              public department_specialtyPersist: Department_SpecialtyPersist) {
    this.tcAuthorization.requireRead("department_specialtys");
    this.department_specialtyDetail = new Department_SpecialtyDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("department_specialtys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.department_specialtyLoading = true;
    this.department_specialtyPersist.getDepartment_Specialty(id).subscribe(department_specialtyDetail => {
      this.department_specialtyDetail = department_specialtyDetail;
      this.department_specialtyLoading = false;
    }, error => {
      console.error(error);
      this.department_specialtyLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.department_specialtyDetail.id);
  }

  editDepartment_Specialty(): void {
    let modalRef = this.department_specialtyNavigator.editDepartment_Specialty(this.department_specialtyDetail.id);
    modalRef.afterClosed().subscribe(modifiedDepartment_SpecialtyDetail => {
      TCUtilsAngular.assign(this.department_specialtyDetail, modifiedDepartment_SpecialtyDetail);
    }, error => {
      console.error(error);
    });
  }

  printDepartment_Specialty(): void {
    this.department_specialtyPersist.print(this.department_specialtyDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print department_specialty", true);
    });
  }

  back(): void {
    this.location.back();
  }

}
