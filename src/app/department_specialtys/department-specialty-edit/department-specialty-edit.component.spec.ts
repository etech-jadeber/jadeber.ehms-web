import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepartmentSpecialtyEditComponent } from './department-specialty-edit.component';

describe('DepartmentSpecialtyEditComponent', () => {
  let component: DepartmentSpecialtyEditComponent;
  let fixture: ComponentFixture<DepartmentSpecialtyEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentSpecialtyEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentSpecialtyEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
