import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Department_SpecialtyDetail} from "../department_specialty.model";
import {Department_SpecialtyPersist} from "../department_specialty.persist";
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { BedroomtypeSummary } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
import { ServiceType, fee_type } from 'src/app/app.enums';
import { FeePersist } from 'src/app/fees/fee.persist';

@Component({
  selector: 'app-department_specialty-edit',
  templateUrl: './department-specialty-edit.component.html',
  styleUrls: ['./department-specialty-edit.component.css']
})
export class Department_SpecialtyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  department_specialtyDetail: Department_SpecialtyDetail;
  pharmacyName: string = "";

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              private bedRoomType:BedroomtypeNavigator,
              private pharmacyNavigator: StoreNavigator,
              private bedRoomTypePersist:BedroomtypePersist,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<Department_SpecialtyEditComponent>,
              public feePersist: FeePersist,
              public persist: Department_SpecialtyPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("department_specialtys");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("employee", "department_specialty ");
      this.department_specialtyDetail = new Department_SpecialtyDetail();
      this.department_specialtyDetail.priceList = {}
      this.department_specialtyDetail.parent_department_id = this.idMode.id;
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("department_specialtys");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("employee", "department_specialty ");
      this.isLoadingResults = true;
      const searchHistory = {...this.feePersist.feeSearchHistory}
      searchHistory.target_id = this.idMode.id;
      searchHistory.fee_type = fee_type.card;
      this.persist.getDepartment_Specialty(this.idMode.id).subscribe(department_specialtyDetail => {
        this.department_specialtyDetail = department_specialtyDetail;
        this.department_specialtyDetail.priceList = {}
        this.feePersist.searchFee(5, 0, 'id', 'asc').subscribe((value) => {
          value.data.forEach(data => {
            this.department_specialtyDetail.priceList[data.target_type] = data.price
          })
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addDepartment_Specialty(this.department_specialtyDetail).subscribe(value => {
      this.tcNotification.success("Department_Specialty added");
      this.department_specialtyDetail.id = value.id;
      this.dialogRef.close(this.department_specialtyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDepartment_Specialty(this.department_specialtyDetail).subscribe(value => {
      this.tcNotification.success("Department_Specialty updated");
      this.dialogRef.close(this.department_specialtyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.department_specialtyDetail == null) {
      return false;
    }


    if (this.department_specialtyDetail.name == null || this.department_specialtyDetail.name == "") {
      return false;
    }


    return true;
  }


  searchPharmacy(): void {
    this.pharmacyNavigator.pickPharmacies().afterClosed().subscribe(
      (pharmacies: StoreSummary[])=> {
        this.department_specialtyDetail.store_id = pharmacies[0].id;
        this.pharmacyName = pharmacies[0].name;
      }
    )
  }

}
