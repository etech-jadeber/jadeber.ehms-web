import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepartmentSpecialtyListComponent } from './department-specialty-list.component';

describe('DepartmentSpecialtyListComponent', () => {
  let component: DepartmentSpecialtyListComponent;
  let fixture: ComponentFixture<DepartmentSpecialtyListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentSpecialtyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentSpecialtyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
