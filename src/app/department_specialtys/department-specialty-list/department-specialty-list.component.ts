import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Department_SpecialtyPersist} from "../department_specialty.persist";
import {Department_SpecialtyNavigator} from "../department_specialty.navigator";
import {Department_SpecialtySummary, Department_SpecialtySummaryPartialList} from "../department_specialty.model";
import { StorePersist } from 'src/app/stores/store.persist';
import { StoreSummary } from 'src/app/stores/store.model';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-department_specialty-list',
  templateUrl: './department-specialty-list.component.html',
  styleUrls: ['./department-specialty-list.component.css']
})
export class Department_SpecialtyListComponent implements OnInit, AfterViewInit {

  department_specialtysData: Department_SpecialtySummary[] = [];
  department_specialtysTotalCount: number = 0;
  department_specialtysSelectAll: boolean = false;
  department_specialtysSelection: Department_SpecialtySummary[] = [];

  department_specialtysDisplayedColumns: string[] = ["select", "action", "name", "price"];
  department_specialtysSearchTextBox: FormControl = new FormControl();
  department_specialtysIsLoading: boolean = false;
  stores: StoreSummary[] = [];

  @Input() parent_department_id:string = this.tcUtilsString.invalid_id;
  @ViewChild(MatPaginator, {static: true}) department_specialtysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) department_specialtysSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public department_specialtyPersist: Department_SpecialtyPersist,
              public department_specialtyNavigator: Department_SpecialtyNavigator,
              public jobPersist: JobPersist,
              public tcUtilsString: TCUtilsString
  ) {

    this.tcAuthorization.requireRead("department_specialtys");
    this.department_specialtysSearchTextBox.setValue(department_specialtyPersist.department_specialtySearchText);
    //delay subsequent keyup events
    this.department_specialtysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.department_specialtyPersist.department_specialtySearchText = value;
      this.searchDepartment_Specialtys();
    });
  }

  ngOnInit() {
    this.department_specialtyPersist.parent_department_id = this.parent_department_id
  }

  ngOnDestroy(){
    this.department_specialtyPersist.parent_department_id = null;
  }


  ngAfterViewInit(): void {

    this.department_specialtysSort.sortChange.subscribe(() => {
      this.department_specialtysPaginator.pageIndex = 0;
      this.searchDepartment_Specialtys(true);
    });

    this.department_specialtysPaginator.page.subscribe(() => {
      this.searchDepartment_Specialtys(true);
    });
    //start by loading items
    this.searchDepartment_Specialtys();
  }

  searchDepartment_Specialtys(isPagination: boolean = false): void {


    let paginator = this.department_specialtysPaginator;
    let sorter = this.department_specialtysSort;

    this.department_specialtysIsLoading = true;
    this.department_specialtysSelection = [];

    this.department_specialtyPersist.searchDepartment_Specialty(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: Department_SpecialtySummaryPartialList) => {
      this.department_specialtysData = partialList.data;
      console.log("the department specialities data are", this.department_specialtysData);
      if (partialList.total != -1) {
        this.department_specialtysTotalCount = partialList.total;
      }
      this.department_specialtysIsLoading = false;
    }, error => {
      this.department_specialtysIsLoading = false;
    });

  }

  downloadDepartment_Specialtys(): void {
    if (this.department_specialtysSelectAll) {
      this.department_specialtyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download department_specialtys", true);
      });
    } else {
      this.department_specialtyPersist.download(this.tcUtilsArray.idsList(this.department_specialtysSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download department_specialtys", true);
      });
    }
  }


  addDepartment_Specialty(): void {
    let dialogRef = this.department_specialtyNavigator.addDepartment_Specialty(this.parent_department_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDepartment_Specialtys();
      }
    });
  }

  editDepartment_Specialty(item: Department_SpecialtySummary) {
    let dialogRef = this.department_specialtyNavigator.editDepartment_Specialty(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDepartment_Specialty(item: Department_SpecialtySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Department_Specialty");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.department_specialtyPersist.deleteDepartment_Specialty(item.id).subscribe(response => {
          this.tcNotification.success("Department_Specialty deleted");
          this.searchDepartment_Specialtys();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

}
