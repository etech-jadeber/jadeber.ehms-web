import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Department_SpecialtyPickComponent } from './department-specialty-pick.component';

describe('Department_SpecialtyPickComponent', () => {
  let component: Department_SpecialtyPickComponent;
  let fixture: ComponentFixture<Department_SpecialtyPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Department_SpecialtyPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Department_SpecialtyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
