import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Department_SpecialtySummary, Department_SpecialtySummaryPartialList} from "../department_specialty.model";
import {Department_SpecialtyPersist} from "../department_specialty.persist";
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { DepartmentSummary } from 'src/app/departments/department.model';


@Component({
  selector: 'app-department_specialty-pick',
  templateUrl: './department-specialty-pick.component.html',
  styleUrls: ['./department-specialty-pick.component.css']
})
export class Department_SpecialtyPickComponent implements OnInit {
  selectOne:boolean = false;
  department_specialtysData: Department_SpecialtySummary[] = [];
  department_specialtysTotalCount: number = 0;
  department_specialtysSelection: Department_SpecialtySummary[] = [];
  department_specialtysDisplayedColumns: string[] = ["select", 'name'];

  department_specialtysSearchTextBox: FormControl = new FormControl();
  department_specialtysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) department_specialtysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) department_specialtysSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public department_specialtyPersist: Department_SpecialtyPersist,
              public departmentPersist: DepartmentPersist,
              public dialogRef: MatDialogRef<Department_SpecialtyPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { selectOne: boolean, parent_id: string }
  ) {
    this.tcAuthorization.requireRead("department_specialtys");
    this.department_specialtysSearchTextBox.setValue(department_specialtyPersist.department_specialtySearchText);
    //delay subsequent keyup events
    this.department_specialtysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.department_specialtyPersist.department_specialtySearchText = value;
      this.searchDepartment_Specialtys();
    });
  }

  ngOnInit() {

    this.department_specialtyPersist.parent_department_id = this.data.parent_id
    this.department_specialtysSort.sortChange.subscribe(() => {
      this.department_specialtysPaginator.pageIndex = 0;
      this.searchDepartment_Specialtys();
    });

    this.department_specialtysPaginator.page.subscribe(() => {
      this.searchDepartment_Specialtys();
    });

    //set initial picker list to 5
    this.department_specialtysPaginator.pageSize = 5;

    //start by loading items
    this.searchDepartment_Specialtys();
  }

  searchDepartment_Specialtys(): void {
    this.department_specialtysIsLoading = true;
    this.department_specialtysSelection = [];

    this.department_specialtyPersist.searchDepartment_Specialty(this.department_specialtysPaginator.pageSize,
      this.department_specialtysPaginator.pageIndex,
      this.department_specialtysSort.active,
      this.department_specialtysSort.direction).subscribe((partialList: Department_SpecialtySummaryPartialList) => {
      this.department_specialtysData = partialList.data;
      if (partialList.total != -1) {
        this.department_specialtysTotalCount = partialList.total;
      }
      this.department_specialtysIsLoading = false;
    }, error => {
      this.department_specialtysIsLoading = false;
    });

  }

  markOneItem(item: Department_SpecialtySummary) {
    if (!this.tcUtilsArray.containsId(this.department_specialtysSelection, item.id)) {
      this.department_specialtysSelection = [];
      this.department_specialtysSelection.push(item);
    } else {
      this.department_specialtysSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.department_specialtysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
