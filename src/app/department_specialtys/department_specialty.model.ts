import { schedule_type } from "../app.enums";
import {TCId} from "../tc/models";
import { TCUtilsString } from "../tc/utils-string";

export class Department_SpecialtySummary extends TCId {
  name : string;
  // store_id: string=TCUtilsString.getInvalidId();
  parent_department_id: string;
  priceList: any;
}

export class Department_SpecialtySummaryPartialList {
  data: Department_SpecialtySummary[];
  total: number;
}

export class Department_SpecialtyDetail extends Department_SpecialtySummary {
  name : string;
  department_id : string;
  store_id: string;
}

export class Department_SpecialtyDashboard {
  total: number;
}
