import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog,MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Department_SpecialtyEditComponent} from "./department-specialty-edit/department-specialty-edit.component";
import {Department_SpecialtyPickComponent} from "./department-specialty-pick/department-specialty-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Department_SpecialtyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  department_specialtysUrl(): string {
    return "/department_specialtys";
  }

  department_specialtyUrl(id: string): string {
    return "/department_specialtys/" + id;
  }

  viewDepartment_Specialtys(): void {
    this.router.navigateByUrl(this.department_specialtysUrl());
  }

  viewDepartment_Specialty(id: string): void {
    this.router.navigateByUrl(this.department_specialtyUrl(id));
  }

  editDepartment_Specialty(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Department_SpecialtyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDepartment_Specialty(parent_id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Department_SpecialtyEditComponent, {
      data: new TCIdMode(parent_id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickDepartment_Specialtys(selectOne: boolean = false, parent_id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Department_SpecialtyPickComponent, {
      data: {selectOne, parent_id},
      width: TCModalWidths.large
    });
    return dialogRef;
  }
  
  pick_all_department_Specialtys(selectOne: boolean = false): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Department_SpecialtyPickComponent, {
      data: {selectOne, department_id: null},
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  pickDepartmentSpecialtys(selectOne: boolean = false, department_id: string[]): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Department_SpecialtyPickComponent, {
      data: {selectOne, department_id},
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
