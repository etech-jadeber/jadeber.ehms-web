import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TcDictionary, TCEnum, TCId} from "../tc/models";
import {
  Department_SpecialtyDashboard,
  Department_SpecialtyDetail,
  Department_SpecialtySummaryPartialList
} from "./department_specialty.model";
import {TCUtilsString} from "../tc/utils-string";
import { Department } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class Department_SpecialtyPersist {

  department_specialtySearchText: string = "";

  parent_department_id: string;

  Department: TCEnum[] = [
   new TCEnum( Department.outpatient, 'outpatient'),
new TCEnum( Department.emergency, 'emergency'),
new TCEnum( Department.inpatient, 'inpatient'),

];


  constructor(private http: HttpClient) {
  }

  searchDepartment_Specialty(pageSize: number, pageIndex: number, sort: string, order: string): Observable<Department_SpecialtySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("department_specialtys", this.department_specialtySearchText, pageSize, pageIndex, sort, order);
    if (this.parent_department_id){
      url = TCUtilsString.appendUrlParameter(url, "parent_department_id", this.parent_department_id)
    }

    return this.http.get<Department_SpecialtySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.department_specialtySearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "department_specialtys/do", new TCDoParam("download_all", this.filters()));
  }

  department_specialtyDashboard(): Observable<Department_SpecialtyDashboard> {
    return this.http.get<Department_SpecialtyDashboard>(environment.tcApiBaseUri + "department_specialtys/dashboard");
  }

  getDepartment_Specialty(id: string): Observable<Department_SpecialtyDetail> {
    return this.http.get<Department_SpecialtyDetail>(environment.tcApiBaseUri + "department_specialtys/" + id);
  }

  addDepartment_Specialty(item: Department_SpecialtyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "department_specialtys/", item);
  }

  updateDepartment_Specialty(item: Department_SpecialtyDetail): Observable<Department_SpecialtyDetail> {
    return this.http.patch<Department_SpecialtyDetail>(environment.tcApiBaseUri + "department_specialtys/" + item.id, item);
  }

  deleteDepartment_Specialty(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "department_specialtys/" + id);
  }

  department_specialtysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "department_specialtys/do", new TCDoParam(method, payload));
  }

  department_specialtyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "department_specialtys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "department_specialtys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "department_specialtys/" + id + "/do", new TCDoParam("print", {}));
  }


}
