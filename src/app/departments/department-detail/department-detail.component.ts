import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DepartmentDetail} from "../department.model";
import {DepartmentPersist} from "../department.persist";
import {DepartmentNavigator} from "../department.navigator";

export enum DepartmentTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-department-detail',
  templateUrl: './department-detail.component.html',
  styleUrls: ['./department-detail.component.css']
})
export class DepartmentDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  departmentLoading:boolean = false;

  DepartmentTabs: typeof DepartmentTabs = DepartmentTabs;
  activeTab: DepartmentTabs = DepartmentTabs.overview;
  //basics
  departmentDetail: DepartmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public departmentNavigator: DepartmentNavigator,
              public  departmentPersist: DepartmentPersist) {
    this.tcAuthorization.requireRead("departments");
    this.departmentDetail = new DepartmentDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("departments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.departmentLoading = true;
    this.departmentPersist.getDepartment(id).subscribe(departmentDetail => {
      this.departmentDetail = departmentDetail;
      this.departmentLoading = false;
    }, error => {
      console.error(error);
      this.departmentLoading = false;
    });
  }

  reload(){
    this.loadDetails(this.departmentDetail.id);
  }

  editDepartment(): void {
    let modalRef = this.departmentNavigator.editDepartment(this.departmentDetail.id);
    modalRef.afterClosed().subscribe(modifiedDepartmentDetail => {
      TCUtilsAngular.assign(this.departmentDetail, modifiedDepartmentDetail);
    }, error => {
      console.error(error);
    });
  }

  printDepartment():void{
    this.departmentPersist.print(this.departmentDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print department", true);
    });
  }

  back():void{
    this.location.back();
  }

}
