import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {DepartmentDetail} from "../department.model";
import {DepartmentPersist} from "../department.persist";


@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  departmentDetail: DepartmentDetail;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<DepartmentEditComponent>,
              public persist: DepartmentPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("departments");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("employee", "departments");
      this.departmentDetail = new DepartmentDetail();
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("departments");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("employee", "departments");
      this.isLoadingResults = true;
      this.persist.getDepartment(this.idMode.id).subscribe(departmentDetail => {
        this.departmentDetail = departmentDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addDepartment(this.departmentDetail).subscribe(value => {
      this.tcNotification.success("Department added");
      this.departmentDetail.id = value.id;
      this.dialogRef.close(this.departmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDepartment(this.departmentDetail).subscribe(value => {
      this.tcNotification.success("Department updated");
      this.dialogRef.close(this.departmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.departmentDetail == null) {
      return false;
    }

    if (this.departmentDetail.name == null || this.departmentDetail.name == "") {
      return false;
    }


    return true;
  }


}
