import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {DepartmentPersist} from "../department.persist";
import {DepartmentNavigator} from "../department.navigator";
import {DepartmentSummary, DepartmentSummaryPartialList} from "../department.model";


@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departmentsData: DepartmentSummary[] = [];
  departmentsTotalCount: number = 0;
  departmentsSelectAll: boolean = false;
  departmentsSelection: DepartmentSummary[] = [];

  departmentsDisplayedColumns: string[] = ["select", "action", "name","type"];
  departmentsSearchTextBox: FormControl = new FormControl();
  departmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) departmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) departmentsSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public departmentPersist: DepartmentPersist,
              public departmentNavigator: DepartmentNavigator,
              public jobPersist: JobPersist,
  ) {

    this.tcAuthorization.requireRead("departments");
    this.departmentsSearchTextBox.setValue(departmentPersist.departmentSearchText);
    //delay subsequent keyup events
    this.departmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.departmentPersist.departmentSearchText = value;
      this.searchDepartments();
    });
  }

  ngOnInit() {

    this.departmentsSort.sortChange.subscribe(() => {
      this.departmentsPaginator.pageIndex = 0;
      this.searchDepartments(true);
    });

    this.departmentsPaginator.page.subscribe(() => {
      this.searchDepartments(true);
    });
    //start by loading items
    this.searchDepartments();

   
  }

  searchDepartments(isPagination: boolean = false): void {


    let paginator = this.departmentsPaginator;
    let sorter = this.departmentsSort;

    this.departmentsIsLoading = true;
    this.departmentsSelection = [];
    this.departmentPersist.searchDepartment(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: DepartmentSummaryPartialList) => {
      this.departmentsData = partialList.data;
      if (partialList.total != -1) {
        this.departmentsTotalCount = partialList.total;
      }
      this.departmentsIsLoading = false;
    }, error => {
      this.departmentsIsLoading = false;
    });

  }

  downloadDepartments(): void {
    if (this.departmentsSelectAll) {
      this.departmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download departments", true);
      });
    } else {
      this.departmentPersist.download(this.tcUtilsArray.idsList(this.departmentsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download departments", true);
      });
    }
  }


  addDepartment(): void {
    let dialogRef = this.departmentNavigator.addDepartment();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDepartments();
      }
    });
  }

  editDepartment(item: DepartmentSummary) {
    let dialogRef = this.departmentNavigator.editDepartment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDepartment(item: DepartmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Department");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.departmentPersist.deleteDepartment(item.id).subscribe(response => {
          this.tcNotification.success("Department deleted");
          this.searchDepartments();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

}
