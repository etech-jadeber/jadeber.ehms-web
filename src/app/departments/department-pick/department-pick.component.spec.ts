import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepartmentPickComponent } from './department-pick.component';

describe('DepartmentPickComponent', () => {
  let component: DepartmentPickComponent;
  let fixture: ComponentFixture<DepartmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
