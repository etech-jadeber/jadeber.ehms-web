import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {DepartmentSummary, DepartmentSummaryPartialList} from "../department.model";
import {DepartmentPersist} from "../department.persist";


@Component({
  selector: 'app-department-pick',
  templateUrl: './department-pick.component.html',
  styleUrls: ['./department-pick.component.css']
})
export class DepartmentPickComponent implements OnInit {

  departmentsData: DepartmentSummary[] = [];
  departmentsTotalCount: number = 0;
  departmentsSelection: DepartmentSummary[] = [];
  departmentsDisplayedColumns: string[] = ["select", 'name'];

  departmentsSearchTextBox: FormControl = new FormControl();
  departmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) departmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) departmentsSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public departmentPersist: DepartmentPersist,
              public dialogRef: MatDialogRef<DepartmentPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("departments");
    this.departmentsSearchTextBox.setValue(departmentPersist.departmentSearchText);
    //delay subsequent keyup events
    this.departmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.departmentPersist.departmentSearchText = value;
      this.searchDepartments();
    });
  }

  ngOnInit() {

    this.departmentsSort.sortChange.subscribe(() => {
      this.departmentsPaginator.pageIndex = 0;
      this.searchDepartments();
    });

    this.departmentsPaginator.page.subscribe(() => {
      this.searchDepartments();
    });

    //set initial picker list to 5
    this.departmentsPaginator.pageSize = 5;

    //start by loading items
    this.searchDepartments();
  }

  searchDepartments(): void {
    this.departmentsIsLoading = true;
    this.departmentsSelection = [];

    this.departmentPersist.searchDepartment(this.departmentsPaginator.pageSize,
      this.departmentsPaginator.pageIndex,
      this.departmentsSort.active,
      this.departmentsSort.direction).subscribe((partialList: DepartmentSummaryPartialList) => {
      this.departmentsData = partialList.data;
      if (partialList.total != -1) {
        this.departmentsTotalCount = partialList.total;
      }
      this.departmentsIsLoading = false;
    }, error => {
      this.departmentsIsLoading = false;
    });

  }

  markOneItem(item: DepartmentSummary) {
    if (!this.tcUtilsArray.containsId(this.departmentsSelection, item.id)) {
      this.departmentsSelection = [];
      this.departmentsSelection.push(item);
    } else {
      this.departmentsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.departmentsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
