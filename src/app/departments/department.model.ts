import {TCId} from "../tc/models";

export class DepartmentSummary extends TCId {
  name : string;
  type:number;
}

export class DepartmentSummaryPartialList {
  data: DepartmentSummary[];
  total: number;
}

export class DepartmentDetail extends DepartmentSummary {
}

export class DepartmentDashboard {
  total: number;
}
