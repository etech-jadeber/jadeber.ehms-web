import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DepartmentEditComponent} from "./department-edit/department-edit.component";
import {DepartmentPickComponent} from "./department-pick/department-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DepartmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  departmentsUrl(): string {
    return "/departments";
  }

  departmentUrl(id: string): string {
    return "/departments/" + id;
  }

  viewDepartments(): void {
    this.router.navigateByUrl(this.departmentsUrl());
  }

  viewDepartment(id: string): void {
    this.router.navigateByUrl(this.departmentUrl(id));
  }

  editDepartment(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DepartmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDepartment(): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DepartmentEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickDepartments(selectOne: boolean=false): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DepartmentPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
