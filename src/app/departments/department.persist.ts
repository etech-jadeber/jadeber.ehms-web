import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary,TCEnumTranslation} from "../tc/models";
import {DepartmentDashboard, DepartmentDetail, DepartmentSummaryPartialList} from "./department.model";
import { ServiceType } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class DepartmentPersist {

  departmentSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchDepartment(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DepartmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("departments", this.departmentSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<DepartmentSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.departmentSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/do", new TCDoParam("download_all", this.filters()));
  }

  departmentDashboard(): Observable<DepartmentDashboard> {
    return this.http.get<DepartmentDashboard>(environment.tcApiBaseUri + "departments/dashboard");
  }

  getDepartment(id: string): Observable<DepartmentDetail> {
    return this.http.get<DepartmentDetail>(environment.tcApiBaseUri + "departments/" + id);
  }

  addDepartment(item: DepartmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/", item);
  }

  updateDepartment(item: DepartmentDetail): Observable<DepartmentDetail> {
    return this.http.patch<DepartmentDetail>(environment.tcApiBaseUri + "departments/" + item.id, item);
  }

  deleteDepartment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "departments/" + id);
  }

  departmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "departments/do", new TCDoParam(method, payload));
  }

  departmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "departments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/" + id + "/do", new TCDoParam("print", {}));
  }




  departmentType: TCEnumTranslation[] = [
    new TCEnumTranslation(ServiceType.inpatient,'InPatient'),
    new TCEnumTranslation(ServiceType.outpatient, 'OutPatient'),
    new TCEnumTranslation(ServiceType.emergency, 'Emergency'),
 
  ];


}
