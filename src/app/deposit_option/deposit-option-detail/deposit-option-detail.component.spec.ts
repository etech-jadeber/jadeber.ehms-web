import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOptionDetailComponent } from './deposit-option-detail.component';

describe('DepositOptionDetailComponent', () => {
  let component: DepositOptionDetailComponent;
  let fixture: ComponentFixture<DepositOptionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositOptionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOptionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
