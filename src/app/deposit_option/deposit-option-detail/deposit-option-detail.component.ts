import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import {  MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { DepositOptionDetail } from '../deposit_option.model';
import { DepositOptionPersist } from '../deposit_option.persist';
import { DepositOptionNavigator } from '../deposit_option.navigator';

export enum DepositOptionTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-deposit_option-detail',
  templateUrl: './deposit-option-detail.component.html',
  styleUrls: ['./deposit-option-detail.component.scss'],
})
export class DepositOptionDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  depositOptionIsLoading: boolean = false;

  DepositOptionTabs: typeof DepositOptionTabs = DepositOptionTabs;
  activeTab: DepositOptionTabs = DepositOptionTabs.overview;
  //basics
  depositOptionDetail: DepositOptionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public depositOptionNavigator: DepositOptionNavigator,
    public depositOptionPersist: DepositOptionPersist
  ) {
    this.tcAuthorization.requireRead('deposit_options');
    this.depositOptionDetail = new DepositOptionDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('deposit_options');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.depositOptionIsLoading = true;
    this.depositOptionPersist.getDepositOption(id).subscribe(
      (depositOptionDetail) => {
        this.depositOptionDetail = depositOptionDetail;
        this.depositOptionIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.depositOptionIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.depositOptionDetail.id);
  }
  editDepositOption(): void {
    let modalRef = this.depositOptionNavigator.editDepositOption(
      this.depositOptionDetail.id
    );
    modalRef.afterClosed().subscribe(
      (depositOptionDetail) => {
        TCUtilsAngular.assign(this.depositOptionDetail, depositOptionDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.depositOptionPersist
      .print(this.depositOptionDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print deposit_option', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
