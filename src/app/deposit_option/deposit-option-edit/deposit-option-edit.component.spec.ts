import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOptionEditComponent } from './deposit-option-edit.component';

describe('DepositOptionEditComponent', () => {
  let component: DepositOptionEditComponent;
  let fixture: ComponentFixture<DepositOptionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositOptionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOptionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
