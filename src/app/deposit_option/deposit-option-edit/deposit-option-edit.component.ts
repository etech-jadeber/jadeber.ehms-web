import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DepositOptionDetail } from '../deposit_option.model';
import { DepositOptionPersist } from '../deposit_option.persist';
@Component({
  selector: 'app-deposit_option-edit',
  templateUrl: './deposit-option-edit.component.html',
  styleUrls: ['./deposit-option-edit.component.scss'],
})
export class DepositOptionEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  depositOptionDetail: DepositOptionDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<DepositOptionEditComponent>,
    public persist: DepositOptionPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('deposit_options');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'deposit_option';
      this.depositOptionDetail = new DepositOptionDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('deposit_options');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'deposit_option';
      this.isLoadingResults = true;
      this.persist.getDepositOption(this.idMode.id).subscribe(
        (depositOptionDetail) => {
          this.depositOptionDetail = depositOptionDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addDepositOption(this.depositOptionDetail).subscribe(
      (value) => {
        this.tcNotification.success('depositOption added');
        this.depositOptionDetail.id = value.id;
        this.dialogRef.close(this.depositOptionDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateDepositOption(this.depositOptionDetail).subscribe(
      (value) => {
        this.tcNotification.success('deposit_option updated');
        this.dialogRef.close(this.depositOptionDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.depositOptionDetail == null) {
      return false;
    }

    if (this.depositOptionDetail.option == null) {
      return false;
    }
    return true;
  }
}
