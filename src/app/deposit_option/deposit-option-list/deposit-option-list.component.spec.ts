import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOptionListComponent } from './deposit-option-list.component';

describe('DepositOptionListComponent', () => {
  let component: DepositOptionListComponent;
  let fixture: ComponentFixture<DepositOptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositOptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
