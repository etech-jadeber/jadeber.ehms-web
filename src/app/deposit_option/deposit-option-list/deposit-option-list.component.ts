import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import {  MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DepositOptionSummary, DepositOptionSummaryPartialList } from '../deposit_option.model';
import { DepositOptionPersist } from '../deposit_option.persist';
import { DepositOptionNavigator } from '../deposit_option.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-deposit_option-list',
  templateUrl: './deposit-option-list.component.html',
  styleUrls: ['./deposit-option-list.component.scss']
})export class DepositOptionListComponent implements OnInit {
  depositOptionsData: DepositOptionSummary[] = [];
  depositOptionsTotalCount: number = 0;
  depositOptionSelectAll:boolean = false;
  depositOptionSelection: DepositOptionSummary[] = [];

 depositOptionsDisplayedColumns: string[] = ["select","action" ,"option" ];
  depositOptionSearchTextBox: FormControl = new FormControl();
  depositOptionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) depositOptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) depositOptionsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public depositOptionPersist: DepositOptionPersist,
                public depositOptionNavigator: DepositOptionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("deposit_options");
       this.depositOptionSearchTextBox.setValue(depositOptionPersist.depositOptionSearchText);
      //delay subsequent keyup events
      this.depositOptionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.depositOptionPersist.depositOptionSearchText = value;
        this.searchDepositOptions();
      });
    } ngOnInit() {
   
      this.depositOptionsSort.sortChange.subscribe(() => {
        this.depositOptionsPaginator.pageIndex = 0;
        this.searchDepositOptions(true);
      });

      this.depositOptionsPaginator.page.subscribe(() => {
        this.searchDepositOptions(true);
      });
      //start by loading items
      this.searchDepositOptions();
    }

  searchDepositOptions(isPagination:boolean = false): void {


    let paginator = this.depositOptionsPaginator;
    let sorter = this.depositOptionsSort;

    this.depositOptionIsLoading = true;
    this.depositOptionSelection = [];

    this.depositOptionPersist.searchDepositOption(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DepositOptionSummaryPartialList) => {
      this.depositOptionsData = partialList.data;
      if (partialList.total != -1) {
        this.depositOptionsTotalCount = partialList.total;
      }
      this.depositOptionIsLoading = false;
    }, error => {
      this.depositOptionIsLoading = false;
    });

  } downloadDepositOptions(): void {
    if(this.depositOptionSelectAll){
         this.depositOptionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download deposit_option", true);
      });
    }
    else{
        this.depositOptionPersist.download(this.tcUtilsArray.idsList(this.depositOptionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download deposit_option",true);
            });
        }
  }
addDepositOption(): void {
    let dialogRef = this.depositOptionNavigator.addDepositOption();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDepositOptions();
      }
    });
  }

  editDepositOption(item: DepositOptionSummary) {
    let dialogRef = this.depositOptionNavigator.editDepositOption(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDepositOption(item: DepositOptionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("deposit_option");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.depositOptionPersist.deleteDepositOption(item.id).subscribe(response => {
          this.tcNotification.success("deposit_option deleted");
          this.searchDepositOptions();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}