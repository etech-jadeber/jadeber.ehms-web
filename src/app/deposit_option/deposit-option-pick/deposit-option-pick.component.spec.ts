import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositOptionPickComponent } from './deposit-option-pick.component';

describe('DepositOptionPickComponent', () => {
  let component: DepositOptionPickComponent;
  let fixture: ComponentFixture<DepositOptionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepositOptionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositOptionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
