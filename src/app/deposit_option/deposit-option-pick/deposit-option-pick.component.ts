import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import {  MatSort } from '@angular/material/sort';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DepositOptionSummary,
  DepositOptionSummaryPartialList,
} from '../deposit_option.model';
import { DepositOptionPersist } from '../deposit_option.persist';
import { DepositOptionNavigator } from '../deposit_option.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-deposit_option-pick',
  templateUrl: './deposit-option-pick.component.html',
  styleUrls: ['./deposit-option-pick.component.scss'],
})
export class DepositOptionPickComponent implements OnInit {
  depositOptionsData: DepositOptionSummary[] = [];
  depositOptionsTotalCount: number = 0;
  depositOptionSelectAll: boolean = false;
  depositOptionSelection: DepositOptionSummary[] = [];

  depositOptionsDisplayedColumns: string[] = ['select', , 'option'];
  depositOptionSearchTextBox: FormControl = new FormControl();
  depositOptionIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  depositOptionsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) depositOptionsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public depositOptionPersist: DepositOptionPersist,
    public depositOptionNavigator: DepositOptionNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<DepositOptionPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('deposit_options');
    this.depositOptionSearchTextBox.setValue(
      depositOptionPersist.depositOptionSearchText
    );
    //delay subsequent keyup events
    this.depositOptionSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.depositOptionPersist.depositOptionSearchText = value;
        this.searchDeposit_options();
      });
  }
  ngOnInit() {
    this.depositOptionsSort.sortChange.subscribe(() => {
      this.depositOptionsPaginator.pageIndex = 0;
      this.searchDeposit_options(true);
    });

    this.depositOptionsPaginator.page.subscribe(() => {
      this.searchDeposit_options(true);
    });
    //start by loading items
    this.searchDeposit_options();
  }

  searchDeposit_options(isPagination: boolean = false): void {
    let paginator = this.depositOptionsPaginator;
    let sorter = this.depositOptionsSort;

    this.depositOptionIsLoading = true;
    this.depositOptionSelection = [];

    this.depositOptionPersist
      .searchDepositOption(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DepositOptionSummaryPartialList) => {
          this.depositOptionsData = partialList.data;
          if (partialList.total != -1) {
            this.depositOptionsTotalCount = partialList.total;
          }
          this.depositOptionIsLoading = false;
        },
        (error) => {
          this.depositOptionIsLoading = false;
        }
      );
  }
  markOneItem(item: DepositOptionSummary) {
    if (!this.tcUtilsArray.containsId(this.depositOptionSelection, item.id)) {
      this.depositOptionSelection = [];
      this.depositOptionSelection.push(item);
    } else {
      this.depositOptionSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.depositOptionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
