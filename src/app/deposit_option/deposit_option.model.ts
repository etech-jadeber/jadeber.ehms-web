import { TCId } from '../tc/models';

export class DepositOptionSummary extends TCId {
  option: number;
}
export class DepositOptionSummaryPartialList {
  data: DepositOptionSummary[];
  total: number;
}
export class DepositOptionDetail extends DepositOptionSummary {}

export class DepositOptionDashboard {
  total: number;
}
