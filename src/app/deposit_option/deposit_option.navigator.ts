import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { DepositOptionEditComponent } from './deposit-option-edit/deposit-option-edit.component';
import { DepositOptionPickComponent } from './deposit-option-pick/deposit-option-pick.component';

@Injectable({
  providedIn: 'root',
})
export class DepositOptionNavigator {
    
  constructor(private router: Router, public dialog: MatDialog) {}
  depositOptionsUrl(): string {
    return '/deposit_options';
  }

  depositOptionUrl(id: string): string {
    return '/deposit_options/' + id;
  }

  viewDepositOptions(): void {
    this.router.navigateByUrl(this.depositOptionsUrl());
  }

  viewDepositOption(id: string): void {
    this.router.navigateByUrl(this.depositOptionUrl(id));
  }

  editDepositOption(id: string): MatDialogRef<DepositOptionEditComponent> {
    const dialogRef = this.dialog.open(DepositOptionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addDepositOption(): MatDialogRef<DepositOptionEditComponent> {
    const dialogRef = this.dialog.open(DepositOptionEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickDepositOptions(
    selectOne: boolean = false
  ): MatDialogRef<DepositOptionPickComponent> {
    const dialogRef = this.dialog.open(DepositOptionPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
