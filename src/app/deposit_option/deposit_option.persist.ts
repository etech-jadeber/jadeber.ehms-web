import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  DepositOptionDashboard,
  DepositOptionDetail,
  DepositOptionSummaryPartialList,
} from './deposit_option.model';

@Injectable({
  providedIn: 'root',
})
export class DepositOptionPersist {
  depositOptionSearchText: string = '';
  
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.depositOptionSearchText;
    //add custom filters
    return fltrs;
  }

  searchDepositOption(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<DepositOptionSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'deposit_option',
      this.depositOptionSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<DepositOptionSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'deposit_option/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  depositOptionDashboard(): Observable<DepositOptionDashboard> {
    return this.http.get<DepositOptionDashboard>(
      environment.tcApiBaseUri + 'deposit_option/dashboard'
    );
  }

  getDepositOption(id: string): Observable<DepositOptionDetail> {
    return this.http.get<DepositOptionDetail>(
      environment.tcApiBaseUri + 'deposit_option/' + id
    );
  }
  addDepositOption(item: DepositOptionDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'deposit_option/',
      item
    );
  }

  updateDepositOption(
    item: DepositOptionDetail
  ): Observable<DepositOptionDetail> {
    return this.http.patch<DepositOptionDetail>(
      environment.tcApiBaseUri + 'deposit_option/' + item.id,
      item
    );
  }

  deleteDepositOption(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'deposit_option/' + id);
  }
  depositOptionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'deposit_option/do',
      new TCDoParam(method, payload)
    );
  }

  depositOptionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'deposit_options/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'deposit_option/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'deposit_option/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
