import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { DepositDetail, Deposit_HistoryDetail, Deposit_HistorySummary } from "../deposit.model";
import { DepositPersist } from "../deposit.persist";
import { DepositNavigator } from "../deposit.navigator";
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { deposit_status, tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { TCId } from 'src/app/tc/models';


export enum DepositTabs {
  overview,
  deposit_historys

}

export enum PaginatorIndexes {
  deposit_historys
}


@Component({
  selector: 'app-deposit-detail',
  templateUrl: './deposit-detail.component.html',
  styleUrls: ['./deposit-detail.component.css']
})
export class DepositDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  depositLoading: boolean = false;

  depositTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  patient: PatientSummary[] = [];
  patientSummary: PatientSummary[] = [];
  depositsTotalCount: number = 0;
  //basics
  depositDetail: DepositDetail;
  DepositStatus = deposit_status

  //deposit_historys
  deposit_historysData: Deposit_HistorySummary[] = [];
  deposit_historysTotalCount: number = 0;
  deposit_historysSelectAll: boolean = false;
  deposit_historysSelected: Deposit_HistorySummary[] = [];
  deposit_historysDisplayedColumns: string[] = ['select', 'action', 'patient_id', 'deposit_amount', 'deposit_date', 'crv', 'deposit_type', 'remark'];
  deposit_historysSearchTextBox: FormControl = new FormControl();
  deposit_historysLoading: boolean = false;
  tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
  start_date: FormControl = new FormControl((new Date(Date.now() - this.tzoffset)).toISOString().slice(0, 16))
  end_date: FormControl = new FormControl()

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public depositNavigator: DepositNavigator,
    public depositPersist: DepositPersist,
    public patientPersist: PatientPersist,
    public menuState: MenuState
  ) {
    this.tcAuthorization.requireRead("deposits");
    this.depositDetail = new DepositDetail();

    //deposit_historys filter
    this.deposit_historysSearchTextBox.setValue(depositPersist.depositHistorySearchHistory.search_text);
    this.deposit_historysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.depositPersist.depositHistorySearchHistory.search_text = value.trim();
      this.searchDeposit_Historys();
    });
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
    this.depositPersist.depositHistorySearchHistory.start_date = tcUtilsDate.toTimeStamp(new Date());
                this.start_date.valueChanges.pipe().subscribe(value => {
                  this.depositPersist.depositHistorySearchHistory.start_date = value && this.tcUtilsDate.toTimeStamp(new Date(value))
                  this.searchDeposit_Historys();
                });

                this.end_date.valueChanges.pipe().subscribe(value => {
                  this.depositPersist.depositHistorySearchHistory.end_date = value && this.tcUtilsDate.toTimeStamp(new Date(value))
                  this.searchDeposit_Historys();
                });
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("deposits");
    this.activeTab = tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    // this.depositPersist.deposit_id = null
    // this.depositPersist.start_date = null;
    // this.depositPersist.end_date = null;
    // this.depositPersist.deposit_historySearchText = "";
    // this.depositPersist.isReport = false;
  }

  printDepositDetail(){
    this.depositPersist.depositDo(this.depositDetail.id, "deposit_detail", this.depositPersist.filters()).subscribe((downloadJob: TCId) => {
      this.jobPersist.followJob(downloadJob.id, "download detail", true);
    })
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.depositDetail.id = id;
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });

    // deposit_historys sorter
    this.sorters.toArray()[PaginatorIndexes.deposit_historys].sortChange.subscribe(() => {
      this.paginators.toArray()[PaginatorIndexes.deposit_historys].pageIndex = 0;
      this.searchDeposit_Historys(true);
    });
    // deposit_historys paginator
    this.paginators.toArray()[PaginatorIndexes.deposit_historys].page.subscribe(() => {
      this.searchDeposit_Historys(true);
    });
  }

  loadDetails(id: string): void {
    this.depositLoading = true;
    this.depositPersist.getDeposit(id).subscribe(depositDetail => {
      this.depositDetail = depositDetail;
      this.menuState.getDataResponse(this.depositDetail);
      this.depositPersist.depositHistorySearchHistory.patient_id = this.depositDetail.patient_id;
      this.depositPersist.depositHistorySearchHistory.deposit_id = id;
      this.getPatient(this.depositDetail.patient_id);
      this.searchDeposit_Historys();
      this.depositLoading = false;
    }, error => {
      console.error(error);
      this.depositLoading = false;
      //below this.depositLoading = false;
      this.searchDeposit_Historys();
    });
  }

  getPatient(patient_id: string){
    this.patientPersist.getPatient(patient_id).subscribe((patient) => {
      this.depositDetail.patient_name = patient.fname + " " + patient.mname + " " + patient.lname
    })
  }

  reload() {
    this.loadDetails(this.depositDetail.id);
  }

  editDeposit(): void {
    let modalRef = this.depositNavigator.editDeposit(this.depositDetail.id);
    modalRef.afterClosed().subscribe(modifiedDepositDetail => {
      TCUtilsAngular.assign(this.depositDetail, modifiedDepositDetail);
    }, error => {
      console.error(error);
    });
  }

  printDeposit(): void {
    this.depositPersist.print(this.depositDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print deposit", true);
    });
  }

  //deposit_historys methods
  searchDeposit_Historys(isPagination: boolean = false): void {
    let paginator = this.paginators.toArray()[PaginatorIndexes.deposit_historys];
    let sorter = this.sorters.toArray()[PaginatorIndexes.deposit_historys];
    this.deposit_historysSelected = [];
    this.deposit_historysLoading = true;
    this.depositPersist.searchDeposit_History(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
      this.deposit_historysData = response.data;
      if (response.total != -1) {
        this.deposit_historysTotalCount = response.total;
      }
      this.deposit_historysLoading = false;
    }, error => { this.deposit_historysLoading = false; });
  }


  addDeposit_History(): void {
    let dialogRef = this.depositNavigator.addDeposit_History(this.depositDetail.id);
    dialogRef.afterClosed().subscribe(newDeposit_History => {
      this.loadDetails(this.depositDetail.id);
    });
  }

  editDeposit_History(item: Deposit_HistoryDetail): void {
    let dialogRef = this.depositNavigator.editDeposit_History(this.depositDetail.id, item.id);
    dialogRef.afterClosed().subscribe(updatedDeposit_History => {
      if (updatedDeposit_History) {
        this.searchDeposit_Historys();
      }
    });
  }

  deleteDeposit_History(item: Deposit_HistoryDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Deposit_History");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.depositPersist.deleteDeposit_History(this.depositDetail.id, item.id).subscribe(response => {
          this.tcNotification.success("deposit_history deleted");
          this.searchDeposit_Historys();
        }, error => {
        });
      }

    });
  }

  downloadDeposit_Historys(): void {
    this.tcNotification.info("Download deposit_historys : " + this.deposit_historysSelected.length);
  }

  back(): void {
    this.location.back();
  }

}
