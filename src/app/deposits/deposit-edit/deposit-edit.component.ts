import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCEnum, TCIdMode, TCModalModes} from "../../tc/models";


import {DepositDetail, Deposit_HistoryDetail} from "../deposit.model";
import {DepositPersist} from "../deposit.persist";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PaymentDetail } from 'src/app/payments/payment.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { now } from 'moment';
import { DepositOptionSummary } from 'src/app/deposit_option/deposit_option.model';
import { DepositOptionPersist } from 'src/app/deposit_option/deposit_option.persist';
import { Outside_OrdersNavigator } from 'src/app/outside_orderss/outside_orders.navigator';
import {PaymentType, deposit_patient_type, service_type} from 'src/app/app.enums';
import { BankNavigator } from 'src/app/bank/bank.navigator';
import {OtherServicesPersist} from "../../other_services/other-services.persist";
import {OtherServicesNavigator} from "../../other_services/other-services.navigator";
import {OtherServicesDetail} from "../../other_services/other-services.model";


@Component({
  selector: 'app-deposit-edit',
  templateUrl: './deposit-edit.component.html',
  styleUrls: ['./deposit-edit.component.css']
})
export class DepositEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  depositDetail: DepositDetail;
  patient:PatientSummary[] = [];
  patientFullName = '';
  guardName = '';
  deposit_date :Date = new Date();
  deposit_historyDetail: Deposit_HistoryDetail = new Deposit_HistoryDetail();
  deposit_type: number=0;
  depositOPtions : DepositOptionSummary[];
  bankName: string;
  paymentType = PaymentType
  filteredPaymentType = [new TCEnum( PaymentType.Cash, 'Cash'), new TCEnum( PaymentType.transfer, 'Transfer')]

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DepositEditComponent>,
              public  persist: DepositPersist,
              public patientPersist: PatientPersist,
              public patientNavigator: PatientNavigator,
              public depositPersist: DepositPersist,
              public tcUtilsDate: TCUtilsDate,
              public bankNavigator: BankNavigator,
              public depositOptionPersist: DepositOptionPersist,
              public outsideOrderNavigator: Outside_OrdersNavigator,
              public otherServicePersist: OtherServicesPersist,
              public otherServiceNavigator: OtherServicesNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  searchBank(){
    let dialogRef = this.bankNavigator.pickBanks(true);
    dialogRef.afterClosed().subscribe((value) => {
      if (value.length){
        this.bankName = value[0].name;
        this.depositDetail.bank_id = value[0].id
      }
    })
  }


  ngOnInit() {
    this.loadDepostiOPtions()
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("deposits");
      this.title = this.appTranslation.getText("general","new") +  " " +  this.appTranslation.getText("financial", "deposit");
      this.depositDetail = new DepositDetail();
      //set necessary defaults
      this.deposit_date = this.tcUtilsDate.now();

    } else {
     this.tcAuthorization.requireUpdate("deposits");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("financial", "deposit");
      this.isLoadingResults = true;
      this.persist.getDeposit(this.idMode.id).subscribe(depositDetail => {
        this.depositDetail = depositDetail;
        this.patientPersist.getPatient(depositDetail.patient_id).subscribe(patient => {
          this.patientFullName = patient.fname + " " + patient.mname + " " + patient.lname
        })
        this.tcUtilsString.isValidId(depositDetail.guard_id) && this.otherServicePersist.getOtherServices(depositDetail.guard_id).subscribe(otherService => {
          this.guardName = otherService.name;
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  loadDepostiOPtions():void{
    this.depositOptionPersist.searchDepositOption(200,0,"option",'asc').subscribe((options)=>{
      if (options)
        this.depositOPtions = options.data
    })
  }

  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addDeposit(this.depositDetail).subscribe(value => {
      this.tcNotification.success("Deposit added");
      this.depositDetail.id = value.id;
      this.dialogRef.close(this.depositDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDeposit(this.depositDetail).subscribe(value => {
      this.tcNotification.success("Deposit updated");
      this.dialogRef.close(this.depositDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.depositDetail == null){
            return false;
          }

          if (this.depositDetail.current_amount == null){
            return false;
          }
          if (!this.depositDetail.crv){
            return false;
          }




        return true;
      }


      searchPatient() {
        let dialogRef
        if (this.depositDetail.patient_type == deposit_patient_type.outside_patient){
          dialogRef = this.outsideOrderNavigator.pickOutside_Orderss(true);
        } else {
          dialogRef = this.patientNavigator.pickPatients(true);
        }
        dialogRef.afterClosed().subscribe((result: PatientSummary) => {
          if (result) {
            if (this.depositDetail.patient_type == deposit_patient_type.outside_patient){
              this.patientFullName = result[0].patient_name
            } else {
              this.patientFullName = result[0].fname + ' ' + result[0].lname;
            }
            this.depositDetail.patient_id = result[0].id;
          }
        });
      }

  searchGuard() {
    const dialogRef = this.otherServiceNavigator.pickOtherServicess(service_type.guard, true);
    dialogRef.afterClosed().subscribe((result: OtherServicesDetail[]) => {
      if (result) {
        this.guardName = result[0].name;
        this.depositDetail.guard_id = result[0].id;
      }
    });
  }

      addDeposit_History(depositDetail:DepositDetail, parent_id:string): void {
        this.deposit_historyDetail.deposit_amount = depositDetail.current_amount;
        this.deposit_historyDetail.deposit_date = this.tcUtilsDate.toTimeStamp(this.deposit_date);
        this.deposit_historyDetail.deposit_id = parent_id;
        this.deposit_historyDetail.deposit_type = this.deposit_type;
        this.deposit_historyDetail.patient_id = depositDetail.patient_id;
        this.deposit_historyDetail.remark = depositDetail.remark;

          this.depositPersist.addDeposit_History(this.deposit_historyDetail.deposit_id, this.deposit_historyDetail).subscribe(value => {
            this.tcNotification.success("Deposit_History added");
            this.deposit_historyDetail.id = value.id;
          });
        }


}
