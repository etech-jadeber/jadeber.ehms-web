import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepositHistoryEditComponent } from './deposit-history-edit.component';

describe('DepositHistoryEditComponent', () => {
  let component: DepositHistoryEditComponent;
  let fixture: ComponentFixture<DepositHistoryEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositHistoryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
