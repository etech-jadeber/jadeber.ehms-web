import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCEnum, TCModalModes, TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Deposit_HistoryDetail} from "../deposit.model";
import {DepositPersist} from "../deposit.persist";
import { PaymentType, deposit_history_type } from 'src/app/app.enums';
import { BankNavigator } from 'src/app/bank/bank.navigator';


@Component({
  selector: 'app-deposit-history-edit',
  templateUrl: './deposit-history-edit.component.html',
  styleUrls: ['./deposit-history-edit.component.css']
})
export class Deposit_HistoryEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  deposit_historyDetail: Deposit_HistoryDetail = new Deposit_HistoryDetail();
  bankName: string;
  paymentType = PaymentType
  filteredPaymentType = [new TCEnum( PaymentType.Cash, 'Cash'), new TCEnum( PaymentType.transfer, 'Transfer')]

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Deposit_HistoryEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: DepositPersist,
              public bankNavigator: BankNavigator,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.context['mode'] === TCModalModes.EDIT;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.ids.context['mode'] === TCModalModes.WIZARD;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isWizard()) {
      this.title = "Refund Form";
      //set necessary defaults
      this.deposit_historyDetail.deposit_type = deposit_history_type.take_out;
    }
    else if (this.isNew()) {
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("financial", "deposit_history");
      //set necessary defaults
      this.deposit_historyDetail.deposit_type = deposit_history_type.additional_deposit;
    } else {
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("financial", "deposit_history");
      this.isLoadingResults = true;
      this.persist.getDeposit_History(this.ids.parentId, this.ids.childId).subscribe(deposit_historyDetail => {
        this.deposit_historyDetail = deposit_historyDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }

  searchBank(){
    let dialogRef = this.bankNavigator.pickBanks(true);
    dialogRef.afterClosed().subscribe((value) => {
      if (value.length){
        this.bankName = value[0].name;
        this.deposit_historyDetail.bank_id = value[0].id
      }
    })
  }



  canSubmit():boolean{
      if (this.deposit_historyDetail == null){
          return false;
        }
        
        if (this.deposit_historyDetail.deposit_type == deposit_history_type.additional_deposit && !this.deposit_historyDetail.deposit_amount) {
          return false;
        }

     if (this.deposit_historyDetail.remark == null || this.deposit_historyDetail.remark  == "") {
                      return false;
                    }

                    if (this.deposit_historyDetail.crv == null || this.deposit_historyDetail.crv  == "") {
                      return false;
                    }

                    if (!this.deposit_historyDetail.payment_type) {
                      return false;
                    }

                    if (this.deposit_historyDetail.payment_type == this.paymentType.transfer && !(this.deposit_historyDetail.bank_id && this.deposit_historyDetail.transaction_no)) {
                      return false;
                    }

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addDeposit_History(this.ids.parentId, this.deposit_historyDetail).subscribe(value => {
      this.tcNotification.success("Deposit_History added");
      this.deposit_historyDetail.id = value.id;
      this.dialogRef.close(this.deposit_historyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDeposit_History(this.ids.parentId, this.deposit_historyDetail).subscribe(value => {
      this.tcNotification.success("Deposit_History updated");
      this.dialogRef.close(this.deposit_historyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


}
