import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {DepositPersist} from "../deposit.persist";
import {DepositNavigator} from "../deposit.navigator";
import {DepositDetail, DepositSummary, DepositSummaryPartialList, Deposit_HistoryDetail, depositFilterColumn} from "../deposit.model";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { deposit_history_type, deposit_override_status, deposit_share_status, deposit_status } from 'src/app/app.enums';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { TCId } from 'src/app/tc/models';


@Component({
  selector: 'app-deposit-list',
  templateUrl: './deposit-list.component.html',
  styleUrls: ['./deposit-list.component.css']
})
export class DepositListComponent implements OnInit {

  depositsData: DepositSummary[] = [];
  depositsTotalCount: number = 0;
  depositsSelectAll:boolean = false;
  depositsSelection: DepositSummary[] = [];

  depositsDisplayedColumns: string[] = ["select","action", 'pid', "patient_id", 'phone_cell', "current_amount", "start_date", "end_date", "remark", 'share_status', 'override_status' ];
  depositsSearchTextBox: FormControl = new FormControl();
  depositsIsLoading: boolean = false;
  depositShareStatus = deposit_share_status
  depositOverrideStatus = deposit_override_status

  depositFilterColumn = depositFilterColumn
  patientSummary:PatientSummary[] = [];
  deposit_historyDetail: Deposit_HistoryDetail = new Deposit_HistoryDetail();

  @ViewChild(MatPaginator, {static: true}) depositsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) depositsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public depositPersist: DepositPersist,
                public depositNavigator: DepositNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public paymentPersist: PaymentPersist,
    ) {

        this.tcAuthorization.requireRead("deposits");
       this.depositsSearchTextBox.setValue(depositPersist.depositSearchHistory.search_text);
      //delay subsequent keyup events
      this.depositsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.depositPersist.depositSearchHistory.search_text = value;
        this.searchDeposits();
      });
      this.depositPersist.depositSearchHistory.status = deposit_status.opened;
    }

    ngOnInit() {

      this.depositsSort.sortChange.subscribe(() => {
        this.depositsPaginator.pageIndex = 0;
        this.searchDeposits(true);
      });

      this.depositsPaginator.page.subscribe(() => {
        this.searchDeposits(true);
      });
      //start by loading items
      this.searchDeposits();
    }

    ngOnDestroy() {
      // this.depositPersist.depositSearchText = ''
    }

  searchDeposits(isPagination:boolean = false): void {


    let paginator = this.depositsPaginator;
    let sorter = this.depositsSort;

    this.depositsIsLoading = true;
    this.depositsSelection = [];

    this.depositPersist.searchDeposit(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DepositSummaryPartialList) => {
      this.depositsData = partialList.data;
      // this.depositsData.forEach(deposit => this.getPatient(deposit))
      if (partialList.total != -1) {
        this.depositsTotalCount = partialList.total;
      }
      this.depositsIsLoading = false;
    }, error => {
      this.depositsIsLoading = false;
    });

  }

  printDeposit(item: DepositDetail){
    this.paymentPersist.paymentsDo("print_cnet", {deposit_id: item.id}).subscribe(() => {
      this.searchDeposits()
    })
  }

  printDepositSummary(item: DepositDetail){
    this.depositPersist.depositDo(item.id, "deposit_summary", {}).subscribe((downloadJob: TCId) => {
      this.jobPersist.followJob(downloadJob.id, "download summary", true);
    })
  }

  makeDepositSharable(item: DepositDetail){
    this.depositPersist.depositDo(item.id, "make_sharable", {}).subscribe((downloadJob: TCId) => {
      this.tcNotification.success("The deposit is now sharable", true);
    })
  }

  makeDepositOverridable(item: DepositDetail){
    this.depositPersist.depositDo(item.id, "make_override", {}).subscribe((downloadJob: TCId) => {
      this.tcNotification.success("The deposit is now overidable", true);
    })
  }

  printDepositDetail(item: DepositDetail){
    this.depositPersist.depositDo(item.id, "deposit_detail", {}).subscribe((downloadJob: TCId) => {
      this.jobPersist.followJob(downloadJob.id, "download detail", true);
    })
  }

  // refundDeposit(deposit: DepositDetail){
  //   this.depositsIsLoading = true
  //   const deposit_history = new Deposit_HistoryDetail()
  //   deposit_history.deposit_amount = deposit.current_amount;
  //   deposit_history.deposit_type = deposit_history_type.take_out;
  //   this.depositPersist.addDeposit_History(deposit.id, deposit_history).subscribe((result) => {
  //       this.depositsIsLoading = false;
  //       this.tcNotification.success("Deposit is successuflly refunded")
  //       this.searchDeposits()
  //   },
  //   error => {
  //     console.log(error)
  //     this.depositsIsLoading = false
  //   }
  //   )
  // }

  refundDeposit(deposit: DepositDetail){
    let dialogRef = this.depositNavigator.refundDeposit_History(deposit.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDeposits();
      }
    });
  }

  downloadDeposits(): void {
    if(this.depositsSelectAll){
         this.depositPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download deposits", true);
      });
    }
    else{
        this.depositPersist.download(this.tcUtilsArray.idsList(this.depositsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download deposits",true);
            });
        }
  }



  addDeposit(): void {
    let dialogRef = this.depositNavigator.addDeposit();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDeposits();
      }
    });
  }

  editDeposit(item: DepositSummary) {
    let dialogRef = this.depositNavigator.editDeposit(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDeposit(item: DepositSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Deposit");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.depositPersist.deleteDeposit(item.id).subscribe(response => {
          this.tcNotification.success("Deposit deleted");
          this.searchDeposits();
        }, error => {
        });
      }

    });
  }

  getPatient(deposit: DepositSummary){
    this.patientPersist.getPatient(deposit.patient_id).subscribe((patient) => {
      deposit.patient_name = patient.fname + " " + patient.mname + " " + patient.lname;
  });
}

  

  back():void{
      this.location.back();
    }

}
