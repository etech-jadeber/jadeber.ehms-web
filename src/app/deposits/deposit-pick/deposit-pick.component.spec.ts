import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DepositPickComponent } from './deposit-pick.component';

describe('DepositPickComponent', () => {
  let component: DepositPickComponent;
  let fixture: ComponentFixture<DepositPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
