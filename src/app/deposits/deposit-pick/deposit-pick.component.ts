import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {DepositDetail, DepositSummary, DepositSummaryPartialList} from "../deposit.model";
import {DepositPersist} from "../deposit.persist";


@Component({
  selector: 'app-deposit-pick',
  templateUrl: './deposit-pick.component.html',
  styleUrls: ['./deposit-pick.component.css']
})
export class DepositPickComponent implements OnInit {

  depositsData: DepositSummary[] = [];
  depositsTotalCount: number = 0;
  depositsSelection: DepositSummary[] = [];
  depositsDisplayedColumns: string[] = ["select", 'deposit_date','initial_amount','current_amount ','patient_id','remark' ];

  depositsSearchTextBox: FormControl = new FormControl();
  depositsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) depositsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) depositsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public depositPersist: DepositPersist,
              public dialogRef: MatDialogRef<DepositPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("deposits");
    this.depositsSearchTextBox.setValue(depositPersist.depositSearchHistory.search_text);
    //delay subsequent keyup events
    this.depositsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.depositPersist.depositSearchHistory.search_text = value;
      this.searchDeposits();
    });
  }

  ngOnInit() {

    this.depositsSort.sortChange.subscribe(() => {
      this.depositsPaginator.pageIndex = 0;
      this.searchDeposits();
    });

    this.depositsPaginator.page.subscribe(() => {
      this.searchDeposits();
    });

    //set initial picker list to 5
    this.depositsPaginator.pageSize = 5;

    //start by loading items
    this.searchDeposits();
  }

  searchDeposits(): void {
    this.depositsIsLoading = true;
    this.depositsSelection = [];

    this.depositPersist.searchDeposit(this.depositsPaginator.pageSize,
        this.depositsPaginator.pageIndex,
        this.depositsSort.active,
        this.depositsSort.direction).subscribe((partialList: DepositSummaryPartialList) => {
      this.depositsData = partialList.data;
      if (partialList.total != -1) {
        this.depositsTotalCount = partialList.total;
      }
      this.depositsIsLoading = false;
    }, error => {
      this.depositsIsLoading = false;
    });

  }

  markOneItem(item: DepositSummary) {
    if(!this.tcUtilsArray.containsId(this.depositsSelection,item.id)){
          this.depositsSelection = [];
          this.depositsSelection.push(item);
        }
        else{
          this.depositsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.depositsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
