import {TCId, TCString} from "../tc/models";

export class DepositSummary extends TCId {
  patient_id : string;
  current_amount  : number;
  remark : string;
  patient_name: string;
  deposited: number;
  used: number;
  crv: string;
  patient_type: number;
  share_status: number;
  override_status: number;
  payment_type: number;
bank_id: string;
transaction_no: string;
tin_no: string;
user_id: string;
}

export const depositFilterColumn: TCString[] = [
  new TCString( ".pid", 'MRN'),
  new TCString( ".patient_name", 'Patient Name'),
  new TCString( ".phone_cell", 'Phone Number'),
];


export class DepositSummaryPartialList {
  data: DepositSummary[];
  total: number;
}

export class DepositDetail extends DepositSummary {
  patient_id : string;
  current_amount  : number;
  deposited: number;
  used: number
  remark : string;
  patient_name: string;
  status: number;
  guard_id: string;
}

export class DepositDashboard {
  total: number;
}

//models for deposit history
export class Deposit_HistorySummary extends TCId {
  deposit_amount : number;
patient_id : string;
remark : string;
deposit_date : number;
deposit_type : number;
deposit_id : string;
crv: string;
payment_type: number;
bank_id: string;
transaction_no: string;
tin_no: string;
user_id: string;
}

export class Deposit_HistorySummaryPartialList {
  data: Deposit_HistorySummary[];
  total: number;
}

export class DepositReportList {
  data: [Deposit_HistorySummary[], number];
  total: number
}

export class Deposit_HistoryDetail extends Deposit_HistorySummary {
  deposit_amount : number;
patient_id : string;
remark : string;
deposit_date : number;
deposit_type : number;
deposit_id : string;
}
