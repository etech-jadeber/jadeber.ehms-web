import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog,MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes,TCParentChildIds} from "../tc/models";

import {DepositEditComponent} from "./deposit-edit/deposit-edit.component";
import {DepositPickComponent} from "./deposit-pick/deposit-pick.component";
import { Deposit_HistoryEditComponent } from "./deposit-history-edit/deposit-history-edit.component";


@Injectable({
  providedIn: 'root'
})

export class DepositNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  depositsUrl(): string {
    return "/deposits";
  }

  depositUrl(id: string): string {
    return "/deposits/" + id;
  }

  viewDeposits(): void {
    this.router.navigateByUrl(this.depositsUrl());
  }

  viewDeposit(id: string): void {
    this.router.navigateByUrl(this.depositUrl(id));
  }

  editDeposit(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DepositEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDeposit(): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DepositEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  refundDeposit_History(deposit_id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(Deposit_HistoryEditComponent, {
          data: new TCParentChildIds(deposit_id, null, {mode: TCModalModes.WIZARD}),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickDeposits(selectOne: boolean=false): MatDialogRef<unknown, any> {
      const dialogRef = this.dialog.open(DepositPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }

    //deposit_historys
editDeposit_History(parentId:string, id: string, mode: string = TCModalModes.EDIT): MatDialogRef<unknown, any> {
  const dialogRef = this.dialog.open(Deposit_HistoryEditComponent, {
    data:  new TCParentChildIds(parentId, id, {mode}),
    width: TCModalWidths.medium
  });
  return dialogRef;
}

addDeposit_History(parentId:string, ): MatDialogRef<unknown, any> {
  return this.editDeposit_History(parentId,null, TCModalModes.EDIT);
}
}
