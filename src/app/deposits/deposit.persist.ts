import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum, TCEnumTranslation} from "../tc/models";
import {DepositDashboard, DepositDetail, DepositSummary, DepositSummaryPartialList, Deposit_HistoryDetail,Deposit_HistorySummary, Deposit_HistorySummaryPartialList} from "./deposit.model";
import {
  deposit_history_type,
  deposit_override_status,
  deposit_patient_type,
  deposit_share_status,
  deposit_status
} from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class DepositPersist {

  // depositSearchText: string = "";
  //   //above constructor
  //   deposit_history_typeId: number ;
  //   patient_id: string;
  //   deposit_id: string;
  //   cashier_id: string;
  //   start_date: number;
  //   end_date: number;
  //   isReport: boolean;
  //   bank_id: string;

  //   statusFilter: number = null;
  //   patientId: string;

    statusType: TCEnum[] = [
      new TCEnum( deposit_status.closed, 'Closed'),
      new TCEnum( deposit_status.opened, 'Opened')
  ];

  depositShareStatus: TCEnum[] = [
    new TCEnum( deposit_share_status.shared, 'Shared'),
    new TCEnum( deposit_share_status.self, 'Self')
];

depositOverrideStatus: TCEnum[] = [
  new TCEnum( deposit_override_status.allowed_override, 'Allow'),
  new TCEnum( deposit_override_status.forbiden_override, 'Forbidden')
];

depositPatientType: TCEnum[] = [
  new TCEnum( deposit_patient_type.outside_patient, 'Outside Patient'),
  new TCEnum( deposit_patient_type.normal_patient, 'Normal Patient')
];

  constructor(private http: HttpClient,public appTranslation: AppTranslation, public tcUtilsDate: TCUtilsDate ) {
  }

  deposit_history_type: TCEnumTranslation[] = [
    new TCEnumTranslation(deposit_history_type.take_out,'take_out'),
    new TCEnumTranslation(deposit_history_type.additional_deposit, 'additional_deposit'),
    new TCEnumTranslation(deposit_history_type.initial_deposit, 'initial_deposit'),
    new TCEnumTranslation(deposit_history_type.medical_expense,'medical_expense'),
  ];

  depositSearchHistory = {
    "patient_id": "",
    "status": deposit_status.opened,
    "approval_status": undefined,
    "start_date": undefined,
    "end_date": undefined,
    "search_text": "",
    "search_column": "",
    "share_status": undefined,
  }

  defaultDepositSearchHistory = {...this.depositSearchHistory}

  resetDepositSearchHistory(){
  this.depositSearchHistory = {...this.defaultDepositSearchHistory}
  }

  searchDeposit(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.depositSearchHistory): Observable<DepositSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("deposits", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.statusFilter){
    //   url = TCUtilsString.appendUrlParameter(url, "status", this.statusFilter.toString())
    // }
    // if (this.patientId){
    //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId);
    // }
    return this.http.get<DepositSummaryPartialList>(url);

  }

  filters(searchHistory = this.depositSearchHistory): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    if (searchHistory.start_date){
      fltrs["start_date"] = searchHistory.start_date.toString();
    }
    if(searchHistory.end_date){
      fltrs["end_date"] = searchHistory.end_date.toString();
    }
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "deposits/do", new TCDoParam("download_all", this.filters()));
  }

  depositDashboard(): Observable<DepositDashboard> {
    return this.http.get<DepositDashboard>(environment.tcApiBaseUri + "deposits/dashboard");
  }

  getDeposit(id: string): Observable<DepositDetail> {
    return this.http.get<DepositDetail>(environment.tcApiBaseUri + "deposits/" + id);
  }

  getDepositByPatient(id: string): Observable<DepositDetail> {
    return this.http.get<DepositDetail>(environment.tcApiBaseUri + "deposits/patient/" + id);
  }

  addDeposit(item: DepositDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "deposits/", item);
  }

  updateDeposit(item: DepositDetail): Observable<DepositDetail> {
    return this.http.patch<DepositDetail>(environment.tcApiBaseUri + "deposits/" + item.id, item);
  }

  deleteDeposit(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "deposits/" + id);
  }

  depositsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "deposits/do", new TCDoParam(method, payload));
  }

  depositDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "deposits/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "deposits/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "deposits/" + id + "/do", new TCDoParam("print", {}));
  }


    //deposit_historys
    // deposit_historySearchText: string = "";
    depositHistorySearchHistory = {
      "deposit_id": "",
      "patient_id": "",
      "patient_name": "",
      "cashier_name": "",
      "user_id": "",
      "deposit_type": undefined,
      "bank_id": "",
      "bank_name": "",
      "status": undefined,
      "start_date": this.tcUtilsDate.toTimeStamp(new Date),
      "end_date": undefined,
      "is_report": false,
      "search_text": "",
      "search_column": "",
    }

    defaultDepositHistorySearchHistory = {...this.depositHistorySearchHistory}

    resetDepositHistorySearchHistory(){
    this.depositHistorySearchHistory = {...this.defaultDepositHistorySearchHistory}
    }

    depositReportSearchHistory = {...this.depositHistorySearchHistory, "is_report": true}

    defaultDepositReportSearchHistory = {...this.depositReportSearchHistory}

    resetDepositReportSearchHistory(){
    this.depositReportSearchHistory = {...this.defaultDepositReportSearchHistory}
    }
    searchDeposit_History( pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.depositHistorySearchHistory): Observable<Deposit_HistorySummaryPartialList> {

      let url = TCUtilsHttp.buildSearchUrl("deposit_historys", searchHistory.search_text, pageSize, pageIndex, sort, order);
      url = TCUtilsString.appendUrlParameters(url, searchHistory);
      // url = TCUtilsString.appendUrlParameter(url, "is_report", `${this.isReport}`)
      // if (this.deposit_id){
      //   url = TCUtilsString.appendUrlParameter(url, "deposit_id", this.deposit_id)
      // }
      // if (this.patient_id){
      //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patient_id)
      // }
      // if (this.cashier_id){
      //   url = TCUtilsString.appendUrlParameter(url, "user_id", this.cashier_id)
      // }
      // if (this.deposit_history_typeId){
      //   url = TCUtilsString.appendUrlParameter(url, "deposit_type", this.deposit_history_typeId.toString())
      // }
      // if (this.start_date){
      //   url = TCUtilsString.appendUrlParameter(url, "start_date", this.start_date.toString())
      // }
      // if (this.end_date){
      //   url = TCUtilsString.appendUrlParameter(url, "end_date", this.end_date.toString())
      // }
      // if (this.bank_id){
      //   url = TCUtilsString.appendUrlParameter(url, "bank_id", this.bank_id)
      // }
      return this.http.get<Deposit_HistorySummaryPartialList>(url);

    }

    getDeposit_History(parentId:string, id: string): Observable<Deposit_HistoryDetail> {
        return this.http.get<Deposit_HistoryDetail>(environment.tcApiBaseUri + "deposits/" + parentId+  "/deposit_historys/" + id);
     }


    addDeposit_History(parentId:string,item: Deposit_HistoryDetail): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "deposits/" + parentId+ "/deposit_historys/", item);
    }

    updateDeposit_History(parentId:string, item: Deposit_HistoryDetail): Observable<Deposit_HistoryDetail> {
      return this.http.patch<Deposit_HistoryDetail>(environment.tcApiBaseUri + "deposits/" + parentId+ "/deposit_historys/" + item.id, item);
    }

    deleteDeposit_History(parentId:string, id: string): Observable<{}> {
        return this.http.delete(environment.tcApiBaseUri + "deposits/" + parentId+ "/deposit_historys/" + id);
    }

    deposit_historysDo(parentId:string, method: string, payload: any): Observable<{}> {
      return this.http.post<{}>(environment.tcApiBaseUri + "deposits/" + parentId+ "/deposit_historys/do", new TCDoParam(method, payload));
    }

    deposit_historyDo(parentId:string, id: string, method: string, payload: any): Observable<{}> {
      return this.http.post<{}>( environment.tcApiBaseUri + "deposits/" + parentId+ "/deposit_historys/" + id + "/do", new TCDoParam(method, payload));
    }

}
