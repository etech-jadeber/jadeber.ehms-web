import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisOrderComponent } from './dialysis-order.component';

describe('DialysisOrderComponent', () => {
  let component: DialysisOrderComponent;
  let fixture: ComponentFixture<DialysisOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisOrderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
