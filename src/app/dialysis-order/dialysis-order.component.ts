import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { DialysisSessionPersist } from '../dialysis_session/dialysis_session.persist';

@Component({
  selector: 'app-dialysis-order',
  templateUrl: './dialysis-order.component.html',
  styleUrls: ['./dialysis-order.component.scss']
})
export class DialysisOrderComponent implements OnInit {
  selectedTabIndex:number;
  constructor(
    public dialysisSessionPersist: DialysisSessionPersist,
  ) { }

  ngOnInit(): void {
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.dialysisSessionPersist.selected_index=matTab.index;
  }

}
