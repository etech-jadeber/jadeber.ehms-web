import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisDetailComponent } from './dialysis-detail.component';

describe('DialysisDetailComponent', () => {
  let component: DialysisDetailComponent;
  let fixture: ComponentFixture<DialysisDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
