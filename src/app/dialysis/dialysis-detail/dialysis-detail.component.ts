import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DialysisDetail} from "../dialysis.model";
import {DialysisPersist} from "../dialysis.persist";
import {DialysisNavigator} from "../dialysis.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { getYear } from 'date-fns';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';

export enum DialysisTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-dialysis-detail',
  templateUrl: './dialysis-detail.component.html',
  styleUrls: ['./dialysis-detail.component.scss']
})
export class DialysisDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  dialysisIsLoading:boolean = false;
  doctorName:string;
  patientName:string;
  patientAge:number;
  patientGender:string;
  patientCardId:number;
  DialysisTabs: typeof DialysisTabs = DialysisTabs;
  activeTab: DialysisTabs = DialysisTabs.overview;
  //basics
  dialysisDetail: DialysisDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public tcUtilsDate: TCUtilsDate,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public dialysisNavigator: DialysisNavigator,
              public form_encounterPersist: Form_EncounterPersist,
              public  dialysisPersist: DialysisPersist,
              public doctorPersist: DoctorPersist,
              public patientPersist: PatientPersist,
              ) {
    this.tcAuthorization.requireRead("dialysiss");
    this.dialysisDetail = new DialysisDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("dialysiss");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.dialysisIsLoading = true;
    this.dialysisPersist.getDialysis(id).subscribe(dialysisDetail => {
          this.dialysisDetail = dialysisDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(dialysisDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.doctorPersist.getDoctor(this.dialysisDetail.doctor).subscribe((result)=>{
            if(result){
              this.doctorName=result.first_name+" "+result.middle_name+" "+result.last_name;
            }
          });
          this.patientPersist.getPatient(this.dialysisDetail.patient_id).subscribe((result)=>{
            if(result){
              this.patientName=result.fname + " " +result.mname+" "+result.lname
              this.patientAge=new Date().getFullYear()-new Date(result.dob).getFullYear();
              this.patientGender=result.sex?"Male":"Female";
              this.patientCardId=result.pid;
            }
          })
          this.dialysisIsLoading = false;
        }, error => {
          console.error(error);
          this.dialysisIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.dialysisDetail.id);
  }
  editDialysis(): void {
    let modalRef = this.dialysisNavigator.editDialysis(this.dialysisDetail.id);
    modalRef.afterClosed().subscribe(dialysisDetail => {
      TCUtilsAngular.assign(this.dialysisDetail, dialysisDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.dialysisPersist.print(this.dialysisDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print dialysis", true);
      });
    }

  back():void{
      this.location.back();
    }

}