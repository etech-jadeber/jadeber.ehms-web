import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisEditComponent } from './dialysis-edit.component';

describe('DialysisEditComponent', () => {
  let component: DialysisEditComponent;
  let fixture: ComponentFixture<DialysisEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
