import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DialysisDetail } from '../dialysis.model';
import { DialysisPersist } from '../dialysis.persist';
import { other_reason } from 'src/app/app.enums';

@Component({
  selector: 'app-dialysis-edit',
  templateUrl: './dialysis-edit.component.html',
  styleUrls: ['./dialysis-edit.component.scss']
})export class DialysisEditComponent implements OnInit {
  other_reason=other_reason;
  indication_for_homodialysis:string[];
  underlying_causes: string[];
  isLoadingResults: boolean = false;
  title: string;
  dialysisDetail: DialysisDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DialysisEditComponent>,
              public  persist: DialysisPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("dialysiss");
      this.title = this.appTranslation.getText("general","new") +  " " + "dialysis";
      this.dialysisDetail = new DialysisDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("dialysiss");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "dialysis";
      this.isLoadingResults = true;
      this.persist.getDialysis(this.idMode.id).subscribe(dialysisDetail => {
        this.dialysisDetail = dialysisDetail;
        this.indication_for_homodialysis=this.transformToList(this.dialysisDetail.indication_for_homodialysis);
        this.underlying_causes=this.transformToList(this.dialysisDetail.underlying_causes);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   this.dialysisDetail.indication_for_homodialysis=this.indication_for_homodialysis.toString();
   this.dialysisDetail.underlying_causes=this.underlying_causes.toString();
    this.persist.addDialysis(this.idMode.id,this.dialysisDetail).subscribe(value => {
      this.tcNotification.success("dialysis added");
      this.dialysisDetail.id = value.id;
      this.dialogRef.close(this.dialysisDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }


  transformToList(stringFile:string):string[]{
    let y=[];
    while(stringFile.indexOf(',')!=-1){
      y=[...y,stringFile.slice(0,stringFile.indexOf(','))]
      stringFile=stringFile.slice(stringFile.indexOf(',')+1);
      if(stringFile.indexOf(',')==-1)
          y=[...y,stringFile]
      }
      return y;
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.dialysisDetail.indication_for_homodialysis=this.indication_for_homodialysis.toString();
    this.dialysisDetail.underlying_causes=this.underlying_causes.toString();

    this.persist.updateDialysis(this.dialysisDetail).subscribe(value => {
      this.tcNotification.success("dialysis updated");
      this.dialogRef.close(this.dialysisDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.dialysisDetail == null){
            return false;
          }


if (this.dialysisDetail.renal_failure == null) {
            return false;
        }
if (this.dialysisDetail.other_reason == null) {
            return false;
        }
if ((this.dialysisDetail.drug_overdose == null || this.dialysisDetail.drug_overdose  == "")&&(this.dialysisDetail.poisoning == null || this.dialysisDetail.poisoning  == "")&&(this.dialysisDetail.other == null || this.dialysisDetail.other  == "")) {
            return false;
        } 
if (this.dialysisDetail.full_diagnosis == null || this.dialysisDetail.full_diagnosis  == "") {
            return false;
        } 
if (this.dialysisDetail.transfer_faculity == null || this.dialysisDetail.transfer_faculity  == "") {
            return false;
        } 
if (this.dialysisDetail.obstractive_cause == null || this.dialysisDetail.obstractive_cause  == "") {
            return false;
        } 
if (this.underlying_causes == null) {
            return false;
        } 
if (this.indication_for_homodialysis == null ) {
            return false;
        } 
if (this.dialysisDetail.physical_exam_and_investigation == null || this.dialysisDetail.physical_exam_and_investigation  == "") {
            return false;
        } 
if (this.dialysisDetail.summary_of_image_tests == null || this.dialysisDetail.summary_of_image_tests  == "") {
            return false;
        } 
if (this.dialysisDetail.relevant_investigation == null) {
            return false;
        }
if (this.dialysisDetail.dialysis_acess_data_inserted == null) {
            return false;
        }
if (this.dialysisDetail.dry_weight == null) {
            return false;
        }
if (this.dialysisDetail.last_hbsag == null) {
            return false;
        }
if (this.dialysisDetail.last_hcvab == null) {

            return false;
        }
if (this.dialysisDetail.last_pict == null) {
            return false;
        }
if (this.dialysisDetail.resent_hbsag == null) {
            return false;
        }
if (this.dialysisDetail.resent_hcvab == null) {
            return false;
        }
if (this.dialysisDetail.resent_pict == null) {
            return false;
        }
if (this.dialysisDetail.dialysis_adequacy == null || this.dialysisDetail.dialysis_adequacy  == "") {
            return false;
        } 
 return true;

 }
 }