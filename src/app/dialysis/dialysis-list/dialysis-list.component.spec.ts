import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisListComponent } from './dialysis-list.component';

describe('DialysisListComponent', () => {
  let component: DialysisListComponent;
  let fixture: ComponentFixture<DialysisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
