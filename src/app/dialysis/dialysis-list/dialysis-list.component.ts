import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisSummary, DialysisSummaryPartialList } from '../dialysis.model';
import { DialysisPersist } from '../dialysis.persist';
import { DialysisNavigator } from '../dialysis.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { DialysisSessionNavigator } from 'src/app/dialysis_session/dialysis_session.navigator';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

class Patient_name{
  id:string;
  name:string;
  
}
@Component({
  selector: 'app-dialysis-list',
  templateUrl: './dialysis-list.component.html',
  styleUrls: ['./dialysis-list.component.scss']
})export class DialysisListComponent implements OnInit {
  dialysissData: DialysisSummary[] = [];
  dialysissTotalCount: number = 0;
  dialysisSelectAll:boolean = false;
  dialysisSelection: DialysisSummary[] = [];
  patientNames:Patient_name[]=[];
  size:number;
  @Input() isSession:boolean=false;
  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>()

 dialysissDisplayedColumns: string[] = ["select","action","date", 'pid'  ,"patient_id","renal_failure","underlying_causes","dialysis_adequacy"];
  dialysisSearchTextBox: FormControl = new FormControl();
  dialysisIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysissPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysissSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisPersist: DialysisPersist,
                public dialysisNavigator: DialysisNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public dialysisSessionNavigator: DialysisSessionNavigator,
                public patientPersist: PatientPersist,
                public form_encounterPersist: Form_EncounterPersist,

    ) {

        this.tcAuthorization.requireRead("dialysiss");
       this.dialysisSearchTextBox.setValue(dialysisPersist.dialysisSearchText);
      //delay subsequent keyup events
      this.dialysisSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisPersist.dialysisSearchText = value;
        this.searchDialysiss();
      });
    } ngOnInit() {
this.dialysisPersist.patientId = this.patientId
      this.dialysissSort.sortChange.subscribe(() => {
        this.dialysissPaginator.pageIndex = 0;
        this.searchDialysiss(true);
      });

      this.dialysissPaginator.page.subscribe(() => {
        this.searchDialysiss(true);
      });
      //start by loading items
      this.searchDialysiss();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.dialysisPersist.encounterId = this.encounterId;
      } else {
      this.dialysisPersist.encounterId = null;
      }
      this.searchDialysiss()
      }

  searchDialysiss(isPagination:boolean = false): void {


    let paginator = this.dialysissPaginator;
    let sorter = this.dialysissSort;

    this.dialysisIsLoading = true;
    this.dialysisSelection = [];
    this.dialysisPersist.searchDialysis(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisSummaryPartialList) => {
      this.dialysissData = partialList.data;
      this.size=partialList.total;
      if (partialList.total != -1) {
        this.dialysissTotalCount = partialList.total;
        // for(let dialysis of this.dialysissData){
        //   this.patientPersist.getPatient(dialysis.patient_id).subscribe((result)=>{
        //     let patientName= new Patient_name();
        //     patientName.name=result.fname + " " + result.mname +" " +result.lname;
        //     patientName.id=dialysis.patient_id;
        //     this.patientNames.push(patientName);
        //   })
          
        // }
      }
      this.dialysisIsLoading = false;
    }, error => {
      this.dialysisIsLoading = false;
    });

  } downloadDialysiss(): void {
    if(this.dialysisSelectAll){
         this.dialysisPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download dialysis", true);
      });
    }
    else{
        this.dialysisPersist.download(this.tcUtilsArray.idsList(this.dialysisSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download dialysis",true);
            });
        }
  }
addDialysis(): void {
    let dialogRef = this.dialysisNavigator.addDialysis(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDialysiss();
      }
    });
  }

  editDialysis(item: DialysisSummary) {
    let dialogRef = this.dialysisNavigator.editDialysis(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  createNewSession(item: DialysisSummary){
    let dialogRef=this.dialysisSessionNavigator.addDialysisSession(item.id)
  }

  deleteDialysis(item: DialysisSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("dialysis");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.dialysisPersist.deleteDialysis(item.id).subscribe(response => {
          this.tcNotification.success("dialysis deleted");
          this.searchDialysiss();
        }, error => {
        });
      }

    });
  
  }
  ngOnDestroy():void{
      this.dialysisPersist.encounterId=TCUtilsString.getInvalidId();
  }
  back():void{
      this.location.back();
    }
}