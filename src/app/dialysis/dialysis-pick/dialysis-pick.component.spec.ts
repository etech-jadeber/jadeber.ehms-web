import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisPickComponent } from './dialysis-pick.component';

describe('DialysisPickComponent', () => {
  let component: DialysisPickComponent;
  let fixture: ComponentFixture<DialysisPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
