import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisSummary, DialysisSummaryPartialList } from '../dialysis.model';
import { DialysisPersist } from '../dialysis.persist';
import { DialysisNavigator } from '../dialysis.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dialysis-pick',
  templateUrl: './dialysis-pick.component.html',
  styleUrls: ['./dialysis-pick.component.scss']
})export class DialysisPickComponent implements OnInit {
  dialysissData: DialysisSummary[] = [];
  dialysissTotalCount: number = 0;
  dialysisSelectAll:boolean = false;
  dialysisSelection: DialysisSummary[] = [];
 dialysissDisplayedColumns: string[] = ["select", ,"encounter_id","patient_id","renal_failure","other_reason","drug_overdose","poisoning","other","full_diagnosis","date","transfer_faculity","obstractive_cause","underlying_causes","indication_for_homodialysis","physical_exam_and_investigation","summary_of_image_tests","relevant_investigation","dialysis_acess_data_inserted","dry_weight","last_HBSAG","last_HCVAB","last_PICT","resent_HBSAG","resent_HCVAB","resent_PICT","dialysis_adequacy","doctor" ];
  dialysisSearchTextBox: FormControl = new FormControl();
  dialysisIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysissPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysissSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisPersist: DialysisPersist,
                public dialysisNavigator: DialysisNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<DialysisPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("dialysiss");
       this.dialysisSearchTextBox.setValue(dialysisPersist.dialysisSearchText);
      //delay subsequent keyup events
      this.dialysisSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisPersist.dialysisSearchText = value;
        this.searchDialysiss();
      });
    } ngOnInit() {
   
      this.dialysissSort.sortChange.subscribe(() => {
        this.dialysissPaginator.pageIndex = 0;
        this.searchDialysiss(true);
      });

      this.dialysissPaginator.page.subscribe(() => {
        this.searchDialysiss(true);
      });
      //start by loading items
      this.searchDialysiss();
    }

  searchDialysiss(isPagination:boolean = false): void {


    let paginator = this.dialysissPaginator;
    let sorter = this.dialysissSort;

    this.dialysisIsLoading = true;
    this.dialysisSelection = [];

    this.dialysisPersist.searchDialysis(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisSummaryPartialList) => {
      this.dialysissData = partialList.data;
      if (partialList.total != -1) {
        this.dialysissTotalCount = partialList.total;
      }
      this.dialysisIsLoading = false;
    }, error => {
      this.dialysisIsLoading = false;
    });

  }
  markOneItem(item: DialysisSummary) {
    if(!this.tcUtilsArray.containsId(this.dialysisSelection,item.id)){
          this.dialysisSelection = [];
          this.dialysisSelection.push(item);
        }
        else{
          this.dialysisSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.dialysisSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }