import {TCId} from "../tc/models";export class DialysisSummary extends TCId {
    encounter_id:string;
    patient_id:string;
    renal_failure:number;
    other_reason:number;
    drug_overdose:string;
    poisoning:string;
    other:string;
    full_diagnosis:string;
    date:number;
    transfer_faculity:string;
    obstractive_cause:string;
    underlying_causes:string;
    indication_for_homodialysis:string;
    physical_exam_and_investigation:string;
    summary_of_image_tests:string;
    relevant_investigation:number;
    dialysis_acess_data_inserted:number;
    dry_weight:number;
    last_hbsag:number;
    last_hcvab:number;
    last_pict:number;
    resent_hbsag:number;
    resent_hcvab:number;
    resent_pict:number;
    dialysis_adequacy:string;
    doctor:string;
    dialysis_id:number;
    id:string;
     
    }
    export class DialysisSummaryPartialList {
      data: DialysisSummary[];
      total: number;
    }
    export class DialysisDetail extends DialysisSummary {
    }
    
    export class DialysisDashboard {
      total: number;
    }