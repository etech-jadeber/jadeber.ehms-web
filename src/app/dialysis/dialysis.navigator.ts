import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DialysisEditComponent} from "./dialysis-edit/dialysis-edit.component";
import {DialysisPickComponent} from "./dialysis-pick/dialysis-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DialysisNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  dialysissUrl(): string {
    return "/dialysiss";
  }

  dialysisUrl(id: string): string {
    return "/dialysiss/" + id;
  }

  viewDialysiss(): void {
    this.router.navigateByUrl(this.dialysissUrl());
  }

  viewDialysis(id: string): void {
    this.router.navigateByUrl(this.dialysisUrl(id));
  }

  editDialysis(id: string): MatDialogRef<DialysisEditComponent> {
    const dialogRef = this.dialog.open(DialysisEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addDialysis(encounterId:string): MatDialogRef<DialysisEditComponent> {
    const dialogRef = this.dialog.open(DialysisEditComponent, {
          data: new TCIdMode(encounterId, TCModalModes.NEW),
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickDialysiss(selectOne: boolean=false): MatDialogRef<DialysisPickComponent> {
      const dialogRef = this.dialog.open(DialysisPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}