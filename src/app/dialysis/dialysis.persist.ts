import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum, TCIdMode} from "../tc/models";
import {DialysisDashboard, DialysisDetail, DialysisSummaryPartialList} from "./dialysis.model";
import { dialysis_acess_data_inserted, indication_for_homodialysis, other_reason, relevant_investigation, renal_failure, serology, underlying_causes } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DialysisPersist {
 dialysisSearchText: string = "";
 patientId: string;
 encounterId:string;

    indicationForHomodialysis: TCIdMode[] = [
     new TCIdMode( indication_for_homodialysis.uremia, 'Uremia with encephalopathy'),
     new TCIdMode( indication_for_homodialysis.excessive, 'Excessive fluid retantion refractory to diuresis'),
     new TCIdMode( indication_for_homodialysis.electrolyte, 'Electrolyte Imbalance'),
     new TCIdMode( indication_for_homodialysis.pericardits_pleurisy, 'Pericardits and or pleurisy'),
     new TCIdMode( indication_for_homodialysis.nephrologist, 'Nephrologist recommendation'),
     new TCIdMode( indication_for_homodialysis.uremic, 'Uremic Encephalopathy'),
     new TCIdMode( indication_for_homodialysis.pericardits, 'Pericarditis'),
     new TCIdMode( indication_for_homodialysis.bleeding, 'Bleeding associated with Uremia'),
     new TCIdMode( indication_for_homodialysis.fluid, 'Fluid overload unresponsive'),
     new TCIdMode( indication_for_homodialysis.electrolyte_imbalance, 'Electrolyte imbalance (Severe and intractable Hyperkalemia'),
     new TCIdMode( indication_for_homodialysis.acid_base, 'Acid Base imbalance (Severe Metabolic Acidosis'),
     new TCIdMode( indication_for_homodialysis.accelerated, 'Accelerated hypertension poorly responsive to antihypertensives'),
  ];
  serology:TCEnum[]=[
    new TCEnum(serology.positive,"Positive"),
    new TCEnum(serology.negative,"Negative")
  ];
  other_reason:TCEnum[]=[
    new TCEnum(other_reason.poisoning,"Poisoning"),
    new TCEnum(other_reason.drug_overdose,"Drug Overdose"),
    new TCEnum(other_reason.other,"other")
  ];
  relevant_investigation:TCEnum[]=[
   new TCEnum(relevant_investigation.hgb,"HGB"),
   new TCEnum(relevant_investigation.calcium,"Calcium"),
   new TCEnum(relevant_investigation.po,"Po4"),
   new TCEnum(relevant_investigation.pth,"PTH")
  ];
  dialysis_acess_data_inserted:TCEnum[]=[
    new TCEnum(dialysis_acess_data_inserted.temporary_catheter,"Temporary catheter"),
    new TCEnum(dialysis_acess_data_inserted.permanent_catheter,"Permanent catheter"),
    new TCEnum(dialysis_acess_data_inserted.AV_fistula,"AV fiftula"),
    new TCEnum(dialysis_acess_data_inserted.AV_graft,"AV graft")
   ];
   renal_failure:TCEnum[]=[
    new TCEnum(renal_failure.AKI,"AKI"),
    new TCEnum(renal_failure.CKD,"CKD"),
    new TCEnum(renal_failure.AKI_on_CKD,"AKI on CKD"),
   ];
   underlying_causes:TCIdMode[]=[
    new TCIdMode(underlying_causes.hypertension,"Hypertension"),
    new TCIdMode(underlying_causes.lupus_nephritis,"Lupus nephritis"),
    new TCIdMode(underlying_causes.diabetes_mellitus,"Diabetes mellitus"),
    new TCIdMode(underlying_causes.drug_induced,"Drug Induced"),
   ];
  

 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.dialysisSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchDialysis(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DialysisSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("dialysis", this.dialysisSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<DialysisSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis/do", new TCDoParam("download_all", this.filters()));
  }

  dialysisDashboard(): Observable<DialysisDashboard> {
    return this.http.get<DialysisDashboard>(environment.tcApiBaseUri + "dialysis/dashboard");
  }

  getDialysis(id: string): Observable<DialysisDetail> {
    return this.http.get<DialysisDetail>(environment.tcApiBaseUri + "dialysis/" + id);
  } addDialysis(encounter_id:string,item: DialysisDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/"+encounter_id+"/dialysis", item);
  }

  updateDialysis(item: DialysisDetail): Observable<DialysisDetail> {
    return this.http.patch<DialysisDetail>(environment.tcApiBaseUri + "dialysis/" + item.id, item);
  }

  deleteDialysis(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "dialysis/" + id);
  }
 dialysissDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysis/do", new TCDoParam(method, payload));
  }

  dialysisDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysiss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis/" + id + "/do", new TCDoParam("print", {}));
  }


}