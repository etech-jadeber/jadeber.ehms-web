import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisMachineDetailComponent } from './dialysis-machine-detail.component';

describe('DialysisMachineDetailComponent', () => {
  let component: DialysisMachineDetailComponent;
  let fixture: ComponentFixture<DialysisMachineDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisMachineDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisMachineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
