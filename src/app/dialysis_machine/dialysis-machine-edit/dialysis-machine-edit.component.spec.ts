import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisMachineEditComponent } from './dialysis-machine-edit.component';

describe('DialysisMachineEditComponent', () => {
  let component: DialysisMachineEditComponent;
  let fixture: ComponentFixture<DialysisMachineEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisMachineEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisMachineEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
