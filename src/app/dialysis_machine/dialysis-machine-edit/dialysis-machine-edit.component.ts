import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DialysisMachineDetail } from '../dialysis_machine.model';
import { DialysisMachinePersist } from '../dialysis_machine.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
@Component({
  selector: 'app-dialysis_machine-edit',
  templateUrl: './dialysis-machine-edit.component.html',
  styleUrls: ['./dialysis-machine-edit.component.scss']
})export class DialysisMachineEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  dialysisMachineDetail: DialysisMachineDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DialysisMachineEditComponent>,
              public  persist: DialysisMachinePersist,
              public bedPersist: BedPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("dialysis_machines");
      this.title = this.appTranslation.getText("general","new") +  " " + "Dialysis Machine";
      this.dialysisMachineDetail = new DialysisMachineDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("dialysis_machines");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "Dialysis Machine";
      this.isLoadingResults = true;
      this.persist.getDialysisMachine(this.idMode.id).subscribe(dialysisMachineDetail => {
        this.dialysisMachineDetail = dialysisMachineDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addDialysisMachine(this.dialysisMachineDetail).subscribe(value => {
      this.tcNotification.success("dialysisMachine added");
      this.dialysisMachineDetail.id = value.id;
      this.dialogRef.close(this.dialysisMachineDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateDialysisMachine(this.dialysisMachineDetail).subscribe(value => {
      this.tcNotification.success("dialysis_machine updated");
      this.dialogRef.close(this.dialysisMachineDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.dialysisMachineDetail == null){
            return false;
          }

if (this.dialysisMachineDetail.serial_no == null || this.dialysisMachineDetail.serial_no  == "") {
            return false;
        } 
if (this.dialysisMachineDetail.machine_no == null || this.dialysisMachineDetail.machine_no  == "") {
            return false;
        } 
 return true;

 }
 }