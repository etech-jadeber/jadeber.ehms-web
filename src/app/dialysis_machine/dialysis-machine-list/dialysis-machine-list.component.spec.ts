import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisMachineListComponent } from './dialysis-machine-list.component';

describe('DialysisMachineListComponent', () => {
  let component: DialysisMachineListComponent;
  let fixture: ComponentFixture<DialysisMachineListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisMachineListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisMachineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
