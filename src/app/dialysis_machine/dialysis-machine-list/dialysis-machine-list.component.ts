import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisMachineSummary, DialysisMachineSummaryPartialList } from '../dialysis_machine.model';
import { DialysisMachinePersist } from '../dialysis_machine.persist';
import { DialysisMachineNavigator } from '../dialysis_machine.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
@Component({
  selector: 'app-dialysis_machine-list',
  templateUrl: './dialysis-machine-list.component.html',
  styleUrls: ['./dialysis-machine-list.component.scss']
})export class DialysisMachineListComponent implements OnInit {
  dialysisMachinesData: DialysisMachineSummary[] = [];
  dialysisMachinesTotalCount: number = 0;
  dialysisMachineSelectAll:boolean = false;
  dialysisMachineSelection: DialysisMachineSummary[] = [];

 dialysisMachinesDisplayedColumns: string[] = ["select","action" ,"serial_no","machine_no","status" ];
  dialysisMachineSearchTextBox: FormControl = new FormControl();
  dialysisMachineIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysisMachinesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysisMachinesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisMachinePersist: DialysisMachinePersist,
                public dialysisMachineNavigator: DialysisMachineNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public bedPersist: BedPersist,

    ) {

        this.tcAuthorization.requireRead("dialysis_machines");
       this.dialysisMachineSearchTextBox.setValue(dialysisMachinePersist.dialysisMachineSearchText);
      //delay subsequent keyup events
      this.dialysisMachineSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisMachinePersist.dialysisMachineSearchText = value;
        this.searchDialysisMachines();
      });
    } ngOnInit() {
   
      this.dialysisMachinesSort.sortChange.subscribe(() => {
        this.dialysisMachinesPaginator.pageIndex = 0;
        this.searchDialysisMachines(true);
      });

      this.dialysisMachinesPaginator.page.subscribe(() => {
        this.searchDialysisMachines(true);
      });
      //start by loading items
      this.searchDialysisMachines();
    }

  searchDialysisMachines(isPagination:boolean = false): void {


    let paginator = this.dialysisMachinesPaginator;
    let sorter = this.dialysisMachinesSort;

    this.dialysisMachineIsLoading = true;
    this.dialysisMachineSelection = [];

    this.dialysisMachinePersist.searchDialysisMachine(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisMachineSummaryPartialList) => {
      this.dialysisMachinesData = partialList.data;
      if (partialList.total != -1) {
        this.dialysisMachinesTotalCount = partialList.total;
      }
      this.dialysisMachineIsLoading = false;
    }, error => {
      this.dialysisMachineIsLoading = false;
    });

  } downloadDialysisMachines(): void {
    if(this.dialysisMachineSelectAll){
         this.dialysisMachinePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download dialysis_machine", true);
      });
    }
    else{
        this.dialysisMachinePersist.download(this.tcUtilsArray.idsList(this.dialysisMachineSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download dialysis_machine",true);
            });
        }
  }
addDialysisMachine(): void {
    let dialogRef = this.dialysisMachineNavigator.addDialysisMachine();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDialysisMachines();
      }
    });
  }

  editDialysisMachine(item: DialysisMachineSummary) {
    let dialogRef = this.dialysisMachineNavigator.editDialysisMachine(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDialysisMachine(item: DialysisMachineSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("dialysis_machine");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.dialysisMachinePersist.deleteDialysisMachine(item.id).subscribe(response => {
          this.tcNotification.success("dialysis_machine deleted");
          this.searchDialysisMachines();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}