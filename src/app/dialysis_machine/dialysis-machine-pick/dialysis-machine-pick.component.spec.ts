import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisMachinePickComponent } from './dialysis-machine-pick.component';

describe('DialysisMachinePickComponent', () => {
  let component: DialysisMachinePickComponent;
  let fixture: ComponentFixture<DialysisMachinePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisMachinePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisMachinePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
