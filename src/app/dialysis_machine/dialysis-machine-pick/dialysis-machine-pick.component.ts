import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisMachineSummary, DialysisMachineSummaryPartialList } from '../dialysis_machine.model';
import { DialysisMachinePersist } from '../dialysis_machine.persist';
import { DialysisMachineNavigator } from '../dialysis_machine.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-dialysis_machine-pick',
  templateUrl: './dialysis-machine-pick.component.html',
  styleUrls: ['./dialysis-machine-pick.component.scss']
})export class DialysisMachinePickComponent implements OnInit {
  dialysisMachinesData: DialysisMachineSummary[] = [];
  dialysisMachinesTotalCount: number = 0;
  dialysisMachineSelectAll:boolean = false;
  dialysisMachineSelection: DialysisMachineSummary[] = [];

 dialysisMachinesDisplayedColumns: string[] = ["select", "serial_no","machine_no" ];
  dialysisMachineSearchTextBox: FormControl = new FormControl();
  dialysisMachineIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysisMachinesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysisMachinesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisMachinePersist: DialysisMachinePersist,
                public dialysisMachineNavigator: DialysisMachineNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<DialysisMachinePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("dialysis_machines");
       this.dialysisMachineSearchTextBox.setValue(dialysisMachinePersist.dialysisMachineSearchText);
      //delay subsequent keyup events
      this.dialysisMachineSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisMachinePersist.dialysisMachineSearchText = value;
        this.searchDialysis_machines();
      });
    } ngOnInit() {
   
      this.dialysisMachinesSort.sortChange.subscribe(() => {
        this.dialysisMachinesPaginator.pageIndex = 0;
        this.searchDialysis_machines(true);
      });

      this.dialysisMachinesPaginator.page.subscribe(() => {
        this.searchDialysis_machines(true);
      });
      //start by loading items
      this.searchDialysis_machines();
    }

  searchDialysis_machines(isPagination:boolean = false): void {


    let paginator = this.dialysisMachinesPaginator;
    let sorter = this.dialysisMachinesSort;

    this.dialysisMachineIsLoading = true;
    this.dialysisMachineSelection = [];
    this.dialysisMachinePersist.availablity="pick_available"
    this.dialysisMachinePersist.searchDialysisMachine(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisMachineSummaryPartialList) => {
      this.dialysisMachinesData = partialList.data;
      if (partialList.total != -1) {
        this.dialysisMachinesTotalCount = partialList.total;
      }
      this.dialysisMachineIsLoading = false;
    }, error => {
      this.dialysisMachineIsLoading = false;
    });

  }
  markOneItem(item: DialysisMachineSummary) {
    if(!this.tcUtilsArray.containsId(this.dialysisMachineSelection,item.id)){
          this.dialysisMachineSelection = [];
          this.dialysisMachineSelection.push(item);
        }
        else{
          this.dialysisMachineSelection = [];
        }
  }

  ngOnDestroy():void{
    this.dialysisMachinePersist.availablity="";
  }

  returnSelected(): void {
    this.dialogRef.close(this.dialysisMachineSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }