import {TCId} from "../tc/models";
export class DialysisMachineSummary extends TCId {
    serial_no:string;
    machine_no:string;
    status:number;
     id:string;
    }
    export class DialysisMachineSummaryPartialList {
      data: DialysisMachineSummary[];
      total: number;
    }
    export class DialysisMachineDetail extends DialysisMachineSummary {
    }
    
    export class DialysisMachineDashboard {
      total: number;
    }