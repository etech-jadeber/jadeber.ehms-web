import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DialysisMachineEditComponent} from "./dialysis-machine-edit/dialysis-machine-edit.component";
import {DialysisMachinePickComponent} from "./dialysis-machine-pick/dialysis-machine-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class DialysisMachineNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  dialysisMachinesUrl(): string {
    return "/dialysis_machines";
  }

  dialysisMachineUrl(id: string): string {
    return "/dialysis_machines/" + id;
  }

  viewDialysisMachines(): void {
    this.router.navigateByUrl(this.dialysisMachinesUrl());
  }

  viewDialysisMachine(id: string): void {
    this.router.navigateByUrl(this.dialysisMachineUrl(id));
  }

  editDialysisMachine(id: string): MatDialogRef<DialysisMachineEditComponent> {
    const dialogRef = this.dialog.open(DialysisMachineEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDialysisMachine(): MatDialogRef<DialysisMachineEditComponent> {
    const dialogRef = this.dialog.open(DialysisMachineEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickDialysisMachines(selectOne: boolean=false): MatDialogRef<DialysisMachinePickComponent> {
      const dialogRef = this.dialog.open(DialysisMachinePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}