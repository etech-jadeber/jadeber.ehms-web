import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {DialysisMachineDashboard, DialysisMachineDetail, DialysisMachineSummaryPartialList} from "./dialysis_machine.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DialysisMachinePersist {
 dialysisMachineSearchText: string = "";
 availablity="";
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.dialysisMachineSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchDialysisMachine(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DialysisMachineSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("dialysis_machine", this.dialysisMachineSearchText, pageSize, pageIndex, sort, order);
    url=TCUtilsString.appendUrlParameter(url,"avaliblity",this.availablity);
    return this.http.get<DialysisMachineSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_machine/do", new TCDoParam("download_all", this.filters()));
  }

  dialysisMachineDashboard(): Observable<DialysisMachineDashboard> {
    return this.http.get<DialysisMachineDashboard>(environment.tcApiBaseUri + "dialysis_machine/dashboard");
  }

  getDialysisMachine(id: string): Observable<DialysisMachineDetail> {
    return this.http.get<DialysisMachineDetail>(environment.tcApiBaseUri + "dialysis_machine/" + id);
  } addDialysisMachine(item: DialysisMachineDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_machine/", item);
  }

  updateDialysisMachine(item: DialysisMachineDetail): Observable<DialysisMachineDetail> {
    return this.http.patch<DialysisMachineDetail>(environment.tcApiBaseUri + "dialysis_machine/" + item.id, item);
  }

  deleteDialysisMachine(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "dialysis_machine/" + id);
  }
 dialysisMachinesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysis_machine/do", new TCDoParam(method, payload));
  }

  dialysisMachineDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysis_machines/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_machine/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_machine/" + id + "/do", new TCDoParam("print", {}));
  }


}