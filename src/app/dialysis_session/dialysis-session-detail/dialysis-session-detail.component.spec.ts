import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisSessionDetailComponent } from './dialysis-session-detail.component';

describe('DialysisSessionDetailComponent', () => {
  let component: DialysisSessionDetailComponent;
  let fixture: ComponentFixture<DialysisSessionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisSessionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisSessionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
