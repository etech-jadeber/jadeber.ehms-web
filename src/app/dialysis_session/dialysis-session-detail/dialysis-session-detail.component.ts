import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DialysisSessionDetail} from "../dialysis_session.model";
import {DialysisSessionPersist} from "../dialysis_session.persist";
import {DialysisSessionNavigator} from "../dialysis_session.navigator";
import { DialysisMachinePersist } from 'src/app/dialysis_machine/dialysis_machine.persist';
import { PatientIntakeFormSummary } from 'src/app/patient_intake_form/patient_intake_form.model';
import { PatientIntakeFormNavigator } from 'src/app/patient_intake_form/patient_intake_form.navigator';
import { MatTabChangeEvent } from '@angular/material/tabs';

export enum DialysisSessionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-dialysis_session-detail',
  templateUrl: './dialysis-session-detail.component.html',
  styleUrls: ['./dialysis-session-detail.component.scss']
})
export class DialysisSessionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  dialysisSessionIsLoading:boolean = false;
  
  DialysisSessionTabs: typeof DialysisSessionTabs = DialysisSessionTabs;
  activeTab: DialysisSessionTabs = DialysisSessionTabs.overview;
  //basics
  machine_no="";
  dialysisSessionDetail: DialysisSessionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public dialysisSessionNavigator: DialysisSessionNavigator,
              public patientIntakeFormNavigator: PatientIntakeFormNavigator,
              public  dialysisSessionPersist: DialysisSessionPersist,
              public dialysisMachinePersist: DialysisMachinePersist,
              ) {
    this.tcAuthorization.requireRead("dialysis_sessions");
    this.dialysisSessionDetail = new DialysisSessionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("dialysis_sessions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.dialysisSessionIsLoading = true;
    this.dialysisSessionPersist.getDialysisSession(id).subscribe(dialysisSessionDetail => {
      if(dialysisSessionDetail){
          this.dialysisSessionDetail = dialysisSessionDetail;
          this.dialysisMachinePersist.getDialysisMachine(dialysisSessionDetail.machine_no).subscribe((result)=>{
            if(result){
              this.machine_no=result.machine_no;
            }
          })
          this.dialysisSessionIsLoading = false;
        }
        else{
          this.location.back();
        }
        }, error => {
          console.error(error);
          this.dialysisSessionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.dialysisSessionDetail.id);
  }
  editDialysisSession(): void {
    let modalRef = this.dialysisSessionNavigator.editDialysisSession(this.dialysisSessionDetail.id);
    modalRef.afterClosed().subscribe(dialysisSessionDetail => {
      TCUtilsAngular.assign(this.dialysisSessionDetail, dialysisSessionDetail);
    }, error => {
      console.error(error);
    });
  }

  // addPatientIntakeForm(): void {
  
  onTabChanged(matTab: MatTabChangeEvent){
    this.dialysisSessionPersist.tab_index=matTab.index;
  }


   printBirth():void{
      this.dialysisSessionPersist.print(this.dialysisSessionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print dialysis_session", true);
      });
    }

  back():void{
      this.location.back();
    }

}