import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisSessionEditComponent } from './dialysis-session-edit.component';

describe('DialysisSessionEditComponent', () => {
  let component: DialysisSessionEditComponent;
  let fixture: ComponentFixture<DialysisSessionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisSessionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisSessionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
