import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DialysisSessionDetail } from '../dialysis_session.model';import { DialysisSessionPersist } from '../dialysis_session.persist';import { DialysisPersist } from 'src/app/dialysis/dialysis.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DialysisMachinePersist } from 'src/app/dialysis_machine/dialysis_machine.persist';
import { DialysisMachineNavigator } from 'src/app/dialysis_machine/dialysis_machine.navigator';
import { DialysisMachineSummary } from 'src/app/dialysis_machine/dialysis_machine.model';
@Component({
  selector: 'app-dialysis_session-edit',
  templateUrl: './dialysis-session-edit.component.html',
  styleUrls: ['./dialysis-session-edit.component.scss']
})export class DialysisSessionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  machine_no:string
  dialysisSessionDetail: DialysisSessionDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DialysisSessionEditComponent>,
              public  persist: DialysisSessionPersist,
              
              public tcUtilsDate: TCUtilsDate,
              public dialysisPersist: DialysisPersist,
              public patientPersist: PatientPersist,
              public dialysisMachinePersist: DialysisMachinePersist,
              public dialysisMachineNavigator: DialysisMachineNavigator,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("dialysis_sessions");
      this.title = this.appTranslation.getText("general","new") +  " " + "dialysis_session";
      this.dialysisSessionDetail = new DialysisSessionDetail();
      if(this.idMode.id!=""){
        this.dialysisPersist.getDialysis(this.idMode.id).subscribe((result)=>{
          if(result){
            this.dialysisSessionDetail.encounter_id=result.encounter_id;
            this.dialysisSessionDetail.dialysis_id=result.dialysis_id;
            this.patientPersist.getPatient(result.patient_id).subscribe((patient)=>{
              if(patient){
                  this.dialysisSessionDetail.patient_name= patient.fname+" "+patient.mname+" "+patient.lname;
                  this.dialysisSessionDetail.card_id=patient.pid;
              }
            })
          }
        });
      }
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("dialysis_sessions");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "dialysis_session";
      this.isLoadingResults = true;
      this.persist.getDialysisSession(this.idMode.id).subscribe(dialysisSessionDetail => {
        this.dialysisSessionDetail = dialysisSessionDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addDialysisSession(this.dialysisSessionDetail).subscribe(value => {
      this.tcNotification.success("dialysisSession added");
      this.dialysisSessionDetail.id = value.id;
      this.dialogRef.close(this.dialysisSessionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateDialysisSession(this.dialysisSessionDetail).subscribe(value => {
      this.tcNotification.success("dialysis_session updated");
      this.dialogRef.close(this.dialysisSessionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  } 
  searchMachine():void{
    let dialogRef=this.dialysisMachineNavigator.pickDialysisMachines();
    dialogRef.afterClosed().subscribe((result:DialysisMachineSummary[])=>{
      if(result){
        console.log(result)
        this.dialysisSessionDetail.machine_no=result[0].id;
        this.machine_no=result[0].machine_no;
      }
    })
  }
  
  canSubmit():boolean{
        if (this.dialysisSessionDetail == null){
            return false;
          }

if (this.dialysisSessionDetail.encounter_id == null || this.dialysisSessionDetail.encounter_id  == "") {
            return false;
        } 
if (this.dialysisSessionDetail.patient_name == null || this.dialysisSessionDetail.patient_name  == "") {
            return false;
        } 
if (this.dialysisSessionDetail.card_id == null ) {
            return false;
        } 
if (this.dialysisSessionDetail.dialysis_id == null ) {
            return false;
        } 
if (this.dialysisSessionDetail.machine_no == null || this.dialysisSessionDetail.machine_no  == "") {
            return false;
        } 
 
if (this.dialysisSessionDetail.service == null) {
            return false;
        }
      return true;
 }
 }