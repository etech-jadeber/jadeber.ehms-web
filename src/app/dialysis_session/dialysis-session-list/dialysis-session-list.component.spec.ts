import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisSessionListComponent } from './dialysis-session-list.component';

describe('DialysisSessionListComponent', () => {
  let component: DialysisSessionListComponent;
  let fixture: ComponentFixture<DialysisSessionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisSessionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisSessionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
