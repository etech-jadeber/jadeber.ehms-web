import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisSessionDetail, DialysisSessionSummary, DialysisSessionSummaryPartialList } from '../dialysis_session.model';
import { DialysisSessionPersist } from '../dialysis_session.persist';
import { DialysisSessionNavigator } from '../dialysis_session.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DialysisMachinePersist } from 'src/app/dialysis_machine/dialysis_machine.persist';
import { DialysisMachineDetail } from 'src/app/dialysis_machine/dialysis_machine.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { payment_status, session_status } from 'src/app/app.enums';
@Component({
  selector: 'app-dialysis_session-list',
  templateUrl: './dialysis-session-list.component.html',
  styleUrls: ['./dialysis-session-list.component.scss']
})
export class DialysisSessionListComponent implements OnInit {
  dialysisSessionsData: DialysisSessionSummary[] = [];
  dialysisSessionsTotalCount: number = 0;
  dialysisSessionSelectAll:boolean = false;
  dialysisSessionSelection: DialysisSessionSummary[] = [];
  machine: DialysisMachineDetail[]=[];
 dialysisSessionsDisplayedColumns: string[] = ["select","action","patient_name","card_id","dialysis_id","machine_no","service" ];
  dialysisSessionSearchTextBox: FormControl = new FormControl();
  dialysisSessionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysisSessionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysisSessionsSort: MatSort;
  @Input() encounter_id=TCUtilsString.getInvalidId();
  invalid_id=TCUtilsString.getInvalidId();
  session_status=session_status;
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisSessionPersist: DialysisSessionPersist,
                public dialysisSessionNavigator: DialysisSessionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public dialysisMachinePersist: DialysisMachinePersist,
                public paymentPersist: PaymentPersist,

    ) {

        this.tcAuthorization.requireRead("dialysis_sessions");
       this.dialysisSessionSearchTextBox.setValue(dialysisSessionPersist.dialysisSessionSearchText);
      //delay subsequent keyup events
      this.dialysisSessionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisSessionPersist.dialysisSessionSearchText = value;
        this.searchDialysisSessions();
      });
    } ngOnInit() {
   
      this.dialysisSessionsSort.sortChange.subscribe(() => {
        this.dialysisSessionsPaginator.pageIndex = 0;
        this.searchDialysisSessions(true);
      });

      this.dialysisSessionsPaginator.page.subscribe(() => {
        this.searchDialysisSessions(true);
      });
      //start by loading items
      this.searchDialysisSessions();
    }

  searchDialysisSessions(isPagination:boolean = false): void {


    let paginator = this.dialysisSessionsPaginator;
    let sorter = this.dialysisSessionsSort;

    this.dialysisSessionIsLoading = true;
    this.dialysisSessionSelection = [];
    this.dialysisSessionPersist.encounter_id=this.encounter_id;
    this.dialysisSessionPersist.searchDialysisSession(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisSessionSummaryPartialList) => {
      this.dialysisSessionsData = partialList.data;
      if (partialList.total != -1) {
        this.dialysisSessionsTotalCount = partialList.total;
        for(let machine of this.dialysisSessionsData){
          this.dialysisMachinePersist.getDialysisMachine(machine.machine_no).subscribe((result)=>{
            if(result){          
              this.machine.push(result);
            }
          });
        }
      }
      this.dialysisSessionIsLoading = false;
    }, error => {
      this.dialysisSessionIsLoading = false;
    });

  } downloadDialysisSessions(): void {
    if(this.dialysisSessionSelectAll){
         this.dialysisSessionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download dialysis_session", true);
      });
    }
    else{
        this.dialysisSessionPersist.download(this.tcUtilsArray.idsList(this.dialysisSessionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download dialysis_session",true);
            });
        }
  }
  completeDialysisSession(session:DialysisSessionDetail):void{
      this.dialysisSessionPersist.dialysisSessionDo(session.id,"complete_session",{}).subscribe((result)=>{
        this.searchDialysisSessions();
      })
  }
  loadDialysisSessionDetail(session:DialysisSessionDetail): void {
    this.paymentPersist.getPayment(session.payment_id).subscribe((result)=>{
      if(result){
        if(result.payment_status==payment_status.not_paid){
           this.tcNotification.error("dialysis fee is not paid");
        }
        else{
          this.router.navigate([this.dialysisSessionNavigator.dialysisSessionUrl(session.id)]);
        }
      }
    })
  }
addDialysisSession(): void {
    let dialogRef = this.dialysisSessionNavigator.addDialysisSession();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDialysisSessions();
      }
    });
  }
  getMachineNo(id:string):string{
    let machine_no="";
    this.dialysisMachinePersist.getDialysisMachine(id).subscribe((result)=>{
      if(result){
        machine_no=result.machine_no;
      }
    });
    return machine_no;
  }
  editDialysisSession(item: DialysisSessionSummary) {
    let dialogRef = this.dialysisSessionNavigator.editDialysisSession(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDialysisSession(item: DialysisSessionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("dialysis_session");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.dialysisSessionPersist.deleteDialysisSession(item.id).subscribe(response => {
          this.tcNotification.success("dialysis_session deleted");
          this.searchDialysisSessions();
        }, error => {
        });
      }

    });
  }  
  ngOnDestroy():void{
    this.dialysisSessionPersist.encounter_id=TCUtilsString.getInvalidId();
  }
  
  back():void{
      this.location.back();
    }
}