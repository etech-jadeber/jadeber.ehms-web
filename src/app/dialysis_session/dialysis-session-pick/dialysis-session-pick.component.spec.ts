import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialysisSessionPickComponent } from './dialysis-session-pick.component';

describe('DialysisSessionPickComponent', () => {
  let component: DialysisSessionPickComponent;
  let fixture: ComponentFixture<DialysisSessionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialysisSessionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialysisSessionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
