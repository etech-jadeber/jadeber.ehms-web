import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DialysisSessionSummary, DialysisSessionSummaryPartialList } from '../dialysis_session.model';
import { DialysisSessionPersist } from '../dialysis_session.persist';
import { DialysisSessionNavigator } from '../dialysis_session.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dialysis_session-pick',
  templateUrl: './dialysis-session-pick.component.html',
  styleUrls: ['./dialysis-session-pick.component.scss']
})export class DialysisSessionPickComponent implements OnInit {
  dialysisSessionsData: DialysisSessionSummary[] = [];
  dialysisSessionsTotalCount: number = 0;
  dialysisSessionSelectAll:boolean = false;
  dialysisSessionSelection: DialysisSessionSummary[] = [];

 dialysisSessionsDisplayedColumns: string[] = ["select", ,"encounter_id","patient_name","card_id","dialysis_id","machine_no","service" ];
  dialysisSessionSearchTextBox: FormControl = new FormControl();
  dialysisSessionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dialysisSessionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dialysisSessionsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialysisSessionPersist: DialysisSessionPersist,
                public dialysisSessionNavigator: DialysisSessionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<DialysisSessionPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("dialysis_sessions");
       this.dialysisSessionSearchTextBox.setValue(dialysisSessionPersist.dialysisSessionSearchText);
      //delay subsequent keyup events
      this.dialysisSessionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dialysisSessionPersist.dialysisSessionSearchText = value;
        this.searchDialysis_sessions();
      });
    } ngOnInit() {
   
      this.dialysisSessionsSort.sortChange.subscribe(() => {
        this.dialysisSessionsPaginator.pageIndex = 0;
        this.searchDialysis_sessions(true);
      });

      this.dialysisSessionsPaginator.page.subscribe(() => {
        this.searchDialysis_sessions(true);
      });
      //start by loading items
      this.searchDialysis_sessions();
    }

  searchDialysis_sessions(isPagination:boolean = false): void {


    let paginator = this.dialysisSessionsPaginator;
    let sorter = this.dialysisSessionsSort;

    this.dialysisSessionIsLoading = true;
    this.dialysisSessionSelection = [];

    this.dialysisSessionPersist.searchDialysisSession(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DialysisSessionSummaryPartialList) => {
      this.dialysisSessionsData = partialList.data;
      if (partialList.total != -1) {
        this.dialysisSessionsTotalCount = partialList.total;
      }
      this.dialysisSessionIsLoading = false;
    }, error => {
      this.dialysisSessionIsLoading = false;
    });

  }
  markOneItem(item: DialysisSessionSummary) {
    if(!this.tcUtilsArray.containsId(this.dialysisSessionSelection,item.id)){
          this.dialysisSessionSelection = [];
          this.dialysisSessionSelection.push(item);
        }
        else{
          this.dialysisSessionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.dialysisSessionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }