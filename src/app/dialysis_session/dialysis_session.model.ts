import {TCId} from "../tc/models";
    export class DialysisSessionSummary extends TCId {
      encounter_id:string;
      patient_name:string;
      card_id:number;
      dialysis_id:number;
      machine_no:string;
      service:number;
      payment_id:string;
      status:number;
      session_id:number;
    }
    export class DialysisSessionSummaryPartialList {
      data: DialysisSessionSummary[];
      total: number;
    }
    export class DialysisSessionDetail extends DialysisSessionSummary {
    }
    
    export class DialysisSessionDashboard {
      total: number;
    }