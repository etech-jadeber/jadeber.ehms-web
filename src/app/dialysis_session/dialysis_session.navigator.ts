import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DialysisSessionEditComponent} from "./dialysis-session-edit/dialysis-session-edit.component";
import {DialysisSessionPickComponent} from "./dialysis-session-pick/dialysis-session-pick.component";
import { DialysisSummary } from "../dialysis/dialysis.model";


@Injectable({
  providedIn: 'root'
})

export class DialysisSessionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  dialysisSessionsUrl(): string {
    return "/dialysis_sessions";
  }

  dialysisOrderUrl():string{
    return "/dialysis_order"
  }

  dialysisSessionUrl(id: string): string {
    return "/dialysis_sessions/" + id;
  }

  viewDialysisSessions(): void {
    this.router.navigateByUrl(this.dialysisSessionsUrl());
  }
  viewDialysisOrder(): void {
    this.router.navigateByUrl(this.dialysisOrderUrl());
  }
  viewDialysisSession(id: string): void {
    this.router.navigateByUrl(this.dialysisSessionUrl(id));
  }

  editDialysisSession(id: string): MatDialogRef<DialysisSessionEditComponent> {
    const dialogRef = this.dialog.open(DialysisSessionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDialysisSession(dialysis_id:string=""): MatDialogRef<DialysisSessionEditComponent> {
    const dialogRef = this.dialog.open(DialysisSessionEditComponent, {
          data: new TCIdMode(dialysis_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickDialysisSessions(selectOne: boolean=false): MatDialogRef<DialysisSessionPickComponent> {
      const dialogRef = this.dialog.open(DialysisSessionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}