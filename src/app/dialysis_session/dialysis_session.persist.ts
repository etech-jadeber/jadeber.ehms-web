import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {DialysisSessionDashboard, DialysisSessionDetail, DialysisSessionSummaryPartialList} from "./dialysis_session.model";
import { dialysis_service_type } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DialysisSessionPersist {
 dialysisSessionSearchText: string = "";
 selected_index:number;
 tab_index:number;
 encounter_id=TCUtilsString.getInvalidId();
 dialysis_service_type:TCEnum[]=[
    new TCEnum(dialysis_service_type.dialysis_service,"Dialysis Service")
 ]

 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.dialysisSessionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchDialysisSession(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DialysisSessionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("dialysis_session", this.dialysisSessionSearchText, pageSize, pageIndex, sort, order);
    url=TCUtilsString.appendUrlParameter(url,"encounter_id",this.encounter_id);
    return this.http.get<DialysisSessionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_session/do", new TCDoParam("download_all", this.filters()));
  }

  dialysisSessionDashboard(): Observable<DialysisSessionDashboard> {
    return this.http.get<DialysisSessionDashboard>(environment.tcApiBaseUri + "dialysis_session/dashboard");
  }

  getDialysisSession(id: string): Observable<DialysisSessionDetail> {
    return this.http.get<DialysisSessionDetail>(environment.tcApiBaseUri + "dialysis_session/" + id);
  } addDialysisSession(item: DialysisSessionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_session/", item);
  }

  updateDialysisSession(item: DialysisSessionDetail): Observable<DialysisSessionDetail> {
    return this.http.patch<DialysisSessionDetail>(environment.tcApiBaseUri + "dialysis_session/" + item.id, item);
  }

  deleteDialysisSession(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "dialysis_session/" + id);
  }
 dialysisSessionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysis_session/do", new TCDoParam(method, payload));
  }

  dialysisSessionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dialysis_session/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_session/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "dialysis_session/" + id + "/do", new TCDoParam("print", {}));
  }


}