import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchOrRejectDetailComponent } from './dispatch-or-reject-detail.component';

describe('DispatchOrRejectDetailComponent', () => {
  let component: DispatchOrRejectDetailComponent;
  let fixture: ComponentFixture<DispatchOrRejectDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispatchOrRejectDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchOrRejectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
