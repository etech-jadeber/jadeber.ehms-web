import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { DispatchOrRejectDetail } from '../dispatch_or_reject.model';
import { DispatchOrRejectPersist } from '../dispatch_or_reject.persist';
import { DispatchOrRejectNavigator } from '../dispatch_or_reject.navigator';

export enum DispatchOrRejectTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-dispatch_or_reject-detail',
  templateUrl: './dispatch-or-reject-detail.component.html',
  styleUrls: ['./dispatch-or-reject-detail.component.css'],
})
export class DispatchOrRejectDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  dispatchOrRejectIsLoading: boolean = false;

  DispatchOrRejectTabs: typeof DispatchOrRejectTabs = DispatchOrRejectTabs;
  activeTab: DispatchOrRejectTabs = DispatchOrRejectTabs.overview;
  //basics
  dispatchOrRejectDetail: DispatchOrRejectDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public dispatchOrRejectNavigator: DispatchOrRejectNavigator,
    public dispatchOrRejectPersist: DispatchOrRejectPersist
  ) {
    this.tcAuthorization.requireRead('dispatch_or_reject');
    this.dispatchOrRejectDetail = new DispatchOrRejectDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('dispatch_or_reject');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.dispatchOrRejectIsLoading = true;
    this.dispatchOrRejectPersist.getDispatchOrReject(id).subscribe(
      (dispatchOrRejectDetail) => {
        this.dispatchOrRejectDetail = dispatchOrRejectDetail;
        this.dispatchOrRejectIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.dispatchOrRejectIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.dispatchOrRejectDetail.id);
  }
  editDispatchOrReject(): void {
    let modalRef = this.dispatchOrRejectNavigator.editDispatchOrReject(
      this.dispatchOrRejectDetail.id
    );
    modalRef.afterClosed().subscribe(
      (dispatchOrRejectDetail) => {
        TCUtilsAngular.assign(
          this.dispatchOrRejectDetail,
          dispatchOrRejectDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.dispatchOrRejectPersist
      .print(this.dispatchOrRejectDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(
          printJob.id,
          'print dispatch_or_reject',
          true
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
