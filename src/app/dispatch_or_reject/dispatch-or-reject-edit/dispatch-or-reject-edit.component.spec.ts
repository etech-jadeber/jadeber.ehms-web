import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchOrRejectEditComponent } from './dispatch-or-reject-edit.component';

describe('DispatchOrRejectEditComponent', () => {
  let component: DispatchOrRejectEditComponent;
  let fixture: ComponentFixture<DispatchOrRejectEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispatchOrRejectEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchOrRejectEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
