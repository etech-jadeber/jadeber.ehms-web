import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DispatchOrRejectDetail, DispatchOrRejectSummaryPartialList } from '../dispatch_or_reject.model';
import { DispatchOrRejectPersist } from '../dispatch_or_reject.persist';
import { Item_In_StorePersist } from 'src/app/item_in_stores/item_in_store.persist';
import { RequestPersist } from 'src/app/requests/request.persist';
import { Item_In_StoreDetail, Item_In_StoreSummary, Item_In_StoreSummaryPartialList } from 'src/app/item_in_stores/item_in_store.model';
import { TCAppInit } from 'src/app/tc/app-init';
import { EmployeeSelfPersist } from 'src/app/storeemployees/employee-self.persist';
@Component({
  selector: 'app-dispatch_or_reject-edit',
  templateUrl: './dispatch-or-reject-edit.component.html',
  styleUrls: ['./dispatch-or-reject-edit.component.css'],
})
export class DispatchOrRejectEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  batch_no:string;
  dispatchOrRejectDetail: DispatchOrRejectDetail;
  item_batch: Item_In_StoreSummary[];
  dispatch_id: string;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<DispatchOrRejectEditComponent>,
    public dispatchOrRejectPersist: DispatchOrRejectPersist,
    public requestPersist: RequestPersist,
    public tcUtilsDate: TCUtilsDate,
    public employeeSelfPersist: EmployeeSelfPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      // this.tcAuthorization.requireCreate('dispatch_or_reject');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'dispatch_or_reject';
      this.dispatchOrRejectDetail = new DispatchOrRejectDetail();

      //set necessary defaults
    } else {
      // this.tcAuthorization.requireUpdate('dispatch_or_reject');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'dispatch_or_reject';
      this.isLoadingResults = true;
      this.dispatchOrRejectPersist
        .getDispatchOrReject(this.idMode.id)
        .subscribe(
          (dispatchOrRejectDetail) => {
            this.dispatchOrRejectDetail = dispatchOrRejectDetail[0];

            this.isLoadingResults = false; 
          },
          (error) => {
            console.log(error);
            this.isLoadingResults = false;
          }
        );

         this.requestPersist
                .requestDo(this.idMode.id, 'get_Item_Batch', {})
                .subscribe((response: Item_In_StoreSummaryPartialList) => {
                  this.item_batch = response.data;
                  if(response.data.length==0){
                    this.dialogRef.close();
                    this.tcNotification.error("there is no such item in the main store");
                  }
                });

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.requestPersist.getRequest(this.idMode.id).subscribe((response) => {
      this.dispatchOrRejectDetail.item_id = response.item_id;
      this.dispatchOrRejectDetail.request_id = response.id;
      this.dispatchOrRejectDetail.store_id = response.requestee_store_id;
      this.dispatchOrRejectDetail.batch_no = 0;
      this.dispatchOrRejectDetail.employee_id = TCAppInit.userInfo.user_id;
      this.dispatchOrRejectDetail.status = 102;


      this.dispatchOrRejectPersist
        .addDispatchOrReject(this.dispatchOrRejectDetail)
        .subscribe(
          (value) => {
            this.tcNotification.success('dispatchOrReject added');
            this.dispatchOrRejectDetail.id = value.id;
            this.dialogRef.close(this.dispatchOrRejectDetail);
          },
          (error) => {
            this.isLoadingResults = false;
          }
        );

        response.status = 102;

          this.employeeSelfPersist.updateRequest(response).subscribe(
            (value) => {
              this.tcNotification.error('Request rejected');
              // this.dialogRef.close(this.requestDetail);
              
            },
            (error) => {
              // this.isLoadingResults = false;
            }
          );
    });

    
  }

  onUpdate(): void {
    this.isLoadingResults = true;
      this.requestPersist.getRequest(this.idMode.id).subscribe((response) => {
      this.dispatchOrRejectDetail.item_id = response.item_id;
      this.dispatchOrRejectDetail.request_id = response.id;
      this.dispatchOrRejectDetail.store_id = response.requestee_store_id;
      this.dispatchOrRejectDetail.reason = "";
      this.dispatchOrRejectDetail.employee_id = TCAppInit.userInfo.user_id;
      this.dispatchOrRejectDetail.status = 103; 
      this.dispatchOrRejectDetail.batch_no = parseInt(this.batch_no);
      // this.dispatchOrRejectDetail.id = this.dispatch_id;

    

          this.dispatchOrRejectPersist.updateDispatchOrReject(this.dispatchOrRejectDetail).subscribe(
      (value) => {
          this.tcNotification.success('dispatch_or_reject updated');
          response.status = 103;
          
          this.isLoadingResults = false;
        this.dialogRef.close(this.dispatchOrRejectDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );

    });

  
  }
  canSubmit(): boolean {
    if (this.dispatchOrRejectDetail == null) {
      return false;
    }
    if (this.batch_no==null&&this.isEdit()) {
      return false;
    }
    if (
      this.dispatchOrRejectDetail.reason == null ||
      this.dispatchOrRejectDetail.reason == '' && this.isNew()
    ) {
      return false;
    }
    return true;
  }
}
