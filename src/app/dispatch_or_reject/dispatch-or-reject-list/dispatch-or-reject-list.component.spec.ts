import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchOrRejectListComponent } from './dispatch-or-reject-list.component';

describe('DispatchOrRejectListComponent', () => {
  let component: DispatchOrRejectListComponent;
  let fixture: ComponentFixture<DispatchOrRejectListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispatchOrRejectListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchOrRejectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
