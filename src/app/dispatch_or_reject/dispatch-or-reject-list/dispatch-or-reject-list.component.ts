import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DispatchOrRejectSummary,
  DispatchOrRejectSummaryPartialList,
} from '../dispatch_or_reject.model';
import { DispatchOrRejectPersist } from '../dispatch_or_reject.persist';
import { DispatchOrRejectNavigator } from '../dispatch_or_reject.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dispatch_or_reject-list',
  templateUrl: './dispatch-or-reject-list.component.html',
  styleUrls: ['./dispatch-or-reject-list.component.css'],
})
export class DispatchOrRejectListComponent implements OnInit {
  dispatchOrRejectsData: DispatchOrRejectSummary[] = [];
  dispatchOrRejectsTotalCount: number = 0;
  dispatchOrRejectSelectAll: boolean = false;
  dispatchOrRejectSelection: DispatchOrRejectSummary[] = [];

  dispatchOrRejectsDisplayedColumns: string[] = [
    'select',
    'action',
    'link',
    'item_id',
    'store_id',
    'employee_id',
    'batch_no',
    'reason',
  ];
  dispatchOrRejectSearchTextBox: FormControl = new FormControl();
  dispatchOrRejectIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  dispatchOrRejectsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) dispatchOrRejectsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dispatchOrRejectPersist: DispatchOrRejectPersist,
    public dispatchOrRejectNavigator: DispatchOrRejectNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob
  ) {
    this.tcAuthorization.requireRead('dispatch_or_reject');
    this.dispatchOrRejectSearchTextBox.setValue(
      dispatchOrRejectPersist.dispatchOrRejectSearchText
    );
    //delay subsequent keyup events
    this.dispatchOrRejectSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.dispatchOrRejectPersist.dispatchOrRejectSearchText = value;
        this.searchDispatchOrRejects();
      });
  }
  ngOnInit() {
    this.dispatchOrRejectsSort.sortChange.subscribe(() => {
      this.dispatchOrRejectsPaginator.pageIndex = 0;
      this.searchDispatchOrRejects(true);
    });

    this.dispatchOrRejectsPaginator.page.subscribe(() => {
      this.searchDispatchOrRejects(true);
    });
    //start by loading items
    this.searchDispatchOrRejects();
  }

  searchDispatchOrRejects(isPagination: boolean = false): void {
    let paginator = this.dispatchOrRejectsPaginator;
    let sorter = this.dispatchOrRejectsSort;

    this.dispatchOrRejectIsLoading = true;
    this.dispatchOrRejectSelection = [];

    this.dispatchOrRejectPersist
      .searchDispatchOrReject(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DispatchOrRejectSummaryPartialList) => {
          this.dispatchOrRejectsData = partialList.data;
          if (partialList.total != -1) {
            this.dispatchOrRejectsTotalCount = partialList.total;
          }
          this.dispatchOrRejectIsLoading = false;
        },
        (error) => {
          this.dispatchOrRejectIsLoading = false;
        }
      );
  }
  downloadDispatchOrRejects(): void {
    if (this.dispatchOrRejectSelectAll) {
      this.dispatchOrRejectPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download dispatch_or_reject',
          true
        );
      });
    } else {
      this.dispatchOrRejectPersist
        .download(this.tcUtilsArray.idsList(this.dispatchOrRejectSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download dispatch_or_reject',
            true
          );
        });
    }
  }
  addDispatchOrReject(): void {
    let dialogRef = this.dispatchOrRejectNavigator.addDispatchOrReject();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchDispatchOrRejects();
      }
    });
  }

  editDispatchOrReject(item: DispatchOrRejectSummary) {
    let dialogRef = this.dispatchOrRejectNavigator.editDispatchOrReject(
      item.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchDispatchOrRejects();
      }
    });
  }

  deleteDispatchOrReject(item: DispatchOrRejectSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('dispatch_or_reject');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.dispatchOrRejectPersist.deleteDispatchOrReject(item.id).subscribe(
          (response) => {
            this.tcNotification.success('dispatch_or_reject deleted');
            this.searchDispatchOrRejects();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
