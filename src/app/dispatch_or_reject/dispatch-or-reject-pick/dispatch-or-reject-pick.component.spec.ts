import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchOrRejectPickComponent } from './dispatch-or-reject-pick.component';

describe('DispatchOrRejectPickComponent', () => {
  let component: DispatchOrRejectPickComponent;
  let fixture: ComponentFixture<DispatchOrRejectPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispatchOrRejectPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchOrRejectPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
