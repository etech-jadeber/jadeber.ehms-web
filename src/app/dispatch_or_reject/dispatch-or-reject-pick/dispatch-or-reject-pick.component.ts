import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
// import {
//   MatSort,
//   MatPaginator,
//   MatDialogRef,
//   MAT_DIALOG_DATA,
// } from '@angular/material';
import {MatDialogRef,MAT_DIALOG_DATA} from '@angular/material/dialog'
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DispatchOrRejectSummary,
  DispatchOrRejectSummaryPartialList,
} from '../dispatch_or_reject.model';
import { DispatchOrRejectPersist } from '../dispatch_or_reject.persist';
import { DispatchOrRejectNavigator } from '../dispatch_or_reject.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dispatch_or_reject-pick',
  templateUrl: './dispatch-or-reject-pick.component.html',
  styleUrls: ['./dispatch-or-reject-pick.component.css'],
})
export class DispatchOrRejectPickComponent implements OnInit {
  dispatchOrRejectsData: DispatchOrRejectSummary[] = [];
  dispatchOrRejectsTotalCount: number = 0;
  dispatchOrRejectSelectAll: boolean = false;
  dispatchOrRejectSelection: DispatchOrRejectSummary[] = [];

  dispatchOrRejectsDisplayedColumns: string[] = [
    'select',
    ,
    'item_id',
    'store_id',
    'employee_id',
    'batch_no',
    'reason',
  ];
  dispatchOrRejectSearchTextBox: FormControl = new FormControl();
  dispatchOrRejectIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  dispatchOrRejectsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) dispatchOrRejectsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dispatchOrRejectPersist: DispatchOrRejectPersist,
    public dispatchOrRejectNavigator: DispatchOrRejectNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<DispatchOrRejectPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('dispatch_or_reject');
    this.dispatchOrRejectSearchTextBox.setValue(
      dispatchOrRejectPersist.dispatchOrRejectSearchText
    );
    //delay subsequent keyup events
    this.dispatchOrRejectSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.dispatchOrRejectPersist.dispatchOrRejectSearchText = value;
        this.searchDispatch_or_rejects();
      });
  }
  ngOnInit() {
    this.dispatchOrRejectsSort.sortChange.subscribe(() => {
      this.dispatchOrRejectsPaginator.pageIndex = 0;
      this.searchDispatch_or_rejects(true);
    });

    this.dispatchOrRejectsPaginator.page.subscribe(() => {
      this.searchDispatch_or_rejects(true);
    });
    //start by loading items
    this.searchDispatch_or_rejects();
  }

  searchDispatch_or_rejects(isPagination: boolean = false): void {
    let paginator = this.dispatchOrRejectsPaginator;
    let sorter = this.dispatchOrRejectsSort;

    this.dispatchOrRejectIsLoading = true;
    this.dispatchOrRejectSelection = [];

    this.dispatchOrRejectPersist
      .searchDispatchOrReject(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DispatchOrRejectSummaryPartialList) => {
          this.dispatchOrRejectsData = partialList.data;
          if (partialList.total != -1) {
            this.dispatchOrRejectsTotalCount = partialList.total;
          }
          this.dispatchOrRejectIsLoading = false;
        },
        (error) => {
          this.dispatchOrRejectIsLoading = false;
        }
      );
  }
  markOneItem(item: DispatchOrRejectSummary) {
    if (
      !this.tcUtilsArray.containsId(this.dispatchOrRejectSelection, item.id)
    ) {
      this.dispatchOrRejectSelection = [];
      this.dispatchOrRejectSelection.push(item);
    } else {
      this.dispatchOrRejectSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.dispatchOrRejectSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
