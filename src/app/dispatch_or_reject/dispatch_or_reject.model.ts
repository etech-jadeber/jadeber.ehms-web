import { TCId } from '../tc/models';
export class DispatchOrRejectSummary extends TCId {
  item_id: string;
  store_id: string;
  employee_id: string;
  batch_no: number;
  reason: string;
  request_id: string;
  status: number;
}
export class DispatchOrRejectSummaryPartialList {
  data: DispatchOrRejectSummary[];
  total: number;
}
export class DispatchOrRejectDetail extends DispatchOrRejectSummary {}

export class DispatchOrRejectDashboard {
  total: number;
}
