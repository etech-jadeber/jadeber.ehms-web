import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {MatDialogRef,MatDialog} from '@angular/material/dialog'

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { DispatchOrRejectEditComponent } from './dispatch-or-reject-edit/dispatch-or-reject-edit.component';
import { DispatchOrRejectPickComponent } from './dispatch-or-reject-pick/dispatch-or-reject-pick.component';
import { DispatchOrRejectSummaryPartialList } from './dispatch_or_reject.model';
@Injectable({
  providedIn: 'root',
})
export class DispatchOrRejectNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  dispatchOrRejectsUrl(): string {
    return '/dispatch_or_rejects';
  }

  dispatchOrRejectUrl(id: string): string {
    return '/dispatch_or_rejects/' + id;
  }

  viewDispatchOrRejects(): void {
    this.router.navigateByUrl(this.dispatchOrRejectsUrl());
  }

  viewDispatchOrReject(id: string): void {
    this.router.navigateByUrl(this.dispatchOrRejectUrl(id));
  }

  editDispatchOrReject(
    id: string
  ): MatDialogRef<DispatchOrRejectEditComponent> {
    const dialogRef = this.dialog.open(DispatchOrRejectEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addDispatchOrReject(item_id: string = null): MatDialogRef<DispatchOrRejectEditComponent> {
    const dialogRef = this.dialog.open(DispatchOrRejectEditComponent, {
      data: new TCIdMode(item_id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickDispatchOrRejects( 
    selectOne: boolean = false
  ): MatDialogRef<DispatchOrRejectPickComponent> {
    const dialogRef = this.dialog.open(DispatchOrRejectPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
