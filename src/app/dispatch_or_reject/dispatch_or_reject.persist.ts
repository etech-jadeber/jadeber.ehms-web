import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  DispatchOrRejectDashboard,
  DispatchOrRejectDetail,
  DispatchOrRejectSummaryPartialList,
} from './dispatch_or_reject.model';

@Injectable({
  providedIn: 'root',
})
export class DispatchOrRejectPersist {
  dispatchOrRejectSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.dispatchOrRejectSearchText;
    //add custom filters
    return fltrs;
  }

  searchDispatchOrReject(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<DispatchOrRejectSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'dispatch_or_reject',
      this.dispatchOrRejectSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    
    return this.http.get<DispatchOrRejectSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'dispatch_or_reject/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  dispatchOrRejectDashboard(): Observable<DispatchOrRejectDashboard> {
    return this.http.get<DispatchOrRejectDashboard>(
      environment.tcApiBaseUri + 'dispatch_or_reject/dashboard'
    );
  }

  getDispatchOrReject(id: string): Observable<DispatchOrRejectDetail> {
    console.log("get id is ", id)
    return this.http.get<DispatchOrRejectDetail>(
      environment.tcApiBaseUri + 'dispatch_or_reject/' + id
    );
  }
  addDispatchOrReject(item: DispatchOrRejectDetail): Observable<TCId> {
    console.log("the item to be added is ",item)
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'dispatch_or_reject/',
      item
    );
  }

  updateDispatchOrReject(
    item: DispatchOrRejectDetail
  ): Observable<DispatchOrRejectDetail> {
    console.log("item.id value is "+ item.id)
    return this.http.patch<DispatchOrRejectDetail>(
      environment.tcApiBaseUri + 'dispatch_or_reject/' + item.id,
      item
    );
  }

  deleteDispatchOrReject(id: string): Observable<{}> {
    return this.http.delete(
      environment.tcApiBaseUri + 'dispatch_or_reject/' + id
    );
  }
  dispatchOrRejectsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'dispatch_or_reject/do',
      new TCDoParam(method, payload)
    );
  }

  dispatchOrRejectDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'dispatch_or_rejects/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'dispatch_or_reject/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'dispatch_or_reject/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
