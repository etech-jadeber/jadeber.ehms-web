import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispenseHistoryDetailComponent } from './dispense-history-detail.component';

describe('DispenseHistoryDetailComponent', () => {
  let component: DispenseHistoryDetailComponent;
  let fixture: ComponentFixture<DispenseHistoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispenseHistoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispenseHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
