import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DispenseHistoryDetail} from "../dispense_history.model";
import {DispenseHistoryPersist} from "../dispense_history.persist";
import {DispenseHistoryNavigator} from "../dispense_history.navigator";

export enum DispenseHistoryTabs {
  overview,
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-dispense_history-detail',
  templateUrl: './dispense-history-detail.component.html',
  styleUrls: ['./dispense-history-detail.component.scss']
})
export class DispenseHistoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  dispenseHistoryIsLoading:boolean = false;
  
  DispenseHistoryTabs: typeof DispenseHistoryTabs = DispenseHistoryTabs;
  activeTab: DispenseHistoryTabs = DispenseHistoryTabs.overview;
  //basics
  dispenseHistoryDetail: DispenseHistoryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public dispenseHistoryNavigator: DispenseHistoryNavigator,
              public  dispenseHistoryPersist: DispenseHistoryPersist) {
    this.tcAuthorization.requireRead("dispense_history");
    this.dispenseHistoryDetail = new DispenseHistoryDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("dispense_history");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.dispenseHistoryIsLoading = true;
    this.dispenseHistoryPersist.getDispenseHistory(id).subscribe(dispenseHistoryDetail => {
          this.dispenseHistoryDetail = dispenseHistoryDetail;
          this.dispenseHistoryIsLoading = false;
        }, error => {
          console.error(error);
          this.dispenseHistoryIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.dispenseHistoryDetail.id);
  }
  editDispenseHistory(): void {
    let modalRef = this.dispenseHistoryNavigator.editDispenseHistory(this.dispenseHistoryDetail.id);
    modalRef.afterClosed().subscribe(dispenseHistoryDetail => {
      TCUtilsAngular.assign(this.dispenseHistoryDetail, dispenseHistoryDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.dispenseHistoryPersist.print(this.dispenseHistoryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print dispense_history", true);
      });
    }

  back():void{
      this.location.back();
    }

}