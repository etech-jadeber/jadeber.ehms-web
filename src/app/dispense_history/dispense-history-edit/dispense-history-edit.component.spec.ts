import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispenseHistoryEditComponent } from './dispense-history-edit.component';

describe('DispenseHistoryEditComponent', () => {
  let component: DispenseHistoryEditComponent;
  let fixture: ComponentFixture<DispenseHistoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispenseHistoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispenseHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
