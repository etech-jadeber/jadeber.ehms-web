import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DispenseHistoryDetail } from '../dispense_history.model';import { DispenseHistoryPersist } from '../dispense_history.persist';@Component({
  selector: 'app-dispense_history-edit',
  templateUrl: './dispense-history-edit.component.html',
  styleUrls: ['./dispense-history-edit.component.scss']
})export class DispenseHistoryEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  dispenseHistoryDetail: DispenseHistoryDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DispenseHistoryEditComponent>,
              public  persist: DispenseHistoryPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("dispense_history");
      this.title = this.appTranslation.getText("general","new") +  " " + "dispense_history";
      this.dispenseHistoryDetail = new DispenseHistoryDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("dispense_history");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "dispense_history";
      this.isLoadingResults = true;
      this.persist.getDispenseHistory(this.idMode.id).subscribe(dispenseHistoryDetail => {
        this.dispenseHistoryDetail = dispenseHistoryDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addDispenseHistory(this.dispenseHistoryDetail).subscribe(value => {
      this.tcNotification.success("dispenseHistory added");
      this.dispenseHistoryDetail.id = value.id;
      this.dialogRef.close(this.dispenseHistoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateDispenseHistory(this.dispenseHistoryDetail).subscribe(value => {
      this.tcNotification.success("dispense_history updated");
      this.dialogRef.close(this.dispenseHistoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.dispenseHistoryDetail == null){
            return false;
          }
        if (this.dispenseHistoryDetail.patient_id == null || this.dispenseHistoryDetail.patient_id  == "") {
                    return false;
                } 
        if (this.dispenseHistoryDetail.provider_id == null || this.dispenseHistoryDetail.provider_id  == "") {
                    return false;
                } 
        if (this.dispenseHistoryDetail.item_in_store_id == null || this.dispenseHistoryDetail.item_in_store_id  == "") {
                    return false;
                } 
        if (this.dispenseHistoryDetail.date == null) {
                    return false;
                }
        if (this.dispenseHistoryDetail.prescription_id == null || this.dispenseHistoryDetail.prescription_id  == "") {
                    return false;
                } 
        if (this.dispenseHistoryDetail.patient_type == null) {
                    return false;
                }
        return true;
 }
 }