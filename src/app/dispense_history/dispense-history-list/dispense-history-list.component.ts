import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DispenseHistorySummary, DispenseHistorySummaryPartialList } from '../dispense_history.model';
import { DispenseHistoryPersist } from '../dispense_history.persist';
import { DispenseHistoryNavigator } from '../dispense_history.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dispense_history-list',
  templateUrl: './dispense-history-list.component.html',
  styleUrls: ['./dispense-history-list.component.scss']
})export class DispenseHistoryListComponent implements OnInit {
  dispenseHistorysData: DispenseHistorySummary[] = [];
  dispenseHistorysTotalCount: number = 0;
  dispenseHistorySelectAll:boolean = false;
  dispenseHistorySelection: DispenseHistorySummary[] = [];

 dispenseHistorysDisplayedColumns: string[] = ["select","action","link" ,"patient_id","provider_id","item_in_store_id","date","prescription_id","patient_type" ];
  dispenseHistorySearchTextBox: FormControl = new FormControl();
  dispenseHistoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dispenseHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dispenseHistorysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dispenseHistoryPersist: DispenseHistoryPersist,
                public dispenseHistoryNavigator: DispenseHistoryNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("dispense_history");
       this.dispenseHistorySearchTextBox.setValue(dispenseHistoryPersist.dispenseHistorySearchText);
      //delay subsequent keyup events
      this.dispenseHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dispenseHistoryPersist.dispenseHistorySearchText = value;
        this.searchDispenseHistorys();
      });
    } ngOnInit() {
   
      this.dispenseHistorysSort.sortChange.subscribe(() => {
        this.dispenseHistorysPaginator.pageIndex = 0;
        this.searchDispenseHistorys(true);
      });

      this.dispenseHistorysPaginator.page.subscribe(() => {
        this.searchDispenseHistorys(true);
      });
      //start by loading items
      this.searchDispenseHistorys();
    }

  searchDispenseHistorys(isPagination:boolean = false): void {


    let paginator = this.dispenseHistorysPaginator;
    let sorter = this.dispenseHistorysSort;

    this.dispenseHistoryIsLoading = true;
    this.dispenseHistorySelection = [];

    this.dispenseHistoryPersist.searchDispenseHistory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DispenseHistorySummaryPartialList) => {
      this.dispenseHistorysData = partialList.data;
      if (partialList.total != -1) {
        this.dispenseHistorysTotalCount = partialList.total;
      }
      this.dispenseHistoryIsLoading = false;
    }, error => {
      this.dispenseHistoryIsLoading = false;
    });

  } downloadDispenseHistorys(): void {
    if(this.dispenseHistorySelectAll){
         this.dispenseHistoryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download dispense_history", true);
      });
    }
    else{
        this.dispenseHistoryPersist.download(this.tcUtilsArray.idsList(this.dispenseHistorySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download dispense_history",true);
            });
        }
  }
addDispenseHistory(): void {
    let dialogRef = this.dispenseHistoryNavigator.addDispenseHistory();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDispenseHistorys();
      }
    });
  }

  editDispenseHistory(item: DispenseHistorySummary) {
    let dialogRef = this.dispenseHistoryNavigator.editDispenseHistory(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDispenseHistory(item: DispenseHistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("dispense_history");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.dispenseHistoryPersist.deleteDispenseHistory(item.id).subscribe(response => {
          this.tcNotification.success("dispense_history deleted");
          this.searchDispenseHistorys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}