import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DispenseHistoryPickComponent } from './dispense-history-pick.component';

describe('DispenseHistoryPickComponent', () => {
  let component: DispenseHistoryPickComponent;
  let fixture: ComponentFixture<DispenseHistoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DispenseHistoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DispenseHistoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
