import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DispenseHistorySummary, DispenseHistorySummaryPartialList } from '../dispense_history.model';
import { DispenseHistoryPersist } from '../dispense_history.persist';
import { DispenseHistoryNavigator } from '../dispense_history.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-dispense_history-pick',
  templateUrl: './dispense-history-pick.component.html',
  styleUrls: ['./dispense-history-pick.component.scss']
})export class DispenseHistoryPickComponent implements OnInit {
  dispenseHistorysData: DispenseHistorySummary[] = [];
  dispenseHistorysTotalCount: number = 0;
  dispenseHistorySelectAll:boolean = false;
  dispenseHistorySelection: DispenseHistorySummary[] = [];

 dispenseHistorysDisplayedColumns: string[] = ["select", ,"patient_id","provider_id","item_in_store_id","date","prescription_id","patient_type" ];
  dispenseHistorySearchTextBox: FormControl = new FormControl();
  dispenseHistoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) dispenseHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) dispenseHistorysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dispenseHistoryPersist: DispenseHistoryPersist,
                public dispenseHistoryNavigator: DispenseHistoryNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<DispenseHistoryPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("dispense_history");
       this.dispenseHistorySearchTextBox.setValue(dispenseHistoryPersist.dispenseHistorySearchText);
      //delay subsequent keyup events
      this.dispenseHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.dispenseHistoryPersist.dispenseHistorySearchText = value;
        this.searchDispense_historys();
      });
    } ngOnInit() {
   
      this.dispenseHistorysSort.sortChange.subscribe(() => {
        this.dispenseHistorysPaginator.pageIndex = 0;
        this.searchDispense_historys(true);
      });

      this.dispenseHistorysPaginator.page.subscribe(() => {
        this.searchDispense_historys(true);
      });
      //start by loading items
      this.searchDispense_historys();
    }

  searchDispense_historys(isPagination:boolean = false): void {


    let paginator = this.dispenseHistorysPaginator;
    let sorter = this.dispenseHistorysSort;

    this.dispenseHistoryIsLoading = true;
    this.dispenseHistorySelection = [];

    this.dispenseHistoryPersist.searchDispenseHistory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DispenseHistorySummaryPartialList) => {
      this.dispenseHistorysData = partialList.data;
      if (partialList.total != -1) {
        this.dispenseHistorysTotalCount = partialList.total;
      }
      this.dispenseHistoryIsLoading = false;
    }, error => {
      this.dispenseHistoryIsLoading = false;
    });

  }
  markOneItem(item: DispenseHistorySummary) {
    if(!this.tcUtilsArray.containsId(this.dispenseHistorySelection,item.id)){
          this.dispenseHistorySelection = [];
          this.dispenseHistorySelection.push(item);
        }
        else{
          this.dispenseHistorySelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.dispenseHistorySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }