import {TCId} from "../tc/models";

export class DispenseHistorySummary extends TCId {
    patient_id:string;
    provider_id:string;
    item_in_store_id:string;
    date:number;
    prescription_id:string;
    patient_type:number;
    }
    
export class DispenseHistorySummaryPartialList {
      data: DispenseHistorySummary[];
      total: number;
    }
   
export class DispenseHistoryDetail extends DispenseHistorySummary {
    }
    
export class DispenseHistoryDashboard {
      total: number;
    }