import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DispenseHistoryEditComponent} from "./dispense-history-edit/dispense-history-edit.component";
import {DispenseHistoryPickComponent} from "./dispense-history-pick/dispense-history-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DispenseHistoryNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  dispenseHistorysUrl(): string {
    return "/dispense_historys";
  }

  dispenseHistoryUrl(id: string): string {
    return "/dispense_historys/" + id;
  }

  viewDispenseHistorys(): void {
    this.router.navigateByUrl(this.dispenseHistorysUrl());
  }

  viewDispenseHistory(id: string): void {
    this.router.navigateByUrl(this.dispenseHistoryUrl(id));
  }

  editDispenseHistory(id: string): MatDialogRef<DispenseHistoryEditComponent> {
    const dialogRef = this.dialog.open(DispenseHistoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDispenseHistory(): MatDialogRef<DispenseHistoryEditComponent> {
    const dialogRef = this.dialog.open(DispenseHistoryEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickDispenseHistorys(selectOne: boolean=false): MatDialogRef<DispenseHistoryPickComponent> {
      const dialogRef = this.dialog.open(DispenseHistoryPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}