import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {DispenseHistoryDashboard, DispenseHistoryDetail, DispenseHistorySummaryPartialList} from "./dispense_history.model";


@Injectable({
  providedIn: 'root'
})
export class DispenseHistoryPersist {
 dispenseHistorySearchText: string = "";
 
 constructor(private http: HttpClient) {
  }
  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.dispenseHistorySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchDispenseHistory(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DispenseHistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("dispense_history", this.dispenseHistorySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<DispenseHistorySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dispense_history/do", new TCDoParam("download_all", this.filters()));
  }

  dispenseHistoryDashboard(): Observable<DispenseHistoryDashboard> {
    return this.http.get<DispenseHistoryDashboard>(environment.tcApiBaseUri + "dispense_history/dashboard");
  }

  getDispenseHistory(id: string): Observable<DispenseHistoryDetail> {
    return this.http.get<DispenseHistoryDetail>(environment.tcApiBaseUri + "dispense_history/" + id);
  } addDispenseHistory(item: DispenseHistoryDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "dispense_history/", item);
  }

  updateDispenseHistory(item: DispenseHistoryDetail): Observable<DispenseHistoryDetail> {
    return this.http.patch<DispenseHistoryDetail>(environment.tcApiBaseUri + "dispense_history/" + item.id, item);
  }

  deleteDispenseHistory(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "dispense_history/" + id);
  }
 dispenseHistorysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dispense_history/do", new TCDoParam(method, payload));
  }

  dispenseHistoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "dispense_historys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "dispense_history/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "dispense_history/" + id + "/do", new TCDoParam("print", {}));
  }


}