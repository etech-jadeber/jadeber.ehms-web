import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorPrivateInfoListComponent } from './doctor-private-info-list.component';

describe('DoctorPrivateInfoListComponent', () => {
  let component: DoctorPrivateInfoListComponent;
  let fixture: ComponentFixture<DoctorPrivateInfoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorPrivateInfoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorPrivateInfoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
