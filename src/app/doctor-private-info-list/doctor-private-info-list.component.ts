import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCUtilsAngular } from '../tc/utils-angular';
import { Doctor_Private_InfoDetail, Doctor_Private_InfoSummary } from '../patients/patients.model';
import { PatientPersist } from '../patients/patients.persist';
import { PatientNavigator } from '../patients/patients.navigator';
@Component({
  selector: 'app-doctor-private-info-list',
  templateUrl: './doctor-private-info-list.component.html',
  styleUrls: ['./doctor-private-info-list.component.css']
})
export class DoctorPrivateInfoListComponent implements OnInit {
  doctor_private_infosData: Doctor_Private_InfoSummary[] = [];
  doctor_private_infosTotalCount: number = 0;
  doctor_private_infosSelectAll: boolean = false;
  doctor_private_infosSelected: Doctor_Private_InfoSummary[] = [];
  doctor_private_infosDisplayedColumns: string[] = ['select', 'action', 'note'];
  doctor_private_infosSearchTextBox: FormControl = new FormControl();
  doctor_private_infosLoading: boolean = false;

@Input() encounterId: any;
@Output() onResult = new EventEmitter<number>();

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public patientPersist: PatientPersist,
    public patientNavigator: PatientNavigator,
  ) { 

    this.doctor_private_infosSearchTextBox.setValue(
      patientPersist.doctor_private_infoSearchText
    );
    this.doctor_private_infosSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.patientPersist.doctor_private_infoSearchText = value.trim();
        this.searchDoctor_Private_Infos();
      });
  }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
        this.paginator.pageIndex = 0;
        this.searchDoctor_Private_Infos(true);
      });
    // doctor_private_infos paginator
    this.paginator.page.subscribe(() => {
        this.searchDoctor_Private_Infos(true);
      });
      this.searchDoctor_Private_Infos(true);
  }

  searchDoctor_Private_Infos(isPagination: boolean = false): void {
    let paginator =
      this.paginator;
    let sorter = this.sorter;
    this.doctor_private_infosSelected = [];
    this.doctor_private_infosLoading = true;
    this.patientPersist
      .searchDoctor_Private_Info(
        this.encounterId,
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.doctor_private_infosData = response.data;

          if (response.total != -1) {
            this.onResult.emit(response.total);
          }
          this.doctor_private_infosLoading = false;
        },
        (error) => {
          this.doctor_private_infosLoading = false;
        }
      );
  }

  addDoctor_Private_Info(): void {
    let dialogRef = this.patientNavigator.addDoctor_Private_Info(
      this.encounterId
    );
    dialogRef.afterClosed().subscribe((newDoctor_Private_Info) => {
      this.searchDoctor_Private_Infos();
    });
  }

  editDoctor_Private_Info(item: Doctor_Private_InfoDetail): void {
    let dialogRef = this.patientNavigator.editDoctor_Private_Info(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedDoctor_Private_Info) => {
      if (updatedDoctor_Private_Info) {
        this.searchDoctor_Private_Infos();
      }
    });
  }

  deleteDoctor_Private_Info(item: Doctor_Private_InfoDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Doctor_Private_Info');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.patientPersist
          .deleteDoctor_Private_Info(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('doctor_private_info deleted');
              this.searchDoctor_Private_Infos();
            },
            (error) => {}
          );
      }
    });
  }

  downloadDoctor_Private_Infos(): void {
    this.tcNotification.info(
      'Download doctor_private_infos : ' +
        this.doctor_private_infosSelected.length
    );
  }


  back(): void {
    this.location.back();
  } 

}
