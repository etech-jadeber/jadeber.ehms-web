import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorConsultationDetailComponent } from './doctor-consultation-detail.component';

describe('DoctorConsultationDetailComponent', () => {
  let component: DoctorConsultationDetailComponent;
  let fixture: ComponentFixture<DoctorConsultationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorConsultationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorConsultationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
