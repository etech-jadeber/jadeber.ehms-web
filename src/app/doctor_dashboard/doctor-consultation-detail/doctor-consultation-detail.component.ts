import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";

import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DoctorConsultationDetail} from "../doctor_consultation.model";
import {DoctorConsultationPersist} from "../doctor_consultation.persist";
import {DoctorConsultationNavigator} from "../doctor_consultation.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DepartmentSummary } from 'src/app/departments/department.model';
import { DoctorDetail, DoctorSummary } from 'src/app/doctors/doctor.model';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { VitalNavigator } from 'src/app/vitals/vital.navigator';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { Admission_FormPersist } from 'src/app/form_encounters/admission_forms/admission_form.persist';

export enum ConsultationTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-doctor-consultation-detail',
  templateUrl: './doctor-consultation-detail.component.html',
  styleUrls: ['./doctor-consultation-detail.component.css']
})
export class DoctorConsultationDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  consultationIsLoading:boolean = false;
  
  ConsultationTabs: typeof ConsultationTabs = ConsultationTabs;
  activeTab: ConsultationTabs = ConsultationTabs.overview;
  doctor: string;
  displayVital:boolean=true;
  displayHistory:boolean=true;
  displayDiagnosis:boolean=true;
  displayExamination:boolean=true;
  displayOrder:boolean=true;
  //basics
  doctorConsultationDetail: DoctorConsultationDetail;
  departmentSpeciality: string;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public addmissionFormPersist: Admission_FormPersist,
              public doctorConsultationNavigator: DoctorConsultationNavigator,
              public  doctorConsultationPersist: DoctorConsultationPersist,
              public vitalNavigator:VitalNavigator,
              public departmentSpecialityPersist: Department_SpecialtyPersist,
              public doctorPersist: DoctorPersist,) {
    this.tcAuthorization.requireRead("consultings");
    this.doctorConsultationDetail = new DoctorConsultationDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("consultings");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }
  toggleVital():void{
    this.displayVital=!this.displayVital;
  }
  toggleHistory():void{
    this.displayHistory=!this.displayHistory;
  }
  toggleDiagnosis():void{
    this.displayDiagnosis=!this.displayDiagnosis;
  }
  toggleExamination():void{
    this.displayExamination=!this.displayExamination;
  }
  toggleOrder():void{
    this.displayOrder=!this.displayOrder;
  }
  
   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.consultationIsLoading = true;
    this.doctorConsultationPersist.getConsultation(id).subscribe(doctorConsultationDetail => {
          this.doctorConsultationDetail = doctorConsultationDetail;
          this.loadConsultingDoctor(doctorConsultationDetail.consulted_doctor_id);
          this.loadConsultingDepartment(doctorConsultationDetail.consulted_department_id);
          this.consultationIsLoading = false;
        }, error => {
          console.error(error);
          this.consultationIsLoading = false;
        });
  }
  
  loadConsultingDepartment(id:string): void {
    this.departmentSpecialityPersist.getDepartment_Specialty(id)
      .subscribe((result) => {
        this.departmentSpeciality = result.name;
      })

  }
  loadConsultingDoctor(id:string): void {
    this.doctorPersist.getDoctor(id)
      .subscribe((result) => {
        this.doctor = `${result.first_name} ${result.middle_name} ${result.last_name}`;
      })
  }
  

  reload(){
    this.loadDetails(this.doctorConsultationDetail.id);
  }
  editConsultation(): void {
    let modalRef = this.doctorConsultationNavigator.editConsultation(this.doctorConsultationDetail.id);
    modalRef.afterClosed().subscribe(doctorConsultationDetail => {
      TCUtilsAngular.assign(this.doctorConsultationDetail, doctorConsultationDetail);
    }, error => {
      console.error(error);
    });
  }
  showVitals():void{
    this.vitalNavigator.viewVitals();
  }
   printBirth():void{
      this.doctorConsultationPersist.print(this.doctorConsultationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print consultation", true);
      });
    }
    form_vitalssTotalCount:number=0;
    OnResult(count: number) {
      this.form_vitalssTotalCount = count;
    }
  back():void{
      this.location.back();
    }

}