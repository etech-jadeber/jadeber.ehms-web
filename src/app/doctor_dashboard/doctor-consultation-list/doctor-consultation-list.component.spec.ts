import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorConsultationListComponent } from './doctor-consultation-list.component';

describe('DoctorConsultationListComponent', () => {
  let component: DoctorConsultationListComponent;
  let fixture: ComponentFixture<DoctorConsultationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorConsultationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorConsultationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
