import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { DoctorConsultationSummary, DoctorConsultationSummaryPartialList,getConsultedPhysicianName,
  getConsultingPhysicianName } from '../doctor_consultation.model';
import { DoctorConsultationPersist } from '../doctor_consultation.persist';
import { DoctorConsultationNavigator } from '../doctor_consultation.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DepartmentSummary } from 'src/app/departments/department.model';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import {DoctorDetail, DoctorSummary, getPrefixPhysicianName} from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import {getPrefixPatientName} from "../../patients/patients.model";
import { encounterFilterColumn, prefixedFilterColumn} from "../../form_encounters/form_encounter.model";
import {physican_type, ServiceType} from "../../app.enums";
import {DoctorNavigator} from "../../doctors/doctor.navigator";
import {Form_EncounterNavigator} from "../../form_encounters/form_encounter.navigator";
@Component({
  selector: 'app-doctor-consultation-list',
  templateUrl: './doctor-consultation-list.component.html',
  styleUrls: ['./doctor-consultation-list.component.css']
}) export class DoctorConsultationListComponent implements OnInit {
  consultationsData: DoctorConsultationSummary[] = [];
  consultationsTotalCount: number = 0;
  consultationSelectAll: boolean = false;
  consultationSelection: DoctorConsultationSummary[] = [];
  departments: DepartmentSummary[] = [];
  departments_specialty : Department_SpecialtySummary[]=[];
  doctors: DoctorSummary[] = [];
  service_type = ServiceType;
  getConsultingPhysicianName = getConsultingPhysicianName;
  prefixedFilterColumn = prefixedFilterColumn;
  getPrefixPatientName = getPrefixPatientName;
  getConsultedPhysicianName = getConsultedPhysicianName;
  start_date: FormControl = new FormControl({ disabled: true, value: this.tcUtilsDate.toFormDate(this.doctorConsultationPersist.consultationSearchHistory.start_date) })
  end_date: FormControl = new FormControl({ disabled: true, value: this.tcUtilsDate.toFormDate(this.doctorConsultationPersist.consultationSearchHistory.end_date) })

  consultationsDisplayedColumns: string[] = ["select", "action", "patient.pid", "patient_fname", "urgent", "patient_convinience","department_specialty_name","consulting_first_name", "consulted_first_name", "brief_history", "date"];
  consultationSearchTextBox: FormControl = new FormControl();
  consultationIsLoading: boolean = false; @ViewChild(MatPaginator, { static: true }) consultationsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) consultationsSort: MatSort;

  constructor(private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public doctorConsultationPersist: DoctorConsultationPersist,
    public doctorConsultationNavigator: DoctorConsultationNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public departmentSpecialityPersist: Department_SpecialtyPersist,
    public doctorPersist: DoctorPersist,
              public doctorNavigator: DoctorNavigator,
              public formEncounterNavigator: Form_EncounterNavigator,

  ) {

    this.tcAuthorization.requireRead("consultings");
    this.consultationSearchTextBox.setValue(doctorConsultationPersist.consultationSearchHistory.search_text);
    //delay subsequent keyup events
    this.consultationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.doctorConsultationPersist.consultationSearchHistory.search_text = value;
      this.searchConsultations();
    });

    this.start_date.valueChanges.pipe().subscribe(value => {
      this.doctorConsultationPersist.consultationSearchHistory.start_date = this.tcUtilsDate.FromFormToTimeStamp(value)
      this.searchConsultations();
    });

    this.end_date.valueChanges.pipe().subscribe(value => {
      this.doctorConsultationPersist.consultationSearchHistory.end_date = this.tcUtilsDate.FromFormToTimeStamp(value)
      this.searchConsultations();
    });
  } ngOnInit() {
    this.consultationsSort.sortChange.subscribe(() => {
      this.consultationsPaginator.pageIndex = 0;
      this.searchConsultations(true);
    });

    this.consultationsPaginator.page.subscribe(() => {
      this.searchConsultations(true);
    });
    //start by loading items
    this.searchConsultations();

  }

  searchDoctors(is_from: boolean = true) {
    let dialogRef = this.doctorNavigator.pickDoctors(false, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        if(is_from) {
          this.doctorConsultationPersist.consultationSearchHistory.doctor_name = result.map((value) => `${value.first_name} ${value.middle_name} ${value.last_name}`).join(' , ');
          this.doctorConsultationPersist.consultationSearchHistory.doctor_id = result.map((value) => value.id).toString();
          this.searchConsultations();
        } else {
          this.doctorConsultationPersist.consultationSearchHistory.to_doctors_full_name = result.map((value) => `${value.first_name} ${value.middle_name} ${value.last_name}`).join(' , ');
          this.doctorConsultationPersist.consultationSearchHistory.to_doctors_id = result.map((value) => value.id).toString();
          this.searchConsultations();
        }
      }
    });
  }

  searchConsultations(isPagination: boolean = false): void {


    let paginator = this.consultationsPaginator;
    let sorter = this.consultationsSort;

    this.consultationIsLoading = true;
    this.consultationSelection = [];

    this.doctorConsultationPersist.consultationsDo("consultations", {}, paginator.pageSize, paginator.pageIndex, sorter.active || "date", sorter.direction || "desc").subscribe((partialList: DoctorConsultationSummaryPartialList) => {
      this.consultationsData = partialList.data;
      if (partialList.total != -1) {
        this.consultationsTotalCount = partialList.total;
        // this.consultationsData.forEach((consultation)=>{
        //   this.departmentSpecialityPersist.getDepartment_Specialty(consultation.consulted_department_id).subscribe((result)=>{
        //     if(result){
        //       this.departments_specialty.push(result);
        //     }
        //   })
        //   this.doctorPersist.getDoctor(consultation.consulted_doctor_id).subscribe((result)=>{
        //     if(result){
        //       this.doctors.push(result)
        //     }
        //   })
        // })
      }
      this.consultationIsLoading = false;
    }, error => {
      this.consultationIsLoading = false;
    });

  }
  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(
      this.doctors,
      doctorId
    );

    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`;
    }
    return '';
  }
  downloadConsultations(): void {
    if (this.consultationSelectAll) {
      this.doctorConsultationPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download consultation", true);
      });
    }
    else {
      this.doctorConsultationPersist.download(this.tcUtilsArray.idsList(this.consultationSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download consultation", true);
      });
    }
  }
  addConsultation(): void {
    let dialogRef = this.doctorConsultationNavigator.addConsultation();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchConsultations();
      }
    });
  }

  editConsultation(item: DoctorConsultationSummary) {
    let dialogRef = this.doctorConsultationNavigator.editConsultation(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  // transferConsultation(item: DoctorConsultationSummary) {
  //   let dialogRef = this.formEncounterNavigator.transferConsulting(item.id);
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.searchConsultations()
  //     }

  //   });
  // }

  deleteConsultation(item: DoctorConsultationSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("consultations");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.doctorConsultationPersist.deleteConsultation(item.id).subscribe(response => {
          this.tcNotification.success("consultation deleted");
          this.searchConsultations();
        }, error => {
        });
      }

    });
  } back(): void {
    this.location.back();
  }
}
