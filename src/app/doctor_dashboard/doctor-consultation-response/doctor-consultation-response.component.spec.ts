import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorConsultationResponseComponent } from './doctor-consultation-response.component';

describe('DoctorConsultationResponseComponent', () => {
  let component: DoctorConsultationResponseComponent;
  let fixture: ComponentFixture<DoctorConsultationResponseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorConsultationResponseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorConsultationResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
