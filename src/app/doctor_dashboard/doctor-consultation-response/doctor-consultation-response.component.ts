import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorConsultationDetail } from '../doctor_consultation.model';
import { DoctorConsultationPersist } from '../doctor_consultation.persist';@Component({
  selector: 'app-doctor-consultation-response',
  templateUrl: './doctor-consultation-response.component.html',
  styleUrls: ['./doctor-consultation-response.component.css']
})export class DoctorConsultationResponseComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  doctorConsultationDetail: DoctorConsultationDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DoctorConsultationResponseComponent>,
              public  persist: DoctorConsultationPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("consultings");
      this.title = this.appTranslation.getText("general","new") +  " " + "consultations";
      this.doctorConsultationDetail = new DoctorConsultationDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("consultings");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "consultations";
      this.isLoadingResults = true;
      this.persist.getConsultation(this.idMode.id).subscribe(doctorConsultationDetail => {
        this.doctorConsultationDetail = doctorConsultationDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addConsultation(this.doctorConsultationDetail).subscribe(value => {
      this.tcNotification.success("consultation added");
      this.doctorConsultationDetail.id = value.id;
      this.dialogRef.close(this.doctorConsultationDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateConsultation(this.doctorConsultationDetail).subscribe(value => {
      this.tcNotification.success("consultation updated");
      this.dialogRef.close(this.doctorConsultationDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.doctorConsultationDetail == null){
            return false;
          }
  if (this.doctorConsultationDetail.clinical_finding == null || this.doctorConsultationDetail.clinical_finding  == "") {
    return false;
  } 
  if (this.doctorConsultationDetail.treatment == null || this.doctorConsultationDetail.treatment  == "") {
    return false;
  } 

 return true;

 }
 }