import { DoctorDetail } from "../doctors/doctor.model";
import {TCId} from "../tc/models";
import { TCUtilsString } from "../tc/utils-string";

export type Prefixed<T> = {
  [K in keyof T as `consulting_${string & K}`]: T[K];
};

export type PrefixedConsulting = Prefixed<DoctorDetail>;

export function getConsultingPhysicianName(doctor: PrefixedConsulting){
  const utilsString = new TCUtilsString(null);
  let name = `${doctor.consulting_first_name} ${doctor.consulting_middle_name} ${doctor.consulting_last_name}`
  name = utilsString.valueOrDefaultString(name, doctor.consulting_title)
  const qualification = utilsString.valueOrDefaultString(doctor.consulting_qualification, "(", ")")
  return utilsString.valueOrDefaultString(name, "", qualification);
}

export type ConsultedPrefix<T> = {
  [K in keyof T as `consulted_${string & K}`]: T[K];
};

export type PrefixedConsulted = ConsultedPrefix<DoctorDetail>;

export function getConsultedPhysicianName(doctor: PrefixedConsulted){
  const utilsString = new TCUtilsString(null);
  let name = `${doctor.consulted_first_name} ${doctor.consulted_middle_name} ${doctor.consulted_last_name}`
  name = utilsString.valueOrDefaultString(name, doctor.consulted_title)
  const qualification = utilsString.valueOrDefaultString(doctor.consulted_qualification, "(", ")")
  return utilsString.valueOrDefaultString(name, "", qualification);
}

export class DoctorConsultationSummary extends TCId {
    encounter_id:string;
    date:number;
    urgent:boolean;
    patient_convinience:boolean;
    brief_history:string;
    report:string;
    clinical_finding:string;
    treatment:string;
    consulted_doctor_id:string;
    consulted_department_id:string;
    report_date:number;
    reason_for_consult:string;
    patient_condition: number;
    // some name:string;
     
    }
    export class DoctorConsultationSummaryPartialList {
      data: DoctorConsultationSummary[];
      total: number;
    }
    export class DoctorConsultationDetail extends DoctorConsultationSummary {
    }
    
    export class DoctorConsultationDashboard {
      total: number;
      consults:number;
    }