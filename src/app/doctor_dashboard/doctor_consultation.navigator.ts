import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { DoctorConsultationResponseComponent } from "./doctor-consultation-response/doctor-consultation-response.component";

@Injectable({
  providedIn: 'root'
})

export class DoctorConsultationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  consultationsUrl(): string {
    return "/consultations";
  }

  consultationUrl(id: string): string {
    return "/consultations/" + id;
  }

  viewConsultations(): void {
    this.router.navigateByUrl(this.consultationsUrl());
  }

  viewConsultation(id: string): void {
    this.router.navigateByUrl(this.consultationUrl(id));
  }

  editConsultation(id: string): MatDialogRef<DoctorConsultationResponseComponent> {
    const dialogRef = this.dialog.open(DoctorConsultationResponseComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addConsultation(): MatDialogRef<DoctorConsultationResponseComponent> {
    const dialogRef = this.dialog.open(DoctorConsultationResponseComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
//    pickConsultations(selectOne: boolean=false): MatDialogRef<ConsultationPickComponent> {
//       const dialogRef = this.dialog.open(ConsultationPickComponent, {
//         data: selectOne,
//         width: TCModalWidths.large
//       });
//       return dialogRef;
//     }
}