import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {DoctorConsultationDashboard, DoctorConsultationDetail, DoctorConsultationSummaryPartialList} from "./doctor_consultation.model";
import {TCUtilsString} from "../tc/utils-string";
import {consultationStatus} from "../app.enums";


@Injectable({
  providedIn: 'root'
})
export class DoctorConsultationPersist {
 // consultationSearchText: string = "";
 constructor(private http: HttpClient) {
  }
  filters(searchHistory = this.consultationSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  consultationSearchHistory = {
    "start_date": undefined,
    "end_date": undefined,
    "search_text": "",
    "search_column": "",
    "doctor_name": "",
    "doctor_id": "",
    status: undefined,
    show_only_mine: true,
    to_doctors_full_name: undefined,
    to_doctors_id: undefined,
  }

  statusEnum: TCEnum[] = [
    new TCEnum(consultationStatus.no_response, "No Response"),
    new TCEnum(consultationStatus.response_given, "Response Given"),
  ]

  defaultConsultationSearchHistory = {...this.consultationSearchHistory}

  resetPatientSearchHistory(){
    this.consultationSearchHistory = {...this.defaultConsultationSearchHistory}
  }

  searchConsultation(pageSize: number, pageIndex: number, sort: string, order: string,searchHistory = this.consultationSearchHistory): Observable<DoctorConsultationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("consulting", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);
    return this.http.get<DoctorConsultationSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "consulting/do", new TCDoParam("download_all", this.filters()));
  }

  DoctorconsultationDashboard(): Observable<DoctorConsultationDashboard> {
    return this.http.get<DoctorConsultationDashboard>(environment.tcApiBaseUri + "consulting/dashboard");
  }

  getConsultation(id: string): Observable<DoctorConsultationDetail> {
    return this.http.get<DoctorConsultationDetail>(environment.tcApiBaseUri + "consulting/" + id);
  }
  addConsultation(item: DoctorConsultationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "consulting/", item);
  }

  updateConsultation(item: DoctorConsultationDetail): Observable<DoctorConsultationDetail> {
    return this.http.patch<DoctorConsultationDetail>(environment.tcApiBaseUri + "consulting/" + item.id, item);
  }

  deleteConsultation(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "consulting/" + id);
  }

 consultationsDo(method: string, payload: any,pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory=this.consultationSearchHistory): Observable<{}> {
   let url = TCUtilsHttp.buildSearchUrl("consultingreport/do", searchHistory.search_text, pageSize, pageIndex, sort, order);
   url = TCUtilsString.appendUrlParameters(url, searchHistory);
   return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  consultationDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "consulting/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "consulting/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "consulting/" + id + "/do", new TCDoParam("print", {}));
  }


}
