import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DoctorDetail} from "../doctor.model";
import {DoctorPersist} from "../doctor.persist";
import {DoctorNavigator} from "../doctor.navigator";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {TCUtilsString} from 'src/app/tc/utils-string';
import {TCId} from 'src/app/tc/models';
import {physican_type, tabs} from 'src/app/app.enums';
import {MenuState} from 'src/app/global-state-manager/global-state';
import {DepartmentPersist} from "../../departments/department.persist";
import {Department_SpecialtyPersist} from "../../department_specialtys/department_specialty.persist";


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-doctor-detail',
  templateUrl: './doctor-detail.component.html',
  styleUrls: ['./doctor-detail.component.css']
})
export class DoctorDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  doctorLoading: boolean = false;

  DoctorTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  doctorDetail: DoctorDetail;
  physician_type: physican_type;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  physican_type = physican_type;

  doctor_department_name: string = "";
  doctor_department_speciality: string = "";

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public tcUtilsDate: TCUtilsDate,
              public jobPersist: JobPersist,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public doctorNavigator: DoctorNavigator,
              public doctorPersist: DoctorPersist,
              public departmentPersist: DepartmentPersist,
              public departmentSpecialityPersist: Department_SpecialtyPersist,
              public menuState: MenuState) {
    this.tcAuthorization.requireRead("doctors");
    this.doctorDetail = new DoctorDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("doctors");
    this.activeTab = tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.doctorLoading = true;
    this.doctorPersist.getDoctor(id).subscribe(doctorDetail => {
      this.doctorDetail = doctorDetail;
      this.doctorDetail.department_id && this.getDoctorDepartment(this.doctorDetail.department_id);
      this.doctorDetail.department_speciality_id && this.getDoctorSpeciality(this.doctorDetail.department_speciality_id);
      this.doctorLoading = false;
      this.menuState.getDataResponse(this.doctorDetail);
      
    }, error => {
      console.error(error);
      this.doctorLoading = false;
    });
  }

  getDoctorDepartment(departmentId: string) {
    this.departmentPersist.getDepartment(departmentId).subscribe(value => {
      this.doctor_department_name = value.name;
    })
  }

  getDoctorSpeciality(departmentSpeciality: string) {
    this.departmentSpecialityPersist.getDepartment_Specialty(departmentSpeciality).subscribe(value => {
      this.doctor_department_speciality = value.name;
    })
  }

  reload() {
    this.loadDetails(this.doctorDetail.id);
  }

  editDoctor(): void {
    let modalRef = this.doctorNavigator.editDoctor(this.doctorDetail.id);
    modalRef.afterClosed().subscribe(modifiedDoctorDetail => {
      TCUtilsAngular.assign(this.doctorDetail, modifiedDoctorDetail);
    }, error => {
      console.error(error);
    });
  }

  printDoctor(): void {
    this.doctorPersist.print(this.doctorDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print doctor", true);
    });
  }

  back(): void {
    this.location.back();
  }


  createUserDoctor() {
    this.doctorPersist
      .doctorDo(this.doctorDetail.id, "account_create_doctor", {})
      .subscribe((response) => {
        this.doctorDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }

  createUserNurse() {
    this.doctorPersist
      .doctorDo(this.doctorDetail.id, "account_create_nurse", {})
      .subscribe((response) => {
        this.doctorDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }


  createUserLab() {
    this.doctorPersist
      .doctorDo(this.doctorDetail.id, "account_create_lab", {})
      .subscribe((response) => {
        this.doctorDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }

  createUserRadiologyist(){
    this.doctorPersist
      .doctorDo(this.doctorDetail.id, "account_create_radiology", {})
      .subscribe((response) => {
        this.doctorDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }
  
  createUserPhysiotherapist(){
    this.doctorPersist
    .doctorDo(this.doctorDetail.id, "account_create_physiotherapy", {})
    .subscribe((response) => {
      this.doctorDetail.login_id = (<TCId>response).id;
      this.tcNotification.success("account created");
      this.reload();
    });
  }

  createUserMidwives(){
    this.doctorPersist
    .doctorDo(this.doctorDetail.id, "account_create_midwives", {})
    .subscribe((response) => {
      this.doctorDetail.login_id = (<TCId>response).id;
      this.tcNotification.success("account created");
      this.reload();
    });
  }

  removeUser() {
    this.doctorPersist
      .doctorDo(this.doctorDetail.id, "remove_account", {})
      .subscribe((response) => {
        this.tcNotification.success("account removed");
        this.reload();
      });
  
    }
    activateUser() {
      this.doctorPersist
        .doctorDo(this.doctorDetail.id, "activate_user", {})
        .subscribe((response) => {
          this.tcNotification.success("account Activated");
          this.reload();
        });
    }

}
