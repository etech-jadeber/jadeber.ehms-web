import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes} from "../../tc/models";


import {DoctorDetail} from "../doctor.model";
import {DoctorPersist} from "../doctor.persist";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {Openemr_UserNavigator} from 'src/app/openemr_users/openemr_user.navigator';
import {Openemr_UserSummary} from 'src/app/openemr_users/openemr_user.model';
import {physican_type} from "../../app.enums";
import {DepartmentNavigator} from "../../departments/department.navigator";
import {Department_SpecialtyNavigator} from "../../department_specialtys/department_specialty.navigator";
import {DepartmentSummary} from "../../departments/department.model";
import {Department_SpecialtySummary} from "../../department_specialtys/department_specialty.model";
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeSummary } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { NgSignaturePadOptions, SignaturePadComponent } from '@almothafar/angular-signature-pad';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { PatientPersist } from 'src/app/patients/patients.persist';


@Component({
  selector: 'app-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.css']
})
export class DoctorEditComponent implements OnInit {

  doctorName: string = "";
  isLoadingResults: boolean = false;
  title: string;
  doctorDetail: DoctorDetail;
  birth_date: Date;
  physican_type = physican_type;
  departmentName: string = "";
  wardName: string = "";
  ward : string = "";
  selectedPhysican_type="";

  departmentSpeciality: string = "";
  department: string = "";
  @ViewChild('signature')
  public signaturePad: SignaturePadComponent;

  signaturePadOptions: NgSignaturePadOptions = { // passed through to szimek/signature_pad constructor
    minWidth: 5,
    canvasWidth: 500,
    canvasHeight: 200
  };

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<DoctorEditComponent>,
              public tcUtilsDate: TCUtilsDate,
              public persist: DoctorPersist,
              public departmentNavigator: DepartmentNavigator,
              public departmentSpecialityPersist: Department_SpecialtyPersist,
              public bedRoomTypeNavigator: BedroomtypeNavigator,
              public bedroomtypePersist: BedroomtypePersist,
              public filePersist: FilePersist,
              public patientPersist: PatientPersist,
              private http: HttpClient,
              public departmentSpecialityNavigator: Department_SpecialtyNavigator,
              public openemr_UserNavigator: Openemr_UserNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("doctors");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("employee", "physician");
      this.doctorDetail = new DoctorDetail();
      this.doctorDetail.email = "";
      this.doctorDetail.title= "";
      this.doctorDetail.qualification = "";
      this.doctorDetail.type = -1;

      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("doctors");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("employee", "physician");
      this.isLoadingResults = true;
      this.persist.getDoctor(this.idMode.id).subscribe(doctorDetail => {
        this.doctorDetail = doctorDetail;
        this.transformDates(true);
        if(this.doctorDetail.ward_id){
          this.bedroomtypePersist.getBedroomtype(doctorDetail.ward_id).subscribe(value=>{
            this.wardName = value.name
          })
        }
        if(this.doctorDetail.department_speciality_id){
          this.departmentSpecialityPersist.getDepartment_Specialty(doctorDetail.department_speciality_id).subscribe(value => {
            this.departmentSpeciality = value.name;
          })
        }
        this.filePersist.getDownloadKey(this.doctorDetail.signature_id).subscribe(
          (res:any)=>{
            
            this.doctorDetail.signature_url = this.filePersist.downloadLink(res.id);
            
          }
        );
            if(this.doctorDetail.department_id){
              this.departmentSpecialityPersist.getDepartment_Specialty(doctorDetail.department_id).subscribe(value => {
                this.department = value.name;
              })
            }
        
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.addDoctor(this.doctorDetail).subscribe(value => {
      this.tcNotification.success("Doctor added");
      this.doctorDetail.id = value.id;
      this.dialogRef.close(this.doctorDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.updateDoctor(this.doctorDetail).subscribe(value => {
      this.tcNotification.success("Doctor updated");
      this.dialogRef.close(this.doctorDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  transformDates(todate: boolean) {
    if (todate) {
      this.birth_date = this.tcUtilsDate.toDate(this.doctorDetail.birth_date);
    } else {
      this.doctorDetail.birth_date = this.tcUtilsDate.toTimeStamp(
        this.birth_date
      );
    }
  }

  clearSignature() {
    this.signaturePad.clear()
  }

  submitSignature() {
    
    fetch(this.signaturePad.toDataURL()).then(
      (res) => {
        res.blob().then(
          bl => {
            let fd: FormData = new FormData();
            fd.append('file', bl);
            this.http.post(this.patientPersist.documentUploadUri(), fd).subscribe({
              next: (response) => {
                this.doctorDetail.signature_id = (<TCId>response).id
                this.filePersist.getDownloadKey(this.doctorDetail.signature_id).subscribe(
                  (res:any)=>{
                    
                    this.doctorDetail.signature_url = this.filePersist.downloadLink(res.id);
                    
                  }
                );
              },

              error: (err) => {
                console.log(err);

              }
            }


            )
          }
        )
      })
  }

  documentUpload(fileInputEvent: any): void {

    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.patientPersist.documentUploadUri(), fd).subscribe({
      next: (response) => {
        this.doctorDetail.signature_id = (<TCId>response).id
                this.filePersist.getDownloadKey(this.doctorDetail.signature_id).subscribe(
                  (res:any)=>{
                    
                    this.doctorDetail.signature_url = this.filePersist.downloadLink(res.id);
                    
                  }
                );
        
      
      },

      error:(err)=>{
        console.log(err);
        
      }
    }
    

    );
  }


  canSubmit(): boolean {

    if (this.doctorDetail == null) {
      return false;
    }

    if (this.doctorDetail.first_name == null || this.doctorDetail.first_name == "") {
      return false;
    }

    if (this.doctorDetail.last_name == null || this.doctorDetail.last_name == "") {
      return false;
    }

    if (this.doctorDetail.middle_name == null || this.doctorDetail.middle_name == "") {
      return false;
    }

    if (this.doctorDetail.telephone == null || this.doctorDetail.telephone == "") {
      return false;
    }

    if (this.doctorDetail.address == null || this.doctorDetail.address == "") {
      return false;
    }

    if (this.doctorDetail.physican_type == null){
      
      return false;
    }
   if(this.doctorDetail.physican_type == 106){
    if (!this.doctorDetail.department_id){
      return false;
    }
    if (!this.doctorDetail.department_speciality_id){
      return false;
    }
   }

    return true;
  }


  searchDoctor() {
    let dialogRef = this.openemr_UserNavigator.pickOpenemr_Users(true);
    dialogRef.afterClosed().subscribe((result: Openemr_UserSummary[]) => {
      if (result) {
        
        this.doctorDetail.open_emr_id = result[0].id;
        this.doctorName = result[0].fname + "" + result[0].lname
      }
    });
  }

  searchDepartment() {
    let dialogRef = this.departmentNavigator.pickDepartments(true);
    dialogRef.afterClosed().subscribe((result: DepartmentSummary[]) => {
      if (result) {
        this.doctorDetail.department_id = result[0].id;
        this.departmentName = result[0].name
      }
    });
  }

  searchWard() {
    let dialogRef = this.bedRoomTypeNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
   
      if (result) {
        this.doctorDetail.ward_id = result[0].id;
        this.wardName = result[0].name
      }
    });
  }
  physican_typeChange(val:number){
  this.doctorDetail.physican_type = val
  if (this.doctorDetail.physican_type != physican_type.doctor){
    this.doctorDetail.department_speciality_id = undefined
    this.doctorDetail.department_id = undefined

  }
  if (this.doctorDetail.physican_type != physican_type.nurse){
    this.doctorDetail.ward_id = undefined
    
  }

  }

  searchDepartmentSpeciality(department: string) {
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, department);
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        if (department == this.tcUtilsString.invalid_id){
          this.doctorDetail.department_id = result[0].id
          this.department = result[0].name
          this.departmentSpeciality = ""
          this.doctorDetail.department_id = result[0].id
          this.doctorDetail.department_speciality_id = this.tcUtilsString.invalid_id
        }else {
          this.doctorDetail.department_speciality_id = result[0].id;
          this.departmentSpeciality = result[0].name
        }
      }
    });
  }

  removeSignature() {
    this.filePersist.deleteFile(this.doctorDetail.signature_id).subscribe(
      (res) => {
        this.doctorDetail.signature_id = this.tcUtilsString.invalid_id
      }
    )
  }
}
