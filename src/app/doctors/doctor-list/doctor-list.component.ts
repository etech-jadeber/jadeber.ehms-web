import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {DoctorPersist} from "../doctor.persist";
import {DoctorNavigator} from "../doctor.navigator";
import {DoctorDetail, doctorFilterColumn, DoctorSummary, DoctorSummaryPartialList} from "../doctor.model";


@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {

  doctorsData: DoctorSummary[] = [];
  doctorsTotalCount: number = 0;
  doctorsSelectAll:boolean = false;
  doctorsSelection: DoctorSummary[] = [];
  doctorFilterColumn = doctorFilterColumn

  doctorsDisplayedColumns: string[] = ["select","action", "first_name","middle_name","last_name","email","telephone","is_male","birth_date","address" ];
  doctorsSearchTextBox: FormControl = new FormControl();
  doctorsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) doctorsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) doctorsSort: MatSort;
  doctorSearchHistory = {...this.doctorPersist.defaultDoctorSearchHistory, "disabled": undefined};

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public doctorPersist: DoctorPersist,
                public doctorNavigator: DoctorNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("doctors");
       this.doctorsSearchTextBox.setValue(this.doctorSearchHistory.search_text);
      //delay subsequent keyup events
      this.doctorsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.doctorSearchHistory.search_text = value;
        this.searchDoctors();
      });
    }

    ngOnInit() {

      this.doctorsSort.sortChange.subscribe(() => {
        this.doctorsPaginator.pageIndex = 0;
        this.searchDoctors(true);
      });

      this.doctorsPaginator.page.subscribe(() => {
        this.searchDoctors(true);
      });
      //start by loading items
      this.searchDoctors();
    }

  searchDoctors(isPagination:boolean = false): void {


    let paginator = this.doctorsPaginator;
    let sorter = this.doctorsSort;

    this.doctorsIsLoading = true;
    this.doctorsSelection = [];

    this.doctorPersist.searchDoctor(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.doctorSearchHistory).subscribe((partialList: DoctorSummaryPartialList) => {
      this.doctorsData = partialList.data;
      if (partialList.total != -1) {
        this.doctorsTotalCount = partialList.total;
      }
      this.doctorsIsLoading = false;
    }, error => {
      this.doctorsIsLoading = false;
    });

  }

  downloadDoctors(): void {
    if(this.doctorsSelectAll){
         this.doctorPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download doctors", true);
      });
    }
    else{
        this.doctorPersist.download(this.tcUtilsArray.idsList(this.doctorsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download doctors",true);
            });
        }
  }



  addDoctor(): void {
    let dialogRef = this.doctorNavigator.addDoctor();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDoctors();
      }
    });
  }

  editDoctor(item: DoctorSummary) {
    let dialogRef = this.doctorNavigator.editDoctor(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDoctor(item: DoctorSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Doctor");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.doctorPersist.deleteDoctor(item.id).subscribe(response => {
          this.tcNotification.success("Doctor deleted");
          this.searchDoctors();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
