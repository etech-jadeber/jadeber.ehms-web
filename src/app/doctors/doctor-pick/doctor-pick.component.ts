import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {DoctorSummary, DoctorSummaryPartialList} from "../doctor.model";
import {DoctorPersist} from "../doctor.persist";


@Component({
  selector: 'app-doctor-pick',
  templateUrl: './doctor-pick.component.html',
  styleUrls: ['./doctor-pick.component.css']
})
export class DoctorPickComponent implements OnInit, OnDestroy {

  doctorsData: DoctorSummary[] = [];
  doctorsTotalCount: number = 0;
  doctorsSelection: DoctorSummary[] = [];
  doctorsDisplayedColumns: string[] = ["select", 'first_name', 'middle_name', 'last_name', 'type'];

  doctorsSearchTextBox: FormControl = new FormControl();
  doctorsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) doctorsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) doctorsSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public doctorPersist: DoctorPersist,
              public dialogRef: MatDialogRef<DoctorPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { selectOne: boolean, physicianType: number, department_id: string, department_specialty_id: string,disabled:boolean}
  ) {
    this.tcAuthorization.requireRead("doctors");
    this.doctorsSearchTextBox.setValue(doctorPersist.doctorSearchHistory.search_text);
    //delay subsequent keyup events
    
    this.doctorPersist.doctorSearchHistory.disabled = data.disabled;

    this.doctorsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.doctorPersist.doctorSearchHistory.search_text = value;
      this.searchDoctors();
    });

    if (this.data.physicianType) {
      this.doctorPersist.doctorSearchHistory.physican_type = this.data.physicianType;
    }
    if (this.data.department_specialty_id){
        this.doctorPersist.doctorSearchHistory.department_speciality_id = this.data.department_specialty_id;
    }
    if (this.data.department_id){
      this.doctorPersist.doctorSearchHistory.department_id = this.data.department_id;
  }
  }

  ngOnDestroy(): void {
    this.doctorPersist.clearFilters();
  }


  ngOnInit() {

    this.doctorsSort.sortChange.subscribe(() => {
      this.doctorsPaginator.pageIndex = 0;
      this.searchDoctors();
    });

    this.doctorsPaginator.page.subscribe(() => {
      this.searchDoctors();
    });

    //set initial picker list to 5
    this.doctorsPaginator.pageSize = 5;

    //start by loading items
    this.searchDoctors();
  }

  searchDoctors(): void {
    this.doctorsIsLoading = true;
    // this.doctorsSelection = [];
      this.doctorPersist.searchDoctor(this.doctorsPaginator.pageSize,
      this.doctorsPaginator.pageIndex,
      this.doctorsSort.active,
      this.doctorsSort.direction).subscribe((partialList: DoctorSummaryPartialList) => {
      this.doctorsData = partialList.data;
      if (partialList.total != -1) {
        this.doctorsTotalCount = partialList.total;
      }
      this.doctorsIsLoading = false;
    }, error => {
      this.doctorsIsLoading = false;
    });
  }

  markOneItem(item: DoctorSummary) {
    if (!this.tcUtilsArray.containsId(this.doctorsSelection, item.id)) {
      this.doctorsSelection = [];
      this.doctorsSelection.push(item);
    } else {
      this.doctorsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.doctorsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
