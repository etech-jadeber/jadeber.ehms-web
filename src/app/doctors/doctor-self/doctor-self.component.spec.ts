import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DoctorSelfComponent } from './doctor-self.component';

describe('DoctorSelfComponent', () => {
  let component: DoctorSelfComponent;
  let fixture: ComponentFixture<DoctorSelfComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorSelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorSelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
