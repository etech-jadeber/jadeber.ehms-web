import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AppTranslation } from 'src/app/app.translation';
import { UserContexts } from 'src/app/tc/app.enums';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { environment } from 'src/environments/environment';
import {TCUtilsArray} from "../../tc/utils-array";
import { DoctorSummary } from '../doctor.model';
import { DoctorPersist } from '../doctor.persist';
import { Surgery_AppointmentDetail, Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList } from 'src/app/surgery_appointments/surgery_appointment.model';
import { Surgery_AppointmentNavigator } from '../surgery_appointment.navigator';
import { Surgery_AppointmentPersist } from 'src/app/surgery_appointments/surgery_appointment.persist';
import { Surgery_TypeSummary } from 'src/app/surgery_types/surgery_type.model';
import { Surgery_TypePersist } from 'src/app/surgery_types/surgery_type.persist';
import { debounceTime } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TCAppInit } from 'src/app/tc/app-init';

export enum DoctorTabs {
  overview,
  profile,
  surgery_appointment
}

export enum PaginatorIndexes {
  surgery_appointment
}

@Component({
  selector: 'app-doctor-self',
  templateUrl: './doctor-self.component.html',
  styleUrls: ['./doctor-self.component.css']
})
export class DoctorSelfComponent implements OnInit {

  title = environment.appName;
  doctorLoading: boolean = true;
  message: string = "";
  doctorDetail: DoctorSummary;
  surgeryTypes: Surgery_TypeSummary[] = [];
  patients: PatientSummary[] = []
  TCAppInit: typeof TCAppInit = TCAppInit;
  doctorTabs: typeof DoctorTabs = DoctorTabs;
  activeTab: DoctorTabs = DoctorTabs.overview;

    //surgery appointment
  surgeryAppointmentsSummary: Surgery_AppointmentSummary[] = [];
  surgeryAppointmentsTotalCount: number = 0;
  surgeryAppointmentsSelectAll: boolean = false;
  surgeryAppointmentsSelected: Surgery_AppointmentSummary[] = [];
  surgeryAppointmentsDisplayedColumns: string[] = ["select","action", "patient_id","surgery_type_id","surgery_date","start_time","end_time","surgery_status", ];
  surgeryAppointmentsSearchTextBox: FormControl = new FormControl();
  surgeryAppointmentsLoading: boolean = false;
  surgeryAppointmentsSelection:Surgery_AppointmentSummary[] = [];
  surgeryAppointmentDetail: Surgery_AppointmentDetail[];
  surgerytypeList: Surgery_TypeSummary[] = [];

  @ViewChildren(MatPaginator) Paginator = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) Sort = new QueryList<MatSort>();


  constructor(public tcAuthorization:TCAuthorization, public doctorPersisit:DoctorPersist, public tcAuthentication:TCAuthentication,
    public tcNotification:TCNotification, public userPersist:UserPersist, public tcUtilsAngular:TCUtilsAngular,
     public appTranslation:AppTranslation, public tcNavigator:TCNavigator,
     public surgeryAppointmentNavigator: Surgery_AppointmentNavigator,
     public surgeryAppointmentPersist: Surgery_AppointmentPersist,
     public surgeryTypePersist: Surgery_TypePersist,
     public tcUtilsArray: TCUtilsArray,
     public patientNavigator: PatientNavigator,
     public patientPersist: PatientPersist,
     public tCUtilsDate: TCUtilsDate) {
      this.surgeryAppointmentsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.surgeryAppointmentPersist.surgery_appointmentSearchText = value;
        this.searchSurgeryAppointments();
      });
      }

  ngOnInit() {
    this.tcAuthorization.requireLogin();
    this.doctorPersisit.initDoctorSelf();
  }

  ngAfterViewInit(): void {
    let employeeId: string = this.tcAuthorization.getUserContexts(UserContexts.employee)[0];
    this.message = "member id: " + employeeId;
    this.loadDetails();
    //surgery appointment sorter
    this.Sort.toArray()[PaginatorIndexes.surgery_appointment].sortChange.subscribe(() => {
      this.Paginator.toArray()[PaginatorIndexes.surgery_appointment].pageIndex = 0;
      this.searchSurgeryAppointments(true);
    });
    // surgery appointment paginator
    this.Paginator.toArray()[PaginatorIndexes.surgery_appointment].page.subscribe(() => {
      this.searchSurgeryAppointments(true);
    })
  };


  loadDetails() {
    this.doctorPersisit.getDocotrSelf().subscribe(result => {
        this.doctorDetail = result;
        this.doctorLoading = false;
      }
      , error => {
        console.error(error);
        this.doctorLoading = false;
      });
  }

  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }

  searchSurgeryAppointments(isPagination: boolean = false): void {
    let paginator = this.Paginator.toArray()[PaginatorIndexes.surgery_appointment];
    let sorter = this.Sort.toArray()[PaginatorIndexes.surgery_appointment];
    this.surgeryAppointmentsLoading = true;
    this.surgeryAppointmentsSelection = [];
    this.surgeryAppointmentPersist.searchSurgery_Appointment(
      paginator.pageSize,
      isPagination ? paginator.pageIndex : 0,
      sorter.active,
      sorter.direction
    ).subscribe(
      (partialList: Surgery_AppointmentSummaryPartialList) => {
        this.surgeryAppointmentsSummary = partialList.data.filter((value) => value.doctor_id == this.doctorDetail.id);
        partialList.data.forEach((data) => {
          this.patientPersist.getPatient(`${data.patient_id}`).subscribe(
            (result) => {
              this.patients.push(result);

            }
          )
          this.surgeryTypePersist.getSurgery_Type(data.surgery_type_id).subscribe(
            (result) => {
              this.surgeryTypes.push(result)
            }
          )
        })
        if (partialList.total != -1){
          this.surgeryAppointmentsTotalCount = partialList.total;
        }
        this.surgeryAppointmentsLoading = false;
      },
      (error) => {
        this.surgeryAppointmentsLoading = false;
      }
    )
  }

  deleteSurgeryAppointment(item: Surgery_AppointmentSummary){
    let dialogRef = this.tcNavigator.confirmDeletion("Surgery_Appointment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.surgeryAppointmentPersist.deleteSurgery_Appointment(item.id).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "deleted"));
          this.searchSurgeryAppointments();
        }, error => {
        });
      }

    });
  }

  editSurgeryAppointment(item: Surgery_AppointmentSummary){
    let dialogRef = this.surgeryAppointmentNavigator.editSurgery_Appointment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  addSurgeryAppointment(){
    let dialogRef = this.surgeryAppointmentNavigator.addSurgery_Appointment();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSurgeryAppointments();
      }
    });
  }

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(value => value.uuidd == patientId);
    if (patient) {
      return `${patient.fname} ${patient.lname}`
    }
    return "";
  }

  setStartSurgery(item: Surgery_AppointmentSummary){
    item.start_time = Math.floor(new Date().getTime()/1000);
    this.surgeryAppointmentPersist.surgery_appointmentDo(item.id,"start_time",item).subscribe(value => {
      this.tcNotification.success("Surgery started");
      this.searchSurgeryAppointments();
    }, error => {
      console.error(error);
    })
  }

  setEndtSurgery(item: Surgery_AppointmentSummary){
    item.end_time = Math.floor(new Date().getTime()/1000);
    this.surgeryAppointmentPersist.surgery_appointmentDo(item.id,"end_time",item).subscribe(value => {
      this.tcNotification.success("Surgery eneded");
      this.searchSurgeryAppointments();
    }, error => {
      console.error(error);
    })
  }

  cancelAppointment(item: Surgery_AppointmentSummary){
    this.surgeryAppointmentPersist.surgery_appointmentDo(item.id,"cancelled",item).subscribe(value => {
      this.tcNotification.success("Surgery cancelled");
      this.searchSurgeryAppointments();
    }, error => {
      console.error(error);
    })
  }

  logout(){
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }

}
