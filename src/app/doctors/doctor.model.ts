import {TCId, TCString} from "../tc/models";
import { TCUtilsString } from "../tc/utils-string";

export const doctorFilterColumn: TCString[] = [
  new TCString( "first_name", 'First Name'),
  new TCString( "middle_name", 'Middle Name'),
  new TCString( "last_name", 'Last Name'),
  new TCString( "telephone", 'Phone Number'),
  ];
export type Prefixed<T> = {
  [K in keyof T as `physician_${string & K}`]: T[K];
};

export type PrefixedDoctor = Prefixed<DoctorDetail>;

export class DoctorSummary extends TCId {
  first_name: string;
  last_name: string;
  middle_name: string;
  email: string;
  telephone: string;
  is_male: boolean = false;
  birth_date: number;
  address: string;
  type: number;
  login_id: string;
  open_emr_id: string;
  physican_type: number;
  department_id: string;
  department_speciality_id: string;
  title: string;
  qualification: string;
  ward_id : string;
  disabled:boolean;
  signature_id: string;
  signature_url: string;
  count:number;

}

export class DoctorSummaryPartialList {
  data: DoctorSummary[];
  total: number;
}

export class DoctorDetail extends DoctorSummary {
}

export class DoctorDashboard {
  total: number;
}

export function getPrefixPhysicianName(doctor: PrefixedDoctor){
  const utilsString = new TCUtilsString(null);
  let name = `${doctor.physician_first_name || ""} ${doctor.physician_middle_name || ""} ${doctor.physician_last_name || ""} `
  name = utilsString.valueOrDefaultString(name, doctor.physician_title)
  const qualification = utilsString.valueOrDefaultString(doctor.physician_qualification, "(", ")")
  return utilsString.valueOrDefaultString(name, "", qualification);
}

export function getPhysicianName(doctor: DoctorDetail){
  const utilsString = new TCUtilsString(null);
  let name = `${doctor.first_name || ""} ${doctor.middle_name || ""} ${doctor.last_name || ""} `
  name = utilsString.valueOrDefaultString(name, doctor.title)
  const qualification = utilsString.valueOrDefaultString(doctor.qualification, "(", ")")
  return utilsString.valueOrDefaultString(name, "", qualification);
}