import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DoctorEditComponent} from "./doctor-edit/doctor-edit.component";
import {DoctorPickComponent} from "./doctor-pick/doctor-pick.component";
import {physican_type} from "../app.enums";
import { TCUtilsString } from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})

export class DoctorNavigator {

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public tcUtilsString: TCUtilsString,
    ) {
  }

  doctorsUrl(): string {
    return "/medical_practitioners";
  }

  doctorUrl(id: string): string {
    return "/medical_practitioners/" + id;
  }

  viewDoctors(): void {
    this.router.navigateByUrl(this.doctorsUrl());
  }

  viewDoctor(id: string): void {
    this.router.navigateByUrl(this.doctorUrl(id));
  }

  editDoctor(id: string): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DoctorEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDoctor(): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DoctorEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickDoctors(selectOne: boolean = false, physicianType: number = -1, department_id: string = null, department_specialty_id: string = null, disabled:boolean = false): MatDialogRef<unknown, any> {
    const dialogRef = this.dialog.open(DoctorPickComponent, {
      data: {selectOne, physicianType, department_id, department_specialty_id,disabled},
      width: TCModalWidths.large,
    });
    return dialogRef; 
  }


  goDoctorHome(): void {
    this.router.navigateByUrl("/doctor-home");
  }

  goLabHome(): void {
    this.router.navigateByUrl("/lab-home");
  }

  goNurseHome(): void {
    this.router.navigateByUrl("/nurse-home");
  }
}
