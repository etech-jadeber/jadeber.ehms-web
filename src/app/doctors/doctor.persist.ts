import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TcDictionary, TCEnumTranslation, TCId} from "../tc/models";
import {DoctorDashboard, DoctorDetail, DoctorSummary, DoctorSummaryPartialList} from "./doctor.model";
import {TCAuthorization} from '../tc/authorization';
import {UserContexts} from '../tc/app.enums';
import {physican_type, physician_type} from '../app.enums';
import {AppTranslation} from '../app.translation';
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class DoctorPersist {

  // doctorSearchText: string = "";
  // doctorId: string = null;
  // nurseId: string;

  // disabled: boolean;

  // duration_time:number;
  // surgery_date: number;
  // physician_typeId: number;
  // department_id: string = "-1";
  // department_speciality_id: string = "-1";

  physician_type: TCEnumTranslation[] = [
    new TCEnumTranslation(physician_type.general, this.appTranslation.getKey('employee', 'general')),
    new TCEnumTranslation(physician_type.cardiologist, this.appTranslation.getKey('employee', 'cardiologist')),
    new TCEnumTranslation(physician_type.neurologist, this.appTranslation.getKey('employee', 'neurologist')),
  ];

  // physican_typeId: number;

  physican_type: TCEnumTranslation[] = [
    new TCEnumTranslation(physican_type.doctor, this.appTranslation.getKey('employee', 'doctor')),
    new TCEnumTranslation(physican_type.mental_health_professional, this.appTranslation.getKey('employee', 'mental_health_professional')),
    new TCEnumTranslation(physican_type.psychatric_nurse, this.appTranslation.getKey('employee', 'psychatric_nurse')),
    new TCEnumTranslation(physican_type.nurse, this.appTranslation.getKey('employee', 'nurse')),
    new TCEnumTranslation(physican_type.lab, this.appTranslation.getKey('employee', 'lab')),
    new TCEnumTranslation(physican_type.radiology, this.appTranslation.getKey('employee', 'radiology')),
    new TCEnumTranslation(physican_type.physiotherapy, this.appTranslation.getKey('employee', 'physiotherapy')),
    new TCEnumTranslation(physican_type.pharmacist, this.appTranslation.getKey('employee', 'pharmacist')),
    // new TCEnumTranslation(physican_type.midwives, this.appTranslation.getKey('employee', 'midwives')),
  ];

  constructor(private http: HttpClient, private tcAuthorization: TCAuthorization, private appTranslation: AppTranslation) {
  }

  doctorSearchHistory = {
    "department_speciality_id": "",
    "department_id": "",
    "physican_type": undefined,
    "duration_time": undefined,
    "surgery_date": undefined,
    "disabled": false,
    "search_text": "",
    "search_column": "",
  }

  defaultDoctorSearchHistory = {...this.doctorSearchHistory}

  resetDoctorSearchHistory(){
  this.doctorSearchHistory = {...this.defaultDoctorSearchHistory}
  }

  searchDoctor(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.doctorSearchHistory): Observable<DoctorSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("doctors", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);

    // if (this.physican_typeId) {
    //   url = TCUtilsString.appendUrlParameter(url, "physican_type", this.physican_typeId.toString());
    // }
    // if(this.duration_time){
    //   url=TCUtilsString.appendUrlParameter(url,"duration_time",this.duration_time.toString());
    // }
    // if(this.surgery_date){
    //   url=TCUtilsString.appendUrlParameter(url,"surgery_date",this.surgery_date.toString());
    // }

    // if (this.department_speciality_id && this.department_speciality_id != "-1") {
    //   url = TCUtilsString.appendUrlParameter(url, "department_speciality_id", this.department_speciality_id);
    // }
    // if (this.department_id && this.department_id != "-1") {
    //   url = TCUtilsString.appendUrlParameter(url, "department_id", this.department_id);
    // }
    // if (this.disabled != null) {
    //   url = TCUtilsString.appendUrlParameter(url, "disabled", this.disabled.toString());
    // }

    return this.http.get<DoctorSummaryPartialList>(url);

  }

  filters(searchHistory = this.doctorSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  clearFilters() {
    // this.physican_typeId = null;
    // this.department_id = "-1";
    // this.department_speciality_id = "-1";
    // this.disabled = null;
  }

  importEmployee () {
    return environment.tcApiBaseUri + "import_employee"
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "doctors/do", new TCDoParam("download_all", this.filters()));
  }

  doctorDashboard(): Observable<DoctorDashboard> {
    return this.http.get<DoctorDashboard>(environment.tcApiBaseUri + "doctors/dashboard");
  }

  getDoctor(id: any): Observable<DoctorDetail> {
    return this.http.get<DoctorDetail>(environment.tcApiBaseUri + "doctors/" + id);
  }

  addDoctor(item: DoctorDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "doctors/", item);
  }

  updateDoctor(item: DoctorDetail): Observable<DoctorDetail> {
    return this.http.patch<DoctorDetail>(environment.tcApiBaseUri + "doctors/" + item.id, item);
  }

  deleteDoctor(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "doctors/" + id);
  }

  doctorsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "doctors/do", new TCDoParam(method, payload));
  }

  doctorDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "doctors/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "doctors/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "doctors/" + id + "/do", new TCDoParam("print", {}));
  }


  initDoctorSelf(): void {
    let doctorIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.doctor);
    if (doctorIds.length == 1) {
      // this.doctorId = doctorIds[0];
    }
  }

  initNurseSelf(): void {
    let nurseIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.nurse);
    if (nurseIds.length == 1) {
      // this.nurseId = nurseIds[0];
    }
  }


  getDocotrSelf(): Observable<DoctorSummary> {
    return this.http.get<DoctorSummary>(environment.tcApiBaseUri + "doctor-self");
  }

}
