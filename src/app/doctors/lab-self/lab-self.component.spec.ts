import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabSelfComponent } from './lab-self.component';

describe('LabSelfComponent', () => {
  let component: LabSelfComponent;
  let fixture: ComponentFixture<LabSelfComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabSelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabSelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
