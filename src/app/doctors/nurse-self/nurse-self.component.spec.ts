import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NurseSelfComponent } from './nurse-self.component';

describe('NurseSelfComponent', () => {
  let component: NurseSelfComponent;
  let fixture: ComponentFixture<NurseSelfComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NurseSelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseSelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
