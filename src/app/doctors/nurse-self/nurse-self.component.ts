import {Component, OnInit} from '@angular/core';
import {AppTranslation} from "../../app.translation";
import {TCNavigator} from "../../tc/navigator";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCAuthentication} from "../../tc/authentication";
import {UserPersist} from "../../tc/users/user.persist";
import {TCNotification} from "../../tc/notification";
import {environment} from "../../../environments/environment";
import {TCAuthorization} from "../../tc/authorization";
import {DoctorDetail} from "../doctor.model";
import {DoctorPersist} from "../doctor.persist";
import {physican_type} from "../../app.enums";
import { TCAppInit } from 'src/app/tc/app-init';


export enum NurseTabs {
  overview,
  profile,
  admissionForm,
  bed_assignment
}

@Component({
  selector: 'app-nurse-self',
  templateUrl: './nurse-self.component.html',
  styleUrls: ['./nurse-self.component.css']
})
export class NurseSelfComponent implements OnInit {

  activeTab: NurseTabs = NurseTabs.overview;
  nurseIdLoading: boolean = false;
  nurseTabs: typeof NurseTabs = NurseTabs;
  title = environment.appName;
  nurseDetail: DoctorDetail;
  message: string = "";
  TCAppInit = TCAppInit;
  role: number = physican_type.nurse;

  constructor(public appTranslation: AppTranslation,
              public tcNavigator: TCNavigator,
              public tcUtilsAngular: TCUtilsAngular,
              public userPersist: UserPersist,
              public tcNotification: TCNotification,
              public nursePersist: DoctorPersist,
              public tcAuthorization: TCAuthorization,
              public tcAuthentication: TCAuthentication,) {
    // this.nurseDetail = new DoctorDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireLogin();
    this.nursePersist.initNurseSelf();
  }

  ngAfterViewInit(): void {
    // this.loadDetails();
  };

  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }

  loadDetails() {
    this.nursePersist.getDocotrSelf().subscribe(result => {
        this.nurseDetail = result;
        this.nurseIdLoading = false;
      }
      , error => {
        console.error(error);
        this.nurseIdLoading = false;
      });
  }

  logout(){
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }
}
