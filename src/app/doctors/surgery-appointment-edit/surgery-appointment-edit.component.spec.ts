import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryAppointmentEditComponent } from './surgery-appointment-edit.component';

describe('SurgeryAppointmentEditComponent', () => {
  let component: SurgeryAppointmentEditComponent;
  let fixture: ComponentFixture<SurgeryAppointmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryAppointmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryAppointmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
