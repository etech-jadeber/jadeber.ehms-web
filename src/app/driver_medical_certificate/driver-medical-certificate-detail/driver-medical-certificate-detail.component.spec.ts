import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverMedicalCertificateDetailComponent } from './driver-medical-certificate-detail.component';

describe('DriverMedicalCertificateDetailComponent', () => {
  let component: DriverMedicalCertificateDetailComponent;
  let fixture: ComponentFixture<DriverMedicalCertificateDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverMedicalCertificateDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverMedicalCertificateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
