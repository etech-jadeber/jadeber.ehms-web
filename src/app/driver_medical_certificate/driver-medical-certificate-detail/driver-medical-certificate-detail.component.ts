import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DriverMedicalCertificatePersist} from "../driver_medical_certificate.persist";
import {DRIVER_MEDICAL_CERTIFICATENavigator} from "../driver_medical_certificate.navigator";
import { DriverMedicalCertificateDetail } from '../driver_medical_certificate.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { encounter_status, tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Physical_ExaminationDetail } from 'src/app/form_encounters/form_encounter.model';

export enum DriverMedicalCertificateTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-driver-medical-certificate-detail',
  templateUrl: './driver-medical-certificate-detail.component.html',
  styleUrls: ['./driver-medical-certificate-detail.component.css']
})
export class DriverMedicalCertificateDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  driverMedicalCertificateIsLoading:boolean = false;
  
  DriverMedicalCertificateTabs: typeof DriverMedicalCertificateTabs = DriverMedicalCertificateTabs;
  //basics

  driverMedicalCertificateDetail: DriverMedicalCertificateDetail;
  physicalExaminationDetail: Physical_ExaminationDetail;
  DriverMedicalCertificateTab :typeof tabs=tabs;
  activeTab: number;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcUtilsDate: TCUtilsDate,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public driverMedicalCertificateNavigator: DRIVER_MEDICAL_CERTIFICATENavigator,
              public  driverMedicalCertificatePersist: DriverMedicalCertificatePersist,
              public patientPersist: PatientPersist,
              public menuState: MenuState,
              public doctorPersist: DoctorPersist,
              public physicalExamination: Form_EncounterPersist,
              public stringUtilities: TCUtilsString,) {
    this.tcAuthorization.requireRead("driver_medical_certificates");
    this.driverMedicalCertificateDetail = new DriverMedicalCertificateDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  } 
  ngOnInit() {
    this.tcAuthorization.requireRead("driver_medical_certificates");
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.driverMedicalCertificateIsLoading = true;
    this.driverMedicalCertificatePersist.getDriverMedicalCertificate(id).subscribe(driverMedicalCertificateDetail => {
          this.driverMedicalCertificateDetail = driverMedicalCertificateDetail;
          if(this.physicalExamination.encounter_is_active == null){
            this.physicalExamination.getForm_Encounter(driverMedicalCertificateDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.physicalExamination.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.menuState.getDataResponse(this.driverMedicalCertificateDetail);
          this.patientPersist.getPatient(this.driverMedicalCertificateDetail.patient_id).subscribe(patient => {
            this.driverMedicalCertificateDetail.patient_name = patient.fname + " " + patient.lname;
          });
          this.doctorPersist.getDoctor(this.driverMedicalCertificateDetail.doctor_id).subscribe(doctor => {
            this.driverMedicalCertificateDetail.doctor_name = doctor.first_name + " " + doctor.last_name;
          });
          if(this.driverMedicalCertificateDetail.physical_examination_id != this.stringUtilities.invalid_id && this.driverMedicalCertificateDetail.physical_examination_id != null){
            this.physicalExamination.getPhysical_Examination(this.driverMedicalCertificateDetail.physical_examination_id).subscribe(result => {
              this.physicalExaminationDetail = result;
            })
          }else{
            this.driverMedicalCertificateDetail.physical_examination_id = null;
          }
          this.driverMedicalCertificateIsLoading = false;
        }, error => {
          console.error(error);
          this.driverMedicalCertificateIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.driverMedicalCertificateDetail.id);
  }
  editDriverMedicalCertificate(): void {
    let modalRef = this.driverMedicalCertificateNavigator.editDriver_medical_certificate(this.driverMedicalCertificateDetail.id);
    modalRef.afterClosed().subscribe(driverMedicalCertificateDetail => {
      TCUtilsAngular.assign(this.driverMedicalCertificateDetail, driverMedicalCertificateDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.driverMedicalCertificatePersist.print(this.driverMedicalCertificateDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print driver_medical_certificate", true);
      });
    }

  back():void{
      this.location.back();
    }

}