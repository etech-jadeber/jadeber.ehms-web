import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverMedicalCertificateEditComponent } from './driver-medical-certificate-edit.component';

describe('DriverMedicalCertificateEditComponent', () => {
  let component: DriverMedicalCertificateEditComponent;
  let fixture: ComponentFixture<DriverMedicalCertificateEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverMedicalCertificateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverMedicalCertificateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
