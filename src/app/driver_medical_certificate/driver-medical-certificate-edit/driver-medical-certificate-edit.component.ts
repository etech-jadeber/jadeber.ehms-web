import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DriverMedicalCertificateDetail } from '../driver_medical_certificate.model';
import { DriverMedicalCertificatePersist } from '../driver_medical_certificate.persist';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Physical_ExaminationDetail } from 'src/app/form_encounters/form_encounter.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DriverLicenseType } from 'src/app/app.enums';
@Component({
  selector: 'app-driver-medical-certificate-edit',
  templateUrl: './driver-medical-certificate-edit.component.html',
  styleUrls: ['./driver-medical-certificate-edit.component.css']
})

export class DriverMedicalCertificateEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientName: string;
  doctorName: string;
  driverLicenseType=DriverLicenseType
  driverMedicalCertificateDetail: DriverMedicalCertificateDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DriverMedicalCertificateEditComponent>,
              public persist: DriverMedicalCertificatePersist,
              public patientNavigator: PatientNavigator,
              public tcUtilsDate: TCUtilsDate,
              public doctorNavigator: DoctorNavigator,
              public physicalExaminationNavigator: Form_EncounterNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("driver_medical_certificates");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText('patient', "driver_medical_certificate");
      this.driverMedicalCertificateDetail = new DriverMedicalCertificateDetail();
      //set necessary defaults
      this.driverMedicalCertificateDetail.status = -1;
      this.driverMedicalCertificateDetail.status_reason = "";

    } else {
     this.tcAuthorization.requireUpdate("driver_medical_certificates");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + this.appTranslation.getText('patient', "driver_medical_certificate");
      this.isLoadingResults = true;
      this.persist.getDriverMedicalCertificate(this.idMode.id).subscribe(driverMedicalCertificateDetail => {
        this.driverMedicalCertificateDetail = driverMedicalCertificateDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } 
   onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addDriverMedicalCertificate(this.idMode.id,this.driverMedicalCertificateDetail).subscribe(value => {
      this.tcNotification.success("driverMedicalCertificate added");
      this.driverMedicalCertificateDetail.id = value.id;
      this.dialogRef.close(this.driverMedicalCertificateDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }


  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateDriverMedicalCertificate(this.driverMedicalCertificateDetail).subscribe(value => {
      this.tcNotification.success("driver_medical_certificate updated");
      this.dialogRef.close(this.driverMedicalCertificateDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  

 canSubmit(): boolean {
    if (this.driverMedicalCertificateDetail == null){
      return false;
    }
    if (this.driverMedicalCertificateDetail.pupil_color == null || this.driverMedicalCertificateDetail.pupil_color == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.pupil_right_eye == null || this.driverMedicalCertificateDetail.pupil_right_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.pupil_left_eye == null || this.driverMedicalCertificateDetail.pupil_left_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.color_perception_left_eye == null || this.driverMedicalCertificateDetail.color_perception_left_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.color_perception_right_eye == null || this.driverMedicalCertificateDetail.color_perception_right_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.visual_distance_left_eye == null || this.driverMedicalCertificateDetail.visual_distance_left_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.visual_distance_right_eye == null || this.driverMedicalCertificateDetail.visual_distance_right_eye == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.sound_perception_left_ear == null || this.driverMedicalCertificateDetail.sound_perception_left_ear == ""){
      return false;
    }
    if (this.driverMedicalCertificateDetail.sound_perception_right_ear == null || this.driverMedicalCertificateDetail.sound_perception_right_ear == ""){
      return false;
    }
    if ((this.driverMedicalCertificateDetail.status_reason == null || this.driverMedicalCertificateDetail.status_reason == "") && this.driverMedicalCertificateDetail.status !=DriverLicenseType.granted){
      return false;
    }
    if (this.driverMedicalCertificateDetail.status == -1){
      return false;
    }
    return true;
 }

 searchPatient():void{
  let dialogRef =  this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe(
      (res:PatientDetail[])=>{
        this.driverMedicalCertificateDetail.patient_id = res[0].id;
        this.patientName = res[0].fname + " " + res[0].mname + " " + res[0].lname;
      }
    )
  }

  searchDoctor(): void{
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe(
      (res: DoctorDetail[]) => {
        this.driverMedicalCertificateDetail.doctor_id = res[0].id;
        this.doctorName = res[0].first_name + " " + res[0].last_name;
      }
    )
  }
 }
