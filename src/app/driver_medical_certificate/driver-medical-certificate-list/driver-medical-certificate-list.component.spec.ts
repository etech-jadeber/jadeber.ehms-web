import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverMedicalCertificateListComponent } from './driver-medical-certificate-list.component';

describe('DriverMedicalCertificateListComponent', () => {
  let component: DriverMedicalCertificateListComponent;
  let fixture: ComponentFixture<DriverMedicalCertificateListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverMedicalCertificateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverMedicalCertificateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
