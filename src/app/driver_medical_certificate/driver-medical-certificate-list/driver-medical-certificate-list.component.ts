import {Component, OnInit, ViewChild,Input,Output,EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCModalWidths, TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DriverMedicalCertificateDetail, DriverMedicalCertificateSummary, DriverMedicalCertificateSummaryPartialList } from '../driver_medical_certificate.model';
import { DriverMedicalCertificatePersist } from '../driver_medical_certificate.persist';
import { DRIVER_MEDICAL_CERTIFICATENavigator } from '../driver_medical_certificate.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-driver-medical-certificate-list',
  templateUrl: './driver-medical-certificate-list.component.html',
  styleUrls: ['./driver-medical-certificate-list.component.css']
})export class DriverMedicalCertificateListComponent implements OnInit {
  driverMedicalCertificatesData: DriverMedicalCertificateSummary[] = [];
  driverMedicalCertificatesTotalCount: number = 0;
  driverMedicalCertificateSelectAll:boolean = false;
  driverMedicalCertificateSelection: DriverMedicalCertificateSummary[] = [];

 driverMedicalCertificatesDisplayedColumns: string[] = ["select", "action", "date", "patient_id", "doctor_id", "status" ];
  driverMedicalCertificateSearchTextBox: FormControl = new FormControl();
  driverMedicalCertificateIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) driverMedicalCertificatesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) driverMedicalCertificatesSort: MatSort;  
  
  @Input() encounterId :any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public driverMedicalCertificatePersist: DriverMedicalCertificatePersist,
                public driverMedicalCertificateNavigator: DRIVER_MEDICAL_CERTIFICATENavigator,
                public patientNavigator: PatientNavigator,
                public doctorNavigator: DoctorNavigator,
                public jobPersist: JobPersist,
                public doctorPersist: DoctorPersist,
                public patientPersist: PatientPersist,
                public form_encounterPersist: Form_EncounterPersist,

    ) {

        this.tcAuthorization.requireRead("driver_medical_certificates");
       this.driverMedicalCertificateSearchTextBox.setValue(driverMedicalCertificatePersist.driverMedicalCertificateSearchText);
      //delay subsequent keyup events
      this.driverMedicalCertificateSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.driverMedicalCertificatePersist.driverMedicalCertificateSearchText = value;
        this.searchDriver_medical_certificates();
      });
    } 
    ngOnInit() {
      this.driverMedicalCertificatePersist.patientId = this.patientId
   
      this.driverMedicalCertificatesSort.sortChange.subscribe(() => {
        this.driverMedicalCertificatesPaginator.pageIndex = 0;
        this.searchDriver_medical_certificates(true);
      });

      this.driverMedicalCertificatesPaginator.page.subscribe(() => {
        this.searchDriver_medical_certificates(true);
      });
      //start by loading items
      this.searchDriver_medical_certificates();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.driverMedicalCertificatePersist.encounterId = this.encounterId;
      } else {
      this.driverMedicalCertificatePersist.encounterId = null;
      }
      this.searchDriver_medical_certificates()
      }

  searchDriver_medical_certificates(isPagination:boolean = false): void {


    let paginator = this.driverMedicalCertificatesPaginator;
    let sorter = this.driverMedicalCertificatesSort;

    this.driverMedicalCertificateIsLoading = true;
    this.driverMedicalCertificateSelection = [];

    this.driverMedicalCertificatePersist.searchDriverMedicalCertificate(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DriverMedicalCertificateSummaryPartialList) => {
      this.driverMedicalCertificatesData = partialList.data;
      this.driverMedicalCertificatesData.forEach((driverMedicalCertificate, idx) => {
        this.patientPersist.getPatient(driverMedicalCertificate.patient_id).subscribe((patient: PatientDetail) => {
          this.driverMedicalCertificatesData[idx].patient_name = patient.fname + " " + patient.lname
        });
        this.doctorPersist.getDoctor(driverMedicalCertificate.doctor_id).subscribe(doctor => {
          this.driverMedicalCertificatesData[idx].doctor_name = doctor.first_name + " " + doctor.last_name
        })
      })
      
      if (partialList.total != -1) {
        this.driverMedicalCertificatesTotalCount = partialList.total;
      }
      this.driverMedicalCertificateIsLoading = false;
    }, error => {
      this.driverMedicalCertificateIsLoading = false;
    });

  } 
  
  downloadDriverMedicalCertificates(): void {
    if(this.driverMedicalCertificateSelectAll){
         this.driverMedicalCertificatePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate", true);
      });
    }
    else{
        this.driverMedicalCertificatePersist.download(this.tcUtilsArray.idsList(this.driverMedicalCertificateSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate",true);
            });
        }
  }
addDriver_medical_certificate(): void {
    let dialogRef = this.driverMedicalCertificateNavigator.addDriver_medical_certificate(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDriver_medical_certificates();
      }
    });
  }

  editDriverMedicalCertificate(item: DriverMedicalCertificateSummary) {
    let dialogRef = this.driverMedicalCertificateNavigator.editDriver_medical_certificate(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDriverMedicalCertificate(item: DriverMedicalCertificateSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("driver_medical_certificate");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.driverMedicalCertificatePersist.deleteDriverMedicalCertificate(item.id).subscribe(response => {
          this.tcNotification.success("driver_medical_certificate deleted");
          this.searchDriver_medical_certificates();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
  }
