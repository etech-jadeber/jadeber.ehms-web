import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DriverMedicalCertificatePickComponent } from './driver-medical-certificate-pick.component';

describe('DriverMedicalCertificatePickComponent', () => {
  let component: DriverMedicalCertificatePickComponent;
  let fixture: ComponentFixture<DriverMedicalCertificatePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverMedicalCertificatePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverMedicalCertificatePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
