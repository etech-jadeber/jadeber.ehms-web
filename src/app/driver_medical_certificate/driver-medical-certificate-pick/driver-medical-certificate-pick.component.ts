import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { DriverMedicalCertificateSummary, DriverMedicalCertificateSummaryPartialList } from '../driver_medical_certificate.model';
import { DriverMedicalCertificatePersist } from '../driver_medical_certificate.persist';

import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DRIVER_MEDICAL_CERTIFICATENavigator } from '../driver_medical_certificate.navigator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-driver_medical_certificate-pick',
  templateUrl: './driver-medical-certificate-pick.component.html',
  styleUrls: ['./driver-medical-certificate-pick.component.css']
})export class DriverMedicalCertificatePickComponent implements OnInit {
  driverMedicalCertificatesData: DriverMedicalCertificateSummary[] = [];
  driverMedicalCertificatesTotalCount: number = 0;
  driverMedicalCertificateSelectAll:boolean = false;
  driverMedicalCertificateSelection: DriverMedicalCertificateSummary[] = [];

 driverMedicalCertificatesDisplayedColumns: string[] = ["select", ,"patient_id","pupils_color","pupil_color","pupil_right_eye","pupil_left_eye","color_perception_left_eye","color_perception_right_eye","visual_distance_right_eye_test","visual_distance_light_eye_test","sound_perception_right_ear","sound_perception_left_ear","physical_examination_id","doctor_id","Status","status","status_reason","date" ];
  driverMedicalCertificateSearchTextBox: FormControl = new FormControl();
  driverMedicalCertificateIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) driverMedicalCertificatesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) driverMedicalCertificatesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public driverMedicalCertificatePersist: DriverMedicalCertificatePersist,
                public driverMedicalCertificateNavigator: DRIVER_MEDICAL_CERTIFICATENavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<DriverMedicalCertificatePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("driver_medical_certificates");
       this.driverMedicalCertificateSearchTextBox.setValue(driverMedicalCertificatePersist.driverMedicalCertificateSearchText);
      //delay subsequent keyup events
      this.driverMedicalCertificateSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.driverMedicalCertificatePersist.driverMedicalCertificateSearchText = value;
        this.searchDriver_medical_certificates();
      });
    } ngOnInit() {
   
      this.driverMedicalCertificatesSort.sortChange.subscribe(() => {
        this.driverMedicalCertificatesPaginator.pageIndex = 0;
        this.searchDriver_medical_certificates(true);
      });

      this.driverMedicalCertificatesPaginator.page.subscribe(() => {
        this.searchDriver_medical_certificates(true);
      });
      //start by loading items
      this.searchDriver_medical_certificates();
    }

  searchDriver_medical_certificates(isPagination:boolean = false): void {


    let paginator = this.driverMedicalCertificatesPaginator;
    let sorter = this.driverMedicalCertificatesSort;

    this.driverMedicalCertificateIsLoading = true;
    this.driverMedicalCertificateSelection = [];

    this.driverMedicalCertificatePersist.searchDriverMedicalCertificate(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DriverMedicalCertificateSummaryPartialList) => {
      this.driverMedicalCertificatesData = partialList.data;
      if (partialList.total != -1) {
        this.driverMedicalCertificatesTotalCount = partialList.total;
      }
      this.driverMedicalCertificateIsLoading = false;
    }, error => {
      this.driverMedicalCertificateIsLoading = false;
    });

  }
  markOneItem(item: DriverMedicalCertificateSummary) {
    if(!this.tcUtilsArray.containsId(this.driverMedicalCertificateSelection,item.id)){
          this.driverMedicalCertificateSelection = [];
          this.driverMedicalCertificateSelection.push(item);
        }
        else{
          this.driverMedicalCertificateSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.driverMedicalCertificateSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }