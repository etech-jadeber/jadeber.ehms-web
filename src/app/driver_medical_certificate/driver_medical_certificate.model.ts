import {TCId} from "../tc/models";

export class DriverMedicalCertificateSummary extends TCId {
    encounter_id: string;
    patient_id:string;
    pupil_color:string;
    pupil_right_eye:string;
    pupil_left_eye:string;
    color_perception_left_eye:string;
    color_perception_right_eye:string;
    visual_distance_right_eye:string;
    visual_distance_left_eye:string;
    sound_perception_right_ear:string;
    sound_perception_left_ear:string;
    physical_examination_id:string;
    doctor_id:string;
    status:number;
    status_reason:string;
    date:number;
    patient_name: string;
    doctor_name: string;
    }
    export class DriverMedicalCertificateSummaryPartialList {
      data: DriverMedicalCertificateSummary[];
      total: number;
    }
    export class DriverMedicalCertificateDetail extends DriverMedicalCertificateSummary {
    }
    
    export class DriverMedicalCertificateDashboard {
      total: number;
    }