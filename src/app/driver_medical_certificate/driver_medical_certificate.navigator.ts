import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {DriverMedicalCertificateEditComponent} from "./driver-medical-certificate-edit/driver-medical-certificate-edit.component";
import { DriverMedicalCertificatePickComponent } from "./driver-medical-certificate-pick/driver-medical-certificate-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DRIVER_MEDICAL_CERTIFICATENavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  driver_medical_certificatesUrl(): string {
    return "/driver_medical_certificates";
  }

  driver_medical_certificateUrl(id: string): string {
    return "/driver_medical_certificates/" + id;
  }

  viewDriver_medical_certificates(): void {
    this.router.navigateByUrl(this.driver_medical_certificatesUrl());
  }

  viewDriver_medical_certificate(id: string): void {
    this.router.navigateByUrl(this.driver_medical_certificateUrl(id));
  }

  editDriver_medical_certificate(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DriverMedicalCertificateEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDriver_medical_certificate(encounter_id : string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DriverMedicalCertificateEditComponent, {
          data: new TCIdMode(encounter_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
  pickDriver_medical_certificates(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(DriverMedicalCertificatePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}