import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {DriverMedicalCertificateDashboard, DriverMedicalCertificateDetail, DriverMedicalCertificateSummaryPartialList} from "./driver_medical_certificate.model";
import { DriverLicenseType } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DriverMedicalCertificatePersist {
  encounterId: string;
  patientId: string;
 driverMedicalCertificateSearchText: string = "";constructor(private http: HttpClient) {
  }

  driverLicenseTypeFilter: number ;

    DriverLicenseType: TCEnum[] = [
     new TCEnum( DriverLicenseType.granted, 'granted'),
  new TCEnum( DriverLicenseType.granted_with_restriction, 'granted_with_restriction'),
  new TCEnum( DriverLicenseType.refused, 'refused'),

  ];

  searchDriverMedicalCertificate(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DriverMedicalCertificateSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("driver_medical_certificate", this.driverMedicalCertificateSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<DriverMedicalCertificateSummaryPartialList>(url);

  }
  
  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.driverMedicalCertificateSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "driver_medical_certificate/do", new TCDoParam("download_all", this.filters()));
  }

  driverMedicalCertificateDashboard(): Observable<DriverMedicalCertificateDashboard> {
    return this.http.get<DriverMedicalCertificateDashboard>(environment.tcApiBaseUri + "driver_medical_certificate/dashboard");
  }

  getDriverMedicalCertificate(id: string): Observable<DriverMedicalCertificateDetail> {
    return this.http.get<DriverMedicalCertificateDetail>(environment.tcApiBaseUri + "driver_medical_certificate/" + id);
  } addDriverMedicalCertificate(parent_id : string ,item: DriverMedicalCertificateDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/"+parent_id+"/driver_medical_certificate", item);
  }

  updateDriverMedicalCertificate(item: DriverMedicalCertificateDetail): Observable<DriverMedicalCertificateDetail> {
    return this.http.patch<DriverMedicalCertificateDetail>(environment.tcApiBaseUri + "driver_medical_certificate/" + item.id, item);
  }

  driverMedicalCertificatesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'driver_medical_certificate/do',
      new TCDoParam(method, payload)
    );
  }

  driverMedicalCertificateDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'driver_medical_certificate/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  deleteDriverMedicalCertificate(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "driver_medical_certificate/" + id);
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "departments/" + id + "/do", new TCDoParam("print", {}));
  }
}