import { TCId } from '../tc/models';

export class DrugtypeSummary extends TCId {
  name: string;
  strength: string;
  form: string;
}

export class DrugtypeSummaryPartialList {
  data: DrugtypeSummary[];
  total: number;
}

export class DrugtypeDetail extends DrugtypeSummary {
  name: string;
  strength: string;
  form: string;
}

export class DrugtypeDashboard {
  total: number;
}
