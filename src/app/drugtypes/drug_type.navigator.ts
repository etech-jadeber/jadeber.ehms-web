import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';
import { DrugtypeEditComponent } from './drugtype-edit/drugtype-edit.component';
import { DrugtypePickComponent } from './drugtype-pick/drugtype-pick.component';

@Injectable({
  providedIn: 'root',
})
export class DrugtypeNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}

  drug_typesUrl(): string {
    return '/drugtypes';
  }

  drug_typeUrl(id: string): string {
    return '/drug_types/' + id;
  }

  viewDrug_Types(): void {
    this.router.navigateByUrl(this.drug_typesUrl());
  }

  viewDrug_Type(id: string): void {
    this.router.navigateByUrl(this.drug_typeUrl(id));
  }

  editDrug_Type(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DrugtypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addDrug_Type(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DrugtypeEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickDrug_Types(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DrugtypePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
