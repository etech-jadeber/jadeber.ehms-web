import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  DrugtypeDashboard,
  DrugtypeDetail,
  DrugtypeSummaryPartialList,
} from './drug_type.model';

@Injectable({
  providedIn: 'root',
})
export class DrugtypePersist {
  drug_typeSearchText: string = '';

  constructor(private http: HttpClient) {}

  searchDrug_Type(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<DrugtypeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'drugtypes',
      this.drug_typeSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<DrugtypeSummaryPartialList>(url);
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.drug_typeSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'drugtypes/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  drug_typeDashboard(): Observable<DrugtypeDashboard> {
    return this.http.get<DrugtypeDashboard>(
      environment.tcApiBaseUri + 'drugtypes/dashboard'
    );
  }

  getDrug_Type(id: string): Observable<DrugtypeDetail> {
    return this.http.get<DrugtypeDetail>(
      environment.tcApiBaseUri + 'drugtypes/' + id
    );
  }

  addDrug_Type(item: DrugtypeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + 'drugtypes/', item);
  }

  updateDrug_Type(item: DrugtypeDetail): Observable<DrugtypeDetail> {
    return this.http.patch<DrugtypeDetail>(
      environment.tcApiBaseUri + 'drugtypes/' + item.id,
      item
    );
  }

  deleteDrug_Type(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'drugtypes/' + id);
  }

  drug_typesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'drugtypes/do',
      new TCDoParam(method, payload)
    );
  }

  drug_typeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'drugtypes/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'drugtypes/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'drugtypes/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
