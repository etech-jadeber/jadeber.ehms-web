import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DrugtypeDetailComponent } from './drugtype-detail.component';

describe('DrugtypeDetailComponent', () => {
  let component: DrugtypeDetailComponent;
  let fixture: ComponentFixture<DrugtypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugtypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugtypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
