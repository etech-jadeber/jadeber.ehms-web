import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { DrugtypeDetail } from '../drug_type.model';
import { DrugtypeNavigator } from '../drug_type.navigator';
import { DrugtypePersist } from '../drug_type.persist';


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-drug_type-detail',
  templateUrl: './drugtype-detail.component.html',
  styleUrls: ['./drugtype-detail.component.css']
})
export class DrugtypeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  drug_typeLoading:boolean = false;
  //basics
  drug_typeDetail: DrugtypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public drug_typeNavigator: DrugtypeNavigator,
              public  drug_typePersist: DrugtypePersist) {
    this.tcAuthorization.requireRead("drug_types");
    this.drug_typeDetail = new DrugtypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("drug_types");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.drug_typeLoading = true;
    this.drug_typePersist.getDrug_Type(id).subscribe(drug_typeDetail => {
          this.drug_typeDetail = drug_typeDetail;
          this.drug_typeLoading = false;
        }, error => {
          console.error(error);
          this.drug_typeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.drug_typeDetail.id);
  }

  editDrug_Type(): void {
    let modalRef = this.drug_typeNavigator.editDrug_Type(this.drug_typeDetail.id);
    modalRef.afterClosed().subscribe(modifiedDrug_TypeDetail => {
      TCUtilsAngular.assign(this.drug_typeDetail, modifiedDrug_TypeDetail);
    }, error => {
      console.error(error);
    });
  }

   printDrug_Type():void{
      this.drug_typePersist.print(this.drug_typeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print drug_type", true);
      });
    }

  back():void{
      this.location.back();
    }

}