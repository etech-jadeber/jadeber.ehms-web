import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DrugtypeEditComponent } from './drugtype-edit.component';

describe('DrugtypeEditComponent', () => {
  let component: DrugtypeEditComponent;
  let fixture: ComponentFixture<DrugtypeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugtypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugtypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
