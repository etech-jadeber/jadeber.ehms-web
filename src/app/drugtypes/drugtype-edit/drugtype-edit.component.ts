import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { DrugtypeDetail } from '../drug_type.model';
import { DrugtypePersist } from '../drug_type.persist';

@Component({
  selector: 'app-drug_type-edit',
  templateUrl: './drugtype-edit.component.html',
  styleUrls: ['./drugtype-edit.component.css'],
})
export class DrugtypeEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  drug_typeDetail: DrugtypeDetail;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<DrugtypeEditComponent>,
    public persist: DrugtypePersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('drug_types');
      this.title = this.appTranslation.getText('general', 'new') + ' ' + this.appTranslation.getText("inventory", "drug_types");
      this.drug_typeDetail = new DrugtypeDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('drug_types');
      this.title =
        this.appTranslation.getText('general', 'edit') + ' ' + this.appTranslation.getText("inventory", "drug_types");
      this.isLoadingResults = true;
      this.persist.getDrug_Type(this.idMode.id).subscribe(
        (drug_typeDetail) => {
          this.drug_typeDetail = drug_typeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addDrug_Type(this.drug_typeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Drug_Type added');
        this.drug_typeDetail.id = value.id;
        this.dialogRef.close(this.drug_typeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDrug_Type(this.drug_typeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Drug_Type updated');
        this.dialogRef.close(this.drug_typeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.drug_typeDetail == null) {
      return false;
    }

    if (this.drug_typeDetail.name == null || this.drug_typeDetail.name == '') {
      return false;
    }

    if (
      this.drug_typeDetail.strength == null ||
      this.drug_typeDetail.strength == ''
    ) {
      return false;
    }

    if (this.drug_typeDetail.form == null || this.drug_typeDetail.form == '') {
      return false;
    }

    return true;
  }
}
