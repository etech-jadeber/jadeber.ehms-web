import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DrugtypeListComponent } from './drugtype-list.component';

describe('DrugtypeListComponent', () => {
  let component: DrugtypeListComponent;
  let fixture: ComponentFixture<DrugtypeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugtypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugtypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
