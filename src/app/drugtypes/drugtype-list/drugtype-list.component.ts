import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  DrugtypeSummary,
  DrugtypeSummaryPartialList,
} from '../drug_type.model';
import { DrugtypePersist } from '../drug_type.persist';
import { DrugtypeNavigator } from '../drug_type.navigator';

@Component({
  selector: 'app-drug_type-list',
  templateUrl: './drugtype-list.component.html',
  styleUrls: ['./drugtype-list.component.css'],
})
export class DrugtypeListComponent implements OnInit {
  drug_typesData: DrugtypeSummary[] = [];
  drug_typesTotalCount: number = 0;
  drug_typesSelectAll: boolean = false;
  drug_typesSelection: DrugtypeSummary[] = [];

  drug_typesDisplayedColumns: string[] = [
    'select',
    'action',
    'name',
    'strength',
    'form',
  ];
  drug_typesSearchTextBox: FormControl = new FormControl();
  drug_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) drug_typesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) drug_typesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public drug_typePersist: DrugtypePersist,
    public drug_typeNavigator: DrugtypeNavigator,
    public jobPersist: JobPersist
  ) {
    this.tcAuthorization.requireRead('drug_types');
    this.drug_typesSearchTextBox.setValue(drug_typePersist.drug_typeSearchText);
    //delay subsequent keyup events
    this.drug_typesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.drug_typePersist.drug_typeSearchText = value;
        this.searchDrug_Types();
      });
  }

  ngOnInit() {
    this.drug_typesSort.sortChange.subscribe(() => {
      this.drug_typesPaginator.pageIndex = 0;
      this.searchDrug_Types(true);
    });

    this.drug_typesPaginator.page.subscribe(() => {
      this.searchDrug_Types(true);
    });
    //start by loading items
    this.searchDrug_Types();
  }

  searchDrug_Types(isPagination: boolean = false): void {
    let paginator = this.drug_typesPaginator;
    let sorter = this.drug_typesSort;

    this.drug_typesIsLoading = true;
    this.drug_typesSelection = [];

    this.drug_typePersist
      .searchDrug_Type(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: DrugtypeSummaryPartialList) => {
          this.drug_typesData = partialList.data;
          if (partialList.total != -1) {
            this.drug_typesTotalCount = partialList.total;
          }
          this.drug_typesIsLoading = false;
        },
        (error) => {
          this.drug_typesIsLoading = false;
        }
      );
  }

  downloadDrug_Types(): void {
    if (this.drug_typesSelectAll) {
      this.drug_typePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download drug_types', true);
      });
    } else {
      this.drug_typePersist
        .download(this.tcUtilsArray.idsList(this.drug_typesSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download drug_types',
            true
          );
        });
    }
  }

  addDrug_Type(): void {
    let dialogRef = this.drug_typeNavigator.addDrug_Type();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchDrug_Types();
      }
    });
  }

  editDrug_Type(item: DrugtypeSummary) {
    let dialogRef = this.drug_typeNavigator.editDrug_Type(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteDrug_Type(item: DrugtypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Drug_Type');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.drug_typePersist.deleteDrug_Type(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Drug_Type deleted');
            this.searchDrug_Types();
          },
          (error) => {}
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }
}
