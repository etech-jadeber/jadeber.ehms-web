import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DrugtypePickComponent } from './drugtype-pick.component';

describe('DrugtypePickComponent', () => {
  let component: DrugtypePickComponent;
  let fixture: ComponentFixture<DrugtypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugtypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugtypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
