import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { debounceTime } from 'rxjs/operators';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { AppTranslation } from '../../app.translation';
import {
  DrugtypeSummary,
  DrugtypeSummaryPartialList,
} from '../drug_type.model';
import { DrugtypePersist } from '../drug_type.persist';

@Component({
  selector: 'app-drug_type-pick',
  templateUrl: './drugtype-pick.component.html',
  styleUrls: ['./drugtype-pick.component.css'],
})
export class DrugtypePickComponent implements OnInit {
  drug_typesData: DrugtypeSummary[] = [];
  drug_typesTotalCount: number = 0;
  drug_typesSelection: DrugtypeSummary[] = [];
  drug_typesDisplayedColumns: string[] = ['select', 'name', 'strength', 'form'];

  drug_typesSearchTextBox: FormControl = new FormControl();
  drug_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) drug_typesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) drug_typesSort: MatSort;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public drug_typePersist: DrugtypePersist,
    public dialogRef: MatDialogRef<DrugtypePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('drug_types');
    this.drug_typesSearchTextBox.setValue(drug_typePersist.drug_typeSearchText);
    //delay subsequent keyup events
    this.drug_typesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.drug_typePersist.drug_typeSearchText = value;
        this.searchDrug_Types();
      });
  }

  ngOnInit() {
    this.drug_typesSort.sortChange.subscribe(() => {
      this.drug_typesPaginator.pageIndex = 0;
      this.searchDrug_Types();
    });

    this.drug_typesPaginator.page.subscribe(() => {
      this.searchDrug_Types();
    });

    //set initial picker list to 5
    this.drug_typesPaginator.pageSize = 5;

    //start by loading items
    this.searchDrug_Types();
  }

  searchDrug_Types(): void {
    this.drug_typesIsLoading = true;
    this.drug_typesSelection = [];

    this.drug_typePersist
      .searchDrug_Type(
        this.drug_typesPaginator.pageSize,
        this.drug_typesPaginator.pageIndex,
        this.drug_typesSort.active,
        this.drug_typesSort.direction
      )
      .subscribe(
        (partialList: DrugtypeSummaryPartialList) => {
          this.drug_typesData = partialList.data;
          if (partialList.total != -1) {
            this.drug_typesTotalCount = partialList.total;
          }
          this.drug_typesIsLoading = false;
        },
        (error) => {
          this.drug_typesIsLoading = false;
        }
      );
  }

  markOneItem(item: DrugtypeSummary) {
    if (!this.tcUtilsArray.containsId(this.drug_typesSelection, item.id)) {
      this.drug_typesSelection = [];
      this.drug_typesSelection.push(item);
    } else {
      this.drug_typesSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.drug_typesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
