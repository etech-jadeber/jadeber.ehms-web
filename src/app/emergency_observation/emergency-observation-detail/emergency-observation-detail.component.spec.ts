import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyObservationDetailComponent } from './emergency-observation-detail.component';

describe('EmergencyObservationDetailComponent', () => {
  let component: EmergencyObservationDetailComponent;
  let fixture: ComponentFixture<EmergencyObservationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencyObservationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyObservationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
