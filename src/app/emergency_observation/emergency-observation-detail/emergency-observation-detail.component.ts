import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { EmergencyObservationDetail } from '../emergency_observation.model';
import { EmergencyObservationNavigator } from '../emergency_observation.navigator';
import { EmergencyObservationPersist } from '../emergency_observation.persist';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { InstructionType, physican_type } from 'src/app/app.enums';
import { FormControl } from '@angular/forms';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { OrdersDetail } from 'src/app/form_encounters/orderss/orders.model';
import { GeneralPatientDetail, PatientDetail } from 'src/app/patients/patients.model';



export enum EmergencyObservationTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-emergency-observation-detail',
  templateUrl: './emergency-observation-detail.component.html',
  styleUrls: ['./emergency-observation-detail.component.scss']
})
export class EmergencyObservationDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  emergencyObservationIsLoading:boolean = false;
  instructionType = InstructionType;
  nurseName:string;
  nurseId : string;
  ordersDetail: OrdersDetail;
  patientDetail: GeneralPatientDetail = new GeneralPatientDetail();

  date:FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  
  EmergencyObservationTabs: typeof EmergencyObservationTabs = EmergencyObservationTabs;
  activeTab: EmergencyObservationTabs = EmergencyObservationTabs.overview;
  //basics
  emergencyObservationDetail: EmergencyObservationDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public doctorNavigator: DoctorNavigator,
              public emergencyObservationNavigator: EmergencyObservationNavigator,
              public  emergencyObservationPersist: EmergencyObservationPersist) {
    this.tcAuthorization.requireRead("emergency_observations");
    this.emergencyObservationDetail = new EmergencyObservationDetail();
    this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.refresh();
    });
  } ngOnInit() {
    this.tcAuthorization.requireRead("emergency_observations");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.emergencyObservationIsLoading = true;
    this.emergencyObservationPersist.getEmergencyObservation(id).subscribe(emergencyObservationDetail => {
          this.emergencyObservationDetail = emergencyObservationDetail;
          this.emergencyObservationDetail.age = new Date().getFullYear()-new Date(emergencyObservationDetail.dob*1000).getFullYear();
          this.patientDetail.dob = this.emergencyObservationDetail.dob;
          this.patientDetail.sex = this.emergencyObservationDetail.sex;
          this.patientDetail.patient_name = this.emergencyObservationDetail.patient_name;
          this.emergencyObservationIsLoading = false;
        }, error => {
          console.error(error);
          this.emergencyObservationIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.emergencyObservationDetail.id);
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.emergencyObservationPersist.selected_index=matTab.index;
  }

  searchNurses():void{
    let dialogRef = this.doctorNavigator.pickDoctors(true,physican_type.nurse);

    dialogRef.afterClosed().subscribe((nurse:DoctorSummary[])=>{
      if(nurse){
        this.nurseName = nurse[0].first_name + " " + nurse[0].middle_name;
        this.nurseId = nurse[0].login_id;
        this.refresh();
      }
    })
  }
  handleBackButton(){
    this.ordersDetail = null;
  }

  refresh():void{
    let encounter_id = this.emergencyObservationDetail.encounter_id;
    this.emergencyObservationDetail.encounter_id = undefined;
    setTimeout(() => {
      this.emergencyObservationDetail.encounter_id = encounter_id;
    }, 1);
  }

  loadOrdersDetail(ordersDetail: OrdersDetail) {
    this.ordersDetail = ordersDetail
  }


   printBirth():void{
      this.emergencyObservationPersist.print(this.emergencyObservationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print emergency_observation", true);
      });
    }

  back():void{
      this.location.back();
    }

}