import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyObservationListComponent } from './emergency-observation-list.component';

describe('EmergencyObservationListComponent', () => {
  let component: EmergencyObservationListComponent;
  let fixture: ComponentFixture<EmergencyObservationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencyObservationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyObservationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
