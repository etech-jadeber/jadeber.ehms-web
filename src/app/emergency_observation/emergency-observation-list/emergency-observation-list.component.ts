import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { EmergencyObservationSummary, EmergencyObservationSummaryPartialList } from '../emergency_observation.model';
import { EmergencyObservationNavigator } from '../emergency_observation.navigator';
import { EmergencyObservationPersist } from '../emergency_observation.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';
import { OrderedOtheServiceNavigator } from 'src/app/ordered_othe_service/ordered_othe_service.navigator';
import { OrderedOtheServiceDetail } from 'src/app/ordered_othe_service/ordered_othe_service.model';
import { Procedure_OrderNavigator } from 'src/app/form_encounters/procedure_orders/procedure_order.navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import {encounterFilterColumn, Form_EncounterDetail} from 'src/app/form_encounters/form_encounter.model';
import {PatientType, ServiceType} from "../../app.enums";
@Component({
  selector: 'app-emergency-observation-list',
  templateUrl: './emergency-observation-list.component.html',
  styleUrls: ['./emergency-observation-list.component.scss']
})export class EmergencyObservationListComponent implements OnInit {
  emergencyObservationsData: EmergencyObservationSummary[] = [];
  emergencyObservationsTotalCount: number = 0;
  emergencyObservationSelectAll:boolean = false;
  emergencyObservationSelection: EmergencyObservationSummary[] = [];
  emergencyObservationFilterColumn = encounterFilterColumn

 emergencyObservationsDisplayedColumns: string[] = ["select", "action", "p.pid", "patient_name",'department','date' ];
  emergencyObservationSearchTextBox: FormControl = new FormControl();
  emergencyObservationIsLoading: boolean = false;
  date: FormControl = new FormControl({disabled: true, value: this.tcUtilsDate.toFormDate(this.emergencyObservationPersist.emergencyObservationSearchHistory.date)})


  @ViewChild(MatPaginator, {static: true}) emergencyObservationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) emergencyObservationsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public form_encounterPersist: Form_EncounterPersist,
                public appointmentPersist: AppointmentPersist,
                public emergencyObservationPersist: EmergencyObservationPersist,
                public emergencyObservationNavigator: EmergencyObservationNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public orderedOtherServiceNavigator: OrderedOtheServiceNavigator,
                public procedureOrderNavigator: Form_EncounterNavigator,
                public otherServiceNaviagator: OtherServicesNavigator,
    ) {

        this.tcAuthorization.requireRead("emergency_observations");
        // emergencyObservationPersist.date.setValue(new Date());
       this.emergencyObservationSearchTextBox.setValue(emergencyObservationPersist.emergencyObservationSearchHistory.search_text);
      //delay subsequent keyup events
      this.emergencyObservationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.emergencyObservationPersist.emergencyObservationSearchHistory.search_text = value;
        this.searchEmergency_observations();
      });
      this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.emergencyObservationPersist.emergencyObservationSearchHistory.date = value ? this.tcUtilsDate.toTimeStamp(value ? value : value._d) : null
        this.searchEmergency_observations()
      })

    } ngOnInit() {

      this.emergencyObservationsSort.sortChange.subscribe(() => {
        this.emergencyObservationsPaginator.pageIndex = 0;
        this.searchEmergency_observations(true);
      });

      this.emergencyObservationsPaginator.page.subscribe(() => {
        this.searchEmergency_observations(true);
      });
      //start by loading items
      this.searchEmergency_observations();
    }

  searchEmergency_observations(isPagination:boolean = false): void {


    let paginator = this.emergencyObservationsPaginator;
    let sorter = this.emergencyObservationsSort;

    this.emergencyObservationIsLoading = true;
    this.emergencyObservationSelection = [];

    this.emergencyObservationPersist.searchEmergencyObservation(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: EmergencyObservationSummaryPartialList) => {
      this.emergencyObservationsData = partialList.data;
      if (partialList.total != -1) {
        this.emergencyObservationsTotalCount = partialList.total;
      }
      this.emergencyObservationIsLoading = false;
    }, error => {
      this.emergencyObservationIsLoading = false;
    });

  }
  back():void{
      this.location.back();
    }

    ngOnDestroy():void{
      // this.emergencyObservationPersist.date.setValue('')
    }

  // ambulanceService(detail: Form_EncounterDetail) {
  //   let dialogRef = this.ambulanceServiceNavigator.addAmbulanceService(detail.id)
  //   dialogRef.afterClosed().subscribe(
  //     (otherService: OrderedOtheServiceDetail) => {
  //     }
  //   )
  // }

    additionalService(detail: EmergencyObservationSummary){
      let dialogRef = this.orderedOtherServiceNavigator.addOrderedOtheService(detail.encounter_id)
      dialogRef.afterClosed().subscribe(
        (otherService: OrderedOtheServiceDetail) => {
          if (otherService){
            this.searchEmergency_observations()
          }
        }
      )
    }

    procedureOrder(detail: EmergencyObservationSummary){
      let dialogRef = this.procedureOrderNavigator.addPatient_Procedure(
        detail.encounter_id
      );
      dialogRef.afterClosed().subscribe((newPatient_Procedure) => {
      });
    }

    // oxygenService(detail: EmergencyObservationSummary){
    //   let dialogRef = this.oxygenServiceNavigator.addOxygenService(detail.encounter_id)
    //   dialogRef.afterClosed().subscribe(
    //     (otherService: OrderedOtheServiceDetail) => {
    //     }
    //   )
    // }
}
