import { TCId } from '../tc/models';

export class EmergencyObservationSummary extends TCId {
  encounter_id: string;
  patient_id: string;
  patient_name:string;
  department: string;
  department_speciality:string;
  sex:boolean;
  dob:number;
  age:number;
  emergency_contact_name:string;
  emergency_contact_phone:string;
  date:number;
  pid:number;
}
export class EmergencyObservationSummaryPartialList {
  data: EmergencyObservationSummary[];
  total: number;
}
export class EmergencyObservationDetail extends EmergencyObservationSummary {}

export class EmergencyObservationDashboard {
  total: number;
}
