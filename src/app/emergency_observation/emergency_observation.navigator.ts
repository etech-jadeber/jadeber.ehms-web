import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

// import {TCModalWidths} from "../tc/utils-angular";
// import {TCIdMode, TCModalModes} from "../tc/models";

// import {EmergencyObservationEditComponent} from "./emergency_observation-edit/emergency_observation-edit.component";
// import {EmergencyObservationPickComponent} from "./emergency_observation-pick/emergency_observation-pick.component";


@Injectable({
  providedIn: 'root'
})

export class EmergencyObservationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  emergency_observationsUrl(): string {
    return "/emergency_observations";
  }

  emergency_observationUrl(id: string): string {
    return "/emergency_observations/" + id;
  }

  viewEmergencyObservations(): void {
    this.router.navigateByUrl(this.emergency_observationsUrl());
  }

  viewEmergencyObservation(id: string): void {
    this.router.navigateByUrl(this.emergency_observationUrl(id));
  }

//   editEmergencyObservation(id: string): MatDialogRef<EmergencyObservationEditComponent> {
//     const dialogRef = this.dialog.open(EmergencyObservationEditComponent, {
//       data: new TCIdMode(id, TCModalModes.EDIT),
//       width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }

//   addEmergencyObservation(): MatDialogRef<EmergencyObservationEditComponent> {
//     const dialogRef = this.dialog.open(EmergencyObservationEditComponent, {
//           data: new TCIdMode(null, TCModalModes.NEW),
//           width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }
  
//    pickEmergencyObservations(selectOne: boolean=false): MatDialogRef<EmergencyObservationPickComponent> {
//       const dialogRef = this.dialog.open(EmergencyObservationPickComponent, {
//         data: selectOne,
//         width: TCModalWidths.large
//       });
//       return dialogRef;
//     }
}