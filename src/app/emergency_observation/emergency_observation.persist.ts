import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {EmergencyObservationDashboard, EmergencyObservationDetail, EmergencyObservationSummaryPartialList} from "./emergency_observation.model";
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';
import { encounter_status } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class EmergencyObservationPersist {
//  emergencyObservationSearchText: string = "";
 selected_index:number;
//  date:FormControl = new FormControl({disabled: true, value: ''});
//  encounterStatusFilter:number =  encounter_status.opened;
//  service_type: number;

 constructor(private http: HttpClient,
      public tcUtilsDate: TCUtilsDate,
  ) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "emergency_observation/" + id + "/do", new TCDoParam("print", {}));
  }  filters(searchHistory = this.emergencyObservationSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }


  emergencyObservationSearchHistory = {
    "date": this.tcUtilsDate.toTimeStamp(new Date()),
    "encounter_status": undefined,
    "service_type": undefined,
    "search_text": "",
    "search_column": "",
}

defaultEmergencyObservationSearchHistory = {...this.emergencyObservationSearchHistory}

resetEmergencyObservationSearchHistory(){
  this.emergencyObservationSearchHistory = {...this.defaultEmergencyObservationSearchHistory}
}
 
  searchEmergencyObservation(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.emergencyObservationSearchHistory): Observable<EmergencyObservationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("emergency_observation", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.get<EmergencyObservationSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "emergency_observation/do", new TCDoParam("download_all", this.filters()));
  }

  emergencyObservationDashboard(): Observable<EmergencyObservationDashboard> {
    return this.http.get<EmergencyObservationDashboard>(environment.tcApiBaseUri + "emergency_observation/dashboard");
  }

  getEmergencyObservation(id: string): Observable<EmergencyObservationDetail> {
    return this.http.get<EmergencyObservationDetail>(environment.tcApiBaseUri + "emergency_observation/" + id);
  }

  addEmergencyObservation(item: EmergencyObservationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "emergency_observation/", item);
  }


 }