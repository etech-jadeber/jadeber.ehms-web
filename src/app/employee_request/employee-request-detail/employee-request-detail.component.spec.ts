import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeRequestDetailComponent } from './employee-request-detail.component';

describe('EmployeeRequestDetailComponent', () => {
  let component: EmployeeRequestDetailComponent;
  let fixture: ComponentFixture<EmployeeRequestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeeRequestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
