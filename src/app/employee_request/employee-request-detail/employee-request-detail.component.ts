import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-employee-request-detail',
  templateUrl: './employee-request-detail.component.html',
  styleUrls: ['./employee-request-detail.component.scss']
})
export class EmployeeRequestDetailComponent implements OnInit {

  id:string
  paramsSubscription: Subscription;
  
  constructor(private route: ActivatedRoute,) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      this.id = this.route.snapshot.paramMap.get('id');
      // this.menuState.changeMenuState(id);
    });

  }

}