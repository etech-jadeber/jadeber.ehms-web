import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { EmployeeRequestSummary, EmployeeRequestSummaryPartialList } from 'src/app/item_request/item_request.model';
import { ItemRequestPersist } from 'src/app/item_request/item_request.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { ItemRequestNavigator } from 'src/app/item_request/item_request.navigator';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';

@Component({
  selector: 'app-employee-request',
  templateUrl: './employee-request.component.html',
  styleUrls: ['./employee-request.component.scss']
})
export class EmployeeRequestComponent implements OnInit {
  employeeRequestsData: EmployeeRequestSummary[] = [];
  employeeRequestsTotalCount: number = 0;

  employeeRequestsDisplayedColumns: string[] = ["name", "store","requested","accepted","rejected","dispatched"];
  employeeRequestSearchTextBox: FormControl = new FormControl();
   @ViewChild(MatPaginator, {static: true}) employeeRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) employeeRequestsSort: MatSort;  
  employeeRequestIsLoading: boolean;
  employeeRequestSelection: any[];
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemRequestPersist: ItemRequestPersist,
                public itemRequestNavigator: ItemRequestNavigator,
                public jobPersist: JobPersist,
                public storeNavigator: StoreNavigator,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("employee_item_requests");
        this.itemRequestPersist.date.setValue(new Date(new Date().toDateString()));
       this.employeeRequestSearchTextBox.setValue(itemRequestPersist.employeeRequestSearchText);
      //delay subsequent keyup events
      this.employeeRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemRequestPersist.employeeRequestSearchText = value;
        this.searchEmployeRequests();
      });


        this.itemRequestPersist.date.valueChanges.subscribe((value)=>{
          this.searchEmployeRequests()
      });

    } ngOnInit() {
   
      this.employeeRequestsSort.sortChange.subscribe(() => {
        this.employeeRequestsPaginator.pageIndex = 0;
        this.searchEmployeRequests(true);
      });

      this.employeeRequestsPaginator.page.subscribe(() => {
        this.searchEmployeRequests(true);
      });
      //start by loading items
      this.searchEmployeRequests();
    }

  searchEmployeRequests(isPagination:boolean = false): void {


    let paginator = this.employeeRequestsPaginator;
    let sorter = this.employeeRequestsSort;

    this.employeeRequestIsLoading = true;
    this.employeeRequestSelection = [];

    this.itemRequestPersist.searchemployeeRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: EmployeeRequestSummaryPartialList) => {
      this.employeeRequestsData = partialList.data;
      
      if (partialList.total != -1) {
        this.employeeRequestsTotalCount = partialList.total;
      }
      this.employeeRequestIsLoading = false;
    }, error => {
      this.employeeRequestIsLoading = false;
    });

  } 

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.itemRequestPersist.store_name = result[0].name;
        this.itemRequestPersist.store_id = result[0].id;
        this.searchEmployeRequests();
      }
    });
  }

 back():void{
      this.location.back();
    }
}