import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EncounterImagesListComponent } from './encounter-images-list.component';

describe('EncounterImagesListComponent', () => {
  let component: EncounterImagesListComponent;
  let fixture: ComponentFixture<EncounterImagesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EncounterImagesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EncounterImagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
