import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, interval } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { Encounter_ImagesDetail, Encounter_ImagesSummary } from '../form_encounters/form_encounter.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-encounter-images-list',
  templateUrl: './encounter-images-list.component.html',
  styleUrls: ['./encounter-images-list.component.css']
})
export class EncounterImagesListComponent implements OnInit {


  encounter_imagessData: Encounter_ImagesSummary[] = [];
  encounter_imagessTotalCount: number = 0;
  encounter_imagessSelectAll: boolean = false;
  encounter_imagessSelected: Encounter_ImagesSummary[] = [];
  encounter_imagessDisplayedColumns: string[] = [
    'select',
    'action',
    'image_id',
    'name'
  ];
  encounter_imagessSearchTextBox: FormControl = new FormControl();
  encounter_imagessLoading: boolean = false;

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
  ) {
    this.encounter_imagessSearchTextBox.setValue(
      form_encounterPersist.encounter_imagesSearchText
    );
    this.encounter_imagessSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.encounter_imagesSearchText = value.trim();
        this.searchEncounter_Imagess();
      });

   }

  ngOnInit(): void {
this.form_encounterPersist.encounterImagePatientId = this.patientId
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchEncounter_Imagess(true);
    });
  // encounter_imagess paginator
  this.paginator.page.subscribe(() => {
      this.searchEncounter_Imagess(true);
    });
    this.searchEncounter_Imagess()
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.form_encounterPersist.encounterImageEncounterId = this.encounterId;
    } else {
    this.form_encounterPersist.encounterImageEncounterId = null;
    }
    this.searchEncounter_Imagess()
    }

  searchEncounter_Imagess(isPagination: boolean = false): void {
    let paginator =
      this.paginator;
    let sorter = this.sorter;
    this.encounter_imagessSelected = [];
    this.encounter_imagessLoading = true;
    this.form_encounterPersist
      .searchEncounter_Images(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.encounter_imagessData = response.data;
          if (response.total != -1) {
            this.encounter_imagessTotalCount = response.total;
          }
          this.encounter_imagessLoading = false;
        },
        (error) => {
          this.encounter_imagessLoading = false;
        }
      );
  }

  addEncounter_Images(): void {
    let dialogRef = this.form_encounterNavigator.addEncounter_Images(
      this.encounterId, this.patientId
    );
    dialogRef.afterClosed().subscribe((newEncounter_Images) => {
      this.searchEncounter_Imagess();
    });
  }

  editEncounter_Images(item: Encounter_ImagesDetail): void {
    let dialogRef = this.form_encounterNavigator.editEncounter_Images(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedEncounter_Images) => {
      if (updatedEncounter_Images) {
        this.searchEncounter_Imagess();
      }
    });
  }

  deleteEncounter_Images(item: Encounter_ImagesDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Encounter_Images');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteEncounter_Images(item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('encounter_images deleted');
              this.searchEncounter_Imagess();
            },
            (error) => {}
          );
      }
    });
  }

  downloadEncounter_Imagess(): void {
    this.tcNotification.info(
      'Download encounter_imagess : ' + this.encounter_imagessSelected.length
    );
  }

  back(): void {
    this.location.back();
  } 

}
