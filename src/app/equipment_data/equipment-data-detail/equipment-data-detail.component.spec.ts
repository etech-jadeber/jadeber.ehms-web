import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentDataDetailComponent } from './equipment-data-detail.component';

describe('EquipmentDataDetailComponent', () => {
  let component: EquipmentDataDetailComponent;
  let fixture: ComponentFixture<EquipmentDataDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentDataDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentDataDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
