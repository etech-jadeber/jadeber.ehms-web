import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { EquipmentDataDetail } from '../equipment_data.model';
import { EquipmentDataPersist } from '../equipment_data.persist';
import { EquipmentDataNavigator } from '../equipment_data.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';

export enum EquipmentDataTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-equipment_data-detail',
  templateUrl: './equipment-data-detail.component.html',
  styleUrls: ['./equipment-data-detail.component.css'],
})
export class EquipmentDataDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  equipmentDataIsLoading: boolean = false;

  EquipmentDataTabs: typeof EquipmentDataTabs = EquipmentDataTabs;
  activeTab: EquipmentDataTabs = EquipmentDataTabs.overview;
  //basics
  equipmentDataDetail: EquipmentDataDetail;
  equipmentOwner: string;
  equipmentUser: string;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public equipmentDataNavigator: EquipmentDataNavigator,
    public equipmentDataPersist: EquipmentDataPersist,
    public doctorPersist: DoctorPersist,
    public itemPersist: ItemPersist,
    public tcUtilsDate: TCUtilsDate,
  ) {
    this.tcAuthorization.requireRead('equipment_datas');
    this.equipmentDataDetail = new EquipmentDataDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('equipment_data');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.equipmentDataIsLoading = true;
    this.equipmentDataPersist.getEquipmentData(id).subscribe(
      (equipmentDataDetail) => {
       
        this.equipmentDataDetail = equipmentDataDetail;

        this.itemPersist.getItem(equipmentDataDetail.equipment_id).subscribe(item => {
          this.equipmentDataDetail.equipment_id = item.name
        })

         this.doctorPersist
           .getDoctor(equipmentDataDetail.equipment_owner)
           .subscribe((doctor) => {
             this.equipmentDataDetail.equipment_owner =
               doctor.first_name + ' ' + doctor.last_name;
           });
           

         this.doctorPersist
           .getDoctor(equipmentDataDetail.contact_person)
           .subscribe((doctor) => {

             this.equipmentDataDetail.contact_person =
               doctor.first_name + ' ' + doctor.last_name;
           });

           
        this.equipmentDataIsLoading = false;
      }, 
      (error) => {
        console.error(error);
        this.equipmentDataIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.equipmentDataDetail.id);
  }
  editEquipmentData(): void {
    let modalRef = this.equipmentDataNavigator.editEquipmentData(
      this.equipmentDataDetail.id
    );
    modalRef.afterClosed().subscribe(
      (equipmentDataDetail) => {
        TCUtilsAngular.assign(this.equipmentDataDetail, equipmentDataDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.equipmentDataPersist
      .print(this.equipmentDataDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print equipment_data', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
