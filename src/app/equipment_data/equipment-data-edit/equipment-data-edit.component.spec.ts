import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentDataEditComponent } from './equipment-data-edit.component';

describe('EquipmentDataEditComponent', () => {
  let component: EquipmentDataEditComponent;
  let fixture: ComponentFixture<EquipmentDataEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentDataEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentDataEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
