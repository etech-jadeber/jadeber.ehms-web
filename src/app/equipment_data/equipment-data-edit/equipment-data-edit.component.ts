import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { EquipmentDataDetail } from '../equipment_data.model';
import { EquipmentDataPersist } from '../equipment_data.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemSummary } from 'src/app/items/item.model';
import { Item_In_StoreDetail } from 'src/app/item_in_stores/item_in_store.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DepartmentPersist } from '../../departments/department.persist';
import { DepartmentNavigator } from 'src/app/departments/department.navigator';
import { DepartmentSummary } from 'src/app/departments/department.model';
import { category, item_type } from 'src/app/app.enums';
@Component({
  selector: 'app-equipment_data-edit',
  templateUrl: './equipment-data-edit.component.html',
  styleUrls: ['./equipment-data-edit.component.css'],
})
export class EquipmentDataEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  equipmentDataDetail: EquipmentDataDetail;
  itemName: string = '';
  item_in_storeDetail: Item_In_StoreDetail;
  power_requirement: string = '';
  ownerName: string = '';
  contactName: string = '';
  outOfService: boolean = false;
  sparepart_available: boolean = false;
  disposal: boolean = false;
  manufactured_year : Date = new Date();
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<EquipmentDataEditComponent>,
    public persist: EquipmentDataPersist,

    public tcUtilsDate: TCUtilsDate,
    public itemNavigator: ItemNavigator,
    public doctorNavigator: DoctorNavigator,
    public departmentPersist: DepartmentPersist,
    public departmentNavigator: DepartmentNavigator,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('equipment_datas');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'equipment_data';
      this.equipmentDataDetail = new EquipmentDataDetail();
           this.equipmentDataDetail.outofservice_reason = '';
           this.equipmentDataDetail.special_disposal_required = false;
           this.equipmentDataDetail.spare_part_place = '';
           this.equipmentDataDetail.spare_part_quantity = 0;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('equipment_datas');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'equipment_data';
      this.isLoadingResults = true;
      this.persist.getEquipmentData(this.idMode.id).subscribe(
        (equipmentDataDetail) => {
          this.equipmentDataDetail = equipmentDataDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.equipmentDataDetail.manufactured_year =
      this.tcUtilsDate.toTimeStamp(new Date(this.manufactured_year));
    console.log('equipment detail is ', this.equipmentDataDetail);
    this.isLoadingResults = true;

    this.persist.addEquipmentData(this.equipmentDataDetail).subscribe(
      (value) => {
        this.tcNotification.success('equipmentData added');
        this.equipmentDataDetail.id = value.id;
        this.dialogRef.close(this.equipmentDataDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateEquipmentData(this.equipmentDataDetail).subscribe(
      (value) => {
        this.tcNotification.success('equipment_data updated');
        this.dialogRef.close(this.equipmentDataDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.equipmentDataDetail == null) {
      return false;
    }

    if (
      this.equipmentDataDetail.equipment_id == null ||
      this.equipmentDataDetail.equipment_id == ''
    ) {
      return false;
    }
    return true;
    if (this.equipmentDataDetail.equipment_type == null) {
      return false;
    }
    if (
      this.equipmentDataDetail.manufacturer == null ||
      this.equipmentDataDetail.manufacturer == ''
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.model == null ||
      this.equipmentDataDetail.model == ''
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.serial == null ||
      this.equipmentDataDetail.serial == ''
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.origin_country == null ||
      this.equipmentDataDetail.origin_country == ''
    ) {
      return false;
    }
    return true;
    if (this.equipmentDataDetail.manufactured_year == null) {
      return false;
    }
    if (this.equipmentDataDetail.power_requirement == null) {
      return false;
    }
    if (this.equipmentDataDetail.current_condition == null) {
      return false;
    }
    // if (
    //   this.equipmentDataDetail.outofservice_reason == null ||
    //   this.equipmentDataDetail.outofservice_reason == ''
    // ) {
    //   return false;
    // }
    // return true;
    // if (this.equipmentDataDetail.special_disposal_required == null) {
    //   return false;
    // }
    if (this.equipmentDataDetail.spare_part_available == null) {
      return false;
    }
    // if (this.equipmentDataDetail.spare_part_quantity == null) {
    //   return false;
    // }
    // if (
    //   this.equipmentDataDetail.spare_part_place == null ||
    //   this.equipmentDataDetail.spare_part_place == ''
    // ) {
    //   return false;
    // }
    // return true;
    if (this.equipmentDataDetail.available_manuals == null) {
      return false;
    }
    if (
      this.equipmentDataDetail.equipment_user == null 
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.equipment_owner == null ||
      this.equipmentDataDetail.equipment_owner == ''
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.contact_person == null ||
      this.equipmentDataDetail.contact_person == ''
    ) {
      return false;
    }
    return true;
    if (
      this.equipmentDataDetail.equipment_location == null ||
      this.equipmentDataDetail.equipment_location == ''
    ) {
      return false;
    }
    return true;
  }

  changeOutOfService(): void {
    // console.log("it is changing")
    if (this.equipmentDataDetail.current_condition == 101) {
      this.outOfService = true;
    } else if (this.equipmentDataDetail.current_condition == 103) {
      this.disposal = true;
      this.outOfService = false;
    } else {
      this.disposal = false;
      this.outOfService = false;
    }
  }

  changeSparepart(): void {
    // console.log("it is changing")
    if (this.equipmentDataDetail.spare_part_available == true) {
      this.sparepart_available = true;
    } else {
      this.sparepart_available = false;
    }
  }

  changeDisposal(): void {
    // console.log("it is changing")
    if (this.equipmentDataDetail.special_disposal_required == true) {
      this.disposal = true;
    } else {
      this.disposal = false;
    }
  }

  searchItem() {
    let dialogRef = this.itemNavigator.pickItems(true,category.supply);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.itemName = result[0].name;
        this.equipmentDataDetail.equipment_id = result[0].id;
      }
    });
  }

  searchOwnerDepartment() {
    let dialogRef = this.departmentNavigator.pickDepartments(true);
    dialogRef.afterClosed().subscribe((result: DepartmentSummary[]) => {
      if (result) {
        this.ownerName = result[0].name
        this.equipmentDataDetail.equipment_owner = result[0].id;
      }
    });
  }

  searchContactDoctor() {
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.contactName = result[0].first_name + result[0].middle_name;
        this.equipmentDataDetail.contact_person = result[0].id;
      }
    });
  }
}
