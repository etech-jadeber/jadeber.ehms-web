import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentDataListComponent } from './equipment-data-list.component';

describe('EquipmentDataListComponent', () => {
  let component: EquipmentDataListComponent;
  let fixture: ComponentFixture<EquipmentDataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentDataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentDataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
