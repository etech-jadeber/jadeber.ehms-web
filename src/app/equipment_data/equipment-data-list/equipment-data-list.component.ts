import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  EquipmentDataSummary,
  EquipmentDataSummaryPartialList,
} from '../equipment_data.model';
import { EquipmentDataPersist } from '../equipment_data.persist';
import { EquipmentDataNavigator } from '../equipment_data.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { DepartmentNavigator } from 'src/app/departments/department.navigator';
import { DepartmentSummaryPartialList } from 'src/app/departments/department.model';
import { ItemPersist } from 'src/app/items/item.persist';
@Component({
  selector: 'app-equipment_data-list',
  templateUrl: './equipment-data-list.component.html',
  styleUrls: ['./equipment-data-list.component.css'],
})
export class EquipmentDataListComponent implements OnInit {
  equipmentDatasData: EquipmentDataSummary[] = [];
  equipmentDatasTotalCount: number = 0;
  equipmentDataSelectAll: boolean = false;
  equipmentDataSelection: EquipmentDataSummary[] = [];
  ownerName: string;

  equipmentDatasDisplayedColumns: string[] = [
    'select',
    'action',
    'link',
    'power_requirement',
    'current_condition',
    'spare_part_available',
    'available_manuals',
    'equipment_user',
    'equipment_owner',
    'contact_person',
    'equipment_location',
  ];
  equipmentDataSearchTextBox: FormControl = new FormControl();
  equipmentDataIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  equipmentDatasPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) equipmentDatasSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public equipmentDataPersist: EquipmentDataPersist,
    public equipmentDataNavigator: EquipmentDataNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public doctorPersist: DoctorPersist,
    public departmentPersist: DepartmentPersist,
    public departmentNavigator: DepartmentNavigator,
    public itemPersist: ItemPersist,
  ) {
    this.tcAuthorization.requireRead('equipment_datas');
    this.equipmentDataSearchTextBox.setValue(
      equipmentDataPersist.equipmentDataSearchText
    );
    //delay subsequent keyup events
    this.equipmentDataSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.equipmentDataPersist.equipmentDataSearchText = value;
        this.searchEquipmentDatas();
      });
  }
  ngOnInit() {
    this.equipmentDatasSort.sortChange.subscribe(() => {
      this.equipmentDatasPaginator.pageIndex = 0;
      this.searchEquipmentDatas(true);
    });

    this.equipmentDatasPaginator.page.subscribe(() => {
      this.searchEquipmentDatas(true);
    });
    //start by loading items
    this.searchEquipmentDatas();
  }

  searchEquipmentDatas(isPagination: boolean = false): void {
    let paginator = this.equipmentDatasPaginator;
    let sorter = this.equipmentDatasSort;

    this.equipmentDataIsLoading = true;
    this.equipmentDataSelection = [];

    this.equipmentDataPersist
      .searchEquipmentData(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: EquipmentDataSummaryPartialList) => {
          // to get the equipment owner 
          partialList.data.forEach((element,idx) => {

            this.departmentPersist
              .searchDepartment(
                paginator.pageSize,
                isPagination ? paginator.pageIndex : 0,
                sorter.active,
                sorter.direction
              )
              .subscribe((departments: DepartmentSummaryPartialList) => {
                departments.data.forEach((department) => {
                  if (department.id === element.equipment_owner) {
                    this.equipmentDatasData[idx].equipment_owner =
                      department.name;
                  }
                });
              });
          });
          // to get the contact person

                 partialList.data.forEach((element, idx) => {
                   this.doctorPersist
                     .getDoctor(element.contact_person)
                     .subscribe((doctor) => {
                       this.equipmentDatasData[idx].contact_person =
                         doctor.first_name + ' ' + doctor.last_name;
                     });
                 });
          // to get the item name
     partialList.data.forEach((element, idx) => {
       this.itemPersist
         .getItem(element.equipment_id)
         .subscribe((item) => {
           this.equipmentDatasData[idx].equipment_id =item.name;
         });
     });          this.equipmentDatasData = partialList.data;
          if (partialList.total != -1) {
            this.equipmentDatasTotalCount = partialList.total;
          }
          this.equipmentDataIsLoading = false;
        },
        (error) => {
          this.equipmentDataIsLoading = false;
        }
      );
  }
  downloadEquipmentDatas(): void {
    if (this.equipmentDataSelectAll) {
      this.equipmentDataPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download equipment_data',
          true
        );
      });
    } else {
      this.equipmentDataPersist
        .download(this.tcUtilsArray.idsList(this.equipmentDataSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download equipment_data',
            true
          );
        });
    }
  }
  addEquipmentData(): void {
    let dialogRef = this.equipmentDataNavigator.addEquipmentData();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchEquipmentDatas();
      }
    });
  }

  editEquipmentData(item: EquipmentDataSummary) {
    let dialogRef = this.equipmentDataNavigator.editEquipmentData(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteEquipmentData(item: EquipmentDataSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('equipment_data');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.equipmentDataPersist.deleteEquipmentData(item.id).subscribe(
          (response) => {
            this.tcNotification.success('equipment_data deleted');
            this.searchEquipmentDatas();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
