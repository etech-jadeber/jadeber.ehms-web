import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentDataPickComponent } from './equipment-data-pick.component';

describe('EquipmentDataPickComponent', () => {
  let component: EquipmentDataPickComponent;
  let fixture: ComponentFixture<EquipmentDataPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentDataPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentDataPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
