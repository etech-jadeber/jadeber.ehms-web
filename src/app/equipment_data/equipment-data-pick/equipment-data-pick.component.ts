import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  EquipmentDataSummary,
  EquipmentDataSummaryPartialList,
} from '../equipment_data.model';
import { EquipmentDataPersist } from '../equipment_data.persist';
import { EquipmentDataNavigator } from '../equipment_data.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-equipment_data-pick',
  templateUrl: './equipment-data-pick.component.html',
  styleUrls: ['./equipment-data-pick.component.css'],
})
export class EquipmentDataPickComponent implements OnInit {
  equipmentDatasData: EquipmentDataSummary[] = [];
  equipmentDatasTotalCount: number = 0;
  equipmentDataSelectAll: boolean = false;
  equipmentDataSelection: EquipmentDataSummary[] = [];

  equipmentDatasDisplayedColumns: string[] = [
    'select',
    ,
    'equipment_id',
    'equipment_type',
    'manufacturer',
    'model',
    'serial',
    'origin_country',
    'manufactured_year',
    'power_requirement',
    'current_condition',
    'outofservice_reason',
    'special_disposal_required',
    'spare_part_available',
    'spare_part_quantity',
    'spare_part_place',
    'available_manuals',
    'equipment_user',
    'equipment_owner',
    'contact_person',
    'equipment_location',
  ];
  equipmentDataSearchTextBox: FormControl = new FormControl();
  equipmentDataIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  equipmentDatasPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) equipmentDatasSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public equipmentDataPersist: EquipmentDataPersist,
    public equipmentDataNavigator: EquipmentDataNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<EquipmentDataPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('equipment_datas');
    this.equipmentDataSearchTextBox.setValue(
      equipmentDataPersist.equipmentDataSearchText
    );
    //delay subsequent keyup events
    this.equipmentDataSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.equipmentDataPersist.equipmentDataSearchText = value;
        this.searchEquipment_datas();
      });
  }
  ngOnInit() {
    this.equipmentDatasSort.sortChange.subscribe(() => {
      this.equipmentDatasPaginator.pageIndex = 0;
      this.searchEquipment_datas(true);
    });

    this.equipmentDatasPaginator.page.subscribe(() => {
      this.searchEquipment_datas(true);
    });
    //start by loading items
    this.searchEquipment_datas();
  }

  searchEquipment_datas(isPagination: boolean = false): void {
    let paginator = this.equipmentDatasPaginator;
    let sorter = this.equipmentDatasSort;

    this.equipmentDataIsLoading = true;
    this.equipmentDataSelection = [];

    this.equipmentDataPersist
      .searchEquipmentData(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: EquipmentDataSummaryPartialList) => {
          this.equipmentDatasData = partialList.data;
          if (partialList.total != -1) {
            this.equipmentDatasTotalCount = partialList.total;
          }
          this.equipmentDataIsLoading = false;
        },
        (error) => {
          this.equipmentDataIsLoading = false;
        }
      );
  }
  markOneItem(item: EquipmentDataSummary) {
    if (!this.tcUtilsArray.containsId(this.equipmentDataSelection, item.id)) {
      this.equipmentDataSelection = [];
      this.equipmentDataSelection.push(item);
    } else {
      this.equipmentDataSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.equipmentDataSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
