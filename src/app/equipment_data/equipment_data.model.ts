import { TCId } from '../tc/models';

export class EquipmentDataSummary extends TCId {
    
  equipment_id: string;
  equipment_type: number;
  manufacturer: string;
  model: string;
  serial: string;
  origin_country: string;
  manufactured_year: number;
  power_requirement: number;
  current_condition: number;
  outofservice_reason: string;
  special_disposal_required: boolean;
  spare_part_available: boolean;
  spare_part_quantity: number;
  spare_part_place: string;
  available_manuals: number;
  equipment_user: number;
  equipment_owner: string;
  contact_person: string;
  equipment_location: string;
}
export class EquipmentDataSummaryPartialList {
  data: EquipmentDataSummary[];
  total: number;
}
export class EquipmentDataDetail extends EquipmentDataSummary {}

export class EquipmentDataDashboard {
  total: number;
}
