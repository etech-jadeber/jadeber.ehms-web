import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA , MatDialog} from '@angular/material/dialog';
import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { EquipmentDataEditComponent } from './equipment-data-edit/equipment-data-edit.component';
import { EquipmentDataPickComponent } from './equipment-data-pick/equipment-data-pick.component';

@Injectable({
  providedIn: 'root',
})
export class EquipmentDataNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  equipmentDatasUrl(): string {
    return '/equipment_datas';
  }

  equipmentDataUrl(id: string): string {
    return '/equipment_datas/' + id;
  }

  viewEquipmentDatas(): void {
    this.router.navigateByUrl(this.equipmentDatasUrl());
  }

  viewEquipmentData(id: string): void {
    this.router.navigateByUrl(this.equipmentDataUrl(id));
  }

  editEquipmentData(id: string): MatDialogRef<EquipmentDataEditComponent> {
    const dialogRef = this.dialog.open(EquipmentDataEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addEquipmentData(): MatDialogRef<EquipmentDataEditComponent> {
    const dialogRef = this.dialog.open(EquipmentDataEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickEquipmentDatas(
    selectOne: boolean = false
  ): MatDialogRef<EquipmentDataPickComponent> {
    const dialogRef = this.dialog.open(EquipmentDataPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
