import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  EquipmentDataDashboard,
  EquipmentDataDetail,
  EquipmentDataSummaryPartialList,
} from './equipment_data.model';
import { available_manuals, current_condition, equipment_type, equipment_user, power_requirement } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class EquipmentDataPersist {
  equipmentDataSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.equipmentDataSearchText;
    //add custom filters
    return fltrs;
  }

  equipment_type: TCEnum[] = [
    new TCEnum(equipment_type.consumables, 'consumables'),
    new TCEnum(equipment_type.medical_equipment, 'medical_equipment'),
    new TCEnum(equipment_type.spare_part, 'spare_part'),
  ];

  current_condition: TCEnum[] = [
    new TCEnum(
      current_condition.operable_and_in_service,
      'operable_and_in_service'
    ),
    new TCEnum(
      current_condition.operable_and_outof_service,
      'operable_and_outof_service'
    ),
    new TCEnum(
      current_condition.requires_maintainance,
      'requires_maintainance'
    ),
    new TCEnum(current_condition.not_repairable, 'not_repairable'),
  ];

  available_manuals: TCEnum[] = [
    new TCEnum(available_manuals.user_manual, 'user_manual'),
    new TCEnum(available_manuals.service_manual, 'service_manual'),
    new TCEnum(available_manuals.other, 'other'),
  ];

  equipment_user: TCEnum[] = [
    new TCEnum(equipment_user.doctor, 'doctor'),
    new TCEnum(equipment_user.nurse, 'nurse'),
    new TCEnum(equipment_user.lab, 'lab'),
    new TCEnum(equipment_user.rad, 'rad'),
    new TCEnum(equipment_user.resident, 'resident'),
  ];

  power_requirement: TCEnum[] = [
    new TCEnum(power_requirement.small, '110v'),
    new TCEnum(power_requirement.large, '220v'),
    new TCEnum(power_requirement.neutral, '0v'),
  ];

  searchEquipmentData(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<EquipmentDataSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'equipment_data',
      this.equipmentDataSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<EquipmentDataSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'equipment_data/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  equipmentDataDashboard(): Observable<EquipmentDataDashboard> {
    return this.http.get<EquipmentDataDashboard>(
      environment.tcApiBaseUri + 'equipment_data/dashboard'
    );
  }

  getEquipmentData(id: string): Observable<EquipmentDataDetail> {
    return this.http.get<EquipmentDataDetail>(
      environment.tcApiBaseUri + 'equipment_data/' + id
    );
  }
  addEquipmentData(item: EquipmentDataDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'equipment_data/',
      item
    );
  }

  updateEquipmentData(
    item: EquipmentDataDetail
  ): Observable<EquipmentDataDetail> {
    return this.http.patch<EquipmentDataDetail>(
      environment.tcApiBaseUri + 'equipment_data/' + item.id,
      item
    );
  }

  deleteEquipmentData(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'equipment_data/' + id);
  }
  equipmentDatasDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'equipment_data/do',
      new TCDoParam(method, payload)
    );
  }

  equipmentDataDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'equipment_datas/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'equipment_data/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'equipment_data/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
