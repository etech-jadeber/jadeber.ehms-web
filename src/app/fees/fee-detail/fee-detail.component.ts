import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {FeeDetail} from "../fee.model";
import {FeePersist} from "../fee.persist";
import {FeeNavigator} from "../fee.navigator";

export enum FeeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-fee-detail',
  templateUrl: './fee-detail.component.html',
  styleUrls: ['./fee-detail.component.css']
})
export class FeeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  feeLoading:boolean = false;
  
  FeeTabs: typeof FeeTabs = FeeTabs;
  activeTab: FeeTabs = FeeTabs.overview;
  //basics
  feeDetail: FeeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public feeNavigator: FeeNavigator,
              public  feePersist: FeePersist) {
    this.tcAuthorization.requireRead("fees");
    this.feeDetail = new FeeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("fees");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.feeLoading = true;
    this.feePersist.getFee(id).subscribe(feeDetail => {
          this.feeDetail = feeDetail;
          this.feeLoading = false;
        }, error => {
          console.error(error);
          this.feeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.feeDetail.id);
  }

  editFee(): void {
    let modalRef = this.feeNavigator.editFee(this.feeDetail.id);
    modalRef.afterClosed().subscribe(modifiedFeeDetail => {
      TCUtilsAngular.assign(this.feeDetail, modifiedFeeDetail);
    }, error => {
      console.error(error);
    });
  }

   printFee():void{
      this.feePersist.print(this.feeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print fee", true);
      });
    }

  back():void{
      this.location.back();
    }

}
