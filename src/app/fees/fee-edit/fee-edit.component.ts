import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCEnum, TCIdMode, TCModalModes } from '../../tc/models';

import { FeeDetail } from '../fee.model';
import { FeePersist } from '../fee.persist';
import { ItemSummary } from 'src/app/items/item.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { Fee_TypeNavigator } from 'src/app/feetypes/fee_type.navigator';
import { Fee_TypeSummary } from 'src/app/feetypes/fee_type.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { FeeFrequency, fee_type, lab_order_type, physican_type, TimeOption, vat_status } from 'src/app/app.enums';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { ProcedureSummary } from 'src/app/procedures/procedure.model';

import { Physiotherapy_TypeNavigator } from 'src/app/physiotherapy_types/physiotherapy_type.navigator';
import { Physiotherapy_TypeSummary } from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_PanelNavigator } from 'src/app/lab_panels/lab_panel.navigator';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_PanelSummary } from 'src/app/lab_panels/lab_panel.model';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { BedroomNavigator } from 'src/app/bed/bedrooms/bedroom.navigator';
import { BedroomSummary } from 'src/app/bed/bedrooms/bedroom.model';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { OtherServicesSummary } from 'src/app/other_services/other-services.model';
import { Department_SpecialtyNavigator } from 'src/app/department_specialtys/department_specialty.navigator';
import { Department_SpecialtyDetail, Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';

@Component({
  selector: 'app-fee-edit',
  templateUrl: './fee-edit.component.html',
  styleUrls: ['./fee-edit.component.css'],
})
export class FeeEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  feeDetail: FeeDetail;
  feeName: string;
  feeTypeName: string;
  parent_department_id:string = "";
  parent_department:string = "";
  department: string = "";
  userFullName:string = "";
  feeType = fee_type;
  timeOption: number = TimeOption.Default;
  fromTime: number;
  TimeOption = TimeOption

  timeMultiplication = {
    [TimeOption.Day] : 24 * 60,
    [TimeOption.Hr] : 60,
    [TimeOption.Min] : 1
  }

  feeFerquencyFilter = {
    [fee_type.ambulance_in_km]: this.persist.FeeFrequncy_Distance,
    [fee_type.ambulance_in_waiting]:this.persist.FeeFrequncy,
    [fee_type.oxygen_service]: this.persist.FeeFrequncy
  }

  feeTimeFilter = {
    [fee_type.ambulance_in_km]:this.persist.timeOption_distance,
    [fee_type.ambulance_in_waiting]:this.persist.timeOption,
    [fee_type.oxygen_service]:this.persist.timeOption
  }

  feePriceFilter = {
    [fee_type.operation_room_fee]:this.persist.priceOption,
  }

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public doctorNavigator:DoctorNavigator,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<FeeEditComponent>,
    public persist: FeePersist,
    public itemNavigator: ItemNavigator,
    public feeTypeNavigator: Fee_TypeNavigator,
    public otherServiceNav:OtherServicesNavigator,
    public procedureNavigator: ProcedureNavigator,
    public bedroomNavigator: BedroomNavigator,
    public physiotherapyNavigator: Physiotherapy_TypeNavigator,
    public labtestNavigator: Lab_TestNavigator,
    public labpanelNavigator: Lab_PanelNavigator,
    public tcUtilsArray: TCUtilsArray,
    public departmentSpecialityNavigator: Department_SpecialtyNavigator,
    public wardNavigator: BedroomtypeNavigator,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('fees');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        this.appTranslation.getText('financial', 'payment');
      this.feeDetail = new FeeDetail();
      this.feeDetail.fee_frequency = FeeFrequency.once;

      this.feeDetail.target_id = this.tcUtilsString.invalid_id;
      //set necessary defaults
      this.feeDetail.vat_status = vat_status.no_vat;
      this.feeDetail.fee_frequency = FeeFrequency.once;


    } else {
      this.tcAuthorization.requireUpdate('fees');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        this.appTranslation.getText('financial', 'payment');
      this.isLoadingResults = true;
      this.persist.getFee(this.idMode.id).subscribe(
        (feeDetail) => {
          this.feeDetail = feeDetail;
          this.convertDate()
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.feeDetail.from_time = this.fromTime * (this.timeMultiplication[this.timeOption] || 1)
    this.persist.addFee(this.feeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Fee added');
        this.feeDetail.id = value.id;
        this.dialogRef.close(this.feeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.feeDetail.from_time = this.fromTime * (this.timeMultiplication[this.timeOption] || 1)
    this.persist.updateFee(this.feeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Fee updated');
        this.dialogRef.close(this.feeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.feeDetail == null) {
      return false;
    }

    if (this.feeDetail.name == null || this.feeDetail.name == '' ) {
      return false;
    }
    if (this.feeDetail.price == null || this.feeDetail.price < 0 || isNaN(this.feeDetail.price)) {
      return false;
    }
    if (this.feeDetail.vat_status == -1){
      return false;
    }
    if(this.feeDetail.fee_type == -1 || this.feeDetail.fee_type == null){
      return false;
    }
    if(this.feeDetail.fee_type == fee_type.ambulance_in_km && this.feeDetail.fee_type == fee_type.ambulance_in_waiting && this.feeDetail.fee_type == fee_type.oxygen_service){
      if (!this.feeDetail.fee_frequency){
        return false;
      }

      if (this.timeOption !== TimeOption.Default && !this.fromTime  ){
        return false;
      }

    }

    return true;
  }
  searchItem() {
    let dialogRef = this.itemNavigator.pickItems(true);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  convertDate() {
    if (this.feeDetail.from_time == 0){
      this.timeOption = TimeOption.Default
    }
    else {
      this.fromTime = this.feeDetail.from_time
      if(this.feeTimeFilter[this.feeDetail.fee_type]){
        this.timeOption = TimeOption.Min
      }
      else if(this.feePriceFilter[this.feeDetail.fee_type]){
        this.timeOption = TimeOption.price
      }
    }
  }

  searchDoctor() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        const {first_name, middle_name, last_name} = result[0]
        this.Name = `${first_name} ${middle_name} ${last_name}` ;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchWards() {
    let dialogRef = this.wardNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeDetail[]) => {
      if (result) {
        this.Name = result[0].name ;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchOtherService(){
    let dialogRef = this.otherServiceNav.pickOtherServicess(null,true);
    dialogRef.afterClosed().subscribe((result: OtherServicesSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  Name:string;
  searchProcedure() {
    let dialogRef = this.procedureNavigator.pickProcedures(true);
    dialogRef.afterClosed().subscribe((result: ProcedureSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchBedrooms() {
    let dialogRef = this.bedroomNavigator.pickBedrooms(true);
    dialogRef.afterClosed().subscribe((result: BedroomSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchPhysiotherapy() {
    let dialogRef = this.physiotherapyNavigator.pickPhysiotherapy_Types(true);
    dialogRef.afterClosed().subscribe((result: Physiotherapy_TypeSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchLabTest(lab_order_type: number) {
    let dialogRef = this.labtestNavigator.pickLab_Tests(true, null, lab_order_type);
    dialogRef.afterClosed().subscribe((result: Lab_TestSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchLabPanel(lab_order_type: number) {
    let dialogRef = this.labpanelNavigator.pickLab_Panels(true, lab_order_type);
    dialogRef.afterClosed().subscribe((result: Lab_PanelSummary[]) => {
      if (result) {
        this.Name = result[0].name;
        this.feeDetail.target_id = result[0].id;
      }
    });
  }

  searchFeeType() {
    let dialogRef = this.feeTypeNavigator.pickFee_Types(true);
    dialogRef.afterClosed().subscribe((result: Fee_TypeSummary[]) => {
      if (result) {
        this.feeTypeName = result[0].name;

      }
    });
  }

  searchDepartmentSpeciality(parent_id: string) {
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, parent_id);
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        if(parent_id == this.tcUtilsString.invalid_id){
        this.parent_department_id = result[0].id;
        this.parent_department = result[0].name
        this.feeDetail.target_id = result[0].id;
        this.department = null
        } else {
        this.feeDetail.target_id = result[0].id;
        this.department = result[0].name
      }
    }
    });
  }

}
