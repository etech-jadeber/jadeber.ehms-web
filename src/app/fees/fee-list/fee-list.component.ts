import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {FeePersist} from "../fee.persist";
import {FeeNavigator} from "../fee.navigator";
import {FeeDetail, feeFilterColumn, FeeSummary, FeeSummaryPartialList} from "../fee.model";
import { Fee_TypePersist } from 'src/app/feetypes/fee_type.persist';
import { Fee_TypeSummary } from 'src/app/feetypes/fee_type.model';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemSummary } from 'src/app/items/item.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TCUtilsNumber } from 'src/app/tc/utils-number';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';


@Component({
  selector: 'app-fee-list',
  templateUrl: './fee-list.component.html',
  styleUrls: ['./fee-list.component.css']
})
export class FeeListComponent implements OnInit {

  feesData: FeeSummary[] = [];
  feesTotalCount: number = 0;
  feesSelectAll:boolean = false;
  feesSelection: FeeSummary[] = [];
  feeTypes: Fee_TypeSummary[] = [];
  items:ItemSummary[]=[];
  feeFilterColumn = feeFilterColumn

  feesDisplayedColumns: string[] = ["select","action","name","price","season","fee_type", "service_type", "item_code"];
  feesSearchTextBox: FormControl = new FormControl();
  feesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) feesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) feesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsString: TCUtilsString,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public feePersist: FeePersist,
                public feeTypePersist: Fee_TypePersist,
                public itemPersist: ItemPersist,
                public feeNavigator: FeeNavigator,
                public jobPersist: JobPersist,
                public doctorPersist: DoctorPersist,
                public tcUtilsNumber: TCUtilsNumber,
                public paymentPersist: PaymentPersist,
                public formEncounterPersist: Form_EncounterPersist
    ) {

        this.tcAuthorization.requireRead("fees");
       this.feesSearchTextBox.setValue(feePersist.feeSearchHistory.search_text);
      //delay subsequent keyup events
      this.feesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.feePersist.feeSearchHistory.search_text = value;
        this.searchFees();
      });
    }

    ngOnInit() {

      this.feesSort.sortChange.subscribe(() => {
        this.feesPaginator.pageIndex = 0;
        this.searchFees(true);
      });

      this.feesPaginator.page.subscribe(() => {
        this.searchFees(true);
      });
      //start by loading items
      this.searchFees();
    }

  searchFees(isPagination:boolean = false): void {


    let paginator = this.feesPaginator;
    let sorter = this.feesSort;

    this.feesIsLoading = true;
    this.feesSelection = [];

    this.feePersist.searchFee(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FeeSummaryPartialList) => {
      this.feesData = partialList.data;
      // this.feesData.forEach(fee => this.getDoctorName(fee))
      if (partialList.total != -1) {
        this.feesTotalCount = partialList.total;
      }

     
      // for(let item of partialList.data){
      //   this.itemPersist.getItem(item.target_id).subscribe(
      //     res=>{
      //       this.items.push(res);
      //     }
      //   );
      // }

      this.feesIsLoading = false;
    }, error => {
      this.feesIsLoading = false;
    });

  }

  downloadFees(): void {
    if(this.feesSelectAll){
         this.feePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download fees", true);
      });
    }
    else{
        this.feePersist.download(this.tcUtilsArray.idsList(this.feesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download fees",true);
            });
        }
  }



  addFee(): void {
    let dialogRef = this.feeNavigator.addFee();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFees();
      }
    });
  }

  editFee(item: FeeSummary) {
    let dialogRef = this.feeNavigator.editFee(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteFee(item: FeeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Fee");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.feePersist.deleteFee(item.id).subscribe(response => {
          this.tcNotification.success("Fee deleted");
          this.searchFees();
        }, error => {
        });
      }

    });
  }
doctorData: DoctorSummary[] = [];

doctorDatas:[] = []
getDoctorName(value:FeeSummary){
  value.target_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(value.target_id).subscribe(doctor => {
    value.doctor_name = doctor.first_name + ' ' + doctor.middle_name + ' ' + doctor.last_name
  })
}  

  back():void{
      this.location.back();
    }

}
