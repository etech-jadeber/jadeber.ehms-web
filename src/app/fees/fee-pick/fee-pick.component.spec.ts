import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeePickComponent } from './fee-pick.component';

describe('FeePickComponent', () => {
  let component: FeePickComponent;
  let fixture: ComponentFixture<FeePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
