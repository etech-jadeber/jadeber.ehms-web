import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {FeeDetail, FeeSummary, FeeSummaryPartialList} from "../fee.model";
import {FeePersist} from "../fee.persist";
import { TCUtilsNumber } from 'src/app/tc/utils-number';
import { PaymentPersist } from 'src/app/payments/payment.persist';


@Component({
  selector: 'app-fee-pick',
  templateUrl: './fee-pick.component.html',
  styleUrls: ['./fee-pick.component.css']
})
export class FeePickComponent implements OnInit {

  feesData: FeeSummary[] = [];
  feesTotalCount: number = 0;
  feesSelection: FeeSummary[] = [];
  feesDisplayedColumns: string[] = ["select","name","price","doctor_id","season","fee_type"];

  feesSearchTextBox: FormControl = new FormControl();
  feesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) feesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) feesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public feePersist: FeePersist,
              public tcUtilsNumber: TCUtilsNumber,
              public dialogRef: MatDialogRef<FeePickComponent>,
              public paymentPersist: PaymentPersist,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, fee_type: number}
  ) {
    this.tcAuthorization.requireRead("fees");
    this.feesSearchTextBox.setValue(feePersist.feeSearchHistory.search_text);
    //delay subsequent keyup events
    this.feesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.feePersist.feeSearchHistory.search_text = value;
      this.searchFees();
    });
    this.feePersist.feeSearchHistory.fee_type = data.fee_type
  }

  ngOnInit() {

    this.feesSort.sortChange.subscribe(() => {
      this.feesPaginator.pageIndex = 0;
      this.searchFees();
    });

    this.feesPaginator.page.subscribe(() => {
      this.searchFees();
    });

    //set initial picker list to 5
    this.feesPaginator.pageSize = 5;

    //start by loading items
    this.searchFees();
  }

  searchFees(): void {
    this.feesIsLoading = true;
    this.feesSelection = [];

    this.feePersist.searchFee(this.feesPaginator.pageSize,
        this.feesPaginator.pageIndex,
        this.feesSort.active,
        this.feesSort.direction).subscribe((partialList: FeeSummaryPartialList) => {
      this.feesData = partialList.data;
      if (partialList.total != -1) {
        this.feesTotalCount = partialList.total;
      }
      this.feesIsLoading = false;
    }, error => {
      this.feesIsLoading = false;
    });

  }

  markOneItem(item: FeeSummary) {
    if(!this.tcUtilsArray.containsId(this.feesSelection,item.id)){
          this.feesSelection = [];
          this.feesSelection.push(item);
        }
        else{
          this.feesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.feesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
