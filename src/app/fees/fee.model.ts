import {TCId, TCString} from "../tc/models";

export class FeeSummary extends TCId {
  target_id : string;
  name : string;
  price : number;
  season : number;
  fee_type : number;
  vat_status: number;
  doctor_name: string;
  service_type: number;
  from_time: number;
  fee_frequency: number;
  target_type: number;
  item_code: string
}

export class FeeByServiceType {
  inpatient: number;
  outpatient: number;
  emergency: number;
  all: number;
}

export const feeFilterColumn: TCString[] = [
  new TCString( "name", 'Name'),
  new TCString( "item_code", 'Item Code'),
  ];

export class FeeSummaryPartialList {
  data: FeeSummary[];
  total: number;
}

export class FeeDetail extends FeeSummary {
  target_id : string;
  name : string;
  price : number;
  season : number;
  fee_type : number;
  vat_status: number;
}

export class FeeDashboard {
  total: number;
}