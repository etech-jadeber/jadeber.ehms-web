import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {FeeEditComponent} from "./fee-edit/fee-edit.component";
import {FeePickComponent} from "./fee-pick/fee-pick.component";


@Injectable({
  providedIn: 'root'
})

export class FeeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  feesUrl(): string {
    return "/fees";
  }

  feeUrl(id: string): string {
    return "/fees/" + id;
  }

  viewFees(): void {
    this.router.navigateByUrl(this.feesUrl());
  }

  viewFee(id: string): void {
    this.router.navigateByUrl(this.feeUrl(id));
  }

  editFee(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(FeeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addFee(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(FeeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickFees(selectOne: boolean=false, fee_type: number = null): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(FeePickComponent, {
        data: {selectOne, fee_type},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
