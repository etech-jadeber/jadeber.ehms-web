import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation, TCEnum} from "../tc/models";
import {FeeDashboard, FeeDetail, FeeSummaryPartialList} from "./fee.model";
import {
  FeeFrequency,
  FeePaymentType,
  fee_type,
  lab_order_type,
  operation_room_type,
  oxygen_type,
  payment_season,
  TimeOption,
  vat_status
} from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class FeePersist {

  // feeSearchText: string = "";
  // vatStatusFilter: number ;
  // targetId: string = "";

    VatStatus: TCEnum[] = [
     new TCEnum( vat_status.no_vat, 'no_vat'),
      new TCEnum( vat_status.vat, 'vat'),
  ];

  OxygenType: TCEnum[] = [
    new TCEnum( oxygen_type.all, 'All'),
  new TCEnum( oxygen_type.less_than_3, '< 3'),
  new TCEnum( oxygen_type.great_than_3, '> 3')
  ]

  OperationRoomType: TCEnum[] = [
    new TCEnum( operation_room_type.all, 'All'),
  new TCEnum( operation_room_type.minor, 'Minor'),
  new TCEnum( operation_room_type.major, 'Major'),
  ]



    FeePaymentType: TCEnum[] = [
     new TCEnum( FeePaymentType.once, 'once'),
  new TCEnum( FeePaymentType.recurring, 'recurring'),

  ];

  fee_type_with_order_type = {
    [fee_type.lab_panel]: lab_order_type.lab,
    [fee_type.lab_test]: lab_order_type.lab,
    [fee_type.rad_panel]: lab_order_type.imaging,
    [fee_type.rad_test]: lab_order_type.imaging,
    [fee_type.pat_test]: lab_order_type.pathology,
    [fee_type.pat_panel]: lab_order_type.pathology
  }

  constructor(private http: HttpClient, private appTranslation:AppTranslation) {
  }

  feeSearchHistory = {
    "target_id": "",
    "fee_type": undefined,
    "service_type": undefined,
    "target_type": undefined,
    "season": undefined,
    "search_text": "",
    "search_column": "",
    "price_column": "price",
  }

  defaultFeeSearchHistory = {...this.feeSearchHistory}

  resetFeeSearchHistory(){
  this.feeSearchHistory = {...this.defaultFeeSearchHistory}
  }

  priceColumnSearchHistory = {
    "search_text": "",
    "search_column": "",
  }

  defaultPriceColumnSearchHistory = {...this.priceColumnSearchHistory}

  resetPriceColumnSearchHistory(){
    this.priceColumnSearchHistory = {...this.defaultPriceColumnSearchHistory}
  }

  searchFee(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.feeSearchHistory): Observable<FeeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("fees", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.targetId){
    //   url = TCUtilsString.appendUrlParameter(url, "target_id", this.targetId);
    // }
    // if (this.fee_typeId){
    //   url = TCUtilsString.appendUrlParameter(url, "fee_type", this.fee_typeId.toString());
    // }

    return this.http.get<FeeSummaryPartialList>(url);

  }

  filters(searchHistory = this.feeSearchHistory): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "fees/do", new TCDoParam("download_all", this.filters()));
  }

  feeDashboard(): Observable<FeeDashboard> {
    return this.http.get<FeeDashboard>(environment.tcApiBaseUri + "fees/dashboard");
  }

  getFee(id: string): Observable<FeeDetail> {
    return this.http.get<FeeDetail>(environment.tcApiBaseUri + "fees/" + id);
  }

  addFee(item: FeeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "fees/", item);
  }

  updateFee(item: FeeDetail): Observable<FeeDetail> {
    return this.http.patch<FeeDetail>(environment.tcApiBaseUri + "fees/" + item.id, item);
  }

  deleteFee(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "fees/" + id);
  }

  feesDo(method: string, payload: any,pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory: any = {}): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("fees/do", (searchHistory?.search_text || ''), pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  feesUpdate (fee_update_type: number, price_column: string) {
    return environment.tcApiBaseUri + "fees_update/" + (fee_update_type.toString() + "/") + price_column
  }

  feeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "fees/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "fees/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "fees/" + id + "/do", new TCDoParam("print", {}));
  }
  payment_seasonId: number ;

  payment_season: TCEnumTranslation[] = [
  new TCEnumTranslation(payment_season.holiday, this.appTranslation.getKey('financial', 'holiday')),
  new TCEnumTranslation(payment_season.weekend, this.appTranslation.getKey('financial', 'weekend')),
  new TCEnumTranslation(payment_season.night_shift, this.appTranslation.getKey('financial', 'night_shift')),
  new TCEnumTranslation(payment_season.normal, this.appTranslation.getKey('patient', 'normal')),
];
// fee_typeId:  number ;

  // fee_type: TCEnumTranslation[] = [
  //   new TCEnumTranslation(fee_type.bed, this.appTranslation.getKey('bed', 'bed')),
  //   new TCEnumTranslation(fee_type.item, this.appTranslation.getKey('inventory', 'item')),
  //   new TCEnumTranslation(fee_type.card, this.appTranslation.getKey('financial', 'card_fee')),
  //   new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure')),
  //   new TCEnumTranslation(fee_type.lab_test, this.appTranslation.getKey('laboratory', 'lab_test_id')),
  //   new TCEnumTranslation(fee_type.lab_panel, this.appTranslation.getKey('laboratory', 'lab_panel_id')),
  //   new TCEnumTranslation(fee_type.dialysis, this.appTranslation.getKey('laboratory', 'dialysis')),
  //   new TCEnumTranslation(fee_type.physio_theraphy, this.appTranslation.getKey('patient', 'physiotherapy')),
  //   new TCEnumTranslation(fee_type.rad_test, this.appTranslation.getKey('laboratory', 'rad_test')),
  //   new TCEnumTranslation(fee_type.rad_panel, this.appTranslation.getKey('laboratory', 'rad_panel')),
  //   new TCEnumTranslation(fee_type.pat_test, this.appTranslation.getKey('laboratory', 'pat_test')),
  //   new TCEnumTranslation(fee_type.pat_panel, this.appTranslation.getKey('laboratory', 'pat_panel')),
  //   new TCEnumTranslation(fee_type.other_service, this.appTranslation.getKey('financial', 'other_fee')),
  //   new TCEnumTranslation(fee_type.ambulance_in_km,this.appTranslation.getKey('financial','ambulance_in_km')),
  //   new TCEnumTranslation(fee_type.ambulance_in_waiting,this.appTranslation.getKey('financial','ambulance_in_waiting')),
  //   new TCEnumTranslation(fee_type.doctor, this.appTranslation.getKey('employee', 'doctor')),
  //   new TCEnumTranslation(fee_type.operation_room_fee, this.appTranslation.getKey('financial', 'operation_room_fee')),
  //   new TCEnumTranslation(fee_type.oxygen_service, this.appTranslation.getKey('financial', 'oxygen_service')),
  // ];




  timeOption: TCEnum[] = [
    new TCEnum(TimeOption.Default, "Default"),
    new TCEnum(TimeOption.Min, "Min"),
    new TCEnum(TimeOption.Hr, "Hr"),
    new TCEnum(TimeOption.Day, "Day"),
  ]
  timeOption_distance: TCEnum[] = [
    new TCEnum(TimeOption.Default, "Default"),
    new TCEnum(TimeOption.km, "km"),
  ]

  priceOption: TCEnum[] = [
    new TCEnum(TimeOption.Default, "Default"),
    new TCEnum(TimeOption.price, "Price"),
  ]


  fee_frequency_filter: number ;

  FeeFrequncy: TCEnum[] = [
    new TCEnum(FeeFrequency.once, "Once"),
    new TCEnum(FeeFrequency.minute, "Minute"),
    new TCEnum(FeeFrequency.hour, "Hour"),
    new TCEnum(FeeFrequency.day, "Day"),
  ];

  FeeFrequncy_Distance: TCEnum[] = [
    new TCEnum(FeeFrequency.km, "Km"),
  ];

  feeTargetType = {
    [fee_type.oxygen_service] : this.OxygenType,
    [fee_type.operation_room_fee]: this.OperationRoomType
  }

}
