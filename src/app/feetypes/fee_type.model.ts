import {TCId} from "../tc/models";

export class Fee_TypeSummary extends TCId {
  name : string;
}

export class Fee_TypeSummaryPartialList {
  data: Fee_TypeSummary[];
  total: number;
}

export class Fee_TypeDetail extends Fee_TypeSummary {
  name : string;
}

export class Fee_TypeDashboard {
  total: number;
}
