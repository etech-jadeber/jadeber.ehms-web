import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Fee_TypeEditComponent} from "./feetype-edit/feetype-edit.component";
import {Fee_TypePickComponent} from "./feetype-pick/feetype-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Fee_TypeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  fee_typesUrl(): string {
    return "/fee_types";
  }

  fee_typeUrl(id: string): string {
    return "/fee_types/" + id;
  }

  viewFee_Types(): void {
    this.router.navigateByUrl(this.fee_typesUrl());
  }

  viewFee_Type(id: string): void {
    this.router.navigateByUrl(this.fee_typeUrl(id));
  }

  editFee_Type(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Fee_TypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addFee_Type(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Fee_TypeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickFee_Types(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Fee_TypePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
