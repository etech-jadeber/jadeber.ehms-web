import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Fee_TypeDashboard, Fee_TypeDetail, Fee_TypeSummaryPartialList} from "./fee_type.model";


@Injectable({
  providedIn: 'root'
})
export class Fee_TypePersist {

  fee_typeSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchFee_Type(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Fee_TypeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("fee_types", this.fee_typeSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Fee_TypeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.fee_typeSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "fee_types/do", new TCDoParam("download_all", this.filters()));
  }

  fee_typeDashboard(): Observable<Fee_TypeDashboard> {
    return this.http.get<Fee_TypeDashboard>(environment.tcApiBaseUri + "fee_types/dashboard");
  }

  getFee_Type(id: string): Observable<Fee_TypeDetail> {
    return this.http.get<Fee_TypeDetail>(environment.tcApiBaseUri + "fee_types/" + id);
  }

  addFee_Type(item: Fee_TypeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "fee_types/", item);
  }

  updateFee_Type(item: Fee_TypeDetail): Observable<Fee_TypeDetail> {
    return this.http.patch<Fee_TypeDetail>(environment.tcApiBaseUri + "fee_types/" + item.id, item);
  }

  deleteFee_Type(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "fee_types/" + id);
  }

  fee_typesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "fee_types/do", new TCDoParam(method, payload));
  }

  fee_typeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "fee_types/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "fee_types/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "fee_types/" + id + "/do", new TCDoParam("print", {}));
  }


}
