import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeetypeDetailComponent } from './feetype-detail.component';

describe('FeetypeDetailComponent', () => {
  let component: FeetypeDetailComponent;
  let fixture: ComponentFixture<FeetypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeetypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeetypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
