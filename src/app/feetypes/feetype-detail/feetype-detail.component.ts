import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Fee_TypeDetail} from "../fee_type.model";
import {Fee_TypePersist} from "../fee_type.persist";
import {Fee_TypeNavigator} from "../fee_type.navigator";

export enum Fee_TypeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-fee_type-detail',
  templateUrl: './feetype-detail.component.html',
  styleUrls: ['./feetype-detail.component.css']
})
export class Fee_TypeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  fee_typeLoading:boolean = false;
  
  Fee_TypeTabs: typeof Fee_TypeTabs = Fee_TypeTabs;
  activeTab: Fee_TypeTabs = Fee_TypeTabs.overview;
  //basics
  fee_typeDetail: Fee_TypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public fee_typeNavigator: Fee_TypeNavigator,
              public  fee_typePersist: Fee_TypePersist) {
    this.tcAuthorization.requireRead("feetypes");
    this.fee_typeDetail = new Fee_TypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("feetypes");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.fee_typeLoading = true;
    this.fee_typePersist.getFee_Type(id).subscribe(fee_typeDetail => {
          this.fee_typeDetail = fee_typeDetail;
          this.fee_typeLoading = false;
        }, error => {
          console.error(error);
          this.fee_typeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.fee_typeDetail.id);
  }

  editFee_Type(): void {
    let modalRef = this.fee_typeNavigator.editFee_Type(this.fee_typeDetail.id);
    modalRef.afterClosed().subscribe(modifiedFee_TypeDetail => {
      TCUtilsAngular.assign(this.fee_typeDetail, modifiedFee_TypeDetail);
    }, error => {
      console.error(error);
    });
  }

   printFee_Type():void{
      this.fee_typePersist.print(this.fee_typeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print fee_type", true);
      });
    }

  back():void{
      this.location.back();
    }

}
