import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Fee_TypeDetail} from "../fee_type.model";
import {Fee_TypePersist} from "../fee_type.persist";


@Component({
  selector: 'app-fee_type-edit',
  templateUrl: './feetype-edit.component.html',
  styleUrls: ['./feetype-edit.component.css']
})
export class Fee_TypeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  fee_typeDetail: Fee_TypeDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Fee_TypeEditComponent>,
              public  persist: Fee_TypePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("fee_types");
      this.title = this.appTranslation.getText("general","new") +  " "+this.appTranslation.getText("financial", "fee_type");
      this.fee_typeDetail = new Fee_TypeDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("fee_types");
      this.title = this.appTranslation.getText("general","edit") +  " "+this.appTranslation.getText("financial", "fee_type");
      this.isLoadingResults = true;
      this.persist.getFee_Type(this.idMode.id).subscribe(fee_typeDetail => {
        this.fee_typeDetail = fee_typeDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addFee_Type(this.fee_typeDetail).subscribe(value => {
      this.tcNotification.success("Fee_Type added");
      this.fee_typeDetail.id = value.id;
      this.dialogRef.close(this.fee_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateFee_Type(this.fee_typeDetail).subscribe(value => {
      this.tcNotification.success("Fee_Type updated");
      this.dialogRef.close(this.fee_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.fee_typeDetail == null){
            return false;
          }

        if (this.fee_typeDetail.name == null || this.fee_typeDetail.name  == "") {
                      return false;
                    }


        return true;
      }


}
