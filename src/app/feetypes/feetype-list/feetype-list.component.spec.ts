import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeetypeListComponent } from './feetype-list.component';

describe('FeetypeListComponent', () => {
  let component: FeetypeListComponent;
  let fixture: ComponentFixture<FeetypeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeetypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeetypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
