import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Fee_TypePersist} from "../fee_type.persist";
import {Fee_TypeNavigator} from "../fee_type.navigator";
import {Fee_TypeDetail, Fee_TypeSummary, Fee_TypeSummaryPartialList} from "../fee_type.model";


@Component({
  selector: 'app-fee_type-list',
  templateUrl: './feetype-list.component.html',
  styleUrls: ['./feetype-list.component.css']
})
export class Fee_TypeListComponent implements OnInit {

  fee_typesData: Fee_TypeSummary[] = [];
  fee_typesTotalCount: number = 0;
  fee_typesSelectAll:boolean = false;
  fee_typesSelection: Fee_TypeSummary[] = [];

  fee_typesDisplayedColumns: string[] = ["select","action", "name", ];
  fee_typesSearchTextBox: FormControl = new FormControl();
  fee_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) fee_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) fee_typesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public fee_typePersist: Fee_TypePersist,
                public fee_typeNavigator: Fee_TypeNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("feetypes");
       this.fee_typesSearchTextBox.setValue(fee_typePersist.fee_typeSearchText);
      //delay subsequent keyup events
      this.fee_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.fee_typePersist.fee_typeSearchText = value;
        this.searchFee_Types();
      });
    }

    ngOnInit() {

      this.fee_typesSort.sortChange.subscribe(() => {
        this.fee_typesPaginator.pageIndex = 0;
        this.searchFee_Types(true);
      });

      this.fee_typesPaginator.page.subscribe(() => {
        this.searchFee_Types(true);
      });
      //start by loading items
      this.searchFee_Types();
    }

  searchFee_Types(isPagination:boolean = false): void {


    let paginator = this.fee_typesPaginator;
    let sorter = this.fee_typesSort;

    this.fee_typesIsLoading = true;
    this.fee_typesSelection = [];

    this.fee_typePersist.searchFee_Type(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Fee_TypeSummaryPartialList) => {
      this.fee_typesData = partialList.data;
      if (partialList.total != -1) {
        this.fee_typesTotalCount = partialList.total;
      }
      this.fee_typesIsLoading = false;
    }, error => {
      this.fee_typesIsLoading = false;
    });

  }

  downloadFee_Types(): void {
    if(this.fee_typesSelectAll){
         this.fee_typePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download fee_types", true);
      });
    }
    else{
        this.fee_typePersist.download(this.tcUtilsArray.idsList(this.fee_typesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download fee_types",true);
            });
        }
  }



  addFee_Type(): void {
    let dialogRef = this.fee_typeNavigator.addFee_Type();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFee_Types();
      }
    });
  }

  editFee_Type(item: Fee_TypeSummary) {
    let dialogRef = this.fee_typeNavigator.editFee_Type(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteFee_Type(item: Fee_TypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Fee_Type");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.fee_typePersist.deleteFee_Type(item.id).subscribe(response => {
          this.tcNotification.success("Fee_Type deleted");
          this.searchFee_Types();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
