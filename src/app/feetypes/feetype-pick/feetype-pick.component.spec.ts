import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FeetypePickComponent } from './feetype-pick.component';

describe('FeetypePickComponent', () => {
  let component: FeetypePickComponent;
  let fixture: ComponentFixture<FeetypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FeetypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeetypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
