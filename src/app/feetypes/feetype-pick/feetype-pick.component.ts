import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Fee_TypeDetail, Fee_TypeSummary, Fee_TypeSummaryPartialList} from "../fee_type.model";
import {Fee_TypePersist} from "../fee_type.persist";


@Component({
  selector: 'app-fee_type-pick',
  templateUrl: './feetype-pick.component.html',
  styleUrls: ['./feetype-pick.component.css']
})
export class Fee_TypePickComponent implements OnInit {

  fee_typesData: Fee_TypeSummary[] = [];
  fee_typesTotalCount: number = 0;
  fee_typesSelection: Fee_TypeSummary[] = [];
  fee_typesDisplayedColumns: string[] = ["select", 'name' ];

  fee_typesSearchTextBox: FormControl = new FormControl();
  fee_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) fee_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) fee_typesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public fee_typePersist: Fee_TypePersist,
              public dialogRef: MatDialogRef<Fee_TypePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("fee_types");
    this.fee_typesSearchTextBox.setValue(fee_typePersist.fee_typeSearchText);
    //delay subsequent keyup events
    this.fee_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.fee_typePersist.fee_typeSearchText = value;
      this.searchFee_Types();
    });
  }

  ngOnInit() {

    this.fee_typesSort.sortChange.subscribe(() => {
      this.fee_typesPaginator.pageIndex = 0;
      this.searchFee_Types();
    });

    this.fee_typesPaginator.page.subscribe(() => {
      this.searchFee_Types();
    });

    //set initial picker list to 5
    this.fee_typesPaginator.pageSize = 5;

    //start by loading items
    this.searchFee_Types();
  }

  searchFee_Types(): void {
    this.fee_typesIsLoading = true;
    this.fee_typesSelection = [];

    this.fee_typePersist.searchFee_Type(this.fee_typesPaginator.pageSize,
        this.fee_typesPaginator.pageIndex,
        this.fee_typesSort.active,
        this.fee_typesSort.direction).subscribe((partialList: Fee_TypeSummaryPartialList) => {
      this.fee_typesData = partialList.data;
      if (partialList.total != -1) {
        this.fee_typesTotalCount = partialList.total;
      }
      this.fee_typesIsLoading = false;
    }, error => {
      this.fee_typesIsLoading = false;
    });

  }

  markOneItem(item: Fee_TypeSummary) {
    if(!this.tcUtilsArray.containsId(this.fee_typesSelection,item.id)){
          this.fee_typesSelection = [];
          this.fee_typesSelection.push(item);
        }
        else{
          this.fee_typesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.fee_typesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
