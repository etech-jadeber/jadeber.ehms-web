import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalAssessmentDetailComponent } from './final-assessment-detail.component';

describe('FinalAssessmentDetailComponent', () => {
  let component: FinalAssessmentDetailComponent;
  let fixture: ComponentFixture<FinalAssessmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalAssessmentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalAssessmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
