import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {FinalAssessmentDetail} from "../final_assessment.model";
import {FinalAssessmentPersist} from "../final_assessment.persist";
import {FinalAssessmentNavigator} from "../final_assessment.navigator";

export enum FinalAssessmentTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-final_assessment-detail',
  templateUrl: './final-assessment-detail.component.html',
  styleUrls: ['./final-assessment-detail.component.scss']
})
export class FinalAssessmentDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  finalAssessmentIsLoading:boolean = false;
  
  FinalAssessmentTabs: typeof FinalAssessmentTabs = FinalAssessmentTabs;
  activeTab: FinalAssessmentTabs = FinalAssessmentTabs.overview;
  //basics
  finalAssessmentDetail: FinalAssessmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public finalAssessmentNavigator: FinalAssessmentNavigator,
              public  finalAssessmentPersist: FinalAssessmentPersist) {
    this.tcAuthorization.requireRead("final_assessments");
    this.finalAssessmentDetail = new FinalAssessmentDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("final_assessments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.finalAssessmentIsLoading = true;
    this.finalAssessmentPersist.getFinalAssessment(id).subscribe(finalAssessmentDetail => {
          this.finalAssessmentDetail = finalAssessmentDetail;
          this.finalAssessmentIsLoading = false;
        }, error => {
          console.error(error);
          this.finalAssessmentIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.finalAssessmentDetail.id);
  }
  editFinalAssessment(): void {
    let modalRef = this.finalAssessmentNavigator.editFinalAssessment(this.finalAssessmentDetail.id);
    modalRef.afterClosed().subscribe(finalAssessmentDetail => {
      TCUtilsAngular.assign(this.finalAssessmentDetail, finalAssessmentDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.finalAssessmentPersist.print(this.finalAssessmentDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print final_assessment", true);
      });
    }

  back():void{
      this.location.back();
    }

}