import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalAssessmentEditComponent } from './final-assessment-edit.component';

describe('FinalAssessmentEditComponent', () => {
  let component: FinalAssessmentEditComponent;
  let fixture: ComponentFixture<FinalAssessmentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalAssessmentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalAssessmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
