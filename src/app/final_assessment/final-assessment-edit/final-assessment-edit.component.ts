import { Component, OnInit, Inject, Input, Injectable, EventEmitter, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { FinalAssessmentDetail } from '../final_assessment.model';
import { FinalAssessmentPersist } from '../final_assessment.persist';
import { Procedure_Order_CodeNavigator } from 'src/app/form_encounters/procedure_order_codes/procedure_order_code.navigator';
import { Icd11Summary } from 'src/app/icd_11/icd_11.model';
import { Icd11Persist } from 'src/app/icd_11/icd_11.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';


@Injectable()
abstract class FinalAssessmentParentEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  finalAssessmentDetail: FinalAssessmentDetail;
  icd11_diagnosis_options: Icd11Summary[];
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public persist: FinalAssessmentPersist,
    public form_encounterPersist: Form_EncounterPersist,
    public form_encounterNavigator: Form_EncounterNavigator,
    public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
    public tcUtilsDate: TCUtilsDate,
    public icd11Persist : Icd11Persist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    this.load_icd11Diagnois();
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('final_assessments');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'final_assessment';
      this.finalAssessmentDetail = new FinalAssessmentDetail();
      this.finalAssessmentDetail.plan = "";
      this.finalAssessmentDetail.pertinent_investigation_finding = "";
      this.finalAssessmentDetail.final_diagnosis = "";
      this.finalAssessmentDetail.impression_preliminary_diagnosis = "";
      this.finalAssessmentDetail.treatment = "";
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('final_assessments');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'final_assessment';
      this.isLoadingResults = true;
      this.persist.getFinalAssessment(this.idMode.id).subscribe(
        (finalAssessmentDetail) => {
          this.finalAssessmentDetail = finalAssessmentDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
 

  pickIcd11() {
    const dialogRef = this.procedureOrderCodeNavigator.pick_icd_11(true);
    dialogRef.afterClosed().subscribe((result: Icd11Summary[] ) => {
      if (result) {
        this.finalAssessmentDetail.final_diagnosis = result[0].category;
      }
    })
  }
  action(finalAssessment: FinalAssessmentDetail){

  }

  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addFinalAssessment(this.finalAssessmentDetail).subscribe(
      (value) => {
        // this.tcNotification.success('finalAssessment added');
        this.action(this.finalAssessmentDetail)
        this.finalAssessmentDetail.id = value.id;
        this.isLoadingResults = false;
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateFinalAssessment(this.finalAssessmentDetail).subscribe(
      (value) => {
  
        // this.tcNotification.success('final_assessment updated');
        this.action(this.finalAssessmentDetail)
        this.isLoadingResults = false;
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.finalAssessmentDetail == null) {
      return false;
    }
    if (
      this.finalAssessmentDetail.final_diagnosis == null ||
      this.finalAssessmentDetail.final_diagnosis == ''
    ) {
      return false;
    }
    return true;
  }

  addPrescriptions(): void {
    let dialogRef = this.form_encounterNavigator.addPrescriptions(
      this.idMode.id
    );
    dialogRef.afterClosed().subscribe((newPrescriptions) => {

    });
  }

  isEmptyString(value: string): boolean {
    return value == "" || value == null;
  }

  isVisible(value: string):boolean {
    return !this.isEmptyString(value) || this.form_encounterPersist.encounter_is_active;
  }

  appendString(value: string):string{
    value = value.trim();
    if (!this.finalAssessmentDetail.final_diagnosis)
      return "\u2022  " + value;
    if(this.finalAssessmentDetail.final_diagnosis.endsWith("\n"))
      return this.finalAssessmentDetail.final_diagnosis + "\u2022  " + value;
    let x = this.finalAssessmentDetail.final_diagnosis.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return  y ?  y+"\u2022  " +value : "\u2022  " + value;
  }
  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }
  updateIcdDiagnosis(event:string,input):void{
    if (event && !input.value.endsWith("\n"))
    this.icd11Persist.icd11SearchText =input.value.split('\u2022').pop().trim();
    else 
      this.icd11Persist.icd11SearchText = "";
    this.load_icd11Diagnois();
  }
  load_icd11Diagnois(){
    this.icd11Persist.searchIcd11(5,0,"category","asc").subscribe((result)=>{
      if(result)
        this.icd11_diagnosis_options = result.data;
    })
  }
}

@Component({
  selector: 'app-final_assessment-edit',
  templateUrl: './final-assessment-edit.component.html',
  styleUrls: ['./final-assessment-edit.component.scss'],
})
export class FinalAssessmentEditComponent extends FinalAssessmentParentEditComponent {
  isLoadingResults: boolean = false;
  title: string;
  finalAssessmentDetail: FinalAssessmentDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<FinalAssessmentEditComponent>,
    public persist: FinalAssessmentPersist,
    public form_encounterPersist: Form_EncounterPersist,
    public form_encouterNavigator: Form_EncounterNavigator,
    public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
    public tcUtilsDate: TCUtilsDate,
    public icd11Persist : Icd11Persist,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {
    super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist, form_encounterPersist, form_encouterNavigator, procedureOrderCodeNavigator,tcUtilsDate,icd11Persist, idMode
    )
  }
  onCancel(): void {
    this.dialogRef.close();
  }
  action(final_assessment: FinalAssessmentDetail): void {
    this.dialogRef.close(final_assessment);
  }
  }

  @Component({
    selector: 'app-final_assessment-edit-list',
    templateUrl: './final-assessment-edit.component.html',
    styleUrls: ['./final-assessment-edit.component.scss'],
  })

  export class FinalAssessmentEditListComponent extends FinalAssessmentParentEditComponent {
    isLoadingResults: boolean = false;
    title: string;
    finalAssessmentDetail: FinalAssessmentDetail;
    dialogRef = null;
    @Output() assign_value = new EventEmitter<any>();


    @Input() encounterId: string;
    constructor(
      public tcAuthorization: TCAuthorization,
      public tcNotification: TCNotification,
      public tcUtilsString: TCUtilsString,
      public appTranslation: AppTranslation,
      public persist: FinalAssessmentPersist,
      public form_encounterPersist: Form_EncounterPersist,
      public form_encouterNavigator: Form_EncounterNavigator,
      public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
      public tcUtilsDate: TCUtilsDate,
      public icd11Persist : Icd11Persist,
    ) {
      super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist, form_encounterPersist, form_encouterNavigator, procedureOrderCodeNavigator, tcUtilsDate, icd11Persist ,{id:null,mode: TCModalModes.NEW})
      
    }
    onCancel():void{

    }

    ngOnInit(): void {
      this.idMode.id = this.encounterId;
      this.persist.searchFinalAssessment(this.encounterId, 1, 0, 'date_of_assessment', 'desc').subscribe(
        diagnosis => {
          if(diagnosis.data.length){
            this.finalAssessmentDetail = diagnosis.data[0] 
          }else{
            this.assign_value.emit({new_final_assessment: true })
           this.finalAssessmentDetail = new FinalAssessmentDetail()
          }
        }
      )
    }
  
    saveResult(){
      if (!this.canSubmit()){
        return;
      }
      this.finalAssessmentDetail.encounter_id = this.encounterId;
      if (this.finalAssessmentDetail.id){
        this.onUpdate()
      } else {
        this.onAdd()
      }
    }
  
    isNew(): boolean {
      return false
    }
  }  