import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalAssessmentListComponent } from './final-assessment-list.component';

describe('FinalAssessmentListComponent', () => {
  let component: FinalAssessmentListComponent;
  let fixture: ComponentFixture<FinalAssessmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalAssessmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalAssessmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
