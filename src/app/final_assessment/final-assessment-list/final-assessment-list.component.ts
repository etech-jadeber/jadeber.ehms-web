import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FinalAssessmentSummary, FinalAssessmentSummaryPartialList } from '../final_assessment.model';
import { FinalAssessmentPersist } from '../final_assessment.persist';
import { FinalAssessmentNavigator } from '../final_assessment.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
@Component({
  selector: 'app-final_assessment-list',
  templateUrl: './final-assessment-list.component.html',
  styleUrls: ['./final-assessment-list.component.scss']
})export class FinalAssessmentListComponent implements OnInit {
  finalAssessmentsData: FinalAssessmentSummary[] = [];
  finalAssessmentsTotalCount: number = 0;
  finalAssessmentSelectAll:boolean = false;
  finalAssessmentSelection: FinalAssessmentSummary[] = [];

 finalAssessmentsDisplayedColumns: string[] = ["select","action","link" ,"impression_preliminary_diagnosis ","plan","pertinent_investigation_finding","final_diagnosis ","treatment" ];
  finalAssessmentSearchTextBox: FormControl = new FormControl();
  finalAssessmentIsLoading: boolean = false; 
  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) finalAssessmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) finalAssessmentsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public finalAssessmentPersist: FinalAssessmentPersist,
                public finalAssessmentNavigator: FinalAssessmentNavigator,
                public form_encounterPersist: Form_EncounterPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("final_assessments");
       this.finalAssessmentSearchTextBox.setValue(finalAssessmentPersist.finalAssessmentSearchText);
      //delay subsequent keyup events
      this.finalAssessmentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.finalAssessmentPersist.finalAssessmentSearchText = value;
        this.searchFinalAssessments();
      });
    } ngOnInit() {
   
      this.finalAssessmentsSort.sortChange.subscribe(() => {
        this.finalAssessmentsPaginator.pageIndex = 0;
        this.searchFinalAssessments(true);
      });

      this.finalAssessmentsPaginator.page.subscribe(() => {
        this.searchFinalAssessments(true);
      });
      //start by loading items
      this.searchFinalAssessments();
    }

  searchFinalAssessments(isPagination:boolean = false): void {


    let paginator = this.finalAssessmentsPaginator;
    let sorter = this.finalAssessmentsSort;

    this.finalAssessmentIsLoading = true;
    this.finalAssessmentSelection = [];

    this.finalAssessmentPersist.searchFinalAssessment(this.encounterId, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FinalAssessmentSummaryPartialList) => {
      this.finalAssessmentsData = partialList.data;
      if (partialList.total != -1) {
        this.finalAssessmentsTotalCount = partialList.total;
      }
      this.finalAssessmentIsLoading = false;
    }, error => {
      this.finalAssessmentIsLoading = false;
    });

  } downloadFinalAssessments(): void {
    if(this.finalAssessmentSelectAll){
         this.finalAssessmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download final_assessment", true);
      });
    }
    else{
        this.finalAssessmentPersist.download(this.tcUtilsArray.idsList(this.finalAssessmentSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download final_assessment",true);
            });
        }
  }
addFinalAssessment(): void {
    let dialogRef = this.finalAssessmentNavigator.addFinalAssessment();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFinalAssessments();
      }
    });
  }

  editFinalAssessment(item: FinalAssessmentSummary) {
    let dialogRef = this.finalAssessmentNavigator.editFinalAssessment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteFinalAssessment(item: FinalAssessmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("final_assessment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.finalAssessmentPersist.deleteFinalAssessment(item.id).subscribe(response => {
          this.tcNotification.success("final_assessment deleted");
          this.searchFinalAssessments();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}