import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalAssessmentPickComponent } from './final-assessment-pick.component';

describe('FinalAssessmentPickComponent', () => {
  let component: FinalAssessmentPickComponent;
  let fixture: ComponentFixture<FinalAssessmentPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinalAssessmentPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalAssessmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
