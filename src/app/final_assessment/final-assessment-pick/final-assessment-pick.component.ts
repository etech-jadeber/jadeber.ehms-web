import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FinalAssessmentSummary, FinalAssessmentSummaryPartialList } from '../final_assessment.model';
import { FinalAssessmentPersist } from '../final_assessment.persist';
import { FinalAssessmentNavigator } from '../final_assessment.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-final_assessment-pick',
  templateUrl: './final-assessment-pick.component.html',
  styleUrls: ['./final-assessment-pick.component.scss']
})export class FinalAssessmentPickComponent implements OnInit {
  finalAssessmentsData: FinalAssessmentSummary[] = [];
  finalAssessmentsTotalCount: number = 0;
  finalAssessmentSelectAll:boolean = false;
  finalAssessmentSelection: FinalAssessmentSummary[] = [];

 finalAssessmentsDisplayedColumns: string[] = ["select", ,"impression_preliminary_diagnosis ","plan","pertinent_investigation_finding","final_diagnosis ","treatment"];
  finalAssessmentSearchTextBox: FormControl = new FormControl();
  finalAssessmentIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) finalAssessmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) finalAssessmentsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public finalAssessmentPersist: FinalAssessmentPersist,
                public finalAssessmentNavigator: FinalAssessmentNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
 public dialogRef: MatDialogRef<FinalAssessmentPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("final_assessments");
       this.finalAssessmentSearchTextBox.setValue(finalAssessmentPersist.finalAssessmentSearchText);
      //delay subsequent keyup events
      this.finalAssessmentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.finalAssessmentPersist.finalAssessmentSearchText = value;
        this.searchFinal_assessments();
      });
    } ngOnInit() {
   
      this.finalAssessmentsSort.sortChange.subscribe(() => {
        this.finalAssessmentsPaginator.pageIndex = 0;
        this.searchFinal_assessments(true);
      });

      this.finalAssessmentsPaginator.page.subscribe(() => {
        this.searchFinal_assessments(true);
      });
      //start by loading items
      this.searchFinal_assessments();
    }

  searchFinal_assessments(isPagination:boolean = false): void {


    let paginator = this.finalAssessmentsPaginator;
    let sorter = this.finalAssessmentsSort;

    this.finalAssessmentIsLoading = true;
    this.finalAssessmentSelection = [];

    this.finalAssessmentPersist.searchFinalAssessment(this.tcUtilsString.invalid_id, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FinalAssessmentSummaryPartialList) => {
      this.finalAssessmentsData = partialList.data;
      if (partialList.total != -1) {
        this.finalAssessmentsTotalCount = partialList.total;
      }
      this.finalAssessmentIsLoading = false;
    }, error => {
      this.finalAssessmentIsLoading = false;
    });

  }
  markOneItem(item: FinalAssessmentSummary) {
    if(!this.tcUtilsArray.containsId(this.finalAssessmentSelection,item.id)){
          this.finalAssessmentSelection = [];
          this.finalAssessmentSelection.push(item);
        }
        else{
          this.finalAssessmentSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.finalAssessmentSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }