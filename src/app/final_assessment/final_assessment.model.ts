import {TCId} from "../tc/models";
export class FinalAssessmentSummary extends TCId {
    impression_preliminary_diagnosis :string;
    plan:string;
    pertinent_investigation_finding:string;
    final_diagnosis :string;
    treatment:string;
    encounter_id: string;
     
    }
    export class FinalAssessmentSummaryPartialList {
      data: FinalAssessmentSummary[];
      total: number;
    }
    export class FinalAssessmentDetail extends FinalAssessmentSummary {
    }
    
    export class FinalAssessmentDashboard {
      total: number;
    }