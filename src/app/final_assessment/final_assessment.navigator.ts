import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {FinalAssessmentEditComponent} from "./final-assessment-edit/final-assessment-edit.component";
import {FinalAssessmentPickComponent} from "./final-assessment-pick/final-assessment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class FinalAssessmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  finalAssessmentsUrl(): string {
    return "/final_assessments";
  }

  finalAssessmentUrl(id: string): string {
    return "/final_assessments/" + id;
  }

  viewFinalAssessments(): void {
    this.router.navigateByUrl(this.finalAssessmentsUrl());
  }

  viewFinalAssessment(id: string): void {
    this.router.navigateByUrl(this.finalAssessmentUrl(id));
  }

  editFinalAssessment(id: string): MatDialogRef<FinalAssessmentEditComponent> {
    const dialogRef = this.dialog.open(FinalAssessmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addFinalAssessment(): MatDialogRef<FinalAssessmentEditComponent> {
    const dialogRef = this.dialog.open(FinalAssessmentEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickFinalAssessments(selectOne: boolean=false): MatDialogRef<FinalAssessmentPickComponent> {
      const dialogRef = this.dialog.open(FinalAssessmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}