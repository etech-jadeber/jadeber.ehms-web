import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {FinalAssessmentDashboard, FinalAssessmentDetail, FinalAssessmentSummaryPartialList} from "./final_assessment.model";


@Injectable({
  providedIn: 'root'
})
export class FinalAssessmentPersist {
 finalAssessmentSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.finalAssessmentSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchFinalAssessment(form_encounter: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<FinalAssessmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + form_encounter + "/final_assessment", this.finalAssessmentSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<FinalAssessmentSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "final_assessment/do", new TCDoParam("download_all", this.filters()));
  }

  finalAssessmentDashboard(): Observable<FinalAssessmentDashboard> {
    return this.http.get<FinalAssessmentDashboard>(environment.tcApiBaseUri + "final_assessment/dashboard");
  }

  getFinalAssessment(id: string): Observable<FinalAssessmentDetail> {
    return this.http.get<FinalAssessmentDetail>(environment.tcApiBaseUri + "final_assessment/" + id);
  } addFinalAssessment(item: FinalAssessmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "final_assessment/", item);
  }

  updateFinalAssessment(item: FinalAssessmentDetail): Observable<FinalAssessmentDetail> {
    return this.http.patch<FinalAssessmentDetail>(environment.tcApiBaseUri + "final_assessment/" + item.id, item);
  }

  deleteFinalAssessment(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "final_assessment/" + id);
  }
 finalAssessmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "final_assessment/do", new TCDoParam(method, payload));
  }

  finalAssessmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "final_assessments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "final_assessment/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "final_assessment/" + id + "/do", new TCDoParam("print", {}));
  }


}