import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowSheetDetailComponent } from './flow-sheet-detail.component';

describe('FlowSheetDetailComponent', () => {
  let component: FlowSheetDetailComponent;
  let fixture: ComponentFixture<FlowSheetDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowSheetDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowSheetDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
