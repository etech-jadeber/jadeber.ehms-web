import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { FlowSheetDetail } from '../flow_sheet.model';
import { FlowSheetNavigator } from '../flow_sheet.navigator';
import { FlowSheetPersist } from '../flow_sheet.persist';



export enum FlowSheetTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-flow-sheet-detail',
  templateUrl: './flow-sheet-detail.component.html',
  styleUrls: ['./flow-sheet-detail.component.scss']
})
export class FlowSheetDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  flowSheetIsLoading:boolean = false;
  
  FlowSheetTabs: typeof FlowSheetTabs = FlowSheetTabs;
  activeTab: FlowSheetTabs = FlowSheetTabs.overview;
  //basics
  flowSheetDetail: FlowSheetDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public flowSheetNavigator: FlowSheetNavigator,
              public  flowSheetPersist: FlowSheetPersist) {
    this.tcAuthorization.requireRead("flow_sheets");
    this.flowSheetDetail = new FlowSheetDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("flow_sheets");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.flowSheetIsLoading = true;
    this.flowSheetPersist.getFlowSheet(id).subscribe(flowSheetDetail => {
          this.flowSheetDetail = flowSheetDetail;
          this.flowSheetIsLoading = false;
        }, error => {
          console.error(error);
          this.flowSheetIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.flowSheetDetail.id);
  }
  editFlowSheet(): void {
    let modalRef = this.flowSheetNavigator.editFlowSheet(this.flowSheetDetail.id);
    modalRef.afterClosed().subscribe(flowSheetDetail => {
      TCUtilsAngular.assign(this.flowSheetDetail, flowSheetDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.flowSheetPersist.print(this.flowSheetDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print flow_sheet", true);
      });
    }

  back():void{
      this.location.back();
    }

}