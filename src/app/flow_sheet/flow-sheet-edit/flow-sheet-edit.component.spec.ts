import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowSheetEditComponent } from './flow-sheet-edit.component';

describe('FlowSheetEditComponent', () => {
  let component: FlowSheetEditComponent;
  let fixture: ComponentFixture<FlowSheetEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowSheetEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowSheetEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
