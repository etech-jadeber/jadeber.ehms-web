import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { FlowSheetDetail } from '../flow_sheet.model';
import { FlowSheetPersist } from '../flow_sheet.persist';
@Component({
  selector: 'app-flow-sheet-edit',
  templateUrl: './flow-sheet-edit.component.html',
  styleUrls: ['./flow-sheet-edit.component.scss'],
})
export class FlowSheetEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  flowSheetDetail: FlowSheetDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<FlowSheetEditComponent>,
    public persist: FlowSheetPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('flow_sheets');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'flow_sheet';
      this.flowSheetDetail = new FlowSheetDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('flow_sheets');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'flow_sheet';
      this.isLoadingResults = true;
      this.persist.getFlowSheet(this.idMode.id).subscribe(
        (flowSheetDetail) => {
          this.flowSheetDetail = flowSheetDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addFlowSheet(this.flowSheetDetail).subscribe(
      (value) => {
        this.tcNotification.success('flowSheet added');
        this.flowSheetDetail.id = value.id;
        this.dialogRef.close(this.flowSheetDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateFlowSheet(this.flowSheetDetail).subscribe(
      (value) => {
        this.tcNotification.success('flow_sheet updated');
        this.dialogRef.close(this.flowSheetDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.flowSheetDetail == null) {
      return false;
    }
  }
}
