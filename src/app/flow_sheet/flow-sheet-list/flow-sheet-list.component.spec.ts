import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowSheetListComponent } from './flow-sheet-list.component';

describe('FlowSheetListComponent', () => {
  let component: FlowSheetListComponent;
  let fixture: ComponentFixture<FlowSheetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowSheetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowSheetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
