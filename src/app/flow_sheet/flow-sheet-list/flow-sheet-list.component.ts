import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FlowSheetSummary, FlowSheetSummaryPartialList } from '../flow_sheet.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FlowSheetNavigator } from '../flow_sheet.navigator';
import { FlowSheetPersist } from '../flow_sheet.persist';
@Component({
  selector: 'app-flow-sheet-list',
  templateUrl: './flow-sheet-list.component.html',
  styleUrls: ['./flow-sheet-list.component.scss']
})

export class FlowSheetListComponent implements OnInit {
  flowSheetsData: FlowSheetSummary[] = [];
  flowSheetsTotalCount: number = 0;
  flowSheetSelectAll:boolean = false;
  flowSheetSelection: FlowSheetSummary[] = [];

  flowSheetsDisplayedColumns: string[] = ["select","action" ,"total_hourly_output" ];
  flowSheetSearchTextBox: FormControl = new FormControl();
  flowSheetIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) flowSheetsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) flowSheetsSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public flowSheetPersist: FlowSheetPersist,
                public flowSheetNavigator: FlowSheetNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("flow_sheets");
       this.flowSheetSearchTextBox.setValue(flowSheetPersist.flowSheetSearchText);
      //delay subsequent keyup events
      this.flowSheetSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.flowSheetPersist.flowSheetSearchText = value;
        this.searchFlow_sheets();
      });
    } ngOnInit() {
   
      this.flowSheetsSort.sortChange.subscribe(() => {
        this.flowSheetsPaginator.pageIndex = 0;
        this.searchFlow_sheets(true);
      });

      this.flowSheetsPaginator.page.subscribe(() => {
        this.searchFlow_sheets(true);
      });
      //start by loading items
      this.searchFlow_sheets();
    }

  searchFlow_sheets(isPagination:boolean = false): void {


    let paginator = this.flowSheetsPaginator;
    let sorter = this.flowSheetsSort;

    this.flowSheetIsLoading = true;
    this.flowSheetSelection = [];

    this.flowSheetPersist.searchFlowSheet(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FlowSheetSummaryPartialList) => {
      this.flowSheetsData = partialList.data;
      if (partialList.total != -1) {
        this.flowSheetsTotalCount = partialList.total;
      }
      this.flowSheetIsLoading = false;
    }, error => {
      this.flowSheetIsLoading = false;
    });

  } downloadFlowSheets(): void {
    if(this.flowSheetSelectAll){
         this.flowSheetPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download flow_sheet", true);
      });
    }
    else{
        this.flowSheetPersist.download(this.tcUtilsArray.idsList(this.flowSheetSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download flow_sheet",true);
            });
        }
  }
addFlow_sheet(): void {
    let dialogRef = this.flowSheetNavigator.addFlowSheet();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFlow_sheets();
      }
    });
  }

  editFlowSheet(item: FlowSheetSummary) {
    let dialogRef = this.flowSheetNavigator.editFlowSheet(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteFlowSheet(item: FlowSheetSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("flow_sheet");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.flowSheetPersist.deleteFlowSheet(item.id).subscribe(response => {
          this.tcNotification.success("flow_sheet deleted");
          this.searchFlow_sheets();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}