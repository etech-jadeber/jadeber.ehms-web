import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowSheetPickComponent } from './flow-sheet-pick.component';

describe('FlowSheetPickComponent', () => {
  let component: FlowSheetPickComponent;
  let fixture: ComponentFixture<FlowSheetPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowSheetPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowSheetPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
