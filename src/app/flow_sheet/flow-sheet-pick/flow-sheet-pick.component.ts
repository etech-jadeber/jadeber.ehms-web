import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FlowSheetSummary, FlowSheetSummaryPartialList } from '../flow_sheet.model';
import { FlowSheetPersist } from '../flow_sheet.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-flow-sheet-pick',
  templateUrl: './flow-sheet-pick.component.html',
  styleUrls: ['./flow-sheet-pick.component.scss']
})
export class FlowSheetPickComponent implements OnInit {
  flowSheetsData: FlowSheetSummary[] = [];
  flowSheetsTotalCount: number = 0;
  flowSheetSelectAll:boolean = false;
  flowSheetSelection: FlowSheetSummary[] = [];

 flowSheetsDisplayedColumns: string[] = ["select","action" ,"total_hourly_output" ];
  flowSheetSearchTextBox: FormControl = new FormControl();
  flowSheetIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) flowSheetsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) flowSheetsSort: MatSort;  
  constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialogRef: MatDialogRef<FlowSheetPickComponent>,
                public flowSheetPersist: FlowSheetPersist,
                @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("flow_sheets");
       this.flowSheetSearchTextBox.setValue(flowSheetPersist.flowSheetSearchText);
      //delay subsequent keyup events
      this.flowSheetSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.flowSheetPersist.flowSheetSearchText = value;
        this.searchFlow_sheets();
      });
    } ngOnInit() {
   
      this.flowSheetsSort.sortChange.subscribe(() => {
        this.flowSheetsPaginator.pageIndex = 0;
        this.searchFlow_sheets(true);
      });

      this.flowSheetsPaginator.page.subscribe(() => {
        this.searchFlow_sheets(true);
      });
      //start by loading items
      this.searchFlow_sheets();
    }

  searchFlow_sheets(isPagination:boolean = false): void {


    let paginator = this.flowSheetsPaginator;
    let sorter = this.flowSheetsSort;

    this.flowSheetIsLoading = true;
    this.flowSheetSelection = [];

    this.flowSheetPersist.searchFlowSheet(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FlowSheetSummaryPartialList) => {
      this.flowSheetsData = partialList.data;
      if (partialList.total != -1) {
        this.flowSheetsTotalCount = partialList.total;
      }
      this.flowSheetIsLoading = false;
    }, error => {
      this.flowSheetIsLoading = false;
    });

  }
  markOneItem(item: FlowSheetSummary) {
    if(!this.tcUtilsArray.containsId(this.flowSheetSelection,item.id)){
          this.flowSheetSelection = [];
          this.flowSheetSelection.push(item);
        }
        else{
          this.flowSheetSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.flowSheetSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }