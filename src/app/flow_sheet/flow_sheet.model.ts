import { TCId } from '../tc/models';
export class FlowSheetSummary extends TCId {
  peep: number;
  date: number;
  rass: number;
  sbp: number;
  dbp: number;
  map: number;
  pr: number;
  rhythm: number;
  temprature: number;
  rbs: number;
  o2_support: number;
  o2_litre_or_fio2: number;
  flow_rate: string;
  rr: string;
  iv_niv: string;
  mode: string;
  pressure_set: number;
  psv: number;
  peak_pressure: number;
  plateau_pressure: number;
  tidal_volume_achieved: number;
  ketamine: number;
  ketofol_amount: number;
  dopamine: number;
  feed_given: number;
  flush: number;
  driving_pressure: number;
  tidal_volume_set: number;
  fio2: number;
  spo2: number;
  total_rr: number;
  propofol: number;
  diazepam_amount: number;
  dopamine_amount: number;
  bolus_fluid_amount: number;
  iv_fluid_amount: number;
  feed_type: string;
  additional_feeding: string;
  rr_set: number;
  ketamine_amount: number;
  propofol_amount: number;
  ketofol: number;
  diazepam: number;
  ne_e: number;
  ne_e_amount: number;
  bolus_fluid_type: number;
  iv_fluid: string;
  medication_fluid_amount: number;
  gastric_residual: number;
  total_hurly_input: number;
  urine_bag_off_ground: string;
  urine_hourly: number;
  urine_total: number;
  ng_og_tube: number;
  abdominal_drainage: number;
  vomitus: number;
  stool: number;
  chest_tube_right: number;
  chest_tube_left: number;
  insensible_loss: number;
  total_hourly_output: number;
}
export class FlowSheetSummaryPartialList {
  data: FlowSheetSummary[];
  total: number;
}
export class FlowSheetDetail extends FlowSheetSummary {}

export class FlowSheetDashboard {
  total: number;
}
