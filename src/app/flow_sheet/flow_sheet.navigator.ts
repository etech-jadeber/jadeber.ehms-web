import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { FlowSheetEditComponent } from './flow-sheet-edit/flow-sheet-edit.component';
import { FlowSheetPickComponent } from './flow-sheet-pick/flow-sheet-pick.component';

@Injectable({
  providedIn: 'root',
})
export class FlowSheetNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  flowSheetsUrl(): string {
    return '/flow_sheets';
  }

  flowSheetUrl(id: string): string {
    return '/flow_sheets/' + id;
  }

  viewFlowSheets(): void {
    this.router.navigateByUrl(this.flowSheetsUrl());
  }

  viewFlowSheet(id: string): void {
    this.router.navigateByUrl(this.flowSheetUrl(id));
  }

  editFlowSheet(id: string): MatDialogRef<FlowSheetEditComponent> {
    const dialogRef = this.dialog.open(FlowSheetEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addFlowSheet(): MatDialogRef<FlowSheetEditComponent> {
    const dialogRef = this.dialog.open(FlowSheetEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickFlowSheets(
    selectOne: boolean = false
  ): MatDialogRef<FlowSheetPickComponent> {
    const dialogRef = this.dialog.open(FlowSheetPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
