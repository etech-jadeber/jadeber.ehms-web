import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  FlowSheetDashboard,
  FlowSheetDetail,
  FlowSheetSummaryPartialList,
} from './flow_sheet.model';

@Injectable({
  providedIn: 'root',
})
export class FlowSheetPersist {
  flowSheetSearchText: string = '';
  constructor(private http: HttpClient) {}

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.flowSheetSearchText;
    //add custom filters
    return fltrs;
  }

  searchFlowSheet(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<FlowSheetSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'flow_sheet',
      this.flowSheetSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<FlowSheetSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'flow_sheet/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  flowSheetDashboard(): Observable<FlowSheetDashboard> {
    return this.http.get<FlowSheetDashboard>(
      environment.tcApiBaseUri + 'flow_sheet/dashboard'
    );
  }

  getFlowSheet(id: string): Observable<FlowSheetDetail> {
    return this.http.get<FlowSheetDetail>(
      environment.tcApiBaseUri + 'flow_sheet/' + id
    );
  }

  addFlowSheet(item: FlowSheetDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "flow_sheet",
      item
    );
  }

  updateFlowSheet(item: FlowSheetDetail): Observable<FlowSheetDetail> {
    return this.http.patch<FlowSheetDetail>(
      environment.tcApiBaseUri + 'flow_sheet/' + item.id,
      item
    );
  }

  deleteFlowSheet(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'flow_sheet/' + id);
  }
  flowSheetsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'flow_sheet/do',
      new TCDoParam(method, payload)
    );
  }

  flowSheetDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'flow_sheet/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'flow_sheet/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'flow_sheet/' + id + '/do',
      new TCDoParam('print', {})
    );
  }


}
