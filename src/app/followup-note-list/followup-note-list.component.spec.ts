import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowupNoteListComponent } from './followup-note-list.component';

describe('FollowupNoteListComponent', () => {
  let component: FollowupNoteListComponent;
  let fixture: ComponentFixture<FollowupNoteListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FollowupNoteListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowupNoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
