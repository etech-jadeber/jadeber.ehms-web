import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, interval } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { Follow_Up_NoteDetail, Follow_Up_NoteSummary } from '../form_encounters/form_encounter.model';
import { CommentDetail, CommentSummary } from '../tc/models';
import { TCAuthentication } from '../tc/authentication';
import { UserPersist } from '../tc/users/user.persist';
import { PatientPersist } from '../patients/patients.persist';
import { TCAppInit } from '../tc/app-init';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-followup-note-list',
  templateUrl: './followup-note-list.component.html',
  styleUrls: ['./followup-note-list.component.css']
})
export class FollowupNoteListComponent implements OnInit {
  //follow_up_notes
  follow_up_notesData: Follow_Up_NoteSummary[] = [];
  follow_up_notesTotalCount: number = 0;
  follow_up_notesSelectAll: boolean = false;
  follow_up_notesSelected: Follow_Up_NoteSummary[] = [];
  notesSelected: Follow_Up_NoteSummary[] = [];
  follow_up_notesDisplayedColumns: string[] = [
    'select',
    'action',
    'note',
    'date',
  ];
  follow_up_notesSearchTextBox: FormControl = new FormControl();
  follow_up_notesLoading: boolean = false;

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public patientPersist: PatientPersist,
  ) { 
        //follow_up_notes filter
        this.follow_up_notesSearchTextBox.setValue(
          form_encounterPersist.follow_up_noteSearchText
        );
        this.follow_up_notesSearchTextBox.valueChanges
          .pipe(debounceTime(500))
          .subscribe((value) => {
            this.form_encounterPersist.follow_up_noteSearchText = value.trim();
            this.searchFollow_Up_Notes();
          });
    
  }

  ngOnInit(): void {
this.form_encounterPersist.followUpNotePatientId = this.patientId
    this.searchFollow_Up_Notes();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.form_encounterPersist.followUpNoteEncounterId= this.encounterId;
    } else {
    this.form_encounterPersist.followUpNoteEncounterId= null;
    }
    this.searchFollow_Up_Notes()
    }
    //follow_up_notes methods
    searchFollow_Up_Notes(isPagination: boolean = false): void {
      this.follow_up_notesSelected = [];
      this.follow_up_notesLoading = true;
      this.form_encounterPersist
        .searchFollow_Up_Note(50, 0, '', 'asc')
        .subscribe(
          (response) => {
            this.follow_up_notesData = response.data;
            if (response.total != -1) {
              this.follow_up_notesTotalCount = response.total;
              this.onResult.emit(response.total);
            }
            this.follow_up_notesLoading = false;
          },
          (error) => {
            this.follow_up_notesLoading = false;
          }
        );
    }
  
    addFollow_Up_Note(): void {
      let modalRef = this.tcNavigator.textInput(
        this.appTranslation.getText('notes', 'note'),
        '',
        true
      );
      modalRef.afterClosed().subscribe((note) => {
        if (note) {
          this.form_encounterPersist
            .addFollow_Up_Note(this.encounterId, { note: note })
            .subscribe((response) => {
              this.searchFollow_Up_Notes();
            });
        }
      });
    }
  
    editFollow_Up_Note(item: Follow_Up_NoteDetail): void {
      let modalRef = this.tcNavigator.textInput(
        this.appTranslation.getText('notes', 'note'),
        item.note,
        true
      );
      modalRef.afterClosed().subscribe((note) => {
        if (note) {
          this.form_encounterPersist
            .updateFollow_Up_Note(
              this.encounterId,
              { note: note },
              item.id
            )
            .subscribe((response) => {
              this.searchFollow_Up_Notes();
            });
        }
      });
    }
    canUpdateFollowUpNote(item: CommentDetail): boolean {
      return (
        this.tcAuthorization.isSysAdmin() ||
        item.commented_by == TCAppInit.userInfo.user_id
      );
    }
  
    deleteFollowUpNote(item: Follow_Up_NoteDetail): void {
      let dialogRef = this.tcNavigator.confirmDeletion('Follow_Up_Note');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist
            .deleteFollow_Up_Note(this.encounterId, item.id)
            .subscribe(
              (response) => {
                this.tcNotification.success('follow_up_note deleted');
                this.searchFollow_Up_Notes();
              },
              (error) => {}
            );
        }
      });
    }
  
    archiveFollowUpNote(item: CommentDetail): void {
      this.form_encounterPersist
        .follow_up_noteDo(this.encounterId, item.id, 'archive', {})
        .subscribe(
          (response) => {
            this.tcNotification.success('note archived');
            this.searchFollow_Up_Notes();
          },
          (error) => {}
        );
    }


  back(): void {
    this.location.back();
  } 


}
