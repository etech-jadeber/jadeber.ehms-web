import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEncountersListComponent } from './form-encounters-list.component';

describe('FormEncountersListComponent', () => {
  let component: FormEncountersListComponent;
  let fixture: ComponentFixture<FormEncountersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEncountersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEncountersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
