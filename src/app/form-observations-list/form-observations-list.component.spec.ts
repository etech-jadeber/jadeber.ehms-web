import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormObservationsListComponent } from './form-observations-list.component';

describe('FormObservationsListComponent', () => {
  let component: FormObservationsListComponent;
  let fixture: ComponentFixture<FormObservationsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormObservationsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormObservationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
