import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_ObservationDetail, Form_ObservationSummary } from '../form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';

@Component({
  selector: 'app-form-observations-list',
  templateUrl: './form-observations-list.component.html',
  styleUrls: ['./form-observations-list.component.css']
})
export class FormObservationsListComponent implements OnInit {
  //form_observations
  form_observationsData: Form_ObservationSummary[] = [];
  form_observationsTotalCount: number = 0;
  form_observationsSelectAll: boolean = false;
  form_observationsSelected: Form_ObservationSummary[] = [];
  form_observationsIsLoading: boolean = false;
  form_observationLoading: boolean = false;
  form_observationsDisplayedColumns: string[] = [
    'select',
    'action',
    'observation',
    'code_type',
    'description',
    'code ',
  ];
  form_observationsSearchTextBox: FormControl = new FormControl();
  form_observationsLoading: boolean = false;


  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public form_observationPersist: Form_EncounterPersist,
  ) { 
    this.tcAuthorization.requireRead("form_observations")
    this.form_observationsSearchTextBox.setValue(
      form_encounterPersist.form_observationSearchText
    );
    this.form_observationsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.form_observationSearchText = value.trim();
        this.searchForm_Observations();
      });

  }

  ngOnInit(): void {
    // form_observations sorter
    this.sorter.sortChange.subscribe(() => {
        this.paginator.pageIndex = 0;
        this.searchForm_Observations(true);
      });
    // form_observations paginator
    this.paginator.page.subscribe(() => {
        this.searchForm_Observations(true);
      });
      this.searchForm_Observations(true);

  }

  //form_observations methods
  searchForm_Observations(isPagination: boolean = false): void {
    let paginator =
      this.paginator;
    let sorter = this.sorter;
    this.form_observationsSelected = [];
    this.form_observationsLoading = true;

    this.form_encounterPersist
      .searchForm_Observation(
        this.encounterId,
        paginator.pageSize,
        paginator.pageIndex,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.form_observationsData = response.data;

          if (response.total != -1) {
            this.form_observationsTotalCount = response.total;
          }
          this.form_observationsLoading = false;
        },
        (error) => {
          this.form_observationsLoading = false;
        }
      );
  }

  addForm_Observation(): void {
    let dialogRef = this.form_encounterNavigator.addForm_Observation(
      this.encounterId
    );
    dialogRef.afterClosed().subscribe((newForm_Observation) => {
      this.searchForm_Observations();
    });
  }

  editForm_Observation(item: Form_ObservationDetail): void {
    let dialogRef = this.form_encounterNavigator.editForm_Observation(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedForm_Observation) => {
      if (updatedForm_Observation) {
        this.searchForm_Observations();
      }
    });
  }

  deleteForm_Observation(item: Form_ObservationDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Form_Observation');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteForm_Observation(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('form_observation deleted');
              this.searchForm_Observations();
            },
            (error) => {}
          );
      }
    });
  }

  downloadForm_Observations(): void {
    this.tcNotification.info(
      'Download form_observations : ' + this.form_observationsSelected.length
    );
  }
  back(): void {
    this.location.back();
  } 

}
