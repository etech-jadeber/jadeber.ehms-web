import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSoapsListComponent } from './form-soaps-list.component';

describe('FormSoapsListComponent', () => {
  let component: FormSoapsListComponent;
  let fixture: ComponentFixture<FormSoapsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSoapsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSoapsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
