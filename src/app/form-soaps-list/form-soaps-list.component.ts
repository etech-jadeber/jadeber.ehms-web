import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_SoapDetail, Form_SoapSummary } from '../form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { TCAuthorization } from '../tc/authorization';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { UserDetail } from '../tc/users/user.model';
import { UserPersist } from '../tc/users/user.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-form-soaps-list',
  templateUrl: './form-soaps-list.component.html',
  styleUrls: ['./form-soaps-list.component.css']
})
export class FormSoapsListComponent implements OnInit {
  form_soapsData: Form_SoapSummary[] = [];
  form_soapsTotalCount: number = 0;
  form_soapsSelectAll: boolean = false;
  form_soapsSelected: Form_SoapSummary[] = [];
  form_soapsDisplayedColumns: string[] = [
    'select',
    'action',
    'date',
    'note',
    // 'subjective',
    // 'objective',
    // 'assessment',
    // 'plan',
    'final_diagnosis',
    'user',
   

  ];
  form_soapsSearchTextBox: FormControl = new FormControl();
  form_soapsLoading: boolean = false;
  user: {[id: string] : UserDetail} = {}

  @Input() encounterId: string;
  @Input() patient_id: string;
  @Input() is_previous: boolean = false;
  @Output() onResult = new EventEmitter<number>();
  @Output() assign_value = new EventEmitter<boolean>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public userPersist: UserPersist,
  ) {
    this.form_soapsSearchTextBox.setValue(
      form_encounterPersist.form_soapSearchText
    );
    this.form_soapsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.form_soapSearchText = value.trim();
        this.searchForm_Soaps();
      });
   }

  ngOnInit(): void {
    if (this.patient_id) {
    this.form_encounterPersist.form_soapPatientId = this.patient_id;
    this.form_encounterPersist.form_soapEncounterId = null;
    } else {
    this.form_encounterPersist.form_soapEncounterId = this.encounterId;
    }
    this.sorter.active = 'date'
    this.sorter.direction = 'desc'
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchForm_Soaps(true);
    });
  // form_soaps paginator
  this.paginator.page.subscribe(() => {
      this.searchForm_Soaps(true);
    });
    this.searchForm_Soaps(true); 
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.form_encounterPersist.form_soapEncounterId = this.encounterId;
    } else {
      this.form_encounterPersist.form_soapEncounterId = null;
    }
    this.searchForm_Soaps()
  }

  searchForm_Soaps(isPagination: boolean = false): void {
    this.tcAuthorization.canRead("form_soaps");
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.form_soapsSelected = [];
    this.form_soapsLoading = true;
    this.form_encounterPersist
      .searchForm_Soap(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.form_soapsData = response.data;
          this.form_soapsData.forEach(value => {
            if (!this.user[value.provider_id]){
              this.user[value.provider_id] = new UserDetail()
              this.userPersist.getUser(value.provider_id).subscribe(
                user => this.user[value.provider_id] = user
              )
            }
          })
          if (response.total != -1) {
            this.assign_value.emit((response.total > 0))
            this.form_soapsTotalCount = response.total
          }
          this.form_soapsLoading = false;
        },
        (error) => {
          this.form_soapsLoading = false;
        }
      );
  }

  addForm_Soap(): void {
    this.tcAuthorization.canCreate("form_soaps");
      let dialogRef = this.form_encounterNavigator.addForm_Soap(
        this.encounterId
      );
      dialogRef.afterClosed().subscribe((newForm_Soap) => {
        this.searchForm_Soaps();
      });
  }

  editForm_Soap(item: Form_SoapDetail): void {
    this.tcAuthorization.canUpdate("form_soaps");
    let dialogRef = this.form_encounterNavigator.editForm_Soap(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedForm_Soap) => {
      if (updatedForm_Soap) {
        this.searchForm_Soaps();
      }
    });
  }
  editForm_Soap_as_new(item: Form_SoapDetail): void {
    this.tcAuthorization.canUpdate("form_soaps");
    let dialogRef = this.form_encounterNavigator.editForm_Soap_as_new(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedForm_Soap) => {
      if (updatedForm_Soap) {
        this.searchForm_Soaps();
      }
    });
  }


  deleteForm_Soap(item: Form_SoapDetail): void {
    this.tcAuthorization.canDelete("form_soaps");
    let dialogRef = this.tcNavigator.confirmDeletion('Form_Soap');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteForm_Soap(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('form_soap deleted');
              this.searchForm_Soaps();
            },
            (error) => {}
          );
      }
    });
  }
  downloadForm_Soaps(): void {
    this.tcNotification.info(
      'Download form_soaps : ' + this.form_soapsSelected.length
    );
  }


back(): void {
  this.location.back();
}

getUser(user_id: string): string {
  if (!this.user[user_id]){
    return ''
  }
  return this.user[user_id].name
}

}
