import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormVitalsListComponent } from './form-vitals-list.component';

describe('FormVitalsListComponent', () => {
  let component: FormVitalsListComponent;
  let fixture: ComponentFixture<FormVitalsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormVitalsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormVitalsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
