import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_VitalsDetail, Form_VitalsSummary } from '../form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { TCAuthorization } from '../tc/authorization';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { PatientPersist } from '../patients/patients.persist';
import { MedicationAdminstrationChartPersist } from '../medication_adminstration_chart/medication_adminstration_chart.persist';
import { ServiceType, temp_method, vital_sign, vital_sign_with_age } from '../app.enums';


@Component({
  selector: 'app-form-vitals-list',
  templateUrl: './form-vitals-list.component.html',
  styleUrls: ['./form-vitals-list.component.css']
})
export class FormVitalsListComponent implements OnInit {

    //form_vitalss

      vital_status_with_age ={
        0:{
          [vital_sign_with_age.pulse_rate]:{
            min:120,
            max:160
          },
          [vital_sign_with_age.respiratory_rate]:{
            min:40,
            max:60
          },
          [vital_sign_with_age.systolic_bp]:{
            min:50,
            max:70
          },
          [vital_sign_with_age.diastolic_bp]:{
            min:60,
            max:89
          }
        },
        0.01:{
          [vital_sign_with_age.pulse_rate]:{
            min:100,
            max:140
          },
          [vital_sign_with_age.respiratory_rate]:{
            min:30,
            max:50
          },
          [vital_sign_with_age.systolic_bp]:{
            min:70,
            max:100
          },
          [vital_sign_with_age.diastolic_bp]:{
            min:60,
            max:89
          }
        },
        1:{
          [vital_sign_with_age.pulse_rate]:{
            min:70,
            max:100
          },
          [vital_sign_with_age.respiratory_rate]:{
            min:20,
            max:40
          },
          [vital_sign_with_age.systolic_bp]:{
            min:80,
            max:110
          },
          [vital_sign_with_age.diastolic_bp]:{
            min:60,
            max:89
          }
        },
        5:{
          [vital_sign_with_age.pulse_rate]:{
            min:60,
            max:90
          },
          [vital_sign_with_age.respiratory_rate]:{
            min:16,
            max:24
          },
          [vital_sign_with_age.systolic_bp]:{
            min:90,
            max:129
          },
          [vital_sign_with_age.diastolic_bp]:{
            min:60,
            max:89
          }
        }
      }

      vital_status_without_age = {
        [vital_sign.temprature]:{
          min:35.5,
          max:37.5
        },
        [vital_sign.oxygen_saturation]:{
          min:95,
          max:100
        },
        [vital_sign.glascow_coma_scale]:{
          min:15,
          max:15
        },
        [vital_sign.bmi]:{
          min:18.5,
          max:25.5
        },
        [vital_sign.head_circumference]:{
          min:45,
          max:60
        }
      }
      
      vital_sign = vital_sign;
      vital_sign_with_age = vital_sign_with_age;

    form_vitalssData: Form_VitalsSummary[] = [];
    form_vitalssTotalCount: number = 0;
    form_vitalssSelectAll: boolean = false;
    form_vitalssSelected: Form_VitalsSummary[] = [];
    form_vitalssDisplayedColumns: string[] = [
      
    ];
    form_vitalssOutpatientDisplayedColumns: string[] = [
      'select',
      'action',
      'creation_time',
      'blood_pressure',
      'pulse',
      'oxygen_saturation',
      'temperature',
      'temp_method',
      'oxygen_saturation_spo2',
      'weight',
      'height',
      'BMI',
      'BSA',
      'head_circumference',
      'muac',
      'pain_score',
      'remark'
      
    ];
    form_vitalssInpatientDisplayedColumns: string[] = [
      'select',
      'action',
      'creation_time',
      'blood_pressure',
      'pulse',
      'oxygen_saturation',
      'temperature',
      'temp_method',
      'oxygen_saturation_spo2',
      'rbs',
      'input',
      'output',
      'pain_score',
      'gcs',
      'weight',
      'height',
      'BMI',
      'BSA',
      'head_circumference',
      'muac',
      'urine_ketone',
      'remark'
      
    ];
    @Input() showVitals: boolean = false;
    colorScheme = {
      domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'],
    };

    multi: any[];
    view: any[] = [900, 400];
  
    // options
    legend: boolean = true;
    showLabels: boolean = true;
    animations: boolean = true;
    xAxis: boolean = true;
    yAxis: boolean = true;
    showYAxisLabel: boolean = true;
    showXAxisLabel: boolean = true;
    xAxisLabel: string = 'Month';
    yAxisLabel: string = 'Value';
    timeline: boolean = true;
    age: number=0;
    ageInMonth:number=255;
  


  form_vitalssSearchTextBox: FormControl = new FormControl();
  form_vitalssLoading: boolean = false;
  @Input() edit: boolean=true;
  @Input() showBar:boolean = true;
  @Input() encounterId: any;
  @Input() patientType: number;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) formVitalsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) formVitalsSort: MatSort;
  @Input() self:boolean;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public patientPersist : PatientPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public medicationPersist: MedicationAdminstrationChartPersist,
  ) { 

        //form_vitalss filter
        this.form_vitalssSearchTextBox.setValue(
          form_encounterPersist.form_vitalsSearchText
        );
        this.form_vitalssSearchTextBox.valueChanges
          .pipe(debounceTime(500))
          .subscribe((value) => {
            this.form_encounterPersist.form_vitalsSearchText = value.trim();
            this.searchForm_Vitalss();
          });
  }

  ngOnInit(): void {
    this.getPatientAge();
    this.form_encounterPersist.self = this.self;
    if (this.patientType == ServiceType.outpatient){
      this.form_vitalssDisplayedColumns = this.form_vitalssOutpatientDisplayedColumns
    } else {
      this.form_vitalssDisplayedColumns = this.form_vitalssInpatientDisplayedColumns
    }
    this.tcAuthorization.canRead("form_vitals");
            // form_vitalss sorter
            this.formVitalsSort.sortChange.subscribe(() => {
              this.formVitalsPaginator.pageIndex = 0;
              this.searchForm_Vitalss(true);
            });
          // form_vitalss paginator
          this.formVitalsPaginator.page.subscribe(() => {
              this.searchForm_Vitalss(true);
            });
            this.searchForm_Vitalss();

  }

    //form_vitalss methods
    searchForm_Vitalss(isPagination: boolean = false): void {
      this.tcAuthorization.canRead("form_vitals");
      let paginator = this.formVitalsPaginator;
      let sorter = this.formVitalsSort
      this.form_vitalssSelected = [];
      this.form_vitalssLoading = true;
      this.form_encounterPersist
        .searchForm_Vitals(
          this.encounterId,
          paginator.pageSize,
          isPagination ? paginator.pageIndex : 0,
          sorter.active,
          sorter.direction
        )
        .subscribe(
          (response) => {
            this.form_vitalssData = response.data;
            if (response.total != -1) {
              this.form_vitalssTotalCount = response.total;
            }
              this.onResult.emit(response.total);
              if (this.form_vitalssData.length) {

                let titles = [
                  'blood_pressure_systolic',
                  'blood_pressure_diastolic',
                  'pulse',
                  'oxygen_saturation',
                  'oxygen_saturation_spo2',
                  'temperature',
                ]
  
                let multi = [];
                titles.forEach((element) => {
                  let multiObj = {
                    name: element,
                    series: [],
                  };
                  this.form_vitalssData.forEach((resel) => {
                    if (resel[element] != null && resel[element] != -1){
                    let obj = {
                      name: this.tcUtilsDate.toDate(resel.creation_time),
                      value: parseFloat(resel[element]),
                    };
                    multiObj.series.push(obj);}
                  });
                  multi.push(multiObj);
                });
                Object.assign(this, { multi });
              }
            console.log(this.multi)
            this.form_vitalssLoading = false;
          },
          (error) => {
            this.form_vitalssLoading = false;
          }
        );
    }
  getPatientAge():void{
  
    this.form_encounterPersist.getForm_Encounter(this.encounterId).subscribe((result)=>{
      this.patientPersist.getPatient(result.pid).subscribe((patient)=>{
       this.age = ((this.tcUtilsDate.toTimeStamp(new Date())- patient.dob) / (60*60*24*365));
       if(this.age<1){
        this.age = parseInt(((this.tcUtilsDate.toTimeStamp(new Date())- patient.dob)*12 / (60*60*24*365)).toString())/100
       }
      });
    });    
  }
    addForm_Vitals(): void {
      this.tcAuthorization.canCreate("form_vitals");
      let dialogRef = this.form_encounterNavigator.addForm_Vitals(
        this.encounterId,
        this.patientType
      );
      dialogRef.afterClosed().subscribe((newForm_Vitals) => {
        this.searchForm_Vitalss();
      });
    }
  
    editForm_Vitals(item: Form_VitalsDetail): void {
      this.tcAuthorization.canUpdate("form_vitals");
      let dialogRef = this.form_encounterNavigator.editForm_Vitals(
        this.encounterId,
        item.id,
        this.patientType,
      );
      dialogRef.afterClosed().subscribe((updatedForm_Vitals) => {
        if (updatedForm_Vitals) {
          this.searchForm_Vitalss();
        }
      });
    }
  
    deleteForm_Vitals(item: Form_VitalsDetail): void {
      this.tcAuthorization.canDelete("form_vitals");
      let dialogRef = this.tcNavigator.confirmDeletion('Form_Vitals');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist
            .deleteForm_Vitals(this.encounterId, item.id)
            .subscribe(
              (response) => {
                this.tcNotification.success('form_vitals deleted');
                this.searchForm_Vitalss();
              },
              (error) => {}
            );
        }
      });
    }
  
    downloadForm_Vitalss(): void {
      this.tcNotification.info(
        'Download form_vitalss : ' + this.form_vitalssSelected.length
      );
    }

    // btw( obj: {min: number, max: number}, num: number){
    //   return num > 38 ? "High Fever" : (num >= obj.min && num <=obj.max ? "Normal Temp" : ( num < obj.min ? "Low Temp" : "High Temp"));
    // }


    getMinAge(age:number):number {
      return age >= 5? 5 : (age > 1 ? 1: (age>0.01 ?  0.01 : 0 )); 
    }

    getVitalSignWithAge(vital_sign:number,value:number):boolean{
      return this.btwn(this.vital_status_with_age[this.getMinAge(this.age)][vital_sign].min, this.vital_status_with_age[this.getMinAge(this.age)][vital_sign].max , value)
    }

    getVitalSign(vital_sign:number,value:number):boolean {
      return this.btwn(this.vital_status_without_age[vital_sign].min, this.vital_status_without_age[vital_sign].max, value)
    }


    btwn(min:number,max:number, num:number):boolean{
      return num>=min&&num<=max;
    }

    getWeight(weight: number) {
      return weight? weight + ' KG':'';
    }
  

    getHeight(height: number) {
      return height ? height + ' CM' : '';
    }
  
    getCentigrade(f: number) {
      return f ? f + '°C' : f;
    }
    // getWeightStatus(bmi: number) {
    //   if (bmi < 18.5) {
    //     return 'Underweight';
    //   }
    //   if (bmi > 18.5 && bmi < 25) {
    //     return 'Normal';
    //   }
    //   if (bmi >= 25 && bmi < 30) {
    //     return 'Overweight';
    //   }
    //   if (bmi >= 30) {
    //     return 'Obese';
    //   }
    // }

    back(): void {
      this.location.back();
    }

    valueOrEmtyString(value: string | number, unit: string){
      if (value == null || value == -1){
        return ""
      }
      return value + " " + unit
    }


}
