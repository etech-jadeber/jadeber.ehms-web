import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClinicalInstructionDetailComponent } from './form-clinical-instruction-detail.component';

describe('FormClinicalInstructionDetailComponent', () => {
  let component: FormClinicalInstructionDetailComponent;
  let fixture: ComponentFixture<FormClinicalInstructionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormClinicalInstructionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClinicalInstructionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
