import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {FormClinicalInstructionDetail} from "../form_clinical_instruction.model";
import {FormClinicalInstructionPersist} from "../form_clinical_instruction.persist";
import {FormClinicalInstructionNavigator} from "../form_clinical_instruction.navigator";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { InstructionType } from 'src/app/app.enums';

export enum FormClinicalInstructionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-form_clinical_instruction-detail',
  templateUrl: './form-clinical-instruction-detail.component.html',
  styleUrls: ['./form-clinical-instruction-detail.component.scss']
})
export class FormClinicalInstructionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  formClinicalInstructionIsLoading:boolean = false;
  
  FormClinicalInstructionTabs: typeof FormClinicalInstructionTabs = FormClinicalInstructionTabs;
  activeTab: FormClinicalInstructionTabs = FormClinicalInstructionTabs.overview;
  instructionType = InstructionType;
  //basics
  formClinicalInstructionDetail: FormClinicalInstructionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public formClinicalInstructionNavigator: FormClinicalInstructionNavigator,
              public  formClinicalInstructionPersist: FormClinicalInstructionPersist,
              public userPersist: UserPersist,
              public formEncounterPersist: Form_EncounterPersist) {
    this.tcAuthorization.requireRead("form_clinical_instructions");
    this.formClinicalInstructionDetail = new FormClinicalInstructionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("form_clinical_instructions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.formClinicalInstructionIsLoading = true;
    this.formClinicalInstructionPersist.getFormClinicalInstruction(id).subscribe(formClinicalInstructionDetail => {
          this.formClinicalInstructionDetail = formClinicalInstructionDetail;
          this.formClinicalInstructionIsLoading = false;
          // this.userPersist.getUser(this.formClinicalInstructionDetail.nurse_id).subscribe(
          //   user => {
          //     this.formClinicalInstructionDetail.nurse_name = user.name
          //   }
          // )
        }, error => {
          console.error(error);
          this.formClinicalInstructionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.formClinicalInstructionDetail.id);
  }
  editFormClinicalInstruction(): void {
    let modalRef = this.formClinicalInstructionNavigator.editFormClinicalInstruction(this.formClinicalInstructionDetail.id);
    modalRef.afterClosed().subscribe(formClinicalInstructionDetail => {
      TCUtilsAngular.assign(this.formClinicalInstructionDetail, formClinicalInstructionDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.formClinicalInstructionPersist.print(this.formClinicalInstructionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print form_clinical_instruction", true);
      });
    }

  back():void{
      this.location.back();
    }

}
