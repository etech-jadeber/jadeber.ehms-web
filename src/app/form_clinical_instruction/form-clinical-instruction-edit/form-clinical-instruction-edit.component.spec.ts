import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormClinicalInstructionEditComponent } from './form-clinical-instruction-edit.component';

describe('FormClinicalInstructionEditComponent', () => {
  let component: FormClinicalInstructionEditComponent;
  let fixture: ComponentFixture<FormClinicalInstructionEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormClinicalInstructionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClinicalInstructionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
