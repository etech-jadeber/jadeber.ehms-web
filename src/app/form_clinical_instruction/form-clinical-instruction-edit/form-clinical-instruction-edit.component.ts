import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCEnum, TCIdMode, TCModalModes } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { FormClinicalInstructionDetail } from '../form_clinical_instruction.model'; import { FormClinicalInstructionPersist } from '../form_clinical_instruction.persist'; import { FormControl } from '@angular/forms';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { InstructionSubType, InstructionType, lab_order_type, physican_type, prescription_type } from 'src/app/app.enums';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { ItemPersist } from 'src/app/items/item.persist';
import { ProcedureDetail } from 'src/app/procedures/procedure.model';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';
import { Physiotherapy_TypeDetail } from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { LabOrdersListComponent } from 'src/app/lab-orders-list/lab-orders-list.component';
import { PrescriptionsDetailForList, PrescriptionsEditComponent, PrescriptionsEditListComponent, frequency_value } from 'src/app/form_encounters/prescriptions-edit/prescriptions-edit.component';
import { PrescriptionsDetail } from 'src/app/form_encounters/form_encounter.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';


@Component({
  selector: 'app-form_clinical_instruction-edit',
  templateUrl: './form-clinical-instruction-edit.component.html',
  styleUrls: ['./form-clinical-instruction-edit.component.css']
}) export class FormClinicalInstructionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  orderType: TCEnum[] = [];
  title: string;
  from_date: FormControl = new FormControl({ disabled: true, value: new Date() });
  to_date: FormControl = new FormControl({ disabled: true, value: new Date() })
  nurseName: string;
  formClinicalInstructionDetail: FormClinicalInstructionDetail;
  treatmentDisplayedColumns : string[] = ["name", "from_date", "to_date", "interval", "action"]
  treatmentsData: FormClinicalInstructionDetail[] = [];
  InstructionType = InstructionType
  prescription_type = prescription_type;
  hr: number;
  min: number;
  procedureName: string;
  physiotherpayName: string;
  procedure_options: ProcedureDetail[] = [];
  physiotherapy_options: Physiotherapy_TypeDetail[] = [];
  lab_order_type: number[] = [];
  investigations = [InstructionType.lab_order, InstructionType.rad_order, InstructionType.pat_order];
  @ViewChild( 'investigation', { static: false }) investigation: LabOrdersListComponent;
  @ViewChild( 'prescription', { static: false }) prescription: PrescriptionsEditListComponent;

  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<FormClinicalInstructionEditComponent>,
    public persist: FormClinicalInstructionPersist,
    public formEnconterPersist: Form_EncounterPersist,
    public tcUtilsDate: TCUtilsDate,
    public doctorNavigator: DoctorNavigator,
    public tcUtilsArray: TCUtilsArray,
    public itemPersist: ItemPersist,
    public labOrderPersist: Lab_OrderPersist,
    public procedurePersist: ProcedurePersist,
    public physiotherapyPersist: Physiotherapy_TypePersist,
    @Inject(MAT_DIALOG_DATA) public idMode: {id: string, mode: string, patient_id: string}) {
      this.orderType = [...this.persist.InstructionType]
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }


  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.isNew()|| this.isWizard()) {
      this.tcAuthorization.requireCreate("form_clinical_instructions");
      this.title = this.appTranslation.getText("general", "new") + " " + "form_clinical_instruction";
      this.formClinicalInstructionDetail = new FormClinicalInstructionDetail();
      this.formClinicalInstructionDetail.encounter_id = this.idMode.id;
      this.formClinicalInstructionDetail.patient_id = this.idMode.patient_id;
      this.formClinicalInstructionDetail.from_date = new Date().setMilliseconds(0) / 1000;
      this.formClinicalInstructionDetail.to_date = new Date().setMilliseconds(0) / 1000;
      //set necessary defaults
      this.formClinicalInstructionDetail.treatment = ""
    } else {
      this.tcAuthorization.requireUpdate("form_clinical_instructions");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "form_clinical_instruction";
      this.isLoadingResults = true;
      this.persist.getFormClinicalInstruction(this.idMode.id).subscribe(formClinicalInstructionDetail => {
        this.formClinicalInstructionDetail = formClinicalInstructionDetail;
        this.formEnconterPersist.getPrescriptions(this.formClinicalInstructionDetail.target_id).subscribe((prescription)=>{
          this.prescription.hr = this.formClinicalInstructionDetail.interval;
          this.prescription.prescriptionsDetailForList = prescription;
        })
        this.from_date.setValue(this.tcUtilsDate.toDate(this.formClinicalInstructionDetail.from_date))
        this.to_date.setValue(this.tcUtilsDate.toDate(this.formClinicalInstructionDetail.to_date))
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  updateProcedure(event:string,input):void{
    this.procedurePersist.procedureSearchText = input.value;
    this.loadProcedure();
  }

  loadProcedure(){
    this.procedurePersist.searchProcedure(5,0,"name","asc").subscribe((result)=>{
      if(result)
        this.procedure_options = result.data;
    })
  }

  procedureSelected(event){
    this.procedureName = event.name
    this.formClinicalInstructionDetail.target_name = event.name;
    this.formClinicalInstructionDetail.temp_id = event.id;
  }

  updatePhysiotherapy(event:string,input):void{
    this.procedurePersist.procedureSearchText = input.value;
    this.loadPhysiotherapy();
  }

  loadPhysiotherapy(){
    this.physiotherapyPersist.searchPhysiotherapy_Type(5,0,"name","asc").subscribe((result)=>{
      if(result)
        this.physiotherapy_options = result.data;
    })
  }

  physiotherapySelected(event){
    this.procedureName = event.name
    this.formClinicalInstructionDetail.target_id = event.id;
  }

  ngAfterViewInit(){
    this.from_date.valueChanges.pipe().subscribe(value => {
      this.formClinicalInstructionDetail.from_date = this.tcUtilsDate.toTimeStamp(value._d)
    });

    this.to_date.valueChanges.pipe().subscribe(value => {
      this.formClinicalInstructionDetail.to_date = this.tcUtilsDate.toTimeStamp(value._d)
    });
  }

  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addFormClinicalInstruction(this.formClinicalInstructionDetail).subscribe(value => {
      this.tcNotification.success("formClinicalInstruction added");
      this.formClinicalInstructionDetail.id = value.id;
      this.dialogRef.close(this.formClinicalInstructionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onReturn():void {
    this.dialogRef.close(this.formClinicalInstructionDetail)
  }
  

  editTreatmentFromList(id: string){
    this.formClinicalInstructionDetail = {...this.tcUtilsArray.getById(this.treatmentsData, id)}
    this.hr = Math.floor(this.formClinicalInstructionDetail.interval / 60)
    this.min = this.formClinicalInstructionDetail.interval % 60
    this.removeTreatmentFromList(id)
  }

  removeTreatmentFromList(element: any){

    this.treatmentsData = this.treatmentsData.filter(treatment => treatment!= element)

  }

  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    // this.formClinicalInstructionDetail.prescription = this.prescription.getDetail();
    // this.formClinicalInstructionDetail.target_name = this.formClinicalInstructionDetail.prescription.drug;
    // this.formClinicalInstructionDetail.instruction = this.formClinicalInstructionDetail.prescription.strength +"-" + this.tcUtilsArray.getEnum(this.formEnconterPersist.frequency , this.formClinicalInstructionDetail.prescription.frequency)?.name + " for " + this.formClinicalInstructionDetail.prescription.duration + this.tcUtilsArray.getEnum(this.formEnconterPersist.frequencyList,this.formClinicalInstructionDetail.prescription.duration_unit)?.name + "-" + this.tcUtilsArray.getEnum(this.prescription.medicationAdminstrationChartPersist.MedicationRoute, this.formClinicalInstructionDetail.prescription.route)?.name + " -  " +this.formClinicalInstructionDetail.prescription.other_information; 
    this.persist.updateFormClinicalInstruction(this.formClinicalInstructionDetail).subscribe(value => {
      this.tcNotification.success("form_clinical_instruction updated");
      this.dialogRef.close(this.formClinicalInstructionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
  canSubmit(): boolean {
    if (this.formClinicalInstructionDetail == null) {
      return false;
    }

    if (this.formClinicalInstructionDetail.from_date == null) {
      return false;
    }

    if (this.formClinicalInstructionDetail.instruction == null) {
      return false;
    }

    // if (this.formClinicalInstructionDetail.type == InstructionType.medication){
    //   this.hr = this.prescription?.hr;
    //   return this.prescription?.canSubmit();
    // }
    
    // if (this.formClinicalInstructionDetail.instruction == null || this.formClinicalInstructionDetail.instruction == "") {
    //   return false;
    // }
    // if (this.hr != null && (this.hr < 0 || this.hr > 24)) {
    //   if (this.min == null || this.min < 0 || this.min > 60) {
    //     return false;
    //   }
    // }
    // if (this.formClinicalInstructionDetail.nurse_id == null || this.formClinicalInstructionDetail.nurse_id == "") {
    //   return false;
    // }
    // if (this.formClinicalInstructionDetail.patient_id == null || this.formClinicalInstructionDetail.patient_id == "") {
    //   return false;
    // }
    // if (this.formClinicalInstructionDetail.to_date == null) {
    //   return false;
    // }
    // if (this.formClinicalInstructionDetail.type == null) {
    //   return false;
    // }
    // if (this.investigations.find((value) =>  value == this.formClinicalInstructionDetail.type)){
    //   return this.investigation.canSubmit();
    // }
    return true;
  }

  getOrderType(){
    switch (this.formClinicalInstructionDetail.type) {
      case InstructionType.lab_order:
        this.lab_order_type = [lab_order_type.lab]
        break;
      case InstructionType.pat_order:
        this.lab_order_type = [lab_order_type.pathology]
        break;
      case InstructionType.rad_order:
        this.lab_order_type = [lab_order_type.imaging]
        break;
      default:
        this.lab_order_type  = [];
        break;
    }
  }

  handleType() {
    this.getOrderType()
    if (this.lab_order_type.length){
      this.investigation.reload(this.lab_order_type)
    }
  }

  getInterval(interval: number){
    return `${Math.floor(interval / 60) }:${interval % 60}`
  }

  searchNurse() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.nurse);
    dialogRef.afterClosed().subscribe((result: DoctorDetail[]) => {
      if (result) {
        this.nurseName = result[0].first_name + " " + result[0].middle_name + " " + result[0].last_name
        this.formClinicalInstructionDetail.nurse_id = result[0].login_id
      }
    })
  }




  addToList(){
    this.formClinicalInstructionDetail.sub_list = [];
    this.formClinicalInstructionDetail.interval = (this.hr || 0) * 60 + (this.min || 0)
    if (this.formClinicalInstructionDetail.type == InstructionType.vital){
      this.formClinicalInstructionDetail.target_name = this.tcUtilsArray.getEnum(this.orderType, InstructionType.vital)?.name
    }
    else if (this.formClinicalInstructionDetail.type == InstructionType.treatment){
      this.formClinicalInstructionDetail.target_name = "treatment";
    }
    else if (this.formClinicalInstructionDetail.type == InstructionType.medication){
      this.formClinicalInstructionDetail.prescription = this.prescription.getDetail()
      this.formClinicalInstructionDetail.target_name = this.formClinicalInstructionDetail.prescription.drug
      
      this.formClinicalInstructionDetail.instruction = this.formClinicalInstructionDetail.prescription.strength +"-" + this.tcUtilsArray.getEnum(this.formEnconterPersist.frequency , this.formClinicalInstructionDetail.prescription.frequency)?.name + " for " + this.formClinicalInstructionDetail.prescription.duration + this.tcUtilsArray.getEnum(this.formEnconterPersist.frequencyList,this.formClinicalInstructionDetail.prescription.duration_unit)?.name + "-" + this.tcUtilsArray.getEnum(this.prescription.medicationAdminstrationChartPersist.MedicationRoute, this.formClinicalInstructionDetail.prescription.route)?.name+ "  - " +this.formClinicalInstructionDetail.prescription.other_information;

      this.prescription.resetData()
    }
    else if (this.lab_order_type.length){
      const orders = this.investigation.getAllOrder()
      this.formClinicalInstructionDetail.target_name =this.tcUtilsArray.getEnum(this.labOrderPersist.lab_order_type, this.lab_order_type[0])?.name + " order";
      for (let order in orders){
        const labTests = orders[order].selectedLabTest
        for (let labTest of labTests){
          this.formClinicalInstructionDetail.sub_list = [...this.formClinicalInstructionDetail.sub_list , {...this.formClinicalInstructionDetail, temp_id: labTest.id, target_name: labTest.name, sub_type: InstructionSubType.test}]
        }
        const labPanels = orders[order].selectedLabPanel
        for (let labPanel of labPanels){
          this.formClinicalInstructionDetail.sub_list = [...this.formClinicalInstructionDetail.sub_list ,{...this.formClinicalInstructionDetail, temp_id: labPanel.id, target_name: labPanel.name, sub_type: InstructionSubType.panel}]
        }

      }
      this.investigation.reset()
    }
    this.treatmentsData = [...this.treatmentsData , {...this.formClinicalInstructionDetail}]
    this.formClinicalInstructionDetail.instruction = ""
    this.formClinicalInstructionDetail.temp_id = null;
    this.formClinicalInstructionDetail.target_id = null;
    this.formClinicalInstructionDetail.target_name = "";
  }
}
