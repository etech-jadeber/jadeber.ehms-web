import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClinicalInstructionListComponent } from './form-clinical-instruction-list.component';

describe('FormClinicalInstructionListComponent', () => {
  let component: FormClinicalInstructionListComponent;
  let fixture: ComponentFixture<FormClinicalInstructionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormClinicalInstructionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClinicalInstructionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
