import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FormClinicalInstructionDetail, FormClinicalInstructionSummary, FormClinicalInstructionSummaryPartialList } from '../form_clinical_instruction.model';
import { FormClinicalInstructionPersist } from '../form_clinical_instruction.persist';
import { FormClinicalInstructionNavigator } from '../form_clinical_instruction.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { encounter_status, InstructionStatus, InstructionType, prescription_type, ServiceType, SubmissionStatus } from 'src/app/app.enums';
import { PrescriptionDrugNavigator } from 'src/app/form_encounters/prescriptions-edit/prescription-drug-navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-form_clinical_instruction-list',
  templateUrl: './form-clinical-instruction-list.component.html',
  styleUrls: ['./form-clinical-instruction-list.component.css']
})

export class FormClinicalInstructionListComponent implements OnInit {
  formClinicalInstructionsData: FormClinicalInstructionSummary[] = [];
  formClinicalInstructionsTotalCount: number = 0;
  formClinicalInstructionSelectAll:boolean = false;
  showVitals:boolean = false;
  showChart:boolean = false;
  serviceType = ServiceType;
  submissionStatus = SubmissionStatus;
  formClinicalInstructionSelection: FormClinicalInstructionSummary[] = [];

//  formClinicalInstructionsDisplayedColumns: string[] = ["select","action", "name" ,"type","from_date","to_date","interval","status","instruction" ];
 formClinicalInstructionsDisplayedColumns: string[] = ["select","action","instruction", "from_date","user_id" ];
  formClinicalInstructionSearchTextBox: FormControl = new FormControl();
  formClinicalInstructionIsLoading: boolean = false;
  instructionType = InstructionType;
  instructionStatus = InstructionStatus;
  nurses: {[id: string]: UserDetail } = {}
  @Input() encounterId: string;
  @Input() showToolBar: boolean = true;
  @Input() matExpansion:boolean = false;
  @Input() patientId: string
  @Input() type:number;
  @Input() target_id:string;
  @Input() is_sub:boolean = false;
  @ViewChild(MatPaginator, {static: true}) formClinicalInstructionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) formClinicalInstructionsSort: MatSort;
  user: {[id: string] : UserDetail} = {}

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public formClinicalInstructionPersist: FormClinicalInstructionPersist,
                public formClinicalInstructionNavigator: FormClinicalInstructionNavigator,
                public formEncounterPersist: Form_EncounterPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public userPersist: UserPersist,
                public prescriptionNavigator: Form_EncounterNavigator,

    ) {

        this.tcAuthorization.requireRead("form_clinical_instructions");
       this.formClinicalInstructionSearchTextBox.setValue(formClinicalInstructionPersist.formClinicalInstructionSearchText);
      //delay subsequent keyup events
      this.formClinicalInstructionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.formClinicalInstructionPersist.formClinicalInstructionSearchText = value;
        this.searchFormClinicalInstructions();
      });
      
    }

    ngOnInit(): void {
      this.formClinicalInstructionPersist.encounterId = this.encounterId
    this.formClinicalInstructionPersist.patientId = this.patientId
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.formClinicalInstructionPersist.encounterId = this.encounterId;
      } else {
        this.formClinicalInstructionPersist.encounterId = null;
      }
      this.searchFormClinicalInstructions()
    }


    ngAfterViewInit() {
      this.formClinicalInstructionsSort.active = "from_date";
      this.formClinicalInstructionsSort.direction = "desc";
      this.formClinicalInstructionsSort.sortChange.subscribe(() => {
        this.formClinicalInstructionsPaginator.pageIndex = 0;
        this.searchFormClinicalInstructions(true);
      });

      this.formClinicalInstructionsPaginator.page.subscribe(() => {
        this.searchFormClinicalInstructions(true);
      });
      //start by loading items
      this.searchFormClinicalInstructions();
    }

  searchFormClinicalInstructions(isPagination:boolean = false): void {


    let paginator = this.formClinicalInstructionsPaginator;
    let sorter = this.formClinicalInstructionsSort;

    this.formClinicalInstructionIsLoading = true;
    this.formClinicalInstructionSelection = [];

    if(this.formEncounterPersist.encounter_is_active == null){
      this.formEncounterPersist.getForm_Encounter(this.encounterId).subscribe((encounter)=>{
        if(encounter){
          this.formEncounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
        }
      })
    }
    this.formClinicalInstructionPersist.type = this.type;
    this.formClinicalInstructionPersist.target_id = this.target_id;
    this.formClinicalInstructionPersist.is_sub = this.is_sub;
    this.formClinicalInstructionPersist.searchFormClinicalInstruction( paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FormClinicalInstructionSummaryPartialList) => {
      this.formClinicalInstructionsData = partialList.data;
      this.formClinicalInstructionsData.forEach(
        instruction => {
          if (instruction.user_id && !this.user[instruction.user_id]){
            this.user[instruction.user_id] = new UserDetail()
            this.userPersist.getUser(instruction.user_id).subscribe(
              user => {
                this.user[instruction.user_id] = user;
              }
            )
          }
        }
      )
      if (partialList.total != -1) {
        this.formClinicalInstructionsTotalCount = partialList.total;
      }
      this.formClinicalInstructionIsLoading = false;
    }, error => {
      this.formClinicalInstructionIsLoading = false;
    });

  }
  toggle():void{
    this.showChart = false;
  }

  sendAll(){
    this.formClinicalInstructionPersist.formClinicalInstructionsDo("send", {data: this.groupByType()}).subscribe(
      value => {
        console.log(value)
      }
    )
  }

  replaceInstruction(instruction: string): string{

    return instruction.replace(/\n/g, '<br />' )
  }


  
  addPrescriptions(): void {
    let dialogRef = this.prescriptionNavigator.addPrescriptions(
      this.encounterId,
      false,
      prescription_type.nurse_prescription
    );
    dialogRef.afterClosed().subscribe((newPrescriptions) => {
      if (newPrescriptions) {
      }
    });
  }

  order(order:FormClinicalInstructionDetail):void{


    if(order.type == InstructionType.medication){
      this.formEncounterPersist.getPrescriptions(order.target_id).subscribe((prescription)=>{
          if(prescription){
            let dialogRef = this.tcNavigator.numberInput("Quantity",prescription.quantity)
            dialogRef.afterClosed().subscribe((result)=>{
              if(result){
                if(prescription.quantity != result){
                  prescription.quantity = result
                  this.formEncounterPersist.updatePrescriptions(prescription).subscribe(val =>{
                    this.orderClinicalInstruction(order);
                  })
                }else{
                  this.orderClinicalInstruction(order);
                }
              }
            })
          }
        })
    }else{
      this.orderClinicalInstruction(order);
    }




   
   
  }

  orderClinicalInstruction(order: FormClinicalInstructionDetail):void{
    this.formClinicalInstructionPersist.formClinicalInstructionDo(order.id,"order", {store_type:100,}).subscribe(
      (value:any) => {
        if(value){
          if(value.status = 100){
            let dialogRef = this.tcNavigator.confirmAction("confirm","order", value.message +" Are you sure to reorder?","Cancel");
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.formClinicalInstructionPersist.formClinicalInstructionDo(order.id,"order", {reorder:true,}).subscribe(value=>{
                  this.tcNotification.success("successfully ordered");
                  this.searchFormClinicalInstructions()
                })
              }
            });
          }
        }else{
          this.tcNotification.success("successfully ordered");
          this.searchFormClinicalInstructions()
        }
      }
    )
  }

  groupByType(){
    const groupedList = {}
    for (let selection of this.formClinicalInstructionSelection){
      if (groupedList[selection.type]){
        groupedList[selection.type].push(selection)
      } else {
        groupedList[selection.type] = [selection]
      }
    }
    return groupedList  
  }

  closeOrder(item: FormClinicalInstructionSummary): void {
    this.formClinicalInstructionPersist.formClinicalInstructionDo(item.id, "stop_order",{}).subscribe((value)=>{
      if(value){
        this.tcNotification.success("The order is closed successfully");
      }
      this.searchFormClinicalInstructions();
    })
  }

  downloadFormClinicalInstructions(): void {
    if(this.formClinicalInstructionSelectAll){
         this.formClinicalInstructionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download form_clinical_instruction", true);
      });
    }
    else{
        this.formClinicalInstructionPersist.download(this.tcUtilsArray.idsList(this.formClinicalInstructionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download form_clinical_instruction",true);
            });
        }
  }
addFormClinicalInstruction(): void {
    let dialogRef = this.formClinicalInstructionNavigator.addFormClinicalInstruction(this.encounterId, this.patientId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFormClinicalInstructions();
      }
    });
  }

  editFormClinicalInstruction(item: FormClinicalInstructionSummary) {
    let dialogRef = this.formClinicalInstructionNavigator.editFormClinicalInstruction(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  getInterval(interval: number){
    return `${Math.floor(interval / 60) }:${interval % 60}`
  }
  deleteFormClinicalInstruction(item: FormClinicalInstructionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("form_clinical_instruction");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.formClinicalInstructionPersist.deleteFormClinicalInstruction(item.id).subscribe(response => {
          this.tcNotification.success("form_clinical_instruction deleted");
          this.searchFormClinicalInstructions();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

  getNurse(id: string){
    return this.nurses[id]?.name ? `${this.nurses[id].name}` : ""
  }

  prescribePrescription(detail: FormClinicalInstructionDetail){
    let dialogRef = this.prescriptionNavigator.wizardPrescription(detail.encounter_id)
    dialogRef.afterClosed().subscribe(data => {
      if(data){
        this.formClinicalInstructionIsLoading = true;
        this.formClinicalInstructionPersist.formClinicalInstructionDo(detail.id, "prescribe", data).subscribe(
          (res) => {
            this.tcNotification.success("The treatment is successfully prescribed")
            this.formClinicalInstructionIsLoading = false;
            this.searchFormClinicalInstructions()
          }
          , (error) => {
            this.formClinicalInstructionIsLoading = false;
          }
        )
      }

    })
  }
}
