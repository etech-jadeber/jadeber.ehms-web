import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormClinicalInstructionPickComponent } from './form-clinical-instruction-pick.component';

describe('FormClinicalInstructionPickComponent', () => {
  let component: FormClinicalInstructionPickComponent;
  let fixture: ComponentFixture<FormClinicalInstructionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormClinicalInstructionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormClinicalInstructionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
