import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { FormClinicalInstructionSummary, FormClinicalInstructionSummaryPartialList } from '../form_clinical_instruction.model';
import { FormClinicalInstructionPersist } from '../form_clinical_instruction.persist';
import { FormClinicalInstructionNavigator } from '../form_clinical_instruction.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-form_clinical_instruction-pick',
  templateUrl: './form-clinical-instruction-pick.component.html',
  styleUrls: ['./form-clinical-instruction-pick.component.scss']
})export class FormClinicalInstructionPickComponent implements OnInit {
  formClinicalInstructionsData: FormClinicalInstructionSummary[] = [];
  formClinicalInstructionsTotalCount: number = 0;
  formClinicalInstructionSelectAll:boolean = false;
  formClinicalInstructionSelection: FormClinicalInstructionSummary[] = [];

 formClinicalInstructionsDisplayedColumns: string[] = ["select","from_date","instruction","interval","nurse_id","patient_id","status","to_date","type" ];
  formClinicalInstructionSearchTextBox: FormControl = new FormControl();
  formClinicalInstructionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) formClinicalInstructionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) formClinicalInstructionsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public formClinicalInstructionPersist: FormClinicalInstructionPersist,
                public formClinicalInstructionNavigator: FormClinicalInstructionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
 public dialogRef: MatDialogRef<FormClinicalInstructionPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("form_clinical_instructions");
       this.formClinicalInstructionSearchTextBox.setValue(formClinicalInstructionPersist.formClinicalInstructionSearchText);
      //delay subsequent keyup events
      this.formClinicalInstructionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.formClinicalInstructionPersist.formClinicalInstructionSearchText = value;
        this.searchForm_clinical_instructions();
      });
    } ngOnInit() {
   
      this.formClinicalInstructionsSort.sortChange.subscribe(() => {
        this.formClinicalInstructionsPaginator.pageIndex = 0;
        this.searchForm_clinical_instructions(true);
      });

      this.formClinicalInstructionsPaginator.page.subscribe(() => {
        this.searchForm_clinical_instructions(true);
      });
      //start by loading items
      this.searchForm_clinical_instructions();
    }

  searchForm_clinical_instructions(isPagination:boolean = false): void {


    let paginator = this.formClinicalInstructionsPaginator;
    let sorter = this.formClinicalInstructionsSort;

    this.formClinicalInstructionIsLoading = true;
    this.formClinicalInstructionSelection = [];

    this.formClinicalInstructionPersist.searchFormClinicalInstruction( paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: FormClinicalInstructionSummaryPartialList) => {
      this.formClinicalInstructionsData = partialList.data;
      if (partialList.total != -1) {
        this.formClinicalInstructionsTotalCount = partialList.total;
      }
      this.formClinicalInstructionIsLoading = false;
    }, error => {
      this.formClinicalInstructionIsLoading = false;
    });

  }
  markOneItem(item: FormClinicalInstructionSummary) {
    if(!this.tcUtilsArray.containsId(this.formClinicalInstructionSelection,item.id)){
          this.formClinicalInstructionSelection = [];
          this.formClinicalInstructionSelection.push(item);
        }
        else{
          this.formClinicalInstructionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.formClinicalInstructionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
