import { PrescriptionsDetailForList } from "../form_encounters/prescriptions-edit/prescriptions-edit.component";
import {TCId} from "../tc/models";export class FormClinicalInstructionSummary extends TCId {
    from_date:number;
    instruction:string;
    interval:number;
    nurse_id:string;
    user_id:string;
    patient_id:string;
    status:number;
    to_date:number;
    type:number;
    nurse_name: string
    encounter_id:string;
    treatment: string;
    target_id: string;
    prescription: PrescriptionsDetailForList
    target_name: string;
    sub_type: number;
    sub:boolean;
    submission_status: number;
    temp_id: string;

    }
    export class FormClinicalInstructionSummaryPartialList {
      data: FormClinicalInstructionSummary[];
      total: number;
    }
    export class FormClinicalInstructionDetail extends FormClinicalInstructionSummary {
      sub_list: FormClinicalInstructionSummary[];
    }

    export class FormClinicalInstructionDashboard {
      total: number;
    }
