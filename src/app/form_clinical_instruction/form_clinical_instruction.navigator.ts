import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {FormClinicalInstructionEditComponent} from "./form-clinical-instruction-edit/form-clinical-instruction-edit.component";
import {FormClinicalInstructionPickComponent} from "./form-clinical-instruction-pick/form-clinical-instruction-pick.component";


@Injectable({
  providedIn: 'root'
})

export class FormClinicalInstructionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  formClinicalInstructionsUrl(): string {
    return "/form_clinical_instructions";
  }

  formClinicalInstructionUrl(id: string): string {
    return "/form_clinical_instructions/" + id;
  }

  viewFormClinicalInstructions(): void {
    this.router.navigateByUrl(this.formClinicalInstructionsUrl());
  }

  viewFormClinicalInstruction(id: string): void {
    this.router.navigateByUrl(this.formClinicalInstructionUrl(id));
  }

  editFormClinicalInstruction(id: string): MatDialogRef<FormClinicalInstructionEditComponent> {
    const dialogRef = this.dialog.open(FormClinicalInstructionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addFormClinicalInstruction(encounter_id:string, patient_id: string): MatDialogRef<FormClinicalInstructionEditComponent> {
    const dialogRef = this.dialog.open(FormClinicalInstructionEditComponent, {
          data:  {id: encounter_id, patient_id: patient_id, mode: TCModalModes.NEW},
          width: TCModalWidths.large
    });
    return dialogRef;
  }

  addFormClinicalInstructionWizard(encounter_id:string, patient_id: string): MatDialogRef<FormClinicalInstructionEditComponent> {
    const dialogRef = this.dialog.open(FormClinicalInstructionEditComponent, {
          data:  {id: encounter_id, patient_id: patient_id, mode: TCModalModes.WIZARD},
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickFormClinicalInstructions(selectOne: boolean=false): MatDialogRef<FormClinicalInstructionPickComponent> {
      const dialogRef = this.dialog.open(FormClinicalInstructionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
