import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {FormClinicalInstructionDashboard, FormClinicalInstructionDetail, FormClinicalInstructionSummaryPartialList} from "./form_clinical_instruction.model";
import { InstructionStatus, InstructionType } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class FormClinicalInstructionPersist {
    instructionTypeFilter: number ;
    encounterId: string;
    patientId: string;

    InstructionType: TCEnum[] = [
     new TCEnum( InstructionType.vital, 'Vital Sign'),
     new TCEnum( InstructionType.medication, 'Medication'),
     new TCEnum( InstructionType.lab_order, 'Laboratory Order'),
     new TCEnum( InstructionType.pat_order, 'Pathology Order'),
     new TCEnum( InstructionType.rad_order, 'Radiology Order'),
     new TCEnum( InstructionType.patient_procedure, 'Patient Procedure'),
     new TCEnum( InstructionType.physiotherapy, 'Physiotherapy'),
     new TCEnum( InstructionType.oxygentherapy, 'Oxygentherapy'),
  // new TCEnum( InstructionType.observation, 'Observation'),
  new TCEnum( InstructionType.treatment, 'Treatment'),

  ];

  instructionStatusFilter: number ;

    InstructionStatus: TCEnum[] = [
     new TCEnum( InstructionStatus.open, 'Open'),
  new TCEnum( InstructionStatus.start, 'Start'),
  new TCEnum( InstructionStatus.close, 'Close'),

  ];


 formClinicalInstructionSearchText: string = "";
 type: number;
  target_id: string;
  is_sub: boolean;

 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.formClinicalInstructionSearchText;
    //add custom filters
    return fltrs;
  }

  searchFormClinicalInstruction( pageSize: number, pageIndex: number, sort: string, order: string,): Observable<FormClinicalInstructionSummaryPartialList> {
    
    let url = TCUtilsHttp.buildSearchUrl("form_clinical_instruction", this.formClinicalInstructionSearchText ,pageSize, pageIndex, sort, order);
    if (this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    }
    if (this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
    }
    if(this.type)
      url = TCUtilsString.appendUrlParameter(url, 'type', this.type.toString())
    
    if(this.is_sub)
      url = TCUtilsString.appendUrlParameter(url, 'is_sub', this.is_sub.toString())
    if(this.target_id)
      url = TCUtilsString.appendUrlParameter(url, 'target_id', this.target_id)
    return this.http.get<FormClinicalInstructionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_clinical_instruction/do", new TCDoParam("download_all", this.filters()));
  }

  formClinicalInstructionDashboard(): Observable<FormClinicalInstructionDashboard> {
    return this.http.get<FormClinicalInstructionDashboard>(environment.tcApiBaseUri + "form_clinical_instruction/dashboard");
  }

  getFormClinicalInstruction(id: string): Observable<FormClinicalInstructionDetail> {
    return this.http.get<FormClinicalInstructionDetail>(environment.tcApiBaseUri + "form_clinical_instruction/" + id);
  }

  addFormClinicalInstruction(data : FormClinicalInstructionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_clinical_instruction/", data);
  }

  updateFormClinicalInstruction(item: FormClinicalInstructionDetail): Observable<FormClinicalInstructionDetail> {
    return this.http.patch<FormClinicalInstructionDetail>(environment.tcApiBaseUri + "form_clinical_instruction/" + item.id, item);
  }

  deleteFormClinicalInstruction(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "form_clinical_instruction/" + id);
  }
 formClinicalInstructionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_clinical_instruction/do", new TCDoParam(method, payload));
  }

  formClinicalInstructionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_clinical_instruction/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "form_clinical_instruction/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "form_clinical_instruction/" + id + "/do", new TCDoParam("print", {}));
  }


}
