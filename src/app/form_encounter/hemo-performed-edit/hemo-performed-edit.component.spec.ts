import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemoPerformedEditComponent } from './hemo-performed-edit.component';

describe('HemoPerformedEditComponent', () => {
  let component: HemoPerformedEditComponent;
  let fixture: ComponentFixture<HemoPerformedEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemoPerformedEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemoPerformedEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
