import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HemoPerformedDetail } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { tr } from 'date-fns/locale';
import { DialysisMachineNavigator } from 'src/app/dialysis_machine/dialysis_machine.navigator';
import { DialysisMachineSummary } from 'src/app/dialysis_machine/dialysis_machine.model';


@Component({
  selector: 'app-hemo_performed-edit',
  templateUrl: './hemo-performed-edit.component.html',
  styleUrls: ['./hemo-performed-edit.component.scss']
})export class HemoPerformedEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  hemoPerformedDetail: HemoPerformedDetail;
  machineId:string ="";
  end_time:string = "";
 start_time:string = "";
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<HemoPerformedEditComponent>,
              public  persist: Form_EncounterPersist,
              public dialysisMachineNav:DialysisMachineNavigator,
              public tcUtilsDate: TCUtilsDate,
              private tCUtilsDate:TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }



  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }
  
  ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("hemo_performed");
      this.title = this.appTranslation.getText("general","new") +  " " + "Hemo Performed";
      this.hemoPerformedDetail = new HemoPerformedDetail();
           this.hemoPerformedDetail.patient_id = this.idMode.parentId;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("hemo_performed");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "hemo_performed";
      this.isLoadingResults = true;
      this.persist.getHemoPerformed(this.idMode.parentId, this.idMode.childId).subscribe(hemoPerformedDetail => {
        this.hemoPerformedDetail = hemoPerformedDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addHemoPerformed(this.idMode.parentId,this.hemoPerformedDetail).subscribe(value => {
      this.tcNotification.success("hemoPerformed added");
      this.hemoPerformedDetail.id = value.id;
      this.dialogRef.close(this.hemoPerformedDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateHemoPerformed(this.idMode.parentId,this.hemoPerformedDetail).subscribe(value => {
      this.tcNotification.success("hemo_performed updated");
      this.dialogRef.close(this.hemoPerformedDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.hemoPerformedDetail == null){
            return false;
          }

if (this.hemoPerformedDetail.patient_id == null || this.hemoPerformedDetail.patient_id  == "") {
            return false;
        }
if (this.hemoPerformedDetail.sub_type == null ) {
            return false;
        }
if (this.hemoPerformedDetail.type == null) {
            return false;
        }
if (this.hemoPerformedDetail.start_time == null ) {
            return false;
        }
if (this.hemoPerformedDetail.end_time == null ) {
            return false;
        }
if (this.hemoPerformedDetail.machine_id == null ) {
            return false;
        }
if (this.hemoPerformedDetail.day == null ) {
            return false;
        }
if (this.hemoPerformedDetail.blood_flow == null || this.hemoPerformedDetail.blood_flow  == "") {
            return false;
        }
if (this.hemoPerformedDetail.uf == null || this.hemoPerformedDetail.uf  == "") {
            return false;
        }
if (this.hemoPerformedDetail.dialysate_no == null || this.hemoPerformedDetail.dialysate_no  == "") {
            return false;
        }
if (this.hemoPerformedDetail.iv_fluid == null || this.hemoPerformedDetail.iv_fluid  == "") {
            return false;
        }
if (this.hemoPerformedDetail.acetate == null || this.hemoPerformedDetail.acetate  == "") {
            return false;
        }
if (this.hemoPerformedDetail.bolus == null || this.hemoPerformedDetail.bolus  == "") {
            return false;
        }
if (this.hemoPerformedDetail.during_dialysis == null || this.hemoPerformedDetail.during_dialysis  == "") {
            return false;
        }
if (this.hemoPerformedDetail.dialyser_type == null || this.hemoPerformedDetail.dialyser_type  == "") {
            return false;
        }

        return true;

 }

 searchMachine() {
  let dialogRef = this.dialysisMachineNav.pickDialysisMachines(true);
  dialogRef.afterClosed().subscribe((result: DialysisMachineSummary) => {
    if (result) {
      this.hemoPerformedDetail.machine_id =   result[0].id;
      this.machineId = result[0].serial_no + " " + result[0].machine_no; 
  
    }
  });
}


 transformDates(todate: boolean = true) {
  if (todate) {
   
    let ed = new Date();
      let end_times:string[] = this.end_time.split(":");
      ed.setHours(parseInt( end_times[0]))
      ed.setMinutes(parseInt( end_times[1]))

    this.hemoPerformedDetail.end_time = this.tCUtilsDate.toTimeStamp(ed);


    let sd = new Date();
    let start_times:string[] = this.start_time.split(":");
    sd.setHours(parseInt( start_times[0]))
    sd.setMinutes(parseInt( start_times[1]))

  this.hemoPerformedDetail.start_time = this.tCUtilsDate.toTimeStamp(sd);


  } else {
  
  }
}
 }