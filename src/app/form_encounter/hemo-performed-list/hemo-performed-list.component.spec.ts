import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemoPerformedListComponent } from './hemo-performed-list.component';

describe('HemoPerformedListComponent', () => {
  let component: HemoPerformedListComponent;
  let fixture: ComponentFixture<HemoPerformedListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemoPerformedListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemoPerformedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
