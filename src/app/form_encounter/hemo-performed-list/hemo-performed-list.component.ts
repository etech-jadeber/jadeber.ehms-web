import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { HemoPerformedDetail, HemoPerformedSummary } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import {Location} from '@angular/common';
@Component({
  selector: 'app-hemo-performed-list',
  templateUrl: './hemo-performed-list.component.html',
  styleUrls: ['./hemo-performed-list.component.scss']
})
export class HemoPerformedListComponent implements OnInit {
  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  hemoPerformedsData: HemoPerformedSummary[] = [];
  hemoPerformedsTotalCount: number = 0;
  hemoPerformedSelectAll:boolean = false;
  hemoPerformedSelection: HemoPerformedSummary[] = [];

 hemoPerformedsDisplayedColumns: string[] = ["select","action" ,"sub_type","type","start_time","end_time","machine_no","day","blood_flow","uf","dialysate_no","dialysate_fluid","acetate","bolus","during_dialysis","dialyser_type","complication","other","iv_fluid" ];
  hemoPerformedSearchTextBox: FormControl = new FormControl();
  hemoPerformedIsLoading: boolean = false;

  constructor(private location:Location, public tcAuthorization:TCAuthorization, public appTranslation:AppTranslation, public tcUtilsArray:TCUtilsArray, private formEncounterNavigator:Form_EncounterNavigator,private formEncounterPersist:Form_EncounterPersist, private tcNotification:TCNotification, private tcNavigator:TCNavigator) {
    this.hemoPerformedSearchTextBox.setValue(
      formEncounterPersist.hemoPerformedSearchText
    );
    this.hemoPerformedSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.formEncounterPersist.form_soapSearchText = value.trim();
        this.searchHemoPerformed();
      });
   }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchHemoPerformed(true);
    });
  // form_soaps paginator
  this.paginator.page.subscribe(() => {
      this.searchHemoPerformed(true);
    });
    this.searchHemoPerformed(true); 
  }


  searchHemoPerformed(isPagination: boolean = false): void {
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.hemoPerformedSelection = [];
    this.hemoPerformedIsLoading = true;
    this.formEncounterPersist.searchHemoPerformed(this.encounterId, paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
      this.hemoPerformedsData = response.data;
      if (response.total != -1) {
        this.hemoPerformedsTotalCount = response.total;
      }
      this.hemoPerformedIsLoading = false;
    }, error => { this.hemoPerformedIsLoading = false; });
  }  deleteHemoPerformed(item: HemoPerformedDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("hemo_performed");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.formEncounterPersist.deleteHemoPerformed(this.encounterId, item.id).subscribe(response => {
          this.tcNotification.success("hemo_performed deleted");
          this.searchHemoPerformed();
        }, error => {
        });
      }

    });
  }  addHemoPerformed(): void {
    let dialogRef = this.formEncounterNavigator.addHemoPerformed(this.encounterId);
    dialogRef.afterClosed().subscribe(res => {
      this.searchHemoPerformed();
    });
  }

  editHemoPerformed(item: HemoPerformedDetail): void {
    let dialogRef = this.formEncounterNavigator.editHemoPerformed(this.encounterId, item.id);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.searchHemoPerformed();
      }
    });
  } downloadHemoPerformeds(): void {
this.tcNotification.info("Download hemo_performed" );  
}

 //Add this on ngAfterInit


 back():void{
  this.location.back();
 }

  

}
