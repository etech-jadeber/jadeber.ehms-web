import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDialysisEditComponent } from './order-dialysis-edit.component';

describe('OrderDialysisEditComponent', () => {
  let component: OrderDialysisEditComponent;
  let fixture: ComponentFixture<OrderDialysisEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderDialysisEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDialysisEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
