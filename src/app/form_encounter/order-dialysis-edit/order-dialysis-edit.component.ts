import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderDialysisDetail } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';

@Component({
  selector: 'app-order_dialysis-edit',
  templateUrl: './order-dialysis-edit.component.html',
  styleUrls: ['./order-dialysis-edit.component.scss']
})export class OrderDialysisEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  orderDialysisDetail: OrderDialysisDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OrderDialysisEditComponent>,
              public  persist: Form_EncounterPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.ids.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.ids.context['mode'] === TCModalModes.CASE;
  }ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("order_dialysis");
      this.title = this.appTranslation.getText("general","new") +  " " + "order_dialysis";
      this.orderDialysisDetail = new OrderDialysisDetail();
      this.orderDialysisDetail.encounter_id = this.ids.parentId;
      this.persist.getForm_Encounter(this.ids.parentId).subscribe(
        res=>{
          this.orderDialysisDetail.patient_id  = res.pid;
        }
      )
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("order_dialysis");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "order_dialysis";
      this.isLoadingResults = true;
      this.persist.getOrderDialysis(this.ids.parentId,this.ids.childId).subscribe(orderDialysisDetail => {
        this.orderDialysisDetail = orderDialysisDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addOrderDialysis(this.ids.parentId,this.orderDialysisDetail).subscribe(value => {
      this.tcNotification.success("orderDialysis added");
      this.orderDialysisDetail.id = value.id;
      this.dialogRef.close(this.orderDialysisDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateOrderDialysis(this.ids.parentId,this.orderDialysisDetail).subscribe(value => {
      this.tcNotification.success("order_dialysis updated");
      this.dialogRef.close(this.orderDialysisDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
     if(this.orderDialysisDetail == null){
      return false;
     }


        return true;


 }
 }
