import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDialysisListComponent } from './order-dialysis-list.component';

describe('OrderDialysisListComponent', () => {
  let component: OrderDialysisListComponent;
  let fixture: ComponentFixture<OrderDialysisListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderDialysisListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDialysisListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
