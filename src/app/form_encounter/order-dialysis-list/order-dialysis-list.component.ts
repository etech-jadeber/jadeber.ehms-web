import { Component, OnInit,EventEmitter, Output, QueryList, ViewChildren, ViewChild, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs';
import { OrderDialysisDetail, OrderDialysisSummary } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import {Location} from '@angular/common';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { AppTranslation } from 'src/app/app.translation';
import { TCUtilsArray } from 'src/app/tc/utils-array';



export enum PaginatorIndexes {
  order_dialysis,
}
@Component({
  selector: 'app-order-dialysis-list',
  templateUrl: './order-dialysis-list.component.html',
  styleUrls: ['./order-dialysis-list.component.scss']
})
export class OrderDialysisListComponent implements OnInit {
  @Input()encounterId:string;

    
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();


  orderDialysissData: OrderDialysisSummary[] = [];
  orderDialysissTotalCount: number = 0;
  orderDialysisSelectAll:boolean = false;
  orderDialysisSelection: OrderDialysisSummary[] = [];

 orderDialysissDisplayedColumns: string[] = ["select","action" ,"type","sub_type"];
  orderDialysisSearchTextBox: FormControl = new FormControl();
  orderDialysisIsLoading: boolean = false;

    

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(public formEncounterPersist:Form_EncounterPersist, private tcNotification:TCNotification, public appTranslation:AppTranslation,   public location: Location,public tcAuthorization:TCAuthorization,public tcUtilsArray:TCUtilsArray,
    private tcNavigator:TCNavigator, private formEncounterNavigator:Form_EncounterNavigator) { 
      this.orderDialysisSearchTextBox.setValue(formEncounterPersist.orderDialysisSearchText);
      this.orderDialysisSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.formEncounterPersist.orderDialysisSearchText = value.trim();
        this.searchOrderDialysis();
      });
  
    }
    back(): void {
      this.location.back();
    }


    @Output() onResult = new EventEmitter<number>();
  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchOrderDialysis(true);
    });
  // encounter_imagess paginator
  this.paginator.page.subscribe(() => {
      this.searchOrderDialysis(true);
    });
  }

 



  searchOrderDialysis(isPagination: boolean = false): void {
    let paginator = this.paginators.toArray()[PaginatorIndexes.order_dialysis];
    let sorter = this.sorters.toArray()[PaginatorIndexes.order_dialysis];

 
    
    this.orderDialysisSelection = [];
    this.orderDialysisIsLoading = true;
    this.formEncounterPersist.searchOrderDialysis(this.encounterId, paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
      this.orderDialysissData = response.data;
      if (response.total != -1) {
        this.orderDialysissTotalCount = response.total;
      }
      this.orderDialysisIsLoading = false;
    }, error => { this.orderDialysisIsLoading = false; });
  }  deleteOrderDialysis(item: OrderDialysisDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("order_dialysis");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.formEncounterPersist.deleteOrderDialysis(this.encounterId, item.id).subscribe(response => {
          this.tcNotification.success("order_dialysis deleted");
          this.searchOrderDialysis();
        }, error => {
        });
      }

    });
  }  addOrderDialysis(): void {
    let dialogRef = this.formEncounterNavigator.addOrderDialysis(this.encounterId);
    dialogRef.afterClosed().subscribe(res => {
      this.searchOrderDialysis();
    });
  }

  editOrderDialysis(item: OrderDialysisDetail): void {
    let dialogRef = this.formEncounterNavigator.editOrderDialysis(this.encounterId, item.id);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.searchOrderDialysis();
      }
    });
  } downloadOrderDialysiss(): void {
this.tcNotification.info("Download order_dialysis" );  
}

ngAfterViewInit(){
  this.searchOrderDialysis();
}

}
