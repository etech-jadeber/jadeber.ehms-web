import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDialysisPickComponent } from './order-dialysis-pick.component';

describe('OrderDialysisPickComponent', () => {
  let component: OrderDialysisPickComponent;
  let fixture: ComponentFixture<OrderDialysisPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderDialysisPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDialysisPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
