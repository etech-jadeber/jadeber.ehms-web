import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { OrderDialysisSummary, OrderDialysisSummaryPartialList } from 'src/app/form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';

@Component({
  selector: 'app-order-dialysis-pick',
  templateUrl: './order-dialysis-pick.component.html',
  styleUrls: ['./order-dialysis-pick.component.scss']
})
export class OrderDialysisPickComponent implements OnInit {

  orderDialysissData: OrderDialysisSummary[] = [];
  orderDialysissTotalCount: number = 0;
  orderDialysisSelectAll:boolean = false;
  orderDialysisSelection: OrderDialysisSummary[] = [];

 orderDialysissDisplayedColumns: string[] = ["select","patient_name","type","sub_type","frequency"];
  orderDialysisSearchTextBox: FormControl = new FormControl();
  orderDialysisIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) orderDialysisPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderDialysisSort: MatSort;


  constructor(public formEncounterPersist:Form_EncounterPersist, private tcNotification:TCNotification, public appTranslation:AppTranslation,public tcAuthorization:TCAuthorization,public tcUtilsArray:TCUtilsArray,
    private tcNavigator:TCNavigator, private formEncounterNavigator:Form_EncounterNavigator,   public dialogRef: MatDialogRef<OrderDialysisPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean)
 {
   // this.tcAuthorization.requireRead("patients");
    this.orderDialysisSearchTextBox.setValue(formEncounterPersist.orderDialysisSearchText);
    //delay subsequent keyup events
    this.orderDialysisSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.formEncounterPersist.orderDialysisSearchText = value;
      this.searDialysis();
    });
  }

  ngOnInit() {

    this.orderDialysisSort.sortChange.subscribe(() => {
      this.orderDialysisPaginator.pageIndex = 0;
      this.searDialysis();
    });

    this.orderDialysisPaginator.page.subscribe(() => {
      this.searDialysis();
    });

    //set initial picker list to 5
    this.orderDialysisPaginator.pageSize = 5;

    //start by loading items
    this.searDialysis();
  }

  searDialysis(): void {
    this.orderDialysisIsLoading = true;
    this.orderDialysisSelection = [];

    this.formEncounterPersist.searchOrderDialysisWithoutId(this.orderDialysisPaginator.pageSize,
        this.orderDialysisPaginator.pageIndex,
        this.orderDialysisSort.active,
        this.orderDialysisSort.direction).subscribe((partialList: OrderDialysisSummaryPartialList) => {
      this.orderDialysissData = partialList.data;
      if (partialList.total != -1) {
        this.orderDialysissTotalCount = partialList.total;
      }
      this.orderDialysisIsLoading = false;
    }, error => {
      this.orderDialysisIsLoading = false;
    });

  }

  markOneItem(item: OrderDialysisSummary) {
    if(!this.tcUtilsArray.containsId(this.orderDialysisSelection,item.id)){
          this.orderDialysisSelection = [];
          this.orderDialysisSelection.push(item);
        }
        else{
          this.orderDialysisSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.orderDialysisSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
