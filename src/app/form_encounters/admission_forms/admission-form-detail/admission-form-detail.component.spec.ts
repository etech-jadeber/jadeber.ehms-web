import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Admission_FormDetailComponent} from "./admission-form-detail.component";


describe('AdmissionFormDetailComponent', () => {
  let component: Admission_FormDetailComponent;
  let fixture: ComponentFixture<Admission_FormDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Admission_FormDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admission_FormDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
