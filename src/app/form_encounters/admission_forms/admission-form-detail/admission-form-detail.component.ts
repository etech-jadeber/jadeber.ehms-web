import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../../tc/authorization";
import { TCUtilsAngular } from "../../../tc/utils-angular";
import { TCNotification } from "../../../tc/notification";
import { TCUtilsArray } from "../../../tc/utils-array";
import { TCNavigator } from "../../../tc/navigator";
import { JobPersist } from "../../../tc/jobs/job.persist";
import { AppTranslation } from "../../../app.translation";


import { Admission_FormDetail } from "../admission_form.model";
import { Admission_FormPersist } from "../admission_form.persist";
import { Admission_FormNavigator } from "../admission_form.navigator";
import { DoctorDetail, DoctorSummary } from "../../../doctors/doctor.model";
import { PatientSummary } from "../../../patients/patients.model";
import { DoctorPersist } from "../../../doctors/doctor.persist";
import { PatientPersist } from "../../../patients/patients.persist";
import { TCUtilsDate } from "../../../tc/utils-date";
import { tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';

export enum Admission_FormTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-admission_form-detail',
  templateUrl: 'admission-form-detail.component.html',
  styleUrls: ['admission-form-detail.component.css']
})
export class Admission_FormDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  admission_formLoading: boolean = false;

  admission_FormTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  admission_formDetail: Admission_FormDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  doctors: DoctorSummary[] = [];
  patients: PatientSummary[] = [];

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    private doctorPersist: DoctorPersist,
    private patientPersist: PatientPersist,
    public admission_formNavigator: Admission_FormNavigator,
    public admission_formPersist: Admission_FormPersist,
    public menuState: MenuState) {
    this.tcAuthorization.requireRead("admission_forms");
    this.admission_formDetail = new Admission_FormDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("admission_forms");
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.admission_formLoading = true;
    this.admission_formPersist.getAdmission_Form(id).subscribe(admission_formDetail => {
      this.admission_formDetail = admission_formDetail;
      this.menuState.getDataResponse(this.admission_formDetail);
      this.admission_formLoading = false;

      this.getDoctors(this.admission_formDetail.admitting_doctor_id);
      this.getDoctors(this.admission_formDetail.nurse_id);
      this.getPatients(this.admission_formDetail.patient_id);

    }, error => {
      console.error(error);
      this.admission_formLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.admission_formDetail.id);
  }

  editAdmission_Form(): void {
    let modalRef = this.admission_formNavigator.editAdmission_Form(this.admission_formDetail.id);
    modalRef.afterClosed().subscribe(modifiedAdmission_FormDetail => {
      TCUtilsAngular.assign(this.admission_formDetail, modifiedAdmission_FormDetail);
    }, error => {
      console.error(error);
    });
  }

  printAdmission_Form(): void {
    this.admission_formPersist.print(this.admission_formDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print admission_form", true);
    });
  }

  back(): void {
    this.location.back();
  }

  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(this.doctors, doctorId);
    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`
    }
    return "";
  }

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(value => value.uuidd == patientId);
    if (patient) {
      return `${patient.fname} ${patient.lname}`
    }
    return "";
  }

  getDoctors(doctorId: string) {
    this.doctorPersist.getDoctor(doctorId).subscribe(
      result => {
        this.doctors.push(result);
      }
    )
  }

  getPatients(patientId: string) {
    this.patientPersist.getPatient(patientId).subscribe(
      result => {
        this.patients.push(result);
      }
    )
  }

}
