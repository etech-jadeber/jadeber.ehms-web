import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Admission_FormEditComponent} from "./admission-form-edit.component";


describe('AdmissionFormEditComponent', () => {
  let component: Admission_FormEditComponent;
  let fixture: ComponentFixture<Admission_FormEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Admission_FormEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admission_FormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
