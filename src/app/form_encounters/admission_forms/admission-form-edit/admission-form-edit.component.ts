import {Component, Inject, Injectable, Input, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsString} from "../../../tc/utils-string";
import {AppTranslation} from "../../../app.translation";

import {TCIdMode, TCModalModes} from "../../../tc/models";


import {Admission_FormDetail} from "../admission_form.model";
import {Admission_FormPersist} from "../admission_form.persist";
import {physican_type} from "../../../app.enums";
import { Form_EncounterPersist } from '../../form_encounter.persist';


@Injectable()
abstract class AdmissionFormComponent{
  isLoadingResults: boolean = false;
  title: string;
  admission_formDetail: Admission_FormDetail;
  patientFullName: string;
  doctorFullName: string;
  nurseFullName: string;
  physicalType = physican_type;


  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public persist: Admission_FormPersist,
              public isDialog: Boolean,
              public idMode: TCIdMode) {
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("patient", "admission_form");
      this.admission_formDetail = new Admission_FormDetail();
      // this.admission_formDetail.admission_form_id = this.data.admissionFormId;


      //set necessary defaults

    } else {
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("patient", "admission_form");
      this.isLoadingResults = true;
      this.persist.getAdmission_Form(this.idMode.id).subscribe((admission_formDetail:Admission_FormDetail) => {
        this.admission_formDetail = admission_formDetail;
        console.log("the admission form is ", this.admission_formDetail);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  action (admission_form: Admission_FormDetail){
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addAdmission_Form(this.admission_formDetail).subscribe(value => {
      this.tcNotification.success("Admission_Form added");
      this.admission_formDetail.id = value.id;
      this.action(this.admission_formDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateAdmission_Form(this.admission_formDetail).subscribe(value => {
      // this.tcNotification.success("Admission_Form updated");
      this.action(this.admission_formDetail)
      this.isLoadingResults = false
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.admission_formDetail == null) {
      return false;
    }

    if (this.admission_formDetail.encounter_id == null) {
      return false;
    }


    return true;
  }


  // searchPatient() {
  //   let dialogRef = this.patientNavigator.pickPatients(true);
  //   dialogRef.afterClosed().subscribe((result: PatientSummary) => {
  //     if (result) {
  //       this.patientFullName = result[0].fname + ' ' + result[0].lname;
  //       this.admission_formDetail.patient_id = result[0].id;
  //     }
  //   });
  // }

  // searchNurses() {
  //   let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.nurse);
  //   dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
  //     if (result) {
  //       this.nurseFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
  //       this.admission_formDetail.nurse_id = result[0].id;
  //     }
  //   });
  // }

  // searchDoctors() {
  //   let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
  //   dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
  //     if (result) {
  //       this.doctorFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
  //       this.admission_formDetail.admitting_doctor_id = result[0].id;
  //     }
  //   });
  // }
}

@Component({
  selector: 'app-admission_form-edit',
  templateUrl: 'admission-form-edit.component.html',
  styleUrls: ['admission-form-edit.component.css']
})
export class Admission_FormEditComponent extends AdmissionFormComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  admission_formDetail: Admission_FormDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: Admission_FormPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public dialogRef: MatDialogRef<AdmissionFormComponent>,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
              ) {
        super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist, true, idMode)
  }

  isNew(): boolean {
    return false
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(admission_formDetail: Admission_FormDetail){
    this.dialogRef.close(admission_formDetail)
  }
  
}


@Component({
  selector: 'app-admission_form-edit-list',
  templateUrl: 'admission-form-edit.component.html',
  styleUrls: ['admission-form-edit.component.css']
})
export class Admission_FormEditListComponent extends AdmissionFormComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  admission_formDetail: Admission_FormDetail;

  @Input() admission_form_id: string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: Admission_FormPersist,
              public form_encounterPersist: Form_EncounterPersist) {
                super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist, false, {id: null, mode: TCModalModes.EDIT})
  }

  ngOnInit(): void {
    this.persist.getAdmission_Form(this.admission_form_id).subscribe(
      admission => {
        this.admission_formDetail = admission
      }
    )
  }

  onCancel(): void {
  }

  saveResult(){
    if (!this.canSubmit()){
      this.tcNotification.error("Admission is not added. Required field is not added")
      return;
    }
    if (this.admission_formDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }
 
}