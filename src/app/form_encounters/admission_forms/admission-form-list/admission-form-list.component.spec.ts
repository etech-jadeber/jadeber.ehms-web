import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Admission_FormListComponent} from "./admission-form-list.component";


describe('AdmissionFormListComponent', () => {
  let component: Admission_FormListComponent;
  let fixture: ComponentFixture<Admission_FormListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Admission_FormListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admission_FormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
