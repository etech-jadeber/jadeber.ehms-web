import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";

import {Admission_FormPersist} from "../admission_form.persist";
import {Admission_FormNavigator} from "../admission_form.navigator";
import {Admission_FormDetail, Admission_FormSummary, Admission_FormSummaryPartialList} from "../admission_form.model";
import {DoctorDetail, DoctorSummary} from "../../../doctors/doctor.model";
import {PatientDetail, PatientSummary} from "../../../patients/patients.model";
import {DoctorPersist} from "../../../doctors/doctor.persist";
import {PatientPersist} from "../../../patients/patients.persist";
import {DoctorNavigator} from "../../../doctors/doctor.navigator";
import {PatientNavigator} from "../../../patients/patients.navigator";
import {admission_form_status, physican_type} from "../../../app.enums";
import {BedassignmentNavigator} from 'src/app/bed/bedassignments/bedassignment.navigator';
import {DepositPersist} from 'src/app/deposits/deposit.persist';
import {DepositDetail} from 'src/app/deposits/deposit.model';
import {BedassignmentPersist} from 'src/app/bed/bedassignments/bedassignment.persist';
import {BedassignmentSummary, ExtendedBedassignmentDetail} from 'src/app/bed/bedassignments/bedassignment.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { UserSummary } from 'src/app/tc/users/user.model';
import { Department_SpecialtyDetail } from 'src/app/department_specialtys/department_specialty.model';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { encounterFilterColumn } from '../../form_encounter.model';


@Component({
  selector: 'app-admission_form-list',
  templateUrl: './admission-form-list.component.html',
  styleUrls: ['./admission-form-list.component.css']
})
export class Admission_FormListComponent implements OnInit, OnDestroy, AfterViewInit {

  admission_formsData: Admission_FormSummary[] = [];
  admission_formsTotalCount: number = 0;
  admission_formsSelectAll: boolean = false;
  admission_formsSelection: Admission_FormSummary[] = [];
  admissionFormFilterColumn = encounterFilterColumn;

  admission_formsDisplayedColumns: string[] = ["select", "action", 'p.pid',"patient_id" , "admission_date", "ward_id", "expected_date_of_discharge", "admission_type", "urgency"];
  admission_formsSearchTextBox: FormControl = new FormControl();
  admission_formsIsLoading: boolean = false;

  @Input() role: number;
  @Input() encounterId: string;
  @Output() onResult = new EventEmitter<number>();
  physicianType = physican_type;

  @ViewChild(MatPaginator, {static: true}) admission_formsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) admission_formsSort: MatSort;

  doctor: UserSummary;
  doctors: DoctorSummary[] = [];
  patients: {[id: string] :PatientSummary} = {};
  departments: {[id: string]: Department_SpecialtyDetail} = {}

  wards: {[id: string]: BedroomtypeDetail} = {}

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public doctorNavigator: DoctorNavigator,
              public patientNavigator: PatientNavigator,
              public admission_formPersist: Admission_FormPersist,
              public admission_formNavigator: Admission_FormNavigator,
              private patientPersist: PatientPersist,
              private depositPersist: DepositPersist,
              private bedPersist: BedassignmentPersist,
              public jobPersist: JobPersist,
              public userPersist: UserPersist,
              public bedassignmentNavigator: BedassignmentNavigator,
              public bedRoomTypePersist: BedroomtypePersist,
              public formEncounterPersist: Form_EncounterPersist,
              public departmentSpecialityPersist: Department_SpecialtyPersist,
  ) {
    this.admission_formPersist.admissionFormSearchHistory.admission_status = admission_form_status.sent;
    this.admission_formPersist.admissionFormSearchHistory.encounter_id = this.encounterId;
  }

  ngAfterViewInit(): void {
    this.admission_formsSearchTextBox.setValue(this.admission_formPersist.admissionFormSearchHistory.search_text);
    this.admission_formPersist.admissionFormSearchHistory.encounter_id = this.encounterId;

    //delay subsequent keyup events
    this.admission_formsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.admission_formPersist.admissionFormSearchHistory.search_text = value;
      this.searchAdmission_Forms();
    });
  }

  ngOnInit() {


    this.admission_formsSort.sortChange.subscribe(() => {
      this.admission_formsPaginator.pageIndex = 0;
      this.searchAdmission_Forms(true);
    });

    this.admission_formsPaginator.page.subscribe(() => {
      this.searchAdmission_Forms(true);
    });
    //start by loading items
    this.searchAdmission_Forms();
  }

  ngOnDestroy(): void {
    this.admission_formPersist.clearFilter();
  }

  searchAdmission_Forms(isPagination: boolean = false): void {


    let paginator = this.admission_formsPaginator;
    let sorter = this.admission_formsSort;

    this.admission_formsIsLoading = true;
    this.admission_formsSelection = [];

    this.admission_formPersist.searchAdmission_Form(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: Admission_FormSummaryPartialList) => {
      this.admission_formsData = partialList.data;

      this.admission_formsData.forEach(value => {
        this.getPatient(value);
        this.getWard(value)
        
      })

      if (partialList.total != -1) {
        this.admission_formsTotalCount = partialList.total;
      }
      this.admission_formsIsLoading = false;
    }, error => {
      this.admission_formsIsLoading = false;
    });

  }

  getPatient(value: Admission_FormSummary) {
    const patient = this.patients[value.patient_id]
    if (patient){
      value.patient_name = `${patient.fname} ${patient.mname} ${patient.lname}`
    } else {
      this.patients[value.patient_id] = new PatientDetail()
      this.patientPersist.getPatient(value.patient_id).subscribe(
        result => {
          this.patients[value.patient_id] = result;
          value.patient_name = `${result.fname} ${result.mname} ${result.lname}`
        }
      )
    }
  }

  getDepartment(value: Admission_FormSummary) {
    const department = this.departments[value.department_specialty_id]
    if (department){
      value.department_specialty_name = department.name;
    } else {
      this.departments[value.department_specialty_id] = new Department_SpecialtyDetail()
      this.departmentSpecialityPersist.getDepartment_Specialty(value.department_specialty_id).subscribe(
        result => {
          this.departments[value.department_specialty_id] = result;
          value.department_specialty_name = result.name;
        }
      )
    }
  }

  getWard(value: Admission_FormSummary) {
    const ward = this.wards[value.ward_id]
    if (ward){
      value.ward_name = ward.name;
    } else {
      this.bedRoomTypePersist.getBedroomtype(value.ward_id).subscribe(
        result => {
          this.wards[value.ward_id] = result;
          value.ward_name = result.name;
        }
      )
    }
  }


  downloadAdmission_Forms(): void {
    if (this.admission_formsSelectAll) {
      this.admission_formPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download admission_forms", true);
      });
    } else {
      this.admission_formPersist.download(this.tcUtilsArray.idsList(this.admission_formsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download admission_forms", true);
      });
    }
  }

  bookBedroom(admissionDetail: Admission_FormDetail): void {
        let dialogRef = this.bedassignmentNavigator.addBedassignment(admissionDetail.id,admissionDetail.ward_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAdmission_Forms();
      }
    });
  }

  // addAdmission_Form(): void {
  //   let dialogRef = this.admission_formNavigator.addAdmission_Form(this.role == physican_type.nurse ? physican_type.nurse : -1, this.encounterId);
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.searchAdmission_Forms();
  //     }
  //   });
  // }

  // editAdmission_Form(item: Admission_FormSummary) {
  //   let dialogRef = this.admission_formNavigator.editAdmission_Form(item.id, this.role == physican_type.nurse ? physican_type.nurse : -1, this.encounterId);
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       TCUtilsAngular.assign(item, result);
  //     }

  //   });
  // }

  deleteAdmission_Form(item: Admission_FormSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Admission_Form");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.admission_formPersist.deleteAdmission_Form(item.id).subscribe(response => {
          this.tcNotification.success("Admission_Form deleted");
          this.searchAdmission_Forms();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }


  // addBedAssignment(admissionForm: Admission_FormSummary): void {
  //   if (this.getDepositBalance() > 0) {
  //     let dialogRef = this.bedassignmentNavigator.addBedassignment(admissionForm.id);
  //   } else {
  //     this.tcNotification.error("Please Deposit first");
  //   }
  // }

  getBedaAssignment(patientId: string) {
    this.bedPersist.getBedassignmentByPatient(patientId).subscribe(res => {
      // this.assignedData = res.data.filter(data => data.status == 100)
    });
  }

  isBedAssigned(data: Admission_FormDetail) {
    return data.status == admission_form_status.bed_assigned;
  }

  isNurseAssigned(data: Admission_FormDetail){
    return data.status = admission_form_status.nurse_assigned;
  }

  loadPatientsDeposit(patientId: string) {
    this.depositPersist.getDepositByPatient(patientId).subscribe(
      result => {
        // this.deposit = result;
      }
    )
  }

  getDepositBalance() {
    // return this.deposit.current_amount;
  }

  dispatchPatient(admissionForm: Admission_FormSummary): void {
    this.admission_formsIsLoading = true;
    this.admission_formPersist.admission_formDo(admissionForm.id, "can_dispatch", admissionForm)
      .subscribe((result) => {
        let dialogRef = this.admission_formNavigator.dispatchPatient(admissionForm.id);
        dialogRef.afterClosed().subscribe(result => {
          this.searchAdmission_Forms();
        });
      })
    this.admission_formsIsLoading = false;
  }


  isDischarged(admissionForm: Admission_FormSummary) {
    return admissionForm.condition != null;
  }

  rejectRequest(admissionForm: Admission_FormSummary){
    this.admission_formPersist.admission_formDo(admissionForm.id, "reject", {}).subscribe(
      result => {
        this.tcNotification.success("Admission is successfully rejected")
        this.searchAdmission_Forms()
      }
    )
  }

}

