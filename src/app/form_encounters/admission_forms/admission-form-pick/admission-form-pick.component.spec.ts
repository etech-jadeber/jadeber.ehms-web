import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Admission_FormPickComponent} from "./admission-form-pick.component";


describe('AdmissionFormPickComponent', () => {
  let component: Admission_FormPickComponent;
  let fixture: ComponentFixture<Admission_FormPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Admission_FormPickComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Admission_FormPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
