import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {Admission_FormDetail, Admission_FormSummary, Admission_FormSummaryPartialList} from "../admission_form.model";
import {Admission_FormPersist} from "../admission_form.persist";
import {DoctorPersist} from "../../../doctors/doctor.persist";
import {PatientPersist} from "../../../patients/patients.persist";
import {DoctorDetail, DoctorSummary} from "../../../doctors/doctor.model";
import {PatientSummary} from "../../../patients/patients.model";


@Component({
  selector: 'app-admission_form-pick',
  templateUrl: './admission-form-pick.component.html',
  styleUrls: ['./admission-form-pick.component.css']
})
export class Admission_FormPickComponent implements OnInit {

  admission_formsData: Admission_FormSummary[] = [];
  admission_formsTotalCount: number = 0;
  admission_formsSelection: Admission_FormSummary[] = [];
  admission_formsDisplayedColumns: string[] = ["select", 'serial_id', 'admitting_doctor_id', 'admission_date', 'planned_procedure', 'nurse_id', 'patient_id'];

  admission_formsSearchTextBox: FormControl = new FormControl();
  admission_formsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) admission_formsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) admission_formsSort: MatSort;

  doctors: DoctorSummary[] = [];
  patients: PatientSummary[] = [];

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public admission_formPersist: Admission_FormPersist,
              private doctorPersist: DoctorPersist,
              private patientPersist: PatientPersist,
              public dialogRef: MatDialogRef<Admission_FormPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.admission_formsSearchTextBox.setValue(admission_formPersist.admissionFormSearchHistory.search_text);
    //delay subsequent keyup events
    this.admission_formsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.admission_formPersist.admissionFormSearchHistory.search_text = value;
      this.searchAdmission_Forms();
    });
  }

  ngOnInit() {

    this.admission_formsSort.sortChange.subscribe(() => {
      this.admission_formsPaginator.pageIndex = 0;
      this.searchAdmission_Forms();
    });

    this.admission_formsPaginator.page.subscribe(() => {
      this.searchAdmission_Forms();
    });

    //set initial picker list to 5
    this.admission_formsPaginator.pageSize = 5;

    //start by loading items
    this.searchAdmission_Forms();
  }

  searchAdmission_Forms(): void {
    this.admission_formsIsLoading = true;
    this.admission_formsSelection = [];

    this.admission_formPersist.searchAdmission_Form(this.admission_formsPaginator.pageSize,
      this.admission_formsPaginator.pageIndex,
      this.admission_formsSort.active,
      this.admission_formsSort.direction).subscribe((partialList: Admission_FormSummaryPartialList) => {
      this.admission_formsData = partialList.data;

      this.admission_formsData.forEach(value => {
        this.getDoctors(value.admitting_doctor_id);
        this.getDoctors(value.nurse_id);
        this.getPatients(value.patient_id);
      })

      if (partialList.total != -1) {
        this.admission_formsTotalCount = partialList.total;
      }
      this.admission_formsIsLoading = false;
    }, error => {
      this.admission_formsIsLoading = false;
    });

  }

  markOneItem(item: Admission_FormSummary) {
    if (!this.tcUtilsArray.containsId(this.admission_formsSelection, item.id)) {
      this.admission_formsSelection = [];
      this.admission_formsSelection.push(item);
    } else {
      this.admission_formsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.admission_formsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  getDoctors(doctorId: string) {
    this.doctorPersist.getDoctor(doctorId).subscribe(
      result => {
        this.doctors.push(result);
      }
    )
  }

  getPatients(patientId: string) {
    this.patientPersist.getPatient(patientId).subscribe(
      result => {
        this.patients.push(result);
      }
    )
  }

  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(this.doctors, doctorId);
    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`
    }
    return "";
  }

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(value => value.uuidd == patientId);
    if (patient) {
      return `${patient.fname} ${patient.lname}`
    }
    return "";
  }

}
