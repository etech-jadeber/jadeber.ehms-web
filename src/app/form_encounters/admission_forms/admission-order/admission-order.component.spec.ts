import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdmissionOrderComponent } from './admission-order.component';

describe('AdmissionOrderComponent', () => {
  let component: AdmissionOrderComponent;
  let fixture: ComponentFixture<AdmissionOrderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdmissionOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmissionOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
