import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {AppTranslation} from "../../../app.translation";
import {DepartmentSummary} from "../../../departments/department.model";
import {DepartmentNavigator} from "../../../departments/department.navigator";
import {Department_SpecialtyNavigator} from "../../../department_specialtys/department_specialty.navigator";
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { Admission_FormPersist } from '../admission_form.persist';
import { AdmissionType, ServiceType } from 'src/app/app.enums';
import { Admission_FormDetail } from '../admission_form.model';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { TCIdMode } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { FinalAssessmentPersist } from 'src/app/final_assessment/final_assessment.persist';
import { DiagnosisPersist } from '../../diagnosiss/diagnosis.persist';
import { BedroomtypeSummary } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { FormClinicalInstructionNavigator } from 'src/app/form_clinical_instruction/form_clinical_instruction.navigator';
import { PatientHistoryPersist } from 'src/app/patient_history/patient_history.persist';

@Component({
  selector: 'app-admission-order',
  templateUrl: './admission-order.component.html',
  styleUrls: ['./admission-order.component.css']
})
export class AdmissionOrderComponent implements OnInit {
  departmentName: string = "";
  department: string = "";
  // department_id: string = "";
  admission_order  = new Admission_FormDetail();
  treatmentDisplayedColumns : string[] = ["name", "from_date", "to_date", "interval", "action"]
  ward: string;

  AdmissionType = AdmissionType;

  constructor(public dialogRef: MatDialogRef<AdmissionOrderComponent>,
              private departmentSpecialityNavigator: Department_SpecialtyNavigator,
              public appTranslation: AppTranslation,
              public formEncounterPersist: Form_EncounterPersist,
              public wardNavigator: BedroomtypeNavigator,
              public admissionFormPersist: Admission_FormPersist,
              public departmentSpecialityPersist: Department_SpecialtyPersist,
              public patientHistoryPersist: PatientHistoryPersist,
              public assesmentPersist: DiagnosisPersist,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public formClinicalInstructionNavigator: FormClinicalInstructionNavigator,
              public bedroomTypeNavigator: BedroomtypeNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
  }

  ngOnInit() {
    this.admission_order.encounter_id = this.idMode.id
    this.admission_order.reason = "";
    this.admission_order.orders = [];
    this.patientHistoryPersist.encounter_id = this.idMode.id
    this.patientHistoryPersist.searchPatientHistory(1,0,"","").subscribe((final_diagnosis)=>{
      if(final_diagnosis.total)
        this.admission_order.admission_diagnosis = final_diagnosis.data[0].provisional_psychiatric_diagnosis;
      else{
        this.admission_order.admission_diagnosis = ""
      }

    })
  }

  onCancel() {
    this.dialogRef.close();
  }

  searchDepartment() {
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, this.tcUtilsString.invalid_id);
    dialogRef.afterClosed().subscribe((result: DepartmentSummary[]) => {
      if (result) {
        this.admission_order.department_specialty_id = result[0].id
        this.department = result[0].name
      }
    });
  }


  
  addFormClinicalInstruction(): void {
    let dialogRef = this.formClinicalInstructionNavigator.addFormClinicalInstructionWizard(this.idMode.id, this.tcUtilsString.invalid_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.admission_order.orders =[...this.admission_order.orders,...result];
      }
    });
  }

  searchWard(): void {
    let dialogRef = this.bedroomTypeNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
      if (result){
        this.ward = result[0].name;
        this.admission_order.ward_id = result[0].id;
      }
    }) 
  }

  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }

  removeTreatmentFromList(element:any){
    this.admission_order.orders = this.admission_order.orders.filter(treatment => treatment != element);
  }

  getInterval(interval: number){
    return `${Math.floor(interval / 60) }:${interval % 60}`
  }

  onAdd() {
    this.admissionFormPersist.addAdmission_Form(this.admission_order).subscribe(
      result => {
        this.tcNotification.success(
          'Admission Order has been added Successfully!'
        );
        this.dialogRef.close(this.admission_order);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  canSubmit() {
    // if (this.admission_order.department_specialty_id == null) {
    //   return false;
    // }
    if (this.admission_order.ward_id == null) {
      return false;
    }
    if (this.admission_order.admission_type == null) {
      return false;
    }
    if (this.admission_order.admission_type == AdmissionType.elective && this.admission_order.urgency == -1) {
      return false;
    }
    if (this.admission_order.expected_days_of_required == null) {
      return false;
    }
  if (this.admission_order.patient_condition == null) {
      return false;
    }
    // if (this.admission_order.admission_diagnosis == null ||  this.admission_order.admission_diagnosis == "") {
    //   return false;
    // }

    if (this.admission_order.reason == null ||  this.admission_order.reason == "") {
      return false;
    }
    if (this.admission_order.reason == null) {
      return false;
    }
    return true;
  }
}
