import { FormClinicalInstructionDetail } from "src/app/form_clinical_instruction/form_clinical_instruction.model";
import {TCId} from "../../tc/models";

export type Prefixed<T> = {
  [K in keyof T as `admission_${string & K}`]: T[K];
};

export type PrefixedAdmission = Prefixed<Admission_FormDetail>;

export class Admission_FormSummary extends TCId {
admitting_doctor_id : string;
admission_date : number;
planned_procedure : string;
nurse_id : string;
patient_id : string;
next_of_kin_name : string;
next_of_kin_address : string;
next_of_kin_phone : string;
next_of_kin_email : string;
serial_id : number;
encounter_id : string;
sent_doctor_id:string;
condition : number;
remark : string;
department_specialty_id:string;
status:number;
reason:string;
chief_complaint:string;
provisional_diagnosis:string;
admission_status:number;
patient_name: string;
department_specialty_name: string;
admission_type : number;
expected_days_of_required : number;
urgency : number = -1;
doctor_id : string;
admission_diagnosis: string;
ward_name:string
ward_id:string
patient_condition: number;
instruction: string;
orders : FormClinicalInstructionDetail[];
}



export class Admission_FormSummaryPartialList {
  data: Admission_FormSummary[];
  total: number;
}

export class Admission_FormDetail extends Admission_FormSummary {
}

export class Admission_FormDashboard {
  total: number;
}

export class AdmitByWardReport {
  name: string;
  male: number;
  female: number;
  total: number;
}