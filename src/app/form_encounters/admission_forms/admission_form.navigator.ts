import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes, TCEnum} from "../../tc/models";
import {Admission_FormEditComponent} from "./admission-form-edit/admission-form-edit.component";
import {Admission_FormPickComponent} from "./admission-form-pick/admission-form-pick.component";
import {physican_type} from "../../app.enums";
import { PatientDispatchComponent } from './patient-dispatch/patient-dispatch.component';
import {AdmissionOrderComponent} from "./admission-order/admission-order.component";


@Injectable({
  providedIn: 'root'
})

export class Admission_FormNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  admission_formsUrl(): string {
    return "/admission_forms";
  }

  nurse_admission_formsUrl(): string {
    return "/nurse_admission_forms";
  }

  admission_formUrl(id: string): string {
    return "/admission_forms/" + id;
  }

  viewAdmission_Forms(): void {
    this.router.navigateByUrl(this.admission_formsUrl());
  }

  viewNurseAdmission_Forms(): void {
    this.router.navigateByUrl(this.nurse_admission_formsUrl());
  }

  viewAdmission_Form(id: string): void {
    this.router.navigateByUrl(this.admission_formUrl(id));
  }

  editAdmission_Form(id: string, role: number = -1, encounterId: string = null): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Admission_FormEditComponent, {
      data: {idMode: new TCIdMode(id, TCModalModes.EDIT), role: role, encounterId: encounterId},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addAdmission_Form(admissionFormId: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Admission_FormEditComponent, {
      data: new TCIdMode(admissionFormId, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  admission_order(encounterId: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(AdmissionOrderComponent, {
      data: new TCIdMode(encounterId, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickAdmission_Forms(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Admission_FormPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  dispatchPatient(id:string){
    const dialogRef = this.dialog.open(PatientDispatchComponent, {
      data: new TCIdMode(id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }
}
