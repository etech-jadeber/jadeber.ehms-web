import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../../tc/utils-http";
import {TcDictionary, TCEnum, TCId} from "../../tc/models";
import {Admission_FormDashboard, Admission_FormDetail, Admission_FormSummaryPartialList} from "./admission_form.model";
import {TCUtilsString} from "../../tc/utils-string";
import {AdmissionStatus, AdmissionType, admission_form_status, admission_reason, discharge_condition, PatientCondition} from "../../app.enums"
import {AppTranslation} from "../../app.translation";


@Injectable({
  providedIn: 'root'
})
export class Admission_FormPersist {

  // admission_formSearchText: string = "";
  encounter_id: string;

  // discharge_conditionId: number;
  // admission_status: number = -1;

  // admissionTypeFilter: number ;

    AdmissionType: TCEnum[] = [
     new TCEnum( AdmissionType.emergency, 'Emergency'),
  new TCEnum( AdmissionType.elective, 'Elective'),

  ];

  patient_condition: TCEnum[] = [
    new TCEnum( PatientCondition.stable, 'Stable'),
    new TCEnum( PatientCondition.critical, 'Critical'),
  ];


  discharge_condition: TCEnum[] = [
    new TCEnum(discharge_condition.treated, 'treated'),
    new TCEnum(discharge_condition.worsen, 'worsen'),
    new TCEnum(discharge_condition.died, 'died'),
    new TCEnum(discharge_condition.no_change, 'no_change'),
  ];

  admissionReason: TCEnum[] = [
    new TCEnum(admission_reason.physical_management, 'Medical management'),
    new TCEnum(admission_reason.observation, 'observation'),
    new TCEnum(admission_reason.surgical_management, 'surgical management'),
    new TCEnum(admission_reason.labory_and_delivery_management, 'labor and delivery management'),
  ];

  admission_form_status: TCEnum[] = [
    new TCEnum(admission_form_status.sent, this.appTranslation.getText('patient', 'sent')),
    new TCEnum(admission_form_status.bed_assigned, this.appTranslation.getText('patient', 'bed_assigned')),
    new TCEnum(admission_form_status.nurse_assigned, this.appTranslation.getText('patient', 'nurse_assigned')),
    new TCEnum(admission_form_status.discharged, this.appTranslation.getText('patient', 'discharged')),
  ];

  constructor(private http: HttpClient, private appTranslation: AppTranslation) {
  }

  admissionFormSearchHistory = {
    "admission_status": undefined,
    "encounter_id": "",
    "patient_id": "",
    "search_text": "",
    "search_column": "",
}

defaultAdmissionFormSearchHistory = {...this.admissionFormSearchHistory}

resetAdmissionFormSearchHistory(){
  this.admissionFormSearchHistory = {...this.defaultAdmissionFormSearchHistory}
}

  searchAdmission_Form(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.admissionFormSearchHistory): Observable<Admission_FormSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("admission_forms", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);
    // if (this.encounterId) {
    //   url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId);
    // }
    // if (this.admission_status != -1) {
    //   url = TCUtilsString.appendUrlParameter(url, "admission_status", this.admission_status.toString());
    // }
    return this.http.get<Admission_FormSummaryPartialList>(url);

  }

  filters(searchHistory = this.admissionFormSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  clearFilter() {
    // this.encounterId = null;
    // this.admission_status = -1;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "admission_forms/do", new TCDoParam("download_all", this.filters()));
  }

  admission_formDashboard(): Observable<Admission_FormDashboard> {
    return this.http.get<Admission_FormDashboard>(environment.tcApiBaseUri + "admission_forms/dashboard");
  }

  getAdmission_Form(id: string): Observable<Admission_FormDetail> {
    return this.http.get<Admission_FormDetail>(environment.tcApiBaseUri + "admission_forms/" + id);
  }

  addAdmission_Form(item: Admission_FormDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "admission_forms/", item);
  }

  updateAdmission_Form(item: Admission_FormDetail): Observable<Admission_FormDetail> {
    return this.http.patch<Admission_FormDetail>(environment.tcApiBaseUri + "admission_forms/" + item.id, item);
  }

  deleteAdmission_Form(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "admission_forms/" + id);
  }

  admission_formsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "admission_forms/do", new TCDoParam(method, payload));
  }

  admission_formDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "admission_forms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "admission_forms/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "admission_forms/" + id + "/do", new TCDoParam("print", {}));
  }

  admissionStatusFilter: number ;

    AdmissionStatus: TCEnum[] = [
     new TCEnum( AdmissionStatus.general, 'general'),
  new TCEnum( AdmissionStatus.accident, 'accident'),
  new TCEnum( AdmissionStatus.surgery, 'surgery'),
  new TCEnum( AdmissionStatus.observation, 'observation'),

  ];


}
