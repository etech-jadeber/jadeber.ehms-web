import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientDispatchComponent } from './patient-dispatch.component';

describe('PatientDispatchComponent', () => {
  let component: PatientDispatchComponent;
  let fixture: ComponentFixture<PatientDispatchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientDispatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDispatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
