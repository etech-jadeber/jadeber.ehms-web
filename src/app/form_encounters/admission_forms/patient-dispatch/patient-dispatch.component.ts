import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { Admission_FormDetail, Admission_FormSummary } from 'src/app/form_encounters/admission_forms/admission_form.model';
import { Admission_FormPersist } from 'src/app/form_encounters/admission_forms/admission_form.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCIdMode } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { AppTranslation } from "../../../app.translation";

@Component({
  selector: 'app-patient-dispatch',
  templateUrl: './patient-dispatch.component.html',
  styleUrls: ['./patient-dispatch.component.css']
})
export class PatientDispatchComponent implements OnInit {
  title: string;
  admissionFormDetail: Admission_FormDetail;
  isLoadingResults: boolean = false;


  constructor(
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PatientDispatchComponent>,
    public persist: Admission_FormPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) { }

  ngOnInit() {
    this.title = this.appTranslation.getText("patient", "dispatch_form");
    this.isLoadingResults = true;
      this.persist.getAdmission_Form(this.idMode.id).subscribe(admission_formDetail => {
        this.admissionFormDetail = admission_formDetail;
        this.admissionFormDetail.condition = -1;
        this.isLoadingResults = false;
      }, error => {
        console.error(error);
        this.isLoadingResults = false;
      })
  }

  dispatchPatient(): void {
    this.isLoadingResults = true;
    this.persist.admission_formDo(this.admissionFormDetail.id, "dispatch", this.admissionFormDetail).subscribe(value => {
      this.tcNotification.success("Patient dispatched");
      this.dialogRef.close()
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  canSubmit() {
    if (this.admissionFormDetail == null) {
      return false;
    }
    if (this.admissionFormDetail.condition == -1) {
      return false;
    }
    return true;
  }

  back(): void {
    this.location.back();
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
