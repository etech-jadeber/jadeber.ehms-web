import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AllergyFormEditComponent } from './allergy-form-edit.component';

describe('AllergyFormEditComponent', () => {
  let component: AllergyFormEditComponent;
  let fixture: ComponentFixture<AllergyFormEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AllergyFormEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllergyFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
