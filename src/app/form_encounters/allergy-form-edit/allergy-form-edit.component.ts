import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AllergyFormDetail, AllergySummary } from 'src/app/allergy/allergy.model';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { AllergyNavigator } from 'src/app/allergy/allergy.navigator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-allergy_form-edit',
  templateUrl: './allergy-form-edit.component.html',
  styleUrls: ['./allergy-form-edit.component.css']
})export class AllergyFormEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  allergyFormDetail: AllergyFormDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<AllergyFormEditComponent>,
              public  persist: Form_EncounterPersist,
              
              private allergyNavigator :AllergyNavigator,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  allergyName:string = "";

  searchAllergy() {
    let dialogRef = this.allergyNavigator.pickAllergys(true);
    dialogRef.afterClosed().subscribe((result: AllergySummary[]) => {
      if (result) {
        this.allergyName = result[0].name;
        this.allergyFormDetail.allergy_id =  result[0].id; 
        
      
      }
    });
  }


  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("allergy_forms");
      this.title = this.appTranslation.getText("general","new") +  " " + "allergy_form";
      this.allergyFormDetail = new AllergyFormDetail();
       this.allergyFormDetail.encounter_id = this.idMode.parentId;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("allergy_forms");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "allergy_form";
      this.isLoadingResults = true;
      this.persist.getAllergyForm(this.idMode.parentId,this.idMode.childId).subscribe(allergyFormDetail => {
        this.allergyFormDetail = allergyFormDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addAllergyForm(this.idMode.parentId,this.allergyFormDetail).subscribe(value => {
      this.tcNotification.success("allergyForm added");
      this.allergyFormDetail.id = value.id;
      this.dialogRef.close(this.allergyFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateAllergyForm(this.idMode.parentId,this.allergyFormDetail).subscribe(value => {
      this.tcNotification.success("allergy_form updated");
      this.dialogRef.close(this.allergyFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.allergyFormDetail == null){
            return false;
          }

if (this.allergyFormDetail.encounter_id == null || this.allergyFormDetail.encounter_id  == "") {
            return false;
        }
if (this.allergyFormDetail.allergy_id == null || this.allergyFormDetail.allergy_id  == "") {
            return false;
        }
if (this.allergyFormDetail.remark == null || this.allergyFormDetail.remark  == "") {
            return false;
        }

        return true;

 }
 }
