import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CaseTransferDetailComponent } from './case-transfer-detail.component';

describe('CaseTransferDetailComponent', () => {
  let component: CaseTransferDetailComponent;
  let fixture: ComponentFixture<CaseTransferDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseTransferDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseTransferDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
