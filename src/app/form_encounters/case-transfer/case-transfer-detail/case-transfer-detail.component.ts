import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {AppTranslation} from "../../../app.translation";


import {Case_TransferDetail} from "../case-transfer.model";
import {Case_TransferPersist} from "../case-transfer.persist";
import {Case_TransferNavigator} from "../case-transfer.navigator";

export enum Case_TransferTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-case_transfer-detail',
  templateUrl: './case-transfer-detail.component.html',
  styleUrls: ['./case-transfer-detail.component.css']
})
export class Case_TransferDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  case_transferLoading:boolean = false;

  Case_TransferTabs: typeof Case_TransferTabs = Case_TransferTabs;
  activeTab: Case_TransferTabs = Case_TransferTabs.overview;
  //basics
  case_transferDetail: Case_TransferDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public case_transferNavigator: Case_TransferNavigator,
              public  case_transferPersist: Case_TransferPersist) {
    this.tcAuthorization.requireRead("case_transfers");
    this.case_transferDetail = new Case_TransferDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("case_transfers");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.case_transferLoading = true;
    this.case_transferPersist.getCase_Transfer(id).subscribe(case_transferDetail => {
          this.case_transferDetail = case_transferDetail;
          this.case_transferLoading = false;
        }, error => {
          console.error(error);
          this.case_transferLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.case_transferDetail.id);
  }

  editCase_Transfer(): void {
    let modalRef = this.case_transferNavigator.editCase_Transfer(this.case_transferDetail.id);
    modalRef.afterClosed().subscribe(modifiedCase_TransferDetail => {
      TCUtilsAngular.assign(this.case_transferDetail, modifiedCase_TransferDetail);
    }, error => {
      console.error(error);
    });
  }

   printCase_Transfer():void{
      this.case_transferPersist.print(this.case_transferDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print case_transfer", true);
      });
    }

  back():void{
      this.location.back();
    }

}
