import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CaseTransferEditComponent } from './case-transfer-edit.component';

describe('CaseTransferEditComponent', () => {
  let component: CaseTransferEditComponent;
  let fixture: ComponentFixture<CaseTransferEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseTransferEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseTransferEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
