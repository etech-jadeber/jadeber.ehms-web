import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../../tc/authorization';
import { TCNotification } from '../../../tc/notification';
import { TCUtilsString } from '../../../tc/utils-string';
import { AppTranslation } from '../../../app.translation';

import { TCIdMode, TCModalModes, TCParentChildIds } from '../../../tc/models';

import { Case_TransferDetail } from '../case-transfer.model';
import { Case_TransferPersist } from '../case-transfer.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { Form_EncounterDetail } from '../../form_encounter.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import {physican_type, ServiceType} from "../../../app.enums";
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { Form_EncounterNavigator } from '../../form_encounter.navigator';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { Department_SpecialtyNavigator } from 'src/app/department_specialtys/department_specialty.navigator';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';

@Component({
  selector: 'app-case_transfer-edit',
  templateUrl: './case-transfer-edit.component.html',
  styleUrls: ['./case-transfer-edit.component.css'],
})
export class Case_TransferEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  case_transferDetail: Case_TransferDetail;
  form_encounterDetail: Form_EncounterDetail;
  caseTransfer: boolean = false;
  patientFullName: string = '';
  date: Date = new Date();
  doctorFullName: string="";
  physicalType = physican_type;
  departmentSpeciality:string;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<Case_TransferEditComponent>,
    public persist: Case_TransferPersist,
    public encounterPersist: Form_EncounterPersist,
    public patientPersist: PatientPersist,
    private tCUtilsDate: TCUtilsDate,
    public doctorNavigator: DoctorNavigator,
    public departmentSpecialityPersist: Department_SpecialtyPersist,
    public departmentSpecialityNavigator: Department_SpecialtyNavigator,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds,
    @Inject(MAT_DIALOG_DATA) public changeMode: TCParentChildIds
  ) {
    this.caseTransfer = changeMode.context['change'] == 'true' ? true : false;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isWizard(): boolean {
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] == TCModalModes.EDIT;
  }

  ngOnInit() {
    if (this.isNew()) {
      this.tcAuthorization.requireCreate('case_transfers');
      this.title =
        this.appTranslation.getText('general', 'new') +
        this.appTranslation.getText('patient', 'case_transfer');
        this.case_transferDetail = new Case_TransferDetail();
      //set necessary defaults
    } else if (this.isWizard()) {
      //change case
      this.title = this.appTranslation.getText('patient', 'case_transfer');
      this.case_transferDetail = new Case_TransferDetail();
      this.form_encounterDetail = new Form_EncounterDetail();
      this.isLoadingResults = true;

      this.encounterPersist.getForm_Encounter(this.ids.childId).subscribe(
        (form_encounterDetail) => {
          this.form_encounterDetail = form_encounterDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );

    } else {
      this.tcAuthorization.requireUpdate('case_transfers');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        this.appTranslation.getText('patient', 'case_transfer');
      this.isLoadingResults = true;
      this.persist.getCase_Transfer(this.ids.parentId).subscribe(
        (case_transferDetail) => {
          this.case_transferDetail = case_transferDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addCase_Transfer(this.ids.childId, this.case_transferDetail).subscribe(
      (value) => {
        this.tcNotification.success('Case_Transfer added');
        this.case_transferDetail.id = value.id;
        this.dialogRef.close(this.case_transferDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateCase_Transfer(this.case_transferDetail).subscribe(
      (value) => {
        this.tcNotification.success('Case_Transfer updated');
        this.dialogRef.close(this.case_transferDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.case_transferDetail == null) {
      return false;
    }

    if (
      this.case_transferDetail.reciever_proider_id == null ||
      this.case_transferDetail.reciever_proider_id == ""
    ) {
      return false;
    }

    return true;
  }
  searchDepartmentSpeciality() {
    this.departmentSpecialityPersist.parent_department_id = this.tcUtilsString.invalid_id
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, this.tcUtilsString.invalid_id);
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        if(this.case_transferDetail.department_speciality !=result[0].id){
        this.case_transferDetail.department_speciality = result[0].id;
        this.departmentSpeciality = result[0].name
        this.doctorFullName=null;
        this.case_transferDetail.reciever_proider_id=null;
        }
      }
    });
  }

  transferCase(): void {
    let caseTransfer: Case_TransferDetail = new Case_TransferDetail();
    caseTransfer.patient_id = this.form_encounterDetail.uuidd;
    caseTransfer.source_provider_id = this.form_encounterDetail.provider_id;
    caseTransfer.reciever_proider_id = this.case_transferDetail.reciever_proider_id;
    caseTransfer.date = this.tCUtilsDate.toTimeStamp(this.date);
    caseTransfer.remark = this.case_transferDetail.remark;
    caseTransfer.encounter = this.form_encounterDetail.id;


    this.persist.addCase_Transfer(this.ids.childId, caseTransfer).subscribe(
      (res) => {
        if (res.id != null) {
          this.dialogRef.close(this.case_transferDetail);
          this.tcNotification.success('Case re assigned');
        } else {
          this.tcNotification.info('Sorry we couldn\'t assign the docor');
          this.dialogRef.close(this.case_transferDetail);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor, this.case_transferDetail.department_speciality);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.doctorFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.case_transferDetail.reciever_proider_id = result[0].id;
      }
    });
  }
  
}
