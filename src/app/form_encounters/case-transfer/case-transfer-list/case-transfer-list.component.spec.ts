import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CaseTransferListComponent } from './case-transfer-list.component';

describe('CaseTransferListComponent', () => {
  let component: CaseTransferListComponent;
  let fixture: ComponentFixture<CaseTransferListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseTransferListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseTransferListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
