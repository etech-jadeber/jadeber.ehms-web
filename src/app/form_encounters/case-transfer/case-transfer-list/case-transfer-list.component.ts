import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';
import {
  Case_TransferSummary,
  Case_TransferSummaryPartialList,
} from '../case-transfer.model';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { AppTranslation } from 'src/app/app.translation';
import { Case_TransferPersist } from '../case-transfer.persist';
import { Case_TransferNavigator } from '../case-transfer.navigator';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { Form_EncounterNavigator } from '../../form_encounter.navigator';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorDetail, DoctorSummary } from 'src/app/doctors/doctor.model';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-case_transfer-list',
  templateUrl: './case-transfer-list.component.html',
  styleUrls: ['./case-transfer-list.component.css'],
})
export class Case_TransferListComponent implements OnInit {
  case_transfersData: Case_TransferSummary[] = [];
  case_transfersTotalCount: number = 0;
  case_transfersSelectAll: boolean = false;
  case_transfersSelection: Case_TransferSummary[] = [];

  case_transfersDisplayedColumns: string[] = [
    'select',
    'action',
    'patient_id',
    'source_provider_id',
    'reciever_proider_id',
    'date',
    'remark',
  ];
  case_transfersSearchTextBox: FormControl = new FormControl();
  case_transfersIsLoading: boolean = false;

  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();
  @ViewChild(MatPaginator, { static: true })
  case_transfersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) case_transfersSort: MatSort;
  doctors: DoctorSummary[] = [];
  patients: PatientSummary[] = [];

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public case_transferPersist: Case_TransferPersist,
    public case_transferNavigator: Case_TransferNavigator,
    public doctorNavigator: DoctorNavigator,
    public formEncounterNavigator: Form_EncounterNavigator,
    public patientNavigator: PatientNavigator,
    public doctorPersist: DoctorPersist,
    public patientPersist: PatientPersist,
    public jobPersist: JobPersist,
    public form_encounterPersist: Form_EncounterPersist,
  ) {
    this.tcAuthorization.requireRead('case_transfers');
    this.case_transfersSearchTextBox.setValue(
      case_transferPersist.case_transferSearchText
    );
    //delay subsequent keyup events
    this.case_transfersSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.case_transferPersist.case_transferSearchText = value;
        this.searchCase_Transfers();
      });
  }

  ngOnInit() {
    this.case_transferPersist.patientId = this.patientId
    this.loadPatients();
    this.getAllDoctors();
    this.case_transfersSort.sortChange.subscribe(() => {
      this.case_transfersPaginator.pageIndex = 0;
      this.searchCase_Transfers(true);
    });

    this.case_transfersPaginator.page.subscribe(() => {
      this.searchCase_Transfers(true);
    });
    //start by loading items
    this.searchCase_Transfers();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.case_transferPersist.encounterId = this.encounterId;
    } else {
    this.case_transferPersist.encounterId = null;
    }
    this.searchCase_Transfers()
    }

  searchCase_Transfers(isPagination: boolean = false): void {
    let paginator = this.case_transfersPaginator;
    let sorter = this.case_transfersSort;

    this.case_transfersIsLoading = true;
    this.case_transfersSelection = [];

    this.case_transferPersist
      .searchCase_Transfer(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: Case_TransferSummaryPartialList) => {
          this.case_transfersData = partialList.data;
          if (partialList.total != -1) {
            this.case_transfersTotalCount = partialList.total;
          }
          this.case_transfersIsLoading = false;
        },
        (error) => {
          this.case_transfersIsLoading = false;
        }
      );
  }

  downloadCase_Transfers(): void {
    if (this.case_transfersSelectAll) {
      this.case_transferPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download case_transfers',
          true
        );
      });
    } else {
      this.case_transferPersist
        .download(this.tcUtilsArray.idsList(this.case_transfersSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download case_transfers',
            true
          );
        });
    }
  }

  addCase_Transfer(): void {
    let dialogRef = this.case_transferNavigator.addCase_Transfer(this.encounterId);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchCase_Transfers();
      }
    });
  }

  editCase_Transfer(item: Case_TransferSummary) {
    let dialogRef = this.case_transferNavigator.editCase_Transfer(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteCase_Transfer(item: Case_TransferSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Case_Transfer');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.case_transferPersist.deleteCase_Transfer(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Case_Transfer deleted');
            this.searchCase_Transfers();
          },
          (error) => {}
        );
      }
    });
  }

  loadPatients() {
    this.patientPersist
      .patientsDo("get_all_patients", "")
      .subscribe((result) => {
        this.patients = (result as PatientSummaryPartialList).data;
      });
  }

  getPatientFullName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patients,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }


  getDoctorFullName(doctorId: string): string {
    let doctors: DoctorSummary = this.tcUtilsArray.getById(
      this.doctors,
      doctorId
    );
    if (doctors) {
      return `${doctors.first_name} ${doctors.middle_name} ${doctors.last_name}`;
    }

  }

  getAllDoctors(): void {
    this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
      this.doctors = doctors;
    });
  }

  back(): void {
    this.location.back();
  }
}
