import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CaseTransferPickComponent } from './case-transfer-pick.component';

describe('CaseTransferPickComponent', () => {
  let component: CaseTransferPickComponent;
  let fixture: ComponentFixture<CaseTransferPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseTransferPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseTransferPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
