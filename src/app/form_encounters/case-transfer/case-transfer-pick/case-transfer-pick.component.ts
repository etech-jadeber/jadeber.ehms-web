import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { debounceTime } from 'rxjs/operators';

import { TCAuthorization } from '../../../tc/authorization';
import { TCUtilsAngular } from '../../../tc/utils-angular';
import { TCUtilsArray } from '../../../tc/utils-array';
import { TCUtilsDate } from '../../../tc/utils-date';
import { TCNotification } from '../../../tc/notification';
import { TCNavigator } from '../../../tc/navigator';
import { AppTranslation } from '../../../app.translation';

import {
  Case_TransferDetail,
  Case_TransferSummary,
  Case_TransferSummaryPartialList,
} from '../case-transfer.model';
import { Case_TransferPersist } from '../case-transfer.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';

@Component({
  selector: 'app-case_transfer-pick',
  templateUrl: './case-transfer-pick.component.html',
  styleUrls: ['./case-transfer-pick.component.css'],
})
export class Case_TransferPickComponent implements OnInit {
  case_transfersData: Case_TransferSummary[] = [];
  case_transfersTotalCount: number = 0;
  case_transfersSelection: Case_TransferSummary[] = [];
  case_transfersDisplayedColumns: string[] = [
    'select',
    'patient_id',
    'source_provider_id',
    'reciever_proider_id',
    'date',
    'remark',
    'encounter',
  ];

  case_transfersSearchTextBox: FormControl = new FormControl();
  case_transfersIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true })
  case_transfersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) case_transfersSort: MatSort;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public case_transferPersist: Case_TransferPersist,
    public dialogRef: MatDialogRef<Case_TransferPickComponent>,
    public tcUtilsString: TCUtilsString,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('case_transfers');
    this.case_transfersSearchTextBox.setValue(
      case_transferPersist.case_transferSearchText
    );
    //delay subsequent keyup events
    this.case_transfersSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.case_transferPersist.case_transferSearchText = value;
        this.searchCase_Transfers();
      });
  }

  ngOnInit() {
    this.case_transfersSort.sortChange.subscribe(() => {
      this.case_transfersPaginator.pageIndex = 0;
      this.searchCase_Transfers();
    });

    this.case_transfersPaginator.page.subscribe(() => {
      this.searchCase_Transfers();
    });

    //set initial picker list to 5
    this.case_transfersPaginator.pageSize = 5;

    //start by loading items
    this.searchCase_Transfers();
  }

  searchCase_Transfers(): void {
    this.case_transfersIsLoading = true;
    this.case_transfersSelection = [];

    this.case_transferPersist
      .searchCase_Transfer(
        this.case_transfersPaginator.pageSize,
        this.case_transfersPaginator.pageIndex,
        this.case_transfersSort.active,
        this.case_transfersSort.direction
      )
      .subscribe(
        (partialList: Case_TransferSummaryPartialList) => {
          this.case_transfersData = partialList.data;
          if (partialList.total != -1) {
            this.case_transfersTotalCount = partialList.total;
          }
          this.case_transfersIsLoading = false;
        },
        (error) => {
          this.case_transfersIsLoading = false;
        }
      );
  }

  markOneItem(item: Case_TransferSummary) {
    if (!this.tcUtilsArray.containsId(this.case_transfersSelection, item.id)) {
      this.case_transfersSelection = [];
      this.case_transfersSelection.push(item);
    } else {
      this.case_transfersSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.case_transfersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
