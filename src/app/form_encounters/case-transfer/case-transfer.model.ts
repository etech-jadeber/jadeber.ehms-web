import { TCId } from "src/app/tc/models";

export class Case_TransferSummary extends TCId {
  patient_id : string;
source_provider_id : string;
reciever_proider_id : string;
date : number;
remark : string;
encounter : string;
department_speciality: string;
}

export class Case_TransferSummaryPartialList {
  data: Case_TransferSummary[];
  total: number;
}

export class Case_TransferDetail extends Case_TransferSummary {
  patient_id : string;
  source_provider_id : string;
  reciever_proider_id : string;
  date : number;
  remark : string;
  encounter : string;
}
