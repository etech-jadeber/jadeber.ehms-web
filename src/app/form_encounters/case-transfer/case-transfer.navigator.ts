import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";import { Case_TransferEditComponent } from "./case-transfer-edit/case-transfer-edit.component";
import { TcDictionary, TCIdMode, TCModalModes, TCParentChildIds } from "src/app/tc/models";
import { TCModalWidths } from "src/app/tc/utils-angular";
import { Case_TransferPickComponent } from "./case-transfer-pick/case-transfer-pick.component";



@Injectable({
  providedIn: 'root'
})

export class Case_TransferNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  case_transfersUrl(): string {
    return "/case_transfers";
  }

  case_transferUrl(id: string): string {
    return "/case_transfers/" + id;
  }

  viewCase_Transfers(): void {
    this.router.navigateByUrl(this.case_transfersUrl());
  }

  viewCase_Transfer(id: string): void {
    this.router.navigateByUrl(this.case_transferUrl(id));
  }

  editCase_Transfer(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Case_TransferEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCase_Transfer(parent_id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Case_TransferEditComponent, {
          data: new TCIdMode(parent_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickCase_Transfers(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Case_TransferPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }

    transferCase(id: string, change:boolean): MatDialogRef<unknown,any> {
      let params: TcDictionary<string> = new TcDictionary();
      params['mode'] = TCModalModes.WIZARD;
      params['change'] = "true";
      const dialogRef = this.dialog.open(Case_TransferEditComponent, {
        data: new TCParentChildIds(null, id, params),
        width: TCModalWidths.medium
      });
      return dialogRef;
    }
}
