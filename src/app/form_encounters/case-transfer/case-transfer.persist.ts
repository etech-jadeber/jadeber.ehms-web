import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {Case_TransferDetail, Case_TransferSummaryPartialList} from "./case-transfer.model";
import { TCDoParam, TCUrlParams, TCUtilsHttp } from 'src/app/tc/utils-http';
import { TcDictionary, TCId } from 'src/app/tc/models';
import { environment } from 'src/environments/environment';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Case_TransferPersist {

  case_transferSearchText: string = "";
  patientId: string;
  encounterId: string;

  constructor(private http: HttpClient) {
  }

  searchCase_Transfer( pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Case_TransferSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("case_transfers", this.case_transferSearchText, pageSize, pageIndex, sort, order);
    if (this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    }
    if (this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter", this.encounterId)
    }
    return this.http.get<Case_TransferSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.case_transferSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "case_transfers/do", new TCDoParam("download_all", this.filters()));
  }


  getCase_Transfer(id: string): Observable<Case_TransferDetail> {
    return this.http.get<Case_TransferDetail>(environment.tcApiBaseUri + "case_transfers/" + id);
  }

  addCase_Transfer(parent_id: string, item: Case_TransferDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parent_id + "/case_transfers", item);
  }

  updateCase_Transfer(item: Case_TransferDetail): Observable<Case_TransferDetail> {
    return this.http.patch<Case_TransferDetail>(environment.tcApiBaseUri + "case_transfers/" + item.id, item);
  }

  updateCaseAndEncounter(item: Case_TransferDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "case_transfers/encounter", item);
  }

  deleteCase_Transfer(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "case_transfers/" + id);
  }

  case_transfersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "case_transfers/do", new TCDoParam(method, payload));
  }

  case_transferDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "case_transfers/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "case_transfers/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "case_transfers/" + id + "/do", new TCDoParam("print", {}));
  }


}
