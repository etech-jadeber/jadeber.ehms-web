import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { ConsultingDetail } from '../form_encounter.model';
import {Form_EncounterPersist } from '../form_encounter.persist'
import {Form_EncounterNavigator} from "../form_encounter.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorDetail, DoctorSummary } from 'src/app/doctors/doctor.model';
import { DepartmentSummary } from 'src/app/departments/department.model';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';

export enum ConsultingTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-consulting-detail',
  templateUrl: './consulting-detail.component.html',
  styleUrls: ['./consulting-detail.component.css']
})
export class ConsultingDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  consultingIsLoading:boolean = false;
  
  ConsultingTabs: typeof ConsultingTabs = ConsultingTabs;
  activeTab: ConsultingTabs = ConsultingTabs.overview;
  //basics

  consultingDetail: ConsultingDetail;
  doctors: DoctorSummary[] = [];
  departments: DepartmentSummary[] = [];
  departmentsSpeciality:Department_SpecialtySummary[]=[];
  
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate:TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public consultingNavigator: Form_EncounterNavigator,
              public  consultingPersist: Form_EncounterPersist,
              public doctorPersist: DoctorPersist,
              public departmentPersist: DepartmentPersist, 
              public department_specialtyPersist:Department_SpecialtyPersist,
              ) {
    this.tcAuthorization.requireRead("consultings");
    this.consultingDetail = new ConsultingDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("consultings");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }
   loadConsultingSpecialityDepartment():void {
    this.department_specialtyPersist.searchDepartment_Specialty(50, 0, "", "")
    .subscribe((result) => {
      this.departmentsSpeciality = result.data;
    })
    
  }
  loadDetails(id: string): void {
  this.consultingIsLoading = true;
    this.consultingPersist.getConsulting(id).subscribe(consultingDetail => {
          this.consultingDetail = consultingDetail;
          this.consultingIsLoading = false;
          this.loadConsultingDoctor();
          this.loadConsultingDepartment();
          this.loadConsultingSpecialityDepartment();
        }, error => {
          console.error(error);
          this.consultingIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.consultingDetail.id);
  }
  loadConsultingDepartment():void {
    this.departmentPersist.searchDepartment(50, 0, "", "")
    .subscribe((result) => {
      this.departments = result.data;
    })
    
  }
  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(
      this.doctors,
      doctorId
    );

    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`;
    }
    return '';
  }

  loadConsultingDoctor():void {
    this.doctorPersist.searchDoctor(50, 0, "", "")
    .subscribe((result)=> {
      this.doctors = result.data;
    })
  }

  editConsulting(): void {
    let modalRef = this.consultingNavigator.editConsulting("",this.consultingDetail.id);
    modalRef.afterClosed().subscribe(consultingDetail => {
      TCUtilsAngular.assign(this.consultingDetail, consultingDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.consultingPersist.print(this.consultingDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print consulting", true);
      });
    }

  back():void{
      this.location.back();
    }

}