import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ConsultingEditComponent } from './consulting-edit.component';

describe('ConsultingEditComponent', () => {
  let component: ConsultingEditComponent;
  let fixture: ComponentFixture<ConsultingEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultingEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultingEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
