import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ConsultingDetail } from '../form_encounter.model';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { id } from '@swimlane/ngx-charts';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DepartmentSummary } from 'src/app/departments/department.model';
import { DepartmentNavigator } from 'src/app/departments/department.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { Department_SpecialtyNavigator } from 'src/app/department_specialtys/department_specialty.navigator';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { physican_type } from 'src/app/app.enums';
import { Admission_FormPersist } from '../admission_forms/admission_form.persist';
@Component({
  selector: 'app-consulting-edit',
  templateUrl: './consulting-edit.component.html',
  styleUrls: ['./consulting-edit.component.css']
})
export class ConsultingEditComponent implements OnInit {
  department :DepartmentSummary[];
  department_speciality_id:string;
  departmentSpeciality:string;
  isLoadingResults: boolean = false;
  title: string;
  doctors: DoctorSummary [] = [];
  departments: DepartmentSummary[] = [];
  departmentName: string;
  doctorFullName: string;
  date: Date = new Date();
  consultingDetail: ConsultingDetail = new ConsultingDetail();
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ConsultingEditComponent>,
              public  persist: Form_EncounterPersist,
              public departmentPersist: DepartmentPersist,
              public doctorPersist: DoctorPersist,
              public tcUtilsDate: TCUtilsDate,
              public departmentNavigator: DepartmentNavigator,
              public doctorNavigator: DoctorNavigator,
              public tCUtilsDate: TCUtilsDate,
              public admissionFormPersist: Admission_FormPersist,
              public departmentSpecialtyPersist: Department_SpecialtyPersist,
              public departmentSpecialityNavigator: Department_SpecialtyNavigator,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }


  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    console.log("The ids outside are", this.ids) 
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("consultings");
      this.title = this.appTranslation.getText("general","new") +  " " + "consulting";
      this.consultingDetail.encounter_id = this.ids.parentId;
      this.consultingDetail.patient_convinience = false;
      this.consultingDetail.urgent = false;
      //set necessary defaults

    } else {
      console.log("The ids are", this.ids)
     this.tcAuthorization.requireUpdate("consultings");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "consulting";
      this.isLoadingResults = true;
      this.persist.getConsulting(this.ids.childId).subscribe(consultingDetail => {
        this.consultingDetail = consultingDetail;
        this.loadConsultingDepartmentSpeciality(consultingDetail.consulting_dept_id)
        this.loadConsultingDoctor(consultingDetail.consulting_doctor_id);
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } 
  
  loadConsultingDepartmentSpeciality(id:string):void {
    this.departmentSpecialtyPersist.getDepartment_Specialty(id).subscribe((result) => {
      if(result){
        this.departmentSpeciality=result.name;
      }
    })
    
  }
  loadConsultingDoctor(id:string):void {
    this.doctorPersist.getDoctor(id)
    .subscribe((result)=> {
      if(result){
        this.doctorFullName=result.first_name + ' ' + result.middle_name
      }
    })
  }

  searchDepartmentSpeciality() {
    let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, this.tcUtilsString.invalid_id );
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        this.consultingDetail.consulting_dept_id = result[0].id;
        this.departmentSpeciality=result[0].name;

      }
    });
  }
  searchdoctor() {
    // this.doctorPersist.department_id = this.consultingDetail.consulting_dept_id;
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor, this.consultingDetail.consulting_dept_id);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.doctorFullName = result[0].first_name + ' ' + result[0].middle_name;
        this.consultingDetail.consulting_doctor_id = result[0].id;
      }
    });
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.consultingDetail.date = this.tCUtilsDate.toTimeStamp(this.date);


    } else {
      this.date = this.tCUtilsDate.toDate(this.consultingDetail.date);


    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
   this.transformDates(true);
    this.persist.addConsulting(this.consultingDetail).subscribe(value => {
      this.tcNotification.success("consulting added");
      this.consultingDetail.id = value.id;
      this.dialogRef.close(this.consultingDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.updateConsulting(this.consultingDetail).subscribe(value => {
      this.tcNotification.success("consulting updated");
      this.dialogRef.close(this.consultingDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.consultingDetail == null){
            return false;
          }

if (this.consultingDetail.encounter_id == null || this.consultingDetail.encounter_id  == "") {
            return false;
        }
if (this.consultingDetail.consulting_dept_id == null || this.consultingDetail.consulting_dept_id  == "") {
            return false;
        }
if (this.consultingDetail.consulting_doctor_id == null || this.consultingDetail.consulting_doctor_id  == "") {
            return false;
        }

if (this.consultingDetail.urgent == null) {
            return false;
        }
if (!this.consultingDetail.patient_convinience) {
            return false;
        }
if (this.consultingDetail.brief_history == null || this.consultingDetail.brief_history  == "") {
            return false;
        }
if (this.consultingDetail.reason_for_consult == null || this.consultingDetail.reason_for_consult  == "") {
  return false;
}
if (this.consultingDetail.patient_condition == null) {
  return false;
}
        return true;
 }
 }