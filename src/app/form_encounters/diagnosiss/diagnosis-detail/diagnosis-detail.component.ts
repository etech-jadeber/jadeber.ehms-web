import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {AppTranslation} from "../../../app.translation";


import {DiagnosisDetail} from "../diagnosis.model";
import {DiagnosisPersist} from "../diagnosis.persist";
import {DiagnosisNavigator} from "../diagnosis.navigator";

export enum DiagnosisTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-diagnosis-detail',
  templateUrl: './diagnosis-detail.component.html',
  styleUrls: ['./diagnosis-detail.component.css']
})
export class DiagnosisDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  diagnosisLoading:boolean = false;
  
  DiagnosisTabs: typeof DiagnosisTabs = DiagnosisTabs;
  activeTab: DiagnosisTabs = DiagnosisTabs.overview;
  //basics
  diagnosisDetail: DiagnosisDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public diagnosisNavigator: DiagnosisNavigator,
              public  diagnosisPersist: DiagnosisPersist) {
    this.tcAuthorization.requireRead("diagnosiss");
    this.diagnosisDetail = new DiagnosisDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("diagnosiss");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.diagnosisLoading = true;
    this.diagnosisPersist.getDiagnosis(id).subscribe(diagnosisDetail => {
          this.diagnosisDetail = diagnosisDetail;
          this.diagnosisLoading = false;
        }, error => {
          console.error(error);
          this.diagnosisLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.diagnosisDetail.id);
  }

  editDiagnosis(): void {
    let modalRef = this.diagnosisNavigator.editDiagnosis(this.diagnosisDetail.id);
    modalRef.afterClosed().subscribe(modifiedDiagnosisDetail => {
      TCUtilsAngular.assign(this.diagnosisDetail, modifiedDiagnosisDetail);
    }, error => {
      console.error(error);
    });
  }

   printDiagnosis():void{
      this.diagnosisPersist.print(this.diagnosisDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print diagnosis", true);
      });
    }

  back():void{
      this.location.back();
    }

}

