import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DiagnosisEditComponent } from './diagnosis-edit.component';

describe('DiagnosisEditComponent', () => {
  let component: DiagnosisEditComponent;
  let fixture: ComponentFixture<DiagnosisEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnosisEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
