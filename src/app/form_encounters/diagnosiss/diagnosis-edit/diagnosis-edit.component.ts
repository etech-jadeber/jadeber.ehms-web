import {Component, OnInit, Inject, Input, Injectable, EventEmitter, Output} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification}  from "../../../tc/notification";
import {TCUtilsString}   from "../../../tc/utils-string";
import {AppTranslation} from "../../../app.translation";

import {TCIdMode, TCModalModes} from "../../../tc/models";

import {OrdersPersist as form_encounterOrderPersist, OrdersPersist} from 'src/app/form_encounters/orderss/orders.persist';


import {DiagnosisDetail} from "../diagnosis.model";
import {DiagnosisPersist} from "../diagnosis.persist";
import { DiagnosisCodes } from '../../procedure_order_codes/procedure_order_code.model';
import { Procedure_Order_CodeNavigator } from '../../procedure_order_codes/procedure_order_code.navigator';
import { Icd11Summary } from 'src/app/icd_11/icd_11.model';
import { diagnosis_certainty, diagnosis_order, diagnosis_status, lab_order_type, Occurance, OrderType, writtenBy } from 'src/app/app.enums';
import { Icd11Persist } from 'src/app/icd_11/icd_11.persist';
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { Form_EncounterNavigator } from '../../form_encounter.navigator';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { Order } from 'src/app/lab_panels/lab_panel.model';


@Injectable()
abstract class DiagnosisEditMainComponent  implements OnInit{
  lab_order_type = lab_order_type;
  is_new: boolean = false;
  isLoadingResults: boolean = false;
  title: string;
  diagnosisDetail: DiagnosisDetail;
  diagnosisDescription: string;
  icd11_diagnosis_options: Icd11Summary[];
  isActive:boolean;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public  persist: DiagnosisPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public form_encounterNavigator: Form_EncounterNavigator,
              public labOrderPersist: OrdersPersist,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public icd11Persist : Icd11Persist,
              @Inject(MAT_DIALOG_DATA) public idMode: {id:string,mode:string,isActive:boolean}) {

  }

  onCancel(): void {
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    this.isActive = this.idMode.isActive || this.form_encounterPersist.encounter_is_active;
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("diagnosiss");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("patient", "diagnosis");
      this.diagnosisDetail = new DiagnosisDetail();
      this.diagnosisDetail.note = "";
      this.diagnosisDetail.certainity = diagnosis_certainty.presumed;
      this.diagnosisDetail.occurance = Occurance.new_occurance;
      this.diagnosisDetail.status = diagnosis_status.active;
      this.diagnosisDetail.diagnosis_order = diagnosis_order.primary;
      this.diagnosisDetail.plan = "";
      this.diagnosisDetail.pertinent_investigation_finding = "";
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("diagnosiss");
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("patient", "diagnosis");
      this.isLoadingResults = true;
      this.persist.getDiagnosis(this.idMode.id).subscribe(diagnosisDetail => {
        this.diagnosisDetail = diagnosisDetail;
        this.isLoadingResults = false;
        this.diagnosisDescription=diagnosisDetail.name;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  addOrder(
    orderType: number,
    isWizard: boolean = true,
    date: number = this.tcUtilsDate.toTimeStamp(new Date())
  ): void {
    if(this.tcAuthorization.requireCreate(this.tcUtilsArray.getEnum(this.labOrderPersist.lab_order_acl, orderType)?.name)){
      let dialogRef = this.form_encounterNavigator.addLab(
        isWizard,
        this.diagnosisDetail.encounter_id,
        null,
        date,
        [orderType]
      );
      dialogRef.afterClosed().subscribe((result: Order) => {
        if (result) {
          console.log(result)
          // this.isLoadingResults = true;
          // this.outside_orderPersist.addOutside_Orders({...this.outside_ordersDetail, payload: result}).subscribe(value => {
          //   this.outside_ordersDetail.id = value.id;
          //     this.dialogRef.close(this.outside_ordersDetail)
          //     this.tcNotification.success('Successfully Added');
          // }, error => {
          //   this.isLoadingResults = false;
          //   console.error(error)
          // })
        }
      });
    }
  }


  isEmptyString(value: string): boolean {
    return value == "" || value == null;
  }

  isVisible(value: string):boolean {
    return !this.isEmptyString(value) || this.form_encounterPersist.encounter_is_active;
  }


  canSubmit():boolean{
        if (this.diagnosisDetail == null){
            return false;
          }
          if (!this.diagnosisDetail.name){
            return false;
          }

        // if (this.diagnosisDetail.name == null || this.diagnosisDetail.name  == "") {
        //     return false;
        //   }
        // if (!this.diagnosisDetail.diagnosis_order) {
        //   return false;
        // }

        // if (!this.diagnosisDetail.certainity){
        //   return false;
        // }

        // if (this.diagnosisDetail.status == null){
        //   return false;
        // }
        // if (this.diagnosisDetail.occurance == null){
        //   return false;
        // }

        return true;
      }

    pickDiagnosisCode() {
      const dialogRef = this.procedureOrderCodeNavigator.pick_diagnosis_code();
      dialogRef.afterClosed().subscribe((result: DiagnosisCodes ) => {
        if (result) {
          this.diagnosisDescription = result.description;
          this.diagnosisDetail.name = result.description;
        }
      })
    }
    appendString(value: string):string{
      value = value.trim();
      if (!this.diagnosisDetail.name)
        return "\u2022  " + value;
      if(this.diagnosisDetail.name.endsWith("\n"))
        return this.diagnosisDetail.name + "\u2022  " + value;
      let x = this.diagnosisDetail.name.split('\u2022  ');
      x.pop();
      let y = x.join("\u2022  ")
      return  y ?  y+"\u2022  " +value : "\u2022  " + value;
    }
    handleInput(event: string, input) {
      if(event && event.length == 1 && !event.endsWith('\u2022'))
        input.value = "\u2022  " + event
      else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
        input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
      if (event && !input.value.endsWith("\n"))
      this.icd11Persist.icd11SearchText =input.value.split('\u2022').pop().trim();
      else
        this.icd11Persist.icd11SearchText = "";
      this.load_icd11Diagnois();
    }
    load_icd11Diagnois(){
      this.icd11Persist.searchIcd11(5,0,"","").subscribe((result)=>{
        if(result)
          this.icd11_diagnosis_options = result.data;
      })
    }
    pickIcd11() {
      const dialogRef = this.procedureOrderCodeNavigator.pick_icd_11();
      dialogRef.afterClosed().subscribe((result: Icd11Summary[] ) => {
        if (result) {
          let value :string[]= [] ;
          result.forEach(val=> value.push(val.category))
          this.diagnosisDetail.name = value.join(", ");
        }
      })
    }
ngOnDestroy():void{
  this.icd11Persist.icd11SearchText = "";
}

}

@Component({
  selector: 'app-diagnosis-edit',
  templateUrl: './diagnosis-edit.component.html',
  styleUrls: ['./diagnosis-edit.component.css']
})
export class DiagnosisEditComponent extends DiagnosisEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  diagnosisDetail: DiagnosisDetail;
  diagnosisDescription: string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DiagnosisEditComponent>,
              public  persist: DiagnosisPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public form_encounterNavigator: Form_EncounterNavigator,
              public labOrderPersist: OrdersPersist,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public icd11Persist : Icd11Persist,
              @Inject(MAT_DIALOG_DATA) public idMode: {id:string,mode:string,isActive:boolean}) {
                super(tcAuthorization, tcNotification, tcUtilsString,tcUtilsDate,tcUtilsArray ,appTranslation ,persist, form_encounterPersist,form_encounterNavigator,labOrderPersist,procedureOrderCodeNavigator, icd11Persist ,idMode)

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.diagnosisDetail.written_by = this.idMode.isActive ? writtenBy.nurse : writtenBy.doctor;
    this.persist.addDiagnosis(this.idMode.id, this.diagnosisDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "diagnosis") + " " + this.appTranslation.getText("general", "added"));
      this.diagnosisDetail.id = value.id;
      this.dialogRef.close(this.diagnosisDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDiagnosis(this.diagnosisDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "diagnosis") + " " + this.appTranslation.getText("general", "updated"));
      this.dialogRef.close(this.diagnosisDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}

@Component({
  selector: 'app-diagnosis-edit-list',
  templateUrl: './diagnosis-edit.component.html',
  styleUrls: ['./diagnosis-edit.component.css']
})
export class DiagnosisEDitListComponent extends DiagnosisEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  diagnosisDetail: DiagnosisDetail;
  diagnosisDescription: string;
  dialogRef = null;

  @Input() encounterId: string;
  @Output() assign_value = new EventEmitter<any>();
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public  persist: DiagnosisPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public form_encounterNavigator: Form_EncounterNavigator,
              public labOrderPersist: OrdersPersist,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public icd11Persist : Icd11Persist,
              ) {
                super(tcAuthorization, tcNotification, tcUtilsString,tcUtilsDate,tcUtilsArray ,appTranslation ,persist, form_encounterPersist,form_encounterNavigator,labOrderPersist,procedureOrderCodeNavigator, icd11Persist,{id:null,mode:TCModalModes.NEW,isActive:false})

  }


  onAdd(): void {
    this.isLoadingResults = true;
    this.diagnosisDetail.written_by = this.idMode.isActive ? writtenBy.nurse : writtenBy.doctor;
    this.persist.addDiagnosis(this.diagnosisDetail.encounter_id, this.diagnosisDetail).subscribe(value => {
      // this.tcNotification.success(this.appTranslation.getText("patient", "diagnosis") + " " + this.appTranslation.getText("general", "added"));
      this.diagnosisDetail.id = value.id;
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDiagnosis(this.diagnosisDetail).subscribe(value => {
      // this.tcNotification.success(this.appTranslation.getText("patient", "diagnosis") + " " + this.appTranslation.getText("general", "updated"));
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })

  }

  ngOnInit(): void {
    this.isActive = this.idMode.isActive || this.form_encounterPersist.encounter_is_active;
    this.persist.written_by = writtenBy.doctor;
    this.persist.searchDiagnosis(this.encounterId, 1, 0, 'date', 'desc').subscribe(
      diagnosis => {
        if(diagnosis.data.length){
          this.diagnosisDetail = diagnosis.data[0];
        }
        else{
            this.assign_value.emit({new_assessment: true })
            this.diagnosisDetail = new DiagnosisDetail();
            this.diagnosisDetail.note = "";
            this.diagnosisDetail.certainity = diagnosis_certainty.presumed;
            this.diagnosisDetail.occurance = Occurance.new_occurance;
            this.diagnosisDetail.status = diagnosis_status.active;
            this.diagnosisDetail.diagnosis_order = diagnosis_order.primary;
            this.diagnosisDetail.plan = "";
            this.diagnosisDetail.pertinent_investigation_finding = "";
            this.diagnosisDetail.encounter_id = this.encounterId
        }
      }
    )
  }

  saveResult(){
    if (!this.canSubmit()){
      return;
    }
    this.diagnosisDetail.encounter_id = this.encounterId;
    if (this.diagnosisDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }

  isNew(): boolean {
    return false
  }
  ngOnDestroy(): void {
      this.persist.written_by = undefined;
      this.icd11Persist.icd11SearchText = "";
  }
}
