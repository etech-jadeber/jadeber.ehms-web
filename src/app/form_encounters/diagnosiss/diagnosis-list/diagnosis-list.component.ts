import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";

import {DiagnosisPersist} from "../diagnosis.persist";
import {DiagnosisNavigator} from "../diagnosis.navigator";
import {DiagnosisDetail, DiagnosisSummary, DiagnosisSummaryPartialList} from "../diagnosis.model";
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { physican_type, writtenBy } from 'src/app/app.enums';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';


@Component({
  selector: 'app-diagnosis-list',
  templateUrl: './diagnosis-list.component.html',
  styleUrls: ['./diagnosis-list.component.css']
})
export class DiagnosisListComponent implements OnInit {

  diagnosissData: DiagnosisSummary[] = [];
  diagnosissTotalCount: number = 0;
  diagnosissSelectAll:boolean = false;
  diagnosissSelection: DiagnosisSummary[] = [];

  diagnosissDisplayedColumns: string[] = ["select","action", "name","certainity","note","date" ];
  diagnosissSearchTextBox: FormControl = new FormControl();
  diagnosissIsLoading: boolean = false;
  @Input() from_nurse :boolean;
  isActive:boolean;

  @Input() role: number;
  @Input() encounterId: string;
  @Input() edit: boolean=true;
  @Output() onResult = new EventEmitter<number>();

  user : {[id:string]:UserDetail} = {}

  nurseName:string;
  nurseId : string;
  date:FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  @ViewChild(MatPaginator, {static: true}) diagnosissPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) diagnosissSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public diagnosisPersist: DiagnosisPersist,
                public diagnosisNavigator: DiagnosisNavigator,
                public jobPersist: JobPersist,
                public form_encouterPersist: Form_EncounterPersist,
                public doctorPersist: DoctorPersist,
                public doctorNavigator: DoctorNavigator,
                public userPersist: UserPersist,
    ) {
        this.tcAuthorization.requireRead("diagnosiss");
        this.diagnosisPersist.date = tcUtilsDate.toTimeStamp(new Date());
       this.diagnosissSearchTextBox.setValue(diagnosisPersist.diagnosisSearchText);
      //delay subsequent keyup events
      this.diagnosissSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.diagnosisPersist.diagnosisSearchText = value;
        this.searchDiagnosiss();
      });
      this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.diagnosisPersist.date = tcUtilsDate.toTimeStamp(value);
        this.searchDiagnosiss();
      });
    }

    ngOnInit() {

      this.doctorPersist.getDocotrSelf().subscribe((nurse: DoctorSummary)=>{
        if(nurse){
          this.nurseName = nurse.first_name + " " + nurse.middle_name;
          this.diagnosisPersist.nurse_id = nurse.login_id;
        }
      })

      this.isActive = this.from_nurse || this.form_encouterPersist.encounter_is_active;

      if(this.from_nurse){
        this.diagnosissDisplayedColumns = [...this.diagnosissDisplayedColumns.slice(0, 3), 'nurse', ...this.diagnosissDisplayedColumns.slice(5)]
      }
       this.diagnosisPersist.written_by = this.from_nurse ? writtenBy.nurse: writtenBy.doctor;
      this.diagnosissSort.sortChange.subscribe(() => {
        this.diagnosissPaginator.pageIndex = 0;
        this.searchDiagnosiss(true);
      });

      this.diagnosissPaginator.page.subscribe(() => {
        this.searchDiagnosiss(true);
      });
      //start by loading items
      this.searchDiagnosiss();
    }

  searchDiagnosiss(isPagination:boolean = false): void {


    let paginator = this.diagnosissPaginator;
    let sorter = this.diagnosissSort;

    this.diagnosissIsLoading = true;
    this.diagnosissSelection = [];

    this.diagnosisPersist.searchDiagnosis(this.encounterId, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: DiagnosisSummaryPartialList) => {
      this.diagnosissData = partialList.data;
      this.diagnosissData.forEach(diagnosis =>{
        if(diagnosis.user_id && !this.user[diagnosis.user_id]){
          this.userPersist.getUser(diagnosis.user_id).subscribe((_user)=>{
            if(_user){
              this.user[diagnosis.user_id] = _user;
            }
          })
        }
      })
      if (partialList.total != -1) {
        this.diagnosissTotalCount = partialList.total;
      }
      this.diagnosissIsLoading = false;
    }, error => {
      this.diagnosissIsLoading = false;
    });

  }


  searchNurses():void{
    let dialogRef = this.doctorNavigator.pickDoctors(true,physican_type.nurse);

    dialogRef.afterClosed().subscribe((nurse:DoctorSummary[])=>{
      if(nurse){
        this.nurseName = nurse[0].first_name + " " + nurse[0].middle_name;
        this.diagnosisPersist.nurse_id = nurse[0].login_id;
        this.searchDiagnosiss();
      }
    })
  }

  downloadDiagnosiss(): void {
    if(this.diagnosissSelectAll){
         this.diagnosisPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download diagnosiss", true);
      });
    }
    else{
        this.diagnosisPersist.download(this.tcUtilsArray.idsList(this.diagnosissSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download diagnosiss",true);
            });
        }
  }



  addDiagnosis(): void {
    let dialogRef = this.diagnosisNavigator.addDiagnosis(this.encounterId,this.isActive);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchDiagnosiss();
      }
    });
  }

  editDiagnosis(item: DiagnosisSummary) {
    let dialogRef = this.diagnosisNavigator.editDiagnosis(item.id, this.isActive);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteDiagnosis(item: DiagnosisSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Diagnosis");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.diagnosisPersist.deleteDiagnosis(item.id).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("patient", "diagnosis") + " " + this.appTranslation.getText("general", "deleted"));
          this.searchDiagnosiss();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

    ngOnDestroy():void{
      this.diagnosisPersist.written_by = undefined;
    }

}

