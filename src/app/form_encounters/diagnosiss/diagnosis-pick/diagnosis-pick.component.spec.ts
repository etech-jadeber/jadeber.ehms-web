import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DiagnosisPickComponent } from './diagnosis-pick.component';

describe('DiagnosisPickComponent', () => {
  let component: DiagnosisPickComponent;
  let fixture: ComponentFixture<DiagnosisPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnosisPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
