import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {DiagnosisDetail, DiagnosisSummary, DiagnosisSummaryPartialList} from "../diagnosis.model";
import {DiagnosisPersist} from "../diagnosis.persist";


@Component({
  selector: 'app-diagnosis-pick',
  templateUrl: './diagnosis-pick.component.html',
  styleUrls: ['./diagnosis-pick.component.css']
})
export class DiagnosisPickComponent implements OnInit {

  diagnosissData: DiagnosisSummary[] = [];
  diagnosissTotalCount: number = 0;
  diagnosissSelection: DiagnosisSummary[] = [];
  diagnosissDisplayedColumns: string[] = ["select", 'name','diagnosis_order','certainity','encounter_id','patient_id','status','note','date' ];

  diagnosissSearchTextBox: FormControl = new FormControl();
  diagnosissIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) diagnosissPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) diagnosissSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public diagnosisPersist: DiagnosisPersist,
              public dialogRef: MatDialogRef<DiagnosisPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("diagnosiss");
    this.diagnosissSearchTextBox.setValue(diagnosisPersist.diagnosisSearchText);
    //delay subsequent keyup events
    this.diagnosissSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.diagnosisPersist.diagnosisSearchText = value;
      this.searchDiagnosiss();
    });
  }

  ngOnInit() {

    this.diagnosissSort.sortChange.subscribe(() => {
      this.diagnosissPaginator.pageIndex = 0;
      this.searchDiagnosiss();
    });

    this.diagnosissPaginator.page.subscribe(() => {
      this.searchDiagnosiss();
    });

    //set initial picker list to 5
    this.diagnosissPaginator.pageSize = 5;

    //start by loading items
    this.searchDiagnosiss();
  }

  searchDiagnosiss(): void {
    this.diagnosissIsLoading = true;
    this.diagnosissSelection = [];

    this.diagnosisPersist.searchDiagnosis("abc", this.diagnosissPaginator.pageSize,
        this.diagnosissPaginator.pageIndex,
        this.diagnosissSort.active,
        this.diagnosissSort.direction).subscribe((partialList: DiagnosisSummaryPartialList) => {
      this.diagnosissData = partialList.data;
      if (partialList.total != -1) {
        this.diagnosissTotalCount = partialList.total;
      }
      this.diagnosissIsLoading = false;
    }, error => {
      this.diagnosissIsLoading = false;
    });

  }

  markOneItem(item: DiagnosisSummary) {
    if(!this.tcUtilsArray.containsId(this.diagnosissSelection,item.id)){
          this.diagnosissSelection = [];
          this.diagnosissSelection.push(item);
        }
        else{
          this.diagnosissSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.diagnosissSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}

