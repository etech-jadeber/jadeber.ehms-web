import {TCId} from "../../tc/models";

export class DiagnosisSummary extends TCId {
  name : string;
diagnosis_order : number;
certainity : number;
encounter_id : string;
patient_id : string;
status : number;
note : string;
date : number;
current:boolean;
occurance : number;
written_by:number;
user_id:string;
pertinent_investigation_finding: string;
plan:string;
}

export class DiagnosisSummaryPartialList {
  data: DiagnosisSummary[];
  total: number;
}

export class DiagnosisDetail extends DiagnosisSummary {
  name : string;
diagnosis_order : number;
certainity : number;
encounter_id : string;
patient_id : string;
status : number;
note : string;
date : number;
occurance : number;
}

export class DiagnosisDashboard {
  total: number;
}
