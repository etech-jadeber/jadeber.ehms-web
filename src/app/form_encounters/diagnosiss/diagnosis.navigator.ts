import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../../tc/models";

import {DiagnosisEditComponent} from "./diagnosis-edit/diagnosis-edit.component";
import {DiagnosisPickComponent} from "./diagnosis-pick/diagnosis-pick.component";


@Injectable({
  providedIn: 'root'
})

export class DiagnosisNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  diagnosissUrl(): string {
    return "/diagnosiss";
  }

  diagnosisUrl(id: string): string {
    return "/diagnosiss/" + id;
  }

  viewDiagnosiss(): void {
    this.router.navigateByUrl(this.diagnosissUrl());
  }

  viewDiagnosis(id: string): void {
    this.router.navigateByUrl(this.diagnosisUrl(id));
  }

  editDiagnosis(id: string,isActive:boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DiagnosisEditComponent, {
      data:{id, mode:TCModalModes.EDIT,isActive},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addDiagnosis(id: string,isActive:boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DiagnosisEditComponent, {
          data: {id:id, mode:TCModalModes.NEW,isActive},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickDiagnosiss(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(DiagnosisPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
