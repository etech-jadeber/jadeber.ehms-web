import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../../tc/models";
import {DiagnosisDashboard, DiagnosisDetail, DiagnosisSummaryPartialList} from "./diagnosis.model";
import {diagnosis_order, diagnosis_certainty, diagnosis_status, Occurance} from "../../app.enums"
import { TCUtilsString } from 'src/app/tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class DiagnosisPersist {

  diagnosisSearchText: string = "";
  diagnosis_orderId: number ;
  diagnosis_certaintyId: number ;
  diagnosis_statusId: number ;
    
  diagnosis_order: TCEnum[] = [
  new TCEnum(diagnosis_order.primary, 'primary'),
  new TCEnum(diagnosis_order.secondary, 'secondary'),
  ];
    
  diagnosis_certainty: TCEnum[] = [
  new TCEnum(diagnosis_certainty.confirmed, 'confirmed'),
  new TCEnum(diagnosis_certainty.presumed, 'presumed'),
  ];

  diagnosis_status: TCEnum[] = [
  new TCEnum(diagnosis_status.active, 'active'),
  new TCEnum(diagnosis_status.inactive, 'inactive'),
  ];

  Occurance: TCEnum[] = [
    new TCEnum(Occurance.new_occurance, 'New'),
    new TCEnum(Occurance.repeated, 'Repeat'),
    new TCEnum(Occurance.new_repeat, 'New Repeat')
  ]
  written_by: number;
  nurse_id: string;
  date: number;

  constructor(private http: HttpClient) {
  }

  searchDiagnosis(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DiagnosisSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/diagnosiss", this.diagnosisSearchText, pageSize, pageIndex, sort, order);
    if (this.diagnosis_orderId) {
      url = TCUtilsString.appendUrlParameter(url, "diagnosis_order", this.diagnosis_orderId.toString());
    }
    if (this.diagnosis_certaintyId) {
      url = TCUtilsString.appendUrlParameter(url, "diagnosis_certainty", this.diagnosis_certaintyId.toString());
    }
    if (this.diagnosis_statusId) {
      url = TCUtilsString.appendUrlParameter(url, "diagnosis_status", this.diagnosis_statusId.toString());
    }
    if (this.written_by) {
      url = TCUtilsString.appendUrlParameter(url, "written_by", this.written_by.toString());
    }
    if (this.nurse_id) {
      url = TCUtilsString.appendUrlParameter(url, "user_id", this.nurse_id);
    }
    if (this.date) {
      url = TCUtilsString.appendUrlParameter(url, "date", this.date.toString());
    }
    return this.http.get<DiagnosisSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.diagnosisSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "diagnosiss/do", new TCDoParam("download_all", this.filters()));
  }

  diagnosisDashboard(): Observable<DiagnosisDashboard> {
    return this.http.get<DiagnosisDashboard>(environment.tcApiBaseUri + "diagnosiss/dashboard");
  }

  getDiagnosis(id: string): Observable<DiagnosisDetail> {
    return this.http.get<DiagnosisDetail>(environment.tcApiBaseUri + "diagnosiss/" + id);
  }

  addDiagnosis(parentId: string, item: DiagnosisDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/diagnosiss", item);
  }

  updateDiagnosis(item: DiagnosisDetail): Observable<DiagnosisDetail> {
    return this.http.patch<DiagnosisDetail>(environment.tcApiBaseUri + "diagnosiss/" + item.id, item);
  }

  deleteDiagnosis(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "diagnosiss/" + id);
  }

  diagnosissDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "diagnosiss/do", new TCDoParam(method, payload));
  }

  diagnosisDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "diagnosiss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "diagnosiss/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "diagnosiss/" + id + "/do", new TCDoParam("print", {}));
  }


}
