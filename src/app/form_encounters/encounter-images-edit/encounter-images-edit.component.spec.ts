import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EncounterImagesEditComponent } from './encounter-images-edit.component';

describe('EncounterImagesEditComponent', () => {
  let component: EncounterImagesEditComponent;
  let fixture: ComponentFixture<EncounterImagesEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EncounterImagesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncounterImagesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
