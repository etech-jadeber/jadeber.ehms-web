import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCId, TCModalModes, TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Encounter_ImagesDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { HttpClient } from '@angular/common/http';
import { FilePersist } from 'src/app/tc/files/file.persist';


@Component({
  selector: 'app-encounter_images-edit',
  templateUrl: './encounter-images-edit.component.html',
  styleUrls: ['./encounter-images-edit.component.css']
})
export class Encounter_ImagesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  encounter_imagesDetail: Encounter_ImagesDetail = new Encounter_ImagesDetail();
  newlyUploadedFileId: string = TCUtilsString.getInvalidId();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Encounter_ImagesEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              public filePersist:FilePersist,
              private http:HttpClient,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.context.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context.mode === TCModalModes.EDIT;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew()) {

      this.title = this.appTranslation.getText("general","new") + " "+ this.appTranslation.getText("patient","images");
      // this.encounter_imagesDetail.encounter = parseInt( this.ids.parentId)
      this.encounter_imagesDetail.patient_id = this.ids.childId;
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general","edit") + " "+ this.appTranslation.getText("patient","images");
      this.isLoadingResults = true;
      this.persist.getEncounter_Images(this.ids.parentId, this.ids.childId).subscribe(encounter_imagesDetail => {
        this.encounter_imagesDetail = encounter_imagesDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.encounter_imagesDetail == null){
          return false;
        }

        if(this.encounter_imagesDetail.image_id == null){
          return false;
        }
     

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addEncounter_Images(this.ids.parentId, this.encounter_imagesDetail).subscribe(value => {
      this.tcNotification.success("Encounter_Images added");
      this.encounter_imagesDetail.id = value.id;
      this.dialogRef.close(this.encounter_imagesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateEncounter_Images(this.ids.parentId, this.encounter_imagesDetail).subscribe(value => {
      this.tcNotification.success("Encounter_Images updated");
      this.dialogRef.close(this.encounter_imagesDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  documentUpload(fileInputEvent: any): void {
    for (let file of fileInputEvent.target.files){
    if (file.size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(file.name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', file);
    this.http.post(this.persist.documentUploadUri(this.ids.parentId), fd).subscribe(response => {
      this.encounter_imagesDetail.image_id = (<TCId>response).id;
      this.encounter_imagesDetail.name = file.name;
      if(this.isNew()){
        this.onAdd()
      }
    });
  }
  }
  
}
