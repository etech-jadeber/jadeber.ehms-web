import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormDrugPickComponent } from './form-drug-pick.component';

describe('FormDrugPickComponent', () => {
  let component: FormDrugPickComponent;
  let fixture: ComponentFixture<FormDrugPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDrugPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDrugPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
