import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Form_EncounterDetail, Form_EncounterSummary, Form_EncounterSummaryPartialList} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";


@Component({
  selector: 'app-form-drug-pick',
  templateUrl: './form-drug-pick.component.html',
  styleUrls: ['./form-drug-pick.component.css']
})
export class FormDrugPickComponent implements OnInit {

  drugData: any[] = [];
  drugTotalCount: number = 0;
  drugSelection: Form_EncounterSummary[] = [];
  drugsDisplayedColumns: string[] = ["select", 'name','unit','size'];

  drugsSearchTextBox: FormControl = new FormControl();
  drugsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) drugsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) drugsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public form_encounterPersist: Form_EncounterPersist,
              public dialogRef: MatDialogRef<FormDrugPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("form_encounters");
    this.drugsSearchTextBox.setValue(form_encounterPersist.drugSearchHistory.search_text);
    //delay subsequent keyup events
    this.drugsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.form_encounterPersist.drugSearchHistory.search_text = value;
      this.searchDrugs();
    });
  }

  ngOnInit() {

    this.drugsSort.sortChange.subscribe(() => {
      this.drugsPaginator.pageIndex = 0;
      this.searchDrugs();
    });

    this.drugsPaginator.page.subscribe(() => {
      this.searchDrugs();
    });

    //set initial picker list to 5
    this.drugsPaginator.pageSize = 5;

    //start by loading items
    this.searchDrugs();
  }

  searchDrugs(): void {

    this.form_encounterPersist.searchDrugs(0, 0, '', 'asc').subscribe(response => {
      this.drugData = response.data;
      
      if (response.total != -1) {
        this.drugTotalCount = response.total;
      }
      this.drugsIsLoading = false;
    }, error => {
      this.drugsIsLoading = false;
    });
  }

  markOneItem(item: Form_EncounterSummary) {
    if(!this.tcUtilsArray.containsId(this.drugSelection,item.id)){
          this.drugSelection = [];
          this.drugSelection.push(item);
        }
        else{
          this.drugSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.drugSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
