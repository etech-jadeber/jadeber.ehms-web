import { ServiceType } from "src/app/app.enums";
import { Form_EncounterDetailComponent } from "./form-encounter-detail.component";

export class FormEncounterTabs {
    constructor(public encounter: Form_EncounterDetailComponent) { 
    }

    encounterTabs = [
        {
            id: "form_vitalss",
            icon: 'fa-heartbeat',
            title: this.encounter.appTranslation.getText('patient', 'form_vitalss'),
            visible: this.encounter.tcAuthorization.canRead('form_vitals'),
            selector: "app-form-vitals-list",
            onResult: this.encounter.OnVitalResult
        },
        // {
        //     id: "medical_historys",
        //     icon: 'fa-users',
        //     title: this.encounter.appTranslation.getText('patient', 'history'),
        //     visible: this.encounter.tcAuthorization.canRead('medical_historys'),
        //     selector: "app-physical-examination-list",
        //     onResult: this.encounter.OnPhysicalExaminationResult
        // },

        // {
        //     id: "family_historys",
        //     icon: 'fa-users',
        //     title: this.encounter.appTranslation.getText('patient', 'family_history'),
        //     visible: this.encounter.tcAuthorization.canRead('family_historys'),
        //     selector: "app-history-data-lis",
        //     onResult: this.encounter.OnHistoryDataResult
        // },

        // {
        //     id: "physical_examinations",
        //     icon: 'fa-user-md',
        //     title: this.encounter.appTranslation.getText('patient', 'physical_examination'),
        //     visible: this.encounter.tcAuthorization.canRead('physical_examinations'),
        //     selector: "app-physical-examination-list",
        //     onResult: this.encounter.OnPhysicalExaminationResult
        // },
        // {
        //     id: "diagnosiss",
        //     icon: 'fa-diagnoses',
        //     title: this.encounter.appTranslation.getText('patient', 'diagnosis'),
        //     visible: this.encounter.tcAuthorization.canRead('diagnosiss'),
        //     selector: "app-diagnosis-list",
        //     onResult: this.encounter.onDiagnosisResult
        // },

        {
            id: "orders",
            icon: 'fa-flask',
            title: this.encounter.appTranslation.getText('patient', 'investigations'),
            visible: (this.encounter.tcAuthorization.canCreate('laboratory_orders') || this.encounter.tcAuthorization.canCreate('radiology_orders') || this.encounter.tcAuthorization.canCreate('pathology_orders')) && this.encounter.form_encounterPersist.encounter_active,
            selector: "app-lab-orders-list",
            onResult: this.encounter.OnLabOrdersResult
        },
        {
            id: "results",
            icon: 'fa-clipboard-list',
            title: this.encounter.appTranslation.getText('procedure', 'result'),
            visible: this.encounter.tcAuthorization.canRead('laboratory_orders') || this.encounter.tcAuthorization.canRead('radiology_orders') || this.encounter.tcAuthorization.canRead('pathology_orders'),
            selector: "app-orders-list",
            onResult: this.encounter.onOrderResult
        },
        {
            id: "prescriptions",
            icon: 'fa-file-medical',
            title: this.encounter.appTranslation.getText('patient', 'prescription'),
            visible: this.encounter.tcAuthorization.canRead('prescriptions'),
            selector: "app-prescription-list",
            onResult: this.encounter.OnPrescriptionResult
        },
        {
            id: "form_soaps",
            icon: 'fa-bullseye',
            title: this.encounter.appTranslation.getText('patient', 'psoap'),
            visible: this.encounter.tcAuthorization.canRead('form_soaps'),
            selector: "app-form-soaps-list",
            onResult: this.encounter.OnFormSoapsResult
        },
        
        
        {
            id: "form_clinical_instructions",
            icon: 'fa-sticky-note',
            title: this.encounter.appTranslation.getText('patient', 'clinical_instruction'),
            visible: this.encounter.tcAuthorization.canRead('form_clinical_instructions'),
            selector: "app-form-clinical-instruction-list",
            onResult: this.encounter.OnFormClinicalInstructionResult
        },

        {
            id: "medication_adminstration_charts",
            icon: 'fa-calendar-check',
            title: this.encounter.appTranslation.getText(
                'patient',
                'medication_adminstration_chart'
            ),
            visible: this.encounter.tcAuthorization.canRead('medication_adminstration_charts'),
            selector: "app-medication-adminstration-chart-list",
            onResult: this.encounter.onMedicalAdminstrationChartResult
        },
        
        
        // {
        //     id: "comments",
        //     icon: 'fa-sticky-note',
        //     title: this.encounter.appTranslation.getText('notes', 'notes'),
        //     visible: this.encounter.tcAuthorization.canRead('comments'),
        //     selector: "app-notes-list",
        //     onResult: this.encounter.OnNotesResult
        // },

        {
            id: "allergy_forms",
            icon: 'fa-sticky-note',
            title: 'Allergy form',
            visible: this.encounter.tcAuthorization.canRead('allergy_forms'),
            selector: "app-alery-form-list",
            onResult: this.encounter.OnAllergyResult
        },

        {
            id: "consultings",
            icon: 'fa-handshake',
            title: this.encounter.appTranslation.getText('patient', 'consultation'),
            visible: this.encounter.tcAuthorization.canRead('consultings'),
            selector: "app-consulting-list",
            onResult: this.encounter.OnConsultingResult
        },

        

        // {
        //     id: "pre_ansthesia_evaluations",
        //     icon: 'fa-calendar-check',
        //     title: "Pre ansthesia evaluation",
        //     visible: this.encounter.tcAuthorization.canRead('pre_ansthesia_evaluations'),
        //     selector: "app-pre-ansthesia-evaluation-edit-list",
        //     onResult: ()=> void{}
        // },

        
        // {
        //     id: "surgery_appointments",
        //     icon: 'fa-calendar-check',
        //     title: this.encounter.appTranslation.getText('patient', 'surgery_appointments'),
        //     visible: this.encounter.tcAuthorization.canRead('surgery_appointments'),
        //     selector: "app-surgery-appointments-list",
        //     onResult: this.encounter.OnSurgeryAppointmentResult
        // },
        // {
        //     id: "review_of_systems",
        //     icon: 'fa-stethoscope',
        //     title: this.encounter.appTranslation.getText('patient', 'review_of_system'),
        //     visible: this.encounter.tcAuthorization.canRead('review_of_systems'),
        //     selector: "app-review-of-system-list",
        //     onResult: this.encounter.OnReviewOfSystemViewResult
        // },
        // {
        //     id: "form_observations",
        //     icon: 'fa-eye',
        //     title: this.encounter.appTranslation.getText('patient', 'observation'),
        //     visible: this.encounter.tcAuthorization.canRead('form_observations'),
        //     selector: "app-form-observations-list",
        //     onResult: this.encounter.OnFormObservationsResult
        // },
        {
            id: "case_transfers",
            icon: 'fa-retweet',
            title: this.encounter.appTranslation.getText('patient', 'case_transfer'),
            visible: this.encounter.tcAuthorization.canRead('case_transfers'),
            selector: "app-case_transfer-list",
            onResult: this.encounter.onCaseTransfer
        },
        {
            id: "doctor_private_infos",
            icon: 'fa-info',
            title: this.encounter.appTranslation.getText('patient', 'private_info'),
            visible: this.encounter.tcAuthorization.canRead('doctor_private_infos'),
            selector: "app-doctor-private-info-list",
            onResult: this.encounter.OnDoctorPrivateInfoResult
        },
       
        {
            id: "patient_procedures",
            icon: 'fa-procedures',
            title: this.encounter.appTranslation.getText('procedure', 'patient_procedures'),
            visible: this.encounter.tcAuthorization.canRead('patient_procedures'),
            selector: "app-patient-procedure-list",
            onResult: this.encounter.OnPatientProcedureResult
        },
        // {
        //     id: "medical_certificate_employees",
        //     icon: 'fa-certificate',
        //     title: this.encounter.appTranslation.getText(
        //         'patient',
        //         'employee_medical_certificate'
        //     ),
        //     visible: this.encounter.tcAuthorization.canRead('medical_certificate_employees'),
        //     selector: "app-medical_certificate_employee-list",
        //     onResult: this.encounter.OnMedicalCertificateResult
        // },


        // {
        //     id: "driver_medical_certificate",
        //     icon: 'fa-certificate',
        //     title: this.encounter.appTranslation.getText('patient', 'driver_medical_certificate'),
        //     visible: this.encounter.tcAuthorization.canRead('driver_medical_certificates'),
        //     selector: "app-driver-medical-certificate-list",
        //     onResult: this.encounter.onDriverMedicalCertificate
        // },

        {
            id: "medical_certificates",
            icon: 'fa-users',
            title: this.encounter.appTranslation.getText("patient", "medical_certificate"),
            visible: this.encounter.tcAuthorization.canRead('medical_certificates'),
            selector: "app-medical-certificate-list",
            onResult: this.encounter.OnMedicalCertificateResult
        },
        {
            id: "encounter_images",
            icon: 'fa-image',
            title: this.encounter.appTranslation.getText('patient', 'images'),
            visible: this.encounter.tcAuthorization.canRead('encounter_images'),
            selector: "app-encounter-images-list",
            onResult: this.encounter.OnEncounterImageResult
        },
        {
            id: "follow_up_notes",
            icon: 'fa-sticky-note',
            title: this.encounter.appTranslation.getText('patient', 'follow_up_note'),
            visible: this.encounter.tcAuthorization.canRead('follow_up_notes'),
            selector: "app-followup-note-list",
            onResult: this.encounter.onFollowUpResult
        },
        {
            id: "rehabilitation_orders",
            icon: 'fa-user-injured',
            title: this.encounter.appTranslation.getText('patient', 'physiotherapy'),
            visible: this.encounter.tcAuthorization.canRead('rehabilitation_orders'),
            selector: "app-rehabilitation-order-list",
            onResult: this.encounter.OnRehabilitationOrderResult
        },
        {
            id: "patient_discharges",
            icon: 'fa-medkit',
            title: this.encounter.appTranslation.getText('patient', 'dispatch'),
            visible: this.encounter.tcAuthorization.canRead('patient_discharges') && this.encounter.form_encounterDetail.service_type == ServiceType.inpatient,
            selector: "app-patient_discharge-list",
            onResult: this.encounter.OnPatientDischargeResult
        },
        {
            id: "patient_referrals",
            icon: 'fa-user-plus',
            title: this.encounter.appTranslation.getText('financial', 'referral'),
            visible: this.encounter.tcAuthorization.canRead('patient_referrals'),
            selector: "app-patient_referral_form-list",
            onResult: this.encounter.OnPatientReferralResult
        },
        
        // {
        //     id: "anc_historys",
        //     icon: 'fa-users',
        //     title: "ANC History",
        //     visible: this.encounter.tcAuthorization.canRead('anc_historys') && !this.encounter.patientDetail.sex,
        //     selector: "app-anc_history",
        //     onResult: this.encounter.OnAncHistory
        // },
        // {
        //     id: "new_born_natal_car",
        //     icon: 'fa-users',
        //     title: "NeoNatal Care",
        //     visible: this.encounter.tcAuthorization.canRead('new_born_natal_cares'),
        //     selector: "app-new_born_natal_care-list",
        //     onResult: this.encounter.OnNewBornNatalCare
        // },
        // {
        //     id: "delivery_summary",
        //     icon: 'fa-person-pregnant',
        //     title: "Delivery summary",
        //     visible: this.encounter.tcAuthorization.canRead('delivery_summarys'),
        //     selector: "app-delivery_summary-list",
        //     onResult: this.encounter.OnDeliverySummaryResult
        // },

        // {
        //     id: "dialysis",
        //     icon: 'fa-stethoscope',
        //     title: "Dialysis",
        //     visible: this.encounter.tcAuthorization.canRead('dialysiss'),
        //     selector: "app-dialysis-list",
        //     onResult: this.encounter.OnMedicalCertificateResult
        // },



        
        {
            id: "operation_note",
            icon: 'fa-clipboard',  
            title: this.encounter.appTranslation.getText('patient', 'operation_note'),
            visible: this.encounter.tcAuthorization.canRead('operation_notes'),
            selector: "app-operation-note-list",
            onResult: this.encounter.OnMedicalCertificateResult
        },

        // {
        //     id: "births",
        //     icon: 'fa-clipboard',  
        //     title: "Birth",
        //     visible: this.encounter.tcAuthorization.canRead('births'),
        //     selector: "app-birth-list",
        //     onResult: this.encounter.onBirthResult
        // },
        {
            id: "deaths",
            icon: 'fa-clipboard',  
            title: "Death",
            visible: this.encounter.tcAuthorization.canRead('deaths'),
            selector: "app-death-list",
            onResult: this.encounter.onDeathResult
        },

        {
            id: "followups",
            icon: 'fa-check-double',  
            title: "Followup",
            visible: this.encounter.tcAuthorization.canRead('form_encounters') && this.encounter.form_encounterDetail.parent_encounter_id != this.encounter.tcUtilsString.invalid_id,
            selector: "app-other-form-encounters-list ",
            onResult: this.encounter.OnMedicalCertificateResult
        },

        {
            id: "form_encounters",
            icon: 'fa-check-double',  
            title: "Previous Visits",
            visible: this.encounter.tcAuthorization.canRead('form_encounters'),
            selector: "app-other-form-encounters-list ",
            onResult: this.encounter.OnMedicalCertificateResult
        },

        // {
        //     id: "additional_services",
        //     icon: 'fa-add',
        //     title: "Additional Srvices",
        //     visible: this.encounter.tcAuthorization.canRead('additional_services'),
        //     selector: 'app-additional-service-list',
        //     onResult: this.encounter.OnMedicalCertificateResult
        // },
        // {
        //     id: "order_other_services",
        //     icon: 'fa-add',
        //     title: "Order Other Srvices",
        //     visible: this.encounter.tcAuthorization.canRead('order_other_services'),
        //     selector: 'app-ordered_othe_service-list',
        //     onResult: this.encounter.OnMedicalCertificateResult
        // }
        
    ]
};
