import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Form_EncounterDetailComponent } from './form-encounter-detail.component';

describe('FormEncounterDetailComponent', () => {
  let component: Form_EncounterDetailComponent;
  let fixture: ComponentFixture<Form_EncounterDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Form_EncounterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Form_EncounterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
