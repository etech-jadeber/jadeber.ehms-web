import {
  AfterViewInit,
  Component,
  ElementRef,
  NgModule,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Location } from '@angular/common';

import { interval, Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import {
  Form_EncounterDetail,
  Form_ObservationSummary,
  PrescriptionsSummary,
  Form_EncounterSummary,
  Form_EncounterSummaryPartialList,
  Patient_ProcedureSummary,
  Form_VitalsSummary,
  Form_SoapSummary,
  EncounterTabs,
} from '../form_encounter.model';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { Form_EncounterNavigator } from '../form_encounter.navigator';
import { FormControl } from '@angular/forms';
import { DoctorDetail, DoctorSummary } from 'src/app/doctors/doctor.model';
import {
  History_DataSummary,
  PatientDetail,
  PatientSummary
} from 'src/app/patients/patients.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { CommentDetail, CommentSummary, TCIdMode, TCModalModes, TCWizardProgress, TCWizardProgressModes } from 'src/app/tc/models';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Procedure_OrderPersist } from "../procedure_orders/procedure_order.persist";
import { Procedure_OrderDetail } from "../procedure_orders/procedure_order.model";
import { BedassignmentSummary } from 'src/app/bed/bedassignments/bedassignment.model';
import { BedassignmentNavigator } from 'src/app/bed/bedassignments/bedassignment.navigator';
import { BedassignmentPersist } from 'src/app/bed/bedassignments/bedassignment.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Surgery_AppointmentPersist } from 'src/app/surgery_appointments/surgery_appointment.persist';
import { Surgery_TypePersist } from 'src/app/surgery_types/surgery_type.persist';
import { AppointmentNavigator } from 'src/app/appointments/appointment.navigator';
import { category, encounter_status, lab_order_type, lab_urgency, prescription_type, ServiceType, surgery_status, tabs, user_context } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { HistoryPersist } from 'src/app/historys/history.persist';
import { HistoryNavigator } from 'src/app/historys/history.navigator';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_PanelDetail, Lab_PanelSummary, SummarizedTest, Test_PanelDetail, Test_PanelSummary } from 'src/app/lab_panels/lab_panel.model';
import { ExtendedLab_TestSummary, Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { OrdersPersist } from '../orderss/orders.persist';
import {
  Physiotherapy_TypeSummary,
  Physiotherapy_TypeSummaryPartialList
} from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';
import { Admission_FormNavigator } from "../admission_forms/admission_form.navigator";
import { Admission_FormPersist } from "../admission_forms/admission_form.persist";
import { Consent_FormNavigator } from 'src/app/consent_forms/consent_form.navigator';
import { ItemPersist } from 'src/app/items/item.persist';
import { Consent_FormPersist } from 'src/app/consent_forms/consent_form.persist';
import { Consent_FormSummary } from 'src/app/consent_forms/consent_form.model';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { BiopsyRequestNavigator } from 'src/app/biopsy_request/biopsy_request.navigator';
import { BiopsyRequestPersist } from 'src/app/biopsy_request/biopsy_request.persist';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { BedroomPersist } from 'src/app/bed/bedrooms/bedroom.persist';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
import { FormEncounterTabs } from './form-encouner-tabs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatTabChangeEvent, MatTabGroup } from '@angular/material/tabs';
import { DepositPersist } from 'src/app/deposits/deposit.persist';
import { PatientVisitFormComponent } from 'src/app/patient-visit-form/patient-visit-form.component';


export enum PaginatorIndexes {
  encounters,
}

@Component({
  selector: 'app-form_encounter-detail',
  templateUrl: './form-encounter-detail.component.html',
  styleUrls: ['./form-encounter-detail.component.css'],
})
export class Form_EncounterDetailComponent
  implements OnInit, OnDestroy, AfterViewInit {
  form_encountersData: Form_EncounterSummary[] = [];
  form_encountersTotalCount: number = 0;
  form_encountersSelectAll: boolean = false;
  form_encountersSelection: Form_EncounterSummary[] = [];
  prescription_type = prescription_type;
  category = category;

  form_encountersDisplayedColumns: string[] = [
    'date',
    'reason',
    'sensitivity',
    'provider_id',
    'inpatient',
  ];
  form_encountersSearchTextBox: FormControl = new FormControl();

  @ViewChild(MatPaginator, { static: true }) form_encountersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) form_encountersSort: MatSort;
  summarizedTest: SummarizedTest[] = [];

  labOrders: Lab_OrderSummary[] = [];
  radiology: Lab_OrderSummary[] = [];
  labPanels: Lab_PanelSummary[] = [];
  labTests: Lab_TestSummary[] = [];
  history_datasData: History_DataSummary[] = [];
  history_datasTotalCount: number = 0;
  radPanels: Lab_PanelSummary[] = [];
  radTests: Lab_TestSummary[] = [];

  selectedLabPanel: Lab_PanelSummary[] = [];
  selectedLabTest: ExtendedLab_TestSummary[] = [];



  selectedradPanel: Lab_PanelSummary[] = [];
  selectedradTest: ExtendedLab_TestSummary[] = [];

  lab_urgency: typeof lab_urgency = lab_urgency;
  laburgency: number = lab_urgency.normal;
  radurgency: number = lab_urgency.normal;

  futureLab: boolean = false;
  futureRad: boolean = false;

  futureLabDate: Date = new Date();
  futureRadDate: Date = new Date();

  physicalTextBox: FormControl = new FormControl();
  paramsSubscription: Subscription;
  form_encounterLoading: boolean = false;
  patientLoading: boolean = false;

  form_EncounterTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  patientDetail: PatientDetail= new PatientDetail();
  responseData = [];
  //basics
  form_encounterDetail: Form_EncounterDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  //form_vitalss
  form_vitalssData: Form_VitalsSummary[] = [];
  form_vitalssTotalCount: number = 0;

  allergyFormsTotalCount: number = 0;

  form_soapsData: Form_SoapSummary[] = [];
  form_soapsTotalCount: number = 0;
  //prescriptionss
  prescriptionssTotalCount: number = 0;
  prescriptionssData: PrescriptionsSummary[] = [];


  notesTotalCount: number = 0;
  notesData: CommentSummary[] = [];
  topProcedureData: Procedure_OrderDetail;

  surgery_appointmentsTotalCount: number = 0;

  review_of_systemsTotalCount: number = 0;
  // anc_initial_evaluationTotalCount: number = 0;
  // post_natal_care: number = 0;
  anc_history: number = 0;


  //form_observations
  form_observationsTotalCount: number = 0;
  form_observationLoading: boolean = false;
  form_observations: boolean = false;

  patient_proceduresTotalCount: number = 0;
  physical_examinationsTotalCount: number = 0;
  //consulting
  consultingsTotalCount: number = 0;
  form_clinical_instructionTotalCount: number;
  //drugs
  drugData: any[] = [];
  drugTotalCount: number = 0;
  drugsIsLoading: boolean = false;

  doctor_private_infosTotalCount: number = 0;


  encounter_imagessTotalCount: number = 0;

  historysTotalCount: number = 0;
  //case transfer
  caseTransferTotalCount: number = 0;

  //follow_up_notes
  follow_up_notesTotalCount: number = 0;
  //rehabilitation_orders
  rehabilitation_ordersTotalCount: number = 0;
  patientDischargesTotalCount: number = 0;
  patientReferralTotalCount: number = 0;

  physiotherapy_typesData: Physiotherapy_TypeSummary[] = [];
  physiotherapy_typesTotalCount: number = 0;
  //job states
  unknown_state = -1;
  queued_state = 1;
  running_state = 2;
  success_state = 3;
  failed_state = 4;
  aborted_state = 5;
  message: string = '';
  patientAge: number;
  mrn:number;
  gender:boolean;
  encounterTabs: EncounterTabs[];
  ServiceType = ServiceType;

  form_observationsData: Form_ObservationSummary[] = [];
  encounterTabSearch: FormControl = new FormControl();

  multi: any[];
  view: any[] = [900, 400];
  remaining_deposit: number;
  encounter_is_active:boolean;

  patient_name: string;
  consentForm: Consent_FormSummary;

  yearSearch: FormControl = new FormControl();
  name: Date = new Date();
  lab_testPanel: Test_PanelSummary[] = [];
  TCUtilsString: typeof TCUtilsString = TCUtilsString;
  user_context = user_context;

  constructor(
    private procedurePersist: ProcedurePersist,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public patientNavigator: PatientNavigator,
    public jobPersist: JobPersist,
    private tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public appTranslation: AppTranslation,
    public form_encounterNavigator: Form_EncounterNavigator,
    public form_encounterPersist: Form_EncounterPersist,
    public consetNav: Consent_FormNavigator,
    public filePersist: FilePersist,
    private patientPersist: PatientPersist,
    private procedureOrderPersist: Procedure_OrderPersist,
    public surgery_appointmentPersist: Surgery_AppointmentPersist,
    public surgeryTypePersist: Surgery_TypePersist,
    private appointmentNavigator: AppointmentNavigator,
    public menuState: MenuState,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public historyPersist: HistoryPersist,
    public historyNavigator: HistoryNavigator,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    public lab_testPersist: Lab_TestPersist,
    private orderPersisit: OrdersPersist,
    private admissionFormPersist: Admission_FormPersist,
    private admissionFormNavigator: Admission_FormNavigator,
    public physiotherapy_typePersist: Physiotherapy_TypePersist,
    public itemPersist: ItemPersist,
    public consentFormPersist: Consent_FormPersist,
    public departmentPersist: DepartmentPersist,
    public biopsyRequestNavigator: BiopsyRequestNavigator,
    private biopsyRequestPersist: BiopsyRequestPersist,
    private doctorPersist: DoctorPersist,
    private deposiPersist: DepositPersist,
    public tcUtilsString: TCUtilsString,
  ) {
    this.tcAuthorization.requireRead('form_encounters');
    this.form_encounterDetail = new Form_EncounterDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
    this.encounterTabSearch.valueChanges.subscribe((value: string) => {
      this.encounterTabs = new FormEncounterTabs(this).encounterTabs.filter((tab) => tab.visible && tab.title.toLowerCase().search(value.toLowerCase()) != -1
      )
    })
  }

  @ViewChild('patientVisit') patientVisit: PatientVisitFormComponent;

  ngOnInit() {
    this.tcAuthorization.requireRead('form_encounters');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    // this.form_encounterPersist.parent_encounter_id = null;
    //     this.form_encounterPersist.encounter_is_active = null;
    //     this.form_encounterPersist.patientId = null;
    //     this.form_encounterPersist.show_only_mine = true;
    //     this.form_encounterPersist.encounterStatusFilter = 100;
    //     this.form_encounterPersist.date = this.tcUtilsDate.toTimeStamp(new Date());
    // this.deposiPersist.patientId = null;
  }

  ngAfterViewInit(): void {
    this.tcAuthorization.requireRead('form_encounters');
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.tcAuthorization.requireRead('form_encounters');
    this.form_encounterLoading = true;
    this.form_encounterPersist.getForm_Encounter(id).subscribe(
      (form_encounterDetail) => {
        this.patientLoading = true;
        this.form_encounterDetail = form_encounterDetail;
        this.form_encounterPersist.encounter_is_active = form_encounterDetail.status == encounter_status.opened;
        this.form_encounterPersist.encounter_active = form_encounterDetail.status == encounter_status.opened;
        this.patientPersist.getPatient(this.form_encounterDetail.pid).subscribe((patient)=>{
          this.patientDetail = patient;
          this.patient_name = patient.fname + " " + patient.mname + " " + patient.lname
          this.patientAge = new Date().getFullYear()-new Date(patient.dob*1000).getFullYear();
          this.mrn = patient.pid;
          this.gender = patient.sex
          this.form_encounterDetail.birth_date = patient.dob;
          this.patientLoading = false;
        });
        // if(this.form_encounterDetail.service_type == ServiceType.inpatient){
        //   this.searchDeposit();
        // }
        this.encounterTabs = new FormEncounterTabs(this).encounterTabs.filter((tab) => tab.visible);
        this.tcAuthorization.canRead('form_vitals') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"form_vitalss"));
        // this.tcAuthorization.canRead('medical_historys') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"medical_historys"));
        // this.tcAuthorization.canRead('family_historys') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"family_historys"));
        // this.tcAuthorization.canRead('physical_examinations') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"physical_examinations"));
        // this.tcAuthorization.canRead('diagnosiss') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"diagnosiss"));
        (this.tcAuthorization.canCreate('laboratory_orders') || this.tcAuthorization.canCreate('radiology_orders') || this.tcAuthorization.canCreate('pathology_orders')) && this.form_encounterPersist.encounter_active && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"orders"));
        (this.tcAuthorization.canRead('laboratory_orders') || this.tcAuthorization.canRead('radiology_orders') || this.tcAuthorization.canRead('pathology_orders')) && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"results"));
        this.tcAuthorization.canRead('prescriptions') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"prescriptions"));
        this.tcAuthorization.canRead('form_soaps') && this.form_encounterPersist.selectedEncounterTabs.unshift(this.tcUtilsArray.getById(this.encounterTabs,"form_soaps"));
        this.tcAuthorization.canRead('form_encounters') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"form_encounters"));
        this.tcAuthorization.canRead('encounter_imagess') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"encounter_images"));

        this.tcAuthorization.canRead('form_clinical_instructions') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"form_clinical_instructions"));
        // this.form_encounterDetail.service_type == ServiceType.inpatient && this.tcAuthorization.canRead('medication_adminstration_charts') && this.form_encounterPersist.selectedEncounterTabs.push(this.tcUtilsArray.getById(this.encounterTabs,"medication_adminstration_charts"));

        this.doctorPersist
          .getDoctor(this.form_encounterDetail.provider_id)
          .subscribe((result: DoctorDetail) => {
            this.form_encounterDetail.provider_name =
              result.first_name + ' ' + result.last_name;
              this.form_encounterDetail.department_id = result.department_speciality_id;
          });
        this.form_encounterLoading = false;
        this.menuState.getDataResponse(this.form_encounterDetail);
        //this.getTopProcedure(this.form_encounterDetail.id, this.form_encounterDetail.pid);
        this.responseData = [];
      },
      (error) => {
        console.error(error);
        this.form_encounterLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.form_encounterDetail.id);
  }

  searchDeposit(): void {
    this.deposiPersist.searchDeposit(1, 0, 'id', 'asc').subscribe(deposit => {
      if(deposit.data.length){
        this.remaining_deposit = deposit.data[0].current_amount
      }
    })
  }

  editForm_Encounter(): void {
    let modalRef = this.form_encounterNavigator.editForm_Encounter(
      this.form_encounterDetail.id
    );
    modalRef.afterClosed().subscribe(
      (modifiedForm_EncounterDetail) => {
        TCUtilsAngular.assign(
          this.form_encounterDetail,
          modifiedForm_EncounterDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printForm_Encounter(): void {
    this.form_encounterPersist
      .print(this.form_encounterDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print form_encounter', true);
      });
  }

  back(): void {
    this.location.back();
  }


  birthCount: number = 0;
  driverMedicalCertificateCount:number = 0;
  deathTotalCount: number = 0;
  bedassignmentsTotalCount: number = 0;
  admissionFormTotalCount: number = 0;
  ordersTotalCount: number = 0;
  procedureOrderTotalCount: number = 0;
  medicalAdminstrationChartCount: number = 0;
  diagnosisTotalCount: number = 0;
  form_DeliverySummaryTotalCount : number = 0;
  newBornNatalCareTotalConut : number = 0;
  // deliverySummaryTotalCount: number = 0;

  onAdmissionFormResult(count: number) {
    this.admissionFormTotalCount = count;
  }

  OnDeliverySummaryResult(count: number) {
    this.form_DeliverySummaryTotalCount = count
  }
  onProcedureOrderResult(count: number) {
    this.procedureOrderTotalCount = count;
  }
  OnVitalResult(count: number) {
    this.form_vitalssTotalCount = count;
  }
  OnAllergyResult(count: number) {
    this.allergyFormsTotalCount = count;
  }
  OnFormSoapsResult(count: number) {
    this.form_soapsTotalCount = count;
  }
  OnHistoryResult(count: number) {
    this.historysTotalCount = count;
  }
  OnHistoryDataResult(count: number) {
    this.history_datasTotalCount = count;
  }
  OnNotesResult(count: number) {
    this.notesTotalCount = count;
  }
  OnFormObservationsResult(count: number) {
    this.form_observationsTotalCount = count;
  }
  OnPrescriptionResult(count: number) {
    this.prescriptionssTotalCount = count;
  }
  OnFormClinicalInstructionResult(count: number) {
    this.form_clinical_instructionTotalCount = count;
  }
  onMedicalAdminstrationChartResult(count: number) {
    this.medicalAdminstrationChartCount = count;
  }
  OnEncounterImageResult(count: number) {
    this.encounter_imagessTotalCount = count;
  }
  onDiagnosisResult(count: number) {
    this.diagnosisTotalCount = count;
  }
  // onDeliverySummaryResult(count: number) {
  //   this.deliverySummaryTotalCount = count;
  // }
  onCaseTransfer(count: number) {
    this.caseTransferTotalCount = count;
  }

  onOrderResult(count: number) {
    this.ordersTotalCount = count;
  }
  OnBedAssignmentResult(count: number) {
    this.bedassignmentsTotalCount = count;
  }
  OnSurgeryAppointmentResult(count: number) {
    this.surgery_appointmentsTotalCount = count;
  }
  OnReviewOfSystemViewResult(count: number) {
    this.review_of_systemsTotalCount = count;
  }
  OnConsultingResult(count: number) {
    this.consultingsTotalCount = count;
  }
  OnPatientProcedureResult(count: number) {
    this.patient_proceduresTotalCount = count;
  }
  onDeathResult(count: number) {
    this.deathTotalCount = count;
  }

  onBirthResult(count: number) {
    this.birthCount = count;
  }
  OnDoctorPrivateInfoResult(count: number) {
    this.doctor_private_infosTotalCount = count;
  }
  OnMedicalCertificateResult(count: number) {
    this.doctor_private_infosTotalCount = count;
  }
  onFollowUpResult(count: number) {
    this.follow_up_notesTotalCount = count;
  }
  OnRehabilitationOrderResult(count: number) {
    this.rehabilitation_ordersTotalCount = count;
  }
  OnPhysicalExaminationResult(count: number) {
    this.physical_examinationsTotalCount = count;
  }
  OnPatientDischargeResult(count: number) {
    this.patientDischargesTotalCount = count;
  }
  OnPatientReferralResult(count: number) {
    this.patientReferralTotalCount = count;
  }
  OnOtherEncountersResult(count: number) {
    this.form_encountersTotalCount = count;
  }
  onIntrapartumCareResult(count: number){
    this.form_encountersTotalCount = count;
  }
  onDriverMedicalCertificate(count: number) {
    this.driverMedicalCertificateCount = count;
  }
  OnLabOrdersResult(count: number) {
    this.form_encountersTotalCount = count;
  }
  // onPresentPregenancyResult(count: number){
  //   this.form_encountersTotalCount = count;
  // }
  // OnAncInitialEvaluation(count: number) {
  //   this.anc_initial_evaluationTotalCount = count;
  // }
  // OnPostNatalCare(count: number){
  //   this.post_natal_care = count;
  // }
  OnAncHistory(count: number) {
    this.anc_history = count;
  }
  OnNewBornNatalCare(count: number) {
    this.newBornNatalCareTotalConut = count;
  }
  form_rosData: any[] = [];

  createFutureAppointment(): void {
    this.tcAuthorization.canCreate('appointments');
    let dialogRef = this.appointmentNavigator.addAppointment(
      this.form_encounterDetail.pid,
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        let wizardProgress: TCWizardProgress = <TCWizardProgress>result;
        if (wizardProgress.progressMode == TCWizardProgressModes.NEXT) {
          this.addLab(true, result.item.schedule_start_time);
        } else if (
          wizardProgress.progressMode == TCWizardProgressModes.FINISH
        ) {
          this.tcNotification.success('Appointment sucessfully added');
        }
      }
    });
  }

  addLab(
    isWizard: boolean = false,
    date: number = this.tcUtilsDate.toTimeStamp(new Date())
  ): void {
    this.tcAuthorization.canCreate('lab_orders');
    let dialogRef = this.form_encounterNavigator.addLab(
      isWizard,
      this.form_encounterDetail.id,
      null,
      date,
      [lab_order_type.imaging, lab_order_type.pathology, lab_order_type.lab]
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.tcNotification.success('sucess');
      }
    });
  }

  closeEncounter(): void {
    this.tcAuthorization.canUpdate('form_encounters');
    let dialogRef = this.tcNavigator.confirmAction(
      'Close',
      'Form Encounter',
      'Are you sure you want to close this encounter?',
      'cancel'
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.form_encounterPersist
          .form_encounterDo(
            this.form_encounterDetail.id,
            'close_encounter',
            this.form_encounterDetail
          )
          .subscribe((response) => {
            this.tcNotification.success('encounter is closed');
            this.back();
          });
      }
    });
  }

  followJob(
    id: string,
    descrption: string,
    is_download_job: boolean = true
  ): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }

    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe((n) => {
      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

          if (is_download_job) {
            this.jobPersist.downloadFileKey(id).subscribe((jobFile) => {
              this.tcNavigator.openUrl(
                this.filePersist.downloadLink(jobFile.download_key)
              );
            });
          }
        } else {
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);
          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }


  getBSAStatus(bsa: number) {
    return 'Normal';
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.form_encounterPersist.selectedTabIndex = matTab.index
  }

  admitPatient() {
    this.tcAuthorization.canCreate('admission_forms');
    let dialogRef = this.admissionFormNavigator.admission_order(this.form_encounterDetail.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        result['encounter_id'] = this.form_encounterDetail.id;
      }
    });
  }
  form_encountersIsLoading: boolean = false;


  getConsentForm() {
    this.consentFormPersist
      .consent_formDo(this.form_encounterDetail.id, 'donloaded_data', {})
      .subscribe((result: Consent_FormSummary) => {
        this.consentForm = result;
      });
  }


  addBiopsyRequest() {
    this.tcAuthorization.canCreate('biopsy_request');
    this.biopsyRequestNavigator.addBiopsyRequest(this.form_encounterDetail.id);
  }

  closeTab(id: string){
    this.tcUtilsArray.removeItemById(this.form_encounterPersist.selectedEncounterTabs, id);
  }

  updateSelectedTab(event: MatCheckboxChange, encounterTab: EncounterTabs){
    if (event.checked){
      if(!this.tcUtilsArray.getById(this.form_encounterPersist.selectedEncounterTabs, encounterTab.id)){
        this.form_encounterPersist.selectedEncounterTabs.push(encounterTab)
      }
    } else {
      if(this.tcUtilsArray.getById(this.form_encounterPersist.selectedEncounterTabs, encounterTab.id)){
        this.tcUtilsArray.removeItemById(this.form_encounterPersist.selectedEncounterTabs, encounterTab.id)
      }
    }
  }

  getMartialStatus(){
    return this.patientDetail.maritial_status != -1 ? `Maritial Status: ${this.tcUtilsArray.getEnum(this.patientPersist.maritalStatus, this.patientDetail.maritial_status)?.name}` : ''
  }
}
