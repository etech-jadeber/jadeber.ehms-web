import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormEncounterEditComponent } from './form-encounter-edit.component';

describe('FormEncounterEditComponent', () => {
  let component: FormEncounterEditComponent;
  let fixture: ComponentFixture<FormEncounterEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEncounterEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEncounterEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
