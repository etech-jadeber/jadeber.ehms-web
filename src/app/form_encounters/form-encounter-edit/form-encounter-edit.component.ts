
import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCModalModes, TCParentChildIds} from "../../tc/models";


import {Form_EncounterDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { bed_status, ServiceType, MeansOfArrival, physican_type, Severity } from 'src/app/app.enums';
import { AppointmentSummary } from 'src/app/appointments/appointment.model';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { QueuesPersist } from 'src/app/queues/queues.persist';


@Component({
  selector: 'app-form_encounter-edit',
  templateUrl: './form-encounter-edit.component.html',
  styleUrls: ['./form-encounter-edit.component.css']
})
export class Form_EncounterEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  showServerity:boolean = false;
  form_encounterDetail: Form_EncounterDetail;
  date: Date = new Date();
  nurseFullName:string = ""
  appointmentSummary:AppointmentSummary = null;
  serviceType =ServiceType;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              private tCUtilsDate: TCUtilsDate,
              public appTranslation: AppTranslation,
              private appointmentPersist:AppointmentPersist,
              private departmentPersist:DepartmentPersist,
              public doctorNav:DoctorNavigator,
              public dialogRef: MatDialogRef<Form_EncounterEditComponent>,
              public persist: Form_EncounterPersist,
              public queuesPersist: QueuesPersist,

              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isWizard(): boolean {
    return this.idMode.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.idMode.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] == TCModalModes.EDIT;

  }


  ngOnInit() {
    this.appointmentPersist.getAppointment(this.idMode.context["appointment_id"]).subscribe(
      {next: res =>{
        this.appointmentSummary  = res;
        console.log(res);
        
        if(this.appointmentSummary.service_type == this.serviceType.emergency){
          this.showServerity = true
        }
       
      }}
    )


    if (this.isNew()) {
      this.tcAuthorization.requireCreate("form_encounters");
      this.title = this.appTranslation.getText("general", "new") + " "+this.appTranslation.getText("patient", "encounter");
      this.form_encounterDetail = new Form_EncounterDetail();
    
      this.form_encounterDetail.severity = Severity.non_urgent;
      this.form_encounterDetail.means_of_arrival = MeansOfArrival.ambulance;  
      //set necessary defaults
    } else if (this.isWizard()) {
      this.tcAuthorization.requireCreate("form_encounters");
      this.title = this.appTranslation.getText("general", "new") + " "+this.appTranslation.getText("patient", "encounter");
      this.form_encounterDetail = new Form_EncounterDetail();
      this.form_encounterDetail.appointmentId = this.idMode.context['appointment_id'];
      this.form_encounterDetail.severity = -1;
      this.form_encounterDetail.reason = ""
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("form_encounters");
      this.title = this.appTranslation.getText("general", "edit") + " " +this.appTranslation.getText("patient", "encounter");
      this.isLoadingResults = true;
      this.persist.getForm_Encounter(this.idMode.childId).subscribe(form_encounterDetail => {
        this.form_encounterDetail = form_encounterDetail;
        this.isLoadingResults = false;
        this.transformDates(false);
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    console.log(this.form_encounterDetail)
    this.persist.addForm_Encounter(this.form_encounterDetail).subscribe(value => {
      this.tcNotification.success("Encounter added");
      this.form_encounterDetail.id = value.id;
      this.queuesPersist.addQueues({pid:value.pid,provider_id:value.provider_id,queue_type:'consultation',encounter_id:value.id}).subscribe((res)=>{
        console.log("queues added",res)
      })
      this.dialogRef.close(this.form_encounterDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.updateForm_Encounter(this.form_encounterDetail).subscribe(value => {
      this.tcNotification.success("Encounter updated");
      this.dialogRef.close(this.form_encounterDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  canSubmit(): boolean {
    if (this.form_encounterDetail == null) {
      return false;
    }

    if (this.form_encounterDetail.reason == null || this.form_encounterDetail.reason == "") {
      return false;
    }


    return true;
  }


  transformDates(todate: boolean = true) {
    if (todate) {
      this.form_encounterDetail.date = this.tCUtilsDate.toTimeStamp(this.date);


    } else {
      this.date = this.tCUtilsDate.toDate(this.form_encounterDetail.date);


    }
  }


}
