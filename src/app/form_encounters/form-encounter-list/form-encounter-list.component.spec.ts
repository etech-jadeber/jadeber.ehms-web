import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Form_EncounterListComponent } from './form-encounter-list.component';

describe('Form_EncounterListComponent', () => {
  let component: Form_EncounterListComponent;
  let fixture: ComponentFixture<Form_EncounterListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Form_EncounterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Form_EncounterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
