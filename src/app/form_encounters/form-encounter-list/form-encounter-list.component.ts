import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Form_EncounterPersist} from "../form_encounter.persist";
import {Form_EncounterNavigator} from "../form_encounter.navigator";
import {encounterFilterColumn, Form_EncounterDetail, Form_EncounterSummary, Form_EncounterSummaryPartialList} from "../form_encounter.model";
import { Case_TransferNavigator } from '../case-transfer/case-transfer.navigator';
import { interval } from 'rxjs';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { VitalNavigator } from 'src/app/vitals/vital.navigator';
import { ServiceType, encounter_status } from 'src/app/app.enums';
import { OpdObservationSummary } from 'src/app/opd_observation/opd_observation.model';
import { OrderedOtheServiceDetail } from 'src/app/ordered_othe_service/ordered_othe_service.model';
import { OrderedOtheServiceNavigator } from 'src/app/ordered_othe_service/ordered_othe_service.navigator';
import { Procedure_OrderNavigator } from '../procedure_orders/procedure_order.navigator';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomDetail } from 'src/app/bed/bedrooms/bedroom.model';
import { BedDetail } from 'src/app/bed/beds/bed.model';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
import { BedroomPersist } from 'src/app/bed/bedrooms/bedroom.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-form_encounter-list',
  templateUrl: './form-encounter-list.component.html',
  styleUrls: ['./form-encounter-list.component.css']
})
export class Form_EncounterListComponent implements OnInit {

  form_encountersData: Form_EncounterSummary[] = [];
  form_encountersTotalCount: number = 0;
  form_encountersSelectAll:boolean = false;
  form_encountersSelection: Form_EncounterSummary[] = [];

  form_encountersOutpatientDisplayedColumns: string[] = ["select","action", "p.pid", "full_name", "date","reason","sensitivity","provider_id","service_type" ];
  form_encountersDisplayedColumns: string[] = this.form_encountersOutpatientDisplayedColumns;
  form_encountersInpatientDisplayedColumns: string[] = ["select","action", "p.pid", "full_name", "date","ward_id","bed_room_id","provider_id","service_type" ];
  form_encountersSearchTextBox: FormControl = new FormControl();
  form_encountersIsLoading: boolean = false;
  patientNameTextBox: FormControl = new FormControl();
  patientIdTextBox: FormControl = new FormControl();
  caseTransfer:boolean=false;
  message: string = "";
  success_state = 3;
  bedRoomName: string = "";
  beds: {[id: string] : BedDetail} = {}
  bedrooms : {[id: string] : BedroomDetail} = {}
  wards : {[id: string] : BedroomtypeDetail} = {}
  service_type = ServiceType
  date: FormControl = new FormControl({disabled: true, value: new Date()})
  encounterFilterColumn = encounterFilterColumn



  @ViewChild(MatPaginator, {static: true}) form_encountersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) form_encountersSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                private tcAsyncJob: TCAsyncJob,
                private filePersist: FilePersist,
                public jobData: JobData,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public form_encounterPersist: Form_EncounterPersist,
                public formVitalNav:VitalNavigator,
                public form_encounterNavigator: Form_EncounterNavigator,
                public jobPersist: JobPersist,
                public caseTransferNavigator: Case_TransferNavigator,
                public orderedOtherServiceNavigator: OrderedOtheServiceNavigator,
                public procedureOrderNavigator: Form_EncounterNavigator,
                public otherServiceNaviagator: OtherServicesNavigator,
                public bedRoomTypeNavigator: BedroomtypeNavigator,
                public wardPersist: BedroomtypePersist,
                public bedPersist: BedPersist,
                public bedRoomPersist: BedroomPersist,
                public tcUtilsString: TCUtilsString,
    ) {

        this.tcAuthorization.requireRead("form_encounters");
        if (this.form_encounterPersist.encounterSearchHistory.service_type == ServiceType.inpatient){
          this.form_encountersDisplayedColumns = this.form_encountersInpatientDisplayedColumns;}
          if (this.form_encounterPersist.encounterSearchHistory.service_type != ServiceType.inpatient){
            this.form_encounterPersist.encounterSearchHistory.date = this.tcUtilsDate.toTimeStamp(new Date());}
       this.form_encountersSearchTextBox.setValue(form_encounterPersist.encounterSearchHistory.search_text);
       this.form_encounterPersist.encounterSearchHistory.encounter_status = encounter_status.opened
       this.form_encounterPersist.encounterSearchHistory.show_only_mine = true;
      //delay subsequent keyup events
      this.form_encountersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.form_encounterPersist.encounterSearchHistory.search_text = value;
        this.searchForm_Encounters();
      });


      this.patientNameTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.form_encounterPersist.patientName = value;
        this.searchForm_Encounters();
      });

      this.patientIdTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.form_encounterPersist.encounterSearchHistory.patientId = value;
        this.searchForm_Encounters();
      });


      this.date.valueChanges.pipe().subscribe(value => {
        this.form_encounterPersist.encounterSearchHistory.date = value ? this.tcUtilsDate.toTimeStamp(value ? value : value._d) : null
        this.searchForm_Encounters();
      });
    }

    ngOnInit() {

      this.form_encountersSort.sortChange.subscribe(() => {
        this.form_encountersPaginator.pageIndex = 0;
        this.searchForm_Encounters(true);
      });

      this.form_encountersPaginator.page.subscribe(() => {
        this.searchForm_Encounters(true);
      });
      //start by loading items
      this.searchForm_Encounters();
    }

    searchWard(){
      let dialogRef = this.bedRoomTypeNavigator.pickBedroomtypes(true)
      dialogRef.afterClosed().subscribe(
        (ward: BedroomtypeDetail[]) => {
          if (ward){
            this.form_encounterPersist.encounterSearchHistory.ward_id = ward[0].id
            this.bedRoomName = ward[0].name
            this.searchForm_Encounters()
          }
        }
      )
    }

    handleServiceChange(event: number) {
      if (event == ServiceType.inpatient){
        this.date.setValue('')
        this.form_encountersDisplayedColumns = this.form_encountersInpatientDisplayedColumns
      } else {
        this.date.setValue(new Date())
        this.form_encountersDisplayedColumns = this.form_encountersOutpatientDisplayedColumns
      }
    }

  searchForm_Encounters(isPagination:boolean = false): void {


    let paginator = this.form_encountersPaginator;
    let sorter = this.form_encountersSort;

    this.form_encountersIsLoading = true;
    this.form_encountersSelection = [];

    this.form_encounterPersist.searchForm_Encounter(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Form_EncounterSummaryPartialList) => {
      this.form_encountersData = partialList.data;
      if (this.form_encounterPersist.encounterSearchHistory.service_type == ServiceType.inpatient){
        this.form_encountersData.forEach(encounter => {
          if (!this.wards[encounter.ward_id]){
            this.wards[encounter.ward_id] = new BedroomtypeDetail()
            encounter.ward_id && encounter.ward_id != this.tcUtilsString.invalid_id && this.wardPersist.getBedroomtype(encounter.ward_id).subscribe(
              ward => {
                this.wards[encounter.ward_id] = ward;
              }
            )
          }
          if (!this.beds[encounter.bed_id]){
            this.beds[encounter.bed_id] = new BedDetail()
            encounter.bed_id && encounter.bed_id != this.tcUtilsString.invalid_id && this.bedPersist.getBed(encounter.bed_id).subscribe(
              bed => {
                this.beds[encounter.bed_id] = bed;
              }
            )
          }
          if (!this.bedrooms[encounter.bed_room_id]){
            this.bedrooms[encounter.bed_room_id] = new BedroomDetail()
            encounter.bed_room_id && encounter.bed_room_id != this.tcUtilsString.invalid_id && this.bedRoomPersist.getBedroom(encounter.bed_room_id).subscribe(
              bedroom => {
                this.bedrooms[encounter.bed_room_id] = bedroom;
              }
            )
          }
        })
      }
      if (partialList.total != -1) {
        this.form_encountersTotalCount = partialList.total;
      }
      this.form_encountersIsLoading = false;
    }, error => {
      this.form_encountersIsLoading = false;
    });

  }

  additionalService(detail: Form_EncounterDetail){
    let dialogRef = this.orderedOtherServiceNavigator.addOrderedOtheService(detail.id)
    dialogRef.afterClosed().subscribe(
      (otherService: OrderedOtheServiceDetail) => {
        if (otherService){
          this.searchForm_Encounters()
        }
      }
    )
  }

  procedureOrder(detail: Form_EncounterDetail){
    let dialogRef = this.procedureOrderNavigator.addPatient_Procedure(
      detail.id
    );
    dialogRef.afterClosed().subscribe((newPatient_Procedure) => {
    });
  }

  oxygenService(detail: Form_EncounterDetail){
    let dialogRef = this.otherServiceNaviagator.addOxygenServices(detail.pid)
    dialogRef.afterClosed().subscribe(
      (otherService: OrderedOtheServiceDetail) => {
      }
    )
  }

  downloadForm_Encounters(): void {
    if(this.form_encountersSelectAll){
         this.form_encounterPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download form_encounters", true);
      });
    }
    else{
        this.form_encounterPersist.download(this.tcUtilsArray.idsList(this.form_encountersSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download form_encounters",true);
            });
        }
  }



  addForm_Encounter(): void {
    let dialogRef = this.form_encounterNavigator.addForm_Encounter();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchForm_Encounters();
      }
    });
  }

  editForm_Encounter(item: Form_EncounterSummary) {
    let dialogRef = this.form_encounterNavigator.editForm_Encounter(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteForm_Encounter(item: Form_EncounterSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Form_Encounter");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.form_encounterPersist.deleteForm_Encounter(item.id).subscribe(response => {
          this.tcNotification.success("Form_Encounter deleted");
          this.searchForm_Encounters();
        }, error => {
        });
      }

    });
  }
  transferCase(item: Form_EncounterSummary) {
    this.caseTransfer=true;
    let dialogRef = this.caseTransferNavigator.transferCase(item.id,this.caseTransfer);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchForm_Encounters();
      }
    });
  }

  createReport(id: string): void {
    this.form_encounterPersist.reportSmmary(id, "patient_summary_report", {id}).subscribe(downloadJob => {
      this.message = "";
      this.jobPersist.followJob(downloadJob.id, "downloading Patient summary reports", true);
      console.log("the downloaded job id is ", downloadJob.id);
      this.jobPersist.getJob(downloadJob.id).subscribe(
        job => {
          const success_state =3;
          if (job.job_state == success_state) {
          }
        }

      )
    }, error => {
    })
  }


  followJob(id: string, descrption: string, is_download_job: boolean = true): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }


    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe(n => {

      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

            if (is_download_job) {
              this.jobPersist.downloadFileKey(id).subscribe(jobFile => {
                this.tcNavigator.openUrl(this.filePersist.downloadLink(jobFile.download_key))
              });
            }

        }
        else{
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);

          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }

  back():void{
      this.location.back();
    }

    ngOnDestroy(): void {
      // this.form_encounterPersist.encounter_department = undefined;
      // this.form_encounterPersist.patientId = "";
      // this.form_encounterPersist.show_only_mine = false;
      // this.form_encounterPersist.form_encounterSearchText = "";
      // this.form_encounterPersist.encounterStatusFilter = null;

    }

    getWard(ward_id: string): string {
      if (!this.wards[ward_id]){
        return ''
      }
      return this.wards[ward_id].name
    }

    getBedroom(bed_room_id: string): string {
      if (!this.bedrooms[bed_room_id]){
        return ''
      }
      return this.bedrooms[bed_room_id].name
    }

    getBed(bed_id: string): string {
      if (!this.beds[bed_id]?.serial_id){
        return ''
      }
      return `${this.beds[bed_id]?.serial_id}`
    }

}
