import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormEncounterPickComponent } from './form-encounter-pick.component';

describe('FormEncounterPickComponent', () => {
  let component: FormEncounterPickComponent;
  let fixture: ComponentFixture<FormEncounterPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEncounterPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEncounterPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
