import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Form_EncounterDetail, Form_EncounterSummary, Form_EncounterSummaryPartialList} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";


@Component({
  selector: 'app-form_encounter-pick',
  templateUrl: './form-encounter-pick.component.html',
  styleUrls: ['./form-encounter-pick.component.css']
})
export class Form_EncounterPickComponent implements OnInit {

  form_encountersData: Form_EncounterSummary[] = [];
  form_encountersTotalCount: number = 0;
  form_encountersSelection: Form_EncounterSummary[] = [];
  form_encountersDisplayedColumns: string[] = ["select", 'date','reason','pid','sensitivity','pc_catid','provider_id','encounter' ];

  form_encountersSearchTextBox: FormControl = new FormControl();
  form_encountersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) form_encountersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) form_encountersSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public form_encounterPersist: Form_EncounterPersist,
              public dialogRef: MatDialogRef<Form_EncounterPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("form_encounters");
    this.form_encountersSearchTextBox.setValue(form_encounterPersist.encounterSearchHistory.search_text);
    //delay subsequent keyup events
    this.form_encountersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.form_encounterPersist.encounterSearchHistory.search_text = value;
      this.searchForm_Encounters();
    });
  }

  ngOnInit() {

    this.form_encountersSort.sortChange.subscribe(() => {
      this.form_encountersPaginator.pageIndex = 0;
      this.searchForm_Encounters();
    });

    this.form_encountersPaginator.page.subscribe(() => {
      this.searchForm_Encounters();
    });

    //set initial picker list to 5
    this.form_encountersPaginator.pageSize = 5;

    //start by loading items
    this.searchForm_Encounters();
  }

  searchForm_Encounters(): void {
    this.form_encountersIsLoading = true;
    this.form_encountersSelection = [];

    this.form_encounterPersist.searchForm_Encounter(this.form_encountersPaginator.pageSize,
        this.form_encountersPaginator.pageIndex,
        this.form_encountersSort.active,
        this.form_encountersSort.direction).subscribe((partialList: Form_EncounterSummaryPartialList) => {
      this.form_encountersData = partialList.data;
      if (partialList.total != -1) {
        this.form_encountersTotalCount = partialList.total;
      }
      this.form_encountersIsLoading = false;
    }, error => {
      this.form_encountersIsLoading = false;
    });

  }

  markOneItem(item: Form_EncounterSummary) {
    if(!this.tcUtilsArray.containsId(this.form_encountersSelection,item.id)){
          this.form_encountersSelection = [];
          this.form_encountersSelection.push(item);
        }
        else{
          this.form_encountersSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.form_encountersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
