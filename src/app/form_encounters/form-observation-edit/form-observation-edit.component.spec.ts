import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormObservationEditComponent } from './form-observation-edit.component';

describe('FormObservationEditComponent', () => {
  let component: FormObservationEditComponent;
  let fixture: ComponentFixture<FormObservationEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormObservationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormObservationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
