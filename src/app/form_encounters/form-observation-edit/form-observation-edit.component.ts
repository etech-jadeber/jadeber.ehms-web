import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Form_ObservationDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";


@Component({
  selector: 'app-form-observation-edit',
  templateUrl: './form-observation-edit.component.html',
  styleUrls: ['./form-observation-edit.component.css']
})
export class Form_ObservationEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  form_observationDetail: Form_ObservationDetail = new Form_ObservationDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Form_ObservationEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew()) {
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("patient", "observation");
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("patient", "observation");
      this.isLoadingResults = true;
      this.persist.getForm_Observation(this.ids.parentId, this.ids.childId).subscribe(form_observationDetail => {
        this.form_observationDetail = form_observationDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.form_observationDetail == null){
          return false;
        }

     if (this.form_observationDetail.observation == null || this.form_observationDetail.observation  == "") {
                      return false;
                    }

if (this.form_observationDetail.code_type == null || this.form_observationDetail.code_type  == "") {
                      return false;
                    }

if (this.form_observationDetail.description == null || this.form_observationDetail.description  == "") {
                      return false;
                    }

if (this.form_observationDetail.code  == null || this.form_observationDetail.code   == "") {
                      return false;
                    }

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addForm_Observation(this.ids.parentId, this.form_observationDetail).subscribe(value => {
      this.tcNotification.success("Form_Observation added");
      this.form_observationDetail.id = value.id;
      this.dialogRef.close(this.form_observationDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateForm_Observation(this.ids.parentId, this.form_observationDetail).subscribe(value => {
      this.tcNotification.success("Form_Observation updated");
      this.dialogRef.close(this.form_observationDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
