import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSoapDetailComponent } from './form-soap-detail.component';

describe('FormSoapDetailComponent', () => {
  let component: FormSoapDetailComponent;
  let fixture: ComponentFixture<FormSoapDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSoapDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSoapDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
