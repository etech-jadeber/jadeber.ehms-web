import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { encounter_status, tabs } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { CompanyDetail } from 'src/app/Companys/Company.model';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { TCAuthorization } from 'src/app/tc/authorization';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import {Location} from '@angular/common';
import { Form_EncounterNavigator } from '../form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { Form_SoapDetail } from '../form_encounter.model';
import { UserPersist } from 'src/app/tc/users/user.persist';

@Component({
  selector: 'app-form-soap-detail',
  templateUrl: './form-soap-detail.component.html',
  styleUrls: ['./form-soap-detail.component.scss']
})
export class FormSoapDetailComponent implements OnInit {
  paramsSubscription: Subscription;
  soapLoading:boolean = false;
  activeTab: number = tabs.overview;
  //basics
  soapDetail: Form_SoapDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public form_encounterPersist: Form_EncounterPersist,
              public formSoapNavigator: Form_EncounterNavigator,
              public  formSoapPersist: Form_EncounterPersist,
              public menuState: MenuState,
              public userPersist: UserPersist,) {
    this.tcAuthorization.requireRead("form_soaps");
    this.soapDetail = new Form_SoapDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("form_soaps");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.soapLoading = true;
    this.formSoapPersist.getForm_Soap(id).subscribe(soapDetail => {
          this.soapDetail = soapDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(soapDetail.encounter).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.userPersist.getUser(this.soapDetail.provider_id).subscribe(
            user => this.soapDetail.provider_name = user.name
          )
          this.menuState.getDataResponse(this.soapDetail);
          this.soapLoading = false;
        }, error => {
          console.error(error);
          this.soapLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.soapDetail.id);
  }

  editSoap(): void {
    let modalRef = this.formSoapNavigator.editForm_Soap(this.soapDetail.encounter, this.soapDetail.id);
    modalRef.afterClosed().subscribe(soapDetail => {
      TCUtilsAngular.assign(this.soapDetail, soapDetail);
    }, error => {
      console.error(error);
    });
  }

   printSoap():void{
      this.formSoapPersist.print(this.soapDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print PSOAP", true);
      });
    }

  back():void{
      this.location.back();
    }

}
