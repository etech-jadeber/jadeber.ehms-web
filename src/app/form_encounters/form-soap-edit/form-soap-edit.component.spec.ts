import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FormSoapEditComponent } from './form-soap-edit.component';

describe('FormSoapEditComponent', () => {
  let component: FormSoapEditComponent;
  let fixture: ComponentFixture<FormSoapEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FormSoapEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSoapEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
