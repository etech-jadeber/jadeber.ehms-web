import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Form_SoapDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { TCAuthorization } from 'src/app/tc/authorization';
import { QueuesPersist } from 'src/app/queues/queues.persist';


@Component({
  selector: 'app-form_soap-edit',
  templateUrl: './form-soap-edit.component.html',
  styleUrls: ['./form-soap-edit.component.css']
})
export class Form_SoapEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  form_soapDetail: Form_SoapDetail = new Form_SoapDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Form_SoapEditComponent>,
              public appTranslation:AppTranslation,
              private tcAuthorization:TCAuthorization,
              public  persist: Form_EncounterPersist,
              public queuesPersist: QueuesPersist,

              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }
  isNew_asNew(): boolean {
    return this.ids.context?.new === true;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log("this.isNew_asNew()",this.isNew_asNew())
    if (this.isNew()) {
      this.tcAuthorization.canCreate("form_soaps");
      this.title = this.appTranslation.getText("general","new") + " "+ " Progress Note";
      //set necessary defaults
      this.form_soapDetail.objective = "";
      this.form_soapDetail.assessment = "";
      this.form_soapDetail.plan = "";
      this.form_soapDetail.problem = "";
      this.form_soapDetail.final_diagnosis = "";
    } else {
      this.tcAuthorization.canUpdate("form_soaps");
      this.title = this.appTranslation.getText("general","edit") + " "+ " Progress Note";
      this.isLoadingResults = true;
      this.persist.getForm_Soap(this.ids.childId).subscribe(form_soapDetail => {
        this.form_soapDetail = form_soapDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.form_soapDetail == null){
          return false;
        }
        if (this.form_soapDetail.note == null || this.form_soapDetail.note  == "") {
          return false;
        }
      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addForm_Soap(this.ids.parentId, this.form_soapDetail).subscribe(value => {
      this.queuesPersist.updateQueues({queue_status:"opened"},this.ids.parentId).subscribe((res)=>{
        console.log("queues upateded lab",res)
       })
      this.tcNotification.success("Form_Soap added");
      this.form_soapDetail.id = value.id;
      this.dialogRef.close(this.form_soapDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onAdd_asNew(): void {
    this.isLoadingResults = true;
    this.persist.addForm_Soap(this.ids.parentId, this.form_soapDetail).subscribe(value => {
      this.tcNotification.success("Form_Soap added");
      this.form_soapDetail.id = value.id;
      this.dialogRef.close(this.form_soapDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateForm_Soap(this.form_soapDetail).subscribe(value => {
      this.tcNotification.success("Form_Soap updated");
      this.dialogRef.close(this.form_soapDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
