import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormVitalsDetailComponent } from './form-vitals-detail.component';

describe('FormVitalsDetailComponent', () => {
  let component: FormVitalsDetailComponent;
  let fixture: ComponentFixture<FormVitalsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormVitalsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormVitalsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
