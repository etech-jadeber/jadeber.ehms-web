import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { encounter_status, ServiceType } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { Form_VitalsDetail } from '../form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { Location } from "@angular/common";

@Component({
  selector: 'app-form-vitals-detail',
  templateUrl: './form-vitals-detail.component.html',
  styleUrls: ['./form-vitals-detail.component.scss']
})
export class FormVitalsDetailComponent implements OnInit {

  paramsSubscription: Subscription;
  vitalIsLoading:boolean = false;
  
  //basics
  vitalDetail: Form_VitalsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public vitalNavigator: Form_EncounterNavigator,
              public form_encounterPersist: Form_EncounterPersist,
              public  vitalPersist: Form_EncounterPersist) {
    this.tcAuthorization.requireRead("form_vitals");
    this.vitalDetail = new Form_VitalsDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("form_vitals");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.vitalIsLoading = true;
    this.vitalPersist.getForm_Vitals(id).subscribe(vitalDetail => {
          this.vitalDetail = vitalDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(vitalDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.vitalIsLoading = false;
        }, error => {
          console.error(error);
          this.vitalIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.vitalDetail.id);
  }
  editMedicationAdminstrationChart(): void {
    let modalRef = this.vitalNavigator.editForm_Vitals("", this.vitalDetail.id, ServiceType.inpatient);
    modalRef.afterClosed().subscribe(vitalDetail => {
      TCUtilsAngular.assign(this.vitalDetail, vitalDetail);
    }, error => {
      console.error(error);
    });
  }

  //  printBirth():void{
  //     this.medicationAdminstrationChartPersist.print(this.medicationAdminstrationChartDetail.id).subscribe(printJob => {
  //       this.jobPersist.followJob(printJob.id, "print medication_adminstration_chart", true);
  //     });
  //   }

  back():void{
      this.location.back();
    }

  valueOrEmptyString(val: number){
    if (val == -1){
      return undefined;
    }
    return val
  }

}
