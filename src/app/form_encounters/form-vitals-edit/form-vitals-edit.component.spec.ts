import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Form_VitalsEditComponent } from './form-vitals-edit.component';

describe('Form_VitalsEditComponent', () => {
  let component: Form_VitalsEditComponent;
  let fixture: ComponentFixture<Form_VitalsEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Form_VitalsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Form_VitalsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
