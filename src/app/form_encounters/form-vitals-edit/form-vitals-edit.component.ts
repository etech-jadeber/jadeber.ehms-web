import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCNotification} from '../../tc/notification';
import {TCUtilsString} from '../../tc/utils-string';
import {TCModalModes, TCParentChildIds} from '../../tc/models';
import {AppTranslation} from '../../app.translation';

import {Form_VitalsDetail, Form_VitalsSummary} from '../form_encounter.model';
import {Form_EncounterPersist} from '../form_encounter.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { MedicationAdminstrationChartPersist } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.persist';
import { ServiceType, temp_method } from 'src/app/app.enums';

@Component({
  selector: 'app-form_vitals-edit',
  templateUrl: './form-vitals-edit.component.html',
  styleUrls: ['./form-vitals-edit.component.css'],
})
export class Form_VitalsEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  form_vitalsDetail: Form_VitalsDetail = new Form_VitalsDetail();
  autoCalculatedBMI: string;
  autoCalculatedBSA: string;
  patientType = ServiceType

  constructor(
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public dialogRef: MatDialogRef<Form_VitalsEditComponent>,
    public appTranslation: AppTranslation,
    private tcAuthorization:TCAuthorization,
    public persist: Form_EncounterPersist,
    public medicationPersist: MedicationAdminstrationChartPersist,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds
  ) {
  }

  isNew(): boolean {
    return this.ids.context.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.ids.context.mode === TCModalModes.WIZARD;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew() || this.isWizard()) {
      this.tcAuthorization.requireCreate("form_vitals");
      this.title = this.appTranslation.getText("general","new") + " "+ this.appTranslation.getText("patient","form_vitals");
      //set necessary defaults
      this.form_vitalsDetail.temp_method = temp_method.infrared;
    } else {
      this.tcAuthorization.requireUpdate("form_vitals");
      this.title = this.appTranslation.getText("general","edit") + " "+ this.appTranslation.getText("patient","form_vitals");
      this.isLoadingResults = true;
      this.persist
        .getForm_Vitals(this.ids.childId)
        .subscribe(
          (form_vitalsDetail: Form_VitalsSummary) => {
            this.form_vitalsDetail = form_vitalsDetail;
            this.isLoadingResults = false;
          },
          (error) => {
            this.isLoadingResults = false;
            console.log(error);
          }
        );
    }
  }

  canSubmit(): boolean {
    if (this.form_vitalsDetail == null) {
      return false;
    }

    if (this.form_vitalsDetail.bmi > 999){
      return false;
    }
    if (this.form_vitalsDetail.pulse == null){
      return false;
    }
  
    if (this.form_vitalsDetail.temperature == null){
      return false;
    }
    if (this.form_vitalsDetail.temp_method == null){
      return false;
    }
    // if(this.form_vitalsDetail.oxygen_saturation == null || this.form_vitalsDetail.oxygen_saturation < 0|| this.form_vitalsDetail.oxygen_saturation > 100){
    //   return false;
    // }
    // if(this.form_vitalsDetail.oxygen_saturation_spo2==null|| this.form_vitalsDetail.oxygen_saturation_spo2 <0|| this.form_vitalsDetail.oxygen_saturation_spo2 > 100){
    //   return false;
    // }
    if(this.form_vitalsDetail.blood_pressure_diastolic==null){
      return false;
    }
    if(this.form_vitalsDetail.blood_pressure_systolic==null){
      return false;
    }
    return true;
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist
      .addForm_Vitals(this.ids.parentId, this.form_vitalsDetail)
      .subscribe(
        {
          next :(value) => {
            this.tcNotification.success('Form_Vitals added');
            this.form_vitalsDetail.id = value.id;
            this.dialogRef.close(this.form_vitalsDetail);
          },
          error: err=>{
            this.isLoadingResults = false;
          }
        }
        
      );

  }

 

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist
      .updateForm_Vitals(this.form_vitalsDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('Form_Vitals updated');
          this.dialogRef.close(this.form_vitalsDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }

  onWizard(): void {
    this.dialogRef.close(this.form_vitalsDetail)
  }

  updateMeasurement(){
    this.updateBMI();
    this.updateBSA();
  }

  updateBMI() {
    this.autoCalculatedBMI = (this.form_vitalsDetail.weight / Math.pow(this.form_vitalsDetail.height / 100, 2)).toPrecision(4);
    this.form_vitalsDetail.bmi = parseFloat(this.autoCalculatedBMI);
  }

  updateBSA() {
    this.autoCalculatedBSA = (Math.sqrt(this.form_vitalsDetail.weight * this.form_vitalsDetail.height) / 60).toPrecision(4);
    this.form_vitalsDetail.bsa = parseFloat(this.autoCalculatedBSA);
  }
}
