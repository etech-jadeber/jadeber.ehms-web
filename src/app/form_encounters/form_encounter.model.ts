import { Consent_FormDetail } from "../consent_forms/consent_form.model";
import { TCId, TCString } from "../tc/models";
import { DiagnosisSummary } from "./diagnosiss/diagnosis.model";

export type VitalImport<T> = {
  [id in keyof Form_VitalsDetail]?: keyof T;
};

export interface searchHistory {
  [kay: string]: any
}

export const encounterFilterColumn: TCString[] = [
  new TCString("p.pid", 'MRN'),
  new TCString("fname", 'First Name'),
  new TCString("mname", 'Middle Name'),
  new TCString("lname", 'Last Name'),
  new TCString("phone_cell", 'Phone Number'),
  new TCString("planned_procedure_text", 'Procedure'),
  new TCString("surgeon", 'Surgeon'),
];

export const prefixedFilterColumn: TCString[] = [
  new TCString("patient.pid", 'MRN'),
  new TCString("patient.full_name", 'Patient Name'),
  new TCString("consulting.full_name", 'Consulting Doctor'),
  new TCString("consulted.full_name", 'Consulted Doctor'),
];

export const patientProcedureFilterColumn: TCString[] = [
  new TCString("pid", 'MRN'),
  new TCString("patient_name", 'Patient Name'),
  new TCString("phone_cell", 'Phone Number'),
  new TCString("name", 'Procedure Name'),
];


export class Form_EncounterSummary extends TCId {
  date : number;
reason : string;
pid : string;
sensitivity : number;
provider_id : string;
assigned_nurse_id : string;
encounter_type : number;
full_name:string;
status : number;
service_type:number;
provider_name:string;
diagnosis:DiagnosisSummary[] = [];
means_of_arrival:number;
profile_pic:string;
severity:number;
department_id:string
patient_name: string;
encounter_count: number;
last_visit: number;
birth_date:number;
is_referal : boolean;
parent_encounter_id: string;
chief_compaint: string;
ward_id: string;
bed_room_id: string;
bed_id: string;
informant:string;
}

export class Form_EncounterSummaryPartialList {
  data: Form_EncounterSummary[];
  total: number;
}

export class Form_EncounterDetail extends Form_EncounterSummary {
  uuidd:string
  appointmentId
}

export class Form_EncounterDashboard {
  total: number;
}
export class Form_VitalsSummary extends TCId {
  pid : string;
weight : number;
BMI:number;
height : number;
temperature : number;
temp_method : number;
pulse : number;
bmi : number;
oxygen_saturation : number;
provider_name:string;
bsa : number;
creation_time: number;
blood_pressure_systolic:number;
blood_pressure_diastolic:number;
oxygen_saturation_spo2:number;
head_circumference:number;
muac:number;
pain_score:number;
output: number;
input: number;
gcs: string;
rbs: string;
urine_ketone: string;
encounter_id: string;
remark: string;


}

export class Form_VitalsSummaryPartialList {
  data: Form_VitalsSummary[];
  total: number;
}

export class Form_VitalsDetail extends Form_VitalsSummary {
  pid : string;
weight : number;
height : number;
temperature : number;
temp_method : number;
pulse : number;
bmi : number;
oxygen_saturation : number;
bsa : number;
}

export class Form_SoapSummary extends TCId {
  pid : string;
activity : string;
subjective : string;
objective : string;
assessment : string;
plan : string;
date : number;
problem : string;
note: string;
final_diagnosis : string;
encounter : string;
provider_id: string;
provider_name: string;
}

export class Form_SoapSummaryPartialList {
  data: Form_SoapSummary[];
  total: number;
}

export class Form_SoapDetail extends Form_SoapSummary {
  pid : string;
activity : string;
subjective : string;
objective : string;
assessment : string;
plan : string;
date : number;
problem : string;
final_diagnosis : string;
encounter : string;
}

export class Form_SoapDashboard {
  total: number;
}
//prescription model
export class PrescriptionsSummary extends TCId {
  form : number;
drug_id : string;
date_added : number;
patient_id : string;
encounter_id : string;
frequency: number;
provider_id : string;
how_to_use : number;
duration_unit:number;
other_information:string;
route:number;
dose: number;
drug:string;
duration:number;
diagnosis:string;
quantity: number;
note: string;
encounter: string;
date_modified: number;
name: string;
dosage: number;
unit:number;
}

export class PrescriptionsSummaryPartialList {
  data: PrescriptionsSummary[];
  total: number;
}

export class PrescriptionsDetail extends PrescriptionsSummary {
  form : number;
drug_id : string;
drug : string;
prescription: string;
date_added : number;
date_modified : number;
patient_id : string;
encounter : string;
provider_id : string;
note : string;
in_patient : number;
drug_name: string;
doctor_name: string;
patient_name: string;
procedure_name: string;
treatment:string;
}

// surgery appointment
export class Surgery_AppointmentSummary extends TCId {
patient_id : string;
planned_procedure : string;
doctor_id : string;
surgery_date : number;
start_time : number;
end_time : number;
surgery_status : number;
encounter_id: string;
diagnosis: string;
surgery_type: number;
order_date: number;
surgery_room: string;
operation_note_id: string;
procedure_name: string;
duration:number;
surgery_room_name: string;
sergeon_name : string;
sergeon_id: string;
patient_name: string;
consent_id:string
key:string;
}

export class Surgery_AppointmentSummaryPartialList {
  data: Surgery_AppointmentSummary[];
  total: number;
}

export class Surgery_AppointmentDetail extends Surgery_AppointmentSummary {
patient_id : string;
surgery_type_id : string;
doctor_id : string;
surgery_date : number;
start_time : number;
end_time : number;
surgery_status : number;
schedule:number;
}

export class Surgery_AppointmentDashboard {
  total: number;
}

export class Patient_ProcedureSummary extends TCId {
  procedure_id : string;
encounter_id : string;
patient_id : string;
doctor_id : string;
created_at : number;
procedure_name:string;
consent_id: string;
needs_consent: boolean;
payment_id: string;
consent_form: Consent_FormDetail;
instruction: string;
quantity: number;
  payment_status: number;
}

export class Patient_ProcedureSummaryPartialList {
  data: Patient_ProcedureSummary[];
  total: number;
}

export class Patient_ProcedureDetail extends Patient_ProcedureSummary {
  procedure_id : string;
encounter_id : string;
patient_id : string;
doctor_id : string;
created_at : number;
}


//observations
export class Form_ObservationSummary extends TCId {
  pid : number;
encounter : number;
observation : string;
code_type : string;
description : string;
code  : string;
}

export class Form_ObservationSummaryPartialList {
  data: Form_ObservationSummary[];
  total: number;
}

export class Form_ObservationDetail extends Form_ObservationSummary {
  pid : number;
encounter : number;
observation : string;
code_type : string;
description : string;
code  : string;
}

//system of review checks
export class Medical_CertificateSummary extends TCId {
  start_date?:number;
  end_date?:number;
  out_start_date?: number;
  out_end_date?: number;
  encounter:string;
age:number;
sex:string;
card_no:string;
diagnosis:string;
rest_required:string; 
// date_of_visit:number;
// sick_leave:string;
recommendation:string;
date_of_issued:number;
doctor:string;
patient_id : string;
}
export class Medical_CertificateSummaryPartialList {
  data: Medical_CertificateSummary[];
  total: number;
}



export class Medical_CertificateDetail extends Medical_CertificateSummary {

}

export class Encounter_ImagesSummary extends TCId {
  encounter : number;
image_id : string;
patient_id: string;
key:string;
}

export class Encounter_ImagesSummaryPartialList {
  data: Encounter_ImagesSummary[];
  total: number;
}

export class Encounter_ImagesDetail extends Encounter_ImagesSummary {
  encounter : number;
image_id : string;
name: string;
}

export class Physical_ExaminationSummary extends TCId {
  general_appearance : string;
ent : string;
eye : string;
head_and_neck : string;
glands : string;
chests : string;
cardiovascular : string;
abdominal : string;
genitourinary : string;
musculoskeletal : string;
integument : string;
neurology : string;
encounter_id : string;
vital_sign : string;
ent_bool : boolean;
eye_bool : boolean;
head_and_neck_bool : boolean;
glands_bool : boolean;
chests_bool : boolean;
cardiovascular_bool : boolean;
abdominal_bool : boolean;
genitourinary_bool : boolean;
musculoskeletal_bool : boolean;
integument_bool : boolean;
neurology_bool : boolean;
type: number;
provider_id: string;
}

export class Physical_ExaminationSummaryPartialList {
  data: Physical_ExaminationSummary[];
  total: number;
}

export class Physical_ExaminationDetail extends Physical_ExaminationSummary {
  general_appearance : string;
ent : string;
eye : string;
head_and_neck : string;
glands : string;
chests : string;
cardiovascular : string;
abdominal : string;
genitourinary : string;
musculoskeletal : string;
integument : string;
neurology : string;
encounter_id : string;
vital_sign : string;
}

export class  Review_Of_SystemSummary extends TCId {
  encounter_id : string;
patient_id:string;
provider_id:string;
general:boolean;
head_and_neck:boolean;
ent:boolean;
eye:boolean;
chest_lungs_breasts:boolean;
cardiovascular:boolean;
gastrointestinal:boolean;
musculoskeletal:boolean;
neurological:boolean;
general_remark:string;
head_and_neck_remark:string;
ent_remark:string;
chest_lungs_breasts_remark:string;
cardiovascular_remark:string;
gastrointestinal_remark:string;
musculoskeletal_remark:string;
neurological_remark:string;
eye_remark:string;
provider_name: string;
}

export class  Review_Of_SystemSummaryPartialList {
  data:  Review_Of_SystemSummary[];
  total: number;
}

export class  Review_Of_SystemDetail extends  Review_Of_SystemSummary {
  encounter_id : string;
// review_option_id : string;
}

export class  Review_Of_SystemDashboard {
  total: number;
}

export class Follow_Up_NoteSummary extends TCId {
  encounter_id : string;
note : string;
date : number;
}

export class Follow_Up_NoteSummaryPartialList {
  data: Follow_Up_NoteSummary[];
  total: number;
}

export class Follow_Up_NoteDetail extends Follow_Up_NoteSummary {
  encounter_id : string;
note : string;
date : number;
}


export class Rehabilitation_OrderSummary extends TCId {
  patient_id : string;
physiotherapy_type_id : string;
doctor_id : string;
encounter_id : string;
note : string;
created_at : number;
treatment_per_week : number;
physiotherapy_id : number;
status:number;
}

export class Rehabilitation_OrderSummaryPartialList {
  data: Rehabilitation_OrderSummary[];
  total: number;
}

export class Rehabilitation_OrderDetail extends Rehabilitation_OrderSummary {
}

export class ConsultingSummary extends TCId {
  encounter_id:string;
  consulting_dept_id:string;
  consulting_doctor_id:string;
  date:number;
  urgent:boolean;
  patient_convinience:boolean;
  brief_history:string;
  reason_for_consult:string;
  patient_condition: number;
  clinical_finding:string;
  treatment:string;
  report:string="";
  report_date;
  frequency:number;
   
  }
  export class ConsultingSummaryPartialList {
    data: ConsultingSummary[];
    total: number;
  }
  export class ConsultingDetail extends ConsultingSummary {
  }
  
  export class ConsultingDashboard {
    total: number;
  }

  export class EncounterTabs {
    id: string;
    icon: string;
    title: string;
    onResult:any;
    selector:string
    visible: boolean;
  }

  export class OrderDialysisSummary extends TCId {
    sub_type:number;
    type:number;
    patient_id:string;
    encounter_id:string;
    scheduled:boolean;
    frequency:number;
    patient_name:string;
     
    }
    export class OrderDialysisSummaryPartialList {
      data: OrderDialysisSummary[];
      total: number;
    }
    export class OrderDialysisDetail extends OrderDialysisSummary {
    }
    
    export class OrderDialysisDashboard {
      total: number;
    }


    export class HemoPerformedSummary extends TCId {
      patient_id:string;
      sub_type:number;
      type:number;
      start_time:number;
      end_time:number;
      machine_id:string;
      day:number;
      blood_flow:string;
      uf:string;
      dialysate_no:string;
      iv_fluid:string;
      acetate:string;
      bolus:string;
      during_dialysis:string;
      dialyser_type:string;
      dialysate_fluid:string;
      other:string;
      complication:string;
       
      }
      export class HemoPerformedSummaryPartialList {
        data: HemoPerformedSummary[];
        total: number;
      }
      export class HemoPerformedDetail extends HemoPerformedSummary {
      }
      
      export class HemoPerformedDashboard {
        total: number;
      }