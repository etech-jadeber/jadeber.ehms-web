import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { Router } from '@angular/router';
import { AppointmentSummary } from '../appointments/appointment.model';
import {
TcDictionary, TCModalModes,
TCParentChildIds,
TCIdMode
} from '../tc/models';
import { TCModalWidths } from '../tc/utils-angular';
import { Form_EncounterEditComponent } from './form-encounter-edit/form-encounter-edit.component';
import { Form_EncounterPickComponent } from './form-encounter-pick/form-encounter-pick.component';
import { Form_SoapEditComponent } from './form-soap-edit/form-soap-edit.component';
import { Form_VitalsEditComponent } from './form-vitals-edit/form-vitals-edit.component';
import { PrescriptionsEditComponent } from './prescriptions-edit/prescriptions-edit.component';
import { Form_Encounter_Surgery_AppointmentEditComponent } from './surgery-appointment-edit/surgery-appointment-edit.component';

import { Form_ObservationEditComponent } from './form-observation-edit/form-observation-edit.component';
import { Medical_CertificateEditComponent } from './medical-certificate-edit/medical-certificate-edit.component';
import { Encounter_ImagesEditComponent } from './encounter-images-edit/encounter-images-edit.component';
import { Physical_ExaminationEditComponent } from './physical-examination-edit/physical-examination-edit.component';
import { Review_Of_SystemEditComponent } from './review-of-system-edit/review-of-system-edit.component';
import { LabEditComponent } from './lab-edit/lab-edit.component';
import { Rehabilitation_OrderEditComponent } from './rehabilitation-order-edit/rehabilitation-order-edit.component';
import { PatientProcedureEditComponent } from './patient-procedure-edit/patient-procedure-edit.component';
import { ConsultingEditComponent } from './consulting-edit/consulting-edit.component';
import { AllergyFormEditComponent } from './allergy-form-edit/allergy-form-edit.component';
import { OrderType, prescription_type } from '../app.enums';
import { OrderDialysisEditComponent } from '../form_encounter/order-dialysis-edit/order-dialysis-edit.component';
import { OrderDialysisPickComponent } from '../form_encounter/order-dialysis-pick/order-dialysis-pick.component';
import { HemoPerformedEditComponent } from '../form_encounter/hemo-performed-edit/hemo-performed-edit.component';
import { Selected_OrdersSummary } from './orderss/orders.model';
import { PrescriptionsSummary } from './form_encounter.model';

@Injectable({
  providedIn: 'root',
})
export class Form_EncounterNavigator {
  constructor(private router: Router, public dialog: MatDialog) { }

  form_encountersUrl(): string {
    return '/form_encounters';
  }

  form_encounterUrl(id: string): string {
    return '/form_encounters/' + id;
  }

  viewForm_Encounters(): void {
    this.router.navigateByUrl(this.form_encountersUrl());
  }
  consultingUrl(id: string): string {
    return "/consultings/" + id;
  }
  overviewUrl(id: string): string {
    return "/overview/" + id;
  }
  form_soapUrl(id: string): string {
    return '/form_soaps/' + id;
  }
  viewConsulting(id: string): void {
    this.router.navigateByUrl(this.consultingUrl(id));
  }

  viewForm_Encounter(id: string): void {
    this.router.navigateByUrl(this.form_encounterUrl(id));
  }

  editForm_Encounter(
    id: string,
    isWizard: boolean = false,
    appointment: AppointmentSummary = null
  ): MatDialogRef<unknown,any> {
    let params: TcDictionary<string> = new TcDictionary();
    params['mode'] = isWizard
      ? TCModalModes.WIZARD
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
    if (appointment != null) {
    }
    if (appointment != null) {
      params['appointment_id'] = appointment.id;
    }

    const dialogRef = this.dialog.open(Form_EncounterEditComponent, {
      data: new TCParentChildIds(null, id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addForm_Encounter(
    isWizard: boolean = false,
    appointment: AppointmentSummary = null
  ): MatDialogRef<unknown,any> {
    return this.editForm_Encounter(null, isWizard, appointment);
  }

  pickForm_Encounters(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_EncounterPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  pickFormDialysis(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OrderDialysisPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }


  //form_vitalss

  form_VitalUrl(id: string){
    return '/form_vitals/' + id
  }

  editForm_Vitals(
    parentId: string,
    id: string,
    patientType: number,
    mode: string = TCModalModes.EDIT
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_VitalsEditComponent, {
      data: new TCParentChildIds(parentId, id, {patientType, mode}),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addForm_Vitals(parentId: string, patientType: number): MatDialogRef<unknown,any> {
    return this.editForm_Vitals(parentId, null, patientType, TCModalModes.NEW);
  }

  wizardForm_Vitals(parentId: string, patientType: number){
    return this.editForm_Vitals(parentId, null, patientType, TCModalModes.WIZARD)
  }
  //consulting
  editConsulting(parentId:string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ConsultingEditComponent, {
      data:  new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addConsulting(parentId:string): MatDialogRef<unknown,any> {
    return this.editConsulting(parentId,null);
  }

  //patient_procedures
patientProceduresUrl():string{
  return "/patient_precedure"
}

patientProcedureUrl(id: string):string{
  return "/patient_precedure/" + id
}

editPatient_Procedure(parentId:string, id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(PatientProcedureEditComponent, {
    data:  new TCParentChildIds(parentId, id),
    width: TCModalWidths.medium
  });
  return dialogRef;
}

addPatient_Procedure(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editPatient_Procedure(parentId,null);
}

wizardPatient_Procedure(): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(PatientProcedureEditComponent, {
    data:  {parentId: null, childId: null, },
    width: TCModalWidths.medium
  });
  return dialogRef;
}

  //form_soaps
  editForm_Soap(parentId: string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_SoapEditComponent, {
      data: new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }
  editForm_Soap_as_new(parentId: string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_SoapEditComponent, {
      data: new TCParentChildIds(parentId, id,{new:true}),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }




  addLab(isWizard:boolean = false,parentId: string,id:string = null,futureDate:number=null, orderType: number[], order_id: string = null, forbiddenSelect: Selected_OrdersSummary[] = []): MatDialogRef<unknown,any> {
    let params: TcDictionary<string | Function | number | number[] | Selected_OrdersSummary[]> = new TcDictionary();
    params['future_date'] = futureDate.toString();
    params['mode'] = isWizard
      ? TCModalModes.WIZARD
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
    params["parentId"]=parentId;
    params["orderType"]=orderType;
    params["order_id"]=order_id
    params['forbiddenSelect'] = forbiddenSelect
    const dialogRef = this.dialog.open(LabEditComponent, {
      data: new TCParentChildIds(null, id, params),
      width: TCModalWidths.xtra_large,
    });
    return dialogRef;


  }




  addForm_Soap(parentId: string,): MatDialogRef<unknown,any> {
    return this.editForm_Soap(parentId, null);
  }

  //prescriptionss
  prescriptionssUrl():string {
    return "/prescriptionss";
  }
  prescriptionsUrl(id: string): string {
    return "/prescriptionss/" + id;
  }
  viewPrescription(id: string): void {
    this.router.navigateByUrl(this.prescriptionsUrl(id));
  }
  editPrescriptions(parent_id:string,id: string, mode: string = TCModalModes.EDIT, isSurgery: boolean = false,type = prescription_type.doctor_prescription): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PrescriptionsEditComponent, {
      data: {parentId: parent_id, childId: id, mode, isSurgery,type},
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  editBatchPrescriptions(prescriptions = [], mode: String= TCModalModes.CASE, type = prescription_type.pharmacist_prescription):MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PrescriptionsEditComponent, {
      data: {prescriptions, mode,isSurgery:false,type},
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addPrescriptions(parentId: string, isSurgery: boolean = false,type = 100): MatDialogRef<unknown,any> {
    return this.editPrescriptions(parentId,null, TCModalModes.NEW, isSurgery, type);
  }

  wizardPrescription(parentId: string): MatDialogRef<unknown, any> {
    return this.editPrescriptions(parentId, null, TCModalModes.WIZARD)
  }

  // surgery appointments
  editSurgery_Appointment(parentId: string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_Encounter_Surgery_AppointmentEditComponent, {
      data: new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }
  //form_observations
  editForm_Observation(parentId: string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_ObservationEditComponent, {
      data: new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSurgery_Appointment(parentId: string): MatDialogRef<unknown,any> {
    return this.editSurgery_Appointment(parentId, null);

  }
  addForm_Observation(parentId: string,): MatDialogRef<unknown,any> {
    return this.editForm_Observation(parentId, null);
  }

//medical_certificates
editMedical_Certificate(parentId:string, id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Medical_CertificateEditComponent, {
    data:  new TCParentChildIds(parentId, id),
    width: TCModalWidths.large
  });
  return dialogRef;
}

addMedical_Certificate(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editMedical_Certificate(parentId,null);
}
//encounter_imagess
editEncounter_Images(parentId:string, id: string, mode:string = TCModalModes.EDIT): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Encounter_ImagesEditComponent, {
      data:  new TCParentChildIds(parentId, id,{mode:mode}),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addEncounter_Images(parentId:string,patient_id:string, mode:string = TCModalModes.NEW): MatDialogRef<unknown,any> {
    return this.editEncounter_Images(parentId,patient_id,mode);
  }

//physical_examinations
editPhysical_Examination(parentId:string, id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Physical_ExaminationEditComponent, {
    data:  new TCParentChildIds(parentId, id),
    width: TCModalWidths.large
  });
  return dialogRef;
}


wizardPhysical_Examination(parentId:string, id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Physical_ExaminationEditComponent, {
    data:  new TCParentChildIds(parentId, id),
    width: TCModalWidths.large
  });
  return dialogRef;
}

addPhysical_Examination(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editPhysical_Examination(parentId,null);
}

// review_of_system
review_of_systemsUrl(): string {
  return "/review_of_systems";
}

 review_of_systemUrl(id: string): string {
  return "/review_of_systems/" + id;
}

viewReview_Of_Systems(): void {
  this.router.navigateByUrl(this. review_of_systemsUrl());
}

viewReview_Of_System(id: string): void {
  this.router.navigateByUrl(this. review_of_systemUrl(id));
}

editReview_Of_System(id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open( Review_Of_SystemEditComponent, {
    data: new TCIdMode(id, TCModalModes.EDIT),
    width: TCModalWidths.large
  });
  return dialogRef;
}

addReview_Of_System(id: string): MatDialogRef< unknown,any> {
  const dialogRef = this.dialog.open( Review_Of_SystemEditComponent, {
        data: new TCIdMode(id, TCModalModes.NEW),
        width: TCModalWidths.large
  });
  return dialogRef;
}




 rehabilitation_OrdersUrl(id: string): string {
  return "/rehabilitation_order/" + id;
}

//rehabilitation_orders
editRehabilitation_Order(id: string,parent_id:string=null): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Rehabilitation_OrderEditComponent, {
    data:  new TCParentChildIds(parent_id, id),
    width: TCModalWidths.medium
  });
  return dialogRef;
}

wizardRehabilitation_Order(id: string,parent_id:string=null): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Rehabilitation_OrderEditComponent, {
    data:  new TCParentChildIds(parent_id, id, {
      mode: TCModalModes.WIZARD
    }),
    width: TCModalWidths.medium
  });
  return dialogRef;
}

addRehabilitation_Order(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editRehabilitation_Order(null,parentId);
}

editAllergyForm(parentId:string, id: string, isEdit = true): MatDialogRef<unknown,any> {
  let params: TcDictionary<string> = new TcDictionary();

  params['mode'] = isEdit
    ? TCModalModes.EDIT :  TCModalModes.NEW;

  const dialogRef = this.dialog.open(AllergyFormEditComponent, {
    data:  new TCParentChildIds(parentId, id,params),
    width: TCModalWidths.medium
  });
  return dialogRef;
}
addAllergyForm(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editAllergyForm(parentId,null,false);
}


editOrderDialysis(parentId:string, id: string): MatDialogRef<OrderDialysisEditComponent> {
  let params: TcDictionary<string> = new TcDictionary();
  params["mode"] =  id == null
    ? TCModalModes.NEW
    : TCModalModes.EDIT;

  const dialogRef = this.dialog.open(OrderDialysisEditComponent, {
    data:  new TCParentChildIds(parentId, id,params),
    width: TCModalWidths.medium
  });
  return dialogRef;
}
addOrderDialysis(parentId:string, ): MatDialogRef<OrderDialysisEditComponent> {
  return this.editOrderDialysis(parentId,null);
}

editHemoPerformed(parentId:string, id: string): MatDialogRef<HemoPerformedEditComponent> {
  let params: TcDictionary<string> = new TcDictionary();
  params["mode"] =  id == null
    ? TCModalModes.NEW
    : TCModalModes.EDIT;

  const dialogRef = this.dialog.open(HemoPerformedEditComponent, {
    data:  new TCParentChildIds(parentId, id,params),
    width: TCModalWidths.medium
  });
  return dialogRef;
}
addHemoPerformed(parentId:string, ): MatDialogRef<HemoPerformedEditComponent> {
  return this.editHemoPerformed(parentId,null);
}

}


