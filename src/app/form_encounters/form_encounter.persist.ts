import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";
import { surgery_status, encounter_status, drug_form, drug_unit, sensetivity, temp_method, lab_urgency, BedAssignmentStatus, MeansOfArrival, Severity, PrescriptionStatus, ServiceType, DialysisType, DialysisSubTypes, TreatmentPerWeek, patient_procedure_status, Frequency, HowToUse, PhysiotherapyStatus, dosage_unit, category } from "../app.enums";
import { AppTranslation } from "../app.translation";
import { Review_Of_System_OptionsSummary } from "../review_of_system_optionss/review_of_system_options.model";
import { SurgeryType } from "../app.enums";
import { TcDictionary, TCEnum, TCEnumTranslation, TCId } from "../tc/models";
import { TCUtilsDate } from '../tc/utils-date';
import { TCDoParam, TCUrlParams, TCUtilsHttp } from "../tc/utils-http";
import { TCUtilsString } from '../tc/utils-string';
import {
Encounter_ImagesDetail,
Encounter_ImagesSummaryPartialList, Form_EncounterDashboard, Form_EncounterDetail, Form_EncounterSummaryPartialList,
PrescriptionsSummary,
Form_SoapDetail, Form_SoapSummaryPartialList, Form_VitalsDetail, Form_VitalsSummaryPartialList, Medical_CertificateDetail, Medical_CertificateSummaryPartialList, PrescriptionsDetail, PrescriptionsSummaryPartialList, Surgery_AppointmentDashboard, Surgery_AppointmentDetail, Surgery_AppointmentSummaryPartialList, Physical_ExaminationSummaryPartialList, Physical_ExaminationDetail, Review_Of_SystemSummaryPartialList, Review_Of_SystemDetail, Review_Of_SystemSummary, Follow_Up_NoteSummaryPartialList, Follow_Up_NoteDetail, Rehabilitation_OrderDetail, Rehabilitation_OrderSummaryPartialList, Patient_ProcedureSummaryPartialList, Patient_ProcedureDetail, ConsultingSummaryPartialList, ConsultingDashboard, ConsultingDetail, EncounterTabs, OrderDialysisSummaryPartialList, OrderDialysisDetail, HemoPerformedSummaryPartialList, HemoPerformedDetail,
VitalImport,

} from "./form_encounter.model";




import { Form_ObservationDetail, Form_ObservationSummaryPartialList, } from "./form_encounter.model";
import { PrescriptionsDetailForList } from "./prescriptions-edit/prescriptions-edit.component";
import { AllergyFormDetail, AllergyFormSummaryPartialList } from "../allergy/allergy.model";
import { FormControl } from "@angular/forms";
import { TCUtilsAngular } from "../tc/utils-angular";

export interface FormReview {
  id: string,
  value: string
}

@Injectable({
  providedIn: 'root'
})
export class Form_EncounterPersist {

  selectedEncounterTabs: EncounterTabs[] = [];
  selectedNurseRecordTabs: EncounterTabs[] = [];
  selectedTabIndex: number = 0;
  form_encounterId: string;
  surgery_date: number;
  // ward_id: string;

  encounter_is_active: boolean;
  encounter_active: boolean;

  // form_encounterSearchText: string = "";
  surgery_appointmentSearchText: string = "";
  review_of_systemSearchText: string = "";
  allergyFormSearchText: string = "";
  // show_only_mine:boolean = true;
  // isPreviosVisit: boolean = false;
  category: number = category.drug;
  selected_tab: string;
  patientName: string = "";
  // patientId:string = "";
  // date:number;
  // from_date: number;
  // to_date: number;
  // start_date: number;
  surgeryStatusFilter: string = "-1";
  // encounter_department: number;
  // parent_encounter_id: string;

  surgery_statuId: number;

  dispatch_status: number;

  serviceTypeDepartment: TCEnumTranslation[] = [
    new TCEnumTranslation(ServiceType.inpatient, 'InPatient'),
    new TCEnumTranslation(ServiceType.outpatient, 'OutPatient'),
    new TCEnumTranslation(ServiceType.emergency, 'Emergency'),
  ];

  serviceType: TCEnumTranslation[] = [
    ...this.serviceTypeDepartment,
    new TCEnumTranslation(ServiceType.referral, 'Referral'),
    new TCEnum(ServiceType.outsideOrder, 'Outside Order'),
    new TCEnum(ServiceType.inhouse_patient, 'Outside Order'),
  ];

  serviceTypeWithAll: TCEnumTranslation[] = [
    new TCEnum(ServiceType.all, 'All'),
    ...this.serviceType,
  ];

  serviceTypeForCredit: TCEnumTranslation[] = [
    new TCEnum(ServiceType.all, 'All'),
    ...this.serviceTypeDepartment,
  ];

  department: TCEnumTranslation[] = [
    new TCEnumTranslation(ServiceType.inpatient, 'InPatient'),
    new TCEnumTranslation(ServiceType.outpatient, 'OutPatient'),
    new TCEnumTranslation(ServiceType.emergency, 'Emergency'),
  ];

  PrescriptionStatuses: TCEnum[] = [
    new TCEnum(PrescriptionStatus.prescribed, "prescribed"),
    new TCEnum(PrescriptionStatus.dispatched, "dispatched"),
    new TCEnum(PrescriptionStatus.void, "void"),
  ];
  physiotherapyStatus: TCEnum[] = [
    new TCEnum(PhysiotherapyStatus.waiting, 'waiting'),
    new TCEnum(PhysiotherapyStatus.inprogress, 'inprogress'),
    new TCEnum(PhysiotherapyStatus.completed, 'completed'),

  ];
  patient_procedure_status: TCEnum[] = [
    new TCEnum(patient_procedure_status.ordered, "ordered"),
    new TCEnum(patient_procedure_status.completed, "completed"),
  ];

  treatmentPerWeek: TCEnum[] = [
    new TCEnum(TreatmentPerWeek.daily, 'daily'),
    new TCEnum(TreatmentPerWeek._6xWeekly, '6x Weekly'),
    new TCEnum(TreatmentPerWeek._5xWeekly, '5x Weekly'),
    new TCEnum(TreatmentPerWeek._4xWeekly, '4x Weekly'),
    new TCEnum(TreatmentPerWeek._3xWeekly, '3x Weekly'),
    new TCEnum(TreatmentPerWeek._2xWeekly, '2x Weekly'),
    new TCEnum(TreatmentPerWeek._1xWeekly, '1x Weekly'),

  ];


  dosageUnit: TCEnum[] = [
    new TCEnum(dosage_unit.mg, 'Mg'),
    new TCEnum(dosage_unit.gm, 'Gm'),
    new TCEnum(dosage_unit.ml, 'Ml'),
    new TCEnum(dosage_unit.l, 'L'),
    new TCEnum(dosage_unit.iu, 'IU'),
    new TCEnum(dosage_unit.kit, 'Kit'),
    new TCEnum(dosage_unit.pack, 'Pack'),
    new TCEnum(dosage_unit.sachet, 'Sachet'),
    new TCEnum(dosage_unit.roll, 'Roll'),
    new TCEnum(dosage_unit.piece, 'Piece'),
    new TCEnum(dosage_unit.bag, 'Bag'),
    new TCEnum(dosage_unit.percent, '%'),
    new TCEnum(dosage_unit.pair, 'Pair'),
    new TCEnum(dosage_unit.mg_ml, 'mg/ml'),
    new TCEnum(dosage_unit.mg_2ml, 'mg/2ml'),
    new TCEnum(dosage_unit.mg_3ml, 'mg/3ml'),
    new TCEnum(dosage_unit.mg_5ml, 'mg/4ml'),
    new TCEnum(dosage_unit.mg_10ml, 'mg/5ml'),

  ]

  frequencyFilter: number;

  frequency: TCEnum[] = [
    new TCEnum(Frequency.none, 'None'),
    new TCEnum(Frequency.immidiately, 'Stat'),
    new TCEnum(Frequency.prn, 'PRN'),
    // new TCEnum( Frequency.twice_a_day, 'Twice a day'),
    // new TCEnum( Frequency.thrice_a_day, 'Thrice a day'),
    // new TCEnum( Frequency.four_times_a_day, 'Four times a day'),
    new TCEnum(Frequency.every_hour, 'Every hour'),
    new TCEnum(Frequency.every_2_hours, 'Every 2 hours'),
    new TCEnum(Frequency.every_4_hours, 'Every 4 hours'),
    new TCEnum(Frequency.every_3_hours, 'Every 3 hours'),
    new TCEnum(Frequency.every_6_hours, 'Every 6 hours'),
    new TCEnum(Frequency.every_8_hours, 'Every 8 hours'),
    new TCEnum(Frequency.every_12_hours, 'Every 12 hours'),
    new TCEnum(Frequency.five_times_a_day, 'Five times a day'),
    new TCEnum(Frequency.once_a_day, 'Once a day'),
    new TCEnum(Frequency.on_alternate_days, 'On alternate days'),
    new TCEnum(Frequency.four_days_week, 'Four days week'),
    new TCEnum(Frequency.five_days_a_week, 'Five days a week'),
    new TCEnum(Frequency.six_days_a_week, 'Six days a week'),
    new TCEnum(Frequency.onece_a_week, 'Once a week'),
    new TCEnum(Frequency.twice_a_week, 'Twice a week'),
    new TCEnum(Frequency.thrice_a_week, 'Thrice a week'),
    new TCEnum(Frequency.every_2_weeks, 'Every 2 weeks'),
    new TCEnum(Frequency.every_3_weeks, 'Every 3 weeks'),

  ];

  HowToUse: TCEnum[] = [
    new TCEnum(HowToUse.before_meals, 'before_meals'),
    new TCEnum(HowToUse.empty_stomach, 'empty_stomach'),
    new TCEnum(HowToUse.after_meals, 'after_meals'),
    new TCEnum(HowToUse.in_the_morning, 'in_the_morning'),
    new TCEnum(HowToUse.in_the_evening, 'in_the_evening'),
    new TCEnum(HowToUse.at_bedtime, 'at_bedtime'),
    new TCEnum(HowToUse.immediately, 'immediately'),
    new TCEnum(HowToUse.as_directed, 'as_directed'),

  ];

  drug_form: TCEnum[] = [
    new TCEnum(drug_form.suspension, 'suspension'),
    new TCEnum(drug_form.tablet, 'tablet'),
    new TCEnum(drug_form.capsule, 'capsule'),
    new TCEnum(drug_form.solution, 'solution'),
    new TCEnum(drug_form.tsp, 'tsp'),
    new TCEnum(drug_form.ml, 'ml'),
    new TCEnum(drug_form.units, 'units'),
    new TCEnum(drug_form.inhalations, 'inhalations'),
    new TCEnum(drug_form.drops, 'gtts(drops)'),
    new TCEnum(drug_form.cream, 'cream'),
    new TCEnum(drug_form.ointment, 'ointment'),
    new TCEnum(drug_form.puff, 'puff'),
    new TCEnum(drug_form.kit, 'kit'),
    new TCEnum(drug_form.piece, 'piece'),
    new TCEnum(drug_form.sachet, 'sachet'),
    new TCEnum(drug_form.jar, 'jar'),
    new TCEnum(drug_form.box, 'box'),
    new TCEnum(drug_form.tin, 'tin'),
    new TCEnum(drug_form.cannister, 'cannister'),
    new TCEnum(drug_form.vial, 'vial'),
    new TCEnum(drug_form.injection, 'injection'),
    new TCEnum(drug_form.syrup, 'syrup'),
    new TCEnum(drug_form.suppository, 'suppository'),
    new TCEnum(drug_form.ampule, 'ampule'),
  ];
  frequencyList = [
    { id: 1, name: "day(s)" },
    { id: 7, name: "week(s)" },
    { id: 30, name: "month(s)" },
    { id: 120, name: "quarterly(s)" },
  ];

  patientTypeFilter: number;

  // patientType: TCEnum[] = [
  //   new TCEnum(PatientType.outPatient, 'outpatient'),
  //   new TCEnum(PatientType.inPatient, 'inpatient'),
  //   new TCEnum(PatientType.referral, 'referral'),
  //   new TCEnum(PatientType.outsideOrder, 'outsideOrder'),
  //
  // ];

  bedAssignmentStatuses: TCEnum[] = [
    new TCEnum(BedAssignmentStatus.urgent, this.appTranslation.getKey("patient", "urgent")),
    new TCEnum(BedAssignmentStatus.normal, this.appTranslation.getKey("patient", "normal")),
  ];
  drug_unit: TCEnum[] = [
    new TCEnum(drug_unit.mg, 'mg'),
    new TCEnum(drug_unit.mg1cc, 'mg/1cc'),
    new TCEnum(drug_unit.mg2cc, 'mg/2cc'),
    new TCEnum(drug_unit.mg3cc, 'mg/3cc'),
    new TCEnum(drug_unit.mg4cc, 'mg/4cc'),
    new TCEnum(drug_unit.mg5cc, 'mg/5cc'),
    new TCEnum(drug_unit.mcg, 'mcg'),
    new TCEnum(drug_unit.grams, 'grams'),
    new TCEnum(drug_unit.mL, 'mL'),
  ];

  surgery_status: TCEnum[] = [
    new TCEnum(surgery_status.sent, 'Waiting'),
    new TCEnum(surgery_status.on_surgery, 'Ongoing'),
    new TCEnum(surgery_status.cancelled, 'Declined'),
    new TCEnum(surgery_status.completed, 'Done'),
  ];

  // encounterStatusFilter: number = 100;
  // encounter_statusId: number = 100;
  encounter_status: TCEnum[] = [
    new TCEnum(encounter_status.opened, 'opened'),
    new TCEnum(encounter_status.closed, 'closed'),
  ];

  temp_methodId: number;

  temp_method: TCEnum[] = [
    new TCEnum(temp_method.oral, 'oral'),
    new TCEnum(temp_method.tympanic, 'tympanic_membrane'),
    new TCEnum(temp_method.axillary, 'axillary'),
    new TCEnum(temp_method.rectal, 'rectal'),
  ];

  surgeryTypeFilter: number;

  SurgeryType: TCEnum[] = [
    new TCEnum(SurgeryType.Elective, 'Elective'),
    new TCEnum(SurgeryType.Emergency, 'Emergency'),
  ];
  self: boolean = true;
  encounter_id: string;
  // encounterSearchColumn: string = "";
  encounterSearchHistory = {
    "show_only_mine": true,
    "is_previous_visit": false,
    "service_type": undefined,
    "patientId": "",
    "encounter_id": "",
    "ward_id": "",
    "bedRoomName": "",
    "encounter_status": encounter_status.opened,
    "parent_encounter_id": "",
    "date": this.tcUtilsDate.toTimeStamp(new Date()),
    "search_text": "",
    "search_column": "",
    "from_date": undefined,
    "to_date": undefined,
    "is_consult":undefined,
  }

  defaultEncounterSearchHistory = {
    "patientId": "",
    "parent_encounter_id": "",
    "encounter_id": "",
    "search_text": "",
    "search_column": ""
  }

  resetEncounterSearchHistory(encounter_status) {
    this.encounter_is_active = encounter_status;
    this.defaultEncounterSearchHistory = { ...this.defaultEncounterSearchHistory }
  }

  otherEncounterSearchHistory = { ...this.encounterSearchHistory }
  defaultOtherEncounterSearchHistory = { ...this.otherEncounterSearchHistory }

  resetOtherEncounterSearchHistory() {
    this.otherEncounterSearchHistory = { ...this.defaultOtherEncounterSearchHistory }
  }

  prescriptionDispenseSearchHistory = {
    "from_date": undefined,
    "to_date": undefined
  }

  defaultPrescriptionDispenseSearchHistory = { ...this.prescriptionDispenseSearchHistory }

  resetPrescriptionDispenseSearchHistory() {
    this.prescriptionDispenseSearchHistory = { ...this.defaultPrescriptionDispenseSearchHistory }
  }

  constructor(private http: HttpClient, private tcUtilsDate: TCUtilsDate, private appTranslation: AppTranslation) {
    // this.date.setHours(0,0,0,0);
  }

  searchForm_Encounter(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory: any = this.encounterSearchHistory): Observable<Form_EncounterSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchWithColumnUrl("form_encounters", searchHistory.search_text, pageSize, pageIndex, sort, order, searchHistory.search_column);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // url = TCUtilsString.appendUrlParameter(url, "show_only_mine", this.show_only_mine?'1':'0');
    // url = TCUtilsString.appendUrlParameter(url, "is_previous_visit", this.isPreviosVisit?'1':'0');
    // if (this.encounter_department){
    //   url = TCUtilsString.appendUrlParameter(url, "service_type", this.encounter_department ? this.encounter_department.toString():'');
    // }
    // if (this.patientId){
    //   url = TCUtilsString.appendUrlParameter(url, "patientId", this.patientId.toString());
    // }
    // if (this.encounter_id){
    //   url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounter_id);
    // }
    // if (this.ward_id){
    //   url = TCUtilsString.appendUrlParameter(url, "ward_id", this.ward_id);
    // }
    // if(this.encounterStatusFilter)
    // {url =  TCUtilsString.appendUrlParameter(url, "encounter_status", this.encounterStatusFilter.toString());}
    // if(this.parent_encounter_id)
    // {url =  TCUtilsString.appendUrlParameter(url, "parent_encounter_id", this.parent_encounter_id);}
    // if (this.date){
    //   url = TCUtilsString.appendUrlParameter(url, "date", this.date.toString())
    // }

    return this.http.get<Form_EncounterSummaryPartialList>(url);

  }


  searchForm_EncounterByPat(pageSize: number, pageIndex: number, sort: string, order: string, patId: string, searchHistory = this.encounterSearchHistory): Observable<Form_EncounterSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/do", searchHistory.search_text, pageSize, pageIndex, sort, order);

    url = TCUtilsString.appendUrlParameter(url, "patientId", patId);

    return this.http.post<Form_EncounterSummaryPartialList>(url, new TCDoParam("get_encounter_by_patient", {}));

  }

  documentUploadUri(memberId: string) {
    return environment.tcApiBaseUri + 'encounter/' + memberId + "/encounter-upload/";
  }


  filters(searchHistory = this.encounterSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  reportSmmary(id: string, method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/" + id + "/do", new TCDoParam(method, payload));
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/do", new TCDoParam("download_all", this.filters()));
  }

  form_encounterDashboard(): Observable<Form_EncounterDashboard> {
    return this.http.get<Form_EncounterDashboard>(environment.tcApiBaseUri + "form_encounters/dashboard");
  }

  getForm_Encounter(id: string): Observable<Form_EncounterDetail> {
    return this.http.get<Form_EncounterDetail>(environment.tcApiBaseUri + "form_encounters/" + id);
  }

  addForm_Encounter(item: Form_EncounterDetail): Observable<Form_EncounterDetail> {
    return this.http.post<Form_EncounterDetail>(environment.tcApiBaseUri + "form_encounters/", item);
  }

  updateForm_Encounter(item: Form_EncounterDetail): Observable<Form_EncounterDetail> {
    return this.http.patch<Form_EncounterDetail>(environment.tcApiBaseUri + "form_encounters/" + item.id, item);
  }

  deleteForm_Encounter(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + id);
  }

  form_encountersDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.encounterSearchHistory): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("form_encounters/do", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  form_encounterDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + id + "/do", new TCDoParam("print", {}));
  }


  temp_methon = ["", "Oral", "Rectal", "Axillary", "Tympanic Membrane"]

  //form_vitalss
  form_vitalsSearchText: string = "";
  searchForm_Vitals(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Form_VitalsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters" + "/form_vitalss", this.form_vitalsSearchText, pageSize, pageIndex, sort, order);

    if (this.self != null) {
      url = TCUtilsString.appendUrlParameter(url, "self", this.self ? "1" : "0");
    }

    url = TCUtilsString.appendUrlParameter(url, 'parentId', parentId.toString())


    return this.http.get<Form_VitalsSummaryPartialList>(url);

  }


  async importLatestVitals<T>(patient_id: string, vital_import: VitalImport<T>, detail: T) {
    let url = TCUtilsHttp.buildSearchUrl("form_encounters" + "/form_vitalss", this.form_vitalsSearchText, 1, 0, 'creation_time', 'desc');
    url = TCUtilsString.appendUrlParameter(url, 'pid', patient_id)
    const value = await this.http.get<Form_VitalsSummaryPartialList>(url).toPromise()
    if (value.total) {
      const data = value.data[0];
      for (let v in vital_import) {
        detail[vital_import[v]] = data[v]
      }
    }
  }


  searchForm_Vitals_Blood_Exchange(id: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Form_VitalsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters" + "/form_vitalss", this.form_vitalsSearchText, pageSize, pageIndex, sort, order);

    if (this.self != null) {
      url = TCUtilsString.appendUrlParameter(url, "self", this.self ? "1" : "0");

    }

    url = TCUtilsString.appendUrlParameter(url, 'blood_exchange_id', id.toString())


    return this.http.get<Form_VitalsSummaryPartialList>(url);

  }

  getForm_Vitals(id: string): Observable<Form_VitalsDetail> {
    return this.http.get<Form_VitalsDetail>(environment.tcApiBaseUri + "form_vitalss/" + id);
  }


  addForm_Vitals(parentId: string, item: Form_VitalsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_vitalss", item);
  }

  updateForm_Vitals(item: Form_VitalsDetail): Observable<Form_VitalsDetail> {
    return this.http.patch<Form_VitalsDetail>(environment.tcApiBaseUri + "/form_vitalss/" + item.id, item);
  }

  deleteForm_Vitals(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_vitalss/" + id);
  }

  // form_vitalssDo(parentId: string, method: string, payload: any): Observable<{}> {
  //   return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_vitalss/do", new TCDoParam(method, payload));
  // }
  form_vitalssDo(parentId: string, method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): any{
    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/form_vitalss/do", this.form_vitalsSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  form_vitalsDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_vitalss/" + id + "/do", new TCDoParam(method, payload));
  }

  //form_soaps
  form_soapSearchText: string = "";
  form_soapEncounterId: string;
  form_soapPatientId: string;
  searchForm_Soap(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Form_SoapSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_soaps/", this.form_soapSearchText, pageSize, pageIndex, sort, order);
    if (this.form_soapEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.form_soapEncounterId)
    }
    if (this.form_soapPatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.form_soapPatientId)
    }
    return this.http.get<Form_SoapSummaryPartialList>(url);

  }

  getForm_Soap(id: string): Observable<Form_SoapDetail> {
    return this.http.get<Form_SoapDetail>(environment.tcApiBaseUri + "form_soaps/" + id);
  }


  addForm_Soap(parentId: string, item: Form_SoapDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_soaps", item);
  }

  updateForm_Soap(item: Form_SoapDetail): Observable<Form_SoapDetail> {
    return this.http.patch<Form_SoapDetail>(environment.tcApiBaseUri + "form_soaps/" + item.id, item);
  }

  deleteForm_Soap(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_soaps/" + id);
  }

  form_soapsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_soaps/do", new TCDoParam(method, payload));
  }

  form_soapDo(parentId: string, id: string, method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri +"form_soaps/" + id + "/do", new TCDoParam(method, payload));
  }


    //prescriptionss
    prescriptionsSearchText: string = "";
    patient_id: string;
    prescriptionEncounterId: string;
    add_date : number;
    searchPrescriptions(pageSize: number, pageIndex: number, sort: string, order: string): Observable<PrescriptionsSummaryPartialList> {
      let url = TCUtilsHttp.buildSearchUrl( "prescriptionss/", this.prescriptionsSearchText, pageSize, pageIndex, sort, order);
      if(this.prescriptionEncounterId){
        url = TCUtilsString.appendUrlParameter(url,'encounter_id'
        , this.prescriptionEncounterId);
      }
      if(this.patient_id){
        url = TCUtilsString.appendUrlParameter(url,'patient_id', this.patient_id);
      }
      if(this.category){
        url = TCUtilsString.appendUrlParameter(url,'category', this.category.toString());
      }
      return this.http.get<PrescriptionsSummaryPartialList>(url);


  }

  getPrescriptions(id: string): Observable<PrescriptionsDetailForList> {
    return this.http.get<PrescriptionsDetailForList>(environment.tcApiBaseUri + "prescriptionss/" + id);
  }
  // getPrescription(id: string): Observable<PrescriptionsSummaryPartialList> {
  //   return this.http.get<PrescriptionsSummaryPartialList>(environment.tcApiBaseUri + "prescriptionss/" + id);
  // }

  addPrescriptions(parentId: string, item: {}): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/prescriptionss/", item);
  }

  updatePrescriptions(item: PrescriptionsDetail): Observable<PrescriptionsDetail> {
    return this.http.patch<PrescriptionsDetail>(environment.tcApiBaseUri + "prescriptionss/" + item.id, item);
  }

  updatePrescriptions2(item: any): Observable<any> {
    console.log('updatePrescriptions',item,item.data[0].id)
    return this.http.patch<any>(environment.tcApiBaseUri + "updatePrescriptionss/" + item.data[0].id, item);
  }
  deletePrescriptions(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/prescriptionss/" + id);
  }


  prescriptionssDo(parentId: string, method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): any{
    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/prescriptionss/do", this.prescriptionsSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  prescriptionsDo(parentId: string, id: string, method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/prescriptionss/" + id + "/do", new TCDoParam(method, payload));
  }
  prescriptionsStatusDo(id: string, method: string, payload: any): any {
    return this.http.post<any>(environment.tcApiBaseUri + "prescriptionss/" + id + "/do", new TCDoParam(method, payload));
  }
  prescriptionsDashboard(): Observable<any> {
    return this.http.get<any>(environment.tcApiBaseUri + "prescriptionss/dashboard");
  }
  // surgery appointments
  surgeryAppointmentPatientId: string;
  surgeryAppointmentEncounterId: string;
  searchSurgery_Appointment(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Surgery_AppointmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("surgery_appointments", this.surgery_appointmentSearchText, pageSize, pageIndex, sort, order);
    if (this.surgeryAppointmentEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.surgeryAppointmentEncounterId)
    }
    if (this.surgeryAppointmentPatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.surgeryAppointmentPatientId)
    }
    url = TCUtilsString.appendUrlParameter(url, "surgery_status", this.surgeryStatusFilter.toString());
    url = TCUtilsString.appendUrlParameter(url, "surgery_date", this.surgery_date ? this.surgery_date.toString() : '')
    return this.http.get<Surgery_AppointmentSummaryPartialList>(url);

  }

  surgery_appointmentDashboard(): Observable<Surgery_AppointmentDashboard> {
    return this.http.get<Surgery_AppointmentDashboard>(environment.tcApiBaseUri + "surgery_appointments/dashboard");
  }

  getSurgery_Appointment(id: string): Observable<Surgery_AppointmentDetail> {
    return this.http.get<Surgery_AppointmentDetail>(environment.tcApiBaseUri + "surgery_appointments/" + id);
  }

  addSurgery_Appointment(parentId: string, item: Surgery_AppointmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/surgery_appointments/", item);
  }

  updateSurgery_Appointment(item: Surgery_AppointmentDetail): Observable<Surgery_AppointmentDetail> {
    return this.http.patch<Surgery_AppointmentDetail>(environment.tcApiBaseUri + "/surgery_appointments/" + item.id, item);
  }

  deleteSurgery_Appointment(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/surgery_appointments/" + id);
  }

  surgery_appointmentsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/surgery_appointments/do", new TCDoParam(method, payload));
  }

  surgery_appointmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_appointments/" + id + "/do", new TCDoParam(method, payload));
  }
  //form_observations
  form_observationSearchText: string = "";
  searchForm_Observation(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Form_ObservationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/form_observations/", this.form_observationSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Form_ObservationSummaryPartialList>(url);

  }

  getForm_Observation(parentId: string, id: string): Observable<Form_ObservationDetail> {
    return this.http.get<Form_ObservationDetail>(environment.tcApiBaseUri + "form_observations/" + id);
  }


  addForm_Observation(parentId: string, item: Form_ObservationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_observations/", item);
  }

  updateForm_Observation(parentId: string, item: Form_ObservationDetail): Observable<Form_ObservationDetail> {
    return this.http.patch<Form_ObservationDetail>(environment.tcApiBaseUri + "form_observations/" + item.id, item);
  }

  deleteForm_Observation(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_observations/" + id);
  }

  form_observationsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_observations/do", new TCDoParam(method, payload));
  }

  form_observationDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/form_observations/" + id + "/do", new TCDoParam(method, payload));
  }

  //consulting

  consultingSearchText: string = "";
  consultingPatientId: string;
  consultigEncounterId: string;
  searchConsulting(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ConsultingSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + TCUtilsString.getInvalidId() + "/consulting", this.consultingSearchText, pageSize, pageIndex, sort, order);
    if (this.consultigEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.consultigEncounterId)
    }
    if (this.consultingPatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.consultingPatientId)
    }
    return this.http.get<ConsultingSummaryPartialList>(url);
  }

  consultingDashboard(): Observable<ConsultingDashboard> {
    return this.http.get<ConsultingDashboard>(environment.tcApiBaseUri + "consulting/dashboard");
  }

  getConsulting(id: string): Observable<ConsultingDetail> {
    return this.http.get<ConsultingDetail>(environment.tcApiBaseUri + "consulting/" + id);
  } addConsulting(item: ConsultingDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "consulting/", item);
  }

  updateConsulting(item: ConsultingDetail): Observable<ConsultingDetail> {
    return this.http.patch<ConsultingDetail>(environment.tcApiBaseUri + "consulting/" + item.id, item);
  }

  deleteConsulting(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "consulting/" + id);
  }
  consultingsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "consulting/do", new TCDoParam(method, payload));
  }

  consultingDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "consulting/" + id + "/do", new TCDoParam(method, payload));
  }

  //patient_procedures
  // patient_procedureSearchText: string = "";
  // patient_procedureEncounterId: string;
  // patient_procedurePatientId: string;
  // patient_procedureProcedureFor: number;
  patientprocedureSearchHistory = {
    "start_date": this.tcUtilsDate.toTimeStamp(new Date()),
    "encounter_id": "",
    "patient_id": "",
    "procedure_for": undefined,
    "search_text": "",
    "search_column": "",
  }


  defaultPatientProcedureSearchHistory = { ...this.patientprocedureSearchHistory }

  resetPatientProcedureSearchHistory() {
    this.patientprocedureSearchHistory = { ...this.defaultPatientProcedureSearchHistory }
  }

  patientProcedureEncounterSearchHistory = {...this.defaultPatientProcedureSearchHistory, start_date: undefined }
  defaultPatientProcedureEncounterSearchHistory = { ...this.patientProcedureEncounterSearchHistory}

  resetPatientProcedureEncounterSearchHistory() {
    this.patientProcedureEncounterSearchHistory = { ...this.defaultPatientProcedureEncounterSearchHistory }
  }
  searchPatient_Procedure(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.patientprocedureSearchHistory): Observable<Patient_ProcedureSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patient_procedures/", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.start_date){
    //   url = TCUtilsString.appendUrlParameter(url, "start_date", this.start_date.toString())
    // }
    // if (this.patient_procedureEncounterId){
    //   url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.patient_procedureEncounterId)
    // }
    // if (this.patient_procedurePatientId){
    //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patient_procedurePatientId)
    // }
    // if (this.patient_procedureProcedureFor){
    //   url = TCUtilsString.appendUrlParameter(url, "procedure_for", this.patient_procedureProcedureFor.toString())
    // }
    return this.http.get<Patient_ProcedureSummaryPartialList>(url);

  }

  getPatient_Procedure(id: string): Observable<Patient_ProcedureDetail> {
    return this.http.get<Patient_ProcedureDetail>(environment.tcApiBaseUri + "/patient_procedures/" + id);
  }

  addPatient_Procedure(parentId: string, item: Patient_ProcedureDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/" + parentId + "/patient_procedures/", item);
  }

  updatePatient_Procedure(parentId: string, item: Patient_ProcedureDetail): Observable<Patient_ProcedureDetail> {
    return this.http.patch<Patient_ProcedureDetail>(environment.tcApiBaseUri + "patients/" + parentId + "/patient_procedures/" + item.id, item);
  }

  deletePatient_Procedure(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "patient_procedures/" + id);
  }

  patient_proceduresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_procedures/do", new TCDoParam(method, payload));
  }

  patient_procedureDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_procedures/" + id + "/do", new TCDoParam(method, payload));
  }


  //get Drugs
  // drugsSearchText: string = '';
  drugSearchHistory = {
    "search_text": "",
  }

  defaultDrugSearchHistory = { ...this.drugSearchHistory }

  resetdrugSearchHistory() {
    this.prescriptionDispenseSearchHistory = { ...this.defaultPrescriptionDispenseSearchHistory }
  }
  searchDrugs(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.drugSearchHistory): Observable<PrescriptionsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("drugs/", searchHistory.search_text, pageSize, pageIndex, sort, order);
    return this.http.get<PrescriptionsSummaryPartialList>(url);
  }

  //medical_certificates
  medical_certificateSearchText: string = "";
  medical_certificateEncounterId: string;
  medical_certificatePatientId: string;
  searchMedical_Certificate(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Medical_CertificateSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("medical_certificates/", this.medical_certificateSearchText, pageSize, pageIndex, sort, order);
    if (this.medical_certificateEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.medical_certificateEncounterId)
    }
    if (this.medical_certificatePatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.medical_certificatePatientId)
    }
    return this.http.get<Medical_CertificateSummaryPartialList>(url);

  }

  getMedical_Certificate(parentId: string, id: string): Observable<Medical_CertificateDetail> {
    return this.http.get<Medical_CertificateDetail>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/medical_certificates/" + id);
  }


  addMedical_Certificate(parentId: string, item: Medical_CertificateDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/medical_certificates/", item);
  }

  updateMedical_Certificate(item: Medical_CertificateDetail): Observable<Medical_CertificateDetail> {
    return this.http.patch<Medical_CertificateDetail>(environment.tcApiBaseUri + "medical_certificates/" + item.id, item);
  }

  deleteMedical_Certificate(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "medical_certificates/" + id);
  }

  medical_certificatesDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/medical_certificates/do", new TCDoParam(method, payload));
  }

  medical_certificateDo(parentId: string, id: string, method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/medical_certificates/" + id + "/do", new TCDoParam(method, payload));
  }

  //encounter_imagess
  encounter_imagesSearchText: string = "";
  encounterImagePatientId: string;
  encounterImageEncounterId: string;
  searchEncounter_Images(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Encounter_ImagesSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("encounter_imagess/", this.encounter_imagesSearchText, pageSize, pageIndex, sort, order);
    if (this.encounterImagePatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.encounterImagePatientId)
    }
    if (this.encounterImageEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter", this.encounterImageEncounterId)
    }
    return this.http.get<Encounter_ImagesSummaryPartialList>(url);

  }

  getEncounter_Images(parentId: string, id: string): Observable<Encounter_ImagesDetail> {
    return this.http.get<Encounter_ImagesDetail>(environment.tcApiBaseUri + "encounter_imagess/" + id);
  }


  addEncounter_Images(parentId: string, item: Encounter_ImagesDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/encounter_imagess/", item);
  }

  updateEncounter_Images(parentId: string, item: Encounter_ImagesDetail): Observable<Encounter_ImagesDetail> {
    return this.http.patch<Encounter_ImagesDetail>(environment.tcApiBaseUri + "encounter_imagess/" + item.id, item);
  }

  deleteEncounter_Images(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "encounter_imagess/" + id);
  }

  encounter_imagessDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/encounter_imagess/do", new TCDoParam(method, payload));
  }

  encounter_imagesDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/encounter_imagess/" + id + "/do", new TCDoParam(method, payload));
  }

  //physical_examinations
  physical_examinationSearchText: string = "";
  date_of_examination: number;
  nurse_id: string;
  searchPhysical_Examination(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Physical_ExaminationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/physical_examinations/", this.physical_examinationSearchText, pageSize, pageIndex, sort, order);
    if (this.date_of_examination) {
      url = TCUtilsString.appendUrlParameter(url, "date_of_examination", this.date_of_examination.toString());
    }
    if (this.nurse_id) {
      url = TCUtilsString.appendUrlParameter(url, "nurse_id", this.nurse_id);
    }
    return this.http.get<Physical_ExaminationSummaryPartialList>(url);

  }

  getPhysical_Examination(id: string): Observable<Physical_ExaminationDetail> {
    return this.http.get<Physical_ExaminationDetail>(environment.tcApiBaseUri + "physical_examinations/" + id);
  }


  addPhysical_Examination(parentId: string, item: Physical_ExaminationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/physical_examinations/", item);
  }

  updatePhysical_Examination(parentId: string, item: Physical_ExaminationDetail): Observable<Physical_ExaminationDetail> {
    return this.http.patch<Physical_ExaminationDetail>(environment.tcApiBaseUri + "physical_examinations/" + item.id, item);
  }

  deletePhysical_Examination(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "physical_examinations/" + id);
  }

  physical_examinationsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/physical_examinations/do", new TCDoParam(method, payload));
  }

  physical_examinationDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/physical_examinations/" + id + "/do", new TCDoParam(method, payload));
  }
  sensetivityId: number;

  sensetivity: TCEnum[] = [
    new TCEnum(sensetivity.urgent, 'urgent'),
    new TCEnum(sensetivity.non_urgent, 'non_urgent'),
  ];

  // review_of_system
  searchReview_Of_System(parent_id: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Review_Of_SystemSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parent_id + "/review_of_systems", this.review_of_systemSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Review_Of_SystemSummaryPartialList>(url);

  }

  getReview_Of_System(id: string): Observable<Review_Of_SystemDetail> {
    return this.http.get<Review_Of_SystemDetail>(environment.tcApiBaseUri + "review_of_systems/" + id);
  }

  addReview_Of_System(parent_id: string, item: Review_Of_SystemDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parent_id + "/review_of_systems", item);
  }

  updateReview_Of_System(encounterId: string, item: Review_Of_SystemDetail): Observable<Review_Of_SystemDetail> {
    return this.http.patch<Review_Of_SystemDetail>(environment.tcApiBaseUri + "review_of_systems/" + encounterId, item);
  }

  deleteReview_Of_System(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "review_of_systems/" + id);
  }

  review_of_systemsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "review_of_systems/do", new TCDoParam(method, payload));
  }

  review_of_systemDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "review_of_systems/" + id + "/do", new TCDoParam(method, payload));
  }


  lab_urgencyId: number;

  lab_urgency: TCEnumTranslation[] = [
    new TCEnumTranslation(lab_urgency.normal, this.appTranslation.getKey('patient', 'non_urgent')),
    new TCEnumTranslation(lab_urgency.urgent, this.appTranslation.getKey('patient', 'urgent')),
  ];

  //follow_up_notes
  follow_up_noteSearchText: string = "";
  followUpNoteEncounterId: string;
  followUpNotePatientId: string
  searchFollow_Up_Note(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Follow_Up_NoteSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("follow_up_notes/", this.follow_up_noteSearchText, pageSize, pageIndex, sort, order);
    if (this.followUpNotePatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.followUpNotePatientId)
    }
    if (this.followUpNoteEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.followUpNoteEncounterId)
    }
    return this.http.get<Follow_Up_NoteSummaryPartialList>(url);

  }

  getFollow_Up_Note(parentId: string, id: string): Observable<Follow_Up_NoteDetail> {
    return this.http.get<Follow_Up_NoteDetail>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/" + id);
  }


  addFollow_Up_Note(parentId: string, item: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/", item);
  }

  updateFollow_Up_Note(parentId: string, item: any, id: string): Observable<Follow_Up_NoteDetail> {
    return this.http.patch<Follow_Up_NoteDetail>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/" + id, item);
  }

  deleteFollow_Up_Note(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/" + id);
  }

  follow_up_notesDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/do", new TCDoParam(method, payload));
  }

  follow_up_noteDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/follow_up_notes/" + id + "/do", new TCDoParam(method, payload));
  }


  //rehabilitation_orders
  // rehabilitation_orderSearchText: string = "";
  // rehabilitation_status: number;
  // rehabilatation_encounterId: string;
  // rehabilatation_patientId: string;

  rehabilationSearchHistory = {
    "status": undefined,
    "encounter_id": "",
    "patient_id": "",
    "search_text": "",
    "search_column": "",
  }


  defaultRehabilationSearchHistory = { ...this.rehabilationSearchHistory }

  resetRehabilationSearchHistory() {
    this.rehabilationSearchHistory = { ...this.defaultRehabilationSearchHistory }
  }

  searchRehabilitation_Order(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.rehabilationSearchHistory): Observable<Rehabilitation_OrderSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("rehabilitation_orders/", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.rehabilitation_status){
    //   url = TCUtilsString.appendUrlParameter(url, "status", this.rehabilitation_status.toString())
    // }
    // if(this.rehabilatation_patientId){
    //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.rehabilatation_patientId)
    //   }
    //   if(this.rehabilatation_encounterId){
    //   url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.rehabilatation_encounterId)
    //   }
    return this.http.get<Rehabilitation_OrderSummaryPartialList>(url);

  }

  getRehabilitation_Order(id: string): Observable<Rehabilitation_OrderDetail> {
    return this.http.get<Rehabilitation_OrderDetail>(environment.tcApiBaseUri + "rehabilitation_orders/" + id);
  }


  addRehabilitation_Order(parentId: string, item: Rehabilitation_OrderDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/rehabilitation_orders/", item);
  }

  updateRehabilitation_Order(parentId: string, item: Rehabilitation_OrderDetail): Observable<Rehabilitation_OrderDetail> {
    return this.http.patch<Rehabilitation_OrderDetail>(environment.tcApiBaseUri + "rehabilitation_orders/" + item.id, item);
  }

  deleteRehabilitation_Order(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounters/" + parentId + "/rehabilitation_orders/" + id);
  }

  rehabilitation_ordersDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/rehabilitation_orders/do", new TCDoParam(method, payload));
  }

  rehabilitation_orderDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "rehabilitation_orders/" + id + "/do", new TCDoParam(method, payload));
  }

  allergyFormEncounterId: string;
  allergyFormPatientId: string;
  searchAllergyForm(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<AllergyFormSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("allergy_form/", this.allergyFormSearchText, pageSize, pageIndex, sort, order);
    if (this.allergyFormEncounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.allergyFormEncounterId)
    }
    if (this.allergyFormPatientId) {
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.allergyFormPatientId)
    }
    return this.http.get<AllergyFormSummaryPartialList>(url);

  }
  getAllergyForm(parentId: string, id: string): Observable<AllergyFormDetail> {
    return this.http.get<AllergyFormDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/" + id);
  }
  addAllergyForm(parentId: string, item: AllergyFormDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/", item);
  }

  updateAllergyForm(parentId: string, item: AllergyFormDetail): Observable<AllergyFormDetail> {
    return this.http.patch<AllergyFormDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/" + item.id, item);
  }

  deleteAllergyForm(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/" + id);
  }
  allergyFormsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/", new TCDoParam(method, payload));
  }

  allergyFormDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/allergy_form/" + id + "/do", new TCDoParam(method, payload));
  }



  meansOfArrival: TCEnumTranslation[] = [
    new TCEnumTranslation(MeansOfArrival.ambulance, "Ambulance"),
    new TCEnumTranslation(MeansOfArrival.walk_in, "Walk In"),
    new TCEnumTranslation(MeansOfArrival.public_service, "Public Service"),
  ];


  severity: TCEnumTranslation[] = [
    new TCEnumTranslation(Severity.rescuitation, "Rescuitation"),
    new TCEnumTranslation(Severity.emergent, "Emergent"),
    new TCEnumTranslation(Severity.urgent, "Urgent"),
    new TCEnumTranslation(Severity.less_urgent, "Less Urgent"),
    new TCEnumTranslation(Severity.non_urgent, "Non Urgent"),
  ];


  orderDialysisSearchText: string = "";


  searchOrderDialysis(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OrderDialysisSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + parentId + "/order_dialysis/", this.orderDialysisSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<OrderDialysisSummaryPartialList>(url);

  }

  searchOrderDialysisWithoutId(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OrderDialysisSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("order_dialysis/", this.orderDialysisSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<OrderDialysisSummaryPartialList>(url);

  }
  getOrderDialysis(parentId: string, id: string): Observable<OrderDialysisDetail> {
    return this.http.get<OrderDialysisDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/order_dialysis/" + id);
  }
  addOrderDialysis(parentId: string, item: OrderDialysisDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/order_dialysis/", item);
  }

  updateOrderDialysis(parentId: string, item: OrderDialysisDetail): Observable<OrderDialysisDetail> {
    return this.http.patch<OrderDialysisDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/order_dialysis/" + item.id, item);
  }

  deleteOrderDialysis(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounter/" + parentId + "/order_dialysis/" + id);
  }
  orderDialysissDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/order_dialysis/", new TCDoParam(method, payload));
  }

  orderDialysisDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "order_dialysis/" + id + "/do", new TCDoParam(method, payload));
  }




  dialysisType: TCEnum[] = [
    new TCEnum(DialysisType.hemodialysis, 'Hemodialysis'),
    new TCEnum(DialysisType.peritoneal, 'Peritoneal'),
  ];


  dialysisSubType: TCEnum[] = [
    new TCEnum(DialysisSubTypes.APD, 'APD'),
    new TCEnum(DialysisSubTypes.CAPD, 'CAPD'),
    new TCEnum(DialysisSubTypes.fistula, 'Fistula'),
    new TCEnum(DialysisSubTypes.intravenous, 'Intravenous'),
  ];


  hemoPerformedSearchText: string = "";



  searchHemoPerformed(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<HemoPerformedSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + parentId + "/hemo_performed/", this.hemoPerformedSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<HemoPerformedSummaryPartialList>(url);

  }
  getHemoPerformed(parentId: string, id: string): Observable<HemoPerformedDetail> {
    return this.http.get<HemoPerformedDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/" + id);
  }
  addHemoPerformed(parentId: string, item: HemoPerformedDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/", item);
  }

  updateHemoPerformed(parentId: string, item: HemoPerformedDetail): Observable<HemoPerformedDetail> {
    return this.http.patch<HemoPerformedDetail>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/" + item.id, item);
  }

  deleteHemoPerformed(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/" + id);
  }
  hemoPerformedsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/", new TCDoParam(method, payload));
  }

  hemoPerformedDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "form_encounter/" + parentId + "/hemo_performed/" + id + "/do", new TCDoParam(method, payload));
  }



}
