import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabEditComponent } from './lab-edit.component';

describe('LabEditComponent', () => {
  let component: LabEditComponent;
  let fixture: ComponentFixture<LabEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
