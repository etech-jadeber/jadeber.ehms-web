import { I } from '@angular/cdk/keycodes';
import { Component, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { lab_order_type, lab_urgency, OrderType } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelSummary, Order } from 'src/app/lab_panels/lab_panel.model';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { TCModalModes, TCParentChildIds } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { OrdersPersist } from '../orderss/orders.persist';
import { LabOrdersListComponent } from 'src/app/lab-orders-list/lab-orders-list.component';
import { CommonOrdersDetail } from '../orderss/orders.model';
import { TCUtilsString } from 'src/app/tc/utils-string';

@Component({
  selector: 'app-lab-edit',
  templateUrl: './lab-edit.component.html',
  styleUrls: ['./lab-edit.component.css']
})
export class LabEditComponent implements OnInit {

  labOrders:Lab_OrderSummary[] = [];
  radiology:Lab_OrderSummary[] = [];
  labPanels:Lab_PanelSummary[] = [];
  labTests:Lab_TestSummary[] = [];

  radPanels:Lab_PanelSummary[] = [];
  radTests:Lab_TestSummary[] = [];

  selectedLabPanel:Lab_PanelSummary[] = [];
  selectedLabTest:Lab_TestSummary[] = [];

  selectedradPanel:Lab_PanelSummary[] = [];
  selectedradTest:Lab_TestSummary[] = [];

  OrderType = OrderType;

  lab_urgency:typeof lab_urgency = lab_urgency
  laburgency:number = lab_urgency.normal;
  radurgency:number = lab_urgency.normal;

  futureLab:boolean = false;
  futureRad:boolean = false;


  futureLabDate:Date = new Date();
  futureRadDate:Date = new Date();
  title: string;

  isWizard(): boolean {
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] == TCModalModes.EDIT;

  }

  onCancel(): void {
    this.dialogRef.close();
  }


  @ViewChild( 'orders', { static: true }) orders: LabOrdersListComponent;
  constructor(   public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    private tcNotification:TCNotification,
    public lab_testPersist: Lab_TestPersist,
    public dialogRef: MatDialogRef<LabEditComponent>,
    public appTranslation:AppTranslation,
    public orderPersisit:OrdersPersist,
    public tcUtilsArray:TCUtilsArray,
    public form_encounterPersist:Form_EncounterPersist,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate:TCUtilsDate,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) { }

  ngOnInit() {
    // this.getALLLabTypes();
    // this.getAllRadiologyTypes();
    this.title = "New Order"
    if(this.ids.context["future_date"] != null){
      this.futureLab = true;
      this.futureRad = true;
      this.futureLabDate = this.tcUtilsDate.toDate(this.ids.context["future_date"]);
      this.futureRadDate = this.tcUtilsDate.toDate(this.ids.context["future_date"]);

    }

  }

  addSelectedOrder(orders: {[id: number]: CommonOrdersDetail}) {
    for (let order in orders){
      orders[order].selectedLabPanel.forEach(panel => {
        this.orderPersisit.addSelected_Orders({lab_panel_id: panel.id, lab_test_id: this.tcUtilsString.invalid_id, order_id: this.ids.context['order_id'], lab_order_id: panel.lab_order_id}).subscribe()
      })
      orders[order].selectedLabTest.forEach((panel,idx, array ) => {
        this.orderPersisit.addSelected_Orders({lab_test_id: panel.id, lab_panel_id: this.tcUtilsString.invalid_id, order_id: this.ids.context['order_id'], lab_order_id: panel.lab_order_id}).subscribe(
          (value) => {
            console.log(array.length, idx)
            if ((array.length - 1 )== idx){
              this.dialogRef.close(orders)
            }
          }
        )
      })
    }
  }

  saveOrder() {
    const orders = this.orders.getAllOrder()
    if (this.isWizard()){
      this.dialogRef.close(orders)
      return;
    }
    if (this.ids.context['order_id']){
      this.addSelectedOrder(orders)
    }else {
      if (this.ids.context['parentId']){
        if (this.ids.context['orderType'].find(order => order == lab_order_type.lab)){
          this.orders.orderLabNow()
        }
        if (this.ids.context['orderType'].find(order => order == lab_order_type.imaging)){
          this.orders.orderRadNow()
        }
        if (this.ids.context['orderType'].find(order => order == lab_order_type.pathology)){
          this.orders.orderPatNow()
        }
      }
      this.dialogRef.close(orders)
    }
  }

  canSubmit(){
    this.orders.canSubmit()
  }

}
