import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalCertificateEditComponent } from './medical-certificate-edit.component';

describe('MedicalCertificateEditComponent', () => {
  let component: MedicalCertificateEditComponent;
  let fixture: ComponentFixture<MedicalCertificateEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCertificateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
