import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Medical_CertificateDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DiagnosisPersist } from '../diagnosiss/diagnosis.persist';
import { FinalAssessmentPersist } from 'src/app/final_assessment/final_assessment.persist';
import { PatientHistoryPersist } from 'src/app/patient_history/patient_history.persist';
import { PatientHistoryDetail } from 'src/app/patient_history/patient_history.model';


@Component({
  selector: 'app-medical_certificate-edit',
  templateUrl: './medical-certificate-edit.component.html',
  styleUrls: ['./medical-certificate-edit.component.css']
})
export class Medical_CertificateEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  date_of_issued:Date  = new Date();
  start_date:Date;
  end_date:Date;
  out_start_date: Date;
  out_end_date: Date ;
  date_of_visit:Date ;
  name:string;
  diagnosis:string[] = [];
  doctor : string;
  medical_certificateDetail: Medical_CertificateDetail = new Medical_CertificateDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Medical_CertificateEditComponent>,
              public appTranslation:AppTranslation,
              public tcUtilsDate:TCUtilsDate,
              private tcUtilDate:TCUtilsDate,
              public  persist: Form_EncounterPersist,
              public  patientHistoryPersist: PatientHistoryPersist,

              public patientPersist :PatientPersist,
              public diagnosisPersist: DiagnosisPersist,
              public finalAssesmentPersist: FinalAssessmentPersist,
              public formSoapPersist:  Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew()) {
      this.title = this.appTranslation.getText("general","new") + " "+ this.appTranslation.getText("patient","medical_certificate");
      this.loadEncounter();
      this.loadDiagnosis();
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general","edit") + " "+ this.appTranslation.getText("patient","medical_certificate");
      this.isLoadingResults = true;
      this.persist.getMedical_Certificate(this.ids.parentId, this.ids.childId).subscribe(medical_certificateDetail => {
        this.loadEncounter();
        this.loadDiagnosis();
        this.medical_certificateDetail = medical_certificateDetail;

        this.transformDate(false);
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }

  loadEncounter(){
    this.persist.getForm_Encounter(this.ids.parentId).subscribe(res=>{
      this.name = res.full_name;
      this.medical_certificateDetail.encounter=res.id;
      this.doctor=res.provider_name;
      this.date_of_visit=this.tcUtilDate.toDate(res.date);
      this.medical_certificateDetail.doctor=res.provider_id;
      this.medical_certificateDetail.patient_id = res.pid;
      // this.medical_certificateDetail.start_date =res.date;
      // this.medical_certificateDetail.out_start_date = res.date;
      // this.start_date = this.tcUtilDate.toDate(res.date)
      this.patientPersist.getPatient(res.pid).subscribe(pat=>{
        this.medical_certificateDetail.card_no =pat.pid.toString();
        this.medical_certificateDetail.sex=pat.sex?"Male":"Female";
        this.medical_certificateDetail.age=parseInt(((this.tcUtilDate.toTimeStamp(new Date())- pat.dob) / (60*60*24*365)).toFixed());
      })
    })

  }

  appendString(value: string):string{
    value = value.trim();
    if (!this.medical_certificateDetail.diagnosis)
      return "\u2022  " + value;
    if(this.medical_certificateDetail.diagnosis.endsWith("\n"))
      return this.medical_certificateDetail.diagnosis + "\u2022  " + value;
    let x = this.medical_certificateDetail.diagnosis.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return  y ?  y+"\u2022  " +value : "\u2022  " + value;
  }
  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }

  loadDiagnosis(){

    this.finalAssesmentPersist.searchFinalAssessment(this.ids.parentId,1,0,"","").subscribe(final_assesments =>{
      if(final_assesments.data.length){
        this.medical_certificateDetail.diagnosis = final_assesments.data[0].final_diagnosis;
      }else{
        this.diagnosisPersist.searchDiagnosis(this.ids.parentId,1,0,"","").subscribe(res=>{
          if(res.data.length){
            this.medical_certificateDetail.diagnosis= res.data[0].name;
          }else {
            this.formSoapPersist.form_soapEncounterId = this.ids.parentId;
            this.formSoapPersist.searchForm_Soap(1,0,"","").subscribe(psoap => {
              this.formSoapPersist.form_soapEncounterId = null;
              if(psoap.data.length){
                this.medical_certificateDetail.diagnosis = psoap.data[0].final_diagnosis;
              }
            });
          }

        });
      }

    })


  }


  canSubmit():boolean{
      if (this.medical_certificateDetail == null){
          return false;
        }

      if (this.medical_certificateDetail.encounter == null || this.medical_certificateDetail.encounter  == "") {
          return false;
      }
      if (this.medical_certificateDetail.age == null) {
                return false;
            }
      if (this.medical_certificateDetail.sex == null || this.medical_certificateDetail.sex  == "") {
                return false;
            }
      if (this.medical_certificateDetail.card_no == null || this.medical_certificateDetail.card_no  == "") {
                return false;
            }
      if (this.medical_certificateDetail.diagnosis == null || this.medical_certificateDetail.diagnosis  == "") {
                return false;
            }
      if (this.medical_certificateDetail.rest_required == null || this.medical_certificateDetail.rest_required  == "") {
                return false;
            }
      if (this.medical_certificateDetail.recommendation == null || this.medical_certificateDetail.recommendation  == "") {
                return false;
            }
      if (this.medical_certificateDetail.doctor == null || this.medical_certificateDetail.doctor  == "") {
                return false;
            }
      return true;

    }


  onAdd(): void {
    this.isLoadingResults = true;
    console.log(this.medical_certificateDetail)
    this.transformDate();
    this.persist.addMedical_Certificate(this.ids.parentId, this.medical_certificateDetail).subscribe(value => {
      this.tcNotification.success("Medical_Certificate added");
      this.medical_certificateDetail.id = value.id;
      this.dialogRef.close(this.medical_certificateDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDate();
    this.persist.updateMedical_Certificate(this.medical_certificateDetail).subscribe(value => {
      this.tcNotification.success("Medical_Certificate updated");
      this.dialogRef.close(this.medical_certificateDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  transformDate(toDate:boolean = true):void{
    if(toDate){
      // this.medical_certificateDetail.date_of_visit =  this.tcUtilDate.toTimeStamp(this.date_of_visit);
      this.medical_certificateDetail.date_of_issued =  this.tcUtilDate.toTimeStamp(this.date_of_issued);
      if(this.end_date){
        this.medical_certificateDetail.end_date = this.tcUtilDate.toTimeStamp(this.end_date);
      }
      if(this.start_date){
        this.medical_certificateDetail.start_date = this.tcUtilDate.toTimeStamp(this.start_date);
      }
      if(this.out_start_date){
        this.medical_certificateDetail.out_start_date = this.tcUtilDate.toTimeStamp(this.out_start_date);
      }
      if(this.out_end_date){
      this.medical_certificateDetail.out_end_date = this.tcUtilDate.toTimeStamp(this.out_end_date);
      }
    }
    else{
     // this.date_of_visit = this.tcUtilDate.toDate(this.medical_certificateDetail.date_of_visit  );
      this.start_date = this.medical_certificateDetail.start_date > 0 ? this.tcUtilDate.toDate(this.medical_certificateDetail.start_date  ) : null;
      this.end_date = this.medical_certificateDetail.end_date > 0 ? this.tcUtilDate.toDate(this.medical_certificateDetail.end_date  )  : null;
     this.out_start_date = this.medical_certificateDetail.out_start_date > 0 ? this.tcUtilDate.toDate(this.medical_certificateDetail.out_start_date)  : null;
     this.out_end_date = this.medical_certificateDetail.out_end_date > 0 ? this.tcUtilDate.toDate(this.medical_certificateDetail.out_end_date)  : null;

    }
  }
}
