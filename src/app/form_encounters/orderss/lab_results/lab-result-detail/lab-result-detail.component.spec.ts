import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Lab_ResultDetailComponent } from './lab-result-detail.component';

describe('Lab_ResultDetailComponent', () => {
  let component: Lab_ResultDetailComponent;
  let fixture: ComponentFixture<Lab_ResultDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_ResultDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_ResultDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
