import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../../../tc/authorization";
import {TCUtilsAngular} from "../../../../tc/utils-angular";
import {TCNotification} from "../../../../tc/notification";
import {TCUtilsArray} from "../../../../tc/utils-array";
import {TCNavigator} from "../../../../tc/navigator";
import {JobPersist} from "../../../../tc/jobs/job.persist";
import {AppTranslation} from "../../../../app.translation";


import {Lab_ResultDetail} from "../lab_result.model";
import {Lab_ResultPersist} from "../lab_result.persist";
import {Lab_ResultNavigator} from "../lab_result.navigator";

export enum Lab_ResultTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-lab-result-detail',
  templateUrl: './lab-result-detail.component.html',
  styleUrls: ['./lab-result-detail.component.css']
})
export class Lab_ResultDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  lab_resultLoading:boolean = false;
  
  Lab_ResultTabs: typeof Lab_ResultTabs = Lab_ResultTabs;
  activeTab: Lab_ResultTabs = Lab_ResultTabs.overview;
  //basics
  lab_resultDetail: Lab_ResultDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public lab_resultNavigator: Lab_ResultNavigator,
              public  lab_resultPersist: Lab_ResultPersist) {
                if (!(this.tcAuthorization.canRead('laboratory_results') || this.tcAuthorization.canRead('radiology_results'))){
                  this.tcNavigator.home()
                }
    this.lab_resultDetail = new Lab_ResultDetail();
  }

  ngOnInit() {
    if (!(this.tcAuthorization.canRead('laboratory_results') || this.tcAuthorization.canRead('radiology_results'))){
      this.tcNavigator.home()
    }
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.lab_resultLoading = true;
    this.lab_resultPersist.getLab_Result(id).subscribe(lab_resultDetail => {
          this.lab_resultDetail = lab_resultDetail;
          this.lab_resultLoading = false;
        }, error => {
          console.error(error);
          this.lab_resultLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.lab_resultDetail.id);
  }

  editLab_Result(): void {
    let modalRef = this.lab_resultNavigator.editLab_Result(this.lab_resultDetail.id);
    modalRef.afterClosed().subscribe(modifiedLab_ResultDetail => {
      TCUtilsAngular.assign(this.lab_resultDetail, modifiedLab_ResultDetail);
    }, error => {
      console.error(error);
    });
  }

   printLab_Result():void{
      this.lab_resultPersist.print(this.lab_resultDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print lab_result", true);
      });
    }

  back():void{
      this.location.back();
    }

}
