import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Lab_ResultEditComponent } from './lab-result-edit.component';

describe('Lab_ResultEditComponent', () => {
  let component: Lab_ResultEditComponent;
  let fixture: ComponentFixture<Lab_ResultEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_ResultEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_ResultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
