import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../../tc/authorization";
import {TCNotification}  from "../../../../tc/notification";
import {TCUtilsString}   from "../../../../tc/utils-string";
import {AppTranslation} from "../../../../app.translation";
import {physican_type, physician_type} from "../../../../app.enums"

import {TCIdMode, TCModalModes} from "../../../../tc/models";


import {Lab_ResultDetail} from "../lab_result.model";
import {Lab_ResultPersist} from "../lab_result.persist";
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { TCNavigator } from 'src/app/tc/navigator';


@Component({
  selector: 'app-lab-result-edit',
  templateUrl: './lab-result-edit.component.html',
  styleUrls: ['./lab-result-edit.component.css']
})
export class Lab_ResultEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lab_resultDetail: Lab_ResultDetail;
  confirmedByLabTechnician: string;
  resultedByLabTechnician: string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Lab_ResultEditComponent>,
              public  persist: Lab_ResultPersist,
              public labTechnicianNavigator: DoctorNavigator,
              public labTechnicianPersist: DoctorPersist,
              public tcNavigator: TCNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     if (!(this.tcAuthorization.canCreate('laboratory_results') || this.tcAuthorization.canCreate('radiology_results'))){
      this.tcNavigator.home()
    }
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("procedure", "lab_results");
      this.lab_resultDetail = new Lab_ResultDetail();
      //set necessary defaults

    } else {
     if (!(this.tcAuthorization.canUpdate('laboratory_results') || this.tcAuthorization.canUpdate('radiology_results'))){
      this.tcNavigator.home()
    }
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("procedure", "lab_results");
      this.isLoadingResults = true;
      this.persist.getLab_Result(this.idMode.id).subscribe(lab_resultDetail => {
        this.lab_resultDetail = lab_resultDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addLab_Result(this.idMode.id, this.lab_resultDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("procedure", "lab_results") + " " + this.appTranslation.getText("general", "added"));
      this.lab_resultDetail.id = value.id;
      this.dialogRef.close(this.lab_resultDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateLab_Result(this.lab_resultDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("procedure", "lab_results") + " " + this.appTranslation.getText("general", "updated"));
      this.dialogRef.close(this.lab_resultDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  searchConfirmedByLabTechnician() {
    // this.labTechnicianPersist.physican_typeId = physican_type.lab;
    let dialogRef = this.labTechnicianNavigator.pickDoctors(true, physican_type.lab);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.confirmedByLabTechnician = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.lab_resultDetail.confirmed_by = result[0].id;
      }
    });
  }

  searchResultedByLabTechnician() {
    // this.labTechnicianPersist.physican_typeId = physican_type.lab;
    let dialogRef = this.labTechnicianNavigator.pickDoctors(true, physican_type.lab);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.resultedByLabTechnician = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.lab_resultDetail.result_by = result[0].id;
      }
    });
  }


  canSubmit():boolean{
        if (this.lab_resultDetail == null){
            return false;
          }
        if (!this.lab_resultDetail.result){
          return false;
        }
        if (!this.lab_resultDetail.result_by){
          return false;
        }
        if (!this.lab_resultDetail.confirmed_by){
          return false;
        }


        return true;
      }


}

