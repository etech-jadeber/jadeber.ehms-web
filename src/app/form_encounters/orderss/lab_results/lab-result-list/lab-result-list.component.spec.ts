import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Lab_ResultListComponent } from './lab-result-list.component';

describe('Lab_ResultListComponent', () => {
  let component: Lab_ResultListComponent;
  let fixture: ComponentFixture<Lab_ResultListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_ResultListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_ResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
