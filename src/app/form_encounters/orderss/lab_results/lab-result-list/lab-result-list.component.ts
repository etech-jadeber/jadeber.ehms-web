import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';
import { OrdersPersist } from "../../../orderss/orders.persist";
import {TCAuthorization} from "../../../../tc/authorization";
import {TCModalWidths, TCUtilsAngular} from "../../../../tc/utils-angular";
import {TCUtilsArray} from "../../../../tc/utils-array";
import {TCUtilsDate} from "../../../../tc/utils-date";
import {TCNotification} from "../../../../tc/notification";
import {TCNavigator} from "../../../../tc/navigator";
import {JobPersist} from "../../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../../app.translation";

import {Lab_ResultPersist} from "../lab_result.persist";
import {Lab_ResultNavigator} from "../lab_result.navigator";
import {Lab_ResultDetail, Lab_ResultSummary, Lab_ResultSummaryPartialList} from "../lab_result.model";
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { Selected_OrdersSummary, Selected_OrdersSummaryPartialList, Selected_OrdersDetail } from '../../../orderss/orders.model';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist'
import { Lab_PanelDetail } from 'src/app/lab_panels/lab_panel.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestDetail, Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { OrdersDetail } from '../../orders.model';
import { lab_order_type, selected_orders_status } from 'src/app/app.enums';
import { environment } from 'src/environments/environment';
import { PathologyResultPersist } from 'src/app/pathology_result/pathology_result.persist';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { PathologyResultNavigator } from 'src/app/pathology_result/pathology_result.navigator';
import { OrdersNavigator } from '../../orders.navigator';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PanelResultListComponent } from '../panel-result-list/panel-result-list.component';



@Component({
  selector: 'app-lab-result-list',
  templateUrl: './lab-result-list.component.html',
  styleUrls: ['./lab-result-list.component.css']
})
export class Lab_ResultListComponent implements OnInit {

  lab_resultsData: Selected_OrdersSummary[] = [];
  lab_resultsTotalCount: number = 0;

  lab_resultsSelectAll:boolean = false;
  lab_resultsSelection: Lab_ResultSummary[] = [];
  previous_lab_resultsData: Lab_ResultSummary[] = [];
  active_lab_test: string

  lab_resultsDisplayedColumns: string[] = ["select","result" ,"labtest_id","lab_panel_id"];
  lab_resultsSearchTextBox: FormControl = new FormControl();
  lab_resultsIsLoading: boolean = false;
  lab_panelLists = [];
  lab_testisLoading: boolean = false;
  isPathology: boolean = false;
  lab_order_type = lab_order_type
  dicoms;

  @Input() orderId: string;
  @Input() orderDetail: OrdersDetail;
  @Input() encounterId: string;
  @Output() onResult = new EventEmitter<number>();
  @Input() patientDetail: PatientDetail
  @Input() order_type: number;
  @Input() isDialog: boolean = false;


  @Output() assign_value = new EventEmitter<boolean>();

  @ViewChild(MatPaginator, {static: true}) lab_resultsPaginator: MatPaginator;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lab_resultPersist: Lab_ResultPersist,
                public ordersPersist: OrdersPersist,
                public lab_resultNavigator: Lab_ResultNavigator,
                public lab_technicianPersist: DoctorPersist,
                public jobPersist: JobPersist,
                public lab_panelPersist: Lab_PanelPersist,
                public lab_testPersist: Lab_TestPersist,
                public stringUtilities: TCUtilsString,
                public userPersist: UserPersist,
                public ordersNavigator: OrdersNavigator,
                public pathologyResultPersist: PathologyResultPersist,
                public labOrderPersist: Lab_OrderPersist,
                public pathologyResultNavigator: PathologyResultNavigator,
    ) {
        if (!(this.tcAuthorization.canRead('laboratory_results') || this.tcAuthorization.canRead('radiology_results'))){
          this.tcNavigator.home()
        }
       this.lab_resultsSearchTextBox.setValue(lab_resultPersist.lab_resultSearchText);
      //delay subsequent keyup events
      this.lab_resultsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lab_resultPersist.lab_resultSearchText = value;
        this.searchLab_Results();
      });
    }

    ngOnInit() {
      this.lab_resultsPaginator.page.subscribe(() => {
        this.searchLab_Results(true);
      });
      //start by loading items
      this.searchLab_Results();
    }

  searchLab_Results(isPagination:boolean = false): void {
      let paginator = this.lab_resultsPaginator
      this.lab_resultsIsLoading = true;
      // if (this.order_type == lab_order_type.imaging && this.orderDetail){
      //   this.ordersPersist.getStudies(this.orderDetail.generated_sample_id).subscribe((value) => {
      //     this.dicoms = value
      //    })
      // }
      this.lab_resultPersist.lab_resultsDo("result_by_order_and_panel", {order_id: this.orderId, encounter_id: this.encounterId, order_type: this.order_type}, paginator.pageSize, isPagination ? 0 : paginator.pageIndex, 'id', 'asc').subscribe(
        (response: any) => {
          this.lab_panelLists = response.data
          this.lab_panelLists.forEach(lab_panel => {
            this.userPersist.getUser(lab_panel.result_by).subscribe(
              user => {
                lab_panel.result_by_name = user.name;
              }
            )
          })
          if (response.total != -1){
            this.assign_value.emit((response.total > 0))
            this.lab_resultsTotalCount = response.total
          }
          this.lab_resultsIsLoading = false
        },
        error => {
          this.lab_resultsIsLoading = false;
          console.error(error)
        }
      )
  }

  buildDicomUrl():string{
    return  this.dicoms && this.dicoms.length ? `${environment.dicom_viewer}${this.dicoms[0]['0020000D'].Value[0]}` : "";
  }


  downloadLab_Results(): void {
    if(this.lab_resultsSelectAll){
         this.lab_resultPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lab_results", true);
      });
    }
    else{
        this.lab_resultPersist.download(this.tcUtilsArray.idsList(this.lab_resultsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lab_results",true);
            });
        }
  }



  addLab_Result(): void {
    let dialogRef = this.lab_resultNavigator.addLab_Result(this.orderId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLab_Results();
      }
    });
  }

  editLab_Result(item: Lab_ResultSummary) {
    let dialogRef = this.lab_resultNavigator.editLab_Result(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchLab_Results();
      }

    });
  }

  deleteLab_Result(item: Lab_ResultSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Lab_Result");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lab_resultPersist.deleteLab_Result(item.id).subscribe(response => {
          this.tcNotification.success("Lab_Result deleted");
          this.searchLab_Results();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }
}

