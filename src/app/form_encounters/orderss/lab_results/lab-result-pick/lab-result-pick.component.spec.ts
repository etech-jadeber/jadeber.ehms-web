import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Lab_ResultPickComponent } from './lab-result-pick.component';

describe('Lab_ResultPickComponent', () => {
  let component: Lab_ResultPickComponent;
  let fixture: ComponentFixture<Lab_ResultPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_ResultPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_ResultPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
