import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../../tc/authorization";
import {TCUtilsAngular} from "../../../../tc/utils-angular";
import {TCUtilsArray} from "../../../../tc/utils-array";
import {TCUtilsDate} from "../../../../tc/utils-date";
import {TCNotification} from "../../../../tc/notification";
import {TCNavigator} from "../../../../tc/navigator";
import {AppTranslation} from "../../../../app.translation";

import {Lab_ResultDetail, Lab_ResultSummary, Lab_ResultSummaryPartialList} from "../lab_result.model";
import {Lab_ResultPersist} from "../lab_result.persist";


@Component({
  selector: 'app-lab-result-pick',
  templateUrl: './lab-result-pick.component.html',
  styleUrls: ['./lab-result-pick.component.css']
})
export class Lab_ResultPickComponent implements OnInit {

  lab_resultsData: Lab_ResultSummary[] = [];
  lab_resultsTotalCount: number = 0;
  lab_resultsSelection: Lab_ResultSummary[] = [];
  lab_resultsDisplayedColumns: string[] = ["select", 'selected_order_id','result','result_by','confirmed_by' ];

  lab_resultsSearchTextBox: FormControl = new FormControl();
  lab_resultsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) lab_resultsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_resultsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public lab_resultPersist: Lab_ResultPersist,
              public dialogRef: MatDialogRef<Lab_ResultPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    if (!(this.tcAuthorization.canRead('laboratory_results') || this.tcAuthorization.canRead('radiology_results'))){
      this.tcNavigator.home()
    }
    this.lab_resultsSearchTextBox.setValue(lab_resultPersist.lab_resultSearchText);
    //delay subsequent keyup events
    this.lab_resultsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.lab_resultPersist.lab_resultSearchText = value;
      this.searchLab_Results();
    });
  }

  ngOnInit() {

    this.lab_resultsSort.sortChange.subscribe(() => {
      this.lab_resultsPaginator.pageIndex = 0;
      this.searchLab_Results();
    });

    this.lab_resultsPaginator.page.subscribe(() => {
      this.searchLab_Results();
    });

    //set initial picker list to 5
    this.lab_resultsPaginator.pageSize = 5;

    //start by loading items
    this.searchLab_Results();
  }

  searchLab_Results(): void {
    this.lab_resultsIsLoading = true;
    this.lab_resultsSelection = [];

    this.lab_resultPersist.searchLab_Result("123", this.lab_resultsPaginator.pageSize,
        this.lab_resultsPaginator.pageIndex,
        this.lab_resultsSort.active,
        this.lab_resultsSort.direction).subscribe((partialList: Lab_ResultSummaryPartialList) => {
      this.lab_resultsData = partialList.data;
      if (partialList.total != -1) {
        this.lab_resultsTotalCount = partialList.total;
      }
      this.lab_resultsIsLoading = false;
    }, error => {
      this.lab_resultsIsLoading = false;
    });

  }

  markOneItem(item: Lab_ResultSummary) {
    if(!this.tcUtilsArray.containsId(this.lab_resultsSelection,item.id)){
          this.lab_resultsSelection = [];
          this.lab_resultsSelection.push(item);
        }
        else{
          this.lab_resultsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.lab_resultsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}

