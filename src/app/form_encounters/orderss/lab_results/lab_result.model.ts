import {TCId} from "../../../tc/models";

export class Lab_ResultSummary extends TCId {
  selected_order_id : string;
result : string;
result_by : string;
confirmed_by : string;
result_by_full_name: string;
confirmed_by_full_name?: string;
lab_test_id: string;
status: number;
result_date: number;
result_description: string;
result_conclusion: string
reject_description: string;
urgency: number;
lab_panel_id: string;
lab_panel_name: string;
result_file_id: string;
key: string;
conclusion: string;
  style: string;
  test_unit_id: string;
  company_id: string;
  reference_range: string;
}

export class ResultProgressDetail {
  patient_id: string;
  patient_name: string;
  pid: number;
  generated_sample_id: string;
  date_of_order: number;
  date_of_result: number;
  total_order: number;
  completed: number;
  rejected: number;
  requested: number;
}

export class ResultProgressPartialList{
  data: ResultProgressDetail[];
  total: number;
}

export class Lab_ResultSummaryPartialList {
  data: Lab_ResultSummary[];
  total: number;
}

export class Lab_ResultDetail extends Lab_ResultSummary {
  selected_order_id : string;
result : string;
result_by : string;
confirmed_by : string;
result_by_full_name: string;
confirmed_by_full_name?: string;
lab_test_id: string;
status: number;
result_date: number;
result_file_id: string;
conclusion: string;
  range?: string;
  min_range?: number;
  max_range?: number;
}

export class Lab_ResultDashboard {
  total: number;
}
