import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../../../tc/models";

import {Lab_ResultEditComponent} from "./lab-result-edit/lab-result-edit.component";
import {Lab_ResultPickComponent} from "./lab-result-pick/lab-result-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Lab_ResultNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  lab_resultsUrl(): string {
    return "/lab_results";
  }

  lab_resultUrl(id: string): string {
    return "/lab_results/" + id;
  }

  resultProgressUrl(): string {
    return "/result_progress";
  }

  viewResultProgress(): void {
    this.router.navigateByUrl(this.resultProgressUrl())
  }

  viewLab_Results(): void {
    this.router.navigateByUrl(this.lab_resultsUrl());
  }

  viewLab_Result(id: string): void {
    this.router.navigateByUrl(this.lab_resultUrl(id));
  }

  editLab_Result(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_ResultEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLab_Result(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_ResultEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickLab_Results(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Lab_ResultPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
