import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../../tc/utils-http";
import {TCId, TcDictionary} from "../../../tc/models";
import {Lab_ResultDashboard, Lab_ResultDetail, Lab_ResultSummaryPartialList} from "./lab_result.model";
import { TCUtilsString } from 'src/app/tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Lab_ResultPersist {

  lab_resultSearchText: string = "";
  lab_test_id: string = "";
  status: number;

  result_progressSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchLab_Result = (parentId: string, pageSize: number, pageIndex: number, sort: string, order: string, lab_test_id?: string, status?: number): Observable<Lab_ResultSummaryPartialList> => {

    let url = TCUtilsHttp.buildSearchUrl("orders/" + parentId + "/lab_results", this.lab_resultSearchText, pageSize, pageIndex, sort, order);
    if (lab_test_id){
      url = TCUtilsString.appendUrlParameter(url, "lab_test_id", this.lab_test_id.toString());
    }
    if (status){
      url = TCUtilsString.appendUrlParameter(url, "status", status.toString())
    }
    return this.http.get<Lab_ResultSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.lab_resultSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_results/do", new TCDoParam("download_all", this.filters()));
  }

  lab_resultDashboard(): Observable<Lab_ResultDashboard> {
    return this.http.get<Lab_ResultDashboard>(environment.tcApiBaseUri + "lab_results/dashboard");
  }

  getLab_Result(id: string): Observable<Lab_ResultDetail> {
    return this.http.get<Lab_ResultDetail>(environment.tcApiBaseUri + "lab_results/" + id);
  }

  addLab_Result(parentId: string, item: Lab_ResultDetail): Observable<TCId> {
    if (item.id != null){
      return this.updateLab_Result(item)
  }
    return this.http.post<TCId>(environment.tcApiBaseUri + "orders/" + parentId + "/lab_results", item);
  }

  updateLab_Result(item: Lab_ResultDetail): Observable<Lab_ResultDetail> {
    return this.http.patch<Lab_ResultDetail>(environment.tcApiBaseUri + "lab_results/" + item.id, item);
  }

  deleteLab_Result(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "lab_results/" + id);
  }

  lab_resultsDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("lab_results/do", this.lab_resultSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  lab_resultDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_results/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "lab_results/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "lab_results/" + id + "/do", new TCDoParam("print", {}));
  }


}
