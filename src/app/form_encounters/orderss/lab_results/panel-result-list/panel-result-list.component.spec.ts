import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelResultListComponent } from './panel-result-list.component';

describe('PanelResultListComponent', () => {
  let component: PanelResultListComponent;
  let fixture: ComponentFixture<PanelResultListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelResultListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
