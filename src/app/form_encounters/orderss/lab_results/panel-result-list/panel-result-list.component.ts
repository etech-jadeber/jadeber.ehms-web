import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {Location} from '@angular/common';
import { Router } from '@angular/router';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { AppTranslation } from 'src/app/app.translation';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { Lab_ResultPersist } from '../lab_result.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCModalWidths, TCUtilsAngular } from 'src/app/tc/utils-angular';
import { OrdersDetail, Selected_OrdersDetail, Selected_OrdersSummary } from '../../../orderss/orders.model';
import { Lab_PanelDetail } from 'src/app/lab_panels/lab_panel.model';
import { Lab_TestDetail } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { lab_order_type, selected_orders_status, sex } from 'src/app/app.enums';
import { Lab_ResultDetail } from '../lab_result.model';
import { PathologyResultNavigator } from 'src/app/pathology_result/pathology_result.navigator';
import { PathologyResultPersist } from 'src/app/pathology_result/pathology_result.persist';
import { environment } from 'src/environments/environment';
import { MatPaginator } from '@angular/material/paginator';
import { RadiologyResultNavigator } from 'src/app/radiology_result/radiology_result.navigator';
import { RadiologyResultDetail } from 'src/app/radiology_result/radiology_result.model';
import { OrdersNavigator } from '../../orders.navigator';
import { LabTestNormalRangePersist } from 'src/app/lab-test-normal-range/lab-test-normal-range.persist';
import { LabTestNormalRangeDetail } from 'src/app/lab-test-normal-range/lab-test-normal-range.model';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PathologyResultDetail } from 'src/app/pathology_result/pathology_result.model';
import { UserDetail } from 'src/app/tc/users/user.model';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { MeasuringUnitPersist } from 'src/app/measuring_unit/measuringUnit.persist';
import { MeasuringUnitDetail } from 'src/app/measuring_unit/measuringUnit.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { FilePersist } from 'src/app/tc/files/file.persist';

@Component({
  selector: 'app-panel-result-list',
  templateUrl: './panel-result-list.component.html',
  styleUrls: ['./panel-result-list.component.scss']
})
export class PanelResultListComponent implements OnInit {

  panel_results: (Lab_ResultDetail|PathologyResultDetail|RadiologyResultDetail)[] = [];
  panel_resultTotalCount: number = 0;
  panel_resultLoading: boolean = false
  users: {[id: string]: UserDetail} = {}
  selected_order_status = selected_orders_status
  lab_order_type = lab_order_type
  Editor = ClassicEditor;
  config = {'toolbar': []};
  units: {[id: string]: MeasuringUnitDetail} = {}

  @Input() orderId: string;
  @Input() panelId: string;
  @Input() lab_order_id: string;
  @Input() patientDetail: PatientDetail;
  @Input() order_type: number;
  @ViewChild(MatPaginator, {static: true}) lab_resultsPaginator: MatPaginator;
  constructor(
    private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public userPersist: UserPersist,
                public lab_panelPersist: Lab_PanelPersist,
                public lab_testPersist: Lab_TestPersist,
                public pathologyResultPersist: PathologyResultPersist,
                public pathologyResultNavigator: PathologyResultNavigator,
                public ordersNavigator: OrdersNavigator,
                public radiologyResultNavigator: RadiologyResultNavigator,
                public measuringUnitPersist: MeasuringUnitPersist,
                public tcUtilsString: TCUtilsString,
                public filePersist: FilePersist,
    public lab_resultPersist: Lab_ResultPersist,
    public labTestNormalRangePersist: LabTestNormalRangePersist,
    public radiologyResultSettingPersist: RadiologyResultSettingsPersist
  ) { }

  ngOnInit(): void {
    this.lab_resultsPaginator.page.subscribe(() => {
      this.searchLabResult(true);
    });
    //start by loading items
    this.searchLabResult()
  }

  searchLabResult(isPagination: boolean = false){
    const paginator = this.lab_resultsPaginator;
    this.panel_resultLoading = true
    this.lab_resultPersist.lab_resultsDo("result_by_order_and_panel", {isList: true, lab_order_id: this.lab_order_id, lab_panel_id: this.panelId, order_id: this.orderId},
    paginator.pageSize, isPagination ? paginator.pageIndex : 0, 'id', 'asc').subscribe((lab_result:any)=> {
      this.panel_results = lab_result.data;
      this.panel_results.forEach(
        result => {
            const res = result as Lab_ResultDetail
            const reference_range = this.tcUtilsString.getReferenceRange(res.reference_range)
            res.min_range = reference_range?.min_range
            res.max_range = reference_range?.max_range
          // this.labTestNormalRangePersist.LabTestNormalRangesDo("get_normal_range", {lab_test_id: result.lab_test_id, sex: this.patientDetail.sex ? sex.male : sex.female, age: this.getPatientAge(this.patientDetail.dob)}).subscribe(
          //   (normalRange: LabTestNormalRangeDetail) => {
          //     res.range  = this.getRangeString(normalRange[0]?.min_range, normalRange[0]?.max_range)
          //     res.min_range = normalRange[0]?.min_range
          //     res.max_range = normalRange[0]?.max_range
          //   }
          // )
          if (!this.units[result.test_unit_id]){
            this.units[result.test_unit_id] = new MeasuringUnitDetail()
          result.test_unit_id && result.test_unit_id != this.tcUtilsString.invalid_id && this.measuringUnitPersist.getMeasuringUnit(result.test_unit_id).subscribe(
            (measuringUnit: MeasuringUnitDetail) => {
              this.units[result.test_unit_id] = measuringUnit
            }
          )}
          if (!this.users[result.result_by]){
            this.users[result.result_by] = new UserDetail()
            this.userPersist.getUser(result.result_by).subscribe(
              user => {
                this.users[result.result_by] = user;
              }
            )
          }
        }
      )
      if(lab_result.total != -1){
        this.panel_resultTotalCount = lab_result.total
      }
      this.panel_resultLoading = false
    }, error => {
      this.panel_resultLoading = false
      console.error(error)
    })
  }

  showResult(selected_orders: Selected_OrdersDetail){
    if (selected_orders.result_file_id){
      this.filePersist.getDownloadKey(selected_orders.result_file_id).subscribe((value: any) => {
        window.open(this.filePersist.downloadLink(value.id))
      })
    }
  }

  getRangeString(min_range: number, max_range: number): string{
    if (min_range && max_range){
      return `(${min_range} - ${max_range})`
    }
    if (min_range){
      return `>${min_range}`
    }
    if (max_range){
      return `<${max_range}`
    }
  }

  getStyle(selectedOrders: Selected_OrdersSummary){
    if (selectedOrders.min_range != null){
      if (selectedOrders.min_range <= +selectedOrders.result){
        return 'green'
      } else {
        return 'red'
      }
    }
    if (selectedOrders.max_range != null){
      if (selectedOrders.max_range >= +selectedOrders.result){
        return 'green'
      } else {
        return 'red'
      }
    }
  }

  showDetail(event:Event, selected: Lab_ResultDetail | PathologyResultDetail | RadiologyResultDetail){
    event.stopPropagation()
    if (this.order_type == lab_order_type.pathology){
      this.pathologyResultNavigator.showPathologyResult(selected as PathologyResultDetail)
    }
    else if (this.order_type == lab_order_type.imaging) {
      this.radiologyResultNavigator.showRadiologyResult(selected as RadiologyResultDetail);
  }else {
    this.ordersNavigator.showResult(selected.lab_test_id, selected.id? selected as Lab_ResultDetail : null);
  }
    }

    getPatientAge(dob: number){
      const year = new Date().getFullYear() - this.tcUtilsDate.toDate(dob).getFullYear()
      const month = new Date().getMonth() - this.tcUtilsDate.toDate(dob).getMonth()
      return year + month / 12
    }

    getUser(id: string){
      return this.users[id] ? this.users[id].name : ""
    }

    getUnit(id: string){
      return this.units[id] ? this.units[id].unit : ""
    }

}
