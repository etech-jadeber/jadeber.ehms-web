import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousResultListComponent } from './previous-result-list.component';

describe('PreviousResultListComponent', () => {
  let component: PreviousResultListComponent;
  let fixture: ComponentFixture<PreviousResultListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviousResultListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
