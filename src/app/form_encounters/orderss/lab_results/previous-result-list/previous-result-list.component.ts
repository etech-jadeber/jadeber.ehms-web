import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { lab_order_type, selected_orders_status } from 'src/app/app.enums';
import { TCAuthorization } from 'src/app/tc/authorization';
import { Lab_ResultDetail, Lab_ResultSummary, Lab_ResultSummaryPartialList } from '../lab_result.model';
import { Lab_ResultPersist } from '../lab_result.persist';
import {Location} from '@angular/common';
import { Router } from '@angular/router';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { AppTranslation } from 'src/app/app.translation';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCModalWidths, TCUtilsAngular } from 'src/app/tc/utils-angular';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { MatPaginator } from '@angular/material/paginator';
import { environment } from 'src/environments/environment';
import { PathologyResultNavigator } from 'src/app/pathology_result/pathology_result.navigator';
import { Selected_OrdersDetail, Selected_OrdersSummary } from '../../orders.model';
import { OrdersNavigator } from '../../orders.navigator';
import { UserDetail } from 'src/app/tc/users/user.model';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';
import { RadiologyResultDetail } from 'src/app/radiology_result/radiology_result.model';
import { RadiologyResultNavigator } from 'src/app/radiology_result/radiology_result.navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-previous-result-list',
  templateUrl: './previous-result-list.component.html',
  styleUrls: ['./previous-result-list.component.scss']
})
export class PreviousResultListComponent implements OnInit {

  previous_lab_resultsData: Lab_ResultSummary[] = [];
  lab_testDisplayedColumns: string[] = ["result_date", 'result_by', "result", "remark"];
  pat_testDisplayedColumns: string[] = ["result_date", 'result_by', 'clinical_finding', 'conclusion', 'detail']
  rad_testDisplayedColumns: string[] = ["result_date", 'result_by', 'recommendation', 'detail']
  previous_lab_resultTotalCount: number = 0;
  prevous_lab_resultLoading: boolean = false;
  users: {[id: string]: UserDetail} = {}
  Editor = ClassicEditor;
  config = {'toolbar': []};


  @Input() patientId: string;
  @Input() labTestId: string;
  @Input() orderId: string;
  @Input() min_range: number;
  @Input() max_range: number;
  @Input() order_type: number;
  @Input() sub_type: number;
  @Input() result_id: string;
  @ViewChild(MatPaginator, {static: true}) lab_resultsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) additionalServicesSort: MatSort;
  constructor(
    private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public userPersist: UserPersist,
    public lab_resultPersist: Lab_ResultPersist,
    public pathologyResultNavigator: PathologyResultNavigator,
                public ordersNavigator: OrdersNavigator,
                public radiologyResultSettingPersist: RadiologyResultSettingsPersist,
                public radiologyResultNavigator: RadiologyResultNavigator,
                public tcUtilsString: TCUtilsString,
                public filePersist: FilePersist
  ) { }

  ngOnInit(): void {

    if (this.order_type == lab_order_type.imaging){
      this.lab_testDisplayedColumns = this.rad_testDisplayedColumns;
    }
    else if (this.order_type == lab_order_type.pathology){
      this.lab_testDisplayedColumns = this.pat_testDisplayedColumns;
    }
    this.lab_resultsPaginator.page.subscribe(() => {
      this.searchPreviousResult(true);
    });
    this.additionalServicesSort.sortChange.subscribe(() => {
      this.lab_resultsPaginator.pageIndex = 0;
      this.searchPreviousResult(true);
    });
    this.additionalServicesSort.active = 'result_date';
    this.additionalServicesSort.direction = 'desc';
    //start by loading items
    this.searchPreviousResult()
  }

  searchPreviousResult(isPagination: boolean = false){
    const paginator = this.lab_resultsPaginator
    let sorter = this.additionalServicesSort;
    this.prevous_lab_resultLoading = true;
    this.lab_resultPersist.lab_resultsDo( "get_by_patient_and_test",
    {patient_id: this.patientId,
      lab_test_id: this.labTestId,
      order_id: this.orderId,
      status: selected_orders_status.approved,
      result_id: this.result_id
    }, paginator.pageSize, isPagination ? 0 : paginator.pageIndex, sorter.active, sorter.direction).subscribe((res: Lab_ResultSummaryPartialList) => {
      this.previous_lab_resultsData = res.data
      this.previous_lab_resultsData.forEach(
        result => {
          this.updateStyle(result)
          if (!this.users[result.result_by]){
            this.users[result.result_by] = new UserDetail()
            this.userPersist.getUser(result.result_by).subscribe(
              user => {
                this.users[result.result_by] = user;
              }
            )
          }
        }
      )
      if(res.total != -1){
        this.previous_lab_resultTotalCount = res.total
      }
      this.prevous_lab_resultLoading = false;

    }, error => {
      this.prevous_lab_resultLoading = false;
      console.error(error)
    })
  }

  updateStyle(selectedOrders: Lab_ResultSummary){
    if (event != null){
      if (this.min_range != null){
        if (+selectedOrders.result <= this.min_range){
          selectedOrders.style = 'green'
        } else {
          selectedOrders.style = 'red'
          return
        }
      }
      if (this.max_range != null){
        if (+selectedOrders.result >= this.max_range){
          selectedOrders.style = 'green'
        } else {
          selectedOrders.style = 'red'
        }
      }
    }
  }

  showResult(selected_orders: Selected_OrdersDetail){
    if (selected_orders.result_file_id){
      this.filePersist.getDownloadKey(selected_orders.result_file_id).subscribe((value: any) => {
        window.open(this.filePersist.downloadLink(value.id))
      })
    }
  }

  showDetail(event:Event, selected: Selected_OrdersSummary, lab_result = false){
    event.stopPropagation()
    if (this.order_type == lab_order_type.pathology){
      this.pathologyResultNavigator.editPathologyResult(selected)
    }
  //    else {
  //     this.prevous_lab_resultLoading = true;
  //     const order_type: boolean = this.order_type === lab_order_type.imaging
  //     if(order_type){
  //     let dialogRef = this.tcNavigator.confirmAction("View Dicom", "Image", `${selected.result_description}<br><br><h3>${selected.result_conclusion}</h3>`, "Cancel", TCModalWidths.large);
  //     dialogRef.afterClosed().subscribe(result => {
  //       this.prevous_lab_resultLoading = false;
  //     if (result){
  //       window.open(`${environment.dicom_url}/Viewer/${this.patientId}?${lab_result ? selected['selected_order_id'] : selected.id}`)
  //       this.prevous_lab_resultLoading = false
  //     }
  //   }
  //   )
  // }else {
  //   let dialogRef = this.ordersNavigator.addResult(selected.lab_test_id, selected.id? selected : null);
  //   dialogRef.afterClosed().subscribe(() => this.prevous_lab_resultLoading = false)
  // }
  // }
  else if (this.order_type == lab_order_type.imaging) {
    this.radiologyResultNavigator.showRadiologyResult(selected as unknown as RadiologyResultDetail);
}else {
  this.ordersNavigator.showResult(selected.lab_test_id, selected.id? selected as Lab_ResultDetail : null);
}
    }

    getUser(id: string){
      return this.users[id] ? this.users[id].name : ""
    }
}
