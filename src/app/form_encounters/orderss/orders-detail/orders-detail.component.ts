import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../../tc/authorization";
import { TCUtilsAngular } from "../../../tc/utils-angular";
import { TCNotification } from "../../../tc/notification";
import { TCUtilsArray } from "../../../tc/utils-array";
import { TCNavigator } from "../../../tc/navigator";
import { JobPersist } from "../../../tc/jobs/job.persist";
import { AppTranslation } from "../../../app.translation";
import { MenuState } from 'src/app/global-state-manager/global-state';


import { OrdersDetail } from "../orders.model";
import { OrdersPersist } from "../orders.persist";
import { OrdersNavigator } from "../orders.navigator";
import { tabs } from 'src/app/app.enums';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Form_EncounterPersist } from '../../form_encounter.persist';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { PatientDetail, PatientSummary } from 'src/app/patients/patients.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-orders-detail',
  templateUrl: './orders-detail.component.html',
  styleUrls: ['./orders-detail.component.css']
})
export class OrdersDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  ordersLoading: boolean = false;

  ordersTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;

  labResultTotalCount: number = 0;
  selectedTabIndex: number = 1;
  //basics
  ordersDetail: OrdersDetail;
  patientDetail: PatientDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public ordersNavigator: OrdersNavigator,
    public menuState: MenuState,
    public tcUtilsDate: TCUtilsDate,
    public doctorPersist: DoctorPersist,
    public patientPersist: PatientPersist,
    public form_encounterPersist: Form_EncounterPersist,
    public ordersPersist: OrdersPersist,
    public tcUtilsString:TCUtilsString,
    public outsidePersist: Outside_OrdersPersist) {
      if (!(this.tcAuthorization.canRead("laboratory_orders") || this.tcAuthorization.canRead("radiology_orders") || this.tcAuthorization.canRead("pathology_orders"))){
        this.tcNotification.error("You have no access")
        this.location.back()
      }
    this.ordersDetail = new OrdersDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    // if (this.tcAuthorization.canRead(this.tcUtilsArray.getEnum(this.ordersPersist.)))
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.ordersLoading = true;
    this.ordersPersist.getOrders(id).subscribe(ordersDetail => {
      this.ordersDetail = ordersDetail;
      this.ordersLoading = false;
      if(this.ordersDetail.doctor_id == this.tcUtilsString.invalid_id){
        this.outsidePersist.getOutside_Orders(this.ordersDetail.encounter_id).subscribe(outsideOrder => {
          this.ordersDetail.patient_full_name = outsideOrder.patient_name
        })
      }else {
        this.doctorPersist.getDoctor(this.ordersDetail.doctor_id).subscribe((doctor: DoctorDetail) => {
          this.ordersDetail.doctor_name = doctor.first_name + " " + doctor.last_name;
        });
        this.patientPersist.getPatient(this.ordersDetail.patient_id).subscribe((patient: PatientSummary) => {
          this.ordersDetail.patient_full_name = patient.fname + " " + patient.lname;
          this.patientDetail = patient;
        });
      }
      this.menuState.getDataResponse(this.ordersDetail);
    }, error => {
      console.error(error);
      this.ordersLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.ordersDetail.id);
  }

  editOrders(): void {
    let modalRef = this.ordersNavigator.editOrders(this.ordersDetail.id);
    modalRef.afterClosed().subscribe(modifiedOrdersDetail => {
      TCUtilsAngular.assign(this.ordersDetail, modifiedOrdersDetail);
    }, error => {
      console.error(error);
    });
  }

  printOrders(): void {
    this.ordersPersist.print(this.ordersDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print orders", true);
    });
  }

  back(): void {
    this.location.back();
  }

  onLabResult(count: number) {
    this.labResultTotalCount = count;
  }

  onTabChange(event=null){
    if (event!=null){
      
    }else {}
  }

}

