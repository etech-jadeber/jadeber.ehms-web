import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification}  from "../../../tc/notification";
import {TCUtilsString}   from "../../../tc/utils-string";
import {AppTranslation} from "../../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../../tc/models";


import {OrdersDetail} from "../orders.model";
import {OrdersPersist} from "../orders.persist";
import { PaymentNavigator } from 'src/app/payments/payment.navigator';
import { PaymentSummary } from 'src/app/payments/payment.model';


@Component({
  selector: 'app-orders-edit',
  templateUrl: './orders-edit.component.html',
  styleUrls: ['./orders-edit.component.css']
})
export class OrdersEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  ordersDetail: OrdersDetail;
  payment: string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OrdersEditComponent>,
              public  persist: OrdersPersist,
              public paymentNavigator: PaymentNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("procedure", "order");
      this.ordersDetail = new OrdersDetail();
      this.ordersDetail.paid = false;
      //set necessary defaults

    } else {
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("procedure", "order");
      this.isLoadingResults = true;
      this.persist.getOrders(this.idMode.id).subscribe(ordersDetail => {
        this.ordersDetail = ordersDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addOrders(this.idMode.id, this.ordersDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("procedure", "order") + " " + this.appTranslation.getText("general", "added"));
      this.ordersDetail.id = value.id;
      this.dialogRef.close(this.ordersDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateOrders(this.ordersDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("procedure", "order") + " " + this.appTranslation.getText("general", "updated"));
      this.dialogRef.close(this.ordersDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  searchPayment() {
    let dialogRef = this.paymentNavigator.pickPayments(true);
    dialogRef.afterClosed().subscribe((result: PaymentSummary) => {
      if (result) {
        this.payment = result[0].crv;
        this.ordersDetail.payment_id = result[0].id;
      }
    });
  }


  canSubmit():boolean{
        if (this.ordersDetail == null){
            return false;
          }
        if (!this.ordersDetail.payment_id){
          return false;
        }
        if (!this.ordersDetail.total_order){
          return false;
        }
        if (!this.ordersDetail.total_price){
          return false;
        }
        


        return true;
      }


}
