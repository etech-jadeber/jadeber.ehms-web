import {Component, OnInit, ViewChild, Input, EventEmitter, Output, ViewChildren, QueryList} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";

import { OrdersPersist } from '../orders.persist';
import {OrdersNavigator} from "../orders.navigator";
import {OrdersDetail, OrdersSummary, OrdersSummaryPartialList} from "../orders.model";
import { PatientSummary, PatientSummaryPartialList, PatientDetail } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { SampleCollectionPersist } from 'src/app/sample_collection/sample_collection.persist';
import { TCId } from 'src/app/tc/models';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { interval } from 'rxjs';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { procedure_for } from 'src/app/app.enums';



@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {

  labOrderssData: OrdersSummary[] = [];
  labOrderssTotalCount: number = 0;
  labOrderssSelectAll:boolean = false;
  labOrderssSelection: OrdersSummary[] = [];
  procedureFor = procedure_for

  labOrderssDisplayedColumns: string[] = ["action", "patient_id","future_date","total_order", "completed", "rejected", "requested", ];
  labOrderssSearchTextBox: FormControl = new FormControl();
  labOrderssIsLoading: boolean = false;
  paginatorEnum: {[id: string]: number} = {};

  radOrderssData: OrdersSummary[] = [];
  radOrderssTotalCount: number = 0;
  radOrderssSelectAll:boolean = false;
  radOrderssSelection: OrdersSummary[] = [];

  radOrderssDisplayedColumns: string[] = ["action", "patient_id","future_date","total_order", "completed", "rejected", "requested", ];
  radOrderssSearchTextBox: FormControl = new FormControl();
  radOrderssIsLoading: boolean = false;

  patOrderssData: OrdersSummary[] = [];
  patOrderssTotalCount: number = 0;
  patOrderssSelectAll:boolean = false;
  patOrderssSelection: OrdersSummary[] = [];

  patOrderssDisplayedColumns: string[] = ["action", "patient_id","future_date","total_order", "completed", "rejected", "requested", ];
  patOrderssSearchTextBox: FormControl = new FormControl();
  patOrderssIsLoading: boolean = false;
  selectedTabIndex: number = 0;

  unknown_state = -1;
  queued_state = 1;
  running_state = 2;
  success_state = 3;
  failed_state = 4;
  aborted_state = 5;
  message:string = '';
  originalEncounterId: string;

  @Input() role: number;
  @Input() encounterId: string;
  @Input() patient_id: string;
  @Input() edit: boolean=true;
  @Input() sample_display: boolean = false;
  @Output() handleSelectedOrder = new EventEmitter<OrdersDetail>();

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public ordersPersist: OrdersPersist,
                public ordersNavigator: OrdersNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public sampleCollectionPersist: SampleCollectionPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
    ) {

      if(!(this.tcAuthorization.canRead('laboratory_orders') || this.tcAuthorization.canRead('radiology_orders') || this.tcAuthorization.canRead('pathology_orders'))){
        tcNavigator.home()
    }
       this.labOrderssSearchTextBox.setValue(ordersPersist.resultSearchHistory.search_text);
      //delay subsequent keyup events
      this.labOrderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.resultSearchHistory.search_text = value;
        this.searchLabOrderss();
      });
      // this.radOrderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      //   this.ordersPersist.ordersSearchText = value;
      //   this.searchLabOrderss();
      // });
    }

    ngOnInit() {
      if (this.sample_display){
      this.labOrderssDisplayedColumns.splice(3, 0, 'sample_collection')
    }
    this.originalEncounterId = this.encounterId;
    if (this.patient_id){
      this.encounterId = null;
    }
      let index = 0;
      if(this.tcAuthorization.canRead('laboratory_orders'))
       {this.paginatorEnum.lab_order = index++}
       if(this.tcAuthorization.canRead('radiology_orders'))
       {this.paginatorEnum.rad_order = index++}
       if(this.tcAuthorization.canRead('pathology_orders'))
       {this.paginatorEnum.pat_order = index++}
      // start by loading items
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.encounterId = this.originalEncounterId;
      } else {
        this.encounterId = null;
      }
      this.onTabChange()
    }

    ngAfterViewInit(){
      this.sorters.toArray()[this.paginatorEnum.lab_order]?.sortChange.subscribe(() => {
        this.paginators.toArray()[this.paginatorEnum.lab_order].pageIndex = 0;
        this.searchLabOrderss(true);
      });

      this.paginators.toArray()[this.paginatorEnum.lab_order]?.page.subscribe(() => {
        this.searchLabOrderss(true);
      });
      if (this.tcAuthorization.canRead('laboratory_orders')){
        this.searchLabOrderss()
      }
      else if (this.tcAuthorization.canRead('radiology_orders')){
        this.searchRadOrders()
      }
      else if (this.tcAuthorization.canRead('pathology_orders')){
        this.searchPatOrders()
      }
        this.sorters.toArray()[this.paginatorEnum.rad_order]?.sortChange.subscribe(() => {
          this.paginators.toArray()[this.paginatorEnum.rad_order].pageIndex = 0;
          this.searchRadOrders(true);
        });
  
        this.paginators.toArray()[this.paginatorEnum.rad_order]?.page.subscribe(() => {
          this.searchRadOrders(true);
        });

        this.sorters.toArray()[this.paginatorEnum.pat_order]?.sortChange.subscribe(() => {
          this.paginators.toArray()[this.paginatorEnum.pat_order].pageIndex = 0;
          this.searchRadOrders(true);
        });
  
        this.paginators.toArray()[this.paginatorEnum.pat_order]?.page.subscribe(() => {
          this.searchRadOrders(true);
        });
      
    }

  searchLabOrderss(isPagination:boolean = false): void {


    let paginator = this.paginators.toArray()[this.paginatorEnum.lab_order];
    let sorter = this.sorters.toArray()[this.paginatorEnum.lab_order];

    this.labOrderssIsLoading = true;
    this.labOrderssSelection = [];

    this.ordersPersist.orderssDo("get_lab_order", {encounter_id: this.encounterId, patient_id: this.patient_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc').subscribe(
      (labs:OrdersSummaryPartialList)=>{
        this.labOrderssData = labs.data
        if (this.sample_display){
          this.labOrderssData.forEach(
            order => {
              this.sampleCollectionPersist.sampleCollectionsDo("sample_to_be_collected", {order_id: order.id}).subscribe(
                (sample: {remaining: number}) => {
                  order.sample_to_be_collected = sample.remaining;
                }
              )
            }
          )
        }
        if (labs.total != -1){
          this.labOrderssTotalCount = labs.total;
        }
        this.labOrderssIsLoading = false;
      }, error => {
        this.labOrderssIsLoading = false;
        console.log(error)
      }
    )

  }

  printResult(item: OrdersDetail){

    this.ordersPersist.ordersDo(item.id, "print_result", item).subscribe((downloadJob: TCId) => {

      this.jobPersist.followJob(downloadJob.id, "printing Result", true);
      let success_state = 3;
      this.jobPersist.getJob(downloadJob.id).subscribe(
        job => {
          if (job.job_state == success_state) {
          }
        }

      )
    }, error => {
    })
  }

  searchRadOrders(isPagination: boolean = false): void{
    let paginator = this.paginators.toArray()[this.paginatorEnum.rad_order];
    let sorter = this.sorters.toArray()[this.paginatorEnum.rad_order];

    this.radOrderssIsLoading = true;
    this.radOrderssSelection = [];

    this.ordersPersist.orderssDo("get_rad_order", {encounter_id: this.encounterId, patient_id: this.patient_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc').subscribe(
      (rads: OrdersSummaryPartialList) => {
        this.radOrderssData = rads.data
        if (rads.total != -1){
          this.radOrderssTotalCount = rads.total;
        }
        this.radOrderssIsLoading = false;
      }, error => {
        this.labOrderssIsLoading = false;
        console.log(error)
      }
    )
  }

  searchPatOrders(isPagination: boolean = false): void{
    let paginator = this.paginators.toArray()[this.paginatorEnum.pat_order];
    let sorter = this.sorters.toArray()[this.paginatorEnum.pat_order];

    this.patOrderssIsLoading = true;
    this.patOrderssSelection = [];

    this.ordersPersist.orderssDo("get_pat_order", {encounter_id: this.encounterId, patient_id: this.patient_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc').subscribe(
      (pats: OrdersSummaryPartialList) => {
        this.patOrderssData = pats.data
        if (pats.total != -1){
          this.patOrderssTotalCount = pats.total;
        }
        this.patOrderssIsLoading = false;
      }, error => {
        this.labOrderssIsLoading = false;
        console.log(error)
      }
    )
  }

  downloadOrderss(): void {
    if(this.labOrderssSelectAll){
         this.ordersPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download orderss", true);
      });
    }
    else{
        this.ordersPersist.download(this.tcUtilsArray.idsList(this.labOrderssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download orderss",true);
            });
        }
  }



  addOrders(): void {
    let dialogRef = this.ordersNavigator.addOrders(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLabOrderss();
      }
    });
  }

  editOrders(item: OrdersSummary) {
    let dialogRef = this.ordersNavigator.editOrders(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteOrders(item: OrdersSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Orders");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.ordersPersist.deleteOrders(item.id).subscribe(response => {
          this.tcNotification.success("Orders deleted");
          this.searchLabOrderss();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

    onTabChange(event=null){
      if (event != null){
        this.selectedTabIndex = event;
      }
      if(this.paginatorEnum.lab_order != null && this.paginatorEnum.lab_order == this.selectedTabIndex){
        this.searchLabOrderss()
      }
       else if (this.paginatorEnum.rad_order != null &&  this.paginatorEnum.rad_order == this.selectedTabIndex) {
        this.searchRadOrders()
      }
      else if (this.paginatorEnum.lab_order != null && this.paginatorEnum.pat_order == this.selectedTabIndex){
        this.searchPatOrders()
      }
    }

}

