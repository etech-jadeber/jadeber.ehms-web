import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrdersPickComponent } from './orders-pick.component';

describe('OrdersPickComponent', () => {
  let component: OrdersPickComponent;
  let fixture: ComponentFixture<OrdersPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OrdersPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
