import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {OrdersDetail, OrdersSummary, OrdersSummaryPartialList} from "../orders.model";
import {OrdersPersist} from "../orders.persist";


@Component({
  selector: 'app-orders-pick',
  templateUrl: './orders-pick.component.html',
  styleUrls: ['./orders-pick.component.css']
})
export class OrdersPickComponent implements OnInit {

  orderssData: OrdersSummary[] = [];
  orderssTotalCount: number = 0;
  orderssSelection: OrdersSummary[] = [];
  orderssDisplayedColumns: string[] = ["select", 'created_at','total_order','doctor_id','encounter_id','patient_id','urgency','total_price','payment_id','paid' ];

  orderssSearchTextBox: FormControl = new FormControl();
  orderssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) orderssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderssSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public ordersPersist: OrdersPersist,
              public dialogRef: MatDialogRef<OrdersPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.orderssSearchTextBox.setValue(ordersPersist.orderSearchHistory.search_text);
    //delay subsequent keyup events
    this.orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.ordersPersist.orderSearchHistory.search_text = value;
      this.searchOrderss();
    });
  }

  ngOnInit() {

    this.orderssSort.sortChange.subscribe(() => {
      this.orderssPaginator.pageIndex = 0;
      this.searchOrderss();
    });

    this.orderssPaginator.page.subscribe(() => {
      this.searchOrderss();
    });

    //set initial picker list to 5
    this.orderssPaginator.pageSize = 5;

    //start by loading items
    this.searchOrderss();
  }

  searchOrderss(): void {
    this.orderssIsLoading = true;
    this.orderssSelection = [];

    this.ordersPersist.searchOrders("all", this.orderssPaginator.pageSize,
        this.orderssPaginator.pageIndex,
        this.orderssSort.active,
        this.orderssSort.direction).subscribe((partialList: OrdersSummaryPartialList) => {
      this.orderssData = partialList.data;
      if (partialList.total != -1) {
        this.orderssTotalCount = partialList.total;
      }
      this.orderssIsLoading = false;
    }, error => {
      this.orderssIsLoading = false;
    });

  }

  markOneItem(item: OrdersSummary) {
    if(!this.tcUtilsArray.containsId(this.orderssSelection,item.id)){
          this.orderssSelection = [];
          this.orderssSelection.push(item);
        }
        else{
          this.orderssSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.orderssSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
