import { Lab_PanelSummary } from "src/app/lab_panels/lab_panel.model";
import { ExtendedLab_TestSummary, Lab_TestSummary } from "src/app/lab_tests/lab_test.model";
import { PathologyResultDetail } from "src/app/pathology_result/pathology_result.model";
import { RadiologyResultDetail } from "src/app/radiology_result/radiology_result.model";
import { SampleCollectionSummary } from "src/app/sample_collection/sample_collection.model";
import {TCId, TCString} from "../../tc/models";
import { Lab_ResultDetail } from "./lab_results/lab_result.model";

export const orderFilterColumn: TCString[] = [
  new TCString( "p.pid", 'MRN'),
  new TCString( "patient_name", 'Patient Name'),
  new TCString( "phone_cell", 'Phone Number'),
  new TCString( "generated_sample_id", 'Sample ID'),
  ];

export class OrdersSummary extends TCId {
  created_at : number;
total_order : number;
doctor_id : string;
encounter_id : string;
patient_id : string;
urgency : number;
total_price : number;
payment_id : string;
paid : boolean;
order_status : number;
future : boolean;
future_date : number;
order_type : number;
doctor_name: string;
patient_name: string;
outside_id:string;
generated_sample_id: string;
patient_type: number;
sample_to_be_collected: number;
sample_collection: SampleCollectionSummary
selected_orders?: Selected_OrdersSummary[]
  is_paid: boolean;
  provider_id: string;
}

export class CommonOrdersDetail {
  future: boolean;
  futureDate: number;
  urgency: number;
  to_be_resulted_by?: string;
  encounter_id: string;
  selectedLabTest: ExtendedLab_TestSummary[];
  selectedLabPanel: Lab_PanelSummary[];
}

export class OrdersSummaryPartialList {
  data: OrdersSummary[];
  total: number;
}

export class OrdersDetail extends OrdersSummary {
  created_at : number;
total_order : number;
doctor_id : string;
encounter_id : string;
patient_id : string;
urgency : number;
total_price : number;
payment_id : string;
paid : boolean;
order_status : number;
future : boolean;
future_date : number;
order_type : number;
generated_sample_id: string;
sample_generated_date: number;
to_be_resulted_by: string;
patient_full_name: string;
note: string;
}

export class OrdersDashboard {
  approved: number;
  available_tests: number;
  awaiting_approval: number;
  total: number;
}

export class Selected_OrdersSummary extends TCId {
  order_id : string;
lab_order_id : string;
lab_test_id : string;
urgency : number;
comment : string;
panel_name:string;
test_name:string;
lab_panel_id : string;
generated_sample_id : number;
result : string;
result_by : string;
confirmed_by : string;
scanned_document_id : string;
lab_test_name: string;
lab_panel_name: string;
lab_order_name: string;
order_name: string;
can_approve: boolean;
status: number;
min_range: number;
max_range: number;
result_id: string;
result_date: number;
result_by_full_name: string;
is_file_uploaded: boolean;
result_description: string;
result_conclusion: string;
reject_description: string;
result_file_id: string;
key: string;
style: string;
clinical_finding: string;
conclusion: string;
gross_examination: string;
microscopic_evaluation: string;
selected_order_id: string;
abnormal_result: string;
normal_result: string;
  pathology_result: PathologyResultDetail;
  radiology_result: RadiologyResultDetail;
  confirm_by: string;
  test_unit_id: string;
  payment_id: string;
  is_paid: boolean;
  tat_time: number;
  tat_style: string;
  company_id: string;
  order_type: number;
  sample_generated_date: number;
  lab_result: Lab_ResultDetail;
  reference_range: string;
}

export class Selected_OrdersSummaryPartialList {
  data: Selected_OrdersSummary[];
  total: number;
}

export class Selected_OrdersDetail extends Selected_OrdersSummary {
  order_id : string;
lab_order_id : string;
lab_test_id : string;
urgency : number;
comment : string;
lab_panel_id : string;
generated_sample_id : number;
result : string;
result_by : string;
confirmed_by : string;
scanned_document_id : string;
lab_test_name: string;
lab_panel_name: string;
lab_order_name: string;
can_approve: boolean;
status: number;
result_id: string;
min_range: number;
max_range: number;
result_date: number;
result_by_full_name: string;
result_file_id: string;
key: string;
tat_time: number;
  tat_style: string;
}

export class ResultDescriptionDetail{
  result_description: string;
  result_conclusion: string;
  reject_description: string;
}

export class SampleCollectionDetail{
  date_of_collection: number

}

export class SelectedOrdersDashboardInfo{
  outpatient: number;
  inpatient: number;
  emergency: number;
  referral: number;
  outside_order: number;
  sex: string;
  order_type: number;
}
