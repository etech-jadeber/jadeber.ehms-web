import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";

import {OrdersEditComponent} from "./orders-edit/orders-edit.component";
import {OrdersPickComponent} from "./orders-pick/orders-pick.component";
import { Selected_OrdersSummary } from "./orders.model";
import { ResultDescriptionEditComponent } from "src/app/orderss/result-description-edit/result-description-edit.component";
import { Lab_ResultDetail } from "./lab_results/lab_result.model";


@Injectable({
  providedIn: 'root'
})

export class OrdersNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  orderssUrl(): string {
    return "/orders";
  }

  ordersUrl(id: string): string {
    return "/orders/" + id;
  }

  viewOrderss(): void {
    this.router.navigateByUrl(this.orderssUrl());
  }

  viewOrders(id: string): void {
    this.router.navigateByUrl(this.ordersUrl(id));
  }

  labOrderssUrl(): string {
    return "/laboratory_orders";
  }

  labOrdersUrl(id: string): string {
    return "/laboratory_orders/" + id;
  }

  labResultApprovalUrl(): string {
    // return "/lab-results-approval";
    return "/laboratory_result_approvals"
  }

  viewLabOrderss(): void {
    this.router.navigateByUrl(this.labOrderssUrl());
  }

  viewLabOrders(id: string): void {
    this.router.navigateByUrl(this.labOrdersUrl(id));
  }

  viewLabResultApprovalUrl(): void {
    this.router.navigateByUrl(this.labResultApprovalUrl());
  }




  radOrderssUrl(): string {
    return "/radiology_orders";
  }

  radResultApprovalUrl(): string {
    return "/radiology_result_approvals"
  }

  radOrdersUrl(id: string): string {
    return "/radiology_orders/" + id;
  }

  patResultApprovalUrl(): string {
    return "/pathology_result_approvals"
  }

  viewRadOrderss(): void {
    this.router.navigateByUrl(this.radOrderssUrl());
  }

  viewRadOrders(id: string): void {
    this.router.navigateByUrl(this.radOrdersUrl(id));
  }

  viewPatResultApprovalUrl(): void {
    this.router.navigateByUrl(this.patResultApprovalUrl())
  }

  viewRadResultApprovalUrl(): void {
    this.router.navigateByUrl(this.radResultApprovalUrl());
  }

  addResult(parentId:string, order: Selected_OrdersSummary, reject:boolean = false, pathology:boolean = false): MatDialogRef<unknown, any>{
    const dialogRef = this.dialog.open(ResultDescriptionEditComponent, {
      data: {parentId: parentId, childId: order, context: {reject, pathology, mode: TCModalModes.NEW}},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  showResult(parentId:string, order: Lab_ResultDetail, reject:boolean = false, pathology:boolean = false): MatDialogRef<unknown, any>{
    const dialogRef = this.dialog.open(ResultDescriptionEditComponent, {
      data: {parentId: parentId, childId: order, context: {reject, pathology, mode: TCModalModes.WIZARD}},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  editOrders(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OrdersEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOrders(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OrdersEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickOrderss(id: string, selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(OrdersPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
