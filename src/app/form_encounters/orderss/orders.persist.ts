import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../../tc/models";
import {OrdersDashboard, OrdersDetail, OrdersSummaryPartialList, SelectedOrdersDashboardInfo, Selected_OrdersDetail, Selected_OrdersSummaryPartialList} from "./orders.model";
import {
  lab_order_type,
  lab_order_status,
  lab_urgency,
  selected_orders_status,
  OrderStatus
} from "../../app.enums"
import { TCUtilsString } from '../../tc/utils-string';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from '../../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class OrdersPersist {

  // resultDate: number;
  // orderedStartDate: number;
  // orderedEndDate: number;
  // // show_only_mine: Boolean = false;
  // rad_order_id: string;
  // rad_order_name: string;
  // rad_order_start_date: number = this.tcUtilsDate.toTimeStamp(new Date())
  // rad_order_end_date: number
  // rad_order_status: number = OrderStatus.not_completed

  // lab_order_persist_id: string;
  // lab_order_name: string;
  // lab_order_start_date: number = this.tcUtilsDate.toTimeStamp(new Date())
  // lab_order_end_date: number = this.tcUtilsDate.toTimeStamp(new Date())
  // lab_order_persist_status: number = OrderStatus.not_completed

  // ordersSearchText: string = "";
  // lab_order_typeId: number ;

    lab_order_type: TCEnum[] = [
    new TCEnum(lab_order_type.imaging, 'imaging'),
    new TCEnum(lab_order_type.lab, 'lab'),
    new TCEnum(lab_order_type.pathology, 'pathology')
  ];

  lab_order_acl: TCEnum[] = [
    new TCEnum(lab_order_type.imaging, 'radiology_orders'),
    new TCEnum(lab_order_type.lab, 'laboratory_orders'),
    new TCEnum(lab_order_type.pathology, 'pathology_orders')
  ]

  lab_result_acl: TCEnum[] = [
    new TCEnum(lab_order_type.imaging, 'radiology_results'),
    new TCEnum(lab_order_type.lab, 'laboratory_results'),
    new TCEnum(lab_order_type.pathology, 'pathology_results')
  ]

  lab_order_statuId: number ;

    lab_order_status: TCEnum[] = [
    new TCEnum(lab_order_status.ordered, 'ordered'),
    new TCEnum(lab_order_status.started, 'started'),
    new TCEnum(lab_order_status.done, 'done'),
  ];

  lab_urgencyId: number ;

    lab_urgency: TCEnum[] = [
    new TCEnum(lab_urgency.normal, 'normal'),
    new TCEnum(lab_urgency.urgent, 'urgency'),
    new TCEnum(lab_urgency.critical, 'critical'),
  ];

  selected_order: TCEnum[] = [
    new TCEnum(selected_orders_status.ready_to_approve, 'ready to approve'),
    new TCEnum(selected_orders_status.approved, 'approved'),
    new TCEnum(selected_orders_status.rejected, 'rejected'),
    new TCEnum(selected_orders_status.submitted, 'submitted'),
  ];

  // orderStatusFilter: number;

    OrderStatus: TCEnum[] = [
     new TCEnum( OrderStatus.completed, 'completed'),
  new TCEnum( OrderStatus.not_completed, 'not_completed'),

  ];

  LabOrderStatus: TCEnum[] = [
    ...this.OrderStatus
  ];

  constructor(private http: HttpClient, private tcUtilsDate: TCUtilsDate) {
  }

  searchOrders(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.orderSearchHistory): Observable<OrdersSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" +  parentId + "/orders", searchHistory.search_text, pageSize, pageIndex, sort, order);
    return this.http.get<OrdersSummaryPartialList>(url);

  }

  filters(searchHistory = this.orderSearchHistory): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "orders/do", new TCDoParam("download_all", this.filters()));
  }

  ordersDashboard(): Observable<OrdersDashboard[]> {
    return this.http.get<OrdersDashboard[]>(environment.tcApiBaseUri + "orders/dashboard");
  }

  getOrders(id: string): Observable<OrdersDetail> {
    return this.http.get<OrdersDetail>(environment.tcApiBaseUri + "orders/" + id);
  }

  getStudies(sample_id: string): Observable<OrdersDetail> {
    console.log(environment.dicom_url + "orthanc/dicom-web/studies?AccessionNumber=" + sample_id)
    return this.http.get<OrdersDetail>(environment.dicom_url + "orthanc/dicom-web/studies?AccessionNumber=" + sample_id);
  }

  addOrders(parentId: string, item: OrdersDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/orders", item);
  }

  updateOrders(item: OrdersDetail): Observable<OrdersDetail> {
    return this.http.patch<OrdersDetail>(environment.tcApiBaseUri + "orders/" + item.id, item);
  }

  deleteOrders(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "orders/" + id);
  }
  orderssDoExcel(method: string, payload: any): Observable<any> {
    return this.http.post<any>(environment.tcApiBaseUri + "orders/do", new TCDoParam(method, payload));
  }

  patOrderSearchHistory = {
    "order_status": OrderStatus.not_completed,
    "ordered_start_date": this.tcUtilsDate.toTimeStamp(new Date()),
    "ordered_end_date": this.tcUtilsDate.toTimeStamp(new Date()),
    "result_date": undefined,
    "show_only_mine": undefined,
    "search_text": "",
    "search_column": "",
}


defaultPatOrderSearchHistory = {...this.patOrderSearchHistory}

resetPatOrderSearchHistory(){
  this.patOrderSearchHistory = {...this.defaultPatOrderSearchHistory}
}

orderSearchHistory = {
  "order_status": undefined,
  "ordered_start_date": undefined,
  "ordered_end_date": undefined,
  "result_date": undefined,
  "show_only_mine": undefined,
  "search_text": "",
  "search_column": "",
}

defaultOrderSearchHistory = {...this.orderSearchHistory}

resetOrderSearchHistory(){
this.orderSearchHistory = {...this.defaultOrderSearchHistory}
}

resultSearchHistory = {
  "order_status": undefined,
  "ordered_start_date": undefined,
  "ordered_end_date": undefined,
  "result_date": undefined,
  "show_only_mine": undefined,
  "search_text": "",
  "search_column": "",
}

defaultResultSearchHistory = {...this.resultSearchHistory}

resetResultSearchHistory(){
this.resultSearchHistory = {...this.defaultResultSearchHistory}
}

labOrderSearchHistory = {
  "order_status": OrderStatus.not_completed,
  "ordered_start_date": this.tcUtilsDate.toTimeStamp(new Date()),
  "ordered_end_date": this.tcUtilsDate.toTimeStamp(new Date()),
  "result_date": undefined,
  "show_only_mine": undefined,
  "search_text": "",
  "search_column": "",
  "lab_order_id": "",
  "lab_order_name": "",
  "panel_name": "",
  "test_name": "",
  "lab_panel_id": '',
  "lab_test_id": "",
}

defaultLabOrderSearchHistory = {...this.labOrderSearchHistory}

resetLabOrderSearchHistory(){
this.labOrderSearchHistory = {...this.defaultLabOrderSearchHistory}
}

radOrderSearchHistory = {
  "order_status": OrderStatus.not_completed,
  "ordered_start_date": this.tcUtilsDate.toTimeStamp(new Date()),
  "ordered_end_date": this.tcUtilsDate.toTimeStamp(new Date()),
  "result_date": undefined,
  "show_only_mine": undefined,
  "search_text": "",
  "search_column": "",
  "rad_order_id": "",
  "rad_order_name": "",
  "panel_name": "",
  "test_name": "",
  "lab_panel_id": '',
  "lab_test_id": "",

}

defaultRadOrderSearchHistory = {...this.radOrderSearchHistory}

resetRadOrderSearchHistory(){
this.radOrderSearchHistory = {...this.defaultRadOrderSearchHistory}
}

tatReportSearchHistory = {
  "order_status": undefined,
  "ordered_start_date": undefined,
  "ordered_end_date": undefined,
  "result_date": undefined,
  "show_only_mine": undefined,
  "search_text": "",
  "search_column": "",
  "order_type": lab_order_type.lab,
  "patient_type": undefined,
}

defaultTatReportSearchHistory = {...this.tatReportSearchHistory}

resetTatReportSearchHistory(){
this.tatReportSearchHistory = {...this.defaultTatReportSearchHistory}
}

  orderssDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.orderSearchHistory): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("orders/do", searchHistory.search_text, pageSize, pageIndex, sort, order)
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.orderStatusFilter){
    //   url = TCUtilsString.appendUrlParameter(url, "order_status", this.orderStatusFilter.toString())
    // }
    // if (this.orderedStartDate){
    //   url = TCUtilsString.appendUrlParameter(url, "ordered_start_date", this.orderedStartDate.toString())
    // }
    // if (this.orderedEndDate){
    //   url = TCUtilsString.appendUrlParameter(url, "ordered_end_date", this.orderedEndDate.toString())
    // }
    // if (this.resultDate){
    //   url = TCUtilsString.appendUrlParameter(url, "result_date", this.resultDate.toString())
    // }
    // url = TCUtilsString.appendUrlParameter(url, "show_only_mine", String(this.show_only_mine))
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  ordersDo(id: string, method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): Observable<any> {
    let url = TCUtilsHttp.buildSearchUrl( "orders/" + id + "/do", this.selected_ordersSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<any>(url, new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "orders/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "orders/" + id + "/do", new TCDoParam("print", {}));
  }
  selected_ordersSearchText: string = "";
  lab_order_id: string = "";

  selectedOrdersDashboard(): Observable<SelectedOrdersDashboardInfo[]> {
    return this.http.get<SelectedOrdersDashboardInfo[]>(environment.tcApiBaseUri + "selected_orders/dashboard");
  }


  searchSelected_Orders(parentId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Selected_OrdersSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "orders/" + parentId+  "/selected_orders/", this.selected_ordersSearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "lab_order_id", this.lab_order_id);
    return this.http.get<Selected_OrdersSummaryPartialList>(url);

  }

  getSelected_Orders(parentId:string, id: string): Observable<Selected_OrdersDetail> {
      return this.http.get<Selected_OrdersDetail>(environment.tcApiBaseUri + "orders/" + parentId+  "/selected_orders/" + id);
   }

   getSelected_Order(id: string): Observable<Selected_OrdersDetail> {
    return this.http.get<Selected_OrdersDetail>(environment.tcApiBaseUri + "selected_orders/" + id);
 }


  addSelected_Orders(item: {lab_panel_id: string, lab_test_id: string, order_id: string, lab_order_id: string}): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri +  "selected_orders", item);
  }

  updateSelected_Orders(parentId:string, item: Selected_OrdersDetail): Observable<Selected_OrdersDetail> {
    return this.http.patch<Selected_OrdersDetail>(environment.tcApiBaseUri + "orders/" + parentId+ "/selected_orders/" + item.id, item);
  }

  deleteSelected_Orders(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri +  "selected_orders/" + id);
  }

  selected_orderssDo(parentId:string, method: string, payload: any, searchText?: string, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): Observable<TCId | Selected_OrdersSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl( "orders/" + parentId+ "/selected_orders/do" , searchText , pageSize, pageIndex, sort, order);
    return this.http.post<TCId | Selected_OrdersSummaryPartialList>(url, new TCDoParam(method, payload));
  }

  selected_ordersDo( id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>( environment.tcApiBaseUri + "selected_orders/" + id + "/do", new TCDoParam(method, payload));
  }

  documentUploadUri(selecteID: string) {
    return environment.tcApiBaseUri + 'selected_orders/' + selecteID + "/selected-upload/";
  }

  dicomUploadUrl(rad_order_id: string){
    return environment.tcApiBaseUri + 'radiology_data/' + rad_order_id + "/dicom_data";
  }

  dicomDatasDo(parentId: string, method: string, payload: any): Observable<{}>{
    return this.http.post<{}>(environment.tcApiBaseUri + "radiology_data/" + parentId + "/dicom_data/do", new TCDoParam(method, payload));
  }

}
