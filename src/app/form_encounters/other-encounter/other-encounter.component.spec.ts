import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherEncounterComponent } from './other-encounter.component';

describe('OtherEncounterComponent', () => {
  let component: OtherEncounterComponent;
  let fixture: ComponentFixture<OtherEncounterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherEncounterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherEncounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
