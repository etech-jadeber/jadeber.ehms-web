import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherFormEncountersListComponent } from './other-form-encounters-list.component';

describe('OtherFormEncountersListComponent', () => {
  let component: OtherFormEncountersListComponent;
  let fixture: ComponentFixture<OtherFormEncountersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherFormEncountersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherFormEncountersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
