import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from 'src/app/app.translation';
import { BiopsyRequestNavigator } from 'src/app/biopsy_request/biopsy_request.navigator';
import { BiopsyRequestPersist } from 'src/app/biopsy_request/biopsy_request.persist';
import { Consent_FormNavigator } from 'src/app/consent_forms/consent_form.navigator';
import { Consent_FormPersist } from 'src/app/consent_forms/consent_form.persist';
import { DepartmentPersist } from 'src/app/departments/department.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { HistoryNavigator } from 'src/app/historys/history.navigator';
import { HistoryPersist } from 'src/app/historys/history.persist';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';
import { Surgery_AppointmentPersist } from 'src/app/surgery_appointments/surgery_appointment.persist';
import { Surgery_TypePersist } from 'src/app/surgery_types/surgery_type.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Form_EncounterSummary, Form_EncounterSummaryPartialList } from '../form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { OrdersPersist } from '../orderss/orders.persist';
import { Location } from '@angular/common';
import { diagnosis_order, lab_order_type } from 'src/app/app.enums';
import {  DiagnosisSummary } from '../diagnosiss/diagnosis.model';
import { Router } from '@angular/router';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { DiagnosisPersist } from '../diagnosiss/diagnosis.persist';
import { PatientDetail } from 'src/app/patients/patients.model';

@Component({
  selector: 'app-other-form-encounters-list',
  templateUrl: './other-form-encounters-list.component.html',
  styleUrls: ['./other-form-encounters-list.component.scss']
})
export class OtherFormEncountersListComponent implements OnInit {

  lab_order_type = lab_order_type
  form_encountersIsLoading: boolean = false;
  form_encountersTotalCount: number = 0;
  form_encountersData: Form_EncounterSummary[] = [];
  form_encountersSelectAll: boolean = false;
  form_encountersSelection: Form_EncounterSummary[] = [];

  form_encountersDisplayedColumns: string[] = [
    'date',
    'reason',
    'sensitivity',
    'provider_id',
    'inpatient',
  ];
  form_encountersSearchTextBox: FormControl = new FormControl();
  value : boolean = false
  has_lab_result: boolean = true;
  has_rad_result: boolean = true;
  has_pat_result: boolean = true;
  has_progress_note: boolean = true;
  has_patient_history: boolean = true;
  @Input() patientDetail: PatientDetail;
  @Input() encounterId: string;
  @Input() parent_encounter_id: string = this.tcUtilsString.invalid_id;

  @ViewChild(MatPaginator, { static: true }) form_encountersPaginator: MatPaginator;
  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public patientNavigator: PatientNavigator,
    public jobPersist: JobPersist,
    public userPersist: UserPersist,
    public appTranslation: AppTranslation,
    public form_encounterNavigator: Form_EncounterNavigator,
    public form_encounterPersist: Form_EncounterPersist,
    public consetNav: Consent_FormNavigator,
    public filePersist: FilePersist,
    public surgery_appointmentPersist: Surgery_AppointmentPersist,
    public surgeryTypePersist: Surgery_TypePersist,
    public menuState: MenuState,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public historyPersist: HistoryPersist,
    public historyNavigator: HistoryNavigator,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    public lab_testPersist: Lab_TestPersist,
    public physiotherapy_typePersist: Physiotherapy_TypePersist,
    public consentFormPersist: Consent_FormPersist,
    public departmentPersist: DepartmentPersist,
    public biopsyRequestNavigator: BiopsyRequestNavigator,
    public tcUtilsString: TCUtilsString,
    public diagnosticPersist: DiagnosisPersist
  ) {
    this.value = this.form_encounterPersist.encounter_is_active;
    this.form_encounterPersist.encounter_is_active = false;
  }

  ngOnInit(): void {
    // this.form_encounterPersist.parent_encounter_id = this.parent_encounter_id;
    // this.form_encounterPersist.encounter_id = this.encounterId;
    // this.form_encounterPersist.patientId = this.patientDetail.id;
    // this.form_encounterPersist.show_only_mine = false;
    // this.form_encounterPersist.isPreviosVisit = true;
    // this.form_encounterPersist.encounterStatusFilter = null;
    // this.form_encounterPersist.date = null;
    this.form_encounterPersist.otherEncounterSearchHistory.parent_encounter_id = this.parent_encounter_id
    this.form_encounterPersist.otherEncounterSearchHistory.patientId = this.patientDetail.id
    this.form_encountersPaginator.page.subscribe(value => {
      this.searchForm_Encounters(true)
    })
    this.searchForm_Encounters()
  }


  primaryDiagnosis(diagnosis: DiagnosisSummary[]):DiagnosisSummary{
    let primary= diagnosis.filter((value)=> value.diagnosis_order==diagnosis_order.primary);
    return primary.length ? primary[0]: (diagnosis.length ? diagnosis[0]:null);
  }

  searchForm_Encounters(isPagination: boolean = false): void {
    this.tcAuthorization.requireRead('form_encounters');
    let paginator = this.form_encountersPaginator;

    this.form_encountersIsLoading = true;
    this.form_encountersSelection = [];

    this.form_encounterPersist
      .searchForm_Encounter(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
       "id",
        "asc",
        this.form_encounterPersist.otherEncounterSearchHistory
      )
      .subscribe(
        (partialList: Form_EncounterSummaryPartialList) => {
          this.form_encountersData = partialList.data;
          this.form_encountersData.forEach((form_encounter) => {
            this.historyPersist.searchHistory(form_encounter.id, 1, 0, 'date_of_history', 'desc').subscribe(
             (diagnosis) => {
              if (diagnosis.data){
                form_encounter.chief_compaint = diagnosis.data[0]?.chief_complaint
              }
            }
            )
          })

            if (partialList.total != -1) {
              this.form_encountersTotalCount = partialList.total;
            }
            this.form_encountersIsLoading = false;
          }, error => {
            this.form_encountersIsLoading = false;
          });
        }

        getEncounterDetail(id:string):void {
          this.form_encounterPersist.selectedTabIndex=null;
          this.value = undefined;
          this.router.navigate([this.form_encounterNavigator.form_encounterUrl(id)]);
      }
        back(): void {
          this.location.back();
        }

        hasLabResult(value: boolean){
          this.has_lab_result = value;
        }
        hasRadResult(value: boolean){
          this.has_rad_result = value;
        }
        hasPatResult(value: boolean){
          this.has_pat_result = value;
        }

        assignPatientVisit(value: boolean): void {
          this.has_patient_history = value;
        }

        assignProgress(value: boolean): void {
          this.has_progress_note = value;
        }

        ngOnDestroy():void{
            // this.form_encounterPersist.encounter_is_active = this.value;
            // this.form_encounterPersist.isPreviosVisit = false;
            // this.form_encounterPersist.encounter_id = undefined;
        }

}
