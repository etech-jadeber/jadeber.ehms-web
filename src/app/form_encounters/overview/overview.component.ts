import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { AppTranslation } from 'src/app/app.translation';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { History_DataSummary } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { environment } from 'src/environments/environment';
import { OrdersPersist } from '../orderss/orders.persist';
import { Form_EncounterDetail, Form_ObservationSummary, Form_SoapSummary, Form_VitalsSummary, PrescriptionsSummary } from '../form_encounter.model';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { Procedure_OrderDetail } from '../procedure_orders/procedure_order.model';
import { Procedure_OrderPersist } from '../procedure_orders/procedure_order.persist';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
   encounterIdRoute = this.route.snapshot.paramMap.get('id');

  @Input() encounterIdTemplate: any;
  @Input() encounterId: string;
  profileUrl = "";

  form_encounterLoading:boolean = true;
  form_observationLoading:boolean = true;
  form_encounterDetail: Form_EncounterDetail;
  form_vitalssData: Form_VitalsSummary[] = [];
  form_vitalssTotalCount: number = 0;
  form_soapsTotalCount:number = 0;
  patientAge: number;
  form_observationsData: Form_ObservationSummary[] = [];
  form_observationsTotalCount:number = 0;
  form_soapsData: Form_SoapSummary[] = [];
  topProcedureData: Procedure_OrderDetail;
  history_datasTotalCount:number = 0;
  prescriptionssTotalCount:number = 0;
  history_datasData: History_DataSummary[] = [];
  prescriptionssData: PrescriptionsSummary[] = [];
  ordersTotalCount: number = 0;
  ordersData: {orders: string, id: string}[] = [];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(public tcAuthorization: TCAuthorization, 
    public appTranslation:AppTranslation,
     public patientPersist:PatientPersist,
      public form_encounterPersist: Form_EncounterPersist,
      public tcUtilsDate:TCUtilsDate, 
      public tcUtilsArray:TCUtilsArray,
       private procedureOrderPersist:Procedure_OrderPersist,
       public ordersPersist: OrdersPersist,
       private route: ActivatedRoute,) { 
    this.tcAuthorization.requireRead('form_encounters')
  }
  calculateAge(dob: number) {
    const current = new Date().getFullYear()   
    this.patientAge = current - new Date(dob*1000).getFullYear()  
  }


  searchForm_Observations(isPagination: boolean = false): void {
    this.form_observationLoading = true;
    this.form_encounterPersist
      .searchForm_Observation(
        this.form_encounterDetail.id,
        50,
        0,
        "",
        ""
      )
      .subscribe(
        (response) => {
          this.form_observationsData = response.data;

          if (response.total != -1) {
            this.form_observationsTotalCount = response.total;
          }
          this.form_observationLoading = false;
        },
        (error) => {
          this.form_observationLoading = false;
        }
      );
  }

  searchOrdersName(isPagination: boolean = false): void {
    const paginator = this.paginator
    this.form_encounterLoading = true;
    this.ordersPersist.orderssDo("orders_name", {}, paginator.pageSize , isPagination ? paginator.pageIndex: 0).subscribe(
      (partialList: {data: {orders: string, id: string}[], total: number}) => {
        this.ordersData = partialList.data;
        if (partialList.total != -1){
          this.ordersTotalCount = partialList.total
        }
        this.form_encounterLoading = false
      }, error => {
        this.form_encounterLoading = false
        console.error(error)
      }
    );
  }


  searchForm_Soaps(isPagination: boolean = false): void {
    this.form_encounterPersist.form_soapEncounterId = this.form_encounterDetail.id;
    this.form_encounterPersist
      .searchForm_Soap(
        50,
        0,
        "",
        ""
      )
      .subscribe(
        (response) => {
          this.form_soapsData = response.data;
          this.form_soapsTotalCount= response.total;
          if (response.total != -1) {
            this.form_soapsTotalCount = response.total;
          }
        },
        (error) => {
        }
      );
  }
  searchFormVitals(): void {
    //form_vitalss methods
      this.form_encounterPersist
        .searchForm_Vitals(
          this.form_encounterDetail.id,
          50,
          0,
          "",
          ""
        )
        .subscribe(
          (response) => {
            this.form_vitalssData = response.data;
            if (response.total != -1) {
              this.form_vitalssTotalCount= response.total;
              if (this.form_vitalssData.length) {
                let titles = Object.keys(this.form_vitalssData[0]);
                let excluded = [
                  'creation_time',
                  'encounter_id',
                  'height',
                  'id',
                  'pid',
                  'temp_method',
                ];
                excluded.forEach((val) => {
                  titles = titles.filter((title) => title != val);
                });
  
                let multi = [];
                titles.forEach((element) => {
                  let multiObj = {
                    name: element,
                    series: [],
                  };
                  this.form_vitalssData.forEach((resel) => {
                    let obj = {
                      name: this.tcUtilsDate.toDate(resel.creation_time),
                      value: parseFloat(resel[element].toFixed(2)),
                    };
                    multiObj.series.push(obj);
                  });
                  multi.push(multiObj);
                });
                Object.assign(this, { multi });
              }
            }
          },
          (error) => {
          }
        );
}

searchHistory_Datas(isPagination: boolean = false): void {
  this.patientPersist
    .searchHistory_Data(
      this.form_encounterDetail.id.toString(),
      50,
      0,
      "",
      ""
    )
    .subscribe(
      (response) => {
        this.history_datasData = response.data;
        if (response.total != -1) {
          this.history_datasTotalCount = response.total;
        }
      },
      (error) => {
      }
    );
}

searchPrescriptionss(isPagination: boolean = false): void {
  // this.form_encounterPersist.parent_encounter_id = this.form_encounterDetail.id
  this.form_encounterPersist
    .searchPrescriptions(
      50,
      0,
      "",
      ""
    )
    .subscribe(
      (response) => {
        this.prescriptionssData = response.data;
        if (response.total != -1) {
          this.prescriptionssTotalCount = response.total;
        }
      },
      (error) => {
      }
    );
}


ngAfterViewInit(): void {
  //subscribe to route to get id changes -> even on routing to the same component with different id  
 this.loadDetails(this.encounterId);
//  this.menuState.changeMenuState(this.profileUrl);
this.paginator.page.subscribe(() => {
  this.searchOrdersName(true);
});

}




  ngOnInit(): void {
    this.encounterId = this.encounterIdTemplate? this.encounterIdTemplate : this.encounterIdRoute;
  }

  loadDetails(id: string): void {
    this.form_encounterLoading = true;
    this.form_encounterPersist.getForm_Encounter(id).subscribe(
      (form_encounterDetail) => {
      
        this.form_encounterDetail = form_encounterDetail;
        this.profileUrl = `${environment.tcApiBaseUri}tc/files/download?fk=${form_encounterDetail.profile_pic}`;
        
        this.form_encounterLoading = false;
        this.searchPatient(form_encounterDetail.pid)
        this.searchFormVitals();
        this.searchForm_Soaps();
        this.searchForm_Observations();
        this.searchPrescriptionss();
        this.searchHistory_Datas();
        this.searchOrdersName()

      },
      (error) => {
        console.error(error);
        this.form_encounterLoading = false;
      }
    );
  }

  searchPatient(pid: string) {
    this.patientPersist.getPatient(pid).subscribe(patient => {
      console.log(patient)
      this.calculateAge(patient.dob)
      this.form_encounterDetail.birth_date = patient.dob;
    })
  }

}
