import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientProcedureEditComponent } from './patient-procedure-edit.component';

describe('PatientProcedureEditComponent', () => {
  let component: PatientProcedureEditComponent;
  let fixture: ComponentFixture<PatientProcedureEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientProcedureEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientProcedureEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
