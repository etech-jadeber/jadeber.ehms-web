  import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Patient_ProcedureDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { ProcedureSummary } from 'src/app/procedures/procedure.model';


@Component({
  selector: 'app-patient-procedure-edit',
  templateUrl: './patient-procedure-edit.component.html',
  styleUrls: ['./patient-procedure-edit.component.css']
})
export class PatientProcedureEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patient_procedureDetail: Patient_ProcedureDetail = new Patient_ProcedureDetail();
  procedureName:string = ""

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              private procedureNav:ProcedureNavigator,
              public dialogRef: MatDialogRef<PatientProcedureEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null && this.ids.parentId != null;
  }

  isEdit(): boolean {
    return this.ids.childId != null && this.ids.parentId != null;
  }

  isWizard(): boolean {
    return this.ids.parentId == null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log(this.isWizard())
    if (this.isNew() || this.isWizard()) {
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("procedure", "patient_procedures");
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("procedure", "patient_procedures");
      this.isLoadingResults = true;
      this.persist.getPatient_Procedure(this.ids.childId).subscribe(patient_procedureDetail => {
        this.patient_procedureDetail = patient_procedureDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.patient_procedureDetail == null){
          return false;
        }

        if (!this.patient_procedureDetail.procedure_id){
          return false;
        }

     

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addPatient_Procedure(this.ids.parentId, this.patient_procedureDetail).subscribe(value => {
      this.tcNotification.success("Patient_Procedure added");
      this.patient_procedureDetail.id = value.id;
      this.dialogRef.close(this.patient_procedureDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updatePatient_Procedure(this.ids.parentId, this.patient_procedureDetail).subscribe(value => {
      this.tcNotification.success("Patient_Procedure updated");
      this.dialogRef.close(this.patient_procedureDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  onWizard(): void {
    this.dialogRef.close(this.patient_procedureDetail)
  }

  searchProdecure():void{
    this.procedureNav.pickProcedures().afterClosed().subscribe(
      (pro:ProcedureSummary[])=>{
        this.patient_procedureDetail.procedure_id = pro[0].id;
        this.procedureName = pro[0].name;
      }
    )
  }
}
