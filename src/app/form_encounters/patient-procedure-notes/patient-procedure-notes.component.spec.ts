import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientProcedureNotesComponent } from './patient-procedure-notes.component';

describe('PatientProcedureNotesComponent', () => {
  let component: PatientProcedureNotesComponent;
  let fixture: ComponentFixture<PatientProcedureNotesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientProcedureNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientProcedureNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
