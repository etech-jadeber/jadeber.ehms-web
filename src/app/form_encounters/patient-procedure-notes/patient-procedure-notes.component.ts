import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { CommentDetail, CommentSummary } from 'src/app/tc/models';
import { Location } from '@angular/common';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { AppTranslation } from 'src/app/app.translation';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCAppInit } from 'src/app/tc/app-init';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';
import { ProcedureNotePersist } from 'src/app/patients/procedureNote.persist';
import { ProcedureDetail, ProcedureNoteSummary } from 'src/app/procedures/procedure.model';
import { Patient_ProcedureDetail } from '../form_encounter.model';
import { UserDetail } from 'src/app/tc/users/user.model';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { ProcedureTemplatePersist } from 'src/app/procedure_template/procedure_template.persist';
import { PatientDetail } from 'src/app/patients/patients.model';

export enum PaginatorIndexes {
  notes,
}

@Component({
  selector: 'app-patient-procedure-notes',
  templateUrl: './patient-procedure-notes.component.html',
  styleUrls: ['./patient-procedure-notes.component.css']
})
export class PatientProcedureNotesComponent implements OnInit {
  paramsSubscription: Subscription;
  id: string;
  Editor = ClassicEditor;
  config = {'toolbar': []};


  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
    //notes
    notesData: ProcedureNoteSummary[] = [];
    notesTotalCount: number = 0;
    patient: PatientDetail
    patient_name: string;
    notesSelectAll: boolean = false;
    notesSelected: ProcedureNoteSummary[] = [];
    notesDisplayedColumns: string[] = ['select', 'action', 'comment_text'];
    notesSearchTextBox: FormControl = new FormControl();
    notesLoading: boolean = false;
    procedureDetail: Patient_ProcedureDetail = new Patient_ProcedureDetail()
    users: {[id: string]: UserDetail} = {}
  constructor(
    private route: ActivatedRoute,
    private patientPersist: PatientPersist,
    private location: Location,
    public from_encounterPersist: Form_EncounterPersist,
    public userPersist: UserPersist,
    public appTranslation: AppTranslation,
    public tcNavigator: TCNavigator,
    public tcAuthorization: TCAuthorization,
    public tcAuthentication: TCAuthentication,
    private tcNotification: TCNotification,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public procedureNotePersist: ProcedureNotePersist,
    public formEncounterPersist: Form_EncounterPersist,
    public radiologyResultSettingPersist: RadiologyResultSettingsPersist,
    public procedureTemplatePersist: ProcedureTemplatePersist,
  ) {

        //notes filter
        this.notesSearchTextBox.setValue(procedureNotePersist.noteSearchText);
        this.notesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
          this.procedureNotePersist.noteSearchText = value.trim();
        });
   }

  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_notes")
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      this.id = this.route.snapshot.paramMap.get('id');
      this.loadDetail(this.id);
      // this.menuState.changeMenuState(id);
    });

  }

  reload() {
    this.searchNotes();
  }

  back(): void {
    this.location.back();
  }

  loadDetail(id:string){
    // if(this.from_encounterPersist.encounter_is_active == null)
    this.formEncounterPersist.encounter_active = true;
    this.from_encounterPersist.getPatient_Procedure(id).subscribe(patient_procedure=>{
      if(patient_procedure){
        this.procedureNotePersist.procedureId = patient_procedure.id
        this.procedureDetail = patient_procedure;
        this.searchNotes();
        this.patientPersist.getPatient(patient_procedure.patient_id).subscribe((patient) => {
            this.patient = patient
            this.patient_name = patient.fname + " " + patient.mname + " " + patient.lname
        })
        // this.from_encounterPersist.getForm_Encounter(patient_procedure.encounter_id).subscribe((encounter)=>{
        //   if(encounter){
        //     this.from_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
        //   }
        // })
      }
    })
  }

    //notes methods
    searchNotes(): void {
      let paginator = this.paginators.toArray()[PaginatorIndexes.notes];
      let sorter = this.sorters.toArray()[PaginatorIndexes.notes];
      this.notesLoading = true;
      this.procedureNotePersist.searchProcedureNote(10, 0, "", "").subscribe(response => {
        this.notesData = response.data;
        this.notesData.forEach(note => {
          if (!this.users[note.provider_id]) {
            this.users[note.provider_id] = new UserDetail()
            this.userPersist.getUser(note.provider_id).subscribe(
              user => {
                this.users[note.provider_id] = user;
              }
            )
          }
        })
        if (response.total != -1) {
          this.notesTotalCount = response.total;
        }
        this.notesLoading = false;
      }, error => {
  
      });
    }

    addNote(): void {
      this.procedureTemplatePersist.procedure_id = this.procedureDetail.procedure_id
        this.procedureTemplatePersist.searchProcedureTemplate(1, 0, 'id', 'asc').subscribe((procedure)=>{
          let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), procedure.data[0]?.template, true, 'Save', 'Cancel', [], '', true);
          modalRef.afterClosed().subscribe(note => {
        if (note) {
          const procedureNote = new ProcedureNoteSummary()
          procedureNote.patient_id = this.procedureDetail.patient_id;
          procedureNote.encounter_id = this.procedureDetail.encounter_id;
          procedureNote.note = note;
          procedureNote.patient_procedure_id = this.id
          this.procedureNotePersist.addProcedureNote(procedureNote).subscribe(response => {
            this.searchNotes();
          });
        }
      });
        })
    }
  
    editNote(item: ProcedureNoteSummary): void {
      let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), item.note, true, 'Save', 'Cancel', [], '', true);
      modalRef.afterClosed().subscribe(note => {
        if (note) {
          item.note = note
          this.procedureNotePersist.updateProcedureNote(item).subscribe(response => {
            this.searchNotes();
          });
        }
      });
    }
  
    canUpdateNote(item: ProcedureNoteSummary): boolean {
      return this.tcAuthorization.isSysAdmin() || item.provider_id == TCAppInit.userInfo.user_id
    }
  
    deleteNote(item: ProcedureNoteSummary): void {
      let dialogRef = this.tcNavigator.confirmDeletion("Note");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          //do the deletion
          this.procedureNotePersist.deleteProcedureNote(item.id).subscribe(response => {
            this.tcNotification.success("note deleted");
            this.searchNotes();
          }, error => {
          });
        }
  
      });
    }
  
    // archiveNote(item: CommentDetail): void {
    //   this.patientPersist.noteDo(this.id, item.id, "archive", {}).subscribe(response => {
    //     this.tcNotification.success("note archived");
    //     this.searchNotes();
    //   }, error => {
    //   });
    // }
    getUser(id: string): string {
      return (this.users[id]?.name || "Unknown User")
    }

    calculateAge(dob: number) {
      const current = new Date().getFullYear()   
      return current - new Date(dob*1000).getFullYear() 
    }

}
