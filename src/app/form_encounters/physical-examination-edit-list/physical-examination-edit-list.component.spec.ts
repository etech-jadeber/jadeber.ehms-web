import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationEditListComponent } from './physical-examination-edit-list.component';

describe('PhysicalExaminationEditListComponent', () => {
  let component: PhysicalExaminationEditListComponent;
  let fixture: ComponentFixture<PhysicalExaminationEditListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationEditListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationEditListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
