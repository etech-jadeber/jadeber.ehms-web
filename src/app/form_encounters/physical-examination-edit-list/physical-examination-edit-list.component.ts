import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { added_by } from 'src/app/app.enums';
import { TCAppInit } from 'src/app/tc/app-init';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Physical_ExaminationDetail, Physical_ExaminationSummaryPartialList } from '../form_encounter.model';
import { Form_EncounterPersist } from '../form_encounter.persist';

@Component({
  selector: 'app-physical-examination-edit-list',
  templateUrl: './physical-examination-edit-list.component.html',
  styleUrls: ['./physical-examination-edit-list.component.scss']
})
export class PhysicalExaminationEditListComponent implements OnInit {

  @Input() encounterId: string;
  @Input() nurseId: string;
  @Input() date: any;
  @ViewChild(MatPaginator, { static: true }) patientsPaginator: MatPaginator;
  physical_examinationsData: Physical_ExaminationDetail[] = []
  physical_examinationsTotal: number;
  physical_examinationLoading: boolean = false;
  canEdit: boolean = false;
  isLoadingResults: boolean = false;
  decreaceBy: number = 0;

  constructor(
    public physicalExaminationPersist: Form_EncounterPersist,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
  ) { }

  ngOnInit(): void {
    this.patientsPaginator.page.subscribe(() => {
      this.searchPhysicalExamination(true);
    });
    this.searchPhysicalExamination()
  }

  searchPhysicalExamination(isPagination: boolean = false): void {

    let paginator = this.patientsPaginator;
    this.physicalExaminationPersist.date_of_examination = this.date ? this.tcUtilsDate.toTimeStamp(this.date):undefined;
    this.physicalExaminationPersist.nurse_id = this.nurseId;

    this.physical_examinationLoading = true;
    if (this.physical_examinationsData.length > 1){
      this.physical_examinationsData.shift()
      this.canEdit = false;
      this.decreaceBy = 0;
      this.physical_examinationLoading = false;
      return;
    }

    this.physicalExaminationPersist.searchPhysical_Examination(this.encounterId, 1, isPagination ? paginator.pageIndex - this.decreaceBy : 0, 'date_of_examination', "desc").subscribe((partialList: Physical_ExaminationSummaryPartialList) => {
      this.physical_examinationsData = partialList.data;
      if (partialList.total != -1) {
        this.physical_examinationsTotal = partialList.total;
      }
      if (!partialList.total){
        this.physical_examinationsData.push(new Physical_ExaminationDetail())
        this.physical_examinationsData[0].encounter_id = this.encounterId
        this.physical_examinationsData[0].type = added_by.nurse;
        this.canEdit = true;
      }
      else if (!paginator.pageIndex){
        if(this.physical_examinationsData[0].provider_id == TCAppInit.userInfo.user_id){
          this.canEdit = true;
        } else  if(this.physical_examinationsTotal < 3){
        this.physical_examinationsData.unshift(new Physical_ExaminationDetail())
        this.physical_examinationsData[0].encounter_id = this.encounterId
        this.physical_examinationsData[0].type = added_by.nurse;
        this.canEdit = true;
        this.physical_examinationsTotal++;
        this.decreaceBy = 1
        }
      } else {
        this.canEdit = false;
      }
      this.physical_examinationLoading = false;
    }, error => {
      this.physical_examinationLoading = false;
    });

  }

  saveResult(){
    if(this.physical_examinationsData[0].id){
      this.onUpdate(this.physical_examinationsData[0])
    } else {
      this.onAdd(this.physical_examinationsData[0])
    }
  }

  onAdd(physical_examinationDetail: Physical_ExaminationDetail): void {
    this.isLoadingResults = true;
    this.physicalExaminationPersist.addPhysical_Examination(physical_examinationDetail.encounter_id, physical_examinationDetail).subscribe(value => {
    this.tcNotification.success("Physical_Examination added");
      this.isLoadingResults = false;
      this.searchPhysicalExamination()
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(physical_examinationDetail: Physical_ExaminationDetail): void {
    this.isLoadingResults = true;
    this.physicalExaminationPersist.updatePhysical_Examination(physical_examinationDetail.encounter_id, physical_examinationDetail).subscribe(value => {
      this.tcNotification.success("Physical_Examination updated");
      this.isLoadingResults = false;
      this.searchPhysicalExamination()
    }, error => {
      this.isLoadingResults = false;
    })

  }

    ngOnDestroy():void{
      this.physicalExaminationPersist.date_of_examination = undefined;
      this.physicalExaminationPersist.nurse_id = undefined;
    }


}
