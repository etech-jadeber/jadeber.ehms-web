import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Physical_ExaminationEditComponent } from './physical-examination-edit.component';

describe('PhysicalExaminationEditComponent', () => {
  let component: Physical_ExaminationEditComponent;
  let fixture: ComponentFixture<Physical_ExaminationEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Physical_ExaminationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Physical_ExaminationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
