import {Component, OnInit, Inject, ViewChildren, QueryList, Input, Injectable, Output, EventEmitter, SimpleChanges} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Physical_ExaminationDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { MatCheckboxChange } from '@angular/material/checkbox';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
import { added_by, PhysicalExaminationOptions, PhysicalExaminationState } from 'src/app/app.enums';
import { PhysicalExaminationOptionsDashboard, PhysicalExaminationOptionsDetail, PhysicalExaminationOptionsSummaryPartialList } from 'src/app/physical_examination_options/physical_examination_options.model';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { PhysicalExaminationOptionsNavigator } from 'src/app/physical_examination_options/physical_examination_options.navigator';

@Injectable()
abstract class Physical_ExaminationComponent implements OnInit {

  is_new :boolean = false;
  isLoadingResults: boolean = false;
  title: string;
  physical_examinationDetail: Physical_ExaminationDetail = new Physical_ExaminationDetail();
  checked: boolean;
  physicalExaminationOptions = {}
  physicalOptions = PhysicalExaminationOptions

  // @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  // @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  @Output() savePhysicalExamintion = new EventEmitter()
  @Input() canEdit: boolean;
  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              public physicalExaminationOptionPersist: PhysicalExaminationOptionsPersist,
              public physicalExamintaionOptionNavigator: PhysicalExaminationOptionsNavigator,
              public tcUtilsArray: TCUtilsArray,
              public ids: TCParentChildIds,
              public isDialog: Boolean) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }


  // general_appearance : string;
  // ent : string;
  // eye : string;
  // head_and_neck : string;
  // glands : string;
  // chests : string;
  // cardiovascular : string;
  // abdominal : string;
  // genitourinary : string;
  // musculoskeletal : string;
  // integument : string;
  // neurology : string;
  // encounter : number;
  // vital_sign : string;

  action(examination: Physical_ExaminationDetail){

  }

  onCancel(){}

  ngOnInit() {
    if (this.isNew()) {
      this.title = this.appTranslation.getText("general", "new") +" "+ this.appTranslation.getText("patient", "physical_examination");
      this.physical_examinationDetail.encounter_id = this.ids.parentId;
      this.physical_examinationDetail.ent = "";
      this.physical_examinationDetail.eye = "";
      this.physical_examinationDetail.head_and_neck = "";
      this.physical_examinationDetail.glands = "";
      this.physical_examinationDetail.chests = "";
      this.physical_examinationDetail.cardiovascular = "";
      this.physical_examinationDetail.abdominal = "";
      this.physical_examinationDetail.genitourinary = "";
      this.physical_examinationDetail.musculoskeletal = "";
      this.physical_examinationDetail.integument = "";
      this.physical_examinationDetail.neurology = "";
      this.physical_examinationDetail.abdominal = "";
      this.physical_examinationDetail.vital_sign = "";
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general", "edit") +" "+ this.appTranslation.getText("patient", "physical_examination");
      this.isLoadingResults = true;
      this.persist.getPhysical_Examination(this.ids.childId).subscribe(physical_examinationDetail => {
        this.physical_examinationDetail = physical_examinationDetail;
        // this.addToOption()
        this.updateNormality()
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }

  getPhysicalExminationOption(physical_option: number, status: boolean ) {
    const field = this.tcUtilsArray.getEnum(this.physicalExaminationOptionPersist.PhysicalExaminationOptions, physical_option)?.name.toLowerCase().replace(/\s/g, '_')
    console.log(field)
    if (status == null){
      this.physical_examinationDetail[field] = ''
      return;
    }
    this.physicalExaminationOptionPersist.physicalExaminationOptionsFilter = physical_option;
    this.physicalExaminationOptionPersist.physicalExaminationStateFilter = status ? PhysicalExaminationState.normal : PhysicalExaminationState.abnormal;
    this.physicalExaminationOptionPersist.searchPhysicalExaminationOptions(5, 0, 'id', 'desc' ).subscribe((value) => {
    if (status){
        this.physical_examinationDetail[field] = "Normal:  \n" + value.data.map(res => "\u2022" + " " + res.findings).join('\n')
      } else {
        this.physical_examinationDetail[field] = "Abnormal:  \n" + value.data.map(res => "\u2022" + " " + res.findings).join('\n')
      }
    })
  }

  searchPhysicalExminationOption(physical_option: number, field: string ) {
    const dialogRef = this.physicalExamintaionOptionNavigator.pickPhysicalExaminationOptionss(physical_option)
    dialogRef.afterClosed().subscribe(
      (result: PhysicalExaminationOptionsDetail[]) => {
        if(result){
          if (this.physical_examinationDetail[field] == "All Normal."){
            this.physical_examinationDetail[field] = "Normal:  \n" + result.map(res => "\u2022" + " " + res.findings).join('\n')
          }
          else if(this.physical_examinationDetail[field] == "All Abnormal."){
            this.physical_examinationDetail[field] = "Abnormal:  \n" + result.map(res => "\u2022" + " " + res.findings).join('\n')
          } else {
            this.physical_examinationDetail[field] += result.map(res => "\n\u2022" + " " + res.findings).join('\n')
          }
        }
      }
    )
  }

  updateChange(value: boolean, field: string){
    this.physical_examinationDetail[field] = `All ${value ? "Normal" : "Abnormal"}.`
  }

  makeAll(value: boolean) : void {
    this.physical_examinationDetail.head_and_neck_bool = value;
    this.physical_examinationDetail.ent_bool = value;
    this.physical_examinationDetail.eye_bool = value;
    this.physical_examinationDetail.chests_bool = value;
    this.physical_examinationDetail.cardiovascular_bool = value;
    this.physical_examinationDetail.glands_bool = value;
    this.physical_examinationDetail.musculoskeletal_bool = value;
    this.physical_examinationDetail.neurology_bool = value;
    this.physical_examinationDetail.abdominal_bool = value;
    this.physical_examinationDetail.genitourinary_bool = value;
    this.physical_examinationDetail.integument_bool = value;
    this.physical_examinationDetail.head_and_neck_bool = value;
    this.getPhysicalExminationOption(this.physicalOptions.ent, value)
    this.getPhysicalExminationOption(this.physicalOptions.eye, value)
    this.getPhysicalExminationOption(this.physicalOptions.chests, value)
    this.getPhysicalExminationOption(this.physicalOptions.cardiovascular, value)
    this.getPhysicalExminationOption(this.physicalOptions.glands, value)
    this.getPhysicalExminationOption(this.physicalOptions.musculoskeletal, value)
    this.getPhysicalExminationOption(this.physicalOptions.neurology, value)
    this.getPhysicalExminationOption(this.physicalOptions.abdominal, value)
    this.getPhysicalExminationOption(this.physicalOptions.genitourinary, value)
    this.getPhysicalExminationOption(this.physicalOptions.integument, value)
    this.getPhysicalExminationOption(this.physicalOptions.head_and_neck, value)
    // this.physical_examinationDetail.ent = str;
    // this.physical_examinationDetail.eye = str;
    // this.physical_examinationDetail.chests = str;
    // this.physical_examinationDetail.cardiovascular = str;
    // this.physical_examinationDetail.glands = str;
    // this.physical_examinationDetail.musculoskeletal = str;
    // this.physical_examinationDetail.neurology = str;
    // this.physical_examinationDetail.abdominal = str;
    // this.physical_examinationDetail.genitourinary = str;
    // this.physical_examinationDetail.integument = str;
    // this.physical_examinationDetail.head_and_neck = str;
  }

  updateNormality(){
    for(let data in this.physical_examinationDetail){
      this.physical_examinationDetail[`${data}_bool`] = !(this.physical_examinationDetail[data]?.toString())?.match(/abnormal/i)
    }
  }



  ngAfterViewInit(){
  //   for (let option of this.physicalExaminationOptionPersist.PhysicalExaminationOptions){
  //   this.paginators.toArray()[option.id - 101].page.pipe().subscribe(value => {
  //     // this.searchPhysicalExaminationOptions(option.id, true)
  //   })
  // }
  }

  // searchPhysicalExaminationOptions(option: number, isPagination:boolean = false){
  //   const paginator = this.paginators.toArray()[option - 101]
  //   this.physicalExaminationOptionPersist.physicalExaminationOptionsFilter = option;
  //   this.physicalExaminationOptionPersist.physicalExaminationStateFilter = this.physicalExaminationOptions[option].state
  //   this.physicalExaminationOptionPersist.searchPhysicalExaminationOptions(paginator.pageSize, isPagination? paginator.pageIndex:0, "id", "asc").subscribe((partialList: PhysicalExaminationOptionsSummaryPartialList) => {
  //      this.physicalExaminationOptions[option].data = partialList.data;
  //     if (partialList.total != -1) {
  //       this.physicalExaminationOptions[option].total_count = partialList.total;
  //     }
  //     this.physicalExaminationOptions[option].is_loading = false;
  //   }, error => {
  //     this.physicalExaminationOptions[option].is_loading = false;
  //   });
  // }

  // checkValidity(opt: string, option: number){
  //   if(!opt){
  //     return false;
  //   }
  //   if(this.tcUtilsString.isValidId(opt)){
  //     return true
  //   } else {
  //     this.physicalExaminationOptions[option].detail = JSON.parse(opt)
  //     return false
  //   }
  // }

  // addToDetail(option: number, value: string, toOption: boolean = false):void{
  //   if (option == this.physicalOptions.abdominal){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.abdominal.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.abdominal = value
  //   }
  //   else if (option == this.physicalOptions.cardiovascular){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.cardiovascular.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.cardiovascular = value
  //   }
  //   else if (option == this.physicalOptions.chests){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.chests.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.chests = value
  //   }
  //   else if (option == this.physicalOptions.ent){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.ent.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.ent = value
  //   }
  //   else if (option == this.physicalOptions.eye){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.eye.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.eye = value
  //   }
  //   else if (option == this.physicalOptions.genitourinary){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.genitourinary.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.genitourinary = value
  //   }
  //   else if (option == this.physicalOptions.glands){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.glands.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.glands = value
  //   }
  //   else if (option == this.physicalOptions.musculoskeletal){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.musculoskeletal.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.musculoskeletal = value
  //   }
  //   else if (option == this.physicalOptions.head_and_neck){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.head_and_neck.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.head_and_neck = value
  //   }
  //   else if (option == this.physicalOptions.integument){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.integument.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.integument = value
  //   }
  //   else if (option == this.physicalOptions.neurology){
  //     if (toOption){
  //       this.physicalExaminationOptions[option].selected = this.physical_examinationDetail.neurology.split(/,(?=[^"])/).filter(opt => this.checkValidity(opt, option))
  //       return;
  //     }
  //     this.physical_examinationDetail.neurology = value
  //   }
  // }

  // addToOption(){
  //   for (const option of this.physicalExaminationOptionPersist.PhysicalExaminationOptions) {
  //     this.checkOptionCreated(option.id)
  //     this.addToDetail(option.id, "", true)
  //   }
  // }

  // checkChecked(option: number, id: string):boolean{
  //   return this.physicalExaminationOptions[option].selected &&  this.physicalExaminationOptions[option].selected.findIndex(value => value == id) != -1
  // }


  canSubmit():boolean{
      if (this.physical_examinationDetail == null){
          return false;
        }
                    // if (this.physical_examinationDetail.ent == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.eye == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.head_and_neck == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.chests == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.cardiovascular == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.glands == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.musculoskeletal == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.neurology == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.abdominal == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.genitourinary == null) {
                    //   return false;
                    // }
                    // if (this.physical_examinationDetail.integument == null) {
                    //   return false;
                    // }
      return true;
    }

    // setCheck(event: MatCheckboxChange, option: number): void {
    //   this.checkOptionCreated(option)
    //   if(this.physicalExaminationOptions[option].checked){
    //     this.physicalExaminationOptions[option].checked = false
    //     this.physicalExaminationOptions[option].state = PhysicalExaminationState.abnormal
    //     this.searchPhysicalExaminationOptions(option)
    //   } else if(this.physicalExaminationOptions[option].checked == null) {
    //     this.physicalExaminationOptions[option].checked = true;
    //     this.physicalExaminationOptions[option].state = PhysicalExaminationState.normal
    //     this.searchPhysicalExaminationOptions(option)
    //   } else {
    //     this.physicalExaminationOptions[option].checked = null;
    //     this.physicalExaminationOptions[option].data = [];
    //     this.physicalExaminationOptions[option].total_count = 0
    //   }
    // }

    // updateFinding(event: MatCheckboxChange, option : number, id: string): void {
    //   if (!this.physicalExaminationOptions[option].selected){
    //     this.physicalExaminationOptions[option].selected = []
    //   }
    //   if (event.checked){
    //     this.physicalExaminationOptions[option].selected.push(id)
    //   } else {
    //     this.physicalExaminationOptions[option].selected = this.physicalExaminationOptions[option].selected.filter(value => value != id)
    //   }
    //   this.addToDetail(option, this.physicalExaminationOptions[option].selected.join(','))
    // }

    // checkOptionCreated(option: number) {
    //   if (!this.physicalExaminationOptions[option]){
    //     this.physicalExaminationOptions[option] = {}
    //   }
    // }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addPhysical_Examination(this.physical_examinationDetail.encounter_id, this.physical_examinationDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success("Physical_Examination added");
      this.physical_examinationDetail.id = value.id;
      this.action(this.physical_examinationDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  // getDetail(isChecked: boolean, id: number) {
  //   if(isChecked){
  //     return (this.physicalExaminationOptions[id].detail && this.physicalExaminationOptions[id].detail.normal) || ""
  //   }
  //   return (this.physicalExaminationOptions[id].detail && this.physicalExaminationOptions[id].detail.abnormal) || ""
  // }

  // setDetail(isChecked:boolean, id: number, value: string){
  //   if (!this.physicalExaminationOptions[id].selected){
  //     this.physicalExaminationOptions[id].selected = []
  //   }
  //   if(isChecked){
  //     this.physicalExaminationOptions[id].detail = {...this.physicalExaminationOptions[id].detail, normal: value}
  //   }else {
  //     this.physicalExaminationOptions[id].detail = {...this.physicalExaminationOptions[id].detail, abnormal : value}
  //   }
  //   this.physicalExaminationOptions[id].selected = this.physicalExaminationOptions[id].selected.filter(value => this.tcUtilsString.isValidId(value))
  //   this.physicalExaminationOptions[id].selected.push(JSON.stringify(this.physicalExaminationOptions[id].detail))
  //   this.addToDetail(id, this.physicalExaminationOptions[id].selected.join(','))
  // }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updatePhysical_Examination(this.physical_examinationDetail.encounter_id, this.physical_examinationDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success("Physical_Examination updated");
      this.action(this.physical_examinationDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })

  }
}


@Component({
  selector: 'app-physical_examination-edit',
  templateUrl: './physical-examination-edit.component.html',
  styleUrls: ['./physical-examination-edit.component.css']
})
export class Physical_ExaminationEditComponent extends Physical_ExaminationComponent {

  // @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  // @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Physical_ExaminationEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              public physicalExaminationOptionPersist: PhysicalExaminationOptionsPersist,
              public physicalExamintaionOptionNavigator: PhysicalExaminationOptionsNavigator,
              public tcUtilsArray: TCUtilsArray,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {
                super(tcNotification, tcUtilsString, appTranslation, persist, physicalExaminationOptionPersist, physicalExamintaionOptionNavigator, tcUtilsArray, ids, true)
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(review: Physical_ExaminationDetail){
    this.dialogRef.close(review)
  }

}


@Component({
  selector: 'app-physical_examination-edit-list',
  templateUrl: './physical-examination-edit.component.html',
  styleUrls: ['./physical-examination-edit.component.css']
})
export class Physical_ExaminationEditListComponent extends Physical_ExaminationComponent {

  // @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  // @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  @Input() encounterId: string;
  @Input() physicalExamination: Physical_ExaminationDetail;
  @Input() canEdit: boolean;
  @Output() savePhysicalExamintion = new EventEmitter()
  @Output() assign_value = new EventEmitter<any>();
  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              public physicalExaminationOptionPersist: PhysicalExaminationOptionsPersist,
              public physicalExamintaionOptionNavigator: PhysicalExaminationOptionsNavigator,
              public tcUtilsArray: TCUtilsArray) {
                super(tcNotification, tcUtilsString, appTranslation, persist, physicalExaminationOptionPersist, physicalExamintaionOptionNavigator, tcUtilsArray, {
                  childId: null, parentId: null, context: null
                }, false)
  }

  ngOnInit(): void {
    console.log("this", this.physicalExamination)
    if (this.physicalExamination){
      this.physical_examinationDetail = this.physicalExamination
      this.persist.encounter_is_active = this.canEdit;
      this.updateNormality()
    }
    else{
    this.persist.searchPhysical_Examination(this.encounterId, 1, 0, 'id', 'desc').subscribe(
      examination => {
        if(examination.data.length){
          this.physical_examinationDetail = examination.data[0]
        }else {
          this.assign_value.emit({new_physical_examination: true })
          this.physical_examinationDetail = new Physical_ExaminationDetail()
        }
        this.physical_examinationDetail.type = added_by.doctor;
        this.updateNormality()
      }
    )}
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.physicalExamination?.currentValue){
      this.physical_examinationDetail = changes.physicalExamination.currentValue as Physical_ExaminationDetail
      this.updateNormality()
    }
    if (changes.canEdit){
      this.persist.encounter_is_active = changes.canEdit.currentValue;
    }
  }

  saveResult(){
    if (!this.canSubmit()){
      return;
    }
    this.physical_examinationDetail.encounter_id = this.encounterId
    if (this.physical_examinationDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }

  isNew(): boolean {
    return false
  }

}
