import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { PrescriptionsDetail } from '../form_encounter.model';
import { Form_EncounterPersist, Form_EncounterPersist as PrescriptionsPersist } from '../form_encounter.persist'; 
import { ItemPersist } from 'src/app/items/item.persist';
import { Form_EncounterNavigator as PrescriptionsNavigator } from '../form_encounter.navigator'; 
import { MedicationAdminstrationChartPersist } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.persist';
import { calculateQuantity } from '../prescriptions-edit/prescriptions-edit.component';
import { encounter_status } from 'src/app/app.enums';
export enum PrescriptionsTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-prescriptions-detail',
  templateUrl: './prescriptions-detail.component.html',
  styleUrls: ['./prescriptions-detail.component.scss']
})
export class PrescriptionsDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  calculateQuantity=calculateQuantity;
  prescriptionsIsLoading:boolean = false;

  drugName:string;
  PrescriptionsTabs: typeof PrescriptionsTabs = PrescriptionsTabs;
  activeTab: PrescriptionsTabs = PrescriptionsTabs.overview;
  //basics
  prescriptionsDetail: PrescriptionsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public prescriptionsNavigator: PrescriptionsNavigator,
              public  prescriptionsPersist: PrescriptionsPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public itemPersist: ItemPersist,
              public medicationAdminstrationChartPersist : MedicationAdminstrationChartPersist,
              ) {
    this.tcAuthorization.requireRead("prescriptions");
    this.prescriptionsDetail = new PrescriptionsDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("prescriptions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.prescriptionsIsLoading = true;
    this.prescriptionsPersist.getPrescriptions(id).subscribe(prescriptionsDetail => {
          this.prescriptionsDetail = prescriptionsDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(prescriptionsDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.prescriptionsDetail.quantity=this.calculateQuantity(prescriptionsDetail.dose,prescriptionsDetail.duration,prescriptionsDetail.duration_unit,prescriptionsDetail.frequency);
          this.itemPersist.getItem(this.prescriptionsDetail.drug_id).subscribe((result)=>{
            this.drugName=result.name;
          })
          this.prescriptionsIsLoading = false;
        }, error => {
          console.error(error);
          this.prescriptionsIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.prescriptionsDetail.id);
  }
  editPrescriptions(): void {
    let modalRef = this.prescriptionsNavigator.editPrescriptions(null,this.prescriptionsDetail.id);
    modalRef.afterClosed().subscribe(prescriptionsDetail => {
      TCUtilsAngular.assign(this.prescriptionsDetail, prescriptionsDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.prescriptionsPersist.print(this.prescriptionsDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print prescriptions", true);
      });
    }

  back():void{
      this.location.back();
    }

}