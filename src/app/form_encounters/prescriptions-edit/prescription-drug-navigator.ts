import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { Router } from '@angular/router';
import { TCModalWidths } from 'src/app/tc/utils-angular';
import { FormDrugPickComponent } from '../form-drug-pick/form-drug-pick.component';

@Injectable({
  providedIn: 'root',
})
export class PrescriptionDrugNavigator {
  constructor(private router: Router, public dialog: MatDialog) { }

//picker for drugs
  pickDrugs(selectOne: boolean = false): MatDialogRef<unknown,any>{
    const dialogRef = this.dialog.open(FormDrugPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}