import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PrescriptionsEditComponent } from './prescriptions-edit.component';

describe('PrescriptionsEditComponent', () => {
  let component: PrescriptionsEditComponent;
  let fixture: ComponentFixture<PrescriptionsEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PrescriptionsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
