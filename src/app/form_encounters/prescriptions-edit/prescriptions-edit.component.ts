import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCEnum, TCId, TCModalModes, TCParentChildIds} from "../../tc/models";
import {PrescriptionsDetail, PrescriptionsSummary} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import {PatientNavigator} from 'src/app/patients/patients.navigator';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {DoctorNavigator} from 'src/app/doctors/doctor.navigator';
import {TCUtilsArray} from 'src/app/tc/utils-array';
import {PrescriptionDrugNavigator} from './prescription-drug-navigator';
import {ItemNavigator} from 'src/app/items/item.navigator';
import { MedicationAdminstrationChartPersist } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.persist';
import { Procedure_Order_CodeNavigator } from '../procedure_order_codes/procedure_order_code.navigator';
import { Icd11Summary } from 'src/app/icd_11/icd_11.model';
import { Frequency, ServiceType, store_type,prescription_type, physican_type } from 'src/app/app.enums';
import { ItemDetail, ItemSummary } from 'src/app/items/item.model';
import { ItemPersist } from 'src/app/items/item.persist';
import { FinalAssessmentPersist } from 'src/app/final_assessment/final_assessment.persist';
import { PrescriptionRequestPersist } from 'src/app/prescription_request/prescription_request.persist';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { PrescriptionRequestDetail } from 'src/app/prescription_request/prescription_request.model';
import { PatientHistoryPersist } from 'src/app/patient_history/patient_history.persist';
import { Icd11Persist } from 'src/app/icd_11/icd_11.persist';

export class PrescriptionsDetailForList extends PrescriptionsDetail {
  strength : number;
  interval: number;
  unit: number;
  store_type: number;
  is_surgery: boolean;
}


export const frequency_value = {
  0:{
    value: 0
  },
  [Frequency.every_12_hours]:{
    value:1/12
  },
  [Frequency.every_2_hours]:{
    value:1/2
  },
  [Frequency.every_2_weeks]:{
    value:1/336
  },
  [Frequency.every_3_hours]:{
    value:1/3
  },
  [Frequency.every_3_weeks]:{
    value:1/504
  },
  [Frequency.every_4_hours]:{
    value:1/4
  },
  [Frequency.every_6_hours]:{
    value:1/6
  },
  [Frequency.every_8_hours]:{
    value:1/8
  },
  [Frequency.every_hour]:{
    value:1
  },
  [Frequency.five_days_a_week]:{
    value:5/168
  },
  [Frequency.five_times_a_day]:{
    value:5/24
  },
  [Frequency.four_days_week]:{
    value:4/168
  },
  // [Frequency.four_times_a_day]:{
  //   value:4/24
  // },
  [Frequency.none]:{
    value:1/24
  },

  [Frequency.prn]:{
    value:1/24
  },

  [Frequency.immidiately]:{
    value:1/24
  },
  [Frequency.on_alternate_days]:{
    value:1/48
  },
  [Frequency.once_a_day]:{
    value:1/24
  },
  [Frequency.onece_a_week]:{
    value: 1/168
  },
  [Frequency.six_days_a_week]:{
    value:6/168
  },
  // [Frequency.thrice_a_day]:{
  //   value:3/24
  // },
  [Frequency.thrice_a_week]:{
    value:3/168
  },
  // [Frequency.twice_a_day]:{
  //   value:2/24
  // },

  [Frequency.twice_a_week]:{
    value:2/168
  }

}

const storeType = {
  [ServiceType.outpatient]:store_type.outpatient,
  [ServiceType.inpatient]:store_type.inpatient,
  [ServiceType.emergency]:store_type.emergency,
  [ServiceType.outsideOrder]:store_type.outpatient,
  [store_type.ward_store]: store_type.ward_store,
  [store_type.or_store]: store_type.or_store
}


@Component({
  selector: 'app-prescriptions-edit',
  templateUrl: './prescriptions-edit.component.html',
  styleUrls: ['./prescriptions-edit.component.css']
})
export class PrescriptionsComponent implements OnInit {

  duration_unit:number;
  isLoadingResults: boolean = false;
  title: string;
  prescriptionsDetail: PrescriptionsDetail;
  patientFullName: string;
  date_modified: Date = new Date();
  doctorFullName: string;
  prescription_type = prescription_type;
  hr :number;

  prescriptionssData: PrescriptionsSummary[] = [];
  prescriptionssTotalCount: number = 0;
  prescriptionssDisplayedColumns: string[] = ['action', 'drug_id'];
  prescriptionssLoading: boolean = false;
  prescriptionsDetailForList: PrescriptionsDetailForList = new PrescriptionsDetailForList();
  icd11_diagnosis_options: Icd11Summary[];

  //prescriptionss methods
  drugs: any[];
  drugName: string = "";
  uuidToNameDrug = [];
  dose: number = 0;
  duration: number = 0;
  frequency: number = -1;

  item_options: ItemDetail[] = [];

  selected_item : ItemDetail ;

  storeTypeEnum : TCEnum[] = [
    new TCEnum(store_type.outpatient, 'outpatient'),
    new TCEnum(store_type.inpatient, 'inpatient'),
    new TCEnum(store_type.emergency, 'emergency'),
    new TCEnum(store_type.opd_store,'Adult OPD'),
    new TCEnum(store_type.ward_store,'Ward Store'),
    new TCEnum(store_type.or_store,'OR Store'),
    new TCEnum(store_type.endoscopy_store, 'Endoscopy Store')
  ];





  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<PrescriptionsEditComponent>,
              public persist: Form_EncounterPersist,
              public itemNav: ItemNavigator,
              public tcUtilsDate: TCUtilsDate,
              public prescriptionDrugNavigator: PrescriptionDrugNavigator,
              public medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public itemPersist: ItemPersist,


              public doctorNavigator: DoctorNavigator,
              public patientHistoryPersist: PatientHistoryPersist,
              @Inject(MAT_DIALOG_DATA) public ids: {prescriptions: PrescriptionRequestDetail[] , parentId: string, childId: string,  mode: string, isSurgery: boolean, type: number},
              public isDialog: Boolean,
             public icd11Persist: Icd11Persist) {

  }


  isNew(): boolean {
    return this.ids.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.mode === TCModalModes.EDIT;
  }
  isWizard(): boolean {
    return this.ids.mode === TCModalModes.WIZARD;
  }
  isCase(): boolean {
    return this.ids.mode === TCModalModes.CASE;
  }


  onCancel(): void {
    this.dialogRef.close();
  }


  appendString(value: string): string {
    value = value.trim();
    if (!this.prescriptionsDetailForList.diagnosis)
      return "\u2022  " + value;
    if (this.prescriptionsDetailForList.diagnosis.endsWith("\n"))
      return this.prescriptionsDetailForList.diagnosis + "\u2022  " + value;
    let x = this.prescriptionsDetailForList.diagnosis.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return y ? y + "\u2022  " + value : "\u2022  " + value;
  }

  load_icd11Diagnois() {
    this.icd11Persist.searchIcd11(5, 0, "", "").subscribe((result) => {
      if (result)
        console.log(result.data)
        this.icd11_diagnosis_options = result.data;
    })
  }

  onKeyDown(event: string, input): void {
    console.log(event, input.value)
    if (event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if (event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value = event.replace(/\n[^(\u2022)]/g, '\n\u2022  ' + event.match(/\n[^(\u2022)]/)[0][1])
    if (event && !input.value.endsWith("\n"))
      this.icd11Persist.icd11SearchText = input.value.split('\u2022').pop().trim();
    else
      this.icd11Persist.icd11SearchText = "";
    this.load_icd11Diagnois();
  }


  ngOnInit() {

    this.prescriptionssDisplayedColumns = this.ids.type == prescription_type.nurse_prescription ?['action', 'drug_id', 'quantity',"doctor_name"]:(this.ids.type == prescription_type.opd_prescription ? ['action', 'drug_id', 'quantity']: this.prescriptionssDisplayedColumns);
    this.persist.getForm_Encounter(this.ids.parentId).subscribe((encounter)=>{
      if(encounter)
        {
        if(this.isNew()){
          this.prescriptionsDetailForList.store_type = storeType[encounter.service_type]
          this.itemPersist.transfer_store_type = storeType[encounter.service_type];
        }
      }
    })

    if (this.isNew() || this.isWizard()) {
      this.tcAuthorization.requireCreate("prescriptions");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText('patient', 'prescription');

      this.prescriptionsDetailForList = new PrescriptionsDetailForList();
      this.prescriptionsDetailForList.duration_unit = 1;
      this.prescriptionsDetailForList.frequency = Frequency.none;
      this.prescriptionsDetailForList.other_information = "";
      this.prescriptionsDetailForList.diagnosis = "";
      this.patientHistoryPersist.encounter_id = this.ids.parentId;
      this.patientHistoryPersist.searchPatientHistory (1,0,"","").subscribe(patient_history =>{
          this.prescriptionsDetailForList.diagnosis = patient_history.data[0]?.provisional_psychiatric_diagnosis
      })
    }else if(this.isCase() && this.ids.prescriptions.length){
      this.tcAuthorization.requireUpdate("prescriptions");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText('patient', 'prescription');
     this.nextEditPrescription()
    }
    else {
      this.tcAuthorization.requireUpdate("prescriptions");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText('patient', 'prescription');
      this.isLoadingResults = true;
      this.prescriptionsDetailForList = new PrescriptionsDetailForList();
      this.persist.getPrescriptions(this.ids.childId).subscribe(prescriptionsDetail => {
        this.prescriptionsDetailForList = prescriptionsDetail;
        // this.prescriptionsDetailForList.quantity=calculateQuantity(prescriptionsDetail.dose,prescriptionsDetail.duration,prescriptionsDetail.duration_unit,prescriptionsDetail.frequency);
        // this.itemPersist.getItem(prescriptionsDetail.drug_id).subscribe((result)=>{
        //   if(result){
        //     this.drugName=this.itemNav.itemName(result);
        //     this.prescriptionsDetailForList.strength = result.strength;
        //   }
        // })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }



  updateItems(event:string,input:string):void{
    this.itemPersist.itemSearchText = input.trim();
    this.loadItems();
  }

  loadItems(){
    this.itemPersist.searchItem(15,0,"","").subscribe((result)=>{
      if(result) {
        this.item_options = result.data;
      }
    })
  }

  nextEditPrescription(){
    this.isLoadingResults = true;
    this.prescriptionsDetailForList = new PrescriptionsDetailForList();
    this.persist.getPrescriptions(this.ids.prescriptions[0].prescription_id).subscribe(prescriptionsDetail => {
      this.prescriptionsDetailForList = prescriptionsDetail;
      this.ids.prescriptions = this.ids.prescriptions.filter(value => value !=this.ids.prescriptions[0])
      this.isLoadingResults = false;
    })
  }

  handleInput(event: string, input) {
    const inputs = input.value.split('\u2022 ') as Array<string>;
    this.updateItems(event, inputs[inputs.length - 1])
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }

  optionSelected(event: ItemDetail){
    this.drugName = this.itemNav.itemName(event);
    this.prescriptionsDetailForList.drug_id = event.id;
    this.prescriptionsDetailForList.strength = event.strength;
    this.prescriptionsDetailForList.dosage = event.strength;
    this.prescriptionsDetailForList.unit = event.unit;
    this.prescriptionsDetailForList.dose = 1;
  }

  searchDoctor() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorDetail[]) => {
      if (result) {
        this.prescriptionsDetailForList.doctor_name = result[0].first_name + " " + result[0].middle_name + " " + result[0].last_name
        this.prescriptionsDetailForList.provider_id = result[0].id
      }
    })
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.prescriptionsDetailForList.date_modified = this.tcUtilsDate.toTimeStamp(this.date_modified);
    this.prescriptionssData.forEach((prescription) => {
      this.persist.addPrescriptions(this.ids.parentId, prescription
        //   {
        //   id: this.ids.parentId,
        //   is_surgery: this.ids.isSurgery,
        //   data: this.prescriptionssData
        // }
        ).subscribe(value => {
          this.tcNotification.success("Prescriptions added");
          // this.prescriptionsDetail.id = value.id;
          this.dialogRef.close(this.prescriptionsDetailForList);
        }, error => {
          this.isLoadingResults = false;
        })
    })
  }

  calculateDosage(event){
    this.prescriptionsDetailForList.dosage = this.prescriptionsDetailForList.strength * event;
  }
  calculateDose(event){
    this.prescriptionsDetailForList.dose = event / this.prescriptionsDetailForList.strength;
  }
  onUpdate(): void {
    this.isLoadingResults = true;
    // if(this.drugName != this.prescriptionsDetailForList.drug)
    //   this.prescriptionsDetailForList.drug_id = undefined;
    // if(this.drugName !="")
    //  this.prescriptionsDetailForList.drug = this.drugName;
    // this.prescriptionsDetailForList.is_surgery = this.ids.isSurgery

    this.persist.updatePrescriptions(this.prescriptionsDetailForList).subscribe(value => {
      this.isLoadingResults = false;
      if(this.ids.prescriptions && this.ids.prescriptions.length){
        this.nextEditPrescription();
      }else{
        this.tcNotification.success("Prescriptions updated");
        this.dialogRef.close(this.prescriptionsDetailForList);
      }

    }, error => {
      this.isLoadingResults = false;
    })

  }

  onWizard(): void {
    this.dialogRef.close(this.prescriptionssData);
  }


  canSubmit(): boolean {
    if (this.prescriptionsDetailForList == null) {
      return false;
    }


    if (this.prescriptionsDetailForList.prescription == null || this.prescriptionsDetailForList.prescription == "" ) {
      return false;
    }
    // if(this.drugName == this.prescriptionsDetailForList.drug){
    //   if (this.prescriptionsDetailForList.dosage == null ) {
    //     return false;
    //   }
    //   if (this.prescriptionsDetailForList.unit == null ) {
    //     return false;
    //   }

    //   if (this.prescriptionsDetailForList.dose == null ) {
    //     return false;
    //   }

    //   if (this.prescriptionsDetailForList.frequency == null ) {
    //     return false;
    //   }

    //   if (this.prescriptionsDetailForList.duration == null) {
    //     return false;
    //   }
    //   if (this.prescriptionsDetailForList.duration_unit == null) {
    //     return false;
    //   }
    // }
    return true;
  }


  addToList() {

    // if(this.drugName != this.prescriptionsDetailForList.drug)
    //   this.prescriptionsDetailForList.drug_id = undefined;
    // this.prescriptionsDetailForList.prescription = this.drugName;
    const newPrescriptionToAdd: PrescriptionsDetailForList = this.prescriptionsDetailForList as PrescriptionsDetailForList;
    this.prescriptionssData = [newPrescriptionToAdd, ...this.prescriptionssData];
    this.resetData();
  }

  resetData() {
    const {provider_id,doctor_name,store_type} = this.prescriptionsDetailForList;
    let diagnosis = this.prescriptionsDetailForList.diagnosis;
    this.prescriptionsDetailForList = new PrescriptionsDetailForList();
    this.prescriptionsDetailForList.other_information = "";
    this.prescriptionsDetailForList.diagnosis = diagnosis;
    this.prescriptionsDetailForList.frequency = Frequency.none;
    this.prescriptionsDetailForList.duration_unit = 1;
    this.prescriptionsDetailForList.provider_id = provider_id;
    this.prescriptionsDetailForList.doctor_name = doctor_name;
    this.prescriptionsDetailForList.store_type = store_type;
    this.drugName=""
  }

  removePrescriptionFromList(prescriptionsDetail: any) {
    this.prescriptionssData = this.prescriptionssData.filter(it => it != prescriptionsDetail);
  }

  searchDrugs(): void {
    let dialogRef = this.itemNav.pickItems(true);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {

      if (result) {

        this.drugName = result[0].name;
        this.prescriptionsDetailForList.drug = this.drugName;
        this.prescriptionsDetailForList.drug_id = result[0].id;

      }

    });

  }

  pickIcd11() {
    const dialogRef = this.procedureOrderCodeNavigator.pick_icd_11();
    dialogRef.afterClosed().subscribe((result: Icd11Summary[] ) => {
      if (result) {
        this.prescriptionsDetailForList.diagnosis = result[0].category;
      }
    })
  }
  getQuantity(event) {
    this.hr = 1/ frequency_value[this.prescriptionsDetailForList.frequency].value
    this.prescriptionsDetailForList.quantity = calculateQuantity(this.prescriptionsDetailForList.dose,this.prescriptionsDetailForList.duration,this.prescriptionsDetailForList.duration_unit,this.prescriptionsDetailForList.frequency) || 0;
  }

  ngOnDestroy():void{
    this.itemPersist.itemSearchText = "";
    this.itemPersist.transfer_store_type = undefined;
  }
}
export function calculateQuantity(dose:number = 0,duration:number = 0,duration_unit:number = 0,frequency:number = 0):number{

  return  dose * Math.ceil(duration * duration_unit*24* frequency_value[frequency]?.value);
}



@Component({
  selector: 'app-prescriptions-edit',
  templateUrl: './prescriptions-edit.component.html',
  styleUrls: ['./prescriptions-edit.component.css']
})
export class PrescriptionsEditComponent extends PrescriptionsComponent {

  // @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  // @ViewChildren(MatSort) sorters = new QueryList<MatSort>();



  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsArray: TCUtilsArray,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PrescriptionsEditComponent>,
    public persist: Form_EncounterPersist,
    public itemNav: ItemNavigator,
    public tcUtilsDate: TCUtilsDate,
    public prescriptionDrugNavigator: PrescriptionDrugNavigator,
    public medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
    public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
    public itemPersist: ItemPersist,



    public doctorNavigator: DoctorNavigator,
    public patientHistoryPersist: PatientHistoryPersist,
    @Inject(MAT_DIALOG_DATA) public ids: {prescriptions: [], parentId: string, childId: string,prescripitons:[],  mode: string, isSurgery: boolean, type:number},
    public icd11Persist: Icd11Persist) {
    super(tcAuthorization, tcNotification, tcUtilsString, tcUtilsArray, appTranslation, null, persist, itemNav, tcUtilsDate, prescriptionDrugNavigator, medicationAdminstrationChartPersist, procedureOrderCodeNavigator, itemPersist, doctorNavigator, patientHistoryPersist, ids, true, icd11Persist) }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(review: PrescriptionsDetailForList){
    this.dialogRef.close(review)
  }

}


@Component({
  selector: 'app-prescriptions-edit-list',
  templateUrl: './prescriptions-edit.component.html',
  styleUrls: ['./prescriptions-edit.component.css']
})
export class PrescriptionsEditListComponent extends PrescriptionsComponent {


  // @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  // @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  @Input() encounterId: string;
  @Input() type: number = 100;
  @Output() savePhysicalExamintion = new EventEmitter()
  icd11_diagnosis_options: Icd11Summary[];
  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsArray: TCUtilsArray,
    public appTranslation: AppTranslation,
    public persist: Form_EncounterPersist,
    public itemNav: ItemNavigator,
    public tcUtilsDate: TCUtilsDate,

    public prescriptionDrugNavigator: PrescriptionDrugNavigator,
    public medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
    public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
    public itemPersist: ItemPersist,
    public doctorNavigator: DoctorNavigator,
    public patientHistoryPersist: PatientHistoryPersist, public icd11Persist: Icd11Persist, ) {
                super(tcAuthorization, tcNotification, tcUtilsString, tcUtilsArray, appTranslation, null, persist, itemNav, tcUtilsDate, prescriptionDrugNavigator, medicationAdminstrationChartPersist, procedureOrderCodeNavigator, itemPersist, doctorNavigator, patientHistoryPersist, { prescriptions: [],
                  childId: null, parentId: null, mode: null, isSurgery: false, type:prescription_type.doctor_prescription
                }, false, icd11Persist)
  }

  ngOnInit(): void {

    this.prescriptionsDetailForList = new PrescriptionsDetailForList()
    this.prescriptionsDetailForList.duration_unit = 1;
    this.prescriptionsDetailForList.frequency = Frequency.none;
    this.ids.type = this.type;
  }


  getDetail(): PrescriptionsDetailForList{
    return this.prescriptionsDetailForList
  }

  // saveResult(){
  //   if (!this.canSubmit()){
  //     this.tcNotification.error("Physical Examination is not added. Required field is not added")
  //     return;
  //   }
  //   this.physical_examinationDetail.encounter_id = this.encounterId
  //   if (this.physical_examinationDetail.id){
  //     this.onUpdate()
  //   } else {
  //     this.onAdd()
  //   }
  // }

  isNew(): boolean {
    return false
  }






  onKeyDown(event: string, input): void { }


}
