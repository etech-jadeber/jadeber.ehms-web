import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DiagnosisCodePickComponent } from './diagnosis-code-pick.component';

describe('DiagnosisCodePickComponent', () => {
  let component: DiagnosisCodePickComponent;
  let fixture: ComponentFixture<DiagnosisCodePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DiagnosisCodePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiagnosisCodePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
