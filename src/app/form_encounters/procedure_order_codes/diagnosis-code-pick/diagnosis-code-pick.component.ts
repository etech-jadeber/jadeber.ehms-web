import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TCIdMode} from "../../../tc/models";
import {AppTranslation} from "../../../app.translation";
import {Procedure_Order_CodePersist} from "../procedure_order_code.persist";
import {FormControl} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {DiagnosisCodes, DiagnosisOrderSummary} from "../procedure_order_code.model";
import { diagnosis_order } from 'src/app/app.enums';
import { TCNotification } from 'src/app/tc/notification';

@Component({
  selector: 'app-diagnosis-code-pick',
  templateUrl: './diagnosis-code-pick.component.html',
  styleUrls: ['./diagnosis-code-pick.component.css']
})
export class DiagnosisCodePickComponent implements OnInit {
  diagnosisSearchText: FormControl = new FormControl();
  diagnosisSearchLoading: boolean = false;
  diagnosisSearchResult: DiagnosisCodes[] = [];
  diagnosisSelection: DiagnosisCodes;
  addNewIcd:boolean=false;
  description:string;
  displayedColumns: string[] = ["selection", "code", "description"];

  constructor(@Inject(MAT_DIALOG_DATA) public idMode: TCIdMode,
              public appTranslation: AppTranslation,
              public persist: Procedure_Order_CodePersist,
              public dialogRef: MatDialogRef<DiagnosisCodePickComponent>,
              public tcNotification: TCNotification,
              ) {
  }

  ngOnInit() {
    this.diagnosisSearchText.valueChanges.pipe(debounceTime(1000)).subscribe(value => {
      this.searchICD10(value);
    });
    this.searchICD10("");
    this.addNewIcd=false;
  }

  searchICD10(searchQuery: string) {
    this.diagnosisSearchLoading = true;
    this.persist.searchICD10(searchQuery).subscribe((result: any[]) => {
      if (result.length > 3) {
        const resultToProcess: [] = result[3];
        const processedResult: DiagnosisCodes[] = [];
        resultToProcess.forEach(value => {
          processedResult.push({code: value[0], description: value[1]});
        })
        let pagesize:number =7-result[3].length
        if(pagesize==0){
        this.diagnosisSearchResult = processedResult; 
        this.addNewIcd = false;
        this.diagnosisSearchLoading = false;
        }
        else if(pagesize>0){
        this.persist.searchDiagnosisOrder(searchQuery,pagesize).subscribe((result)=>{          
          result.data.forEach(res => {
            processedResult.push({code: 'unassigned', description: res.description})
          });
        this.diagnosisSearchResult = processedResult;
        this.addNewIcd = this.diagnosisSearchResult.length==0;
        this.diagnosisSearchLoading = false;
        })
      }
      }
    }, error => {
      console.error(error);
      this.diagnosisSearchLoading = false;
    });
  }
  addNewICD():void{
    let diaginosisOrder:DiagnosisOrderSummary = new DiagnosisOrderSummary();
    diaginosisOrder.description=this.description;
    this.persist.addDiagnosisOrder(diaginosisOrder).subscribe((res)=>{
      if(res){
        this.tcNotification.success("new ICD added");
        this.searchICD10(this.diagnosisSearchText.value);
        this.description="";
      }
    }); 
  }
  returnSelected() {
    this.dialogRef.close(this.diagnosisSelection);
  }

  onCancel() {
    this.dialogRef.close(null);
  }

  setSelection(element: DiagnosisCodes) {
    this.diagnosisSelection = element;
  }
}
