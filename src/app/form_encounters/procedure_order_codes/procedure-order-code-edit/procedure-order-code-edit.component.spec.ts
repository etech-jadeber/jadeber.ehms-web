import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureOrderCodeEditComponent } from './procedure-order-code-edit.component';

describe('ProcedureOrderCodeEditComponent', () => {
  let component: ProcedureOrderCodeEditComponent;
  let fixture: ComponentFixture<ProcedureOrderCodeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOrderCodeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOrderCodeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
