import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TCIdMode} from "../../../tc/models";
import {DiagnosisCodes, Procedure_Order_CodeDetail} from "../procedure_order_code.model";
import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsString} from "../../../tc/utils-string";
import {TCNotification} from "../../../tc/notification";
import {Procedure_Order_CodePersist} from "../procedure_order_code.persist";
import {AppTranslation} from "../../../app.translation";
import {Procedure_OrderPersist, ProcedureStatus} from "../../procedure_orders/procedure_order.persist";
import {Procedure_Order_CodeNavigator} from "../procedure_order_code.navigator";
import { Procedure_TestsNavigator } from 'src/app/procedure_testss/procedure_tests.navigator';
import { Procedure_TestsSummary } from 'src/app/procedure_testss/procedure_tests.model';

@Component({
  selector: 'app-procedure-order-code-edit',
  templateUrl: './procedure-order-code-edit.component.html',
  styleUrls: ['./procedure-order-code-edit.component.css']
})
export class ProcedureOrderCodeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedure_order_codeDetail: Procedure_Order_CodeDetail;

  procedureType: ProcedureStatus[];
  diagnosisDescription: string;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public dialogRef: MatDialogRef<ProcedureOrderCodeEditComponent>,
              public procedureTestPicker:Procedure_TestsNavigator,
              public persist: Procedure_Order_CodePersist,
              public procedureOrderPersist: Procedure_OrderPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
    this.procedureType = this.procedureOrderPersist.procedureType;
    this.procedure_order_codeDetail = new Procedure_Order_CodeDetail();
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

  }


  canSubmit(): boolean {
    if (this.procedure_order_codeDetail == null) {
      return false;
    }

    if (this.procedure_order_codeDetail.procedure_code == null || this.procedure_order_codeDetail.procedure_code == "") {
      return false;
    }

    if (this.procedure_order_codeDetail.procedure_name == null || this.procedure_order_codeDetail.procedure_name == "") {
      return false;
    }


    if (this.procedure_order_codeDetail.diagnoses == null || this.procedure_order_codeDetail.diagnoses == "") {
      return false;
    }

    if (this.procedure_order_codeDetail.procedure_type == null || this.procedure_order_codeDetail.procedure_type == "") {
      return false;
    }

    return true;
  }

  onAdd() {
    this.dialogRef.close(this.procedure_order_codeDetail);
  }

  pickDiagnosisCode() {
    const dialogRef = this.procedureOrderCodeNavigator.pick_diagnosis_code();
    dialogRef.afterClosed().subscribe((result: DiagnosisCodes ) => {
      if (result) {
        this.diagnosisDescription = result.description;
        this.procedure_order_codeDetail.diagnoses = result.description;
      }
    })
  }


  pickDiagnosisTest() {
    const dialogRef = this.procedureTestPicker.pickProcedure_Testss();
    dialogRef.afterClosed().subscribe((result: Procedure_TestsSummary[] ) => {
      if (result) {
        this.procedure_order_codeDetail.procedure_name = result[0].name;
        this.procedure_order_codeDetail.procedure_code = result[0].code;
      }
    })
  }
}
