import {TCId} from "../../tc/models";

export class Procedure_Order_CodeSummary extends TCId {
  procedure_order_id : number;
  procedure_order_seq : number;
  procedure_code : string;
  procedure_name : string;
  procedure_source : string;
  diagnoses : string;
  do_not_send : boolean;
  procedure_order_title : string;
  transport : string;
  procedure_type : string;
}

export class Procedure_Order_CodeSummaryPartialList {
  data: Procedure_Order_CodeSummary[];
  total: number;
}

export class Procedure_Order_CodeDetail extends Procedure_Order_CodeSummary {
  procedure_order_id : number;
  procedure_order_seq : number;
  procedure_code : string;
  procedure_name : string;
  procedure_source : string;
  diagnoses : string;
  do_not_send : boolean;
  procedure_order_title : string;
  transport : string;
  procedure_type : string;
}

export class Procedure_Order_CodeDashboard {
  total: number;
}


export class DiagnosisCodes{
  code: string;
  description :string
}

export class DiagnosisOrderSummary extends TCId {
  description:string;
  }
  export class DiagnosisOrderSummaryPartialList {
    data: DiagnosisOrderSummary[];
    total: number;
  }