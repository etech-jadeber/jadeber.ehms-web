import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";import {TCIdMode, TCModalModes} from "../../tc/models";
import {TCModalWidths} from "../../tc/utils-angular";
import {DiagnosisCodePickComponent} from "./diagnosis-code-pick/diagnosis-code-pick.component";
import { Icd11PickComponent } from "src/app/icd_11/icd11-pick/icd11-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Procedure_Order_CodeNavigator {

  constructor(public dialog: MatDialog) {
  }


  pick_diagnosis_code(): MatDialogRef<DiagnosisCodePickComponent> {
    return this.dialog.open(DiagnosisCodePickComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
  }
  pick_icd_11(selectOne: boolean=false): MatDialogRef<Icd11PickComponent> {
    return this.dialog.open(Icd11PickComponent, {
      data: selectOne,
      width: TCModalWidths.medium
    });
  }

}
