import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {DiagnosisOrderSummary, DiagnosisOrderSummaryPartialList, Procedure_Order_CodeDetail, Procedure_Order_CodeSummaryPartialList} from "./procedure_order_code.model";
import {TCUrlParams, TCUtilsHttp} from "../../tc/utils-http";
import {TcDictionary, TCId} from "../../tc/models";
import {environment} from "../../../environments/environment";
import {TCUtilsString} from "../../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class Procedure_Order_CodePersist {

  procedure_order_codeSearchText: string = "";
  procedureOrderId: string;

  constructor(private http: HttpClient) {
  }

  searchProcedure_Order_Code(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_Order_CodeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("procedure_order_codes", this.procedure_order_codeSearchText, pageSize, pageIndex, sort, order);
    if (this.procedureOrderId) {
      url = TCUtilsString.appendUrlParameter(url, "procedure_order_id", this.procedureOrderId.toString());
    }
    url = TCUtilsString.appendUrlParameter(url, "procedure_order_codes_with_result", "-1");
    return this.http.get<Procedure_Order_CodeSummaryPartialList>(url);
  }

  searchProcedure_Order_Code_With_Result(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_Order_CodeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("procedure_order_codes", this.procedure_order_codeSearchText, pageSize, pageIndex, sort, order);
    if (this.procedureOrderId) {
      url = TCUtilsString.appendUrlParameter(url, "procedure_order_id", this.procedureOrderId.toString());
    }
    url = TCUtilsString.appendUrlParameter(url, "procedure_order_codes_with_result", "1");
    return this.http.get<Procedure_Order_CodeSummaryPartialList>(url);
  }

  clearFilter() {
    this.procedureOrderId = null;
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_order_codeSearchText;
    //add custom filters
    return fltrs;
  }

  addProcedure_Order_Code(item: Procedure_Order_CodeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_order_codes/", item);
  }

  deleteProcedure_Order_Code(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "procedure_order_codes/" + id);
  }

  searchICD10(searchQuery: string){
    const searchFor = "code,name";
    return this.http.get(environment.nationalLibraryOfMedicineAPIUri + `api/icd10cm/v3/search?sf=${searchFor}&authenticity_token&terms=${searchQuery}`);
  }

  searchDiagnosisOrder(searchQuery:string, pageSize:number): Observable<DiagnosisOrderSummaryPartialList>{
    let url = TCUtilsHttp.buildSearchUrl("diagnosis_order", searchQuery, pageSize, 0, "", "");
    return this.http.get<DiagnosisOrderSummaryPartialList>(url);
  }
  addDiagnosisOrder(item: DiagnosisOrderSummary): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "diagnosis_order/", item);
  }

}
