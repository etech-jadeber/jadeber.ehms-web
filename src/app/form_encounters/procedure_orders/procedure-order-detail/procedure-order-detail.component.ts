import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {Procedure_OrderDetail} from "../procedure_order.model";
import {Procedure_OrderPersist} from "../procedure_order.persist";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {AppTranslation} from "../../../app.translation";
import {TCUtilsDate} from "../../../tc/utils-date";
import {Procedure_Order_CodePersist} from "../../procedure_order_codes/procedure_order_code.persist";
import {Procedure_Order_CodeSummary} from "../../procedure_order_codes/procedure_order_code.model";
import {Procedure_Order_CodeNavigator} from "../../procedure_order_codes/procedure_order_code.navigator";
import {Procedure_OrderNavigator} from "../procedure_order.navigator";
import {DoctorPersist} from "../../../doctors/doctor.persist";
import {PatientPersist} from "../../../patients/patients.persist";
import {Procedure_ProviderPersist} from "../../../procedure_providers/procedure_provider.persist";

export enum Procedure_OrderTabs {
  overview,
  lab_result
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-procedure_order-detail',
  templateUrl: './procedure-order-detail.component.html',
  styleUrls: ['./procedure-order-detail.component.css']
})
export class Procedure_OrderDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  procedure_orderLoading: boolean = false;

  Procedure_OrderTabs: typeof Procedure_OrderTabs = Procedure_OrderTabs;
  activeTab: Procedure_OrderTabs = Procedure_OrderTabs.overview;
  //basics
  procedure_orderDetail: Procedure_OrderDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  procedureOrderCodes: Procedure_Order_CodeSummary[] = [];
  displayedColumns: String[] = ["procedure_type", "procedure_test", "procedure_code", "diagnosis_code"];

  procedureOrderId: string;



  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public jobPersist: JobPersist,
              public tCUtilsDate: TCUtilsDate,
              public appTranslation: AppTranslation,
              private procedureOrderCodePersist: Procedure_Order_CodePersist,
              public procedureOrderNavigator: Procedure_OrderNavigator,
              public procedure_orderPersist: Procedure_OrderPersist) {
    this.tcAuthorization.requireRead("procedure_orders");
    this.procedure_orderDetail = new Procedure_OrderDetail();
  }



  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_orders");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    this.procedureOrderCodePersist.clearFilter();
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.procedureOrderId = id;
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.procedure_orderLoading = true;
    this.procedure_orderPersist.getProcedure_Order(id).subscribe(procedure_orderDetail => {
      this.procedure_orderDetail = procedure_orderDetail;

      this.procedure_orderLoading = false;
      this.procedureOrderCodePersist.procedureOrderId = procedure_orderDetail.id;
      this.loadProcedureOrderCodes();
    }, error => {
      console.error(error);
      this.procedure_orderLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.procedure_orderDetail.id.toString());
  }


  back(): void {
    this.location.back();
  }


  private loadProcedureOrderCodes() {
    this.procedureOrderCodePersist.searchProcedure_Order_Code(100, 0, "", "").subscribe(
      result => {
        this.procedureOrderCodes = result.data;
      },
      error => {
        console.error(error);
      }
    )
  }

  deleteProcedureCodes(procedure_order_id: number) {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Order_Code");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedureOrderCodePersist.deleteProcedure_Order_Code(procedure_order_id.toString()).subscribe(response => {
          this.tcNotification.success("Procedure_Order_Code deleted");
          this.loadProcedureOrderCodes();
        }, error => {
        });
      }
    });
  }

  getNextProcedureOrderSeq() {
    if (this.procedureOrderCodes.length > 0) {
      const list = this.procedureOrderCodes.map(value => value.procedure_order_seq);
      const maxNumber = Math.max(...list);
      return maxNumber + 1;
    }
    return 1;
  }

  addProcedureOrderCode(procedure_order_id: number) {
    let dialogRef = this.procedureOrderNavigator.addProcedure_Order_Code();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.procedure_order_id = this.procedure_orderDetail.procedure_order_id;
        result.procedure_order_seq = this.getNextProcedureOrderSeq();
        this.procedureOrderCodePersist.addProcedure_Order_Code(result).subscribe(result => {
          this.tcNotification.success("Procedure_Order_Code added");
          this.reload();
        }, error => {
          console.error(error);
        });
      }
    });
  }
}
