import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {Procedure_OrderDetail} from "../procedure_order.model";
import {Procedure_OrderPersist, ProcedureStatus} from "../procedure_order.persist";
import {TCIdMode, TCModalModes} from "../../../tc/models";
import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsString} from "../../../tc/utils-string";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";
import {PatientSummary} from "../../../patients/patients.model";
import {PatientNavigator} from "../../../patients/patients.navigator";
import {physican_type} from "../../../app.enums";
import {DoctorSummary} from "../../../doctors/doctor.model";
import {DoctorNavigator} from "../../../doctors/doctor.navigator";
import {TCUtilsDate} from "../../../tc/utils-date";
import {Form_EncounterPersist} from '../../form_encounter.persist';


import {DiagnosisCodes, Procedure_Order_CodeSummary} from "../../procedure_order_codes/procedure_order_code.model";
import {Procedure_ProviderNavigator} from "../../../procedure_providers/procedure_provider.navigator";
import {Procedure_ProviderSummary} from "../../../procedure_providers/procedure_provider.model";
import {Procedure_Order_CodeNavigator} from "../../procedure_order_codes/procedure_order_code.navigator";
import { Procedure_TestsPickComponent } from 'src/app/procedure_testss/procedure-tests-pick/procedure-tests-pick.component';
import { Procedure_TestsNavigator } from 'src/app/procedure_testss/procedure_tests.navigator';
import { Procedure_TestsSummary } from 'src/app/procedure_testss/procedure_tests.model';

@Component({
  selector: 'app-procedure_order-edit',
  templateUrl: './procedure-order-edit.component.html',
  styleUrls: ['./procedure-order-edit.component.css']
})
export class Procedure_OrderEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedure_orderDetail: Procedure_OrderDetail;
  patientFullName: string;
  physicianFullName: string;
  local_date_collected: Date;

  procedurePriority: ProcedureStatus[] = [
    {name: "unassigned", value: ""},
    {name: "normal", value: "normal"},
    {name: "high", value: "high"},
  ];

  procedureStatus: ProcedureStatus[] = [
    {name: "unassigned", value: ""},
    {name: "pending", value: "pending"},
    {name: "routed", value: "routed"},
    {name: "complete", value: "complete"},
    {name: "canceled", value: "canceled"},
  ];

  idMode: TCIdMode;
  procedureTestName:string;
  showProcedureOrderCodeForm: boolean = false;

  procedureType: ProcedureStatus[];

  currentProcedureOrderCode: Procedure_Order_CodeSummary = new Procedure_Order_CodeSummary();
  procedureOrderCodes: Procedure_Order_CodeSummary[] = [];
  displayedColumns: String[] = ["action", "procedure_type", "procedure_test", "diagnosis_code"];
  private mySequence: number = 1;
  public labName: string;
  diagnosisDescription: string;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              private procedureTestPicker:Procedure_TestsNavigator,
              public dialogRef: MatDialogRef<Procedure_OrderEditComponent>,
              public persist: Procedure_OrderPersist,
              private encounterPersisit: Form_EncounterPersist,
              private patientNavigator: PatientNavigator,
              private procedureProviderNavigator: Procedure_ProviderNavigator,
              private physicianNavigator: DoctorNavigator,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public data: { idMode: TCIdMode, encounterId: string, id?: string }) {
    this.idMode = data.idMode;
    this.procedureType = this.persist.procedureType;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("procedure_orders");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("procedure", "procedure");
      this.procedure_orderDetail = new Procedure_OrderDetail();
      this.procedure_orderDetail.encounter_id = this.data.encounterId;
      this.procedure_orderDetail.history_order = true;
      this.procedure_orderDetail.order_status = 'Pending';
      this.procedure_orderDetail.clinical_hx = "";
      this.local_date_collected =   this.tcUtilsDate.now();
      this.procedure_orderDetail.date_collected =  this.tcUtilsDate.toTimeStamp(this.tcUtilsDate.now());
      this.encounterPersisit.getForm_Encounter(this.procedure_orderDetail.encounter_id.toString()).subscribe(
        res => {
          this.procedure_orderDetail.provider_id = res.provider_id;
          this.procedure_orderDetail.patient_id = res.pid;
        }
      );
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("procedure_orders");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("procedure", "procedure");
      this.isLoadingResults = true;
      this.persist.getProcedure_Order(this.idMode.id).subscribe(procedure_orderDetail => {
        this.procedure_orderDetail = procedure_orderDetail;
        this.local_date_collected = this.tcUtilsDate.mySqlDateTimeToJsDate(this.procedure_orderDetail.date_collected);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateProcedure_Order(this.procedure_orderDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Order updated");
      this.dialogRef.close(this.procedure_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.procedure_orderDetail == null) {
      return false;
    }

    if (this.procedure_orderDetail.provider_id == null) {
      return false;
    }

    if (this.procedure_orderDetail.patient_id == null) {
      return false;
    }

    if (this.procedure_orderDetail.lab_id == null) {
      return false;
    }

    if (this.local_date_collected == null) {
      return false;
    }

    this.procedure_orderDetail.date_collected = this.tcUtilsDate.toTimeStamp(this.local_date_collected);

    if (this.procedure_orderDetail.order_priority == null) {
      return false;
    }

    if (this.procedure_orderDetail.patient_instructions == null) {
      return false;
    }

    if (this.procedure_orderDetail.clinical_hx == null) {
      return false;
    }

    if (this.procedure_orderDetail.history_order == null) {
      return false;
    }

    return true;
  }


  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.procedure_orderDetail.patient_id = result[0].id;
      }
    });
  }

  searchPhysician() {
    let dialogRef = this.physicianNavigator.pickDoctors(true, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.physicianFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.procedure_orderDetail.provider_id = result[0].open_emr_id;
      }
    });
  }

  addProcedureOrderCode() {
    if (this.currentProcedureOrderCode) {
      this.currentProcedureOrderCode.procedure_order_seq = this.mySequence;
      this.procedureOrderCodes = [...this.procedureOrderCodes, this.currentProcedureOrderCode];
      this.currentProcedureOrderCode = new Procedure_Order_CodeSummary();
      this.mySequence = this.mySequence + 1;
    }
  }

  removeProcedureOrderCode(procedure_order_seq: number) {
    this.procedureOrderCodes = this.procedureOrderCodes.filter(value => value.procedure_order_seq !== procedure_order_seq);
  }

  canSubmitProcedureCode() {
    if (this.currentProcedureOrderCode.procedure_name == null) {
      return false;
    }
    if (this.currentProcedureOrderCode.procedure_code == null) {
      return false;
    }
    if (this.currentProcedureOrderCode.procedure_type == null) {
      return false;
    }
    if (this.currentProcedureOrderCode.diagnoses == null) {
      return false;
    }
    return true;
  }

  saveProcedure() {
    this.procedure_orderDetail["procedure_order_codes"] = this.procedureOrderCodes;
    this.isLoadingResults = true;
    this.persist.addProcedure_Order(this.procedure_orderDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Order added");
      this.procedure_orderDetail.id = value.id;
      this.dialogRef.close(this.procedure_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  toggleShowProcedureOrderCodeForm() {
    this.showProcedureOrderCodeForm = !this.showProcedureOrderCodeForm;
  }

  searchProcedureProvide() {
    let dialogRef = this.procedureProviderNavigator.pickProcedure_Providers(true);
    dialogRef.afterClosed().subscribe((result: Procedure_ProviderSummary[]) => {
      if (result && result[0].active) {
        this.labName = result[0].name;
        this.procedure_orderDetail.lab_id = result[0].id;
      }
    });
  }

  pickDiagnosisCode() {
    const dialogRef = this.procedureOrderCodeNavigator.pick_diagnosis_code();
    dialogRef.afterClosed().pipe().subscribe((result: DiagnosisCodes) => {
      if (result) {
        this.diagnosisDescription = result.description;
        this.currentProcedureOrderCode.diagnoses = result.description;
      }
    })
  }

  pickDiagnosisTest() {
    const dialogRef = this.procedureTestPicker.pickProcedure_Testss();
    dialogRef.afterClosed().subscribe((result: Procedure_TestsSummary[] ) => {
      if (result) {
        this.currentProcedureOrderCode.procedure_code = result[0].code;
        this.currentProcedureOrderCode.procedure_name = result[0].name;

      }
    })
  }
}
