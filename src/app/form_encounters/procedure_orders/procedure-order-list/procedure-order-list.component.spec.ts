import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureOrderListComponent } from './procedure-order-list.component';

describe('ProcedureOrderListComponent', () => {
  let component: ProcedureOrderListComponent;
  let fixture: ComponentFixture<ProcedureOrderListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
