import {AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {Procedure_OrderPersist} from "../procedure_order.persist";
import {Procedure_OrderNavigator} from "../procedure_order.navigator";
import {Procedure_OrderSummary, Procedure_OrderSummaryPartialList} from "../procedure_order.model";
import {TCUtilsDate} from "../../../tc/utils-date";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {AppTranslation} from "../../../app.translation";
import {Procedure_ProviderNavigator} from "../../../procedure_providers/procedure_provider.navigator";
import {Procedure_ProviderPersist} from "../../../procedure_providers/procedure_provider.persist";


@Component({
  selector: 'app-procedure_order-list',
  templateUrl: './procedure-order-list.component.html',
  styleUrls: ['./procedure-order-list.component.css']
})
export class Procedure_OrderListComponent implements OnInit, AfterViewInit, OnDestroy {

  procedure_ordersData: Procedure_OrderSummary[] = [];
  procedure_ordersTotalCount: number = 0;
  procedure_ordersSelectAll: boolean = false;
  procedure_ordersSelection: Procedure_OrderSummary[] = [];

  procedure_ordersDisplayedColumns: string[] = ["select", "action", "procedure_order_id", "date_collected", "date_ordered", "order_priority", "order_status", "patient_instructions", "lab_id"];
  procedure_ordersSearchTextBox: FormControl = new FormControl();
  procedure_ordersIsLoading: boolean = false;

  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();
  @ViewChild(MatPaginator, {static: true}) procedure_ordersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_ordersSort: MatSort;

  procedureProviders = [];

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public procedureOrderNavigator: Procedure_OrderNavigator,
              public appTranslation: AppTranslation,
              public procedure_orderPersist: Procedure_OrderPersist,
              public procedure_orderNavigator: Procedure_OrderNavigator,
              public procedureProviderNavigator: Procedure_ProviderNavigator,
              private procedureProvidePersist: Procedure_ProviderPersist,
              public jobPersist: JobPersist,
  ) {

  }

  ngAfterViewInit(): void {
    this.tcAuthorization.requireRead("procedure_orders");
    this.procedure_ordersSearchTextBox.setValue(this.procedure_orderPersist.procedure_orderSearchText);
    this.procedure_orderPersist.encounterId = this.encounterId;
    //delay subsequent keyup events
    this.procedure_ordersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_orderPersist.procedure_orderSearchText = value;
      this.searchProcedure_Orders();
    });
  }

  ngOnInit() {
    this.procedure_orderPersist.encounterId = this.encounterId;

    this.procedure_ordersSort.sortChange.subscribe(() => {
      this.procedure_ordersPaginator.pageIndex = 0;
      this.searchProcedure_Orders(true);
    });

    this.procedure_ordersPaginator.page.subscribe(() => {
      this.searchProcedure_Orders(true);
    });
    //start by loading items
    this.searchProcedure_Orders();
  }

  searchProcedure_Orders(isPagination: boolean = false): void {


    let paginator = this.procedure_ordersPaginator;
    let sorter = this.procedure_ordersSort;

    this.procedure_ordersIsLoading = true;
    this.procedure_ordersSelection = [];

    this.procedure_orderPersist.searchProcedure_Order(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: Procedure_OrderSummaryPartialList) => {
      this.procedure_ordersData = partialList.data;
      this.onResult.emit(partialList.total);

      partialList.data.forEach(value => {
        this.getAndStoreProcedureProvider(value.lab_id);
      })

      if (partialList.total != -1) {
        this.procedure_ordersTotalCount = partialList.total;
      }
      this.procedure_ordersIsLoading = false;
    }, error => {
      this.procedure_ordersIsLoading = false;
    });

  }

  getAndStoreProcedureProvider(providerId: string){
    this.procedureProvidePersist.getProcedure_Provider(providerId).subscribe(value => {
      this.procedureProviders.push(value);
    })
  }

  downloadProcedure_Orders(): void {
    if (this.procedure_ordersSelectAll) {
      this.procedure_orderPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_orders", true);
      });
    } else {
      this.procedure_orderPersist.download(this.tcUtilsArray.idsList(this.procedure_ordersSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_orders", true);
      });
    }
  }


  addProcedure_Order(): void {
    let dialogRef = this.procedure_orderNavigator.addProcedure_Order(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Orders();
      }
    });
  }

  editProcedure_Order(item: Procedure_OrderSummary) {
    let dialogRef = this.procedure_orderNavigator.editProcedure_Order(item.id.toString(), this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Orders();
      }
    });
  }

  deleteProcedure_Order(item: Procedure_OrderSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Order");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedure_orderPersist.deleteProcedure_Order(item.id.toString()).subscribe(response => {
          this.tcNotification.success("Procedure_Order deleted");
          this.searchProcedure_Orders();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }


  convertToDate(b: any) {
    return this.tcUtilsDate.mySqlDateTimeToJsDate(b);
  }

  ngOnDestroy(): void {
    this.procedure_orderPersist.clearFilter();
  }
}
