import {Component, Input, OnInit} from '@angular/core';
import {Procedure_OrderDetail} from "../procedure_order.model";
import {AppTranslation} from "../../../app.translation";
import {Procedure_OrderNavigator} from "../procedure_order.navigator";
import {TCUtilsDate} from "../../../tc/utils-date";
import {Procedure_OrderPersist} from "../procedure_order.persist";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {TCAuthorization} from "../../../tc/authorization";
import {Form_EncounterNavigator} from "../../form_encounter.navigator";
import {DoctorPersist} from "../../../doctors/doctor.persist";
import {PatientPersist} from "../../../patients/patients.persist";
import {Procedure_ProviderPersist} from "../../../procedure_providers/procedure_provider.persist";
import {TCUtilsArray} from "../../../tc/utils-array";

@Component({
  selector: 'app-procedure-order-overview',
  templateUrl: './procedure-order-overview.component.html',
  styleUrls: ['./procedure-order-overview.component.css']
})
export class ProcedureOrderOverviewComponent implements OnInit {

  procedure_orderDetail: Procedure_OrderDetail;
  @Input() procedureOrderId: string;

  medicalPractitioners = [];
  patients = [];
  procedureProviders = []

  constructor(public appTranslation: AppTranslation,
              public procedure_orderPersist: Procedure_OrderPersist,
              public jobPersist: JobPersist,
              public tCUtilsArray: TCUtilsArray,
              private doctorsPersist: DoctorPersist,
              private patientsPersist: PatientPersist,
              private procedureProvidersPersist: Procedure_ProviderPersist,
              public tcAuthorization: TCAuthorization,
              public form_EncounterNavigator: Form_EncounterNavigator,
              public tCUtilsDate: TCUtilsDate,) {
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_orders");
  }

  ngAfterViewInit(): void {
    this.loadDetails(this.procedureOrderId);
  }

  loadDetails(id: string): void {
    this.procedure_orderPersist.getProcedure_Order(id).subscribe(procedure_orderDetail => {
      this.procedure_orderDetail = procedure_orderDetail;

      this.getAndSavePatients(this.procedure_orderDetail.patient_id);
      this.getAndSaveProcedureProviders(this.procedure_orderDetail.lab_id);
      this.getAndSaveMedicalPractitioners(this.procedure_orderDetail.provider_id);

    }, error => {
      console.error(error);
    });
  }

  printProcedure_Order(): void {
    this.procedure_orderPersist.print(this.procedure_orderDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print procedure_order", true);
    });
  }

  getAndSaveMedicalPractitioners(medicalPractitionersId: String) {
    this.doctorsPersist.getDoctor(medicalPractitionersId).subscribe(value => {
      this.medicalPractitioners.push(value);
    })
  }

  getAndSavePatients(patientId: string) {
    this.patientsPersist.getPatient(patientId).subscribe(value => {
      this.patients.push(value);
    })
  }

  getAndSaveProcedureProviders(labId: string) {
    this.procedureProvidersPersist.getProcedure_Provider(labId).subscribe(value => {
      this.procedureProviders.push(value);
    })
  }

}
