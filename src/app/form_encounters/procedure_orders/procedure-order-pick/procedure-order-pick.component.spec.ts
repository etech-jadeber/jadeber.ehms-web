import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureOrderPickComponent } from './procedure-order-pick.component';

describe('ProcedureOrderPickComponent', () => {
  let component: ProcedureOrderPickComponent;
  let fixture: ComponentFixture<ProcedureOrderPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOrderPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOrderPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
