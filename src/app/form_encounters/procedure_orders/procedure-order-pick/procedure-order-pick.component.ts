import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {Procedure_OrderSummary, Procedure_OrderSummaryPartialList} from "../procedure_order.model";
import {Procedure_OrderPersist} from "../procedure_order.persist";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {AppTranslation} from "../../../app.translation";


@Component({
  selector: 'app-procedure_order-pick',
  templateUrl: './procedure-order-pick.component.html',
  styleUrls: ['./procedure-order-pick.component.css']
})
export class Procedure_OrderPickComponent implements OnInit {

  procedure_ordersData: Procedure_OrderSummary[] = [];
  procedure_ordersTotalCount: number = 0;
  procedure_ordersSelection: Procedure_OrderSummary[] = [];
  procedure_ordersDisplayedColumns: string[] = ["select", 'procedure_order_id', 'provider_id', 'patient_id', 'encounter_id', 'date_collected', 'date_ordered', 'order_priority', 'order_status', 'patient_instructions', 'activity', 'control_id', 'lab_id', 'specimen_type', 'specimen_location', 'specimen_volume', 'date_transmitted', 'clinical_hx', 'external_id', 'history_order', 'order_diagnosis'];

  procedure_ordersSearchTextBox: FormControl = new FormControl();
  procedure_ordersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_ordersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_ordersSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public procedure_orderPersist: Procedure_OrderPersist,
              public dialogRef: MatDialogRef<Procedure_OrderPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("procedure_orders");
    this.procedure_ordersSearchTextBox.setValue(procedure_orderPersist.procedure_orderSearchText);
    //delay subsequent keyup events
    this.procedure_ordersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_orderPersist.procedure_orderSearchText = value;
      this.searchProcedure_Orders();
    });
  }

  ngOnInit() {

    this.procedure_ordersSort.sortChange.subscribe(() => {
      this.procedure_ordersPaginator.pageIndex = 0;
      this.searchProcedure_Orders();
    });

    this.procedure_ordersPaginator.page.subscribe(() => {
      this.searchProcedure_Orders();
    });

    //set initial picker list to 5
    this.procedure_ordersPaginator.pageSize = 5;

    //start by loading items
    this.searchProcedure_Orders();
  }

  searchProcedure_Orders(): void {
    this.procedure_ordersIsLoading = true;
    this.procedure_ordersSelection = [];

    this.procedure_orderPersist.searchProcedure_Order(this.procedure_ordersPaginator.pageSize,
      this.procedure_ordersPaginator.pageIndex,
      this.procedure_ordersSort.active,
      this.procedure_ordersSort.direction).subscribe((partialList: Procedure_OrderSummaryPartialList) => {
      this.procedure_ordersData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_ordersTotalCount = partialList.total;
      }
      this.procedure_ordersIsLoading = false;
    }, error => {
      this.procedure_ordersIsLoading = false;
    });

  }

  markOneItem(item: Procedure_OrderSummary) {
    if (!this.tcUtilsArray.containsId(this.procedure_ordersSelection, item.id)) {
      this.procedure_ordersSelection = [];
      this.procedure_ordersSelection.push(item);
    } else {
      this.procedure_ordersSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.procedure_ordersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
