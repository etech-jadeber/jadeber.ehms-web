import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureOrderResultComponent } from './procedure-order-result.component';

describe('ProcedureOrderResultComponent', () => {
  let component: ProcedureOrderResultComponent;
  let fixture: ComponentFixture<ProcedureOrderResultComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOrderResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOrderResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
