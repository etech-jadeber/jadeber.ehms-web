import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Procedure_resultPersist} from "../../../procedure_providers/pending-review-result/procedure_result.persist";
import {ReportResultUnion} from "../../../procedure_providers/procedure_report.model";
import {TCUtilsDate} from "../../../tc/utils-date";
import {AppTranslation} from "../../../app.translation";

@Component({
  selector: 'app-procedure-order-result',
  templateUrl: './procedure-order-result.component.html',
  styleUrls: ['./procedure-order-result.component.css']
})
export class ProcedureOrderResultComponent implements OnInit {

  reportResultUnionResult: ReportResultUnion;

  constructor(public procedureOrderResultPersist: Procedure_resultPersist,
              public dialogRef: MatDialogRef<ProcedureOrderResultComponent>,
              public tcUtilsDate: TCUtilsDate,
              public appTranslation: AppTranslation,
              @Inject(MAT_DIALOG_DATA) public data: {
                procedure_order_seq: String,
                procedure_order_id: string
              }
  ) {
  }

  ngOnInit() {
    this.procedureOrderResultPersist.getProcedureOrderResult(this.data.procedure_order_seq.toString(), this.data.procedure_order_id.toString()).subscribe(
      result => {
        this.reportResultUnionResult = result;
      }, error => {
        console.error(error);
      }
    )
  }

  onCancel() {
    this.dialogRef.close();
  }
}
