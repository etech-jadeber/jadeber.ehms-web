import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureOrderResultsComponent } from './procedure-order-results.component';

describe('ProcedureOrderResultsComponent', () => {
  let component: ProcedureOrderResultsComponent;
  let fixture: ComponentFixture<ProcedureOrderResultsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureOrderResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureOrderResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
