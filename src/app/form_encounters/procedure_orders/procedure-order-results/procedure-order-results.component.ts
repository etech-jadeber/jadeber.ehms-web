import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AppTranslation} from "../../../app.translation";
import {Location} from "@angular/common";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {Procedure_Order_CodePersist} from "../../procedure_order_codes/procedure_order_code.persist";
import {Procedure_Order_CodeSummary} from "../../procedure_order_codes/procedure_order_code.model";
import {Procedure_OrderNavigator} from "../procedure_order.navigator";

@Component({
  selector: 'app-procedure-order-results',
  templateUrl: './procedure-order-results.component.html',
  styleUrls: ['./procedure-order-results.component.css']
})
export class ProcedureOrderResultsComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() procedureOrderId: string;
  procedureOrderCodes: Procedure_Order_CodeSummary[] = [];
  displayedColumns: String[] = ["procedure_type", "procedure_test", "procedure_code", "diagnosis_code", "action"];

  constructor(public appTranslation: AppTranslation,
              public tcUtilsAngular: TCUtilsAngular,
              private procedureOrderCodePersist: Procedure_Order_CodePersist,
              private procedureOrderNavigator: Procedure_OrderNavigator,
              private location: Location,) {
  }

  ngOnInit() {
  }

  back(): void {
    this.location.back();
  }

  ngAfterViewInit(): void {
    this.procedureOrderCodePersist.procedureOrderId = this.procedureOrderId;
    this.procedureOrderCodePersist.searchProcedure_Order_Code_With_Result(100, 0, "", "").subscribe(value => {
      this.procedureOrderCodes = value.data;
    }, error => {
      console.error(error);
    })
  }

  ngOnDestroy(): void {
    this.procedureOrderCodePersist.clearFilter()
  }

  viewResult(element: Procedure_Order_CodeSummary) {
    this.procedureOrderNavigator.openViewProcedureResultDialog(element.procedure_order_seq.toString(), element.procedure_order_id.toString());
  }
}
