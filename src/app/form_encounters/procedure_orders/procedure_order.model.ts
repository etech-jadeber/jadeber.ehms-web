import {TCId} from "../../tc/models";

export class Procedure_OrderSummary extends TCId {
  procedure_order_id: number;
  provider_id: string;
  patient_id: string;
  encounter_id: string;
  date_collected: number;
  date_ordered: number;
  order_priority: string;
  order_status: string;
  patient_instructions: string;
  activity: boolean;
  control_id: number;
  lab_id: string;
  specimen_type: string;
  specimen_location: string;
  specimen_volume: string;
  date_transmitted: number;
  clinical_hx: string;
  external_id: number;
  history_order: true;
  order_diagnosis: string;
}

export class Procedure_OrderSummaryPartialList {
  data: Procedure_OrderSummary[];
  total: number;
}

export class Procedure_OrderDetail extends Procedure_OrderSummary {
  procedure_order_id: number;
  provider_id: string;
  patient_id: string;
  encounter_id: string;
  date_collected: number;
  date_ordered: number;
  order_priority: string;
  order_status: string;
  patient_instructions: string;
  activity: boolean;
  control_id: number;
  lab_id: string;
  specimen_type: string;
  specimen_location: string;
  specimen_volume: string;
  date_transmitted: number;
  clinical_hx: string;
  external_id: number;

  order_diagnosis: string;
}

export class Procedure_OrderDashboard {
  total: number;
}
