import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";import {Procedure_OrderEditComponent} from "./procedure-order-edit/procedure-order-edit.component";
import {TCIdMode, TCModalModes} from "../../tc/models";
import {TCModalWidths} from "../../tc/utils-angular";
import {Procedure_OrderPickComponent} from "./procedure-order-pick/procedure-order-pick.component";
import {ProcedureOrderResultComponent} from "./procedure-order-result/procedure-order-result.component";
import {
  ProcedureOrderCodeEditComponent
} from "../procedure_order_codes/procedure-order-code-edit/procedure-order-code-edit.component";


@Injectable({
  providedIn: 'root'
})

export class Procedure_OrderNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  procedure_ordersUrl(): string {
    return "/procedure_orders";
  }

  procedure_orderUrl(id: string): string {
    return "/procedure_orders/" + id;
  }

  viewProcedure_Orders(): void {
    this.router.navigateByUrl(this.procedure_ordersUrl());
  }

  viewProcedure_Order(id: string): void {
    this.router.navigateByUrl(this.procedure_orderUrl(id));
  }

  editProcedure_Order(id: string, encounterId: number = -1): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_OrderEditComponent, {
      data: {idMode: new TCIdMode(id, TCModalModes.EDIT), encounterId: encounterId, id: id},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure_Order(encounterId: number): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_OrderEditComponent, {
      data: {idMode: new TCIdMode(null, TCModalModes.NEW), encounterId: encounterId},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  openViewProcedureResultDialog(procedure_order_seq: string, procedure_order_id: string): MatDialogRef<unknown,any> {
    return this.dialog.open(ProcedureOrderResultComponent, {
      data: {
        procedure_order_seq: procedure_order_seq,
        procedure_order_id: procedure_order_id
      },
      width: TCModalWidths.medium
    });
  }

  pickProcedure_Orders(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_OrderPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addProcedure_Order_Code(): MatDialogRef<ProcedureOrderCodeEditComponent> {
    return this.dialog.open(ProcedureOrderCodeEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
  }
}
