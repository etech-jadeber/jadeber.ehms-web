import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {
  Procedure_OrderDashboard,
  Procedure_OrderDetail,
  Procedure_OrderSummaryPartialList
} from "./procedure_order.model";
import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../../tc/utils-http";
import {TcDictionary, TCId} from "../../tc/models";
import {environment} from "../../../environments/environment";
import {TCUtilsString} from "../../tc/utils-string";

export interface ProcedureStatus {
  name: string;
  value: string;
}

@Injectable({
  providedIn: 'root'
})
export class Procedure_OrderPersist {

  procedure_orderSearchText: string = "";
  encounterId: number;

  procedureType: ProcedureStatus[] = [
    {name: "intervention", value: "intervention"},
    {name: "laboratory_test", value: "laboratory_test"},
    {name: "physical_exam", value: "physical_exam"},
    {name: "risk_category", value: "risk_category"},
    {name: "patient_characteristics", value: "patient_characteristics"},
    {name: "imaging", value: "imaging"},
    {name: "encounter_checkup_procedure", value: "enc_checkup_procedure"},
  ];

  constructor(private http: HttpClient) {
  }

  searchProcedure_Order(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_OrderSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_orders", this.procedure_orderSearchText, pageSize, pageIndex, sort, order);
    if (this.encounterId) {
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId.toString());
    }
    return this.http.get<Procedure_OrderSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_orderSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_orders/do", new TCDoParam("download_all", this.filters()));
  }

  procedure_orderDashboard(): Observable<Procedure_OrderDashboard> {
    return this.http.get<Procedure_OrderDashboard>(environment.tcApiBaseUri + "procedure_orders/dashboard");
  }

  getProcedure_Order(id: string): Observable<Procedure_OrderDetail> {
    return this.http.get<Procedure_OrderDetail>(environment.tcApiBaseUri + "procedure_orders/" + id);
  }

  addProcedure_Order(item: Procedure_OrderDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_orders/", item);
  }

  updateProcedure_Order(item: Procedure_OrderDetail): Observable<Procedure_OrderDetail> {
    return this.http.patch<Procedure_OrderDetail>(environment.tcApiBaseUri + "procedure_orders/" + item.id, item);
  }

  deleteProcedure_Order(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "procedure_orders/" + id);
  }

  procedure_ordersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_orders/do", new TCDoParam(method, payload));
  }

  procedure_orderDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_orders/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_orders/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_orders/" + id + "/do", new TCDoParam("print", {}));
  }


  clearFilter() {
    this.encounterId = null;
  }
}
