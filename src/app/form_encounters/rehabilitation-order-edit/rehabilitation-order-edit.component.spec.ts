import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RehabilitationOrderEditComponent } from './rehabilitation-order-edit.component';

describe('RehabilitationOrderEditComponent', () => {
  let component: RehabilitationOrderEditComponent;
  let fixture: ComponentFixture<RehabilitationOrderEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RehabilitationOrderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RehabilitationOrderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
