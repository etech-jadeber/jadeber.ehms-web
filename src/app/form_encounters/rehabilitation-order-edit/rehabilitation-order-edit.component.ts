import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCModalModes, TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Rehabilitation_OrderDetail} from "../form_encounter.model";
import {Form_EncounterPersist} from "../form_encounter.persist";
import { Physiotherapy_TypeSummary } from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { Physiotherapy_TypeNavigator } from 'src/app/physiotherapy_types/physiotherapy_type.navigator';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';


@Component({
  selector: 'app-rehabilitation-order-edit',
  templateUrl: './rehabilitation-order-edit.component.html',
  styleUrls: ['./rehabilitation-order-edit.component.css']
})
export class Rehabilitation_OrderEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  physiotherapyTypeName:string;
  rehabilitation_orderDetail: Rehabilitation_OrderDetail = new Rehabilitation_OrderDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Rehabilitation_OrderEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Form_EncounterPersist,
              public physiotherapyTypeNavigator: Physiotherapy_TypeNavigator,
              public physiotherapy_TypePersisit: Physiotherapy_TypePersist, 
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  isWizard(): boolean {
    return this.ids.context && this.ids.context['mode'] === TCModalModes.WIZARD
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew() || this.isWizard()) {
      this.title = this.appTranslation.getText("general", "new") + " "+this.appTranslation.getText("patient", "physiotherapy");
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general", "edit") + " "+this.appTranslation.getText("patient", "physiotherapy");
      this.isLoadingResults = true;
      this.persist.getRehabilitation_Order(this.ids.childId).subscribe(rehabilitation_orderDetail => {
        this.rehabilitation_orderDetail = rehabilitation_orderDetail;
        this.physiotherapy_TypePersisit.getPhysiotherapy_Type(this.rehabilitation_orderDetail.physiotherapy_type_id).subscribe((response)=>{
          if(response){
            this.physiotherapyTypeName=response.name;
          }
        })
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.rehabilitation_orderDetail == null){
          return false;
        }

     if (this.rehabilitation_orderDetail.note == null || this.rehabilitation_orderDetail.note  == "") {
                      return false;
                    }
      if (this.rehabilitation_orderDetail.treatment_per_week == null ) {
        return false;
      }
      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addRehabilitation_Order(this.ids.parentId, this.rehabilitation_orderDetail).subscribe(value => {
      this.tcNotification.success("Rehabilitation_Order added");
      this.rehabilitation_orderDetail.id = value.id;
      this.dialogRef.close(this.rehabilitation_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onWizard(): void {
    this.dialogRef.close(this.rehabilitation_orderDetail)
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateRehabilitation_Order(this.ids.parentId, this.rehabilitation_orderDetail).subscribe(value => {
      this.tcNotification.success("Rehabilitation_Order updated");
      this.dialogRef.close(this.rehabilitation_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  searchPhysiotherapyTypes() {
    let dialogRef = this.physiotherapyTypeNavigator.pickPhysiotherapy_Types(true);
    dialogRef.afterClosed().subscribe((result: Physiotherapy_TypeSummary[]) => {
      if (result) {
        this.physiotherapyTypeName = result[0].name
        this.rehabilitation_orderDetail.physiotherapy_type_id = result[0].id;
      }
    });
  }
}
