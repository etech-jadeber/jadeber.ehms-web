import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Review_Of_SystemEditComponent } from './review-of-system-edit.component';

describe('Review_Of_SystemEditComponent', () => {
  let component: Review_Of_SystemEditComponent;
  let fixture: ComponentFixture<Review_Of_SystemEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Review_Of_SystemEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Review_Of_SystemEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
