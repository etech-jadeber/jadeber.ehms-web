import {Component, OnInit, Inject, ViewChild, ElementRef, Input, Injectable} from '@angular/core';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";
import {COMMA, ENTER} from '@angular/cdk/keycodes';

import {TCIdMode, TCModalModes} from "../../tc/models";


import { Review_Of_SystemDetail, Review_Of_SystemSummary} from "../form_encounter.model";
import { Form_EncounterPersist} from "../form_encounter.persist";
import { Review_Of_System_OptionsSummary, Review_Of_System_OptionsSummaryPartialList } from 'src/app/review_of_system_optionss/review_of_system_options.model';
import { Form, FormControl } from '@angular/forms';
import { Review_Of_System_OptionsPersist } from 'src/app/review_of_system_optionss/review_of_system_options.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { JsonpInterceptor } from '@angular/common/http';

@Injectable()
abstract class  Review_Of_SystemComponent implements OnInit {

  selectable = true;
  removable = true;

  isLoadingResults: boolean = false;
  title: string;
   review_of_systemDetail:  Review_Of_SystemDetail;
   separatorKeysCodes: number[] = [ENTER, COMMA];
   review_of_system_optionssData: Review_Of_System_OptionsSummary[] = [];
   reviewCtl:FormControl = new FormControl();
   addedReview_of_system_optionssData: Review_Of_System_OptionsSummary[] = [];
   review_of_systemDatas:Review_Of_SystemSummary[]=[];
   review_of_system_optionssTotalCount: number = 0;
   review_of_system_optionssSelectAll:boolean = false;
   review_of_system_optionssSelection: Review_Of_System_OptionsSummary[] = [];
 
   review_of_system_optionssDisplayedColumns: string[] = ["select","action", "name","type", ];
   review_of_system_optionssSearchTextBox: FormControl = new FormControl();
   review_of_system_optionssIsLoading: boolean = false;

   @ViewChild(MatPaginator, {static: true}) review_of_system_optionssPaginator: MatPaginator;
   @ViewChild(MatSort, {static: true}) review_of_system_optionssSort: MatSort;
 

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public tcUtilsArray:TCUtilsArray,
              public  persist:  Form_EncounterPersist,
              public idMode: TCIdMode,
              public isDialog: Boolean) {

  }


  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate(this.appTranslation.getText("patient", "review_of_systems"));
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "review_of_system");
      this.review_of_systemDetail = new  Review_Of_SystemDetail();
      //set necessary defaults
      this.review_of_systemDetail.encounter_id = this.idMode.id;

      // this.searchReview_Of_System_Optionss();
    } else {
     this.tcAuthorization.requireUpdate(this.appTranslation.getText("patient", "review_of_systems"));
      this.title = this.appTranslation.getText("general","edit") +  " " +  this.appTranslation.getText("patient", "review_of_system");
      this.isLoadingResults=true;
      this.persist.getReview_Of_System(this.idMode.id).subscribe((res)=>{
        this.review_of_systemDetail =res;
        // this.searchReview_Of_System_Optionss();
        this.isLoadingResults=false;
      });


    }
  }

  action (review: Review_Of_SystemDetail){
  }

  onCancel (){}

  onAdd(): void {
    //console.table(this.addedReview_of_system_optionssData);
    
    //return
    this.isLoadingResults = true;
    this.persist.addReview_Of_System(this.review_of_systemDetail.encounter_id, this.review_of_systemDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success(this.appTranslation.getText("patient", "review_of_systems") + " " + this.appTranslation.getText("general", "added"));
      this. review_of_systemDetail.id = value.id;
      this.action(this.review_of_systemDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateReview_Of_System(this.review_of_systemDetail.id,this.review_of_systemDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success(this.appTranslation.getText("patient", "review_of_systems") + " " + this.appTranslation.getText("general", "updated"));
      this.action(this.review_of_systemDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })

  }

  makeAll(value: boolean) : void {
    this.review_of_systemDetail.general = value;
    this.review_of_systemDetail.head_and_neck = value;
    this.review_of_systemDetail.ent = value;
    this.review_of_systemDetail.eye = value;
    this.review_of_systemDetail.chest_lungs_breasts = value;
    this.review_of_systemDetail.cardiovascular = value;
    this.review_of_systemDetail.gastrointestinal = value;
    this.review_of_systemDetail.musculoskeletal = value;
    this.review_of_systemDetail.neurological = value;
  }

  resetField(){
    this.makeAll(null);
    this.review_of_systemDetail.general_remark = "";
    this.review_of_systemDetail.head_and_neck_remark = "";
    this.review_of_systemDetail.ent_remark = "";
    this.review_of_systemDetail.eye_remark = "";
    this.review_of_systemDetail.chest_lungs_breasts_remark = "";
    this.review_of_systemDetail.cardiovascular_remark = "";
    this.review_of_systemDetail.gastrointestinal_remark = "";
    this.review_of_systemDetail.neurological_remark = "";
    this.review_of_systemDetail.musculoskeletal_remark = "";
  }


  canSubmit():boolean{
        if (this.review_of_systemDetail == null){
            return false;
          }
          if (this.review_of_systemDetail.general == null){
            return false;
          }
          if (this.review_of_systemDetail.head_and_neck == null){
            return false;
          }
          if (this.review_of_systemDetail.ent == null){
            return false;
          }
          if (this.review_of_systemDetail.eye == null){
            return false;
          }
          if (this.review_of_systemDetail.chest_lungs_breasts == null){
            return false;
          }
          if (this.review_of_systemDetail.cardiovascular == null){
            return false;
          }
          if (this.review_of_systemDetail.gastrointestinal == null){
            return false;
          }
          if (this.review_of_systemDetail.musculoskeletal == null){
            return false;
          }
          if (this.review_of_systemDetail.neurological == null){
            return false;
          }
        return true;
      }
      toggle(selection:any[],item:any){
        this.tcUtilsArray.toggleSelection(selection,item);
        item.checked=!item.checked 
      }

      // searchReview_Of_System_Optionss(isPagination:boolean = false): void {
      //   this.review_of_system_optionsPersist.review_of_system_optionssDo("get_all",{}).subscribe(
      //     (res:Review_Of_System_OptionsSummary[])=>{
      //       this.review_of_system_optionssData = res;
      //         for(let rs of this.review_of_systemDatas){
      //            const rso=this.tcUtilsArray.getById(this.review_of_system_optionssData,rs.review_option_id)
      //             rso.checked=true;
      //             this.addedReview_of_system_optionssData.push(rso);
      //         }
      //     }
      //   )    
      //   }

     
}


@Component({
  selector: 'app-review-of-system-edit',
  templateUrl: './review-of-system-edit.component.html',
  styleUrls: ['./review-of-system-edit.component.css']
})
export class  Review_Of_SystemEditComponent extends Review_Of_SystemComponent {
 

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public tcUtilsArray:TCUtilsArray,
              public dialogRef: MatDialogRef< Review_Of_SystemEditComponent>,
              public  persist:  Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, tcUtilsArray, persist, idMode, true)
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(review: Review_Of_SystemDetail){
    this.dialogRef.close(review)
  }
}

@Component({
  selector: 'app-review-of-system-edit-list',
  templateUrl: './review-of-system-edit.component.html',
  styleUrls: ['./review-of-system-edit.component.css']
})
export class  Review_Of_SystemEditListComponent extends Review_Of_SystemComponent {
 
  @Input() encounterId: string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public tcUtilsArray:TCUtilsArray,
              public  persist:  Form_EncounterPersist,) {
                super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, tcUtilsArray, persist, {id: null, mode: TCModalModes.NEW}, false)
  }

  ngOnInit(): void {
    this.persist.searchReview_Of_System(this.encounterId, 1, 0, 'date_of_review', 'desc').subscribe(
      review => {
        this.review_of_systemDetail = review.data.length ? review.data[0] : new Review_Of_SystemDetail()
      }
    )
  }

  saveResult(){
    if (!this.canSubmit()){
      return;
    }
    this.review_of_systemDetail.encounter_id = this.encounterId;
    if (this.review_of_systemDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }

  isNew(): boolean {
    return false
  }

}