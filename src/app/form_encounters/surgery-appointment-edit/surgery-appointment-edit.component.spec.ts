import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Surgery_AppointmentEditComponent } from './surgery-appointment-edit.component';


describe('SurgeryAppointmentEditComponent', () => {
  let component: Surgery_AppointmentEditComponent;
  let fixture: ComponentFixture<Surgery_AppointmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Surgery_AppointmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Surgery_AppointmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
