import { Component, Inject, OnInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { OperationNoteNavigator } from 'src/app/operation_note/operation_note.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { ProcedureSummary } from 'src/app/procedures/procedure.model';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { Procedure_TypeSummary } from 'src/app/procedure_types/procedure_type.model';
import { Surgery_TypeSummary } from 'src/app/surgery_types/surgery_type.model';
import { Surgery_TypeNavigator } from 'src/app/surgery_types/surgery_type.navigator';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { AppTranslation } from "../../app.translation";
import { TCAuthorization } from "../../tc/authorization";
import { TCId, TCParentChildIds } from "../../tc/models";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { Surgery_AppointmentDetail } from '../form_encounter.model';
import { Form_EncounterPersist } from '../form_encounter.persist';
import { DiagnosisCodes } from '../procedure_order_codes/procedure_order_code.model';
import { Procedure_Order_CodeNavigator } from '../procedure_order_codes/procedure_order_code.navigator';
import { SurgeryRoomNavigator } from 'src/app/surgery_room/surgery_room.navigator';
import { SurgeryRoomDetail } from 'src/app/surgery_room/surgery_room.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { OperationNotePersist } from 'src/app/operation_note/operation_note.persist';
import { Icd11Summary } from 'src/app/icd_11/icd_11.model';
import { FormControl } from '@angular/forms';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';





@Component({
  selector: 'app-surgery-appointment-edit',
  templateUrl: './surgery-appointment-edit.component.html',
  styleUrls: ['./surgery-appointment-edit.component.css']
})
export class Form_Encounter_Surgery_AppointmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  surgery_appointmentDetail: Surgery_AppointmentDetail;
  procedureTypeName: string;
  patientName: string;
  doctorName: string;
  start_time:Date;
  end_time:Date;
  appointed_time: string;
  dateTime: string;
  surgeryRoom:string;
  duration:string;
  min:number;
  hr: number;
  schedule_date : string;
  schedule_time : string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Form_Encounter_Surgery_AppointmentEditComponent>,
              public  persist: Form_EncounterPersist,
              public surgeryTypeNavigator: Surgery_TypeNavigator,
              public patientNavigator: PatientNavigator,
              public doctorNavigator: DoctorNavigator,
              public tCUtilsDate: TCUtilsDate,
              public procedureOrderCodeNavigator: Procedure_Order_CodeNavigator,
              public procedureNavigator: ProcedureNavigator,
              public operationNoteNavigator: OperationNoteNavigator,
              public formEncounterPersist: Form_EncounterPersist,
              public surgeryRoomNavigator: SurgeryRoomNavigator,
              public doctorPersist: DoctorPersist,
              public filePersist: FilePersist,
              private http:HttpClient,
              public operationNotePersist: OperationNotePersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  dateAndTime(timedate: Date): [string, string]{
    const date = `${timedate.getFullYear()}-${('0' + (timedate.getMonth() + 1)).slice(-2)}-${('0' + timedate.getDate()).slice(-2)}`
    const time = `${('0' + timedate.getHours()).slice(-2)}:${('0' + timedate.getMinutes()).slice(-2)}`
    return [date, time];
  }

  ngOnInit() {
    const today = new Date()
    const [date, time] = this.dateAndTime(today)
    this.dateTime = `${date}T${time}`
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("surgery_appointments");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "surgery_appointment");
      this.surgery_appointmentDetail = new Surgery_AppointmentDetail();
      this.surgery_appointmentDetail.surgery_type = -1;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("surgery_appointments");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "surgery_appointment");
      this.isLoadingResults = true;
      this.persist.getSurgery_Appointment(this.ids.childId).subscribe(partialList => {
      this.surgery_appointmentDetail = partialList;
      this.surgery_appointmentDetail.surgery_room = this.tcUtilsString.invalid_id
      this.hr =0;
      this.min = 0;
      this.isLoadingResults = false;
      if(this.surgery_appointmentDetail.surgery_date){
        const [date, time] = this.dateAndTime(new Date(this.surgery_appointmentDetail.surgery_date * 1000))
        this.appointed_time = `${date}T${time}`
        this.schedule_date = date
        this.schedule_time =time;
      }
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.surgery_appointmentDetail.duration = this.hr * 3600 + this.min *60
    // parseInt(this.duration.slice(0,2))*3600+parseInt(this.duration.slice(3))*60;
    this.surgery_appointmentDetail.surgery_date = new Date(this.appointed_time).getTime() / 1000;
    this.persist.addSurgery_Appointment(this.ids.parentId, this.surgery_appointmentDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "added"));
      this.surgery_appointmentDetail.id = value.id;
      this.dialogRef.close(this.surgery_appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    // this.surgery_appointmentDetail.duration = this.hr * 3600 + this.min *60;
    // parseInt(this.duration.slice(0,2))*3600+parseInt(this.duration.slice(3))*60;
    this.surgery_appointmentDetail.surgery_date = new Date(`${this.schedule_date}T${this.schedule_time}`).getTime() / 1000;
    // this.doctorPersist.duration_time=this.hr * 3600 + this.min *60;
    // this.doctorPersist.surgery_date=new Date(this.appointed_time).getTime() / 1000;
    // this.operationNotePersist.surgery_date=new Date(this.appointed_time).getTime() / 1000;
    // this.formEncounterPersist.getForm_Encounter(this.surgery_appointmentDetail.encounter_id).subscribe(form_encounter => {
    // let dialogRef = this.operationNoteNavigator.addOperationNote(this.surgery_appointmentDetail.id)
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result){
    //     this.surgery_appointmentDetail.operation_note_id = result.id;
        this.persist.updateSurgery_Appointment(this.surgery_appointmentDetail).subscribe(value => {
          this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "updated"));
          this.dialogRef.close(this.surgery_appointmentDetail);
          this.isLoadingResults = false;
        }, error => {
          this.isLoadingResults = false;
        })
    //  }
    //   else{
    //     this.isLoadingResults = false;
    //   }
    // })
    // })

  }

  documentUpload(fileInputEvent: any): void {

    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.persist.documentUploadUri(this.ids.parentId), fd).subscribe(response => {
      this.surgery_appointmentDetail.consent_id = (<TCId>response).id;
    });
  }

  canSubmit():boolean{

        if (this.surgery_appointmentDetail.planned_procedure == null || this.surgery_appointmentDetail.planned_procedure == ""){
          return false;
        }

        if (this.surgery_appointmentDetail.diagnosis == null || this.surgery_appointmentDetail.diagnosis == ""){
          return false;
        }

        if (this.surgery_appointmentDetail.surgery_type == -1){
          return false;
        }
        if (this.appointed_time == null){
          return false;
        }

        // if(this.surgery_appointmentDetail.consent_id == null){
        //   return false
        // }
        // if(!this.duration.match("^[0-9]{2}:[0-9]{2}$")){
        //   return false;
        //  }
         
        // if (this.surgeryRoom == null){    
        //   return false;
        // }

        return true;
      }
      
  canUpdate():boolean{

    if (this.surgery_appointmentDetail == null){
      return false;
    }
    
    if (this.appointed_time == null){
      return false;
    }
    // if(!this.duration.match("^[0-9]{2}:[0-9]{2}$")){
    //   return false;
    //  }
    if(this.surgery_appointmentDetail.surgery_date == null){
      return false
    }
    if(this.surgery_appointmentDetail.diagnosis == null){
      return false
    }
    if (this.surgeryRoom == null){    
      return false;
    }
    return true;
  }

      searchProcedureType() {
        let dialogRef = this.procedureNavigator.pickProcedures(true);
        dialogRef.afterClosed().subscribe((result: ProcedureSummary[]) => {
          if (result) {
            this.procedureTypeName = result[0].name;
            this.surgery_appointmentDetail.planned_procedure = result[0].id;
          }
        });
      }

      searchSurgeryRoom(){
        this.appointed_time= `${this.schedule_date}T${this.schedule_time}`
        let dialogRef =this.surgeryRoomNavigator.pickSurgeryRooms(this.appointed_time,this.surgery_appointmentDetail.duration.toString());
        dialogRef.afterClosed().subscribe((result:SurgeryRoomDetail[])=>{
          if(result){
            this.surgeryRoom=result[0].name;
            this.surgery_appointmentDetail.surgery_room=result[0].id;
          }
        });
      }

      searchPatient(){
        let dialogRef = this.patientNavigator.pickPatients(true);
        dialogRef.afterClosed().subscribe((result: PatientSummary[]) => {
          if (result){
            this.patientName = result[0].fname + " " + result[0].lname;
            this.surgery_appointmentDetail.patient_id = result[0].uuidd;
          }
        })
      }

      searchDoctor(){
        let dialogRef = this.doctorNavigator.pickDoctors(true);
        dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
          if (result){
            this.doctorName = result[0].first_name + " " + result[0].last_name;
            this.surgery_appointmentDetail.doctor_id = result[0].id;
          }
        })
      }


      pickIcd11() {
        const dialogRef = this.procedureOrderCodeNavigator.pick_icd_11();
        dialogRef.afterClosed().subscribe((result: Icd11Summary[] ) => {
          if (result) {
            this.surgery_appointmentDetail.diagnosis = result[0].category;
          }
        })
      }

      ngOnDestroy():void{
        // this.doctorPersist.duration_time=0;
        // this.doctorPersist.surgery_date=0;
        this.operationNotePersist.surgery_date=0;
      }
      

}

