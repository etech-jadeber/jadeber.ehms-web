import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'full_date'
})
export class CustomDatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    // Check if the value is null, and return null if it is
    if (value === null || value === undefined) {
      return null;
    }

    // Multiply the input date by 1000 to convert it to milliseconds
    const dateInMillis = value * 1000;

    // Format the date using Angular's DatePipe
    const datePipe: DatePipe = new DatePipe('en-US');
    return datePipe.transform(dateInMillis, 'MMM dd, yyyy. h:mm a');
  }
}
