import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class MenuState {

    private menuState: BehaviorSubject<string> = new BehaviorSubject<string>("");
    private dataState: BehaviorSubject<[]> = new BehaviorSubject<[]>([]);
    private tabResponseState: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    currentMenuState = this.menuState.asObservable();
    currentTabResponseState = this.tabResponseState.asObservable();
    currentDataResponseState = this.dataState.asObservable();

    constructor() {
    }

    changeMenuState(newMenuState: string) {
        this.menuState.next(newMenuState);
    }

    sendTabMenuResponse(tab) {
        this.tabResponseState.next(tab);
    }

    getDataResponse(data) {
        this.dataState.next(data);
    }
}