import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMemberDetailComponent } from './group-member-detail.component';

describe('GroupMemberDetailComponent', () => {
  let component: GroupMemberDetailComponent;
  let fixture: ComponentFixture<GroupMemberDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupMemberDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMemberDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
