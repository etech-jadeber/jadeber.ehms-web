import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {GroupMemberDetail} from "../groupMember.model";
import {GroupMemberPersist} from "../groupMember.persist";
import {GroupMemberNavigator} from "../groupMember.navigator";

export enum GroupMemberTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-group_member-detail',
  templateUrl: './group-member-detail.component.html',
  styleUrls: ['./group-member-detail.component.scss']
})
export class GroupMemberDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  groupMemberIsLoading:boolean = false;
  
  GroupMemberTabs: typeof GroupMemberTabs = GroupMemberTabs;
  activeTab: GroupMemberTabs = GroupMemberTabs.overview;
  //basics
  groupMemberDetail: GroupMemberDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public groupMemberNavigator: GroupMemberNavigator,
              public  groupMemberPersist: GroupMemberPersist) {
    this.tcAuthorization.requireRead("group_members");
    this.groupMemberDetail = new GroupMemberDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("group_members");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.groupMemberIsLoading = true;
    this.groupMemberPersist.getGroupMember(id).subscribe(groupMemberDetail => {
          this.groupMemberDetail = groupMemberDetail;
          this.groupMemberIsLoading = false;
        }, error => {
          console.error(error);
          this.groupMemberIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.groupMemberDetail.id);
  }
  editGroupMember(): void {
    let modalRef = this.groupMemberNavigator.editGroupMember(this.groupMemberDetail.id);
    modalRef.afterClosed().subscribe(groupMemberDetail => {
      TCUtilsAngular.assign(this.groupMemberDetail, groupMemberDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.groupMemberPersist.print(this.groupMemberDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print group_member", true);
      });
    }

  back():void{
      this.location.back();
    }

}