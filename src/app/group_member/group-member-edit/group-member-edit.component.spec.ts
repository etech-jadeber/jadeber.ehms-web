import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMemberEditComponent } from './group-member-edit.component';

describe('GroupMemberEditComponent', () => {
  let component: GroupMemberEditComponent;
  let fixture: ComponentFixture<GroupMemberEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupMemberEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMemberEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
