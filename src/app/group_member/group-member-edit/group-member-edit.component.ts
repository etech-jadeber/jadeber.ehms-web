import {Component, OnInit, Inject} from '@angular/core';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { GroupMemberDetail } from '../groupMember.model';
import { GroupMemberPersist } from '../groupMember.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { group_member_status } from 'src/app/app.enums';
@Component({
  selector: 'app-group_member-edit',
  templateUrl: './group-member-edit.component.html',
  styleUrls: ['./group-member-edit.component.scss']
})export class GroupMemberEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientFullName:string;
  groupMemberDetail: GroupMemberDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<GroupMemberEditComponent>,
              public  persist: GroupMemberPersist,
              private patientPersist: PatientPersist,
              private patientNavigator: PatientNavigator,

              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("group_members");
      this.title = this.appTranslation.getText("general","new") +  " " + "group_member";
      this.groupMemberDetail = new GroupMemberDetail();
      console.log("edit",this.idMode.id)
      this.groupMemberDetail.session_therapy_id = this.idMode.id
      this.groupMemberDetail.note = ""
      this.groupMemberDetail.recommendation = ""
      this.groupMemberDetail.status = group_member_status.active

      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("group_members");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "group_member";
      this.isLoadingResults = true;
      this.persist.getGroupMember(this.idMode.id).subscribe(groupMemberDetail => {
        this.groupMemberDetail = groupMemberDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    
   
    this.persist.addGroupMember(this.groupMemberDetail).subscribe(value => {
      this.tcNotification.success("groupMember added");
      this.groupMemberDetail.id = value.id;
      this.dialogRef.close(this.groupMemberDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }
  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.groupMemberDetail.pid = result[0].id;
      }
    });
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    console.log("update",this.groupMemberDetail)

    this.persist.updateGroupMember(this.groupMemberDetail).subscribe(value => {
      this.tcNotification.success("group_member updated");
      this.dialogRef.close(this.groupMemberDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.groupMemberDetail == null){
            return false;
          }
          if(this.groupMemberDetail.pid == null || this.groupMemberDetail.pid == ""){
            return false
          }
          // if(this.groupMemberDetail.note == "" || this.groupMemberDetail.note == null){
          //   return false
          // }
          return true;

 }
 
 }