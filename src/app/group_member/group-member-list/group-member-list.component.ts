import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';

import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { GroupMemberDetail, GroupMemberSummary, GroupMemberSummaryPartialList } from '../groupMember.model';
import { GroupMemberPersist } from '../groupMember.persist';
import { GroupMemberNavigator } from '../groupMember.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { SocketService } from 'src/app/socketService';
import { NotificationComponent } from 'src/app/notification/notification.component';
import { NotificationPersist } from 'src/app/notification/notification.persist';
@Component({
  selector: 'app-group_member-list',
  templateUrl: './group-member-list.component.html',
  styleUrls: ['./group-member-list.component.scss']
})export class GroupMemberListComponent implements OnInit {
  groupMembersData: GroupMemberSummary[] = [];
  groupMembersTotalCount: number = 0;
  groupMemberSelectAll:boolean = false;
  groupMemberSelection: GroupMemberSummary[] = [];
  session_therapy_id :string;
  width:100
  groupMemberDetail: GroupMemberDetail;
  url:string;

 groupMembersDisplayedColumns: string[] = ["select","action","patient","note","recommendation","status","action2" ];
  groupMemberSearchTextBox: FormControl = new FormControl();
  groupMemberIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) groupMembersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) groupMembersSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public groupMemberPersist: GroupMemberPersist,
                public groupMemberNavigator: GroupMemberNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                private route: ActivatedRoute,
                // private socketService: SocketService,
                private notififcationComponent: NotificationComponent,
                private notificationPersist:NotificationPersist,
   

                



    ) {

        this.tcAuthorization.requireRead("group_members");
       this.groupMemberSearchTextBox.setValue(groupMemberPersist.groupMemberSearchText);
      //delay subsequent keyup events
      this.groupMemberSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.groupMemberPersist.groupMemberSearchText = value;
        this.searchGroup_members();
      });
    } ngOnInit() {
      
      console.log("pagesixe",this.notififcationComponent.pageSize)


      this.route.params.subscribe(params => {
        
        this.session_therapy_id = params.id
      });
   
      this.groupMembersSort.sortChange.subscribe(() => {
        this.groupMembersPaginator.pageIndex = 0;
        this.searchGroup_members(true);
      });

      this.groupMembersPaginator.page.subscribe(() => {
        this.searchGroup_members(true);
      });
      //start by loading items
      this.searchGroup_members();
    }

  searchGroup_members(isPagination:boolean = false): void {


    let paginator = this.groupMembersPaginator;
    let sorter = this.groupMembersSort;

    this.groupMemberIsLoading = true;
    this.groupMemberSelection = [];

    this.groupMemberPersist.groupMembersDo(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction,this.session_therapy_id,"getGroupMembers",{}).subscribe((partialList: GroupMemberSummaryPartialList) => {
      console.log("======================",partialList)
      this.groupMembersData = partialList.data;
      if (partialList.total != -1) {
        this.groupMembersTotalCount = partialList.total;
      }
      this.groupMemberIsLoading = false;
    }, error => {
      this.groupMemberIsLoading = false;
    });

  } downloadGroupMembers(): void {
    if(this.groupMemberSelectAll){
         this.groupMemberPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download group_member", true);
      });
    }
    else{
        this.groupMemberPersist.download(this.tcUtilsArray.idsList(this.groupMemberSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download group_member",true);
            });
        }
  }
addGroup_member(): void {
    let dialogRef = this.groupMemberNavigator.addGroupMember(this.session_therapy_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchGroup_members();
      }
    });
  }

  editGroupMember(item: GroupMemberSummary) {
    let dialogRef = this.groupMemberNavigator.editGroupMember(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteGroupMember(item: GroupMemberSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("group_member");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.groupMemberPersist.deleteGroupMember(item.id).subscribe(response => {
          this.tcNotification.success("group_member deleted");
          this.searchGroup_members();
        }, error => {
          
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    updateGroupMeber(data:any){
        
      

      this.groupMemberPersist.updateGroupMember(data).subscribe(value => {
    // ********************************************** socket notification ******************************

        // this.notificationPersist.addNotification({msg:"the group meber is update",notifier:106,notified:107,date:Math.round(new Date().getTime() / 1000),links:`/session_therapys/${this.session_therapy_id}`,target_type:"patient",target_id:data.pid}).subscribe((res:any)=>{
        //   console.log("some answer")
        //   this.socketService.groupMemebrUpdate({msg:{msg:"the group meber is update",notified:107,date:Math.round(new Date().getTime() / 1000)},room:"nurs"})
        //   this.socketService.sendNotfication({msg:"some msge",room:"nurs"});
        //   this.socketService.updateCount("nurs");
    
           
        // }, error => {
        //  console.log("error",error)
        // })

    // ********************************************** socket notification ******************************

        this.tcNotification.success("group_member updated");

      }, error => {
        this.tcNotification.error("something is wrong");

      })
    }
    onBlur(data:any){
      this.groupMemberPersist.updateGroupMember(data).subscribe(value => {
        this.tcNotification.success("group_member updated");
      }, error => {
        this.tcNotification.error("something is wrong");

      })
      
         
    }

}