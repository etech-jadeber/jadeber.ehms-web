import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMemberPickComponent } from './group-member-pick.component';

describe('GroupMemberPickComponent', () => {
  let component: GroupMemberPickComponent;
  let fixture: ComponentFixture<GroupMemberPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupMemberPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMemberPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
