import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { GroupMemberSummary, GroupMemberSummaryPartialList } from '../groupMember.model';
import { GroupMemberPersist } from '../groupMember.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-group_member-pick',
  templateUrl: './group-member-pick.component.html',
  styleUrls: ['./group-member-pick.component.scss']
})export class GroupMemberPickComponent implements OnInit {
  groupMembersData: GroupMemberSummary[] = [];
  groupMembersTotalCount: number = 0;
  groupMemberSelectAll:boolean = false;
  groupMemberSelection: GroupMemberSummary[] = [];

 groupMembersDisplayedColumns: string[] = ["select","action" ,"status " ];
  groupMemberSearchTextBox: FormControl = new FormControl();
  groupMemberIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) groupMembersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) groupMembersSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public groupMemberPersist: GroupMemberPersist,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean,
 public dialogRef: MatDialogRef<GroupMemberPickComponent>,

    ) {

        this.tcAuthorization.requireRead("group_members");
       this.groupMemberSearchTextBox.setValue(groupMemberPersist.groupMemberSearchText);
      //delay subsequent keyup events
      this.groupMemberSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.groupMemberPersist.groupMemberSearchText = value;
        this.searchGroup_members();
      });
    } ngOnInit() {
   
      this.groupMembersSort.sortChange.subscribe(() => {
        this.groupMembersPaginator.pageIndex = 0;
        this.searchGroup_members(true);
      });

      this.groupMembersPaginator.page.subscribe(() => {
        this.searchGroup_members(true);
      });
      //start by loading items
      this.searchGroup_members();
    }

  searchGroup_members(isPagination:boolean = false): void {


    let paginator = this.groupMembersPaginator;
    let sorter = this.groupMembersSort;

    this.groupMemberIsLoading = true;
    this.groupMemberSelection = [];

    this.groupMemberPersist.searchGroupMember(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: GroupMemberSummaryPartialList) => {
      this.groupMembersData = partialList.data;
      if (partialList.total != -1) {
        this.groupMembersTotalCount = partialList.total;
      }
      this.groupMemberIsLoading = false;
    }, error => {
      this.groupMemberIsLoading = false;
    });

  }
  markOneItem(item: GroupMemberSummary) {
    if(!this.tcUtilsArray.containsId(this.groupMemberSelection,item.id)){
          this.groupMemberSelection = [];
          this.groupMemberSelection.push(item);
        }
        else{
          this.groupMemberSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.groupMemberSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }