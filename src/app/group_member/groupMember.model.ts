import {TCId} from "../tc/models";export class GroupMemberSummary extends TCId {
    date:number;
    note:string;
    recommendation :string;
    status :number;
    pid:string;
    session_therapy_id:string;
 
     
    }
    export class GroupMemberSummaryPartialList {
      data: GroupMemberSummary[];
      total: number;
    }
    export class GroupMemberDetail extends GroupMemberSummary {
    }
    
    export class GroupMemberDashboard {
      total: number;
    }