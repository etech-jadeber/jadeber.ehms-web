import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {GroupMemberEditComponent} from "./group-member-edit/group-member-edit.component";
import {GroupMemberPickComponent} from "./group-member-pick/group-member-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class GroupMemberNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  group_membersUrl(): string {
    return "/group_members";
  }

  group_memberUrl(id: string): string {
    return "/group_members/" + id;
  }

  viewGroupMembers(): void {
    this.router.navigateByUrl(this.group_membersUrl());
  }

  viewGroupMember(id: string): void {
    this.router.navigateByUrl(this.group_memberUrl(id));
  }

  editGroupMember(id: string): MatDialogRef<GroupMemberEditComponent> {
    const dialogRef = this.dialog.open(GroupMemberEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addGroupMember(id:string): MatDialogRef<GroupMemberEditComponent> {
    const dialogRef = this.dialog.open(GroupMemberEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickGroupMembers(selectOne: boolean=false): MatDialogRef<GroupMemberPickComponent> {
      const dialogRef = this.dialog.open(GroupMemberPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}