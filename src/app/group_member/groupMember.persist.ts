import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {GroupMemberDashboard, GroupMemberDetail, GroupMemberSummaryPartialList} from "./groupMember.model";
import { group_member_status } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class GroupMemberPersist {
 groupMemberSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "group_member/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.groupMemberSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchGroupMember(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<GroupMemberSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("group_member", this.groupMemberSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<GroupMemberSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "group_member/do", new TCDoParam("download_all", this.filters()));
  }

  groupMemberDashboard(): Observable<GroupMemberDashboard> {
    return this.http.get<GroupMemberDashboard>(environment.tcApiBaseUri + "group_member/dashboard");
  }

  getGroupMember(id: string): Observable<GroupMemberDetail> {
    return this.http.get<GroupMemberDetail>(environment.tcApiBaseUri + "group_member/" + id);
  }


 
  addGroupMember(item: GroupMemberDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "group_member",
      item
    );
  }

  updateGroupMember(item: GroupMemberDetail): Observable<GroupMemberDetail> {
    return this.http.patch<GroupMemberDetail>(
      environment.tcApiBaseUri + 'group_member/' + item.id,
      item
    );
  }

  deleteGroupMember(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'group_member/' + id);
  }
  groupMembersDo(pageSize: number, pageIndex: number, sort: string, order: string,id:string,method: string, payload: any): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl('group_member/do', this.groupMemberSearchText, pageSize, pageIndex, sort, order);
    
    if(id){
      url = TCUtilsString.appendUrlParameter(url, "id", id)
    }
    
    return this.http.post<{}>(url, new TCDoParam(method, payload) );
    // return this.http.post<{}>(
    //   environment.tcApiBaseUri + 'group_member/do',
    //   new TCDoParam(method, payload)
    // );
  }

  preAnsthesiaEvaluationDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'group_member/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'group_member/do',
      new TCDoParam('download', ids)
    );
  }
  group_member_status: TCEnumTranslation[] = [
    new TCEnumTranslation(group_member_status.active, "Active"),
    new TCEnumTranslation(group_member_status.deactive, "Deactive"),

  ];
 }