import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemodialysisScheduledPateintDetailComponent } from './hemodialysis-scheduled-pateint-detail.component';

describe('HemodialysisScheduledPateintDetailComponent', () => {
  let component: HemodialysisScheduledPateintDetailComponent;
  let fixture: ComponentFixture<HemodialysisScheduledPateintDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemodialysisScheduledPateintDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemodialysisScheduledPateintDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
