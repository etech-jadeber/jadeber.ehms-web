import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {HemodialysisScheduledPateintDetail} from "../hemodialysis.model";
import {HemodialysisScheduledPateintPersist} from "../hemodialysis.persist";
import {HemodialysisScheduledPateintNavigator} from "../hemodialysis.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export enum HemodialysisScheduledPateintTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-hemodialysisscheduled_pateint-detail',
  templateUrl: './hemodialysis-scheduled-pateint-detail.component.html',
  styleUrls: ['./hemodialysis-scheduled-pateint-detail.component.scss']
})
export class HemodialysisScheduledPateintDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  hemodialysisScheduledPateintIsLoading:boolean = false;
  
  HemodialysisScheduledPateintTabs: typeof HemodialysisScheduledPateintTabs = HemodialysisScheduledPateintTabs;
  activeTab: HemodialysisScheduledPateintTabs = HemodialysisScheduledPateintTabs.overview;
  //basics
  hemodialysisScheduledPateintDetail: HemodialysisScheduledPateintDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public hemodialysisScheduledPateintNavigator: HemodialysisScheduledPateintNavigator,
              public  hemodialysisScheduledPateintPersist: HemodialysisScheduledPateintPersist) {
    this.tcAuthorization.requireRead("hemodialysis_scheduled_pateint");
    this.hemodialysisScheduledPateintDetail = new HemodialysisScheduledPateintDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("hemodialysis_scheduled_pateint");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.hemodialysisScheduledPateintIsLoading = true;
    this.hemodialysisScheduledPateintPersist.getHemodialysisScheduledPateint(id).subscribe(hemodialysisScheduledPateintDetail => {
          this.hemodialysisScheduledPateintDetail = hemodialysisScheduledPateintDetail;
          this.hemodialysisScheduledPateintIsLoading = false;
        }, error => {
          console.error(error);
          this.hemodialysisScheduledPateintIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.hemodialysisScheduledPateintDetail.id);
  }
  editHemodialysisScheduledPateint(): void {
    let modalRef = this.hemodialysisScheduledPateintNavigator.editHemodialysisScheduledPateint(this.hemodialysisScheduledPateintDetail.id);
    modalRef.afterClosed().subscribe(hemodialysisScheduledPateintDetail => {
      TCUtilsAngular.assign(this.hemodialysisScheduledPateintDetail, hemodialysisScheduledPateintDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.hemodialysisScheduledPateintPersist.print(this.hemodialysisScheduledPateintDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print hemodialysis_scheduled_pateint", true);
      });
    }

  back():void{
      this.location.back();
    }

}
