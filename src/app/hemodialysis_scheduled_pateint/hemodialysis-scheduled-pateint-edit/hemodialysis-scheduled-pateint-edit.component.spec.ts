import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemodialysisScheduledPateintEditComponent } from './hemodialysis-scheduled-pateint-edit.component';

describe('HemodialysisScheduledPateintEditComponent', () => {
  let component: HemodialysisScheduledPateintEditComponent;
  let fixture: ComponentFixture<HemodialysisScheduledPateintEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemodialysisScheduledPateintEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemodialysisScheduledPateintEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
