import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { HemodialysisScheduledPateintDetail } from '../hemodialysis.model';
import { HemodialysisScheduledPateintPersist } from '../hemodialysis.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { OrderDialysisSummary } from 'src/app/form_encounters/form_encounter.model';
import { DialysisMachineNavigator } from 'src/app/dialysis_machine/dialysis_machine.navigator';
import { DialysisMachineSummary } from 'src/app/dialysis_machine/dialysis_machine.model';
@Component({
  selector: 'app-hemodialysis-scheduled_pateint-edit',
  templateUrl: './hemodialysis-scheduled-pateint-edit.component.html',
  styleUrls: ['./hemodialysis-scheduled-pateint-edit.component.scss']
})export class HemodialysisScheduledPateintEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  hemodialysisScheduledPateintDetail: HemodialysisScheduledPateintDetail;

  patientFullName:string ="";
  machineId:string = "";
  start_time: string;
  end_time: string = "";
  last_date: Date = new Date();
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<HemodialysisScheduledPateintEditComponent>,
              public  persist: HemodialysisScheduledPateintPersist,
              public formEncounterNavigator:Form_EncounterNavigator,
              public tCUtilsDate:TCUtilsDate,
              public tcUtilsDate: TCUtilsDate,
              public dialysisMachineNav:DialysisMachineNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }


  searchSchedule() {
    let dialogRef = this.formEncounterNavigator.pickFormDialysis(true);
    dialogRef.afterClosed().subscribe((result: OrderDialysisSummary) => {
      if (result) {
        this.patientFullName =   result[0].patient_name;
        this.hemodialysisScheduledPateintDetail.patient_id = result[0].patient_id;
        this.hemodialysisScheduledPateintDetail.order_id = result[0].id;
      }
    });
  }


  searchMachine() {
    let dialogRef = this.dialysisMachineNav.pickDialysisMachines(true);
    dialogRef.afterClosed().subscribe((result: DialysisMachineSummary) => {
      if (result) {
        this.hemodialysisScheduledPateintDetail.machine_id =   result[0].id;
        this.machineId = result[0].serial_no + " " + result[0].machine_no; 
    
      }
    });
  }


  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("hemodialysis_scheduled_pateint");
      this.title = this.appTranslation.getText("general","new") +  " " + "Dialysis Schedule";
      this.hemodialysisScheduledPateintDetail = new HemodialysisScheduledPateintDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("hemodialysis_scheduled_pateint");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "Dialysis Schedule";
      this.isLoadingResults = true;
      this.persist.getHemodialysisScheduledPateint(this.idMode.id).subscribe(hemodialysisScheduledPateintDetail => {
        this.hemodialysisScheduledPateintDetail = hemodialysisScheduledPateintDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
   
    this.persist.addHemodialysisScheduledPateint(this.hemodialysisScheduledPateintDetail).subscribe(value => {
      this.tcNotification.success("hemodialysisScheduledPateint added");
      this.hemodialysisScheduledPateintDetail.id = value.id;
      this.dialogRef.close(this.hemodialysisScheduledPateintDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();

    this.persist.updateHemodialysisScheduledPateint(this.hemodialysisScheduledPateintDetail).subscribe(value => {
      this.tcNotification.success("hemodialysis_scheduled_pateint updated");
      this.dialogRef.close(this.hemodialysisScheduledPateintDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.hemodialysisScheduledPateintDetail == null){
            return false;
          }

if (this.hemodialysisScheduledPateintDetail.patient_id == null || this.hemodialysisScheduledPateintDetail.patient_id  == "") {
            return false;
        } 
 return true;
if (this.hemodialysisScheduledPateintDetail.date == null) {
            return false;
        }
if (this.hemodialysisScheduledPateintDetail.start_time == null) {
            return false;
        }
if (this.hemodialysisScheduledPateintDetail.end_time == null) {
            return false;
        }
if (this.hemodialysisScheduledPateintDetail.machine_id == null || this.hemodialysisScheduledPateintDetail.machine_id  == "") {
            return false;
        } 
 return true;
if (this.hemodialysisScheduledPateintDetail.last_date == null) {
            return false;
        }
if (this.hemodialysisScheduledPateintDetail.order_id == null || this.hemodialysisScheduledPateintDetail.order_id  == "") {
            return false;
        } 
 return true;

 }


 transformDates(todate: boolean = true) {
  if (todate) {
    this.hemodialysisScheduledPateintDetail.last_date = this.tCUtilsDate.toTimeStamp(this.last_date);

    let ed = new Date();
      let end_times:string[] = this.end_time.split(":");
      ed.setHours(parseInt( end_times[0]))
      ed.setMinutes(parseInt( end_times[1]))

    this.hemodialysisScheduledPateintDetail.end_time = this.tCUtilsDate.toTimeStamp(ed);


    let sd = new Date();
    let start_times:string[] = this.start_time.split(":");
    sd.setHours(parseInt( start_times[0]))
    sd.setMinutes(parseInt( start_times[1]))

  this.hemodialysisScheduledPateintDetail.start_time = this.tCUtilsDate.toTimeStamp(sd);


  } else {
  
  }
}
 }
