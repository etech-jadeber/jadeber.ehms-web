import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemodialysisScheduledPateintListComponent } from './hemodialysis-scheduled-pateint-list.component';

describe('HemodialysisScheduledPateintListComponent', () => {
  let component: HemodialysisScheduledPateintListComponent;
  let fixture: ComponentFixture<HemodialysisScheduledPateintListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemodialysisScheduledPateintListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemodialysisScheduledPateintListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
