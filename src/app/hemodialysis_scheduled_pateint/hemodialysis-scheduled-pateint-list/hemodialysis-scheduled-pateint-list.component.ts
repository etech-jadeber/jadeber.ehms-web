import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { HemodialysisScheduledPateintDetail, HemodialysisScheduledPateintSummary, HemodialysisScheduledPateintSummaryPartialList } from '../hemodialysis.model';
import { HemodialysisScheduledPateintPersist } from '../hemodialysis.persist';
import { HemodialysisScheduledPateintNavigator } from '../hemodialysis.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
@Component({
  selector: 'app-hemodialysis_scheduled_pateint-list',
  templateUrl: './hemodialysis-scheduled-pateint-list.component.html',
  styleUrls: ['./hemodialysis-scheduled-pateint-list.component.scss']
})export class HemodialysisScheduledPateintListComponent implements OnInit {
  hemodialysisScheduledPateintsData: HemodialysisScheduledPateintSummary[] = [];
  hemodialysisScheduledPateintsTotalCount: number = 0;
  hemodialysisScheduledPateintSelectAll:boolean = false;
  hemodialysisScheduledPateintSelection: HemodialysisScheduledPateintSummary[] = [];

 hemodialysisScheduledPateintsDisplayedColumns: string[] = ["select","action" ,"patient_id","date","start_time","end_time","machine_no","last_date" ];
  hemodialysisScheduledPateintSearchTextBox: FormControl = new FormControl();
  hemodialysisScheduledPateintIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) hemodialysisScheduledPateintsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) hemodialysisScheduledPateintsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public form_EncounterNavigator: Form_EncounterNavigator,
                public hemodialysisScheduledPateintPersist: HemodialysisScheduledPateintPersist,
                public hemodialysisScheduledPateintNavigator: HemodialysisScheduledPateintNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("hemodialysis_scheduled_pateint");
       this.hemodialysisScheduledPateintSearchTextBox.setValue(hemodialysisScheduledPateintPersist.hemodialysisScheduledPateintSearchText);
      //delay subsequent keyup events
      this.hemodialysisScheduledPateintSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.hemodialysisScheduledPateintPersist.hemodialysisScheduledPateintSearchText = value;
        this.searchHemodialysisScheduledPateints();
      });
    } ngOnInit() {
   
      this.hemodialysisScheduledPateintsSort.sortChange.subscribe(() => {
        this.hemodialysisScheduledPateintsPaginator.pageIndex = 0;
        this.searchHemodialysisScheduledPateints(true);
      });

      this.hemodialysisScheduledPateintsPaginator.page.subscribe(() => {
        this.searchHemodialysisScheduledPateints(true);
      });
      //start by loading items
      this.searchHemodialysisScheduledPateints();
    }

  searchHemodialysisScheduledPateints(isPagination:boolean = false): void {


    let paginator = this.hemodialysisScheduledPateintsPaginator;
    let sorter = this.hemodialysisScheduledPateintsSort;

    this.hemodialysisScheduledPateintIsLoading = true;
    this.hemodialysisScheduledPateintSelection = [];

    this.hemodialysisScheduledPateintPersist.searchHemodialysisScheduledPateint(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: HemodialysisScheduledPateintSummaryPartialList) => {
      this.hemodialysisScheduledPateintsData = partialList.data;
      if (partialList.total != -1) {
        this.hemodialysisScheduledPateintsTotalCount = partialList.total;
      }
      this.hemodialysisScheduledPateintIsLoading = false;
    }, error => {
      this.hemodialysisScheduledPateintIsLoading = false;
    });

  } downloadHemodialysisScheduledPateints(): void {
    if(this.hemodialysisScheduledPateintSelectAll){
         this.hemodialysisScheduledPateintPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download hemodialysis_scheduled_pateint", true);
      });
    }
    else{
        this.hemodialysisScheduledPateintPersist.download(this.tcUtilsArray.idsList(this.hemodialysisScheduledPateintSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download hemodialysis_scheduled_pateint",true);
            });
        }
  }
addHemodialysisScheduledPateint(): void {
    let dialogRef = this.hemodialysisScheduledPateintNavigator.addHemodialysisScheduledPateint();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchHemodialysisScheduledPateints();
      }
    });
  }

  editHemodialysisScheduledPateint(item: HemodialysisScheduledPateintSummary) {
    let dialogRef = this.hemodialysisScheduledPateintNavigator.editHemodialysisScheduledPateint(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteHemodialysisScheduledPateint(item: HemodialysisScheduledPateintSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("hemodialysis_scheduled_pateint");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.hemodialysisScheduledPateintPersist.deleteHemodialysisScheduledPateint(item.id).subscribe(response => {
          this.tcNotification.success("hemodialysis_scheduled_pateint deleted");
          this.searchHemodialysisScheduledPateints();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }


    addPerformedDialysis(item:HemodialysisScheduledPateintDetail){
      this.form_EncounterNavigator.addHemoPerformed(item.patient_id).afterClosed().subscribe(
        res=>{
          console.log(res);
          
        }
      )

    }
}
