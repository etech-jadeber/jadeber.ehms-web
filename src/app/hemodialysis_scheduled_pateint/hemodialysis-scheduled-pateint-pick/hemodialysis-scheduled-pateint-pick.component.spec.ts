import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HemodialysisScheduledPateintPickComponent } from './hemodialysis-scheduled-pateint-pick.component';

describe('HemodialysisScheduledPateintPickComponent', () => {
  let component: HemodialysisScheduledPateintPickComponent;
  let fixture: ComponentFixture<HemodialysisScheduledPateintPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HemodialysisScheduledPateintPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HemodialysisScheduledPateintPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
