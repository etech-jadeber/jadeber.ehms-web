import {TCId} from "../tc/models";export class HemodialysisScheduledPateintSummary extends TCId {
    patient_id:string;
    date:number;
    start_time:number;
    end_time:number;
    machine_id:string;
    last_date:number;
    order_id:string;
     
    }
    export class HemodialysisScheduledPateintSummaryPartialList {
      data: HemodialysisScheduledPateintSummary[];
      total: number;
    }
    export class HemodialysisScheduledPateintDetail extends HemodialysisScheduledPateintSummary {
    }
    
    export class HemodialysisScheduledPateintDashboard {
      total: number;
    }
    