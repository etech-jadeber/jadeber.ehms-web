import {Router} from "@angular/router";
import {Injectable} from "@angular/core";


import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {HemodialysisScheduledPateintEditComponent} from "./hemodialysis-scheduled-pateint-edit/hemodialysis-scheduled-pateint-edit.component";
import {HemodialysisScheduledPateintPickComponent} from "./hemodialysis-scheduled-pateint-pick/hemodialysis-scheduled-pateint-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class HemodialysisScheduledPateintNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  hemodialysisScheduledPateintsUrl(): string {
    return "/hemodialysis_scheduled_pateints";
  }

  hemodialysisScheduledPateintUrl(id: string): string {
    return "/hemodialysis_scheduled_pateints/" + id;
  }

  viewHemodialysisScheduledPateints(): void {
    this.router.navigateByUrl(this.hemodialysisScheduledPateintsUrl());
  }

  viewHemodialysisScheduledPateint(id: string): void {
    this.router.navigateByUrl(this.hemodialysisScheduledPateintUrl(id));
  }

  editHemodialysisScheduledPateint(id: string): MatDialogRef<HemodialysisScheduledPateintEditComponent> {
    const dialogRef = this.dialog.open(HemodialysisScheduledPateintEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addHemodialysisScheduledPateint(): MatDialogRef<HemodialysisScheduledPateintEditComponent> {
    const dialogRef = this.dialog.open(HemodialysisScheduledPateintEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickHemodialysisScheduledPateints(selectOne: boolean=false): MatDialogRef<HemodialysisScheduledPateintPickComponent> {
      const dialogRef = this.dialog.open(HemodialysisScheduledPateintPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
