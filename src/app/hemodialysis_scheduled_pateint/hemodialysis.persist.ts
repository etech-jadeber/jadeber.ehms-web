import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {HemodialysisScheduledPateintDashboard, HemodialysisScheduledPateintDetail, HemodialysisScheduledPateintSummaryPartialList} from "./hemodialysis.model";
import { day_of_the_week } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class HemodialysisScheduledPateintPersist {
 hemodialysisScheduledPateintSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.hemodialysisScheduledPateintSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchHemodialysisScheduledPateint(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<HemodialysisScheduledPateintSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("hemodialysis_scheduled_pateint", this.hemodialysisScheduledPateintSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<HemodialysisScheduledPateintSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/do", new TCDoParam("download_all", this.filters()));
  }

  hemodialysisScheduledPateintDashboard(): Observable<HemodialysisScheduledPateintDashboard> {
    return this.http.get<HemodialysisScheduledPateintDashboard>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/dashboard");
  }

  getHemodialysisScheduledPateint(id: string): Observable<HemodialysisScheduledPateintDetail> {
    return this.http.get<HemodialysisScheduledPateintDetail>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/" + id);
  } addHemodialysisScheduledPateint(item: HemodialysisScheduledPateintDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/", item);
  }

  updateHemodialysisScheduledPateint(item: HemodialysisScheduledPateintDetail): Observable<HemodialysisScheduledPateintDetail> {
    return this.http.patch<HemodialysisScheduledPateintDetail>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/" + item.id, item);
  }

  deleteHemodialysisScheduledPateint(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/" + id);
  }
 hemodialysisScheduledPateintsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/do", new TCDoParam(method, payload));
  }

  hemodialysisScheduledPateintDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateints/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "hemodialysis_scheduled_pateint/" + id + "/do", new TCDoParam("print", {}));
  }


  day_of_the_week: TCEnum[] = [
    new TCEnum(day_of_the_week.Monday, 'Monday'),
    new TCEnum(day_of_the_week.Tuesday, 'Tuesday'),
    new TCEnum(day_of_the_week.Wednesday, 'Wednesday'),
    new TCEnum(day_of_the_week.Thursday, 'Thursday'),
    new TCEnum(day_of_the_week.Friday, 'Friday'),
    new TCEnum(day_of_the_week.Saturday, 'Saturday'),
    new TCEnum(day_of_the_week.Sunday, 'Sunday'),
    ];

}
