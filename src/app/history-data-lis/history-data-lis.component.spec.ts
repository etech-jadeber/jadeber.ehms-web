import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryDataLisComponent } from './history-data-lis.component';

describe('HistoryDataLisComponent', () => {
  let component: HistoryDataLisComponent;
  let fixture: ComponentFixture<HistoryDataLisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryDataLisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryDataLisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
