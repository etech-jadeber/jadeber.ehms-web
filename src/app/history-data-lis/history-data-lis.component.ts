import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { History_DataDetail, History_DataSummary } from '../patients/patients.model';
import { PatientNavigator } from '../patients/patients.navigator';
import { PatientPersist } from '../patients/patients.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';

@Component({
  selector: 'app-history-data-lis',
  templateUrl: './history-data-lis.component.html',
  styleUrls: ['./history-data-lis.component.css']
})
export class HistoryDataLisComponent implements OnInit {

  history_datasData: History_DataSummary[] = [];
  history_datasTotalCount: number = 0;
  history_datasSelectAll: boolean = false;
  history_datasSelected: History_DataSummary[] = [];
  history_datasDisplayedColumns: string[] = [
    'select',
    'history_siblings',
    'hisotry_father',
    'history_mother',
    'history_off_spring',
    'history_spouse',
    'coffee',
    'alcohol',
    'tobacco',
  ];
  history_datasSearchTextBox: FormControl = new FormControl();
  history_datasLoading: boolean = false;

  @Input() patientId: any;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist, 
    public patientPersist: PatientPersist,
    public patientNavigator: PatientNavigator,
  ) {
    this.history_datasSearchTextBox.setValue(historyPersist.historySearchText);
    this.history_datasSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.historyPersist.historySearchText = value.trim();
      });
   }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchHistory_Datas(true);
    });
  // prescriptionss paginator
  this.paginator.page.subscribe(() => {
    this.searchHistory_Datas(true);
  });
  this.searchHistory_Datas(true);
  }

  searchHistory_Datas(isPagination: boolean = false): void {
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.history_datasSelected = [];
    this.history_datasLoading = true;
    this.patientPersist
      .searchHistory_Data(
        this.patientId.toString(),
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.history_datasData = response.data;
          if (response.total != -1) {
            this.history_datasTotalCount = response.total;
          }
          this.history_datasLoading = false;
        },
        (error) => {
          this.history_datasLoading = false;
        }
      );
  }

  addHistory_Data(): void {
    if (this.history_datasData.length > 0) {
      this.editHistory_Data(this.history_datasData[0]);
      return;
    }
    let dialogRef = this.patientNavigator.addHistory_Data(
      this.patientId.toString()
    );
    dialogRef.afterClosed().subscribe((newHistory_Data) => {
      this.searchHistory_Datas();
    });
  }

  editHistory_Data(item: History_DataDetail): void {
    let dialogRef = this.patientNavigator.editHistory_Data(
      this.patientId.toString(),
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedHistory_Data) => {
      if (updatedHistory_Data) {
        this.searchHistory_Datas();
      }
    });
  }

  deleteHistory_Data(item: History_DataDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('History_Data');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.patientPersist
          .deleteHistory_Data(this.patientId.toString(), item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('history_data deleted');
              this.searchHistory_Datas();
            },
            (error) => {}
          );
      }
    });
  }

  downloadHistory_Datas(): void {
    this.tcNotification.info(
      'Download history_datas : ' + this.history_datasSelected.length
    );
  }

  back(): void {
    this.location.back();
  }


}
