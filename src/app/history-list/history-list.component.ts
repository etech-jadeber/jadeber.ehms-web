import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistorySummary } from '../historys/history.model';
import { HistoryNavigator } from '../historys/history.navigator';
import { HistoryPersist } from '../historys/history.persist';
import { History_DataSummary } from '../patients/patients.model';
import { TCAuthorization } from '../tc/authorization';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsAngular } from '../tc/utils-angular';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.css']
})
export class HistoryListComponent implements OnInit {
  historysData: HistorySummary[] = [];
  historysTotalCount: number = 0;
  historysSelectAll: boolean = false;
  historysSelection: HistorySummary[] = [];

  historysDisplayedColumns: string[] = [
    'select',
    'action',
    'chief_complaint',
    'hpi',
    'past_medical_history',
    'medical_history',
    'family_history',
    'social_history',
    'occupation_history',
    'obstetrics_history',
    'encounter',
  ];
  historysSearchTextBox: FormControl = new FormControl();
  historysIsLoading: boolean = false;


  @Input() encounterId: any;
  @Input() edit: boolean=true;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public historyNavigator: HistoryNavigator,
  ) { 
    this.historysSearchTextBox.setValue(
      form_encounterPersist.form_soapSearchText
    );
    this.historysSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.form_soapSearchText = value.trim();
        this.searchHistorys();
      });
  }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchHistorys(true);
    });
  // encounter_imagess paginator
  this.paginator.page.subscribe(() => {
    this.searchHistorys(true);
  });
  this.searchHistorys(true);
  }


  searchHistorys(isPagination: boolean = false): void {
    let paginator = this.paginator;
    let sorter = this.sorter;

    this.historysIsLoading = false;
    this.historysSelection = [];

    this.historyPersist
      .searchHistory(
        this.encounterId,
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.historysData = response.data;

          if (response.total != -1) {
            this.onResult.emit(response.total);
          }
          this.historysIsLoading = false;
        },
        (error) => {
          this.historysIsLoading = false;
        }
      );
  }

  downloadHistorys(): void {
    if (this.historysSelectAll) {
      this.historyPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download historys', true);
      });
    } else {
      this.historyPersist
        .download(this.tcUtilsArray.idsList(this.historysSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'download historys', true);
        });
    }
  }

  addHistory(): void {
    if (this.historysData.length > 0) {
      this.editHistory(this.historysData[0]);
      return;
    }
    let dialogRef = this.historyNavigator.addHistory(
      this.encounterId.toString()
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchHistorys();
      }
    });
  }
  editHistory(item: HistorySummary) {
    let dialogRef = this.historyNavigator.editHistory(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteHistory(item: HistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('History');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.historyPersist
          .deleteHistory(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('History deleted');
              this.searchHistorys();
            },
            (error) => {}
          );
      }
    });
  }
  back(): void {
    this.location.back();
  }

}
