import {Component, OnInit, Inject, Input, Directive, Injectable, Output, EventEmitter} from '@angular/core';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCEnum, TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";


import {HistoryDetail} from "../history.model";
import {HistoryPersist} from "../history.persist";
import { ChiefCompliantOptionSummary } from 'src/app/chief_compliant_option/chief_compliant_option.model';
import { ChiefCompliantOptionPersist } from 'src/app/chief_compliant_option/chief_compliant_option.persist';
import { FormControl } from '@angular/forms';
import { debounceTime, map, Observable, startWith } from 'rxjs';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { QueuesPersist } from 'src/app/queues/queues.persist';

@Injectable()
abstract class HistoryComponent {
  isLoadingResults: boolean = false;
  title: string;
  historyDetail: HistoryDetail;
  is_new: boolean = false;
  chiefComplaintOptions :ChiefCompliantOptionSummary[]=[]; 
  chiefComplaintOptionsValue:string;
  component:string;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: HistoryPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public ids: TCParentChildIds,
              public isDialog: Boolean,
              public chiefComplaintOptionsPersist: ChiefCompliantOptionPersist,
              public queuesPersist: QueuesPersist,

              ) {
    this.loadChiefComplaintOptions();
  }
  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }


  ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("medical_historys");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "history");
      this.historyDetail = new HistoryDetail();
      this.historyDetail.encounter_id = this.ids.parentId;
      this.historyDetail.chief_complaint="";
      this.historyDetail.family_history="";
      this.historyDetail.hpi="";
      this.historyDetail.obstetrics_history="";
      this.historyDetail.occupation_history="";
      this.historyDetail.past_medical_history="";
      this.historyDetail.social_history="";
      this.historyDetail.medical_history="";
      this.historyDetail.mother_history="";
      this.historyDetail.father_histroy="";
      this.historyDetail.siblings_history="";
      this.historyDetail.others_history="";
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("medical_historys");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "history");
      this.historyDetail = new HistoryDetail();
      this.isLoadingResults = true;
      this.persist.getHistory(this.ids.childId,this.ids.parentId).subscribe(historyDetail => {
        this.historyDetail = historyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  loadChiefComplaintOptions():void{
      this.chiefComplaintOptionsPersist.searchChiefCompliantOption(5,0,"name","asc").subscribe((value)=>{
        if(value){
            this.chiefComplaintOptions = value.data;
        }
      })
  }

  isEmptyString(value: string): boolean {
    return value == "" || value == null;
  }

  isVisible(value: string):boolean {
    return !this.isEmptyString(value) || this.form_encounterPersist.encounter_is_active;
  }

  appendString(value: string):string{
    value = value.trim();
    if (!this.historyDetail.chief_complaint)
      return "\u2022  " + value;
    if(this.historyDetail.chief_complaint.endsWith("\n"))
      return this.historyDetail.chief_complaint + "\u2022  " + value;
    let x = this.historyDetail.chief_complaint.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return  y ?  y+"\u2022  " +value : "\u2022  " + value;
  }
  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
    if (event && !input.value.endsWith("\n"))
    this.chiefComplaintOptionsPersist.chiefCompliantOptionSearchText =input.value.split('\u2022').pop().trim();
    else 
      this.chiefComplaintOptionsPersist.chiefCompliantOptionSearchText = "";
    this.loadChiefComplaintOptions();
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addHistory(this.historyDetail.encounter_id, this.historyDetail).subscribe(value => {
      // this.tcNotification.success("History added");
      this.historyDetail.id = value.id;
      this.action(this.historyDetail);
      this.isLoadingResults = false
    }, error => {
      this.isLoadingResults = false;
      console.log(error)
    })
  }

  action (history: HistoryDetail){
    this.queuesPersist.updateQueues({queue_status:"opened"},history.encounter_id).subscribe((res)=>{
      console.log("queues upateded",res)
     })
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateHistory(this.historyDetail.encounter_id,this.historyDetail).subscribe(value => {
      // this.tcNotification.success("History updated");
      this.action(this.historyDetail);
      this.isLoadingResults = false
    }, error => {
      this.isLoadingResults = false;
      console.log(error)
    })

  }


  canSubmit():boolean{
        if (this.historyDetail == null){
            return false;
          }

          if(this.historyDetail.chief_complaint == "" ){
            return false;
          }

          if(this.historyDetail.hpi == "" ){
            return false;
          }

          // if(this.historyDetail.past_medical_history == "" ){
          //   return false;
          // }


        return true;
      }

      onCancel():void{
      }
}

@Component({
  selector: 'app-history-edit-list',
  templateUrl: './history-edit.component.html',
  styleUrls: ['./history-edit.component.css']
})
export class HistoryEditListComponent extends HistoryComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  historyDetail: HistoryDetail;
  @Input() encounterId: string;
  @Output() date_time = new EventEmitter<number>();
  @Output() assign_value = new EventEmitter<any>();
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: HistoryPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public chiefComplaintOptionsPersist: ChiefCompliantOptionPersist,
              public queuesPersist: QueuesPersist,

              ) {
        super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist, form_encounterPersist, {childId: null, context: null, parentId: null}, false,chiefComplaintOptionsPersist,queuesPersist)
  }

  ngOnInit(): void {
    this.persist.searchHistory(this.encounterId, 1, 0, 'date_of_history', 'desc').subscribe(
      history => {
        if(history.data.length) {
          this.historyDetail =  history.data[0]
        } else{
          this.assign_value.emit({new_history: true })
          this.historyDetail = new HistoryDetail();
        } 
        this.date_time.emit(this.historyDetail.date_of_history);
      }
    )
  }

  saveResult(){
    if (!this.canSubmit()){
      return;
    }
    this.historyDetail.encounter_id = this.encounterId;
    if (this.historyDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }

  isNew(): boolean {
    return false
  }


}

@Component({
  selector: 'app-history-edit',
  templateUrl: './history-edit.component.html',
  styleUrls: ['./history-edit.component.css']
})
export class HistoryEditComponent extends HistoryComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  historyDetail: HistoryDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: HistoryPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public chiefComplaintOptionsPersist: ChiefCompliantOptionPersist,
              public dialogRef: MatDialogRef<HistoryEditComponent>,
              public queuesPersist: QueuesPersist,

              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {
                super(tcAuthorization, tcNotification, tcUtilsString, appTranslation, persist,form_encounterPersist, ids, true,chiefComplaintOptionsPersist,queuesPersist)
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  action(history: HistoryDetail){
    this.dialogRef.close(history)
  }
}
