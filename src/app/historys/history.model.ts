import {TCId} from "../tc/models";

export class HistorySummary extends TCId {
  chief_complaint : string;
hpi : string;
past_medical_history : string;
medical_history : string;
family_history : string;
social_history : string;
occupation_history : string;
obstetrics_history : string;
encounter_id : string;
mother_history: string;
father_histroy: string;
siblings_history: string;
date_of_history: number;
others_history: string;
}

export class HistorySummaryPartialList {
  data: HistorySummary[];
  total: number;
}

export class HistoryDetail extends HistorySummary {
  chief_complaint : string;
hpi : string;
past_medical_history : string;
medical_history : string;
family_history : string;
social_history : string;
occupation_history : string;
obstetrics_history : string;
}

export class HistoryDashboard {
  total: number;
}
