import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {HistoryEditComponent} from "./history-edit/history-edit.component";


@Injectable({
  providedIn: 'root'
})

export class HistoryNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  historysUrl(): string {
    return "/historys";
  }

  historyUrl(id: string): string {
    return "/historys/" + id;
  }

  viewHistorys(): void {
    this.router.navigateByUrl(this.historysUrl());
  }

  viewHistory(id: string): void {
    this.router.navigateByUrl(this.historyUrl(id));
  }


  addHistory(parentId:string, ): MatDialogRef<unknown,any> {
    return this.editHistory(parentId,null);
  }

  editHistory(parentId:string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(HistoryEditComponent, {
      data:  new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  
//    pickHistorys(selectOne: boolean=false): MatDialogRef<HistoryPickComponent> {
//       const dialogRef = this.dialog.open(HistoryPickComponent, {
//         data: selectOne,
//         width: TCModalWidths.large
//       });
//       return dialogRef;
//     }
}
