import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {HistoryDashboard, HistoryDetail, HistorySummaryPartialList} from "./history.model";


@Injectable({
  providedIn: 'root'
})
export class HistoryPersist {

  historySearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchHistory(parentId: string,pageSize: number, pageIndex: number, sort: string, order: string,): Observable<HistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId+ "/history", this.historySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<HistorySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.historySearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "historys/do", new TCDoParam("download_all", this.filters()));
  }

  historyDashboard(parentId:string): Observable<HistoryDashboard> {
    return this.http.get<HistoryDashboard>(environment.tcApiBaseUri + "form_encounters/" + parentId +"/historys/dashboard");
  }

  getHistory(id: string, parentId:string): Observable<HistoryDetail> {
    return this.http.get<HistoryDetail>(environment.tcApiBaseUri + "historys/" + id);
  }

//   addHistory(item: HistoryDetail): Observable<TCId> {
//     return this.http.post<TCId>(environment.tcApiBaseUri + "historys/", item);
//   }
  addHistory(parentId:string,item): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId+ "/history/", item);
  }

  updateHistory(parentId:string,item: HistoryDetail): Observable<HistoryDetail> {
    return this.http.patch<HistoryDetail>(environment.tcApiBaseUri +   "historys/" + item.id, item);
  }

  deleteHistory(parentId:string,id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri +"historys/" + id);
  }

  historysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "historys/do", new TCDoParam(method, payload));
  }

  historyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "historys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "historys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "historys/" + id + "/do", new TCDoParam("print", {}));
  }


}
