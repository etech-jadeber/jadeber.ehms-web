import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { HolidayDetail } from '../holiday.model';
import { HolidayPersist } from '../holiday.persist';
import { HolidayNavigator } from '../holiday.navigator';

export enum HolidayTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-holiday-detail',
  templateUrl: './holiday-detail.component.html',
  styleUrls: ['./holiday-detail.component.scss'],
})
export class HolidayDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  holidayIsLoading: boolean = false;

  HolidayTabs: typeof HolidayTabs = HolidayTabs;
  activeTab: HolidayTabs = HolidayTabs.overview;
  //basics
  holidayDetail: HolidayDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public holidayNavigator: HolidayNavigator,
    public holidayPersist: HolidayPersist
  ) {
    this.tcAuthorization.requireRead('holidays');
    this.holidayDetail = new HolidayDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('holidays');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.holidayIsLoading = true;
    this.holidayPersist.getHoliday(id).subscribe(
      (holidayDetail) => {
        this.holidayDetail = holidayDetail;
        this.holidayIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.holidayIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.holidayDetail.id);
  }
  editHoliday(): void {
    let modalRef = this.holidayNavigator.editHoliday(
      this.holidayDetail.id
    );
    modalRef.afterClosed().subscribe(
      (holidayDetail) => {
        TCUtilsAngular.assign(this.holidayDetail, holidayDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.holidayPersist
      .print(this.holidayDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print holiday', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
