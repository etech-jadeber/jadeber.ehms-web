import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { HolidayDetail } from '../holiday.model';
import { HolidayPersist } from '../holiday.persist';
@Component({
  selector: 'app-holiday-edit',
  templateUrl: './holiday-edit.component.html',
  styleUrls: ['./holiday-edit.component.scss'],
})
export class HolidayEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  holidayDetail: HolidayDetail;
  holidayDate: Date;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<HolidayEditComponent>,
    public persist: HolidayPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('holidays');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'holiday';
      this.holidayDetail = new HolidayDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('holidays');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'holiday';
      this.isLoadingResults = true;
      this.persist.getHoliday(this.idMode.id).subscribe(
        (holidayDetail) => {
          this.holidayDetail = holidayDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.holidayDetail.date = this.tcUtilsDate.toTimeStamp(this.holidayDate)
    this.persist.addHoliday(this.holidayDetail).subscribe(
      (value) => {
        this.tcNotification.success('holiday added');
        this.holidayDetail.id = value.id;
        this.dialogRef.close(this.holidayDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateHoliday(this.holidayDetail).subscribe(
      (value) => {
        this.tcNotification.success('holiday updated');
        this.dialogRef.close(this.holidayDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.holidayDetail == null) {
      return false;
    }

    if (this.holidayDetail.name == null) {
      return false;
    }
    if (this.holidayDate== null) {
      return false;
    }
    return true;
  }
}
