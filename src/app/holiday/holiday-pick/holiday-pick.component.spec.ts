import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HolidayPickComponent } from './holiday-pick.component';

describe('HolidayPickComponent', () => {
  let component: HolidayPickComponent;
  let fixture: ComponentFixture<HolidayPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HolidayPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HolidayPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
