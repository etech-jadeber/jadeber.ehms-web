import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  HolidaySummary,
  HolidaySummaryPartialList,
} from '../holiday.model';
import { HolidayPersist } from '../holiday.persist';
import { HolidayNavigator } from '../holiday.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-holiday-pick',
  templateUrl: './holiday-pick.component.html',
  styleUrls: ['./holiday-pick.component.scss'],
})
export class HolidayPickComponent implements OnInit {
  holidaysData: HolidaySummary[] = [];
  holidaysTotalCount: number = 0;
  holidaySelectAll: boolean = false;
  holidaySelection: HolidaySummary[] = [];

  holidaysDisplayedColumns: string[] = [
    'select',
    'name',
    'date',
  ];
  holidaySearchTextBox: FormControl = new FormControl();
  holidayIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  holidaysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) holidaysSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public holidayPersist: HolidayPersist,
    public holidayNavigator: HolidayNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<HolidayPickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('holidays');
    this.holidaySearchTextBox.setValue(
      holidayPersist.holidaySearchText
    );
    //delay subsequent keyup events
    this.holidaySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.holidayPersist.holidaySearchText = value;
        this.searchHolidays();
      });
  }
  ngOnInit() {
    this.holidaysSort.sortChange.subscribe(() => {
      this.holidaysPaginator.pageIndex = 0;
      this.searchHolidays(true);
    });

    this.holidaysPaginator.page.subscribe(() => {
      this.searchHolidays(true);
    });
    //start by loading items
    this.searchHolidays();
  }

  searchHolidays(isPagination: boolean = false): void {
    let paginator = this.holidaysPaginator;
    let sorter = this.holidaysSort;

    this.holidayIsLoading = true;
    this.holidaySelection = [];

    this.holidayPersist
      .searchHoliday(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: HolidaySummaryPartialList) => {
          this.holidaysData = partialList.data;
          if (partialList.total != -1) {
            this.holidaysTotalCount = partialList.total;
          }
          this.holidayIsLoading = false;
        },
        (error) => {
          this.holidayIsLoading = false;
        }
      );
  }
  markOneItem(item: HolidaySummary) {
    if (!this.tcUtilsArray.containsId(this.holidaySelection, item.id)) {
      this.holidaySelection = [];
      this.holidaySelection.push(item);
    } else {
      this.holidaySelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.holidaySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
