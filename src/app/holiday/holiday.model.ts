import { TCId } from '../tc/models';
export class HolidaySummary extends TCId {
  name: number;
  date: number;
}
export class HolidaySummaryPartialList {
  data: HolidaySummary[];
  total: number;
}
export class HolidayDetail extends HolidaySummary {}

export class HolidayDashboard {
  total: number;
}
