import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { HolidayEditComponent } from './holiday-edit/holiday-edit.component';
import { HolidayPickComponent } from './holiday-pick/holiday-pick.component';

@Injectable({
  providedIn: 'root',
})
export class HolidayNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  holidaysUrl(): string {
    return '/holidays';
  }

  holidayUrl(id: string): string {
    return '/holidays/' + id;
  }

  viewHolidays(): void {
    this.router.navigateByUrl(this.holidaysUrl());
  }

  viewHoliday(id: string): void {
    this.router.navigateByUrl(this.holidayUrl(id));
  }

  editHoliday(id: string): MatDialogRef<HolidayEditComponent> {
    const dialogRef = this.dialog.open(HolidayEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addHoliday(): MatDialogRef<HolidayEditComponent> {
    const dialogRef = this.dialog.open(HolidayEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickHolidays(
    selectOne: boolean = false
  ): MatDialogRef<HolidayPickComponent> {
    const dialogRef = this.dialog.open(HolidayPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
