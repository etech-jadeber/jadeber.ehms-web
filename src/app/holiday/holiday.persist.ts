import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  HolidayDashboard,
  HolidayDetail,
  HolidaySummaryPartialList,
} from './holiday.model';
import { store_type } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class HolidayPersist {
  holidaySearchText: string = '';
  store_type: TCEnum[] = [
    new TCEnum(store_type.outpatient, 'outpatient'),
    new TCEnum(store_type.inpatient, 'inpatient'),
    new TCEnum(store_type.emergency, 'emergency'),
  ];

  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.holidaySearchText;
    //add custom filters
    return fltrs;
  }

  searchHoliday(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<HolidaySummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'holiday',
      this.holidaySearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<HolidaySummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'holiday/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  holidayDashboard(): Observable<HolidayDashboard> {
    return this.http.get<HolidayDashboard>(
      environment.tcApiBaseUri + 'holiday/dashboard'
    );
  }

  getHoliday(id: string): Observable<HolidayDetail> {
    return this.http.get<HolidayDetail>(
      environment.tcApiBaseUri + 'holiday/' + id
    );
  }
  addHoliday(item: HolidayDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'holiday/',
      item
    );
  }

  updateHoliday(item: HolidayDetail): Observable<HolidayDetail> {
    return this.http.patch<HolidayDetail>(
      environment.tcApiBaseUri + 'holiday/' + item.id,
      item
    );
  }

  deleteHoliday(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'holiday/' + id);
  }
  holidaysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'holiday/do',
      new TCDoParam(method, payload)
    );
  }

  holidayDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'holiday/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'holiday/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'holiday/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
