import { Component, OnInit } from '@angular/core';
import { ServiceType } from '../app.enums';
import { DialysisSessionNavigator } from '../dialysis_session/dialysis_session.navigator';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { OrdersNavigator } from '../form_encounters/orderss/orders.navigator';
import { PatientNavigator } from '../patients/patients.navigator';
import { PaymentNavigator } from '../payments/payment.navigator';
import { Physiotherapy } from '../physiotherapy/physiotherapy';
import { PrescriptionRequestNavigator } from '../prescription_request/prescription_request.navigator';
import { Nurse_RecordNavigator } from '../nurse_record/nurse_Record_Navigator';
import { BedroomtypePersist } from '../bed/bedroomtypes/bedroomtype.persist';
import { AppointmentNavigator } from '../appointments/appointment.navigator';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  ward:string = "Ward"
  constructor(
    public patientNavigator: PatientNavigator,
    public form_encounterPersist: Form_EncounterPersist,
    public wardPersist: BedroomtypePersist,
    public form_encounterNavigator: Form_EncounterNavigator,
    public ordersNavigator: OrdersNavigator,
    public paymentNavigator: PaymentNavigator,
    public prescriptionRequestNavigator: PrescriptionRequestNavigator,
    public pysiotherapyNavigator: Physiotherapy,
    public nurse_recordNavigator: Nurse_RecordNavigator,
    public appointmentNavigator: AppointmentNavigator,
    public dialysisSessionNavigator: DialysisSessionNavigator,
  ) { }

  ngOnInit(): void {
    this.wardPersist.bedroomtypesDo("get_ward",{}).subscribe((value:any)=>{
      console.log(value)
      if(value.id){
        this.ward = value.id;
      }
    })
  }

  formEncounter() {
    this.form_encounterPersist.encounterSearchHistory.show_only_mine = true;
    this.form_encounterNavigator.viewForm_Encounters()
  }

  outpatient(){
    this.form_encounterPersist.encounterSearchHistory.service_type = ServiceType.outpatient;
    this.formEncounter()
  }

  inpatient() {
    this.form_encounterPersist.encounterSearchHistory.service_type = ServiceType.inpatient;
    this.form_encounterPersist.encounterSearchHistory.date = null;
    this.formEncounter()
  }
  emergency() {
    this.form_encounterPersist.encounterSearchHistory.service_type = ServiceType.emergency;
    this.formEncounter()
  }

}
