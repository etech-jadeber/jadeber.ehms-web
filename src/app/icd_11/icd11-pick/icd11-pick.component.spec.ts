import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Icd11PickComponent } from './icd11-pick.component';

describe('Icd11PickComponent', () => {
  let component: Icd11PickComponent;
  let fixture: ComponentFixture<Icd11PickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Icd11PickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Icd11PickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
