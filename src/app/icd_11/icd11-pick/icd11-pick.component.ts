import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { Icd11Summary, Icd11SummaryPartialList,Icd11Detail } from '../icd_11.model';
import { Icd11Persist } from '../icd_11.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-icd_11-pick',
  templateUrl: './icd11-pick.component.html',
  styleUrls: ['./icd11-pick.component.scss']
})
export class Icd11PickComponent implements OnInit {
  icd11sData: Icd11Summary[] = [];
  icd11sTotalCount: number = 0;
  icd11SelectAll:boolean = false;
  icd11Selection: Icd11Summary[] = [];
  category:string;
 icd11sDisplayedColumns: string[] = ["select","code","category" ];
  icd11SearchTextBox: FormControl = new FormControl();
  icd11IsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) icd11sPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) icd11sSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public icd11Persist: Icd11Persist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<Icd11PickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("icd_11");
       this.icd11SearchTextBox.setValue(icd11Persist.icd11SearchText);
      //delay subsequent keyup events
      this.icd11SearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.icd11Persist.icd11SearchText = value;
        this.searchIcd_11s();
      });
    } ngOnInit() {
      
      this.icd11Persist.icd11SearchText = "";
   
      this.icd11sSort.sortChange.subscribe(() => {
        this.icd11sPaginator.pageIndex = 0;
        this.searchIcd_11s(true);
      });

      this.icd11sPaginator.page.subscribe(() => {
        this.searchIcd_11s(true);
      });
      //start by loading items
      this.searchIcd_11s();
    }

  searchIcd_11s(isPagination:boolean = false): void {


    let paginator = this.icd11sPaginator;
    let sorter = this.icd11sSort;

    this.icd11IsLoading = true;

    this.icd11Persist.searchIcd11(isPagination ? paginator.pageSize :5, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Icd11SummaryPartialList) => {
      this.icd11sData = partialList.data;
      if (partialList.total != -1) {
        this.icd11sTotalCount = partialList.total;
      }
      this.icd11IsLoading = false;
    }, error => {
      this.icd11IsLoading = false;
    });

  }
  addNewICD11():void{
    let item :Icd11Detail= new Icd11Detail();
    item.category=this.category
    this.icd11Persist.addIcd11(item).subscribe((res)=>{
      if(res){
        this.tcNotification.success("new ICD added");
        this.category="";
        this.searchIcd_11s();
      }
    });
  }
  markOneItem(item: Icd11Summary) {
    if(!this.tcUtilsArray.containsId(this.icd11Selection,item.id)){
          this.icd11Selection = [];
          this.icd11Selection.push(item);
        }
        else{
          this.icd11Selection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.icd11Selection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  ngOnDestroy():void{
    this.icd11Persist.icd11SearchText=""
  }
  }