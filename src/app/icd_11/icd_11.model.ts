import {TCId} from "../tc/models";
export class Icd11Summary extends TCId {
   category:string;
    code:string;
     
    }
    export class Icd11SummaryPartialList {
      data: Icd11Summary[];
      total: number;
    }
    export class Icd11Detail extends Icd11Summary {
    }
    
    export class Icd11Dashboard {
      total: number;
    }