import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Icd11Dashboard, Icd11Detail, Icd11SummaryPartialList} from "./icd_11.model";


@Injectable({
  providedIn: 'root'
})
export class Icd11Persist {
 icd11SearchText: string = "";
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.icd11SearchText;
    //add custom filters
    return fltrs;
  }
 
  searchIcd11(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Icd11SummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("icd_11", this.icd11SearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Icd11SummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "icd_11/do", new TCDoParam("download_all", this.filters()));
  }

  icd11Dashboard(): Observable<Icd11Dashboard> {
    return this.http.get<Icd11Dashboard>(environment.tcApiBaseUri + "icd_11/dashboard");
  }

  getIcd11(id: string): Observable<Icd11Detail> {
    return this.http.get<Icd11Detail>(environment.tcApiBaseUri + "icd_11/" + id);
  } 
  addIcd11(item: Icd11Detail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "icd_11/", item);
  }

  updateIcd11(item: Icd11Detail): Observable<Icd11Detail> {
    return this.http.patch<Icd11Detail>(environment.tcApiBaseUri + "icd_11/" + item.id, item);
  }

  deleteIcd11(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "icd_11/" + id);
  }
 icd11sDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "icd_11/do", new TCDoParam(method, payload));
  }

  icd11Do(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "icd_11s/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "icd_11/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "icd_11/" + id + "/do", new TCDoParam("print", {}));
  }


}