import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcuPhysicalAssessmentDetailComponent } from './icu-physical-assessment-detail.component';

describe('IcuPhysicalAssessmentDetailComponent', () => {
  let component: IcuPhysicalAssessmentDetailComponent;
  let fixture: ComponentFixture<IcuPhysicalAssessmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcuPhysicalAssessmentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcuPhysicalAssessmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
