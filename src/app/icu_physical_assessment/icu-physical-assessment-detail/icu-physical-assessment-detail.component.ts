import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';
import { IcuPhysicalAssessmentDetail } from '../icu_physical_assessment.model';
import { IcuPhysicalAssessmentNavigator } from '../icu_physical_assessment.navigator';
import { IcuPhysicalAssessmentPersist } from '../icu_physical_assessment.persist';


export enum IcuPhysicalAssessmentTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-icu-physical-assessment-detail',
  templateUrl: './icu-physical-assessment-detail.component.html',
  styleUrls: ['./icu-physical-assessment-detail.component.scss'],
})
export class IcuPhysicalAssessmentDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  icuPhysicalAssessmentIsLoading: boolean = false;

  IcuPhysicalAssessmentTabs: typeof IcuPhysicalAssessmentTabs =
    IcuPhysicalAssessmentTabs;
  activeTab: IcuPhysicalAssessmentTabs = IcuPhysicalAssessmentTabs.overview;
  //basics
  icuPhysicalAssessmentDetail: IcuPhysicalAssessmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public icuPhysicalAssessmentNavigator: IcuPhysicalAssessmentNavigator,
    public icuPhysicalAssessmentPersist: IcuPhysicalAssessmentPersist
  ) {
    this.tcAuthorization.requireRead('icu_physical_assessments');
    this.icuPhysicalAssessmentDetail = new IcuPhysicalAssessmentDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('icu_physical_assessments');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.icuPhysicalAssessmentIsLoading = true;
    this.icuPhysicalAssessmentPersist.getIcuPhysicalAssessment(id).subscribe(
      (icuPhysicalAssessmentDetail) => {
        this.icuPhysicalAssessmentDetail = icuPhysicalAssessmentDetail;
        this.icuPhysicalAssessmentIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.icuPhysicalAssessmentIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.icuPhysicalAssessmentDetail.id);
  }
  editIcuPhysicalAssessment(): void {
    let modalRef =
      this.icuPhysicalAssessmentNavigator.editIcuPhysicalAssessment(
        this.icuPhysicalAssessmentDetail.id
      );
    modalRef.afterClosed().subscribe(
      (icuPhysicalAssessmentDetail) => {
        TCUtilsAngular.assign(
          this.icuPhysicalAssessmentDetail,
          icuPhysicalAssessmentDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.icuPhysicalAssessmentPersist
      .print(this.icuPhysicalAssessmentDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(
          printJob.id,
          'print icu_physical_assessment',
          true
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
