import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcuPhysicalAssessmentEditComponent } from './icu-physical-assessment-edit.component';

describe('IcuPhysicalAssessmentEditComponent', () => {
  let component: IcuPhysicalAssessmentEditComponent;
  let fixture: ComponentFixture<IcuPhysicalAssessmentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcuPhysicalAssessmentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcuPhysicalAssessmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
