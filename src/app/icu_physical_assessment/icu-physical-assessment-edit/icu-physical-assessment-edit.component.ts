import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { IcuPhysicalAssessmentDetail } from '../icu_physical_assessment.model';
import { IcuPhysicalAssessmentPersist } from '../icu_physical_assessment.persist';
@Component({
  selector: 'app-icu-physical-assessment-edit',
  templateUrl: './icu-physical-assessment-edit.component.html',
  styleUrls: ['./icu-physical-assessment-edit.component.scss'],
})
export class IcuPhysicalAssessmentEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;

  foley_insert_date: string;
  date_of_last_bm: string ;
  ng_og_insertion_date: string;
  icuPhysicalAssessmentDetail: IcuPhysicalAssessmentDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<IcuPhysicalAssessmentEditComponent>,
    public persist: IcuPhysicalAssessmentPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('icu_physical_assessments');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'ICU Physical Assessment';
      this.icuPhysicalAssessmentDetail = new IcuPhysicalAssessmentDetail();

      this.icuPhysicalAssessmentDetail.encounter_id = this.idMode.id;
      this.icuPhysicalAssessmentDetail.avpu = "";
      this.icuPhysicalAssessmentDetail.ett_size = "";
      this.icuPhysicalAssessmentDetail.intact_cm = "";
      this.icuPhysicalAssessmentDetail.track_intact = "";
      this.icuPhysicalAssessmentDetail.track_care = false;
      this.icuPhysicalAssessmentDetail.vent_circuit_checked = false;
      this.icuPhysicalAssessmentDetail.bac_filter = false;
      this.icuPhysicalAssessmentDetail.humidifier = false;
      this.icuPhysicalAssessmentDetail.placement_checked = false;
      this.icuPhysicalAssessmentDetail.drianage = "";
      this.icuPhysicalAssessmentDetail.abd_soft = false;
      this.icuPhysicalAssessmentDetail.abd_distended = false;
      this.icuPhysicalAssessmentDetail.vomiting = false;
      this.icuPhysicalAssessmentDetail.diarrhea = false;
      this.icuPhysicalAssessmentDetail.consistency = "";
      this.icuPhysicalAssessmentDetail.foley = false;
      this.icuPhysicalAssessmentDetail.condom_catheter = false;
      this.icuPhysicalAssessmentDetail.foley_care_provided = "";
      this.icuPhysicalAssessmentDetail.urine_appearance = "";
      this.icuPhysicalAssessmentDetail.breakdown = false;
      this.icuPhysicalAssessmentDetail.intact = false;
      this.icuPhysicalAssessmentDetail.bedsore = false;
      this.icuPhysicalAssessmentDetail.wound_care = false;
      this.icuPhysicalAssessmentDetail.bruise = false;
      this.icuPhysicalAssessmentDetail.edema = false;
      this.icuPhysicalAssessmentDetail.perianal_care = false;
      this.icuPhysicalAssessmentDetail.bed_bath = false;

      this.icuPhysicalAssessmentDetail.speak_clearly = "";
      this.icuPhysicalAssessmentDetail.neuro_flowsheet = false;
      this.icuPhysicalAssessmentDetail.pupil = "";
      this.icuPhysicalAssessmentDetail.follows_commands = false;
      this.icuPhysicalAssessmentDetail.nods_appropriately_to_questions = false;
      this.icuPhysicalAssessmentDetail.all_extremities_equal_in_strength = false;
      this.icuPhysicalAssessmentDetail.hop_up_degree = "";
      this.icuPhysicalAssessmentDetail.brisk_cap_refill = false;
      this.icuPhysicalAssessmentDetail.peripheral_edema = "";
      this.icuPhysicalAssessmentDetail.skin = "";
      this.icuPhysicalAssessmentDetail.heard = false;
      this.icuPhysicalAssessmentDetail.csm = false;
      this.icuPhysicalAssessmentDetail.line_passed = false;
      this.icuPhysicalAssessmentDetail.line_zeroed = false;
      this.icuPhysicalAssessmentDetail.rt_radial_pulse = "";
      this.icuPhysicalAssessmentDetail.lt_radial_pulse = "";
      this.icuPhysicalAssessmentDetail.description = "";
      this.icuPhysicalAssessmentDetail.breath_sound_clear_bilat = false;
      this.icuPhysicalAssessmentDetail.breath_sound_clear_after_suctioned = false;
      this.icuPhysicalAssessmentDetail.r_pedal_pulse = "";
      this.icuPhysicalAssessmentDetail.l_pedal_pulse = "";
      this.icuPhysicalAssessmentDetail.exp_wheeze = false;
      this.icuPhysicalAssessmentDetail.insp_wheeze = false;
      this.icuPhysicalAssessmentDetail.abd = "";
      this.icuPhysicalAssessmentDetail.ng_tube = false;
      this.icuPhysicalAssessmentDetail.bowel_sound = false;
      this.icuPhysicalAssessmentDetail.hypoactive_bowel_sound = false;
      this.icuPhysicalAssessmentDetail.absenet_bowel_sound = false;
      this.icuPhysicalAssessmentDetail.ostomy_stoma_color = "";
      this.icuPhysicalAssessmentDetail.suction = false;
      this.icuPhysicalAssessmentDetail.gravity = false;
      this.icuPhysicalAssessmentDetail.bruise_dicoration = "";
      this.icuPhysicalAssessmentDetail.breath_sound_clear_with_suction = false;
      this.icuPhysicalAssessmentDetail.breath_sound_decrease_on = "";
      this.icuPhysicalAssessmentDetail.crackle = "";
      this.icuPhysicalAssessmentDetail.bubbling = "";
      this.icuPhysicalAssessmentDetail.og_tube = false;
      this.icuPhysicalAssessmentDetail.chest_tube = "";
      this.icuPhysicalAssessmentDetail.gcs = "";
      this.icuPhysicalAssessmentDetail.seizura = "";
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('icu_physical_assessments');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'ICU Physical Assessment';
      this.isLoadingResults = true;
      this.persist.getIcuPhysicalAssessment(this.idMode.id).subscribe(
        (icuPhysicalAssessmentDetail) => {
          this.icuPhysicalAssessmentDetail = icuPhysicalAssessmentDetail;
          this.transformDates(false);
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist
      .addIcuPhysicalAssessment(this.icuPhysicalAssessmentDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('icuPhysicalAssessment added');
          this.icuPhysicalAssessmentDetail.id = value.id;
          this.dialogRef.close(this.icuPhysicalAssessmentDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }


  transformDates(todate: boolean = true) {
    if (todate) {
      this.icuPhysicalAssessmentDetail.foley_insert_date = this.foley_insert_date ?
        new Date(this.foley_insert_date).getTime() / 1000 : null;
      this.icuPhysicalAssessmentDetail.date_of_last_bm = this.date_of_last_bm ?
        new Date(this.date_of_last_bm).getTime() / 1000 : null;
        this.icuPhysicalAssessmentDetail.ng_og_insertion_date = this.ng_og_insertion_date ?
        new Date(this.ng_og_insertion_date).getTime() / 1000 : null;
    } else {
      this.foley_insert_date = this.icuPhysicalAssessmentDetail.foley_insert_date &&  this.getStartTime(this.tcUtilsDate.toDate(this.icuPhysicalAssessmentDetail.foley_insert_date));
      this.date_of_last_bm = this.icuPhysicalAssessmentDetail.date_of_last_bm && this.getStartTime(this.tcUtilsDate.toDate(this.icuPhysicalAssessmentDetail.date_of_last_bm));
      this.ng_og_insertion_date = this.icuPhysicalAssessmentDetail.ng_og_insertion_date && this.getStartTime(this.tcUtilsDate.toDate(this.icuPhysicalAssessmentDetail.ng_og_insertion_date));
    }
  }
  
  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}`;
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist
      .updateIcuPhysicalAssessment(this.icuPhysicalAssessmentDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('icu_physical_assessment updated');
          this.dialogRef.close(this.icuPhysicalAssessmentDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }
  canSubmit(): boolean {
    if (this.icuPhysicalAssessmentDetail == null) {
      return false;
    }
    return true;
  }

  isDisabled():boolean{
    return false;
  }
}
