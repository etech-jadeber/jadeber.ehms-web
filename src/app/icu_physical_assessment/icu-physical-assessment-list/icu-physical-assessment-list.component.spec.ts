import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcuPhysicalAssessmentListComponent } from './icu-physical-assessment-list.component';

describe('IcuPhysicalAssessmentListComponent', () => {
  let component: IcuPhysicalAssessmentListComponent;
  let fixture: ComponentFixture<IcuPhysicalAssessmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcuPhysicalAssessmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcuPhysicalAssessmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
