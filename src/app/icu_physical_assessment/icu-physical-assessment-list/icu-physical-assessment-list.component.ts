import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { IcuPhysicalAssessmentSummary, IcuPhysicalAssessmentSummaryPartialList } from '../icu_physical_assessment.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { IcuPhysicalAssessmentNavigator } from '../icu_physical_assessment.navigator';
import { IcuPhysicalAssessmentPersist } from '../icu_physical_assessment.persist';
@Component({
  selector: 'app-icu-physical-assessment-list',
  templateUrl: './icu-physical-assessment-list.component.html',
  styleUrls: ['./icu-physical-assessment-list.component.scss']
})export class IcuPhysicalAssessmentListComponent implements OnInit {
  icuPhysicalAssessmentsData: IcuPhysicalAssessmentSummary[] = [];
  icuPhysicalAssessmentsTotalCount: number = 0;
  icuPhysicalAssessmentSelectAll:boolean = false;
  icuPhysicalAssessmentSelection: IcuPhysicalAssessmentSummary[] = [];

 icuPhysicalAssessmentsDisplayedColumns: string[] = ["select","action" ,"bed_bath" ];
  icuPhysicalAssessmentSearchTextBox: FormControl = new FormControl();
  icuPhysicalAssessmentIsLoading: boolean = false;  
  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) icuPhysicalAssessmentsPaginator: MatPaginator;
  // @ViewChild(MatSort, {static: true}) icuPhysicalAssessmentsSort: MatSort;  
  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public icuPhysicalAssessmentPersist: IcuPhysicalAssessmentPersist,
                public icuPhysicalAssessmentNavigator: IcuPhysicalAssessmentNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("icu_physical_assessments");
       this.icuPhysicalAssessmentSearchTextBox.setValue(icuPhysicalAssessmentPersist.icuPhysicalAssessmentSearchText);
      //delay subsequent keyup events
      this.icuPhysicalAssessmentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.icuPhysicalAssessmentPersist.icuPhysicalAssessmentSearchText = value;
        this.searchIcu_physical_assessments();
      });
    } ngOnInit() {
   
      // this.icuPhysicalAssessmentsSort.sortChange.subscribe(() => {
      //   this.icuPhysicalAssessmentsPaginator.pageIndex = 0;
      //   this.searchIcu_physical_assessments(true);
      // });

      this.icuPhysicalAssessmentsPaginator.page.subscribe(() => {
        this.searchIcu_physical_assessments(true);
      });
      //start by loading items
      this.icuPhysicalAssessmentPersist.encounter_id = this.encounterId;
      this.searchIcu_physical_assessments();
    }

  searchIcu_physical_assessments(isPagination:boolean = false): void {


    let paginator = this.icuPhysicalAssessmentsPaginator;
    // let sorter = this.icuPhysicalAssessmentsSort;

    this.icuPhysicalAssessmentIsLoading = true;
    this.icuPhysicalAssessmentSelection = [];

    this.icuPhysicalAssessmentPersist.searchIcuPhysicalAssessment(paginator.pageSize, isPagination? paginator.pageIndex:0, "date", "desc").subscribe((partialList: IcuPhysicalAssessmentSummaryPartialList) => {
      this.icuPhysicalAssessmentsData = partialList.data;
      if (partialList.total != -1) {
        this.icuPhysicalAssessmentsTotalCount = partialList.total;
      }
      this.icuPhysicalAssessmentIsLoading = false;
    }, error => {
      this.icuPhysicalAssessmentIsLoading = false;
    });

  } downloadIcuPhysicalAssessments(): void {
    if(this.icuPhysicalAssessmentSelectAll){
         this.icuPhysicalAssessmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download icu_physical_assessment", true);
      });
    }
    else{
        this.icuPhysicalAssessmentPersist.download(this.tcUtilsArray.idsList(this.icuPhysicalAssessmentSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download icu_physical_assessment",true);
            });
        }
  }
addIcu_physical_assessment(): void {
    let dialogRef = this.icuPhysicalAssessmentNavigator.addIcuPhysicalAssessment(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchIcu_physical_assessments();
      }
    });
  }

  editIcuPhysicalAssessment(item: IcuPhysicalAssessmentSummary) {
    let dialogRef = this.icuPhysicalAssessmentNavigator.editIcuPhysicalAssessment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteIcuPhysicalAssessment(item: IcuPhysicalAssessmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("icu_physical_assessment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.icuPhysicalAssessmentPersist.deleteIcuPhysicalAssessment(item.id).subscribe(response => {
          this.tcNotification.success("icu_physical_assessment deleted");
          this.searchIcu_physical_assessments();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    isDisabled():boolean{
      return true;
    }

    ngOnDestroy():void {
      this.icuPhysicalAssessmentPersist.encounter_id = null;
    }
}