import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcuPhysicalAssessmentPickComponent } from './icu-physical-assessment-pick.component';

describe('IcuPhysicalAssessmentPickComponent', () => {
  let component: IcuPhysicalAssessmentPickComponent;
  let fixture: ComponentFixture<IcuPhysicalAssessmentPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcuPhysicalAssessmentPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcuPhysicalAssessmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
