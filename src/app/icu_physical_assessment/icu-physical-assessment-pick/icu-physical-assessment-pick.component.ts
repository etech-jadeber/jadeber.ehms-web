import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import { IcuPhysicalAssessmentSummary, IcuPhysicalAssessmentSummaryPartialList } from '../icu_physical_assessment.model';
import { IcuPhysicalAssessmentPersist } from '../icu_physical_assessment.persist';
@Component({
  selector: 'app-icu-physical-assessment-pick',
  templateUrl: './icu-physical-assessment-pick.component.html',
  styleUrls: ['./icu-physical-assessment-pick.component.scss'],
})
export class IcuPhysicalAssessmentPickComponent implements OnInit {
  icuPhysicalAssessmentsData: IcuPhysicalAssessmentSummary[] = [];
  icuPhysicalAssessmentsTotalCount: number = 0;
  icuPhysicalAssessmentSelectAll: boolean = false;
  icuPhysicalAssessmentSelection: IcuPhysicalAssessmentSummary[] = [];

  icuPhysicalAssessmentsDisplayedColumns: string[] = [
    'select',
    'action',
    'bed_bath',
  ];
  icuPhysicalAssessmentSearchTextBox: FormControl = new FormControl();
  icuPhysicalAssessmentIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  icuPhysicalAssessmentsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) icuPhysicalAssessmentsSort: MatSort;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<IcuPhysicalAssessmentPickComponent>,
    public icuPhysicalAssessmentPersist: IcuPhysicalAssessmentPersist,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('icu_physical_assessments');
    this.icuPhysicalAssessmentSearchTextBox.setValue(
      icuPhysicalAssessmentPersist.icuPhysicalAssessmentSearchText
    );
    //delay subsequent keyup events
    this.icuPhysicalAssessmentSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.icuPhysicalAssessmentPersist.icuPhysicalAssessmentSearchText =
          value;
        this.searchIcu_physical_assessments();
      });
  }
  ngOnInit() {
    this.icuPhysicalAssessmentsSort.sortChange.subscribe(() => {
      this.icuPhysicalAssessmentsPaginator.pageIndex = 0;
      this.searchIcu_physical_assessments(true);
    });

    this.icuPhysicalAssessmentsPaginator.page.subscribe(() => {
      this.searchIcu_physical_assessments(true);
    });
    //start by loading items
    this.searchIcu_physical_assessments();
  }

  searchIcu_physical_assessments(isPagination: boolean = false): void {
    let paginator = this.icuPhysicalAssessmentsPaginator;
    let sorter = this.icuPhysicalAssessmentsSort;

    this.icuPhysicalAssessmentIsLoading = true;
    this.icuPhysicalAssessmentSelection = [];

    this.icuPhysicalAssessmentPersist
      .searchIcuPhysicalAssessment(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: IcuPhysicalAssessmentSummaryPartialList) => {
          this.icuPhysicalAssessmentsData = partialList.data;
          if (partialList.total != -1) {
            this.icuPhysicalAssessmentsTotalCount = partialList.total;
          }
          this.icuPhysicalAssessmentIsLoading = false;
        },
        (error) => {
          this.icuPhysicalAssessmentIsLoading = false;
        }
      );
  }
  markOneItem(item: IcuPhysicalAssessmentSummary) {
    if (
      !this.tcUtilsArray.containsId(
        this.icuPhysicalAssessmentSelection,
        item.id
      )
    ) {
      this.icuPhysicalAssessmentSelection = [];
      this.icuPhysicalAssessmentSelection.push(item);
    } else {
      this.icuPhysicalAssessmentSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.icuPhysicalAssessmentSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
