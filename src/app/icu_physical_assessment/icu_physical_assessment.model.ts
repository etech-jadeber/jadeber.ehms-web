import { TCId } from '../tc/models';
export class IcuPhysicalAssessmentSummary extends TCId {
  avpu: string;
  gag_reflex_intact: boolean;
  cough_reflex_intact: boolean;
  rt_palpable: boolean;
  lt_palpable: boolean;
  lt_absent: boolean;
  rt_absent: boolean;
  ett_size: string;
  intact_cm: string;
  track_intact: string;
  track_care: boolean;
  color: string;
  amount: string;
  vent_circuit_checked: boolean;
  bac_filter: boolean;
  humidifier: boolean;
  ng_og_tube: boolean;
  placement_checked: boolean;
  drianage: string;
  abd_soft: boolean;
  abd_distended: boolean;
  vomiting: boolean;
  diarrhea: boolean;
  consistency: string;
  date_of_last_bm: number;
  foley: boolean;
  condom_catheter: boolean;
  foley_insert_date: number;
  foley_care_provided: string;
  urine_appearance: string;
  breakdown: boolean;
  intact: boolean;
  bedsore: boolean;
  wound_care: boolean;
  bruise: boolean;
  edema: boolean;
  perianal_care: boolean;
  bed_bath: boolean;

  speak_clearly: string;
  neuro_flowsheet: boolean;
  pupil: string;
  follows_commands: boolean;
  nods_appropriately_to_questions: boolean;
  all_extremities_equal_in_strength: boolean;
  hop_up_degree: string;
  brisk_cap_refill: boolean;
  peripheral_edema: string;
  skin: string;
  heard: boolean;
  csm: boolean;
  line_passed: boolean;
  line_zeroed: boolean;
  rt_radial_pulse: string;
  lt_radial_pulse: string;
  description: string;
  breath_sound_clear_bilat: boolean;
  breath_sound_clear_after_suctioned: boolean;
  r_pedal_pulse: string;
  l_pedal_pulse: string;
  exp_wheeze: boolean;
  insp_wheeze: boolean;
  abd: string;
  ng_tube: boolean;
  bowel_sound: boolean;
  hypoactive_bowel_sound: boolean;
  absenet_bowel_sound: boolean;
  ostomy_stoma_color: string;
  suction: boolean;
  gravity: boolean;
  bruise_dicoration: string;
  breath_sound_clear_with_suction: boolean;
  breath_sound_decrease_on: string;
  crackle: string;
  bubbling: string;
  chest_tube: string;
  og_tube: boolean;
  patient_id: string;
  encounter_id: string;
  gcs: string;
  seizura: string;
  ng_og_insertion_date:number;
  date: number;
}
export class IcuPhysicalAssessmentSummaryPartialList {
  data: IcuPhysicalAssessmentSummary[];
  total: number;
}
export class IcuPhysicalAssessmentDetail extends IcuPhysicalAssessmentSummary {

}

export class IcuPhysicalAssessmentDashboard {
  total: number;
}
