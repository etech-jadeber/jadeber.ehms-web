import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';
import { IcuPhysicalAssessmentEditComponent } from './icu-physical-assessment-edit/icu-physical-assessment-edit.component';
import { IcuPhysicalAssessmentPickComponent } from './icu-physical-assessment-pick/icu-physical-assessment-pick.component';


@Injectable({
  providedIn: 'root',
})
export class IcuPhysicalAssessmentNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  icu_physical_assessmentsUrl(): string {
    return '/icu_physical_assessments';
  }

  icu_physical_assessmentUrl(id: string): string {
    return '/icu_physical_assessments/' + id;
  }

  viewIcuPhysicalAssessments(): void {
    this.router.navigateByUrl(this.icu_physical_assessmentsUrl());
  }

  viewIcuPhysicalAssessment(id: string): void {
    this.router.navigateByUrl(this.icu_physical_assessmentUrl(id));
  }

  editIcuPhysicalAssessment(
    id: string
  ): MatDialogRef<IcuPhysicalAssessmentEditComponent> {
    const dialogRef = this.dialog.open(IcuPhysicalAssessmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  addIcuPhysicalAssessment(encounter_id: string): MatDialogRef<IcuPhysicalAssessmentEditComponent> {
    const dialogRef = this.dialog.open(IcuPhysicalAssessmentEditComponent, {
      data: new TCIdMode(encounter_id, TCModalModes.NEW),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  pickIcuPhysicalAssessments(
    selectOne: boolean = false
  ): MatDialogRef<IcuPhysicalAssessmentPickComponent> {
    const dialogRef = this.dialog.open(IcuPhysicalAssessmentPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
