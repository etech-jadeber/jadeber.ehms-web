import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  IcuPhysicalAssessmentDashboard,
  IcuPhysicalAssessmentDetail,
  IcuPhysicalAssessmentSummaryPartialList,
} from './icu_physical_assessment.model';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class IcuPhysicalAssessmentPersist {
  icuPhysicalAssessmentSearchText: string = '';
  encounter_id: string;
  constructor(private http: HttpClient) {}

  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.icuPhysicalAssessmentSearchText;
    //add custom filters
    return fltrs;
  }

  searchIcuPhysicalAssessment(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<IcuPhysicalAssessmentSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'icu_physical_assessment',
      this.icuPhysicalAssessmentSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if(this.encounter_id){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounter_id)
    }
    return this.http.get<IcuPhysicalAssessmentSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'icu_physical_assessment/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  icuPhysicalAssessmentDashboard(): Observable<IcuPhysicalAssessmentDashboard> {
    return this.http.get<IcuPhysicalAssessmentDashboard>(
      environment.tcApiBaseUri + 'icu_physical_assessment/dashboard'
    );
  }

  getIcuPhysicalAssessment(
    id: string
  ): Observable<IcuPhysicalAssessmentDetail> {
    return this.http.get<IcuPhysicalAssessmentDetail>(
      environment.tcApiBaseUri + 'icu_physical_assessment/' + id
    );
  }
  addIcuPhysicalAssessment(item: IcuPhysicalAssessmentDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "icu_physical_assessment",
      item
    );
  }

  updateIcuPhysicalAssessment(item: IcuPhysicalAssessmentDetail): Observable<IcuPhysicalAssessmentDetail> {
    return this.http.patch<IcuPhysicalAssessmentDetail>(
      environment.tcApiBaseUri + 'icu_physical_assessment/' + item.id,
      item
    );
  }

  deleteIcuPhysicalAssessment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'icu_physical_assessment/' + id);
  }
  icuPhysicalAssessmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'icu_physical_assessment/do',
      new TCDoParam(method, payload)
    );
  }

  icuPhysicalAssessmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'icu_physical_assessment/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'icu_physical_assessment/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "icu_physical_assessment/" + id + "/do", new TCDoParam("print", {}));
  }


}
