import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Insurance_CompanyDashboard, Insurance_CompanyDetail, Insurance_CompanySummaryPartialList} from "./insurance_company.model";


@Injectable({
  providedIn: 'root'
})
export class Insurance_CompanyPersist {

  insurance_companySearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchInsurance_Company(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Insurance_CompanySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("insurance_companys", this.insurance_companySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Insurance_CompanySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.insurance_companySearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "insurance_companys/do", new TCDoParam("download_all", this.filters()));
  }

  insurance_companyDashboard(): Observable<Insurance_CompanyDashboard> {
    return this.http.get<Insurance_CompanyDashboard>(environment.tcApiBaseUri + "insurance_companys/dashboard");
  }

  getInsurance_Company(id: string): Observable<Insurance_CompanyDetail> {
    return this.http.get<Insurance_CompanyDetail>(environment.tcApiBaseUri + "insurance_companys/" + id);
  }

  addInsurance_Company(item: Insurance_CompanyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "insurance_companys/", item);
  }

  updateInsurance_Company(item: Insurance_CompanyDetail): Observable<Insurance_CompanyDetail> {
    return this.http.patch<Insurance_CompanyDetail>(environment.tcApiBaseUri + "insurance_companys/" + item.id, item);
  }

  deleteInsurance_Company(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "insurance_companys/" + id);
  }

  insurance_companysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "insurance_companys/do", new TCDoParam(method, payload));
  }

  insurance_companyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "insurance_companys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "insurance_companys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "insurance_companys/" + id + "/do", new TCDoParam("print", {}));
  }


}
