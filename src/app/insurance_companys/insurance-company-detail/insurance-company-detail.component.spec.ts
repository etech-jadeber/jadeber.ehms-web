import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuranceCompanyDetailComponent } from './insurance-company-detail.component';

describe('InsuranceCompanyDetailComponent', () => {
  let component: InsuranceCompanyDetailComponent;
  let fixture: ComponentFixture<InsuranceCompanyDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceCompanyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceCompanyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
