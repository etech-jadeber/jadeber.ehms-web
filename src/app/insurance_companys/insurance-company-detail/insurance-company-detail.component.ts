import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Insurance_CompanyDetail} from "../insurance_company.model";
import {Insurance_CompanyPersist} from "../ insurance_company.persist";
import {Insurance_CompanyNavigator} from "../insurance_company.navigator";

export enum Insurance_CompanyTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-insurance_company-detail',
  templateUrl: './insurance-company-detail.component.html',
  styleUrls: ['./insurance-company-detail.component.css']
})
export class InsuranceCompanyDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  insurance_companyLoading:boolean = false;
  
  Insurance_CompanyTabs: typeof Insurance_CompanyTabs = Insurance_CompanyTabs;
  activeTab: Insurance_CompanyTabs = Insurance_CompanyTabs.overview;
  //basics
  insurance_companyDetail: Insurance_CompanyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public insurance_companyNavigator: Insurance_CompanyNavigator,
              public  insurance_companyPersist: Insurance_CompanyPersist) {
    this.tcAuthorization.requireRead("insurance_companys");
    this.insurance_companyDetail = new Insurance_CompanyDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("insurance_companys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.insurance_companyLoading = true;
    this.insurance_companyPersist.getInsurance_Company(id).subscribe(insurance_companyDetail => {
          this.insurance_companyDetail = insurance_companyDetail;
          this.insurance_companyLoading = false;
        }, error => {
          console.error(error);
          this.insurance_companyLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.insurance_companyDetail.id);
  }

  editInsurance_Company(): void {
    let modalRef = this.insurance_companyNavigator.editInsurance_Company(this.insurance_companyDetail.id);
    modalRef.afterClosed().subscribe(modifiedInsurance_CompanyDetail => {
      TCUtilsAngular.assign(this.insurance_companyDetail, modifiedInsurance_CompanyDetail);
    }, error => {
      console.error(error);
    });
  }

   printInsurance_Company():void{
      this.insurance_companyPersist.print(this.insurance_companyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print insurance_company", true);
      });
    }

  back():void{
      this.location.back();
    }

}
