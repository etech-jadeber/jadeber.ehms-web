import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Insurance_CompanyDetail, Insurance_CompanySummary, Insurance_CompanySummaryPartialList} from "../insurance_company.model";
import {Insurance_CompanyPersist} from "../ insurance_company.persist";


@Component({
  selector: 'app-insurance_company-edit',
  templateUrl: './insurance-company-edit.component.html',
  styleUrls: ['./insurance-company-edit.component.css']
})
export class InsuranceCompanyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  insurance_companyDetail: Insurance_CompanyDetail;
  insuranceCompanes: Insurance_CompanySummary[] = [];
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<InsuranceCompanyEditComponent>,
              public  persist: Insurance_CompanyPersist,
              public insuranceCompanyPersist: Insurance_CompanyPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("insurance_companys");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("inventory","insurance_company") ;
      this.insurance_companyDetail = new Insurance_CompanyDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("insurance_companys");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("inventory","insurance_company");
      this.isLoadingResults = true;
      this.persist.getInsurance_Company(this.idMode.id).subscribe(insurance_companyDetail => {
        this.insurance_companyDetail = insurance_companyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addInsurance_Company(this.insurance_companyDetail).subscribe(value => {
      this.tcNotification.success("Insurance_Company added");
      this.insurance_companyDetail.id = value.id;
      this.dialogRef.close(this.insurance_companyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateInsurance_Company(this.insurance_companyDetail).subscribe(value => {
      this.tcNotification.success("Insurance_Company updated");
      this.dialogRef.close(this.insurance_companyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.insurance_companyDetail == null){
            return false;
          }

        if (this.insurance_companyDetail.name == null || this.insurance_companyDetail.name  == "") {
                      return false;
                    }


        return true;
      }


}
