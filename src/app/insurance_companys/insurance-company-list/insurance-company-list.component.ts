import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Insurance_CompanyPersist} from "../ insurance_company.persist";
import {Insurance_CompanyNavigator} from "../insurance_company.navigator";
import {Insurance_CompanyDetail, Insurance_CompanySummary, Insurance_CompanySummaryPartialList} from "../insurance_company.model";


@Component({
  selector: 'app-insurance-company-list',
  templateUrl: './insurance-company-list.component.html',
  styleUrls: ['./insurance-company-list.component.css']
})
export class InsuranceCompanyListComponent implements OnInit {

  insurance_companysData: Insurance_CompanySummary[] = [];
  insurance_companysTotalCount: number = 0;
  insurance_companysSelectAll:boolean = false;
  insurance_companysSelection: Insurance_CompanySummary[] = [];

  insurance_companysDisplayedColumns: string[] = ["select","action", "name", ];
  insurance_companysSearchTextBox: FormControl = new FormControl();
  insurance_companysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) insurance_companysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) insurance_companysSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public insurance_companyPersist: Insurance_CompanyPersist,
                public insurance_companyNavigator: Insurance_CompanyNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("insurance_companys");
       this.insurance_companysSearchTextBox.setValue(insurance_companyPersist.insurance_companySearchText);
      //delay subsequent keyup events
      this.insurance_companysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.insurance_companyPersist.insurance_companySearchText = value;
        this.searchInsurance_Companys();
      });
    }

    ngOnInit() {

      this.insurance_companysSort.sortChange.subscribe(() => {
        this.insurance_companysPaginator.pageIndex = 0;
        this.searchInsurance_Companys(true);
      });

      this.insurance_companysPaginator.page.subscribe(() => {
        this.searchInsurance_Companys(true);
      });
      //start by loading items
      this.searchInsurance_Companys();
    }

  searchInsurance_Companys(isPagination:boolean = false): void {


    let paginator = this.insurance_companysPaginator;
    let sorter = this.insurance_companysSort;

    this.insurance_companysIsLoading = true;
    this.insurance_companysSelection = [];

    this.insurance_companyPersist.searchInsurance_Company(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Insurance_CompanySummaryPartialList) => {
      this.insurance_companysData = partialList.data;
      if (partialList.total != -1) {
        this.insurance_companysTotalCount = partialList.total;
      }
      this.insurance_companysIsLoading = false;
    }, error => {
      this.insurance_companysIsLoading = false;
    });

  }

  downloadInsurance_Companys(): void {
    if(this.insurance_companysSelectAll){
         this.insurance_companyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download insurance_companys", true);
      });
    }
    else{
        this.insurance_companyPersist.download(this.tcUtilsArray.idsList(this.insurance_companysSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download insurance_companys",true);
            });
        }
  }



  addInsurance_Company(): void {
    let dialogRef = this.insurance_companyNavigator.addInsurance_Company();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchInsurance_Companys();
      }
    });
  }

  editInsurance_Company(item: Insurance_CompanySummary) {
    let dialogRef = this.insurance_companyNavigator.editInsurance_Company(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteInsurance_Company(item: Insurance_CompanySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Insurance_Company");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.insurance_companyPersist.deleteInsurance_Company(item.id).subscribe(response => {
          this.tcNotification.success("Insurance_Company deleted");
          this.searchInsurance_Companys();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
