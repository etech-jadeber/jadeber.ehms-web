import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuranceCompanyPickComponent } from './insurance-company-pick.component';

describe('InsuranceCompanyPickComponent', () => {
  let component: InsuranceCompanyPickComponent;
  let fixture: ComponentFixture<InsuranceCompanyPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceCompanyPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceCompanyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
