import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Insurance_CompanyDetail, Insurance_CompanySummary, Insurance_CompanySummaryPartialList} from "../insurance_company.model";
import {Insurance_CompanyPersist} from "../ insurance_company.persist";


@Component({
  selector: 'app-insurance_company-pick',
  templateUrl: './insurance-company-pick.component.html',
  styleUrls: ['./insurance-company-pick.component.css']
})
export class InsuranceCompanyPickComponent implements OnInit {

  insurance_companysData: Insurance_CompanySummary[] = [];
  insurance_companysTotalCount: number = 0;
  insurance_companysSelection: Insurance_CompanySummary[] = [];
  insurance_companysDisplayedColumns: string[] = ["select", 'name' ];

  insurance_companysSearchTextBox: FormControl = new FormControl();
  insurance_companysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) insurance_companysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) insurance_companysSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public insurance_companyPersist: Insurance_CompanyPersist,
              public dialogRef: MatDialogRef<InsuranceCompanyPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("insurance_companys");
    this.insurance_companysSearchTextBox.setValue(insurance_companyPersist.insurance_companySearchText);
    //delay subsequent keyup events
    this.insurance_companysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.insurance_companyPersist.insurance_companySearchText = value;
      this.searchInsurance_Companys();
    });
  }

  ngOnInit() {

    this.insurance_companysSort.sortChange.subscribe(() => {
      this.insurance_companysPaginator.pageIndex = 0;
      this.searchInsurance_Companys();
    });

    this.insurance_companysPaginator.page.subscribe(() => {
      this.searchInsurance_Companys();
    });

    //set initial picker list to 5
    this.insurance_companysPaginator.pageSize = 5;

    //start by loading items
    this.searchInsurance_Companys();
  }

  searchInsurance_Companys(): void {
    this.insurance_companysIsLoading = true;
    this.insurance_companysSelection = [];

    this.insurance_companyPersist.searchInsurance_Company(this.insurance_companysPaginator.pageSize,
        this.insurance_companysPaginator.pageIndex,
        this.insurance_companysSort.active,
        this.insurance_companysSort.direction).subscribe((partialList: Insurance_CompanySummaryPartialList) => {
      this.insurance_companysData = partialList.data;
      if (partialList.total != -1) {
        this.insurance_companysTotalCount = partialList.total;
      }
      this.insurance_companysIsLoading = false;
    }, error => {
      this.insurance_companysIsLoading = false;
    });

  }

  markOneItem(item: Insurance_CompanySummary) {
    if(!this.tcUtilsArray.containsId(this.insurance_companysSelection,item.id)){
          this.insurance_companysSelection = [];
          this.insurance_companysSelection.push(item);
        }
        else{
          this.insurance_companysSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.insurance_companysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
