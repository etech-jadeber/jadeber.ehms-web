import {TCId} from "../tc/models";

export class Insurance_CompanySummary extends TCId {
  name : string;
}

export class Insurance_CompanySummaryPartialList {
  data: Insurance_CompanySummary[];
  total: number;
}

export class Insurance_CompanyDetail extends Insurance_CompanySummary {
  name : string;
}

export class Insurance_CompanyDashboard {
  total: number;
}
