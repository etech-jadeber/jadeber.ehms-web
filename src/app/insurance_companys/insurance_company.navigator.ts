import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {InsuranceCompanyEditComponent} from "./insurance-company-edit/insurance-company-edit.component";
import {InsuranceCompanyPickComponent} from "./insurance-company-pick/insurance-company-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Insurance_CompanyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  insurance_companysUrl(): string {
    return "/insurance_companys";
  }

  insurance_companyUrl(id: string): string {
    return "/insurance_companys/" + id;
  }

  viewInsurance_Companys(): void {
    this.router.navigateByUrl(this.insurance_companysUrl());
  }

  viewInsurance_Company(id: string): void {
    this.router.navigateByUrl(this.insurance_companyUrl(id));
  }

  editInsurance_Company(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(InsuranceCompanyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addInsurance_Company(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(InsuranceCompanyEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickInsurance_Companys(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(InsuranceCompanyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
