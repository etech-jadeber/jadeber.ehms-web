import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuredEmployeeDetailComponent } from './insured-employee-detail.component';

describe('InsuredEmployeeDetailComponent', () => {
  let component: InsuredEmployeeDetailComponent;
  let fixture: ComponentFixture<InsuredEmployeeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuredEmployeeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuredEmployeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
