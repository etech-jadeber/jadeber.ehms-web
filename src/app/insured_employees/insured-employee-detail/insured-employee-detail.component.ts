import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Insured_EmployeeDetail} from "../insured_employee.model";
import {Insured_EmployeePersist} from "../insured_employee.persist";
import {Insured_EmployeeNavigator} from "../insured_employee.navigator";

export enum Insured_EmployeeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-insured_employee-detail',
  templateUrl: './insured-employee-detail.component.html',
  styleUrls: ['./insured-employee-detail.component.css']
})
export class InsuredEmployeeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  insured_employeeLoading:boolean = false;
  
  Insured_EmployeeTabs: typeof Insured_EmployeeTabs = Insured_EmployeeTabs;
  activeTab: Insured_EmployeeTabs = Insured_EmployeeTabs.overview;
  //basics
  insured_employeeDetail: Insured_EmployeeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public insured_employeeNavigator: Insured_EmployeeNavigator,
              public  insured_employeePersist: Insured_EmployeePersist) {
    this.tcAuthorization.requireRead("insured_employees");
    this.insured_employeeDetail = new Insured_EmployeeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("insured_employees");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.insured_employeeLoading = true;
    this.insured_employeePersist.getInsured_Employee(id).subscribe(insured_employeeDetail => {
          this.insured_employeeDetail = insured_employeeDetail;
          this.insured_employeeLoading = false;
        }, error => {
          console.error(error);
          this.insured_employeeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.insured_employeeDetail.id);
  }

  editInsured_Employee(): void {
    let modalRef = this.insured_employeeNavigator.editInsured_Employee(this.insured_employeeDetail.id);
    modalRef.afterClosed().subscribe(modifiedInsured_EmployeeDetail => {
      TCUtilsAngular.assign(this.insured_employeeDetail, modifiedInsured_EmployeeDetail);
    }, error => {
      console.error(error);
    });
  }

   printInsured_Employee():void{
      this.insured_employeePersist.print(this.insured_employeeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print insured_employee", true);
      });
    }

  back():void{
      this.location.back();
    }

}
