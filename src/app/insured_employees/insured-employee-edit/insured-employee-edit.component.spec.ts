import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuredEmployeeEditComponent } from './insured-employee-edit.component';

describe('InsuredEmployeeEditComponent', () => {
  let component: InsuredEmployeeEditComponent;
  let fixture: ComponentFixture<InsuredEmployeeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuredEmployeeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuredEmployeeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
