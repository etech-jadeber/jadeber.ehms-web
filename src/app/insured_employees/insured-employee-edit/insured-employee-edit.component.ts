import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes} from "../../tc/models";


import {Insured_EmployeeDetail} from "../insured_employee.model";
import {Insured_EmployeePersist} from "../insured_employee.persist";
import { CompanyPersist } from 'src/app/Companys/Company.persist';
import { CompanyDetail, CompanySummary, CompanySummaryPartialList } from 'src/app/Companys/Company.model';
import { EmployeePersist } from 'src/app/storeemployees/employee.persist';
import { EmployeeSummary, EmployeeSummaryPartialList } from 'src/app/storeemployees/employee.model';
import { PatientDetail, PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { CompanyNavigator } from 'src/app/Companys/Company.navigator';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { CameraNavigator } from 'src/app/patients/camera.navigator';
import { HttpClient } from '@angular/common/http';
import { WebcamImage } from 'ngx-webcam';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { CompanyCreditServicePackageNavigator } from 'src/app/company_credit_service_package/companyCreditServicePackage.navigator';
import { CompanyCreditServicePackageDetail } from 'src/app/company_credit_service_package/companyCreditServicePackage.model';
import { CompanyCreditServicePackagePersist } from 'src/app/company_credit_service_package/companyCreditServicePackage.persist';
import { Job_States } from 'src/app/app.enums';
import { JobPersist } from 'src/app/tc/jobs/job.persist';


@Component({
  selector: 'app-insured_employee-edit',
  templateUrl: './insured-employee-edit.component.html',
  styleUrls: ['./insured-employee-edit.component.css']
})
export class InsuredEmployeeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  insured_employeeDetail: Insured_EmployeeDetail;
  companies: CompanySummary[] = [];
  patients: PatientSummary[] = [];
  companyName: string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<InsuredEmployeeEditComponent>,
              public  persist: Insured_EmployeePersist,
              public companyPersist: CompanyPersist,
              public employeePersist: EmployeePersist,
              public patientPersist: PatientPersist,
              public companyNavigator: CompanyNavigator,
              public patienNavigator: PatientNavigator,
              public filePersist: FilePersist,
              public cameraNav: CameraNavigator,
              public http: HttpClient,
              public tcUtilsArray: TCUtilsArray,
              public packageNavigator: CompanyCreditServicePackageNavigator,
              public packagePersist: CompanyCreditServicePackagePersist,
              public insured_employee_persist: Insured_EmployeePersist,
              public jobPersist: JobPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.WIZARD) {
      this.tcAuthorization.requireCreate("insured_employees");
       this.title = "Import Employee" ;
       this.insured_employeeDetail = new Insured_EmployeeDetail();
       this.insured_employeeDetail.company_id = this.idMode.id;
       //set necessary defaults
 
     }
    else if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("insured_employees");
      this.title = this.appTranslation.getText("general","new") +   " " + this.appTranslation.getText("inventory","insured_employees") ;
      this.insured_employeeDetail = new Insured_EmployeeDetail();
      this.insured_employeeDetail.company_id = this.idMode.id;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("insured_employees");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("inventory","insured_employees") ;
      this.isLoadingResults = true;
      this.persist.getInsured_Employee(this.idMode.id).subscribe(insured_employeeDetail => {
        this.insured_employeeDetail = insured_employeeDetail;
        this.companyPersist.getCompany(this.insured_employeeDetail.company_id).subscribe(company => {
          this.insured_employeeDetail.company_name = company.name;
        })
        this.tcUtilsString.isValidId(this.insured_employeeDetail.patient_id) && this.patientPersist.getPatient(this.insured_employeeDetail.patient_id).subscribe(patient => {
          this.insured_employeeDetail.patient_name = patient.fname + ' ' + patient.mname + ' ' + patient.lname
        })
        this.tcUtilsString.isValidId(this.insured_employeeDetail.package_id) && this.packagePersist.getCompanyCreditServicePackage(this.insured_employeeDetail.package_id).subscribe(creditPackage => {
          this.insured_employeeDetail.package_name = creditPackage.name
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addInsured_Employee(this.idMode.id ,this.insured_employeeDetail).subscribe(value => {
      this.tcNotification.success("Insured_Employee added");
      this.insured_employeeDetail.id = value.id;
      this.dialogRef.close(this.insured_employeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  excelUpload(fileInputEvent: any) {
    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }
    this.insured_employeeDetail.fileInputEvent = fileInputEvent.target.files[0]
  }


  importEmployee(): void {

    let fd: FormData = new FormData();
    fd.append('file', this.insured_employeeDetail.fileInputEvent);
    this.http.post(this.insured_employee_persist.insuredEmployeeImportUrl(this.insured_employeeDetail.package_id), fd).subscribe({
      next: (uplodJob: TCId) => {
        this.jobPersist.followJob(uplodJob.id, "uploading employees", false, () => {
          this.dialogRef.close(this.insured_employeeDetail);
        });        
      },

      error:(err)=>{
        console.log(err);
        
      }
    }
    

    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateInsured_Employee(this.insured_employeeDetail).subscribe(value => {
      this.tcNotification.success("Insured_Employee updated");
      this.dialogRef.close(this.insured_employeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  searchCompany(){
    let dialogRef = this.companyNavigator.pickCompanys(true)
    dialogRef.afterClosed().subscribe((result: CompanyDetail[]) => {
      this.insured_employeeDetail.company_id = result[0].id;
      this.companyName = result[0].name;
    })
  }

  searchPackage() {
    let dialogRef = this.packageNavigator.pickCompanyCreditServicePackages(this.insured_employeeDetail.company_id, true)
    dialogRef.afterClosed().subscribe((result: CompanyCreditServicePackageDetail[]) => {
      if(result){
        this.insured_employeeDetail.package_name = result[0].name
        this.insured_employeeDetail.package_id = result[0].id;
      }
    })
  }

  searchPatient(){
    let dialogRef = this.patienNavigator.pickPatients(true)
    dialogRef.afterClosed().subscribe((result: PatientDetail[]) => {
      if(result){
        this.insured_employeeDetail.full_name = result[0].fname + " " + result[0].mname + " " + result[0].lname
      this.insured_employeeDetail.patient_name = this.insured_employeeDetail.full_name
      this.insured_employeeDetail.patient_id = result[0].id;
      }
    })
  }

  canSubmit():boolean{
        if (this.insured_employeeDetail == null){
            return false;
          }
          if (this.isWizard()){
            if(!this.insured_employeeDetail.package_id){
              return false;
            }
            if(!this.insured_employeeDetail.fileInputEvent){
              return false;
            }
            return true;
          }

        if (this.insured_employeeDetail.full_name == null || this.insured_employeeDetail.full_name  == "") {
                      return false;
                    }
                    // if (this.insured_employeeDetail.covered_amount == null || this.insured_employeeDetail.covered_amount  == 0) {
                    //   return false;
                    // }
                    if (this.insured_employeeDetail.employee_id == null || this.insured_employeeDetail.employee_id  == "") {
                      return false;
                    }

        return true;
      }

      takePic(key: string, pic: string){
        this.cameraNav.takePic().afterClosed().subscribe(
          {
            next:(res:WebcamImage)=>{

             let fd: FormData = new FormData();
             const rawData = atob(res.imageAsBase64);
             const bytes = new Array(rawData.length);
             for (var x = 0; x < rawData.length; x++) {
               bytes[x] = rawData.charCodeAt(x);
              }
              const arr = new Uint8Array(bytes);
              const blob = new Blob([arr], {type: 'image/png'});


             fd.append('file', blob);

             this.http.post(this.persist.documentUploadUri(), fd).subscribe({
              next: (response) => {
                this.insured_employeeDetail[key] = (<TCId>response).id;
               this.filePersist.getDownloadKey(this.insured_employeeDetail[key]).subscribe(
                  (res:any)=>{
                    this.insured_employeeDetail[pic] = this.filePersist.downloadLink(res.id);
                  }
                );


              },

              error:(err)=>{
                console.log(err);

              }
            });

            },
            error:(err)=>{

            }
          }
        )
      }



      documentUpload(fileInputEvent: any, key: string, pic: string): void {

        if (fileInputEvent.target.files[0].size > 20000000) {
          this.tcNotification.error("File too big");
          return;
        }

        if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
          this.tcNotification.error("Unsupported file type");
          return;
        }

        let fd: FormData = new FormData();
        fd.append('file', fileInputEvent.target.files[0]);
        this.http.post(this.persist.documentUploadUri(), fd).subscribe({
          next: (response) => {
            this.insured_employeeDetail[key] = (<TCId>response).id;
           this.filePersist.getDownloadKey(this.insured_employeeDetail[key]).subscribe(
              (res:any)=>{

                this.insured_employeeDetail[pic] = this.filePersist.downloadLink(res.id);

              }
            );


          },

          error:(err)=>{
            console.log(err);

          }
        }


        );
      }

}
