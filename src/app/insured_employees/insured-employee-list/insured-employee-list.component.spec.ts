import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuredEmployeeListComponent } from './insured-employee-list.component';

describe('InsuredEmployeeListComponent', () => {
  let component: InsuredEmployeeListComponent;
  let fixture: ComponentFixture<InsuredEmployeeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuredEmployeeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuredEmployeeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
