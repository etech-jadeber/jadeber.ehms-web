import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Insured_EmployeePersist} from "../insured_employee.persist";
import {Insured_EmployeeNavigator} from "../insured_employee.navigator";
import {Insured_EmployeeDetail, Insured_EmployeeSummary, Insured_EmployeeSummaryPartialList} from "../insured_employee.model";
import { CompanySummary, CompanySummaryPartialList } from 'src/app/Companys/Company.model';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { EmployeeDetail } from 'src/app/storeemployees/employee.model';
import { InsuredEmployeeStatus } from 'src/app/app.enums';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { Job_States } from 'src/app/tc/app.enums';
import { TCId } from 'src/app/tc/models';
import { environment } from 'src/environments/environment';
import { CompanyCreditServicePackageNavigator } from 'src/app/company_credit_service_package/companyCreditServicePackage.navigator';
import { CompanyCreditServiceDetail } from 'src/app/company_credit_service/company_credit_service.model';
import { CompanyCreditServicePackageDetail } from 'src/app/company_credit_service_package/companyCreditServicePackage.model';


@Component({
  selector: 'app-insured_employee-list',
  templateUrl: './insured-employee-list.component.html',
  styleUrls: ['./insured-employee-list.component.css']
})
export class InsuredEmployeeListComponent implements OnInit {


  insured_employeesData: Insured_EmployeeSummary[] = [];
  insured_employeesTotalCount: number = 0;
  insured_employeesSelectAll:boolean = false;
  insured_employeesSelection: Insured_EmployeeSummary[] = [];
  packageName: string;

  insured_employeesDisplayedColumns: string[] = ["select","action", "full_name", "employee_id", "image_id", "document_id", "covered_amount", "start_date", "end_date", "status" ];
  insured_employeesSearchTextBox: FormControl = new FormControl();
  insured_employeesIsLoading: boolean = false;
  insured_employeeStatus = InsuredEmployeeStatus;

  @ViewChild(MatPaginator, {static: true}) insured_employeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) insured_employeesSort: MatSort;

  @Input() companyId: string;
  @Output() onResult = new EventEmitter<number>();

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public insured_employeePersist: Insured_EmployeePersist,
                public insured_employeeNavigator: Insured_EmployeeNavigator,
                public companyCreditPackageNavigator: CompanyCreditServicePackageNavigator,
                public jobPersist: JobPersist,
                public companyPersist: CompanyPersist,
                public patientPersist: PatientPersist,
                public patientNavigator: PatientNavigator,
                public tcUtilsString: TCUtilsString,
                public filePersist: FilePersist,
                private http:HttpClient,
    ) {

        this.tcAuthorization.requireRead("insured_employees");
       this.insured_employeesSearchTextBox.setValue(insured_employeePersist.insured_employeeSearchText);
      //delay subsequent keyup events
      this.insured_employeesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.insured_employeePersist.insured_employeeSearchText = value;
        this.searchInsured_Employees();
      });
    }

    ngOnInit() {
      this.insured_employeesSort.sortChange.subscribe(() => {
        this.insured_employeesPaginator.pageIndex = 0;
        this.searchInsured_Employees(true);
      });

      this.insured_employeesPaginator.page.subscribe(() => {
        this.searchInsured_Employees(true);
      });
      //start by loading items
      this.searchInsured_Employees();
    }

    ngOnDestroy() {
      this.insured_employeePersist.packageId = null;
    }

  searchInsured_Employees(isPagination:boolean = false): void {


    let paginator = this.insured_employeesPaginator;
    let sorter = this.insured_employeesSort;

    this.insured_employeesIsLoading = true;
    this.insured_employeesSelection = [];

    this.insured_employeePersist.searchInsured_Employee(this.companyId, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Insured_EmployeeSummaryPartialList) => {
      this.insured_employeesData = partialList.data;
      this.insured_employeesData.forEach(employee => {
        this.companyPersist.getCompany(employee.company_id).subscribe((company) => {
          employee.company_name = company.name;
        })
      })
      if (partialList.total != -1) {
        this.insured_employeesTotalCount = partialList.total;
      }
      this.insured_employeesIsLoading = false;
    }, error => {
      this.insured_employeesIsLoading = false;
    });

  }

  searchPackages(){
    let dialogRef = this.companyCreditPackageNavigator.pickCompanyCreditServicePackages(this.companyId, true)
    dialogRef.afterClosed().subscribe(
      (data: CompanyCreditServicePackageDetail[]) => {
        if(data){
          this.packageName = data[0].name;
          this.insured_employeePersist.packageId = data[0].id;
        }
      }
    )
  }

  downloadInsured_Employees(): void {
    if(this.insured_employeesSelectAll){
         this.insured_employeePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download insured_employees", true);
      });
    }
    else{
        this.insured_employeePersist.download(this.tcUtilsArray.idsList(this.insured_employeesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download insured_employees",true);
            });
        }
  }

  addInsured_Employee(): void {
    let dialogRef = this.insured_employeeNavigator.addInsured_Employee(this.companyId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchInsured_Employees();
      }
    });
  }

  editInsured_Employee(item: Insured_EmployeeSummary) {
    let dialogRef = this.insured_employeeNavigator.editInsured_Employee(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  importEmployee() {
    let dialogRef = this.insured_employeeNavigator.importInsured_Employee(this.companyId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchInsured_Employees();
      }

    });
  }

  documentUpload(fileInputEvent: any): void {
    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.insured_employeePersist.insuredEmployeeImportUrl(this.companyId), fd).subscribe({
      next: (uplodJob: TCId) => {
        this.jobPersist.followJob(uplodJob.id, "uploading employees", false);
        this.jobPersist.getJob(uplodJob.id).subscribe(
        job => {
          if (job.job_state == Job_States.success) {
            this.searchInsured_Employees();
          }
        }
      )
        
      },

      error:(err)=>{
        console.log(err);
        
      }
    }
    

    );
  }

  deleteInsured_Employee(item: Insured_EmployeeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Insured_Employee");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.insured_employeePersist.deleteInsured_Employee(item.id).subscribe(response => {
          this.tcNotification.success("Insured_Employee deleted");
          this.searchInsured_Employees();
        }, error => {
        });
      }

    });
  }

  connectPatient(employee: EmployeeDetail) {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.insured_employeesIsLoading = true
        this.insured_employeePersist.insured_employeeDo(employee.id, "connect_patient", {patient_id:result[0].id})
        .subscribe(response => {
          if (response) {
            this.insured_employeesIsLoading = false;
            this.tcNotification.success("Insured Employee connected.");
            this.searchInsured_Employees();
          }
        }, error => {
          this.insured_employeesIsLoading = false;
          console.log(error)
        }
        )
      }
    });
  }

  terminateEmployee(employee: EmployeeDetail){
    this.insured_employeesIsLoading = true
    this.insured_employeePersist.insured_employeeDo(employee.id, "terminate_patient", {}).subscribe(
      response => {
        if(response) {
          this.insured_employeesIsLoading = false;
          this.tcNotification.success("The employee is terminated.");
          this.searchInsured_Employees();
        }
      }, error => {
        this.insured_employeesIsLoading = false;
        console.log(error)
      }
    )
  }

  back():void{
      this.location.back();
    }

    viewImage(fileId: string) {
      this.filePersist.getDownloadKey(fileId).subscribe(value => {
        this.tcNavigator.openUrl(this.filePersist.downloadLink(value['id']), true)
      })
    }
}
