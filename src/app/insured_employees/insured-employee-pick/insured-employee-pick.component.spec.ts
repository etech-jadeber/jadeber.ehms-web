import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InsuredEmployeePickComponent } from './insured-employee-pick.component';

describe('InsuredEmployeePickComponent', () => {
  let component: InsuredEmployeePickComponent;
  let fixture: ComponentFixture<InsuredEmployeePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuredEmployeePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuredEmployeePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
