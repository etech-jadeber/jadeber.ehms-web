import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Insured_EmployeeDetail, Insured_EmployeeSummary, Insured_EmployeeSummaryPartialList} from "../insured_employee.model";
import {Insured_EmployeePersist} from "../insured_employee.persist";
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-insured_employee-pick',
  templateUrl: './insured-employee-pick.component.html',
  styleUrls: ['./insured-employee-pick.component.css']
})
export class InsuredEmployeePickComponent implements OnInit {

  insured_employeesData: Insured_EmployeeSummary[] = [];
  insured_employeesTotalCount: number = 0;
  insured_employeesSelection: Insured_EmployeeSummary[] = [];
  insured_employeesDisplayedColumns: string[] = ["select", 'full_name','company_id','employee_id','covered_amount','patient_id' ];

  insured_employeesSearchTextBox: FormControl = new FormControl();
  insured_employeesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) insured_employeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) insured_employeesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public insured_employeePersist: Insured_EmployeePersist,
              public dialogRef: MatDialogRef<InsuredEmployeePickComponent>,
              public tcUtilsSring: TCUtilsString,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, companyId: string}
  ) {
    this.tcAuthorization.requireRead("insured_employees");
    this.insured_employeePersist.company_id = this.data.companyId;
    this.insured_employeesSearchTextBox.setValue(insured_employeePersist.insured_employeeSearchText);
    //delay subsequent keyup events
    this.insured_employeesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.insured_employeePersist.insured_employeeSearchText = value;
      this.searchInsured_Employees();
    });
  }

  ngOnInit() {

    this.insured_employeesSort.sortChange.subscribe(() => {
      this.insured_employeesPaginator.pageIndex = 0;
      this.searchInsured_Employees();
    });

    this.insured_employeesPaginator.page.subscribe(() => {
      this.searchInsured_Employees();
    });

    //set initial picker list to 5
    this.insured_employeesPaginator.pageSize = 5;

    //start by loading items
    this.searchInsured_Employees();
  }

  ngOnDestroy() {
    this.insured_employeePersist.company_id = null;
  }

  searchInsured_Employees(): void {
    this.insured_employeesIsLoading = true;
    this.insured_employeesSelection = [];

    this.insured_employeePersist.searchInsured_Employee(this.data.companyId,this.insured_employeesPaginator.pageSize,
        this.insured_employeesPaginator.pageIndex,
        this.insured_employeesSort.active,
        this.insured_employeesSort.direction).subscribe((partialList: Insured_EmployeeSummaryPartialList) => {
      this.insured_employeesData = partialList.data;
      if (partialList.total != -1) {
        this.insured_employeesTotalCount = partialList.total;
      }
      this.insured_employeesIsLoading = false;
    }, error => {
      this.insured_employeesIsLoading = false;
    });

  }

  markOneItem(item: Insured_EmployeeSummary) {
    if(!this.tcUtilsArray.containsId(this.insured_employeesSelection,item.id)){
          this.insured_employeesSelection = [];
          this.insured_employeesSelection.push(item);
        }
        else{
          this.insured_employeesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.insured_employeesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
