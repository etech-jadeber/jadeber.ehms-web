import {TCId} from "../tc/models";

export class Insured_EmployeeSummary extends TCId {
  full_name : string;
company_id : string;
employee_id : string;
covered_amount : number;
patient_id : string;
patient_name: string;
company_name: string;
start_date: number;
end_date: number;
image_id: string;
image_url: string;
document_id: string;
document_url: string;
package_id: string;
package_name: string;
fileInputEvent: any;
}

export class Insured_EmployeeSummaryPartialList {
  data: Insured_EmployeeSummary[];
  total: number;
}

export class Insured_EmployeeDetail extends Insured_EmployeeSummary {
  full_name : string;
company_id : string;
employee_id : string;
covered_amount : number;
patient_id : string;
profile_url: any;
}

export class Insured_EmployeeDashboard {
  total: number;
}
