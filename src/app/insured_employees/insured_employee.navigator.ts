import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {InsuredEmployeeEditComponent} from "./insured-employee-edit/insured-employee-edit.component";
import {InsuredEmployeePickComponent} from "./insured-employee-pick/insured-employee-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Insured_EmployeeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  insured_employeesUrl(): string {
    return "/insured_employees";
  }

  insured_employeeUrl(id: string): string {
    return "/insured_employees/" + id;
  }

  viewInsured_Employees(): void {
    this.router.navigateByUrl(this.insured_employeesUrl());
  }

  viewInsured_Employee(id: string): void {
    this.router.navigateByUrl(this.insured_employeeUrl(id));
  }

  editInsured_Employee(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(InsuredEmployeeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  importInsured_Employee(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(InsuredEmployeeEditComponent, {
      data: new TCIdMode(id, TCModalModes.WIZARD),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addInsured_Employee(parent_id : string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(InsuredEmployeeEditComponent, {
          data: new TCIdMode(parent_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickInsured_Employees(companyId: string = null, selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(InsuredEmployeePickComponent, {
        data: {selectOne, companyId},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
