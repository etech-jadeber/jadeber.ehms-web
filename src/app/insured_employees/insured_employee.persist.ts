import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Insured_EmployeeDashboard, Insured_EmployeeDetail, Insured_EmployeeSummaryPartialList} from "./insured_employee.model";
import { InsuredEmployeeStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Insured_EmployeePersist {

  insured_employeeSearchText: string = "";
  packageId: string;
  company_id: string;

  insuredEmployeeStatusFilter: number = -1;

  InsuredEmployeeStatus: TCEnum[] = [
  new TCEnum( InsuredEmployeeStatus.covered, 'Covered'),
  new TCEnum( InsuredEmployeeStatus.terminated, 'Terminated'),

  ];

  constructor(private http: HttpClient) {
  }

  documentUploadUri() {
    return environment.tcApiBaseUri + 'patients/upload-image/';
  }

  searchInsured_Employee(companyId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Insured_EmployeeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("company/" + companyId + "/insured_employees", this.insured_employeeSearchText, pageSize, pageIndex, sort, order);
    if (this.insuredEmployeeStatusFilter != -1){
      url = TCUtilsString.appendUrlParameter(url, "status", this.insuredEmployeeStatusFilter.toString())
    }
    if(this.packageId){
      url = TCUtilsString.appendUrlParameter(url, "package_id", this.packageId)
    }
    return this.http.get<Insured_EmployeeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.insured_employeeSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "insured_employees/do", new TCDoParam("download_all", this.filters()));
  }

  insured_employeeDashboard(): Observable<Insured_EmployeeDashboard> {
    return this.http.get<Insured_EmployeeDashboard>(environment.tcApiBaseUri + "insured_employees/dashboard");
  }

  getInsured_Employee(id: string): Observable<Insured_EmployeeDetail> {
    return this.http.get<Insured_EmployeeDetail>(environment.tcApiBaseUri + "insured_employees/" + id);
  }

  addInsured_Employee(parent_id: string, item: Insured_EmployeeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "company/" + parent_id + "/insured_employees", item);
  }

  updateInsured_Employee(item: Insured_EmployeeDetail): Observable<Insured_EmployeeDetail> {
    return this.http.patch<Insured_EmployeeDetail>(environment.tcApiBaseUri + "insured_employees/" + item.id, item);
  }

  deleteInsured_Employee(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "insured_employees/" + id);
  }

  insured_employeesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "insured_employees/do", new TCDoParam(method, payload));
  }

  insured_employeeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "insured_employees/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "insured_employees/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "insured_employees/" + id + "/do", new TCDoParam("print", {}));
  }

  insuredEmployeeImportUrl (company_id: string) {
    return environment.tcApiBaseUri + "insured_employees/import_employees/" + company_id
  }


}
