import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterchangeableItemDetailComponent } from './interchangeable-item-detail.component';

describe('InterchangeableItemDetailComponent', () => {
  let component: InterchangeableItemDetailComponent;
  let fixture: ComponentFixture<InterchangeableItemDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterchangeableItemDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterchangeableItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
