import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {InterchangeableItemDetail} from "../interchangeable_item.model";
import {InterchangeableItemPersist} from "../interchangeable_item.persist";
import {InterchangeableItemNavigator} from "../interchangeable_item.navigator";

export enum InterchangeableItemTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-interchangeable_item-detail',
  templateUrl: './interchangeable-item-detail.component.html',
  styleUrls: ['./interchangeable-item-detail.component.scss']
})
export class InterchangeableItemDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  interchangeableItemIsLoading:boolean = false;
  
  InterchangeableItemTabs: typeof InterchangeableItemTabs = InterchangeableItemTabs;
  activeTab: InterchangeableItemTabs = InterchangeableItemTabs.overview;
  //basics
  interchangeableItemDetail: InterchangeableItemDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public interchangeableItemNavigator: InterchangeableItemNavigator,
              public  interchangeableItemPersist: InterchangeableItemPersist) {
    this.tcAuthorization.requireRead("interchangeable_items");
    this.interchangeableItemDetail = new InterchangeableItemDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("interchangeable_items");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.interchangeableItemIsLoading = true;
    this.interchangeableItemPersist.getInterchangeableItem(id).subscribe(interchangeableItemDetail => {
          this.interchangeableItemDetail = interchangeableItemDetail;
          this.interchangeableItemIsLoading = false;
        }, error => {
          console.error(error);
          this.interchangeableItemIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.interchangeableItemDetail.id);
  }
  editInterchangeableItem(): void {
    let modalRef = this.interchangeableItemNavigator.editInterchangeableItem(this.interchangeableItemDetail.id);
    modalRef.afterClosed().subscribe(interchangeableItemDetail => {
      TCUtilsAngular.assign(this.interchangeableItemDetail, interchangeableItemDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.interchangeableItemPersist.print(this.interchangeableItemDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print interchangeable_item", true);
      });
    }

  back():void{
      this.location.back();
    }

}
