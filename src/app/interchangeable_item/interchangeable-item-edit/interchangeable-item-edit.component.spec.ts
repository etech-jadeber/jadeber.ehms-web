import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterchangeableItemEditComponent } from './interchangeable-item-edit.component';

describe('InterchangeableItemEditComponent', () => {
  let component: InterchangeableItemEditComponent;
  let fixture: ComponentFixture<InterchangeableItemEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterchangeableItemEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterchangeableItemEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
