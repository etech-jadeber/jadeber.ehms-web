import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { InterchangeableItemDetail } from '../interchangeable_item.model';import { InterchangeableItemPersist } from '../interchangeable_item.persist';import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemDetail } from 'src/app/items/item.model';
@Component({
  selector: 'app-interchangeable_item-edit',
  templateUrl: './interchangeable-item-edit.component.html',
  styleUrls: ['./interchangeable-item-edit.component.scss']
})export class InterchangeableItemEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  interchangeableItemDetail: InterchangeableItemDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<InterchangeableItemEditComponent>,
              public  persist: InterchangeableItemPersist,
              public itemNavigator: ItemNavigator,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("interchangeable_items");
      this.title = this.appTranslation.getText("general","new") +  " " + "interchangeable_item";
      this.interchangeableItemDetail = new InterchangeableItemDetail();
      this.interchangeableItemDetail.main_drug_id = this.idMode.id;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("interchangeable_items");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "interchangeable_item";
      this.isLoadingResults = true;
      this.persist.getInterchangeableItem(this.idMode.id).subscribe(interchangeableItemDetail => {
        this.interchangeableItemDetail = interchangeableItemDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addInterchangeableItem(this.interchangeableItemDetail).subscribe(value => {
      this.tcNotification.success("interchangeableItem added");
      this.interchangeableItemDetail.id = value.id;
      this.dialogRef.close(this.interchangeableItemDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateInterchangeableItem(this.interchangeableItemDetail).subscribe(value => {
      this.tcNotification.success("interchangeable_item updated");
      this.dialogRef.close(this.interchangeableItemDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  } 
  
  canSubmit():boolean{
        if (this.interchangeableItemDetail == null){
            return false;
          }
        if (this.interchangeableItemDetail.main_drug_id == null || this.interchangeableItemDetail.main_drug_id  == "") {
            return false;
        } 
        if (this.interchangeableItemDetail.interchangeable_id == null || this.interchangeableItemDetail.interchangeable_id  == "") {
            return false;
        }
        return true;
 }

 searchItem(){
  let dialogRef = this.itemNavigator.pickItems(true)
    dialogRef.afterClosed().subscribe((result: ItemDetail[]) => {
      if(result){
        this.interchangeableItemDetail.interchangeable_id = result[0].id
      this.interchangeableItemDetail.interchangeable_name = result[0].name
      }
    })
 }
 }
