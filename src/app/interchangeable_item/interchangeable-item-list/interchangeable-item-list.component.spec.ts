import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterchangeableItemListComponent } from './interchangeable-item-list.component';

describe('InterchangeableItemListComponent', () => {
  let component: InterchangeableItemListComponent;
  let fixture: ComponentFixture<InterchangeableItemListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterchangeableItemListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterchangeableItemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
