import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { InterchangeableItemSummary, InterchangeableItemSummaryPartialList } from '../interchangeable_item.model';
import { InterchangeableItemPersist } from '../interchangeable_item.persist';
import { InterchangeableItemNavigator } from '../interchangeable_item.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemPersist } from 'src/app/items/item.persist';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
@Component({
  selector: 'app-interchangeable_item-list',
  templateUrl: './interchangeable-item-list.component.html',
  styleUrls: ['./interchangeable-item-list.component.scss']
})
export class InterchangeableItemListComponent implements OnInit {
  interchangeableItemsData: InterchangeableItemSummary[] = [];
  interchangeableItemsTotalCount: number = 0;
  interchangeableItemSelectAll:boolean = false;
  interchangeableItemSelection: InterchangeableItemSummary[] = [];

 interchangeableItemsDisplayedColumns: string[] = ["select","action","interchangable_id","status" ];
  interchangeableItemSearchTextBox: FormControl = new FormControl();
  interchangeableItemIsLoading: boolean = false;  
  
  @ViewChild(MatPaginator, {static: true}) interchangeableItemsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) interchangeableItemsSort: MatSort;
  @Input() mainItemId: string;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public interchangeableItemPersist: InterchangeableItemPersist,
                public interchangeableItemNavigator: InterchangeableItemNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public companyPersist: CompanyPersist,

    ) {

        this.tcAuthorization.requireRead("interchangeable_items");
       this.interchangeableItemSearchTextBox.setValue(interchangeableItemPersist.interchangeableItemSearchText);
      //delay subsequent keyup events
      this.interchangeableItemSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.interchangeableItemPersist.interchangeableItemSearchText = value;
        this.searchInterchangeableItems();
      });
    } ngOnInit() {
   
      this.interchangeableItemsSort.sortChange.subscribe(() => {
        this.interchangeableItemsPaginator.pageIndex = 0;
        this.searchInterchangeableItems(true);
      });

      this.interchangeableItemsPaginator.page.subscribe(() => {
        this.searchInterchangeableItems(true);
      });
      //start by loading items
      this.searchInterchangeableItems();
    }

  searchInterchangeableItems(isPagination:boolean = false): void {


    let paginator = this.interchangeableItemsPaginator;
    let sorter = this.interchangeableItemsSort;

    this.interchangeableItemIsLoading = true;
    this.interchangeableItemSelection = [];

    this.interchangeableItemPersist.searchInterchangeableItem(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.mainItemId).subscribe((partialList: InterchangeableItemSummaryPartialList) => {
      this.interchangeableItemsData = partialList.data;
      this.interchangeableItemsData.forEach( (interchangeable) => this.itemPersist.getItem(interchangeable.interchangeable_id).subscribe(
        item => {
          interchangeable.interchangeable_name = item.name;
        }
      ))
      if (partialList.total != -1) {
        this.interchangeableItemsTotalCount = partialList.total;
      }
      this.interchangeableItemIsLoading = false;
    }, error => {
      this.interchangeableItemIsLoading = false;
    });

  } downloadInterchangeableItems(): void {
    if(this.interchangeableItemSelectAll){
         this.interchangeableItemPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download interchangeable_item", true);
      });
    }
    else{
        this.interchangeableItemPersist.download(this.tcUtilsArray.idsList(this.interchangeableItemSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download interchangeable_item",true);
            });
        }
  }
addInterchangeableItem(): void {
    let dialogRef = this.interchangeableItemNavigator.addInterchangeableItem(this.mainItemId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchInterchangeableItems();
      }
    });
  }

  editInterchangeableItem(item: InterchangeableItemSummary) {
    let dialogRef = this.interchangeableItemNavigator.editInterchangeableItem(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteInterchangeableItem(item: InterchangeableItemSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("interchangeable_item");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.interchangeableItemPersist.deleteInterchangeableItem(item.id).subscribe(response => {
          this.tcNotification.success("interchangeable_item deleted");
          this.searchInterchangeableItems();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
