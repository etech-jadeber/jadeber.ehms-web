import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterchangeableItemPickComponent } from './interchangeable-item-pick.component';

describe('InterchangeableItemPickComponent', () => {
  let component: InterchangeableItemPickComponent;
  let fixture: ComponentFixture<InterchangeableItemPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterchangeableItemPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterchangeableItemPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
