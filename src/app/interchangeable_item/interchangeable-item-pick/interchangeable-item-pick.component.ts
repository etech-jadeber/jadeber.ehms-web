import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { InterchangeableItemSummary, InterchangeableItemSummaryPartialList } from '../interchangeable_item.model';
import { InterchangeableItemPersist } from '../interchangeable_item.persist';
import { InterchangeableItemNavigator } from '../interchangeable_item.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemPersist } from 'src/app/items/item.persist';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
@Component({
  selector: 'app-interchangeable_item-pick',
  templateUrl: './interchangeable-item-pick.component.html',
  styleUrls: ['./interchangeable-item-pick.component.scss']
})export class InterchangeableItemPickComponent implements OnInit {
  interchangeableItemsData: InterchangeableItemSummary[] = [];
  interchangeableItemsTotalCount: number = 0;
  interchangeableItemSelectAll:boolean = false;
  interchangeableItemSelection: InterchangeableItemSummary[] = [];

 interchangeableItemsDisplayedColumns: string[] = ["select","interchangable_id","status" ];
  interchangeableItemSearchTextBox: FormControl = new FormControl();
  interchangeableItemIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) interchangeableItemsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) interchangeableItemsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public interchangeableItemPersist: InterchangeableItemPersist,
                public interchangeableItemNavigator: InterchangeableItemNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemPersist: ItemPersist,
                public companyPersist: CompanyPersist,
 public dialogRef: MatDialogRef<InterchangeableItemPickComponent>,@Inject(MAT_DIALOG_DATA) public data : {selectOne: boolean, parentId: string}

    ) {

        this.tcAuthorization.requireRead("interchangeable_items");
       this.interchangeableItemSearchTextBox.setValue(interchangeableItemPersist.interchangeableItemSearchText);
      //delay subsequent keyup events
      this.interchangeableItemSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.interchangeableItemPersist.interchangeableItemSearchText = value;
        this.searchInterchangeable_items();
      });
    } ngOnInit() {
   
      this.interchangeableItemsSort.sortChange.subscribe(() => {
        this.interchangeableItemsPaginator.pageIndex = 0;
        this.searchInterchangeable_items(true);
      });

      this.interchangeableItemsPaginator.page.subscribe(() => {
        this.searchInterchangeable_items(true);
      });
      //start by loading items
      this.searchInterchangeable_items();
    }

  searchInterchangeable_items(isPagination:boolean = false): void {


    let paginator = this.interchangeableItemsPaginator;
    let sorter = this.interchangeableItemsSort;

    this.interchangeableItemIsLoading = true;
    this.interchangeableItemSelection = [];

    this.interchangeableItemPersist.searchInterchangeableItem(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.data.parentId).subscribe((partialList: InterchangeableItemSummaryPartialList) => {
      this.interchangeableItemsData = partialList.data;
      this.interchangeableItemsData.forEach( (interchangeable) => this.itemPersist.getItem(interchangeable.interchangeable_id).subscribe(
        item => {
          interchangeable.interchangeable_name = item.name;
        }
      ))
      if (partialList.total != -1) {
        this.interchangeableItemsTotalCount = partialList.total;
      }
      this.interchangeableItemIsLoading = false;
    }, error => {
      this.interchangeableItemIsLoading = false;
    });

  }
  markOneItem(item: InterchangeableItemSummary) {
    if(!this.tcUtilsArray.containsId(this.interchangeableItemSelection,item.id)){
          this.interchangeableItemSelection = [];
          this.interchangeableItemSelection.push(item);
        }
        else{
          this.interchangeableItemSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.interchangeableItemSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
