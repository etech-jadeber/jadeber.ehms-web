import {TCId} from "../tc/models";

export class InterchangeableItemSummary extends TCId {
    main_drug_id:string;
    interchangeable_id:string;
    status:number;
    main_drug_name: string;
    interchangeable_name: string;
     
    }
    export class InterchangeableItemSummaryPartialList {
      data: InterchangeableItemSummary[];
      total: number;
    }
    export class InterchangeableItemDetail extends InterchangeableItemSummary {
    }
    
    export class InterchangeableItemDashboard {
      total: number;
    }
    