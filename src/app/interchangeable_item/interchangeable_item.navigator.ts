import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {InterchangeableItemEditComponent} from "./interchangeable-item-edit/interchangeable-item-edit.component";
import {InterchangeableItemPickComponent} from "./interchangeable-item-pick/interchangeable-item-pick.component";


@Injectable({
  providedIn: 'root'
})

export class InterchangeableItemNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  interchangeableItemsUrl(): string {
    return "/interchangeable_items";
  }

  interchangeableItemUrl(id: string): string {
    return "/interchangeable_items/" + id;
  }

  viewInterchangeableItems(): void {
    this.router.navigateByUrl(this.interchangeableItemsUrl());
  }

  viewInterchangeableItem(id: string): void {
    this.router.navigateByUrl(this.interchangeableItemUrl(id));
  }

  editInterchangeableItem(id: string): MatDialogRef<InterchangeableItemEditComponent> {
    const dialogRef = this.dialog.open(InterchangeableItemEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addInterchangeableItem(parentId: string): MatDialogRef<InterchangeableItemEditComponent> {
    const dialogRef = this.dialog.open(InterchangeableItemEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickInterchangeableItems(parentId: string, selectOne: boolean=false): MatDialogRef<InterchangeableItemPickComponent> {
      const dialogRef = this.dialog.open(InterchangeableItemPickComponent, {
        data: {parentId, selectOne},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
