import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {InterchangeableItemDashboard, InterchangeableItemDetail, InterchangeableItemSummaryPartialList} from "./interchangeable_item.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class InterchangeableItemPersist {
 interchangeableItemSearchText: string = "";
 
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.interchangeableItemSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchInterchangeableItem(pageSize: number, pageIndex: number, sort: string, order: string, parentId: string = null): Observable<InterchangeableItemSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("interchangeable_item", this.interchangeableItemSearchText, pageSize, pageIndex, sort, order);
    if(parentId){
        url = TCUtilsString.appendUrlParameter(url, "main_item_id", parentId)
    }
    return this.http.get<InterchangeableItemSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "interchangeable_item/do", new TCDoParam("download_all", this.filters()));
  }

  interchangeableItemDashboard(): Observable<InterchangeableItemDashboard> {
    return this.http.get<InterchangeableItemDashboard>(environment.tcApiBaseUri + "interchangeable_item/dashboard");
  }

  getInterchangeableItem(id: string): Observable<InterchangeableItemDetail> {
    return this.http.get<InterchangeableItemDetail>(environment.tcApiBaseUri + "interchangeable_item/" + id);
  } addInterchangeableItem(item: InterchangeableItemDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "interchangeable_item/", item);
  }

  updateInterchangeableItem(item: InterchangeableItemDetail): Observable<InterchangeableItemDetail> {
    return this.http.patch<InterchangeableItemDetail>(environment.tcApiBaseUri + "interchangeable_item/" + item.id, item);
  }

  deleteInterchangeableItem(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "interchangeable_item/" + id);
  }
 interchangeableItemsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "interchangeable_item/do", new TCDoParam(method, payload));
  }

  interchangeableItemDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "interchangeable_items/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "interchangeable_item/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "interchangeable_item/" + id + "/do", new TCDoParam("print", {}));
  }


}
