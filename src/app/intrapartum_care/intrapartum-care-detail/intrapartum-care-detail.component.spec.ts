import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrapartumCareDetailComponent } from './intrapartum-care-detail.component';

describe('IntrapartumCareDetailComponent', () => {
  let component: IntrapartumCareDetailComponent;
  let fixture: ComponentFixture<IntrapartumCareDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntrapartumCareDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrapartumCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
