import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {IntrapartumCareDetail} from "../intrapartum_care.model";
import {IntrapartumCarePersist} from "../intrapartum_care.persist";
import {IntrapartumCareNavigator} from "../intrapartum_care.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';

export enum IntrapartumCareTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-intrapartum_care-detail',
  templateUrl: './intrapartum-care-detail.component.html',
  styleUrls: ['./intrapartum-care-detail.component.css']
})
export class IntrapartumCareDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  intrapartumCareIsLoading:boolean = false;
  rupture_of_membranes_time:string;
  
  IntrapartumCareTabs: typeof IntrapartumCareTabs = IntrapartumCareTabs;
  activeTab: IntrapartumCareTabs = IntrapartumCareTabs.overview;
  //basics
  intrapartumCareDetail: IntrapartumCareDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public tcUtilsDate:TCUtilsDate,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public intrapartumCareNavigator: IntrapartumCareNavigator,
              public  intrapartumCarePersist: IntrapartumCarePersist) {
    this.tcAuthorization.requireRead("intrapartum_cares");
    this.intrapartumCareDetail = new IntrapartumCareDetail();
  } 
  ngOnInit() {

    // console.log(this.intrapartumCareDetail.rupture_of_membranes_time);
    
    
    this.tcAuthorization.requireRead("intrapartum_cares");
    // console.log(this.intrapartumCareDetail);
    // console.log(this.intrapartumCareDetail.rupture_of_membranes_time);
    // console.log(this.rupture_of_membranes_time);
  }
transform():void{
  this.rupture_of_membranes_time=`${Math.floor(this.intrapartumCareDetail.rupture_of_membranes_time/100)}:${Math.floor(this.intrapartumCareDetail.rupture_of_membranes_time/10)%10}${this.intrapartumCareDetail.rupture_of_membranes_time%10}`;
}
  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);

    });
   }

  loadDetails(id: string): void {
  this.intrapartumCareIsLoading = true;
    this.intrapartumCarePersist.getIntrapartumCare(id).subscribe(intrapartumCareDetail => {
          this.intrapartumCareDetail = intrapartumCareDetail;
          this.intrapartumCareIsLoading = false;
          this.transform();
        }, error => {
          console.error(error);
          this.intrapartumCareIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.intrapartumCareDetail.id);
  }
  editIntrapartumCare(): void {
    let modalRef = this.intrapartumCareNavigator.editIntrapartumCare(this.intrapartumCareDetail.id);
    modalRef.afterClosed().subscribe(intrapartumCareDetail => {
      TCUtilsAngular.assign(this.intrapartumCareDetail, intrapartumCareDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.intrapartumCarePersist.print(this.intrapartumCareDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print intrapartum_care", true);
      });
    }

  back():void{
      this.location.back();
    }

}