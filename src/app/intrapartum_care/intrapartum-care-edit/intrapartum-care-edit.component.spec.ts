import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrapartumCareEditComponent } from './intrapartum-care-edit.component';

describe('IntrapartumCareEditComponent', () => {
  let component: IntrapartumCareEditComponent;
  let fixture: ComponentFixture<IntrapartumCareEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntrapartumCareEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrapartumCareEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
