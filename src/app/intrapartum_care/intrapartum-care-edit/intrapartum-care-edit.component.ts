import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { IntrapartumCareDetail } from '../intrapartum_care.model';import { IntrapartumCarePersist } from '../intrapartum_care.persist';@Component({
selector: 'app-intrapartum_care-edit',
templateUrl: './intrapartum-care-edit.component.html',
styleUrls: ['./intrapartum-care-edit.component.css']
})export class IntrapartumCareEditComponent implements OnInit {

isLoadingResults: boolean = false;
title: string;
date_and_time_of_admission:string;
rupture_of_membranes_time:string;
intrapartumCareDetail: IntrapartumCareDetail;
constructor(public tcAuthorization:TCAuthorization,
public tcNotification: TCNotification,
public tcUtilsString:TCUtilsString,
public appTranslation:AppTranslation,
public dialogRef: MatDialogRef<IntrapartumCareEditComponent>,
public tcUtilsDate: TCUtilsDate,


public persist: IntrapartumCarePersist,

@Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

}

onCancel(): void {
this.dialogRef.close();
}

isNew(): boolean {
return this.idMode.mode === TCModalModes.NEW;
}

isEdit(): boolean {
return this.idMode.mode === TCModalModes.EDIT;
}

isCase(): boolean {
return this.idMode.mode === TCModalModes.CASE;
}
ngOnInit() {
if (this.idMode.mode == TCModalModes.NEW) {
this.tcAuthorization.requireCreate("intrapartum_cares");
this.title = this.appTranslation.getText("general","new") + " " + "Intrapartum Care";
this.intrapartumCareDetail = new IntrapartumCareDetail();
this.intrapartumCareDetail.rupture_of_membranes=""
const [date, time] = this.dateAndTime(new Date());
this.date_and_time_of_admission = `${date}T${time}`;
//set necessary defaults

} else {
this.tcAuthorization.requireUpdate("intrapartum_cares");
this.title = this.appTranslation.getText("general","edit") + " " + " " + "Intrapartum Care";
this.isLoadingResults = true;
this.persist.getIntrapartumCare(this.idMode.id).subscribe(intrapartumCareDetail => {
this.intrapartumCareDetail = intrapartumCareDetail;
this.transformDates(false);
this.isLoadingResults = false;
}, error => {
console.log(error);
this.isLoadingResults = false;
})

}
} 
onAdd(): void {

this.isLoadingResults = true;
this.transformDates();

this.persist.addIntrapartumCare(this.intrapartumCareDetail,this.idMode.id).subscribe(value => {
this.tcNotification.success("Intrapartum Care added");
this.intrapartumCareDetail.id = value.id;
this.dialogRef.close(this.intrapartumCareDetail);
}, error => {
this.isLoadingResults = false;
})
}

onUpdate(): void {
this.isLoadingResults = true;
this.transformDates(); 
this.persist.updateIntrapartumCare(this.intrapartumCareDetail).subscribe(value => {
this.tcNotification.success("Intrapartum Care updated");
this.dialogRef.close(this.intrapartumCareDetail);
}, error => {
this.isLoadingResults = false;
})

} 
transformDates(todate: boolean = true) {

if (todate) {
    this.intrapartumCareDetail.date_and_time_of_admission  = new Date(this.date_and_time_of_admission).getTime()/1000;
    if(this.rupture_of_membranes_time && this.rupture_of_membranes_time != "" )
        this.intrapartumCareDetail.rupture_of_membranes_time =  parseInt(this.rupture_of_membranes_time.slice(0,2))*100+parseInt(this.rupture_of_membranes_time.slice(3)) ;
} 
else { 

    const [date, time] = this.dateAndTime(new Date(this.intrapartumCareDetail.date_and_time_of_admission * 1000));
    this.date_and_time_of_admission = `${date}T${time}`;

    this.rupture_of_membranes_time =`${Math.floor(this.intrapartumCareDetail.rupture_of_membranes_time/100)}:${Math.floor(this.intrapartumCareDetail.rupture_of_membranes_time/10)%10}${this.intrapartumCareDetail.rupture_of_membranes_time%10}`;
    if(this.rupture_of_membranes_time.length == 4){
        this.rupture_of_membranes_time = '0' + this.rupture_of_membranes_time;
    }
}
}
canSubmit():boolean{
if (this.intrapartumCareDetail == null){
return false;
}
if(this.rupture_of_membranes_time && !this.rupture_of_membranes_time.match("^[0-9]{2}:[0-9]{2}$")){
 return false;
}

return true;

}
dateAndTime(timedate: Date): [string, string]{
    const date = `${timedate.getFullYear()}-${('0' + (timedate.getMonth() + 1)).slice(-2)}-${('0' + timedate.getDate()).slice(-2)}`
    const time = `${('0' + timedate.getHours()).slice(-2)}:${('0' + timedate.getMinutes()).slice(-2)}`
    return [date, time];
  }
}