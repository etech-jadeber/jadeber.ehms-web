import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrapartumCareListComponent } from './intrapartum-care-list.component';

describe('IntrapartumCareListComponent', () => {
  let component: IntrapartumCareListComponent;
  let fixture: ComponentFixture<IntrapartumCareListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntrapartumCareListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrapartumCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
