import { Component, EventEmitter, Input, OnInit, Output, ViewChild, } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { IntrapartumCareSummary, IntrapartumCareSummaryPartialList } from '../intrapartum_care.model';
import { IntrapartumCarePersist } from '../intrapartum_care.persist';
import { IntrapartumCareNavigator } from '../intrapartum_care.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-intrapartum_care-list',
  templateUrl: './intrapartum-care-list.component.html',
  styleUrls: ['./intrapartum-care-list.component.css']
}) export class IntrapartumCareListComponent implements OnInit {
  intrapartumCaresData: IntrapartumCareSummary[] = [];
  intrapartumCaresTotalCount: number = 0;
  intrapartumCareSelectAll: boolean = false;
  intrapartumCareSelection: IntrapartumCareSummary[] = [];

  intrapartumCaresDisplayedColumns: string[] = ["select", "action",  "date_and_time_of_admission", "rupture_of_membranes", "fatal_heartbeat", "amniotic_fluid", "moulding", "cervical_dilation"];
  intrapartumCareSearchTextBox: FormControl = new FormControl();
  showIntrapartumCares: boolean = false;
  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'],
  };

  multifatal: any[];
  multicervical: any[];
  multitemp: any[];
  multibp: any[];
  multicontraction: any[];
  view: number[] = [900, 400];
  yAxisTicksfatal:number[]=[80,90,100,110,120,130,140,150,160,170,180,190,200];
  yAxisTicksCervix:number[]=[0,1,2,3,4,5,6,7,8,9,10];
  yAxisTicksContraction:number[]=[1,2,3,4,5];
  yAxisTicksBp:number[]=[60,70,80,90,100,110,120,130,140,150,160,170,180];
  // options
  legend: boolean = false;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Time';
  yAxisLabel: string = 'Value';
  timeline: boolean = true;
  intrapartumCareIsLoading: boolean = false;

  @Input() ancHistoryId: any;
  @Output() onResult = new EventEmitter<number>()
  @ViewChild(MatSort, { static: true }) intrapartumCaresSort: MatSort;
  @ViewChild(MatPaginator, { static: true }) intrapartumCaresPaginator: MatPaginator;

  constructor(private router: Router,
    private location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public jobPersist: JobPersist,
    public intrapartumCareNavigator: IntrapartumCareNavigator,
    public intrapartumCarePersist: IntrapartumCarePersist,


  ) {

    this.tcAuthorization.requireRead("intrapartum_cares");
    this.intrapartumCareSearchTextBox.setValue(intrapartumCarePersist.intrapartumCareSearchText);
    //delay subsequent keyup events
    this.intrapartumCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.intrapartumCarePersist.intrapartumCareSearchText = value;
      this.searchIntrapartumCares();
    });
  } ngOnInit() {
    this.intrapartumCaresSort.sortChange.subscribe(() => {
      this.intrapartumCaresPaginator.pageIndex = 0;
      this.searchIntrapartumCares(true);
    });

    this.intrapartumCaresPaginator.page.subscribe(() => {
      this.searchIntrapartumCares(true);
    });
    //start by loading items
    this.searchIntrapartumCares();
  }

  searchIntrapartumCares(isPagination: boolean = false): void {


    let paginator = this.intrapartumCaresPaginator;
    let sorter = this.intrapartumCaresSort;

    this.intrapartumCareIsLoading = true;
    this.intrapartumCareSelection = [];

    this.intrapartumCarePersist.searchIntrapartumCare(this.ancHistoryId, paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: IntrapartumCareSummaryPartialList) => {
      this.intrapartumCaresData = partialList.data;
      if (partialList.total != -1) {
        this.intrapartumCaresTotalCount = partialList.total;

        this.onResult.emit(partialList.total);
        if (this.intrapartumCaresData.length) {
          let fatal=['fatal_heartbeat'];
          let cervical = ['cervical_dilation'];
          let bp=['bp'];
          let temprature=['temprature'];
          let contractions_per_10_min=['contractions_per_10_min'];
          this.multifatal=this.multiFun(fatal);
          this.multicervical=this.multiFun(cervical);
          this.multibp=this.multiFun(bp);
          this.multitemp=this.multiFun(temprature);
          this.multicontraction=this.multiFun(contractions_per_10_min);
          
        }
      }
      this.intrapartumCareIsLoading = false;
    }, error => {
      this.intrapartumCareIsLoading = false;
    });
    
  } 
  
  multiFun(title : string[]):any[]{
    let multi = [];
    title.forEach((element) => {

      let multiObj = {
        name: element,
        series: [],
      };
      this.intrapartumCaresData.forEach((resel) => {
        let obj = {
          name: this.tcUtilsDate.toDate(resel.date_and_time_of_admission),
          value: parseFloat(resel[element]),
        };
        multiObj.series.push(obj);
      });
      multi.push(multiObj);
    });
    
    // Object.assign(this, { multi });

    return multi;

  }

  downloadIntrapartumCares(): void {
    if (this.intrapartumCareSelectAll) {
      this.intrapartumCarePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download intrapartum_care", true);
      });
    }
    else {
      this.intrapartumCarePersist.download(this.tcUtilsArray.idsList(this.intrapartumCareSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download intrapartum_care", true);
      });
    }
  }
  addIntrapartumCare(): void {
    let dialogRef = this.intrapartumCareNavigator.addIntrapartumCare(this.ancHistoryId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchIntrapartumCares();
      }
    });
  }

  editIntrapartumCare(item: IntrapartumCareSummary) {
    let dialogRef = this.intrapartumCareNavigator.editIntrapartumCare(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteIntrapartumCare(item: IntrapartumCareSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("intrapartum_care");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.intrapartumCarePersist.deleteIntrapartumCare(item.id).subscribe(response => {
          this.tcNotification.success("intrapartum_care deleted");
          this.searchIntrapartumCares();
        }, error => {
        });
      }

    });
  } back(): void {
    this.location.back();
  }
}