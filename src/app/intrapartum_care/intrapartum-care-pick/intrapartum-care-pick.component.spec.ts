import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrapartumCarePickComponent } from './intrapartum-care-pick.component';

describe('IntrapartumCarePickComponent', () => {
  let component: IntrapartumCarePickComponent;
  let fixture: ComponentFixture<IntrapartumCarePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntrapartumCarePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrapartumCarePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
