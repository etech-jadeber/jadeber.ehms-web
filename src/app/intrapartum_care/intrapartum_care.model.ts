import {TCId} from "../tc/models";export class IntrapartumCareSummary extends TCId {
    encounter_id:string;
    date_and_time_of_admission:number;
    rupture_of_membranes:string;
    rupture_of_membranes_time:number;
    fatal_heartbeat:number;
    amniotic_fluid:string;
    moulding:string;
    cervical_dilation:number;
    contractions_per_10_min:number;
    oxytocin_u_l:string;
    oxytocin_drops_min:number;
    drugs_and_fluids_given:string;
    pulse:number;
    bp:number;
    temprature:number;
    urine_protein:string;
    urine_acetone:string;
    urine_volume:string;
    patient_id:string;
    anc_history:string;
    sbp: number;
    dbp: number;
     
    }
    export class IntrapartumCareSummaryPartialList {
      data: IntrapartumCareSummary[];
      total: number;
    }
    export class IntrapartumCareDetail extends IntrapartumCareSummary {

    }
    
    export class IntrapartumCareDashboard {
      total: number;
    }