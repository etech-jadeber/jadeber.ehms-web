import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {IntrapartumCareEditComponent} from "./intrapartum-care-edit/intrapartum-care-edit.component";
import {IntrapartumCarePickComponent} from "./intrapartum-care-pick/intrapartum-care-pick.component";


@Injectable({
  providedIn: 'root'
})

export class IntrapartumCareNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  intrapartumCaresUrl(): string {
    return "/intrapartum_cares";
  }

  intrapartumCareUrl(id: string): string {
    return "/intrapartum_cares/" + id;
  }

  viewIntrapartumCares(): void {
    this.router.navigateByUrl(this.intrapartumCaresUrl());
  }

  viewIntrapartumCare(id: string): void {
    this.router.navigateByUrl(this.intrapartumCareUrl(id));
  }

  editIntrapartumCare(id: string,): MatDialogRef<IntrapartumCareEditComponent> {
    const dialogRef = this.dialog.open(IntrapartumCareEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addIntrapartumCare(encounterId: string): MatDialogRef<IntrapartumCareEditComponent> {
    const dialogRef = this.dialog.open(IntrapartumCareEditComponent, {
          data: new TCIdMode(encounterId, TCModalModes.NEW),
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickIntrapartumCares(selectOne: boolean=false): MatDialogRef<IntrapartumCarePickComponent> {
      const dialogRef = this.dialog.open(IntrapartumCarePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}