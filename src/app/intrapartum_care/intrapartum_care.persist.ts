import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {IntrapartumCareDashboard, IntrapartumCareDetail, IntrapartumCareSummaryPartialList} from "./intrapartum_care.model";


@Injectable({
  providedIn: 'root'
})
export class IntrapartumCarePersist {
 intrapartumCareSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.intrapartumCareSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchIntrapartumCare(encounterId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<IntrapartumCareSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("anc_history/"+encounterId+"/intrapartum_care", this.intrapartumCareSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<IntrapartumCareSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "intrapartum_care/do", new TCDoParam("download_all", this.filters()));
  }

  intrapartumCareDashboard(): Observable<IntrapartumCareDashboard> {
    return this.http.get<IntrapartumCareDashboard>(environment.tcApiBaseUri + "intrapartum_care/dashboard");
  }

  getIntrapartumCare(id: string): Observable<IntrapartumCareDetail> {
    return this.http.get<IntrapartumCareDetail>(environment.tcApiBaseUri + "intrapartum_care/" + id);
  } 
  
  
  addIntrapartumCare(item: IntrapartumCareDetail, encounterId:string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri +"form_encounters/"+encounterId+ "/intrapartum_care/", item);
  }

  updateIntrapartumCare(item: IntrapartumCareDetail): Observable<IntrapartumCareDetail> {
    return this.http.patch<IntrapartumCareDetail>(environment.tcApiBaseUri + "intrapartum_care/" + item.id, item);
  }

  deleteIntrapartumCare(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "intrapartum_care/" + id);
  }
 intrapartumCaresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "intrapartum_care/do", new TCDoParam(method, payload));
  }

  intrapartumCareDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "intrapartum_cares/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "intrapartum_care/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "intrapartum_care/" + id + "/do", new TCDoParam("print", {}));
  }


}