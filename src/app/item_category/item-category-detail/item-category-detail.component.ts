import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ItemCategoryDetail} from "../item_category.model";
import {ItemCategoryPersist} from "../item_category.persist";
import {ItemCategoryNavigator} from "../item_category.navigator";

export enum ItemCategoryTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-item_category-detail',
  templateUrl: './item-category-detail.component.html',
  styleUrls: ['./item-category-detail.component.scss']
})
export class ItemCategoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemCategoryIsLoading:boolean = false;
  
  ItemCategoryTabs: typeof ItemCategoryTabs = ItemCategoryTabs;
  activeTab: ItemCategoryTabs = ItemCategoryTabs.overview;
  //basics
  itemCategoryDetail: ItemCategoryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public itemCategoryNavigator: ItemCategoryNavigator,
              public  itemCategoryPersist: ItemCategoryPersist) {
    this.tcAuthorization.requireRead("item_categorys");
    this.itemCategoryDetail = new ItemCategoryDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("item_categorys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.itemCategoryIsLoading = true;
    this.itemCategoryPersist.getItemCategory(id).subscribe(itemCategoryDetail => {
          this.itemCategoryDetail = itemCategoryDetail;
          this.itemCategoryIsLoading = false;
        }, error => {
          console.error(error);
          this.itemCategoryIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.itemCategoryDetail.id);
  }
  editItemCategory(): void {
    let modalRef = this.itemCategoryNavigator.editItemCategory(this.itemCategoryDetail.id);
    modalRef.afterClosed().subscribe(itemCategoryDetail => {
      TCUtilsAngular.assign(this.itemCategoryDetail, itemCategoryDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.itemCategoryPersist.print(this.itemCategoryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_category", true);
      });
    }

  back():void{
      this.location.back();
    }

}