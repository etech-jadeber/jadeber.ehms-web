import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemCategoryDetail } from '../item_category.model';import { ItemCategoryPersist } from '../item_category.persist';import { ItemCategoryNavigator } from '../item_category.navigator';
@Component({
  selector: 'app-item_category-edit',
  templateUrl: './item-category-edit.component.html',
  styleUrls: ['./item-category-edit.component.scss']
})export class ItemCategoryEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  itemCategoryDetail: ItemCategoryDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ItemCategoryEditComponent>,
              public  persist: ItemCategoryPersist,
              public categoryNavigator: ItemCategoryNavigator,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("item_categorys");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_category";
      this.itemCategoryDetail = new ItemCategoryDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("item_categorys");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "item_category";
      this.isLoadingResults = true;
      this.persist.getItemCategory(this.idMode.id).subscribe(itemCategoryDetail => {
        this.itemCategoryDetail = itemCategoryDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addItemCategory(this.itemCategoryDetail).subscribe(value => {
      this.tcNotification.success("itemCategory added");
      this.itemCategoryDetail.id = value.id;
      this.dialogRef.close(this.itemCategoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateItemCategory(this.itemCategoryDetail).subscribe(value => {
      this.tcNotification.success("item_category updated");
      this.dialogRef.close(this.itemCategoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  canSubmit():boolean{
    if (this.itemCategoryDetail == null){
        return false;
      }

    if (this.itemCategoryDetail.name == null || this.itemCategoryDetail.name  == "") {
                return false;
    }  
    if (this.itemCategoryDetail.category == null) {
                return false;
    } 
    if(this.itemCategoryDetail.abbrivation == null || this.itemCategoryDetail.abbrivation ==""){
      return false;
    }
    return true;

 }

 }