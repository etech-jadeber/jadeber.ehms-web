import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemCategorySummary, ItemCategorySummaryPartialList } from '../item_category.model';
import { ItemCategoryPersist } from '../item_category.persist';
import { ItemCategoryNavigator } from '../item_category.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-item_category-list',
  templateUrl: './item-category-list.component.html',
  styleUrls: ['./item-category-list.component.scss']
})export class ItemCategoryListComponent implements OnInit {
  itemCategorysData: ItemCategorySummary[] = [];
  itemCategorysTotalCount: number = 0;
  itemCategorySelectAll:boolean = false;
  itemCategorySelection: ItemCategorySummary[] = [];

 itemCategorysDisplayedColumns: string[] = ["select","action","name","category", "abbrivation" ];
  itemCategorySearchTextBox: FormControl = new FormControl();
  itemCategoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemCategorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemCategorysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemCategoryPersist: ItemCategoryPersist,
                public itemCategoryNavigator: ItemCategoryNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,

    ) {

        this.tcAuthorization.requireRead("item_categorys");
       this.itemCategorySearchTextBox.setValue(itemCategoryPersist.itemCategorySearchText);
      //delay subsequent keyup events
      this.itemCategorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemCategoryPersist.itemCategorySearchText = value;
        this.searchItemCategorys();
      });
    } ngOnInit() {
   
      this.itemCategorysSort.sortChange.subscribe(() => {
        this.itemCategorysPaginator.pageIndex = 0;
        this.searchItemCategorys(true);
      });

      this.itemCategorysPaginator.page.subscribe(() => {
        this.searchItemCategorys(true);
      });
      //start by loading items
      this.searchItemCategorys();
    }

  searchItemCategorys(isPagination:boolean = false): void {


    let paginator = this.itemCategorysPaginator;
    let sorter = this.itemCategorysSort;

    this.itemCategoryIsLoading = true;
    this.itemCategorySelection = [];

    this.itemCategoryPersist.searchItemCategory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemCategorySummaryPartialList) => {
      this.itemCategorysData = partialList.data;
      if (partialList.total != -1) {
        this.itemCategorysTotalCount = partialList.total;
      }
      this.itemCategoryIsLoading = false;
    }, error => {
      this.itemCategoryIsLoading = false;
    });

  } downloadItemCategorys(): void {
    if(this.itemCategorySelectAll){
         this.itemCategoryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_category", true);
      });
    }
    else{
        this.itemCategoryPersist.download(this.tcUtilsArray.idsList(this.itemCategorySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download item_category",true);
            });
        }
  }
addItemCategory(): void {
    let dialogRef = this.itemCategoryNavigator.addItemCategory();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItemCategorys();
      }
    });
  }

  editItemCategory(item: ItemCategorySummary) {
    let dialogRef = this.itemCategoryNavigator.editItemCategory(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteItemCategory(item: ItemCategorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("item_category");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.itemCategoryPersist.deleteItemCategory(item.id).subscribe(response => {
          this.tcNotification.success("item_category deleted");
          this.searchItemCategorys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}