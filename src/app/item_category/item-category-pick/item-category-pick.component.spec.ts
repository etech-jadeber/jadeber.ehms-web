import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemCategoryPickComponent } from './item-category-pick.component';

describe('ItemCategoryPickComponent', () => {
  let component: ItemCategoryPickComponent;
  let fixture: ComponentFixture<ItemCategoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemCategoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCategoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
