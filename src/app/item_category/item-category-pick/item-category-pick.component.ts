import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemCategorySummary, ItemCategorySummaryPartialList } from '../item_category.model';
import { ItemCategoryPersist } from '../item_category.persist';
import { ItemCategoryNavigator } from '../item_category.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-item_category-pick',
  templateUrl: './item-category-pick.component.html',
  styleUrls: ['./item-category-pick.component.scss']
})export class ItemCategoryPickComponent implements OnInit {
  itemCategorysData: ItemCategorySummary[] = [];
  itemCategorysTotalCount: number = 0;
  itemCategorySelectAll:boolean = false;
  itemCategorySelection: ItemCategorySummary[] = [];

 itemCategorysDisplayedColumns: string[] = ["select","name","category" ];
  itemCategorySearchTextBox: FormControl = new FormControl();
  itemCategoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemCategorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemCategorysSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemCategoryPersist: ItemCategoryPersist,
                public itemCategoryNavigator: ItemCategoryNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ItemCategoryPickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, category: string}

    ) {

        this.tcAuthorization.requireRead("item_categorys");
       this.itemCategorySearchTextBox.setValue(itemCategoryPersist.itemCategorySearchText);
      //delay subsequent keyup events
      this.itemCategorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemCategoryPersist.itemCategorySearchText = value;
        this.searchItem_categorys();
      });
    } ngOnInit() {
   
      this.itemCategorysSort.sortChange.subscribe(() => {
        this.itemCategorysPaginator.pageIndex = 0;
        this.searchItem_categorys(true);
      });

      this.itemCategorysPaginator.page.subscribe(() => {
        this.searchItem_categorys(true);
      });
      //start by loading items
      this.searchItem_categorys();
    }

  searchItem_categorys(isPagination:boolean = false): void {


    let paginator = this.itemCategorysPaginator;
    let sorter = this.itemCategorysSort;

    this.itemCategoryIsLoading = true;
    this.itemCategorySelection = [];

    this.itemCategoryPersist.searchItemCategory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.data.category).subscribe((partialList: ItemCategorySummaryPartialList) => {
      this.itemCategorysData = partialList.data;
      if (partialList.total != -1) {
        this.itemCategorysTotalCount = partialList.total;
      }
      this.itemCategoryIsLoading = false;
    }, error => {
      this.itemCategoryIsLoading = false;
    });

  }
  markOneItem(item: ItemCategorySummary) {
    if(!this.tcUtilsArray.containsId(this.itemCategorySelection,item.id)){
          this.itemCategorySelection = [];
          this.itemCategorySelection.push(item);
        }
        else{
          this.itemCategorySelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemCategorySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }