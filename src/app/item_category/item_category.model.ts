import {TCId} from "../tc/models";

export class ItemCategorySummary extends TCId {
    category:number;
    name:string;
    abbrivation:string;
    }
    export class ItemCategorySummaryPartialList {
      data: ItemCategorySummary[];
      total: number;
    }
    export class ItemCategoryDetail extends ItemCategorySummary {
    }
    
    export class ItemCategoryDashboard {
      total: number;
    }