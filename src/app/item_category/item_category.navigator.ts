import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ItemCategoryEditComponent} from "./item-category-edit/item-category-edit.component";
import {ItemCategoryPickComponent} from "./item-category-pick/item-category-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ItemCategoryNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  itemCategorysUrl(): string {
    return "/item_categorys";
  }

  itemCategoryUrl(id: string): string {
    return "/item_categorys/" + id;
  }

  viewItemCategorys(): void {
    this.router.navigateByUrl(this.itemCategorysUrl());
  }

  viewItemCategory(id: string): void {
    this.router.navigateByUrl(this.itemCategoryUrl(id));
  }

  editItemCategory(id: string): MatDialogRef<ItemCategoryEditComponent> {
    const dialogRef = this.dialog.open(ItemCategoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemCategory(): MatDialogRef<ItemCategoryEditComponent> {
    const dialogRef = this.dialog.open(ItemCategoryEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickItemCategorys(category: string, selectOne: boolean=false): MatDialogRef<ItemCategoryPickComponent> {
      const dialogRef = this.dialog.open(ItemCategoryPickComponent, {
        data: {selectOne, category},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}