import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {ItemCategoryDashboard, ItemCategoryDetail, ItemCategorySummaryPartialList} from "./item_category.model";
import { TCUtilsString } from '../tc/utils-string';
import { category } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class ItemCategoryPersist {
 itemCategorySearchText: string = "";
 
 category: TCEnum[] = [
  new TCEnum(category.drug, 'Drug'),
  new TCEnum(category.supply, 'Supply'),
];
 

 constructor(private http: HttpClient) {
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemCategorySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchItemCategory(pageSize: number, pageIndex: number, sort: string, order: string,category:string=null): Observable<ItemCategorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_category", this.itemCategorySearchText, pageSize, pageIndex, sort, order);
    if(category){
      url = TCUtilsString.appendUrlParameter(url, "category", category);
    }
    return this.http.get<ItemCategorySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_category/do", new TCDoParam("download_all", this.filters()));
  }

  itemCategoryDashboard(): Observable<ItemCategoryDashboard> {
    return this.http.get<ItemCategoryDashboard>(environment.tcApiBaseUri + "item_category/dashboard");
  }

  getItemCategory(id: string): Observable<ItemCategoryDetail> {
    return this.http.get<ItemCategoryDetail>(environment.tcApiBaseUri + "item_category/" + id);
  } addItemCategory(item: ItemCategoryDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_category/", item);
  }

  updateItemCategory(item: ItemCategoryDetail): Observable<ItemCategoryDetail> {
    return this.http.patch<ItemCategoryDetail>(environment.tcApiBaseUri + "item_category/" + item.id, item);
  }

  deleteItemCategory(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "item_category/" + id);
  }
 itemCategorysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_category/do", new TCDoParam(method, payload));
  }

  itemCategoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_categorys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "item_category/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "item_category/" + id + "/do", new TCDoParam("print", {}));
  }


}