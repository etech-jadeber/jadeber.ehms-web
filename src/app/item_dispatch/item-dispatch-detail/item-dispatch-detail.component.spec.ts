import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDispatchDetailComponent } from './item-dispatch-detail.component';

describe('ItemDispatchDetailComponent', () => {
  let component: ItemDispatchDetailComponent;
  let fixture: ComponentFixture<ItemDispatchDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDispatchDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDispatchDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
