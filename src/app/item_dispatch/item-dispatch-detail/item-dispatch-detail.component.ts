import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ItemDispatchDetail} from "../item_dispatch.model";
import {ItemDispatchPersist} from "../item_dispatch.persist";
import {ItemDispatchNavigator} from "../item_dispatch.navigator";

export enum ItemDispatchTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-item_dispatch-detail',
  templateUrl: './item-dispatch-detail.component.html',
  styleUrls: ['./item-dispatch-detail.component.scss']
})
export class ItemDispatchDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemDispatchIsLoading:boolean = false;
  
  ItemDispatchTabs: typeof ItemDispatchTabs = ItemDispatchTabs;
  activeTab: ItemDispatchTabs = ItemDispatchTabs.overview;
  //basics
  itemDispatchDetail: ItemDispatchDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public itemDispatchNavigator: ItemDispatchNavigator,
              public  itemDispatchPersist: ItemDispatchPersist) {
    this.tcAuthorization.requireRead("item_dispatchs");
    this.itemDispatchDetail = new ItemDispatchDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("item_dispatchs");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.itemDispatchIsLoading = true;
    this.itemDispatchPersist.getItemDispatch(id).subscribe(itemDispatchDetail => {
          this.itemDispatchDetail = itemDispatchDetail;
          this.itemDispatchIsLoading = false;
        }, error => {
          console.error(error);
          this.itemDispatchIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.itemDispatchDetail.id);
  }
  editItemDispatch(): void {
    let modalRef = this.itemDispatchNavigator.editItemDispatch(this.itemDispatchDetail.id);
    modalRef.afterClosed().subscribe(itemDispatchDetail => {
      TCUtilsAngular.assign(this.itemDispatchDetail, itemDispatchDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.itemDispatchPersist.print(this.itemDispatchDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_dispatch", true);
      });
    }

  back():void{
      this.location.back();
    }

}