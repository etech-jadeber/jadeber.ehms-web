import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDispatchEditComponent } from './item-dispatch-edit.component';

describe('ItemDispatchEditComponent', () => {
  let component: ItemDispatchEditComponent;
  let fixture: ComponentFixture<ItemDispatchEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDispatchEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDispatchEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
