import {Component, OnInit, Inject, Injectable, Input, EventEmitter, Output} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemDispatchDetail } from '../item_dispatch.model';import { ItemDispatchPersist } from '../item_dispatch.persist';
import { TransferNavigator } from 'src/app/transfers/transfer.navigator';
import { TransferDetail, TransferSummary } from 'src/app/transfers/transfer.model';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { PrescriptionRequestStatus } from 'src/app/app.enums';
import { StorePersist } from 'src/app/stores/store.persist';
import { StoreDetail } from 'src/app/stores/store.model';
import { MarkupPricePersist } from 'src/app/markup_price/markup_price.persist';
import { PrescriptionRequestSummary } from 'src/app/prescription_request/prescription_request.model';
import { ItemDispatchNavigator } from '../item_dispatch.navigator';
import { ItemNavigator } from 'src/app/items/item.navigator';


@Injectable()
abstract class ItemDispatchComponent implements OnInit  {
  itemDispatchsDisplayedColumns: string[] = ["action","edit","item_name","batch_no","quantity"];
  batch_no:string;
  remaining :number;
  totalQuantity:number=0;
  service_type:number;
  total_price:number = 0;
  isDialog:boolean;
  status: number;
  isLoadingResults: boolean = false;
  title: string;
  itemDispatchDetailList: ItemDispatchDetail[]=[];
  prescriptionRequestStatus = PrescriptionRequestStatus;
  itemDispatchDetail: ItemDispatchDetail;
  constructor(
      public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray :TCUtilsArray,
              public appTranslation:AppTranslation,
              public itemDispatchNavigator: ItemDispatchNavigator,
              public  persist: ItemDispatchPersist,
              public itemNav : ItemNavigator,
              public storePersist: StorePersist,
              public markupPricePersist: MarkupPricePersist,
              public tcUtilsDate: TCUtilsDate,
              public transferNavigator: TransferNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: {id:any|[],status:number,mode:string,list:ItemDispatchDetail[]}) {

  }

  onCancel(): void {
  }

  closeDialog(){

  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }
  
  ngOnInit() {
    this.idMode.status = this.idMode.status ? this.idMode.status : this.status;
    this.itemDispatchsDisplayedColumns = this.idMode.status == PrescriptionRequestStatus.accept ? this.itemDispatchsDisplayedColumns : ["item_name","batch_no","quantity"];
    if (this.isNew() || this.isCase()) {
     this.tcAuthorization.requireCreate("item_dispatchs");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_dispatch";
      this.itemDispatchDetail = new ItemDispatchDetail();
      this.itemDispatchDetail.request_id=this.idMode.id.id;
      this.storePersist.storesDo("self",{}).subscribe((store:StoreDetail)=>{
        if(store){
            this.service_type = store.store_type;
        }
      })
      //set necessary defaults
      if(this.idMode.status == PrescriptionRequestStatus.accept){
        this.remaining = +this.idMode.id.quantity || 0;
      }
      else if (this.idMode.status == PrescriptionRequestStatus.dispatch){
        this.remaining = 0;
        this.totalQuantity = this.idMode.id.quantity
        this.persist.itemDispatchsDo("item_dispatch_with_prescription_request",{request_id : this.idMode.id.id}).subscribe((value: any) =>{
          if(value.total == 0){
            this.tcNotification.error("No paid item")
            this.onCancel();
          }
          this.itemDispatchDetailList = value.data;
  
        })
      }

    }else if(this.isWizard()){ 
      this.tcAuthorization.requireCreate("item_dispatchs");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_dispatch";
      this.itemDispatchDetail = new ItemDispatchDetail();
      this.itemDispatchDetail.request_id= this.idMode.id[0].id;
      this.itemDispatchDetailList = [...this.itemDispatchDetailList, ...this.idMode.list]
      this.storePersist.storesDo("self",{}).subscribe((store:StoreDetail)=>{
        if(store){
            this.service_type = store.store_type;
        }
      })
      //set necessary defaults
      if(this.idMode.status == PrescriptionRequestStatus.accept){
        this.remaining = +this.idMode.id[0].quantity || 0;
      }
      else if (this.idMode.status == PrescriptionRequestStatus.dispatch){
        this.remaining = 0;
        this.totalQuantity = this.idMode.id.quantity
        this.persist.itemDispatchsDo("item_dispatch_with_prescription_request",{request_id : this.idMode.id.id}).subscribe((value: any) =>{
          if(value.total == 0){
            this.tcNotification.error("No paid item")
            this.onCancel();
          }
          this.itemDispatchDetailList = value.data;
  
        })
      }
    } else {
     this.tcAuthorization.requireUpdate("item_dispatchs");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "item_dispatch";
      this.isLoadingResults = true;
      this.persist.getItemDispatch(this.idMode.id).subscribe(itemDispatchDetail => {
        this.itemDispatchDetail = itemDispatchDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  

  AddToList():void{
    this.totalQuantity+=+this.itemDispatchDetail.quantity;
    this.remaining-=+this.itemDispatchDetail.quantity
    this.total_price += this.itemDispatchDetail.total_price; 
    this.itemDispatchDetailList=[this.itemDispatchDetail,...this.itemDispatchDetailList];
    this.itemDispatchDetail=new ItemDispatchDetail();
    this.itemDispatchDetail.request_id=this.isWizard() ? this.idMode.id[0].id : this.idMode.id.id;
  }

  removeFromList(item:ItemDispatchDetail):void{
    this.totalQuantity-=+item.quantity;
    this.remaining+=+item.quantity;
    this.total_price -= item.total_price;
    this.itemDispatchDetailList=this.itemDispatchDetailList.filter((value)=> value != item);
  }
  editFromList(item:ItemDispatchDetail){
    this.removeFromList(item);
    this.itemDispatchDetail=item;
  }

  acceptOrDispatchItems(){
    this.itemDispatchDetailList.forEach(value =>{
      if(value.request_id == this.itemDispatchDetail.request_id){
        value.disabled = true;
      }
    })
    this.tcUtilsArray.removeItem(this.idMode.id, this.idMode.id[0],"id")
    this.totalQuantity = 0;
    if(this.idMode.id.length){
      this.itemDispatchDetail = new ItemDispatchDetail();
      this.itemDispatchDetail.request_id = this.idMode.id[0].id;
      this.remaining = +this.idMode.id[0].quantity || 0;
    }

  }


  transferPick(){
    let dialogRef = this.transferNavigator.pickTransfers(this.isWizard()  ? this.idMode.id[0].drug_id : (this.idMode.id.interchange_id?this.idMode.id.interchange_id : this.idMode.id.drug_id));
      dialogRef.afterClosed().subscribe((result:TransferSummary[]) => {
        if(result){
         if(this.itemDispatchDetailList.some(value=>(value.target_id==result[0].id && value.request_id == this.itemDispatchDetail.request_id))){
          this.tcNotification.error("You have already selected this item please edit if you want")
         }else if(!result[0].unit_price){
          this.tcNotification.error("No unit price added")
         }
         else{
          this.itemDispatchDetail.target_id=result[0].id;
          this.itemDispatchDetail.item_name = this.itemNav.itemName(result[0]);
          
          this.itemDispatchDetail.batch_no=result[0].batch_no;
          this.itemDispatchDetail.remaining_of_transfer=result[0].remaining;
          this.markupPricePersist.markupPricesDo("total_price",{unit_price:result[0].unit_price,service_type:this.service_type}).subscribe((price:   {total_price:number})=>{
            this.itemDispatchDetail.price = price.total_price;
          })
         }
          
        }
        })
      }
  onAdd(): void {
    this.isLoadingResults = true;
    
    (this.isWizard() || this.isNew()) && this.persist.addItemDispatch(this.itemDispatchDetailList).subscribe(value => {
      this.tcNotification.success("itemDispatch added");
      // this.itemDispatchDetail.id = value.id;
      this.closeDialog();
    }, error => {
      this.isLoadingResults = false;
    })
    this.isCase() && this.persist.addItemRequestDispatch(this.itemDispatchDetailList).subscribe(value => {
      this.tcNotification.success("itemDispatch added");
      // this.itemDispatchDetail.id = value.id;
      this.closeDialog();
    }, error => {
      this.isLoadingResults = false;
    })
  }
  canSaveDispatch():boolean{
    return true;
  }

  onDispatch():void {
    this.persist.itemDispatchsDo("dispatch",{data: this.itemDispatchDetailList, request_id : this.idMode.id.id}).subscribe((result)=>{
      if(result){
        this.tcNotification.success("You have successfully dispatched");
        this.closeDialog();
      }
    })
  }

  totalPriceCalculate():  void{
    this.itemDispatchDetail.total_price = this.itemDispatchDetail.price * this.itemDispatchDetail.quantity;
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateItemDispatch(this.itemDispatchDetail).subscribe(value => {
      this.tcNotification.success("item_dispatch updated");
      this.closeDialog();
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.itemDispatchDetail == null){
            return false;
          }

        if (this.isDialog && this.itemDispatchDetail.request_id == null || this.itemDispatchDetail.request_id  == "") {
                    return false;
                } 
        if (this.itemDispatchDetail.target_id == null || this.itemDispatchDetail.target_id  == "") {
                    return false;
                } 
        if( this.itemDispatchDetail.quantity == null || this.itemDispatchDetail.quantity <=0 || this.itemDispatchDetail.quantity>this.itemDispatchDetail.remaining_of_transfer){
          return false;
        }
        if (this.idMode.id.quantity && this.isDialog && (this.itemDispatchDetail.quantity>this.remaining)) {
                    return false;
                }

        return true;
 }
 canSave():boolean{
  if(this.totalQuantity <= 0){
    return false;
  }
  if ((this.isWizard() && this.idMode.id[0].quantity &&  this.totalQuantity!=+this.idMode.id[0].quantity)|| (this.idMode.id.quantity && this.totalQuantity!=+this.idMode.id.quantity)){
    return false
  }
  return true;
 }

 }




 @Component({
  selector: 'app-item_dispatch-edit',
  templateUrl: './item-dispatch-edit.component.html',
  styleUrls: ['./item-dispatch-edit.component.scss']
})
export class ItemDispatchEditComponent extends ItemDispatchComponent {

  isDialog:boolean = true;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray :TCUtilsArray,
              public appTranslation:AppTranslation,
              public itemDispatchNavigator: ItemDispatchNavigator,
              public dialogRef: MatDialogRef<ItemDispatchEditComponent>,
              public  persist: ItemDispatchPersist,
              public itemNav : ItemNavigator,
              public storePersist: StorePersist,
              public markupPricePersist: MarkupPricePersist,
              public tcUtilsDate: TCUtilsDate,
              public transferNavigator: TransferNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: {id:any,status:number,mode:string,list:ItemDispatchDetail[]}) {
              super(tcAuthorization,tcNotification,tcUtilsString,tcUtilsArray,appTranslation,itemDispatchNavigator,persist,itemNav,storePersist,markupPricePersist,tcUtilsDate,transferNavigator,idMode)

  }

  onCancel(): void {
    this.dialogRef.close();
  }
 closeDialog(){
    this.dialogRef.close(this.itemDispatchDetailList);
  }

 }


 @Component({
  selector: 'app-item_dispatch-edit-list',
  templateUrl: './item-dispatch-edit.component.html',
  styleUrls: ['./item-dispatch-edit.component.scss']
})
 export class ItemDispatchEditChildComponent extends ItemDispatchComponent {
  
  isDialog:boolean = false;
  @Input() status: number;
  @Input() can_save: boolean;
  @Output() save = new EventEmitter<string>();
  @Output() cancel = new EventEmitter<string>();
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray :TCUtilsArray,
              public appTranslation:AppTranslation,
              public itemDispatchNavigator : ItemDispatchNavigator,
              public  persist: ItemDispatchPersist,
              public itemNav : ItemNavigator,
              public storePersist: StorePersist,
              public markupPricePersist: MarkupPricePersist,
              public tcUtilsDate: TCUtilsDate,
              public transferNavigator: TransferNavigator
              ) {

                super(tcAuthorization,tcNotification,tcUtilsString,tcUtilsArray,appTranslation,itemDispatchNavigator,persist,itemNav,storePersist,markupPricePersist,tcUtilsDate,transferNavigator,{ id: {},status: 0,mode: TCModalModes.NEW,list:[]})

  }

  onAdd(): void {
      this.save.emit();
  }

  onCancel(): void {
      this.cancel.emit();
  }

  canSaveDispatch(): boolean {
      return this.can_save;
  }

 
 }