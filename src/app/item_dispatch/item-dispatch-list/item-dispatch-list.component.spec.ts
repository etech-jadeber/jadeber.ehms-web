import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDispatchListComponent } from './item-dispatch-list.component';

describe('ItemDispatchListComponent', () => {
  let component: ItemDispatchListComponent;
  let fixture: ComponentFixture<ItemDispatchListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDispatchListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDispatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
