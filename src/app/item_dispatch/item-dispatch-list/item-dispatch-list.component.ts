import {Component, OnInit, ViewChild,Input} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemDispatchSummary, ItemDispatchSummaryPartialList } from '../item_dispatch.model';
import { ItemDispatchPersist } from '../item_dispatch.persist';
import { ItemDispatchNavigator } from '../item_dispatch.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TransferPersist } from 'src/app/transfers/transfer.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemReceivePersist } from 'src/app/item_receive/item_receive.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { dispatch_status } from 'src/app/app.enums';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
import { ItemReturnNavigator } from 'src/app/item_return/item_return.navigator';

@Component({
  selector: 'app-item_dispatch-list',
  templateUrl: './item-dispatch-list.component.html',
  styleUrls: ['./item-dispatch-list.component.scss']
})export class ItemDispatchListComponent implements OnInit {
  itemDispatchsData: ItemDispatchSummary[] = [];
  itemDispatchsTotalCount: number = 0;
  itemDispatchSelectAll:boolean = false;
  itemDispatchSelection: ItemDispatchSummary[] = [];
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
 itemDispatchsDisplayedColumns: string[] = ["select","mrn","name" ,"batch_no","quantity","dispatch_date","response_date" ];
  itemDispatchSearchTextBox: FormControl = new FormControl();
  itemDispatchIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) itemDispatchsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemDispatchsSort: MatSort;
  @Input() prescription_request_id:string;
  @Input() dispatch_status:number;
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemNavigator: ItemNavigator,
                public itemDispatchPersist: ItemDispatchPersist,
                public itemDispatchNavigator: ItemDispatchNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public transferPersist: TransferPersist,
                public itemPersist: ItemPersist,
                public itemReceivePersist: ItemReceivePersist,
                public categoryNavigator: ItemCategoryNavigator,
                public storeNavigator: StoreNavigator,
                public categoryPersist: ItemCategoryPersist,
                public tcUtilsString: TCUtilsString,
                public itemReturnNavigator: ItemReturnNavigator,

    ) {

        this.tcAuthorization.requireRead("item_dispatchs");
       this.itemDispatchSearchTextBox.setValue(itemDispatchPersist.itemDispatchSearchText);
      //delay subsequent keyup events
      this.itemDispatchSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemDispatchPersist.itemDispatchSearchText = value;
        this.searchItemDispatchs();
      });
      this.itemDispatchPersist.start_date.valueChanges.subscribe((value)=>{

          this.searchItemDispatchs()
      });
      this.itemDispatchPersist.end_date.valueChanges.subscribe((value)=>{
          this.searchItemDispatchs()
      });
    } ngOnInit() {
      if (this.prescription_request_id){
        this.itemDispatchsDisplayedColumns.splice(1, 0, 'action')
      }
      this.itemDispatchPersist.dispatch_status = this.dispatch_status;
      this.itemDispatchsSort.sortChange.subscribe(() => {
      this.itemDispatchsSort.active="dispatch_date";
      this.itemDispatchsSort.direction = "desc";
        this.itemDispatchsPaginator.pageIndex = 0;
        this.searchItemDispatchs(true);
      });

      this.itemDispatchsPaginator.page.subscribe(() => {
        this.searchItemDispatchs(true);
      });
      this.itemDispatchsSort.active="dispatch_date";
      this.itemDispatchsSort.direction = "desc";
      //start by loading items
      this.searchItemDispatchs();
    }

  searchItemDispatchs(isPagination:boolean = false): void {


    let paginator = this.itemDispatchsPaginator;
    let sorter = this.itemDispatchsSort;

    this.itemDispatchIsLoading = true;
    this.itemDispatchSelection = [];
    this.itemDispatchPersist.prescription_request_id=this.prescription_request_id;
    this.itemDispatchPersist.searchItemDispatch(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemDispatchSummaryPartialList) => {
      this.itemDispatchsData = partialList.data;
      if (partialList.total != -1) {
        this.itemDispatchsTotalCount = partialList.total;
        this.itemDispatchsData.forEach(itemDispatch=>{
          this.itemPersist.getItem(itemDispatch.item_id).subscribe((item)=>{
            itemDispatch.item_name=item.name;
          })

        })
      }
      this.itemDispatchIsLoading = false;
    }, error => {
      this.itemDispatchIsLoading = false;
    });

  } downloadItemDispatchs(): void {
    if(this.itemDispatchSelectAll){
         this.itemDispatchPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_dispatch", true);
      });
    }
    else{
        this.itemDispatchPersist.download(this.tcUtilsArray.idsList(this.itemDispatchSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download item_dispatch",true);
            });
        }
  }


  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.itemDispatchPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.itemDispatchPersist.categoryId = result[0].id;
      }
      this.searchItemDispatchs()
    })
  }
  
  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.itemDispatchPersist.store_name = result[0].name;
        this.itemDispatchPersist.store_id = result[0].id;
        this.searchItemDispatchs();
      }
    });
  }

// addItemDispatch(): void {
//     let dialogRef = this.itemDispatchNavigator.addItemDispatch();
//     dialogRef.afterClosed().subscribe(result => {
//       if (result) {
//         this.searchItemDispatchs();
//       }
//     });
//   }

  editItemDispatch(item: ItemDispatchSummary) {
    let dialogRef = this.itemDispatchNavigator.editItemDispatch(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  adddItemDispatchReturn(item: ItemDispatchSummary) {
    let dialogRef = this.itemReturnNavigator.addItemDispatchReturn(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItemDispatchs();
      }

    });
  }

  deleteItemDispatch(item: ItemDispatchSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("item_dispatch");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.itemDispatchPersist.deleteItemDispatch(item.id).subscribe(response => {
          this.tcNotification.success("item_dispatch deleted");
          this.searchItemDispatchs();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
    ngOnDestroy():void{
      this.itemDispatchPersist.start_date.setValue('',{emitEvent:false});
      this.itemDispatchPersist.end_date.setValue('',{emitEvent:false});
      // this.itemDispatchPersist.dispatch_status = dispatch_status.dispatched
    }
}