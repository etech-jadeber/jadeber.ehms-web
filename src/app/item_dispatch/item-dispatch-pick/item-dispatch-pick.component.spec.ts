import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDispatchPickComponent } from './item-dispatch-pick.component';

describe('ItemDispatchPickComponent', () => {
  let component: ItemDispatchPickComponent;
  let fixture: ComponentFixture<ItemDispatchPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDispatchPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDispatchPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
