import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemDispatchSummary, ItemDispatchSummaryPartialList } from '../item_dispatch.model';
import { ItemDispatchPersist } from '../item_dispatch.persist';
import { ItemDispatchNavigator } from '../item_dispatch.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-item_dispatch-pick',
  templateUrl: './item-dispatch-pick.component.html',
  styleUrls: ['./item-dispatch-pick.component.scss']
})export class ItemDispatchPickComponent implements OnInit {
  itemDispatchsData: ItemDispatchSummary[] = [];
  itemDispatchsTotalCount: number = 0;
  itemDispatchSelectAll:boolean = false;
  itemDispatchSelection: ItemDispatchSummary[] = [];

 itemDispatchsDisplayedColumns: string[] = ["select", ,"prescription_request_id","transfer_id","quantity" ];
  itemDispatchSearchTextBox: FormControl = new FormControl();
  itemDispatchIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemDispatchsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemDispatchsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemDispatchPersist: ItemDispatchPersist,
                public itemDispatchNavigator: ItemDispatchNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ItemDispatchPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("item_dispatchs");
       this.itemDispatchSearchTextBox.setValue(itemDispatchPersist.itemDispatchSearchText);
      //delay subsequent keyup events
      this.itemDispatchSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemDispatchPersist.itemDispatchSearchText = value;
        this.searchItem_dispatchs();
      });
    } ngOnInit() {
   
      this.itemDispatchsSort.sortChange.subscribe(() => {
        this.itemDispatchsPaginator.pageIndex = 0;
        this.searchItem_dispatchs(true);
      });

      this.itemDispatchsPaginator.page.subscribe(() => {
        this.searchItem_dispatchs(true);
      });
      //start by loading items
      this.searchItem_dispatchs();
    }

  searchItem_dispatchs(isPagination:boolean = false): void {


    let paginator = this.itemDispatchsPaginator;
    let sorter = this.itemDispatchsSort;

    this.itemDispatchIsLoading = true;
    this.itemDispatchSelection = [];

    this.itemDispatchPersist.searchItemDispatch(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemDispatchSummaryPartialList) => {
      this.itemDispatchsData = partialList.data;
      if (partialList.total != -1) {
        this.itemDispatchsTotalCount = partialList.total;
      }
      this.itemDispatchIsLoading = false;
    }, error => {
      this.itemDispatchIsLoading = false;
    });

  }
  
  markOneItem(item: ItemDispatchSummary) {
    if(!this.tcUtilsArray.containsId(this.itemDispatchSelection,item.id)){
          this.itemDispatchSelection = [];
          this.itemDispatchSelection.push(item);
        }
        else{
          this.itemDispatchSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemDispatchSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }