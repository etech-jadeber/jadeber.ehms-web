import {TCId} from "../tc/models";export class ItemDispatchSummary extends TCId {
    request_id:string;
    request_type: number;
    target_type:number;
    target_id:string;
    quantity:number;
    batch_no:string;
    item_id: string;
    dispatch_date:number;
    dispatch_status: number;
    dispatch_to: string;
    dispatch_by: string;
    item_name:string;
    remaining_of_transfer:number;
    price:number;
    total_price: number;
    response_date: number;
    disabled: boolean;
    }
    export class ItemDispatchSummaryPartialList {
      data: ItemDispatchSummary[];
      total: number;
    }
    export class ItemDispatchDetail extends ItemDispatchSummary {
    }
    
    export class ItemDispatchDashboard {
      total: number;
    }