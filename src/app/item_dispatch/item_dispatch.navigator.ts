import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ItemDispatchEditComponent} from "./item-dispatch-edit/item-dispatch-edit.component";
import {ItemDispatchPickComponent} from "./item-dispatch-pick/item-dispatch-pick.component";
import { PrescriptionRequestSummary } from "../prescription_request/prescription_request.model";
import { PrescriptionRequestStatus } from "../app.enums";
import { ItemDispatchDetail } from "./item_dispatch.model";


@Injectable({
  providedIn: 'root'
})

export class ItemDispatchNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  itemDispatchsUrl(): string {
    return "/item_dispatchs";
  }

  itemDispatchUrl(id: string): string {
    return "/item_dispatchs/" + id;
  }

  viewItemDispatchs(): void {
    this.router.navigateByUrl(this.itemDispatchsUrl());
  }

  viewItemDispatch(id: string): void {
    this.router.navigateByUrl(this.itemDispatchUrl(id));
  }

  editItemDispatch(id: string): MatDialogRef<ItemDispatchEditComponent> {
    const dialogRef = this.dialog.open(ItemDispatchEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemDispatch(item:any | any[], request_status: number = PrescriptionRequestStatus.accept,mode:string = TCModalModes.NEW,list: ItemDispatchDetail[] = []): MatDialogRef<ItemDispatchEditComponent> {
  const dialogRef = this.dialog.open(ItemDispatchEditComponent, {
          data: {id:item, status: request_status , mode:mode,list :list},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickItemDispatchs(selectOne: boolean=false): MatDialogRef<ItemDispatchPickComponent> {
      const dialogRef = this.dialog.open(ItemDispatchPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}