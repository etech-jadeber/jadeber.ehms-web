import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {ItemDispatchDashboard, ItemDispatchDetail, ItemDispatchSummaryPartialList} from "./item_dispatch.model";
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from "@angular/forms";
import { dispatch_status } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class ItemDispatchPersist {
  prescription_request_id: string;
  categoryId: string;
  category: number;
  start_date: FormControl = new FormControl({disabled: true, value: ""});
  end_date: FormControl = new FormControl({disabled: true, value: ""});
  dispatch_status: number;
  dispatchStatus: TCEnum[] = [
    new TCEnum(dispatch_status.accepted, 'Accepted'),
    new TCEnum(dispatch_status.dispatched, 'Dispatched'),
    new TCEnum(dispatch_status.cancelled, 'Cancelled'),
  ];


 itemDispatchSearchText: string = "";
 store_name: string;
store_id: string;
constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemDispatchSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchItemDispatch(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ItemDispatchSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_dispatch", this.itemDispatchSearchText, pageSize, pageIndex, sort, order);
    if (this.prescription_request_id){
      url =TCUtilsString.appendUrlParameter(url, "request_id", this.prescription_request_id);
    }
    if (this.start_date.value){
      url = TCUtilsString.appendUrlParameter(url, "start_date", (new Date(this.start_date.value).getTime()/1000).toString())
    }

    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if (this.end_date.value){
      url = TCUtilsString.appendUrlParameter(url, "end_date", (new Date(this.end_date.value).getTime()/1000).toString())
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if(this.dispatch_status){
      url = TCUtilsString.appendUrlParameter(url, "dispatch_status", this.dispatch_status.toString())
    }    
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    return this.http.get<ItemDispatchSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_dispatch/do", new TCDoParam("download_all", this.filters()));
  }

  itemDispatchDashboard(): Observable<ItemDispatchDashboard> {
    return this.http.get<ItemDispatchDashboard>(environment.tcApiBaseUri + "item_dispatch/dashboard");
  }

  getItemDispatch(id: string): Observable<ItemDispatchDetail> {
    return this.http.get<ItemDispatchDetail>(environment.tcApiBaseUri + "item_dispatch/" + id);
  } addItemDispatch(item: ItemDispatchDetail[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_dispatch/", {"data":item});
  }

  addItemRequestDispatch(item: ItemDispatchDetail[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_dispatch_to_employee/", {"data":item});
  }

  updateItemDispatch(item: ItemDispatchDetail): Observable<ItemDispatchDetail> {
    return this.http.patch<ItemDispatchDetail>(environment.tcApiBaseUri + "item_dispatch/" + item.id, item);
  }

  deleteItemDispatch(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "item_dispatch/" + id);
  }
 itemDispatchsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_dispatch/do", new TCDoParam(method, payload));
  }

  itemDispatchDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_dispatch/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "item_dispatch/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "item_dispatch/" + id + "/do", new TCDoParam("print", {}));
  }


}