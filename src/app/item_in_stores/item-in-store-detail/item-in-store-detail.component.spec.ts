import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Item_In_StoreDetailComponent } from './item-in-store-detail.component';

describe('Item_In_StoreDetailComponent', () => {
  let component: Item_In_StoreDetailComponent;
  let fixture: ComponentFixture<Item_In_StoreDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Item_In_StoreDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Item_In_StoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
