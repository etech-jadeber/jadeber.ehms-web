import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Item_In_StoreDetail} from "../item_in_store.model";
import {Item_In_StorePersist} from "../item_in_store.persist";
import {Item_In_StoreNavigator} from "../item_in_store.navigator";

export enum Item_In_StoreTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-item_in_store-detail',
  templateUrl: 'item-in-store-detail.component.html',
  styleUrls: ['item-in-store-detail.component.css']
})
export class Item_In_StoreDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  item_in_storeLoading: boolean = false;

  Item_In_StoreTabs: typeof Item_In_StoreTabs = Item_In_StoreTabs;
  activeTab: Item_In_StoreTabs = Item_In_StoreTabs.overview;
  //basics
  item_in_storeDetail: Item_In_StoreDetail;
  isMain: boolean= false;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public jobPersist: JobPersist,
              public appTranslation: AppTranslation,
              public item_in_storeNavigator: Item_In_StoreNavigator,
              public item_in_storePersist: Item_In_StorePersist) {
    this.tcAuthorization.requireRead("item_in_stores");
    this.item_in_storeDetail = new Item_In_StoreDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("item_in_stores");
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      console.log(this.route.snapshot.queryParamMap)
      this.isMain = this.route.snapshot.queryParamMap.get('isMain') == 'true';

      this.loadDetails(id);
    });
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    // this.paramsSubscription = this.route.params.subscribe(param => {
    //   let id = this.route.snapshot.paramMap.get('id');
    //   this.loadDetails(id);
    // });
  }

  loadDetails(id: string): void {
    this.item_in_storeDetail.id = id;
    // this.item_in_storeLoading = true;
    // this.item_in_storePersist.getItem_In_Store(id).subscribe(item_in_storeDetail => {
    //   this.item_in_storeDetail = item_in_storeDetail;
    //   this.item_in_storeLoading = false;
    // }, error => {
    //   console.error(error);
    //   this.item_in_storeLoading = false;
    // });
  }

  reload() {
    this.loadDetails(this.item_in_storeDetail.id);
  }

  editItem_In_Store(): void {
    let modalRef = this.item_in_storeNavigator.editItem_In_Store(this.item_in_storeDetail.id);
    modalRef.afterClosed().subscribe(modifiedItem_In_StoreDetail => {
      TCUtilsAngular.assign(this.item_in_storeDetail, modifiedItem_In_StoreDetail);
    }, error => {
      console.error(error);
    });
  }

  printItem_In_Store(): void {
    this.item_in_storePersist.print(this.item_in_storeDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print item_in_store", true);
    });
  }

  back(): void {
    this.location.back();
  }

}
