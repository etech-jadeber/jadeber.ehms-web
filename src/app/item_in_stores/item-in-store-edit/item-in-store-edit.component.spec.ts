import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemInStoreEditComponent } from './item-in-store-edit.component';

describe('ItemInStoreEditComponent', () => {
  let component: ItemInStoreEditComponent;
  let fixture: ComponentFixture<ItemInStoreEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInStoreEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInStoreEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
