import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Item_In_StoreDetail} from "../item_in_store.model";
import {Item_In_StorePersist} from "../item_in_store.persist";
import {StoreSummary} from "../../stores/store.model";
import {StoreNavigator} from "../../stores/store.navigator";
import {ItemSummary} from "../../items/item.model";
import {ItemNavigator} from "../../items/item.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { item_type } from 'src/app/app.enums';


@Component({
  selector: 'app-item_in_store-edit',
  templateUrl: 'item-in-store-edit.component.html',
  styleUrls: ['item-in-store-edit.component.css'],
})
export class Item_In_StoreEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  item_in_storeDetail: Item_In_StoreDetail;
  storeName: string = '';
  itemName: string = '';
  expire_date: Date = new Date();
  registered_date: Date = new Date();
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<Item_In_StoreEditComponent>,
    public persist: Item_In_StorePersist,
    public storeNavigator: StoreNavigator,
    public itemNavigator: ItemNavigator,
    public tCUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('item_in_stores');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        this.appTranslation.getText('inventory', 'items_in_store');
      this.item_in_storeDetail = new Item_In_StoreDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('item_in_stores');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        this.appTranslation.getText('inventory', 'items_in_store');
      this.isLoadingResults = true;
      this.persist.getItem_In_Store(this.idMode.id).subscribe(
        (item_in_storeDetail) => {
          this.item_in_storeDetail = item_in_storeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.item_in_storeDetail.expire_date =
      this.tCUtilsDate.toTimeStamp(new Date(this.expire_date));

        this.item_in_storeDetail.registered_date =
          this.tCUtilsDate.toTimeStamp(new Date(this.registered_date));

    console.log("item in store detail is ", this.item_in_storeDetail)
 
    this.isLoadingResults = true;
    this.item_in_storeDetail.gross_quantity = this.item_in_storeDetail.net_quantity;
    this.item_in_storeDetail.item_type=parseInt(this.idMode.id);
    this.persist.addItem_In_Store(this.item_in_storeDetail).subscribe(value => {
      this.tcNotification.success("Item_In_Store added");
      this.item_in_storeDetail.id = value.id;
      this.dialogRef.close(this.item_in_storeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateItem_In_Store(this.item_in_storeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Item_In_Store updated');
        this.dialogRef.close(this.item_in_storeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.item_in_storeDetail == null) {
      return false;
    }

    if (
      this.item_in_storeDetail.remark == null ||
      this.item_in_storeDetail.remark == ''
    ) {
      return false;
    }

    return true;
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.storeName = result[0].name;
        this.item_in_storeDetail.store_id = result[0].id;
      }
    });
  }

  searchItem() {
    let dialogRef = this.itemNavigator.pickItems(true,parseInt(this.idMode.id));
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.itemName = result[0].name;
        this.item_in_storeDetail.item_id = result[0].id;
      }
    });
  }
}
