import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemInStoreListComponent } from './item-in-store-list.component';

describe('ItemInStoreListComponent', () => {
  let component: ItemInStoreListComponent;
  let fixture: ComponentFixture<ItemInStoreListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInStoreListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInStoreListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
