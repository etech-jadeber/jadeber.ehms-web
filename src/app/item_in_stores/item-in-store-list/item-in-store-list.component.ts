import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Item_In_StoreNavigator} from "../item_in_store.navigator";
import {Item_In_StoreDetail, Item_In_StoreSummary, Item_In_StoreSummaryPartialList} from "../item_in_store.model";
import {Item_In_StorePersist} from "../item_in_store.persist";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {StoreDetail, StoreSummary} from "../../stores/store.model";
import {ItemDetail} from "../../items/item.model";
import {ItemNavigator} from "../../items/item.navigator";
import {StoreNavigator} from "../../stores/store.navigator";
import { EquipmentDataNavigator } from 'src/app/equipment_data/equipment_data.navigator';
import { item_type } from 'src/app/app.enums';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
export enum PaginatorIndexes {
  drug,
  consumable,
  equipment,
}

@Component({
  selector: 'app-item_in_store-list',
  templateUrl: 'item-in-store-list.component.html',
  styleUrls: ['item-in-store-list.component.css'],
})
export class Item_In_StoreListComponent implements OnInit {
  drugsData: Item_In_StoreSummary[] = [];
  consumablesData: Item_In_StoreSummary[] = [];
  drugsTotalCount: number = 0;
  consumablesTotalCount: number = 0;
  drugsSelectAll: boolean = false;
  consumablesSelectAll: boolean = false;
  drugsSelection: Item_In_StoreSummary[] = [];
  consumablesSelection: Item_In_StoreSummary[] = [];

  drugsDisplayedColumns: string[] = [
    'select',
    'item_id',
    'item_description',
    'max',
    'min',
    'in',
    'out',
    'remaining',
  ];

  consumablesDisplayedColumns: string[] = [
    'select',
    'action',
    'item_id',
    'store_id',
    'batch_no',
    'item_description',
    'buying_price',
    'selling_price',
    'gross_quantity',
    'net_quantity',
    'registered_date',
    'expire_date',
  ];
  drugsSearchTextBox: FormControl = new FormControl();
  drugsIsLoading: boolean = false;
  consumablesSearchTextBox: FormControl = new FormControl();
  consumablesIsLoading: boolean = false;
  PaginatorIndexes: typeof PaginatorIndexes = PaginatorIndexes;
  activeTab: PaginatorIndexes = PaginatorIndexes.drug;
  selectedTabIndex: number;
  @ViewChild(MatPaginator, {static: true}) item_in_storePaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) item_in_storeSorter: MatSort;
  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public itemNavigator: ItemNavigator,
    public storeNavigator: StoreNavigator,
    public appTranslation: AppTranslation,
    public item_in_storePersist: Item_In_StorePersist,
    public item_in_storeNavigator: Item_In_StoreNavigator,
    public formEncounterPersist: Form_EncounterPersist,
    private itemPersist: ItemPersist,
    private storePersist: StorePersist,
    public jobPersist: JobPersist,
    public equipmentDataNavigator: EquipmentDataNavigator
  ) {
    this.tcAuthorization.requireRead("item_in_stores");
    this.drugsSearchTextBox.setValue(
      item_in_storePersist.item_in_storeSearchText
    );
    this.consumablesSearchTextBox.setValue(
      item_in_storePersist.item_in_storeSearchText
    );
    //delay subsequent keyup events
    this.drugsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.item_in_storePersist.item_in_storeSearchText = value;
        this.searchItem_In_Stores();
      });
      this.consumablesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.item_in_storePersist.item_in_storeSearchText = value;
        this.searchItem_In_Stores();
      });
  }

  ngOnInit() {
    this.activeTab=PaginatorIndexes.drug
  }
  ngAfterViewInit(){

    this.item_in_storeSorter.sortChange.subscribe(() => {
      this.item_in_storePaginator.pageIndex = 0;
      this.searchDrugs(true);
    });
    this.item_in_storePaginator.page.subscribe(() => {
      this.searchDrugs(true);
    });
    // this.sorters.toArray()[PaginatorIndexes.consumable].sortChange.subscribe(() => {
    //   this.paginators.toArray()[PaginatorIndexes.consumable].pageIndex = 0;
    //   this.searchConsumables(true);
    // });
    // consumable paginator
    // this.paginators .toArray()[PaginatorIndexes.consumable].page.subscribe(() => {
    //   this.searchConsumables(true);
    // });
    this.searchDrugs();
  }
 searchDrugs(pag:boolean=false){
  this.searchItem_In_Stores(pag);
 }
 searchConsumables(pag:boolean=false){
  this.item_in_storePersist.item_type=item_type.consumable;
  this.searchItem_In_Stores(pag);
 }
 onTabChanged(event:any){
  if(event.index==0){
   this.searchDrugs();
  }
  else if(event.index==1){
    this.searchConsumables();
  }
 }
  searchItem_In_Stores(isPagination: boolean = false): void {
    // let index=this.item_in_storePersist.item_type==item_type.drug ? PaginatorIndexes.drug:PaginatorIndexes.consumable;
    let paginator = this.item_in_storePaginator;
    let sorter = this.item_in_storeSorter; 
    // this.item_in_storePersist.item_type==item_type.drug ?this.drugsIsLoading = true:this.consumablesIsLoading=true;
    // this.item_in_storePersist.item_type==item_type.drug ?this.drugsSelection = []:this.consumablesSelection=[];
    this.drugsIsLoading = true;
    this.item_in_storePersist
      .searchItem_In_Store(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: Item_In_StoreSummaryPartialList) => {
          this.drugsData = partialList.data
          if (partialList.total != -1) {
          //   if(this.item_in_storePersist.item_type==item_type.consumable){
          //     this.consumablesTotalCount = partialList.total;
          //     this.consumablesData = partialList.data;
          //   }
          // else if(this.item_in_storePersist.item_type==item_type.drug){
          //     this.drugsTotalCount = partialList.total;
          //     this.drugsData = partialList.data;
          // }
          this.drugsTotalCount = partialList.total
        }

          // this.item_in_storePersist.item_type==item_type.drug ?this.drugsIsLoading = false:this.consumablesIsLoading=false;
          this.drugsIsLoading = false;
        },
        (error) => {
          // this.item_in_storePersist.item_type==item_type.drug ?this.drugsIsLoading = false:this.consumablesIsLoading=false;
          this.drugsIsLoading = false;
        }
      );
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.item_in_storePersist.store_name = result[0].name;
        this.item_in_storePersist.store_id = result[0].id;
        this.searchItem_In_Stores();
      }
    });
  }

  downloadItem_In_Stores(): void {
if(this.item_in_storePersist.item_type==item_type.drug){
    if (this.drugsSelectAll) {
      this.item_in_storePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download item_in_stores',
          true
        );
      });
    } else {
      this.item_in_storePersist
        .download(this.tcUtilsArray.idsList(this.drugsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download item_in_stores',
            true
          );
        });
    }
  }
  else{
    if (this.consumablesSelectAll) {
      this.item_in_storePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download item_in_stores',
          true
        );
      });
    } else {
      this.item_in_storePersist
        .download(this.tcUtilsArray.idsList(this.consumablesSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download item_in_stores',
            true
          );
        });
    }
  }
  }


  back(): void {
    this.location.back();
  }
  isActiveTab(routeStart: string): boolean {
    return this.router.url.startsWith(routeStart);
  }
}
