import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemInStorePickComponent } from './item-in-store-pick.component';

describe('ItemInStorePickComponent', () => {
  let component: ItemInStorePickComponent;
  let fixture: ComponentFixture<ItemInStorePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemInStorePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemInStorePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
