import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Item_In_StoreSummary, Item_In_StoreSummaryPartialList} from "../item_in_store.model";
import {Item_In_StorePersist} from "../item_in_store.persist";
import { ItemPersist } from 'src/app/items/item.persist';
import { StorePersist } from 'src/app/stores/store.persist';
import { ThisReceiver } from '@angular/compiler';
import { ItemSummary } from 'src/app/items/item.model';
import { StoreSummary } from 'src/app/stores/store.model';


@Component({
  selector: 'app-item_in_store-pick',
  templateUrl: 'item-in-store-pick.component.html',
  styleUrls: ['item-in-store-pick.component.css']
})
export class Item_In_StorePickComponent implements OnInit {

  item_in_storesData: Item_In_StoreSummary[] = [];
  item_in_storesTotalCount: number = 0;
  item_in_storesSelection: Item_In_StoreSummary[] = [];
  item_in_storesDisplayedColumns: string[] = ["select", 'item_id', 'store_id', 'quantity', 'remark'];

  item_in_storesSearchTextBox: FormControl = new FormControl();
  item_in_storesIsLoading: boolean = false;
  items: ItemSummary[] = [];
  stores: StoreSummary[] = [];

  @ViewChild(MatPaginator, {static: true}) item_in_storesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) item_in_storesSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public item_in_storePersist: Item_In_StorePersist,
              public dialogRef: MatDialogRef<Item_In_StorePickComponent>,
              private itemPersist: ItemPersist,
              private storePersist: StorePersist,
              @Inject(MAT_DIALOG_DATA) public selectOne: any
  ) {
    this.tcAuthorization.requireRead("item_in_stores");
    this.item_in_storesSearchTextBox.setValue(item_in_storePersist.item_in_storeSearchText);
    //delay subsequent keyup events
    this.item_in_storesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.item_in_storePersist.item_in_storeSearchText = value;
      this.searchItem_In_Stores();
    });
  }

  ngOnInit() {
    this.searchItem();
    this.searchStore();
    this.item_in_storesSort.sortChange.subscribe(() => {
      this.item_in_storesPaginator.pageIndex = 0;
      this.searchItem_In_Stores();
    });

    this.item_in_storesPaginator.page.subscribe(() => {
      this.searchItem_In_Stores();
    });

    //set initial picker list to 5
    this.item_in_storesPaginator.pageSize = 5;

    //start by loading items
    this.searchItem_In_Stores();
  }

  searchItem_In_Stores(): void {
    this.item_in_storesIsLoading = true;
    this.item_in_storesSelection = [];
    if(typeof this.selectOne != "boolean"){
      this.item_in_storePersist.item_in_storesDo("items_to_dispatch", this.selectOne.prescribedDrug).subscribe((result: Item_In_StoreSummary[]) => {
      this.item_in_storesData = result;
      this.item_in_storesTotalCount = result.length;
      this.item_in_storesIsLoading = false;
      })
    } else {
    this.item_in_storePersist.searchItem_In_Store(this.item_in_storesPaginator.pageSize,
      this.item_in_storesPaginator.pageIndex,
      this.item_in_storesSort.active,
      this.item_in_storesSort.direction).subscribe((partialList: Item_In_StoreSummaryPartialList) => {
      this.item_in_storesData = partialList.data;
      if (partialList.total != -1) {
        this.item_in_storesTotalCount = partialList.total;
      }
      this.item_in_storesIsLoading = false;
    }, error => {
      this.item_in_storesIsLoading = false;
    });
  }

  }

  markOneItem(item: Item_In_StoreSummary) {
    if (!this.tcUtilsArray.containsId(this.item_in_storesSelection, item.id)) {
      this.item_in_storesSelection = [];
      this.item_in_storesSelection.push(item);
    } else {
      this.item_in_storesSelection = [];
    }
  }

  searchItem(): void {
    this.itemPersist.searchItem(50, 0, "", "").subscribe(result => {
      this.items = result.data
    })
  }

  searchStore():void {
    this.storePersist.searchStore(50, 0, "", "").subscribe(result => {
      this.stores = result.data;
    })
  }

  returnSelected(): void {
    this.dialogRef.close(this.item_in_storesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
