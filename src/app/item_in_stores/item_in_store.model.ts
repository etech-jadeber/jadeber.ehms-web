import {TCId} from "../tc/models";

export class Item_In_StoreSummary extends TCId {
  id : string;
  batch_no: number;
  item_id : string;
  store_id : string;
  quantity : number;
  remark : string;
  item_type : number;
  unit_price: number;
}

export class Item_In_StoreSummaryPartialList {
  data: Item_In_StoreSummary[];
  total: number;
}

export class Item_In_StoreDetail extends Item_In_StoreSummary {
  item_id : string;
  store_id : string;
  quantity : number;
  remark : string;

  expire_date: number;
  // batch_no: number;
  item_description: string;
  registered_date: number;
  buying_price: number;
  selling_price: number;
  gross_quantity: number;
  net_quantity: number;
}

export class Item_In_StoreDashboard {
  total: number;
}

export class Bin_cardSummary extends TCId {
entry_type : number;
item_type : number;
item_id : string;
quantity : number;
store_id : string;
store : string;
batch : number;
expire_date : number;
description : string;
registration_date : number;
balance: number;
in:number;
out:number;
}

export class Bin_cardSummaryPartialList {
  data: Bin_cardSummary[];
  total: number;
  balance:number;
}

export class Bin_cardDetail extends Bin_cardSummary {
  entry_type : number;
item_type : number;
item_id : string;
quantity : number;
store_id : string;
batch : number;
expire_date : number;
description : string;
registration_date : number;
}

export class ItemLedgerEntryPartial{
  entry_type : number;
  batch : number;
  description : string;
  expire_date : number;
}

export class Bin_cardDashboard {
  total: number;
}
