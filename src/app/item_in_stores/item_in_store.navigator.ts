import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import {Item_In_StoreEditComponent} from "./item-in-store-edit/item-in-store-edit.component";
import {Item_In_StorePickComponent} from "./item-in-store-pick/item-in-store-pick.component";
import { PrescriptionsDetail } from "../form_encounters/form_encounter.model";

@Injectable({
  providedIn: 'root'
})

export class Item_In_StoreNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  item_in_storesUrl(): string {
    return "/item_in_stores";
  }

  item_in_storeUrl(id: string): string {
    return "/item_in_stores/" + id;
  }

  item_in_storesUrl_t(id: string, is_main:boolean = false): string {
    return "/item_in_stores/" + id + '?isMain='+is_main.toString();
  }
  viewItem_In_Stores(): void {
    this.router.navigateByUrl(this.item_in_storesUrl());
  }

  viewItem_In_Store(id: string): void {
    this.router.navigateByUrl(this.item_in_storeUrl(id));
  }

  editItem_In_Store(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_In_StoreEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItem_In_Store(item_type:number=-1): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_In_StoreEditComponent, {
      data: new TCIdMode(item_type.toString(), TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickItem_In_Stores(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_In_StorePickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  drugs(prescribedDrug:PrescriptionsDetail): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_In_StorePickComponent, {
      data: {selectOne: false, prescribedDrug},
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
