import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, timestamp} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Bin_cardSummaryPartialList, Item_In_StoreDashboard, Item_In_StoreDetail, Item_In_StoreSummaryPartialList} from "./item_in_store.model";
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class Item_In_StorePersist {

  item_type:number;
  item_in_storeSearchText: string = "";
  bin_cardSearchText: string = "";
  store_id: string;
  store_name: string;
  stock_out:boolean = false;

  constructor(private http: HttpClient) {
  }

  searchItem_In_Store(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Item_In_StoreSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_in_stores", this.item_in_storeSearchText, pageSize, pageIndex, sort, order);
    if(this.item_type){
      url=TCUtilsString.appendUrlParameter(url, "item_type",this.item_type.toString());
    }
          
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }

    url = TCUtilsString.appendUrlParameter(url, "stock_out", `${this.stock_out}`)
    return this.http.get<Item_In_StoreSummaryPartialList>(url);

  }

  binCardSearch(item_id:string, pageSize: number, pageIndex: number, sort: string, order: string, from_date=null, to_date =null): Observable<Bin_cardSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("item_in_stores/do", this.bin_cardSearchText, pageSize, pageIndex, sort, order);
    if(this.store_id)
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    url = TCUtilsString.appendUrlParameter(url, "item_id", item_id)
    if (from_date){
      url = TCUtilsString.appendUrlParameter(url, "from_date", (new Date(from_date).valueOf() / 1000).toString());
    }
    if (to_date){
      url = TCUtilsString.appendUrlParameter(url, "to_date", (new Date(to_date).valueOf() / 1000).toString());
    }
    return this.http.post<Bin_cardSummaryPartialList>(url, new TCDoParam("bin_card", {}));
  }
  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.item_in_storeSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_in_stores/do", new TCDoParam("download_all", this.filters()));
  }

  item_in_storeDashboard(): Observable<Item_In_StoreDashboard> {
    return this.http.get<Item_In_StoreDashboard>(environment.tcApiBaseUri + "item_in_stores/dashboard");
  }

  getItem_In_Store(id: string): Observable<Item_In_StoreDetail> {
    return this.http.get<Item_In_StoreDetail>(environment.tcApiBaseUri + "item_in_stores/" + id);
  }

  addItem_In_Store(item: Item_In_StoreDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_in_stores/", item);
  }

  updateItem_In_Store(item: Item_In_StoreDetail): Observable<Item_In_StoreDetail> {
    return this.http.patch<Item_In_StoreDetail>(environment.tcApiBaseUri + "item_in_stores/" + item.id, item);
  }

  deleteItem_In_Store(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "item_in_stores/" + id);
  }

  item_in_storesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_in_stores/do", new TCDoParam(method, payload));
  }

  item_in_storeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_in_stores/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_in_stores/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_in_stores/" + id + "/do", new TCDoParam("print", {}));
  }
  expiration(timeStamp: number): string{
    let tcUtilsDate : TCUtilsDate = new TCUtilsDate();
    timeStamp = tcUtilsDate.toTimeStamp(new Date(tcUtilsDate.toDate(timeStamp).getFullYear(),tcUtilsDate.toDate(timeStamp).getMonth(),tcUtilsDate.toDate(timeStamp).getDate()));
    return timeStamp > tcUtilsDate.toTimeStamp(new Date(new Date().getFullYear(),new Date().getMonth() + 6,new Date().getDate())) ? '' :(timeStamp > tcUtilsDate.toTimeStamp(new Date(new Date().getFullYear(),new Date().getMonth() + 1,new Date().getDate())) ? 'notice-expired-row' : (timeStamp > tcUtilsDate.toTimeStamp(new Date()) ? 'nearly-expired-row':'expired-row'))
  } 

}
