import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemLedgerEntryDetailComponent } from './item-ledger-entry-detail.component';

describe('ItemLedgerEntryDetailComponent', () => {
  let component: ItemLedgerEntryDetailComponent;
  let fixture: ComponentFixture<ItemLedgerEntryDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLedgerEntryDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLedgerEntryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
