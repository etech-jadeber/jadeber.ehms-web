import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Item_Ledger_EntryDetail} from "../item_ledger_entry.model";
import {Item_Ledger_EntryPersist} from "../item_ledger_entry.persist";
import {Item_Ledger_EntryNavigator} from "../item_ledger_entry.navigator";
import { ItemSummary, ItemSummaryPartialList } from 'src/app/items/item.model';
import { ItemPersist } from 'src/app/items/item.persist';
import { StoreSummary, StoreSummaryPartialList } from 'src/app/stores/store.model';
import { StorePersist } from 'src/app/stores/store.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { StoreNavigator } from 'src/app/stores/store.navigator';


export enum PaginatorIndexes {

}

export enum ItemLedgerentryTabs {
  overview
}


@Component({
  selector: 'app-item-ledger-entry-detail',
  templateUrl: './item-ledger-entry-detail.component.html',
  styleUrls: ['./item-ledger-entry-detail.component.css']
})
export class Item_Ledger_EntryDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  item_ledger_entryLoading:boolean = false;
  //basics
  item_ledger_entryDetail: Item_Ledger_EntryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  items: ItemSummary[] = [];
  stores: StoreSummary[] = [];

  itemLedgerentryTabs: typeof ItemLedgerentryTabs = ItemLedgerentryTabs ;
  activeTab: ItemLedgerentryTabs = ItemLedgerentryTabs.overview;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public item_ledger_entryNavigator: Item_Ledger_EntryNavigator,
              public  item_ledger_entryPersist: Item_Ledger_EntryPersist,
              public itemsPersist: ItemPersist,
              public storePersist: StorePersist,
              public tcUtilsDate: TCUtilsDate,
              public itemNavigator: ItemNavigator,
              public storeNavigator: StoreNavigator,
              ) {
    this.tcAuthorization.requireRead("item_ledger_entrys");
    this.item_ledger_entryDetail = new Item_Ledger_EntryDetail();
  }

  ngOnInit() {
    this.loadStores();
    this.loadItems();
    this.tcAuthorization.requireRead("item_ledger_entrys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.item_ledger_entryLoading = true;
    this.item_ledger_entryPersist.getItem_Ledger_Entry(id).subscribe(item_ledger_entryDetail => {
          this.item_ledger_entryDetail = item_ledger_entryDetail;
          this.item_ledger_entryLoading = false;
        }, error => {
          console.error(error);
          this.item_ledger_entryLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.item_ledger_entryDetail.id);
  }

  editItem_Ledger_Entry(): void {
    let modalRef = this.item_ledger_entryNavigator.editItem_Ledger_Entry(this.item_ledger_entryDetail.id);
    modalRef.afterClosed().subscribe(modifiedItem_Ledger_EntryDetail => {
      TCUtilsAngular.assign(this.item_ledger_entryDetail, modifiedItem_Ledger_EntryDetail);
    }, error => {
      console.error(error);
    });
  }

   printItem_Ledger_Entry():void{
      this.item_ledger_entryPersist.print(this.item_ledger_entryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_ledger_entry", true);
      });
    }

    loadItems() {
      this.itemsPersist.itemsDo('get_all_items', '').subscribe((result) => {
        this.items = (result as ItemSummaryPartialList).data;
      });
    }

    loadStores() {
      this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
        this.stores = (result as StoreSummaryPartialList).data;
      });
    }

  back():void{
      this.location.back();
    }

}