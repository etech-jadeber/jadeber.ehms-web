import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemLedgerEntryEditComponent } from './item-ledger-entry-edit.component';

describe('ItemLedgerEntryEditComponent', () => {
  let component: ItemLedgerEntryEditComponent;
  let fixture: ComponentFixture<ItemLedgerEntryEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLedgerEntryEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLedgerEntryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
