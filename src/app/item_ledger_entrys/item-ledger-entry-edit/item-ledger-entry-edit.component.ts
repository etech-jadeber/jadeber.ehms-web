import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Item_Ledger_EntryDetail} from "../item_ledger_entry.model";
import {Item_Ledger_EntryPersist} from "../item_ledger_entry.persist";
import {TCUtilsDate} from "../../tc/utils-date";
import {StoreNavigator} from "../../stores/store.navigator";
import {ItemNavigator} from "../../items/item.navigator";
import {StoreSummary} from "../../stores/store.model";
import {ItemSummary} from "../../items/item.model";


@Component({
  selector: 'app-item_ledger_entry-edit',
  templateUrl: './item-ledger-entry-edit.component.html',
  styleUrls: ['./item-ledger-entry-edit.component.css']
})
export class Item_Ledger_EntryEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  item_ledger_entryDetail: Item_Ledger_EntryDetail;
  expire_date: Date;
  storeName: string;
  itemName: string;
  date_of_registration: Date;
  minDate: Date = new Date();

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public tcUtilsDate: TCUtilsDate,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<Item_Ledger_EntryEditComponent>,
              public persist: Item_Ledger_EntryPersist,
              public storeNavigator: StoreNavigator,
              public itemNavigator: ItemNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("item_ledger_entrys");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("inventory", "ledger_entry");
      this.item_ledger_entryDetail = new Item_Ledger_EntryDetail();
      this.item_ledger_entryDetail.item_type = -1;
      this.item_ledger_entryDetail.entry_type = -1;
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("item_ledger_entrys");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("inventory", "ledger_entry");
      this.isLoadingResults = true;
      this.persist.getItem_Ledger_Entry(this.idMode.id).subscribe(item_ledger_entryDetail => {
        this.item_ledger_entryDetail = item_ledger_entryDetail;
        this.transformDates(true);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.addItem_Ledger_Entry(this.item_ledger_entryDetail).subscribe(value => {
      this.tcNotification.success("Item_Ledger_Entry added");
      this.item_ledger_entryDetail.id = value.id;
      this.dialogRef.close(this.item_ledger_entryDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.updateItem_Ledger_Entry(this.item_ledger_entryDetail).subscribe(value => {
      this.tcNotification.success("Item_Ledger_Entry updated");
      this.dialogRef.close(this.item_ledger_entryDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    return true;
    if (this.item_ledger_entryDetail == null) {
      return false;
    }
    if (this.item_ledger_entryDetail.item_type == -1 || this.item_ledger_entryDetail.entry_type == -1) {
      return false;
    }

    if (this.item_ledger_entryDetail.description == null || this.item_ledger_entryDetail.description == "") {
      return false;
    }

    if(this.expire_date == null || this.date_of_registration==null){
      return false;
    }

    return true;
  }

  transformDates(todate: boolean) {
    if (todate) {
      this.expire_date = this.tcUtilsDate.toDate(this.item_ledger_entryDetail.expire_date);
      this.date_of_registration = this.tcUtilsDate.toDate(this.item_ledger_entryDetail.registration_date);
    } else {
      this.item_ledger_entryDetail.expire_date = this.tcUtilsDate.toTimeStamp(
        this.expire_date
      );
      this.item_ledger_entryDetail.registration_date = this.tcUtilsDate.toTimeStamp(
        this.date_of_registration
      );
    }
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.storeName = result[0].name;
        this.item_ledger_entryDetail.store_id = result[0].id;
      }
    });
  }

  searchItem() {
    let dialogRef = this.itemNavigator.pickItems(true);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.itemName = result[0].name;
        this.item_ledger_entryDetail.item_id = result[0].id;
      }
    });
  }



}
