import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemLedgerEntryListComponent } from './item-ledger-entry-list.component';

describe('ItemLedgerEntryListComponent', () => {
  let component: ItemLedgerEntryListComponent;
  let fixture: ComponentFixture<ItemLedgerEntryListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLedgerEntryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLedgerEntryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
