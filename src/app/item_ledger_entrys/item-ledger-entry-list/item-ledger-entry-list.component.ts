import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Item_Ledger_EntryPersist} from "../item_ledger_entry.persist";
import {Item_Ledger_EntryNavigator} from "../item_ledger_entry.navigator";
import {
  Item_Ledger_EntryDetail,
  Item_Ledger_EntrySummary,
  Item_Ledger_EntrySummaryPartialList
} from "../item_ledger_entry.model";
import {ItemDetail, ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {ItemNavigator} from "../../items/item.navigator";
import {StoreNavigator} from "../../stores/store.navigator";
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';


@Component({
  selector: 'app-item_ledger_entry-list',
  templateUrl: './item-ledger-entry-list.component.html',
  styleUrls: ['./item-ledger-entry-list.component.css']
})
export class Item_Ledger_EntryListComponent implements OnInit {

  item_ledger_entrysData: Item_Ledger_EntrySummary[] = [];
  item_ledger_entrysTotalCount: number = 0;
  item_ledger_entrysSelectAll: boolean = false;
  item_ledger_entrysSelection: Item_Ledger_EntrySummary[] = [];
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
  item_ledger_entrysDisplayedColumns: string[] = ["select", "name", "entry_type", "quantity", "from","to", "batch_no", "date"];

  // item_ledger_entrysDisplayedColumns: string[] = ["select","date", "name", "entry_type", "received","issued", "return_damage" , "from","to", "batch_no" ];

  item_ledger_entrysSearchTextBox: FormControl = new FormControl();
  item_ledger_entrysIsLoading: boolean = false;
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""}) 

  @ViewChild(MatPaginator, {static: true}) item_ledger_entrysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) item_ledger_entrysSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public itemNavigator: ItemNavigator,
              public storeNavigator: StoreNavigator,
              public appTranslation: AppTranslation,
              public item_ledger_entryPersist: Item_Ledger_EntryPersist,
              public item_ledger_entryNavigator: Item_Ledger_EntryNavigator,
              public jobPersist: JobPersist,
              public itemsPersist: ItemPersist,
              public storePersist: StorePersist,
              public categoryPersist: ItemCategoryPersist,
              public categoryNavigator: ItemCategoryNavigator,
              public tcUtilsString: TCUtilsString,
  ) {

    this.tcAuthorization.requireRead("item_ledger_entrys");
    this.item_ledger_entrysSearchTextBox.setValue(item_ledger_entryPersist.item_ledger_entrySearchText);
    //delay subsequent keyup events
    this.item_ledger_entrysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.item_ledger_entryPersist.item_ledger_entrySearchText = value;
      this.searchItem_Ledger_Entrys();
    });
  }

  ngOnInit() {

    this.item_ledger_entrysSort.sortChange.subscribe(() => {
      this.item_ledger_entrysPaginator.pageIndex = 0;
      this.searchItem_Ledger_Entrys(true);
    });

    this.item_ledger_entrysPaginator.page.subscribe(() => {
      this.searchItem_Ledger_Entrys(true);
    });
    this.from_date.valueChanges.pipe().subscribe(value => {
      this.searchItem_Ledger_Entrys();
    });

    this.to_date.valueChanges.pipe().subscribe(value => {
      this.searchItem_Ledger_Entrys();
    });
    //start by loading items
    this.searchItem_Ledger_Entrys();
    // this.loadItems();
  }

  searchItem_Ledger_Entrys(isPagination: boolean = false): void {

    let paginator = this.item_ledger_entrysPaginator;
    let sorter = this.item_ledger_entrysSort;

    this.item_ledger_entrysIsLoading = true;
    this.item_ledger_entrysSelection = [];

    this.item_ledger_entryPersist.searchItem_Ledger_Entry(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction, this.from_date.value, this.to_date.value).subscribe((partialList: Item_Ledger_EntrySummaryPartialList) => {
      this.item_ledger_entrysData = partialList.data;
      if (partialList.total != -1) {
        this.item_ledger_entrysTotalCount = partialList.total;
      }
      this.item_ledger_entrysIsLoading = false;
    }, error => {
      this.item_ledger_entrysIsLoading = false;
    });

  }

  downloadItem_Ledger_Entrys(): void {
    if (this.item_ledger_entrysSelectAll) {
      this.item_ledger_entryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_ledger_entrys", true);
      });
    } else {
      this.item_ledger_entryPersist.download(this.tcUtilsArray.idsList(this.item_ledger_entrysSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_ledger_entrys", true);
      });
    }
  }


  addItem_Ledger_Entry(): void {
    let dialogRef = this.item_ledger_entryNavigator.addItem_Ledger_Entry();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItem_Ledger_Entrys();
      }
    });
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.item_ledger_entryPersist.store_name = result[0].name;
        this.item_ledger_entryPersist.store_id = result[0].id;
        this.searchItem_Ledger_Entrys();
      }
    });
  }
 
  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.item_ledger_entryPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.item_ledger_entryPersist.categoryId = result[0].id;
      }
      this.searchItem_Ledger_Entrys()
    })
  }
  
  editItem_Ledger_Entry(item: Item_Ledger_EntrySummary) {
    let dialogRef = this.item_ledger_entryNavigator.editItem_Ledger_Entry(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchItem_Ledger_Entrys();
      }

    });
  }

  deleteItem_Ledger_Entry(item: Item_Ledger_EntrySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Item Ledger Entry");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.item_ledger_entryPersist.deleteItem_Ledger_Entry(item.id).subscribe(response => {
          this.tcNotification.success("Item_Ledger_Entry deleted");
          this.searchItem_Ledger_Entrys();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

  // loadItems() {
  //   this.itemsPersist.itemsDo('get_all_items', '').subscribe((result) => {
  //     this.items = (result as ItemSummaryPartialList).data;
  //   });
  // }


}
