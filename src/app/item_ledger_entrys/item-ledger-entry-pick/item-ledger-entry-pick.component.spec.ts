import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemLedgerEntryPickComponent } from './item-ledger-entry-pick.component';

describe('ItemLedgerEntryPickComponent', () => {
  let component: ItemLedgerEntryPickComponent;
  let fixture: ComponentFixture<ItemLedgerEntryPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemLedgerEntryPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemLedgerEntryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
