import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {
  Item_Ledger_EntryDetail,
  Item_Ledger_EntrySummary,
  Item_Ledger_EntrySummaryPartialList
} from "../item_ledger_entry.model";
import {Item_Ledger_EntryPersist} from "../item_ledger_entry.persist";
import {ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {ItemPersist} from "../../items/item.persist";


@Component({
  selector: 'app-item_ledger_entry-pick',
  templateUrl: 'item-ledger-entry-pick.component.html',
  styleUrls: ['item-ledger-entry-pick.component.css']
})
export class Item_Ledger_EntryPickComponent implements OnInit {

  item_ledger_entrysData: Item_Ledger_EntrySummary[] = [];
  items: ItemSummary[] = [];
  item_ledger_entrysTotalCount: number = 0;
  item_ledger_entrysSelection: Item_Ledger_EntrySummary[] = [];
  item_ledger_entrysDisplayedColumns: string[] = ["select", 'entry_type','item_id', 'item_type', 'quantity', 'batch', 'registration_date'];

  item_ledger_entrysSearchTextBox: FormControl = new FormControl();
  item_ledger_entrysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) item_ledger_entrysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) item_ledger_entrysSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public itemsPersist: ItemPersist,
              public item_ledger_entryPersist: Item_Ledger_EntryPersist,
              public dialogRef: MatDialogRef<Item_Ledger_EntryPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("item_ledger_entrys");
    this.item_ledger_entrysSearchTextBox.setValue(item_ledger_entryPersist.item_ledger_entrySearchText);
    //delay subsequent keyup events
    this.item_ledger_entrysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.item_ledger_entryPersist.item_ledger_entrySearchText = value;
      this.searchItem_Ledger_Entrys();
    });
  }

  ngOnInit() {

    this.item_ledger_entrysSort.sortChange.subscribe(() => {
      this.item_ledger_entrysPaginator.pageIndex = 0;
      this.searchItem_Ledger_Entrys();
    });

    this.item_ledger_entrysPaginator.page.subscribe(() => {
      this.searchItem_Ledger_Entrys();
    });

    //set initial picker list to 5
    this.item_ledger_entrysPaginator.pageSize = 5;

    //start by loading items
    this.searchItem_Ledger_Entrys();
    this.loadItems();
  }

  searchItem_Ledger_Entrys(): void {
    this.item_ledger_entrysIsLoading = true;
    this.item_ledger_entrysSelection = [];

    this.item_ledger_entryPersist.searchItem_Ledger_Entry(this.item_ledger_entrysPaginator.pageSize,
      this.item_ledger_entrysPaginator.pageIndex,
      this.item_ledger_entrysSort.active,
      this.item_ledger_entrysSort.direction).subscribe((partialList: Item_Ledger_EntrySummaryPartialList) => {
      this.item_ledger_entrysData = partialList.data;
      if (partialList.total != -1) {
        this.item_ledger_entrysTotalCount = partialList.total;
      }
      this.item_ledger_entrysIsLoading = false;
    }, error => {
      this.item_ledger_entrysIsLoading = false;
    });

  }

  markOneItem(item: Item_Ledger_EntrySummary) {
    if (!this.tcUtilsArray.containsId(this.item_ledger_entrysSelection, item.id)) {
      this.item_ledger_entrysSelection = [];
      this.item_ledger_entrysSelection.push(item);
    } else {
      this.item_ledger_entrysSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.item_ledger_entrysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  loadItems() {
    this.itemsPersist.itemsDo('get_all_items', '').subscribe((result) => {
      this.items = (result as ItemSummaryPartialList).data;
    });
  }

}
