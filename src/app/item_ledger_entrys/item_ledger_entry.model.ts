import {TCId} from "../tc/models";

export class Item_Ledger_EntrySummary extends TCId {
  entry_type : number;
item_type : number;
item_id : string;
quantity : number;
store_id : string;
store : string;
batch : number;
expire_date : number;
description : string;
registration_date : number;
balance: number;
}

export class Item_Ledger_EntrySummaryPartialList {
  data: Item_Ledger_EntrySummary[];
  total: number;
}

export class Item_Ledger_EntryDetail extends Item_Ledger_EntrySummary {
  entry_type : number;
item_type : number;
item_id : string;
quantity : number;
store_id : string;
batch : number;
expire_date : number;
description : string;
registration_date : number;
}

export class ItemLedgerEntryPartial{
  entry_type : number;
  batch : number;
  description : string;
  expire_date : number;
}

export class Item_Ledger_EntryDashboard {
  total: number;
}
