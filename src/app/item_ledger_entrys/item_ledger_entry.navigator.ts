import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes, TCParentChildIds } from '../tc/models';

import { Item_Ledger_EntryEditComponent } from './item-ledger-entry-edit/item-ledger-entry-edit.component';
import { Item_Ledger_EntryPickComponent } from './item-ledger-entry-pick/item-ledger-entry-pick.component';
import { Form_VitalsEditComponent } from '../form_encounters/form-vitals-edit/form-vitals-edit.component';

@Injectable({
  providedIn: 'root',
})
export class Item_Ledger_EntryNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}

  item_ledger_entrysUrl(): string {
    return '/item_ledger_entrys';
  }

  item_ledger_entryUrl(id: string): string {
    return '/item_ledger_entrys/' + id;
  }

  viewItem_Ledger_Entrys(): void {
    this.router.navigateByUrl(this.item_ledger_entrysUrl());
  }

  viewItem_Ledger_Entry(id: string): void {
    this.router.navigateByUrl(this.item_ledger_entryUrl(id));
  }

  editItem_Ledger_Entry(
    id: string
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_Ledger_EntryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addItem_Ledger_Entry(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_Ledger_EntryEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickItem_Ledger_Entrys(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Item_Ledger_EntryPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  //form_vitalss
  editForm_Vitals(
    parentId: string,
    id: string
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Form_VitalsEditComponent, {
      data: new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addForm_Vitals(parentId: string): MatDialogRef<unknown,any> {
    return this.editForm_Vitals(parentId, null);
  }
}
