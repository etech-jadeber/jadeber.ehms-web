import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {Item_Ledger_EntryDashboard, Item_Ledger_EntryDetail, Item_Ledger_EntrySummaryPartialList} from "./item_ledger_entry.model";
import { item_type, entry_type } from '../app.enums';
import {TCUtilsString} from "../tc/utils-string";
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class Item_Ledger_EntryPersist {

  item_ledger_entrySearchText: string = "";
  item_typeId: number;
  entry_typeId: number ;
  itemTypeFilter: string = "-1";
  entryTypeFilter: string = "-1";
  categoryId: string;
  category: number;
  store_name: string;
  store_id: string;
  constructor(
    private http: HttpClient,
    private appTranslation: AppTranslation,
    ) {
  }
  item_type: TCEnumTranslation[] = [
    new TCEnumTranslation(item_type.drug , 'drug'),
    new TCEnumTranslation(item_type.consumable, 'consumable'),
    new TCEnumTranslation(item_type.equipment ,'equipment'),
  ];

  entry_type: TCEnumTranslation[] = [
    new TCEnumTranslation(entry_type.sales, 'sales'),
    new TCEnumTranslation(entry_type.purchase, 'purchase'),
    new TCEnumTranslation(entry_type.transfer,'transfer'),
   new TCEnumTranslation(entry_type.lost_damage,'lost_damage'),

  ];



  searchItem_Ledger_Entry(pageSize: number, pageIndex: number, sort: string, order: string, from_date = null, to_date = null): Observable<Item_Ledger_EntrySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_ledger_entrys", this.item_ledger_entrySearchText, pageSize, pageIndex, sort, order);

      url = TCUtilsString.appendUrlParameter(url, "item_type", this.itemTypeFilter.toString());
      url = TCUtilsString.appendUrlParameter(url, "entry_type", this.entryTypeFilter.toString());
      if (from_date){
        url = TCUtilsString.appendUrlParameter(url, "from_date", (new Date(from_date).valueOf() / 1000).toString());
      }
      if (to_date){
        url = TCUtilsString.appendUrlParameter(url, "to_date", (new Date(to_date).valueOf() / 1000).toString());
      }
      if(this.categoryId){
        url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
      }
      if(this.category){
        url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
      }    
      if(this.store_id){
        url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
      }

    return this.http.get<Item_Ledger_EntrySummaryPartialList>(url);

  }
  clearFilters(): void {
    this.item_ledger_entrySearchText = "";
    this.itemTypeFilter = "-1";
    this.entryTypeFilter = "-1";
  }
  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.item_ledger_entrySearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_ledger_entrys/do", new TCDoParam("download_all", this.filters()));
  }

  item_ledger_entryDashboard(): Observable<Item_Ledger_EntryDashboard> {
    return this.http.get<Item_Ledger_EntryDashboard>(environment.tcApiBaseUri + "item_ledger_entrys/dashboard");
  }

  getItem_Ledger_Entry(id: string): Observable<Item_Ledger_EntryDetail> {
    return this.http.get<Item_Ledger_EntryDetail>(environment.tcApiBaseUri + "item_ledger_entrys/" + id);
  }

  addItem_Ledger_Entry(item: Item_Ledger_EntryDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_ledger_entrys/", item);
  }

  updateItem_Ledger_Entry(item: Item_Ledger_EntryDetail): Observable<Item_Ledger_EntryDetail> {
    return this.http.patch<Item_Ledger_EntryDetail>(environment.tcApiBaseUri + "item_ledger_entrys/" + item.id, item);
  }

  deleteItem_Ledger_Entry(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "item_ledger_entrys/" + id);
  }

  item_ledger_entrysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_ledger_entrys/do", new TCDoParam(method, payload));
  }

  item_ledger_entryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_ledger_entrys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "item_ledger_entrys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "item_ledger_entrys/" + id + "/do", new TCDoParam("print", {}));
  }


}
