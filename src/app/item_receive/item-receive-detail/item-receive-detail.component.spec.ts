import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReceiveDetailComponent } from './item-receive-detail.component';

describe('ItemReceiveDetailComponent', () => {
  let component: ItemReceiveDetailComponent;
  let fixture: ComponentFixture<ItemReceiveDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReceiveDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReceiveDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
