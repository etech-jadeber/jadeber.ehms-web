import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ItemReceiveDetail} from "../item_receive.model";
import {ItemReceivePersist} from "../item_receive.persist";
import {ItemReceiveNavigator} from "../item_receive.navigator";

export enum ItemReceiveTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-item_receive-detail',
  templateUrl: './item-receive-detail.component.html',
  styleUrls: ['./item-receive-detail.component.scss']
})
export class ItemReceiveDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemReceiveIsLoading:boolean = false;
  
  ItemReceiveTabs: typeof ItemReceiveTabs = ItemReceiveTabs;
  activeTab: ItemReceiveTabs = ItemReceiveTabs.overview;
  //basics
  itemReceiveDetail: ItemReceiveDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public itemReceiveNavigator: ItemReceiveNavigator,
              public  itemReceivePersist: ItemReceivePersist) {
    this.tcAuthorization.requireRead("item_receives");
    this.itemReceiveDetail = new ItemReceiveDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("item_receives");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.itemReceiveIsLoading = true;
    this.itemReceivePersist.getItemReceive(id).subscribe(itemReceiveDetail => {
          this.itemReceiveDetail = itemReceiveDetail;
          this.itemReceiveIsLoading = false;
        }, error => {
          console.error(error);
          this.itemReceiveIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.itemReceiveDetail.id);
  }
  editItemReceive(): void {
    let modalRef = this.itemReceiveNavigator.editItemReceive(this.itemReceiveDetail.id);
    modalRef.afterClosed().subscribe(itemReceiveDetail => {
      TCUtilsAngular.assign(this.itemReceiveDetail, itemReceiveDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.itemReceivePersist.print(this.itemReceiveDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_receive", true);
      });
    }

  back():void{
      this.location.back();
    }

}
