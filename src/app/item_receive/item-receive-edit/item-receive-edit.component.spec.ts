import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReceiveEditComponent } from './item-receive-edit.component';

describe('ItemReceiveEditComponent', () => {
  let component: ItemReceiveEditComponent;
  let fixture: ComponentFixture<ItemReceiveEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReceiveEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReceiveEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
