import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemReceiveDetail } from '../item_receive.model';import { ItemReceivePersist } from '../item_receive.persist';import { PurchaseStatus, ReceiveStatus, units } from 'src/app/app.enums';
import { RequestPersist } from 'src/app/requests/request.persist';
@Component({
  selector: 'app-item_receive-edit',
  templateUrl: './item-receive-edit.component.html',
  styleUrls: ['./item-receive-edit.component.scss']
})export class ItemReceiveEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  itemReceiveDetail: ItemReceiveDetail;
  receiveStatus = ReceiveStatus;
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ItemReceiveEditComponent>,
              public  persist: ItemReceivePersist,
              public requestPersist: RequestPersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: {id: string, mode: string, quantity: number}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  isAccept(): boolean {
    return this.idMode.mode === "accept";
  }
  isReject(): boolean {
    return this.idMode.mode === "reject";
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("item_receives");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_receive";
      this.itemReceiveDetail = new ItemReceiveDetail();
      this.itemReceiveDetail.purchase_id = this.idMode.id
      this.itemReceiveDetail.quantity = this.idMode.quantity
      this.itemReceiveDetail.unit = units.piece;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("item_receives");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "item_receive";
      this.isLoadingResults = true;
      this.persist.getItemReceive(this.idMode.id).subscribe(itemReceiveDetail => {
        this.itemReceiveDetail = itemReceiveDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } 
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addItemReceive(this.itemReceiveDetail).subscribe(value => {
      this.tcNotification.success("itemReceive added");
      this.itemReceiveDetail.id = value.id
      this.dialogRef.close(this.itemReceiveDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    if(this.isAccept() || this.isReject()){
      this.dialogRef.close(this.itemReceiveDetail);
      return;
    }
    this.isLoadingResults = true;
   

    this.persist.updateItemReceive(this.itemReceiveDetail).subscribe(value => {
      this.tcNotification.success("item_receive updated");
      this.dialogRef.close(this.itemReceiveDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
    if (this.itemReceiveDetail == null){
      return false;
    }
    if(!this.isAccept() && !this.isReject()){

if (this.itemReceiveDetail.purchase_id == null || this.itemReceiveDetail.purchase_id  == "") {
        return false;
    } 
if (this.itemReceiveDetail.quantity == null) {
        return false;
    }
if (this.itemReceiveDetail.unit == null) {
        return false;
    }
// if (this.itemReceiveDetail.selling_price == null) {
//         return false;
//     }
    }
    else if (this.isAccept()){
if (this.itemReceiveDetail.batch_no == null || this.itemReceiveDetail.batch_no  == "") {
        return false;
    } 
if (this.itemReceiveDetail.location == null || this.itemReceiveDetail.location  == "") {
        return false;
    }
    } else {
      if (this.itemReceiveDetail.reject_description == null || this.itemReceiveDetail.reject_description  == "") {
        return false;
    }
    }

  return true;
 }
 }
