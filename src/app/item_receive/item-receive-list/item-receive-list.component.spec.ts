import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReceiveListComponent } from './item-receive-list.component';

describe('ItemReceiveListComponent', () => {
  let component: ItemReceiveListComponent;
  let fixture: ComponentFixture<ItemReceiveListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReceiveListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReceiveListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
