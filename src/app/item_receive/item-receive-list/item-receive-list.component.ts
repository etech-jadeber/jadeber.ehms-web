import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemReceiveDetail, ItemReceiveSummary, ItemReceiveSummaryPartialList } from '../item_receive.model';
import { ItemReceivePersist } from '../item_receive.persist';
import { ItemReceiveNavigator } from '../item_receive.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { RequestPersist } from 'src/app/requests/request.persist';
import { PurchasePersist } from 'src/app/purchases/purchase.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { ReceiveStatus } from 'src/app/app.enums';
import { LostDamageNavigator } from 'src/app/lostDamages/lostDamage.navigator';
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { Item_In_StorePersist } from 'src/app/item_in_stores/item_in_store.persist';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreSummary } from 'src/app/stores/store.model';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { SupplierNavigator } from 'src/app/suppliers/supplier.navigator';
import { SupplierPersist } from 'src/app/suppliers/supplier.persist';
import { SupplierDetail } from 'src/app/suppliers/supplier.model';
import { TCId } from 'src/app/tc/models';
@Component({
  selector: 'app-item_receive-list',
  templateUrl: './item-receive-list.component.html',
  styleUrls: ['./item-receive-list.component.scss']
})export class ItemReceiveListComponent implements OnInit {
  itemReceivesData: ItemReceiveSummary[] = [];
  itemReceivesTotalCount: number = 0;
  itemReceiveSelectAll:boolean = false;
  itemReceiveSelection: ItemReceiveSummary[] = [];
  refNoBox: FormControl = new FormControl();
  crvNumberBox: FormControl = new FormControl();
  receiveStatus = ReceiveStatus;
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
 itemReceivesDisplayedColumns: string[] = ["select","action","item_id","batch_no",'crv_number',"ref_no","location","quantity","unit","unit_price","status", "sent_date", "received_date",'expire_date', "remaining" ];
  itemReceiveSearchTextBox: FormControl = new FormControl();
  itemReceiveIsLoading: boolean = false; 
  
  @ViewChild(MatPaginator, {static: true}) itemReceivesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemReceivesSort: MatSort;
  @Input() purchaseId: string;
  @Input() item_id : string
  store_name: string;
  success_state: number = 1;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemReceivePersist: ItemReceivePersist,
                public itemReceiveNavigator: ItemReceiveNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public requestPersist: RequestPersist,
                public supplierPersist: SupplierPersist,
                public supplierNavigator: SupplierNavigator,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public categoryPersist: ItemCategoryPersist,
                public lostDamageNavigator: LostDamageNavigator,
                public requestNavigator: RequestNavigator,
                public storeNavigator: StoreNavigator,
                public tcUtilsString: TCUtilsString,
                public categoryNavigator: ItemCategoryNavigator,
                public item_in_storePersist: Item_In_StorePersist,

    ) {

        this.tcAuthorization.requireRead("item_receives");
       this.itemReceiveSearchTextBox.setValue(itemReceivePersist.itemReceiveSearchText);
      //delay subsequent keyup events
      this.itemReceiveSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemReceivePersist.itemReceiveSearchText = value;
        this.searchItemReceives();
      });
      this.itemReceivePersist.from_date.valueChanges.pipe().subscribe(value => {
        this.searchItemReceives();
      });
  
      this.itemReceivePersist.to_date.valueChanges.pipe().subscribe(value => {
        this.searchItemReceives();
      });

    this.refNoBox.valueChanges
    .pipe(debounceTime(500))
    .subscribe((value) => {
      this.itemReceivePersist.ref_no = value;
      this.searchItemReceives();
    });

    this.crvNumberBox.valueChanges
    .pipe(debounceTime(500))
    .subscribe((value) => {
      this.itemReceivePersist.crv_number = value;
      this.searchItemReceives();
    });
    } ngOnInit() {
   
      this.itemReceivesSort.sortChange.subscribe(() => {
        this.itemReceivesPaginator.pageIndex = 0;
        this.searchItemReceives(true);
      });

      this.itemReceivesPaginator.page.subscribe(() => {
        this.searchItemReceives(true);
      });
      //start by loading items
      this.searchItemReceives();
    }

  searchItemReceives(isPagination:boolean = false): void {


    let paginator = this.itemReceivesPaginator;
    let sorter = this.itemReceivesSort;

    this.itemReceiveIsLoading = true;
    this.itemReceiveSelection = [];

    this.itemReceivePersist.searchItemReceive(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction,this.item_id).subscribe((partialList: ItemReceiveSummaryPartialList) => {
      this.itemReceivesData = partialList.data;
      if (partialList.total != -1) {
        this.itemReceivesTotalCount = partialList.total;
      }
      this.itemReceiveIsLoading = false;
    }, error => {
      this.itemReceiveIsLoading = false;
    });

  } downloadItemReceives(): void {
    if(this.itemReceiveSelectAll){
         this.itemReceivePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_receive", true);
      });
    }
    else{
        this.itemReceivePersist.download(this.tcUtilsArray.idsList(this.itemReceiveSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download item_receive",true);
            });
        }
  }
addItemReceive(): void {
    let dialogRef = this.itemReceiveNavigator.addItemReceive(this.purchaseId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItemReceives();
      }
    });
  }

  addLostDamage(item: ItemReceiveSummary){
    let dialogRef = this.lostDamageNavigator.addLostDamage(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItemReceives();
      }
    });
}
  acceptItem(item: ItemReceiveSummary){
    let dialogRef = this.itemReceiveNavigator.addItemReceiveInfo(item.id, 'accept');
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.itemReceivePersist.itemReceiveDo(item.id, "accept", result).subscribe(
          (result) => {
              this.searchItemReceives()
          }
        )
      }
    })
  }

  rejectItem(item: ItemReceiveSummary){
    let dialogRef = this.requestNavigator.addReason();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.itemReceivePersist.itemReceiveDo(item.id, "reject", {reject_description: result.reason}).subscribe(
          (result) => {
              this.searchItemReceives()
          }
        )
      }
    })
  }




  firstPrint():void {
    if(this.itemReceivePersist.status != this.receiveStatus.received){
      this.tcNotification.error("please select received status")
      return;
    }
    if(!this.itemReceivePersist.supplier_id){
      this.tcNotification.error("please select Supplier")
      return;
    }     
    if(!this.itemReceivePersist.crv_number){
      this.tcNotification.error("please enter the crv number")
      return;
    }
    this.itemReceiveSelection = this.itemReceiveSelection.filter(value => value.ref_no == null  && value.status == this.receiveStatus.received)
    if (this.itemReceiveSelection.length)  
      this.printTransfers()
    else 
      this.tcNotification.error("no selected request without Ref No.")
  }

  secondPrint():void {
    if(!this.itemReceivePersist.ref_no){
      this.tcNotification.error("please enter the refernce no")
      return;
    }
    this.itemReceiveSelection = this.itemReceiveSelection.filter(value => value.ref_no == this.itemReceivePersist.ref_no && value.status == this.receiveStatus.received)
    if (this.itemReceiveSelection.length)  
      this.printTransfers()
    else 
      this.tcNotification.error("no selected request with Ref No.")
  }

  printTransfers():void {
    let ids = ""
    this.itemReceiveSelection.forEach((value, idx)=>{
      ids += ids =="" ? value.id : ","+ value.id;
    })

    let params = {ids : ids,ref_no: this.itemReceivePersist.ref_no, start_date: this.itemReceivePersist.from_date.value && (new Date(this.itemReceivePersist.from_date.value).getTime()/1000).toString(), end_date: this.itemReceivePersist.to_date.value && (new Date(this.itemReceivePersist.to_date.value).getTime()/1000).toString(), crv_number : this.itemReceivePersist.crv_number, supplier_id: this.itemReceivePersist.supplier_id }
    this.itemReceivePersist.itemReceivesDo("print_good_receive",params).subscribe((downloadJob: TCId) => {
      this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
        this.jobPersist.followJob( downloadJob.id,'printing good receive', true);
        if (job.job_state == this.success_state) {
        }
      });
    });
  }


  searchSupplier(){
    let dialogRef = this.supplierNavigator.pickSuppliers(true)
    dialogRef.afterClosed().subscribe((result: SupplierDetail[]) => {
      if(result){
      this.itemReceivePersist.supplier_name = result[0].name;
      this.itemReceivePersist.supplier_id = result[0].id
      this.searchItemReceives()
      }
    })
  }

  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.itemReceivePersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.itemReceivePersist.categoryId = result[0].id;
      }
      this.searchItemReceives()
    })
  }

  editItemReceive(item: ItemReceiveSummary) {
    let dialogRef = this.itemReceiveNavigator.editItemReceive(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.store_name = result[0].name;
        this.itemReceivePersist.store_id = result[0].id;
        this.searchItemReceives();
      }
    });
  }


  deleteItemReceive(item: ItemReceiveSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("item_receive");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.itemReceivePersist.deleteItemReceive(item.id).subscribe(response => {
          this.tcNotification.success("item_receive deleted");
          this.searchItemReceives();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
