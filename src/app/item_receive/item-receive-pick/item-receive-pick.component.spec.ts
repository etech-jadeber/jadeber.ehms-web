import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReceivePickComponent } from './item-receive-pick.component';

describe('ItemReceivePickComponent', () => {
  let component: ItemReceivePickComponent;
  let fixture: ComponentFixture<ItemReceivePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReceivePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReceivePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
