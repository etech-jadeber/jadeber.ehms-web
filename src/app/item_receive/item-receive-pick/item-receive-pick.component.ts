import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemReceiveSummary, ItemReceiveSummaryPartialList } from '../item_receive.model';
import { ItemReceivePersist } from '../item_receive.persist';
import { ItemReceiveNavigator } from '../item_receive.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ReceiveStatus } from 'src/app/app.enums';
@Component({
  selector: 'app-item_receive-pick',
  templateUrl: './item-receive-pick.component.html',
  styleUrls: ['./item-receive-pick.component.scss']
})export class ItemReceivePickComponent implements OnInit {
  itemReceivesData: ItemReceiveSummary[] = [];
  itemReceivesTotalCount: number = 0;
  itemReceiveSelectAll:boolean = false;
  itemReceiveSelection: ItemReceiveSummary[] = [];

 itemReceivesDisplayedColumns: string[] = ["select",'name',"received_date","batch_no","location","quantity","remaining","unit","unit_price",'expire_date',"remaining" ];
  itemReceiveSearchTextBox: FormControl = new FormControl();
  itemReceiveIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemReceivesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemReceivesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemReceivePersist: ItemReceivePersist,
                public itemReceiveNavigator: ItemReceiveNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public requestPersist: RequestPersist,
 public dialogRef: MatDialogRef<ItemReceivePickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, itemId: string;}

    ) {
        this.tcAuthorization.requireRead("item_receives");
       this.itemReceiveSearchTextBox.setValue(itemReceivePersist.itemReceiveSearchText);
       this.itemReceivePersist.isRemaining = true;
       this.itemReceivePersist.status = ReceiveStatus.received
       this.itemReceivePersist.not_expired = true;
      //delay subsequent keyup events
      this.itemReceiveSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemReceivePersist.itemReceiveSearchText = value;
        this.searchItem_receives();
      });
    } ngOnInit() {
   
      this.itemReceivesSort.sortChange.subscribe(() => {
        this.itemReceivesPaginator.pageIndex = 0;
        this.searchItem_receives(true);
      });

      this.itemReceivesPaginator.page.subscribe(() => {
        this.searchItem_receives(true);
      });
      //start by loading items
      this.searchItem_receives();
    }

  searchItem_receives(isPagination:boolean = false): void {


    let paginator = this.itemReceivesPaginator;
    let sorter = this.itemReceivesSort;

    this.itemReceiveIsLoading = true;
    this.itemReceiveSelection = [];
    this.itemReceivePersist.searchItemReceive(paginator.pageSize, isPagination? paginator.pageIndex:0, "expire_date", "asc", this.data.itemId).subscribe((partialList: ItemReceiveSummaryPartialList) => {
      this.itemReceivesData = partialList.data;
      if (partialList.total != -1) {
        this.itemReceivesTotalCount = partialList.total;
      }
      this.itemReceiveIsLoading = false;
    }, error => {
      this.itemReceiveIsLoading = false;
    });

  }
  markOneItem(item: ItemReceiveSummary) {
    if(!this.tcUtilsArray.containsId(this.itemReceiveSelection,item.id)){
          this.itemReceiveSelection = [];
          this.itemReceiveSelection.push(item);
        }
        else{
          this.itemReceiveSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemReceiveSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  ngOnDestroy():void {
    this.itemReceivePersist.status = null;
    this.itemReceivePersist.not_expired = false;
  }

  }
