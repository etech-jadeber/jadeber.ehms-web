import {TCId} from "../tc/models";

export class ItemReceiveSummary extends TCId {
    purchase_id:string;
    received_date:number;
    batch_no:string;
    location:string;
    quantity:number;
    unit:number;
    selling_price:number;
    status:number;
    reject_description: string;
    item_id: string;
    item_name: string;
    remaining:number;
    expire_date: number;
    unit_price : number;
    ref_no: string;
     
    }
    export class ItemReceiveSummaryPartialList {
      data: ItemReceiveSummary[];
      total: number;
    }
    export class ItemReceiveDetail extends ItemReceiveSummary {
    }
    
    export class ItemReceiveDashboard {
      total: number;
    }
    