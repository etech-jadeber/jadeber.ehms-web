import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ItemReceiveEditComponent} from "./item-receive-edit/item-receive-edit.component";
import {ItemReceivePickComponent} from "./item-receive-pick/item-receive-pick.component";
import { ReceiveStatus } from "../app.enums";


@Injectable({
  providedIn: 'root'
})

export class ItemReceiveNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  itemReceivesUrl(): string {
    return "/item_receives";
  }

  itemReceiveUrl(id: string): string {
    return "/item_receives/" + id;
  }

  viewItemReceives(): void {
    this.router.navigateByUrl(this.itemReceivesUrl());
  }

  viewItemReceive(id: string): void {
    this.router.navigateByUrl(this.itemReceiveUrl(id));
  }

  editItemReceive(id: string): MatDialogRef<ItemReceiveEditComponent> {
    const dialogRef = this.dialog.open(ItemReceiveEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemReceive(purchaseId: string, quantity: number = null): MatDialogRef<ItemReceiveEditComponent> {
    const dialogRef = this.dialog.open(ItemReceiveEditComponent, {
          data: {id : purchaseId, mode: TCModalModes.NEW, quantity},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemReceiveInfo(id: string, mode: string): MatDialogRef<ItemReceiveEditComponent> {
    const dialogRef = this.dialog.open(ItemReceiveEditComponent, {
          data: new TCIdMode(id, mode),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickItemReceives(itemId: string = null, status: number = ReceiveStatus.received,  selectOne: boolean=false): MatDialogRef<ItemReceivePickComponent> {
      const dialogRef = this.dialog.open(ItemReceivePickComponent, {
        data: {selectOne, itemId, status},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
