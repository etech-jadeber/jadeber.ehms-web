import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {ItemReceiveDashboard, ItemReceiveDetail, ItemReceiveSummaryPartialList} from "./item_receive.model";
import { ReceiveStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class ItemReceivePersist {
 itemReceiveSearchText: string = "";
 categoryId: string;
 category: number;
 isRemaining: boolean = true;
 not_expired: boolean = false;
 
 status: number ;

    ReceiveStatus: TCEnum[] = [
     new TCEnum( ReceiveStatus.sent, 'Sent'),
  new TCEnum( ReceiveStatus.received, 'Received'),
  new TCEnum( ReceiveStatus.rejected, 'Rejected'),

  ];
  store_id: string;
  ref_no: string;
  crv_number: string;
  supplier_name: string;
  supplier_id: string;

 
 constructor(private http: HttpClient,
  public tcUtilsDate: TCUtilsDate) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemReceiveSearchText;
    //add custom filters
    return fltrs;
  }

  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""}) 
 
  searchItemReceive(pageSize: number, pageIndex: number, sort: string, order: string, item_id = null): Observable<ItemReceiveSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_receive", this.itemReceiveSearchText, pageSize, pageIndex, sort, order);
    if(item_id){
      url = TCUtilsString.appendUrlParameter(url, "item_id", item_id);
    }
    if(this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString());
    }
    if(this.not_expired){
      url = TCUtilsString.appendUrlParameter(url, "not_expired", this.not_expired ? "1":"0");
    }
    if (this.from_date.value){
      url = TCUtilsString.appendUrlParameter(url, "from_date", this.tcUtilsDate.toTimeStamp(this.from_date.value).toString())
    }
    if (this.to_date.value){
      url = TCUtilsString.appendUrlParameter(url, "to_date", this.tcUtilsDate.toTimeStamp(this.to_date.value).toString())
    }

    if(this.ref_no){
      url = TCUtilsString.appendUrlParameter(url, "ref_no", this.ref_no);
    }
    if(this.supplier_id){
      url = TCUtilsString.appendUrlParameter(url, "supplier_id", this.supplier_id);
    }
    if(this.crv_number){
      url = TCUtilsString.appendUrlParameter(url, "crv_number", this.crv_number);
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }        
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }    
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    url = TCUtilsString.appendUrlParameter(url, "isRemaining", `${this.isRemaining}`)
    return this.http.get<ItemReceiveSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_receive/do", new TCDoParam("download_all", this.filters()));
  }

  itemReceiveDashboard(): Observable<ItemReceiveDashboard> {
    return this.http.get<ItemReceiveDashboard>(environment.tcApiBaseUri + "item_receive/dashboard");
  }

  getItemReceive(id: string): Observable<ItemReceiveDetail> {
    return this.http.get<ItemReceiveDetail>(environment.tcApiBaseUri + "item_receive/" + id);
  } addItemReceive(item: ItemReceiveDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_receive/", item);
  }

  updateItemReceive(item: ItemReceiveDetail): Observable<ItemReceiveDetail> {
    return this.http.patch<ItemReceiveDetail>(environment.tcApiBaseUri + "item_receive/" + item.id, item);
  }

  deleteItemReceive(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "item_receive/" + id);
  }
 itemReceivesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_receive/do", new TCDoParam(method, payload));
  }

  itemReceiveDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_receive/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "item_receive/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "item_receive/" + id + "/do", new TCDoParam("print", {}));
  }


}
