import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemRequestDetailComponent } from './item-request-detail.component';

describe('ItemRequestDetailComponent', () => {
  let component: ItemRequestDetailComponent;
  let fixture: ComponentFixture<ItemRequestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemRequestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
