import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";

import { ItemRequestDetail } from '../item_request.model';
import { ItemRequestNavigator } from '../item_request.navigator';
import { ItemRequestPersist } from '../item_request.persist';

export enum ItemRequestTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-item-request-detail',
  templateUrl: './item-request-detail.component.html',
  styleUrls: ['./item-request-detail.component.scss']
})
export class ItemRequestDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemRequestIsLoading:boolean = false;
  
  ItemRequestTabs: typeof ItemRequestTabs = ItemRequestTabs;
  activeTab: ItemRequestTabs = ItemRequestTabs.overview;
  //basics
  itemRequestDetail: ItemRequestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public itemRequestNavigator: ItemRequestNavigator,
              public  itemRequestPersist: ItemRequestPersist) {
        this.tcAuthorization.requireRead("item_requests");
        this.itemRequestDetail = new ItemRequestDetail();
      } 
      
  ngOnInit() {
    this.tcAuthorization.requireRead("item_requests");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.itemRequestIsLoading = true;
    this.itemRequestPersist.getItemRequest(id).subscribe(itemRequestDetail => {
          this.itemRequestDetail = itemRequestDetail;
          this.itemRequestIsLoading = false;
        }, error => {
          console.error(error);
          this.itemRequestIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.itemRequestDetail.id);
  }
  editItemRequest(): void {
    let modalRef = this.itemRequestNavigator.editItemRequest(this.itemRequestDetail.id);
    modalRef.afterClosed().subscribe(itemRequestDetail => {
      TCUtilsAngular.assign(this.itemRequestDetail, itemRequestDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.itemRequestPersist.print(this.itemRequestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_request", true);
      });
    }

  back():void{
      this.location.back();
    }

}