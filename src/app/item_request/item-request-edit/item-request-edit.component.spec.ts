import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemRequestEditComponent } from './item-request-edit.component';

describe('ItemRequestEditComponent', () => {
  let component: ItemRequestEditComponent;
  let fixture: ComponentFixture<ItemRequestEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemRequestEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemRequestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
