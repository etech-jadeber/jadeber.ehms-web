import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemRequestDetail } from '../item_request.model';
import { ItemRequestPersist } from '../item_request.persist';
import { StoreSummary } from 'src/app/stores/store.model';
import { ItemSummary } from 'src/app/items/item.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { category } from 'src/app/app.enums';
import { ItemPersist } from 'src/app/items/item.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';

@Component({
  selector: 'app-item-request-edit',
  templateUrl: './item-request-edit.component.html',
  styleUrls: ['./item-request-edit.component.scss']
})export class ItemRequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  itemRequestDetail: ItemRequestDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public itemNavigator: ItemNavigator,
              public itemPersist: ItemPersist,
              public storeNavigator: StoreNavigator,
              public dialogRef: MatDialogRef<ItemRequestEditComponent>,
              public  persist: ItemRequestPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("item_requests");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_request";
      this.itemRequestDetail = new ItemRequestDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("item_requests");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "item_request";
      this.isLoadingResults = true;
      this.persist.getItemRequest(this.idMode.id).subscribe(itemRequestDetail => {
        this.itemRequestDetail = itemRequestDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    console.log(this.itemRequestDetail);
    
    this.persist.addItemRequest(this.itemRequestDetail).subscribe(value => {
      this.tcNotification.success("itemRequest added");
      this.itemRequestDetail.id = value.id;
      this.dialogRef.close(this.itemRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateItemRequest(this.itemRequestDetail).subscribe(value => {
      this.tcNotification.success("item_request updated");
      this.dialogRef.close(this.itemRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  canSubmit():boolean{
        if (this.itemRequestDetail == null){
            return false;
          }
        if (this.itemRequestDetail.item_id == null){
          return false;
        }

        if(this.itemRequestDetail.store_id == null){
          return false;
        }

        if(this.itemRequestDetail.quantity == null || this.itemRequestDetail.quantity < 0){
          return false;
        }
        

        return true;

 }
 searchItem() {
  this.itemPersist.category = category.supply;
  let dialogRef = this.itemNavigator.pickItems(true);
  dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
    this.itemPersist.category = undefined;
    if (result) {
      this.itemRequestDetail.item_name = this.itemNavigator.itemName(result[0]);
      this.itemRequestDetail.item_id = result[0].id;
    }
  });
}

 searchStore() {
  let dialogRef = this.storeNavigator.pickStores(true);
  dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
    if (result) {
      console.log(result)
      this.itemRequestDetail.store_name = result[0].name;
      this.itemRequestDetail.store_id = result[0].id;
      console.log(result[0].name,result[0].id, this.itemRequestDetail.store_id)
    }
  });
}

 }