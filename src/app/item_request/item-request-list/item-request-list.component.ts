import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { ItemRequestSummary, ItemRequestSummaryPartialList } from '../item_request.model';
import { ItemRequestNavigator } from '../item_request.navigator';
import { ItemRequestPersist } from '../item_request.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { ItemDetail } from 'src/app/items/item.model';
import { ItemPersist } from 'src/app/items/item.persist';
import { StoreDetail, StoreSummary } from 'src/app/stores/store.model';
import { StorePersist } from 'src/app/stores/store.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { RequestPersist } from 'src/app/requests/request.persist';
import { PrescriptionRequestStatus, PrescriptionStatus, request_status } from 'src/app/app.enums';
import { ItemDispatchNavigator } from 'src/app/item_dispatch/item_dispatch.navigator';
import { TCModalModes } from 'src/app/tc/models';
import { StoreNavigator } from 'src/app/stores/store.navigator';
@Component({
  selector: 'app-item-request-list',
  templateUrl: './item-request-list.component.html',
  styleUrls: ['./item-request-list.component.scss']
})
export class ItemRequestListComponent implements OnInit {
  itemRequestsData: ItemRequestSummary[] = [];
  itemRequestsTotalCount: number = 0;
  itemRequestSelectAll:boolean = false;
  itemRequestSelection: ItemRequestSummary[] = [];

  itemRequestsDisplayedColumns: string[] = ["select","action","item_id","store_id","quantity","requestor_id","request_date","responsed_by","response_date","status" ];
  itemRequestSearchTextBox: FormControl = new FormControl();
  request_status = request_status;
  itemRequestIsLoading: boolean = false; 
  user : {[id:string]:UserDetail} = {}
  items : {[id:string]:ItemDetail} = {}
  stores : {[id:string]:StoreDetail} = {}
  @Input() request_id:string;
   @ViewChild(MatPaginator, {static: true}) itemRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemRequestsSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public itemPersist: ItemPersist,
                public appTranslation:AppTranslation,
                public itemRequestPersist: ItemRequestPersist,
                public storePersist: StorePersist,
                public requestPersist: RequestPersist,
                public storeNavigator: StoreNavigator,
                public itemNavigator: ItemNavigator,
                public userPersist: UserPersist,
                public itemRequestNavigator: ItemRequestNavigator,
                public jobPersist: JobPersist,
                public itemDispatchNavigator: ItemDispatchNavigator,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("item_requests");
       this.itemRequestSearchTextBox.setValue(itemRequestPersist.itemRequestSearchText);
      //delay subsequent keyup events
      this.itemRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemRequestPersist.itemRequestSearchText = value;
        this.searchItem_requests();
      });
    } ngOnInit() {
   
      this.itemRequestsSort.sortChange.subscribe(() => {
        this.itemRequestsPaginator.pageIndex = 0;
        this.searchItem_requests(true);
      });

      this.itemRequestsPaginator.page.subscribe(() => {
        this.searchItem_requests(true);
      });
      //start by loading items
      this.searchItem_requests();
    }

  searchItem_requests(isPagination:boolean = false): void {


    let paginator = this.itemRequestsPaginator;
    let sorter = this.itemRequestsSort;

    this.itemRequestIsLoading = true;
    this.itemRequestSelection = [];

    this.itemRequestPersist.requestor_id = this.request_id;
    this.itemRequestPersist.searchItemRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemRequestSummaryPartialList) => {
      this.itemRequestsData = partialList.data;
      this.itemRequestsData.forEach(item_requestData =>{
        if(!this.user[item_requestData.requestor_id]){
          this.userPersist.getUser(item_requestData.requestor_id).subscribe((_user)=>{
            if(_user){
              this.user[item_requestData.requestor_id] = _user;
            }
          })
        }

        if(item_requestData.responsed_by && !this.user[item_requestData.responsed_by]){
          this.userPersist.getUser(item_requestData.requestor_id).subscribe((_user)=>{
            if(_user){
              this.user[item_requestData.requestor_id] = _user;
            }
          })
        }
          if(!this.items[item_requestData.item_id]){
            this.itemPersist.getItem(item_requestData.item_id).subscribe((_item)=>{
              if(_item)
              this.items[item_requestData.item_id] = _item;
            })
          }
          if(!this.stores[item_requestData.store_id]){
            this.storePersist.getStore(item_requestData.store_id).subscribe((_store)=>{
              if(_store)
              this.stores[item_requestData.store_id] = _store;
            })
          }
        

      })
      
      if (partialList.total != -1) {
        this.itemRequestsTotalCount = partialList.total;
      }
      this.itemRequestIsLoading = false;
    }, error => {
      this.itemRequestIsLoading = false;
    });

  } downloadItemRequests(): void {
    if(this.itemRequestSelectAll){
         this.itemRequestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_request", true);
      });
    }
    else{
        this.itemRequestPersist.download(this.tcUtilsArray.idsList(this.itemRequestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download item_request",true);
            });
        }
  }
addItem_request(): void {
    let dialogRef = this.itemRequestNavigator.addItemRequest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchItem_requests();
      }
    });
  }

  editItemRequest(item: ItemRequestSummary) {
    let dialogRef = this.itemRequestNavigator.editItemRequest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  acceptItemRequest(item: ItemRequestSummary){
    this.itemRequestPersist.itemRequestDo(item.id, "accept",{}).subscribe((value)=>{
      this.tcNotification.success("The item request accepted");
      this.searchItem_requests()
    })
  }


  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.itemRequestPersist.store_name = result[0].name;
        this.itemRequestPersist.store_id = result[0].id;
        this.searchItem_requests();
      }
    });
  }

  rejectItemRequest(item: ItemRequestSummary){
    this.itemRequestPersist.itemRequestDo(item.id, "reject",{}).subscribe((value)=>{
      this.tcNotification.success("The item request rejected");
      this.searchItem_requests()
    })
  }

  acceptOrDispatchItems(item: ItemRequestSummary){
    let dialogRef = this.itemDispatchNavigator.addItemDispatch(item, PrescriptionRequestStatus.accept,TCModalModes.CASE);
  dialogRef.afterClosed().subscribe(result => {
    if(result){
      this.searchItem_requests();
      
    }
  })
  }

  deleteItemRequest(item: ItemRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("item_request");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.itemRequestPersist.deleteItemRequest(item.id).subscribe(response => {
          this.tcNotification.success("item_request deleted");
          this.searchItem_requests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}