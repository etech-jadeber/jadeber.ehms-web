import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemRequestPickComponent } from './item-request-pick.component';

describe('ItemRequestPickComponent', () => {
  let component: ItemRequestPickComponent;
  let fixture: ComponentFixture<ItemRequestPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemRequestPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemRequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
