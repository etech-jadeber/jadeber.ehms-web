import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemRequestSummary, ItemRequestSummaryPartialList } from '../item_request.model';
import { ItemRequestPersist } from '../item_request.persist';
@Component({
  selector: 'app-item-request-pick',
  templateUrl: './item-request-pick.component.html',
  styleUrls: ['./item-request-pick.component.scss']
})export class ItemRequestPickComponent implements OnInit {
  itemRequestsData: ItemRequestSummary[] = [];
  itemRequestsTotalCount: number = 0;
  itemRequestSelectAll:boolean = false;
  itemRequestSelection: ItemRequestSummary[] = [];

 itemRequestsDisplayedColumns: string[] = ["select","action","responsed_by" ];
  itemRequestSearchTextBox: FormControl = new FormControl();
  itemRequestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemRequestsSort: MatSort;  
  constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                
                public dialogRef: MatDialogRef<ItemRequestPickComponent>,
                public itemRequestPersist: ItemRequestPersist,
                @Inject(MAT_DIALOG_DATA) public selectOne: boolean
                ) {

              this.tcAuthorization.requireRead("item_requests");
            this.itemRequestSearchTextBox.setValue(itemRequestPersist.itemRequestSearchText);
            //delay subsequent keyup events
            this.itemRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
              this.itemRequestPersist.itemRequestSearchText = value;
              this.searchItem_requests();
          });
    } ngOnInit() {
   
      this.itemRequestsSort.sortChange.subscribe(() => {
        this.itemRequestsPaginator.pageIndex = 0;
        this.searchItem_requests(true);
      });

      this.itemRequestsPaginator.page.subscribe(() => {
        this.searchItem_requests(true);
      });
      //start by loading items
      this.searchItem_requests();
    }

  searchItem_requests(isPagination:boolean = false): void {


    let paginator = this.itemRequestsPaginator;
    let sorter = this.itemRequestsSort;

    this.itemRequestIsLoading = true;
    this.itemRequestSelection = [];

    this.itemRequestPersist.searchItemRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemRequestSummaryPartialList) => {
      this.itemRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.itemRequestsTotalCount = partialList.total;
      }
      this.itemRequestIsLoading = false;
    }, error => {
      this.itemRequestIsLoading = false;
    });

  }
  markOneItem(item: ItemRequestSummary) {
    if(!this.tcUtilsArray.containsId(this.itemRequestSelection,item.id)){
          this.itemRequestSelection = [];
          this.itemRequestSelection.push(item);
        }
        else{
          this.itemRequestSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemRequestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }