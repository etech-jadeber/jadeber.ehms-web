import {TCId} from "../tc/models";

export class ItemRequestSummary extends TCId {
    requestor_id:string;
    requestor_name:string;

    store_id: string;
    request_date:number;
    response_date:number;
    status: number;
    quantity:number;
    item_id: string;
    item_name:string;
    responsed_by:string;
    responsor_name:string;
    store_name: string;
    total: number;
     
    }
    export class ItemRequestSummaryPartialList {
      data: ItemRequestSummary[];
      total: number;
    }
    export class ItemRequestDetail extends ItemRequestSummary {
    }
    
    export class ItemRequestDashboard {
      total: number;
    }

    export class EmployeeRequestSummary {
      name:string;
      requested:number;
      accepted:number;
      rejected:number;
      dispatched:number;
    }

    export class EmployeeRequestSummaryPartialList {
      data: EmployeeRequestSummary[];
      total: number;
    }