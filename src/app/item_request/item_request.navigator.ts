import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import { ItemRequestEditComponent } from "./item-request-edit/item-request-edit.component";
import { ItemRequestPickComponent } from "./item-request-pick/item-request-pick.component";



@Injectable({
  providedIn: 'root'
})

export class ItemRequestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  item_requestsUrl(): string {
    return "/item_requests";
  }
  
  employee_item_requestsUrl():string {
    return "/employee_item_requests"
  }

  employee_item_requestUrl(id:string):string {
    return "/employee_item_requests/" + id;
  }
  
  item_requestUrl(id: string): string {
    return "/item_requests/" + id;
  }

  viewItemRequests(): void {
    this.router.navigateByUrl(this.item_requestsUrl());
  }

  viewItemRequest(id: string): void {
    this.router.navigateByUrl(this.item_requestUrl(id));
  }

  editItemRequest(id: string): MatDialogRef<ItemRequestEditComponent> {
    const dialogRef = this.dialog.open(ItemRequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemRequest(): MatDialogRef<ItemRequestEditComponent> {
    const dialogRef = this.dialog.open(ItemRequestEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickItemRequests(selectOne: boolean=false): MatDialogRef<ItemRequestPickComponent> {
      const dialogRef = this.dialog.open(ItemRequestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}