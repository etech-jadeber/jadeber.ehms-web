import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ItemRequestDashboard, ItemRequestDetail, ItemRequestSummaryPartialList} from "./item_request.model";
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class ItemRequestPersist {
  employeeRequestSearchText :string = "";
  date: FormControl = new FormControl({disabled: true, value: ""});

 itemRequestSearchText: string = "";
 requestor_id : string;
  store_name: string;
  store_id: string;
 constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_request/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemRequestSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchItemRequest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ItemRequestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_request", this.itemRequestSearchText, pageSize, pageIndex, sort, order);
    if(this.requestor_id){
       url = TCUtilsString.appendUrlParameter(url, "requestor_id",this.requestor_id);
    }

    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    return this.http.get<ItemRequestSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_request/do", new TCDoParam("download_all", this.filters()));
  }

  searchemployeeRequest(pageSize: number, pageIndex: number, sort: string, order: string) {
    let url = TCUtilsHttp.buildSearchUrl("item_request/do", this.itemRequestSearchText, pageSize, pageIndex, sort, order);
    // if(this.date.value)
    //   url = TCUtilsString.appendUrlParameter(url, "date", (new Date(this.date.value).getTime()/1000).toString())
    
    if(this.store_id){
        url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
      }

    return this.http.post<{}>(url,new TCDoParam("employee_item_request", {}));
  }

  itemRequestDashboard(): Observable<ItemRequestDashboard> {
    return this.http.get<ItemRequestDashboard>(environment.tcApiBaseUri + "item_request/dashboard");
  }

  getItemRequest(id: string): Observable<ItemRequestDetail> {
    return this.http.get<ItemRequestDetail>(environment.tcApiBaseUri + "item_request/" + id);
  }
  addItemRequest(item: ItemRequestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + 'item_request/', item);
  }

  updateItemRequest(item: ItemRequestDetail): Observable<ItemRequestDetail> {
    return this.http.patch<ItemRequestDetail>(
      environment.tcApiBaseUri + 'item_request/' + item.id,
      item
    );
  }

  deleteItemRequest(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'item_request/' + id);
  }

  itemRequestsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'item_request/do',
      new TCDoParam(method, payload)
    );
  }

  itemRequestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'item_request/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'item_request/do',
      new TCDoParam('download', ids)
    );
  }

 }