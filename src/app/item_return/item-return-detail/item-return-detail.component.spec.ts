import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnDetailComponent } from './item-return-detail.component';

describe('ItemReturnDetailComponent', () => {
  let component: ItemReturnDetailComponent;
  let fixture: ComponentFixture<ItemReturnDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
