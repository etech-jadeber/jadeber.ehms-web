import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ItemReturnDetail} from "../item_return.model";
import {ItemReturnPersist} from "../item_return.persist";
import {ItemReturnNavigator} from "../item_return.navigator";

export enum ItemReturnTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-item_return-detail',
  templateUrl: './item-return-detail.component.html',
  styleUrls: ['./item-return-detail.component.scss']
})
export class ItemReturnDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemReturnIsLoading:boolean = false;
  
  ItemReturnTabs: typeof ItemReturnTabs = ItemReturnTabs;
  activeTab: ItemReturnTabs = ItemReturnTabs.overview;
  //basics
  itemReturnDetail: ItemReturnDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public itemReturnNavigator: ItemReturnNavigator,
              public  itemReturnPersist: ItemReturnPersist) {
    this.tcAuthorization.requireRead("item_returns");
    this.itemReturnDetail = new ItemReturnDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("item_returns");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.itemReturnIsLoading = true;
    this.itemReturnPersist.getItemReturn(id).subscribe(itemReturnDetail => {
          this.itemReturnDetail = itemReturnDetail;
          this.itemReturnIsLoading = false;
        }, error => {
          console.error(error);
          this.itemReturnIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.itemReturnDetail.id);
  }
  editItemReturn(): void {
    let modalRef = this.itemReturnNavigator.editItemReturn(this.itemReturnDetail.id);
    modalRef.afterClosed().subscribe(itemReturnDetail => {
      TCUtilsAngular.assign(this.itemReturnDetail, itemReturnDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.itemReturnPersist.print(this.itemReturnDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print item_return", true);
      });
    }

  back():void{
      this.location.back();
    }

}
