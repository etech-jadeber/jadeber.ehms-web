import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnEditComponent } from './item-return-edit.component';

describe('ItemReturnEditComponent', () => {
  let component: ItemReturnEditComponent;
  let fixture: ComponentFixture<ItemReturnEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
