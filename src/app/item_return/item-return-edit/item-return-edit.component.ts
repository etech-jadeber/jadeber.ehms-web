import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ItemReturnDetail } from '../item_return.model';import { ItemReturnPersist } from '../item_return.persist';import { ItemDispatchPersist } from 'src/app/item_dispatch/item_dispatch.persist';
@Component({
  selector: 'app-item_return-edit',
  templateUrl: './item-return-edit.component.html',
  styleUrls: ['./item-return-edit.component.scss']
})export class ItemReturnEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  itemReturnDetail: ItemReturnDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ItemReturnEditComponent>,
              public persist: ItemReturnPersist,
              public item_dispatchPersist: ItemDispatchPersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isItemDispatch(): boolean {
    return this.idMode.mode === 'ITEM_DISPATCH';
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.isItemDispatch()){
      this.tcAuthorization.requireCreate("item_returns");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_return";
      this.itemReturnDetail = new ItemReturnDetail();
      this.itemReturnDetail.target_id = this.idMode.id
    }
    else if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("item_returns");
      this.title = this.appTranslation.getText("general","new") +  " " + "item_return";
      this.itemReturnDetail = new ItemReturnDetail();
      this.itemReturnDetail.target_id = this.idMode.id
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("item_returns");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "item_return";
      this.isLoadingResults = true;
      this.persist.getItemReturn(this.idMode.id).subscribe(itemReturnDetail => {
        this.itemReturnDetail = itemReturnDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addItemReturn(this.itemReturnDetail).subscribe(value => {
      this.tcNotification.success("itemReturn added");
      this.itemReturnDetail.id = value.id;
      this.dialogRef.close(this.itemReturnDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onAddItemDispatchReturn(): void {
    this.isLoadingResults = true;
   
    this.item_dispatchPersist.itemDispatchDo(this.idMode.id, "return",  {quantity: this.itemReturnDetail.quantity}).subscribe((value: TCId) => {
      this.tcNotification.success("itemReturn added");
      this.dialogRef.close(this.itemReturnDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateItemReturn(this.itemReturnDetail).subscribe(value => {
      this.tcNotification.success("item_return updated");
      this.dialogRef.close(this.itemReturnDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.itemReturnDetail == null){
            return false;
          }

      if (this.itemReturnDetail.target_id == null || this.itemReturnDetail.target_id  == "") {
                  return false;
              } 
      if (this.itemReturnDetail.quantity == null) {
                  return false;
              }
            return true;
 }
 }
