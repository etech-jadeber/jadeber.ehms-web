import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnGroupListComponent } from './item-return-group-list.component';

describe('ItemReturnGroupListComponent', () => {
  let component: ItemReturnGroupListComponent;
  let fixture: ComponentFixture<ItemReturnGroupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnGroupListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnGroupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
