import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnListComponent } from './item-return-list.component';

describe('ItemReturnListComponent', () => {
  let component: ItemReturnListComponent;
  let fixture: ComponentFixture<ItemReturnListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
