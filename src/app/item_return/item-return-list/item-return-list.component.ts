import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemReturnDetail, ItemReturnSummary, ItemReturnSummaryPartialList } from '../item_return.model';
import { ItemReturnPersist } from '../item_return.persist';
import { ItemReturnNavigator } from '../item_return.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemReceivePersist } from 'src/app/item_receive/item_receive.persist';
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ReceiveStatus, return_from } from 'src/app/app.enums';
import { StoreDetail, StoreSummary } from 'src/app/stores/store.model';
import { StorePersist } from 'src/app/stores/store.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { TCId } from 'src/app/tc/models';
@Component({
  selector: 'app-item_return-list',
  templateUrl: './item-return-list.component.html',
  styleUrls: ['./item-return-list.component.scss']
})export class ItemReturnListComponent implements OnInit {
  itemReturnsData: ItemReturnSummary[] = [];
  ReceiveStatus =  ReceiveStatus;
  itemReturnsTotalCount: number = 0;
  itemReturnSelectAll:boolean = false;
  itemReturnSelection: ItemReturnSummary[] = [];
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
  itemReturnsDisplayedColumns: string[] = ["select","action","name" ,"batch_no","ref_no","store","quantity","status",'request_date' ];
  itemReturnSearchTextBox: FormControl = new FormControl();
  itemReturnIsLoading: boolean = false;  
  stores : {[id: string]: StoreDetail} = {}
  users: {[id: string]: UserDetail} = {}
  refNoBox: FormControl = new FormControl();
  
  @ViewChild(MatPaginator, {static: true}) itemReturnsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemReturnsSort: MatSort;
  @Input() isRequested: boolean
  success_state: number= 1;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemReturnPersist: ItemReturnPersist,
                public itemReturnNavigator: ItemReturnNavigator,
                public itemNavigator: ItemNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemReceivePersist: ItemReceivePersist,
                public requestNavigator: RequestNavigator,
                public categoryNavigator: ItemCategoryNavigator,
                public categoryPersist: ItemCategoryPersist,
                public tcUtilsString: TCUtilsString,
                public storePersist: StorePersist,
                public storeNavigator: StoreNavigator,
                public userPersist: UserPersist,

    ) {

        this.tcAuthorization.requireRead("item_returns");
       this.itemReturnSearchTextBox.setValue(itemReturnPersist.itemReturnSearchText);
      //delay subsequent keyup events
      this.itemReturnSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemReturnPersist.itemReturnSearchText = value;
        this.searchItemReturns();
      });
      this.itemReturnPersist.from_date.valueChanges.pipe().subscribe(value => {
        this.searchItemReturns();
      });
  
      this.refNoBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.itemReturnPersist.ref_no = value;
        this.searchItemReturns();
      });
      this.itemReturnPersist.to_date.valueChanges.pipe().subscribe(value => {
        this.searchItemReturns();
      });
    } ngOnInit() {
   
      this.itemReturnsSort.sortChange.subscribe(() => {
        this.itemReturnsPaginator.pageIndex = 0;
        this.searchItemReturns(true);
      });

      this.itemReturnsPaginator.page.subscribe(() => {
        this.searchItemReturns(true);
      });
      //start by loading items
      this.searchItemReturns();
    }

  searchItemReturns(isPagination:boolean = false): void {


    let paginator = this.itemReturnsPaginator;
    let sorter = this.itemReturnsSort;

    this.itemReturnIsLoading = true;
    this.itemReturnSelection = [];

    this.itemReturnPersist.searchItemReturn(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.isRequested).subscribe((partialList: ItemReturnSummaryPartialList) => {
      this.itemReturnsData = partialList.data;
      this.itemReturnsData.forEach((item_return) => {
        if (item_return.return_from == return_from.store && !this.stores[item_return.return_from_id]){
          this.stores[item_return.return_from_id] = new StoreDetail()
          this.storePersist.getStore(item_return.return_from_id).subscribe(store => {
            this.stores[item_return.return_from_id] = store;
          })
        }
        else if (this.users[item_return.return_from_id]){
          this.users[item_return.return_from_id] = new UserDetail()
          this.userPersist.getUser(item_return.return_from_id).subscribe(user => {
            this.users[item_return.return_from_id] = user;
          })
        }
      })
      if (partialList.total != -1) {
        this.itemReturnsTotalCount = partialList.total;
      }
      this.itemReturnIsLoading = false;
    }, error => {
      this.itemReturnIsLoading = false;
    });

  } downloadItemReturns(): void {
    if(this.itemReturnSelectAll){
         this.itemReturnPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download item_return", true);
      });
    }
    else{
        this.itemReturnPersist.download(this.tcUtilsArray.idsList(this.itemReturnSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download item_return",true);
            });
        }
  }
// addItemReturn(): void {
//     let dialogRef = this.itemReturnNavigator.addItemReturn();
//     dialogRef.afterClosed().subscribe(result => {
//       if (result) {
//         this.searchItemReturns();
//       }
//     });
//   }


searchStoreFrom() {
  let dialogRef = this.storeNavigator.pickStores(true);
  dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
    if (result) {
      this.itemReturnPersist.store_from_name = result[0].name;
      this.itemReturnPersist.return_from_id = result[0].id;
      this.searchItemReturns();
    }
  });
}

firstPrint():void {
  if(!this.itemReturnPersist.return_from_id){
    this.tcNotification.error("please select store from")
    return;
  }  if(this.itemReturnPersist.status != this.ReceiveStatus.received){
    this.tcNotification.error("please select received status")
    return;
  }
  this.itemReturnSelection = this.itemReturnSelection.filter(value => value.ref_no == null  && value.status == this.ReceiveStatus.received)
  if (this.itemReturnSelection.length)  
    this.printTransfers()
  else 
    this.tcNotification.error("no selected request without Ref No.")
}

secondPrint():void {
  if(!this.itemReturnPersist.ref_no){
    this.tcNotification.error("please enter the refernce no")
    return;
  }
  this.itemReturnSelection = this.itemReturnSelection.filter(value => value.ref_no == this.itemReturnPersist.ref_no && value.status == this.ReceiveStatus.received)
  if (this.itemReturnSelection.length)  
    this.printTransfers()
  else 
    this.tcNotification.error("no selected request with Ref No.")
}

printTransfers():void {
  let ids = ""
  this.itemReturnSelection.forEach((value, idx)=>{
    ids += ids =="" ? value.id : ","+ value.id;
  })

  let params = {ids : ids,ref_no: this.itemReturnPersist.ref_no, start_date: this.itemReturnPersist.from_date.value && (new Date(this.itemReturnPersist.from_date.value).getTime()/1000).toString(), end_date: this.itemReturnPersist.to_date.value && (new Date(this.itemReturnPersist.to_date.value).getTime()/1000).toString(), return_from_id : this.itemReturnPersist.return_from_id }
  this.itemReturnPersist.itemReturnsDo("print_item_return",params).subscribe((downloadJob: TCId) => {
    this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
      this.jobPersist.followJob( downloadJob.id,'printing returns', true);
      if (job.job_state == this.success_state) {
      }
    });
  });
}


searchStore() {
  let dialogRef = this.storeNavigator.pickStores(true);
  dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
    if (result) {
      this.itemReturnPersist.store_name = result[0].name;
      this.itemReturnPersist.store_id = result[0].id;
      this.searchItemReturns();
    }
  });
}

searchCategory(isSub: boolean = false){
  let dialogRef = this.categoryNavigator.pickItemCategorys(this.itemReturnPersist.category?.toString() , true)
  dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
    if(result){
      this.subCategoryName = result[0].name
      this.itemReturnPersist.categoryId = result[0].id;
    }
    this.searchItemReturns()
  })
}

  editItemReturn(item: ItemReturnSummary) {
    let dialogRef = this.itemReturnNavigator.editItemReturn(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  rejectRequest(request: ItemReturnDetail){
    let dialogRef = this.requestNavigator.addReason();
    dialogRef.afterClosed().subscribe(
      result => {
        if (result){
          this.respondRequest(request, 'reject', {reject_description : result.reason})
        }
      }
    )
  }

  respondRequest(request: ItemReturnDetail, respond: string, payload = {}) {
    this.itemReturnIsLoading = true;
    this.itemReturnPersist.itemReturnDo(request.id, respond, payload).subscribe(
      result => {
          if(result){
            this.tcNotification.success("request accepted")
          }
          this.itemReturnIsLoading = false;
          this.searchItemReturns()
      }, error => {
        this.itemReturnIsLoading = false;
        console.error(error)
      }
    )
  }

  deleteItemReturn(item: ItemReturnSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("item_return");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.itemReturnPersist.deleteItemReturn(item.id).subscribe(response => {
          this.tcNotification.success("item_return deleted");
          this.searchItemReturns();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getFromName(item: ItemReturnSummary): string {
      if (item.return_from == return_from.store){
        return this.stores[item.return_from_id]?.name || ""
      } else {
        return this.users[item.return_from_id]?.name || ""
      }
    }
}
