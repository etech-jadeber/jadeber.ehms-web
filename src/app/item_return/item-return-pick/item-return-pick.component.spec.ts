import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemReturnPickComponent } from './item-return-pick.component';

describe('ItemReturnPickComponent', () => {
  let component: ItemReturnPickComponent;
  let fixture: ComponentFixture<ItemReturnPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemReturnPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemReturnPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
