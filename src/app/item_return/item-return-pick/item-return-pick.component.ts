import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
// import {MatSort, MatPaginator, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ItemReturnSummary, ItemReturnSummaryPartialList } from '../item_return.model';
import { ItemReturnPersist } from '../item_return.persist';
import { ItemReturnNavigator } from '../item_return.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-item_return-pick',
  templateUrl: './item-return-pick.component.html',
  styleUrls: ['./item-return-pick.component.scss']
})export class ItemReturnPickComponent implements OnInit {
  itemReturnsData: ItemReturnSummary[] = [];
  itemReturnsTotalCount: number = 0;
  itemReturnSelectAll:boolean = false;
  itemReturnSelection: ItemReturnSummary[] = [];

 itemReturnsDisplayedColumns: string[] = ["select", ,"transfer_id","requester_id","responder_id","quantity","status" ];
  itemReturnSearchTextBox: FormControl = new FormControl();
  itemReturnIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) itemReturnsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemReturnsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemReturnPersist: ItemReturnPersist,
                public itemReturnNavigator: ItemReturnNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<ItemReturnPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("item_returns");
       this.itemReturnSearchTextBox.setValue(itemReturnPersist.itemReturnSearchText);
      //delay subsequent keyup events
      this.itemReturnSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.itemReturnPersist.itemReturnSearchText = value;
        this.searchItem_returns();
      });
    } ngOnInit() {
   
      this.itemReturnsSort.sortChange.subscribe(() => {
        this.itemReturnsPaginator.pageIndex = 0;
        this.searchItem_returns(true);
      });

      this.itemReturnsPaginator.page.subscribe(() => {
        this.searchItem_returns(true);
      });
      //start by loading items
      this.searchItem_returns();
    }

  searchItem_returns(isPagination:boolean = false): void {


    let paginator = this.itemReturnsPaginator;
    let sorter = this.itemReturnsSort;

    this.itemReturnIsLoading = true;
    this.itemReturnSelection = [];

    this.itemReturnPersist.searchItemReturn(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemReturnSummaryPartialList) => {
      this.itemReturnsData = partialList.data;
      if (partialList.total != -1) {
        this.itemReturnsTotalCount = partialList.total;
      }
      this.itemReturnIsLoading = false;
    }, error => {
      this.itemReturnIsLoading = false;
    });

  }
  markOneItem(item: ItemReturnSummary) {
    if(!this.tcUtilsArray.containsId(this.itemReturnSelection,item.id)){
          this.itemReturnSelection = [];
          this.itemReturnSelection.push(item);
        }
        else{
          this.itemReturnSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemReturnSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
