import {TCId} from "../tc/models";export class ItemReturnSummary extends TCId {
    target_type: number;
    target_id:string;
    requester_id:string;
    responder_id:string;
    quantity:number;
    status:number;
    return_from: number;
    return_from_id: string;
    ref_no: string;
     
    }
    export class ItemReturnSummaryPartialList {
      data: ItemReturnSummary[];
      total: number;
    }
    export class ItemReturnDetail extends ItemReturnSummary {
    }
    
    export class ItemReturnDashboard {
      total: number;
    }
    