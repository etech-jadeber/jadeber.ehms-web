import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ItemReturnEditComponent} from "./item-return-edit/item-return-edit.component";
import {ItemReturnPickComponent} from "./item-return-pick/item-return-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ItemReturnNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  itemReturnsUrl(): string {
    return "/item_returns";
  }

  itemReturnUrl(id: string): string {
    return "/item_returns/" + id;
  }

  viewItemReturns(): void {
    this.router.navigateByUrl(this.itemReturnsUrl());
  }

  viewItemReturn(id: string): void {
    this.router.navigateByUrl(this.itemReturnUrl(id));
  }

  editItemReturn(id: string): MatDialogRef<ItemReturnEditComponent> {
    const dialogRef = this.dialog.open(ItemReturnEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemReturn(tranferId: string): MatDialogRef<ItemReturnEditComponent> {
    const dialogRef = this.dialog.open(ItemReturnEditComponent, {
          data: new TCIdMode(tranferId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addItemDispatchReturn(item_dispatch_id: string): MatDialogRef<ItemReturnEditComponent> {
    const dialogRef = this.dialog.open(ItemReturnEditComponent, {
          data: new TCIdMode(item_dispatch_id, 'ITEM_DISPATCH'),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickItemReturns(selectOne: boolean=false): MatDialogRef<ItemReturnPickComponent> {
      const dialogRef = this.dialog.open(ItemReturnPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
