import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ItemReturnDashboard, ItemReturnDetail, ItemReturnSummaryPartialList} from "./item_return.model";
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from '../tc/utils-date';
import { return_from } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class ItemReturnPersist {
 itemReturnSearchText: string = "";
 status: number;
 categoryId: string;
 category: number;
 from_date: FormControl = new FormControl({disabled: true, value: ""});
 to_date: FormControl = new FormControl({disabled: true, value: ""}) 
  store_name: string;
  store_id: string;

  return_from_id: string;
  store_from_name: string;
  ref_no: string;

 
 constructor(private http: HttpClient,
  public tcUtilsDate: TCUtilsDate) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemReturnSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchItemReturn(pageSize: number, pageIndex: number, sort: string, order: string, isRequested: boolean = true): Observable<ItemReturnSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("item_return", this.itemReturnSearchText, pageSize, pageIndex, sort, order);
    if (this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    url = TCUtilsString.appendUrlParameter(url, "isRequested", `${isRequested}`)
    if (this.from_date.value){
      url = TCUtilsString.appendUrlParameter(url, "from_date", this.tcUtilsDate.toTimeStamp(this.from_date.value).toString())
    }
    if (this.to_date.value){
      url = TCUtilsString.appendUrlParameter(url, "to_date", this.tcUtilsDate.toTimeStamp(this.to_date.value).toString())
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }


    if(this.ref_no){
      url = TCUtilsString.appendUrlParameter(url, "ref_no", this.ref_no);
    }
       
    if(this.return_from_id){
      url = TCUtilsString.appendUrlParameter(url, "return_from_id", this.return_from_id)
    }

    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    return this.http.get<ItemReturnSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_return/do", new TCDoParam("download_all", this.filters()));
  }

  itemReturnDashboard(): Observable<ItemReturnDashboard> {
    return this.http.get<ItemReturnDashboard>(environment.tcApiBaseUri + "item_return/dashboard");
  }

  getItemReturn(id: string): Observable<ItemReturnDetail> {
    return this.http.get<ItemReturnDetail>(environment.tcApiBaseUri + "item_return/" + id);
  } addItemReturn(item: ItemReturnDetail): Observable<TCId> {
    item.return_from = return_from.store;
    return this.http.post<TCId>(environment.tcApiBaseUri + "item_return/", item);
  }

  updateItemReturn(item: ItemReturnDetail): Observable<ItemReturnDetail> {
    return this.http.patch<ItemReturnDetail>(environment.tcApiBaseUri + "item_return/" + item.id, item);
  }

  deleteItemReturn(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "item_return/" + id);
  }
 itemReturnsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_return/do", new TCDoParam(method, payload));
  }

  itemReturnDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "item_return/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "item_return/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "item_return/" + id + "/do", new TCDoParam("print", {}));
  }


}
