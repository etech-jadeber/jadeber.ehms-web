import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from 'rxjs';

import {TCAuthorization} from '../../tc/authorization';
import {TCUtilsAngular} from '../../tc/utils-angular';
import {TCNotification} from '../../tc/notification';
import {TCUtilsArray} from '../../tc/utils-array';
import {TCNavigator} from '../../tc/navigator';
import {JobPersist} from '../../tc/jobs/job.persist';
import {AppTranslation} from '../../app.translation';

import {ItemDetail} from '../item.model';
import {ItemPersist} from '../item.persist';
import {ItemNavigator} from '../item.navigator';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {StorePersist} from "../../stores/store.persist";
import {StoreNavigator} from "../../stores/store.navigator";
import { tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';

export enum PaginatorIndexes {}

export enum ItemTabs {
  overview
}


@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css'],
})


export class ItemDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  itemLoading: boolean = false;
  //basics
  itemDetail: ItemDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();


  itemTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  stores: StoreSummary[] = [];


  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public itemNavigator: ItemNavigator,
    public itemPersist: ItemPersist,
    public storePersist: StorePersist,
    public storeNavigator: StoreNavigator,
    public menuState:MenuState,
    public formEncounterPersist: Form_EncounterPersist,
    public categoryPersist: ItemCategoryPersist,
  ) {
    this.tcAuthorization.requireRead('items');
    this.itemDetail = new ItemDetail();
        //tab state manager
        this.menuState.currentTabResponseState.subscribe((res) => {
          this.activeTab = res;
        })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead('items');
    this.loadStores();
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.itemDetail.id = null;
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.itemLoading = true;
    this.itemPersist.getItem(id).subscribe(
      (itemDetail) => {
        this.itemDetail = itemDetail;
        this.categoryPersist.getItemCategory(this.itemDetail.sub_category_id).subscribe(
          category => {
            this.itemDetail.subcategory_name = category.name;
          }
        )
        this.menuState.getDataResponse(this.itemDetail);
        this.itemLoading = false;
      },
      (error) => {
        console.error(error);
        this.itemLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.itemDetail.id);
  }

  editItem(): void {
    let modalRef = this.itemNavigator.editItem(this.itemDetail.id);
    modalRef.afterClosed().subscribe(
      (modifiedItemDetail) => {
        TCUtilsAngular.assign(this.itemDetail, modifiedItemDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printItem(): void {
    this.itemPersist.print(this.itemDetail.id).subscribe((printJob) => {
      this.jobPersist.followJob(printJob.id, 'print item', true);
    });
  }

  back(): void {
    this.location.back();
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }
}
