import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { category, item_type, store_service_type } from 'src/app/app.enums';

import {TCAuthorization} from '../../tc/authorization';
import {TCNotification} from '../../tc/notification';
import {TCUtilsString} from '../../tc/utils-string';
import {AppTranslation} from '../../app.translation';

import {TCIdMode, TCModalModes} from '../../tc/models';

import {ItemDetail} from '../item.model';
import {ItemPersist} from '../item.persist';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {StoreSummary} from "../../stores/store.model";
import {StoreNavigator} from "../../stores/store.navigator";
import { DrugtypeSummary } from '../../drugtypes/drug_type.model';
import { DrugtypeNavigator } from '../../drugtypes/drug_type.navigator'
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { ItemNavigator } from '../item.navigator';

@Component({
  selector: 'app-item-edit',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css'],
})
export class ItemEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  itemDetail: ItemDetail;
  category = category
  storeName: string;
  isDrug: boolean = false;
  categoryId: string;
  item_options : ItemDetail[] = [];
  selectedNameFnc: () => void

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<ItemEditComponent>,
    public persist: ItemPersist,
    public tcUtilsDate: TCUtilsDate,
    public itemNav: ItemNavigator,
    public storeNavigator: StoreNavigator,
    public drug_TypeNavigator: DrugtypeNavigator,
    public itemPersist: ItemPersist,
    public categoryNavigator: ItemCategoryNavigator,
    public categoryPersist: ItemCategoryPersist,
    public formEncounterPersist: Form_EncounterPersist,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('items');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        this.appTranslation.getText('inventory', 'items');
      this.itemDetail = new ItemDetail();
      this.itemDetail.item_description = "";
      // this.itemDetail.brand_name = "";
      this.selectedNameFnc = () => {}
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('items');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        this.appTranslation.getText('inventory', 'items');
      this.isLoadingResults = true;
      this.persist.getItem(this.idMode.id).subscribe(
        (itemDetail) => {
          this.itemDetail = itemDetail;
          this.categoryPersist.getItemCategory(this.itemDetail.sub_category_id).subscribe(
            category => {
              this. itemDetail.subcategory_name = category.name;
            });
            }
          )
          this.isLoadingResults = false;
        }
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.itemDetail.store_type = store_service_type.pharmacy;
    this.itemDetail.search_column = this.itemNav.itemName(this.itemDetail);
    this.persist.addItem(this.itemDetail).subscribe(
      (value) => {
        this.tcNotification.success('Item added');
        this.itemDetail.id = value.id;
        this.dialogRef.close(this.itemDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.itemDetail.store_type = store_service_type.pharmacy;
    this.itemDetail.search_column = this.itemNav.itemName(this.itemDetail);
    this.persist.updateItem(this.itemDetail).subscribe(
      (value) => {
        this.tcNotification.success('Item updated');
        this.dialogRef.close(this.itemDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.itemDetail == null) {
      return false;
    }

    if (this.itemDetail.name == null || this.itemDetail.name == "") {
      return false;
    }

    // if (this.itemDetail.sub_category_id == null || this.itemDetail.sub_category_id == ""){
    //   return false;
    // }
 
     if (this.itemDetail.item_description == null) {
              return false;
            }
    // if (this.itemDetail.brand_name == null) {
    //   return false;
    // }
    // if (this.itemDetail.item_code == null || this.itemDetail.item_code == "") {
    //   return false;
    // }

    // if(this.itemDetail.category == null ){
    //   return false;
    // }
    // if(this.itemDetail.strength == null || this.itemDetail.unit == null || this.itemDetail.preparation == null)
    //   return false;
 
    return true;
  }


  updateItems(event:string,input):void{
    this.itemPersist.itemSearchText = input.value;
    this.loadItems();
  }
  loadItems(){
    this.itemPersist.searchItem(5,0,"","").subscribe((result)=>{
      if(result)
        this.item_options = result.data;
    })
  }

  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.itemDetail.category.toString(), true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result.length){
        this.itemDetail.subcategory_name = result[0].name
        this.itemDetail.sub_category_id = result[0].id;
    }
    })
  }

  ngOnDestroy():void {
    this.itemPersist.itemSearchText = "";
  }

}


