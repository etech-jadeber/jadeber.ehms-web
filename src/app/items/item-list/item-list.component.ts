import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from 'rxjs/operators';
import {Location} from '@angular/common';

import {TCAuthorization} from '../../tc/authorization';
import {TCUtilsAngular} from '../../tc/utils-angular';
import {TCUtilsArray} from '../../tc/utils-array';
import {TCUtilsDate} from '../../tc/utils-date';
import {TCNotification} from '../../tc/notification';
import {TCNavigator} from '../../tc/navigator';
import {JobPersist} from '../../tc/jobs/job.persist';

import {AppTranslation} from '../../app.translation';

import {ItemPersist} from '../item.persist';
import {ItemNavigator} from '../item.navigator';
import {ItemDetail, ItemSummary, ItemSummaryPartialList} from '../item.model';
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {StorePersist} from "../../stores/store.persist";
import {StoreNavigator} from "../../stores/store.navigator";
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCId } from 'src/app/tc/models';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css'],
})
export class ItemListComponent implements OnInit {
  itemsData: ItemSummary[] = [];
  itemsTotalCount: number = 0;
  itemsSelectAll: boolean = false;
  itemsSelection: ItemSummary[] = [];
  stores: StoreSummary[] = [];

  success_state = 3;
  message: string = ''; 

  itemsDisplayedColumns: string[] = [
    'select',
    'action',
    'name',
    // 'item_code',
    // 'category',
    // "sub_category",
    // "item_description"
  ];
  itemsSearchTextBox: FormControl = new FormControl();
  itemsIsLoading: boolean = false;
  showReorder: boolean = false;

  @ViewChild(MatPaginator, {static: true}) itemsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public itemPersist: ItemPersist,
    public itemNavigator: ItemNavigator,
    public storePersist: StorePersist,
    public storeNavigator: StoreNavigator,
    public jobPersist: JobPersist,
    public formEncounterPersist: Form_EncounterPersist,
    public categoryPersist: ItemCategoryPersist
  ) {
    //this.tcAuthorization.requireRead('items');
    this.itemsSearchTextBox.setValue(itemPersist.itemSearchText);
    //delay subsequent keyup events
    this.itemsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.itemPersist.itemSearchText = value;
        this.searchItems();
      });
  }

  ngOnInit() {
    this.itemsSort.sortChange.subscribe(() => {
      this.itemsPaginator.pageIndex = 0;
      this.searchItems(true);
    });

    this.itemsPaginator.page.subscribe(() => {
      this.searchItems(true);
    });
    //start by loading items
    this.searchItems();
  }

  searchItems(isPagination: boolean = false): void {
    let paginator = this.itemsPaginator;
    let sorter = this.itemsSort;

    this.itemsIsLoading = true;
    this.showReorder = false;
    this.itemsSelection = [];

    this.itemPersist
      .searchItem(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: ItemSummaryPartialList) => {
          this.itemsData = partialList.data;
          this.itemsData.forEach(item => {
            this.categoryPersist.getItemCategory(item.sub_category_id).subscribe(
              category => {
                item.subcategory_name = category.name
              }
            )
          })
          if (partialList.total != -1) {
            this.itemsTotalCount = partialList.total;
          }
          this.itemsIsLoading = false;
        },
        (error) => {
          this.itemsIsLoading = false;
        }
      );
  }

  downloadItems(): void {
    if (this.itemsSelectAll) {
      this.itemPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download items', true);
      });
    } else {
      this.itemPersist
        .download(this.tcUtilsArray.idsList(this.itemsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'download items', true);
        });
    }
  }





  addItem(): void {
    let dialogRef = this.itemNavigator.addItem();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchItems();
      }
    });
  }

  editItem(item: ItemSummary) {
    let dialogRef = this.itemNavigator.editItem(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchItems();
      }
    });
  }


  exportItems(): void {
    this.itemPersist.itemsDo('item_list', {} ).subscribe(
      (downloadJob: TCId) => {
        this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
          this.jobPersist.followJob( downloadJob.id,'exporting items', true);
          if (job.job_state == this.success_state) {
            // item.print_status = PrintStatus.printed
            // this.printRequestPersist.updatePrintRequest(item)
          }
        });
      },
      (error) => {}
    );
  

  }

  deleteItem(item: ItemSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Item');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.itemPersist.deleteItem(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Item deleted');
            this.searchItems();
          },
          (error) => {
          }
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }

}
