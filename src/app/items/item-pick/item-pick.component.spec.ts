import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ItemPickComponent } from './item-pick.component';

describe('ItemPickComponent', () => {
  let component: ItemPickComponent;
  let fixture: ComponentFixture<ItemPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
