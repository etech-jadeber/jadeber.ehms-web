import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { debounceTime } from 'rxjs/operators';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { AppTranslation } from '../../app.translation';

import { ItemDetail, ItemSummary, ItemSummaryPartialList } from '../item.model';
import { ItemPersist } from '../item.persist';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';

@Component({
  selector: 'app-item-pick',
  templateUrl: './item-pick.component.html',
  styleUrls: ['./item-pick.component.css'],
})
export class ItemPickComponent implements OnInit {
  itemsData: ItemSummary[] = [];
  itemsTotalCount: number = 0;
  itemsSelection: ItemSummary[] = [];
  selectOne: boolean;
  itemsDisplayedColumns: string[] = [
    'select',
    'name',
    'item_code',
    'category',
    "sub_category",
    "item_description"
  ];

  itemsSearchTextBox: FormControl = new FormControl();
  itemsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) itemsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) itemsSort: MatSort;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public itemPersist: ItemPersist,
    public categoryPersist: ItemCategoryPersist,
    public formEncounterPersist: Form_EncounterPersist,
    public dialogRef: MatDialogRef<ItemPickComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    // this.tcAuthorization.requireRead('items');
    // selectOne
    this.selectOne=data.selectOne;
    this.itemsSearchTextBox.setValue(itemPersist.itemSearchText);
    //delay subsequent keyup events
    this.itemsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.itemPersist.itemSearchText = value;
        this.searchItems();
      });
  }

  ngOnInit() {
    this.selectOne=this.data.selectOne;
    this.itemsSort.sortChange.subscribe(() => {
      this.itemsPaginator.pageIndex = 0;
      this.searchItems();
    });

    this.itemsPaginator.page.subscribe(() => {
      this.searchItems();
    });

    //set initial picker list to 5
    this.itemsPaginator.pageSize = 5;

    //start by loading items
    this.searchItems();
  }

  searchItems(): void {
    this.itemsIsLoading = true;
    // this.itemsSelection = [];

    this.itemPersist.category=this.data.category;
    this.itemPersist
      .searchItem(
        this.itemsPaginator.pageSize,
        this.itemsPaginator.pageIndex,
        this.itemsSort.active,
        this.itemsSort.direction,
      )
      .subscribe(
        (partialList: ItemSummaryPartialList) => {
          this.itemsData = partialList.data;
          this.itemsData.forEach(item => {
            this.categoryPersist.getItemCategory(item.sub_category_id).subscribe(
              category => {
                item.subcategory_name = category.name
              }
            )
          })
          if (partialList.total != -1) {
            this.itemsTotalCount = partialList.total;
          }
          this.itemsIsLoading = false;
        },
        (error) => {
          this.itemsIsLoading = false;
        }
      );
  }

  markOneItem(item: ItemSummary) {
    if (!this.tcUtilsArray.containsId(this.itemsSelection, item.id)) {
      this.itemsSelection = [];
      this.itemsSelection.push(item);
    } else {
      this.itemsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.itemsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
