import { TCId } from '../tc/models';

export class ItemSummary extends TCId {
  item_code: string;
  reorder_point : number;
  name: string;
  status: number;
  item_description: string;
  subcategory_name: string;
  sub_category_id: string;
  category : number;
  store_type: number ;
  preparation : number;
  strength:number;
  unit:number;
  brand_name:string;
  search_column :string;

}

export class ItemSummaryPartialList {
  data: ItemSummary[];
  total: number;
}

export class ItemExpireDateReport{
  id: string;
  item_name: string;
  details: string;
  price: number;
  convertedDetail: {batch_no: string;
  quantity: number;
  expire_date: number}[] = []}

export class ItemExpireDateReportPartialList {
  data: ItemExpireDateReport[];
  total: number;
}

export class ItemDetail extends ItemSummary {
}

export class ItemDashboard {
  total: number;
}
