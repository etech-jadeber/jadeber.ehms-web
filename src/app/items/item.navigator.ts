import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { ItemEditComponent } from './item-edit/item-edit.component';
import { ItemPickComponent } from './item-pick/item-pick.component';
import { ItemDetail } from './item.model';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { TCUtilsArray } from '../tc/utils-array';
import { category } from '../app.enums';
import { el } from 'date-fns/locale';

@Injectable({
  providedIn: 'root',
})
export class ItemNavigator {
  constructor(private router: Router,
    public dialog: MatDialog,
    public formEncounterPersist: Form_EncounterPersist,
    public tcUtilsArray: TCUtilsArray,
    ) {}

  itemsUrl(): string {
    return '/items';
  }

  itemUrl(id: string): string {
    return '/items/' + id;
  }

  viewItems(): void {
    this.router.navigateByUrl(this.itemsUrl());
  }

  viewItem(id: string): void {
    this.router.navigateByUrl(this.itemUrl(id));
  }


  itemName(element: ItemDetail|any):string{
    if(element == null){
      return "";
    }
    return element.name
      // return element.name + "-" + element.strength + this.tcUtilsArray.getEnum(this.formEncounterPersist.dosageUnit, element.unit)?.name + "-" + this.tcUtilsArray.getEnum(this.formEncounterPersist.drug_form, element.preparation)?.name +(element.brand_name ? "("+element.brand_name+")":"")
  }

  editItem(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ItemEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addItem(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ItemEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickItems(selectOne: boolean = false,category:number=null): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ItemPickComponent, {
      data: {"selectOne":selectOne,"category":category},
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
