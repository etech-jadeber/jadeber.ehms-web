import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

import {TCDoParam, TCUrlParams, TCUtilsHttp} from '../tc/utils-http';
import {TcDictionary, TCEnum, TCId} from '../tc/models';
import {ItemDashboard, ItemDetail, ItemExpireDateReport, ItemExpireDateReportPartialList, ItemSummaryPartialList,} from './item.model';
import {equipment_type, item_type,product_category, store_service_type} from '../app.enums';
import {TCUtilsString} from '../tc/utils-string';
import { FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ItemPersist {
  itemSearchText: string = '';

  category: number;
  store_type: number = store_service_type.pharmacy;
  transfer_store_type:number;

  item_type: TCEnum[] = [
    new TCEnum(item_type.drug, 'drug'),
    new TCEnum(item_type.consumable, 'consumable'),
    new TCEnum(item_type.equipment, 'equipment'),
  ];

  product_category: TCEnum[] = [
    new TCEnum(product_category.Ointment, 'Ointment'),
    new TCEnum(product_category.Cream, 'Cream'),
    new TCEnum(product_category.Injection, 'Injection'),
    new TCEnum(product_category.Suspension, 'Suspension'),
    new TCEnum(product_category.Capsule, 'Capsule'),
    new TCEnum(product_category.Inhaler, 'Inhaler'),
    new TCEnum(product_category.Lotion, 'Lotion'),
    new TCEnum(product_category.Syrup, 'Syrup'),
    new TCEnum(product_category.OralRinse, 'OralRinse'),
    new TCEnum(product_category.Drops, 'Drops'),
    new TCEnum(product_category.Solution, 'Solution'),
    new TCEnum(product_category.MouthPaint, 'MouthPaint'),
    new TCEnum(product_category.Pessary, 'Pessary'),
    new TCEnum(product_category.Gel, 'Gel'),
    new TCEnum(product_category.OralSolution, 'OralSolution'),
    new TCEnum(product_category.Powder, 'Powder'),
    new TCEnum(product_category.NasalSolution, 'NasalSolution'),
    new TCEnum(product_category.Rotacap, 'Rotacap'),
  ];

  from_date: number;
  to_date: number;
  store_name: any;
  store_id: any;


  constructor(private http: HttpClient) {}

  searchItem(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<ItemSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'items',
      this.itemSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if (this.category) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'category',
        this.category.toString()
      );
    }
    if (this.store_type) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'store_type',
        this.store_type.toString()
      );
    }
    if (this.transfer_store_type) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'transfer_store_type',
        this.transfer_store_type.toString()
      );
    }


    return this.http.get<ItemSummaryPartialList>(url);
  }

  searchItemExpireDateReport(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<ItemExpireDateReportPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'items/do',
      this.itemSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if (this.from_date) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'from_date',
        this.from_date.toString()
      );
    }
    if (this.store_id) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'store_id',
        this.store_id
      );
    }
    if (this.to_date) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'to_date',
        this.to_date.toString()
      );
    }

    return this.http.post<ItemExpireDateReportPartialList>(url, new TCDoParam("expire_date_report", {}));
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.itemSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'items/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  itemDashboard(): Observable<ItemDashboard> {
    return this.http.get<ItemDashboard>(
      environment.tcApiBaseUri + 'items/dashboard'
    );
  }

  getItem(id: string): Observable<ItemDetail> {
    return this.http.get<ItemDetail>(environment.tcApiBaseUri + 'items/' + id);
  }

  addItem(item: ItemDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + 'items/', item);
  }

  updateItem(item: ItemDetail): Observable<ItemDetail> {
    return this.http.patch<ItemDetail>(
      environment.tcApiBaseUri + 'items/' + item.id,
      item
    );
  }

  deleteItem(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'items/' + id);
  }

  itemsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'items/do',
      new TCDoParam(method, payload)
    );
  }

  itemDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'items/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'items/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'items/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
