import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabOrdersListComponent } from './lab-orders-list.component';

describe('LabOrdersListComponent', () => {
  let component: LabOrdersListComponent;
  let fixture: ComponentFixture<LabOrdersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabOrdersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabOrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
