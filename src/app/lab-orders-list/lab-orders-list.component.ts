import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData} from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCAuthentication } from '../tc/authentication';
import { UserPersist } from '../tc/users/user.persist';
import { Lab_OrderSummary } from '../lab_orders/lab_order.model';
import { Lab_PanelDetail, Lab_PanelSummary, Order, SummarizedTest, Test_PanelDetail, Test_PanelSummary } from '../lab_panels/lab_panel.model';
import { ExtendedLab_TestSummary, Lab_TestDetail, Lab_TestSummary } from '../lab_tests/lab_test.model';
import { History_DataSummary } from '../patients/patients.model';
import {ActivenessStatus, lab_order_type, lab_urgency, OrderType, physican_type, physician_type, procedure_for} from 'src/app/app.enums';
import { Lab_OrderPersist } from '../lab_orders/lab_order.persist';
import { Lab_PanelPersist } from '../lab_panels/lab_panel.persist';
import { Lab_TestPersist } from '../lab_tests/lab_test.persist';
import { OrdersPersist } from '../form_encounters/orderss/orders.persist';
import {TCUtilsString} from 'src/app/tc/utils-string';
import { DoctorNavigator } from '../doctors/doctor.navigator';
import { DoctorDetail } from '../doctors/doctor.model';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { CommonOrdersDetail, OrdersDetail, OrdersSummary, Selected_OrdersDetail, Selected_OrdersSummary, Selected_OrdersSummaryPartialList } from '../form_encounters/orderss/orders.model';
import { MatDialogRef } from '@angular/material/dialog';
import { TCModalWidths } from '../tc/utils-angular';
import { ProcedureDetail, ProcedureSummary } from '../procedures/procedure.model';
import { ProcedurePersist } from '../procedures/procedure.persist';
import { Patient_ProcedureDetail } from '../form_encounters/form_encounter.model';
import { Procedure_OrderDetail } from '../form_encounters/procedure_orders/procedure_order.model';

@Component({
  selector: 'app-lab-orders-list',
  templateUrl: './lab-orders-list.component.html',
  styleUrls: ['./lab-orders-list.component.css']
})
export class LabOrdersListComponent implements OnInit {

  isLoadingResults: boolean = false;
  labOrders: Lab_OrderSummary[] = [];
  radiology: Lab_OrderSummary[] = [];
  pathology: Lab_OrderSummary[] = [];
  labPanels: Lab_PanelSummary[] = [];
  filteredLabPanels: Lab_PanelSummary[] = [];
  labTests: Lab_TestSummary[] = [];
  selectedLabOrderId: string;
  filteredlabTests: Lab_TestSummary[] = [];
  history_datasData: History_DataSummary[] = [];
  history_datasTotalCount: number = 0;
  radPanels: Lab_PanelSummary[] = [];
  filteredradPanels: Lab_PanelSummary[] = [];
  patPanels: Lab_PanelSummary[] = [];
  filteredpatPanels: Lab_PanelSummary[] = [];
  radTests: Lab_TestSummary[] = [];
  filteredradTests: Lab_TestSummary[] = [];
  patTests: Lab_TestSummary[] = [];
  filteredpatTests: Lab_TestSummary[] = [];
  selectedPatOrderId: string;
  selectedRadOrderId: string
  lab_order_type = lab_order_type;
  showPrevious: boolean = false;

  selectedLabPanel: Lab_PanelSummary[] = [];
  selectedLabTest: ExtendedLab_TestSummary[] = [];

  selectedradPanel: Lab_PanelSummary[] = [];
  selectedradTest: ExtendedLab_TestSummary[] = [];

  selectedpatPanel: Lab_PanelSummary[] = [];
  selectedpatTest: ExtendedLab_TestSummary[] = [];

  lab_urgency: typeof lab_urgency = lab_urgency;
  laburgency: number = lab_urgency.normal;
  radurgency: number = lab_urgency.normal;
  futureRadDate: Date = new Date();
  futureLab: boolean = false;
  futureRad: boolean = false;
  labNote: string;
  radNote: string;
  patNote: string;
  procedureNote: string;
  lab_testPanel: Test_PanelSummary[] = [];
  futureLabDate: Date = new Date();
  summarizedTest: SummarizedTest[] = [];
  radiologistName: string;
  laboratorySearchTextBox: FormControl = new FormControl();
  radiologySearchTextBox:  FormControl = new FormControl();
  pathologySearchTextBox:  FormControl = new FormControl();
  procedureSearchTextBox:  FormControl = new FormControl();
  proceduresSelection: ProcedureSummary[] = [];
  filteredproceduresData: ProcedureSummary[] = []
  proceduresData: ProcedureSummary[] = [];
  radiologistId: string;
  selectedOrders : Selected_OrdersDetail[] = []
  selectedProcedures : Patient_ProcedureDetail[] = []
  selectedPatientProcedures : Patient_ProcedureDetail[] = []
  orderedTests = {
    [lab_order_type.imaging]: this.radTests,
    [lab_order_type.pathology]: this.patTests,
    [lab_order_type.lab]: this.labTests,
  }
  orders: OrdersSummary[]  = [];
  @Input() encounterId: string;
  @Input() isDialog: boolean = false;
  @Input() orderType: number[] = [lab_order_type.imaging, lab_order_type.lab, lab_order_type.pathology];
  @Input() forbiddenSelect: Selected_OrdersSummary[] = [];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  invalidId:string = TCUtilsString.getInvalidId();
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsString: TCUtilsString,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public lab_orderPersist: Lab_OrderPersist,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_testPersist: Lab_TestPersist,
    public orderPersisit: OrdersPersist,
    public doctorNavigator: DoctorNavigator,
    public procedurePersist: ProcedurePersist,
    public patientProcedurePersist: Form_EncounterPersist
  ) {
    if(!(this.tcAuthorization.canCreate('laboratory_orders') || this.tcAuthorization.canCreate('radiology_orders') || this.tcAuthorization.canCreate('pathology_orders'))){
      tcNavigator.dialog.closeAll()
      tcNotification.error("You can not create order")
  }
  this.laboratorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
    this.filteredLabPanels = this.labPanels.filter(lab_panel => lab_panel.name.match(new RegExp(value, 'gi')));
    this.filteredlabTests = this.labTests.filter(lab_test => lab_test.name.match(new RegExp(value, 'gi')));
  });
  this.radiologySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
    this.filteredradPanels = this.radPanels.filter(rad_panel => rad_panel.name.match(new RegExp(value, 'gi')));
    this.filteredradTests = this.radTests.filter(rad_test => rad_test.name.match(new RegExp(value, 'gi')));
  });
  this.pathologySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
    this.filteredpatPanels = this.patPanels.filter(pat_panel => pat_panel.name.match(new RegExp(value, 'gi')));
    this.filteredpatTests = this.patTests.filter(pat_test => pat_test.name.match(new RegExp(value, 'gi')));
  });
  this.procedureSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
    this.filteredproceduresData = this.proceduresData.filter(procedure => procedure.name.match(new RegExp(value, 'gi')));
  });
   }

  ngOnInit(): void {
    this.reload()
  }

  getSelectedTest(order_type: number){
    switch (order_type) {
      case lab_order_type.imaging:
        return this.selectedradTest;
        case lab_order_type.pathology:
          return this.selectedpatTest;
          case lab_order_type.lab:
        return this.selectedLabTest;
    }
  }

  getSelectedPanel(order_type: number){
    switch (order_type) {
      case lab_order_type.imaging:
        return this.selectedradPanel;
        case lab_order_type.pathology:
          return this.selectedpatPanel;
          case lab_order_type.lab:
        return this.selectedLabPanel;
    }
  }

  reload(order_type: number[] = this.orderType): void {
    this.orderType = order_type;
    this.isLoadingResults = true
    if(this.isOrderContain(lab_order_type.lab)){
      this.getALLLabTypes();
    }
    if(this.isOrderContain(lab_order_type.imaging)){
      this.getAllRadiologyTypes();
    }
    if(this.isOrderContain(lab_order_type.pathology)){
      this.getAllPathologyTypes();
    }
    if (this.orderType.length){
      this.getAllLabTestPanels();
    }
    if(this.encounterId){
      this.getAllSelectedOrders();
      this.getOrders()
      this.getProcedures();
      this.getAllSelectedProcedure();
    }
  }


  getOrders(isPagination: boolean = false){
    let paginator = this.paginator;
    this.orderPersisit.searchOrders(this.encounterId, 10,
      isPagination ? paginator.pageIndex : 0,
      'created_at',
      'desc').subscribe(
        (orders) => {
          this.orders = orders.data;
        }
      )

  }

  getAllSelectedOrders(){
    this.isLoadingResults = true
    this.orderPersisit.selected_orderssDo(this.encounterId, "get_all", {}).subscribe(
      (result : Selected_OrdersSummaryPartialList) => {
        this.selectedOrders = result.data;
        this.isLoadingResults = false
      },error => {
        console.log(error)
        this.isLoadingResults = false;
      } 
    )
  }

  getAllSelectedProcedure(){
    this.isLoadingResults = true
    this.patientProcedurePersist.patient_proceduresDo("get_all", {encounter_id: this.encounterId}).subscribe(
      (result : Patient_ProcedureDetail[]) => {
        this.selectedProcedures = result;
        this.isLoadingResults = false
      },error => {
        console.log(error)
        this.isLoadingResults = false;
      } 
    )
  }

  reset() {
    this.selectedLabPanel = [];
    this.selectedLabTest = [];
    this.selectedradPanel = [];
    this.selectedpatPanel = [];
    this.selectedpatTest = [];
    this.selectedradTest = [];
  }

  getProcedures(){
    this.procedurePersist.proceduresDo("get_all", {procedure_for: procedure_for.diagnostic}).subscribe((partialList: ProcedureDetail[]) => {
      this.proceduresData = partialList;
      this.filteredproceduresData = partialList
    }, error => {
    });
  }

  getAllOrder(): {[id: number]: CommonOrdersDetail} {
    return {
      [lab_order_type.imaging]: {future: this.futureRad,
    futureDate: this.tcUtilsDate.toTimeStamp(this.futureRadDate),
    urgency: this.radurgency,
    to_be_resulted_by: this.radiologistId,
    selectedLabPanel: this.selectedradPanel,
    selectedLabTest: this.selectedradTest,
    encounter_id: this.encounterId},
    [lab_order_type.lab]:{
      future: this.futureLab,
      futureDate: this.tcUtilsDate.toTimeStamp(this.futureLabDate),
      urgency: this.laburgency,
      selectedLabPanel: this.selectedLabPanel,
      selectedLabTest: this.selectedLabTest,
      encounter_id: this.encounterId,
    },
    [lab_order_type.pathology]:{
      future: this.futureRad,
      futureDate: this.tcUtilsDate.toTimeStamp(this.futureRadDate),
      urgency: this.radurgency,
      selectedLabPanel: this.selectedpatPanel,
      selectedLabTest: this.selectedpatTest,
      encounter_id: this.encounterId,
    }}
  }


  addLab(
    isWizard: boolean = false,
    date: number = this.tcUtilsDate.toTimeStamp(new Date())
  ): void {
    this.tcAuthorization.canCreate("laboratory_orders");
    let dialogRef = this.form_encounterNavigator.addLab(
      isWizard,
      this.encounterId,
      null,
      date,
      [lab_order_type.imaging, lab_order_type.pathology, lab_order_type.lab]
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.tcNotification.success('sucess');
      }
    });
  }

  canSubmit(): boolean{
    let canSubmit = false;
    if (this.selectedLabPanel.length || this.selectedLabTest.length ){
      canSubmit = true;
    }
    if (this.selectedpatPanel.length || this.selectedpatTest.length ){
      canSubmit = true;
    }
    if (this.selectedradPanel.length || this.selectedradTest.length ){
      canSubmit = true;
    }
    return canSubmit;
  }

  getBSAStatus(bsa: number) {
    return 'Normal';
  }

  isOrderContain(order_type: number){
    return this.orderType.some(order => order == order_type)
  }


  getALLLabTypes(): void {
    this.isLoadingResults = true
    if(this.isOrderContain(lab_order_type.lab)){
      this.lab_orderPersist
      .lab_ordersDo('get_all', { type: lab_order_type.lab })
      .subscribe((labOrders: Lab_OrderSummary[]) => {
        this.labOrders = [{id: this.tcUtilsString.invalid_id, name: "All Laboratory", status: ActivenessStatus.active, type: lab_order_type.lab},...labOrders];
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
    }
  }

  getAllRadiologyTypes(): void {
    this.isLoadingResults = true
    if(this.isOrderContain(lab_order_type.imaging)){
    this.lab_orderPersist
      .lab_ordersDo('get_all', { type: lab_order_type.imaging })
      .subscribe((radOrders: Lab_OrderSummary[]) => {
        this.radiology = [{id: this.tcUtilsString.invalid_id, name: "All Radiology", status: ActivenessStatus.active, type: lab_order_type.imaging},...radOrders];
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
    }
  }

  getAllPathologyTypes(): void {
    this.isLoadingResults = true
    if(this.isOrderContain(lab_order_type.pathology)){
    this.lab_orderPersist
      .lab_ordersDo('get_all', { type: lab_order_type.pathology })
      .subscribe((patOrders: Lab_OrderSummary[]) => {
        this.pathology = [{id: this.tcUtilsString.invalid_id, name: "All Pathology", status: ActivenessStatus.active, type: lab_order_type.pathology},...patOrders];
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
    }
  }

  addSelectedOrder(lab_panels:  Lab_PanelDetail[], lab_tests:  Lab_TestDetail[], order_id: string) {
      if (!lab_panels.length && !lab_tests.length){
        this.isLoadingResults = false;
        this.tcNotification.info("You didn't add any additional")
      }
      lab_panels.forEach((panel, idx, array) => {
        this.orderPersisit.addSelected_Orders({lab_panel_id: panel.id, lab_test_id: this.tcUtilsString.invalid_id, order_id, lab_order_id: panel.lab_order_id}).subscribe(
          (value) => {
            if ((array.length - 1 )== idx && !lab_tests.length ){
              this.tcNotification.success("added")
              this.isLoadingResults = false;
            }
          }, error => {
            console.log(error);
            this.isLoadingResults = false;
          }
        )
      })
      lab_tests.forEach((panel,idx, array ) => {
        this.orderPersisit.addSelected_Orders({lab_test_id: panel.id, lab_panel_id: this.tcUtilsString.invalid_id, order_id, lab_order_id: panel.lab_order_id}).subscribe(
          (value) => {
            if ((array.length - 1 )== idx){
              this.tcNotification.success("added")
              this.isLoadingResults = false;
            }
          }, error => {
            console.log(error);
            this.isLoadingResults = false;
          }
        )
      })
  }

  setLabType(order_id: string) {
    this.selectedLabOrderId = order_id;
    this.isLoadingResults = true
    this.lab_panelPersist
      .lab_panelsDo('get_by_type', { order_id, type: lab_order_type.lab })
      .subscribe((labPanels: Lab_PanelSummary[]) => {
        this.labPanels = labPanels;
        this.filteredLabPanels = labPanels
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
      this.isLoadingResults = true
    this.lab_testPersist
      .lab_testsDo('get_by_type', { order_id, type: lab_order_type.lab })
      .subscribe((labTests: Lab_TestSummary[]) => {
        this.labTests = labTests;
        this.filteredlabTests = labTests.sort((previous, next) => {
          if (previous.test_sequence === null) {
            return 1;
          }
        
          if (next.test_sequence === null) {
            return -1;
          }
        
          if (previous.test_sequence === next.test_sequence) {
            return 0;
          }
          return previous.test_sequence - next.test_sequence
      });
        this.orderedTests[lab_order_type.lab] = this.labTests
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
  }

  setRadType(order_id: string) {
    this.isLoadingResults = true
    this.selectedRadOrderId = order_id
    this.lab_panelPersist
      .lab_panelsDo('get_by_type', {order_id, type: lab_order_type.imaging })
      .subscribe((labPanels: Lab_PanelSummary[]) => {
        this.radPanels = labPanels;
        this.filteredradPanels = labPanels
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
      this.isLoadingResults = true
    this.lab_testPersist
      .lab_testsDo('get_by_type', {order_id, type: lab_order_type.imaging })
      .subscribe((labTests: Lab_TestSummary[]) => {
        this.radTests = labTests;
        this.orderedTests[lab_order_type.imaging] = this.radTests
        this.filteredradTests = labTests.sort((previous, next) => {
          return previous.test_sequence - next.test_sequence
  });
  this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
  }

  setPatType(order_id: string) {
    this.isLoadingResults = true
    this.selectedPatOrderId = order_id
    this.lab_panelPersist
      .lab_panelsDo('get_by_type', {order_id, type: lab_order_type.pathology })
      .subscribe((labPanels: Lab_PanelSummary[]) => {
        this.patPanels = labPanels;
        this.filteredpatPanels = labPanels
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
      this.isLoadingResults = true
    this.lab_testPersist
      .lab_testsDo('get_by_type', {order_id, type: lab_order_type.pathology })
      .subscribe((labTests: Lab_TestSummary[]) => {
        this.patTests = labTests;
        this.orderedTests[lab_order_type.pathology] = this.patTests
        this.filteredpatTests = labTests.sort((previous, next) => {
          return previous.test_sequence - next.test_sequence
  });
  this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
  }

  orderLabNow(): void {
    this.isLoadingResults = true
    const order = () => {
      let payload = {
        future: this.futureLab,
        futureDate: this.tcUtilsDate.toTimeStamp(this.futureLabDate),
        urgency: this.laburgency,
        selectedLabPanel: this.selectedLabPanel,
        selectedLabTest: this.selectedLabTest,
        encounter_id: this.encounterId,
        note: this.labNote
      };
      this.orderPersisit
        .orderssDo('lab_new_order', payload)
        .subscribe((res) => {
          this.selectedLabPanel = [];
          this.selectedLabTest = [];
          this.tcNotification.success('Added');
          this.reload()
          this.isLoadingResults = false
          this.labNote = ''
        }, error => {
          console.log(error)
          this.isLoadingResults = false
        });
    }
    if (this.orders.some(order => (!order.generated_sample_id && order.order_type == lab_order_type.lab))){
      const notGeneratedOrder = this.orders.find(order => (!order.generated_sample_id && order.order_type == lab_order_type.lab))
      const filteredOrder = this.selectedOrders.filter(selected => notGeneratedOrder.id == selected.order_id);
      let dialogRef = this.tcNavigator.confirmAction("AS New Order","Confirm", "The patient has order which sample is not collected. What did you want to take this order?  " , "Add on Previous", TCModalWidths.medium, 'new_order', 'add_previous');
      dialogRef.afterClosed().subscribe(result => {
        if (result == 'add_previous') {
          const labPanel = this.selectedLabPanel.filter(panel => !filteredOrder.some(selected => selected.lab_panel_id == panel.id))
          const labTest = this.selectedLabTest.filter(test => !filteredOrder.some(selected => selected.lab_test_id == test.id))
          this.addSelectedOrder(labPanel, labTest, notGeneratedOrder.id)
        } else if (result == 'new_order'){
          order()
        } else {
          this.isLoadingResults = false;
        }
  
      });
    } 
     else if (this.selectedLabPanel.some((value) => this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_panel_id', value.id)) ||
    this.selectedLabTest.some((value) =>  this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_test_id', value.id) || this.isTestSelected(value.id))){
      let dialogRef = this.tcNavigator.confirmAction("Confirm","Order", "Would you like to send it again" , "Cancel");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          order()
        }
  
      });
    } else {
      order()
    }
  }

  orderRadNow(): void {
    const order = () => {
      let payload = {
        future: this.futureRad,
        futureDate: this.tcUtilsDate.toTimeStamp(this.futureRadDate),
        urgency: this.radurgency,
        to_be_resulted_by: this.radiologistId,
        selectedLabPanel: this.selectedradPanel,
        selectedLabTest: this.selectedradTest,
        encounter_id: this.encounterId,
        note: this.radNote
      };
      this.isLoadingResults = true
      this.orderPersisit
        .orderssDo('rad_new_order', payload)
        .subscribe((res) => {
          this.selectedradPanel = []
          this.selectedradTest = []
          this.tcNotification.success('Added');
          this.reload()
          this.radNote = ''
          this.isLoadingResults = false
        }, error => {
          console.log(error)
          this.isLoadingResults = false;
        });
    }
    if (this.orders.some(order => (!order.generated_sample_id && order.order_type == lab_order_type.imaging))){
      const notGeneratedOrder = this.orders.find(order => (!order.generated_sample_id && order.order_type == lab_order_type.imaging))
      const filteredOrder = this.selectedOrders.filter(selected => notGeneratedOrder.id == selected.order_id);
      let dialogRef = this.tcNavigator.confirmAction("AS New Order","Confirm", "The patient has order which image is not collected. What did you want to take this order?  " , "Add on Previous", TCModalWidths.medium, 'new_order', 'add_previous');
      dialogRef.afterClosed().subscribe(result => {
        console.log("result:", result)
        if (result == 'add_previous') {
          const labPanel = this.selectedradPanel.filter(panel => !filteredOrder.some(selected => selected.lab_panel_id == panel.id))
          const labTest = this.selectedradTest.filter(test => !filteredOrder.some(selected => selected.lab_test_id == test.id))
          this.addSelectedOrder(labPanel, labTest, notGeneratedOrder.id)
        } else if (result == 'new_order'){
          order()
        } else {
          this.isLoadingResults = false;
        }
  
      });
    } 
     else if (this.selectedradPanel.some((value) => this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_panel_id', value.id)) ||
    this.selectedradTest.some((value) =>  this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_test_id', value.id) || this.isTestSelected(value.id))){
      let dialogRef = this.tcNavigator.confirmAction("Confirm","Order", "Would you like to send it again" , "Cancel");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          order()
        }
  
      });
    } else {
      order()
    }
  }

  handleChange(order_type: number, lab_test: Lab_TestDetail, event: MatCheckboxChange) {
    const selectedTest = this.getSelectedTest(order_type)
    const selectedPanel = this.getSelectedPanel(order_type)
    if (event.checked){
      selectedTest.push({...lab_test, lab_panel_id: this.tcUtilsString.invalid_id})
    } else {
      const test_panel = this.tcUtilsArray.getByKey(this.lab_testPanel, 'lab_test_id', lab_test.id)
      if (test_panel && this.tcUtilsArray.containsId(selectedPanel, test_panel.lab_panel_id)){
        const tests = this.tcUtilsArray.getByProperty(this.lab_testPanel, (testPanel) => {
         return testPanel.lab_panel_id == test_panel.lab_panel_id
        });
        selectedTest.push(...this.tcUtilsArray.getByProperty(this.orderedTests[order_type], (labTest) => this.tcUtilsArray.containsByKey(tests, 'lab_test_id', labTest.id) ))
        this.tcUtilsArray.removeItemById(selectedPanel, test_panel.lab_panel_id)
      }
      this.tcUtilsArray.removeItem(selectedTest, lab_test)
    }
  }

  isTestSelected(lab_test_id: string): boolean{
    return this.lab_testPanel.filter(test_panel => test_panel.lab_test_id == lab_test_id).some(test_panel => this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_panel_id', test_panel.lab_panel_id))
  }
 
  orderPatNow(): void {
    const order = () => {
      let payload = {
        future: this.futureRad,
        futureDate: this.tcUtilsDate.toTimeStamp(this.futureRadDate),
        urgency: this.radurgency,
        selectedLabPanel: this.selectedpatPanel,
        selectedLabTest: this.selectedpatTest,
        encounter_id: this.encounterId,
        note: this.patNote
      };
      this.isLoadingResults = true
      this.orderPersisit
        .orderssDo('pat_new_order', payload)
        .subscribe((res) => {
          this.selectedpatPanel = []
          this.selectedpatTest = []
          this.tcNotification.success('Added');
          this.reload()
          this.patNote = ''
          this.isLoadingResults = false
        }, error => {
          console.log(error)
          this.isLoadingResults = false;
        });
    }
    if (this.orders.some(order => (!order.generated_sample_id && order.order_type == lab_order_type.pathology))){
      const notGeneratedOrder = this.orders.find(order => (!order.generated_sample_id && order.order_type == lab_order_type.pathology))
      const filteredOrder = this.selectedOrders.filter(selected => notGeneratedOrder.id == selected.order_id);
      let dialogRef = this.tcNavigator.confirmAction("AS New Order","Confirm", "The patient has order which sample is not collected. What did you want to take this order?  " , "Add on Previous", TCModalWidths.medium, 'new_order', 'add_previous');
      dialogRef.afterClosed().subscribe(result => {
        console.log("result:", result)
        if (result == 'add_previous') {
          const labPanel = this.selectedpatPanel.filter(panel => !filteredOrder.some(selected => selected.lab_panel_id == panel.id))
          const labTest = this.selectedpatTest.filter(test => !filteredOrder.some(selected => selected.lab_test_id == test.id))
          this.addSelectedOrder(labPanel, labTest, notGeneratedOrder.id)
        } else if (result == 'new_order'){
          order()
        } else {
          this.isLoadingResults = false;
        }
  
      });
    } 
     else if (this.selectedpatPanel.some((value) => this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_panel_id', value.id)) ||
    this.selectedpatTest.some((value) =>  this.tcUtilsArray.containsByKey(this.selectedOrders, 'lab_test_id', value.id) || this.isTestSelected(value.id))){
      let dialogRef = this.tcNavigator.confirmAction("Confirm","Order", "Would you like to send it again" , "Cancel");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          order()
        }
  
      });
    } else {
      order()
    }
  }

  orderProcedureNow() {
    const order = () => {
    this.isLoadingResults = true
      this.proceduresSelection.forEach((procedure, idx) => {
        const patientProcedure = new Patient_ProcedureDetail()
        patientProcedure.procedure_id = procedure.id;
        patientProcedure.instruction = this.procedureNote;
        this.patientProcedurePersist.addPatient_Procedure(this.encounterId, patientProcedure).subscribe(() => {
          if (idx == this.proceduresSelection.length - 1){
              this.isLoadingResults = false
              this.proceduresSelection = []
              this.tcNotification.success('Added');
              this.reload()
              this.procedureNote = ''
          }
        })
      })
    }
    if (this.proceduresSelection.some((value) => this.tcUtilsArray.contains(this.selectedProcedures, 'procedure_id', value.id))){
      let dialogRef = this.tcNavigator.confirmAction("Confirm","Order", "Would you like to send it again" , "Cancel");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          order()
        }
  
      });
    } else {
      order()
    }
  }

  getAllLabTestPanels() {
    var groupBy = function (xs: Test_PanelSummary[], key: string) {
      return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
      }, []);
    };
    this.isLoadingResults = true
    this.lab_panelPersist
      .test_panelsDo(TCUtilsString.getInvalidId(), 'get_all', {})
      .subscribe((panels: Test_PanelSummary[]) => {
        this.lab_testPanel = panels.sort((previous, next) => {
          if(previous.lab_panel_id == next.lab_panel_id){
              return previous.lab_test_sequence - next.lab_test_sequence
          }
          return previous.lab_panel_id.localeCompare(next.lab_panel_id)
      });
        this.summarizedTest = groupBy(panels, 'lab_panel_id');
        this.isLoadingResults = false
      }, error => {
        console.log(error)
        this.isLoadingResults = false;
      });
  }

  addTestWhenLabIsSelected(panelId: string, order_type: lab_order_type) {
    const test = lab_order_type.imaging == order_type ? this.radTests : ( lab_order_type.lab == order_type ? this.labTests : this.patTests);
    let labTestPanels: Test_PanelSummary[] = this.summarizedTest[panelId];
    const panel = order_type == lab_order_type.imaging ? this.radPanels : ( lab_order_type.lab == order_type ? this.labPanels : this.patPanels);
    const selectedPanel = order_type == lab_order_type.imaging ? this.selectedradPanel : (lab_order_type.lab == order_type ? this.selectedLabPanel : this.selectedpatPanel);
    let selectedTest = order_type == lab_order_type.imaging ? this.selectedradTest : ( lab_order_type.lab == order_type ? this.selectedLabTest : this.selectedpatTest);

    if (labTestPanels != undefined) {
      labTestPanels.forEach((element) => {
        let lab: any = test.find((x) => x.id == element.lab_test_id);
        lab['lab_panel_id'] = element.lab_panel_id;
        if (
          selectedPanel.includes(
            panel.find((x) => x.id == panelId)
          )
        ) {
          // add lab
          this.tcUtilsArray.removeItem(selectedTest, lab);
        } else {
          // remove

          selectedTest = selectedTest.filter(function (value) {
            return (
              value !== lab ||
              value.lab_panel_id == TCUtilsString.getInvalidId()
            );
          });
        }
      });
    }
  }

  checkIfPartOfLabTest(lab_test_id: string, rad: lab_order_type, isForbidden: boolean = false) {
    let selectedPanel;
    if(!isForbidden){
      selectedPanel = rad == lab_order_type.imaging ? this.selectedradPanel : ( rad == lab_order_type.lab ? this.selectedLabPanel : this.selectedpatPanel);
    } else {
      selectedPanel = this.forbiddenSelect.map((forbidden) => ({id: forbidden.lab_panel_id}))
    }
    
    return (
      selectedPanel &&
      this.lab_testPanel.filter(
        (test_panel: Test_PanelDetail) =>
          selectedPanel.filter(
            (lab_panel: Lab_PanelDetail) =>
              lab_panel.id == test_panel.lab_panel_id
          ).length != 0 && test_panel.lab_test_id == lab_test_id
      ).length != 0
    );
  }

  back(): void {
    this.location.back();
  }

  searchRadiologist(){
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.radiology)
    dialogRef.afterClosed().subscribe(
      (result: DoctorDetail[]) => {
        if (result){
          if (result[0].login_id == this.tcUtilsString.invalid_id){
            this.tcNotification.error("The User has no account")
            return;
          }
          this.radiologistName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`
          this.radiologistId = result[0].login_id
        }
      }
    )
  }


}
