import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabTestNormalRangeDetailComponent } from './lab-test-normal-range-detail.component';

describe('LabTestNormalRangeDetailComponent', () => {
  let component: LabTestNormalRangeDetailComponent;
  let fixture: ComponentFixture<LabTestNormalRangeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabTestNormalRangeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestNormalRangeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
