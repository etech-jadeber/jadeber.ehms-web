import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { LabTestNormalRangeDetail } from '../lab-test-normal-range.model';
import { LabTestNormalRangePersist } from '../lab-test-normal-range.persist';
import { LabTestNormalRangeNavigator } from '../lab-test-normal-range.navigator';

export enum LabTestNormalRangeTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-lab-test-normal-range-detail',
  templateUrl: './lab-test-normal-range-detail.component.html',
  styleUrls: ['./lab-test-normal-range-detail.component.scss'],
})
export class LabTestNormalRangeDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  labTestNormalRangeIsLoading: boolean = false;

  LabTestNormalRangeTabs: typeof LabTestNormalRangeTabs = LabTestNormalRangeTabs;
  activeTab: LabTestNormalRangeTabs = LabTestNormalRangeTabs.overview;
  //basics
  labTestNormalRangeDetail: LabTestNormalRangeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public labTestNormalRangeNavigator: LabTestNormalRangeNavigator,
    public labTestNormalRangePersist: LabTestNormalRangePersist
  ) {
    this.tcAuthorization.requireRead('lab_test_normal_ranges');
    this.labTestNormalRangeDetail = new LabTestNormalRangeDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('lab_test_normal_ranges');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.labTestNormalRangeIsLoading = true;
    this.labTestNormalRangePersist.getLabTestNormalRange(id).subscribe(
      (labTestNormalRangeDetail) => {
        this.labTestNormalRangeDetail = labTestNormalRangeDetail;
        this.labTestNormalRangeIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.labTestNormalRangeIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.labTestNormalRangeDetail.id);
  }
  editLabTestNormalRange(): void {
    let modalRef = this.labTestNormalRangeNavigator.editLabTestNormalRange(
      this.labTestNormalRangeDetail.id
    );
    modalRef.afterClosed().subscribe(
      (labTestNormalRangeDetail) => {
        TCUtilsAngular.assign(this.labTestNormalRangeDetail, labTestNormalRangeDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.labTestNormalRangePersist
      .print(this.labTestNormalRangeDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print labTestNormalRange', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
