import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabTestNormalRangeEditComponent } from './lab-test-normal-range-edit.component';

describe('LabTestNormalRangeEditComponent', () => {
  let component: LabTestNormalRangeEditComponent;
  let fixture: ComponentFixture<LabTestNormalRangeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabTestNormalRangeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestNormalRangeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
