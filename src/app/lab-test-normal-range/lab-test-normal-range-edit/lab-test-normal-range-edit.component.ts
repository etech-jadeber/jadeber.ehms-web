import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { LabTestNormalRangeDetail } from '../lab-test-normal-range.model';
import { LabTestNormalRangePersist } from '../lab-test-normal-range.persist';
import { sex } from 'src/app/app.enums';
@Component({
  selector: 'app-lab-test-normal-range-edit',
  templateUrl: './lab-test-normal-range-edit.component.html',
  styleUrls: ['./lab-test-normal-range-edit.component.scss'],
})
export class LabTestNormalRangeEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  labTestNormalRangeDetail: LabTestNormalRangeDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<LabTestNormalRangeEditComponent>,
    public persist: LabTestNormalRangePersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('lab_test_normal_ranges');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'labTestNormalRange';
      this.labTestNormalRangeDetail = new LabTestNormalRangeDetail();
      this.labTestNormalRangeDetail.min_age = 0;
      this.labTestNormalRangeDetail.lab_test_id = this.idMode.id;
      this.labTestNormalRangeDetail.sex = sex.all;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('lab_test_normal_ranges');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'LabTestNormalRange';
      this.isLoadingResults = true;
      this.persist.getLabTestNormalRange(this.idMode.id).subscribe(
        (labTestNormalRangeDetail) => {
          this.labTestNormalRangeDetail = labTestNormalRangeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addLabTestNormalRange(this.labTestNormalRangeDetail).subscribe(
      (value) => {
        this.tcNotification.success('labTestNormalRange added');
        this.labTestNormalRangeDetail.id = value.id;
        this.dialogRef.close(this.labTestNormalRangeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateLabTestNormalRange(this.labTestNormalRangeDetail).subscribe(
      (value) => {
        this.tcNotification.success('labTestNormalRange updated');
        this.dialogRef.close(this.labTestNormalRangeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.labTestNormalRangeDetail == null) {
      return false;
    }
    if (this.labTestNormalRangeDetail.lab_test_id == null) {
      return false;
    }
    if (this.labTestNormalRangeDetail.sex== null) {
      return false;
    }
    if (!(this.labTestNormalRangeDetail.min_range || this.labTestNormalRangeDetail.max_range)) {
      return false;
    }
    if (this.labTestNormalRangeDetail.min_range && this.labTestNormalRangeDetail.max_range && this.labTestNormalRangeDetail.min_range > this.labTestNormalRangeDetail.max_range) {
      return false;
    }
    return true;
  }
}
