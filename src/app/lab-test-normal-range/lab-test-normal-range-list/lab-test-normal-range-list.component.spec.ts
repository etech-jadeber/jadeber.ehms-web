import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabTestNormalRangeListComponent } from './lab-test-normal-range-list.component';

describe('LabTestNormalRangeListComponent', () => {
  let component: LabTestNormalRangeListComponent;
  let fixture: ComponentFixture<LabTestNormalRangeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabTestNormalRangeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestNormalRangeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
