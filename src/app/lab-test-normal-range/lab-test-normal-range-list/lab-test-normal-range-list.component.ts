import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  LabTestNormalRangeSummary,
  LabTestNormalRangeSummaryPartialList,
} from '../lab-test-normal-range.model';
import { LabTestNormalRangePersist } from '../lab-test-normal-range.persist';
import { LabTestNormalRangeNavigator } from '../lab-test-normal-range.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-lab-test-normal-range-list',
  templateUrl: './lab-test-normal-range-list.component.html',
  styleUrls: ['./lab-test-normal-range-list.component.scss'],
})
export class LabTestNormalRangeListComponent implements OnInit {
  labTestNormalRangesData: LabTestNormalRangeSummary[] = [];
  labTestNormalRangesTotalCount: number = 0;
  labTestNormalRangeSelectAll: boolean = false;
  labTestNormalRangeSelection: LabTestNormalRangeSummary[] = [];

  labTestNormalRangesDisplayedColumns: string[] = [
    'select',
    'action',
    'sex',
    'min_age',
    'min_range',
    'max_range',
  ];
  labTestNormalRangeSearchTextBox: FormControl = new FormControl();
  labTestNormalRangeIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  labTestNormalRangesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) labTestNormalRangesSort: MatSort;
  @Input() lab_test_id: string;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public labTestNormalRangePersist: LabTestNormalRangePersist,
    public labTestNormalRangeNavigator: LabTestNormalRangeNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
  ) {
    this.tcAuthorization.requireRead('lab_test_normal_ranges');
    this.labTestNormalRangeSearchTextBox.setValue(
      labTestNormalRangePersist.labTestNormalRangeSearchText
    );
    //delay subsequent keyup events
    this.labTestNormalRangeSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.labTestNormalRangePersist.labTestNormalRangeSearchText = value;
        this.searchLabTestNormalRanges();
      });
  }
  ngOnInit() {
    this.labTestNormalRangePersist.lab_test_id = this.lab_test_id
    this.labTestNormalRangesSort.sortChange.subscribe(() => {
      this.labTestNormalRangesPaginator.pageIndex = 0;
      this.searchLabTestNormalRanges(true);
    });

    this.labTestNormalRangesPaginator.page.subscribe(() => {
      this.searchLabTestNormalRanges(true);
    });
    //start by loading items
    this.searchLabTestNormalRanges();
  }

  searchLabTestNormalRanges(isPagination: boolean = false): void {
    let paginator = this.labTestNormalRangesPaginator;
    let sorter = this.labTestNormalRangesSort;

    this.labTestNormalRangeIsLoading = true;
    this.labTestNormalRangeSelection = [];

    this.labTestNormalRangePersist
      .searchLabTestNormalRange(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: LabTestNormalRangeSummaryPartialList) => {
          this.labTestNormalRangesData = partialList.data;
          if (partialList.total != -1) {
            this.labTestNormalRangesTotalCount = partialList.total;
          }
          this.labTestNormalRangeIsLoading = false;
        },
        (error) => {
          this.labTestNormalRangeIsLoading = false;
        }
      );
  }
  downloadLabTestNormalRanges(): void {
    if (this.labTestNormalRangeSelectAll) {
      this.labTestNormalRangePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download labTestNormalRange',
          true
        );
      });
    } else {
      this.labTestNormalRangePersist
        .download(this.tcUtilsArray.idsList(this.labTestNormalRangeSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download labTestNormalRange',
            true
          );
        });
    }
  }
  addLabTestNormalRange(): void {
    let dialogRef = this.labTestNormalRangeNavigator.addLabTestNormalRange(this.lab_test_id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchLabTestNormalRanges();
      }
    });
  }

  editLabTestNormalRange(item: LabTestNormalRangeSummary) {
    let dialogRef = this.labTestNormalRangeNavigator.editLabTestNormalRange(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteLabTestNormalRange(item: LabTestNormalRangeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('LabTestNormalRange');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.labTestNormalRangePersist.deleteLabTestNormalRange(item.id).subscribe(
          (response) => {
            this.tcNotification.success('LabTestNormalRange deleted');
            this.searchLabTestNormalRanges();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
