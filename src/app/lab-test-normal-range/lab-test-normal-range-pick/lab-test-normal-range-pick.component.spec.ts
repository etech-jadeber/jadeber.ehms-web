import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabTestNormalRangePickComponent } from './lab-test-normal-range-pick.component';

describe('LabTestNormalRangePickComponent', () => {
  let component: LabTestNormalRangePickComponent;
  let fixture: ComponentFixture<LabTestNormalRangePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabTestNormalRangePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestNormalRangePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
