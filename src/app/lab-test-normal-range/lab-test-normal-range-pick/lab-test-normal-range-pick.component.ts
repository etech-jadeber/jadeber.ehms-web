import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  LabTestNormalRangeSummary,
  LabTestNormalRangeSummaryPartialList,
} from '../lab-test-normal-range.model';
import { LabTestNormalRangePersist } from '../lab-test-normal-range.persist';
import { LabTestNormalRangeNavigator } from '../lab-test-normal-range.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-lab-test-normal-range-pick',
  templateUrl: './lab-test-normal-range-pick.component.html',
  styleUrls: ['./lab-test-normal-range-pick.component.scss'],
})
export class LabTestNormalRangePickComponent implements OnInit {
  labTestNormalRangesData: LabTestNormalRangeSummary[] = [];
  labTestNormalRangesTotalCount: number = 0;
  labTestNormalRangeSelectAll: boolean = false;
  labTestNormalRangeSelection: LabTestNormalRangeSummary[] = [];

  labTestNormalRangesDisplayedColumns: string[] = [
    'select',
    'name',
    'date',
  ];
  labTestNormalRangeSearchTextBox: FormControl = new FormControl();
  labTestNormalRangeIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  labTestNormalRangesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) labTestNormalRangesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public labTestNormalRangePersist: LabTestNormalRangePersist,
    public labTestNormalRangeNavigator: LabTestNormalRangeNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<LabTestNormalRangePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('lab_test_normal_ranges');
    this.labTestNormalRangeSearchTextBox.setValue(
      labTestNormalRangePersist.labTestNormalRangeSearchText
    );
    //delay subsequent keyup events
    this.labTestNormalRangeSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.labTestNormalRangePersist.labTestNormalRangeSearchText = value;
        this.searchLabTestNormalRanges();
      });
  }
  ngOnInit() {
    this.labTestNormalRangesSort.sortChange.subscribe(() => {
      this.labTestNormalRangesPaginator.pageIndex = 0;
      this.searchLabTestNormalRanges(true);
    });

    this.labTestNormalRangesPaginator.page.subscribe(() => {
      this.searchLabTestNormalRanges(true);
    });
    //start by loading items
    this.searchLabTestNormalRanges();
  }

  searchLabTestNormalRanges(isPagination: boolean = false): void {
    let paginator = this.labTestNormalRangesPaginator;
    let sorter = this.labTestNormalRangesSort;

    this.labTestNormalRangeIsLoading = true;
    this.labTestNormalRangeSelection = [];

    this.labTestNormalRangePersist
      .searchLabTestNormalRange(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: LabTestNormalRangeSummaryPartialList) => {
          this.labTestNormalRangesData = partialList.data;
          if (partialList.total != -1) {
            this.labTestNormalRangesTotalCount = partialList.total;
          }
          this.labTestNormalRangeIsLoading = false;
        },
        (error) => {
          this.labTestNormalRangeIsLoading = false;
        }
      );
  }
  markOneItem(item: LabTestNormalRangeSummary) {
    if (!this.tcUtilsArray.containsId(this.labTestNormalRangeSelection, item.id)) {
      this.labTestNormalRangeSelection = [];
      this.labTestNormalRangeSelection.push(item);
    } else {
      this.labTestNormalRangeSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.labTestNormalRangeSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
