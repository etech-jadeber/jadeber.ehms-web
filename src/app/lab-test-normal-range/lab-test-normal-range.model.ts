import { TCId } from '../tc/models';
export class LabTestNormalRangeSummary extends TCId {
  lab_test_id: string;
  min_age: number;
  min_range: number;
  max_range: number;
  sex: number;

}
export class LabTestNormalRangeSummaryPartialList {
  data: LabTestNormalRangeSummary[];
  total: number;
}
export class LabTestNormalRangeDetail extends LabTestNormalRangeSummary {}

export class LabTestNormalRangeDashboard {
  total: number;
}
