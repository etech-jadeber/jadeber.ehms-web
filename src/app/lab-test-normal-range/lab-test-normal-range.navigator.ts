import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { LabTestNormalRangeEditComponent } from './lab-test-normal-range-edit/lab-test-normal-range-edit.component';
import { LabTestNormalRangePickComponent } from './lab-test-normal-range-pick/lab-test-normal-range-pick.component';

@Injectable({
  providedIn: 'root',
})
export class LabTestNormalRangeNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  labTestNormalRangesUrl(): string {
    return '/lab_test_normal_ranges';
  }

  labTestNormalRangeUrl(id: string): string {
    return '/lab_test_normal_ranges/' + id;
  }

  viewlabTestNormalRanges(): void {
    this.router.navigateByUrl(this.labTestNormalRangesUrl());
  }

  viewlabTestNormalRange(id: string): void {
    this.router.navigateByUrl(this.labTestNormalRangeUrl(id));
  }

  editLabTestNormalRange(id: string): MatDialogRef<LabTestNormalRangeEditComponent> {
    const dialogRef = this.dialog.open(LabTestNormalRangeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addLabTestNormalRange(lab_test_id: string): MatDialogRef<LabTestNormalRangeEditComponent> {
    const dialogRef = this.dialog.open(LabTestNormalRangeEditComponent, {
      data: new TCIdMode(lab_test_id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickLabTestNormalRanges(
    selectOne: boolean = false
  ): MatDialogRef<LabTestNormalRangePickComponent> {
    const dialogRef = this.dialog.open(LabTestNormalRangePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
