import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  LabTestNormalRangeDashboard,
  LabTestNormalRangeDetail,
  LabTestNormalRangeSummaryPartialList,
} from './lab-test-normal-range.model';
import { sex } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class LabTestNormalRangePersist {
  labTestNormalRangeSearchText: string = '';
  lab_test_id: string;
  sex: TCEnum[] = [
    new TCEnum(sex.all, 'All'),
    new TCEnum(sex.male, 'Male'),
    new TCEnum(sex.female, 'Female'),
  ];

  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.labTestNormalRangeSearchText;
    //add custom filters
    return fltrs;
  }

  searchLabTestNormalRange(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<LabTestNormalRangeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'lab_test_normal_range',
      this.labTestNormalRangeSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if (this.lab_test_id){
      url = TCUtilsString.appendUrlParameter(url, "lab_test_id", this.lab_test_id)
    }
    return this.http.get<LabTestNormalRangeSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lab_test_normal_range/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  labTestNormalRangeDashboard(): Observable<LabTestNormalRangeDashboard> {
    return this.http.get<LabTestNormalRangeDashboard>(
      environment.tcApiBaseUri + 'lab_test_normal_range/dashboard'
    );
  }

  getLabTestNormalRange(id: string): Observable<LabTestNormalRangeDetail> {
    return this.http.get<LabTestNormalRangeDetail>(
      environment.tcApiBaseUri + 'lab_test_normal_range/' + id
    );
  }
  addLabTestNormalRange(item: LabTestNormalRangeDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lab_test_normal_range/',
      item
    );
  }

  updateLabTestNormalRange(item: LabTestNormalRangeDetail): Observable<LabTestNormalRangeDetail> {
    return this.http.patch<LabTestNormalRangeDetail>(
      environment.tcApiBaseUri + 'lab_test_normal_range/' + item.id,
      item
    );
  }

  deleteLabTestNormalRange(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'lab_test_normal_range/' + id);
  }
  LabTestNormalRangesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'lab_test_normal_range/do',
      new TCDoParam(method, payload)
    );
  }

  labTestNormalRangeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'lab_test_normal_range/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lab_test_normal_range/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lab_test_normal_range/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
