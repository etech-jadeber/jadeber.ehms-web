import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabOrderDetailComponent } from './lab-order-detail.component';

describe('LabOrderDetailComponent', () => {
  let component: LabOrderDetailComponent;
  let fixture: ComponentFixture<LabOrderDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabOrderDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabOrderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
