import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Lab_OrderDetail} from "../lab_order.model";
import {Lab_OrderPersist} from "../lab_order.persist";
import {Lab_OrderNavigator} from "../lab_order.navigator";



@Component({
  selector: 'app-lab_order-detail',
  templateUrl: './lab-order-detail.component.html',
  styleUrls: ['./lab-order-detail.component.css']
})
export class Lab_OrderDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  lab_orderLoading:boolean = false;
  //basics
  lab_orderDetail: Lab_OrderDetail;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public lab_orderNavigator: Lab_OrderNavigator,
              public  lab_orderPersist: Lab_OrderPersist) {
    this.tcAuthorization.requireRead("order_givens");
    this.lab_orderDetail = new Lab_OrderDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("order_givens");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.lab_orderLoading = true;
    this.lab_orderPersist.getLab_Order(id).subscribe(lab_orderDetail => {
          this.lab_orderDetail = lab_orderDetail;
          this.lab_orderLoading = false;
        }, error => {
          console.error(error);
          this.lab_orderLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.lab_orderDetail.id);
  }

  editLab_Order(): void {
    let modalRef = this.lab_orderNavigator.editLab_Order(this.lab_orderDetail.id);
    modalRef.afterClosed().subscribe(modifiedLab_OrderDetail => {
      TCUtilsAngular.assign(this.lab_orderDetail, modifiedLab_OrderDetail);
    }, error => {
      console.error(error);
    });
  }

   printLab_Order():void{
      this.lab_orderPersist.print(this.lab_orderDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print lab_order", true);
      });
    }

  back():void{
      this.location.back();
    }

}
