import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Lab_OrderDetail} from "../lab_order.model";
import {Lab_OrderPersist} from "../lab_order.persist";


@Component({
  selector: 'app-lab_order-edit',
  templateUrl: './lab-order-edit.component.html',
  styleUrls: ['./lab-order-edit.component.css']
})
export class Lab_OrderEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lab_orderDetail: Lab_OrderDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Lab_OrderEditComponent>,
              public  persist: Lab_OrderPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("order_givens");
      this.title = this.appTranslation.getText("general","new") +  " Order";
      this.lab_orderDetail = new Lab_OrderDetail();
      //set necessary defaults
      this.lab_orderDetail.type = -1;

    } else {
     this.tcAuthorization.requireUpdate("order_givens");
      this.title = this.appTranslation.getText("general","edit") +  " Order";
      this.isLoadingResults = true;
      this.persist.getLab_Order(this.idMode.id).subscribe(lab_orderDetail => {
        this.lab_orderDetail = lab_orderDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addLab_Order(this.lab_orderDetail).subscribe(value => {
      this.tcNotification.success("Lab_Order added");
      this.lab_orderDetail.id = value.id;
      this.dialogRef.close(this.lab_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateLab_Order(this.lab_orderDetail).subscribe(value => {
      this.tcNotification.success("Lab_Order updated");
      this.dialogRef.close(this.lab_orderDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.lab_orderDetail == null){
            return false;
          }

        if (this.lab_orderDetail.name == null || this.lab_orderDetail.name  == "") {
                      return false;
                    }
        if (this.lab_orderDetail.type == -1) {
          return false;
        }

        return true;
      }


}
