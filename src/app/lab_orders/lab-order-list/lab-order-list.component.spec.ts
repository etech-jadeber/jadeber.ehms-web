import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Lab_OrderListComponent} from "./lab-order-list.component";

describe('LabOrderListComponent', () => {
  let component: Lab_OrderListComponent;
  let fixture: ComponentFixture<Lab_OrderListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_OrderListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_OrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
