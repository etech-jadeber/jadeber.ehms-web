import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Lab_OrderPersist} from "../lab_order.persist";
import {Lab_OrderNavigator} from "../lab_order.navigator";
import { Lab_OrderSummary, Lab_OrderSummaryPartialList} from "../lab_order.model";
import { ActivenessStatus } from 'src/app/app.enums';

export enum PaginatorIndexes{
  order,
  panel,
  test
}

@Component({
  selector: 'app-lab_order-list',
  templateUrl: './lab-order-list.component.html',
  styleUrls: ['./lab-order-list.component.css']
})
export class Lab_OrderListComponent implements OnInit {

  lab_ordersData: Lab_OrderSummary[] = [];
  lab_ordersTotalCount: number = 0;
  lab_ordersSelectAll:boolean = false;
  lab_ordersSelection: Lab_OrderSummary[] = [];

  lab_ordersDisplayedColumns: string[] = ["select","action", "name", "type" ];
  lab_ordersSearchTextBox: FormControl = new FormControl();
  lab_ordersIsLoading: boolean = false;
  activnessStatus = ActivenessStatus;

  @ViewChild(MatPaginator, {static: true}) lab_ordersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_ordersSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lab_orderNavigator: Lab_OrderNavigator,
                public lab_orderPersist: Lab_OrderPersist,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("order_givens");
       this.lab_ordersSearchTextBox.setValue(lab_orderPersist.lab_orderSearchText);
      //delay subsequent keyup events
      this.lab_ordersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lab_orderPersist.lab_orderSearchText = value;
        this.searchLab_Orders();
      });
    }

    ngOnInit(): void {

    }

    ngAfterViewInit() {

      this.lab_ordersSort.sortChange.subscribe(() => {
        this.lab_ordersPaginator.pageIndex = 0;
        this.searchLab_Orders(true);
      });

      this.lab_ordersPaginator.page.subscribe(() => {
        this.searchLab_Orders(true);
      });
      //start by loading items
      this.lab_ordersPaginator.pageSize = 5;
      this.searchLab_Orders();
    }

  searchLab_Orders(isPagination:boolean = false): void {

    let paginator = this.lab_ordersPaginator;
    let sorter = this.lab_ordersSort;

    this.lab_ordersIsLoading = true;
    this.lab_ordersSelection = [];

    this.lab_orderPersist.searchLab_Order(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Lab_OrderSummaryPartialList) => {
      this.lab_ordersData = partialList.data;
      if (partialList.total != -1) {
        this.lab_ordersTotalCount = partialList.total;
      }
      this.lab_ordersIsLoading = false;
    }, error => {
      this.lab_ordersIsLoading = false;
    });

  }

  downloadLab_Orders(): void {
    if(this.lab_ordersSelectAll){
         this.lab_orderPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lab_orders", true);
      });
    }
    else{
        this.lab_orderPersist.download(this.tcUtilsArray.idsList(this.lab_ordersSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lab_orders",true);
            });
        }
  }



  addLab_Order(): void {
    let dialogRef = this.lab_orderNavigator.addLab_Order();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLab_Orders();
      }
    });
  }

  editLab_Order(item: Lab_OrderSummary) {
    let dialogRef = this.lab_orderNavigator.editLab_Order(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  updateActveness(item: Lab_OrderSummary, method: string) {
    this.lab_orderPersist.lab_orderDo(item.id, method, {}).subscribe(
      value => {
        this.tcNotification.success("The status is updated")
        TCUtilsAngular.assign(item, {...item, status : method == 'active' ? ActivenessStatus.active : ActivenessStatus.freeze})
      }
    )
  }

  deleteLab_Order(item: Lab_OrderSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Lab_Order");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lab_orderPersist.deleteLab_Order(item.id).subscribe(response => {
          this.tcNotification.success("Lab_Order deleted");
          this.searchLab_Orders();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}


