import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabOrderPickComponent } from './lab-order-pick.component';

describe('LabOrderPickComponent', () => {
  let component: LabOrderPickComponent;
  let fixture: ComponentFixture<LabOrderPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabOrderPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabOrderPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
