import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Lab_OrderDetail, Lab_OrderSummary, Lab_OrderSummaryPartialList} from "../lab_order.model";
import {Lab_OrderPersist} from "../lab_order.persist";


@Component({
  selector: 'app-lab_order-pick',
  templateUrl: './lab-order-pick.component.html',
  styleUrls: ['./lab-order-pick.component.css']
})
export class Lab_OrderPickComponent implements OnInit {

  lab_ordersData: Lab_OrderSummary[] = [];
  lab_ordersTotalCount: number = 0;
  lab_ordersSelection: Lab_OrderSummary[] = [];
  lab_ordersDisplayedColumns: string[] = ["select", 'name','type' ];

  lab_ordersSearchTextBox: FormControl = new FormControl();
  lab_ordersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) lab_ordersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_ordersSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public lab_orderPersist: Lab_OrderPersist,
              public dialogRef: MatDialogRef<Lab_OrderPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, diagnosisType: number}
  ) {
    this.tcAuthorization.requireRead("order_givens");
    this.lab_ordersSearchTextBox.setValue(lab_orderPersist.lab_orderSearchText);
    //delay subsequent keyup events
    this.lab_ordersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.lab_orderPersist.lab_orderSearchText = value;
      this.searchLab_Orders();
    });
  }

  ngOnInit() {

    this.lab_ordersSort.sortChange.subscribe(() => {
      this.lab_ordersPaginator.pageIndex = 0;
      this.searchLab_Orders();
    });

    this.lab_ordersPaginator.page.subscribe(() => {
      this.searchLab_Orders();
    });

    //set initial picker list to 5
    this.lab_ordersPaginator.pageSize = 5;

    //start by loading items
    this.searchLab_Orders();
  }

  searchLab_Orders(): void {
    this.lab_ordersIsLoading = true;
    this.lab_ordersSelection = [];
    let diagnosisType = this.data.diagnosisType? this.data.diagnosisType : null;
    this.lab_orderPersist.lab_order_typeId = diagnosisType;
    this.lab_orderPersist.searchLab_Order(this.lab_ordersPaginator.pageSize,
        this.lab_ordersPaginator.pageIndex,
        this.lab_ordersSort.active,
        this.lab_ordersSort.direction).subscribe((partialList: Lab_OrderSummaryPartialList) => {
      this.lab_ordersData = partialList.data;
      if (partialList.total != -1) {
        this.lab_ordersTotalCount = partialList.total;
      }
      this.lab_ordersIsLoading = false;
    }, error => {
      this.lab_ordersIsLoading = false;
    });

  }

  markOneItem(item: Lab_OrderSummary) {
    if(!this.tcUtilsArray.containsId(this.lab_ordersSelection,item.id)){
          this.lab_ordersSelection = [];
          this.lab_ordersSelection.push(item);
        }
        else{
          this.lab_ordersSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.lab_ordersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
