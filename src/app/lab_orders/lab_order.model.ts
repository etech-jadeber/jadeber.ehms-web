import {TCId} from "../tc/models";

export class Lab_OrderSummary extends TCId {
  name : string;
type : number;
status: number;
}

export class Lab_OrderSummaryPartialList {
  data: Lab_OrderSummary[];
  total: number;
}

export class Lab_OrderDetail extends Lab_OrderSummary {
  name : string;
  type : number;
}

export class Lab_OrderDashboard {
  total: number;
}
