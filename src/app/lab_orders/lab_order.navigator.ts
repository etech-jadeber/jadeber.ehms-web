import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Lab_OrderEditComponent} from "./lab-order-edit/lab-order-edit.component";
import {Lab_OrderPickComponent} from "./lab-order-pick/lab-order-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Lab_OrderNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  lab_ordersUrl(): string {
    return "/order_givens";
  }

  lab_orderUrl(id: string): string {
    return "/order_givens/" + id;
  }

  viewLab_Orders(): void {
    this.router.navigateByUrl(this.lab_ordersUrl());
  }

  viewLab_Order(id: string): void {
    this.router.navigateByUrl(this.lab_orderUrl(id));
  }

  editLab_Order(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_OrderEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLab_Order(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_OrderEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickLab_Orders(selectOne: boolean=false, diagnosisType: number = null): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Lab_OrderPickComponent, {
        data: {selectOne, diagnosisType},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
