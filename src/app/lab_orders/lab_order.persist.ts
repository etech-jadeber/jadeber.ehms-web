import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Lab_OrderDashboard, Lab_OrderDetail, Lab_OrderSummaryPartialList} from "./lab_order.model";
import { lab_order_type } from '../app.enums'
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Lab_OrderPersist {

  lab_orderSearchText: string = "";
  lab_order_typeId: number ;

    lab_order_type: TCEnum[] = [
    new TCEnum(lab_order_type.imaging, 'imaging'),
    new TCEnum(lab_order_type.lab, 'lab'),
    new TCEnum(lab_order_type.pathology, 'pathology')
  ];


  constructor(private http: HttpClient) {
  }

  documentUploadUri(memberId: string) {
    return environment.tcApiBaseUri + "lab_result_upload/" + memberId;
  }

  searchLab_Order( pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Lab_OrderSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("orders_given", this.lab_orderSearchText, pageSize, pageIndex, sort, order);
    if (this.lab_order_typeId) {
      url = TCUtilsString.appendUrlParameter(url, "lab_order_type", this.lab_order_typeId.toString());
    }
    console.log("the url is ", url);
    return this.http.get<Lab_OrderSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.lab_orderSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "orders_given/do", new TCDoParam("download_all", this.filters()));
  }

  lab_orderDashboard(): Observable<Lab_OrderDashboard> {
    return this.http.get<Lab_OrderDashboard>(environment.tcApiBaseUri + "orders_given/dashboard");
  }

  getLab_Order(id: string): Observable<Lab_OrderDetail> {
    return this.http.get<Lab_OrderDetail>(environment.tcApiBaseUri + "orders_given/" + id);
  }

  addLab_Order(item: Lab_OrderDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "orders_given/", item);
  }

  updateLab_Order(item: Lab_OrderDetail): Observable<Lab_OrderDetail> {
    return this.http.patch<Lab_OrderDetail>(environment.tcApiBaseUri + "orders_given/" + item.id, item);
  }

  deleteLab_Order(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "orders_given/" + id);
  }

  lab_ordersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "orders_given/do", new TCDoParam(method, payload));
  }

  lab_orderDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "orders_given/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "orders_given/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "orders_given/" + id + "/do", new TCDoParam("print", {}));
  }


}
