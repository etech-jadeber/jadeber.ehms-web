import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabPanelDetailComponent } from './lab-panel-detail.component';

describe('LabPanelDetailComponent', () => {
  let component: LabPanelDetailComponent;
  let fixture: ComponentFixture<LabPanelDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabPanelDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabPanelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
