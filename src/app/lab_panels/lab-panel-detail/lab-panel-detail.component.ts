import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Lab_PanelDetail, Test_PanelDetail, Test_PanelSummary} from "../lab_panel.model";
import {Lab_PanelPersist} from "../lab_panel.persist";
import {Lab_PanelNavigator} from "../lab_panel.navigator";
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_OrderSummary, Lab_OrderSummaryPartialList } from 'src/app/lab_orders/lab_order.model';
import { Test_PanelNavigator } from '../test-panel.navigator';
import { FormControl } from '@angular/forms';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { TCUtilsNumber } from 'src/app/tc/utils-number';

export enum Lab_PanelTabs {
  overview,lab_test
}

export enum PaginatorIndexes {
  test_panels
}


@Component({
  selector: 'app-lab-panel-detail',
  templateUrl: './lab-panel-detail.component.html',
  styleUrls: ['./lab-panel-detail.component.css']
})
export class Lab_PanelDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  lab_panelLoading:boolean = false;
  
  Lab_PanelTabs: typeof Lab_PanelTabs = Lab_PanelTabs;
  activeTab: Lab_PanelTabs = Lab_PanelTabs.overview;
  //basics
  lab_panelDetail: Lab_PanelDetail;
  lab_ordersData: Lab_OrderSummary[] = [];


  lab_TestSummary: Lab_TestSummary[] = [];
test_panelsTotalCount: number = 0;
test_panelsSelectAll:boolean = false;
test_panelsSelected: Test_PanelSummary[] = [];
test_panelsDisplayedColumns: string[] = ['select','action','lab_test_id'];
test_panelsSearchTextBox: FormControl = new FormControl();
test_panelsLoading:boolean = false;


  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              private test_PanelNavigator:Test_PanelNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              private labTestPersist:Lab_TestPersist,
              public lab_panelNavigator: Lab_PanelNavigator,
              public  lab_panelPersist: Lab_PanelPersist,
              public lab_orderPersist: Lab_OrderPersist,
              public tcUtilsNumber: TCUtilsNumber,
              ) {
    this.tcAuthorization.requireRead("order_panels");
    this.lab_panelDetail = new Lab_PanelDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("order_panels");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.lab_panelLoading = true;
    this.lab_panelPersist.getLab_Panel(id).subscribe(lab_panelDetail => {
          this.lab_panelDetail = lab_panelDetail;
          this.searchLab_Orders(lab_panelDetail);
          this.lab_panelLoading = false;
          this.searchTest_Panels();
        }, error => {
          console.error(error);
          this.lab_panelLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.lab_panelDetail.id);
  }

  editLab_Panel(): void {
    let modalRef = this.lab_panelNavigator.editLab_Panel(this.lab_panelDetail.id);
    modalRef.afterClosed().subscribe(modifiedLab_PanelDetail => {
      TCUtilsAngular.assign(this.lab_panelDetail, modifiedLab_PanelDetail);
    }, error => {
      console.error(error);
    });
  }

   printLab_Panel():void{
      this.lab_panelPersist.print(this.lab_panelDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print lab_panel", true);
      });
    }

  back():void{
      this.location.back();
    }
    searchLab_Orders(lab_panelOrder: Lab_PanelDetail): void {
  
      this.lab_orderPersist.getLab_Order(lab_panelOrder.lab_order_id).subscribe((order: Lab_OrderSummary) => {
        lab_panelOrder.lab_order_name = order.name;
    });
}



addLabTest():void{
  this.test_PanelNavigator.addTest_Panel(this.lab_panelDetail.id, this.lab_panelDetail.lab_order_id).afterClosed().subscribe(
    res=>{
    if (res){
      this.searchTest_Panels()
    }
    }
  )
}



deleteTest_Panel(item: Test_PanelDetail): void {
  let dialogRef = this.tcNavigator.confirmDeletion("Test_Panel");
  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      //do the deletion
      this.lab_panelPersist.deleteTest_Panel(item.id).subscribe(response => {
        this.tcNotification.success("Test Panel Deleted!");
        this.searchTest_Panels();
      }, error => {
      });
    }

  });
}

searchTest_Panels(isPagination:boolean = false):void {
  this.labTestPersist.lab_testsDo("get_by_panel_type",{"type":this.lab_panelDetail.id}).subscribe((lab_TestSummary:Lab_TestSummary[])=>{
    this.lab_TestSummary = lab_TestSummary;
    
  })
}

}
