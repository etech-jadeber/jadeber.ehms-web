import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabPanelEditComponent } from './lab-panel-edit.component';

describe('LabPanelEditComponent', () => {
  let component: LabPanelEditComponent;
  let fixture: ComponentFixture<LabPanelEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabPanelEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabPanelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
