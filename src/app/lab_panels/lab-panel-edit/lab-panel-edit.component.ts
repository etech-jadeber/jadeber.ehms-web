import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Lab_PanelDetail} from "../lab_panel.model";
import {Lab_PanelPersist} from "../lab_panel.persist";
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';


@Component({
  selector: 'app-lab-panel-edit',
  templateUrl: './lab-panel-edit.component.html',
  styleUrls: ['./lab-panel-edit.component.css']
})
export class Lab_PanelEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lab_panelDetail: Lab_PanelDetail;
  labOrderName:string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Lab_PanelEditComponent>,
              public  persist: Lab_PanelPersist,
              public  labOrderPersist: Lab_OrderPersist,
              public  labOrderNavigator: Lab_OrderNavigator,
              public feePersist: FeePersist,
              public tcUtilsArray: TCUtilsArray,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("order_panels");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText('laboratory','lab_panel_id');
      this.lab_panelDetail = new Lab_PanelDetail();
      if (this.idMode.id){
        this.labOrderPersist.getLab_Order(this.idMode.id).subscribe(order => {
          this.lab_panelDetail.lab_order_id = order.id;
          this.labOrderName = order.name;
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          this.isLoadingResults = false;
        })
      }
      //set necessary defaults
      this.lab_panelDetail.vat_status = -1;

    } else {
     this.tcAuthorization.requireUpdate("order_panels");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText('laboratory','lab_panel_id');
      this.isLoadingResults = true;
      this.persist.getLab_Panel(this.idMode.id).subscribe(lab_panelDetail => {
        this.lab_panelDetail = lab_panelDetail;
        this.labOrderPersist.getLab_Order(lab_panelDetail.lab_order_id).subscribe((orderDetail) => {
          this.labOrderName = orderDetail.name;
          this.isLoadingResults = false;
        })
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addLab_Panel(this.lab_panelDetail).subscribe(value => {
      this.tcNotification.success("Lab_Panel added");
      this.lab_panelDetail.id = value.id;
      this.dialogRef.close(this.lab_panelDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateLab_Panel(this.lab_panelDetail).subscribe(value => {
      this.tcNotification.success("Lab_Panel updated");
      this.dialogRef.close(this.lab_panelDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.lab_panelDetail == null){
            return false;
          }

        if (this.lab_panelDetail.name == null || this.lab_panelDetail.name  == "") {
                      return false;
                    }
        if (this.lab_panelDetail.vat_status == -1){
          return false
        }
        if (this.lab_panelDetail.price == null){
          return false;
        }
        if (this.lab_panelDetail.lab_order_id == null){
          return false;
        }

        return true;
      }
      searchLabOrders() {
        let dialogRef = this.labOrderNavigator.pickLab_Orders(true);
        dialogRef.afterClosed().subscribe((result: Lab_OrderSummary[]) => {
          if (result) {
            this.labOrderName = result[0].name
            this.lab_panelDetail.lab_order_id = result[0].id;
          }
        });
      }
}
