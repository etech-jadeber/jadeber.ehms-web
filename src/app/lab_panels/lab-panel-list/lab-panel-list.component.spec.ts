import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabPanelListComponent } from './lab-panel-list.component';

describe('LabPanelListComponent', () => {
  let component: LabPanelListComponent;
  let fixture: ComponentFixture<LabPanelListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabPanelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabPanelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
