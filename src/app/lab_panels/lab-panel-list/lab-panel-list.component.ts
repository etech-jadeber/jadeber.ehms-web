import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Lab_PanelPersist} from "../lab_panel.persist";
import {Lab_PanelNavigator} from "../lab_panel.navigator";
import {Lab_PanelDetail, Lab_PanelSummary, Lab_PanelSummaryPartialList} from "../lab_panel.model";
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { ActivenessStatus } from 'src/app/app.enums';
import { TCUtilsNumber } from 'src/app/tc/utils-number';


@Component({
  selector: 'app-lab-panel-list',
  templateUrl: './lab-panel-list.component.html',
  styleUrls: ['./lab-panel-list.component.css']
})
export class Lab_PanelListComponent implements OnInit {
  lab_panelsData: Lab_PanelSummary[] = [];
  lab_panelsTotalCount: number = 0;
  lab_panelsSelectAll:boolean = false;
  lab_panelsSelection: Lab_PanelSummary[] = [];

  lab_panelsDisplayedColumns: string[] = ["select","action", "name", "order", "price" ];
  lab_panelsSearchTextBox: FormControl = new FormControl();
  lab_panelsIsLoading: boolean = false;
  activnessStatus = ActivenessStatus;

  @Input() orderId: string;
  @ViewChild(MatPaginator, {static: true}) lab_panelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_panelsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lab_panelPersist: Lab_PanelPersist,
                public lab_panelNavigator: Lab_PanelNavigator,
                public lab_orderPersist: Lab_OrderPersist,
                public jobPersist: JobPersist,
                public tcUtilsNumber: TCUtilsNumber,
    ) {

        this.tcAuthorization.requireRead("order_panels");
       this.lab_panelsSearchTextBox.setValue(lab_panelPersist.lab_panelSearchText);
      //delay subsequent keyup events
      this.lab_panelsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lab_panelPersist.lab_panelSearchText = value;
        this.searchLab_Panels(false);
      });
    }

    ngOnInit() {
      this.lab_panelPersist.lab_order_id = this.orderId;
      this.lab_panelsSort.sortChange.subscribe(() => {
        this.lab_panelsPaginator.pageIndex = 0;
        this.searchLab_Panels(true);
      });

      this.lab_panelsPaginator.page.subscribe(() => {
        this.searchLab_Panels(true);
      });

      this.lab_panelsPaginator.pageSize = 5;
      //start by loading items
      this.searchLab_Panels(false);
      
    }

    ngOnDestroy(){
      this.lab_panelPersist.lab_order_id = null;
    }

  searchLab_Panels(isPagination:boolean = false): void {


    let paginator = this.lab_panelsPaginator;
    let sorter = this.lab_panelsSort;

    this.lab_panelsIsLoading = true;
    this.lab_panelsSelection = [];

    this.lab_panelPersist.searchLab_Panel(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Lab_PanelSummaryPartialList) => {
      this.lab_panelsData = partialList.data;
      this.lab_panelsData.forEach(panel => {
        this.lab_orderPersist.getLab_Order(panel.lab_order_id).subscribe(order => panel.lab_order_name = order.name);
      })
      if (partialList.total != -1) {
        this.lab_panelsTotalCount = partialList.total;
      }
      this.lab_panelsIsLoading = false;
    }, error => {
      this.lab_panelsIsLoading = false;
    });

  }

  downloadLab_Panels(): void {
    if(this.lab_panelsSelectAll){
         this.lab_panelPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lab_panels", true);
      });
    }
    else{
        this.lab_panelPersist.download(this.tcUtilsArray.idsList(this.lab_panelsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lab_panels",true);
            });
        }
  }


  updateActveness(item: Lab_PanelSummary, method: string) {
    this.lab_panelPersist.lab_panelDo(item.id, method, {}).subscribe(
      value => {
        this.tcNotification.success("The status is updated")
        TCUtilsAngular.assign(item, {...item, status : method == 'active' ? ActivenessStatus.active : ActivenessStatus.freeze})
      }
    )
  }



  addLab_Panel(): void {
    let dialogRef = this.lab_panelNavigator.addLab_Panel(this.orderId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLab_Panels(false);
      }
    });
  }

  editLab_Panel(item: Lab_PanelSummary) {
    let dialogRef = this.lab_panelNavigator.editLab_Panel(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteLab_Panel(item: Lab_PanelSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Lab_Panel");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lab_panelPersist.deleteLab_Panel(item.id).subscribe(response => {
          this.tcNotification.success("Lab_Panel deleted");
          this.searchLab_Panels(false);
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
