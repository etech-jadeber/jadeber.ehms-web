import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabPanelPickComponent } from './lab-panel-pick.component';

describe('LabPanelPickComponent', () => {
  let component: LabPanelPickComponent;
  let fixture: ComponentFixture<LabPanelPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabPanelPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabPanelPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
