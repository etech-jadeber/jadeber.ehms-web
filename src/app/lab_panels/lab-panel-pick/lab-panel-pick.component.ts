import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Lab_PanelDetail, Lab_PanelSummary, Lab_PanelSummaryPartialList} from "../lab_panel.model";
import {Lab_PanelPersist} from "../lab_panel.persist";
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';


@Component({
  selector: 'app-lab-panel-pick',
  templateUrl: './lab-panel-pick.component.html',
  styleUrls: ['./lab-panel-pick.component.css']
})
export class Lab_PanelPickComponent implements OnInit {

  lab_panelsData: Lab_PanelSummary[] = [];
  lab_panelsTotalCount: number = 0;
  lab_panelsSelection: Lab_PanelSummary[] = [];
  lab_panelsDisplayedColumns: string[] = ["select", 'lab_order_id','name','price' ];

  lab_panelsSearchTextBox: FormControl = new FormControl();
  lab_panelsIsLoading: boolean = false;
  selectOne: boolean;

  @ViewChild(MatPaginator, {static: true}) lab_panelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_panelsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public lab_panelPersist: Lab_PanelPersist,
              public labOrderPersist: Lab_OrderPersist,
              public dialogRef: MatDialogRef<Lab_PanelPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, lab_order_type: number}
  ) {
    this.tcAuthorization.requireRead("order_panels");
    this.lab_panelsSearchTextBox.setValue(lab_panelPersist.lab_panelSearchText);
    //delay subsequent keyup events
    this.lab_panelsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.lab_panelPersist.lab_panelSearchText = value;
      this.searchLab_Panels();
    });
    this.selectOne = this.data.selectOne;
    this.lab_panelPersist.lab_order_type = this.data.lab_order_type
  }

  ngOnInit() {

    this.lab_panelsSort.sortChange.subscribe(() => {
      this.lab_panelsPaginator.pageIndex = 0;
      this.searchLab_Panels();
    });

    this.lab_panelsPaginator.page.subscribe(() => {
      this.searchLab_Panels();
    });

    //set initial picker list to 5
    this.lab_panelsPaginator.pageSize = 5;

    //start by loading items
    this.searchLab_Panels();
    this.SearchLabOrders();
  }

  searchLab_Panels(): void {
    this.lab_panelsIsLoading = true;
    this.lab_panelsSelection = [];

    this.lab_panelPersist.searchLab_Panel( this.lab_panelsPaginator.pageSize,
        this.lab_panelsPaginator.pageIndex,
        this.lab_panelsSort.active,
        this.lab_panelsSort.direction).subscribe((partialList: Lab_PanelSummaryPartialList) => {
      this.lab_panelsData = partialList.data;
      if (partialList.total != -1) {
        this.lab_panelsTotalCount = partialList.total;
      }
      this.lab_panelsIsLoading = false;
    }, error => {
      this.lab_panelsIsLoading = false;
    });

  }

  markOneItem(item: Lab_PanelSummary) {
    if(!this.tcUtilsArray.containsId(this.lab_panelsSelection,item.id)){
          this.lab_panelsSelection = [];
          this.lab_panelsSelection.push(item);
        }
        else{
          this.lab_panelsSelection = [];
        }
  }
  labOrderData: Lab_OrderSummary[] = [];

  SearchLabOrders(){
    this.labOrderPersist.searchLab_Order(50, 0, "", "asc").subscribe(result=>{
      this.labOrderData = result.data;

    })
  }

   getLabOrderName(labOrderId:string){
      let labOrderDatas = this.labOrderData.length ? this.tcUtilsArray.getById(this.labOrderData, labOrderId) : {};
    return labOrderDatas.name;
      }

   returnSelected(): void {
    this.dialogRef.close(this.lab_panelsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
