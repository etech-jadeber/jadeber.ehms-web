import { Lab_OrderSummary } from "../lab_orders/lab_order.model";
import { ExtendedLab_TestSummary } from "../lab_tests/lab_test.model";
import {TCId} from "../tc/models";

export class Lab_PanelSummary extends TCId {
  lab_order_id : string;
name : string;
price : number;
type: number;
vat_status: number;
lab_order_name: string;
status: number
}

export class Order {
  future: boolean;
  futureDate: number;
  urgency: number;
  selectedLabPanel: Lab_PanelSummary[];
  selectedLabTest: ExtendedLab_TestSummary[];
  order_type: string;
}

export class Lab_PanelSummaryPartialList {
  data: Lab_PanelSummary[];
  total: number;
}

export class Lab_PanelDetail extends Lab_PanelSummary {
  lab_order_id : string;
name : string;
price : number;
vat_status: number;
}

export class Lab_PanelDashboard {
  total: number;
}

export class Test_PanelSummary extends TCId {
  lab_panel_id : string;
lab_test_id : string;
lab_test_sequence: number;
lab_test_name: string;
}

export class SummarizedTest {
  id:string;
  testPanelSummary:Test_PanelSummary[];
}

export class Test_PanelSummaryPartialList {
  data: Test_PanelSummary[];
  total: number;
}

export class Test_PanelDetail extends Test_PanelSummary {
  lab_panel_id : string;
lab_test_id : string;
}