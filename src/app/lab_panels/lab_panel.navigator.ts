import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {Lab_PanelEditComponent} from "./lab-panel-edit/lab-panel-edit.component";
import {Lab_PanelPickComponent} from "./lab-panel-pick/lab-panel-pick.component";



@Injectable({
  providedIn: 'root'
})

export class Lab_PanelNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  lab_panelsUrl(): string {
    return "/lab_panels";
  }

  lab_panelUrl(id: string): string {
    return "/lab_panels/" + id;
  }

  viewLab_Panels(): void {
    this.router.navigateByUrl(this.lab_panelsUrl());
  }

  viewLab_Panel(id: string): void {
    this.router.navigateByUrl(this.lab_panelUrl(id));
  }

  editLab_Panel(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_PanelEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLab_Panel(parentId: string = null): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_PanelEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickLab_Panels(selectOne: boolean=false, lab_order_type: number): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Lab_PanelPickComponent, {
        data: {selectOne, lab_order_type},
        width: TCModalWidths.large
      });
      return dialogRef;
    }

  
}
