import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Lab_PanelDashboard, Lab_PanelDetail, Lab_PanelSummaryPartialList, Test_PanelDetail, Test_PanelSummaryPartialList} from "./lab_panel.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Lab_PanelPersist {

  lab_panelSearchText: string = "";
  lab_order_id: string;
  lab_order_type: number;

  constructor(private http: HttpClient) {
  }
  searchLab_Panel(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Lab_PanelSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("/lab_panels", this.lab_panelSearchText, pageSize, pageIndex, sort, order);
    if (this.lab_order_id){
      url = TCUtilsString.appendUrlParameter(url, "lab_order_id", this.lab_order_id);
    }
    if (this.lab_order_type){
      url = TCUtilsString.appendUrlParameter(url, "type", this.lab_order_type.toString())
    }
    return this.http.get<Lab_PanelSummaryPartialList>(url);

  }
  
  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.lab_panelSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_panels/do", new TCDoParam("download_all", this.filters()));
  }

  lab_panelDashboard(): Observable<Lab_PanelDashboard> {
    return this.http.get<Lab_PanelDashboard>(environment.tcApiBaseUri + "lab_panels/dashboard");
  }

  getLab_Panel(id: string): Observable<Lab_PanelDetail> {
    return this.http.get<Lab_PanelDetail>(environment.tcApiBaseUri + "lab_panels/" + id);
  }

  addLab_Panel(item: Lab_PanelDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_panels/", item);
  }

  updateLab_Panel(item: Lab_PanelDetail): Observable<Lab_PanelDetail> {
    return this.http.patch<Lab_PanelDetail>(environment.tcApiBaseUri + "lab_panels/" + item.id, item);
  }

  deleteLab_Panel(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "lab_panels/" + id);
  }

  lab_panelsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_panels/do", new TCDoParam(method, payload));
  }

  lab_panelDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_panels/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "lab_panels/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "lab_panels/" + id + "/do", new TCDoParam("print", {}));
  }

  test_panelSearchText: string = "";
  searchTest_Panel(parentId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Test_PanelSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "lab_panels/" + parentId+  "/test_panels", this.test_panelSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Test_PanelSummaryPartialList>(url);

  }

  getTest_Panel(id: string): Observable<Test_PanelDetail> {
      return this.http.get<Test_PanelDetail>(environment.tcApiBaseUri +  "test_panels/" + id);
   }


  addTest_Panel(parentId:string,item: Test_PanelDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_panels/" + parentId+ "/test_panels/", item);
  }

  updateTest_Panel(item: Test_PanelDetail): Observable<Test_PanelDetail> {
    return this.http.patch<Test_PanelDetail>(environment.tcApiBaseUri + "test_panels/" + item.id, item);
  }

  deleteTest_Panel(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "test_panels/" + id);
  }

  test_panelsDo(parentId:string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_panels/" + parentId+ "/test_panels/do", new TCDoParam(method, payload));
  }

  test_panelDo( id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>( environment.tcApiBaseUri + "test_panels/" + id + "/do", new TCDoParam(method, payload));
  }


}
