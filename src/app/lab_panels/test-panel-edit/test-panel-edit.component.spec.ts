import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestPanelEditComponent } from './test-panel-edit.component';

describe('TestPanelEditComponent', () => {
  let component: TestPanelEditComponent;
  let fixture: ComponentFixture<TestPanelEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestPanelEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPanelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
