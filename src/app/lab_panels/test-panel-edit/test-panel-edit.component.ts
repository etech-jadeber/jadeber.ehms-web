import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";

import {Test_PanelDetail} from "../lab_panel.model";
import {Lab_PanelPersist} from "../lab_panel.persist";
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';


@Component({
  selector: 'app-test_panel-edit',
  templateUrl: './test-panel-edit.component.html',
  styleUrls: ['./test-panel-edit.component.css']
})
export class Test_PanelEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  test_panelDetail: Test_PanelDetail = new Test_PanelDetail();
  testFullName:string = "";

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Test_PanelEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: Lab_PanelPersist,
              public  lab_TestPersist: Lab_TestPersist,
              public  lab_TestNavigator: Lab_TestNavigator,
              @Inject(MAT_DIALOG_DATA) public ids: {mode: string, id: string, order_id: string}) {

  }

  isNew(): boolean {
    return this.ids.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.mode === TCModalModes.EDIT;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew()) {
      this.title = "New Test_Panel";
      //set necessary defaults
    } else {
      this.title = "Edit Test_Panel";
      this.isLoadingResults = true;
      this.persist.getTest_Panel(this.ids.id).subscribe(test_panelDetail => {
        this.test_panelDetail = test_panelDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.test_panelDetail == null){
          return false;
        }
      if (this.test_panelDetail.lab_test_id == null){
        return false;
      }
     

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addTest_Panel(this.ids.id, this.test_panelDetail).subscribe(value => {
      this.tcNotification.success("Test_Panel added");
      this.test_panelDetail.id = value.id;
      this.dialogRef.close(this.test_panelDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateTest_Panel(this.test_panelDetail).subscribe(value => {
      this.tcNotification.success("Test_Panel updated");
      this.dialogRef.close(this.test_panelDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  searchTests():void{
    this.lab_TestNavigator.pickLab_Tests(true, this.ids.order_id).afterClosed().subscribe(
      (test:Lab_TestSummary[])=>{
        if (test){this.test_panelDetail.lab_test_id = test[0].id;
        this.testFullName = test[0].name;}
      }
    )
  }
}
