import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestPanelListComponent } from './test-panel-list.component';

describe('TestPanelListComponent', () => {
  let component: TestPanelListComponent;
  let fixture: ComponentFixture<TestPanelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestPanelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPanelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
