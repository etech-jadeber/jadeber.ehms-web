import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { AppTranslation } from 'src/app/app.translation';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { Lab_PanelDetail, Test_PanelDetail, Test_PanelSummary, Test_PanelSummaryPartialList } from '../lab_panel.model';
import { Lab_PanelNavigator } from '../lab_panel.navigator';
import { Lab_PanelPersist } from '../lab_panel.persist';
import { Test_PanelNavigator } from '../test-panel.navigator';
import {Location} from '@angular/common';

@Component({
  selector: 'app-test-panel-list',
  templateUrl: './test-panel-list.component.html',
  styleUrls: ['./test-panel-list.component.scss']
})
export class TestPanelListComponent implements OnInit {
  test_panelsData: Test_PanelSummary[] = [];
  test_panelsTotalCount: number = 0;
  test_panelsSelectAll:boolean = false;
  test_panelsSelection: Test_PanelSummary[] = [];

  test_panelsDisplayedColumns: string[] = ["select","action", "lab_test_id", "lab_test_sequence" ];
  test_panelsSearchTextBox: FormControl = new FormControl();
  test_panelsIsLoading: boolean = false;

  @Input() lab_panelDetail: Lab_PanelDetail
  @ViewChild(MatPaginator, {static: true}) test_panelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) test_panelsSort: MatSort;
  constructor(
    private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              private test_PanelNavigator:Test_PanelNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              private labTestPersist:Lab_TestPersist,
              public lab_panelNavigator: Lab_PanelNavigator,
              public  lab_panelPersist: Lab_PanelPersist,
              public lab_orderPersist: Lab_OrderPersist,
  ) { }

  ngOnInit(): void {
    this.test_panelsSort.sortChange.subscribe(() => {
      this.test_panelsPaginator.pageIndex = 0;
      this.searchTest_Panels(true);
    });

    this.test_panelsPaginator.page.subscribe(() => {
      this.searchTest_Panels(true);
    });

    this.test_panelsPaginator.pageSize = 5;
    //start by loading items
    this.test_panelsSort.active = "lab_test_sequence"
    this.test_panelsSort.direction = "asc"
    this.searchTest_Panels(false);
  }

  addLabTest():void{
    this.test_PanelNavigator.addTest_Panel(this.lab_panelDetail.id, this.lab_panelDetail.lab_order_id).afterClosed().subscribe(
      res=>{
      if (res){
        this.searchTest_Panels()
      }
      }
    )
  }


  editLabTest(id: string):void{
    this.test_PanelNavigator.editTest_Panel(id).afterClosed().subscribe(
      res=>{
      if (res){
        this.searchTest_Panels()
      }
      }
    )
  }



  deleteTest_Panel(item: Test_PanelDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Test_Panel");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lab_panelPersist.deleteTest_Panel(item.id).subscribe(response => {
          this.tcNotification.success("Test Panel Deleted!");
          this.searchTest_Panels();
        }, error => {
        });
      }

    });
  }

  searchTest_Panels(isPagination:boolean = false):void {
    let paginator = this.test_panelsPaginator;
    let sorter = this.test_panelsSort;

    this.test_panelsIsLoading = true;
    this.test_panelsSelection = [];

    this.lab_panelPersist.searchTest_Panel(this.lab_panelDetail.id, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe(
      (partialList: Test_PanelSummaryPartialList) => {
      this.test_panelsData = partialList.data;
      this.test_panelsData.forEach(panel => {
        this.labTestPersist.getLab_Test(panel.lab_test_id).subscribe(order =>
          panel.lab_test_name = order.name);
      })
      if (partialList.total != -1) {
        this.test_panelsTotalCount = partialList.total;
      }
      this.test_panelsIsLoading = false;
    }, error => {
      this.test_panelsIsLoading = false;
    });
  }

  back():void{
    this.location.back();
  }

}
