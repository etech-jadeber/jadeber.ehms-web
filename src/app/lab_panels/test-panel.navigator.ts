import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import { Test_PanelEditComponent } from "./test-panel-edit/test-panel-edit.component";




@Injectable({
  providedIn: 'root'
})

export class Test_PanelNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  editTest_Panel(id:string,  order_id:string = null,mode = TCModalModes.EDIT): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Test_PanelEditComponent, {
      data:  {id, order_id, mode},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addTest_Panel(parentId:string, orderId:string ): MatDialogRef<unknown,any> {
    return this.editTest_Panel(parentId, orderId, TCModalModes.NEW);
  }

}