import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { LabReportDetail, LabReportSummary, LabReportSummaryPartialList } from '../lab_report.model';
import { LabReportPersist } from '../lab_report.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_PanelSummary } from 'src/app/lab_panels/lab_panel.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { lab_order_type } from 'src/app/app.enums';
@Component({
  selector: 'app-lab_report-list',
  templateUrl: './lab-report-list.component.html',
  styleUrls: ['./lab-report-list.component.css']
})
export class LabReportListComponent implements OnInit {
  labReportsData: LabReportSummary[] = [];
  labReportsTotalCount: number = 0;
  labReportSelectAll:boolean = false;
  labReportSelection: LabReportSummary[] = [];
  labPanels: Lab_PanelSummary [] = [];
  labReportsDisplayedColumns: string[] = ["select","action","name","inpatient","outpatient","referral", "emergency", "outside","total" ];
  labReportSearchTextBox: FormControl = new FormControl();
  labReportIsLoading: boolean = false;
  labOrderDetailId: string = "";
  labOrderDetailName: string = "";
  parametersObservable: any;
  diagnosisType: number;
  labTest: Lab_TestSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

  @ViewChild(MatPaginator, {static: true}) labReportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) labReportsSort: MatSort;

constructor(
    private labOrderNavigator: Lab_OrderNavigator,
    private router: ActivatedRoute,
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization:TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate:TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation:AppTranslation,
    public labReportPersist: LabReportPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public labpanelPersist: Lab_PanelPersist,
    public labOrderPersist: Lab_OrderPersist,
    public labTestPersist: Lab_TestPersist,

    ) {
       this.labReportSearchTextBox.setValue(labReportPersist.labReportSearchText);
      //delay subsequent keyup events
      this.labReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.labReportPersist.labReportSearchText = value;
        this.searchLabReports();
      });
      this.from_date.valueChanges.pipe().subscribe(value => {
        this.labReportPersist.from_date = new Date(value._d).getTime()/1000;
        this.searchLabReports();
      });

      this.to_date.valueChanges.pipe().subscribe(value => {
        this.labReportPersist.to_date = new Date(value._d).getTime()/1000;
        this.searchLabReports();
      });
    }

     ngOnInit() {
      this.parametersObservable = this.router.params.subscribe(params => {
        this.route.data.subscribe((result) => { this.diagnosisType = result.diagnosis_type;this.labReportPersist.diagnosisType = this.diagnosisType;})
        this.checkRequireReadAcl()
        this.labOrderDetailName = "";
        this.searchLabReports();
      });
      this.labReportsSort.sortChange.subscribe(() => {
        this.labReportsPaginator.pageIndex = 0;
        this.searchLabReports(true);
      });

      this.labReportsPaginator.page.subscribe(() => {
        this.searchLabReports(true);
      });
      //start by loading items
      this.searchLabReports();
    }

    ngOnDestroy() {
      if(this.parametersObservable != null) {
        this.parametersObservable.unsubscribe();
      }
    }

  searchLabReports(isPagination:boolean = false): void {

    if (this.labOrderDetailId === "") {
      this.labOrderDetailId = TCUtilsString.getInvalidId();
    }
    let paginator = this.labReportsPaginator;
    let sorter = this.labReportsSort;

    this.labReportIsLoading = true;
    this.labReportSelection = [];
    this.labReportPersist.searchLabReport(this.diagnosisType, this.labOrderDetailId ,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: LabReportSummaryPartialList) => {
      this.labReportsData = partialList.data;
      if (partialList.total != -1) {
        this.labReportsTotalCount = partialList.total;
      }
      this.labReportIsLoading = false;
    }, error => {
      this.labReportIsLoading = false;
    });

  }

  chooseLabOrder(): void {
    let dialogRef = this.labOrderNavigator.pickLab_Orders(true, this.diagnosisType);
    dialogRef.afterClosed().subscribe((result: Lab_OrderSummary[]) => {
      this.labOrderDetailId = result.length > 0? result[0].id : "";
      this.labOrderDetailName = result.length > 0? result[0].name : "";
      this.searchLabReports(true);
    })
  }

  getDiagnosisName(): string {
    let name = this.diagnosisType == lab_order_type.imaging? "Imaging" : (this.diagnosisType == lab_order_type.lab? "Lab" : (this.diagnosisType == lab_order_type.pathology? "Pathology" : ""))
    return name;
  }


  downloadLabReports(): void {
    if(this.labReportSelectAll){
         this.labReportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lab_report", true);
      });
    }
    else{
        this.labReportPersist.download(this.tcUtilsArray.idsList(this.labReportSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lab_report",true);
            });
        }
  }
 back():void{
      this.location.back();
    }

    checkRequireReadAcl(){
      if (+this.diagnosisType == lab_order_type.lab){
        this.tcAuthorization.requireRead('laboratory_reports')
      }
      else if (+this.diagnosisType == lab_order_type.imaging){
        this.tcAuthorization.requireRead('radiology_reports')
      }
      else if (+this.diagnosisType == lab_order_type.pathology){
        this.tcAuthorization.requireRead('pathology_reports')
      }
    }

    checkReadAcl(): boolean{
      if (+this.diagnosisType == lab_order_type.lab){
        return this.tcAuthorization.canRead('laboratory_reports')
      }
      else if (+this.diagnosisType == lab_order_type.imaging){
        return this.tcAuthorization.canRead('radiology_reports')
      }
      else if (+this.diagnosisType == lab_order_type.pathology){
        return this.tcAuthorization.canRead('pathology_reports')
      }
    }

    checkUpdateAcl(): boolean{
      if (+this.diagnosisType == lab_order_type.lab){
        return this.tcAuthorization.canUpdate('laboratory_reports')
      }
      else if (+this.diagnosisType == lab_order_type.imaging){
        return this.tcAuthorization.canUpdate('radiology_reports')
      }
      else if (+this.diagnosisType == lab_order_type.pathology){
        return this.tcAuthorization.canUpdate('pathology_reports')
      }
    }

    checkDeleteAcl(): boolean{
      if (+this.diagnosisType == lab_order_type.lab){
        return this.tcAuthorization.canDelete('laboratory_reports')
      }
      else if (+this.diagnosisType == lab_order_type.imaging){
        return this.tcAuthorization.canDelete('radiology_reports')
      }
      else if (+this.diagnosisType == lab_order_type.pathology){
       return this.tcAuthorization.canDelete('pathology_reports')
      }
    }
}
