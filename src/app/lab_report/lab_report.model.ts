import {TCId} from "../tc/models";export class LabReportSummary extends TCId {
    test:string;
    inpatient:number;
    outpatient:number;
    referral:number;
    total:number;
     
    }
    export class LabReportSummaryPartialList {
      data: LabReportSummary[];
      total: number;
    }
    export class LabReportDetail extends LabReportSummary {
    }
    
    export class LabReportDashboard {
      total: number;
    }