import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {LabReportDashboard, LabReportDetail, LabReportSummaryPartialList} from "./lab_report.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class LabReportPersist {
  from_date: number;
 to_date: number;
 is_panel: boolean;
 diagnosisType: number;
 labReportSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.labReportSearchText;
    if (this.to_date){
      fltrs['to_date'] = this.to_date.toString();
    }
    if (this.from_date){
      fltrs['from_date'] = this.from_date.toString();
    }
    if (this.is_panel != null){
    fltrs['is_panel'] = this.is_panel.toString();
    }
    if (this.diagnosisType){
      fltrs['diagnosis_type'] = this.diagnosisType.toString();
      }
    //add custom filters
    return fltrs;
  }

  searchLabReport(diagnosisType: number, labOrderId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<LabReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("lab_order/" + labOrderId + "/lab_report/", this.labReportSearchText, pageSize, pageIndex, sort, order);
    url += "&diagnosis_type=" + diagnosisType;
    if(this.from_date){
      url = TCUtilsString.appendUrlParameter(url, "from_date", this.from_date.toString())
    }
    if(this.to_date){
      url = TCUtilsString.appendUrlParameter(url, "to_date", this.to_date.toString())
    }
    if(this.is_panel != null){
      url = TCUtilsString.appendUrlParameter(url, "is_panel", this.is_panel.toString())
    }
    return this.http.get<LabReportSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_report/do", new TCDoParam("download_all", this.filters()));
  }

  labReportDashboard(): Observable<LabReportDashboard> {
    return this.http.get<LabReportDashboard>(environment.tcApiBaseUri + "lab_report/dashboard");
  }

  getLabReport(id: string): Observable<LabReportDetail> {
    return this.http.get<LabReportDetail>(environment.tcApiBaseUri + "lab_report/" + id);
  } addLabReport(item: LabReportDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_report/", item);
  }

  updateLabReport(item: LabReportDetail): Observable<LabReportDetail> {
    return this.http.patch<LabReportDetail>(environment.tcApiBaseUri + "lab_report/" + item.id, item);
  }

  deleteLabReport(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "lab_report/" + id);
  }
 labReportsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_report/do", new TCDoParam(method, payload));
  }

  labReportDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_reports/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "lab_report/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "lab_report/" + id + "/do", new TCDoParam("print", {}));
  }


}
