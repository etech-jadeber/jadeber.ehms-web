import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Lab_TestDetailComponent } from './lab-test-detail.component';

describe('LabTestDetailComponent', () => {
  let component: Lab_TestDetailComponent;
  let fixture: ComponentFixture<Lab_TestDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Lab_TestDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Lab_TestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
