import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Lab_TestDetail} from "../lab_test.model";
import {Lab_TestPersist} from "../lab_test.persist";
import {Lab_TestNavigator} from "../lab_test.navigator";
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_OrderSummary, Lab_OrderSummaryPartialList } from 'src/app/lab_orders/lab_order.model';
import { lab_order_type } from 'src/app/app.enums';

export enum Lab_TestTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-lab-test-detail',
  templateUrl: './lab-test-detail.component.html',
  styleUrls: ['./lab-test-detail.component.css']
})
export class Lab_TestDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  lab_testLoading:boolean = false;
  lab_order_type = lab_order_type

  Lab_TestTabs: typeof Lab_TestTabs = Lab_TestTabs;
  activeTab: Lab_TestTabs = Lab_TestTabs.overview;
  //basics
  lab_testDetail: Lab_TestDetail;
  lab_ordersData: Lab_OrderSummary[] = []

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public lab_testNavigator: Lab_TestNavigator,
              public  lab_testPersist: Lab_TestPersist,
              public lab_orderPersist: Lab_OrderPersist,) {
    this.tcAuthorization.requireRead("order_tests");
    this.lab_testDetail = new Lab_TestDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("order_tests");
    this.searchLab_Orders();
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.lab_testLoading = true;
    this.lab_testPersist.getLab_Test(id).subscribe(lab_testDetail => {
          this.lab_testDetail = lab_testDetail;
          this.lab_testLoading = false;
        }, error => {
          console.error(error);
          this.lab_testLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.lab_testDetail.id);
  }

  editLab_Test(): void {
    let modalRef = this.lab_testNavigator.editLab_Test(this.lab_testDetail.id);
    modalRef.afterClosed().subscribe(modifiedLab_TestDetail => {
      TCUtilsAngular.assign(this.lab_testDetail, modifiedLab_TestDetail);
    }, error => {
      console.error(error);
    });
  }

   printLab_Test():void{
      this.lab_testPersist.print(this.lab_testDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print lab_test", true);
      });
    }

  back():void{
      this.location.back();
    }
    searchLab_Orders(): void {

      this.lab_orderPersist.searchLab_Order(50, 0, "", "asc").subscribe((partialList: Lab_OrderSummaryPartialList) => {
        this.lab_ordersData = partialList.data;
    });
}

getLabOrders(itemID:string){
   let laborders:Lab_OrderSummary = this.tcUtilsArray.getById(
      this.lab_ordersData,
      itemID
    )
    if(laborders){
      return laborders.name;
    }
}
}
