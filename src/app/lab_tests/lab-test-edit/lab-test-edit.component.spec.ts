import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabTestEditComponent } from './lab-test-edit.component';

describe('LabTestEditComponent', () => {
  let component: LabTestEditComponent;
  let fixture: ComponentFixture<LabTestEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabTestEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
