import {Component, OnInit, Inject, Input} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Lab_TestDetail} from "../lab_test.model";
import {Lab_TestPersist} from "../lab_test.persist";
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { FeePersist } from 'src/app/fees/fee.persist';
import { MeasuringUnitNavigator } from 'src/app/measuring_unit/measuringUnit.navigator';
import { MeasuringUnitSummary } from 'src/app/measuring_unit/measuringUnit.model';
import { MeasuringUnitPersist } from 'src/app/measuring_unit/measuringUnit.persist';


@Component({
  selector: 'app-lab-test-edit',
  templateUrl: './lab-test-edit.component.html',
  styleUrls: ['./lab-test-edit.component.css']
})
export class Lab_TestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lab_testDetail: Lab_TestDetail;
  days;
  hours;
  minutes;
  testUnitName: string;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Lab_TestEditComponent>,
              public  persist: Lab_TestPersist,
              public  labOrderPersist: Lab_OrderPersist,
              public  labOrderNavigator: Lab_OrderNavigator,
              public tcUtilsArray: TCUtilsArray,
              public feePersist: FeePersist,
              public measuringUnitNavigator: MeasuringUnitNavigator,
              public measuringUnitPersist: MeasuringUnitPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("order_tests");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText('laboratory','lab_test_id');
      this.lab_testDetail = new Lab_TestDetail();
      //set necessary defaults
      this.lab_testDetail.vat_status = -1;
      if(this.idMode.id){
        this.labOrderPersist.getLab_Order(this.idMode.id).subscribe(order => {
          this.labOrderName = order.name;
          this.lab_testDetail.lab_order_id = order.id
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          this.isLoadingResults = false;
        })
      }

    } else {
     this.tcAuthorization.requireUpdate("order_tests");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText('laboratory','lab_test_id');
      this.isLoadingResults = true;
      this.persist.getLab_Test(this.idMode.id).subscribe(lab_testDetail => {
        this.lab_testDetail = lab_testDetail;
        this.convertDate(this.lab_testDetail.tat_time)
        this.labOrderPersist.getLab_Order(this.lab_testDetail.lab_order_id).subscribe(orderDetail => {
          this.labOrderName = orderDetail.name;
        })
        this.measuringUnitPersist.getMeasuringUnit(this.lab_testDetail.test_unit_id).subscribe(measuringUnit => {
          this.testUnitName = measuringUnit.unit;
          this.isLoadingResults = false;
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  convertDate(date: number) {
    if (date == -1 || !date){
      return;
    }
    this.days = this.zeroPad(Math.floor((date) / (24 * 3600)), 2)
    const dayRem = (date) % (24 * 3600)
    this.hours = this.zeroPad(Math.floor(dayRem / 3600), 2)
    const hourRem = dayRem % 3600
    this.minutes = this.zeroPad(Math.floor(hourRem / 60), 2)
  }

  zeroPad = (num, places) => String(num).padStart(places, '0')

  check(type) {
    if (type === 'hours') {
      this.hours = this.getValidValue(this.hours, 23);
    } else if (type === 'minutes') {
      this.minutes = this.getValidValue(this.minutes, 59);
    }
    else if (type === 'days') {
      this.days = this.getValidValue(this.days, 99);
    }

  }

  getDuration() {
    console.log(this.days, this.hours, this.minutes)
    return (this.days || this.hours || this.minutes) ?  (this.days || 0) * 3600 * 24 + (this.hours || 0) * 3600 + (this.minutes || 0) * 60 : -1;
  }

  getValidValue(value, max) {
    let n;
    if (/^\d+$/.test(value)) {
      n = parseInt(value);
      n = Math.max(0, n);
      n = Math.min(max, n);
      return this.zeroPad(n, 2);
    } else {
      return new String('00');
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.lab_testDetail.tat_time = this.getDuration()
    this.persist.addLab_Test(this.lab_testDetail).subscribe(value => {
      this.tcNotification.success("Lab_Test added");
      this.lab_testDetail.id = value.id;
      this.dialogRef.close(this.lab_testDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    console.log(this.getDuration())
    this.lab_testDetail.tat_time = this.getDuration()
    this.persist.updateLab_Test(this.lab_testDetail).subscribe(value => {
      this.tcNotification.success("Lab_Test updated");
      this.dialogRef.close(this.lab_testDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.lab_testDetail == null){
            return false;
          }

        if (!this.lab_testDetail.lab_order_id){
          return false;
        }

        if (this.lab_testDetail.name == null || this.lab_testDetail.name  == "") {
                      return false;
                    }
        // if (this.lab_testDetail.min_range != null && this.lab_testDetail.max_range != null && this.lab_testDetail.min_range > this.lab_testDetail.max_range){
        //   return false;
        // }
        if (this.lab_testDetail.price == null){
          return false;
        }
        if (this.lab_testDetail.vat_status == -1){
          return false
        }
        if (this.lab_testDetail.test_sequence == null){
          return false
        }


        return true;
      }
      labOrderName:string;
      searchLabOrders() {
        let dialogRef = this.labOrderNavigator.pickLab_Orders(true);
        dialogRef.afterClosed().subscribe((result: Lab_OrderSummary[]) => {
          if (result) {
            this.labOrderName = result[0].name
            this.lab_testDetail.lab_order_id = result[0].id;
          }
        });
      }

      searchMeasuringUnit() {
        let dialogRef = this.measuringUnitNavigator.pickMeasuringUnits(true);
        dialogRef.afterClosed().subscribe((result: MeasuringUnitSummary[]) => {
          if (result) {
            this.testUnitName = result[0].unit
            this.lab_testDetail.test_unit_id = result[0].id;
          }
        });
      }
}
