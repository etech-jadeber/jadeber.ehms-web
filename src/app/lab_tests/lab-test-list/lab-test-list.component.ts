import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Lab_TestPersist} from "../lab_test.persist";
import {Lab_TestNavigator} from "../lab_test.navigator";
import {Lab_TestDetail, Lab_TestSummary, Lab_TestSummaryPartialList} from "../lab_test.model";
import { Lab_OrderSummary, Lab_OrderSummaryPartialList } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { ActivenessStatus } from 'src/app/app.enums';
import { TCUtilsNumber } from 'src/app/tc/utils-number';


@Component({
  selector: 'app-lab-test-list',
  templateUrl: './lab-test-list.component.html',
  styleUrls: ['./lab-test-list.component.css']
})
export class Lab_TestListComponent implements OnInit {
  @Input() orderId:string;
  
  lab_testsData: Lab_TestSummary[] = [];
  lab_testsTotalCount: number = 0;
  lab_testsSelectAll:boolean = false;
  lab_testsSelection: Lab_TestSummary[] = [];

  lab_testsDisplayedColumns: string[] = ["select","action","name","price",  "test_sequence"];
  lab_testsSearchTextBox: FormControl = new FormControl();
  lab_testsIsLoading: boolean = false;
  activnessStatus = ActivenessStatus;

  lab_ordersData: Lab_OrderSummary[] = []
  @ViewChild(MatPaginator, {static: true}) lab_testsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_testsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lab_testPersist: Lab_TestPersist,
                public lab_testNavigator: Lab_TestNavigator,
                public lab_orderPersist: Lab_OrderPersist,
                public jobPersist: JobPersist,
                public tcUtilsNumber: TCUtilsNumber,
    ) {

        this.tcAuthorization.requireRead("order_tests");
       this.lab_testsSearchTextBox.setValue(lab_testPersist.lab_testSearchText);
      //delay subsequent keyup events
      this.lab_testsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lab_testPersist.lab_testSearchText = value;
        this.searchLab_Tests();
      });
    }

    ngOnInit() {
      this.lab_testPersist.lab_order_id = this.orderId;
      this.lab_testsSort.sortChange.subscribe(() => {
        this.lab_testsPaginator.pageIndex = 0;
        this.searchLab_Tests(true);
      });

      this.lab_testsPaginator.page.subscribe(() => {
        this.searchLab_Tests(true);
      });

      this.lab_testsPaginator.pageSize = 5;
      //start by loading items
      this.searchLab_Tests();
    }

    ngOnDestroy(){
      this.lab_testPersist.lab_order_id = null;
    }

  searchLab_Tests(isPagination:boolean = false): void {


    let paginator = this.lab_testsPaginator;
    let sorter = this.lab_testsSort;

    this.lab_testsIsLoading = true;
    this.lab_testsSelection = [];

    this.lab_testPersist.searchLab_Test(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Lab_TestSummaryPartialList) => {
      this.lab_testsData = partialList.data;
      if (partialList.total != -1) {
        this.lab_testsTotalCount = partialList.total;
      }
      this.lab_testsIsLoading = false;
    }, error => {
      this.lab_testsIsLoading = false;
    });

  }

  downloadLab_Tests(): void {
    if(this.lab_testsSelectAll){
         this.lab_testPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lab_tests", true);
      });
    }
    else{
        this.lab_testPersist.download(this.tcUtilsArray.idsList(this.lab_testsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lab_tests",true);
            });
        }
  }



  addLab_Test(): void {
    let dialogRef = this.lab_testNavigator.addLab_Test(this.orderId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLab_Tests();
      }
    });
  }

  editLab_Test(item: Lab_TestSummary) {
    let dialogRef = this.lab_testNavigator.editLab_Test(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  updateActveness(item: Lab_TestSummary, method: string) {
    this.lab_testPersist.lab_testDo(item.id, method, {}).subscribe(
      value => {
        this.tcNotification.success("The status is updated")
        TCUtilsAngular.assign(item, {...item, status : method == 'active' ? ActivenessStatus.active : ActivenessStatus.freeze})
      }
    )
  }
  deleteLab_Test(item: Lab_TestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Lab_Test");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lab_testPersist.deleteLab_Test(item.id).subscribe(response => {
          this.tcNotification.success("Lab_Test deleted");
          this.searchLab_Tests();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }



}

