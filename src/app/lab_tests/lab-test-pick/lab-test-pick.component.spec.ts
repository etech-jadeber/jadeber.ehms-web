import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LabTestPickComponent } from './lab-test-pick.component';

describe('LabTestPickComponent', () => {
  let component: LabTestPickComponent;
  let fixture: ComponentFixture<LabTestPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LabTestPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabTestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
