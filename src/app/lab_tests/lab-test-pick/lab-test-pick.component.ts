import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Lab_TestDetail, Lab_TestSummary, Lab_TestSummaryPartialList} from "../lab_test.model";
import {Lab_TestPersist} from "../lab_test.persist";
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';


@Component({
  selector: 'app-lab-test-pick',
  templateUrl: './lab-test-pick.component.html',
  styleUrls: ['./lab-test-pick.component.css']
})
export class Lab_TestPickComponent implements OnInit {

  lab_testsData: Lab_TestSummary[] = [];
  lab_testsTotalCount: number = 0;
  lab_testsSelection: Lab_TestSummary[] = [];
  lab_testsDisplayedColumns: string[] = ["select", 'lab_order_id','name','min_range','max_range','price' ];

  lab_testsSearchTextBox: FormControl = new FormControl();
  lab_testsIsLoading: boolean = false;
  selectOne: boolean;

  @ViewChild(MatPaginator, {static: true}) lab_testsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lab_testsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public lab_testPersist: Lab_TestPersist,
              public labOrderPersist: Lab_OrderPersist,
              public dialogRef: MatDialogRef<Lab_TestPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data :{selectOne: boolean, lab_order_id: string, lab_order_type: number}
  ) {
    this.tcAuthorization.requireRead("order_tests");
    this.lab_testsSearchTextBox.setValue(lab_testPersist.lab_testSearchText);
    //delay subsequent keyup events
    this.lab_testsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.lab_testPersist.lab_testSearchText = value;
      this.lab_testsPaginator.pageIndex = 0;
      this.searchLab_Tests();
    });
    this.selectOne = data.selectOne
    this.lab_testPersist.lab_order_type = this.data.lab_order_type;
    this.lab_testPersist.lab_order_id = this.data.lab_order_id
  }

  ngOnInit() {

    this.lab_testsSort.sortChange.subscribe(() => {
      this.lab_testsPaginator.pageIndex = 0;
      this.searchLab_Tests();
    });

    this.lab_testsPaginator.page.subscribe(() => {
      this.searchLab_Tests();
    });

    //set initial picker list to 5
    this.lab_testsPaginator.pageSize = 5;

    //start by loading items
    this.searchLab_Tests();
    // this.SearchLabOrders();
  }

  searchLab_Tests(): void {
    this.lab_testsIsLoading = true;
    this.lab_testsSelection = [];

    this.lab_testPersist.searchLab_Test(this.lab_testsPaginator.pageSize,
        this.lab_testsPaginator.pageIndex,
        this.lab_testsSort.active,
        this.lab_testsSort.direction).subscribe((partialList: Lab_TestSummaryPartialList) => {
      this.lab_testsData = partialList.data;
      this.lab_testsData.forEach(lab_test => {
        this.getLabOrderName(lab_test)
      })
      if (partialList.total != -1) {
        this.lab_testsTotalCount = partialList.total;
      }
      this.lab_testsIsLoading = false;
    }, error => {
      this.lab_testsIsLoading = false;
    });

  }

  markOneItem(item: Lab_TestSummary) {
    if(!this.tcUtilsArray.containsId(this.lab_testsSelection,item.id)){
          this.lab_testsSelection = [];
          this.lab_testsSelection.push(item);
        }
        else{
          this.lab_testsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.lab_testsSelection);
  }
  labOrderData: Lab_OrderSummary[];

  // SearchLabOrders(){
  //   this.labOrderPersist.searchLab_Order(null, 50, 0, "", "asc").subscribe(result=>{
  //     this.labOrderData = result.data;
  //   })
    
  // }
  
   getLabOrderName(lab_test: Lab_TestSummary){
      let labOrderDatas = this.labOrderPersist.getLab_Order(lab_test.lab_order_id).subscribe(
        lab_order => lab_test.lab_order_name = lab_order.name
      );
      }

  onCancel(): void {
    this.dialogRef.close();
  }

}
