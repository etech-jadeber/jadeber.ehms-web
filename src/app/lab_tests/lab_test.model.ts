import {TCId} from "../tc/models";
import { TCUtilsString } from "../tc/utils-string";

export class Lab_TestSummary extends TCId {
  lab_order_id : string;
name : string;
type: number;
// min_range : number;
// max_range : number;
price : number;
vat_status: number;
test_unit: number;
lab_order_name: string;
tat_time: number;
status: number;
test_sequence: number;
test_unit_id: string;
}


export class Lab_TestSummaryPartialList {
  data: Lab_TestSummary[];
  total: number;
}

export class Lab_TestDetail extends Lab_TestSummary {
  lab_order_id : string;
name : string;
min_range : number;
max_range : number;
price : number;
vat_status: number;
}

export class ExtendedLab_TestSummary extends Lab_TestDetail {
  lab_panel_id : string= TCUtilsString.getInvalidId();

}

export class Lab_TestDashboard {
  total: number;
}
