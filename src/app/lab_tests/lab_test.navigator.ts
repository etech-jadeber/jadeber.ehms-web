import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Lab_TestEditComponent} from "./lab-test-edit/lab-test-edit.component";
import {Lab_TestPickComponent} from "./lab-test-pick/lab-test-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Lab_TestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  lab_testsUrl(): string {
    return "/lab_tests";
  }

  lab_testUrl(id: string): string {
    return "/lab_tests/" + id;
  }

  viewLab_Tests(): void {
    this.router.navigateByUrl(this.lab_testsUrl());
  }

  viewLab_Test(id: string): void {
    this.router.navigateByUrl(this.lab_testUrl(id));
  }

  editLab_Test(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_TestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLab_Test(parentId: string = null): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Lab_TestEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickLab_Tests(selectOne: boolean=false, lab_order_id: string = null, lab_order_type: number = null): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Lab_TestPickComponent, {
        data: {selectOne, lab_order_type, lab_order_id},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
