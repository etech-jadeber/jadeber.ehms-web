import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Lab_TestDashboard, Lab_TestDetail, Lab_TestSummaryPartialList} from "./lab_test.model";
import { MeasuringUnit } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Lab_TestPersist {

  lab_testSearchText: string = "";
  lab_order_id: string;
  lab_order_type: number;

  measuringUnitFilter: number ;

    MeasuringUnit: TCEnum[] = [
     new TCEnum( MeasuringUnit["X10^3/MM^3"], 'x10^3/mm^3'),
  new TCEnum( MeasuringUnit.PG, 'pg'),
  new TCEnum( MeasuringUnit['U/L'], 'U/L'),
  new TCEnum( MeasuringUnit['G/DL'], 'g/dl'),
  new TCEnum( MeasuringUnit['%'], '%'),
  new TCEnum( MeasuringUnit.FL, 'fl'),
  new TCEnum( MeasuringUnit['X10^6/UL'], 'x10^6/uL'),
  new TCEnum( MeasuringUnit['NG/DL'], 'ng/dl'),
  new TCEnum( MeasuringUnit['ML/MIN/1.73M^2'], 'mL/min/1.73m^2'),
  new TCEnum( MeasuringUnit['PG/ML'], 'Pg/ml'),
  new TCEnum( MeasuringUnit['MICRO IU/ML'], 'Micro IU/ml'),
  new TCEnum( MeasuringUnit['CELLS/HPF'], 'Cells/HPF'),
  new TCEnum( MeasuringUnit.HPF, 'hpf'),
  new TCEnum( MeasuringUnit.IPF, 'ipf'),

  ];

  constructor(private http: HttpClient) {
  }
  
  searchLab_Test(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Lab_TestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("lab_tests", this.lab_testSearchText, pageSize, pageIndex, sort, order);
    if(this.lab_order_id){
      url = TCUtilsString.appendUrlParameter(url, "lab_order_id", this.lab_order_id)
    }
    if (this.lab_order_type){
      url = TCUtilsString.appendUrlParameter(url, "type", this.lab_order_type.toString())
    }
    return this.http.get<Lab_TestSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.lab_testSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_tests/do", new TCDoParam("download_all", this.filters()));
  }

  lab_testDashboard(): Observable<Lab_TestDashboard> {
    return this.http.get<Lab_TestDashboard>(environment.tcApiBaseUri + "lab_tests/dashboard");
  }

  getLab_Test(id: string): Observable<Lab_TestDetail> {
    return this.http.get<Lab_TestDetail>(environment.tcApiBaseUri + "lab_tests/" + id);
  }

  addLab_Test(item: Lab_TestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "lab_tests/", item);
  }

  updateLab_Test(item: Lab_TestDetail): Observable<Lab_TestDetail> {
    return this.http.patch<Lab_TestDetail>(environment.tcApiBaseUri + "lab_tests/" + item.id, item);
  }

  deleteLab_Test(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "lab_tests/" + id);
  }

  lab_testsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_tests/do", new TCDoParam(method, payload));
  }

  lab_testDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "lab_tests/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "lab_tests/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "lab_tests/" + id + "/do", new TCDoParam("print", {}));
  }


}
