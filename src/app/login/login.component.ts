import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";

import {TcDictionary} from "../tc/models";
import {TCAuthentication} from "../tc/authentication";
import {TCNavigator} from "../tc/navigator";
import { TCAppInit } from '../tc/app-init';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router, private activatedRoute: ActivatedRoute, private http: HttpClient, private tcAuthorization: TCAuthentication, public tcNavigator: TCNavigator) {
  }

  params: TcDictionary<string> = {};

  loggedIn(token: string): void {

    this.tcAuthorization.login(token, () => {
      // this.tcNavigator.dashboard();
      this.router.navigateByUrl('/home')
    }, true);

  }

  ngOnInit() {
    if (TCAppInit.isLoggedIn) {
      // this.tcNavigator.dashboard();
      this.router.navigateByUrl('/home')
    } else {
      //fragment url parameters and extract token
      this.activatedRoute.fragment.subscribe((fragment: string) => {
        fragment = fragment.substr(7);
        for (var keyVal of fragment.split("&")) {
          var split = keyVal.split("=");
          if (split.length == 2) {
            this.params[split[0]] = split[1];
          }
        }
        if (fragment.includes("token")) {
          this.loggedIn(this.params["token"]);
          let refreshToken = this.params['refresh_token'];
      
          this.tcAuthorization.persisitRefresh(refreshToken);
          console.log();
          

        } else {
          console.error("token not found");
        }
      });
    }
  }

}