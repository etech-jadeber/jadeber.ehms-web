import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LostDamageDetailComponent } from './lost-damage-detail.component';

describe('LostDamageDetailComponent', () => {
  let component: LostDamageDetailComponent;
  let fixture: ComponentFixture<LostDamageDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LostDamageDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostDamageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
