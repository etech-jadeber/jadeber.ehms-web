import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {LostDamageDetail} from "../lostDamage.model";
import {LostDamagePersist} from "../lostDamage.persist";
import {LostDamageNavigator} from "../lostDamage.navigator";

export enum LostDamageTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-lost_damage-detail',
  templateUrl: './lost-damage-detail.component.html',
  styleUrls: ['./lost-damage-detail.component.css']
})
export class LostDamageDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  lostDamageIsLoading:boolean = false;
  
  LostDamageTabs: typeof LostDamageTabs = LostDamageTabs;
  activeTab: LostDamageTabs = LostDamageTabs.overview;
  //basics
  lostDamageDetail: LostDamageDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public lostDamageNavigator: LostDamageNavigator,
              public  lostDamagePersist: LostDamagePersist) {
    this.tcAuthorization.requireRead("lost_damages");
    this.lostDamageDetail = new LostDamageDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("lost_damages");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.lostDamageIsLoading = true;
    this.lostDamagePersist.getLostDamage(id).subscribe(lostDamageDetail => {
          this.lostDamageDetail = lostDamageDetail;
          this.lostDamageIsLoading = false;
        }, error => {
          console.error(error);
          this.lostDamageIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.lostDamageDetail.id);
  }
  editLostDamage(): void {
    let modalRef = this.lostDamageNavigator.editLostDamage(this.lostDamageDetail.id);
    modalRef.afterClosed().subscribe(lostDamageDetail => {
      TCUtilsAngular.assign(this.lostDamageDetail, lostDamageDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.lostDamagePersist.print(this.lostDamageDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print lost_damage", true);
      });
    }

  back():void{
      this.location.back();
    }

}