import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LostDamageEditComponent } from './lost-damage-edit.component';

describe('LostDamageEditComponent', () => {
  let component: LostDamageEditComponent;
  let fixture: ComponentFixture<LostDamageEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LostDamageEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostDamageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
