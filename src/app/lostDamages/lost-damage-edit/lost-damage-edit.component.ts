import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { LostDamageDetail } from '../lostDamage.model';
import { LostDamagePersist } from '../lostDamage.persist';import { RequestPersist } from 'src/app/requests/request.persist';
import { units } from 'src/app/app.enums';
@Component({
  selector: 'app-lost_damage-edit',
  templateUrl: './lost-damage-edit.component.html',
  styleUrls: ['./lost-damage-edit.component.css']
})export class LostDamageEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lostDamageDetail: LostDamageDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<LostDamageEditComponent>,
              public  persist: LostDamagePersist,
              public requestPersist: RequestPersist,
              public tcUtilsDate: TCUtilsDate,
              public lostDamagePersist: LostDamagePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("lost_damages");
      this.title = this.appTranslation.getText("general","new") +  " " + "lost_damage";
      this.lostDamageDetail = new LostDamageDetail();
      this.lostDamageDetail.unit = units.piece;
      //set necessary defaults
      this.lostDamageDetail.transfer_or_receive_id = this.idMode.id;

    } else {
     this.tcAuthorization.requireUpdate("lost_damages");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "lost_damage";
      this.isLoadingResults = true;
      this.persist.getLostDamage(this.idMode.id).subscribe(lostDamageDetail => {
        this.lostDamageDetail = lostDamageDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addLostDamage(this.lostDamageDetail).subscribe(value => {
      this.tcNotification.success("lostDamage added");
      this.lostDamageDetail.id = value.id;
      this.dialogRef.close(this.lostDamageDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateLostDamage(this.lostDamageDetail).subscribe(value => {
      this.tcNotification.success("lost_damage updated");
      this.dialogRef.close(this.lostDamageDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.lostDamageDetail == null){
            return false;
          }

if (this.lostDamageDetail.transfer_or_receive_id == null || this.lostDamageDetail.transfer_or_receive_id  == "") {
            return false;
        }
if (this.lostDamageDetail.reason == null || this.lostDamageDetail.reason  == "") {
            return false;
        }
if (this.lostDamageDetail.quantity == null) {
            return false;
        }
if (this.lostDamageDetail.unit == null) {
            return false;
        }
        if (this.lostDamageDetail.status == null) {
          return false;
      }
 return true;

 }
 }