import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LostDamageListComponent } from './lost-damage-list.component';

describe('LostDamageListComponent', () => {
  let component: LostDamageListComponent;
  let fixture: ComponentFixture<LostDamageListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LostDamageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostDamageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
