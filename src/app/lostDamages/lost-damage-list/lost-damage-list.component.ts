import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { LostDamageSummary, LostDamageSummaryPartialList } from '../lostDamage.model';
import { LostDamagePersist } from '../lostDamage.persist';
import { LostDamageNavigator } from '../lostDamage.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { LostDamageStatus } from 'src/app/app.enums';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
@Component({
  selector: 'app-lost_damage-list',
  templateUrl: './lost-damage-list.component.html',
  styleUrls: ['./lost-damage-list.component.css']
})export class LostDamageListComponent implements OnInit {
  lostDamagesData: LostDamageSummary[] = [];
  lostDamagesTotalCount: number = 0;
  lostDamageSelectAll:boolean = false;
  lostDamageSelection: LostDamageSummary[] = [];
  categoryName: string;
  subCategoryName: string;
  categoryId: string;

 lostDamagesDisplayedColumns: string[] = ["select","action","name", "batch_no", "quantity", "unit", "date","reason","status","date_of_disposal" ];
  lostDamageSearchTextBox: FormControl = new FormControl();
  lostDamageIsLoading: boolean = false;  

  lostDamageStatus = LostDamageStatus
  
  @ViewChild(MatPaginator, {static: true}) lostDamagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lostDamagesSort: MatSort;
  @Input() lostDamageId: string;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lostDamagePersist: LostDamagePersist,
                public lostDamageNavigator: LostDamageNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public requestPersist: RequestPersist,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public storeNavigator: StoreNavigator,
                public tcUtilsString: TCUtilsString,
                public formEncounterPersist: Form_EncounterPersist,
                public categoryNavigator: ItemCategoryNavigator,
                public categoryPersist: ItemCategoryPersist,

    ) {

        this.tcAuthorization.requireRead("lost_damages");
       this.lostDamageSearchTextBox.setValue(lostDamagePersist.lostDamageSearchText);
      //delay subsequent keyup events
      this.lostDamageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lostDamagePersist.lostDamageSearchText = value;
        this.searchLostDamages();
      });
      this.lostDamagePersist.start_date.valueChanges.subscribe((value)=>{

        this.searchLostDamages()
    });
    this.lostDamagePersist.end_date.valueChanges.subscribe((value)=>{
        this.searchLostDamages()
    });
    } ngOnInit() {
   
      this.lostDamagesSort.sortChange.subscribe(() => {
        this.lostDamagesPaginator.pageIndex = 0;
        this.searchLostDamages(true);
      });

      this.lostDamagesPaginator.page.subscribe(() => {
        this.searchLostDamages(true);
      });
      //start by loading items
      this.searchLostDamages();
    }

  searchLostDamages(isPagination:boolean = false): void {


    let paginator = this.lostDamagesPaginator;
    let sorter = this.lostDamagesSort;

    this.lostDamageIsLoading = true;
    this.lostDamageSelection = [];

    this.lostDamagePersist.searchLostDamage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: LostDamageSummaryPartialList) => {
      this.lostDamagesData = partialList.data;
      if (partialList.total != -1) {
        this.lostDamagesTotalCount = partialList.total;
      }
      this.lostDamageIsLoading = false;
    }, error => {
      this.lostDamageIsLoading = false;
    });

  } downloadLostDamages(): void {
    if(this.lostDamageSelectAll){
         this.lostDamagePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download lost_damage", true);
      });
    }
    else{
        this.lostDamagePersist.download(this.tcUtilsArray.idsList(this.lostDamageSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download lost_damage",true);
            });
        }
  }


  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.lostDamagePersist.store_name = result[0].name;
        this.lostDamagePersist.store_id = result[0].id;
        this.searchLostDamages();
      }
    });
  }
  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.lostDamagePersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.lostDamagePersist.categoryId = result[0].id;
      }
      this.searchLostDamages()
    })
  }
addLostDamage(): void {
    let dialogRef = this.lostDamageNavigator.addLostDamage(this.lostDamageId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLostDamages();
      }
    });
  }

  dispose(item: LostDamageSummary){
    this.lostDamagePersist.lostDamageDo(item.id, "dispose", {}).subscribe(
      (result) => {
        this.searchLostDamages()
      }
    )
  }

  editLostDamage(item: LostDamageSummary) {
    let dialogRef = this.lostDamageNavigator.editLostDamage(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteLostDamage(item: LostDamageSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("lost_damage");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lostDamagePersist.deleteLostDamage(item.id).subscribe(response => {
          this.tcNotification.success("lost_damage deleted");
          this.searchLostDamages();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}