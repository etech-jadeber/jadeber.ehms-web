import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LostDamagePickComponent } from './lost-damage-pick.component';

describe('LostDamagePickComponent', () => {
  let component: LostDamagePickComponent;
  let fixture: ComponentFixture<LostDamagePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LostDamagePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LostDamagePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
