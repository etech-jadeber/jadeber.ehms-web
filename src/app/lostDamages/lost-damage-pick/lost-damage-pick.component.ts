import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { LostDamageSummary, LostDamageSummaryPartialList } from '../lostDamage.model';
import { LostDamagePersist } from '../lostDamage.persist';
import { LostDamageNavigator } from '../lostDamage.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-lost_damage-pick',
  templateUrl: './lost-damage-pick.component.html',
  styleUrls: ['./lost-damage-pick.component.css']
})export class LostDamagePickComponent implements OnInit {
  lostDamagesData: LostDamageSummary[] = [];
  lostDamagesTotalCount: number = 0;
  lostDamageSelectAll:boolean = false;
  lostDamageSelection: LostDamageSummary[] = [];

 lostDamagesDisplayedColumns: string[] = ["select", ,"transfer_or_receive_id","date","reason","user_id","status","quantity","unit","date_of_disposal","disposed_by" ];
  lostDamageSearchTextBox: FormControl = new FormControl();
  lostDamageIsLoading: boolean = false; 
   @ViewChild(MatPaginator, {static: true}) lostDamagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) lostDamagesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lostDamagePersist: LostDamagePersist,
                public lostDamageNavigator: LostDamageNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<LostDamagePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("lost_damages");
       this.lostDamageSearchTextBox.setValue(lostDamagePersist.lostDamageSearchText);
      //delay subsequent keyup events
      this.lostDamageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lostDamagePersist.lostDamageSearchText = value;
        this.searchLost_damages();
      });
    } ngOnInit() {
   
      this.lostDamagesSort.sortChange.subscribe(() => {
        this.lostDamagesPaginator.pageIndex = 0;
        this.searchLost_damages(true);
      });

      this.lostDamagesPaginator.page.subscribe(() => {
        this.searchLost_damages(true);
      });
      //start by loading items
      this.searchLost_damages();
    }

  searchLost_damages(isPagination:boolean = false): void {


    let paginator = this.lostDamagesPaginator;
    let sorter = this.lostDamagesSort;

    this.lostDamageIsLoading = true;
    this.lostDamageSelection = [];

    this.lostDamagePersist.searchLostDamage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: LostDamageSummaryPartialList) => {
      this.lostDamagesData = partialList.data;
      if (partialList.total != -1) {
        this.lostDamagesTotalCount = partialList.total;
      }
      this.lostDamageIsLoading = false;
    }, error => {
      this.lostDamageIsLoading = false;
    });

  }
  markOneItem(item: LostDamageSummary) {
    if(!this.tcUtilsArray.containsId(this.lostDamageSelection,item.id)){
          this.lostDamageSelection = [];
          this.lostDamageSelection.push(item);
        }
        else{
          this.lostDamageSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.lostDamageSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }