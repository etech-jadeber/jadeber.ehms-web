import {TCId} from "../tc/models";

export class LostDamageSummary extends TCId {
  transfer_or_receive_id:string;
  date:number;
  reason:string;
  user_id:string;
  status:number;
  quantity:number;
  unit:number;
  date_of_disposal:number;
  disposed_by:string;
  item_id: string;
  item_name: string;
   
  }
  export class LostDamageSummaryPartialList {
    data: LostDamageSummary[];
    total: number;
  }
  export class LostDamageDetail extends LostDamageSummary {
  }
  
  export class LostDamageDashboard {
    total: number;
  }