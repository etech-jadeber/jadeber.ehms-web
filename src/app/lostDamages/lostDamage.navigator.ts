import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {LostDamageEditComponent} from "./lost-damage-edit/lost-damage-edit.component";
import {LostDamagePickComponent} from "./lost-damage-pick/lost-damage-pick.component";


@Injectable({
  providedIn: 'root'
})

export class LostDamageNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  lostDamagesUrl(): string {
    return "/lost_damages";
  }

  lostDamageUrl(id: string): string {
    return "/lost_damages/" + id;
  }

  viewLostDamages(): void {
    this.router.navigateByUrl(this.lostDamagesUrl());
  }

  viewLostDamage(id: string): void {
    this.router.navigateByUrl(this.lostDamageUrl(id));
  }

  editLostDamage(id: string): MatDialogRef<LostDamageEditComponent> {
    const dialogRef = this.dialog.open(LostDamageEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLostDamage(parent_id: string): MatDialogRef<LostDamageEditComponent> {
    const dialogRef = this.dialog.open(LostDamageEditComponent, {
          data: new TCIdMode(parent_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickLostDamages(selectOne: boolean=false): MatDialogRef<LostDamagePickComponent> {
      const dialogRef = this.dialog.open(LostDamagePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}