import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnumTranslation, TCEnum } from '../tc/models';
import {
  LostDamageDashboard,
  LostDamageDetail,
  LostDamageSummaryPartialList,
} from './lostDamage.model';
import { LostDamageStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from "@angular/forms";

@Injectable({
  providedIn: 'root',
})
export class LostDamagePersist {
  start_date: FormControl = new FormControl({disabled: true, value: ""});
  end_date: FormControl = new FormControl({disabled: true, value: ""});
  lostDamageSearchText: string = '';
  category: number;
  categoryId: string;

  status: number ;

    LostDamageStatus: TCEnum[] = [
     new TCEnum( LostDamageStatus.lost, 'Lost'),
  new TCEnum( LostDamageStatus.damage, 'Damage'),
  new TCEnum( LostDamageStatus.dispose, 'Disposed'),

  ];
  store_name: string;
  store_id: string;


  constructor(private http: HttpClient) {}

  searchLostDamage(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<LostDamageSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'lostDamages',
      this.lostDamageSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if(this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if (this.start_date.value){
      url = TCUtilsString.appendUrlParameter(url, "start_date", (new Date(this.start_date.value).getTime()/1000).toString())
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if (this.end_date.value){
      url = TCUtilsString.appendUrlParameter(url, "end_date", (new Date(this.end_date.value).getTime()/1000).toString())
    }
    return this.http.get<LostDamageSummaryPartialList>(url);
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.lostDamageSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lostDamages/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  lostDamageDashboard(): Observable<LostDamageDashboard> {
    return this.http.get<LostDamageDashboard>(
      environment.tcApiBaseUri + 'lostDamages/dashboard'
    );
  }

  getLostDamage(id: string): Observable<LostDamageDetail> {
    return this.http.get<LostDamageDetail>(
      environment.tcApiBaseUri + 'lostDamages/' + id
    );
  }

  addLostDamage(item: LostDamageDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lostDamages/',
      item
    );
  }

  updateLostDamage(item: LostDamageDetail): Observable<LostDamageDetail> {
    return this.http.patch<LostDamageDetail>(
      environment.tcApiBaseUri + 'lostDamages/' + item.id,
      item
    );
  }

  deleteLostDamage(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'lostDamages/' + id);
  }

  lostDamagesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'lostDamages/do',
      new TCDoParam(method, payload)
    );
  }

  lostDamageDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'lostDamages/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lostDamages/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'lostDamages/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
