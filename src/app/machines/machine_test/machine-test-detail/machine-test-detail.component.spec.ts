import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTestDetailComponent } from './machine-test-detail.component';

describe('MachineTestDetailComponent', () => {
  let component: MachineTestDetailComponent;
  let fixture: ComponentFixture<MachineTestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineTestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
