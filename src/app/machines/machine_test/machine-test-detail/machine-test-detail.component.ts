import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";
import {AppTranslation} from "../../../app.translation";


import {MachineTestDetail} from "../machine-test.model";
import {MachineTestPersist} from "../machine-test.persist";
import {MachineTestNavigator} from "../machine-test.navigator";

export enum MachineTestTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-machine_test-detail',
  templateUrl: './machine-test-detail.component.html',
  styleUrls: ['./machine-test-detail.component.css']
})
export class MachineTestDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  machineTestIsLoading:boolean = false;
  
  MachineTestTabs: typeof MachineTestTabs = MachineTestTabs;
  activeTab: MachineTestTabs = MachineTestTabs.overview;
  //basics
  machineTestDetail: MachineTestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public machineTestNavigator: MachineTestNavigator,
              public  machineTestPersist: MachineTestPersist) {
    this.tcAuthorization.requireRead("machine_test");
    this.machineTestDetail = new MachineTestDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("machine_test");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.machineTestIsLoading = true;
    this.machineTestPersist.getMachineTest(id).subscribe(machineTestDetail => {
          this.machineTestDetail = machineTestDetail;
          this.machineTestIsLoading = false;
        }, error => {
          console.error(error);
          this.machineTestIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.machineTestDetail.id);
  }
  editMachineTest(): void {
    let modalRef = this.machineTestNavigator.editMachineTest(this.machineTestDetail.id);
    modalRef.afterClosed().subscribe(machineTestDetail => {
      TCUtilsAngular.assign(this.machineTestDetail, machineTestDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.machineTestPersist.print(this.machineTestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print machine_test", true);
      });
    }

  back():void{
      this.location.back();
    }

}