import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTestEditComponent } from './machine-test-edit.component';

describe('MachineTestEditComponent', () => {
  let component: MachineTestEditComponent;
  let fixture: ComponentFixture<MachineTestEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineTestEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
