import {Component, OnInit, Inject, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification}  from "../../../tc/notification";
import {TCUtilsString}   from "../../../tc/utils-string";
import {AppTranslation} from "../../../app.translation";

import {TCIdMode, TCModalModes} from "../../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MachineTestDetail, MachineTestSummary } from '../machine-test.model';
import { MachineTestPersist } from '../machine-test.persist';
import { MachineTestListComponent } from '../machine-test-list/machine-test-list.component';
import { MachineTestNavigator } from '../machine-test.navigator';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';

@Component({
  selector: 'app-machine_test-edit',
  templateUrl: './machine-test-edit.component.html',
  styleUrls: ['./machine-test-edit.component.css']
})export class MachineTestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  lab_name: string
  machineTestDetail: MachineTestDetail;
  
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MachineTestEditComponent>,
              public  persist: MachineTestPersist,
              public machintestNavigator: MachineTestNavigator,
              public tcUtilsDate: TCUtilsDate,
              public labTestNavigator: Lab_TestNavigator,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

    searchLabs() {
    let dialogRef = this.labTestNavigator.pickLab_Tests(true);
    dialogRef.afterClosed().subscribe((result: Lab_TestSummary) => {
      if (result) {
        this.lab_name = result[0].name
        this.machineTestDetail.lab_test_id = result[0].id
      }
    });
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("machine_test");
      this.title = this.appTranslation.getText("general","new") +  " " + "machine_test";
      this.machineTestDetail = new MachineTestDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("machine_test");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "machine_test";
      this.isLoadingResults = true;
      this.persist.getMachineTest(this.idMode.id).subscribe(machineTestDetail => {
        this.machineTestDetail = machineTestDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.machineTestDetail.machine_id = this.persist.machineId;
    
    this.persist.addMachine_test(this.machineTestDetail).subscribe(value => {
      this.tcNotification.success("machineTest added");
      this.machineTestDetail.id = value.id;
      this.dialogRef.close(this.machineTestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   
    console.log(this.machineTestDetail)
    this.persist.updateMachineTest(this.machineTestDetail).subscribe(value => {
      this.tcNotification.success("machine_test updated");
      this.dialogRef.close(this.machineTestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.machineTestDetail == null){
            return false;
          }

          return true;

 }
 }