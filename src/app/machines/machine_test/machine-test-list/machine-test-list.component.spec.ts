import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTestListComponent } from './machine-test-list.component';

describe('MachineTestListComponent', () => {
  let component: MachineTestListComponent;
  let fixture: ComponentFixture<MachineTestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineTestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
