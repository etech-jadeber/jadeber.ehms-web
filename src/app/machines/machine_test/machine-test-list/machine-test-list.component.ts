import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator'; 
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";
import { MachineTestSummary, MachineTestSummaryPartialList } from '../machine-test.model';
import {  MachineTestPersist } from '../machine-test.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MachineTestNavigator } from '../machine-test.navigator';
import { MachinesDetail } from '../../machines.model';
import { MachinesPersist } from '../../machines.persist';
import { Lab_TestDetail } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
@Component({
  selector: 'app-machine_test-list',
  templateUrl: './machine-test-list.component.html',
  styleUrls: ['./machine-test-list.component.css']
})export class MachineTestListComponent implements OnInit {
  machineTestsData: MachineTestSummary[] = [];
  machineTestsTotalCount: number = 0;
  machineTestSelectAll:boolean = false;
  machineTestSelection: MachineTestSummary[] = [];
  @Input() machineId : any;
  machine: {[id: string] : MachinesDetail} = {}
  lab: {[id: string] : Lab_TestDetail} = {}

 machineTestsDisplayedColumns: string[] = ["select","action", "Machine", "Lab test", "Lab code","status" ];
  machineTestSearchTextBox: FormControl = new FormControl();
  machineTestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) machineTestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) machineTestsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public machineTestPersist: MachineTestPersist,
                public machineTestNavigator: MachineTestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public machinePersist: MachinesPersist,
                public labTestPersist: Lab_TestPersist,

    ) {

        this.tcAuthorization.requireRead("machine_test");
       this.machineTestSearchTextBox.setValue(machineTestPersist.machineTestSearchText);
      //delay subsequent keyup events
      this.machineTestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.machineTestPersist.machineTestSearchText = value;
        this.searchMachine_tests();
      });
    } ngOnInit() {
   
      this.machineTestsSort.sortChange.subscribe(() => {
        this.machineTestsPaginator.pageIndex = 0;
        this.searchMachine_tests(true);
      });

      this.machineTestsPaginator.page.subscribe(() => {
        this.searchMachine_tests(true);
      });
      //start by loading items
      this.searchMachine_tests();
    }

  searchMachine_tests(isPagination:boolean = false): void {


    let paginator = this.machineTestsPaginator;
    let sorter = this.machineTestsSort;

    this.machineTestIsLoading = true;
    this.machineTestSelection = [];
    this.machineTestPersist.machineId = this.machineId
    this.machineTestPersist.searchMachineTest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MachineTestSummaryPartialList) => {
      this.machineTestsData = partialList.data;
      this.machineTestsData.forEach(data => {
          if (!this.machine[data.machine_id]) {
            this.machine[data.machine_id] = new MachinesDetail()
            this.machinePersist.getMachines(data.machine_id).subscribe(
              machine => {
                this.machine[data.machine_id] = machine;
              }
            )
          }

          if (!this.lab[data.lab_test_id]) {
            this.lab[data.lab_test_id] = new Lab_TestDetail()
            this.labTestPersist.getLab_Test(data.lab_test_id).subscribe(
              lab => {
                this.lab[data.lab_test_id] = lab;
              }
            )
          }

        })

        
      if (partialList.total != -1) {
        this.machineTestsTotalCount = partialList.total;
      }
      this.machineTestIsLoading = false;
    }, error => {
      this.machineTestIsLoading = false;
    });

  } downloadMachineTests(): void {
    if(this.machineTestSelectAll){
         this.machineTestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download machine_test", true);
      });
    }
    else{
        this.machineTestPersist.download(this.tcUtilsArray.idsList(this.machineTestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download machine_test",true);
            });
        }
  }
  
addMachine_test(): void {
    let dialogRef = this.machineTestNavigator.addMachineTest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMachine_tests();
      }
    });
  }

  editMachineTest(item: MachineTestSummary) {
    let dialogRef = this.machineTestNavigator.editMachineTest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMachineTest(item: MachineTestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("machine_test");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.machineTestPersist.deleteMachineTest(item.id).subscribe(response => {
          this.tcNotification.success("machine_test deleted");
          this.searchMachine_tests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }


    getMachine(machine_id: string): string {
  if (!this.machine[machine_id]){
    return ''
  }
  return this.machine[machine_id].name
}

    getLab(lab_id: string): string {
  if (!this.lab[lab_id]){
    return ''
  }
  return this.lab[lab_id].name
}
}