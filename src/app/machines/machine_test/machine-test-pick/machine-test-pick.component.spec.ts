import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineTestPickComponent } from './machine-test-pick.component';

describe('MachineTestPickComponent', () => {
  let component: MachineTestPickComponent;
  let fixture: ComponentFixture<MachineTestPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachineTestPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineTestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
