import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";
import { MachineTestSummary, MachineTestSummaryPartialList } from '../machine-test.model';
import { MachineTestPersist } from '../machine-test.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-machine_test-pick',
  templateUrl: './machine-test-pick.component.html',
  styleUrls: ['./machine-test-pick.component.css']
})export class MachineTestPickComponent implements OnInit {
  machineTestsData: MachineTestSummary[] = [];
  machineTestsTotalCount: number = 0;
  machineTestSelectAll:boolean = false;
  machineTestSelection: MachineTestSummary[] = [];

 machineTestsDisplayedColumns: string[] = ["select","action", "Machine", "Lab test", "Lab code","status" ];
  machineTestSearchTextBox: FormControl = new FormControl();
  machineTestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) machineTestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) machineTestsSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public machineTestPersist: MachineTestPersist,
                public dialogRef: MatDialogRef<MachineTestPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("machine_test");
       this.machineTestSearchTextBox.setValue(machineTestPersist.machineTestSearchText);
      //delay subsequent keyup events
      this.machineTestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.machineTestPersist.machineTestSearchText = value;
        this.searchMachine_tests();
      });
    } ngOnInit() {
   
      this.machineTestsSort.sortChange.subscribe(() => {
        this.machineTestsPaginator.pageIndex = 0;
        this.searchMachine_tests(true);
      });

      this.machineTestsPaginator.page.subscribe(() => {
        this.searchMachine_tests(true);
      });
      //start by loading items
      this.searchMachine_tests();
    }

  searchMachine_tests(isPagination:boolean = false): void {


    let paginator = this.machineTestsPaginator;
    let sorter = this.machineTestsSort;

    this.machineTestIsLoading = true;
    this.machineTestSelection = [];

    this.machineTestPersist.searchMachineTest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MachineTestSummaryPartialList) => {
      this.machineTestsData = partialList.data;
      if (partialList.total != -1) {
        this.machineTestsTotalCount = partialList.total;
      }
      this.machineTestIsLoading = false;
    }, error => {
      this.machineTestIsLoading = false;
    });

  }
  markOneItem(item: MachineTestSummary) {
    if(!this.tcUtilsArray.containsId(this.machineTestSelection,item.id)){
          this.machineTestSelection = [];
          this.machineTestSelection.push(item);
        }
        else{
          this.machineTestSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.machineTestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }