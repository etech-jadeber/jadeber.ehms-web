import {TCId} from "../../tc/models";

export class MachineTestSummary extends TCId {
machine_id?:string;
lab_test_id:string;
lab_code?:string;
added_date?:number;
added_by?:string;
updated_date?:number;
updated_by?:string;
status?:number;
 
}
export class MachineTestSummaryPartialList {
  data: MachineTestSummary[];
  total: number;
}
export class MachineTestDetail extends MachineTestSummary {
}

export class MachineTestDashboard {
  total: number;
}