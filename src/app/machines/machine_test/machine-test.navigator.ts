import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../../tc/models";

import {MachineTestEditComponent} from "./machine-test-edit/machine-test-edit.component";
import {MachineTestPickComponent} from "./machine-test-pick/machine-test-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MachineTestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  machine_testsUrl(): string {
    return "/machine_tests";
  }

  machine_testUrl(id: string): string {
    return "/machine_tests/" + id;
  }

  viewMachineTests(): void {
    this.router.navigateByUrl(this.machine_testsUrl());
  }

  viewMachineTest(id: string): void {
    this.router.navigateByUrl(this.machine_testUrl(id));
  }

  editMachineTest(id: string): MatDialogRef<MachineTestEditComponent> {
    const dialogRef = this.dialog.open(MachineTestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMachineTest(): MatDialogRef<MachineTestEditComponent> {
    const dialogRef = this.dialog.open(MachineTestEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMachineTests(selectOne: boolean=false): MatDialogRef<MachineTestPickComponent> {
      const dialogRef = this.dialog.open(MachineTestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}