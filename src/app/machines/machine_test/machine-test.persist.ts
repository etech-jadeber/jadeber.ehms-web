import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../../tc/utils-http";
import {TCId, TcDictionary} from "../../tc/models";
import {MachineTestDashboard, MachineTestDetail, MachineTestSummaryPartialList} from "./machine-test.model";


@Injectable({
  providedIn: 'root'
})
export class MachineTestPersist {
 machineTestSearchText: string = "";
 machineId: string;
 
 constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "machine_test/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.machineTestSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchMachineTest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MachineTestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("machine_test", this.machineTestSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<MachineTestSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "machine_test/do", new TCDoParam("download_all", this.filters()));
  }


  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'machine_test/do',
      new TCDoParam('download', ids)
    );
  }

     updateMachineTest(item: MachineTestDetail): Observable<MachineTestDetail> {
    return this.http.patch<MachineTestDetail>(
      environment.tcApiBaseUri + 'machine_test/' + item.id,
      item
    );
  }


  
  addMachine_test(item: MachineTestDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'machine_test/',
      item
    );
  }


    deleteMachineTest(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'machine_test/' + id);
  }

  machineTestDashboard(): Observable<MachineTestDashboard> {
    return this.http.get<MachineTestDashboard>(environment.tcApiBaseUri + "machine_test/dashboard");
  }

  getMachineTest(id: string): Observable<MachineTestDetail> {
    return this.http.get<MachineTestDetail>(environment.tcApiBaseUri + "machine_test/" + id);
  }
 }