import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {MachinesDetail} from "../machines.model";
import {MachinesPersist} from "../machines.persist";
import {MachinesNavigator} from "../machines.navigator";

export enum MachinesTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-machines-detail',
  templateUrl: './machines-detail.component.html',
  styleUrls: ['./machines-detail.component.css']
})
export class MachinesDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  machinesIsLoading:boolean = false;
  
  MachinesTabs: typeof MachinesTabs = MachinesTabs;
  activeTab: MachinesTabs = MachinesTabs.overview;
  //basics
  machinesDetail: MachinesDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public machinesNavigator: MachinesNavigator,
              public  machinesPersist: MachinesPersist) {
    this.tcAuthorization.requireRead("machines");
    this.machinesDetail = new MachinesDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("machines");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.machinesIsLoading = true;
    this.machinesPersist.getMachines(id).subscribe(machinesDetail => {
          this.machinesDetail = machinesDetail;
          this.machinesIsLoading = false;
        }, error => {
          console.error(error);
          this.machinesIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.machinesDetail.id);
  }
  editMachines(): void {
    let modalRef = this.machinesNavigator.editMachines(this.machinesDetail.id);
    modalRef.afterClosed().subscribe(machinesDetail => {
      TCUtilsAngular.assign(this.machinesDetail, machinesDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.machinesPersist.print(this.machinesDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print machines", true);
      });
    }

  back():void{
      this.location.back();
    }

}