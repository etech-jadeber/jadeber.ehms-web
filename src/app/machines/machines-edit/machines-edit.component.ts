import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";
import { lab_order_type } from "../../app.enums"

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MachinesDetail } from '../machines.model';import { MachinesPersist } from '../machines.persist';@Component({
  selector: 'app-machines-edit',
  templateUrl: './machines-edit.component.html',
  styleUrls: ['./machines-edit.component.css']
})export class MachinesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  machinesDetail: MachinesDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MachinesEditComponent>,
              public  persist: MachinesPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }


  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("machines");
      this.title = this.appTranslation.getText("general","new") +  " " + "machines";
      this.machinesDetail = new MachinesDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("machines");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "machines";
      this.isLoadingResults = true;
      this.persist.getMachines(this.idMode.id).subscribe(machinesDetail => {
        this.machinesDetail = machinesDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    console.log(this.machinesDetail)
    this.persist.addMachines(this.machinesDetail).subscribe(value => {
      this.tcNotification.success("machines added");
      this.machinesDetail.id = value.id;
      this.dialogRef.close(this.machinesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateMachines(this.machinesDetail).subscribe(value => {
      this.tcNotification.success("machines updated");
      this.dialogRef.close(this.machinesDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.machinesDetail == null){
            return false;
          }

        return true;

 }
 }