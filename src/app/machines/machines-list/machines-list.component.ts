import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MachinesSummary, MachinesSummaryPartialList } from '../machines.model';
import { MachinesPersist } from '../machines.persist';
import { MachinesNavigator } from '../machines.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Observable } from 'rxjs';
import { TCId } from 'src/app/tc/models';
import { TCDoParam } from 'src/app/tc/utils-http';
import { environment } from 'src/environments/environment';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
@Component({
  selector: 'app-machines-list',
  templateUrl: './machines-list.component.html',
  styleUrls: ['./machines-list.component.css']
})

export class MachinesListComponent implements OnInit {
  machinessData: MachinesSummary[] = [];
  machinessTotalCount: number = 0;
  machinesSelectAll:boolean = false;
  machinesSelection: MachinesSummary[] = [];
  user: {[id: string] : UserDetail} = {}

 machinessDisplayedColumns: string[] = ["select","action", "name", "machine_code", "order_type" ,"status" ];
  machinesSearchTextBox: FormControl = new FormControl();
  machinesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) machinessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) machinessSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public machinesPersist: MachinesPersist,
                public machinesNavigator: MachinesNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public userPersist: UserPersist,

    ) {

        this.tcAuthorization.requireRead("machines");
       this.machinesSearchTextBox.setValue(machinesPersist.machinesSearchText);
      //delay subsequent keyup events
      this.machinesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.machinesPersist.machinesSearchText = value;
        this.searchMachiness();
      });
    } ngOnInit() {
   
      this.machinessSort.sortChange.subscribe(() => {
        this.machinessPaginator.pageIndex = 0;
        this.searchMachiness(true);
      });

      this.machinessPaginator.page.subscribe(() => {
        this.searchMachiness(true);
      });
      //start by loading items
      this.searchMachiness();
    }

  searchMachiness(isPagination:boolean = false): void {


    let paginator = this.machinessPaginator;
    let sorter = this.machinessSort;

    this.machinesIsLoading = true;
    this.machinesSelection = [];

    this.machinesPersist.searchMachines(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MachinesSummaryPartialList) => {
      this.machinessData = partialList.data;
       this.machinessData.forEach(data => {
          if (!this.user[data.added_by]) {
            this.user[data.added_by] = new UserDetail()
            this.userPersist.getUser(data.added_by).subscribe(
              user => {
                this.user[data.added_by] = user;
              }
            )
          }
        })
      if (partialList.total != -1) {
        this.machinessTotalCount = partialList.total;
      }
      this.machinesIsLoading = false;
    }, error => {
      this.machinesIsLoading = false;
    });

  } downloadMachiness(): void {
    if(this.machinesSelectAll){
         this.machinesPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download machines", true);
      });
    }
    else{
        this.machinesPersist.download(this.tcUtilsArray.idsList(this.machinesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download machines",true);
            });
        }
  }
  
addMachines(): void {
    let dialogRef = this.machinesNavigator.addMachines();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMachiness();
      }
    });
  }

  editMachines(item: MachinesSummary) {
    let dialogRef = this.machinesNavigator.editMachines(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }


  deleteMachines(item: MachinesSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("machines");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.machinesPersist.deleteMachines(item.id).subscribe(response => {
          this.tcNotification.success("machines deleted");
          this.searchMachiness();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getUser(user_id: string): string {
  if (!this.user[user_id]){
    return ''
  }
  return this.user[user_id].name
}
}