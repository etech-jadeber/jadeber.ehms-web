import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesPickComponent } from './machines-pick.component';

describe('MachinesPickComponent', () => {
  let component: MachinesPickComponent;
  let fixture: ComponentFixture<MachinesPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MachinesPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
