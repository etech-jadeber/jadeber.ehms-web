import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import { MachinesSummary, MachinesSummaryPartialList } from '../machines.model';
import { MachinesPersist } from '../machines.persist';

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-machines-pick',
  templateUrl: './machines-pick.component.html',
  styleUrls: ['./machines-pick.component.css']
})


export class MachinesPickComponent implements OnInit {
  machinessData: MachinesSummary[] = [];
  machinessTotalCount: number = 0;
  machinesSelectAll:boolean = false;
  machinesSelection: MachinesSummary[] = [];

 machinessDisplayedColumns: string[] = ["select","action", "name", "machine_code", "order_type", "registeration_date", "added_by" ,"status" ];
  machinesSearchTextBox: FormControl = new FormControl();
  machinesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) machinessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) machinessSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public machinesPersist: MachinesPersist,
                public dialogRef: MatDialogRef<MachinesPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean,
 
    ) {

        this.tcAuthorization.requireRead("machines");
       this.machinesSearchTextBox.setValue(machinesPersist.machinesSearchText);
      //delay subsequent keyup events
      this.machinesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.machinesPersist.machinesSearchText = value;
        this.searchMachiness();
      });
    } ngOnInit() {
   
      this.machinessSort.sortChange.subscribe(() => {
        this.machinessPaginator.pageIndex = 0;
        this.searchMachiness(true);
      });

      this.machinessPaginator.page.subscribe(() => {
        this.searchMachiness(true);
      });
      //start by loading items
      this.searchMachiness();
    }

  searchMachiness(isPagination:boolean = false): void {


    let paginator = this.machinessPaginator;
    let sorter = this.machinessSort;

    this.machinesIsLoading = true;
    this.machinesSelection = [];

    this.machinesPersist.searchMachines(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MachinesSummaryPartialList) => {
      this.machinessData = partialList.data;
      if (partialList.total != -1) {
        this.machinessTotalCount = partialList.total;
      }
      this.machinesIsLoading = false;
    }, error => {
      this.machinesIsLoading = false;
    });

  }
  markOneItem(item: MachinesSummary) {
    if(!this.tcUtilsArray.containsId(this.machinesSelection,item.id)){
          this.machinesSelection = [];
          this.machinesSelection.push(item);
        }
        else{
          this.machinesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.machinesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }