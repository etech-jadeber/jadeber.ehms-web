import {TCId} from "../tc/models";



export class MachinesSummary extends TCId {
    name:string;
    machine_code:string;
    order_type:number;
    registeration_date:number;
    added_by:string;
    updated_date?:number;
    updated_by?:string;
    status?:number;
    
}
export class MachinesSummaryPartialList {
  data: MachinesSummary[];
  total: number;
}

export class MachinesDetail extends MachinesSummary {
}

export class MachinesDashboard {
  total: number;
}