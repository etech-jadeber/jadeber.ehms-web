import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {MachinesEditComponent} from "./machines-edit/machines-edit.component";
import {MachinesPickComponent} from "./machines-pick/machines-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MachinesNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  machinessUrl(): string {
    return "/machiness";
  }

  machinesUrl(id: string): string {
    return "/machines/" + id;
  }

  viewMachiness(): void {
    this.router.navigateByUrl(this.machinessUrl());
  }

  viewMachines(id: string): void {
    this.router.navigateByUrl(this.machinesUrl(id));
  }

  editMachines(id: string): MatDialogRef<MachinesEditComponent> {
    const dialogRef = this.dialog.open(MachinesEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMachines(): MatDialogRef<MachinesEditComponent> {
    const dialogRef = this.dialog.open(MachinesEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMachiness(selectOne: boolean=false): MatDialogRef<MachinesPickComponent> {
      const dialogRef = this.dialog.open(MachinesPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}