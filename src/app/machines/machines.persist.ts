import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCEnumTranslation, TCId, TcDictionary} from "../tc/models";
import {MachinesDashboard, MachinesDetail, MachinesSummaryPartialList} from "./machines.model";
import { lab_order_type } from '../app.enums';
import { ActivenessStatus } from 'src/app/app.enums';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})

export class MachinesPersist {
 machinesSearchText: string = "";
 
 constructor(private http: HttpClient, private appTranslation: AppTranslation,) {

}



print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "machines/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.machinesSearchText;
    //add custom filters
    return fltrs;
  }

   download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'machines/do',
      new TCDoParam('download', ids)
    );
  }

  deleteMachines(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'machines/' + id);
  }


  
  addMachines(item: MachinesDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'machines/',
      item
    );
  }


    updateMachines(item: MachinesDetail): Observable<MachinesDetail> {
    return this.http.patch<MachinesDetail>(
      environment.tcApiBaseUri + 'machines/' + item.id,
      item
    );
  }
 

  searchMachines(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MachinesSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("machines", this.machinesSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<MachinesSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "machines/do", new TCDoParam("download_all", this.filters()));
  }

  machinesDashboard(): Observable<MachinesDashboard> {
    return this.http.get<MachinesDashboard>(environment.tcApiBaseUri + "machines/dashboard");
  }

  getMachines(id: string): Observable<MachinesDetail> {
    return this.http.get<MachinesDetail>(environment.tcApiBaseUri + "machines/" + id);
  }

  lab_order_type: TCEnumTranslation[] = [
  new TCEnumTranslation(lab_order_type.imaging, this.appTranslation.getKey('patient', 'lab_order_imaging')),
  new TCEnumTranslation(lab_order_type.lab, this.appTranslation.getKey('procedure',"lab")),
  new TCEnumTranslation(lab_order_type.pathology, this.appTranslation.getKey('laboratory',"path_report")),
];

status : TCEnumTranslation[] = [
  new TCEnumTranslation(ActivenessStatus.active, this.appTranslation.getKey('bed', 'active')),
  new TCEnumTranslation(ActivenessStatus.freeze, this.appTranslation.getKey('inventory',"freeze")),
];

 }