import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupPriceDetailComponent } from './markup-price-detail.component';

describe('MarkupPriceDetailComponent', () => {
  let component: MarkupPriceDetailComponent;
  let fixture: ComponentFixture<MarkupPriceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkupPriceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupPriceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
