import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { MarkupPriceDetail } from '../markup_price.model';
import { MarkupPricePersist } from '../markup_price.persist';
import { MarkupPriceNavigator } from '../markup_price.navigator';

export enum MarkupPriceTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-markup_price-detail',
  templateUrl: './markup-price-detail.component.html',
  styleUrls: ['./markup-price-detail.component.scss'],
})
export class MarkupPriceDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  markupPriceIsLoading: boolean = false;

  MarkupPriceTabs: typeof MarkupPriceTabs = MarkupPriceTabs;
  activeTab: MarkupPriceTabs = MarkupPriceTabs.overview;
  //basics
  markupPriceDetail: MarkupPriceDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public markupPriceNavigator: MarkupPriceNavigator,
    public markupPricePersist: MarkupPricePersist
  ) {
    this.tcAuthorization.requireRead('markup_prices');
    this.markupPriceDetail = new MarkupPriceDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('markup_prices');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.markupPriceIsLoading = true;
    this.markupPricePersist.getMarkupPrice(id).subscribe(
      (markupPriceDetail) => {
        this.markupPriceDetail = markupPriceDetail;
        this.markupPriceIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.markupPriceIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.markupPriceDetail.id);
  }
  editMarkupPrice(): void {
    let modalRef = this.markupPriceNavigator.editMarkupPrice(
      this.markupPriceDetail.id
    );
    modalRef.afterClosed().subscribe(
      (markupPriceDetail) => {
        TCUtilsAngular.assign(this.markupPriceDetail, markupPriceDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.markupPricePersist
      .print(this.markupPriceDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print markup_price', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
