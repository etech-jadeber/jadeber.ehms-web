import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupPriceEditComponent } from './markup-price-edit.component';

describe('MarkupPriceEditComponent', () => {
  let component: MarkupPriceEditComponent;
  let fixture: ComponentFixture<MarkupPriceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkupPriceEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupPriceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
