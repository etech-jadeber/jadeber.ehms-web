import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MarkupPriceDetail } from '../markup_price.model';
import { MarkupPricePersist } from '../markup_price.persist';
import { StorePersist } from 'src/app/stores/store.persist';
@Component({
  selector: 'app-markup_price-edit',
  templateUrl: './markup-price-edit.component.html',
  styleUrls: ['./markup-price-edit.component.scss'],
})
export class MarkupPriceEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  markupPriceDetail: MarkupPriceDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public storePersist: StorePersist,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<MarkupPriceEditComponent>,
    public persist: MarkupPricePersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('markup_prices');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'markup_price';
      this.markupPriceDetail = new MarkupPriceDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('markup_prices');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'markup_price';
      this.isLoadingResults = true;
      this.persist.getMarkupPrice(this.idMode.id).subscribe(
        (markupPriceDetail) => {
          this.markupPriceDetail = markupPriceDetail;
          if(this.markupPriceDetail.to_max_of_unit_price == 3.40282e+38){
            this.markupPriceDetail.default = true;
          }
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addMarkupPrice(this.markupPriceDetail).subscribe(
      (value) => {
        this.tcNotification.success('markupPrice added');
        this.markupPriceDetail.id = value.id;
        this.dialogRef.close(this.markupPriceDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateMarkupPrice(this.markupPriceDetail).subscribe(
      (value) => {
        this.tcNotification.success('markup_price updated');
        this.dialogRef.close(this.markupPriceDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onDefaultChange(event:any):void{
    if(!this.markupPriceDetail.default){
      this.markupPriceDetail.to_max_of_unit_price = undefined;
      console.log()
    }
  }
  canSubmit(): boolean {
    if (this.markupPriceDetail == null) {
      return false;
    }

    if (this.markupPriceDetail.percent == null) {
      return false;
    }
    if (this.markupPriceDetail.service_type == null) {
      return false;
    }
    if(!this.markupPriceDetail.default &&  this.markupPriceDetail.to_max_of_unit_price == null){
      return false;
    }
    return true;
  }
}
