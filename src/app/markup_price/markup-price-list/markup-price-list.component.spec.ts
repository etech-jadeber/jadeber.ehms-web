import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupPriceListComponent } from './markup-price-list.component';

describe('MarkupPriceListComponent', () => {
  let component: MarkupPriceListComponent;
  let fixture: ComponentFixture<MarkupPriceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkupPriceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupPriceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
