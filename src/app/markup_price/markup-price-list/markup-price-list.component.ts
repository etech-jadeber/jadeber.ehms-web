import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  MarkupPriceSummary,
  MarkupPriceSummaryPartialList,
} from '../markup_price.model';
import { MarkupPricePersist } from '../markup_price.persist';
import { MarkupPriceNavigator } from '../markup_price.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { StorePersist } from 'src/app/stores/store.persist';
@Component({
  selector: 'app-markup_price-list',
  templateUrl: './markup-price-list.component.html',
  styleUrls: ['./markup-price-list.component.scss'],
})
export class MarkupPriceListComponent implements OnInit {
  markupPricesData: MarkupPriceSummary[] = [];
  markupPricesTotalCount: number = 0;
  markupPriceSelectAll: boolean = false;
  markupPriceSelection: MarkupPriceSummary[] = [];

  markupPricesDisplayedColumns: string[] = [
    'select',
    'action',
    'service_type',
    'to_max_of_unit_price',
    'percent',
  ];
  markupPriceSearchTextBox: FormControl = new FormControl();
  markupPriceIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  markupPricesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) markupPricesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public storePersist: StorePersist,
    public markupPricePersist: MarkupPricePersist,
    public markupPriceNavigator: MarkupPriceNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
  ) {
    this.tcAuthorization.requireRead('markup_prices');
    this.markupPriceSearchTextBox.setValue(
      markupPricePersist.markupPriceSearchText
    );
    //delay subsequent keyup events
    this.markupPriceSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.markupPricePersist.markupPriceSearchText = value;
        this.searchMarkupPrices();
      });
  }
  ngOnInit() {
    this.markupPricesSort.sortChange.subscribe(() => {
      this.markupPricesPaginator.pageIndex = 0;
      this.searchMarkupPrices(true);
    });

    this.markupPricesPaginator.page.subscribe(() => {
      this.searchMarkupPrices(true);
    });
    //start by loading items
    this.searchMarkupPrices();
  }

  searchMarkupPrices(isPagination: boolean = false): void {
    let paginator = this.markupPricesPaginator;
    let sorter = this.markupPricesSort;

    this.markupPriceIsLoading = true;
    this.markupPriceSelection = [];

    this.markupPricePersist
      .searchMarkupPrice(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: MarkupPriceSummaryPartialList) => {
          this.markupPricesData = partialList.data;
          if (partialList.total != -1) {
            this.markupPricesTotalCount = partialList.total;
          }
          this.markupPriceIsLoading = false;
        },
        (error) => {
          this.markupPriceIsLoading = false;
        }
      );
  }
  downloadMarkupPrices(): void {
    if (this.markupPriceSelectAll) {
      this.markupPricePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download markup_price',
          true
        );
      });
    } else {
      this.markupPricePersist
        .download(this.tcUtilsArray.idsList(this.markupPriceSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download markup_price',
            true
          );
        });
    }
  }
  addMarkupPrice(): void {
    let dialogRef = this.markupPriceNavigator.addMarkupPrice();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchMarkupPrices();
      }
    });
  }

  editMarkupPrice(item: MarkupPriceSummary) {
    let dialogRef = this.markupPriceNavigator.editMarkupPrice(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteMarkupPrice(item: MarkupPriceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('markup_price');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.markupPricePersist.deleteMarkupPrice(item.id).subscribe(
          (response) => {
            this.tcNotification.success('markup_price deleted');
            this.searchMarkupPrices();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
