import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkupPricePickComponent } from './markup-price-pick.component';

describe('MarkupPricePickComponent', () => {
  let component: MarkupPricePickComponent;
  let fixture: ComponentFixture<MarkupPricePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarkupPricePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkupPricePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
