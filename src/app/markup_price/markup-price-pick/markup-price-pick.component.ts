import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  MarkupPriceSummary,
  MarkupPriceSummaryPartialList,
} from '../markup_price.model';
import { MarkupPricePersist } from '../markup_price.persist';
import { MarkupPriceNavigator } from '../markup_price.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-markup_price-pick',
  templateUrl: './markup-price-pick.component.html',
  styleUrls: ['./markup-price-pick.component.scss'],
})
export class MarkupPricePickComponent implements OnInit {
  markupPricesData: MarkupPriceSummary[] = [];
  markupPricesTotalCount: number = 0;
  markupPriceSelectAll: boolean = false;
  markupPriceSelection: MarkupPriceSummary[] = [];

  markupPricesDisplayedColumns: string[] = [
    'select',
    ,
    'percent',
    'service_type',
  ];
  markupPriceSearchTextBox: FormControl = new FormControl();
  markupPriceIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  markupPricesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) markupPricesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public markupPricePersist: MarkupPricePersist,
    public markupPriceNavigator: MarkupPriceNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<MarkupPricePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('markup_prices');
    this.markupPriceSearchTextBox.setValue(
      markupPricePersist.markupPriceSearchText
    );
    //delay subsequent keyup events
    this.markupPriceSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.markupPricePersist.markupPriceSearchText = value;
        this.searchMarkup_prices();
      });
  }
  ngOnInit() {
    this.markupPricesSort.sortChange.subscribe(() => {
      this.markupPricesPaginator.pageIndex = 0;
      this.searchMarkup_prices(true);
    });

    this.markupPricesPaginator.page.subscribe(() => {
      this.searchMarkup_prices(true);
    });
    //start by loading items
    this.searchMarkup_prices();
  }

  searchMarkup_prices(isPagination: boolean = false): void {
    let paginator = this.markupPricesPaginator;
    let sorter = this.markupPricesSort;

    this.markupPriceIsLoading = true;
    this.markupPriceSelection = [];

    this.markupPricePersist
      .searchMarkupPrice(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: MarkupPriceSummaryPartialList) => {
          this.markupPricesData = partialList.data;
          if (partialList.total != -1) {
            this.markupPricesTotalCount = partialList.total;
          }
          this.markupPriceIsLoading = false;
        },
        (error) => {
          this.markupPriceIsLoading = false;
        }
      );
  }
  markOneItem(item: MarkupPriceSummary) {
    if (!this.tcUtilsArray.containsId(this.markupPriceSelection, item.id)) {
      this.markupPriceSelection = [];
      this.markupPriceSelection.push(item);
    } else {
      this.markupPriceSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.markupPriceSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
