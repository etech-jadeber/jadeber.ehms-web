import { TCId } from '../tc/models';
export class MarkupPriceSummary extends TCId {
  percent: number;
  service_type: number;
  to_max_of_unit_price: number;
  default:boolean;
}
export class MarkupPriceSummaryPartialList {
  data: MarkupPriceSummary[];
  total: number;
}
export class MarkupPriceDetail extends MarkupPriceSummary {}

export class MarkupPriceDashboard {
  total: number;
}
