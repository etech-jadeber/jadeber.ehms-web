import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { MarkupPriceEditComponent } from './markup-price-edit/markup-price-edit.component';
import { MarkupPricePickComponent } from './markup-price-pick/markup-price-pick.component';

@Injectable({
  providedIn: 'root',
})
export class MarkupPriceNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  markupPricesUrl(): string {
    return '/markup_prices';
  }

  markupPriceUrl(id: string): string {
    return '/markup_prices/' + id;
  }

  viewMarkupPrices(): void {
    this.router.navigateByUrl(this.markupPricesUrl());
  }

  viewMarkupPrice(id: string): void {
    this.router.navigateByUrl(this.markupPriceUrl(id));
  }

  editMarkupPrice(id: string): MatDialogRef<MarkupPriceEditComponent> {
    const dialogRef = this.dialog.open(MarkupPriceEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addMarkupPrice(): MatDialogRef<MarkupPriceEditComponent> {
    const dialogRef = this.dialog.open(MarkupPriceEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickMarkupPrices(
    selectOne: boolean = false
  ): MatDialogRef<MarkupPricePickComponent> {
    const dialogRef = this.dialog.open(MarkupPricePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
