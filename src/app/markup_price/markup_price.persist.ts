import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  MarkupPriceDashboard,
  MarkupPriceDetail,
  MarkupPriceSummaryPartialList,
} from './markup_price.model';
import { store_type } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class MarkupPricePersist {
  markupPriceSearchText: string = '';
  store_type: TCEnum[] = [
    new TCEnum(store_type.outpatient, 'outpatient'),
    new TCEnum(store_type.inpatient, 'inpatient'),
    new TCEnum(store_type.emergency, 'emergency'),
  ];

  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.markupPriceSearchText;
    //add custom filters
    return fltrs;
  }

  searchMarkupPrice(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<MarkupPriceSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'markup_price',
      this.markupPriceSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<MarkupPriceSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'markup_price/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  markupPriceDashboard(): Observable<MarkupPriceDashboard> {
    return this.http.get<MarkupPriceDashboard>(
      environment.tcApiBaseUri + 'markup_price/dashboard'
    );
  }

  getMarkupPrice(id: string): Observable<MarkupPriceDetail> {
    return this.http.get<MarkupPriceDetail>(
      environment.tcApiBaseUri + 'markup_price/' + id
    );
  }
  addMarkupPrice(item: MarkupPriceDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'markup_price/',
      item
    );
  }

  updateMarkupPrice(item: MarkupPriceDetail): Observable<MarkupPriceDetail> {
    return this.http.patch<MarkupPriceDetail>(
      environment.tcApiBaseUri + 'markup_price/' + item.id,
      item
    );
  }

  deleteMarkupPrice(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'markup_price/' + id);
  }
  markupPricesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'markup_price/do',
      new TCDoParam(method, payload)
    );
  }

  markupPriceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'markup_prices/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'markup_price/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'markup_price/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
