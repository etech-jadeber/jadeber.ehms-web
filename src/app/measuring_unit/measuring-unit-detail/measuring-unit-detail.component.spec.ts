import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringUnitDetailComponent } from './measuring-unit-detail.component';

describe('MeasuringUnitDetailComponent', () => {
  let component: MeasuringUnitDetailComponent;
  let fixture: ComponentFixture<MeasuringUnitDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasuringUnitDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringUnitDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
