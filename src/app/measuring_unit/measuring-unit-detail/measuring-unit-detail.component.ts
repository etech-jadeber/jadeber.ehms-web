import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {MeasuringUnitDetail} from "../measuringUnit.model";
import {MeasuringUnitPersist} from "../measuringUnit.persist";
import {MeasuringUnitNavigator} from "../measuringUnit.navigator";

export enum MeasuringUnitTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-measuring_unit-detail',
  templateUrl: './measuring-unit-detail.component.html',
  styleUrls: ['./measuring-unit-detail.component.scss']
})
export class MeasuringUnitDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  measuringUnitIsLoading:boolean = false;
  
  MeasuringUnitTabs: typeof MeasuringUnitTabs = MeasuringUnitTabs;
  activeTab: MeasuringUnitTabs = MeasuringUnitTabs.overview;
  //basics
  measuringUnitDetail: MeasuringUnitDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public measuringUnitNavigator: MeasuringUnitNavigator,
              public  measuringUnitPersist: MeasuringUnitPersist) {
    this.tcAuthorization.requireRead("measuring_units");
    this.measuringUnitDetail = new MeasuringUnitDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("measuring_units");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.measuringUnitIsLoading = true;
    this.measuringUnitPersist.getMeasuringUnit(id).subscribe(measuringUnitDetail => {
          this.measuringUnitDetail = measuringUnitDetail;
          this.measuringUnitIsLoading = false;
        }, error => {
          console.error(error);
          this.measuringUnitIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.measuringUnitDetail.id);
  }
  editMeasuringUnit(): void {
    let modalRef = this.measuringUnitNavigator.editMeasuringUnit(this.measuringUnitDetail.id);
    modalRef.afterClosed().subscribe(measuringUnitDetail => {
      TCUtilsAngular.assign(this.measuringUnitDetail, measuringUnitDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.measuringUnitPersist.print(this.measuringUnitDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print measuring_unit", true);
      });
    }

  back():void{
      this.location.back();
    }

}