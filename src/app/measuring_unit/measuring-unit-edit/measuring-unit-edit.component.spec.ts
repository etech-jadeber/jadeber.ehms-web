import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringUnitEditComponent } from './measuring-unit-edit.component';

describe('MeasuringUnitEditComponent', () => {
  let component: MeasuringUnitEditComponent;
  let fixture: ComponentFixture<MeasuringUnitEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasuringUnitEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringUnitEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
