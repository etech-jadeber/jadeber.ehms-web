import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MeasuringUnitDetail } from '../measuringUnit.model';
import { MeasuringUnitPersist } from '../measuringUnit.persist';

@Component({
  selector: 'app-measuring_unit-edit',
  templateUrl: './measuring-unit-edit.component.html',
  styleUrls: ['./measuring-unit-edit.component.scss']
})export class MeasuringUnitEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  measuringUnitDetail: MeasuringUnitDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MeasuringUnitEditComponent>,
              public  persist: MeasuringUnitPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("measuring_units");
      this.title = this.appTranslation.getText("general","new") +  " " + "measuring_unit";
      this.measuringUnitDetail = new MeasuringUnitDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("measuring_units");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "measuring_unit";
      this.isLoadingResults = true;
      this.persist.getMeasuringUnit(this.idMode.id).subscribe(measuringUnitDetail => {
        this.measuringUnitDetail = measuringUnitDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addMeasuringUnit(this.measuringUnitDetail).subscribe(value => {
      this.tcNotification.success("measuringUnit added");
      this.measuringUnitDetail.id = value.id;
      this.dialogRef.close(this.measuringUnitDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateMeasuringUnit(this.measuringUnitDetail).subscribe(value => {
      this.tcNotification.success("measuring_unit updated");
      this.dialogRef.close(this.measuringUnitDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.measuringUnitDetail == null){
            return false;
          }
          if (!this.measuringUnitDetail.unit){
            return false;
          }
      return true;

 }
 }