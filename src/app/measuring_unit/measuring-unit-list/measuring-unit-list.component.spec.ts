import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringUnitListComponent } from './measuring-unit-list.component';

describe('MeasuringUnitListComponent', () => {
  let component: MeasuringUnitListComponent;
  let fixture: ComponentFixture<MeasuringUnitListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasuringUnitListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringUnitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
