import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MeasuringUnitSummary, MeasuringUnitSummaryPartialList } from '../measuringUnit.model';
import { MeasuringUnitPersist } from '../measuringUnit.persist';
import { MeasuringUnitNavigator } from '../measuringUnit.navigator';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
@Component({
  selector: 'app-measuring_unit-list',
  templateUrl: './measuring-unit-list.component.html',
  styleUrls: ['./measuring-unit-list.component.scss']
})export class MeasuringUnitListComponent implements OnInit {
  measuringUnitsData: MeasuringUnitSummary[] = [];
  measuringUnitsTotalCount: number = 0;
  measuringUnitSelectAll:boolean = false;
  measuringUnitSelection: MeasuringUnitSummary[] = [];

 measuringUnitsDisplayedColumns: string[] = ["select","action","unit" ];
 measuringUnitsSearchTextBox: FormControl = new FormControl();
  measuringUnitIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) measuringUnitsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) measuringUnitsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public measuringUnitPersist: MeasuringUnitPersist,
                public measuringUnitNavigator: MeasuringUnitNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("measuring_units");
       this.measuringUnitsSearchTextBox.setValue(measuringUnitPersist.measuringUnitSearchText);
      //delay subsequent keyup events
      this.measuringUnitsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.measuringUnitPersist.measuringUnitSearchText = value;
        this.searchMeasuring_units();
      });
    } ngOnInit() {
   
      this.measuringUnitsSort.sortChange.subscribe(() => {
        this.measuringUnitsPaginator.pageIndex = 0;
        this.searchMeasuring_units(true);
      });

      this.measuringUnitsPaginator.page.subscribe(() => {
        this.searchMeasuring_units(true);
      });
      //start by loading items
      this.searchMeasuring_units();
    }

  searchMeasuring_units(isPagination:boolean = false): void {


    let paginator = this.measuringUnitsPaginator;
    let sorter = this.measuringUnitsSort;

    this.measuringUnitIsLoading = true;
    this.measuringUnitSelection = [];

    this.measuringUnitPersist.searchMeasuringUnit(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MeasuringUnitSummaryPartialList) => {
      this.measuringUnitsData = partialList.data;
      if (partialList.total != -1) {
        this.measuringUnitsTotalCount = partialList.total;
      }
      this.measuringUnitIsLoading = false;
    }, error => {
      this.measuringUnitIsLoading = false;
    });

  } downloadMeasuringUnits(): void {
    if(this.measuringUnitSelectAll){
         this.measuringUnitPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download measuring_unit", true);
      });
    }
    else{
        this.measuringUnitPersist.download(this.tcUtilsArray.idsList(this.measuringUnitSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download measuring_unit",true);
            });
        }
  }
addMeasuring_unit(): void {
    let dialogRef = this.measuringUnitNavigator.addMeasuringUnit();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMeasuring_units();
      }
    });
  }

  editMeasuringUnit(item: MeasuringUnitSummary) {
    let dialogRef = this.measuringUnitNavigator.editMeasuringUnit(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMeasuringUnit(item: MeasuringUnitSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("measuring_unit");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.measuringUnitPersist.deleteMeasuringUnit(item.id).subscribe(response => {
          this.tcNotification.success("measuring_unit deleted");
          this.searchMeasuring_units();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}