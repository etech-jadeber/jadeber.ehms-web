import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasuringUnitPickComponent } from './measuring-unit-pick.component';

describe('MeasuringUnitPickComponent', () => {
  let component: MeasuringUnitPickComponent;
  let fixture: ComponentFixture<MeasuringUnitPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeasuringUnitPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuringUnitPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
