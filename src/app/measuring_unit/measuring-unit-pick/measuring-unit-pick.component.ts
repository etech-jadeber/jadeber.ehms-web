import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MeasuringUnitSummary, MeasuringUnitSummaryPartialList } from '../measuringUnit.model';
import { MeasuringUnitPersist } from '../measuringUnit.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-measuring_unit-pick',
  templateUrl: './measuring-unit-pick.component.html',
  styleUrls: ['./measuring-unit-pick.component.scss']
})export class MeasuringUnitPickComponent implements OnInit {
  measuringUnitsData: MeasuringUnitSummary[] = [];
  measuringUnitsTotalCount: number = 0;
  measuringUnitSelectAll:boolean = false;
  measuringUnitsSelection: MeasuringUnitSummary[] = [];

 measuringUnitsDisplayedColumns: string[] = ["select","unit" ];
  measuringUnitSearchTextBox: FormControl = new FormControl();
  measuringUnitIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) measuringUnitsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) measuringUnitsSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public measuringUnitPersist: MeasuringUnitPersist,
                public dialogRef: MatDialogRef<MeasuringUnitPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("measuring_units");
       this.measuringUnitSearchTextBox.setValue(measuringUnitPersist.measuringUnitSearchText);
      //delay subsequent keyup events
      this.measuringUnitSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.measuringUnitPersist.measuringUnitSearchText = value;
        this.searchMeasuring_units();
      });
    } ngOnInit() {
   
      this.measuringUnitsSort.sortChange.subscribe(() => {
        this.measuringUnitsPaginator.pageIndex = 0;
        this.searchMeasuring_units(true);
      });

      this.measuringUnitsPaginator.page.subscribe(() => {
        this.searchMeasuring_units(true);
      });
      //start by loading items
      this.searchMeasuring_units();
    }

  searchMeasuring_units(isPagination:boolean = false): void {


    let paginator = this.measuringUnitsPaginator;
    let sorter = this.measuringUnitsSort;

    this.measuringUnitIsLoading = true;
    this.measuringUnitsSelection = [];

    this.measuringUnitPersist.searchMeasuringUnit(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MeasuringUnitSummaryPartialList) => {
      this.measuringUnitsData = partialList.data;
      if (partialList.total != -1) {
        this.measuringUnitsTotalCount = partialList.total;
      }
      this.measuringUnitIsLoading = false;
    }, error => {
      this.measuringUnitIsLoading = false;
    });

  }
  markOneItem(item: MeasuringUnitSummary) {
    if(!this.tcUtilsArray.containsId(this.measuringUnitsSelection,item.id)){
          this.measuringUnitsSelection = [];
          this.measuringUnitsSelection.push(item);
        }
        else{
          this.measuringUnitsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.measuringUnitsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }