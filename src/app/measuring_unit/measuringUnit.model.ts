import {TCId} from "../tc/models";

export class MeasuringUnitSummary extends TCId {
    unit:string;
     
    }
    export class MeasuringUnitSummaryPartialList {
      data: MeasuringUnitSummary[];
      total: number;
    }
    export class MeasuringUnitDetail extends MeasuringUnitSummary {
    }
    
    export class MeasuringUnitDashboard {
      total: number;
    }