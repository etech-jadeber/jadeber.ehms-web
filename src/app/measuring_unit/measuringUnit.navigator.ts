import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {MeasuringUnitEditComponent} from "./measuring-unit-edit/measuring-unit-edit.component";
import {MeasuringUnitPickComponent} from "./measuring-unit-pick/measuring-unit-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MeasuringUnitNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  measuring_unitsUrl(): string {
    return "/measuring_units";
  }

  measuring_unitUrl(id: string): string {
    return "/measuring_units/" + id;
  }

  viewMeasuringUnits(): void {
    this.router.navigateByUrl(this.measuring_unitsUrl());
  }

  viewMeasuringUnit(id: string): void {
    this.router.navigateByUrl(this.measuring_unitUrl(id));
  }

  editMeasuringUnit(id: string): MatDialogRef<MeasuringUnitEditComponent> {
    const dialogRef = this.dialog.open(MeasuringUnitEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMeasuringUnit(): MatDialogRef<MeasuringUnitEditComponent> {
    const dialogRef = this.dialog.open(MeasuringUnitEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMeasuringUnits(selectOne: boolean=false): MatDialogRef<MeasuringUnitPickComponent> {
      const dialogRef = this.dialog.open(MeasuringUnitPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}