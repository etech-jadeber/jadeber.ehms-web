import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {MeasuringUnitDashboard, MeasuringUnitDetail, MeasuringUnitSummaryPartialList} from "./measuringUnit.model";


@Injectable({
  providedIn: 'root'
})
export class MeasuringUnitPersist {

 measuringUnitSearchText: string = "";
 
 constructor(private http: HttpClient) {
  }

 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "measuring_unit/" + id + "/do", new TCDoParam("print", {}));
  }  
  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.measuringUnitSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchMeasuringUnit(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MeasuringUnitSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("measuring_unit", this.measuringUnitSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<MeasuringUnitSummaryPartialList>(url);

  } 

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "measuring_unit/do", new TCDoParam("download", ids));
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "measuring_unit/do", new TCDoParam("download_all", this.filters()));
  }

  measuringUnitDashboard(): Observable<MeasuringUnitDashboard> {
    return this.http.get<MeasuringUnitDashboard>(environment.tcApiBaseUri + "measuring_unit/dashboard");
  }

  getMeasuringUnit(id: string): Observable<MeasuringUnitDetail> {
    return this.http.get<MeasuringUnitDetail>(environment.tcApiBaseUri + "measuring_unit/" + id);
  }

  addMeasuringUnit(item: MeasuringUnitDetail): Observable<TCId> {
    return this.http.post<MeasuringUnitDetail>(environment.tcApiBaseUri + "measuring_unit/", item);
  }

  updateMeasuringUnit(item: MeasuringUnitDetail): Observable<MeasuringUnitDetail> {
    return this.http.patch<MeasuringUnitDetail>(environment.tcApiBaseUri + "measuring_unit/" + item.id, item);
  }

  deleteMeasuringUnit(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "measuring_unit/" + id);
  }

  measuringUnitDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "measuring_unit/do", new TCDoParam(method, payload));
  }

  measuringUnitsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "measuring_unit/" + id + "/do", new TCDoParam(method, payload));
  }
 }