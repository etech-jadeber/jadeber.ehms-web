import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCertificateListComponent } from './medical-certificate-list.component';

describe('MedicalCertificateListComponent', () => {
  let component: MedicalCertificateListComponent;
  let fixture: ComponentFixture<MedicalCertificateListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCertificateListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
