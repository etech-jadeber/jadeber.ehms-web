import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, interval } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { Medical_CertificateDetail, Medical_CertificateSummary } from '../form_encounters/form_encounter.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-medical-certificate-list',
  templateUrl: './medical-certificate-list.component.html',
  styleUrls: ['./medical-certificate-list.component.css']
})
export class MedicalCertificateListComponent implements OnInit {

  message: string = '';
  success_state = 3;

  medical_certificatesData: Medical_CertificateSummary[] = [];
  medical_certificatesTotalCount: number = 0;
  medical_certificatesSelectAll: boolean = false;
  medical_certificatesSelected: Medical_CertificateSummary[] = [];
  medical_certificatesDisplayedColumns: string[] = ["select","action","encounter","age","sex","card_no","diagnosis","date_of_visit","sick_leave","recommendation","date_of_issued","doctor" ];
  
  medical_certificatesSearchTextBox: FormControl = new FormControl();
  medical_certificatesLoading: boolean = false;


  name:string
  doctor:string;

@Input() encounterId: string;
@Input() patientId: string;
@Output() onResult = new EventEmitter<number>();

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sorter: MatSort;
timeLabels: string[] = ['12 AM', '1 AM', '2 AM', '3 AM', '4 AM', '5 AM', '6 AM', '7 AM','8 AM','9 AM','10 AM','11 AM','12 PM','1 PM', '2 PM', '3 PM', '4 PM', '5 PM', '6 PM', '7 PM','8 PM','9 PM','10 PM','11 PM'];
 
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
  ) { 
    this.medical_certificatesSearchTextBox.setValue(
      form_encounterPersist.medical_certificateSearchText
    );
    this.medical_certificatesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.medical_certificateSearchText = value.trim();
        this.searchMedical_Certificates();
      });
  }

  ngOnInit(): void {
    this.form_encounterPersist.medical_certificatePatientId = this.patientId
    this.tcAuthorization.requireRead("medical_certificates");
    this.sorter.sortChange.subscribe(() => {
        this.paginator.pageIndex = 0;
        this.searchMedical_Certificates(true);
      });
    // medical_certificates paginator
    this.paginator.page.subscribe(() => {
        this.searchMedical_Certificates(true);
      });
      this.loadEncounter();
      this.searchMedical_Certificates(true);
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.form_encounterPersist.medical_certificateEncounterId = this.encounterId;
    } else {
      this.form_encounterPersist.medical_certificateEncounterId = null;
    }
    this.searchMedical_Certificates()
  }

  searchMedical_Certificates(isPagination: boolean = false): void {
    let paginator =
      this.paginator;
    let sorter = this.sorter;
    this.medical_certificatesSelected = [];
    this.medical_certificatesLoading = true;
    this.form_encounterPersist
      .searchMedical_Certificate(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.medical_certificatesData = response.data;
          if (response.total != -1) {
            this.medical_certificatesTotalCount = response.total;
          }
          this.medical_certificatesLoading = false;
        },
        (error) => {
          this.medical_certificatesLoading = false;
        }
      );
  }
  loadEncounter(){
    this.form_encounterPersist.getForm_Encounter(this.encounterId).subscribe(res=>{
      this.name = res.full_name;
      this.doctor=res.provider_name;
    
    })
  }
  addMedical_Certificate(): void {
    let dialogRef = this.form_encounterNavigator.addMedical_Certificate(
      this.encounterId
    );
    dialogRef.afterClosed().subscribe((newMedical_Certificate) => {
      this.searchMedical_Certificates();
    });
  }

  editMedical_Certificate(item: Medical_CertificateDetail): void {
    let dialogRef = this.form_encounterNavigator.editMedical_Certificate(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedMedical_Certificate) => {
      if (updatedMedical_Certificate) {
        this.searchMedical_Certificates();
      }
    });
  }

  deleteMedical_Certificate(item: Medical_CertificateDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Medical_Certificate');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteMedical_Certificate(item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('medical_certificate deleted');
              this.searchMedical_Certificates();
            },
            (error) => {}
          );
      }
    });
  }

  downloadMedical_Certificates(): void {
    this.tcNotification.info(
      'Download medical_certificates : ' +
        this.medical_certificatesSelected.length
    );
    this.form_encounterPersist
      .medical_certificatesDo(
        this.encounterId,
        'download_medical_certificate',
        { id: this.encounterId }
      )
      .subscribe(
        (downloadJob) => {
          this.followJob('', 'downloading medical certificate', true);
          let success_state = 3;
          this.jobPersist.getJob('').subscribe((job) => {
            if (job.job_state == success_state) {
            }
          });
        },
        (error) => {}
      );
  }

  printMedicalCertificates(item: Medical_CertificateSummary) {
    this.form_encounterPersist
      .prescriptionsDo(
        this.encounterId,
        item.id,
        'print_medical_certificate',
        item
      )
      .subscribe(
        (downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'printing medical certificate', true);
          let success_state = 3;
          this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
            if (job.job_state == success_state) {
            }
          });
        },
        (error) => {}
      );
  }

  followJob(
    id: string,
    descrption: string,
    is_download_job: boolean = true
  ): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }

    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe((n) => {
      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

          if (is_download_job) {
            this.jobPersist.downloadFileKey(id).subscribe((jobFile) => {
              this.tcNavigator.openUrl(
                this.filePersist.downloadLink(jobFile.download_key)
              );
            });
          }
        } else {
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);
          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }



  back(): void {
    this.location.back();
  } 

}
