import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalReportChartsComponent } from './medical-report-charts.component';

describe('MedicalReportChartsComponent', () => {
  let component: MedicalReportChartsComponent;
  let fixture: ComponentFixture<MedicalReportChartsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalReportChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalReportChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
