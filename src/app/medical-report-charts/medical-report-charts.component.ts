import {Component, OnInit} from '@angular/core';
import {AppTranslation} from "../app.translation";
import {TCAuthorization} from "../tc/authorization";
import {Location} from "@angular/common";
import {MedicalReportChartsPersist} from "./medical-report-charts.persist";

@Component({
  selector: 'app-medical-report-charts',
  templateUrl: './medical-report-charts.component.html',
  styleUrls: ['./medical-report-charts.component.css']
})
export class MedicalReportChartsComponent implements OnInit {
  result = [];
  showTopDiagnosis: boolean = false;
  showMonthlyEncounter: boolean = false;
  monthlyEncounter = [];

  constructor(public appTranslation: AppTranslation,
              public tcAuthorization: TCAuthorization,
              private medicalReportChartsPersist: MedicalReportChartsPersist,
              private location: Location,) {
  }

  ngOnInit() {
    this.medicalReportChartsPersist.medicalReportChartsDashboard().subscribe((value) => {
      this.handleTopDiagnosis(value["top_diagnosis"]);
      this.handleMonthlyEncounter(value["monthly_encounters"]);
    }, error => {
      console.error(error);
    })
  }

  private handleMonthlyEncounter(localResults) {
    this.monthlyEncounter.push({
      "name": "Monthly encounters",
      "series": localResults
    });
    this.showMonthlyEncounter = true;
  }

  private handleTopDiagnosis(localResults) {
    const formattedLocalResults = [];
    localResults.forEach(value1 => {
      formattedLocalResults.push({name: value1['diagnoses'], value: value1['value']});
    })
    this.result = formattedLocalResults;
    this.showTopDiagnosis = true;
  }

  back(): void {
    this.location.back();
  }
}
