import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";

@Injectable({
  providedIn: 'root'
})

export class MedicalReportChartsNavigator {
  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  medicalReportChartsUrl(): string {
    return "/medical_report_charts";
  }

  viewMedicalReportCharts(): void {
    this.router.navigateByUrl(this.medicalReportChartsUrl());
  }
}
