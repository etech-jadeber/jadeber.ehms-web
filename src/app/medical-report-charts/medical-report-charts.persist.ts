import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class MedicalReportChartsPersist {

  constructor(private http: HttpClient) {
  }

  medicalReportChartsDashboard() {
    return this.http.get(environment.tcApiBaseUri + "/medical_report_charts/dashboard");
  }

}
