import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalCertificateEmployeeDetailComponent } from './medical-certificate-employee-detail.component';

describe('MedicalCertificateEmployeeDetailComponent', () => {
  let component: MedicalCertificateEmployeeDetailComponent;
  let fixture: ComponentFixture<MedicalCertificateEmployeeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCertificateEmployeeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateEmployeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
