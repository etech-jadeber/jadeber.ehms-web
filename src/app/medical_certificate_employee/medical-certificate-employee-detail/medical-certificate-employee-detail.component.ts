import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { MedicalCertificateEmployeeDetail } from '../medical_certificate_employee.model';
import { MedicalCertificateEmployeePersist } from '../medical_certificate_employee.persist';
import { MedicalCertificateEmployeeNavigator } from '../medical_certificate_employee.navigator';

export enum MedicalCertificateEmployeeTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-medical_certificate_employee-detail',
  templateUrl: './medical-certificate-employee-detail.component.html',
  styleUrls: ['./medical-certificate-employee-detail.component.css'],
})
export class MedicalCertificateEmployeeDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  medicalCertificateEmployeeIsLoading: boolean = false;

  MedicalCertificateEmployeeTabs: typeof MedicalCertificateEmployeeTabs =
    MedicalCertificateEmployeeTabs;
  activeTab: MedicalCertificateEmployeeTabs =
    MedicalCertificateEmployeeTabs.overview;
  //basics
  medicalCertificateEmployeeDetail: MedicalCertificateEmployeeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public medicalCertificateEmployeeNavigator: MedicalCertificateEmployeeNavigator,
    public medicalCertificateEmployeePersist: MedicalCertificateEmployeePersist
  ) {
    this.tcAuthorization.requireRead('medical_certificate_employees');
    this.medicalCertificateEmployeeDetail =
      new MedicalCertificateEmployeeDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('medical_certificate_employees');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.medicalCertificateEmployeeIsLoading = true;
    this.medicalCertificateEmployeePersist
      .getMedicalCertificateEmployee(id)
      .subscribe(
        (medicalCertificateEmployeeDetail) => {
          this.medicalCertificateEmployeeDetail =
            medicalCertificateEmployeeDetail;
          this.medicalCertificateEmployeeIsLoading = false;
        },
        (error) => {
          console.error(error);
          this.medicalCertificateEmployeeIsLoading = false;
        }
      );
  }

  reload() {
    this.loadDetails(this.medicalCertificateEmployeeDetail.id);
  }
  editMedicalCertificateEmployee(): void {
    let modalRef =
      this.medicalCertificateEmployeeNavigator.editMedicalCertificateEmployee(
        this.medicalCertificateEmployeeDetail.id
      );
    modalRef.afterClosed().subscribe(
      (medicalCertificateEmployeeDetail) => {
        TCUtilsAngular.assign(
          this.medicalCertificateEmployeeDetail,
          medicalCertificateEmployeeDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.medicalCertificateEmployeePersist
      .print(this.medicalCertificateEmployeeDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(
          printJob.id,
          'print medical_certificate_employee',
          true
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
