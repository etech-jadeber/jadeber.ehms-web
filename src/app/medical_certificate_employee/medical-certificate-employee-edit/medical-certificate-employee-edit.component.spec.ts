import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalCertificateEmployeeEditComponent } from './medical-certificate-employee-edit.component';

describe('MedicalCertificateEmployeeEditComponent', () => {
  let component: MedicalCertificateEmployeeEditComponent;
  let fixture: ComponentFixture<MedicalCertificateEmployeeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCertificateEmployeeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateEmployeeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
