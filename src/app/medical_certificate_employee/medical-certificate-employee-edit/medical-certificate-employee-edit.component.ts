import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MedicalCertificateEmployeeDetail } from '../medical_certificate_employee.model';
import { MedicalCertificateEmployeePersist } from '../medical_certificate_employee.persist';
import {PatientNavigator} from "../../patients/patients.navigator";
import { PatientSummary } from 'src/app/patients/patients.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { physican_type } from 'src/app/app.enums';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';

@Component({
  selector: 'app-medical_certificate_employee-edit',
  templateUrl: './medical-certificate-employee-edit.component.html',
  styleUrls: ['./medical-certificate-employee-edit.component.css'],
})
export class MedicalCertificateEmployeeEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  medicalCertificateEmployeeDetail: MedicalCertificateEmployeeDetail;
  checkPastDisease: boolean = false;
  checkHospitalized: boolean = false;
  doctorFullName : string;
  pid : string;
  doctor_id : string;



  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<MedicalCertificateEmployeeEditComponent>,
    public persist: MedicalCertificateEmployeePersist,

    public tcUtilsDate: TCUtilsDate,
    public patientNavigator: PatientNavigator,
    public doctorNavigator: DoctorNavigator,
    public appointmentPersist: AppointmentPersist,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('medical_certificate_employees');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'medical_certificate_employee';
      this.medicalCertificateEmployeeDetail =
        new MedicalCertificateEmployeeDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('medical_certificate_employees');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'medical_certificate_employee';
      this.isLoadingResults = true;
      this.persist.getMedicalCertificateEmployee(this.idMode.id).subscribe(
        (medicalCertificateEmployeeDetail) => {
          this.medicalCertificateEmployeeDetail =
            medicalCertificateEmployeeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.medicalCertificateEmployeeDetail.sex = true;
    this.persist
      .addMedicalCertificateEmployee(this.idMode.id,this.medicalCertificateEmployeeDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('medicalCertificateEmployee added');
          this.medicalCertificateEmployeeDetail.id = value.id;
          this.dialogRef.close(this.medicalCertificateEmployeeDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist
      .updateMedicalCertificateEmployee(this.medicalCertificateEmployeeDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('medical_certificate_employee updated');
          this.dialogRef.close(this.medicalCertificateEmployeeDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }
  canSubmit(): boolean {
    if (this.medicalCertificateEmployeeDetail == null) {
      return false;
    }
    if (this.medicalCertificateEmployeeDetail.doctor_examination == null || this.medicalCertificateEmployeeDetail.doctor_examination == ""){
      return false;
    }
    if (this.medicalCertificateEmployeeDetail.condition == null || this.medicalCertificateEmployeeDetail.condition == ""){
      return false;
    }
    if (this.medicalCertificateEmployeeDetail.employment_fit == null){
      return false;
    }
    return true;
  }

  patientData: PatientSummary;
  patientFullName: String;

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientData = result;
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.pid = result[0].id;

        // this.patientFullName = result[0].fname + ' ' + result[0].lname;
        // this.birthDetail.mothername = result[0].id;
      }
    });
  }

  searchDoctors() {
    let dialogRef = this.doctorNavigator.pickDoctors(
      true,
      physican_type.doctor
    );
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.doctorFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.doctor_id = result[0].id;
        // this.appointmentPersist.doctorId = result[0].id;
        // this.searchAppointments();
      }
    });
  }
}
