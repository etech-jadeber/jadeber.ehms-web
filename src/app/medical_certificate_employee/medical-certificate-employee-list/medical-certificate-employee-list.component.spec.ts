import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalCertificateEmployeeListComponent } from './medical-certificate-employee-list.component';

describe('MedicalCertificateEmployeeListComponent', () => {
  let component: MedicalCertificateEmployeeListComponent;
  let fixture: ComponentFixture<MedicalCertificateEmployeeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCertificateEmployeeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateEmployeeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
