import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCModalWidths, TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  MedicalCertificateEmployeeDetail,
  MedicalCertificateEmployeeSummary,
  MedicalCertificateEmployeeSummaryPartialList,
} from '../medical_certificate_employee.model';
import { MedicalCertificateEmployeePersist } from '../medical_certificate_employee.persist';
import { MedicalCertificateEmployeeNavigator } from '../medical_certificate_employee.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MedicalCertificateEmployeeDetailComponent } from '../medical-certificate-employee-detail/medical-certificate-employee-detail.component';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-medical_certificate_employee-list',
  templateUrl: './medical-certificate-employee-list.component.html',
  styleUrls: ['./medical-certificate-employee-list.component.css'],
})
export class MedicalCertificateEmployeeListComponent implements OnInit {
  medicalCertificateEmployeesData: MedicalCertificateEmployeeSummary[] = [];
  medicalCertificateEmployeesTotalCount: number = 0;
  medicalCertificateEmployeeSelectAll: boolean = false;
  medicalCertificateEmployeeSelection: MedicalCertificateEmployeeSummary[] = [];
  age : number;
  medicalCertificateEmployeesDisplayedColumns: string[] = [
    'select',
    'action',
    'doctor_examination',
   'condition',
    'employment_fit',
  ];
  medicalCertificateEmployeeSearchTextBox: FormControl = new FormControl();
  medicalCertificateEmployeeIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  medicalCertificateEmployeesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  medicalCertificateEmployeesSort: MatSort;

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  constructor(
    private router: Router,
    public form_encounterPersist: Form_EncounterPersist,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public medicalCertificateEmployeePersist: MedicalCertificateEmployeePersist,
    public medicalCertificateEmployeeNavigator: MedicalCertificateEmployeeNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
  ) {
    this.tcAuthorization.requireRead('medical_certificate_employees');
    this.medicalCertificateEmployeeSearchTextBox.setValue(
      medicalCertificateEmployeePersist.medicalCertificateEmployeeSearchText
    );
    //delay subsequent keyup events
    this.medicalCertificateEmployeeSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.medicalCertificateEmployeePersist.medicalCertificateEmployeeSearchText =
          value;
        this.searchMedical_certificate_employees();
      });
  }
  ngOnInit() {
    this.medicalCertificateEmployeePersist.patientId = this.patientId
    this.medicalCertificateEmployeesSort.sortChange.subscribe(() => {
      this.medicalCertificateEmployeesPaginator.pageIndex = 0;
      this.searchMedical_certificate_employees(true);
    });

    this.medicalCertificateEmployeesPaginator.page.subscribe(() => {
      this.searchMedical_certificate_employees(true);
    });
    //start by loading items
    this.searchMedical_certificate_employees();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.medicalCertificateEmployeePersist.encounterId = this.encounterId;
    } else {
    this.medicalCertificateEmployeePersist.encounterId = null;
    }
    this.searchMedical_certificate_employees()
    }

  searchMedical_certificate_employees(isPagination: boolean = false): void {
    let paginator = this.medicalCertificateEmployeesPaginator;
    let sorter = this.medicalCertificateEmployeesSort;

    this.medicalCertificateEmployeeIsLoading = true;
    this.medicalCertificateEmployeeSelection = [];

    this.medicalCertificateEmployeePersist
      .searchMedicalCertificateEmployee(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: MedicalCertificateEmployeeSummaryPartialList) => {
          this.medicalCertificateEmployeesData = partialList.data;
          if (partialList.total != -1) {
            this.medicalCertificateEmployeesTotalCount = partialList.total;
          }
          this.medicalCertificateEmployeeIsLoading = false;
        },
        (error) => {
          this.medicalCertificateEmployeeIsLoading = false;
        }
      );
  }
  downloadMedicalCertificateEmployees(): void {
    if (this.medicalCertificateEmployeeSelectAll) {
      this.medicalCertificateEmployeePersist
        .downloadAll()
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download medical_certificate_employee',
            true
          );
        });
    } else {
      this.medicalCertificateEmployeePersist
        .download(
          this.tcUtilsArray.idsList(this.medicalCertificateEmployeeSelection)
        )
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download medical_certificate_employee',
            true
          );
        });
    }
  }
  addMedical_certificate_employee(): void {
    let dialogRef =
      this.medicalCertificateEmployeeNavigator.addMedicalCertificateEmployee(
        this.encounterId
      );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchMedical_certificate_employees();
      }
    });
  }

  editMedicalCertificateEmployee(item: MedicalCertificateEmployeeSummary) {
    let dialogRef =
      this.medicalCertificateEmployeeNavigator.editMedicalCertificateEmployee(
        item.id
      );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteMedicalCertificateEmployee(
    item: MedicalCertificateEmployeeSummary
  ): void {
    let dialogRef = this.tcNavigator.confirmDeletion(
      'medical_certificate_employee'
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.medicalCertificateEmployeePersist
          .deleteMedicalCertificateEmployee(item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success(
                'medical_certificate_employee deleted'
              );
              this.searchMedical_certificate_employees();
            },
            (error) => {}
          );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
