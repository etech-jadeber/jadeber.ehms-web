import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicalCertificateEmployeePickComponent } from './medical-certificate-employee-pick.component';

describe('MedicalCertificateEmployeePickComponent', () => {
  let component: MedicalCertificateEmployeePickComponent;
  let fixture: ComponentFixture<MedicalCertificateEmployeePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalCertificateEmployeePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCertificateEmployeePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
