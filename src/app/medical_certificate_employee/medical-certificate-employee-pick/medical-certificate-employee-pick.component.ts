import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  MedicalCertificateEmployeeSummary,
  MedicalCertificateEmployeeSummaryPartialList,
} from '../medical_certificate_employee.model';
import { MedicalCertificateEmployeePersist } from '../medical_certificate_employee.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-medical_certificate_employee-pick',
  templateUrl: './medical-certificate-employee-pick.component.html',
  styleUrls: ['./medical-certificate-employee-pick.component.css'],
})
export class MedicalCertificateEmployeePickComponent implements OnInit {
  medicalCertificateEmployeesData: MedicalCertificateEmployeeSummary[] = [];
  medicalCertificateEmployeesTotalCount: number = 0;
  medicalCertificateEmployeeSelectAll: boolean = false;
  medicalCertificateEmployeeSelection: MedicalCertificateEmployeeSummary[] = [];

  medicalCertificateEmployeesDisplayedColumns: string[] = [
    'select',
    ,
    'cpa',
    'name',
    'address',
    'age',
    'sex',
    'disease_past',
    'hospitalized',
    'doctor_examination',
    'condition',
    'blood_pressure',
    'lung',
    'lung_xray',
    'urine',
    'blood',
    'sugar',
    'albumen',
    'arobllid',
    'hbred',
    'reger',
    'white',
    'cells',
    'visual_acuit',
    'trachoma',
    'progressive_eye_disease',
    'ear',
    'serological',
    'ns_disturbance',
    'mental_status',
    'employment_fit',
    'doctor_name',
  ];
  medicalCertificateEmployeeSearchTextBox: FormControl = new FormControl();
  medicalCertificateEmployeeIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  medicalCertificateEmployeesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  medicalCertificateEmployeesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public medicalCertificateEmployeePersist: MedicalCertificateEmployeePersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<MedicalCertificateEmployeePickComponent>,
    public tcUtilsString: TCUtilsString,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('medical_certificate_employees');
    this.medicalCertificateEmployeeSearchTextBox.setValue(
      medicalCertificateEmployeePersist.medicalCertificateEmployeeSearchText
    );
    //delay subsequent keyup events
    this.medicalCertificateEmployeeSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.medicalCertificateEmployeePersist.medicalCertificateEmployeeSearchText =
          value;
        this.searchMedical_certificate_employees();
      });
  }
  ngOnInit() {
    this.medicalCertificateEmployeesSort.sortChange.subscribe(() => {
      this.medicalCertificateEmployeesPaginator.pageIndex = 0;
      this.searchMedical_certificate_employees(true);
    });

    this.medicalCertificateEmployeesPaginator.page.subscribe(() => {
      this.searchMedical_certificate_employees(true);
    });
    //start by loading items
    this.searchMedical_certificate_employees();
  }

  searchMedical_certificate_employees(isPagination: boolean = false): void {
    let paginator = this.medicalCertificateEmployeesPaginator;
    let sorter = this.medicalCertificateEmployeesSort;

    this.medicalCertificateEmployeeIsLoading = true;
    this.medicalCertificateEmployeeSelection = [];

    this.medicalCertificateEmployeePersist
      .searchMedicalCertificateEmployee(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: MedicalCertificateEmployeeSummaryPartialList) => {
          this.medicalCertificateEmployeesData = partialList.data;
          if (partialList.total != -1) {
            this.medicalCertificateEmployeesTotalCount = partialList.total;
          }
          this.medicalCertificateEmployeeIsLoading = false;
        },
        (error) => {
          this.medicalCertificateEmployeeIsLoading = false;
        }
      );
  }
  markOneItem(item: MedicalCertificateEmployeeSummary) {
    if (
      !this.tcUtilsArray.containsId(
        this.medicalCertificateEmployeeSelection,
        item.id
      )
    ) {
      this.medicalCertificateEmployeeSelection = [];
      this.medicalCertificateEmployeeSelection.push(item);
    } else {
      this.medicalCertificateEmployeeSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.medicalCertificateEmployeeSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
