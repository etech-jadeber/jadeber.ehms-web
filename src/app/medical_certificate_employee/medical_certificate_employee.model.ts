import {TCId} from "../tc/models";
export class MedicalCertificateEmployeeSummary extends TCId {
cpa:string;
name:string;
address:string;
age:number;
sex:boolean;
disease_past:string;
hospitalized:string;
doctor_examination:string;
condition:string;
blood_pressure:string;
lung:string;
lung_xray:string;
urine:string;
blood:string;
sugar:string;
albumen:string;
arobllid:string;
hbred:string;
reger:string;
white:string;
cells:string;
visual_acuit:string;
trachoma:string;
progressive_eye_disease:string;
ear:string;
serological:string;
ns_disturbance:string;
mental_status:string;
employment_fit:string;
doctor_name:string;
pid: string;
doctor_id: string;
 
}
export class MedicalCertificateEmployeeSummaryPartialList {
  data: MedicalCertificateEmployeeSummary[];
  total: number;
}
export class MedicalCertificateEmployeeDetail extends MedicalCertificateEmployeeSummary {
}

export class MedicalCertificateEmployeeDashboard {
  total: number;
}