import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { MedicalCertificateEmployeeEditComponent } from './medical-certificate-employee-edit/medical-certificate-employee-edit.component';
import { MedicalCertificateEmployeePickComponent } from './medical-certificate-employee-pick/medical-certificate-employee-pick.component';

@Injectable({
  providedIn: 'root',
})
export class MedicalCertificateEmployeeNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  medical_certificate_employeesUrl(): string {
    return '/medical_certificate_employees';
  }

  medical_certificate_employeeUrl(id: string): string {
    return '/medical_certificate_employees/' + id;
  }

  viewMedicalCertificateEmployees(): void {
    this.router.navigateByUrl(this.medical_certificate_employeesUrl());
  }

  viewMedicalCertificateEmployee(id: string): void {
    this.router.navigateByUrl(this.medical_certificate_employeeUrl(id));
  }

  editMedicalCertificateEmployee(
    id: string
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(
      MedicalCertificateEmployeeEditComponent,
      {
        data: new TCIdMode(id, TCModalModes.EDIT),
        width: TCModalWidths.medium,
      }
    );
    return dialogRef;
  }

  addMedicalCertificateEmployee(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(
      MedicalCertificateEmployeeEditComponent,
      {
        data: new TCIdMode(id, TCModalModes.NEW),
        width: TCModalWidths.medium,
      }
    );
    return dialogRef;
  }

  pickMedicalCertificateEmployees(
    selectOne: boolean = false
  ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(
      MedicalCertificateEmployeePickComponent,
      {
        data: selectOne,
        width: TCModalWidths.large,
      }
    );
    return dialogRef;
  }
}
