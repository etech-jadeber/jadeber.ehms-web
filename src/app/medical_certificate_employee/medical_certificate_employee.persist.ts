import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  MedicalCertificateEmployeeDashboard,
  MedicalCertificateEmployeeDetail,
  MedicalCertificateEmployeeSummaryPartialList,
} from './medical_certificate_employee.model';
import { MedicalEmploymentStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class MedicalCertificateEmployeePersist {
  medicalCertificateEmployeeSearchText: string = '';
  medicalEmploymentStatusFilter: number ;
  patientId: string;
  encounterId: string;

    MedicalEmploymentStatus: TCEnum[] = [
     new TCEnum( MedicalEmploymentStatus.fit, 'Fit'),
  new TCEnum( MedicalEmploymentStatus.unfit, 'Unfit'),

  ];

  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.medicalCertificateEmployeeSearchText;
    //add custom filters
    return fltrs;
  }

  searchMedicalCertificateEmployee(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<MedicalCertificateEmployeeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
     'medical_certificate_employee',
      this.medicalCertificateEmployeeSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "pid", this.patientId)
    }
    if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
    }
    return this.http.get<MedicalCertificateEmployeeSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'medical_certificate_employee/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  medicalCertificateEmployeeDashboard(): Observable<MedicalCertificateEmployeeDashboard> {
    return this.http.get<MedicalCertificateEmployeeDashboard>(
      environment.tcApiBaseUri + 'medical_certificate_employee/dashboard'
    );
  }

  getMedicalCertificateEmployee(
    id: string
  ): Observable<MedicalCertificateEmployeeDetail> {
    return this.http.get<MedicalCertificateEmployeeDetail>(
      environment.tcApiBaseUri + 'medical_certificate_employee/' + id
    );
  }
  addMedicalCertificateEmployee(id: string,
    item: MedicalCertificateEmployeeDetail
  ): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri +
        'form_encounter/' +
        id +
        '/medical_certificate_employee',
      item
    );
  }

  updateMedicalCertificateEmployee(
    item: MedicalCertificateEmployeeDetail
  ): Observable<MedicalCertificateEmployeeDetail> {
    return this.http.patch<MedicalCertificateEmployeeDetail>(
      environment.tcApiBaseUri + 'medical_certificate_employee/' + item.id,
      item
    );
  }

  deleteMedicalCertificateEmployee(id: string): Observable<{}> {
    return this.http.delete(
      environment.tcApiBaseUri + 'medical_certificate_employee/' + id
    );
  }
  medicalCertificateEmployeesDo(parent_id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + "form_encounters/" +  parent_id + '/medical_certificate_employee/do',
      new TCDoParam(method, payload)
    );
  }

  medicalCertificateEmployeeDo(
    id: string,
    method: string,
    payload: any
  ): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'medical_certificate_employees/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'medical_certificate_employee/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'medical_certificate_employee/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
