import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupPackageDetailComponent } from './medical-checkup-package-detail.component';

describe('MedicalCheckupPackageDetailComponent', () => {
  let component: MedicalCheckupPackageDetailComponent;
  let fixture: ComponentFixture<MedicalCheckupPackageDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupPackageDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupPackageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
