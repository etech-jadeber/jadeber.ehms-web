import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {MedicalCheckupPackageDetail} from "../medical_checkup_package.model";
import {MedicalCheckupPackagePersist} from "../medical_checkup_package.persist";
import {MedicalCheckupPackageNavigator} from "../medical_checkup_package.navigator";
import { TCUtilsNumber } from 'src/app/tc/utils-number';

export enum MedicalCheckupPackageTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-medical_checkup_package-detail',
  templateUrl: './medical-checkup-package-detail.component.html',
  styleUrls: ['./medical-checkup-package-detail.component.scss']
})
export class MedicalCheckupPackageDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  medicalCheckupPackageIsLoading:boolean = false;
  
  MedicalCheckupPackageTabs: typeof MedicalCheckupPackageTabs = MedicalCheckupPackageTabs;
  activeTab: MedicalCheckupPackageTabs = MedicalCheckupPackageTabs.overview;
  //basics
  medicalCheckupPackageDetail: MedicalCheckupPackageDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public tcUtilsNumber: TCUtilsNumber,
              public appTranslation:AppTranslation,
              public medicalCheckupPackageNavigator: MedicalCheckupPackageNavigator,
              public  medicalCheckupPackagePersist: MedicalCheckupPackagePersist) {
    this.tcAuthorization.requireRead("medical_checkup_packages");
    this.medicalCheckupPackageDetail = new MedicalCheckupPackageDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("medical_checkup_packages");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.medicalCheckupPackageIsLoading = true;
    this.medicalCheckupPackagePersist.getMedicalCheckupPackage(id).subscribe(medicalCheckupPackageDetail => {
          this.medicalCheckupPackageDetail = medicalCheckupPackageDetail;
          this.medicalCheckupPackageIsLoading = false;
        }, error => {
          console.error(error);
          this.medicalCheckupPackageIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.medicalCheckupPackageDetail.id);
  }
  editMedicalCheckupPackage(): void {
    let modalRef = this.medicalCheckupPackageNavigator.editMedicalCheckupPackage(this.medicalCheckupPackageDetail.id);
    modalRef.afterClosed().subscribe(medicalCheckupPackageDetail => {
      TCUtilsAngular.assign(this.medicalCheckupPackageDetail, medicalCheckupPackageDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.medicalCheckupPackagePersist.print(this.medicalCheckupPackageDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print medical_checkup_package", true);
      });
    }

  back():void{
      this.location.back();
    }

}
