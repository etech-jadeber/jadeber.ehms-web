import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupPackageEditComponent } from './medical-checkup-package-edit.component';

describe('MedicalCheckupPackageEditComponent', () => {
  let component: MedicalCheckupPackageEditComponent;
  let fixture: ComponentFixture<MedicalCheckupPackageEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupPackageEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupPackageEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
