import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MedicalCheckupPackageDetail } from '../medical_checkup_package.model';
import { MedicalCheckupPackagePersist } from '../medical_checkup_package.persist';

@Component({
  selector: 'app-medical_checkup_package-edit',
  templateUrl: './medical-checkup-package-edit.component.html',
  styleUrls: ['./medical-checkup-package-edit.component.scss']
})
export class MedicalCheckupPackageEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  medicalCheckupPackageDetail: MedicalCheckupPackageDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MedicalCheckupPackageEditComponent>,
              public  persist: MedicalCheckupPackagePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("medical_checkup_packages");
      this.title = this.appTranslation.getText("general","new") +  " " + "medical_checkup_package";
      this.medicalCheckupPackageDetail = new MedicalCheckupPackageDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("medical_checkup_packages");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "medical_checkup_package";
      this.isLoadingResults = true;
      this.persist.getMedicalCheckupPackage(this.idMode.id).subscribe(medicalCheckupPackageDetail => {
        this.medicalCheckupPackageDetail = medicalCheckupPackageDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addMedicalCheckupPackage(this.medicalCheckupPackageDetail).subscribe(value => {
      this.tcNotification.success("medicalCheckupPackage added");
      this.medicalCheckupPackageDetail.id = value.id;
      this.dialogRef.close(this.medicalCheckupPackageDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateMedicalCheckupPackage(this.medicalCheckupPackageDetail).subscribe(value => {
      this.tcNotification.success("medical_checkup_package updated");
      this.dialogRef.close(this.medicalCheckupPackageDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.medicalCheckupPackageDetail == null){
            return false;
          }

if (this.medicalCheckupPackageDetail.name == null || this.medicalCheckupPackageDetail.name  == "") {
            return false;
        } 
if (this.medicalCheckupPackageDetail.price == null) {
            return false;
        }
        return true;
 }
 }
