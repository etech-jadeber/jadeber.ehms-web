import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupPackageListComponent } from './medical-checkup-package-list.component';

describe('MedicalCheckupPackageListComponent', () => {
  let component: MedicalCheckupPackageListComponent;
  let fixture: ComponentFixture<MedicalCheckupPackageListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupPackageListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupPackageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
