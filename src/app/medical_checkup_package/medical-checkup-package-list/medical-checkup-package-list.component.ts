import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MedicalCheckupPackageSummary, MedicalCheckupPackageSummaryPartialList } from '../medical_checkup_package.model';
import { MedicalCheckupPackagePersist } from '../medical_checkup_package.persist';
import { MedicalCheckupPackageNavigator } from '../medical_checkup_package.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsNumber } from 'src/app/tc/utils-number';
@Component({
  selector: 'app-medical_checkup_package-list',
  templateUrl: './medical-checkup-package-list.component.html',
  styleUrls: ['./medical-checkup-package-list.component.scss']
})export class MedicalCheckupPackageListComponent implements OnInit {
  medicalCheckupPackagesData: MedicalCheckupPackageSummary[] = [];
  medicalCheckupPackagesTotalCount: number = 0;
  medicalCheckupPackageSelectAll:boolean = false;
  medicalCheckupPackageSelection: MedicalCheckupPackageSummary[] = [];

 medicalCheckupPackagesDisplayedColumns: string[] = ["select","action" ,"name","price" ];
  medicalCheckupPackageSearchTextBox: FormControl = new FormControl();
  medicalCheckupPackageIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) medicalCheckupPackagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) medicalCheckupPackagesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public medicalCheckupPackagePersist: MedicalCheckupPackagePersist,
                public medicalCheckupPackageNavigator: MedicalCheckupPackageNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsumber: TCUtilsNumber

    ) {

        this.tcAuthorization.requireRead("medical_checkup_packages");
       this.medicalCheckupPackageSearchTextBox.setValue(medicalCheckupPackagePersist.medicalCheckupPackageSearchText);
      //delay subsequent keyup events
      this.medicalCheckupPackageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.medicalCheckupPackagePersist.medicalCheckupPackageSearchText = value;
        this.searchMedicalCheckupPackages();
      });
    } ngOnInit() {
   
      this.medicalCheckupPackagesSort.sortChange.subscribe(() => {
        this.medicalCheckupPackagesPaginator.pageIndex = 0;
        this.searchMedicalCheckupPackages(true);
      });

      this.medicalCheckupPackagesPaginator.page.subscribe(() => {
        this.searchMedicalCheckupPackages(true);
      });
      //start by loading items
      this.searchMedicalCheckupPackages();
    }

  searchMedicalCheckupPackages(isPagination:boolean = false): void {


    let paginator = this.medicalCheckupPackagesPaginator;
    let sorter = this.medicalCheckupPackagesSort;

    this.medicalCheckupPackageIsLoading = true;
    this.medicalCheckupPackageSelection = [];

    this.medicalCheckupPackagePersist.searchMedicalCheckupPackage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MedicalCheckupPackageSummaryPartialList) => {
      this.medicalCheckupPackagesData = partialList.data;
      if (partialList.total != -1) {
        this.medicalCheckupPackagesTotalCount = partialList.total;
      }
      this.medicalCheckupPackageIsLoading = false;
    }, error => {
      this.medicalCheckupPackageIsLoading = false;
    });

  } downloadMedicalCheckupPackages(): void {
    if(this.medicalCheckupPackageSelectAll){
         this.medicalCheckupPackagePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download medical_checkup_package", true);
      });
    }
    else{
        this.medicalCheckupPackagePersist.download(this.tcUtilsArray.idsList(this.medicalCheckupPackageSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download medical_checkup_package",true);
            });
        }
  }
addMedicalCheckupPackage(): void {
    let dialogRef = this.medicalCheckupPackageNavigator.addMedicalCheckupPackage();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMedicalCheckupPackages();
      }
    });
  }

  editMedicalCheckupPackage(item: MedicalCheckupPackageSummary) {
    let dialogRef = this.medicalCheckupPackageNavigator.editMedicalCheckupPackage(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMedicalCheckupPackage(item: MedicalCheckupPackageSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("medical_checkup_package");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.medicalCheckupPackagePersist.deleteMedicalCheckupPackage(item.id).subscribe(response => {
          this.tcNotification.success("medical_checkup_package deleted");
          this.searchMedicalCheckupPackages();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
