import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupPackagePickComponent } from './medical-checkup-package-pick.component';

describe('MedicalCheckupPackagePickComponent', () => {
  let component: MedicalCheckupPackagePickComponent;
  let fixture: ComponentFixture<MedicalCheckupPackagePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupPackagePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupPackagePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
