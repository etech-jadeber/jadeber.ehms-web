import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MedicalCheckupPackageSummary, MedicalCheckupPackageSummaryPartialList } from '../medical_checkup_package.model';
import { MedicalCheckupPackagePersist } from '../medical_checkup_package.persist';
import { MedicalCheckupPackageNavigator } from '../medical_checkup_package.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-medical_checkup_package-pick',
  templateUrl: './medical-checkup-package-pick.component.html',
  styleUrls: ['./medical-checkup-package-pick.component.scss']
})export class MedicalCheckupPackagePickComponent implements OnInit {
  medicalCheckupPackagesData: MedicalCheckupPackageSummary[] = [];
  medicalCheckupPackagesTotalCount: number = 0;
  medicalCheckupPackageSelectAll:boolean = false;
  medicalCheckupPackageSelection: MedicalCheckupPackageSummary[] = [];

 medicalCheckupPackagesDisplayedColumns: string[] = ["select","name","price" ];
  medicalCheckupPackageSearchTextBox: FormControl = new FormControl();
  medicalCheckupPackageIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) medicalCheckupPackagesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) medicalCheckupPackagesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public medicalCheckupPackagePersist: MedicalCheckupPackagePersist,
                public medicalCheckupPackageNavigator: MedicalCheckupPackageNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<MedicalCheckupPackagePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("medical_checkup_packages");
       this.medicalCheckupPackageSearchTextBox.setValue(medicalCheckupPackagePersist.medicalCheckupPackageSearchText);
      //delay subsequent keyup events
      this.medicalCheckupPackageSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.medicalCheckupPackagePersist.medicalCheckupPackageSearchText = value;
        this.searchMedical_checkup_packages();
      });
    } ngOnInit() {
   
      this.medicalCheckupPackagesSort.sortChange.subscribe(() => {
        this.medicalCheckupPackagesPaginator.pageIndex = 0;
        this.searchMedical_checkup_packages(true);
      });

      this.medicalCheckupPackagesPaginator.page.subscribe(() => {
        this.searchMedical_checkup_packages(true);
      });
      //start by loading items
      this.searchMedical_checkup_packages();
    }

  searchMedical_checkup_packages(isPagination:boolean = false): void {


    let paginator = this.medicalCheckupPackagesPaginator;
    let sorter = this.medicalCheckupPackagesSort;

    this.medicalCheckupPackageIsLoading = true;
    this.medicalCheckupPackageSelection = [];

    this.medicalCheckupPackagePersist.searchMedicalCheckupPackage(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MedicalCheckupPackageSummaryPartialList) => {
      this.medicalCheckupPackagesData = partialList.data;
      if (partialList.total != -1) {
        this.medicalCheckupPackagesTotalCount = partialList.total;
      }
      this.medicalCheckupPackageIsLoading = false;
    }, error => {
      this.medicalCheckupPackageIsLoading = false;
    });

  }
  markOneItem(item: MedicalCheckupPackageSummary) {
    if(!this.tcUtilsArray.containsId(this.medicalCheckupPackageSelection,item.id)){
          this.medicalCheckupPackageSelection = [];
          this.medicalCheckupPackageSelection.push(item);
        }
        else{
          this.medicalCheckupPackageSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.medicalCheckupPackageSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
