import {TCId} from "../tc/models";

export class MedicalCheckupPackageSummary extends TCId {
    name:string;
    price:number;
     
    }
    export class MedicalCheckupPackageSummaryPartialList {
      data: MedicalCheckupPackageSummary[];
      total: number;
    }
    export class MedicalCheckupPackageDetail extends MedicalCheckupPackageSummary {
    }
    
    export class MedicalCheckupPackageDashboard {
      total: number;
    }
    