import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {MedicalCheckupPackageEditComponent} from "./medical-checkup-package-edit/medical-checkup-package-edit.component";
import {MedicalCheckupPackagePickComponent} from "./medical-checkup-package-pick/medical-checkup-package-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MedicalCheckupPackageNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  medicalCheckupPackagesUrl(): string {
    return "/medical_checkup_packages";
  }

  medicalCheckupPackageUrl(id: string): string {
    return "/medical_checkup_packages/" + id;
  }

  viewMedicalCheckupPackages(): void {
    this.router.navigateByUrl(this.medicalCheckupPackagesUrl());
  }

  viewMedicalCheckupPackage(id: string): void {
    this.router.navigateByUrl(this.medicalCheckupPackageUrl(id));
  }

  editMedicalCheckupPackage(id: string): MatDialogRef<MedicalCheckupPackageEditComponent> {
    const dialogRef = this.dialog.open(MedicalCheckupPackageEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMedicalCheckupPackage(): MatDialogRef<MedicalCheckupPackageEditComponent> {
    const dialogRef = this.dialog.open(MedicalCheckupPackageEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMedicalCheckupPackages(selectOne: boolean=false): MatDialogRef<MedicalCheckupPackagePickComponent> {
      const dialogRef = this.dialog.open(MedicalCheckupPackagePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
