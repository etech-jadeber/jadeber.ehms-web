import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {MedicalCheckupPackageDashboard, MedicalCheckupPackageDetail, MedicalCheckupPackageSummaryPartialList} from "./medical_checkup_package.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class MedicalCheckupPackagePersist {
 medicalCheckupPackageSearchText: string = "";
 status: number;
 
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.medicalCheckupPackageSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchMedicalCheckupPackage(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MedicalCheckupPackageSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("medical_checkup_package", this.medicalCheckupPackageSearchText, pageSize, pageIndex, sort, order);
    if (this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    return this.http.get<MedicalCheckupPackageSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_package/do", new TCDoParam("download_all", this.filters()));
  }

  medicalCheckupPackageDashboard(): Observable<MedicalCheckupPackageDashboard> {
    return this.http.get<MedicalCheckupPackageDashboard>(environment.tcApiBaseUri + "medical_checkup_package/dashboard");
  }

  getMedicalCheckupPackage(id: string): Observable<MedicalCheckupPackageDetail> {
    return this.http.get<MedicalCheckupPackageDetail>(environment.tcApiBaseUri + "medical_checkup_package/" + id);
  } addMedicalCheckupPackage(item: MedicalCheckupPackageDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_package/", item);
  }

  updateMedicalCheckupPackage(item: MedicalCheckupPackageDetail): Observable<MedicalCheckupPackageDetail> {
    return this.http.patch<MedicalCheckupPackageDetail>(environment.tcApiBaseUri + "medical_checkup_package/" + item.id, item);
  }

  deleteMedicalCheckupPackage(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "medical_checkup_package/" + id);
  }
 medicalCheckupPackagesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "medical_checkup_package/do", new TCDoParam(method, payload));
  }

  medicalCheckupPackageDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "medical_checkup_package/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_package/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_package/" + id + "/do", new TCDoParam("print", {}));
  }


}
