import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupServiceDetailComponent } from './medical-checkup-service-detail.component';

describe('MedicalCheckupServiceDetailComponent', () => {
  let component: MedicalCheckupServiceDetailComponent;
  let fixture: ComponentFixture<MedicalCheckupServiceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupServiceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupServiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
