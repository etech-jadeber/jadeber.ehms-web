import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {MedicalCheckupServiceDetail} from "../medical_checkup_service.model";
import {MedicalCheckupServicePersist} from "../medical_checkup_service.persist";
import {MedicalCheckupServiceNavigator} from "../medical_checkup_service.navigator";

export enum MedicalCheckupServiceTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-medical_checkup_service-detail',
  templateUrl: './medical-checkup-service-detail.component.html',
  styleUrls: ['./medical-checkup-service-detail.component.scss']
})
export class MedicalCheckupServiceDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  medicalCheckupServiceIsLoading:boolean = false;
  
  MedicalCheckupServiceTabs: typeof MedicalCheckupServiceTabs = MedicalCheckupServiceTabs;
  activeTab: MedicalCheckupServiceTabs = MedicalCheckupServiceTabs.overview;
  //basics
  medicalCheckupServiceDetail: MedicalCheckupServiceDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public medicalCheckupServiceNavigator: MedicalCheckupServiceNavigator,
              public  medicalCheckupServicePersist: MedicalCheckupServicePersist) {
    this.tcAuthorization.requireRead("medical_checkup_services");
    this.medicalCheckupServiceDetail = new MedicalCheckupServiceDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("medical_checkup_services");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.medicalCheckupServiceIsLoading = true;
    this.medicalCheckupServicePersist.getMedicalCheckupService(id).subscribe(medicalCheckupServiceDetail => {
          this.medicalCheckupServiceDetail = medicalCheckupServiceDetail;
          this.medicalCheckupServiceIsLoading = false;
        }, error => {
          console.error(error);
          this.medicalCheckupServiceIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.medicalCheckupServiceDetail.id);
  }
  editMedicalCheckupService(): void {
    let modalRef = this.medicalCheckupServiceNavigator.editMedicalCheckupService(this.medicalCheckupServiceDetail.id);
    modalRef.afterClosed().subscribe(medicalCheckupServiceDetail => {
      TCUtilsAngular.assign(this.medicalCheckupServiceDetail, medicalCheckupServiceDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.medicalCheckupServicePersist.print(this.medicalCheckupServiceDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print medical_checkup_service", true);
      });
    }

  back():void{
      this.location.back();
    }

}
