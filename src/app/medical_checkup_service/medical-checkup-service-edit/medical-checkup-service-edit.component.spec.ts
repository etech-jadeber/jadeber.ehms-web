import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupServiceEditComponent } from './medical-checkup-service-edit.component';

describe('MedicalCheckupServiceEditComponent', () => {
  let component: MedicalCheckupServiceEditComponent;
  let fixture: ComponentFixture<MedicalCheckupServiceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupServiceEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
