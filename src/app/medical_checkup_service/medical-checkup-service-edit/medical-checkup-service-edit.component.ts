import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MedicalCheckupServiceDetail } from '../medical_checkup_service.model';
import { MedicalCheckupServicePersist } from '../medical_checkup_service.persist';
import { FeePersist } from 'src/app/fees/fee.persist';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { BedroomNavigator } from 'src/app/bed/bedrooms/bedroom.navigator';
import { BedroomSummary } from 'src/app/bed/bedrooms/bedroom.model';
import { Lab_PanelSummary } from 'src/app/lab_panels/lab_panel.model';
import { Lab_PanelNavigator } from 'src/app/lab_panels/lab_panel.navigator';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { OtherServicesSummary } from 'src/app/other_services/other-services.model';
import { Physiotherapy_TypeSummary } from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { Physiotherapy_TypeNavigator } from 'src/app/physiotherapy_types/physiotherapy_type.navigator';
import { ProcedureSummary } from 'src/app/procedures/procedure.model';
import { fee_type, lab_order_type, physican_type } from 'src/app/app.enums';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { Department_SpecialtyNavigator } from 'src/app/department_specialtys/department_specialty.navigator';

@Component({
  selector: 'app-medical_checkup_service-edit',
  templateUrl: './medical-checkup-service-edit.component.html',
  styleUrls: ['./medical-checkup-service-edit.component.scss']
})export class MedicalCheckupServiceEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  Name: string;
  feeType = fee_type
  lab_order_type = lab_order_type
  parent_department_id: string;
  parent_department: string;
  department: string;
  medicalCheckupServiceDetail: MedicalCheckupServiceDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MedicalCheckupServiceEditComponent>,
              public  persist: MedicalCheckupServicePersist,
              public feePersist: FeePersist,
              public tcUtilsDate: TCUtilsDate,
              public otherServiceNav: OtherServicesNavigator,
              public procedureNavigator: ProcedureNavigator,
              public bedroomNavigator: BedroomNavigator,
              public physiotherapyNavigator: Physiotherapy_TypeNavigator,
              public labtestNavigator: Lab_TestNavigator,
              public labpanelNavigator: Lab_PanelNavigator,
              public departmentSpecialityNavigator: Department_SpecialtyNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("medical_checkup_service");
      this.title = this.appTranslation.getText("general","new") +  " " + "medical_checkup_service";
      this.medicalCheckupServiceDetail = new MedicalCheckupServiceDetail();
      //set necessary defaults
      this.medicalCheckupServiceDetail.medical_checkup_package_id = this.idMode.id;

    } else {
     this.tcAuthorization.requireUpdate("medical_checkup_service");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "medical_checkup_service";
      this.isLoadingResults = true;
      this.persist.getMedicalCheckupService(this.idMode.id).subscribe(medicalCheckupServiceDetail => {
        this.medicalCheckupServiceDetail = medicalCheckupServiceDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addMedicalCheckupService(this.medicalCheckupServiceDetail).subscribe(value => {
      this.tcNotification.success("medicalCheckupService added");
      this.medicalCheckupServiceDetail.id = value.id;
      this.dialogRef.close(this.medicalCheckupServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateMedicalCheckupService(this.medicalCheckupServiceDetail).subscribe(value => {
      this.tcNotification.success("medical_checkup_service updated");
      this.dialogRef.close(this.medicalCheckupServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.medicalCheckupServiceDetail == null){
            return false;
          }

if (this.medicalCheckupServiceDetail.medical_checkup_package_id == null || this.medicalCheckupServiceDetail.medical_checkup_package_id  == "") {
            return false;
        }
if (this.medicalCheckupServiceDetail.service_type == null) {
            return false;
        }
if (this.medicalCheckupServiceDetail.target_id == null || this.medicalCheckupServiceDetail.target_id  == "") {
            return false;
        }
 return true;

 }


searchOtherService(){
  let dialogRef = this.otherServiceNav.pickOtherServicess(null,true);
  dialogRef.afterClosed().subscribe((result: OtherServicesSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

searchProcedure() {
  let dialogRef = this.procedureNavigator.pickProcedures(true);
  dialogRef.afterClosed().subscribe((result: ProcedureSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

searchBedrooms() {
  let dialogRef = this.bedroomNavigator.pickBedrooms(true);
  dialogRef.afterClosed().subscribe((result: BedroomSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

searchPhysiotherapy() {
  let dialogRef = this.physiotherapyNavigator.pickPhysiotherapy_Types(true);
  dialogRef.afterClosed().subscribe((result: Physiotherapy_TypeSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

searchLabTest(lab_order_type: number) {
  let dialogRef = this.labtestNavigator.pickLab_Tests(true, null, lab_order_type);
  dialogRef.afterClosed().subscribe((result: Lab_TestSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

searchLabPanel(lab_order_type: number) {
  let dialogRef = this.labpanelNavigator.pickLab_Panels(true, lab_order_type);
  dialogRef.afterClosed().subscribe((result: Lab_PanelSummary[]) => {
    if (result) {
      this.Name = result[0].name;
      this.medicalCheckupServiceDetail.target_id = result[0].id;
    }
  });
}

updateTarget(event: number){
  if (event == fee_type.card){
    this.medicalCheckupServiceDetail.target_id = this.tcUtilsString.invalid_id
  }
}

searchDepartmentSpeciality(parent_id: string) {
  let dialogRef = this.departmentSpecialityNavigator.pickDepartment_Specialtys(true, parent_id);
  dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
    if (result) {
      if(parent_id == this.tcUtilsString.invalid_id){
      this.parent_department_id = result[0].id;
      this.parent_department = result[0].name
      this.medicalCheckupServiceDetail.target_id = result[0].id;
      this.department = null
      } else {
      this.medicalCheckupServiceDetail.target_id = result[0].id;
      this.department = result[0].name
    }
  }
  });
}
 }
