import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupServiceListComponent } from './medical-checkup-service-list.component';

describe('MedicalCheckupServiceListComponent', () => {
  let component: MedicalCheckupServiceListComponent;
  let fixture: ComponentFixture<MedicalCheckupServiceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupServiceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
