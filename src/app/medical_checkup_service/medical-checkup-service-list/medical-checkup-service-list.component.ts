import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MedicalCheckupServiceDetail, MedicalCheckupServiceSummary, MedicalCheckupServiceSummaryPartialList } from '../medical_checkup_service.model';
import { MedicalCheckupServicePersist } from '../medical_checkup_service.persist';
import { MedicalCheckupServiceNavigator } from '../medical_checkup_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FeePersist } from 'src/app/fees/fee.persist';
import { MedicalCheckupPackagePersist } from 'src/app/medical_checkup_package/medical_checkup_package.persist';
import { fee_type } from 'src/app/app.enums';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';
import { MedicalCheckupPackageDetail } from 'src/app/medical_checkup_package/medical_checkup_package.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
@Component({
  selector: 'app-medical_checkup_service-list',
  templateUrl: './medical-checkup-service-list.component.html',
  styleUrls: ['./medical-checkup-service-list.component.scss']
})export class MedicalCheckupServiceListComponent implements OnInit {
  medicalCheckupServicesData: MedicalCheckupServiceSummary[] = [];
  medicalCheckupServicesTotalCount: number = 0;
  medicalCheckupServiceSelectAll:boolean = false;
  medicalCheckupServiceSelection: MedicalCheckupServiceSummary[] = [];
  total_price: number = 0;
 medicalCheckupServicesDisplayedColumns: string[] = ["select","action","service_type","target_id" ];
  medicalCheckupServiceSearchTextBox: FormControl = new FormControl();
  medicalCheckupServiceIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) medicalCheckupServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) medicalCheckupServicesSort: MatSort;
  @Input() medicalCheckupPackageId: string;
  fetchTarget: {[id: number] : (element: MedicalCheckupServiceDetail) => any}
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public medicalCheckupServicePersist: MedicalCheckupServicePersist,
                public medicalCheckupServiceNavigator: MedicalCheckupServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public medicalCheckupPackagePersist: MedicalCheckupPackagePersist,
                public feePersist: FeePersist,
                public procedurePersist: ProcedurePersist,
                public lab_testPersist: Lab_TestPersist,
                public lab_panelPersist: Lab_PanelPersist,
                public physio_therapyPerist: Physiotherapy_TypePersist,
                public tcUtilsString: TCUtilsString,
                public departmentSpecialityPersist: Department_SpecialtyPersist,

    ) {

        this.tcAuthorization.requireRead("medical_checkup_services");
       this.medicalCheckupServiceSearchTextBox.setValue(medicalCheckupServicePersist.medicalCheckupServiceSearchText);
      //delay subsequent keyup events
      this.medicalCheckupServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.medicalCheckupServicePersist.medicalCheckupServiceSearchText = value;
        this.searchMedicalCheckupServices();
      });
      this.fetchTarget = {
        [fee_type.procedure] : this.getProcedure,
        [fee_type.lab_test]: this.getLabTest,
        [fee_type.lab_panel]: this.getLabPanel,
        [fee_type.rad_test]: this.getLabTest,
        [fee_type.rad_panel]: this.getLabPanel,
        [fee_type.pat_test]: this.getLabTest,
        [fee_type.pat_panel]: this.getLabPanel,
        [fee_type.physio_theraphy]: this.getPhysiotherapy,
        [fee_type.card]: this.getDoctor,
        // [fee_type.dialysis]: this.getDialysis
      }
    } 
    ngOnInit() {
      this.medicalCheckupServicePersist.medicalCheckupPackageId = this.medicalCheckupPackageId
      this.medicalCheckupServicesSort.sortChange.subscribe(() => {
        this.medicalCheckupServicesPaginator.pageIndex = 0;
        this.searchMedicalCheckupServices(true);
      });

      this.medicalCheckupServicesPaginator.page.subscribe(() => {
        this.searchMedicalCheckupServices(true);
      });
      //start by loading items
      this.searchMedicalCheckupServices();
    }

  searchMedicalCheckupServices(isPagination:boolean = false): void {
    let paginator = this.medicalCheckupServicesPaginator;
    let sorter = this.medicalCheckupServicesSort;

    this.medicalCheckupServiceIsLoading = true;
    this.medicalCheckupServiceSelection = [];
    this.getTotalFee()
    this.medicalCheckupServicePersist.searchMedicalCheckupService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MedicalCheckupServiceSummaryPartialList) => {
      this.medicalCheckupServicesData = partialList.data;
      this.medicalCheckupServicesData.forEach(
        data => this.fetchTarget[data.service_type](data)
      )
      if (partialList.total != -1) {
        this.medicalCheckupServicesTotalCount = partialList.total;
      }
      this.medicalCheckupServiceIsLoading = false;
    }, error => {
      this.medicalCheckupServiceIsLoading = false;
    });

  } downloadMedicalCheckupServices(): void {
    if(this.medicalCheckupServiceSelectAll){
         this.medicalCheckupServicePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download medical_checkup_service", true);
      });
    }
    else{
        this.medicalCheckupServicePersist.download(this.tcUtilsArray.idsList(this.medicalCheckupServiceSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download medical_checkup_service",true);
            });
        }
  }
addMedicalCheckupService(): void {
    let dialogRef = this.medicalCheckupServiceNavigator.addMedicalCheckupService(this.medicalCheckupPackageId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMedicalCheckupServices();
      }
    });
  }

  editMedicalCheckupService(item: MedicalCheckupServiceSummary) {
    let dialogRef = this.medicalCheckupServiceNavigator.editMedicalCheckupService(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMedicalCheckupService(item: MedicalCheckupServiceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("medical_checkup_service");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.medicalCheckupServicePersist.deleteMedicalCheckupService(item.id).subscribe(response => {
          this.tcNotification.success("medical_checkup_service deleted");
          this.searchMedicalCheckupServices();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getTotalFee(){
      this.medicalCheckupPackagePersist.medicalCheckupPackageDo(this.medicalCheckupPackageId, "get_package_fee", {}).subscribe(
        (data: {total: number}) => {
          this.total_price = data.total
        }
      )
    }

    getLabTest = (element: MedicalCheckupServiceDetail) => {
      this.lab_testPersist.getLab_Test(element.target_id).subscribe(
        data => {
          element.target_name = data.name;
        }
      )
    }

    getLabPanel = (element: MedicalCheckupServiceDetail) => {
      this.lab_panelPersist.getLab_Panel(element.target_id).subscribe(
        data => element.target_name = data.name
      )
    }

    // getDialysis(id: string, element: MedicalCheckupServiceDetail){
    //   this.dialysis
    // }

    getPhysiotherapy = (element: MedicalCheckupServiceDetail) => {
      this.physio_therapyPerist.getPhysiotherapy_Type(element.target_id).subscribe(
        data => element.target_name = data.name
      )
    }

    getProcedure = (element: MedicalCheckupServiceDetail) => {
      this.procedurePersist.getProcedure(element.target_id).subscribe(
        data => element.target_name = data.name
      )
    }

    getDoctor = (element: MedicalCheckupServiceDetail) => {
      element.target_id != this.tcUtilsString.invalid_id && this.departmentSpecialityPersist.getDepartment_Specialty(element.target_id).subscribe(
        data => element.target_name = data.name
      )
    }
}
