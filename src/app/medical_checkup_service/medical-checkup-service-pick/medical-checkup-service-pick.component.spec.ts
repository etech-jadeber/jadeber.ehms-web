import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalCheckupServicePickComponent } from './medical-checkup-service-pick.component';

describe('MedicalCheckupServicePickComponent', () => {
  let component: MedicalCheckupServicePickComponent;
  let fixture: ComponentFixture<MedicalCheckupServicePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalCheckupServicePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalCheckupServicePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
