import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MedicalCheckupServiceSummary, MedicalCheckupServiceSummaryPartialList } from '../medical_checkup_service.model';
import { MedicalCheckupServicePersist } from '../medical_checkup_service.persist';
import { MedicalCheckupServiceNavigator } from '../medical_checkup_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-medical_checkup_service-pick',
  templateUrl: './medical-checkup-service-pick.component.html',
  styleUrls: ['./medical-checkup-service-pick.component.scss']
})export class MedicalCheckupServicePickComponent implements OnInit {
  medicalCheckupServicesData: MedicalCheckupServiceSummary[] = [];
  medicalCheckupServicesTotalCount: number = 0;
  medicalCheckupServiceSelectAll:boolean = false;
  medicalCheckupServiceSelection: MedicalCheckupServiceSummary[] = [];

 medicalCheckupServicesDisplayedColumns: string[] = ["select", ,"medical_checkup_package_id","service_type","target_id" ];
  medicalCheckupServiceSearchTextBox: FormControl = new FormControl();
  medicalCheckupServiceIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) medicalCheckupServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) medicalCheckupServicesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public medicalCheckupServicePersist: MedicalCheckupServicePersist,
                public medicalCheckupServiceNavigator: MedicalCheckupServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<MedicalCheckupServicePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("medical_checkup_services");
       this.medicalCheckupServiceSearchTextBox.setValue(medicalCheckupServicePersist.medicalCheckupServiceSearchText);
      //delay subsequent keyup events
      this.medicalCheckupServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.medicalCheckupServicePersist.medicalCheckupServiceSearchText = value;
        this.searchMedical_checkup_services();
      });
    } ngOnInit() {
   
      this.medicalCheckupServicesSort.sortChange.subscribe(() => {
        this.medicalCheckupServicesPaginator.pageIndex = 0;
        this.searchMedical_checkup_services(true);
      });

      this.medicalCheckupServicesPaginator.page.subscribe(() => {
        this.searchMedical_checkup_services(true);
      });
      //start by loading items
      this.searchMedical_checkup_services();
    }

  searchMedical_checkup_services(isPagination:boolean = false): void {


    let paginator = this.medicalCheckupServicesPaginator;
    let sorter = this.medicalCheckupServicesSort;

    this.medicalCheckupServiceIsLoading = true;
    this.medicalCheckupServiceSelection = [];

    this.medicalCheckupServicePersist.searchMedicalCheckupService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MedicalCheckupServiceSummaryPartialList) => {
      this.medicalCheckupServicesData = partialList.data;
      if (partialList.total != -1) {
        this.medicalCheckupServicesTotalCount = partialList.total;
      }
      this.medicalCheckupServiceIsLoading = false;
    }, error => {
      this.medicalCheckupServiceIsLoading = false;
    });

  }
  markOneItem(item: MedicalCheckupServiceSummary) {
    if(!this.tcUtilsArray.containsId(this.medicalCheckupServiceSelection,item.id)){
          this.medicalCheckupServiceSelection = [];
          this.medicalCheckupServiceSelection.push(item);
        }
        else{
          this.medicalCheckupServiceSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.medicalCheckupServiceSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
