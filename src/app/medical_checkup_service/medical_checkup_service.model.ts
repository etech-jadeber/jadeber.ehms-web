import {TCId} from "../tc/models";

export class MedicalCheckupServiceSummary extends TCId {
    medical_checkup_package_id:string;
    service_type:number;
    target_id:string;
    target_name: string;
     
    }
    export class MedicalCheckupServiceSummaryPartialList {
      data: MedicalCheckupServiceSummary[];
      total: number;
    }
    export class MedicalCheckupServiceDetail extends MedicalCheckupServiceSummary {
    }
    
    export class MedicalCheckupServiceDashboard {
      total: number;
    }
    