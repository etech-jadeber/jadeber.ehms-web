import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {MedicalCheckupServiceEditComponent} from "./medical-checkup-service-edit/medical-checkup-service-edit.component";
import {MedicalCheckupServicePickComponent} from "./medical-checkup-service-pick/medical-checkup-service-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MedicalCheckupServiceNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  medicalCheckupServicesUrl(): string {
    return "/medical_checkup_services";
  }

  medicalCheckupServiceUrl(id: string): string {
    return "/medical_checkup_services/" + id;
  }

  viewMedicalCheckupServices(): void {
    this.router.navigateByUrl(this.medicalCheckupServicesUrl());
  }

  viewMedicalCheckupService(id: string): void {
    this.router.navigateByUrl(this.medicalCheckupServiceUrl(id));
  }

  editMedicalCheckupService(id: string): MatDialogRef<MedicalCheckupServiceEditComponent> {
    const dialogRef = this.dialog.open(MedicalCheckupServiceEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMedicalCheckupService(medicalPackageId: string): MatDialogRef<MedicalCheckupServiceEditComponent> {
    const dialogRef = this.dialog.open(MedicalCheckupServiceEditComponent, {
          data: new TCIdMode(medicalPackageId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMedicalCheckupServices(selectOne: boolean=false): MatDialogRef<MedicalCheckupServicePickComponent> {
      const dialogRef = this.dialog.open(MedicalCheckupServicePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
