import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {MedicalCheckupServiceDashboard, MedicalCheckupServiceDetail, MedicalCheckupServiceSummaryPartialList} from "./medical_checkup_service.model";
import { fee_type } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class MedicalCheckupServicePersist {
  fee_type: TCEnumTranslation[] = [
    new TCEnumTranslation(fee_type.card, this.appTranslation.getKey('financial', 'card_fee')),
    new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure')),
    new TCEnumTranslation(fee_type.lab_test, this.appTranslation.getKey('laboratory', 'lab_test_id')),
    new TCEnumTranslation(fee_type.lab_panel, this.appTranslation.getKey('laboratory', 'lab_panel_id')),
    new TCEnumTranslation(fee_type.rad_test, this.appTranslation.getKey('laboratory', 'rad_test')),
    new TCEnumTranslation(fee_type.rad_panel, this.appTranslation.getKey('laboratory', 'rad_panel')),
    new TCEnumTranslation(fee_type.pat_test, this.appTranslation.getKey('laboratory', 'pat_test')),
    new TCEnumTranslation(fee_type.pat_panel, this.appTranslation.getKey('laboratory', 'pat_panel')),
  ];
  medicalCheckupPackageId: string;
 medicalCheckupServiceSearchText: string = "";constructor(private http: HttpClient, private appTranslation:AppTranslation) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.medicalCheckupServiceSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchMedicalCheckupService(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MedicalCheckupServiceSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("medical_checkup_service", this.medicalCheckupServiceSearchText, pageSize, pageIndex, sort, order);
    if (this.medicalCheckupPackageId){
      url = TCUtilsString.appendUrlParameter(url, "medical_checkup_package_id", this.medicalCheckupPackageId)
    }
    return this.http.get<MedicalCheckupServiceSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_service/do", new TCDoParam("download_all", this.filters()));
  }

  medicalCheckupServiceDashboard(): Observable<MedicalCheckupServiceDashboard> {
    return this.http.get<MedicalCheckupServiceDashboard>(environment.tcApiBaseUri + "medical_checkup_service/dashboard");
  }

  getMedicalCheckupService(id: string): Observable<MedicalCheckupServiceDetail> {
    return this.http.get<MedicalCheckupServiceDetail>(environment.tcApiBaseUri + "medical_checkup_service/" + id);
  } addMedicalCheckupService(item: MedicalCheckupServiceDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_service/", item);
  }

  updateMedicalCheckupService(item: MedicalCheckupServiceDetail): Observable<MedicalCheckupServiceDetail> {
    return this.http.patch<MedicalCheckupServiceDetail>(environment.tcApiBaseUri + "medical_checkup_service/" + item.id, item);
  }

  deleteMedicalCheckupService(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "medical_checkup_service/" + id);
  }
 medicalCheckupServicesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "medical_checkup_service/do", new TCDoParam(method, payload));
  }

  medicalCheckupServiceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "medical_checkup_services/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_service/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "medical_checkup_service/" + id + "/do", new TCDoParam("print", {}));
  }


}
