import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicationAdminstrationChartDetailComponent } from './medication-adminstration-chart-detail.component';

describe('MedicationAdminstrationChartDetailComponent', () => {
  let component: MedicationAdminstrationChartDetailComponent;
  let fixture: ComponentFixture<MedicationAdminstrationChartDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationAdminstrationChartDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationAdminstrationChartDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
