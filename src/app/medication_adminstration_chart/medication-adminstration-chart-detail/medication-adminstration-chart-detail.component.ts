import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {MedicationAdminstrationChartDetail, MedicationAdminstrationChartSummary} from "../medication_adminstration_chart.model";
import {MedicationAdminstrationChartPersist} from "../medication_adminstration_chart.persist";
import {Medication_adminstration_chartNavigator} from "../medication_adminstration_chart.navigator";
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { PrescriptionsDetail } from 'src/app/form_encounters/form_encounter.model';
import { ItemPersist } from 'src/app/items/item.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';

export enum MedicationAdminstrationChartTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-medication-adminstration-chart-detail',
  templateUrl: './medication-adminstration-chart-detail.component.html',
  styleUrls: ['./medication-adminstration-chart-detail.component.css']
})
export class MedicationAdminstrationChartDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  medicationAdminstrationChartIsLoading:boolean = false;
  
  MedicationAdminstrationChartTabs: typeof MedicationAdminstrationChartTabs = MedicationAdminstrationChartTabs;
  activeTab: MedicationAdminstrationChartTabs = MedicationAdminstrationChartTabs.overview;
  //basics
  medicationAdminstrationChartDetail: MedicationAdminstrationChartDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public medicationAdminstrationChartNavigator: Medication_adminstration_chartNavigator,
              public  medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
              public prescriptionPersist: Form_EncounterPersist,
              public itemPersist: ItemPersist,
              public userPersist: UserPersist,) {
    this.tcAuthorization.requireRead("medication_adminstration_charts");
    this.medicationAdminstrationChartDetail = new MedicationAdminstrationChartDetail();
    this.medicationAdminstrationChartDetail.prescription_detail = new PrescriptionsDetail()
  } ngOnInit() {
    this.tcAuthorization.requireRead("medication_adminstration_charts");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.medicationAdminstrationChartIsLoading = true;
    this.medicationAdminstrationChartPersist.getMedicationAdminstrationChart(id).subscribe(medicationAdminstrationChartDetail => {
          this.medicationAdminstrationChartDetail = medicationAdminstrationChartDetail;
          this.prescriptionPersist.getPrescriptions(this.medicationAdminstrationChartDetail.prescription_id).subscribe(
            prescription => {
              this.medicationAdminstrationChartDetail.prescription_detail = prescription
              this.itemPersist.getItem(prescription.drug_id).subscribe(
                item => {
                  prescription.drug_name = item.name
                }
              )
              this.userPersist.getUser(medicationAdminstrationChartDetail.given_by).subscribe(
                user => {
                  this.medicationAdminstrationChartDetail.given_by_name = user.name
                }
              )
            }
          )
          this.medicationAdminstrationChartIsLoading = false;
        }, error => {
          console.error(error);
          this.medicationAdminstrationChartIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.medicationAdminstrationChartDetail.id);
  }
  editMedicationAdminstrationChart(): void {
    let modalRef = this.medicationAdminstrationChartNavigator.editMedication_adminstration_chart(this.medicationAdminstrationChartDetail.id);
    modalRef.afterClosed().subscribe(medicationAdminstrationChartDetail => {
      TCUtilsAngular.assign(this.medicationAdminstrationChartDetail, medicationAdminstrationChartDetail);
    }, error => {
      console.error(error);
    });
  }

  //  printBirth():void{
  //     this.medicationAdminstrationChartPersist.print(this.medicationAdminstrationChartDetail.id).subscribe(printJob => {
  //       this.jobPersist.followJob(printJob.id, "print medication_adminstration_chart", true);
  //     });
  //   }

  back():void{
      this.location.back();
    }

}