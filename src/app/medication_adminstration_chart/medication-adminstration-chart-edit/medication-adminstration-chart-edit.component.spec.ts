import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicationAdminstrationChartEditComponent } from './medication-adminstration-chart-edit.component';

describe('MedicationAdminstrationChartEditComponent', () => {
  let component: MedicationAdminstrationChartEditComponent;
  let fixture: ComponentFixture<MedicationAdminstrationChartEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationAdminstrationChartEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationAdminstrationChartEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
