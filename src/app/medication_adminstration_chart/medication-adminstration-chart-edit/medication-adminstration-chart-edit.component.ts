import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { MedicationAdminstrationChartDetail } from '../medication_adminstration_chart.model';
import { MedicationAdminstrationChartPersist } from '../medication_adminstration_chart.persist';
import { T } from '@angular/cdk/keycodes';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Prescription } from 'src/app/nurse_record/nurse_record.model';
@Component({
  selector: 'app-medication_adminstration_chart-edit',
  templateUrl: './medication-adminstration-chart-edit.component.html',
  styleUrls: ['./medication-adminstration-chart-edit.component.css']
})export class MedicationAdminstrationChartEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  medicationAdminstrationChartDetail: MedicationAdminstrationChartDetail;
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MedicationAdminstrationChartEditComponent>,
              public  persist: MedicationAdminstrationChartPersist,
              public form_encounterPersist: Form_EncounterPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context.mode === TCModalModes.CASE;
  }

  isWizard(): boolean {
    return this.idMode.context.mode === TCModalModes.WIZARD;
  }
  
  ngOnInit() {
    if (this.isNew() || this.isWizard()) {
     this.tcAuthorization.requireCreate("medication_adminstration_charts");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "medication_adminstration_chart");
      this.medicationAdminstrationChartDetail = new MedicationAdminstrationChartDetail();
      this.medicationAdminstrationChartDetail.encounter_id = this.idMode.parentId;


      // this.medicationAdminstrationChartDetail.prescription_id = this.idMode.childId
      // this.form_encounterPersist.getPrescriptions(this.idMode.childId).subscribe((prescription)=>{
      //   if(prescription){
      //     this.medicationAdminstrationChartDetail.route = prescription.route;
      //     this.medicationAdminstrationChartDetail.drug_dose = prescription.dose;
      //   }        
      // })
      // this.medicationAdminstrationChartDetail.pain = -1;
      
      //set necessary defaults

    } 
    else {
     this.tcAuthorization.requireUpdate("medication_adminstration_charts");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + this.appTranslation.getText("patient", "medication_adminstration_chart");
      this.isLoadingResults = true;
      this.persist.getMedicationAdminstrationChart(this.idMode.parentId).subscribe(medicationAdminstrationChartDetail => {
        this.medicationAdminstrationChartDetail = medicationAdminstrationChartDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addMedicationAdminstrationChart(this.idMode.parentId, this.medicationAdminstrationChartDetail).subscribe(value => {
      this.tcNotification.success("medicationAdminstrationChart added");
      this.medicationAdminstrationChartDetail.id = value.id;
      this.dialogRef.close(this.medicationAdminstrationChartDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateMedicationAdminstrationChart(this.medicationAdminstrationChartDetail).subscribe(value => {
      this.tcNotification.success("medication_adminstration_chart updated");
      this.dialogRef.close(this.medicationAdminstrationChartDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  

  onWizard(): void {
    this.dialogRef.close(this.medicationAdminstrationChartDetail)
  }
  
  canSubmit(): boolean{
        if (this.medicationAdminstrationChartDetail == null){
            return false;
        }
        
        if(this.medicationAdminstrationChartDetail.medical_administration == null || this.medicationAdminstrationChartDetail.medical_administration == ""){
          return false;
        }
          
        return true;
  }
 }