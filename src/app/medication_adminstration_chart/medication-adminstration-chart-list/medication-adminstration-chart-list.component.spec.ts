import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicationAdminstrationChartListComponent } from './medication-adminstration-chart-list.component';

describe('MedicationAdminstrationChartListComponent', () => {
  let component: MedicationAdminstrationChartListComponent;
  let fixture: ComponentFixture<MedicationAdminstrationChartListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationAdminstrationChartListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationAdminstrationChartListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
