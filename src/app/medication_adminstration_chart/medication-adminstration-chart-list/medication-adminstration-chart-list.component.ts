import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { InpatientPrescriptionWithMedication, MedicationAdminstrationChartSummary, MedicationAdminstrationChartSummaryPartialList } from '../medication_adminstration_chart.model';
import { MedicationAdminstrationChartPersist } from '../medication_adminstration_chart.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Medication_adminstration_chartNavigator } from '../medication_adminstration_chart.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Medical_CertificateSummaryPartialList, PrescriptionsDetail, PrescriptionsSummary, PrescriptionsSummaryPartialList } from 'src/app/form_encounters/form_encounter.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ItemPersist } from 'src/app/items/item.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-medication-adminstration-chart-list',
  templateUrl: './medication-adminstration-chart-list.component.html',
  styleUrls: ['./medication-adminstration-chart-list.component.css']
})

export class MedicationAdminstrationChartListComponent implements OnInit {
  medicationAdminstrationChartsData: MedicationAdminstrationChartSummary[] = [];
  medicationAdminstrationChartsTotalCount: number = 0;
  medicationAdminstrationChartSelectAll:boolean = false;
  medicationAdminstrationChartSelection: MedicationAdminstrationChartSummary[] = [];
  

 medicationAdminstrationChartsDisplayedColumns: string[] = ["select","action","date","medication_adminstration", "given_by"];
  medicationAdminstrationChartSearchTextBox: FormControl = new FormControl();
  medicationAdminstrationChartIsLoading: boolean = false;
  prescriptionWithMedication: InpatientPrescriptionWithMedication[] = [];
  active_prescription: string = "";
  
  @Input() encounterId: string;
  @Input() patientId: string;
  @Input() showToolBar: boolean = true;
  @Output() onResult = new EventEmitter<number>();

  user: {[id: string] : UserDetail} = {}
  date: FormControl = new FormControl({disabled: true, value: new Date()})

  @ViewChild(MatPaginator, {static: true}) medicationAdminstrationChartsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) medicationAdminstrationChartsSort: MatSort;  
  
  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
                public medicationAdminstrationChartNavigator: Medication_adminstration_chartNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public prescriptionPersist: Form_EncounterPersist,
                public tcUtilsString: TCUtilsString,
                public userPersist: UserPersist,
                public item_persist: ItemPersist,

    ) {

        this.tcAuthorization.requireRead("medication_adminstration_charts");
      //  this.medicationAdminstrationChartsSearchTextBox.setValue(medicationAdminstrationChartPersist.medicationAdminstrationChartSearchText);
      //delay subsequent keyup events
      this.medicationAdminstrationChartPersist.date = this.tcUtilsDate.toTimeStamp(new Date());
      this.medicationAdminstrationChartSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.medicationAdminstrationChartPersist.medicationAdminstrationChartSearchText = value;
        // this.searchMedication_adminstration_charts();
      });
      
      this.date.valueChanges.pipe().subscribe(value => {
        this.medicationAdminstrationChartPersist.date = this.tcUtilsDate.toTimeStamp(value._d)
        this.searchMedicationAdminstrationCharts();
      });
    } 
    
    ngOnInit() {
      this.medicationAdminstrationChartPersist.encounterId = this.encounterId
      this.medicationAdminstrationChartPersist.patientId = this.patientId  
      this.medicationAdminstrationChartsSort.sortChange.subscribe(() => {
        this.medicationAdminstrationChartsPaginator.pageIndex = 0;
        this.searchMedicationAdminstrationCharts(true);
      });

      this.medicationAdminstrationChartsPaginator.page.subscribe(() => {
        this.searchMedicationAdminstrationCharts(true);
      });

      this.medicationAdminstrationChartsSort.active = 'date';
      this.medicationAdminstrationChartsSort.direction = 'desc';
      //start by loading items
        this.searchMedicationAdminstrationCharts();
      
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.medicationAdminstrationChartPersist.encounterId = this.encounterId;
      } else {
        this.medicationAdminstrationChartPersist.encounterId = null;
      }
      this.searchMedicationAdminstrationCharts()
    }




  searchMedicationAdminstrationCharts(isPagination:boolean = false): void {
    let paginator = this.medicationAdminstrationChartsPaginator;
    let sorter = this.medicationAdminstrationChartsSort;

    // this.medicationAdminstrationChartIsLoading = true;
    this.medicationAdminstrationChartSelection = [];

    this.medicationAdminstrationChartPersist.searchMedicationAdminstrationChart(paginator.pageSize, isPagination? paginator.pageIndex:0, 'date_and_time', 'desc').subscribe((partialList: MedicationAdminstrationChartSummaryPartialList) => {
      this.medicationAdminstrationChartsData = partialList.data;
      this.medicationAdminstrationChartsData.forEach(
        medicationAdminstration => {
          if (medicationAdminstration.given_by && !this.user[medicationAdminstration.given_by]){
            this.user[medicationAdminstration.given_by] = new UserDetail()
            this.userPersist.getUser(medicationAdminstration.given_by).subscribe(
              user => {
                this.user[medicationAdminstration.given_by] = user;
              }
            )
          }
        }
      )

      if (partialList.total != -1) {
        this.medicationAdminstrationChartsTotalCount = partialList.total;
      }
      this.medicationAdminstrationChartIsLoading = false;
    }, error => {
      this.medicationAdminstrationChartIsLoading = false;
    });
  } 

  
  downloadMedicationAdminstrationCharts(): void {
    if(this.medicationAdminstrationChartSelectAll){
         this.medicationAdminstrationChartPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download medication_adminstration_chart", true);
      });
    }
    else{
        this.medicationAdminstrationChartPersist.download(this.tcUtilsArray.idsList(this.medicationAdminstrationChartSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download medication_adminstration_chart",true);
            });
        }
  }
addMedication_adminstration_chart(): void {
  
    let dialogRef = this.medicationAdminstrationChartNavigator.addMedication_adminstration_chart(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMedicationAdminstrationCharts();
      }
    });
  }

  editMedicationAdminstrationChart(item: MedicationAdminstrationChartSummary) {
    let dialogRef = this.medicationAdminstrationChartNavigator.editMedication_adminstration_chart(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }


  replaceValue(value: string): string{

    return value.replace(/\n/g, '<br />' )
  }

  deleteMedicationAdminstrationChart(item: MedicationAdminstrationChartSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("medication_adminstration_chart");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.medicationAdminstrationChartPersist.deleteMedicationAdminstrationChart(item.id).subscribe(response => {
          this.tcNotification.success("medication_adminstration_chart deleted");
          this.searchMedicationAdminstrationCharts()
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
  
  }