import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MedicationAdminstrationChartPickComponent } from './medication-adminstration-chart-pick.component';

describe('MedicationAdminstrationChartPickComponent', () => {
  let component: MedicationAdminstrationChartPickComponent;
  let fixture: ComponentFixture<MedicationAdminstrationChartPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationAdminstrationChartPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationAdminstrationChartPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
