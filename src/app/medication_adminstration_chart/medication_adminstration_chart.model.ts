import { Medical_CertificateSummaryPartialList, PrescriptionsDetail } from "../form_encounters/form_encounter.model";
import {TCId} from "../tc/models";

export class MedicationAdminstrationChartSummary extends TCId {
  date_and_time:number;
  patient_id:string;
  pain:number;
  drug_dose:number;
  given_by:string;
  route: number;
  prescription_id: string;
  medical_administration: string;
  encounter_id: string;
  given_by_name: string;
  prescription_detail: PrescriptionsDetail = new PrescriptionsDetail();
  }

  export class InpatientPrescriptionWithMedication{
      name: string;
      id: string;
      medication: MedicationAdminstrationChartSummary[];
      route: number;
      dose: number;
      item_id: string;
  }
  
  export class MedicationAdminstrationChartSummaryPartialList {
    data: MedicationAdminstrationChartSummary[];
    total: number;
  }

  export class MedicationAdminstrationChartDetail extends MedicationAdminstrationChartSummary {
  }
  
  export class MedicationAdminstrationChartDashboard {
    total: number;
  }