import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import {MedicationAdminstrationChartEditComponent} from "./medication-adminstration-chart-edit/medication-adminstration-chart-edit.component";
import {MedicationAdminstrationChartPickComponent} from "./medication-adminstration-chart-pick/medication-adminstration-chart-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Medication_adminstration_chartNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  medication_adminstration_chartsUrl(): string {
    return "/medication_adminstration_charts";
  }

  medication_adminstration_chartUrl(id: string): string {
    return "/medication_adminstration_charts/" + id;
  }

  viewMedication_adminstration_charts(): void {
    this.router.navigateByUrl(this.medication_adminstration_chartsUrl());
  }

  viewMedication_adminstration_chart(id: string): void {
    this.router.navigateByUrl(this.medication_adminstration_chartUrl(id));
  }

  editMedication_adminstration_chart(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(MedicationAdminstrationChartEditComponent, {
      data: new TCParentChildIds(id, "", {mode: TCModalModes.EDIT}),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMedication_adminstration_chart(encounterId: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(MedicationAdminstrationChartEditComponent, {
          data: new TCParentChildIds(encounterId, null, {mode: TCModalModes.NEW}),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  wizardMedication_adminstration_chart(encounterId: string, prescription_id: string): MatDialogRef<unknown,any>{
    const dialogRef = this.dialog.open(MedicationAdminstrationChartEditComponent, {
      data: new TCParentChildIds(encounterId, prescription_id, {mode: TCModalModes.WIZARD}),
      width: TCModalWidths.medium
});
return dialogRef;
  }
  
   pickMedication_adminstration_charts(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(MedicationAdminstrationChartPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}