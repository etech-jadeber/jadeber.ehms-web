import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {MedicationAdminstrationChartDashboard, MedicationAdminstrationChartDetail, MedicationAdminstrationChartSummaryPartialList} from "./medication_adminstration_chart.model";
import { TCUtilsString } from '../tc/utils-string';
import { MedicationRoute, PainStatus } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class MedicationAdminstrationChartPersist {
 medicationAdminstrationChartSearchText: string = "";
 prescription_id: string = "";
 encounterId: string;
 patientId: string;

 medicationRouteFilter: number ;

    MedicationRoute: TCEnum[] = [
     new TCEnum( MedicationRoute.Intramuscular, 'Intramuscular'),
  new TCEnum( MedicationRoute.Intravenous, 'Intravenous'),
  new TCEnum( MedicationRoute.Oral, 'Oral'),
  new TCEnum( MedicationRoute.Subcutaneous, 'Subcutaneous'),
  new TCEnum( MedicationRoute.Per_Rectum, 'Per Rectum'),
  new TCEnum( MedicationRoute.Per_Vaginal, 'Per vaginal'),
  new TCEnum( MedicationRoute.Sub_Lingual, 'Sub lingual'),
  new TCEnum( MedicationRoute.Nasogastric, 'Nasogastric'),
  new TCEnum( MedicationRoute.Intradermal, 'Intradermal'),
  new TCEnum( MedicationRoute.Intraperitoneal, 'Intraperitoneal'),
  new TCEnum( MedicationRoute.Intrathecal, 'Intrathecal'),
  new TCEnum( MedicationRoute.Intraosseous, 'Intraosseous'),
  new TCEnum( MedicationRoute.Topical, 'Topical'),
  new TCEnum( MedicationRoute.Nasal, 'Nasal'),
  new TCEnum( MedicationRoute.Inhalation, 'Inhalation'),

  ];

  painStatusFilter: number ;

    PainStatus: TCEnum[] = [
     new TCEnum( PainStatus.No_Pain, 'No Pain'),
  new TCEnum( PainStatus.Mild_Pain, 'Mild Pain'),
  new TCEnum( PainStatus.Moderate_Pain, 'Moderate Pain'),
  new TCEnum( PainStatus.Severe_Pain, 'Severe Pain'),
  new TCEnum( PainStatus.Worst_Pain, 'Worst Pain'),

  ];
  date: number;
 
 constructor(private http: HttpClient) {
  }
  filters(){

  }

  searchMedicationAdminstrationChart( pageSize: number, pageIndex: number, sort: string, order: string): Observable<MedicationAdminstrationChartSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("medication_adminstration_chart", this.medicationAdminstrationChartSearchText, pageSize, pageIndex, sort, order);
    if (this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "form_encounter_id", this.encounterId)
    }
    if (this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    }
    if(this.date){
        url = TCUtilsString.appendUrlParameter(url, "date", this.date.toString());
    }
    // if (this.prescription_id) {
    //   url = TCUtilsString.appendUrlParameter(url, "prescription_id", this.prescription_id);
    // }
    return this.http.get<MedicationAdminstrationChartSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medication_adminstration_chart/do", new TCDoParam("download_all", this.filters()));
  }
  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "medication_adminstration_chart/do", new TCDoParam("download", ids));

}
  medicationAdminstrationChartDashboard(): Observable<MedicationAdminstrationChartDashboard> {
    return this.http.get<MedicationAdminstrationChartDashboard>(environment.tcApiBaseUri + "medication_adminstration_chart/dashboard");
  }

  getMedicationAdminstrationChart(id: string): Observable<MedicationAdminstrationChartDetail> {
    return this.http.get<MedicationAdminstrationChartDetail>(environment.tcApiBaseUri + "medication_adminstration_chart/" + id);
  }
  
  addMedicationAdminstrationChart(parentId: string, item: MedicationAdminstrationChartDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/medication_adminstration_chart", item);
  }

  updateMedicationAdminstrationChart(item: MedicationAdminstrationChartDetail): Observable<MedicationAdminstrationChartDetail> {
    return this.http.patch<MedicationAdminstrationChartDetail>(environment.tcApiBaseUri + "medication_adminstration_chart/" + item.id, item);
  }

  deleteMedicationAdminstrationChart(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "medication_adminstration_chart/" + id);
  }

}