import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MessagesEditComponent } from './messages-edit.component';

describe('MessagesEditComponent', () => {
  let component: MessagesEditComponent;
  let fixture: ComponentFixture<MessagesEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
