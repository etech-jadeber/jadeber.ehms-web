import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {MessagesDetail} from "../messages.model";
import {MessagesPersist} from "../messages.persist";
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { UserNavigator } from 'src/app/tc/users/user.navigator';
import { UserDetail } from 'src/app/tc/users/user.model';
import { PatientDetail } from 'src/app/patients/patients.model';


@Component({
  selector: 'app-messages-edit',
  templateUrl: './messages-edit.component.html',
  styleUrls: ['./messages-edit.component.css']
})
export class MessagesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  messagesDetail: MessagesDetail;
  patientName:string = "";
  userName:string = "";

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              private userNavigator:UserNavigator,
            private patientNavigator:PatientNavigator,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MessagesEditComponent>,
              public  persist: MessagesPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("messages");
      this.title = this.appTranslation.getText("general","new") +  " Messages";
      this.messagesDetail = new MessagesDetail();
      this.messagesDetail.about = this.tcUtilsString.invalid_id;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("messages");
      this.title = this.appTranslation.getText("general","edit") +  " Messages";
      this.isLoadingResults = true;
      this.persist.getMessages(this.idMode.id).subscribe(messagesDetail => {
        this.messagesDetail = messagesDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addMessages(this.messagesDetail).subscribe(value => {
      this.tcNotification.success("Messages added");
      this.messagesDetail.id = value.id;
      this.dialogRef.close(this.messagesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateMessages(this.messagesDetail).subscribe(value => {
      this.tcNotification.success("Messages updated");
      this.dialogRef.close(this.messagesDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.messagesDetail == null){
            return false;
          }

        if (this.messagesDetail.message == null || this.messagesDetail.message  == "") {
                      return false;
                    }


        return true;
      }
searchUser():void{
  let dialogRef =  this.userNavigator.pickUsers(true);
  dialogRef.afterClosed().subscribe(
    (res:UserDetail[])=>{
      this.messagesDetail.message_to = res[0].id;
      this.userName = res[0].name;
    }
  )
}

searchPatient():void{
let dialogRef =  this.patientNavigator.pickPatients(true);
  dialogRef.afterClosed().subscribe(
    (res:PatientDetail[])=>{
      this.messagesDetail.about = res[0].uuidd;
      this.patientName = res[0].fname + " " + res[0].mname + " " + res[0].lname;

    }
  )
}

}
