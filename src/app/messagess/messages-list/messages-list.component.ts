import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {MessagesPersist} from "../messages.persist";
import {MessagesNavigator} from "../messages.navigator";
import {MessagesDetail, MessagesSummary, MessagesSummaryPartialList} from "../messages.model";


@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.css']
})
export class MessagesListComponent implements OnInit {

  messagessData: MessagesSummary[] = [];
  messagessTotalCount: number = 0;
  messagessSelectAll:boolean = false;
  messagessSelection: MessagesSummary[] = [];

  messagessDisplayedColumns: string[] = ["select","action", "from","to","about","message","status","type", ];
  messagessSearchTextBox: FormControl = new FormControl();
  messagessIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) messagessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) messagessSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public messagesPersist: MessagesPersist,
                public messagesNavigator: MessagesNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("messages");
       this.messagessSearchTextBox.setValue(messagesPersist.messagesSearchText);
      //delay subsequent keyup events
      this.messagessSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.messagesPersist.messagesSearchText = value;
        this.searchMessagess();
      });
    }

    ngOnInit() {

      this.messagessSort.sortChange.subscribe(() => {
        this.messagessPaginator.pageIndex = 0;
        this.searchMessagess(true);
      });

      this.messagessPaginator.page.subscribe(() => {
        this.searchMessagess(true);
      });
      //start by loading items
      this.searchMessagess();
    }

  searchMessagess(isPagination:boolean = false): void {


    let paginator = this.messagessPaginator;
    let sorter = this.messagessSort;

    this.messagessIsLoading = true;
    this.messagessSelection = [];

    this.messagesPersist.searchMessages(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: MessagesSummaryPartialList) => {
      this.messagessData = partialList.data;
      if (partialList.total != -1) {
        this.messagessTotalCount = partialList.total;
      }
      this.messagessIsLoading = false;
    }, error => {
      this.messagessIsLoading = false;
    });

  }

  downloadMessagess(): void {
    if(this.messagessSelectAll){
         this.messagesPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download messagess", true);
      });
    }
    else{
        this.messagesPersist.download(this.tcUtilsArray.idsList(this.messagessSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download messagess",true);
            });
        }
  }



  addMessages(): void {
    let dialogRef = this.messagesNavigator.addMessages();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMessagess();
      }
    });
  }

  editMessages(item: MessagesSummary) {
    let dialogRef = this.messagesNavigator.editMessages(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMessages(item: MessagesSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Messages");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.messagesPersist.deleteMessages(item.id).subscribe(response => {
          this.tcNotification.success("Messages deleted");
          this.searchMessagess();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
