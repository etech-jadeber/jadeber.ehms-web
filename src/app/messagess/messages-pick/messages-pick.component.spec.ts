import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MessagesPickComponent } from './messages-pick.component';

describe('MessagesPickComponent', () => {
  let component: MessagesPickComponent;
  let fixture: ComponentFixture<MessagesPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
