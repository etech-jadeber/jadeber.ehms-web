import { TCId } from "../tc/models";

export class MessagesSummary extends TCId {
    message_from : string;
  message_to : string;
  about : string;
  message : string;
  status : number;
  type : number;
  doctor_from:string = "";
  doctor_to:string = "";
  about_name:string = "";
  }
  
  export class MessagesSummaryPartialList {
    data: MessagesSummary[];
    total: number;
  }
  
  export class MessagesDetail extends MessagesSummary {
 
  }
  
  export class MessagesDashboard {
    total: number;
  }