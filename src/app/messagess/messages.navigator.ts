import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {MessagesEditComponent} from "./messages-edit/messages-edit.component";
import {MessagesPickComponent} from "./messages-pick/messages-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MessagesNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  messagessUrl(): string {
    return "/messages";
  }

  messagesUrl(id: string): string {
    return "/messages/" + id;
  }

  viewMessagess(): void {
    this.router.navigateByUrl(this.messagessUrl());
  }

  viewMessages(id: string): void {
    this.router.navigateByUrl(this.messagesUrl(id));
  }

  editMessages(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(MessagesEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMessages(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(MessagesEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickMessagess(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(MessagesPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
