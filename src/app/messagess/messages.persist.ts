import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {MessagesDashboard, MessagesDetail, MessagesSummaryPartialList} from "./messages.model";
import { message_status, message_type } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class MessagesPersist {

  messagesSearchText: string = "";

  constructor(private http: HttpClient,private appTranslation:AppTranslation) {
  }

  searchMessages(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MessagesSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("messagess", this.messagesSearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "status", this.message_statuId.toString());
    return this.http.get<MessagesSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.messagesSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "messagess/do", new TCDoParam("download_all", this.filters()));
  }

  messagesDashboard(): Observable<MessagesDashboard> {
    return this.http.get<MessagesDashboard>(environment.tcApiBaseUri + "messagess/dashboard");
  }

  getMessages(id: string): Observable<MessagesDetail> {
    return this.http.get<MessagesDetail>(environment.tcApiBaseUri + "messagess/" + id);
  }

  addMessages(item: MessagesDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "messagess/", item);
  }

  updateMessages(item: MessagesDetail): Observable<MessagesDetail> {
    return this.http.patch<MessagesDetail>(environment.tcApiBaseUri + "messagess/" + item.id, item);
  }

  deleteMessages(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "messagess/" + id);
  }

  messagessDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "messagess/do", new TCDoParam(method, payload));
  }

  messagesDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "messagess/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "messagess/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "messagess/" + id + "/do", new TCDoParam("print", {}));
  }


  message_typeId: number ;
    
    message_type: TCEnumTranslation[] = [
    new TCEnumTranslation(message_type.pharmacy, this.appTranslation.getKey('inventory', 'pharmacy')),
    new TCEnumTranslation(message_type.bill_collect, this.appTranslation.getKey('patient', 'bill_collect')),
    new TCEnumTranslation(message_type.priority, this.appTranslation.getKey('patient', 'priority')),
    new TCEnumTranslation(message_type.lab_result, this.appTranslation.getKey('procedure', 'lab_results')),
  ];

  message_statuId: number  =  message_status.new;
    
    message_status: TCEnumTranslation[] = [
    new TCEnumTranslation(message_status.new, this.appTranslation.getKey('general', 'new')),
    new TCEnumTranslation(message_status.done, this.appTranslation.getKey('general', 'done')),
    new TCEnumTranslation(message_status.read, this.appTranslation.getKey('general', 'read')),
    new TCEnumTranslation(message_status.forwarded, this.appTranslation.getKey('general', 'forwarded')),
  ];

}
