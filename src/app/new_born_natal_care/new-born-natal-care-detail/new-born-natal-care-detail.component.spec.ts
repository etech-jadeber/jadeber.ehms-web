import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBornNatalCareDetailComponent } from './new-born-natal-care-detail.component';

describe('NewBornNatalCareDetailComponent', () => {
  let component: NewBornNatalCareDetailComponent;
  let fixture: ComponentFixture<NewBornNatalCareDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBornNatalCareDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBornNatalCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
