import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {NewBornNatalCareDetail} from "../new_born_natal_care.model";
import {NewBornNatalCarePersist} from "../new_born_natal_care.persist";
import {NewBornNatalCareNavigator} from "../new_born_natal_care.navigator";
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { encounter_status } from 'src/app/app.enums';

export enum NewBornNatalCareTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-new-born-natal-care-detail',
  templateUrl: './new-born-natal-care-detail.component.html',
  styleUrls: ['./new-born-natal-care-detail.component.scss']
})
export class NewBornNatalCareDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  newBornNatalCareIsLoading:boolean = false;
  
  NewBornNatalCareTabs: typeof NewBornNatalCareTabs = NewBornNatalCareTabs;
  activeTab: NewBornNatalCareTabs = NewBornNatalCareTabs.overview;
  //basics
  newBornNatalCareDetail: NewBornNatalCareDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public newBornNatalCareNavigator: NewBornNatalCareNavigator,
              public form_encounterPersist: Form_EncounterPersist,
              public  newBornNatalCarePersist: NewBornNatalCarePersist,
              public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,
              ) {
    this.tcAuthorization.requireRead("new_born_natal_cares");
    this.newBornNatalCareDetail = new NewBornNatalCareDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("new_born_natal_cares");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.newBornNatalCareIsLoading = true;
    this.newBornNatalCarePersist.getNewBornNatalCare(id).subscribe(newBornNatalCareDetail => {
          this.newBornNatalCareDetail = newBornNatalCareDetail;
          if(this.form_encounterPersist.encounter_is_active == null){
            this.form_encounterPersist.getForm_Encounter(newBornNatalCareDetail.encounter_id).subscribe((encounter)=>{
              if(encounter){
                this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
              }
            })
          }
          this.newBornNatalCareIsLoading = false;
        }, error => {
          console.error(error);
          this.newBornNatalCareIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.newBornNatalCareDetail.id);
  }
  editNewBornNatalCare(): void {
    let modalRef = this.newBornNatalCareNavigator.editNewBornNatalCare(this.newBornNatalCareDetail.id);
    modalRef.afterClosed().subscribe(newBornNatalCareDetail => {
      TCUtilsAngular.assign(this.newBornNatalCareDetail, newBornNatalCareDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.newBornNatalCarePersist.print(this.newBornNatalCareDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print new_born__natal_care", true);
      });
    }

  back():void{
      this.location.back();
    }

}
