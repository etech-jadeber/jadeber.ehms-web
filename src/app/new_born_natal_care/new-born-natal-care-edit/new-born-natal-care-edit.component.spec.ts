import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBornNatalCareEditComponent } from './new-born-natal-care-edit.component';

describe('NewBornNatalCareEditComponent', () => {
  let component: NewBornNatalCareEditComponent;
  let fixture: ComponentFixture<NewBornNatalCareEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBornNatalCareEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBornNatalCareEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
