import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes, TCParentChildIds } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { NewBornNatalCareDetail } from '../new_born_natal_care.model';
import { NewBornNatalCarePersist } from '../new_born_natal_care.persist';
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';
@Component({
  selector: 'app-new-born-natal_care-edit',
  templateUrl: './new-born-natal-care-edit.component.html',
  styleUrls: ['./new-born-natal-care-edit.component.scss'],
})
export class NewBornNatalCareEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  newBornNatalCareDetail: NewBornNatalCareDetail;
  dateTime: string;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<NewBornNatalCareEditComponent>,
    public persist: NewBornNatalCarePersist,
    public tCUtilsDate: TCUtilsDate,
    public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,

    @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }

  dateAndTime(timedate: Date): [string, string] {
    const date = `${timedate.getFullYear()}-${(
      '0' +
      (timedate.getMonth() + 1)
    ).slice(-2)}-${('0' + timedate.getDate()).slice(-2)}`;
    const time = `${('0' + timedate.getHours()).slice(-2)}:${(
      '0' + timedate.getMinutes()
    ).slice(-2)}`;
    return [date, time];
  }

  ngOnInit() {
    const today = new Date();
    const [date, time] = this.dateAndTime(today);
    this.dateTime = `${date}`;
    if (this.idMode.context['mode'] == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('new_born_natal_cares');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'NeoNatal Care';
      this.newBornNatalCareDetail = new NewBornNatalCareDetail();
      this.newBornNatalCareDetail.new_born_natal_visit = this.idMode.context['visits'] + 1;
      this.newBornNatalCareDetail.baby_breathing = "";
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('new_born_natal_care');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'NeoNatal Care';
      this.isLoadingResults = true;
      this.transformDates(false);
      this.persist.getNewBornNatalCare(this.idMode.childId).subscribe(
        (newBornNatalCareDetail) => {
          this.newBornNatalCareDetail = newBornNatalCareDetail;
          const [date, time] = this.dateAndTime(
            new Date(this.newBornNatalCareDetail.date_checked * 1000)
          );
          this.dateTime = `${date}`;
          this.isLoadingResults = false;
      },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addNewBornNatalCare(this.idMode.parentId,this.newBornNatalCareDetail).subscribe(
      (value) => {
        this.tcNotification.success('NeoNatalCare added');
        this.newBornNatalCareDetail.id = value.id;
        this.dialogRef.close(this.newBornNatalCareDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateNewBornNatalCare(this.newBornNatalCareDetail).subscribe(
      (value) => {
        this.tcNotification.success('neo_natal_care updated');
        this.dialogRef.close(this.newBornNatalCareDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.newBornNatalCareDetail.date_checked =
        new Date(this.dateTime).getTime() / 1000;
    } else {
      // this.newBornNatalCareDetail.date_checked = this.tCUtilsDate.toDate(this.newBornNatalCareDetail.date_checked);
    }
  }

  canSubmit(): boolean {
    if (this.newBornNatalCareDetail == null) {
      return false;
    }

    if (this.newBornNatalCareDetail.new_born_natal_visit == -1) {
      return false;
    }
    if (
      this.newBornNatalCareDetail.baby_breathing == null ||
      this.newBornNatalCareDetail.baby_breathing == ''
    ) {
      return false;
    }
    // if (this.newBornNatalCareDetail.bp == null) {
    //   return false;
    // }
    // if (this.newBornNatalCareDetail.tpr == null) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.uterus_contracted_look_for_pph == null ||
    //   this.newBornNatalCareDetail.uterus_contracted_look_for_pph == ''
    // ) {
    //   return false;
    // }
    // if (this.newBornNatalCareDetail.date_checked == null) {
    //   return false;
    // }
    // if (this.newBornNatalCareDetail.temp_value == null) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.dribbling_leaking_urine == null ||
    //   this.newBornNatalCareDetail.dribbling_leaking_urine == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.anemia == null ||
    //   this.newBornNatalCareDetail.anemia == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.vaginal_discharge == null ||
    //   this.newBornNatalCareDetail.vaginal_discharge == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.pelvic_exam == null ||
    //   this.newBornNatalCareDetail.pelvic_exam == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.newBornNatalCareDetail.breast == null ||
    //   this.newBornNatalCareDetail.breast == ''
    // ) {
    //   return false;
    // }
    return true;
  }
}
