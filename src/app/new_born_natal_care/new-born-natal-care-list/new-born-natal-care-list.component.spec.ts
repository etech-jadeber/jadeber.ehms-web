import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBornNatalCareListComponent } from './new-born-natal-care-list.component';

describe('NewBornNatalCareListComponent', () => {
  let component: NewBornNatalCareListComponent;
  let fixture: ComponentFixture<NewBornNatalCareListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBornNatalCareListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBornNatalCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
