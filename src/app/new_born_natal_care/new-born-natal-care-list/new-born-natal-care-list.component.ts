import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  NewBornNatalCareSummary,
  NewBornNatalCareSummaryPartialList,
} from '../new_born_natal_care.model';
import { NewBornNatalCarePersist } from '../new_born_natal_care.persist';
import { NewBornNatalCareNavigator } from '../new_born_natal_care.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-new_born_natal_care-list',
  templateUrl: './new-born-natal-care-list.component.html',
  styleUrls: ['./new-born-natal-care-list.component.scss'],
})
export class NewBornNatalCareListComponent implements OnInit {
  newBornNatalCaresData: NewBornNatalCareSummary[] = [];
  newBornNatalCaresTotalCount: number = 0;
  newBornNatalCareSelectAll: boolean = false;
  newBornNatalCareSelection: NewBornNatalCareSummary[] = [];
  newBornNatalCaresDisplayedColumns: string[] = [
    'action',
    'new_born_natal_visit',
    'date_checked',
    'baby_breathing',
    'baby_breast_feeding',
    'baby_weight',
    'immunization',
    'arv_px_for_newborn',
  ];
  newBornNatalCareSearchTextBox: FormControl = new FormControl();
  newBornNatalCareIsLoading: boolean = false;

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();
  
  @ViewChild(MatPaginator, { static: true })
  newBornNatalCaresPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) newBornNatalCaresSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public newBornNatalCarePersist: NewBornNatalCarePersist,
    public newBornNatalCareNavigator: NewBornNatalCareNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public form_encounterPersist: Form_EncounterPersist,
    public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,

  ) {
    this.tcAuthorization.requireRead('new_born_natal_cares');
    this.newBornNatalCareSearchTextBox.setValue(
      newBornNatalCarePersist.newBornNatalCareSearchText
    );
    //delay subsequent keyup events
    this.newBornNatalCareSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.newBornNatalCarePersist.newBornNatalCareSearchText = value;
        this.searchNewBornNatalCares();
      });
  }
  ngOnInit() {
this.newBornNatalCarePersist.patientId = this.patientId
    this.newBornNatalCaresSort.sortChange.subscribe(() => {
      this.newBornNatalCaresPaginator.pageIndex = 0;
      this.searchNewBornNatalCares(true);
    });

    this.newBornNatalCaresPaginator.page.subscribe(() => {
      this.searchNewBornNatalCares(true);
    });
    // start by loading items
    this.searchNewBornNatalCares();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.newBornNatalCarePersist.encounterId = this.encounterId;
    } else {
    this.newBornNatalCarePersist.encounterId = null;
    }
    this.searchNewBornNatalCares()
    }

  searchNewBornNatalCares(isPagination: boolean = false): void {
    let paginator = this.newBornNatalCaresPaginator;
    let sorter = this.newBornNatalCaresSort;

    this.newBornNatalCareIsLoading = true;
    this.newBornNatalCareSelection = [];

    this.newBornNatalCarePersist.searchNewBornNatalCare(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe(
      (partialList: NewBornNatalCareSummaryPartialList) => {
        this.newBornNatalCaresData = partialList.data;
        if (partialList.total != -1) {
          this.newBornNatalCaresTotalCount = partialList.total;
        }
        this.newBornNatalCareIsLoading = false;
      },
      (error) => {
        this.newBornNatalCareIsLoading = false;
      }
    );
  }
  downloadNewBornNatalCares(): void {
    if (this.newBornNatalCareSelectAll) {
      this.newBornNatalCarePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download new_born_natal_care',
          true
        );
      });
    } else {
      this.newBornNatalCarePersist
        .download(this.tcUtilsArray.idsList(this.newBornNatalCareSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download new_born_natal_care',
            true
          );
        });
    }
  }
  addNewBornNatalCare(): void {
    let dialogRef = this.newBornNatalCareNavigator.addNewBornNatalCare(this.encounterId,this.newBornNatalCaresTotalCount);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchNewBornNatalCares();
      }
    });
  }

  editNewBornNatalCare(item: NewBornNatalCareSummary) {
    let dialogRef = this.newBornNatalCareNavigator.editNewBornNatalCare(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteNewBornNatalCare(item: NewBornNatalCareSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('new_born_natal_care');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.newBornNatalCarePersist.deleteNewBornNatalCare(item.id).subscribe(
          (response) => {
            this.tcNotification.success('new_born_natal_care deleted');
            this.searchNewBornNatalCares();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
