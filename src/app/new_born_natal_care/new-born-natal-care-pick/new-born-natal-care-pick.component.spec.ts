import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBornNatalCarePickComponent } from './new-born-natal-care-pick.component';

describe('NewBornNatalCarePickComponent', () => {
  let component: NewBornNatalCarePickComponent;
  let fixture: ComponentFixture<NewBornNatalCarePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewBornNatalCarePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBornNatalCarePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
