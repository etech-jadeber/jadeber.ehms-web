import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TcDictionary, TCIdMode, TCModalModes, TCParentChildIds } from '../tc/models';

import { NewBornNatalCareEditComponent } from './new-born-natal-care-edit/new-born-natal-care-edit.component';
import { NewBornNatalCarePickComponent } from './new-born-natal-care-pick/new-born-natal-care-pick.component';

@Injectable({
  providedIn: 'root',
})
export class NewBornNatalCareNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  newBornNatalCaresUrl(): string {
    return '/new_born_natal_cares';
  }

  newBornNatalCareUrl(id: string): string {
    return '/new_born_natal_cares/' + id;
  }

  viewNewBornNatalCares(): void {
    this.router.navigateByUrl(this.newBornNatalCaresUrl());
  }

  viewNewBornNatalCare(id: string): void {
    this.router.navigateByUrl(this.newBornNatalCareUrl(id));
  }

  editNewBornNatalCare(id: string): MatDialogRef<NewBornNatalCareEditComponent> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = TCModalModes.EDIT;
    const dialogRef = this.dialog.open(NewBornNatalCareEditComponent, {
      data: new TCParentChildIds(null,id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addNewBornNatalCare(parentId:string,visits:number): MatDialogRef<NewBornNatalCareEditComponent> {
    let params: TcDictionary<any> = new TcDictionary();
    params["mode"] = TCModalModes.NEW;
    params["visits"] = visits;
    
    const dialogRef = this.dialog.open(NewBornNatalCareEditComponent, {
      data: new TCParentChildIds(parentId, null,params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickNewBornNatalCares(
    selectOne: boolean = false
  ): MatDialogRef<NewBornNatalCarePickComponent> {
    const dialogRef = this.dialog.open(NewBornNatalCarePickComponent, {
      data: selectOne,
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }
}
