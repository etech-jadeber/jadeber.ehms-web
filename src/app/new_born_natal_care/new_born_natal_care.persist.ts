import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import { TCUtilsString } from '../tc/utils-string';
import {
  NewBornNatalCareDashboard,
  NewBornNatalCareDetail,
  NewBornNatalCareSummaryPartialList,
} from './new_born_natal_care.model';
import { PostNatalCareStatus, PostNatalVisit } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class NewBornNatalCarePersist {
  postNatalVisitFilter: number;
  patientId: string;
  encounterId: string;

  PostNatalVisit: TCEnum[] = [
    new TCEnum(PostNatalVisit.First_Visit, 'First Visit'),
    new TCEnum(PostNatalVisit.Second_Visit, 'Second Visit'),
    new TCEnum(PostNatalVisit.Third_Visit, 'Third Visit'),
  ];

  NewBornNatalCareStatusFilter: number;

  NewBornNatalCareStatus: TCEnum[] = [
    new TCEnum(PostNatalCareStatus.completed, 'Completed'),
    new TCEnum(PostNatalCareStatus.incomplete, 'Incomplete'),
  ];

  newBornNatalCareSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.newBornNatalCareSearchText;
    //add custom filters
    return fltrs;
  }

  searchNewBornNatalCare(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<NewBornNatalCareSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'new_born_natal_care',
      this.newBornNatalCareSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<NewBornNatalCareSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'new_born_natal_care/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  postNatalCareDashboard(): Observable<NewBornNatalCareDashboard> {
    return this.http.get<NewBornNatalCareDashboard>(
      environment.tcApiBaseUri + 'new_born_natal_care/dashboard'
    );
  }

  getNewBornNatalCare(id: string): Observable<NewBornNatalCareDetail> {
    return this.http.get<NewBornNatalCareDetail>(
      environment.tcApiBaseUri + 'new_born_natal_care/' + id
    );
  }
  addNewBornNatalCare(parentID:string,item: NewBornNatalCareDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'anc_history/'+ parentID +'/new_born_natal_care',
      item
    );
  }

  updateNewBornNatalCare(
    item: NewBornNatalCareDetail
  ): Observable<NewBornNatalCareDetail> {
    return this.http.patch<NewBornNatalCareDetail>(
      environment.tcApiBaseUri + 'new_born_natal_care/' + item.id,
      item
    );
  }

  deleteNewBornNatalCare(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'new_born_natal_care/' + id);
  }
  postNatalCaresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'new_born_natal_care/do',
      new TCDoParam(method, payload)
    );
  }

  postNatalCareDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'new_born_natal_cares/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'new_born_natal_care/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'new_born_natal_care/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
