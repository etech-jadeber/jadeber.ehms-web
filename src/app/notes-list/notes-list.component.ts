import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { PatientPersist } from '../patients/patients.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthentication } from '../tc/authentication';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { UserPersist } from '../tc/users/user.persist';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCAppInit } from '../tc/app-init';
import { PatientNotePersist } from '../patients/patientNote.persist';
import { PatientNoteSummary } from '../patients/patients.model';
import { UserDetail } from '../tc/users/user.model';


@Component({
  selector: 'app-notes-list',
  templateUrl: './notes-list.component.html',
  styleUrls: ['./notes-list.component.css']
})
export class NotesListComponent implements OnInit {
  notesData: PatientNoteSummary[] = [];
  notesTotalCount: number = 0;
  notesSelectAll: boolean = false;
  notesSelected: PatientNoteSummary[] = [];
  notesDisplayedColumns: string[] = ['select', 'action', 'comment_text'];
  notesSearchTextBox: FormControl = new FormControl();
  notesLoading: boolean = true;
  users: {[id: string]: UserDetail} = {}


  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public patientPersist: PatientPersist,
    public userPersist: UserPersist,
    public tcAuthentication: TCAuthentication,
    public patientNotePersist: PatientNotePersist,
  ) { 
    this.notesSearchTextBox.setValue(patientNotePersist.noteSearchText);
    this.notesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.patientNotePersist.noteSearchText = value.trim();
        this.searchNotes();
      });
  }

  ngOnInit(): void {
    this.tcAuthorization.requireRead("patient_notes")
    this.patientNotePersist.encounterId = this.encounterId;
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchNotes();
    });
    this.sorter.active = 'date';
    this.sorter.direction = 'desc';
  // prescriptionss paginator
  this.paginator.page.subscribe(() => {
    this.searchNotes();
  });
  this.searchNotes();
  }


  searchNotes(): void {
    let paginator = this.paginator;
    let sorter = this.sorter;
    this.notesLoading = true;
    this.notesLoading = true;
    this.patientNotePersist
      .searchPatientNote(
        paginator.pageSize,
        0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.notesData = response.data;
          this.notesData.forEach(note => {
            if (!this.users[note.provider_id]) {
              this.users[note.provider_id] = new UserDetail()
              this.userPersist.getUser(note.provider_id).subscribe(
                user => {
                  this.users[note.provider_id] = user;
                }
              )
            }
          })
          if (response.total != -1) {
            this.notesTotalCount = response.total;
          }
          this.notesLoading = false;
        },
        (error) => {}
      );
  }

  addNote(): void {
    let modalRef = this.tcNavigator.dateTextInput(
        this.appTranslation.getText('notes', 'note'),
        "",
        true
      );
    modalRef.afterClosed().subscribe((note) => {
      if (note) {
        const patientNote = new PatientNoteSummary()
        patientNote.encounter_id = this.encounterId;
        patientNote.patient_id = this.patientId;
        patientNote.note = note.text;
        patientNote.date = note.date;
        this.patientNotePersist
          .addPatientNote(patientNote)
          .subscribe((response) => {
            this.searchNotes();
          });
      }
    });
  }

  editNote(item: PatientNoteSummary): void {
    let modalRef = this.tcNavigator.dateTextInput(
      this.appTranslation.getText('notes', 'note'),
      item.note,
      true,
      item.date
    );
    modalRef.afterClosed().subscribe((note) => {
      if (note) {
        item.note = note.text;
        item.date = note.date
        this.patientNotePersist
          .updatePatientNote(item)
          .subscribe((response) => {
            this.searchNotes();
          });
      }
    });
  }

  canUpdateNote(item: PatientNoteSummary): boolean {
    return (
      this.tcAuthorization.isSysAdmin() ||
      item.provider_id == TCAppInit.userInfo.user_id
    );
  }

  deleteNote(item: PatientNoteSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Note');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.patientNotePersist
          .deletePatientNote(item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success('note deleted');
              this.searchNotes();
            },
            (error) => {}
          );
      }
    });
  }

  // archiveNote(item: CommentDetail): void {
  //   this.patientPersist
  //     .noteDo(this.encounterId, item.id, 'archive', {})
  //     .subscribe(
  //       (response) => {
  //         this.tcNotification.success('note archived');
  //         this.searchNotes();
  //       },
  //       (error) => {}
  //     );
  // }

  back(): void {
    this.location.back();
  }

  getUser(id: string): string {
    return (this.users[id]?.name || "Unknown User")
  }
}
