import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator, PageEvent} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../tc/authorization";
import {TCUtilsAngular} from "../tc/utils-angular";
import {TCUtilsArray} from "../tc/utils-array";
import {TCUtilsDate} from "../tc/utils-date";
import {TCNotification} from "../tc/notification";
import {TCNavigator} from "../tc/navigator";
import {JobPersist} from "../tc/jobs/job.persist";

import {AppTranslation} from "../app.translation";
import { NotificationDetail, NotificationSummary, NotificationSummaryPartialList } from './notification.model';
import { NotificationNavigator } from './notification.navigator';
import { NotificationPersist } from './notification.persist';
import { JobData } from '../tc/jobs/job.model';
import { FilePersist } from '../tc/files/file.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { AppComponent } from '../app.component';
import { TCUtilsHttp } from '../tc/utils-http';
// import { SocketService } from '../socketService';
@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-notification-list',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})

export class NotificationComponent implements OnInit {
  notificationsData: NotificationSummary[] = [];
  notificationsTotalCount: number = 0;
  notificationSelectAll:boolean = false;
  notificationSelection: NotificationSummary[] = [];
  length = 0;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;
  notificationDetail:NotificationDetail;

 notificationsDisplayedColumns: string[] = ["select","action" ,"msg" ];
  notificationSearchTextBox: FormControl = new FormControl();
  notificationIsLoading: boolean = false; 
  //  @ViewChild(MatPaginator, {static: true}) 
  // notificationsPaginator: MatPaginator;
  // @ViewChild(MatSort, {static: true}) notificationsSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public notificationPersist: NotificationPersist,
                public notificationNavigator: NotificationNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
    // public socketService: SocketService,
   



    ) {

        this.tcAuthorization.requireRead("notification");
       this.notificationSearchTextBox.setValue(notificationPersist.notificationSearchText);
      //delay subsequent keyup events
      this.notificationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.notificationPersist.notificationSearchText = value;
        this.searchNotifications();
      });
    } ngOnInit() {
      
    // ********************************************** socket notification ******************************
      
      // this.socketService.socket.on('groupMamber_update', (data) => {
      

      //     this.searchNotifications()
      //     this.notificationPersist.updateNotificationCount().subscribe((res)=>{
      //       console.log("updates successed")
              
      //     })
      
      // });
    // ********************************************** socket notification ******************************

      this.notificationDetail = new NotificationDetail()
   
      // this.notificationsSort.sortChange.subscribe(() => {
      //   this.pageIndex = 0;
      //   this.searchNotifications(true);
      // });

      // this.notificationsPaginator.page.subscribe(() => {
      //   this.searchNotifications(true);
      // });
      //start by loading items
      this.searchNotifications();
    }


    handlePageEvent(event: PageEvent) {
      this.length = event.length;
      this.pageSize = event.pageSize;
      this.pageIndex = event.pageIndex;
      console.log("handlePageEvent",event)
      this.notificationPersist.searchNotification(event.pageSize, event.pageIndex).subscribe((partialList: NotificationSummaryPartialList) => {
        this.notificationsData = partialList.data;
        console.log("this.notificationsData",partialList)
        if (partialList.total != -1) {
          this.length = partialList.total;
        }
        this.notificationIsLoading = false;
      }, error => {
        this.notificationIsLoading = false;
      });
    }
  searchNotifications(isPagination:boolean = false): void {


    // let paginator = this.notificationsPaginator;
    // let sorter = this.notificationsSort;

    this.notificationIsLoading = true;
    this.notificationSelection = [];

    this.notificationPersist.searchNotification(this.pageSize, isPagination? this.pageIndex:0).subscribe((partialList: NotificationSummaryPartialList) => {
      this.notificationsData = partialList.data;
      console.log("this.notificationsData",partialList)
      if (partialList.total != -1) {
        this.length = partialList.total;
      }
      this.notificationIsLoading = false;
    }, error => {
      this.notificationIsLoading = false;
    });

  } 
  addNotification(msg1:string, notifier:number, notified:number): void {
  // console.log("haha")
   
  //   this.notificationPersist.addNotification({msg:msg1,notifier,notified,date:Math.round(new Date().getTime() / 1000)}).subscribe((res:any)=>{
  //     console.log("some answer")
  //       this.searchNotifications()
  //   }, error => {
  //    console.log("error",error)
  //   })
  }

  

   back():void{
      this.location.back();
    }
}