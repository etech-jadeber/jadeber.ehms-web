import {TCId} from "../tc/models";
export class NotificationSummary extends TCId {
  notified:number;
    notifier:number;
    date:number;
    msg:string;
    id:string;
    links:string;
    patient:string;
     
    }
    export class NotificationSummaryPartialList {
      data: NotificationSummary[];
      total: number;
    }
    export class NotificationDetail extends NotificationSummary {
    }
    
    export class NotificationDashboard {
      total: number;
    }