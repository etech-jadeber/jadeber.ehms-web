import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";


import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class NotificationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  notificationUrl(): string {
    return "/notification";
  }

  session_therapyUrl(id: string): string {
    return "/notification/" + id;
  }

  viewNotifications(): void {
    this.router.navigateByUrl(this.notificationUrl());
  }

  viewNotification(id: string): void {
    this.router.navigateByUrl(this.session_therapyUrl(id));
  }


}