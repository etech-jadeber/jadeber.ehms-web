import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {NotificationDashboard, NotificationDetail, NotificationSummaryPartialList} from "./notification.model";


@Injectable({
  providedIn: 'root'
})
export class NotificationPersist {
 notificationSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "notification/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.notificationSearchText;
    //add custom filters
    return fltrs;
  }
  addNotification(item: any): Observable<any> {
    console.log("--------------------------------------*********************")
    return this.http.post<any>(
        "http://localhost:5000/notification",
      item
    );
  }
  updateNotificationCount(): Observable<any> {
    console.log("--------------------------------------*********************")
    return this.http.post<any>(
        "http://localhost:5000/notification/update",
      {}
    );
  }
  getNotificationCount(id:string): Observable<any> {
    console.log("--------------------------------------*********************")
    return this.http.get<any>(
        "http://localhost:5000/notification/count/"+id
    );
  }
 
  searchNotification(pageSize: number, pageIndex: number): Observable<NotificationSummaryPartialList> {
       console.log("]]]]]",pageSize,pageIndex)
    let url = `http://localhost:5000/notification?st=${this.notificationSearchText}&ps=${pageSize}&pi=${pageIndex}`;
    return this.http.get<NotificationSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "notification/do", new TCDoParam("download_all", this.filters()));
  }

  notificationDashboard(): Observable<NotificationDashboard> {
    return this.http.get<NotificationDashboard>(environment.tcApiBaseUri + "notification/dashboard");
  }

  getNotification(id: string): Observable<NotificationDetail> {
    return this.http.get<NotificationDetail>(environment.tcApiBaseUri + "notification/" + id);
  }
 }