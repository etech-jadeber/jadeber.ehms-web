import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseHistoryDetailComponent } from './nurse-history-detail.component';

describe('NurseHistoryDetailComponent', () => {
  let component: NurseHistoryDetailComponent;
  let fixture: ComponentFixture<NurseHistoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseHistoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
