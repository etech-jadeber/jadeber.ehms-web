import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {NurseHistoryDetail} from "../nurse_history.model";
import {NurseHistoryPersist} from "../nurse_history.persist";
import {NurseHistoryNavigator} from "../nurse_history.navigator";

export enum NurseHistoryTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-nurse-history-detail',
  templateUrl: './nurse-history-detail.component.html',
  styleUrls: ['./nurse-history-detail.component.scss']
})
export class NurseHistoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  nurseHistoryIsLoading:boolean = false;
  
  NurseHistoryTabs: typeof NurseHistoryTabs = NurseHistoryTabs;
  activeTab: NurseHistoryTabs = NurseHistoryTabs.overview;
  //basics
  nurseHistoryDetail: NurseHistoryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public nurseHistoryNavigator: NurseHistoryNavigator,
              public  nurseHistoryPersist: NurseHistoryPersist) {
    this.tcAuthorization.requireRead("nurse_historys");
    this.nurseHistoryDetail = new NurseHistoryDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("nurse_historys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.nurseHistoryIsLoading = true;
    this.nurseHistoryPersist.getNurseHistory(id).subscribe(nurseHistoryDetail => {
          this.nurseHistoryDetail = nurseHistoryDetail;
          this.nurseHistoryIsLoading = false;
        }, error => {
          console.error(error);
          this.nurseHistoryIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.nurseHistoryDetail.id);
  }
  editNurseHistory(): void {
    let modalRef = this.nurseHistoryNavigator.editNurseHistory(this.nurseHistoryDetail.id);
    modalRef.afterClosed().subscribe(nurseHistoryDetail => {
      TCUtilsAngular.assign(this.nurseHistoryDetail, nurseHistoryDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.nurseHistoryPersist.print(this.nurseHistoryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print nurse_history", true);
      });
    }

  back():void{
      this.location.back();
    }

}