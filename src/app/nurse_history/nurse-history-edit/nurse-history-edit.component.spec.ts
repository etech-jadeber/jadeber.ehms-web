import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseHistoryEditComponent } from './nurse-history-edit.component';

describe('NurseHistoryEditComponent', () => {
  let component: NurseHistoryEditComponent;
  let fixture: ComponentFixture<NurseHistoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseHistoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
