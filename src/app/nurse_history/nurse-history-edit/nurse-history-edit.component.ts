import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { NurseHistoryDetail } from '../nurse_history.model';import { NurseHistoryPersist } from '../nurse_history.persist';import { ChiefCompliantOptionSummary } from 'src/app/chief_compliant_option/chief_compliant_option.model';
import { ChiefCompliantOptionPersist } from 'src/app/chief_compliant_option/chief_compliant_option.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
@Component({
  selector: 'app-nurse-history-edit',
  templateUrl: './nurse-history-edit.component.html',
  styleUrls: ['./nurse-history-edit.component.scss']
})export class NurseHistoryEditComponent implements OnInit {

  nurseHistorysDisplayedColumns: string[] = ["action","edit", "cheif_compliant","duration" ];
  isLoadingResults: boolean = false;
  title: string;
  nurseHistoryDetailList: NurseHistoryDetail[] = [];
  chiefComplaintOptions :ChiefCompliantOptionSummary[]=[]; 
  chiefComplaintOptionsValue:string;
  
  nurseHistoryDetail: NurseHistoryDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<NurseHistoryEditComponent>,
              public  persist: NurseHistoryPersist,
              
              public chiefComplaintOptionsPersist: ChiefCompliantOptionPersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    this.loadChiefComplaintOptions();
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("nurse_historys");
      this.title = this.appTranslation.getText("general","new") +  " " + "nurse_history";
      this.nurseHistoryDetail = new NurseHistoryDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("nurse_historys");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "nurse_history";
      this.isLoadingResults = true;
      this.persist.getNurseHistory(this.idMode.id).subscribe(nurseHistoryDetail => {
        this.nurseHistoryDetail = nurseHistoryDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    for( let nurse_history of this.nurseHistoryDetailList){
      this.persist.addNurseHistory(this.idMode.id, nurse_history).subscribe(value => {
        this.tcNotification.success("nurseHistory added");
        this.nurseHistoryDetail.id = value.id;
        this.dialogRef.close(this.nurseHistoryDetail);
      }, error => {
        this.isLoadingResults = false;
      })
    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateNurseHistory(this.nurseHistoryDetail).subscribe(value => {
      this.tcNotification.success("nurse_history updated");
      this.dialogRef.close(this.nurseHistoryDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  


  searchValue(event:any,input:any):void{
    this.chiefComplaintOptionsPersist.chiefCompliantOptionSearchText = input.value;
    this.loadChiefComplaintOptions();
  }
  loadChiefComplaintOptions():void{
      this.chiefComplaintOptionsPersist.searchChiefCompliantOption(5,0,"name","asc").subscribe((value)=>{
        if(value){
            this.chiefComplaintOptions = value.data;
        }
      })
  }

  AddToList():void{
    this.nurseHistoryDetailList=[this.nurseHistoryDetail,...this.nurseHistoryDetailList];
    this.nurseHistoryDetail=new NurseHistoryDetail();
    this.chiefComplaintOptionsPersist.chiefCompliantOptionSearchText = "";
  }

  removeFromList(item:NurseHistoryDetail):void{
    this.nurseHistoryDetailList=this.nurseHistoryDetailList.filter((value)=> value != item);
  }
  editFromList(item:NurseHistoryDetail){
    this.removeFromList(item);
    this.nurseHistoryDetail=item;
  }

  canSave():boolean {
    if (this.nurseHistoryDetailList.length == 0){
      return false
    }
    return true;
  }
  
  canSubmit():boolean{
        if (this.nurseHistoryDetail == null){
            return false;
          }

          if (this.nurseHistoryDetail.cheif_compliant == null || this.nurseHistoryDetail.cheif_compliant == ""){
            return false;
          }
          if (this.nurseHistoryDetail.duration == null){
            return false;
          }
          if (this.nurseHistoryDetail.duration_unit == null){
            return false;
          }

          return true;

 }

 ngOnDestroy():void{
  this.chiefComplaintOptionsPersist.chiefCompliantOptionSearchText = "";
 }

 }