import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseHistoryListComponent } from './nurse-history-list.component';

describe('NurseHistoryListComponent', () => {
  let component: NurseHistoryListComponent;
  let fixture: ComponentFixture<NurseHistoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseHistoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseHistoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
