import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { NurseHistorySummary, NurseHistorySummaryPartialList } from '../nurse_history.model';
import { NurseHistoryPersist } from '../nurse_history.persist';
import { NurseHistoryNavigator } from '../nurse_history.navigator';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
@Component({
  selector: 'app-nurse-history-list',
  templateUrl: './nurse-history-list.component.html',
  styleUrls: ['./nurse-history-list.component.scss']
})export class NurseHistoryListComponent implements OnInit {
  nurseHistorysData: NurseHistorySummary[] = [];
  nurseHistorysTotalCount: number = 0;
  nurseHistorySelectAll:boolean = false;
  nurseHistorySelection: NurseHistorySummary[] = [];

  nurseHistorysDisplayedColumns: string[] = ["select","action" ,"cheif_compliant","duration","nurse","date" ];
  nurseHistorySearchTextBox: FormControl = new FormControl();
  @Input() encounterId:any;
  @Input() nurseId:any;
  @Input() date:any;

  user : {[id:string]:UserDetail} = {}
  nurseHistoryIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) nurseHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) nurseHistorysSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public nurseHistoryPersist: NurseHistoryPersist,
                public nurseHistoryNavigator: NurseHistoryNavigator,
                public userPersist: UserPersist,
                public jobPersist: JobPersist,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("nurse_historys");
       this.nurseHistorySearchTextBox.setValue(nurseHistoryPersist.nurseHistorySearchText);
      //delay subsequent keyup events
      this.nurseHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.nurseHistoryPersist.nurseHistorySearchText = value;
        this.searchNurse_historys();
      });
    } ngOnInit() {
   
      this.nurseHistorysSort.sortChange.subscribe(() => {
        this.nurseHistorysPaginator.pageIndex = 0;
        this.searchNurse_historys(true);
      });

      this.nurseHistorysPaginator.page.subscribe(() => {
        this.searchNurse_historys(true);
      });
      //start by loading items
      this.searchNurse_historys();
    }

  searchNurse_historys(isPagination:boolean = false): void {


    let paginator = this.nurseHistorysPaginator;
    let sorter = this.nurseHistorysSort;

    this.nurseHistoryIsLoading = true;
    this.nurseHistorySelection = [];

    this.nurseHistoryPersist.date = this.date? this.tcUtilsDate.toTimeStamp(this.date) : undefined;
    this.nurseHistoryPersist.nurse_id = this.nurseId;
    this.nurseHistoryPersist.searchNurseHistory(this.encounterId ,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: NurseHistorySummaryPartialList) => {
      this.nurseHistorysData = partialList.data;
      this.nurseHistorysData.forEach((nurseHistory)=>{
        if(nurseHistory){
          if(!this.user[nurseHistory.nurse_id]){
            this.userPersist.getUser(nurseHistory.nurse_id).subscribe((_user)=>{
              if(_user){
                this.user[nurseHistory.nurse_id] = _user;
              }
            })
          }
        }
      })
      if (partialList.total != -1) {
        this.nurseHistorysTotalCount = partialList.total;
      }
      this.nurseHistoryIsLoading = false;
    }, error => {
      this.nurseHistoryIsLoading = false;
    });

  } downloadNurseHistorys(): void {
    if(this.nurseHistorySelectAll){
         this.nurseHistoryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download nurse_history", true);
      });
    }
    else{
        this.nurseHistoryPersist.download(this.tcUtilsArray.idsList(this.nurseHistorySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download nurse_history",true);
            });
        }
  }
addNurse_history(): void {
    let dialogRef = this.nurseHistoryNavigator.addNurseHistory(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchNurse_historys();
      }
    });
  }

  editNurseHistory(item: NurseHistorySummary) {
    let dialogRef = this.nurseHistoryNavigator.editNurseHistory(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteNurseHistory(item: NurseHistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("nurse_history");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.nurseHistoryPersist.deleteNurseHistory(item.id).subscribe(response => {
          this.tcNotification.success("nurse_history deleted");
          this.searchNurse_historys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    ngOnDestroy():void{
      this.nurseHistoryPersist.date = undefined;
      this.nurseHistoryPersist.nurse_id = undefined;
    }
}