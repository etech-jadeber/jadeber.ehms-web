import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NurseHistoryPickComponent } from './nurse-history-pick.component';

describe('NurseHistoryPickComponent', () => {
  let component: NurseHistoryPickComponent;
  let fixture: ComponentFixture<NurseHistoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NurseHistoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseHistoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
