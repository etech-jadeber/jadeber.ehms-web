import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  NurseHistorySummary,
  NurseHistorySummaryPartialList,
} from '../nurse_history.model';
import { NurseHistoryPersist } from '../nurse_history.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-nurse-history-pick',
  templateUrl: './nurse-history-pick.component.html',
  styleUrls: ['./nurse-history-pick.component.scss'],
})
export class NurseHistoryPickComponent implements OnInit {
  nurseHistorysData: NurseHistorySummary[] = [];
  nurseHistorysTotalCount: number = 0;
  nurseHistorySelectAll:boolean = false;
  nurseHistorySelection: NurseHistorySummary[] = [];
  nurseHistorysDisplayedColumns: string[] = [
    'select',
    'action',
    'chief_compliant',
    'duration',
    'duration_unit',
  ];
  nurseHistorySearchTextBox: FormControl = new FormControl();
  nurseHistoryIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  nurseHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) nurseHistorysSort: MatSort;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<NurseHistoryPickComponent>,
    public nurseHistoryPersist: NurseHistoryPersist,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('nurse_historys');
    this.nurseHistorySearchTextBox.setValue(
      nurseHistoryPersist.nurseHistorySearchText
    );
    //delay subsequent keyup events
    this.nurseHistorySearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.nurseHistoryPersist.nurseHistorySearchText = value;
        this.searchNurse_historys();
      });
  }
  ngOnInit() {
    this.nurseHistorysSort.sortChange.subscribe(() => {
      this.nurseHistorysPaginator.pageIndex = 0;
      this.searchNurse_historys(true);
    });

    this.nurseHistorysPaginator.page.subscribe(() => {
      this.searchNurse_historys(true);
    });
    //start by loading items
    this.searchNurse_historys();
  }

  searchNurse_historys(isPagination: boolean = false): void {
    let paginator = this.nurseHistorysPaginator;
    let sorter = this.nurseHistorysSort;

    this.nurseHistoryIsLoading = true;
    this.nurseHistorySelection = [];

    this.nurseHistoryPersist
      .searchNurseHistory("",
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: NurseHistorySummaryPartialList) => {
          this.nurseHistorysData = partialList.data;
          if (partialList.total != -1) {
            this.nurseHistorysTotalCount = partialList.total;
          }
          this.nurseHistoryIsLoading = false;
        },
        (error) => {
          this.nurseHistoryIsLoading = false;
        }
      );
  }
  markOneItem(item: NurseHistorySummary) {
    if (!this.tcUtilsArray.containsId(this.nurseHistorySelection, item.id)) {
      this.nurseHistorySelection = [];
      this.nurseHistorySelection.push(item);
    } else {
      this.nurseHistorySelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.nurseHistorySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
