import {TCId} from "../tc/models";

export class NurseHistorySummary extends TCId {
    cheif_compliant:string;
    duration:number;
    duration_unit:number;
    date:number;
    nurse_id:string;
    }
    export class NurseHistorySummaryPartialList {
      data: NurseHistorySummary[];
      total: number;
    }
    export class NurseHistoryDetail extends NurseHistorySummary {
    }
    
    export class NurseHistoryDashboard {
      total: number;
    }