import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { NurseHistoryEditComponent } from "./nurse-history-edit/nurse-history-edit.component";
import { NurseHistoryPickComponent } from "./nurse-history-pick/nurse-history-pick.component";

@Injectable({
  providedIn: 'root'
})

export class NurseHistoryNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  nurse_historysUrl(): string {
    return "/nurse_historys";
  }

  nurse_historyUrl(id: string): string {
    return "/nurse_historys/" + id;
  }

  viewNurseHistorys(): void {
    this.router.navigateByUrl(this.nurse_historysUrl());
  }

  viewNurseHistory(id: string): void {
    this.router.navigateByUrl(this.nurse_historyUrl(id));
  }

  editNurseHistory(id: string): MatDialogRef<NurseHistoryEditComponent> {
    const dialogRef = this.dialog.open(NurseHistoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addNurseHistory(encounter_id:string): MatDialogRef<NurseHistoryEditComponent> {
    const dialogRef = this.dialog.open(NurseHistoryEditComponent, {
          data: new TCIdMode(encounter_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickNurseHistorys(selectOne: boolean=false): MatDialogRef<NurseHistoryPickComponent> {
      const dialogRef = this.dialog.open(NurseHistoryPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}