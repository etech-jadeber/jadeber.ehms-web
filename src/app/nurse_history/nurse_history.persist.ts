import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {NurseHistoryDashboard, NurseHistoryDetail, NurseHistorySummaryPartialList} from "./nurse_history.model";
import { durationUnit } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class NurseHistoryPersist {
 nurseHistorySearchText: string = "";
 date:number;
 nurse_id : string;

  duration_unit :TCEnum[] = [
    new TCEnum(durationUnit.minute, 'Minute'),
    new TCEnum(durationUnit.hour, "Hour"),
    new TCEnum(durationUnit.day, 'Day'),
    new TCEnum(durationUnit.week, "Week"),
    new TCEnum(durationUnit.month, 'Month'),
    new TCEnum(durationUnit.year, "Year"),
  ]

 constructor(private http: HttpClient) {
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.nurseHistorySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchNurseHistory(parentId:string,pageSize: number, pageIndex: number, sort: string, order: string,): Observable<NurseHistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + parentId + "/nurse_history", this.nurseHistorySearchText, pageSize, pageIndex, sort, order);
    if(this.date)
      url = TCUtilsString.appendUrlParameter(url, "date",this.date.toString());
    if(this.nurse_id)
     url = TCUtilsString.appendUrlParameter(url, "nurse_id",this.nurse_id);
    return this.http.get<NurseHistorySummaryPartialList>(url);

  } 
  
  nurseHistoryDashboard(): Observable<NurseHistoryDashboard> {
    return this.http.get<NurseHistoryDashboard>(
      environment.tcApiBaseUri + 'nurse_history/dashboard'
    );
  }

  getNurseHistory(id: string): Observable<NurseHistoryDetail> {
    return this.http.get<NurseHistoryDetail>(
      environment.tcApiBaseUri + 'nurse_history/' + id
    );
  }
  addNurseHistory(parent_id:string, item: NurseHistoryDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "form_encounter/" + parent_id +"/nurse_history",
      item
    );
  }

  updateNurseHistory(item: NurseHistoryDetail): Observable<NurseHistoryDetail> {
    return this.http.patch<NurseHistoryDetail>(
      environment.tcApiBaseUri + 'nurse_history/' + item.id,
      item
    );
  }

  deleteNurseHistory(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'nurse_history/' + id);
  }
  nurseHistorysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nurse_history/do',
      new TCDoParam(method, payload)
    );
  }

  nurseHistoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nurse_history/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nurse_history/do", new TCDoParam("download_all", this.filters()));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'nurse_history/do',
      new TCDoParam('download', ids)
    );
  }


  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nurse_history/" + id + "/do", new TCDoParam("print", {}));
  }  
 }