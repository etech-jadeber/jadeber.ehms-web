import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NursePlanOfCareDetailComponent } from './nurse-plan-of-care-detail.component';

describe('NursePlanOfCareDetailComponent', () => {
  let component: NursePlanOfCareDetailComponent;
  let fixture: ComponentFixture<NursePlanOfCareDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NursePlanOfCareDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NursePlanOfCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
