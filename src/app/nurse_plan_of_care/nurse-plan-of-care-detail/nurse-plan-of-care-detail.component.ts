import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { NursePlanOfCareDetail } from '../nurse_plan_of_care.model';
import { NursePlanOfCarePersist } from '../nurse_plan_of_care.persist';
import { NursePlanOfCareNavigator } from '../nurse_plan_of_care.navigator';

export enum NursePlanOfCareTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-nurse-plan-of-care-detail',
  templateUrl: './nurse-plan-of-care-detail.component.html',
  styleUrls: ['./nurse-plan-of-care-detail.component.scss'],
})
export class NursePlanOfCareDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  nursePlanOfCareIsLoading: boolean = false;

  NursePlanOfCareTabs: typeof NursePlanOfCareTabs = NursePlanOfCareTabs;
  activeTab: NursePlanOfCareTabs = NursePlanOfCareTabs.overview;
  //basics
  nursePlanOfCareDetail: NursePlanOfCareDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public nursePlanOfCareNavigator: NursePlanOfCareNavigator,
    public nursePlanOfCarePersist: NursePlanOfCarePersist
  ) {
    this.tcAuthorization.requireRead('nurse_plan_of_cares');
    this.nursePlanOfCareDetail = new NursePlanOfCareDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('nurse_plan_of_cares');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.nursePlanOfCareIsLoading = true;
    this.nursePlanOfCarePersist.getNursePlanOfCare(id).subscribe(
      (nursePlanOfCareDetail) => {
        this.nursePlanOfCareDetail = nursePlanOfCareDetail;
        this.nursePlanOfCareIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.nursePlanOfCareIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.nursePlanOfCareDetail.id);
  }
  editNursePlanOfCare(): void {
    let modalRef = this.nursePlanOfCareNavigator.editNursePlanOfCare(
      this.nursePlanOfCareDetail.id
    );
    modalRef.afterClosed().subscribe(
      (nursePlanOfCareDetail) => {
        TCUtilsAngular.assign(
          this.nursePlanOfCareDetail,
          nursePlanOfCareDetail
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.nursePlanOfCarePersist
      .print(this.nursePlanOfCareDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(
          printJob.id,
          'print nurse_plan_of_care',
          true
        );
      });
  }

  back(): void {
    this.location.back();
  }
}
