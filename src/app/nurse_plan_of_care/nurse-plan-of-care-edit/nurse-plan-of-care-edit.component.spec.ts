import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NursePlanOfCareEditComponent } from './nurse-plan-of-care-edit.component';

describe('NursePlanOfCareEditComponent', () => {
  let component: NursePlanOfCareEditComponent;
  let fixture: ComponentFixture<NursePlanOfCareEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NursePlanOfCareEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NursePlanOfCareEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
