import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { NursePlanOfCareDetail } from '../nurse_plan_of_care.model';
import { NursePlanOfCarePersist } from '../nurse_plan_of_care.persist';
import { PlanOfCareNavigator } from 'src/app/plan_of_care/plan_of_care.navigator';
import { PlanOfCareSummary } from 'src/app/plan_of_care/plan_of_care.model';
@Component({
  selector: 'app-nurse-plan-of-care-edit',
  templateUrl: './nurse-plan-of-care-edit.component.html',
  styleUrls: ['./nurse-plan-of-care-edit.component.scss']
})

export class NursePlanOfCareEditComponent implements OnInit {

  plan_of_care_name:string;
  isLoadingResults: boolean = false;
  title: string;
  nursePlanOfCareDetail: NursePlanOfCareDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<NursePlanOfCareEditComponent>,
              public planOfCareNavigator: PlanOfCareNavigator,
              public  persist: NursePlanOfCarePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("nurse_plan_of_cares");
      this.title = this.appTranslation.getText("general","new") +  " " + "nurse_plan_of_care";
      this.nursePlanOfCareDetail = new NursePlanOfCareDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("nurse_plan_of_cares");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "nurse_plan_of_care";
      this.isLoadingResults = true;
      this.persist.getNursePlanOfCare(this.idMode.id).subscribe(nursePlanOfCareDetail => {
        this.nursePlanOfCareDetail = nursePlanOfCareDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addNursePlanOfCare(this.idMode.id,this.nursePlanOfCareDetail).subscribe(value => {
      this.tcNotification.success("nursePlanOfCare added");
      this.nursePlanOfCareDetail.id = value.id;
      this.dialogRef.close(this.nursePlanOfCareDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateNursePlanOfCare(this.nursePlanOfCareDetail).subscribe(value => {
      this.tcNotification.success("nurse_plan_of_care updated");
      this.dialogRef.close(this.nursePlanOfCareDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  searchPlaneOfCare():void{
      let dialogRef = this.planOfCareNavigator.pickPlanOfCares(null,true);
      dialogRef.afterClosed().subscribe((result: PlanOfCareSummary) => {
        if (result) {
          this.plan_of_care_name = result[0].name;
          this.nursePlanOfCareDetail.plan_of_care_id = result[0].id;
        }
      });
  
  }
  
  canSubmit():boolean{
        if (this.nursePlanOfCareDetail == null){
            return false;
          }
          if (this.nursePlanOfCareDetail.plan_of_care_id == null){
            return false;
          }

          return true;

 }
 }