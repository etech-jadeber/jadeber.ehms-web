import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NursePlanOfCareListComponent } from './nurse-plan-of-care-list.component';

describe('NursePlanOfCareListComponent', () => {
  let component: NursePlanOfCareListComponent;
  let fixture: ComponentFixture<NursePlanOfCareListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NursePlanOfCareListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NursePlanOfCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
