import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { NursePlanOfCareSummary, NursePlanOfCareSummaryPartialList } from '../nurse_plan_of_care.model';
import { NursePlanOfCareNavigator } from '../nurse_plan_of_care.navigator';
import { NursePlanOfCarePersist } from '../nurse_plan_of_care.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { PlanOfCareDetail, PlanOfCareSummary } from 'src/app/plan_of_care/plan_of_care.model';
import { PlanOfCarePersist } from 'src/app/plan_of_care/plan_of_care.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { PlanOfCareNavigator } from 'src/app/plan_of_care/plan_of_care.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { physican_type } from 'src/app/app.enums';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
@Component({
  selector: 'app-nurse-plan-of-care-list',
  templateUrl: './nurse-plan-of-care-list.component.html',
  styleUrls: ['./nurse-plan-of-care-list.component.scss']
})

export class  NursePlanOfCareListComponent implements OnInit {
  nursePlanOfCaresData: NursePlanOfCareSummary[] = [];
  nursePlanOfCaresTotalCount: number = 0;
  nursePlanOfCareSelectAll:boolean = false;
  nursePlanOfCareSelection: NursePlanOfCareSummary[] = [];

  disable:boolean= true;
  nursePlanOfCaresDisplayedColumns: string[] = ["select","action" ,"plan_of_care_id", "nurse_id", "frequency","date" ];
  nursePlanOfCareSearchTextBox: FormControl = new FormControl();
  nursePlanOfCareIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) nursePlanOfCaresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) nursePlanOfCaresSort: MatSort;  
  @Input() encounterId:any;
  @Input() implementation:boolean = false;
  nurseName:string;
  nurseId : string;

  user : {[id:string]:UserDetail} = {}

  plan_of_care : {[id:string]:PlanOfCareDetail} = {}

 date:FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});

  constructor(
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public nursePlanOfCarePersist: NursePlanOfCarePersist,
                public nursePlanOfCareNavigator: NursePlanOfCareNavigator,
                public planOfCareNavigator: PlanOfCareNavigator,
                public planOfCarePersist: PlanOfCarePersist,
                public doctorPersist: DoctorPersist,
                public doctorNavigator: DoctorNavigator,
                public userPersist: UserPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

      this.nursePlanOfCarePersist.date.setValue(new Date(new Date().toDateString()));
        this.tcAuthorization.requireRead("nurse_plan_of_cares");
       this.nursePlanOfCareSearchTextBox.setValue(nursePlanOfCarePersist.nursePlanOfCareSearchText);
      //delay subsequent keyup events
      this.nursePlanOfCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.nursePlanOfCarePersist.nursePlanOfCareSearchText = value;
        this.searchNurse_plan_of_cares();
      });

      this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
          this.nursePlanOfCarePersist.date.setValue(value)
          this.searchNurse_plan_of_cares();
      });
    } 
    ngOnInit() {
   
      this.doctorPersist.getDocotrSelf().subscribe((nurse: DoctorSummary)=>{
        if(nurse){
          this.nurseName = nurse.first_name + " " + nurse.middle_name;
          this.nursePlanOfCarePersist.nurse_id = nurse.login_id;
        }
      })
      
      this.nursePlanOfCaresSort.sortChange.subscribe(() => {
        this.nursePlanOfCaresPaginator.pageIndex = 0;
        this.searchNurse_plan_of_cares(true);
      });

      this.nursePlanOfCaresPaginator.page.subscribe(() => {
        this.searchNurse_plan_of_cares(true);
      });
      if(this.implementation){
        this.nursePlanOfCaresDisplayedColumns = [...this.nursePlanOfCaresDisplayedColumns.slice(0,3),"status"]
      }
      //start by loading items
      this.searchNurse_plan_of_cares();
    }

  searchNurse_plan_of_cares(isPagination:boolean = false): void {


    let paginator = this.nursePlanOfCaresPaginator;
    let sorter = this.nursePlanOfCaresSort;

    this.nursePlanOfCareIsLoading = true;
    this.nursePlanOfCareSelection = [];
    console.log("me")
    this.nursePlanOfCarePersist.implementation = this.implementation ? "1":"";
    this.nursePlanOfCarePersist.searchNursePlanOfCare(this.encounterId, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: NursePlanOfCareSummaryPartialList) => {
      this.nursePlanOfCaresData = partialList.data;
      this.nursePlanOfCaresData.forEach((nurse_plan_of_care)=>{
        if(nurse_plan_of_care){
          if(!this.user[nurse_plan_of_care.nurse_id]){
            this.userPersist.getUser(nurse_plan_of_care.nurse_id).subscribe((_user)=>{
              if(_user){
                this.user[nurse_plan_of_care.nurse_id] = _user;
              }
            })
          }
          if(!this.plan_of_care[nurse_plan_of_care.plan_of_care_id]){
            this.planOfCarePersist.getPlanOfCare(nurse_plan_of_care.plan_of_care_id).subscribe((_plan_of_care)=>{
              if(_plan_of_care){
                this.plan_of_care[nurse_plan_of_care.plan_of_care_id] = _plan_of_care;
              }
            })
          }
        }
      })
      if (partialList.total != -1) {
        this.nursePlanOfCaresTotalCount = partialList.total;
      }
      this.nursePlanOfCareIsLoading = false;
    }, error => {
      this.nursePlanOfCareIsLoading = false;
    });

  } downloadNursePlanOfCares(): void {
    if(this.nursePlanOfCareSelectAll){
         this.nursePlanOfCarePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download nurse_plan_of_care", true);
      });
    }
    else{
        this.nursePlanOfCarePersist.download(this.tcUtilsArray.idsList(this.nursePlanOfCareSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download nurse_plan_of_care",true);
            });
        }
  }
addNurse_plan_of_care(): void {
    let dialogRef = this.nursePlanOfCareNavigator.addNursePlanOfCare(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchNurse_plan_of_cares();
      }
    });
  }

  searchNurses():void{
    let dialogRef = this.doctorNavigator.pickDoctors(true,physican_type.nurse);

    dialogRef.afterClosed().subscribe((nurse:DoctorSummary[])=>{
      if(nurse){
        this.nurseName = nurse[0].first_name + " " + nurse[0].middle_name;
        this.nursePlanOfCarePersist.nurse_id = nurse[0].login_id;
        this.searchNurse_plan_of_cares();
      }
    })
  }

  edit(element:NursePlanOfCareSummary):void{
    element.disable = true;
  }
  editStatus(element: NursePlanOfCareSummary):void {
    element.status = undefined;
  }
  saveStatus(element: NursePlanOfCareSummary):void {
    this.nursePlanOfCarePersist.nursePlanOfCareDo(element.id,"change_status",{status:element.status}).subscribe((value)=>{
      if(value)
        this.tcNotification.success("You have successfully approve the status")
    });
  }
  save(element: NursePlanOfCareSummary):void{
    element.disable = false;
    this.nursePlanOfCarePersist.nursePlanOfCareDo(element.id,"change_frequency",{frequency:element.frequency}).subscribe((value)=>{
      if(value)
        this.tcNotification.success("You have successfully changed the frequency")
    });
  }
  editNursePlanOfCare(item: NursePlanOfCareSummary) {
    let dialogRef = this.nursePlanOfCareNavigator.editNursePlanOfCare(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        if(!this.plan_of_care[item.plan_of_care_id]){
          this.planOfCarePersist.getPlanOfCare(item.plan_of_care_id).subscribe((_plan_of_care)=>{
            if(_plan_of_care){
              this.plan_of_care[item.plan_of_care_id] = _plan_of_care;
            }
          })
        }
      }

    });
  }

  searchPlaneOfCare():void{
    let dialogRef = this.planOfCareNavigator.pickPlanOfCares(this.encounterId,true);
    dialogRef.afterClosed().subscribe((results: PlanOfCareSummary[]) => {
      if (results.length) {
        for(let result of results ){
          let refresh = false
          if(results.indexOf(result) == results.length -1)
            refresh = true
          let nursePlanOfCare = new NursePlanOfCareSummary();
          nursePlanOfCare.plan_of_care_id = result.id;
          this.addPlaneOfCare(nursePlanOfCare,refresh)
        }
      }
      else{
        this.tcNotification.info("No newly added nursePlanOfCare ");
      }
    });

}

  addPlaneOfCare(nurse_plan_of_care: NursePlanOfCareSummary, refresh:boolean):void{
    this.nursePlanOfCarePersist.addNursePlanOfCare(this.encounterId, nurse_plan_of_care).subscribe(value => {
      if(value)
        this.tcNotification.success("nursePlanOfCare added");
      if(refresh)
        this.searchNurse_plan_of_cares(); 
    });
  }

  deleteNursePlanOfCare(item: NursePlanOfCareSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("nurse_plan_of_care");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.nursePlanOfCarePersist.deleteNursePlanOfCare(item.id).subscribe(response => {
          this.tcNotification.success("nurse_plan_of_care deleted");
          this.searchNurse_plan_of_cares();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }


}