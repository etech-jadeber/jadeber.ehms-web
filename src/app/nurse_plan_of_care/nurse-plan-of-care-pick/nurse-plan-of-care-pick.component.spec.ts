import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NursePlanOfCarePickComponent } from './nurse-plan-of-care-pick.component';

describe('NursePlanOfCarePickComponent', () => {
  let component: NursePlanOfCarePickComponent;
  let fixture: ComponentFixture<NursePlanOfCarePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NursePlanOfCarePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NursePlanOfCarePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
