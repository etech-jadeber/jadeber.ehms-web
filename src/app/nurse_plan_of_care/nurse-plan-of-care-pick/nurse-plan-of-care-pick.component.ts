import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { NursePlanOfCareSummary, NursePlanOfCareSummaryPartialList } from '../nurse_plan_of_care.model';
import { NursePlanOfCarePersist } from '../nurse_plan_of_care.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NursePlanOfCareNavigator } from '../nurse_plan_of_care.navigator';
@Component({
  selector: 'app-nurse-plan-of-care-pick',
  templateUrl: './nurse-plan-of-care-pick.component.html',
  styleUrls: ['./nurse-plan-of-care-pick.component.scss']
})export class NursePlanOfCarePickComponent implements OnInit {
  nursePlanOfCaresData: NursePlanOfCareSummary[] = [];
  nursePlanOfCaresTotalCount: number = 0;
  nursePlanOfCareSelectAll:boolean = false;
  nursePlanOfCareSelection: NursePlanOfCareSummary[] = [];

  nursePlanOfCaresDisplayedColumns: string[] = ["select","action" ,"plan_of_care_id", "nurse_id","date" ];
  nursePlanOfCareSearchTextBox: FormControl = new FormControl();
  nursePlanOfCareIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) nursePlanOfCaresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) nursePlanOfCaresSort: MatSort;  
   constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialogRef: MatDialogRef<NursePlanOfCarePickComponent>,
                public nursePlanOfCarePersist: NursePlanOfCarePersist,
                public nursePlaneOfCareNavigator: NursePlanOfCareNavigator,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("nurse_plan_of_cares");
       this.nursePlanOfCareSearchTextBox.setValue(nursePlanOfCarePersist.nursePlanOfCareSearchText);
      //delay subsequent keyup events
      this.nursePlanOfCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.nursePlanOfCarePersist.nursePlanOfCareSearchText = value;
        this.searchNurse_plan_of_cares();
      });
    } ngOnInit() {
   
      this.nursePlanOfCaresSort.sortChange.subscribe(() => {
        this.nursePlanOfCaresPaginator.pageIndex = 0;
        this.searchNurse_plan_of_cares(true);
      });

      this.nursePlanOfCaresPaginator.page.subscribe(() => {
        this.searchNurse_plan_of_cares(true);
      });
      //start by loading items
      this.searchNurse_plan_of_cares();
    }

  searchNurse_plan_of_cares(isPagination:boolean = false): void {


    let paginator = this.nursePlanOfCaresPaginator;
    let sorter = this.nursePlanOfCaresSort;

    this.nursePlanOfCareIsLoading = true;
    this.nursePlanOfCareSelection = [];

    this.nursePlanOfCarePersist.searchNursePlanOfCare("", paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: NursePlanOfCareSummaryPartialList) => {
      this.nursePlanOfCaresData = partialList.data;
      if (partialList.total != -1) {
        this.nursePlanOfCaresTotalCount = partialList.total;
      }
      this.nursePlanOfCareIsLoading = false;
    }, error => {
      this.nursePlanOfCareIsLoading = false;
    });

  }
  markOneItem(item: NursePlanOfCareSummary) {
    if(!this.tcUtilsArray.containsId(this.nursePlanOfCareSelection,item.id)){
          this.nursePlanOfCareSelection = [];
          this.nursePlanOfCareSelection.push(item);
        }
        else{
          this.nursePlanOfCareSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.nursePlanOfCareSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }