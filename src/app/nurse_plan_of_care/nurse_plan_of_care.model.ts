import {TCId} from "../tc/models";

export class NursePlanOfCareSummary extends TCId {
    plan_of_care_id:string;
    nurse_id:string;
    date:number;
    frequency:number;
    disable:boolean;
    disable_status:boolean;
    status: number;
     
    }
    export class NursePlanOfCareSummaryPartialList {
      data: NursePlanOfCareSummary[];
      total: number;
    }
    export class NursePlanOfCareDetail extends NursePlanOfCareSummary {
    }
    
    export class NursePlanOfCareDashboard {
      total: number;
    }