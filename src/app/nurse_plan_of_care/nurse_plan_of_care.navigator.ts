import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { NursePlanOfCareEditComponent } from "./nurse-plan-of-care-edit/nurse-plan-of-care-edit.component";
import { NursePlanOfCarePickComponent } from "./nurse-plan-of-care-pick/nurse-plan-of-care-pick.component";


@Injectable({
  providedIn: 'root'
})

export class NursePlanOfCareNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  nurse_plan_of_caresUrl(): string {
    return "/nurse_plan_of_cares";
  }

  nurse_plan_of_careUrl(id: string): string {
    return "/nurse_plan_of_cares/" + id;
  }

  viewNursePlanOfCares(): void {
    this.router.navigateByUrl(this.nurse_plan_of_caresUrl());
  }

  viewNursePlanOfCare(id: string): void {
    this.router.navigateByUrl(this.nurse_plan_of_careUrl(id));
  }

  editNursePlanOfCare(id: string): MatDialogRef<NursePlanOfCareEditComponent> {
    const dialogRef = this.dialog.open(NursePlanOfCareEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addNursePlanOfCare(parent_id:string): MatDialogRef<NursePlanOfCareEditComponent> {
    const dialogRef = this.dialog.open(NursePlanOfCareEditComponent, {
          data: new TCIdMode(parent_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickNursePlanOfCares(selectOne: boolean=false): MatDialogRef<NursePlanOfCarePickComponent> {
      const dialogRef = this.dialog.open(NursePlanOfCarePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}