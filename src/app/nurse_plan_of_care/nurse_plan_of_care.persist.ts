import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {NursePlanOfCareDashboard, NursePlanOfCareDetail, NursePlanOfCareSummaryPartialList} from "./nurse_plan_of_care.model";
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';
import { plan_status } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class NursePlanOfCarePersist {
 nursePlanOfCareSearchText: string = "";
 date:FormControl = new FormControl({disabled: true, value: ''});
 nurse_id : string;
 
 planStatus: TCEnum[] = [
  new TCEnum(plan_status.done, 'Done'),
  new TCEnum(plan_status.undone, 'Undone'),
]
implementation: string;

 constructor(private http: HttpClient) {
  }
    filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.nursePlanOfCareSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchNursePlanOfCare(parent_id:string,pageSize: number, pageIndex: number, sort: string, order: string,): Observable<NursePlanOfCareSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "form_encounter/" + parent_id +"/nurse_plan_of_care", this.nursePlanOfCareSearchText, pageSize, pageIndex, sort, order);
    if (this.date.value){
      url = TCUtilsString.appendUrlParameter(url, "date", (new Date(this.date.value).getTime()/1000).toString())
    }
    if(this.nurse_id){
      url = TCUtilsString.appendUrlParameter(url, "nurse_id", this.nurse_id)
    }
    if(this.implementation){
      url = TCUtilsString.appendUrlParameter(url, "self", this.implementation)
    }
    return this.http.get<NursePlanOfCareSummaryPartialList>(url);

  } 

  nursePlanOfCareDashboard(): Observable<NursePlanOfCareDashboard> {
    return this.http.get<NursePlanOfCareDashboard>(environment.tcApiBaseUri + "nurse_plan_of_care/dashboard");
  }

  getNursePlanOfCare(id: string): Observable<NursePlanOfCareDetail> {
    return this.http.get<NursePlanOfCareDetail>(environment.tcApiBaseUri + "nurse_plan_of_care/" + id);
  }

  addNursePlanOfCare(parent_id:string, item: NursePlanOfCareDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "form_encounter/" + parent_id +"/nurse_plan_of_care",
      item
    );
  }

  updateNursePlanOfCare(item: NursePlanOfCareDetail): Observable<NursePlanOfCareDetail> {
    return this.http.patch<NursePlanOfCareDetail>(
      environment.tcApiBaseUri + 'nurse_plan_of_care/' + item.id,
      item
    );
  }

  deleteNursePlanOfCare(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'nurse_plan_of_care/' + id);
  }
  nursePlanOfCaresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nurse_plan_of_care/do',
      new TCDoParam(method, payload)
    );
  }

  nursePlanOfCareDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nurse_plan_of_care/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }



  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'nurse_plan_of_care/do',
      new TCDoParam('download', ids)
    );
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nurse_plan_of_care/do", new TCDoParam("download_all", this.filters()));
  }
  
  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nurse_plan_of_care/" + id + "/do", new TCDoParam("print", {}));
  }  
 }