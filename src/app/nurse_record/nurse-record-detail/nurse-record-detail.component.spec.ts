import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NurseRecordDetailComponent } from './nurse-record-detail.component';

describe('NurseRecordDetailComponent', () => {
  let component: NurseRecordDetailComponent;
  let fixture: ComponentFixture<NurseRecordDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NurseRecordDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseRecordDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
