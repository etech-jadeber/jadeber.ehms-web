import {
  Component,
  OnInit,
} from '@angular/core';
import { BedPersist } from 'src/app/bed/beds/bed.persist';

import { Nurse_RecordNavigator } from '../nurse_Record_Navigator';
import { debounceTime, interval, Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { NurseRecord, Prescription } from '../nurse_record.model';
import { AppTranslation } from 'src/app/app.translation';
import { Location } from '@angular/common';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Patient_TreatmentNavigator } from 'src/app/patient_treatments/patient_treatment.navigator';
import { TCAuthorization } from '../../tc/authorization';
import { Patient_RecordNavigator } from 'src/app/patient_records/patient_record.navigator';
import {
  Patient_RecordSummary,
} from 'src/app/patient_records/patient_record.model';
import { FormControl } from '@angular/forms';
import { Patient_RecordPersist } from 'src/app/patient_records/patient_record.persist';
import { TCNavigator } from '../../tc/navigator';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Patient_TreatmentPersist } from 'src/app/patient_treatments/patient_treatment.persist';
import { Patient_TreatmentDetail } from 'src/app/patient_treatments/patient_treatment.model';
import { Admission_FormPersist } from 'src/app/form_encounters/admission_forms/admission_form.persist';
import { Admission_FormSummary } from 'src/app/form_encounters/admission_forms/admission_form.model';
import { HistoryPersist } from 'src/app/historys/history.persist';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { OrdersDetail } from 'src/app/form_encounters/orderss/orders.model';
import { PatientDetail } from 'src/app/patients/patients.model';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { category, encounter_status, InstructionType, physican_type, prescription_type, ServiceType } from 'src/app/app.enums';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';

export enum PaginatorIndexes {
  nurse_record,
  nursing_record,
}
@Component({
  selector: 'app-nurse-record-detail',
  templateUrl: './nurse-record-detail.component.html',
  styleUrls: ['./nurse-record-detail.component.css'],
})
export class NurseRecordDetailComponent implements OnInit {
  paramsSubscription: Subscription;
  nurseRecordDetail: NurseRecord;
  recordLoading: boolean = false;
  admissionLoading: boolean = false;
  nurseName:string;
  category = category;
  nurseId : string;
  service_type = ServiceType;
  prescription_type = prescription_type;

  instructionType = InstructionType

  admisssionDetail:Admission_FormSummary = new Admission_FormSummary();
  patientDetail: PatientDetail = new PatientDetail();
  ordersDetail: OrdersDetail;
  admissionFormSearchHistory = {...this.admissionFormPersist.defaultAdmissionFormSearchHistory}
  date:FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  id:string;
  constructor(
    private route: ActivatedRoute,
    public bedPersist: BedPersist,
    public nurse_recordNavigator: Nurse_RecordNavigator,
    public appTranslation: AppTranslation,
    private location: Location,
    public tcUtilsDate: TCUtilsDate,
    public patient_treatmentNavigator: Patient_TreatmentNavigator,
    public tcAuthorization: TCAuthorization,
    public patient_recordNavigator: Patient_RecordNavigator,
    public admissionFormPersist: Admission_FormPersist,
    public patient_recordPersist: Patient_RecordPersist,
    public form_encounterPersist: Form_EncounterPersist,
    public historyPersist: HistoryPersist,
    public tcNavigator: TCNavigator,
    public tcNotification: TCNotification,
    public doctorPersist: DoctorPersist,
    public doctorNavigator: DoctorNavigator,
    public departmentSpecialtyPersist: Department_SpecialtyPersist,
    public tcUtilsArray: TCUtilsArray,
    public prescriptionNavigator: Form_EncounterNavigator,
  ) {
    this.nurseRecordDetail = new NurseRecord();
    this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.refresh();
    });
  }

  ngOnInit() {
    this.doctorPersist.getDocotrSelf().subscribe((nurse: DoctorSummary)=>{
      if(nurse){
        this.nurseName = nurse.first_name + " " + nurse.middle_name;
        this.nurseId = nurse.login_id;
      }
    })
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      this.id = this.route.snapshot.paramMap.get('id');
      // this.menuState.changeMenuState(id);
      this.loadDetails(this.id);
    });
  }

  loadDetails(id: string): void {
    this.recordLoading = true;
    this.admissionLoading = true;
    this.nurseRecordDetail = new NurseRecord()
    this.bedPersist
      .bedsDo('single_bed_record', { id })
      .subscribe((result: any) => {
        this.nurseRecordDetail = result[0];
        this.recordLoading = false;
        this.departmentSpecialtyPersist.getDepartment_Specialty(this.nurseRecordDetail.department_speciality_id).subscribe((department_speciality)=>{
          if(department_speciality){
            this.nurseRecordDetail.department = department_speciality.name;
          }
        })
        this.form_encounterPersist.getForm_Encounter(this.nurseRecordDetail.encounter_id).subscribe((encounter)=>{
          if(encounter){
            this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
            this.admissionFormSearchHistory.encounter_id = encounter.parent_encounter_id
            this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
            this.form_encounterPersist.encounter_active = encounter.status == encounter_status.opened;
            this.admissionFormPersist.searchAdmission_Form(1,0,"","").subscribe((admission)=>{
              if(admission.total){
                  this.admisssionDetail = admission.data[0];
                  this.admissionLoading = false;
                  this.historyPersist.searchHistory(encounter.parent_encounter_id,1,0,"","").subscribe((history)=>{
                    if(history.data.length)
                      this.admisssionDetail.chief_complaint = history.data[0].chief_complaint
                  })
              }
            });
          }
        })
        this.patientDetail.fname = this.nurseRecordDetail.first_name
        this.patientDetail.mname = this.nurseRecordDetail.middle_name
        this.patientDetail.lname = this.nurseRecordDetail.last_name
        this.patientDetail.sex = this.nurseRecordDetail.sex == 'male'
        this.patientDetail.dob = this.nurseRecordDetail.dob
      }),
      (error) => {
        console.error(error);
        this.recordLoading = false;
      };
  }
  
  onTabChanged(matTab: MatTabChangeEvent){
    this.nurse_recordNavigator.selected_index=matTab.index;
  }

  handleBackButton(){
    this.ordersDetail = null;
  }

  searchNurses():void{
      let dialogRef = this.doctorNavigator.pickDoctors(true,physican_type.nurse);
      dialogRef.afterClosed().subscribe((nurse:DoctorSummary[])=>{
        if(nurse){
          this.nurseName = nurse[0].first_name + " " + nurse[0].middle_name;
          this.nurseId = nurse[0].login_id;
          this.refresh();
        }
      })
    }

    refresh():void{
      let encounter_id = this.nurseRecordDetail.encounter_id;
      this.nurseRecordDetail.encounter_id = undefined;
      setTimeout(() => {
        this.nurseRecordDetail.encounter_id = encounter_id;
      }, 1);
    }


  reload() {
    this.loadDetails(this.nurseRecordDetail.encounter_id);
  }

  loadOrdersDetail(ordersDetail: OrdersDetail) {
    this.ordersDetail = ordersDetail
  }

  back(): void {
    this.location.back();
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    // this.admissionFormPersist.encounterId = undefined;
    // this.form_encounterPersist.parent_encounter_id = null;
    //     this.form_encounterPersist.encounter_is_active = null;
    //     this.form_encounterPersist.patientId = null;
    //     this.form_encounterPersist.show_only_mine = true;
    //     this.form_encounterPersist.encounterStatusFilter = 100;
    //     this.form_encounterPersist.date = this.tcUtilsDate.toTimeStamp(new Date());
  }

  addPrescription(){
    let dialog = this.prescriptionNavigator.addPrescriptions(this.nurseRecordDetail.encounter_id)
    dialog.afterClosed().subscribe(value => {
      if (value) {
        this.reload()
      }
    })
  }

}
