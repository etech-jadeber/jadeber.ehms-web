import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NurseRecordListComponent } from './nurse-record-list.component';

describe('NurseRecordListComponent', () => {
  let component: NurseRecordListComponent;
  let fixture: ComponentFixture<NurseRecordListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NurseRecordListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NurseRecordListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
