import { Component, OnInit, ViewChild } from '@angular/core';
import { AppTranslation } from 'src/app/app.translation';
import { BedPersist } from 'src/app/bed/beds/bed.persist';

import { NurseRecord } from '../nurse_record.model';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import { Nurse_RecordNavigator } from '../nurse_Record_Navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import {debounceTime} from "rxjs/operators";
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import {fee_type, physican_type, physician_type, ServiceType} from 'src/app/app.enums';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { TCNotification } from 'src/app/tc/notification';
import { OrderedOtheServiceNavigator } from 'src/app/ordered_othe_service/ordered_othe_service.navigator';
import { OrderedOtheServiceDetail } from 'src/app/ordered_othe_service/ordered_othe_service.model';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import {encounterFilterColumn, Form_EncounterDetail} from 'src/app/form_encounters/form_encounter.model';
import {BedassignmentPersist} from "../../bed/bedassignments/bedassignment.persist";


@Component({
  selector: 'app-nurse-record-list',
  templateUrl: './nurse-record-list.component.html',
  styleUrls: ['./nurse-record-list.component.css']
})
export class NurseRecordListComponent implements OnInit {

   nurseRecord : NurseRecord[] = [];
   nurseRecordLoading: boolean = false;
   nurseRecordTotal: number = 0;
   nurseRecordSearchTextBox: FormControl = new FormControl();
   nurseRecordFilterColumn = encounterFilterColumn;

  ageRangeDisplayColumn: string[] = ["action", "p.pid", "patient_Name","sex", "ward","room","bed_no", "admission_date"];

  @ViewChild(MatPaginator, {static: true}) nurseRecordPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) nurseRecordSort: MatSort;


  constructor( public appTranslation:AppTranslation,
                public bedPersist: BedPersist,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate: TCUtilsDate,
                public nurse_recordNavigator: Nurse_RecordNavigator,
                public tcUtilsString: TCUtilsString,
                public tcUtilsAngular: TCUtilsAngular,
                public wardNavigator: BedroomtypeNavigator,
                public doctorNavigator: DoctorNavigator,
                public paymentPersist: PaymentPersist,
                public tcNotification: TCNotification,
                public orderedOtherServiceNavigator: OrderedOtheServiceNavigator,
                public otherServiceNaviagator: OtherServicesNavigator,
                public procedureOrderNavigator: Form_EncounterNavigator,
               public bedAssignmentPersist: BedassignmentPersist,

    ) {
      this.nurseRecordSearchTextBox.setValue(bedPersist.nurseRecordListSearchHistory.search_text);
      //delay subsequent keyup events
      this.nurseRecordSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.bedPersist.nurseRecordListSearchHistory.search_text = value;
        this.searchBedRecord();
      });
    }

  ngOnInit() {
    this.nurseRecordSort.sortChange.subscribe(() => {
      this.nurseRecordPaginator.pageIndex = 0;
      this.searchBedRecord(true);
    });

    this.nurseRecordPaginator.page.subscribe(() => {
      this.searchBedRecord(true);
    });
    this.nurseRecordSort.active = 'mrn'
    //start by loading items
    this.searchBedRecord();
  }

  searchBedRecord(isPagination:boolean = false): void{
    let paginator = this.nurseRecordPaginator;
    let sorter = this.nurseRecordSort;
    this.nurseRecordLoading = true;
    this.bedPersist.bedsDo("bed_record", {ward_id: this.bedPersist.nurseRecordListSearchHistory.ward_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((result:any) => {
      this.nurseRecord = result.data;
      this.nurseRecordLoading = false;
      if(result.total !== -1){
        this.nurseRecordTotal = result.total;
      }
    }, error => {
      console.error(error);
      this.nurseRecordLoading = false;
      });
  }


  searchWard(){
    let dialogRef = this.wardNavigator.pickBedroomtypes()
    dialogRef.afterClosed().subscribe(
      (ward: BedroomtypeDetail[]) => {
        if (ward){
          this.bedPersist.nurseRecordListSearchHistory.ward_id = ward[0].id
          this.bedPersist.nurseRecordListSearchHistory.wardName = ward[0].name
          this.searchBedRecord()
        }
      }
    )
  }

  // ambulanceService(detail: Form_EncounterDetail) {
  //   let dialogRef = this.ambulanceServiceNavigator.addAmbulanceService(detail.id)
  //   dialogRef.afterClosed().subscribe(
  //     (otherService: OrderedOtheServiceDetail) => {
  //     }
  //   )
  // }

  additionalService(detail: NurseRecord){
    let dialogRef = this.orderedOtherServiceNavigator.addOrderedOtheService(detail.encounter_id)
    dialogRef.afterClosed().subscribe(
      (otherService: OrderedOtheServiceDetail) => {
        if (otherService){
          this.searchBedRecord()
        }
      }
    )
  }

  procedureOrder(detail: NurseRecord){
    let dialogRef = this.procedureOrderNavigator.addPatient_Procedure(
      detail.encounter_id
    );
    dialogRef.afterClosed().subscribe((newPatient_Procedure) => {
    });
  }

  // oxygenService(detail: NurseRecord){
  //   let dialogRef = this.oxygenServiceNavigator.addOxygenService(detail.encounter_id)
  //   dialogRef.afterClosed().subscribe(
  //     (otherService: OrderedOtheServiceDetail) => {
  //     }
  //   )
  // }

  // mealService(detail: NurseRecord){
  //   let dialogRef = this.mealOrderNavigator.addMealOrder(detail.encounter_id)
  //   dialogRef.afterClosed().subscribe(
  //     (otherService: OrderedOtheServiceDetail) => {
  //     }
  //   )
  // }

}
