import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import { BedPersist } from "../bed/beds/bed.persist";


// import {BirthEditComponent} from "./birth-edit/birth-edit.component";
// import {BirthPickComponent} from "./birth-pick/birth-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Nurse_RecordNavigator {
  selected_index:number;

  constructor(private router: Router,
              public dialog: MatDialog,    
              public bedPersist: BedPersist,  
              ) {
  }

  nurseNavigatorUrl(): string {
    return "/nurse_record";
  }

  nurseRecordDetailUrl(): string {
    return "/nurse_record_detail";
  }

  recordUrl(id: string): string {
    return "/nurse_record/" + id;
  }

  viewNurseRecord(): void {
    this.router.navigateByUrl(this.nurseNavigatorUrl());
  }

  viewRecordDetail(pid: String): void {
    // this.bedPersist.bedsDo("single_bed_record", {pid}).subscribe((result:any) => {
    //   // this.patientStatistics = result;
    //   console.log("single detail value is ",result)
    //   // this.nurseRecord = result;
    // });
    this.router.navigateByUrl(this.nurseRecordDetailUrl());
  }
//   viewBirth(id: string): void {
//     this.router.navigateByUrl(this.birthUrl(id));
//   }

//   editBirth(id: string): MatDialogRef<BirthEditComponent> {
//     const dialogRef = this.dialog.open(BirthEditComponent, {
//       data: new TCIdMode(id, TCModalModes.EDIT),
//       width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }

//   addBirth(): MatDialogRef<BirthEditComponent> {
//     const dialogRef = this.dialog.open(BirthEditComponent, {
//           data: new TCIdMode(null, TCModalModes.NEW),
//           width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }
  
//    pickBirths(selectOne: boolean=false): MatDialogRef<BirthPickComponent> {
//       const dialogRef = this.dialog.open(BirthPickComponent, {
//         data: selectOne,
//         width: TCModalWidths.large
//       });
//       return dialogRef;
//     }
}
