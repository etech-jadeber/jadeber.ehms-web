// import {TCId} from "../tc/models";

export class NurseRecord {
ward : String;
room_no : string;
bed_no : number;
first_name: string;
middle_name: string;
last_name: string;
sex: string;
department: string;
department_speciality: string;
admission_date: number;
pid: string;
encounter_id: string;
next_of_kin_name: string;
next_of_kin_address: string;
next_of_kin_email: string;
next_of_kin_phone: string;
patient_name:string;
admission_form_id: string;
start_date: number;
department_speciality_id: string;
dob: number;
}

export class Prescription {
date_added : number;
drug : string;
note : string;
quantity: number;
unit: number;
}