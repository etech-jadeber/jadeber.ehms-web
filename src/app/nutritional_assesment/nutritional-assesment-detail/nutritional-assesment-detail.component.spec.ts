import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionalAssesmentDetailComponent } from './nutritional-assesment-detail.component';

describe('NutritionalAssesmentDetailComponent', () => {
  let component: NutritionalAssesmentDetailComponent;
  let fixture: ComponentFixture<NutritionalAssesmentDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutritionalAssesmentDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionalAssesmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
