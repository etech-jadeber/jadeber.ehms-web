import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {NutritionalAssesmentDetail} from "../nutritional_assesment.model";
import {NutritionalAssesmentPersist} from "../nutritional_assesment.persist";
import {NutritionalAssesmentNavigator} from "../nutritional_assesment.navigator";

export enum NutritionalAssesmentTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-nutritional-assesment-detail',
  templateUrl: './nutritional-assesment-detail.component.html',
  styleUrls: ['./nutritional-assesment-detail.component.scss']
})
export class NutritionalAssesmentDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  nutritionalAssesmentIsLoading:boolean = false;
  
  NutritionalAssesmentTabs: typeof NutritionalAssesmentTabs = NutritionalAssesmentTabs;
  activeTab: NutritionalAssesmentTabs = NutritionalAssesmentTabs.overview;
  //basics
  nutritionalAssesmentDetail: NutritionalAssesmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public nutritionalAssesmentNavigator: NutritionalAssesmentNavigator,
              public  nutritionalAssesmentPersist: NutritionalAssesmentPersist) {
    this.tcAuthorization.requireRead("nutritional_assesments");
    this.nutritionalAssesmentDetail = new NutritionalAssesmentDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("nutritional_assesments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.nutritionalAssesmentIsLoading = true;
    this.nutritionalAssesmentPersist.getNutritionalAssesment(id).subscribe(nutritionalAssesmentDetail => {
          this.nutritionalAssesmentDetail = nutritionalAssesmentDetail;
          this.nutritionalAssesmentIsLoading = false;
        }, error => {
          console.error(error);
          this.nutritionalAssesmentIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.nutritionalAssesmentDetail.id);
  }
  editNutritionalAssesment(): void {
    let modalRef = this.nutritionalAssesmentNavigator.editNutritionalAssesment(this.nutritionalAssesmentDetail.id);
    modalRef.afterClosed().subscribe(nutritionalAssesmentDetail => {
      TCUtilsAngular.assign(this.nutritionalAssesmentDetail, nutritionalAssesmentDetail);
    }, error => {
      console.error(error);
    });
  }

   print():void{
      this.nutritionalAssesmentPersist.print(this.nutritionalAssesmentDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print nutritional_assesment", true);
      });
    }

  back():void{
      this.location.back();
    }

}