import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionalAssesmentEditComponent } from './nutritional-assesment-edit.component';

describe('NutritionalAssesmentEditComponent', () => {
  let component: NutritionalAssesmentEditComponent;
  let fixture: ComponentFixture<NutritionalAssesmentEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutritionalAssesmentEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionalAssesmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
