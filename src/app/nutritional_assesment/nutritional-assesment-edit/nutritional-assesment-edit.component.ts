import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { NutritionalAssesmentDetail } from '../nutritional_assesment.model';import { NutritionalAssesmentPersist } from '../nutritional_assesment.persist';@Component({
  selector: 'app-nutritional-assesment-edit',
  templateUrl: './nutritional-assesment-edit.component.html',
  styleUrls: ['./nutritional-assesment-edit.component.scss']
})export class NutritionalAssesmentEditComponent implements OnInit {


  nutritionalAssesmentsDisplayedColumns: string[] = ["action","edit", "assesment" ];
  isLoadingResults: boolean = false;
  title: string;
  nutritionalAssesmentDetailList: NutritionalAssesmentDetail[] = [];
  nutritionalAssesmentDetail: NutritionalAssesmentDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<NutritionalAssesmentEditComponent>,
              public  persist: NutritionalAssesmentPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("nutritional_assesments");
      this.title = this.appTranslation.getText("general","new") +  " " + "nutritional_assesment";
      this.nutritionalAssesmentDetail = new NutritionalAssesmentDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("nutritional_assesments");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "nutritional_assesment";
      this.isLoadingResults = true;
      this.persist.getNutritionalAssesment(this.idMode.id).subscribe(nutritionalAssesmentDetail => {
        this.nutritionalAssesmentDetail = nutritionalAssesmentDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  AddToList():void{
    this.nutritionalAssesmentDetailList=[this.nutritionalAssesmentDetail,...this.nutritionalAssesmentDetailList];
    this.nutritionalAssesmentDetail=new NutritionalAssesmentDetail();
  }

  removeFromList(item:NutritionalAssesmentDetail):void{
    this.nutritionalAssesmentDetailList=this.nutritionalAssesmentDetailList.filter((value)=> value != item);
  }
  editFromList(item:NutritionalAssesmentDetail){
    this.removeFromList(item);
    this.nutritionalAssesmentDetail=item;
  }

  canSave():boolean {
    if (this.nutritionalAssesmentDetailList.length==0){
      return false
    }
    return true;
  }

  onAdd(): void {
    this.isLoadingResults = true;
    for( let nutritional_assesment of this.nutritionalAssesmentDetailList){
      this.persist.addNutritionalAssesment(this.idMode.id,nutritional_assesment).subscribe(value => {
        this.tcNotification.success("nutritionalAssesment added");
        this.nutritionalAssesmentDetail.id = value.id;
        this.dialogRef.close(this.nutritionalAssesmentDetail);
      }, error => {
        this.isLoadingResults = false;
      })
    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateNutritionalAssesment(this.nutritionalAssesmentDetail).subscribe(value => {
      this.tcNotification.success("nutritional_assesment updated");
      this.dialogRef.close(this.nutritionalAssesmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  canSubmit():boolean{
      if (this.nutritionalAssesmentDetail == null){
          return false;
        }

      if (this.nutritionalAssesmentDetail.assesment == null || this.nutritionalAssesmentDetail.assesment == ""){
        return false;
      }

      return true;

 }
 }