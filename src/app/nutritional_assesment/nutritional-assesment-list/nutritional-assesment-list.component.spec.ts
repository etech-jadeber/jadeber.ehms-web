import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionalAssesmentListComponent } from './nutritional-assesment-list.component';

describe('NutritionalAssesmentListComponent', () => {
  let component: NutritionalAssesmentListComponent;
  let fixture: ComponentFixture<NutritionalAssesmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutritionalAssesmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionalAssesmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
