import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { NutritionalAssesmentSummary, NutritionalAssesmentSummaryPartialList } from '../nutritional_assesment.model';
import { NutritionalAssesmentPersist } from '../nutritional_assesment.persist';
import { NutritionalAssesmentNavigator } from '../nutritional_assesment.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
@Component({
  selector: 'app-nutritional-assesment-list',
  templateUrl: './nutritional-assesment-list.component.html',
  styleUrls: ['./nutritional-assesment-list.component.scss']
})export class NutritionalAssesmentListComponent implements OnInit {
  nutritionalAssesmentsData: NutritionalAssesmentSummary[] = [];
  nutritionalAssesmentsTotalCount: number = 0;
  nutritionalAssesmentSelectAll:boolean = false;
  nutritionalAssesmentSelection: NutritionalAssesmentSummary[] = [];

  nutritionalAssesmentsDisplayedColumns: string[] = ["select","action", "assesment","nurse_id","date" ];
  nutritionalAssesmentSearchTextBox: FormControl = new FormControl();
  nutritionalAssesmentIsLoading: boolean = false;  


  @Input() encounterId:any;
  @Input() nurseId:any;
  @Input() date:any;
  user : {[id:string]:UserDetail} = {}
  @ViewChild(MatPaginator, {static: true}) nutritionalAssesmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) nutritionalAssesmentsSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public nutritionalAssesmentPersist: NutritionalAssesmentPersist,
                public nutritionalAssesmentNavigator: NutritionalAssesmentNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public userPersist: UserPersist,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("nutritional_assesments");
       this.nutritionalAssesmentSearchTextBox.setValue(nutritionalAssesmentPersist.nutritionalAssesmentSearchText);
      //delay subsequent keyup events
      this.nutritionalAssesmentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.nutritionalAssesmentPersist.nutritionalAssesmentSearchText = value;
        this.searchNutritional_assesments();
      });
    } ngOnInit() {
   
      this.nutritionalAssesmentsSort.sortChange.subscribe(() => {
        this.nutritionalAssesmentsPaginator.pageIndex = 0;
        this.searchNutritional_assesments(true);
      });

      this.nutritionalAssesmentsPaginator.page.subscribe(() => {
        this.searchNutritional_assesments(true);
      });
      //start by loading items
      this.searchNutritional_assesments();
    }

  searchNutritional_assesments(isPagination:boolean = false): void {


    let paginator = this.nutritionalAssesmentsPaginator;
    let sorter = this.nutritionalAssesmentsSort;

    this.nutritionalAssesmentIsLoading = true;
    this.nutritionalAssesmentSelection = [];

    this.nutritionalAssesmentPersist.date = this.date? this.tcUtilsDate.toTimeStamp(this.date) : undefined;
    this.nutritionalAssesmentPersist.nurse_id = this.nurseId;
    this.nutritionalAssesmentPersist.searchNutritionalAssesment(this.encounterId,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: NutritionalAssesmentSummaryPartialList) => {
      this.nutritionalAssesmentsData = partialList.data;
      this.nutritionalAssesmentsData.forEach((nutritionalAssesment)=>{
        if(nutritionalAssesment){
          if(!this.user[nutritionalAssesment.nurse_id]){
            this.userPersist.getUser(nutritionalAssesment.nurse_id).subscribe((_user)=>{
              if(_user){
                this.user[nutritionalAssesment.nurse_id] = _user;
              }
            })
          }
        }
      })
      if (partialList.total != -1) {
        this.nutritionalAssesmentsTotalCount = partialList.total;
      }
      this.nutritionalAssesmentIsLoading = false;
    }, error => {
      this.nutritionalAssesmentIsLoading = false;
    });

  } downloadNutritionalAssesments(): void {
    if(this.nutritionalAssesmentSelectAll){
         this.nutritionalAssesmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download nutritional_assesment", true);
      });
    }
    else{
        this.nutritionalAssesmentPersist.download(this.tcUtilsArray.idsList(this.nutritionalAssesmentSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download nutritional_assesment",true);
            });
        }
  }
addNutritional_assesment(): void {
    let dialogRef = this.nutritionalAssesmentNavigator.addNutritionalAssesment(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchNutritional_assesments();
      }
    });
  }

  editNutritionalAssesment(item: NutritionalAssesmentSummary) {
    let dialogRef = this.nutritionalAssesmentNavigator.editNutritionalAssesment(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteNutritionalAssesment(item: NutritionalAssesmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("nutritional_assesment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.nutritionalAssesmentPersist.deleteNutritionalAssesment(item.id).subscribe(response => {
          this.tcNotification.success("nutritional_assesment deleted");
          this.searchNutritional_assesments();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}