import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionalAssesmentPickComponent } from './nutritional-assesment-pick.component';

describe('NutritionalAssesmentPickComponent', () => {
  let component: NutritionalAssesmentPickComponent;
  let fixture: ComponentFixture<NutritionalAssesmentPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutritionalAssesmentPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionalAssesmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
