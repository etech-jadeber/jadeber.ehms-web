import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import { NutritionalAssesmentSummary, NutritionalAssesmentSummaryPartialList } from '../nutritional_assesment.model';
import { NutritionalAssesmentPersist } from '../nutritional_assesment.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-nutritional-assesment-pick',
  templateUrl: './nutritional-assesment-pick.component.html',
  styleUrls: ['./nutritional-assesment-pick.component.scss'],
})
export class NutritionalAssesmentPickComponent implements OnInit {
  nutritionalAssesmentsData: NutritionalAssesmentSummary[] = [];
  nutritionalAssesmentsTotalCount: number = 0;
  nutritionalAssesmentSelectAll: boolean = false;
  nutritionalAssesmentsSelection: NutritionalAssesmentSummary[] = [];

  nutritionalAssesmentsDisplayedColumns: string[] = [
    'select',
    'action',
    'date',
    'nurse_id',
  ];
  nutritionalAssesmentSearchTextBox: FormControl = new FormControl();
  nutritionalAssesmentIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  nutritionalAssesmentsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) nutritionalAssesmentsSort: MatSort;
nutritionalAssesmentNavigator: any;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<NutritionalAssesmentPickComponent>,
    public nutritionalAssesmentPersist: NutritionalAssesmentPersist,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('nutritional_assesments');
    this.nutritionalAssesmentSearchTextBox.setValue(
      nutritionalAssesmentPersist.nutritionalAssesmentSearchText
    );
    //delay subsequent keyup events
    this.nutritionalAssesmentSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.nutritionalAssesmentPersist.nutritionalAssesmentSearchText = value;
        this.searchNutritional_assesments();
      });
  }
  ngOnInit() {
    this.nutritionalAssesmentsSort.sortChange.subscribe(() => {
      this.nutritionalAssesmentsPaginator.pageIndex = 0;
      this.searchNutritional_assesments(true);
    });

    this.nutritionalAssesmentsPaginator.page.subscribe(() => {
      this.searchNutritional_assesments(true);
    });
    //start by loading items
    this.searchNutritional_assesments();
  }

  searchNutritional_assesments(isPagination: boolean = false): void {
    let paginator = this.nutritionalAssesmentsPaginator;
    let sorter = this.nutritionalAssesmentsSort;

    this.nutritionalAssesmentIsLoading = true;
    this.nutritionalAssesmentsSelection = [];

    this.nutritionalAssesmentPersist
      .searchNutritionalAssesment("",
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: NutritionalAssesmentSummaryPartialList) => {
          this.nutritionalAssesmentsData = partialList.data;
          if (partialList.total != -1) {
            this.nutritionalAssesmentsTotalCount = partialList.total;
          }
          this.nutritionalAssesmentIsLoading = false;
        },
        (error) => {
          this.nutritionalAssesmentIsLoading = false;
        }
      );
  }
  markOneItem(item: NutritionalAssesmentSummary) {
    if (
      !this.tcUtilsArray.containsId(
        this.nutritionalAssesmentsSelection,
        item.id
      )
    ) {
      this.nutritionalAssesmentsSelection = [];
      this.nutritionalAssesmentsSelection.push(item);
    } else {
      this.nutritionalAssesmentsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.nutritionalAssesmentsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
