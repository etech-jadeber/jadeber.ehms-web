import { TCId } from '../tc/models';

export class NutritionalAssesmentSummary extends TCId {
  assesment: string;
  date: number;
  nurse_id: string;
}
export class NutritionalAssesmentSummaryPartialList {
  data: NutritionalAssesmentSummary[];
  total: number;
}
export class NutritionalAssesmentDetail extends NutritionalAssesmentSummary {}

export class NutritionalAssesmentDashboard {
  total: number;
}
