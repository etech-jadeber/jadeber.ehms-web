import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { NutritionalAssesmentEditComponent } from "./nutritional-assesment-edit/nutritional-assesment-edit.component";
import { NutritionalAssesmentPickComponent } from "./nutritional-assesment-pick/nutritional-assesment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class NutritionalAssesmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  nutritional_assesmentsUrl(): string {
    return "/nutritional_assesments";
  }

  nutritional_assesmentUrl(id: string): string {
    return "/nutritional_assesments/" + id;
  }

  viewNutritionalAssesments(): void {
    this.router.navigateByUrl(this.nutritional_assesmentsUrl());
  }

  viewNutritionalAssesment(id: string): void {
    this.router.navigateByUrl(this.nutritional_assesmentUrl(id));
  }

  editNutritionalAssesment(id: string): MatDialogRef<NutritionalAssesmentEditComponent> {
    const dialogRef = this.dialog.open(NutritionalAssesmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addNutritionalAssesment(parent_id:string): MatDialogRef<NutritionalAssesmentEditComponent> {
    const dialogRef = this.dialog.open(NutritionalAssesmentEditComponent, {
          data: new TCIdMode(parent_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickNutritionalAssesments(selectOne: boolean=false): MatDialogRef<NutritionalAssesmentPickComponent> {
      const dialogRef = this.dialog.open(NutritionalAssesmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}