import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {NutritionalAssesmentDashboard, NutritionalAssesmentDetail, NutritionalAssesmentSummaryPartialList} from "./nutritional_assesment.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class NutritionalAssesmentPersist {
 nutritionalAssesmentSearchText: string = "";
  date: number;
  nurse_id:string;
 
 constructor(private http: HttpClient) {
  }
  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.nutritionalAssesmentSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchNutritionalAssesment(parent_id:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<NutritionalAssesmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + parent_id +"/nutritional_assesment", this.nutritionalAssesmentSearchText, pageSize, pageIndex, sort, order);    
    if(this.date)
      url = TCUtilsString.appendUrlParameter(url, "date",this.date.toString());
    if(this.nurse_id)
      url = TCUtilsString.appendUrlParameter(url, "nurse_id",this.nurse_id);
    return this.http.get<NutritionalAssesmentSummaryPartialList>(url);

  } 
  nutritionalAssesmentDashboard(): Observable<NutritionalAssesmentDashboard> {
    return this.http.get<NutritionalAssesmentDashboard>(environment.tcApiBaseUri + "nutritional_assesment/dashboard");
  }

  getNutritionalAssesment(id: string): Observable<NutritionalAssesmentDetail> {
    return this.http.get<NutritionalAssesmentDetail>(environment.tcApiBaseUri + "nutritional_assesment/" + id);
  }


  addNutritionalAssesment(parent_id:string, item: NutritionalAssesmentDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "form_encounter/" + parent_id +"/nutritional_assesment",
      item
    );
  }

  updateNutritionalAssesment(item: NutritionalAssesmentDetail): Observable<NutritionalAssesmentDetail> {
    return this.http.patch<NutritionalAssesmentDetail>(
      environment.tcApiBaseUri + 'nutritional_assesment/' + item.id,
      item
    );
  }

  deleteNutritionalAssesment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'nutritional_assesment/' + id);
  }
  nutritionalAssesmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nutritional_assesment/do',
      new TCDoParam(method, payload)
    );
  }

  nutritionalAssesmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'nutritional_assesment/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }



  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'nutritional_assesment/do',
      new TCDoParam('download', ids)
    );
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nutritional_assesment/do", new TCDoParam("download_all", this.filters()));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "nutritional_assesment/" + id + "/do", new TCDoParam("print", {}));
  }

  
 }