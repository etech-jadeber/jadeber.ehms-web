import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpdObservationDetailComponent } from './opd-observation-detail.component';

describe('OpdObservationDetailComponent', () => {
  let component: OpdObservationDetailComponent;
  let fixture: ComponentFixture<OpdObservationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpdObservationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpdObservationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
