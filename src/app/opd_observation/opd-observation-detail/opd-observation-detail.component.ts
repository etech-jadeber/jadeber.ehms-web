import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { OpdObservationDetail } from '../opd_observation.model';
import { OpdObservationNavigator } from '../opd_observation.navigator';
import { OpdObservationPersist } from '../opd_observationpersist';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { InstructionType, physican_type, prescription_type, store_type } from 'src/app/app.enums';
import { FormControl } from '@angular/forms';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { OrdersDetail } from 'src/app/form_encounters/orderss/orders.model';
import { GeneralPatientDetail, PatientDetail } from 'src/app/patients/patients.model';



export enum OpdObservationTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-opd-observation-detail',
  templateUrl: './opd-observation-detail.component.html',
  styleUrls: ['./opd-observation-detail.component.scss']
})
export class OpdObservationDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  opdObservationIsLoading:boolean = false;
  instructionType = InstructionType;
  nurseName:string;
  nurseId : string;
  ordersDetail: OrdersDetail;
  prescription_type = prescription_type;
  patientDetail: GeneralPatientDetail = new GeneralPatientDetail();
  store_type = store_type;
  date:FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  
  OpdObservationTabs: typeof OpdObservationTabs = OpdObservationTabs;
  activeTab: OpdObservationTabs = OpdObservationTabs.overview;
  //basics
  opdObservationDetail: OpdObservationDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public doctorNavigator: DoctorNavigator,
              public opdObservationNavigator: OpdObservationNavigator,
              public  opdObservationPersist: OpdObservationPersist) {
    this.tcAuthorization.requireRead("opd_observations");
    this.opdObservationDetail = new OpdObservationDetail();
    this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.refresh();
    });
  } ngOnInit() {
    this.tcAuthorization.requireRead("opd_observations");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.opdObservationIsLoading = true;
    this.opdObservationPersist.getOpdObservation(id).subscribe(opdObservationDetail => {
          this.opdObservationDetail = opdObservationDetail;
          this.opdObservationDetail.age = new Date().getFullYear()-new Date(opdObservationDetail.dob*1000).getFullYear();
          this.patientDetail.dob = this.opdObservationDetail.dob;
          this.patientDetail.sex = this.opdObservationDetail.sex;
          this.patientDetail.patient_name = this.opdObservationDetail.patient_name;
          this.opdObservationIsLoading = false;
        }, error => {
          console.error(error);
          this.opdObservationIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.opdObservationDetail.id);
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.opdObservationPersist.selected_index=matTab.index;
  }

  searchNurses():void{
    let dialogRef = this.doctorNavigator.pickDoctors(true,physican_type.nurse);

    dialogRef.afterClosed().subscribe((nurse:DoctorSummary[])=>{
      if(nurse){
        this.nurseName = nurse[0].first_name + " " + nurse[0].middle_name;
        this.nurseId = nurse[0].login_id;
        this.refresh();
      }
    })
  }
  handleBackButton(){
    this.ordersDetail = null;
  }

  refresh():void{
    let encounter_id = this.opdObservationDetail.encounter_id;
    this.opdObservationDetail.encounter_id = undefined;
    setTimeout(() => {
      this.opdObservationDetail.encounter_id = encounter_id;
    }, 1);
  }

  loadOrdersDetail(ordersDetail: OrdersDetail) {
    this.ordersDetail = ordersDetail
  }


   printBirth():void{
      this.opdObservationPersist.print(this.opdObservationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print opd_observation", true);
      });
    }

  back():void{
      this.location.back();
    }

}