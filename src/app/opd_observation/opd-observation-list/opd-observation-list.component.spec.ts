import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpdObservationListComponent } from './opd-observation-list.component';

describe('OpdObservationListComponent', () => {
  let component: OpdObservationListComponent;
  let fixture: ComponentFixture<OpdObservationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpdObservationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpdObservationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
