import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { OpdObservationSummary, OpdObservationSummaryPartialList } from '../opd_observation.model';
import { OpdObservationNavigator } from '../opd_observation.navigator';
import { OpdObservationPersist } from '../opd_observationpersist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';
import { EmergencyObservationSummary } from 'src/app/emergency_observation/emergency_observation.model';
import { OrderedOtheServiceDetail } from 'src/app/ordered_othe_service/ordered_othe_service.model';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { OrderedOtheServiceNavigator } from 'src/app/ordered_othe_service/ordered_othe_service.navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import {encounterFilterColumn, Form_EncounterDetail} from 'src/app/form_encounters/form_encounter.model';
import {PatientType, service_type, ServiceType} from "../../app.enums";
import { PatientPersist } from 'src/app/patients/patients.persist';
@Component({
  selector: 'app-opd-observation-list',
  templateUrl: './opd-observation-list.component.html',
  styleUrls: ['./opd-observation-list.component.scss']
})
export class OpdObservationListComponent implements OnInit {
  opdObservationsData: OpdObservationSummary[] = [];
  opdObservationsTotalCount: number = 0;
  opdObservationSelectAll:boolean = false;
  opdObservationSelection: OpdObservationSummary[] = [];
  opdObservationFilterColumn = encounterFilterColumn

  opdObservationsDisplayedColumns: string[] = ["select", "action", "p.pid", "patient_name","schedule_type",'department','date' ];
  opdObservationSearchTextBox: FormControl = new FormControl();
  opdObservationIsLoading: boolean = false;
  date: FormControl = new FormControl({disabled: true, value: this.tcUtilsDate.toFormDate(this.opdObservationPersist.opdObservationSearchHistory.date)})


  @ViewChild(MatPaginator, {static: true}) opdObservationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) opdObservationsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public form_encounterPersist: Form_EncounterPersist,
                public appointmentPersist: AppointmentPersist,
                public opdObservationPersist: OpdObservationPersist,
                public opdObservationNavigator: OpdObservationNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public patientPersist: PatientPersist,
                public otherServiceNaviagator: OtherServicesNavigator,
                public orderedOtherServiceNavigator: OrderedOtheServiceNavigator,
                public procedureOrderNavigator: Form_EncounterNavigator,

    ) {

        this.tcAuthorization.requireRead("opd_observations");
        // opdObservationPersist.date.setValue(new Date());
       this.opdObservationSearchTextBox.setValue(opdObservationPersist.opdObservationSearchHistory.search_text);
      //delay subsequent keyup events
      this.opdObservationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.opdObservationPersist.opdObservationSearchHistory.search_text = value;
        this.searchOpd_observations();
      });
      this.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.opdObservationPersist.opdObservationSearchHistory.date = value ? this.tcUtilsDate.toTimeStamp(value ? value : value._d) : null
        this.searchOpd_observations()
      })

    } ngOnInit() {

      this.opdObservationsSort.sortChange.subscribe(() => {
        this.opdObservationsPaginator.pageIndex = 0;
        this.searchOpd_observations(true);
      });

      this.opdObservationsPaginator.page.subscribe(() => {
        this.searchOpd_observations(true);
      });
      //start by loading items
      this.searchOpd_observations();
    }

  searchOpd_observations(isPagination:boolean = false): void {


    let paginator = this.opdObservationsPaginator;
    let sorter = this.opdObservationsSort;

    this.opdObservationIsLoading = true;
    this.opdObservationSelection = [];

    this.opdObservationPersist.searchOpdObservation(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OpdObservationSummaryPartialList) => {
      this.opdObservationsData = partialList.data;
      if (partialList.total != -1) {
        this.opdObservationsTotalCount = partialList.total;
      }
      this.opdObservationIsLoading = false;
    }, error => {
      this.opdObservationIsLoading = false;
    });

  }
  back():void{
      this.location.back();
    }

    ngOnDestroy():void{
      // this.opdObservationPersist.date.setValue('')
    }

  // ambulanceService(detail: Form_EncounterDetail) {
  //   let dialogRef = this.ambulanceServiceNavigator.addAmbulanceService(detail.id)
  //   dialogRef.afterClosed().subscribe(
  //     (otherService: OrderedOtheServiceDetail) => {
  //     }
  //   )
  // }

    additionalService(detail: OpdObservationSummary){
      let dialogRef = this.orderedOtherServiceNavigator.addOrderedOtheService(detail.id)
      dialogRef.afterClosed().subscribe(
        (otherService: OrderedOtheServiceDetail) => {
          if (otherService){
            this.searchOpd_observations()
          }
        }
      )
    }

    procedureOrder(detail: OpdObservationSummary){
      let dialogRef = this.procedureOrderNavigator.addPatient_Procedure(
        detail.id
      );
      dialogRef.afterClosed().subscribe((newPatient_Procedure) => {
      });
    }

    // oxygenService(detail: OpdObservationSummary){
    //   let dialogRef = this.oxygenServiceNavigator.addOxygenService(detail.id)
    //   dialogRef.afterClosed().subscribe(
    //     (otherService: OrderedOtheServiceDetail) => {
    //     }
    //   )
    // }
}
