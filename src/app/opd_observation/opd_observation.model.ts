import { TCId } from '../tc/models';

export class OpdObservationSummary extends TCId {
  encounter_id: string;
  patient_id: string;
  patient_name:string;
  department: string;
  department_speciality:string;
  sex:boolean;
  dob:number;
  age:number;
  opd_contact_name:string;
  opd_contact_phone:string;
  date:number;
  pid:number;
}
export class OpdObservationSummaryPartialList {
  data: OpdObservationSummary[];
  total: number;
}
export class OpdObservationDetail extends OpdObservationSummary {}

export class OpdObservationDashboard {
  total: number;
}
