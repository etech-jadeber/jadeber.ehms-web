import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

// import {TCModalWidths} from "../tc/utils-angular";
// import {TCIdMode, TCModalModes} from "../tc/models";

// import {OpdObservationEditComponent} from "./opd_observation-edit/opd_observation-edit.component";
// import {OpdObservationPickComponent} from "./opd_observation-pick/opd_observation-pick.component";


@Injectable({
  providedIn: 'root'
})

export class OpdObservationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  opd_observationsUrl(): string {
    return "/opd_observations";
  }

  opd_observationUrl(id: string): string {
    return "/opd_observations/" + id;
  }

  viewOpdObservations(): void {
    this.router.navigateByUrl(this.opd_observationsUrl());
  }

  viewOpdObservation(id: string): void {
    this.router.navigateByUrl(this.opd_observationUrl(id));
  }

//   editOpdObservation(id: string): MatDialogRef<OpdObservationEditComponent> {
//     const dialogRef = this.dialog.open(OpdObservationEditComponent, {
//       data: new TCIdMode(id, TCModalModes.EDIT),
//       width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }

//   addOpdObservation(): MatDialogRef<OpdObservationEditComponent> {
//     const dialogRef = this.dialog.open(OpdObservationEditComponent, {
//           data: new TCIdMode(null, TCModalModes.NEW),
//           width: TCModalWidths.medium
//     });
//     return dialogRef;
//   }
  
//    pickOpdObservations(selectOne: boolean=false): MatDialogRef<OpdObservationPickComponent> {
//       const dialogRef = this.dialog.open(OpdObservationPickComponent, {
//         data: selectOne,
//         width: TCModalWidths.large
//       });
//       return dialogRef;
//     }
}