import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCEnumTranslation, TCId, TCString, TcDictionary} from "../tc/models";
import {OpdObservationDashboard, OpdObservationDetail, OpdObservationSummaryPartialList} from "./opd_observation.model";
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';
import { encounter_status, schedule_type } from '../app.enums';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class OpdObservationPersist {
//  opdObservationSearchText: string = "";
 selected_index:number;
//  date:FormControl = new FormControl({disabled: true, value: ''});
//  encounterStatusFilter:number =  encounter_status.opened;
//  service_type: number;

 constructor(private http: HttpClient,
      public tcUtilsDate: TCUtilsDate,
      private appTranslation: AppTranslation,
  ) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "opd_observation/" + id + "/do", new TCDoParam("print", {}));
  }  filters(searchHistory = this.opdObservationSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  opdObservationSearchHistory = {
    "date": this.tcUtilsDate.toTimeStamp(new Date()),
    "encounter_status": undefined,
    "service_type": undefined,
    "search_text": "",
    "search_column": "",
}

defaultOpdObservationSearchHistory = {...this.opdObservationSearchHistory}

resetOpdObservationSearchHistory(){
  this.opdObservationSearchHistory = {...this.defaultOpdObservationSearchHistory}
}
 
  searchOpdObservation(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.opdObservationSearchHistory): Observable<OpdObservationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("opd_observation", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);
    // if(this.date.value)
    //   url = TCUtilsString.appendUrlParameter(url, "date",Math.round(this.date.value / 1000).toString() )
    // if(this.encounterStatusFilter)
    // {url =  TCUtilsString.appendUrlParameter(url, "encounter_status", this.encounterStatusFilter.toString());}
    // if(this.service_type)
    // {url =  TCUtilsString.appendUrlParameter(url, "service_type", this.service_type.toString());}
    return this.http.get<OpdObservationSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "opd_observation/do", new TCDoParam("download_all", this.filters()));
  }

  opdObservationDashboard(): Observable<OpdObservationDashboard> {
    return this.http.get<OpdObservationDashboard>(environment.tcApiBaseUri + "opd_observation/dashboard");
  }

  getOpdObservation(id: string): Observable<OpdObservationDetail> {
    return this.http.get<OpdObservationDetail>(environment.tcApiBaseUri + "opd_observation/" + id);
  }

  addOpdObservation(item: OpdObservationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "opd_observation/", item);
  }


 }