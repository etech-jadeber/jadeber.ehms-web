import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OpenemrUserPickComponent } from './openemr-user-pick.component';

describe('OpenemrUserPickComponent', () => {
  let component: OpenemrUserPickComponent;
  let fixture: ComponentFixture<OpenemrUserPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenemrUserPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenemrUserPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
