import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Openemr_UserDetail, Openemr_UserSummary, Openemr_UserSummaryPartialList} from "../openemr_user.model";
import {Openemr_UserPersist} from "../openemr_user.persist";


@Component({
  selector: 'app-openemr_user-pick',
  templateUrl: './openemr-user-pick.component.html',
  styleUrls: ['./openemr-user-pick.component.css']
})
export class Openemr_UserPickComponent implements OnInit {

  openemr_usersData: Openemr_UserSummary[] = [];
  openemr_usersTotalCount: number = 0;
  openemr_usersSelection: Openemr_UserSummary[] = [];
  openemr_usersDisplayedColumns: string[] = ["select", 'fname','lname','mname','openemr_id' ];

  openemr_usersSearchTextBox: FormControl = new FormControl();
  openemr_usersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) openemr_usersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) openemr_usersSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public openemr_userPersist: Openemr_UserPersist,
              public dialogRef: MatDialogRef<Openemr_UserPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("openemr_users");
    this.openemr_usersSearchTextBox.setValue(openemr_userPersist.openemr_userSearchText);
    //delay subsequent keyup events
    this.openemr_usersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.openemr_userPersist.openemr_userSearchText = value;
      this.searchOpenemr_Users();
    });
  }

  ngOnInit() {

    this.openemr_usersSort.sortChange.subscribe(() => {
      this.openemr_usersPaginator.pageIndex = 0;
      this.searchOpenemr_Users();
    });

    this.openemr_usersPaginator.page.subscribe(() => {
      this.searchOpenemr_Users();
    });

    //set initial picker list to 5
    this.openemr_usersPaginator.pageSize = 5;

    //start by loading items
    this.searchOpenemr_Users();
  }

  searchOpenemr_Users(): void {
    this.openemr_usersIsLoading = true;
    this.openemr_usersSelection = [];

    this.openemr_userPersist.searchOpenemr_User(this.openemr_usersPaginator.pageSize,
        this.openemr_usersPaginator.pageIndex,
        this.openemr_usersSort.active,
        this.openemr_usersSort.direction).subscribe((partialList: Openemr_UserSummaryPartialList) => {
      this.openemr_usersData = partialList.data;
    
      if (partialList.total != -1) {
        this.openemr_usersTotalCount = partialList.total;
      }
      this.openemr_usersIsLoading = false;
    }, error => {
      this.openemr_usersIsLoading = false;
    });

  }

  markOneItem(item: Openemr_UserSummary) {
    if(!this.tcUtilsArray.containsId(this.openemr_usersSelection,item.id)){
          this.openemr_usersSelection = [];
          this.openemr_usersSelection.push(item);
        }
        else{
          this.openemr_usersSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.openemr_usersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
