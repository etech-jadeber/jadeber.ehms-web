import {TCId} from "../tc/models";

export class Openemr_UserSummary extends TCId {
  fname : string;
lname : string;
mname : string;
openemr_id : number;
}

export class Openemr_UserSummaryPartialList {
  data: Openemr_UserSummary[];
  total: number;
}

export class Openemr_UserDetail extends Openemr_UserSummary {
  fname : string;
lname : string;
mname : string;
openemr_id : number;
}

export class Openemr_UserDashboard {
  total: number;
}
