import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import { Openemr_UserPickComponent } from "./openemr-user-pick/openemr-user-pick.component";



@Injectable({
  providedIn: 'root'
})

export class Openemr_UserNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  openemr_usersUrl(): string {
    return "/openemr_users";
  }

  openemr_userUrl(id: string): string {
    return "/openemr_users/" + id;
  }

  viewOpenemr_Users(): void {
    this.router.navigateByUrl(this.openemr_usersUrl());
  }

  viewOpenemr_User(id: string): void {
    this.router.navigateByUrl(this.openemr_userUrl(id));
  }

 
  
   pickOpenemr_Users(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Openemr_UserPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
