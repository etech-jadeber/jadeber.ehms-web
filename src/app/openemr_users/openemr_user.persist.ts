import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Openemr_UserDashboard, Openemr_UserDetail, Openemr_UserSummaryPartialList} from "./openemr_user.model";


@Injectable({
  providedIn: 'root'
})
export class Openemr_UserPersist {

  openemr_userSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchOpenemr_User(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Openemr_UserSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("openemr_users", this.openemr_userSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Openemr_UserSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.openemr_userSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "openemr_users/do", new TCDoParam("download_all", this.filters()));
  }

  openemr_userDashboard(): Observable<Openemr_UserDashboard> {
    return this.http.get<Openemr_UserDashboard>(environment.tcApiBaseUri + "openemr_users/dashboard");
  }

  getOpenemr_User(id: string): Observable<Openemr_UserDetail> {
    return this.http.get<Openemr_UserDetail>(environment.tcApiBaseUri + "openemr_users/" + id);
  }

  addOpenemr_User(item: Openemr_UserDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "openemr_users/", item);
  }

  updateOpenemr_User(item: Openemr_UserDetail): Observable<Openemr_UserDetail> {
    return this.http.patch<Openemr_UserDetail>(environment.tcApiBaseUri + "openemr_users/" + item.id, item);
  }

  deleteOpenemr_User(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "openemr_users/" + id);
  }

  openemr_usersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "openemr_users/do", new TCDoParam(method, payload));
  }

  openemr_userDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "openemr_users/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "openemr_users/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "openemr_users/" + id + "/do", new TCDoParam("print", {}));
  }


}
