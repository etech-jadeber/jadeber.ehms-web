import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperationNoteDetailComponent } from './operation-note-detail.component';

describe('OperationNoteDetailComponent', () => {
  let component: OperationNoteDetailComponent;
  let fixture: ComponentFixture<OperationNoteDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationNoteDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationNoteDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
