import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {OperationNoteDetail} from "../operation_note.model";
import {OperationNotePersist} from "../operation_note.persist";
import {OperationNoteNavigator} from "../operation_note.navigator";
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { ServiceType, SurgeryNoteWriters, tabs } from 'src/app/app.enums';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PrescriptionDrugNavigator } from 'src/app/form_encounters/prescriptions-edit/prescription-drug-navigator';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Patient_ProcedureDetail } from 'src/app/form_encounters/form_encounter.model';

export enum OperationNoteTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-operation-note-detail',
  templateUrl: './operation-note-detail.component.html',
  styleUrls: ['./operation-note-detail.component.css']
})
export class OperationNoteDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  operationNoteIsLoading:boolean = false;
  
  //basics
  surgeryNoteWriters = SurgeryNoteWriters;     
  OperationNoteTabs: typeof OperationNoteTabs = OperationNoteTabs;
  patient_detail : PatientDetail = new PatientDetail();
  activeTab: OperationNoteTabs = OperationNoteTabs.overview;
  operationNoteDetail: OperationNoteDetail;
  // @Input() id:string;  
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public operationNoteNavigator: OperationNoteNavigator,
              public  operationNotePersist: OperationNotePersist,
              public doctorPersist: DoctorPersist,
              public patientPersist: PatientPersist,
              public doctorNavigator: DoctorNavigator,
              public tcUtilsDate: TCUtilsDate,
              public patientNavigator: PatientNavigator,
              public menuState: MenuState,
              public prescriptionNavigator: Form_EncounterNavigator,
              public procedurePersist: ProcedurePersist,
              public formEncounterPersist: Form_EncounterPersist) {
    this.tcAuthorization.requireRead("operation_notes");
    this.operationNoteDetail = new OperationNoteDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  } 
  
  ngOnInit() {
    this.tcAuthorization.requireRead("operation_notes");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.operationNoteIsLoading = true;
    this.operationNotePersist.getOperationNote(id).subscribe(operationNoteDetail => {
          this.operationNoteDetail = operationNoteDetail;
            operationNoteDetail.surgeonsDetail = [];
            // this.doctorPersist.getDoctor(operationNoteDetail.sergeon_id).subscribe(value =>{
            //   operationNoteDetail.surgeonsDetail.push(value)
            // })
            // operationNoteDetail.surgeons.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['surgeon_id']).subscribe(value => {
            //     operationNoteDetail.surgeonsDetail.push(value)
            // }))
            this.patientPersist.getPatient(operationNoteDetail.patient_id).subscribe((patient) => {
              operationNoteDetail.patient_name = patient.fname + " " + patient.lname;
              this.patient_detail = patient;
            })
            operationNoteDetail.nursesDetail = [];
            // operationNoteDetail.nurses.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['nurse_id']).subscribe(value => {
            //     operationNoteDetail.nursesDetail.push(value)
            // }))
            operationNoteDetail.assistantsDetail = [];
            // operationNoteDetail.assistants.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['assistant_id']).subscribe(value => {
            //     operationNoteDetail.assistantsDetail.push(value)
            // }))
            operationNoteDetail.anaesthetistsDetail = [];
            // operationNoteDetail.anaesthetists.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['anaesthetist_id']).subscribe(value => {
            //     operationNoteDetail.anaesthetistsDetail.push(value)
            // }))
            operationNoteDetail.runner_nurseDetail = [];
            // operationNoteDetail.runner_nurses.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['runner_nurse_id']).subscribe(value => {
            //     console.log(value, "runner nurse");
            //     operationNoteDetail.runner_nurseDetail.push(value)
            // }))
            operationNoteDetail.proceduresDone = []
          this.formEncounterPersist.patient_proceduresDo("by_operation_note", {operation_note_id: id}).subscribe(
            (patientProcedures: Patient_ProcedureDetail[]) => {
              patientProcedures.forEach((patientProcedure) => {
                this.procedurePersist.getProcedure(patientProcedure.procedure_id).subscribe(procedure => {
                  this.operationNoteDetail.proceduresDone = [...this.operationNoteDetail.proceduresDone, procedure];
                })
              })
            }
          )
          this.operationNoteIsLoading = false;
          this.menuState.getDataResponse(this.operationNoteDetail);
        }, error => {
          console.error(error);
          this.operationNoteIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.operationNoteDetail.id);
  }
  editOperationNote(writer: number): void {
    let modalRef = this.operationNoteNavigator.editOperationNote(this.operationNoteDetail.id, writer, ServiceType.inpatient);
    modalRef.afterClosed().subscribe(operationNoteDetail => {
      TCUtilsAngular.assign(this.operationNoteDetail, operationNoteDetail);
      this.loadDetails(operationNoteDetail.id)
    }, error => {
      console.error(error);
    });
  }

  addPrescription(){

    let dialog = this.prescriptionNavigator.addPrescriptions(this.operationNoteDetail.encounter_id, true)
    dialog.afterClosed().subscribe(value => {
      if (value) {
        this.reload()
      }
    })
  }

   printBirth():void{
      this.operationNotePersist.print(this.operationNoteDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print operation_note", true);
      });
    }

  back():void{
      this.location.back();
    }
    calculateAge(dob: number): number {
      const current = new Date().getFullYear()   
      return current - new Date(dob*1000).getFullYear()  
    }

}