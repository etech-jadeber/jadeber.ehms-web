import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperationNoteEditComponent } from './operation-note-edit.component';

describe('OperationNoteEditComponent', () => {
  let component: OperationNoteEditComponent;
  let fixture: ComponentFixture<OperationNoteEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationNoteEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationNoteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
