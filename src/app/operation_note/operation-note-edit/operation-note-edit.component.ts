import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { OperationNoteDetail } from '../operation_note.model';
import { OperationNotePersist } from '../operation_note.persist';
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { Surgery_TypeNavigator } from 'src/app/surgery_types/surgery_type.navigator';
import { Surgery_TypeSummary } from 'src/app/surgery_types/surgery_type.model';
import { ProcedureDetail, ProcedureSummary } from 'src/app/procedures/procedure.model';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Surgery_AppointmentPersist } from 'src/app/surgery_appointments/surgery_appointment.persist';
import { ActivenessStatus, physican_type, SurgeryNoteWriters } from 'src/app/app.enums';
import { Patient_ProcedureDetail } from 'src/app/form_encounters/form_encounter.model';

@Component({
  selector: 'app-operation-note-edit',
  templateUrl: './operation-note-edit.component.html',
  styleUrls: ['./operation-note-edit.component.css']
})

export class OperationNoteEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientFullName: string;
  surgeonsNames: string;
  assistantsNames: string;
  nursesNames: string;
  runnerNursesNames: string;
  anaesthetistsNames: string;
  surgeryTypeName: string;
  operationNoteDetail: OperationNoteDetail;
  procedureName: string;
  surgeryNoteWriters = SurgeryNoteWriters;    
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OperationNoteEditComponent>,
              public  persist: OperationNotePersist,
              public patientNavigator: PatientNavigator,
              public tcUtilsDate: TCUtilsDate,
              public doctorNavigator: DoctorNavigator,
              public surgeryTypeNavigator: Surgery_TypeNavigator,
              private procedureNav:ProcedureNavigator,
              public doctorPersist: DoctorPersist,
              public patientPersist: PatientPersist,
              public procedurePersist: ProcedurePersist,
              public formEncounterPersist: Form_EncounterPersist,
              public surgeryAppointmentPersist: Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: {tcIdMode: TCIdMode, surgeryNoteWriter: number, service_tye}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.tcIdMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.tcIdMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.tcIdMode.mode === TCModalModes.CASE;
  }
  
  ngOnInit() {
    if (this.idMode.tcIdMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("operation_notes");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText('patient', "operation_note");
      this.operationNoteDetail = new OperationNoteDetail();
      this.operationNoteDetail.complication_during_operation = "";
      this.operationNoteDetail.encounter_id= this.idMode.tcIdMode.id;
      this.surgeryAppointmentPersist.getSurgery_Appointment(this.idMode.tcIdMode.id).subscribe(appointment => {
        this.operationNoteDetail.id = appointment.operation_note_id
        this.operationNoteDetail.encounter_id=appointment.encounter_id;
          if(this.operationNoteDetail.id){
            this.operationNoteDetailLoad(this.operationNoteDetail.id);
          }
        this.formEncounterPersist.getForm_Encounter(appointment.encounter_id).subscribe(encounter => {
          this.patientPersist.getPatient(encounter.pid).subscribe(
            patient => {this.patientFullName = patient.fname + " " + patient.lname
          this.operationNoteDetail.patient_id = patient.id
          }
          )
        })
        this.procedurePersist.getProcedure(appointment.planned_procedure).subscribe(procedure => {
          this.operationNoteDetail.surgery_type = procedure.id
          this.surgeryTypeName = procedure.name
          
        })
      })
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("operation_notes");
      this.title = this.appTranslation.getText("general","edit") +  " "  + this.appTranslation.getText('patient', "operation_note");
      this.isLoadingResults = true;
      this.operationNoteDetailLoad(this.idMode.tcIdMode.id)
    }
  }  
  

  operationNoteDetailLoad(operation_note_id: string){
    this.persist.getOperationNote(operation_note_id).subscribe(operationNoteDetail => {
      if (!operationNoteDetail.complication_during_operation){
        operationNoteDetail.complication_during_operation = "";
      }
      this.operationNoteDetail = {...operationNoteDetail};
      if (this.idMode.surgeryNoteWriter == SurgeryNoteWriters.surgeon)
      {
      // this.operationNoteDetail.surgeons = [];
          // this.surgeonsNames = "";
          // operationNoteDetail.surgeons.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['surgeon_id']).subscribe(doctor => {
          //     this.surgeonsNames += (this.surgeonsNames ? ", " : "") + doctor.first_name + doctor.last_name
          //     this.operationNoteDetail.surgeons = [...this.operationNoteDetail.surgeons, doctor.id];
          // }))
          // this.operationNoteDetail.nurses = [];
          // this.nursesNames = "";
          // operationNoteDetail.nurses.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['nurse_id']).subscribe(doctor => {
          //     this.nursesNames += (this.nursesNames ? ", " : "") + doctor.first_name + doctor.last_name
          //     this.operationNoteDetail.nurses = [...this.operationNoteDetail.nurses, doctor.id];
          // }))
          // this.operationNoteDetail.assistants = [];
          // this.assistantsNames = "";
          // operationNoteDetail.assistants.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['assistant_id']).subscribe(doctor => {
          //   this.assistantsNames += (this.assistantsNames ? ", " : "") + doctor.first_name + doctor.last_name
          //   this.operationNoteDetail.assistants = [...this.operationNoteDetail.assistants, doctor.id];
          // }))
          // this.operationNoteDetail.anaesthetists = [];
          // this.anaesthetistsNames = "";
          // operationNoteDetail.anaesthetists.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['anaesthetist_id']).subscribe(doctor => {
          //   this.anaesthetistsNames += (this.anaesthetistsNames ? ", " : "") + doctor.first_name + doctor.last_name
          //   this.operationNoteDetail.anaesthetists = [...this.operationNoteDetail.anaesthetists, doctor.id];
          // }))

          // this.operationNoteDetail.runner_nurses = [];
          // this.runnerNursesNames = "";
          // operationNoteDetail.runner_nurses.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['runner_nurse_id']).subscribe(doctor => {
          //   this.runnerNursesNames += (this.runnerNursesNames ? ", " : "") + doctor.first_name + doctor.last_name
          //   this.operationNoteDetail.runner_nurses = [...this.operationNoteDetail.runner_nurses, doctor.id];
          // }))
          this.operationNoteDetail.procedures = []
          this.procedureName = ""
          this.formEncounterPersist.patient_proceduresDo("by_operation_note", {operation_note_id}).subscribe(
            (patientProcedures: Patient_ProcedureDetail[]) => {
              patientProcedures.forEach((patientProcedure) => {
                this.procedurePersist.getProcedure(patientProcedure.procedure_id).subscribe(procedure => {
                  this.procedureName += (this.procedureName ? ", " : "") + procedure.name
                  this.operationNoteDetail.procedures = [...this.operationNoteDetail.procedures, procedure.id];
                })
              })
            }
          )
        }
      this.isLoadingResults = false;
    }, error => {
      console.log(error);
      this.isLoadingResults = false;
    })
  }

  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addOperationNote(this.operationNoteDetail).subscribe(value => {
      this.tcNotification.success("operationNote added");
      this.operationNoteDetail.id = value.id;
      this.dialogRef.close(this.operationNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateOperationNote(this.operationNoteDetail).subscribe(value => {
      this.tcNotification.success("operation_note updated");
      this.dialogRef.close(this.operationNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.operationNoteDetail == null){
            return false;
          }
        if (this.operationNoteDetail.surgeons == null || !this.operationNoteDetail.surgeons.length){
          return false;
        }
        // if (this.operationNoteDetail.nurses == null || !this.operationNoteDetail.nurses.length){
        //   return false;
        // }
        // if (this.operationNoteDetail.runner_nurses == null || !this.operationNoteDetail.runner_nurses.length){
        //   return false;
        // }
        // if (this.operationNoteDetail.assistants == null || !this.operationNoteDetail.assistants.length){
        //   return false;
        // }
        // if (this.operationNoteDetail.anaesthetists == null || !this.operationNoteDetail.anaesthetists.length){
        //   return false;
        // }
        // if (this.operationNoteDetail.patient_id == null || this.operationNoteDetail.patient_id == ''){
        //   return false;
        // }if (this.operationNoteDetail.surgery_type == null || this.operationNoteDetail.surgery_type == ''){
        //   return false;
        // }
        return true;
     }

 canUpdate(){
  if (this.operationNoteDetail == null){
    return false;
  }
  if (this.operationNoteDetail.patient_id == null || this.operationNoteDetail.patient_id == ''){
    return false;
  }
  if(!this.operationNoteDetail.procedures.length){
    return false;
  }
  // if (this.operationNoteDetail.surgeons == null || !this.operationNoteDetail.surgeons.length){
  //   return false;
  // }
  // if (this.operationNoteDetail.nurses == null || !this.operationNoteDetail.nurses.length){
  //   return false;
  // }
  // if (this.operationNoteDetail.runner_nurses == null || !this.operationNoteDetail.runner_nurses.length){
  //   return false;
  // }
  // if (this.operationNoteDetail.assistants == null || !this.operationNoteDetail.assistants.length){
  //   return false;
  // }
  // if (this.operationNoteDetail.anaesthetists == null || !this.operationNoteDetail.anaesthetists.length){
  //   return false;
  // }
  // if (this.operationNoteDetail.patient_id == null || this.operationNoteDetail.patient_id == ''){
  //   return false;
  // }if (this.operationNoteDetail.surgery_type == null || this.operationNoteDetail.surgery_type == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.incisoin == null || this.operationNoteDetail.incisoin == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.pre_op_diagnosis == null || this.operationNoteDetail.pre_op_diagnosis == ''){
  //   return false;
  // }

  // if (this.operationNoteDetail.procedure_done == null || this.operationNoteDetail.procedure_done == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.post_op_diagnosis == null || this.operationNoteDetail.post_op_diagnosis == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.post_op_order == null || this.operationNoteDetail.post_op_order == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.indecation_for_operation == null || this.operationNoteDetail.indecation_for_operation == ''){
  //   return false;
  // }if (this.operationNoteDetail.operation_procedure_findings == null || this.operationNoteDetail.operation_procedure_findings == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.complication_during_operation == null || this.operationNoteDetail.complication_during_operation == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.closure == null || this.operationNoteDetail.closure == ''){
  //   return false;
  // }
  // if (this.operationNoteDetail.drains == null || this.operationNoteDetail.drains == ''){
  //   return false;
  // }
  return true;
 }

 searchPatient() {
  let dialogRef = this.patientNavigator.pickPatients(true);
  dialogRef.afterClosed().subscribe((result: PatientSummary) => {
    if (result) {
      this.patientFullName = result[0].fname + ' ' + result[0].lname;
      this.operationNoteDetail.patient_id = result[0].id;
    }
  });
}

searchProcedure() {
  let dialogRef = this.procedureNav.pickProcedures(false, this.idMode.service_tye, ActivenessStatus.active);
  dialogRef.afterClosed().subscribe((result: ProcedureDetail[]) => {
    if (result) {
      this.operationNoteDetail.procedures = [];
      this.procedureName = result.map((value) => value.name).join(", ");
      result.forEach((res) => {
        this.operationNoteDetail.procedures.push(res.id);
      })
    }
  });
}

searchSergeons(){
  let dialogRef = this.doctorNavigator.pickDoctors(false,physican_type.doctor,null);
  dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
    if (result){
      // this.operationNoteDetail.surgeons = [];
      // this.surgeonsNames = "";
      // result.forEach((res) => {
      //   this.operationNoteDetail.surgeons.push(res.id);
      //   this.surgeonsNames += (this.surgeonsNames ? ", " : "") + res.first_name + " " + res.last_name 
      // })
    }
  });
}

searchAssistants(){
  let dialogRef = this.doctorNavigator.pickDoctors(false,physican_type.doctor,null);
  dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
    if (result){
      // this.operationNoteDetail.assistants = [];
      // this.assistantsNames = "";
      // result.forEach((res) => {
      //   this.operationNoteDetail.assistants.push(res.id);
      //   this.assistantsNames += (this.assistantsNames ? ", " : "") + res.first_name + " " + res.last_name 
      // })
    }
  });
}

searchNurses(){
  let dialogRef = this.doctorNavigator.pickDoctors(false,physican_type.nurse,null);
  dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
    if (result){
      // this.operationNoteDetail.nurses = [];
      // this.nursesNames = "";
      // result.forEach((res) => {
      //   this.operationNoteDetail.nurses.push(res.id);
      //   this.nursesNames += (this.nursesNames ? ", " : "") + res.first_name + " " + res.last_name 
      // })
    }
  });
}

searchRunnerNurses(){
  let dialogRef = this.doctorNavigator.pickDoctors(false,physican_type.nurse,null);
  dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
    if (result){
      // this.operationNoteDetail.runner_nurses = [];
      // this.runnerNursesNames = "";
      // result.forEach((res) => {
      //   this.operationNoteDetail.runner_nurses.push(res.id);
      //   this.runnerNursesNames += (this.runnerNursesNames ? ", " : "") + res.first_name + " " + res.last_name 
      // })
    }
  });
}
searchAnaesthetists(){
  let dialogRef = this.doctorNavigator.pickDoctors(false,physican_type.nurse,null);
  dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
    if (result){
      // this.operationNoteDetail.anaesthetists = [];
      // this.anaesthetistsNames = "";
      // result.forEach((res) => {
      //   this.operationNoteDetail.anaesthetists.push(res.id);
      //   this.anaesthetistsNames += (this.anaesthetistsNames ? ", " : "") + res.first_name + " " + res.last_name 
      // })
    }
  });
}

searchSurgeryType():void{
  this.procedureNav.pickProcedures().afterClosed().subscribe(
    (pro:ProcedureSummary[])=>{
      this.operationNoteDetail.surgery_type = pro[0].id;
      this.surgeryTypeName = pro[0].name;
    }
  )
}
 }