import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperationNoteListComponent } from './operation-note-list.component';

describe('OperationNoteListComponent', () => {
  let component: OperationNoteListComponent;
  let fixture: ComponentFixture<OperationNoteListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationNoteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationNoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
