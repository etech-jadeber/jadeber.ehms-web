import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OperationNoteSummary, OperationNoteSummaryPartialList } from '../operation_note.model';
import { OperationNotePersist } from '../operation_note.persist';
import { OperationNoteNavigator } from '../operation_note.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Surgery_TypePersist } from 'src/app/surgery_types/surgery_type.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { SurgeryNoteWriters } from 'src/app/app.enums';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Patient_ProcedureDetail } from 'src/app/form_encounters/form_encounter.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
@Component({
  selector: 'app-operation-note-list',
  templateUrl: './operation-note-list.component.html',
  styleUrls: ['./operation-note-list.component.css']
})export class OperationNoteListComponent implements OnInit {
  operationNotesData: OperationNoteSummary[] = [];
  operationNotesTotalCount: number = 0;
  operationNoteSelectAll:boolean = false;
  operationNoteSelection: OperationNoteSummary[] = [];
 operationNotesDisplayedColumns: string[] = ["select","action","date","surgeons","surgery_type","patient_id" ];
  operationNoteSearchTextBox: FormControl = new FormControl();
  operationNoteIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) operationNotesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) operationNotesSort: MatSort;
 
  @Input() encounterId: string;
  @Input() patientId: string;
  @Input() service_type: number;
  @Output() onResult = new EventEmitter<number>();

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public operationNotePersist: OperationNotePersist,
                public operationNoteNavigator: OperationNoteNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public doctorPersist: DoctorPersist,
                public surgeryTypePersist: Surgery_TypePersist,
                public patientPersist: PatientPersist,
                public patientNavigator: PatientNavigator,
                public doctorNavigator: DoctorNavigator,
                public procedurePersist: ProcedurePersist,
                public form_encounterPersist: Form_EncounterPersist,
                public tcUtilsString: TCUtilsString
    ) {

        this.tcAuthorization.requireRead("operation_notes");
       this.operationNoteSearchTextBox.setValue(operationNotePersist.operationNoteSearchText);
      //delay subsequent keyup events
      this.operationNoteSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.operationNotePersist.operationNoteSearchText = value;
        this.searchOperation_notes();
      });
    } 
    
    ngOnInit() {   
      this.operationNotePersist.encounterId = this.encounterId
      this.operationNotePersist.patientId = this.patientId  
      
      this.operationNotesSort.sortChange.subscribe(() => {
        this.operationNotesPaginator.pageIndex = 0;
        this.searchOperation_notes(true);
      });

      this.operationNotesPaginator.page.subscribe(() => {
        this.searchOperation_notes(true);
      });
      this.operationNotesSort.active = 'date'
      this.operationNotesSort.direction = 'desc'
      //start by loading items
      this.searchOperation_notes();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.operationNotePersist.encounterId = this.encounterId;
      } else {
        this.operationNotePersist.encounterId = null;
      }
      this.searchOperation_notes()
    }

  searchOperation_notes(isPagination:boolean = false): void {


    let paginator = this.operationNotesPaginator;
    let sorter = this.operationNotesSort;

    this.operationNoteIsLoading = true;
    this.operationNoteSelection = [];

    this.operationNotePersist.searchOperationNote(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OperationNoteSummaryPartialList) => {
      this.operationNotesData = partialList.data;
      this.operationNotesData.forEach((operation) => {
        // operation.surgeonsDetail = [];
        // this.doctorPersist.getDoctor(operation.sergeon_id).subscribe(value =>{
        //   operation.surgeonsDetail.push(value)
        // })
        // operation.surgeons.forEach(sugeo => this.doctorPersist.getDoctor(sugeo['surgeon_id']).subscribe(value => {
        //     operation.surgeonsDetail.push(value)
        // }))
        this.patientPersist.getPatient(operation.patient_id).subscribe((patient) => {
          operation.patient_name = patient.fname + " " + patient.lname;
        })
        operation.proceduresDone = []
        this.form_encounterPersist.patient_proceduresDo("by_operation_note", {operation_note_id: operation.id}).subscribe(
          (patientProcedures: Patient_ProcedureDetail[]) => {
            patientProcedures.forEach((patientProcedure) => {
              this.procedurePersist.getProcedure(patientProcedure.procedure_id).subscribe(procedure => {
                operation.proceduresDone = [...operation.proceduresDone, procedure];
              })
            })
          }
        )
      })
      if (partialList.total != -1) {
        this.operationNotesTotalCount = partialList.total;
      }
      this.operationNoteIsLoading = false;
    }, error => {
      this.operationNoteIsLoading = false;
    });

  } downloadOperationNotes(): void {
    if(this.operationNoteSelectAll){
         this.operationNotePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download operation_note", true);
      });
    }
    else{
        this.operationNotePersist.download(this.tcUtilsArray.idsList(this.operationNoteSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download operation_note",true);
            });
        }
  }

  editOperationNote(item: OperationNoteSummary, surgeryNoteWriter: number) {
    let dialogRef = this.operationNoteNavigator.editOperationNote(item.id, surgeryNoteWriter, this.service_type);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchOperation_notes();
      }

    });
  }

  addOperationNote() {
    let dialogRef = this.operationNoteNavigator.addOperationNote(this.encounterId, SurgeryNoteWriters.surgeon, this.service_type);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // TCUtilsAngular.assign(item, result);
        this.searchOperation_notes();
      }

    });
  }

  canWrite(operation_note: OperationNoteSummary): boolean {
    if (operation_note.complication_during_operation != null ||
      operation_note.complication_during_operation != "" ||
      operation_note.indecation_for_operation != null ||
      operation_note.complication_during_operation != "" ||
      operation_note.operation_procedure_findings != null||
      operation_note.complication_during_operation != ""){
        return true
      }
    return false;
  }

  deleteOperationNote(item: OperationNoteSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("operation_note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.operationNotePersist.deleteOperationNote(item.id).subscribe(response => {
          this.tcNotification.success("operation_note deleted");
          this.searchOperation_notes();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}