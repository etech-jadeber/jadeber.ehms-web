import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OperationNotePickComponent } from './operation-note-pick.component';

describe('OperationNotePickComponent', () => {
  let component: OperationNotePickComponent;
  let fixture: ComponentFixture<OperationNotePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationNotePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationNotePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
