import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OperationNoteSummary, OperationNoteSummaryPartialList } from '../operation_note.model';
import { OperationNotePersist } from '../operation_note.persist';
import { OperationNoteNavigator } from '../operation_note.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-operation_note-pick',
  templateUrl: './operation-note-pick.component.html',
  styleUrls: ['./operation-note-pick.component.css']
})export class OperationNotePickComponent implements OnInit {
  operationNotesData: OperationNoteSummary[] = [];
  operationNotesTotalCount: number = 0;
  operationNoteSelectAll:boolean = false;
  operationNoteSelection: OperationNoteSummary[] = [];

 operationNotesDisplayedColumns: string[] = ["select", ,"patient_id","indecation_for_operation","surgery_type","operation_procedure_findings","complication_during_operation","date" ];
  operationNoteSearchTextBox: FormControl = new FormControl();
  operationNoteIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) operationNotesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) operationNotesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public operationNotePersist: OperationNotePersist,
                public operationNoteNavigator: OperationNoteNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<OperationNotePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("operation_notes");
       this.operationNoteSearchTextBox.setValue(operationNotePersist.operationNoteSearchText);
      //delay subsequent keyup events
      this.operationNoteSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.operationNotePersist.operationNoteSearchText = value;
        this.searchOperation_notes();
      });
    } ngOnInit() {
   
      this.operationNotesSort.sortChange.subscribe(() => {
        this.operationNotesPaginator.pageIndex = 0;
        this.searchOperation_notes(true);
      });

      this.operationNotesPaginator.page.subscribe(() => {
        this.searchOperation_notes(true);
      });
      //start by loading items
      this.searchOperation_notes();
    }

  searchOperation_notes(isPagination:boolean = false): void {


    let paginator = this.operationNotesPaginator;
    let sorter = this.operationNotesSort;

    this.operationNoteIsLoading = true;
    this.operationNoteSelection = [];

    this.operationNotePersist.searchOperationNote(50, 0, '', 'asc').subscribe((partialList: OperationNoteSummaryPartialList) => {
      this.operationNotesData = partialList.data;
      if (partialList.total != -1) {
        this.operationNotesTotalCount = partialList.total;
      }
      this.operationNoteIsLoading = false;
    }, error => {
      this.operationNoteIsLoading = false;
    });

  }
  markOneItem(item: OperationNoteSummary) {
    if(!this.tcUtilsArray.containsId(this.operationNoteSelection,item.id)){
          this.operationNoteSelection = [];
          this.operationNoteSelection.push(item);
        }
        else{
          this.operationNoteSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.operationNoteSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }