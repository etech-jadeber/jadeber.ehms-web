import {TCId} from "../tc/models";
import { DoctorDetail } from "../doctors/doctor.model";
import { ProcedureDetail } from "../procedures/procedure.model";

export class OperationNoteSummary extends TCId {
    patient_id:string;
    indecation_for_operation:string;
    surgery_type:string;
    operation_procedure_findings:string;
    incisoin:string;
    procedure_done:string;
    complication_during_operation:string;
    drains:string;
    closure:string;
    specimens_sent_for_investigation:string;
    estimated_blood_loss:string;
    post_op_diagnosis:string;
    post_op_order:string;
    date:number;
    sergeon_id: string;
    surgeons: string;
    scrub_nurses: string;
    runner_nurses:string;
    assistants: string;
    anaesthetists: string;
    procedures: string[]
    surgeonsDetail: DoctorDetail[];
    nursesDetail: DoctorDetail[];
    assistantsDetail: DoctorDetail[];
    anaesthetistsDetail: DoctorDetail[];
    runner_nurseDetail: DoctorDetail[];
    proceduresDone: ProcedureDetail[];
    patient_name: string;
    surgery_type_name: string;
    assistant_note: string;
    anesthesis_note: string;
    scrub_note: string;
    runner_nurse_note:string;
    encounter_id:string;
    pre_op_diagnosis: string;
    }
    export class OperationNoteSummaryPartialList {
      data: OperationNoteSummary[];
      total: number;
    }
    export class OperationNoteDetail extends OperationNoteSummary {
    }
    
    export class OperationNoteDashboard {
      total: number;
    }