import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {OperationNoteEditComponent} from "./operation-note-edit/operation-note-edit.component";
import {OperationNotePickComponent} from "./operation-note-pick/operation-note-pick.component";


@Injectable({
  providedIn: 'root'
})

export class OperationNoteNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  operation_notesUrl(): string {
    return "/operation_notes";
  }

  operation_noteUrl(id: string): string {
    return "/operation_notes/" + id;
  }

  viewOperationNotes(): void {
    this.router.navigateByUrl(this.operation_notesUrl());
  }

  viewOperationNote(id: string): void {
    this.router.navigateByUrl(this.operation_noteUrl(id));
  }

  editOperationNote(id: string, surgeryNoteWriter: number, service_type: number): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OperationNoteEditComponent, {
      data: {tcIdMode: new TCIdMode(id, TCModalModes.EDIT), surgeryNoteWriter, service_type},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOperationNote(parentId: string, surgeryNoteWriter: number, service_type: number): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OperationNoteEditComponent, {
          data: {tcIdMode: new TCIdMode(parentId, TCModalModes.NEW), surgeryNoteWriter, service_type},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
   pickOperationNotes(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(OperationNotePickComponent, {
        data: selectOne,
        width: TCModalWidths.large,
      });
      return dialogRef;
    }
}