import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {OperationNoteDashboard, OperationNoteDetail, OperationNoteSummaryPartialList} from "./operation_note.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class OperationNotePersist {
  surgery_date:number;
 operationNoteSearchText: string = "";
 encounterId:string;
 patientId: string;
 
 constructor(private http: HttpClient) {
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.operationNoteSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchOperationNote(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OperationNoteSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("operation_note", this.operationNoteSearchText, pageSize, pageIndex, sort, order);
    if (this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
    }
    if (this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    }
    return this.http.get<OperationNoteSummaryPartialList>(url);

  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "operation_note/do", new TCDoParam("download_all", this.filters()));
  }

  operationNoteDashboard(): Observable<OperationNoteDashboard> {
    return this.http.get<OperationNoteDashboard>(environment.tcApiBaseUri + "operation_note/dashboard");
  }

  getOperationNote(id: string): Observable<OperationNoteDetail> {
    return this.http.get<OperationNoteDetail>(environment.tcApiBaseUri + "operation_note/" + id);
  } 
  
  addOperationNote(item: OperationNoteDetail): Observable<TCId> {
    let url=environment.tcApiBaseUri + "operation_note/";
    if(this.surgery_date){
    url=TCUtilsString.appendUrlParameter(url,"surgery_date",this.surgery_date.toString());
  }
    return this.http.post<TCId>(url, item);
  }

  updateOperationNote(item: OperationNoteDetail): Observable<OperationNoteDetail> {
    return this.http.patch<OperationNoteDetail>(environment.tcApiBaseUri + "operation_note/" + item.id, item);
  }

  deleteOperationNote(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "operation_note/" + id);
  }

  operationNotesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "operation_note/do", new TCDoParam(method, payload));
  }

  operationNoteDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "operation_notes/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "operation_note/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "operation_note/" + id + "/do", new TCDoParam("print", {}));
  }


}