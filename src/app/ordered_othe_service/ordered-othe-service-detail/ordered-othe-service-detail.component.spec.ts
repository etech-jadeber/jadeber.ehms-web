import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedOtheServiceDetailComponent } from './ordered-othe-service-detail.component';

describe('OrderedOtheServiceDetailComponent', () => {
  let component: OrderedOtheServiceDetailComponent;
  let fixture: ComponentFixture<OrderedOtheServiceDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderedOtheServiceDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedOtheServiceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
