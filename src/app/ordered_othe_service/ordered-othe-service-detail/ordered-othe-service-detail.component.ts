import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {OrderedOtheServiceDetail} from "../ordered_othe_service.model";
import {OrderedOtheServicePersist} from "../ordered_othe_service.persist";
import {OrderedOtheServiceNavigator} from "../ordered_othe_service.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export enum OrderedOtheServiceTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-ordered_othe_service-detail',
  templateUrl: './ordered-othe-service-detail.component.html',
  styleUrls: ['./ordered-othe-service-detail.component.scss']
})
export class OrderedOtheServiceDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  orderedOtheServiceIsLoading:boolean = false;
  
  OrderedOtheServiceTabs: typeof OrderedOtheServiceTabs = OrderedOtheServiceTabs;
  activeTab: OrderedOtheServiceTabs = OrderedOtheServiceTabs.overview;
  //basics
  orderedOtheServiceDetail: OrderedOtheServiceDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public orderedOtheServiceNavigator: OrderedOtheServiceNavigator,
              public  orderedOtheServicePersist: OrderedOtheServicePersist) {
    this.tcAuthorization.requireRead("order_other_services");
    this.orderedOtheServiceDetail = new OrderedOtheServiceDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("order_other_services");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.orderedOtheServiceIsLoading = true;
    this.orderedOtheServicePersist.getOrderedOtheService(id).subscribe(orderedOtheServiceDetail => {
          this.orderedOtheServiceDetail = orderedOtheServiceDetail;
          this.orderedOtheServiceIsLoading = false;
        }, error => {
          console.error(error);
          this.orderedOtheServiceIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.orderedOtheServiceDetail.id);
  }
  editOrderedOtheService(): void {
    let modalRef = this.orderedOtheServiceNavigator.editOrderedOtheService(this.orderedOtheServiceDetail.id);
    modalRef.afterClosed().subscribe(orderedOtheServiceDetail => {
      TCUtilsAngular.assign(this.orderedOtheServiceDetail, orderedOtheServiceDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.orderedOtheServicePersist.print(this.orderedOtheServiceDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print ordered_othe_service", true);
      });
    }

  back():void{
      this.location.back();
    }

}