import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedOtheServiceEditComponent } from './ordered-othe-service-edit.component';

describe('OrderedOtheServiceEditComponent', () => {
  let component: OrderedOtheServiceEditComponent;
  let fixture: ComponentFixture<OrderedOtheServiceEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderedOtheServiceEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedOtheServiceEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
