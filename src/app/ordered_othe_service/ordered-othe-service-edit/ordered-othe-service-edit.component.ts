import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { OrderedOtheServiceDetail } from '../ordered_othe_service.model';import { OrderedOtheServicePersist } from '../ordered_othe_service.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { OtherServicesSummary } from 'src/app/other_services/other-services.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import {physican_type, service_type} from 'src/app/app.enums';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { FormControl } from '@angular/forms';
import {OtherServicesPersist} from "../../other_services/other-services.persist";
@Component({
  selector: 'app-ordered_othe_service-edit',
  templateUrl: './ordered-othe-service-edit.component.html',
  styleUrls: ['./ordered-othe-service-edit.component.scss']
})export class OrderedOtheServiceEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
  start_date: FormControl = new FormControl((new Date(Date.now() - this.tzoffset)).toISOString().slice(0, 16))
  orderedOtheServiceDetail: OrderedOtheServiceDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OrderedOtheServiceEditComponent>,
              public  persist: OrderedOtheServicePersist,
              public otherServicePersist: OtherServicesPersist,
              private otherServiceNAv:OtherServicesNavigator,
              private patientNav:PatientNavigator,
              public tcUtilsDate: TCUtilsDate,
              public doctorNav: DoctorNavigator,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                this.start_date.valueChanges.pipe().subscribe(value => {
                  this.orderedOtheServiceDetail.date = this.tcUtilsDate.toTimeStamp(new Date(value))
                });

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }
  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW || this.isWizard()) {
     this.tcAuthorization.requireCreate("order_other_services");
      this.title = this.appTranslation.getText("general","new") +  " " + "ordered_othe_service";
      this.orderedOtheServiceDetail = new OrderedOtheServiceDetail();
      this.orderedOtheServiceDetail.encounter_id = this.idMode.id
      //set necessary defaults
      this.orderedOtheServiceDetail.date = this.tcUtilsDate.toTimeStamp(new Date())

    } else {
     this.tcAuthorization.requireUpdate("order_other_services");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "ordered_othe_service";
      this.isLoadingResults = true;
      this.persist.getOrderedOtheService(this.idMode.id).subscribe(orderedOtheServiceDetail => {
        this.orderedOtheServiceDetail = orderedOtheServiceDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addOrderedOtheService(this.orderedOtheServiceDetail).subscribe(value => {
      this.tcNotification.success("orderedOtheService added");
      this.orderedOtheServiceDetail.id = value.id;
      this.dialogRef.close(this.orderedOtheServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onWizard() {
    this.dialogRef.close(this.orderedOtheServiceDetail)
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateOrderedOtheService(this.orderedOtheServiceDetail).subscribe(value => {
      this.tcNotification.success("ordered_othe_service updated");
      this.dialogRef.close(this.orderedOtheServiceDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.orderedOtheServiceDetail == null){
            return false;
          }

// if (this.orderedOtheServiceDetail.encounter_id == null || this.orderedOtheServiceDetail.encounter_id  == "") {
//             return false;
//         }

if (this.orderedOtheServiceDetail.other_service_id == null || this.orderedOtheServiceDetail.other_service_id  == "") {
            return false;
        }
 return true;

 }




 searchProvider() {
  let dialogRef = this.doctorNav.pickDoctors(true, physican_type.doctor);
  dialogRef.afterClosed().subscribe((result: DoctorDetail[]) => {
    if (result) {
      this.orderedOtheServiceDetail.provider_id = result[0].id;
      this.providerName = result[0].first_name + " " + result[0].middle_name + " " + result[0].last_name;
    }
  });
}



searchService() {
  let dialogRef = this.otherServiceNAv.pickOtherServicess(this.orderedOtheServiceDetail.service_type,true);
  dialogRef.afterClosed().subscribe((result: OtherServicesSummary[]) => {
    if (result) {
      this.orderedOtheServiceDetail.other_service_id = result[0].id;
      this.serviceName = result[0].name
    }
  });
}


providerName:string = "";
serviceName:string = "";



 }
