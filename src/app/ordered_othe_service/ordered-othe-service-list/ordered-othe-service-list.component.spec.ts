import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedOtheServiceListComponent } from './ordered-othe-service-list.component';

describe('OrderedOtheServiceListComponent', () => {
  let component: OrderedOtheServiceListComponent;
  let fixture: ComponentFixture<OrderedOtheServiceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderedOtheServiceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedOtheServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
