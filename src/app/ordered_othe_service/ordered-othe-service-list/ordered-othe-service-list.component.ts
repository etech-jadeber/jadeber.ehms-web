import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OrderedOtheServiceSummary, OrderedOtheServiceSummaryPartialList } from '../ordered_othe_service.model';
import { OrderedOtheServicePersist } from '../ordered_othe_service.persist';
import { OrderedOtheServiceNavigator } from '../ordered_othe_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { OtherServicesDetail } from 'src/app/other_services/other-services.model';
import { OtherServicesPersist } from 'src/app/other_services/other-services.persist';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import {UserDetail} from "../../tc/users/user.model";
import {UserPersist} from "../../tc/users/user.persist";
@Component({
  selector: 'app-ordered_othe_service-list',
  templateUrl: './ordered-othe-service-list.component.html',
  styleUrls: ['./ordered-othe-service-list.component.scss']
})export class OrderedOtheServiceListComponent implements OnInit {
  orderedOtheServicesData: OrderedOtheServiceSummary[] = [];
  orderedOtheServicesTotalCount: number = 0;
  orderedOtheServiceSelectAll:boolean = false;
  orderedOtheServiceSelection: OrderedOtheServiceSummary[] = [];

 orderedOtheServicesDisplayedColumns: string[] = ["select","action","other_service_id","quantity","remark", "given_by", "date" ];
  orderedOtheServiceSearchTextBox: FormControl = new FormControl();
  orderedOtheServiceIsLoading: boolean = false;
  other_services: {[id: string]:OtherServicesDetail} = {}
  other_services_name: string;
  users: {[id: string]: UserDetail} = {}

  @Input() encounter_id : string
  @Input() patientId: string;
  @ViewChild(MatPaginator, {static: true}) orderedOtheServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderedOtheServicesSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public userPersist: UserPersist,
                public appTranslation:AppTranslation,
                public orderedOtheServicePersist: OrderedOtheServicePersist,
                public orderedOtheServiceNavigator: OrderedOtheServiceNavigator,
                public otherServicePersist: OtherServicesPersist,
                public otherServiceNavigator: OtherServicesNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,

    ) {

        this.tcAuthorization.requireRead("order_other_services");
       this.orderedOtheServiceSearchTextBox.setValue(orderedOtheServicePersist.orderedOtheServiceSearchText);
      //delay subsequent keyup events
      this.orderedOtheServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.orderedOtheServicePersist.orderedOtheServiceSearchText = value;
        this.searchOrderedOtheServices();
      });
    } ngOnInit() {
      this.orderedOtheServicePersist.encounterId = this.encounter_id
this.orderedOtheServicePersist.patientId = this.patientId
      this.orderedOtheServicePersist.encounterId = this.encounter_id;
      this.orderedOtheServicesSort.sortChange.subscribe(() => {
        this.orderedOtheServicesPaginator.pageIndex = 0;
        this.searchOrderedOtheServices(true);
      });

      this.orderedOtheServicesPaginator.page.subscribe(() => {
        this.searchOrderedOtheServices(true);
      });
      //start by loading items
      this.searchOrderedOtheServices();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.orderedOtheServicePersist.encounterId = this.encounter_id;
      } else {
      this.orderedOtheServicePersist.encounterId = null;
      }
      this.searchOrderedOtheServices()
      }

  searchOrderedOtheServices(isPagination:boolean = false): void {


    let paginator = this.orderedOtheServicesPaginator;
    let sorter = this.orderedOtheServicesSort;

    this.orderedOtheServiceIsLoading = true;
    this.orderedOtheServiceSelection = [];

    this.orderedOtheServicePersist.searchOrderedOtheService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OrderedOtheServiceSummaryPartialList) => {
      this.orderedOtheServicesData = partialList.data;
      this.orderedOtheServicesData.forEach(ordered => {
        if (!this.other_services[ordered.other_service_id]){
          this.other_services[ordered.other_service_id] = new OtherServicesDetail()
          this.otherServicePersist.getOtherServices(ordered.other_service_id).subscribe(
            other_service => this.other_services[ordered.other_service_id] = other_service
          )
        }
        if ( !this.users[ordered.given_by]) {
          this.users[ordered.given_by] = new UserDetail()
          this.userPersist.getUser(ordered.given_by).subscribe(
            user => {
              this.users[ordered.given_by] = user;
            }
          )
        }
      })
      if (partialList.total != -1) {
        this.orderedOtheServicesTotalCount = partialList.total;
      }
      this.orderedOtheServiceIsLoading = false;
    }, error => {
      this.orderedOtheServiceIsLoading = false;
    });

  } downloadOrderedOtheServices(): void {
    if(this.orderedOtheServiceSelectAll){
         this.orderedOtheServicePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download ordered_othe_service", true);
      });
    }
    else{
        this.orderedOtheServicePersist.download(this.tcUtilsArray.idsList(this.orderedOtheServiceSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download ordered_othe_service",true);
            });
        }
  }
addOrderedOtheService(): void {
    let dialogRef = this.orderedOtheServiceNavigator.addOrderedOtheService(this.encounter_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchOrderedOtheServices();
      }
    });
  }

  editOrderedOtheService(item: OrderedOtheServiceSummary) {
    let dialogRef = this.orderedOtheServiceNavigator.editOrderedOtheService(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  searchOtherService() {
    let dialogRef = this.otherServiceNavigator.pickOtherServicess(null,true);
    dialogRef.afterClosed().subscribe((result: OtherServicesDetail[]) => {
      if (result) {
        this.other_services_name = result[0].name;
        this.orderedOtheServicePersist.other_service_id = result[0].id
        this.searchOrderedOtheServices()
      }
    });
  }

  getOtherService(id: string){
    const other_service = this.other_services[id]
    return (other_service.name || '')
  }

  deleteOrderedOtheService(item: OrderedOtheServiceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("ordered_othe_service");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.orderedOtheServicePersist.deleteOrderedOtheService(item.id).subscribe(response => {
          this.tcNotification.success("ordered_othe_service deleted");
          this.searchOrderedOtheServices();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

  getUser(id: string): string {
    return (this.users[id]?.name || "")
  }
}
