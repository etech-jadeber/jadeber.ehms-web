import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderedOtheServicePickComponent } from './ordered-othe-service-pick.component';

describe('OrderedOtheServicePickComponent', () => {
  let component: OrderedOtheServicePickComponent;
  let fixture: ComponentFixture<OrderedOtheServicePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderedOtheServicePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderedOtheServicePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
