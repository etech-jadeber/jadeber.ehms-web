import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OrderedOtheServiceSummary, OrderedOtheServiceSummaryPartialList } from '../ordered_othe_service.model';
import { OrderedOtheServicePersist } from '../ordered_othe_service.persist';
import { OrderedOtheServiceNavigator } from '../ordered_othe_service.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-ordered_othe_service-pick',
  templateUrl: './ordered-othe-service-pick.component.html',
  styleUrls: ['./ordered-othe-service-pick.component.scss']
})export class OrderedOtheServicePickComponent implements OnInit {
  orderedOtheServicesData: OrderedOtheServiceSummary[] = [];
  orderedOtheServicesTotalCount: number = 0;
  orderedOtheServiceSelectAll:boolean = false;
  orderedOtheServiceSelection: OrderedOtheServiceSummary[] = [];

 orderedOtheServicesDisplayedColumns: string[] = ["select", ,"patient_id","other_service_id","patient_name","remark" ];
  orderedOtheServiceSearchTextBox: FormControl = new FormControl();
  orderedOtheServiceIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) orderedOtheServicesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderedOtheServicesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public orderedOtheServicePersist: OrderedOtheServicePersist,
                public orderedOtheServiceNavigator: OrderedOtheServiceNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<OrderedOtheServicePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("order_other_services");
       this.orderedOtheServiceSearchTextBox.setValue(orderedOtheServicePersist.orderedOtheServiceSearchText);
      //delay subsequent keyup events
      this.orderedOtheServiceSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.orderedOtheServicePersist.orderedOtheServiceSearchText = value;
        this.searchOrdered_othe_services();
      });
    } ngOnInit() {
   
      this.orderedOtheServicesSort.sortChange.subscribe(() => {
        this.orderedOtheServicesPaginator.pageIndex = 0;
        this.searchOrdered_othe_services(true);
      });

      this.orderedOtheServicesPaginator.page.subscribe(() => {
        this.searchOrdered_othe_services(true);
      });
      //start by loading items
      this.searchOrdered_othe_services();
    }

  searchOrdered_othe_services(isPagination:boolean = false): void {


    let paginator = this.orderedOtheServicesPaginator;
    let sorter = this.orderedOtheServicesSort;

    this.orderedOtheServiceIsLoading = true;
    this.orderedOtheServiceSelection = [];

    this.orderedOtheServicePersist.searchOrderedOtheService(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OrderedOtheServiceSummaryPartialList) => {
      this.orderedOtheServicesData = partialList.data;
      if (partialList.total != -1) {
        this.orderedOtheServicesTotalCount = partialList.total;
      }
      this.orderedOtheServiceIsLoading = false;
    }, error => {
      this.orderedOtheServiceIsLoading = false;
    });

  }
  markOneItem(item: OrderedOtheServiceSummary) {
    if(!this.tcUtilsArray.containsId(this.orderedOtheServiceSelection,item.id)){
          this.orderedOtheServiceSelection = [];
          this.orderedOtheServiceSelection.push(item);
        }
        else{
          this.orderedOtheServiceSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.orderedOtheServiceSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }