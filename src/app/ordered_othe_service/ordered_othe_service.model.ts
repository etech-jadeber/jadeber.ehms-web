import {TCId} from "../tc/models";export class OrderedOtheServiceSummary extends TCId {
    patient_id:string;
    other_service_id:string;
    provider_id:string;
    quantity:number;
    patient_name: string;
    date: number;
    encounter_id: string;
    service_type: number;
    given_by: string;

    }
    export class OrderedOtheServiceSummaryPartialList {
      data: OrderedOtheServiceSummary[];
      total: number;
    }
    export class OrderedOtheServiceDetail extends OrderedOtheServiceSummary {
    }

    export class OrderedOtheServiceDashboard {
      total: number;
    }
