import {Router} from "@angular/router";
import {Injectable} from "@angular/core";


import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {OrderedOtheServiceEditComponent} from "./ordered-othe-service-edit/ordered-othe-service-edit.component";
import {OrderedOtheServicePickComponent} from "./ordered-othe-service-pick/ordered-othe-service-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class OrderedOtheServiceNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  orderedOtheServicesUrl(): string {
    return "/ordered_othe_services";
  }

  orderedOtheServiceUrl(id: string): string {
    return "/ordered_othe_services/" + id;
  }

  viewOrderedOtheServices(): void {
    this.router.navigateByUrl(this.orderedOtheServicesUrl());
  }

  viewOrderedOtheService(id: string): void {
    this.router.navigateByUrl(this.orderedOtheServiceUrl(id));
  }

  editOrderedOtheService(id: string): MatDialogRef<OrderedOtheServiceEditComponent> {
    const dialogRef = this.dialog.open(OrderedOtheServiceEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOrderedOtheService(encounter_id: string): MatDialogRef<OrderedOtheServiceEditComponent> {
    const dialogRef = this.dialog.open(OrderedOtheServiceEditComponent, {
          data: new TCIdMode(encounter_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  wizardOrderedOtheService(): MatDialogRef<OrderedOtheServiceEditComponent> {
    const dialogRef = this.dialog.open(OrderedOtheServiceEditComponent, {
          data: new TCIdMode(null, TCModalModes.WIZARD),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickOrderedOtheServices(selectOne: boolean=false): MatDialogRef<OrderedOtheServicePickComponent> {
      const dialogRef = this.dialog.open(OrderedOtheServicePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
