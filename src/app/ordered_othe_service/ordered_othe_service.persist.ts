import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {OrderedOtheServiceDashboard, OrderedOtheServiceDetail, OrderedOtheServiceSummaryPartialList} from "./ordered_othe_service.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class OrderedOtheServicePersist {
  other_service_id: string;
  encounterId: string;
  patientId: string;
  service_type: number;
 orderedOtheServiceSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.orderedOtheServiceSearchText;
    //add custom filters
    return fltrs;
  }

  searchOrderedOtheService(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OrderedOtheServiceSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("ordered_othe_service", this.orderedOtheServiceSearchText, pageSize, pageIndex, sort, order);
    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    if(this.service_type){
      url = TCUtilsString.appendUrlParameter(url, "service_type", this.service_type.toString())
    }
    if (this.other_service_id){
      url = TCUtilsString.appendUrlParameter(url, "other_service_id", this.other_service_id);
    }
    return this.http.get<OrderedOtheServiceSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ordered_othe_service/do", new TCDoParam("download_all", this.filters()));
  }

  orderedOtheServiceDashboard(): Observable<OrderedOtheServiceDashboard> {
    return this.http.get<OrderedOtheServiceDashboard>(environment.tcApiBaseUri + "ordered_othe_service/dashboard");
  }

  getOrderedOtheService(id: string): Observable<OrderedOtheServiceDetail> {
    return this.http.get<OrderedOtheServiceDetail>(environment.tcApiBaseUri + "ordered_othe_service/" + id);
  } addOrderedOtheService(item: OrderedOtheServiceDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ordered_othe_service/", item);
  }

  updateOrderedOtheService(item: OrderedOtheServiceDetail): Observable<OrderedOtheServiceDetail> {
    return this.http.patch<OrderedOtheServiceDetail>(environment.tcApiBaseUri + "ordered_othe_service/" + item.id, item);
  }

  deleteOrderedOtheService(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "ordered_othe_service/" + id);
  }
 orderedOtheServicesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "ordered_othe_service/do", new TCDoParam(method, payload));
  }

  orderedOtheServiceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "ordered_othe_services/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "ordered_othe_service/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "ordered_othe_service/" + id + "/do", new TCDoParam("print", {}));
  }


}
