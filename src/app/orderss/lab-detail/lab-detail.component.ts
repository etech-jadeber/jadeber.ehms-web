import {AfterViewInit, Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {OrdersDetail} from "../../form_encounters/orderss/orders.model";
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { GeneralPatientDetail, PatientDetail } from 'src/app/patients/patients.model';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import {PatientType, payment_status, ResultType, selected_orders_status, tabs} from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import {TCUtilsDate} from "../../tc/utils-date";
import { TCUtilsString } from 'src/app/tc/utils-string';
import { SampleCollectionNavigator } from 'src/app/sample_collection/sample_collection.navigator';
import { SampleCollectionPersist } from 'src/app/sample_collection/sample_collection.persist';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { FileNavigator } from 'src/app/tc/files/file.navigator';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';

export enum PaginatorIndexes {
  selected_orderss
}

@Component({
  selector: 'lab-orders-detail',
  templateUrl: './lab-detail.component.html',
  styleUrls: ['./lab-detail.component.css']
})
export class LabDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  ordersLoading:boolean = false;

  labListTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  resultType: number;
  ordersDetail: OrdersDetail;
  patientDetail: GeneralPatientDetail;
  payment_status = payment_status
  selected_orders_status = selected_orders_status

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public filePersist:FilePersist,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              private http:HttpClient,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public ordersNavigator: OrdersNavigator,
              public  ordersPersist: OrdersPersist,
              public patientPersist: PatientPersist,
              public doctorPersist: DoctorPersist,
              public menuState: MenuState,
              public sampleCollectionNavigator: SampleCollectionNavigator,
              public sampleCollectionPersist: SampleCollectionPersist,
              public tcUtilsString: TCUtilsString,
              public outsidePersist: Outside_OrdersPersist,
              public fileNavigator: FileNavigator,) {

    this.ordersDetail = new OrdersDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      this.route.data.subscribe((result) => { this.resultType = result.isPat ? ResultType.pat: ResultType.lab})
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.ordersLoading = true;
    this.ordersPersist.getOrders(id).subscribe(ordersDetail => {
          this.ordersDetail = ordersDetail;
          if (this.ordersDetail.patient_type == PatientType.outsideOrder){
            this.outsidePersist.getOutside_Orders(this.ordersDetail.encounter_id).subscribe(result => {
              this.ordersDetail.patient_name = result.patient_name
              this.patientDetail = {...result, dob: result.birth_date, pid: result.serial_id }
            })
          } else {
            this.patientPersist.getPatient(this.ordersDetail.patient_id).subscribe((result : PatientDetail) => {
              this.ordersDetail.patient_name = result.fname + " " + result.mname + " " + result.lname;
              this.patientDetail = {patient_name: this.ordersDetail.patient_name, ...result};
            });
            this.doctorPersist.getDoctor(this.ordersDetail.doctor_id).subscribe((result: DoctorDetail) => {
              this.ordersDetail.doctor_name = result.first_name + " " + result.middle_name + " " + result.last_name;
            });
          }
          this.ordersLoading = false;
          this.menuState.getDataResponse(this.ordersDetail);
        }, error => {
          console.error(error);
          this.ordersLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.ordersDetail.id);
  }


   printOrders():void{
      this.ordersPersist.print(this.ordersDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print orders", true);
      });
    }

  back():void{
      this.location.back();
    }

    calculateAge(dob: number): number {
      const current = new Date().getFullYear()
      return current - new Date(dob*1000).getFullYear()
    }

}
