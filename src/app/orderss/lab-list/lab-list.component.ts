import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {orderFilterColumn, OrdersDetail, OrdersSummary, OrdersSummaryPartialList} from "../../form_encounters/orderss/orders.model";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { lab_order_type, lab_urgency, OrderStatus } from "src/app/app.enums";
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { interval } from 'rxjs';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCId } from 'src/app/tc/models';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';


@Component({
  selector: 'lab-orders-list',
  templateUrl: './lab-list.component.html',
  styleUrls: ['./lab-list.component.css']
})
export class  LabListComponent implements OnInit {

  orderssData: OrdersSummary[] = [];
  orderssTotalCount: number = 0;
  orderssSelectAll:boolean = false;
  orderssSelection: OrdersSummary[] = [];
  labOrderFilterColumn = orderFilterColumn;

  orderssDisplayedColumns: string[] = ["select", "action", "p.pid", "patient_name", "provider_id", "generated_sample_id", "future_date","total_order","completed", "rejected", "requested", "patient_type"];
  orderssSearchTextBox: FormControl = new FormControl();
  orderssIsLoading: boolean = false;
  startDateTextField: FormControl = new FormControl({disabled: true, value: this.ordersPersist.labOrderSearchHistory.ordered_start_date ? this.tcUtilsDate.toDate(this.ordersPersist.labOrderSearchHistory.ordered_start_date) : null});
  endDateTextField: FormControl = new FormControl({disabled: true, value: this.ordersPersist.labOrderSearchHistory.ordered_end_date ? this.tcUtilsDate.toDate(this.ordersPersist.labOrderSearchHistory.ordered_end_date) : null});
  doctors: {[id: string]: DoctorDetail} = {}

  //job states
  unknown_state = -1;
  queued_state = 1;
  running_state = 2;
  success_state = 3;
  failed_state = 4;
  aborted_state = 5;
  message:string = '';

  @ViewChild(MatPaginator, {static: true}) orderssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderssSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public ordersPersist: OrdersPersist,
                public ordersNavigator: OrdersNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public doctorPersist: DoctorPersist,
                public paymentPersist: PaymentPersist,
                public encounterPersist: Form_EncounterPersist,
                public tcUtilsString: TCUtilsString,
                public outsidePersist: Outside_OrdersPersist,
                public  jobData: JobData,
                public tcAsyncJob: TCAsyncJob,
                public filePersist: FilePersist,
                public labOrderNavigator: Lab_OrderNavigator,
    ) {

        this.tcAuthorization.requireRead("laboratory_orders");
       this.orderssSearchTextBox.setValue(ordersPersist.labOrderSearchHistory.search_text);
      //delay subsequent keyup events
      this.orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.labOrderSearchHistory.search_text = value;
        this.searchOrderss();
      });
      // this.ordersPersist.orderedStartDate = this.ordersPersist.lab_order_start_date
      // this.ordersPersist.orderedEndDate = this.ordersPersist.lab_order_end_date
      this.startDateTextField.valueChanges.subscribe(value => {
        this.ordersPersist.labOrderSearchHistory.ordered_start_date =  value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        // this.ordersPersist.lab_order_start_date = this.ordersPersist.orderedStartDate
        this.searchOrderss();
      });
      this.endDateTextField.valueChanges.subscribe(value => {
        this.ordersPersist.labOrderSearchHistory.ordered_end_date =  value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        // this.ordersPersist.lab_order_end_date = this.ordersPersist.orderedEndDate
        this.searchOrderss();
      });
    }

    ngOnDestroy() {
      // this.ordersPersist.orderStatusFilter = null;
      // this.ordersPersist.lab_order_id = null;
      // this.ordersPersist.orderedStartDate = null;
      // this.ordersPersist.orderedEndDate = null;
      // this.ordersPersist.lab_order_statuId = null;
      // this.ordersPersist.lab_urgencyId = null;
      // this.ordersPersist.resultDate = null;
      // this.ordersPersist.show_only_mine = null;
    }

    ngOnInit() {
      // this.ordersPersist.orderStatusFilter = this.ordersPersist.rad_order_status
      this.orderssSort.sortChange.subscribe(() => {
        this.orderssPaginator.pageIndex = 0;
        this.searchOrderss(true);
      });

      this.orderssPaginator.page.subscribe(() => {
        this.searchOrderss(true);
      });


      //start by loading items
      this.searchOrderss();
    }

  searchOrderss(isPagination:boolean = false): void {
    let paginator = this.orderssPaginator;
    let sorter = this.orderssSort;
    this.orderssIsLoading = true;
    this.ordersPersist.orderssDo("get_lab_order",{lab_order_id: this.ordersPersist.labOrderSearchHistory.lab_order_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc', this.ordersPersist.labOrderSearchHistory).subscribe(
      (labs:OrdersSummaryPartialList)=>{
        this.orderssData = labs.data;
        this.orderssData.forEach(
          orders => {
            if (!this.doctors[orders.provider_id]){
              this.doctors[orders.provider_id] = new DoctorDetail()
              orders.provider_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(orders.provider_id).subscribe(
                doctor => {
                  this.doctors[orders.provider_id] = doctor;
                }
              )
            }
            this.ordersPersist.ordersDo(orders.id, "has_paid_order", {}).subscribe((value: {is_paid: boolean}) => {
             orders.is_paid = value.is_paid
            })
          }
        )
        if (labs.total != -1) {
          this.orderssTotalCount = labs.total;
        }
        this.orderssIsLoading = false
      }, error => {
        this.orderssIsLoading = false;
        console.error(error)
      }
    )


  }

  printResult(item: OrdersDetail){

    this.ordersPersist.ordersDo(item.id, "print_result", item).subscribe((downloadJob: TCId) => {

      this.jobPersist.followJob(downloadJob.id, "printing Result", true);
      let success_state = 3;
      this.jobPersist.getJob(downloadJob.id).subscribe(
        job => {
          if (job.job_state == success_state) {
          }
        }

      )
    }, error => {
    })
  }

  pringOrder(order: OrdersDetail){
    this.ordersPersist.ordersDo(order.id, "print_order", {}).subscribe(
      (downloadJob: TCId) => {
        this.jobPersist.followJob(downloadJob.id, 'printing Order', true);
        let success_state = 3;
        this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
          if (job.job_state == success_state) {
          }
        });
      },
      (error) => {}
    )
  }
  
  searchLabOrder(){
    const dialogRef = this.labOrderNavigator.pickLab_Orders(false, lab_order_type.lab);
    dialogRef.afterClosed().subscribe(
      (orders: Lab_OrderDetail[]) => {
        if (orders.length){
          const ids = orders.map(order => order.id);
           const names = orders.map(order => order.name);
          this.ordersPersist.labOrderSearchHistory.lab_order_id = ids.join(',')
          this.ordersPersist.labOrderSearchHistory.lab_order_name = names.join(',')
          this.searchOrderss()
        }
      }
    )
  }

  followJob(id: string, descrption: string, is_download_job: boolean = true): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }


    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe(n => {

      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

            if (is_download_job) {
              this.jobPersist.downloadFileKey(id).subscribe(jobFile => {
                this.tcNavigator.openUrl(this.filePersist.downloadLink(jobFile.download_key), true)
              });
            }

        }
        else{
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);
          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }

  downloadOrderss(): void {
    if(this.orderssSelectAll){
         this.ordersPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download orderss", true);
      });
    }
    else{
        this.ordersPersist.download(this.tcUtilsArray.idsList(this.orderssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download orderss",true);
            });
        }
  }



  deleteOrders(item: OrdersSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Orders");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.ordersPersist.deleteOrders(item.id).subscribe(response => {
          this.tcNotification.success("Orders deleted");
          this.searchOrderss();
        }, error => {
        });
      }

    });
  }

  loadOrderDetail(orders: OrdersDetail): void {
    if(!orders.generated_sample_id){
      this.tcNotification.error("Sample ID is not generated")
    }
    else{
      this.router.navigate([this.ordersNavigator.labOrdersUrl(orders.id)]);
    }
  }

  pringQRCode(order: OrdersDetail){
    this.ordersPersist.ordersDo(order.id, "print_qr_code", {}).subscribe(
      (downloadJob: TCId) => {
        this.jobPersist.followJob(downloadJob.id, 'printing QR', true);
        let success_state = 3;
        this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
          if (job.job_state == success_state) {
          }
        });
      },
      (error) => {}
    )
  }

  generateSampleId(order: OrdersDetail) {
    this.ordersPersist.ordersDo(order.id, "generate_sample_id", {}).subscribe((result: Lab_OrderDetail) => {
      this.searchOrderss();
    })
  }

  back():void{
      this.location.back();
    }

    getPatientStatus(order: OrdersDetail){
      if (order.urgency == lab_urgency.critical || !order.is_paid){
        return 'red'
      }
      if (order.urgency = lab_urgency.urgent){
        return '#1A73E8'
      }else {
        return 'green'
      }
    }

    getDoctor(id: string){
      const {first_name, middle_name, last_name} = this.doctors[id]
      return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
    }

}
