import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LabResultApprovalListComponent } from './lab-result-approval-list.component';

describe('LabResultApprovalListComponent', () => {
  let component: LabResultApprovalListComponent;
  let fixture: ComponentFixture<LabResultApprovalListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LabResultApprovalListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LabResultApprovalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
