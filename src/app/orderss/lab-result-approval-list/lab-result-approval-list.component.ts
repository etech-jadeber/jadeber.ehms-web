import {Component, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { lab_order_type, lab_urgency, selected_orders_status } from 'src/app/app.enums';
import { Lab_ResultPersist } from 'src/app/form_encounters/orderss/lab_results/lab_result.persist';
import { Lab_ResultDetail, Lab_ResultSummary, Lab_ResultSummaryPartialList } from 'src/app/form_encounters/orderss/lab_results/lab_result.model';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { PathologyResultPersist } from 'src/app/pathology_result/pathology_result.persist';
import { PathologyResultNavigator } from 'src/app/pathology_result/pathology_result.navigator';
import { Selected_OrdersSummary } from 'src/app/form_encounters/orderss/orders.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
@Component({
  selector: 'app-lab-result-approval-list',
  templateUrl: './lab-result-approval-list.component.html',
  styleUrls: ['./lab-result-approval-list.component.css']
})
export class LabResultApprovalListComponent implements OnInit {
  labResultApprovalsData: Lab_ResultSummary[] = [];
  labResultApprovalsTotalCount: number = 0;
  labResultApprovalSelectAll:boolean = false;
  labResultApprovalSelection: Lab_ResultSummary[] = [];
  lab_urgency = lab_urgency
 labResultApprovalsDisplayedColumns: string[] = ["select","action","lab_test" ,"urgency","lab_panel","result", "detail" ];
 lab_resultSearchText: FormControl = new FormControl();
  labResultApprovalIsLoading: boolean = false; 
  
  @Input() orderId:string;
  @Input() orderType: number;
  @Input() lab_order_id: string;
  @Output() removeOrder = new EventEmitter();
  @ViewChild(MatPaginator, {static: true}) labResultApprovalsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) labResultApprovalsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public labResultApprovalPersist: Lab_ResultPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public orderPersist: OrdersPersist,
                public labPanelPersist: Lab_PanelPersist,
                public tcUtilsString: TCUtilsString,
                public ordersNavigator: OrdersNavigator,
                public pathologyResultPersist: PathologyResultPersist,
                public pathologyResultNavigator: PathologyResultNavigator,

    ) {

        this.tcAuthorization.requireRead("laboratory_result_approvals");
       this.lab_resultSearchText.setValue(labResultApprovalPersist.lab_resultSearchText);
      //delay subsequent keyup events
      this.lab_resultSearchText.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.labResultApprovalPersist.lab_resultSearchText = value;
        this.searchLabResultApprovals();
      });
    } 
    
    ngOnInit() {
      this.labResultApprovalsSort.sortChange.subscribe(() => {
        this.labResultApprovalsPaginator.pageIndex = 0;
        this.searchLabResultApprovals(true);
      });

      this.labResultApprovalsPaginator.page.subscribe(() => {
        this.searchLabResultApprovals(true);
      });
      //start by loading items
      this.searchLabResultApprovals();
    }

  searchLabResultApprovals(isPagination:boolean = false): void {


    let paginator = this.labResultApprovalsPaginator;
    let sorter = this.labResultApprovalsSort;

    this.labResultApprovalIsLoading = true;
    this.labResultApprovalSelection = [];

    this.labResultApprovalPersist.lab_resultsDo("need_approve_tests", {order_type: this.orderType, status: selected_orders_status.ready_to_approve, order_id : this.orderId, lab_order_id: this.lab_order_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Lab_ResultSummaryPartialList) => {
      this.labResultApprovalsData = partialList.data;
      if(!this.labResultApprovalsData.length){
        this.removeOrder.emit()
      }
      if (partialList.total != -1) {
        this.labResultApprovalsTotalCount = partialList.total;
      }
      this.labResultApprovalIsLoading = false;
    }, error => {
      this.labResultApprovalIsLoading = false;
    });

  }
 downloadLabResultApprovals(): void {
    if(this.labResultApprovalSelectAll){
         this.labResultApprovalPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate", true);
      });
    }
    else{
        this.labResultApprovalPersist.download(this.tcUtilsArray.idsList(this.labResultApprovalSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate",true);
            });
        }
  }
  approveOrder(selected_order_detail: Lab_ResultDetail) {
    if(this.orderType == lab_order_type.lab){
      this.labResultApprovalPersist.lab_resultDo(selected_order_detail.id, "approve", {}).subscribe(result => this.searchLabResultApprovals());
    } else {
      this.pathologyResultPersist.pathologyResultDo(selected_order_detail.id, "approve", {}).subscribe(result => this.searchLabResultApprovals())
    }
  }
  

  rejectOrder(selected_order_detail: Selected_OrdersSummary) {
    let dialogRef = this.ordersNavigator.addResult(selected_order_detail.lab_test_id, selected_order_detail, true);
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        if(this.orderType == lab_order_type.lab){
          this.labResultApprovalPersist.lab_resultDo(selected_order_detail.id, "reject", {reject_description: result.reject_description}).subscribe(res => 
            {
              this.searchLabResultApprovals()
            });
          } else {
            this.pathologyResultPersist.pathologyResultDo(selected_order_detail.id, "reject", {reject_description: result.reject_description}).subscribe(res => {
              this.searchLabResultApprovals()
            })
          }
        }
    })
  }

  showDetail(item: Selected_OrdersSummary){
    item.result_id = item.id
    let dialogRef;
    if(this.orderType == lab_order_type.lab){
      dialogRef = this.ordersNavigator.showResult(item.lab_test_id, item);
    } else {
      dialogRef = this.pathologyResultNavigator.showPathologyResult(item)
    }
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  approveSelectedResults(){
    this.labResultApprovalSelection.forEach(resultApproval => {
      this.approveOrder(resultApproval)
    })
  }

  back():void{
      this.location.back();
    }
}