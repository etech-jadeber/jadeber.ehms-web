import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadDetailComponent } from './rad-detail.component';

describe('RadDetailComponent', () => {
  let component: RadDetailComponent;
  let fixture: ComponentFixture<RadDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
