import { AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { interval, Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { OrdersDetail, OrdersSummary, OrdersSummaryPartialList, Selected_OrdersDetail, Selected_OrdersSummary } from "../../form_encounters/orderss/orders.model";
import { FormControl } from '@angular/forms';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { GeneralPatientDetail, PatientDetail } from 'src/app/patients/patients.model';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import {lab_order_type, PatientType, payment_status, ResultType, selected_orders_status, tabs} from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { Lab_TestDetail, Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelDetail } from 'src/app/lab_panels/lab_panel.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { TCUtilsDate } from "../../tc/utils-date";
import { Lab_ResultPersist } from 'src/app/form_encounters/orderss/lab_results/lab_result.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Lab_ResultDetail, Lab_ResultSummary } from 'src/app/form_encounters/orderss/lab_results/lab_result.model';
import { environment } from 'src/environments/environment';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCId } from 'src/app/tc/models';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Form_EncounterDetail } from 'src/app/form_encounters/form_encounter.model';


export enum PaginatorIndexes {
  rad_selected_orderss,
  selected_orderss_to_approve
}


@Component({
  selector: 'rad-orders-detail',
  templateUrl: './rad-detail.component.html',
  styleUrls: ['./rad-detail.component.css']
})
export class RadDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  rad_selected_orderssData: OrdersSummary[] = [];
  message: string = '';
  success_state = 3;
  PatientType = PatientType;
  rad_selected_orderssTotalCount: number = 0;
  rad_selected_orderssSelectAll: boolean = false;
  rad_selected_orderssSelected: Selected_OrdersSummary[] = [];
  rad_selected_orderssDisplayedColumns: string[] = ['action', 'labtest_id', 'urgency', 'lab_panel_id', 'scanned_document_id', 'status'];
  rad_selected_orderssSearchTextBox: FormControl = new FormControl();
  rad_selected_orderssLoading: boolean = false;
  selected_lab_orders = new Set();
  resultType: number = ResultType.img;
  dicoms;

  paramsSubscription: Subscription;
  ordersLoading: boolean = false;
  patientDetail: GeneralPatientDetail;

  radListTabs: typeof tabs = tabs;
  lab_order_type = lab_order_type
  activeTab: number = tabs.overview;
  //basics
  ordersDetail: OrdersDetail;
  form_encounter: Form_EncounterDetail;
  @ViewChild(MatPaginator, {static: true}) selectedOrderPaginator: MatPaginator;

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public ordersNavigator: OrdersNavigator,
    public ordersPersist: OrdersPersist,
    private http: HttpClient,
    public patientPersist: PatientPersist,
    public filePersist: FilePersist,
    public doctorPersist: DoctorPersist,
    public menuState: MenuState,
    public lab_testPersist: Lab_TestPersist,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    public rad_resultPersist: Lab_ResultPersist,
    public stringUtilities: TCUtilsString,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public formEncounterNavigator: Form_EncounterNavigator,
    public form_encounterPersist: Form_EncounterPersist,
    public outsidePersist: Outside_OrdersPersist) {
    this.ordersDetail = new OrdersDetail();
    this.rad_selected_orderssSearchTextBox.setValue(ordersPersist.selected_ordersSearchText);
    this.rad_selected_orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.ordersPersist.selected_ordersSearchText = value.trim();
      this.searchSelected_Orderss();
    });
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
    this.selectedOrderPaginator.page.subscribe(value => {
      this.searchSelected_Orderss(true)
    })
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.ordersLoading = true;
    this.ordersPersist.getOrders(id).subscribe(ordersDetail => {
      this.ordersDetail = ordersDetail;
      if (this.ordersDetail.patient_type === PatientType.outsideOrder){
        this.outsidePersist.getOutside_Orders(this.ordersDetail.encounter_id).subscribe(result => {
          this.ordersDetail.patient_name = result.patient_name
          this.patientDetail = {...result, dob: result.birth_date, pid: result.serial_id}
        })
      } else {
        this.patientPersist.getPatient(this.ordersDetail.patient_id).subscribe((result : PatientDetail) => {
          this.ordersDetail.patient_name = result.fname + " " + result.lname;
          this.patientDetail = {...result, patient_name: this.ordersDetail.patient_name}
        });
        this.doctorPersist.getDoctor(this.ordersDetail.doctor_id).subscribe((result: DoctorDetail) => {
          this.ordersDetail.doctor_name = result.first_name + " " + result.middle_name + " " + result.last_name;
        });
        this.form_encounterPersist.getForm_Encounter(ordersDetail.encounter_id).subscribe((encounter)=>{
          if(encounter){
            this.form_encounter = encounter;
          }
        })
      }
      // this.ordersPersist.getStudies(this.ordersDetail.generated_sample_id).subscribe((value) => {
      //   this.dicoms = value
      //  })

      this.ordersLoading = false;
      this.menuState.getDataResponse(this.ordersDetail);
    }, error => {
      console.error(error);
      this.ordersLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.ordersDetail.id);
  }

  addOrder(
    order_type: number
  ): void {
    this.tcAuthorization.canCreate('lab_orders');
    let dialogRef = this.formEncounterNavigator.addLab(
      false,
      this.ordersDetail.encounter_id,
      null,
      this.tcUtilsDate.toTimeStamp(new Date()),
      [order_type]
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // this.tcNotification.success('Added');
      }
    });
  }

  buildDicomUrl():string{
    return  this.dicoms && this.dicoms.length ? `${environment.dicom_viewer}${this.dicoms[0]['0020000D'].Value[0]}` : "";
  }


  printOrders(): void {
    this.ordersPersist.print(this.ordersDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print orders", true);
    });
  }

  back(): void {
    this.location.back();
  }

  searchSelected_Orderss(isPagination: boolean = false): void {
    let paginator = this.selectedOrderPaginator;
    this.rad_selected_orderssLoading = true;
    this.ordersPersist.ordersDo(this.ordersDetail.id, "order_by_type", {payment_status: payment_status.paid}, paginator.pageSize, isPagination ? paginator.pageIndex : 0, "name", "asc").subscribe((response: OrdersSummaryPartialList) => {
      this.rad_selected_orderssData = response.data;
      if (response.total != -1){
        this.rad_selected_orderssTotalCount = response.total
      }
      this.rad_selected_orderssLoading = false;
    },error=>{this.rad_selected_orderssLoading = false});
  }


  updateResult(selected_orderss: Selected_OrdersSummary) {
    const lab_result = new Lab_ResultDetail()
    lab_result.result = selected_orderss.result;
    lab_result.lab_test_id = selected_orderss.lab_test_id;
    lab_result.result_description = selected_orderss.result_description;
    lab_result.result_conclusion = selected_orderss.result_conclusion;
    this.rad_resultPersist.addLab_Result(selected_orderss.id, lab_result).subscribe(
      (response: Lab_ResultDetail) => {
        this.searchSelected_Orderss()
      }
    )
  }

  generateSampleId() {
    this.ordersPersist.ordersDo(this.ordersDetail.id, "generate_sample_id", {}).subscribe((result: Lab_OrderDetail) => {
      this.reload();
    })
  }



  documentUpload(fileInputEvent: any, element: Selected_OrdersDetail): void {
    this.rad_selected_orderssLoading = true;
    let fd: FormData = new FormData();
    for (const file of fileInputEvent.target.files) {
      if (file.size > 20000000) {
        this.tcNotification.error("File too big");
        return;
      }
      fd.append('file', file);
    }
    this.http.post(this.ordersPersist.dicomUploadUrl(element.id), fd).subscribe(response => {
      this.rad_selected_orderssLoading = false
      this.tcNotification.success("Success");
      this.searchSelected_Orderss();
    });
  }

  approveOrder(selected_order_detail: Selected_OrdersDetail) {
    // selected_order_detail.status = selected_orders_status.approved;
    // this.updateResult(selected_order_detail);
    this.rad_resultPersist.lab_resultDo(selected_order_detail.result_id, "approve", {}).subscribe(result => this.searchSelected_Orderss());
  }

  rejectOrder(selected_order_detail: Selected_OrdersDetail) {
    // selected_order_detail.status = selected_orders_status.rejected;
    // selected_order_detail.result = -1;
    // this.updateResult(selected_order_detail);
    let dialogRef = this.ordersNavigator.addResult(selected_order_detail.lab_test_id, selected_order_detail, true);
    dialogRef.afterClosed().subscribe(result => {
      if (result){
      this.rad_resultPersist.lab_resultDo(selected_order_detail.result_id, "reject", {reject_description: result.reject_description}).subscribe(res =>
        {
          this.reload()
        });
      }
    })
  }

  printRadOrder(item: Selected_OrdersDetail){
    this.ordersPersist
      .selected_orderssDo(
        item.id,"print",item
      )
      .subscribe(
        (downloadJob: TCId) => {
          this.jobPersist.followJob(downloadJob.id, 'printing rad order', true);
          let success_state = 3;
          this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
            if (job.job_state == success_state) {
            }
          });
        });
  }

  followJob(
    id: string,
    descrption: string,
    is_download_job: boolean = true
  ): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }

    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe((n) => {
      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        console.log(jobstate)
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

          if (is_download_job) {
            this.jobPersist.downloadFileKey(id).subscribe((jobFile) => {
              this.tcNavigator.openUrl(
                this.filePersist.downloadLink(jobFile.download_key)
              );
            });
          }
        } else {
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);
          console.log(this.message,"djkdkd");
          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }


  filteredData(data: Selected_OrdersSummary[], lab_order: string) {
    return data.filter(order => order.order_name == lab_order)
  }

  addResult(item: Selected_OrdersSummary){
      let dialogRef = this.ordersNavigator.addResult(item.order_id, item);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          item.result_description = result.result_description
          item.result_conclusion = result.result_conclusion
          this.updateResult(item)
        }
      });
    }

    updateTab(event: number){
      if(event){
        this.searchSelected_Orderss()
      }else {
        this.reload()
      }
    }

    calculateAge(dob: number): number {
      const current = new Date().getFullYear()
      return current - new Date(dob*1000).getFullYear()
    }
}
