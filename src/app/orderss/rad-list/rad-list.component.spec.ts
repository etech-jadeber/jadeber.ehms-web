import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadListComponent } from './rad-list.component';

describe('RadListComponent', () => {
  let component: RadListComponent;
  let fixture: ComponentFixture<RadListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
