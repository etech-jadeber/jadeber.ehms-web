import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {orderFilterColumn, OrdersDetail, OrdersSummary, OrdersSummaryPartialList} from "../../form_encounters/orderss/orders.model";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientDetail } from 'src/app/patients/patients.model';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { lab_order_type, lab_urgency, OrderStatus } from 'src/app/app.enums';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCAppInit } from 'src/app/tc/app-init';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { TCId } from 'src/app/tc/models';


@Component({
  selector: 'rad-orders-list',
  templateUrl: './rad-list.component.html',
  styleUrls: ['./rad-list.component.css']
})
export class  RadListComponent implements OnInit {

  orderssData: OrdersSummary[] = [];
  orderssTotalCount: number = 0;
  orderssSelectAll:boolean = false;
  orderssSelection: OrdersSummary[] = [];
  radOrderFilterColumn = orderFilterColumn;

  orderssDisplayedColumns: string[] = ["select", "action", "p.pid", "patient_name", "provider_id", "generated_sample_id", "future_date","total_order","completed", "rejected", "requested", "patient_type"];
  orderssSearchTextBox: FormControl = new FormControl();
  orderssIsLoading: boolean = false;
  user_id: string;
  startDateTextField: FormControl = new FormControl({disabled: true, value: this.ordersPersist.radOrderSearchHistory.ordered_start_date ? this.tcUtilsDate.toDate(this.ordersPersist.radOrderSearchHistory.ordered_start_date) : null});
  endDateTextField: FormControl = new FormControl({disabled: true, value: this.ordersPersist.radOrderSearchHistory.ordered_end_date ? this.tcUtilsDate.toDate(this.ordersPersist.radOrderSearchHistory.ordered_end_date) : null});
  doctors: {[id: string]: DoctorDetail} = {}

  @ViewChild(MatPaginator, {static: true}) orderssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) orderssSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public ordersPersist: OrdersPersist,
                public ordersNavigator: OrdersNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public doctorPersist: DoctorPersist,
                public tcUtilsString: TCUtilsString,
                public outsidePersist: Outside_OrdersPersist,
                public encounterPersist: Form_EncounterPersist,
                public labOrderNavigator: Lab_OrderNavigator
    ) {

        this.tcAuthorization.requireRead("radiology_orders");
       this.orderssSearchTextBox.setValue(ordersPersist.radOrderSearchHistory.search_text);
      //delay subsequent keyup events
      this.orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.ordersPersist.radOrderSearchHistory.search_text = value;
        this.searchOrderss();
      });
      this.user_id = TCAppInit.userInfo.user_id
      this.ordersPersist.radOrderSearchHistory.ordered_start_date = this.ordersPersist.radOrderSearchHistory.ordered_start_date
      this.startDateTextField.valueChanges.subscribe(value => {
        this.ordersPersist.radOrderSearchHistory.ordered_start_date =  value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        // this.ordersPersist.radOrderSearchHistory.ordered_start_date = this.ordersPersist.orderedStartDate
        this.searchOrderss();
      });
      this.endDateTextField.valueChanges.subscribe(value => {
        this.ordersPersist.radOrderSearchHistory.ordered_end_date =  value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        // this.ordersPersist.rad_order_end_date = this.ordersPersist.orderedEndDate
        this.searchOrderss();
      });
    }

    ngOnDestroy() {
      // this.ordersPersist.rad_order_status = this.ordersPersist.orderStatusFilter
      // this.ordersPersist.orderStatusFilter = null;
      // this.ordersPersist.lab_order_id = null;
      // this.ordersPersist.orderedStartDate = null;
      // this.ordersPersist.orderedEndDate = null;
      // this.ordersPersist.lab_order_statuId = null;
      // this.ordersPersist.lab_urgencyId = null;
      // this.ordersPersist.resultDate = null;
      // this.ordersPersist.show_only_mine = null;
    }

    ngOnInit() {
      // this.ordersPersist.orderStatusFilter = this.ordersPersist.rad_order_status
      this.orderssSort.sortChange.subscribe(() => {
        this.orderssPaginator.pageIndex = 0;
        this.searchOrderss(true);
      });

      this.orderssPaginator.page.subscribe(() => {
        this.searchOrderss(true);
      });
      //start by loading items
      this.searchOrderss();
    }

    searchOrderss(isPagination:boolean = false): void {
      let paginator = this.orderssPaginator;
      let sorter = this.orderssSort;
      this.orderssIsLoading = true;
      this.ordersPersist.orderssDo("get_rad_order",{lab_order_id: this.ordersPersist.radOrderSearchHistory.rad_order_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active?? 'future_date', sorter.direction? sorter.direction : 'desc', this.ordersPersist.radOrderSearchHistory).subscribe(
        (labs:OrdersSummaryPartialList)=>{
          this.orderssData = labs.data;
          this.orderssData.forEach(
            orders => {
              if (!this.doctors[orders.provider_id]){
                this.doctors[orders.provider_id] = new DoctorDetail()
                orders.provider_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(orders.provider_id).subscribe(
                  doctor => {
                    this.doctors[orders.provider_id] = doctor;
                  }
                )
              }
              this.ordersPersist.ordersDo(orders.id, "has_paid_order", {}).subscribe((value: {is_paid: boolean}) => {
                orders.is_paid = value.is_paid
               })
            }
          )
          if (labs.total != -1){
            this.orderssTotalCount = labs.total
          }
          this.orderssIsLoading = false;
        }, error => {
          this.orderssIsLoading = false;
          console.error(error)
        }
      )


    }

    pringOrder(order: OrdersDetail){
      this.ordersPersist.ordersDo(order.id, "print_order", {}).subscribe(
        (downloadJob: TCId) => {
          this.jobPersist.followJob(downloadJob.id, 'printing Order', true);
          let success_state = 3;
          this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
            if (job.job_state == success_state) {
            }
          });
        },
        (error) => {}
      )
    }

    searchLabOrder(){
      const dialogRef = this.labOrderNavigator.pickLab_Orders(false, lab_order_type.imaging);
      dialogRef.afterClosed().subscribe(
        (orders: Lab_OrderDetail[]) => {
          if (orders.length){
            const ids = orders.map(order => order.id);
            const names = orders.map(order => order.name);
            this.ordersPersist.radOrderSearchHistory.rad_order_id = ids.join(',')
            this.ordersPersist.radOrderSearchHistory.rad_order_name = names.join(',')
            this.searchOrderss()
          }
        }
      )
    }

    generateSampleId(order: OrdersDetail) {
    this.ordersPersist.ordersDo(order.id, "generate_sample_id", {}).subscribe((result) => {
      this.searchOrderss();
    })
  }

  downloadOrderss(): void {
    if(this.orderssSelectAll){
         this.ordersPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download orderss", true);
      });
    }
    else{
        this.ordersPersist.download(this.tcUtilsArray.idsList(this.orderssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download orderss",true);
            });
        }
  }



  deleteOrders(item: OrdersSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Orders");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.ordersPersist.deleteOrders(item.id).subscribe(response => {
          this.tcNotification.success("Orders deleted");
          this.searchOrderss();
        }, error => {
        });
      }

    });
  }

  printResult(item: OrdersDetail){

    this.ordersPersist.ordersDo(item.id, "print_result", item).subscribe((downloadJob: TCId) => {

      this.jobPersist.followJob(downloadJob.id, "printing Result", true);
      let success_state = 3;
      this.jobPersist.getJob(downloadJob.id).subscribe(
        job => {
          if (job.job_state == success_state) {
          }
        }

      )
    }, error => {
    })
  }

  back():void{
      this.location.back();
    }

    loadOrderDetail(orders: OrdersDetail): void {
      if(!orders.generated_sample_id){
        this.tcNotification.error("Sample ID is not generated")
      }
      else{
        this.router.navigate([this.ordersNavigator.radOrdersUrl(orders.id)]);
      }
    }

    // getPatientStatus(order: OrdersDetail){
    //   if (order.urgency == lab_urgency.critical){
    //     return 'red'
    //   }
    //   if (order.urgency = lab_urgency.urgent){
    //     return '#1A73E8'
    //   }else {
    //     return 'green'
    //   }
    // }

    getPatientStatus(order: OrdersDetail){
      if (order.urgency == lab_urgency.critical || !order.is_paid){
        return 'red'
      }
      if (order.urgency = lab_urgency.urgent){
        return '#1A73E8'
      }else {
        return 'green'
      }
    }

    getDoctor(id: string){
      const {first_name, middle_name, last_name} = this.doctors[id]
      return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
    }
}
