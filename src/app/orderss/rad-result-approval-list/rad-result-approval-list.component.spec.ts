import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadResultApprovalListComponent } from './rad-result-approval-list.component';

describe('RadResultApprovalListComponent', () => {
  let component: RadResultApprovalListComponent;
  let fixture: ComponentFixture<RadResultApprovalListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadResultApprovalListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadResultApprovalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
