import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { lab_order_type, ResultType, selected_orders_status } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { Lab_ResultDetail, Lab_ResultSummary, Lab_ResultSummaryPartialList } from 'src/app/form_encounters/orderss/lab_results/lab_result.model';
import { Lab_ResultPersist } from 'src/app/form_encounters/orderss/lab_results/lab_result.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCModalWidths, TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import {Location} from '@angular/common';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { environment } from 'src/environments/environment';
import { RadiologyResultPersist } from 'src/app/radiology_result/radiology_result.persist';
import { RadiologyResultNavigator } from 'src/app/radiology_result/radiology_result.navigator';
import { RadiologyResultDetail } from 'src/app/radiology_result/radiology_result.model';
import { Selected_OrdersSummary } from 'src/app/form_encounters/orderss/orders.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';

@Component({
  selector: 'app-rad-result-approval-list',
  templateUrl: './rad-result-approval-list.component.html',
  styleUrls: ['./rad-result-approval-list.component.css']
})
export class RadResultApprovalListComponent implements OnInit {

  radResultApprovalsData: Lab_ResultSummary[] = [];
  radResultApprovalsTotalCount: number = 0;
  radResultApprovalSelectAll:boolean = false;
  radResultApprovalSelection: Lab_ResultSummary[] = [];

 radResultApprovalsDisplayedColumns: string[] = ["select","action","lab_test_id" ,"urgency","lab_panel","result", "detail" ];
 lab_resultSearchText: FormControl = new FormControl();
  radResultApprovalIsLoading: boolean = false; 
  
  @Input() orderId: string;
  @Input() patientId: string;
  @Input() lab_order_id: string;
  @Output() removeOrder = new EventEmitter<string>();
  @ViewChild(MatPaginator, {static: true}) radResultApprovalsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) radResultApprovalsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public radResultApprovalPersist: RadiologyResultPersist,
                public labResultPersist: Lab_ResultPersist,
                public radResultApprovalNavigator: OrdersNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public orderPersist: OrdersPersist,
                public labPanelPersist: Lab_PanelPersist,
                public tcUtilsString: TCUtilsString,
                public radResultNavigator: RadiologyResultNavigator,

    ) {

        this.tcAuthorization.requireRead("radiology_result_approvals");
       this.lab_resultSearchText.setValue(radResultApprovalPersist.radiologyResultSearchText);
      //delay subsequent keyup events
      this.lab_resultSearchText.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.radResultApprovalPersist.radiologyResultSearchText = value;
        this.searchradResultApprovals();
      });
    } 

    ngOnInit(): void {
      
    }
    
    ngAfterViewInit() {
   
      this.radResultApprovalsSort.sortChange.subscribe(() => {
        this.radResultApprovalsPaginator.pageIndex = 0;
        this.searchradResultApprovals(true);
      });

      this.radResultApprovalsPaginator.page.subscribe(() => {
        this.searchradResultApprovals(true);
      });
      //start by loading items
      this.searchradResultApprovals();
    }

  searchradResultApprovals(isPagination:boolean = false): void {


    let paginator = this.radResultApprovalsPaginator;
    let sorter = this.radResultApprovalsSort;

    this.radResultApprovalIsLoading = true;
    this.radResultApprovalSelection = [];

    this.labResultPersist.lab_resultsDo("need_approve_tests", {order_type: lab_order_type.imaging, status: selected_orders_status.ready_to_approve, order_id : this.orderId, lab_order_id: this.lab_order_id}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Lab_ResultSummaryPartialList) => {
      this.radResultApprovalsData = partialList.data;
      if(!this.radResultApprovalsData.length){
        this.removeOrder.emit(this.orderId)
      }
      if (partialList.total != -1) {
        this.radResultApprovalsTotalCount = partialList.total;
      }
      this.radResultApprovalIsLoading = false;
    }, error => {
      this.radResultApprovalIsLoading = false;
    });

  }
 downloadradResultApprovals(): void {
    if(this.radResultApprovalSelectAll){
         this.radResultApprovalPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate", true);
      });
    }
    else{
        this.radResultApprovalPersist.download(this.tcUtilsArray.idsList(this.radResultApprovalSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download driver_medical_certificate",true);
            });
        }
  }
  approveOrder(selected_order_detail: Lab_ResultDetail) {
    this.radResultApprovalPersist.radiologyResultDo(selected_order_detail.id, "approve", {}).subscribe(result => this.searchradResultApprovals());
  }

  rejectOrder(selected_order_detail: Selected_OrdersSummary) {
    let dialogRef = this.radResultApprovalNavigator.addResult(selected_order_detail.lab_test_id, selected_order_detail, true);
    dialogRef.afterClosed().subscribe(result => {
      if (result){
      this.radResultApprovalPersist.radiologyResultDo(selected_order_detail.id, "reject", {reject_description: result.reject_description}).subscribe(res => 
        {
          this.searchradResultApprovals()
        });
      }
    })
  }

  showDetail(selected: RadiologyResultDetail){
    // let dialogRef = this.tcNavigator.confirmAction("View Dicom", "Image", `${selected.result}<br><br><h3>${selected.conclusion}</h3>`, "Cancel", TCModalWidths.large);
    //     dialogRef.afterClosed().subscribe(result => {
    //       this.radResultApprovalIsLoading = false;
    //     if (result){
    //       window.open(`${environment.dicom_url}/Viewer/${this.patientId}?${selected.selected_order_id}`)
    //       this.radResultApprovalIsLoading = false
    //     }
    //   })
    let dialogRef = this.radResultNavigator.showRadiologyResult(selected)
  }

  approveSelectedResults(){
    this.radResultApprovalSelection.forEach(resultApproval => {
      this.approveOrder(resultApproval)
    })
  }

  back():void{
      this.location.back();
    }

}
