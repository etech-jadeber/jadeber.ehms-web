import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResultApprovalListComponent } from './result-approval-list.component';

describe('ResultApprovalListComponent', () => {
  let component: ResultApprovalListComponent;
  let fixture: ComponentFixture<ResultApprovalListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultApprovalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultApprovalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
