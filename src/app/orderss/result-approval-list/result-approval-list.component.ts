import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {ActivatedRoute, Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Lab_ResultSummary, Lab_ResultSummaryPartialList } from 'src/app/form_encounters/orderss/lab_results/lab_result.model';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { lab_order_type } from 'src/app/app.enums';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';

@Component({
  selector: 'app-orders-to-approve-list',
  templateUrl: './result-approval-list.component.html',
  styleUrls: ['./result-approval-list.component.css']
})
export class ResultApprovalListComponent implements OnInit {

  ordersData: Lab_ResultSummary[] = [];
  ordersTotalCount: number = 0;
  orderSelectAll:boolean = false;
  orderSelection: Lab_ResultSummary[] = []; 
  ordersSearchText: FormControl = new FormControl();
  orderIsLoading: boolean = false; 
  order_type: number;
  lab_order_type = lab_order_type
  
  @ViewChild(MatPaginator, {static: true}) ordersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) ordersSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public orderPersist: OrdersPersist,
                public orderNavigator: OrdersNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public labPanelPersist: Lab_PanelPersist,
                public tcUtilsString: TCUtilsString,
                private route: ActivatedRoute

    ) {
       this.ordersSearchText.setValue(orderPersist.orderSearchHistory.search_text);
      //delay subsequent keyup events
      this.ordersSearchText.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.orderPersist.orderSearchHistory.search_text = value;
        this.searchorders();
      });
    }
    
    ngOnInit() {

      this.ordersPaginator.page.subscribe(() => {
        this.searchorders(true);
      });
      this.ordersPaginator.pageSize = 5;
      //start by loading items
      if(this.route.snapshot.routeConfig.path == 'laboratory_result_approvals'){
        this.tcAuthorization.requireRead("laboratory_result_approvals");
        this.order_type = lab_order_type.lab
      } else if (this.route.snapshot.routeConfig.path == 'radiology_result_approvals'){
        this.tcAuthorization.requireRead("radiology_result_approvals");
        this.order_type = lab_order_type.imaging
      }
      else if (this.route.snapshot.routeConfig.path == 'pathology_result_approvals'){
        this.tcAuthorization.requireRead("pathology_result_approvals");
        this.order_type = lab_order_type.pathology
      }
      this.searchorders();
    }

  searchorders(isPagination:boolean = false): void {


    let paginator = this.ordersPaginator;
    let sorter = this.ordersSort;

    this.orderIsLoading = true;
    this.orderSelection = [];

    this.orderPersist.orderssDo("needs_order_approval", {order_type: this.order_type}, paginator.pageSize, isPagination? paginator.pageIndex:0, "id", "asc").subscribe((partialList: Lab_ResultSummaryPartialList) => {
      this.ordersData = partialList.data;
      if (partialList.total != -1) {
        this.ordersTotalCount = partialList.total;
      }
      this.orderIsLoading = false;
    }, error => {
      this.orderIsLoading = false;
    });

  }
 downloadorders(): void {
    if(this.orderSelectAll){
         this.orderPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download order_list", true);
      });
    }
    else{
        this.orderPersist.download(this.tcUtilsArray.idsList(this.orderSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download order_list",true);
            });
        }
  }

  removeOrder(){
    this.searchorders()
  }
  
  back():void{
      this.location.back();
    }

}
