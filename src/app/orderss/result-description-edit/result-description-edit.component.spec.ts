import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultDescriptionEditComponent } from './result-description-edit.component';

describe('ResultDescriptionEditComponent', () => {
  let component: ResultDescriptionEditComponent;
  let fixture: ComponentFixture<ResultDescriptionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultDescriptionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultDescriptionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
