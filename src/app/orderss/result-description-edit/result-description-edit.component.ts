import { Component, Inject, OnInit } from '@angular/core';
import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Lab_ResultPersist } from 'src/app/form_encounters/orderss/lab_results/lab_result.persist';
import { lab_order_status, selected_orders_status } from 'src/app/app.enums';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { Selected_OrdersSummary } from 'src/app/form_encounters/orderss/orders.model';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';


@Component({
  selector: 'app-result-description-edit',
  templateUrl: './result-description-edit.component.html',
  styleUrls: ['./result-description-edit.component.css']
})
export class ResultDescriptionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  resultDescriptionDetail: Selected_OrdersSummary;
  onlyShow: boolean = false;
  // minRange: number;
  // maxRange: number;
  style: string = ""
  reject: boolean = false;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ResultDescriptionEditComponent>,
              public persist: Lab_ResultPersist,
              public selectedPersist: OrdersPersist,
              public labTestPersist: Lab_TestPersist,
              public tcNavigator: TCNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: {parentId: string, childId: Selected_OrdersSummary | null, context: {reject: boolean, mode: string}}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.context.mode == TCModalModes.WIZARD;
  }


  ngOnInit() {
    this.reject = this.idMode.context.reject;
    if(this.reject){
      this.resultDescriptionDetail = new Selected_OrdersSummary()
    }
    else if (this.isWizard()){
      this.title = "Laboratory Result";
      this.resultDescriptionDetail = this.idMode.childId as Selected_OrdersSummary;
      this.onlyShow = true;
    }
    else if (this.isNew()) {
      this.tcAuthorization.requireCreate('laboratory_results')
      this.title = this.appTranslation.getText("general","new") +  " Result";
      this.resultDescriptionDetail = this.idMode.childId as Selected_OrdersSummary;
      //set necessary defaults

      // this.getMinMaxRange()

    }
     else if (this.isEdit()) {
     this.tcAuthorization.requireUpdate("laboratory_results");
     this.title = this.appTranslation.getText("general","edit") +  " Result";
      this.isLoadingResults = true;
        // this.getMinMaxRange()
        this.resultDescriptionDetail = this.idMode.childId as Selected_OrdersSummary;
        if(this.resultDescriptionDetail.status === selected_orders_status.ready_to_approve || 
          this.resultDescriptionDetail.status === selected_orders_status.approved){
          this.onlyShow = true;
          this.title = "Result"
        }
        this.isLoadingResults = false;

    }
  }

  onAdd(): void {
      this.dialogRef.close(this.resultDescriptionDetail);
  }

  onUpdate(): void {
      this.dialogRef.close(this.resultDescriptionDetail);
  }


  canSubmit():boolean{
        if (this.reject && (this.resultDescriptionDetail.reject_description)){
          return true;
        }
        if (this.resultDescriptionDetail == null){
            return false;
          }

        if (this.resultDescriptionDetail.result_description == null || this.resultDescriptionDetail.result_description  === "") {
                      return false;
                    }
        if (this.resultDescriptionDetail.result_conclusion == null) {
          return false;
        }

        return true;
      }

      // getMinMaxRange(){
      //   this.labTestPersist.getLab_Test(this.idMode.parentId).subscribe(
      //     result => {
      //       if (result.min_range != null){
      //         this.minRange = result.min_range
      //       }
      //       if (result.max_range != null){
      //         this.maxRange = result.max_range
      //       }
      //       this.updateStyle(+this.resultDescriptionDetail.result_description)
      //     }
      //   )
      // }

      updateStyle(event: number){
        if (event != null){
          if (this.resultDescriptionDetail.min_range != null){
            if (this.resultDescriptionDetail.min_range <= event){
              this.style = 'green'
            } else {
              this.style = 'red'
              return
            }
          }
          if (this.resultDescriptionDetail.max_range != null){
            if (this.resultDescriptionDetail.max_range >= event){
              this.style = 'green'
            } else {
              this.style = 'red'
            }
          }
        }
      }


}
