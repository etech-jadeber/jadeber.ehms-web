import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import {  ResultType, payment_status, selected_orders_status, sex } from 'src/app/app.enums';
import { Lab_ResultPersist } from 'src/app/form_encounters/orderss/lab_results/lab_result.persist';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { PathologyResultNavigator } from 'src/app/pathology_result/pathology_result.navigator';
import { PathologyResultPersist } from 'src/app/pathology_result/pathology_result.persist';
import { SampleCollectionNavigator } from 'src/app/sample_collection/sample_collection.navigator';
import { SampleCollectionPersist } from 'src/app/sample_collection/sample_collection.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { OrdersDetail, Selected_OrdersDetail, Selected_OrdersSummary, Selected_OrdersSummaryPartialList } from '../../form_encounters/orderss/orders.model';
import {Location} from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AppTranslation } from 'src/app/app.translation';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { Lab_ResultDetail, Lab_ResultSummary } from 'src/app/form_encounters/orderss/lab_results/lab_result.model';
import { debounceTime } from 'rxjs';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { TCId } from 'src/app/tc/models';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { interval, Subscription } from "rxjs";
import { environment } from 'src/environments/environment';
import { PathologyResultDetail } from 'src/app/pathology_result/pathology_result.model';
import { RadiologyResultNavigator } from 'src/app/radiology_result/radiology_result.navigator';
import { RadiologyResultDetail } from 'src/app/radiology_result/radiology_result.model';
import { RadiologyResultPersist } from 'src/app/radiology_result/radiology_result.persist';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCAppInit } from 'src/app/tc/app-init';
import { LabTestNormalRangePersist } from 'src/app/lab-test-normal-range/lab-test-normal-range.persist';
import { LabTestNormalRangeDetail } from 'src/app/lab-test-normal-range/lab-test-normal-range.model';
import { PatientDetail } from 'src/app/patients/patients.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { MeasuringUnitDetail } from 'src/app/measuring_unit/measuringUnit.model';
import { MeasuringUnitPersist } from 'src/app/measuring_unit/measuringUnit.persist';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { OutsourcingCompanyPersist } from 'src/app/outsourcing_companys/outsourcing_company.persist';
import { OutsourcingCompanyDetail } from 'src/app/outsourcing_companys/outsourcing_company.model';

@Component({
  selector: 'app-selected-orders',
  templateUrl: './selected-orders.component.html',
  styleUrls: ['./selected-orders.component.scss']
})
export class SelectedOrdersComponent implements OnInit {
  message: string = '';
  success_state = 3;
  selected_orderssData: Selected_OrdersSummary[] = [];
  selected_orderssTotalCount: number = 0;
  selected_orderssSelectAll:boolean = false;
  selected_orderssSelected: Selected_OrdersSummary[] = [];
  selected_orderssDisplayedColumns: string[] = ["select", 'action', 'labtest_id', 'lab_panel_id', 'result_input', 'company', 'normal_range', 'conclusion', 'scanned_document_id', 'user_id', 'status', 'tat_time'];
  radSelected_OrderDisplayedColumns: string[] = ["select", 'rad_action', 'labtest_id', 'urgency', 'rad_result', 'scanned_document_id', 'status', 'tat_time']
  patSelected_OrderDisplayedColumns: string[] = ["select", 'action', 'labtest_id', 'lab_panel_id', 'urgency', 'pat_result','company', 'scanned_document_id', 'status', 'tat_time']
  selected_orderssSearchTextBox: FormControl = new FormControl('');
  selected_orderssLoading:boolean = false;
  selected_orders_status = selected_orders_status
  result_type = ResultType
  patientId: string;
  users: {[id: string]: UserDetail } = {}
  units: {[id: string]: MeasuringUnitDetail} = {}
  dicoms;
  outsourcing_companys: OutsourcingCompanyDetail[] = []

  @Input() order: OrdersDetail;
  @Input() labOrderId: string;
  @Input() resultType: number;
  @Input() status: number;
  @Input() patientDetail: PatientDetail;
  @Input() canAdd: boolean;
  @Output() allowSave = new EventEmitter<{canSave: boolean, labOrderId: string}>()
  @ViewChild(MatPaginator, {static: true}) selectedOrderPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) selectedOrderSort: MatSort;
  canCreate: boolean;
  canUpdate: boolean;
  canRead: boolean;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization:TCAuthorization,
    public tcAuthentication: TCAuthentication,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public filePersist:FilePersist,
    public tcNotification: TCNotification,
    public tcNavigator:TCNavigator,
    private http:HttpClient,
    public jobPersist:JobPersist,
    public appTranslation:AppTranslation,
    public pathologyResultNavigator: PathologyResultNavigator,
    public pathologyResultPersist: PathologyResultPersist,
    public ordersNavigator: OrdersNavigator,
    public  ordersPersist: OrdersPersist,
    public tcUtilsString: TCUtilsString,
    public lab_testPersist: Lab_TestPersist,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    public lab_resultPersist: Lab_ResultPersist,
    public sampleCollectionNavigator: SampleCollectionNavigator,
    public sampleCollectionPersist: SampleCollectionPersist,
    public formEncounterPersist: Form_EncounterPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public radiologyResultNavigator: RadiologyResultNavigator,
    public radiologyResultPersist: RadiologyResultPersist,
    public labTestNormalRangePersist: LabTestNormalRangePersist,
    public outsourcingCompanyPersist: OutsourcingCompanyPersist,
    public userPersist: UserPersist,
    public measuringUnitPersist: MeasuringUnitPersist,
    public paymentPersist: PaymentPersist,
  ) {
    this.selected_orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.ordersPersist.selected_ordersSearchText = value.trim();
      this.getSelectedOrderDeail();
    });
  }

  ngOnInit(): void {
    if(this.resultType == ResultType.img){
      this.status = selected_orders_status.approved;
      this.selected_orderssDisplayedColumns = this.radSelected_OrderDisplayedColumns
    }
    if(this.resultType == ResultType.pat){
      this.selected_orderssDisplayedColumns = this.patSelected_OrderDisplayedColumns
      this.getOutsourcingCompany()
    }
    if(this.resultType == ResultType.lab){
      this.getOutsourcingCompany()
    }
    this.canCreate = this.tcAuthorization.canCreate(this.tcUtilsArray.getEnum(this.ordersPersist.lab_result_acl, this.order.order_type)?.name)
    this.canUpdate = this.tcAuthorization.canUpdate(this.tcUtilsArray.getEnum(this.ordersPersist.lab_result_acl, this.order.order_type)?.name)
    this.canRead = this.tcAuthorization.canRead(this.tcUtilsArray.getEnum(this.ordersPersist.lab_result_acl, this.order.order_type)?.name)
    this.ordersPersist.lab_order_id = this.labOrderId
    this.selectedOrderPaginator.page.subscribe(value => {
      this.ordersPersist.lab_order_id = this.labOrderId
      this.saveResult(true)
    })
    this.selectedOrderSort.sortChange.subscribe(value => {
      this.ordersPersist.lab_order_id = this.labOrderId
      this.getSelectedOrderDeail(true)
    })
    this.selectedOrderSort.active = "status"
    this.selectedOrderPaginator.pageSize = 50;
    this.getSelectedOrderDeail()
  }

  ngOnDestroy(): void {
    this.ordersPersist.lab_order_id = null;
  }

  documentsUpload(fileInputEvent: any, element: Selected_OrdersDetail): void {
    this.selected_orderssLoading = true;
    let fd: FormData = new FormData();
    for (const file of fileInputEvent.target.files) {
      if (file.size > 20000000) {
        this.tcNotification.error("File too big");
        return;
      }
      fd.append('file', file);
    }
    this.http.post(this.ordersPersist.dicomUploadUrl(element.id), fd).subscribe(response => {
      this.selected_orderssLoading = false
      this.tcNotification.success("Success");
      this.getSelectedOrderDeail();
    });
  }

  getOutsourcingCompany(){
    this.outsourcingCompanyPersist.OutsourcingCompanysDo("get_all_active", {}).subscribe(
      (value: OutsourcingCompanyDetail[]) => {
        this.outsourcing_companys = value
      }
    )
  }

getSelectedOrderDeail(isPagination: boolean = false){
  const paginator = this.selectedOrderPaginator
  const sorter = this.selectedOrderSort
  this.selected_orderssLoading = true
  this.ordersPersist.selected_orderssDo(this.order.id, "get_test_by_type", {"lab_order_id": this.labOrderId}, this.selected_orderssSearchTextBox.value , paginator.pageSize,
    isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((value: Selected_OrdersSummaryPartialList) => {
      this.selected_orderssData = value.data;
      this.selected_orderssData.forEach(order => {
        // this.ordersPersist.dicomDatasDo(order.id, 'didUploadDicom', {}).subscribe(
        //   (result: any) => {
        //     order.dataUploaded = result.dataUploaded
        //   })
        this.paymentPersist.getPayment(order.payment_id).subscribe((payment) => {
          order.is_paid = payment.payment_status == payment_status.paid
        })
        this.resultType == this.result_type.lab && this.labTestNormalRangePersist.LabTestNormalRangesDo("get_normal_range", {lab_test_id: order.lab_test_id, sex: this.patientDetail.sex ? sex.male : sex.female, age: this.getPatientAge(this.patientDetail.dob)}).subscribe(
          (normalRange: LabTestNormalRangeDetail) => {
            order.min_range = normalRange[0]?.min_range
            order.max_range = normalRange[0]?.max_range

          }
        )
        if (this.resultType == this.result_type.lab && !this.units[order.test_unit_id]){
          this.units[order.test_unit_id] = new MeasuringUnitDetail()
          order.test_unit_id && order.test_unit_id != this.tcUtilsString.invalid_id && this.measuringUnitPersist.getMeasuringUnit(order.test_unit_id).subscribe(
          (measuringUnit: MeasuringUnitDetail) => {
            this.units[order.test_unit_id] = measuringUnit
          }
        )}
        this.lab_resultPersist.lab_resultsDo("get_result", {
          selected_order_id: order.id,
          lab_test_id: order.lab_test_id
        }).subscribe(
          (result: (Lab_ResultSummary | PathologyResultDetail | RadiologyResultDetail)[]) => {
            if (result.length){
              if (!this.users[result[0].result_by]){
                this.users[result[0].result_by] = new UserDetail()
                this.userPersist.getUser(result[0].result_by).subscribe(
                  nurse => {
                    this.users[result[0].result_by] = nurse;
                  }
                )
              }
              order.result_by = result[0].result_by
              order.status = result[0].status
              order.result_file_id = result[0].result_file_id
              order.result_id = result[0].id
              order.result_date = result[0].result_date
              order.company_id = result[0].company_id
              if (this.resultType == this.result_type.lab)
              {
                const lab_result = result[0] as Lab_ResultDetail
                order.result = lab_result.result
                order.result_conclusion = lab_result.result_conclusion;
                order.lab_result = lab_result
                const reference_range = this.tcUtilsString.getReferenceRange(lab_result.reference_range)
                order.min_range = reference_range?.min_range
                order.max_range = reference_range?.max_range
              if (lab_result.result.match(/^\d+$/g)){
                this.updateStyle(+order.result, order)
              }
            }
              else if(this.resultType == this.result_type.pat){
                order.pathology_result = result[0] as PathologyResultDetail
              }
              else if (this.resultType == this.result_type.img) {
                order.radiology_result = result[0] as RadiologyResultDetail
              }
            } else {
              order.company_id = this.tcUtilsString.invalid_id;
            }
          }
        )
        if(order.test_unit_id && order.test_unit_id != this.tcUtilsString.invalid_id && !this.units[order.test_unit_id])
         {
          this.units[order.test_unit_id] = new MeasuringUnitDetail()
          this.measuringUnitPersist.getMeasuringUnit(order.test_unit_id).subscribe(
          (measuringUnit: MeasuringUnitDetail) => {
            this.units[order.test_unit_id] = measuringUnit
          }
        )
      }
      })
      if (value.total != -1){
        this.selected_orderssTotalCount = value.total
      }
      this.selected_orderssLoading = false;
    }, error => {
      this.selected_orderssLoading = false;
      console.error(error)
    })
}

updateStyle(event: number, selectedOrders: Selected_OrdersSummary){
  if (event != null){
    if (selectedOrders.min_range != null){
      if (selectedOrders.min_range <= event){
        selectedOrders.style = 'green'
      } else {
        selectedOrders.style = 'red'
        return
      }
    }
    if (selectedOrders.max_range != null){
      if (selectedOrders.max_range >= event){
        selectedOrders.style = 'green'
      } else {
        selectedOrders.style = 'red'
      }
    }
  }
}

reload(){
  this.getSelectedOrderDeail()
}


showResult(selected_orders: Selected_OrdersDetail){
  if (selected_orders.result_file_id){
    this.filePersist.getDownloadKey(selected_orders.result_file_id).subscribe((value: any) => {
      window.open(this.filePersist.downloadLink(value.id))
    })
  }
}


documentUpload(fileInputEvent: any, selected_orderss:Selected_OrdersSummary): void {

  // if (fileInputEvent.target.files[0].size > 20000000) {
  //   this.tcNotification.error("File too big");
  //   return;
  // }

  // if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
  //   this.tcNotification.error("Unsupported file type");
  //   return;
  // }

  let fd: FormData = new FormData();
  fd.append('file', fileInputEvent.target.files[0]);
  this.http.post(this.lab_orderPersist.documentUploadUri(this.order.id), fd).subscribe(response => {
    if (!selected_orderss.pathology_result && this.resultType == this.result_type.pat){
      selected_orderss.pathology_result = new PathologyResultDetail()
      selected_orderss.pathology_result.result_file_id = response['id']
      selected_orderss.pathology_result.selected_order_id = selected_orderss.id
      selected_orderss.pathology_result.lab_test_id = selected_orderss.lab_test_id
    } else if (this.resultType == this.result_type.pat) {
     selected_orderss.pathology_result.result_file_id = response['id']
    }
    if (!selected_orderss.radiology_result && this.resultType == this.result_type.img){
      selected_orderss.radiology_result = new RadiologyResultDetail()
      selected_orderss.radiology_result.result_file_id = response['id']
      selected_orderss.radiology_result.selected_order_id = selected_orderss.id
      selected_orderss.radiology_result.lab_test_id = selected_orderss.lab_test_id
      this.saveRadResult([selected_orderss.radiology_result], false)
    }else if (this.resultType == this.result_type.img) {
      selected_orderss.radiology_result.result_file_id = response['id']
      this.saveRadResult([selected_orderss.radiology_result], false)
     }
    if (!selected_orderss.lab_result && this.resultType == this.result_type.lab){
      selected_orderss.lab_result = new Lab_ResultDetail()
      selected_orderss.lab_result.result_file_id = response['id']
      selected_orderss.lab_result.selected_order_id = selected_orderss.id
      selected_orderss.lab_result.lab_test_id = selected_orderss.lab_test_id
    }else if (this.resultType == this.result_type.lab) {
      selected_orderss.lab_result.result_file_id = response['id']
    }
  selected_orderss.result_file_id = response['id']
    this.allowSave.emit({canSave: true, labOrderId : this.labOrderId})
  });
}



approveOrder(selected_order_detail: Selected_OrdersDetail) {
  if (this.resultType == ResultType.lab){
    this.lab_resultPersist.lab_resultDo(selected_order_detail.result_id, "approve", {}).subscribe(result => this.reload());
  } else if (this.resultType == ResultType.pat) {
    this.pathologyResultPersist.pathologyResultDo(selected_order_detail.result_id, "approve", {}).subscribe(result => this.reload());
  } else {
    this.radiologyResultPersist.radiologyResultDo(selected_order_detail.result_id, "approve", {}).subscribe(result => this.reload());
  }
}

rejectOrder(selected_order_detail: Selected_OrdersDetail) {
  let dialogRef = this.ordersNavigator.addResult(selected_order_detail.lab_test_id, null, true);
  dialogRef.afterClosed().subscribe(result => {
    if (result){
      if (this.resultType == ResultType.lab){
        this.lab_resultPersist.lab_resultDo(selected_order_detail.result_id, "reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      } else if (this.resultType == ResultType.pat) {
        this.pathologyResultPersist.pathologyResultDo(selected_order_detail.result_id, "reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      } else {
        this.radiologyResultPersist.radiologyResultDo(selected_order_detail.result_id, "reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      }
    }
  })
}

masterRejectOrder(selected_order_detail: Selected_OrdersDetail) {
  let dialogRef = this.ordersNavigator.addResult(selected_order_detail.lab_test_id, null, true);
  dialogRef.afterClosed().subscribe(result => {
    if (result){
      if (this.resultType == ResultType.lab){
        this.lab_resultPersist.lab_resultDo(selected_order_detail.result_id, "master_reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      } else if (this.resultType == ResultType.pat) {
        this.pathologyResultPersist.pathologyResultDo(selected_order_detail.result_id, "master_reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      } else {
        this.radiologyResultPersist.radiologyResultDo(selected_order_detail.result_id, "master_reject", {reject_description: result.reject_description}).subscribe(res =>
          {
            this.reload()
          });
      }
    }
  })
}

submitResult(selected_order_detail: Selected_OrdersDetail){
  this.radiologyResultPersist.radiologyResultDo(selected_order_detail.result_id, "submit", {}).subscribe(res =>
    {
      this.reload()
    });
}

addResult(item: Selected_OrdersSummary){
  if (this.resultType == ResultType.pat){
    let result = item.pathology_result
    if (!result){
      result = new PathologyResultDetail()
    }
    result.selected_order_id = item.id;
    result.lab_test_id = item.lab_test_id;
    this.addPatResult(result);
  } else if (this.resultType == ResultType.img){
    let result = item.radiology_result
    if (!result){
      result = new RadiologyResultDetail()
    }
    result.selected_order_id = item.id;
    result.lab_test_id = item.lab_test_id;
    this.addRadResult(result)
  } else if (this.resultType == ResultType.lab){
    this.addLabResult(item)
  }
}

addLabResult(item: Selected_OrdersSummary){
  let dialogRef;
  if ((item.status == selected_orders_status.approved || item.status == selected_orders_status.ready_to_approve ) && this.canRead){
    dialogRef = this.ordersNavigator.showResult(item.lab_test_id, {...item});
  } else if (this.canCreate) {
    dialogRef = this.ordersNavigator.addResult(item.lab_test_id, {...item});
  }
  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      item.result = result.result
      item.result_conclusion = result.result_conclusion
      this.allowSave.emit({canSave: true, labOrderId : this.labOrderId})
      this.updateStyle(result.result, item);
    }
  });
}

addRadResult(item: RadiologyResultDetail){
  let dialogRef;
  if (item.status && (item.status == selected_orders_status.approved || item.status == selected_orders_status.ready_to_approve ) && this.canRead){
    dialogRef = this.radiologyResultNavigator.showRadiologyResult({...item});
  } else if (this.canCreate) {
    if (!item.id){
      dialogRef = this.radiologyResultNavigator.addRadiologyResult({...item}, this.order.encounter_id);
    } else {
      dialogRef = this.radiologyResultNavigator.editRadiologyResult({...item});
    }
  }
  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      this.getSelectedOrderDeail()
      // console.log(item)
      // item = result
      // console.log(item)
      // // this.allowSave.emit({canSave: true, labOrderId : this.labOrderId})
      // // this.updateStyle(result.result, item);
      // this.saveRadResult([item], false)
    }
  });
}

zeroPad = (num, places) => String(num).padStart(places, '0')

getDifferenceDate(element: Selected_OrdersDetail) : string {
  if (!element.result_date || !this.order.sample_generated_date){
    return "";
  }
  const diff = element.result_date - this.order.sample_generated_date
  if (element.tat_time && element.tat_time != -1 && element.tat_time >= diff){
    element.tat_style = 'green'
  }
  else if (element.tat_time && element.tat_time != -1 && element.tat_time < diff){
    element.tat_style = 'red'
  } else {
    element.tat_style = 'black'
  }
  const day = Math.floor(diff / (24 * 3600))
  const dayRem = diff % (24 * 3600)
  const hour = Math.floor(dayRem / 3600)
  const hourRem = dayRem % 3600
  const min = Math.floor(hourRem / 60)
  return `${this.zeroPad(day, 2)}:${this.zeroPad(hour, 2)}:${this.zeroPad(min, 2)}`
}

addPatResult(item: PathologyResultDetail){
  let dialogRef;
  if (item.status && (item.status == selected_orders_status.approved || item.status == selected_orders_status.ready_to_approve ) && this.canRead){
    dialogRef = this.pathologyResultNavigator.showPathologyResult({...item});
  } else if (this.canCreate) {
    if (!item.id){
      dialogRef = this.pathologyResultNavigator.addPathologyResult({...item}, this.order.encounter_id);
    } else {
      dialogRef = this.pathologyResultNavigator.editPathologyResult({...item});
    }
  }
  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      item = result
      // this.allowSave.emit({canSave: true, labOrderId : this.labOrderId})
      // this.updateStyle(result.re, item);
      this.savePathologyResult([item], false)
    }
  })
}

canSubmitPatResult(pathologyResultDetail): boolean {
  if (pathologyResultDetail == null) {
    return false;
  }

  if (pathologyResultDetail.clinical_finding == null || pathologyResultDetail.clinical_finding == "") {
    return false;
  }
  if (pathologyResultDetail.gross_examination == null || pathologyResultDetail.gross_examination == "") {
    return false;
  }
  if (pathologyResultDetail.microscopic_evaluation == null || pathologyResultDetail.microscopic_evaluation == "") {
    return false;
  }
  if (pathologyResultDetail.conclusion == null || pathologyResultDetail.conclusion == "") {
    return false;
  }
  if (pathologyResultDetail.comment == null || pathologyResultDetail.comment == "") {
    return false;
  }
  return true;

}

saveResult(nextPage: boolean = false){
  if (this.selected_orderssData.length){
    this.selected_orderssLoading = true;
    const results = this.selected_orderssData.filter(selected => (!selected.status || selected.status == selected_orders_status.rejected || selected.status == selected_orders_status.submitted) &&
    (selected.result || selected.clinical_finding || selected.normal_result || selected.result_file_id))
    if(!results.length){
      this.selected_orderssLoading = false;
      this.getSelectedOrderDeail(true)
      this.allowSave.emit({canSave: false, labOrderId : this.labOrderId})
    }
      if (this.resultType == this.result_type.pat){
        this.savePathologyResult(results.map(val => val.pathology_result), nextPage)
      } else if(this.resultType == this.result_type.lab) {
        this.saveLabResult(results.map(val => ({...val.lab_result, ...val})), nextPage)
      }
      // else {
      //   const rad = results.map(result => result.radiology_result)
      //   this.saveRadResult(rad, nextPage)
      // }
  }
}

savePathologyResult(results: PathologyResultDetail[], nextPage: boolean){
  results.forEach((value, idx, ary) => {
    // const pat_result = value
    // pat_result.result_file_id = value.result_file_id;
    // pat_result.lab_test_id = value.lab_test_id;
    // pat_result.id = value.id
    // pat_result.company_id = value.company_id
      this.pathologyResultPersist.addPathologyResult(value.selected_order_id, value).subscribe(
        value => {
          if (idx == ary.length - 1){
            this.selected_orderssLoading = false;
            if(nextPage){
              this.getSelectedOrderDeail(true)
            }else {
              this.reload()
            }
            this.allowSave.emit({canSave: false, labOrderId : this.labOrderId})
          }
        }, err => {
          this.selected_orderssLoading = false;
          console.log(err)
        }
      )
  })
}

approveSelectedResults(){
  this.selected_orderssSelected.forEach(resultApproval => {
    this.approveOrder(resultApproval)
  })
}

masterRejectSelectedResults(){
  this.selected_orderssSelected.forEach(resultApproval => {
    this.masterRejectOrder(resultApproval)
  })
}

saveLabResult(results: Selected_OrdersSummary[], nextPage: boolean){
  results.forEach((value, idx, ary) => {
  const lab_result = new Lab_ResultDetail()
      lab_result.result = value.result;
      lab_result.lab_test_id = value.lab_test_id;
      lab_result.result_conclusion = value.result_conclusion;
      lab_result.result_file_id = value.result_file_id;
      lab_result.id = value.result_id
      lab_result.company_id = value.company_id
      this.lab_resultPersist.addLab_Result(value.id, lab_result).subscribe(
        value => {
          if (idx == ary.length - 1){
            this.selected_orderssLoading = false;
            if(nextPage){
              this.getSelectedOrderDeail(true)
            }else {
              this.reload()
            }
            this.allowSave.emit({canSave: false, labOrderId : this.labOrderId})
          }
        }, error => {
          this.selected_orderssLoading = false;
          console.error(error)
        }
      )
  }
  )
}

saveRadResult(results: RadiologyResultDetail[], nextPage: boolean){
  results.forEach((value, idx, ary) => {
      this.radiologyResultPersist.addRadiologyResult(value).subscribe(
        value => {
          if (idx == ary.length - 1){
            this.selected_orderssLoading = false;
            if(nextPage){
              this.getSelectedOrderDeail(true)
            }else {
              const selectedOrder = new Selected_OrdersDetail()
              selectedOrder.result_id = value.id
              // this.approveOrder(selectedOrder)
            }
            this.allowSave.emit({canSave: false, labOrderId : this.labOrderId})
          }
        }, error => {
          this.selected_orderssLoading = false;
          console.error(error)
        }
      )
  }
  )
}

getRangeString(min_range: number, max_range: number): string{
  if (min_range && max_range){
    return ` (${min_range} - ${max_range})`
  }
  if (min_range){
    return ` >${min_range}`
  }
  if (max_range){
    return ` <${max_range}`
  }
}

buildDicomUrl(elementId:string):string{
  return  `${environment.dicom_viewer}${this.dicoms[0]['0020000D'].Value[0]}`;
}

allReadyToApprove(){
  return this.selected_orderssSelected.every(selected => selected.status == selected_orders_status.ready_to_approve)
}

allReadyToMasterReject(){
  return this.selected_orderssSelected.every(selected => selected.status == selected_orders_status.approved)
}

getPatientAge(dob: number){
  const year = new Date().getFullYear() - this.tcUtilsDate.toDate(dob).getFullYear()
  const month = new Date().getMonth() - this.tcUtilsDate.toDate(dob).getMonth()
  return year + month / 12
}

getUser(id: string){
  return this.users[id] ? `${this.users[id]?.name}` : ""
}

getUnit(id: string) {
  const unit = this.units[id]?.unit
  return unit ? `${unit}` : ''
}

}
