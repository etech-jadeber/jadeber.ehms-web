import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedOrdersListComponent } from './selected-orders-list.component';

describe('SelectedOrdersListComponent', () => {
  let component: SelectedOrdersListComponent;
  let fixture: ComponentFixture<SelectedOrdersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedOrdersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedOrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
