import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute } from '@angular/router';
import { Subscription, debounceTime } from 'rxjs';
import { tabs, selected_orders_status, ResultType, payment_status } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { OrdersSummary, OrdersDetail, OrdersSummaryPartialList, SampleCollectionDetail } from 'src/app/form_encounters/orderss/orders.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { Outside_OrdersPersist } from 'src/app/outside_orderss/outside_orders.persist';
import { GeneralPatientDetail, PatientDetail } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { SampleCollectionNavigator } from 'src/app/sample_collection/sample_collection.navigator';
import { SampleCollectionPersist } from 'src/app/sample_collection/sample_collection.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FileNavigator } from 'src/app/tc/files/file.navigator';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { SelectedOrdersComponent } from '../selected-orders-detail/selected-orders.component';
import {Location} from '@angular/common';

@Component({
  selector: 'app-selected-orders-list',
  templateUrl: './selected-orders-list.component.html',
  styleUrls: ['./selected-orders-list.component.scss']
})
export class SelectedOrdersListComponent implements OnInit {

  selected_orderssData: OrdersSummary[] = [];
  selected_orderssTotalCount: number = 0;
  selected_orderssSelectAll:boolean = false;
  selected_orderssSelected: OrdersSummary[] = [];
  selected_orderssDisplayedColumns: string[] = ['action', 'labtest_id', 'result_input', 'lab_panel_id', 'result', 'scanned_document_id', 'status'];
  selected_orderssSearchTextBox: FormControl = new FormControl();
  active_panel: string;

  paramsSubscription: Subscription;
  ordersLoading:boolean = false;

  labListTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  selected_orders_status = selected_orders_status;
  //basics

  canSave: {id: string}[] = [];
  @Input() resultType: number;
  @Input() ordersDetail: OrdersDetail;
  @Input() lab_order_id: string;
  @ViewChild(MatPaginator, {static: true}) selectedOrderPaginator: MatPaginator;
  @ViewChildren('child') children = new QueryList<SelectedOrdersComponent>();
  @Input() patientDetail: GeneralPatientDetail;
  @Input() sampleStatus: number;
  @Input() payment_status;
  @Output() handleBackButton = new EventEmitter()

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public filePersist:FilePersist,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              private http:HttpClient,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public ordersNavigator: OrdersNavigator,
              public  ordersPersist: OrdersPersist,
              public patientPersist: PatientPersist,
              public doctorPersist: DoctorPersist,
              public menuState: MenuState,
              public sampleCollectionNavigator: SampleCollectionNavigator,
              public sampleCollectionPersist: SampleCollectionPersist,
              public tcUtilsString: TCUtilsString,
              public outsidePersist: Outside_OrdersPersist,
              public fileNavigator: FileNavigator,) {

    this.ordersDetail = new OrdersDetail();
    this.selected_orderssSearchTextBox.setValue(ordersPersist.selected_ordersSearchText);
    this.selected_orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.ordersPersist.selected_ordersSearchText = value.trim();
      this.searchOrdersByType();
    });
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
    this.patientDetail && this.selectedOrderPaginator.page.subscribe(value => {
      this.searchOrdersByType(true)
    })
    this.searchOrdersByType()

  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  allowCanSave(event: {canSave: boolean, labOrderId: string}){
        if (event.canSave){
          if (!this.tcUtilsArray.containsId(this.canSave, event.labOrderId)){
            this.canSave = [...this.canSave, {id: event.labOrderId}]
          }
        } else {
          this.canSave = this.canSave.filter(save => save.id != event.labOrderId);
        }
  }

  searchOrdersByType(isPagination:boolean = false):void {
    let paginator = this.selectedOrderPaginator;
    this.ordersLoading = true;
    this.ordersPersist.ordersDo(this.ordersDetail.id, "order_by_type", {sample_status: this.sampleStatus, payment_status: this.payment_status, lab_order_id: this.lab_order_id}, paginator.pageSize, isPagination ? paginator.pageIndex : 0, "name", "asc").subscribe((response: OrdersSummaryPartialList) => {
      this.selected_orderssData = response.data;
      this.selected_orderssData.forEach(
        selected => {
          this.ordersDetail.generated_sample_id && this.sampleCollectionPersist.sampleCollectionsDo("get_sample_collection", {
            selected_order_id: selected.id,
            generated_sample_id: this.ordersDetail.generated_sample_id
          }).subscribe(
            (sample: SampleCollectionDetail) => {
              selected.sample_collection = sample[0]
            }
          )
        }
      )
      if (response.total != -1){
        this.selected_orderssTotalCount = response.total
      }
      this.ordersLoading = false;
    },error=>{
      this.ordersLoading = false
      console.error(error)
    });
}


   printOrders():void{
      this.ordersPersist.print(this.ordersDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print orders", true);
      });
    }

  back():void{
    if (this.handleBackButton.observed){
      this.handleBackButton.emit()
    } else{
      this.location.back();
    }
    }

  addSample(item: Lab_OrderDetail){
    let dialogRef = this.sampleCollectionNavigator.addSampleCollection(this.ordersDetail.id, item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.searchOrdersByType()
      }
    })
  }

  editSample(sampleId: string){
    let dialogRef = this.sampleCollectionNavigator.editSampleCollection(sampleId);
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.searchOrdersByType()
      }
    })
  }

  saveResult(i: number){
    this.children.toArray()[i].saveResult()
  }

  acceptSample(sample){
    this.sampleCollectionPersist.getSampleCollection(sample.sample_collection.id).subscribe(
      result => {
        result.sample_status = selected_orders_status.approved;
        this.sampleCollectionPersist.updateSampleCollection(result).subscribe(
          res => {
              this.searchOrdersByType()
          }
        )
      }
    )
  }

  rejectSample(sample){
    let dialogRef = this.sampleCollectionNavigator.rejectSampleCollection(sample.sample_collection.id)
    dialogRef.afterClosed().subscribe(result => {
      if (result){
        this.searchOrdersByType()
      }
    })
  }


  generateSampleId() {
    this.ordersPersist.ordersDo(this.ordersDetail.id, "generate_sample_id", {}).subscribe((result: Lab_OrderDetail) => {
      this.searchOrdersByType();
    })
  }

}
