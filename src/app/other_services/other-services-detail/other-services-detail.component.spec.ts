import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherServicesDetailComponent } from './other-services-detail.component';

describe('OtherServicesDetailComponent', () => {
  let component: OtherServicesDetailComponent;
  let fixture: ComponentFixture<OtherServicesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherServicesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherServicesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
