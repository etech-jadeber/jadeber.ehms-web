import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {OtherServicesDetail} from "../other-services.model";
import {OtherServicesPersist} from "../other-services.persist";
import {OtherServicesNavigator} from "../other-services.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

export enum OtherServicesTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-other_services-detail',
  templateUrl: './other-services-detail.component.html',
  styleUrls: ['./other-services-detail.component.scss']
})
export class OtherServicesDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  otherServicesIsLoading:boolean = false;
  
  OtherServicesTabs: typeof OtherServicesTabs = OtherServicesTabs;
  activeTab: OtherServicesTabs = OtherServicesTabs.overview;
  //basics
  otherServicesDetail: OtherServicesDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public otherServicesNavigator: OtherServicesNavigator,
              public  otherServicesPersist: OtherServicesPersist) {
    this.tcAuthorization.requireRead("other_services");
    this.otherServicesDetail = new OtherServicesDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("other_services");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.otherServicesIsLoading = true;
    this.otherServicesPersist.getOtherServices(id).subscribe(otherServicesDetail => {
          this.otherServicesDetail = otherServicesDetail;
          this.otherServicesIsLoading = false;
        }, error => {
          console.error(error);
          this.otherServicesIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.otherServicesDetail.id);
  }
  editOtherServices(): void {
    let modalRef = this.otherServicesNavigator.editOtherServices(this.otherServicesDetail.id);
    modalRef.afterClosed().subscribe(otherServicesDetail => {
      TCUtilsAngular.assign(this.otherServicesDetail, otherServicesDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.otherServicesPersist.print(this.otherServicesDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print other_services", true);
      });
    }

  back():void{
      this.location.back();
    }

}