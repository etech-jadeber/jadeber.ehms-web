import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherServicesEditComponent } from './other-services-edit.component';

describe('OtherServicesEditComponent', () => {
  let component: OtherServicesEditComponent;
  let fixture: ComponentFixture<OtherServicesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherServicesEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherServicesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
