import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { OtherServicesDetail } from '../other-services.model';import { OtherServicesPersist } from '../other-services.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FeePersist } from 'src/app/fees/fee.persist';
@Component({
  selector: 'app-other_services-edit',
  templateUrl: './other-services-edit.component.html',
  styleUrls: ['./other-services-edit.component.scss']
})export class OtherServicesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  otherServicesDetail: OtherServicesDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OtherServicesEditComponent>,
              public  persist: OtherServicesPersist,
              public feePersist: FeePersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW || this.isWizard()) {
     this.tcAuthorization.requireCreate("other_services");
      this.title = this.appTranslation.getText("general","new") +  " " + "other services";
      this.otherServicesDetail = new OtherServicesDetail();
      //set necessary defaults
      this.otherServicesDetail.vat_status = -1;

    } else {
     this.tcAuthorization.requireUpdate("other_services");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "other services";
      this.isLoadingResults = true;
      this.persist.getOtherServices(this.idMode.id).subscribe(otherServicesDetail => {
        this.otherServicesDetail = otherServicesDetail;
        const feeSearchHistory = {...this.feePersist.defaultFeeSearchHistory}
        feeSearchHistory.target_id = this.otherServicesDetail.id;
        this.feePersist.searchFee(1, 0, 'id', 'asc').subscribe(result => {
          if (result.data.length){
            this.otherServicesDetail.price = result.data[0].price
            this.otherServicesDetail.vat_status = result.data[0].vat_status
          }
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addOtherServices(this.otherServicesDetail).subscribe(value => {
      this.tcNotification.success("otherServices added");
      this.otherServicesDetail.id = value.id;
      this.dialogRef.close(this.otherServicesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateOtherServices(this.otherServicesDetail).subscribe(value => {
      this.tcNotification.success("other_services updated");
      this.dialogRef.close(this.otherServicesDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  onWizard(): void {
    this.dialogRef.close(this.otherServicesDetail);
  }


  canSubmit():boolean{
        if (this.otherServicesDetail == null){
            return false;
          }

if (this.otherServicesDetail.name == null || this.otherServicesDetail.name  == "") {
            return false;
        }

        if (this.otherServicesDetail.price == null){
          return false;
        }

        // if (this.otherServicesDetail.vat_status == -1){
        //   return false;
        // }
        return true;

 }
 }
