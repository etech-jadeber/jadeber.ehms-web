import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherServicesListComponent } from './other-services-list.component';

describe('OtherServicesListComponent', () => {
  let component: OtherServicesListComponent;
  let fixture: ComponentFixture<OtherServicesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherServicesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherServicesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
