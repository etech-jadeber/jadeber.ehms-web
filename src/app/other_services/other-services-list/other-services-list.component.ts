import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OtherServicesSummary, OtherServicesSummaryPartialList } from '../other-services.model';
import { OtherServicesPersist } from '../other-services.persist';
import { OtherServicesNavigator } from '../other-services.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
@Component({
  selector: 'app-other_services-list',
  templateUrl: './other-services-list.component.html',
  styleUrls: ['./other-services-list.component.scss']
})export class OtherServicesListComponent implements OnInit {
  otherServicessData: OtherServicesSummary[] = [];
  otherServicessTotalCount: number = 0;
  otherServicesSelectAll:boolean = false;
  otherServicesSelection: OtherServicesSummary[] = [];

 otherServicessDisplayedColumns: string[] = ["select","action" ,"name","remark" ];
  otherServicesSearchTextBox: FormControl = new FormControl();
  otherServicesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) otherServicessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) otherServicessSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public otherServicesPersist: OtherServicesPersist,
                public otherServicesNavigator: OtherServicesNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("other_services");
       this.otherServicesSearchTextBox.setValue(otherServicesPersist.otherServicesSearchText);
      //delay subsequent keyup events
      this.otherServicesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.otherServicesPersist.otherServicesSearchText = value;
        this.searchOtherServicess();
      });
    } ngOnInit() {
   
      this.otherServicessSort.sortChange.subscribe(() => {
        this.otherServicessPaginator.pageIndex = 0;
        this.searchOtherServicess(true);
      });

      this.otherServicessPaginator.page.subscribe(() => {
        this.searchOtherServicess(true);
      });
      //start by loading items
      this.searchOtherServicess();
    }

  searchOtherServicess(isPagination:boolean = false): void {


    let paginator = this.otherServicessPaginator;
    let sorter = this.otherServicessSort;

    this.otherServicesIsLoading = true;
    this.otherServicesSelection = [];

    this.otherServicesPersist.searchOtherServices(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OtherServicesSummaryPartialList) => {
      this.otherServicessData = partialList.data;
      if (partialList.total != -1) {
        this.otherServicessTotalCount = partialList.total;
      }
      this.otherServicesIsLoading = false;
    }, error => {
      this.otherServicesIsLoading = false;
    });

  } downloadOtherServicess(): void {
    if(this.otherServicesSelectAll){
         this.otherServicesPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download other_services", true);
      });
    }
    else{
        this.otherServicesPersist.download(this.tcUtilsArray.idsList(this.otherServicesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download other_services",true);
            });
        }
  }
addOtherServices(): void {
    let dialogRef = this.otherServicesNavigator.addOtherServices();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchOtherServicess();
      }
    });
  }

  editOtherServices(item: OtherServicesSummary) {
    let dialogRef = this.otherServicesNavigator.editOtherServices(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteOtherServices(item: OtherServicesSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("other_services");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.otherServicesPersist.deleteOtherServices(item.id).subscribe(response => {
          this.tcNotification.success("other_services deleted");
          this.searchOtherServicess();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}