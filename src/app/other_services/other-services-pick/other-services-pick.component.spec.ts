import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherServicesPickComponent } from './other-services-pick.component';

describe('OtherServicesPickComponent', () => {
  let component: OtherServicesPickComponent;
  let fixture: ComponentFixture<OtherServicesPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherServicesPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherServicesPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
