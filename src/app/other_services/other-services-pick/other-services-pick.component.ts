import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { OtherServicesSummary, OtherServicesSummaryPartialList } from '../other-services.model';
import { OtherServicesPersist } from '../other-services.persist';
import { OtherServicesNavigator } from '../other-services.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-other_services-pick',
  templateUrl: './other-services-pick.component.html',
  styleUrls: ['./other-services-pick.component.scss']
})export class OtherServicesPickComponent implements OnInit {
  otherServicessData: OtherServicesSummary[] = [];
  otherServicessTotalCount: number = 0;
  otherServicesSelectAll:boolean = false;
  otherServicesSelection: OtherServicesSummary[] = [];

 otherServicessDisplayedColumns: string[] = ["select","name","remark" ];
  otherServicesSearchTextBox: FormControl = new FormControl();
  otherServicesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) otherServicessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) otherServicessSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public otherServicesPersist: OtherServicesPersist,
                public otherServicesNavigator: OtherServicesNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<OtherServicesPickComponent>,@Inject(MAT_DIALOG_DATA) public data: {
  selectOne: boolean,
    service_type: number
}

    ) {

        this.tcAuthorization.requireRead("other_services");
       this.otherServicesSearchTextBox.setValue(otherServicesPersist.otherServicesSearchText);
      //delay subsequent keyup events
      if(data.service_type){
        this.otherServicesPersist.service_type = data.service_type
      }
      this.otherServicesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.otherServicesPersist.otherServicesSearchText = value;
        this.searchOther_servicess();
      });
    } ngOnInit() {

      this.otherServicessSort.sortChange.subscribe(() => {
        this.otherServicessPaginator.pageIndex = 0;
        this.searchOther_servicess(true);
      });

      this.otherServicessPaginator.page.subscribe(() => {
        this.searchOther_servicess(true);
      });
      //start by loading items
      this.searchOther_servicess();
    }

    ngOnDestroy(){
  this.otherServicesPersist.service_type = null
    }

  searchOther_servicess(isPagination:boolean = false): void {


    let paginator = this.otherServicessPaginator;
    let sorter = this.otherServicessSort;

    this.otherServicesIsLoading = true;
    this.otherServicesSelection = [];

    this.otherServicesPersist.searchOtherServices(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OtherServicesSummaryPartialList) => {
      this.otherServicessData = partialList.data;
      if (partialList.total != -1) {
        this.otherServicessTotalCount = partialList.total;
      }
      this.otherServicesIsLoading = false;
    }, error => {
      this.otherServicesIsLoading = false;
    });

  }
  markOneItem(item: OtherServicesSummary) {
    if(!this.tcUtilsArray.containsId(this.otherServicesSelection,item.id)){
          this.otherServicesSelection = [];
          this.otherServicesSelection.push(item);
        }
        else{
          this.otherServicesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.otherServicesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
