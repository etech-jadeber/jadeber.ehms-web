import {TCId} from "../tc/models";export class OtherServicesSummary extends TCId {
    name:string;
    remark:string;
    price: number;
    vat_status: number;
    service_type: number;

    }
    export class OtherServicesSummaryPartialList {
      data: OtherServicesSummary[];
      total: number;
    }
    export class OtherServicesDetail extends OtherServicesSummary {
    }

    export class OtherServicesDashboard {
      total: number;
    }

    export class OxygenServicesSummary extends TCId {
      patient_id:string;
      start_date:number;
      end_date: number;
      oxygen_type: number;
      }
