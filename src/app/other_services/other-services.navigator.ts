import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {OtherServicesEditComponent} from "./other-services-edit/other-services-edit.component";
import {OtherServicesPickComponent} from "./other-services-pick/other-services-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { OxygenServiceComponent } from "./oxygen-service/oxygen-service.component";
import { OxygenService } from "../additional_service/additional_service.model";


@Injectable({
  providedIn: 'root'
})

export class OtherServicesNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  otherServicessUrl(): string {
    return "/other_servicess";
  }

  otherServicesUrl(id: string): string {
    return "/other_servicess/" + id;
  }

  viewOtherServicess(): void {
    this.router.navigateByUrl(this.otherServicessUrl());
  }

  viewOtherServices(id: string): void {
    this.router.navigateByUrl(this.otherServicesUrl(id));
  }

  editOtherServices(id: string): MatDialogRef<OtherServicesEditComponent> {
    const dialogRef = this.dialog.open(OtherServicesEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOtherServices(): MatDialogRef<OtherServicesEditComponent> {
    const dialogRef = this.dialog.open(OtherServicesEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOxygenServices(patient_id: string): MatDialogRef<OxygenServiceComponent> {
    const dialogRef = this.dialog.open(OxygenServiceComponent, {
          data: new TCIdMode(patient_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickOtherServicess(service_type: number = null, selectOne: boolean=false): MatDialogRef<OtherServicesPickComponent> {
      const dialogRef = this.dialog.open(OtherServicesPickComponent, {
        data: {selectOne, service_type},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
