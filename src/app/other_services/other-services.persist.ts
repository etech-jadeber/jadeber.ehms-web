import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {OtherServicesDashboard, OtherServicesDetail,OtherServicesSummaryPartialList, OxygenServicesSummary} from "./other-services.model";
import {service_type} from "../app.enums";
import {TCUtilsString} from "../tc/utils-string";



@Injectable({
  providedIn: 'root'
})
export class OtherServicesPersist {
 otherServicesSearchText: string = "";
 service_type: number;
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.otherServicesSearchText;
    //add custom filters
    return fltrs;
  }

  serviceType: TCEnum[] = [
    new TCEnum(service_type.other_service, "Other Service"),
    new TCEnum(service_type.medication, "Medication"),
    new TCEnum(service_type.guard, "Guard"),]

  searchOtherServices(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OtherServicesSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("other_services", this.otherServicesSearchText, pageSize, pageIndex, sort, order);
    if(this.service_type){
      url = TCUtilsString.appendUrlParameter(url, "service_type", this.service_type.toString());
    }
    return this.http.get<OtherServicesSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "other_services/do", new TCDoParam("download_all", this.filters()));
  }

  otherServicesDashboard(): Observable<OtherServicesDashboard> {
    return this.http.get<OtherServicesDashboard>(environment.tcApiBaseUri + "other_services/dashboard");
  }

  getOtherServices(id: string): Observable<OtherServicesDetail> {
    return this.http.get<OtherServicesDetail>(environment.tcApiBaseUri + "other_services/" + id);
  } addOtherServices(item: OtherServicesDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "other_services/", item);
  }

  addOxygenServices(item: OxygenServicesSummary): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "oxygen_services/", item);
  }

  updateOtherServices(item: OtherServicesDetail): Observable<OtherServicesDetail> {
    return this.http.patch<OtherServicesDetail>(environment.tcApiBaseUri + "other_services/" + item.id, item);
  }

  deleteOtherServices(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "other_services/" + id);
  }
 otherServicessDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "other_services/do", new TCDoParam(method, payload));
  }

  otherServicesDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "other_servicess/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "other_services/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "other_services/" + id + "/do", new TCDoParam("print", {}));
  }


}
