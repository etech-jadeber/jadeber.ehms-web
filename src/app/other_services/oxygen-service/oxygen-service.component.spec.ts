import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OxygenServiceComponent } from './oxygen-service.component';

describe('OxygenServiceComponent', () => {
  let component: OxygenServiceComponent;
  let fixture: ComponentFixture<OxygenServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OxygenServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OxygenServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
