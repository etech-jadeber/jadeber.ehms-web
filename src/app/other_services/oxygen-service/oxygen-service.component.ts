import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppTranslation } from 'src/app/app.translation';
import { FeePersist } from 'src/app/fees/fee.persist';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { OtherServicesEditComponent } from '../other-services-edit/other-services-edit.component';
import { OxygenServicesSummary } from '../other-services.model';
import { OtherServicesPersist } from '../other-services.persist';

@Component({
  selector: 'app-oxygen-service',
  templateUrl: './oxygen-service.component.html',
  styleUrls: ['./oxygen-service.component.scss']
})
export class OxygenServiceComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  start_time: string;
  end_time: string;
  otherServicesDetail: OxygenServicesSummary;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OtherServicesEditComponent>,
              public  persist: OtherServicesPersist,
              public feePersist: FeePersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("oxygen_services");
      this.title = this.appTranslation.getText("general","new") +  " " + "oxygen services";
      this.otherServicesDetail = new OxygenServicesSummary();
      this.otherServicesDetail.patient_id = this.idMode.id

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist.addOxygenServices(this.otherServicesDetail).subscribe(value => {
      this.tcNotification.success("Oxygen Service added");
      this.otherServicesDetail.id = value.id;
      this.dialogRef.close(this.otherServicesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

 canSubmit():boolean{
        if (this.otherServicesDetail == null){
            return false;
          }

if (!this.start_time) {
            return false;
        }
if (!this.end_time) {
            return false;
        }

        if (!this.otherServicesDetail.patient_id){
          return false;
        }
        if (!this.otherServicesDetail.oxygen_type){
          return false;
        }

        // if (this.otherServicesDetail.vat_status == -1){
        //   return false;
        // }
        return true;

 }

 transformDates() {

    this.otherServicesDetail.start_date = new Date(this.start_time).getTime() / 1000;
    this.otherServicesDetail.end_date = new Date(this.end_time).getTime() / 1000;
}

}
