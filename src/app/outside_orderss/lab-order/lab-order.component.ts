


import { I } from '@angular/cdk/keycodes';
import { Component, Inject, OnInit } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { lab_order_type, lab_urgency } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Lab_OrderSummary } from 'src/app/lab_orders/lab_order.model';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_PanelSummary } from 'src/app/lab_panels/lab_panel.model';
import { Lab_PanelPersist } from 'src/app/lab_panels/lab_panel.persist';
import { Lab_TestSummary } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { TCModalModes, TCParentChildIds } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Outside_OrdersPersist } from '../outside_orders.persist';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';


@Component({
  selector: 'app-lab-order',
  templateUrl: './lab-order.component.html',
  styleUrls: ['./lab-order.component.css']
})
export class LabOrderComponent implements OnInit {

  labOrders:Lab_OrderSummary[] = [];
  radiology:Lab_OrderSummary[] = [];
  labPanels:Lab_PanelSummary[] = [];
  labTests:Lab_TestSummary[] = [];

  radPanels:Lab_PanelSummary[] = [];
  radTests:Lab_TestSummary[] = [];

  selectedLabPanel:Lab_PanelSummary[] = [];
  selectedLabTest:Lab_TestSummary[] = [];

  selectedradPanel:Lab_PanelSummary[] = [];
  selectedradTest:Lab_TestSummary[] = [];



  lab_urgency:typeof lab_urgency = lab_urgency
  laburgency:number = lab_urgency.normal;
  radurgency:number = lab_urgency.normal;

  futureLab:boolean = false;
  futureRad:boolean = false;


  futureLabDate:Date = new Date();
  futureRadDate:Date = new Date();
  title: string;
 
  isWizard(): boolean {
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] == TCModalModes.EDIT;

  }

  onCancel(): void {
    this.dialogRef.close();
  }



  constructor(   public lab_panelPersist: Lab_PanelPersist,
    public lab_orderPersist: Lab_OrderPersist,
    private tcNotification:TCNotification,
    public lab_testPersist: Lab_TestPersist,
    public dialogRef: MatDialogRef<LabOrderComponent>,
    public appTranslation:AppTranslation,
    public orderPersisit:OrdersPersist,
    public tcUtilsArray:TCUtilsArray,
    private labOrderPersisit:Outside_OrdersPersist,
    public form_encounterPersist:Form_EncounterPersist,
    public tcUtilsDate:TCUtilsDate,
    @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) { }

  ngOnInit() {
    this.getALLLabTypes();
    this.getAllRadiologyTypes();
    this.futureLab = false;
      this.futureRad = false;
      this.futureLabDate = new Date();
      this.futureRadDate = new Date();
  }


  getALLLabTypes():void{
    this.lab_orderPersist.lab_ordersDo("get_all", {"type":lab_order_type.lab}).subscribe(
      (labOrders:Lab_OrderSummary[])=>{
        this.labOrders = labOrders;
        
      }
    )

  }
  
  getAllRadiologyTypes():void{
    this.lab_orderPersist.lab_ordersDo("get_all", {"type":lab_order_type.imaging}).subscribe(
      (radOrders:Lab_OrderSummary[])=>{
        this.radiology = radOrders;
        
      }
    )
  }
  
  setLabType(labType:string){
    this.lab_panelPersist.lab_panelsDo("get_by_type", {"type":labType}).subscribe(
      (labPanels:Lab_PanelSummary[])=>{
        this.labPanels = labPanels;
        
      }
    )

    this.lab_testPersist.lab_testsDo("get_by_type", {"type":labType}).subscribe(
      (labTests:Lab_TestSummary[])=>{
        this.labTests = labTests;
        
      }
    )
    
  }



  setRadType(labType:string){
    this.lab_panelPersist.lab_panelsDo("get_by_type", {"type":labType}).subscribe(
      (labPanels:Lab_PanelSummary[])=>{
        this.radPanels = labPanels;
        
      }
    )

    this.lab_testPersist.lab_testsDo("get_by_type", {"type":labType}).subscribe(
      (labTests:Lab_TestSummary[])=>{
        this.radTests = labTests;
        
      }
    )
    
  }

  orderLabNow():void{
    let payload  =  {"outside_id":this.ids.parentId,"future":this.futureLab, "futureDate":this.tcUtilsDate.toTimeStamp(this.futureLabDate),   "urgency":this.laburgency,"selectedLabPanel":this.selectedLabPanel,"selectedLabTest":this.selectedLabTest};
    this.labOrderPersisit.outside_orderssDo("lab_new_order",payload).subscribe(
      res=>{
        this.tcNotification.success("Added");
      }
    )
  }
 


  orderRadNow():void{
    let payload  =  {"outside_id":this.ids.parentId,"future":this.futureRad, "futureDate":this.tcUtilsDate.toTimeStamp(this.futureRadDate), "urgency":this.radurgency,"selectedLabPanel":this.selectedradPanel,"selectedLabTest":this.selectedradTest};
    this.labOrderPersisit.outside_orderssDo("rad_new_order",payload).subscribe(
      res=>{
        this.tcNotification.success("Added");
      }
    )
  }

}
