import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsideOrdersDetailComponent } from './outside-orders-detail.component';

describe('OutsideOrdersDetailComponent', () => {
  let component: OutsideOrdersDetailComponent;
  let fixture: ComponentFixture<OutsideOrdersDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideOrdersDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideOrdersDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
