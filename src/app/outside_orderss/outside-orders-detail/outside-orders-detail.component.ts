import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Outside_OrdersDetail} from "../outside_orders.model";
import {Outside_OrdersPersist} from "../outside_orders.persist";
import {Outside_OrdersNavigator} from "../outside_orders.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';

export enum Outside_OrdersTabs {
  overview,rad_order,lab_order,prescription
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-outside_orders-detail',
  templateUrl: './outside-orders-detail.component.html',
  styleUrls: ['./outside-orders-detail.component.css']
})
export class Outside_OrdersDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  outside_ordersLoading:boolean = false;

  Outside_OrdersTabs: typeof Outside_OrdersTabs = Outside_OrdersTabs;
  activeTab: Outside_OrdersTabs = Outside_OrdersTabs.overview;
  //basics
  outside_ordersDetail: Outside_OrdersDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  ordersTotalCount: number = 0;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              private tcUtilsDate:TCUtilsDate,
              private outsidNav:Outside_OrdersNavigator,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public outside_ordersNavigator: Outside_OrdersNavigator,
              public  outside_ordersPersist: Outside_OrdersPersist,
              public tcUtilsString: TCUtilsString,
              public filePersist: FilePersist) {
    this.tcAuthorization.requireRead("outside_orders");
    this.outside_ordersDetail = new Outside_OrdersDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("outside_orders");
  }

  viewPrescription(fileId: string) {
    this.filePersist.getDownloadKey(fileId).subscribe(value => {
      this.tcNavigator.openUrl(this.filePersist.downloadLink(value['id']), true)
    })
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }
   prescriptionssTotalCount:number=0;
   OnPrescriptionResult(count: number) {
    this.prescriptionssTotalCount = count;
  }
  loadDetails(id: string): void {
  this.outside_ordersLoading = true;
    this.outside_ordersPersist.getOutside_Orders(id).subscribe(outside_ordersDetail => {
          this.outside_ordersDetail = outside_ordersDetail;
          this.outside_ordersLoading = false;
        }, error => {
          console.error(error);
          this.outside_ordersLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.outside_ordersDetail.id);
  }

  addOutside_Orders(): void {
    let modalRef = this.outside_ordersNavigator.additionalOrders(this.outside_ordersDetail.id);
    modalRef.afterClosed().subscribe(modifiedOutside_OrdersDetail => {
      this.loadDetails(this.outside_ordersDetail.id);
    }, error => {
      console.error(error);
    });
  }

  editOutside_Orders(): void {
    let modalRef = this.outside_ordersNavigator.editOutside_Orders(this.outside_ordersDetail.id);
    modalRef.afterClosed().subscribe(modifiedOutside_OrdersDetail => {
      TCUtilsAngular.assign(this.outside_ordersDetail, modifiedOutside_OrdersDetail);
    }, error => {
      console.error(error);
    });
  }

   printOutside_Orders():void{
      this.outside_ordersPersist.print(this.outside_ordersDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print outside_orders", true);
      });
    }

  back():void{
      this.location.back();
    }


    addLab(): void {
      let dialogRef = this.outsidNav.addLab(this.outside_ordersDetail.id);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.tcNotification.success("sucess");
        }
      });
    }

    onOrderResult(count: number) {
      this.ordersTotalCount = count;
    }
}