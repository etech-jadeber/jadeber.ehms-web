import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Outside_OrdersEditComponent } from './outside-orders-edit.component';

describe('Outside_OrdersEditComponent', () => {
  let component: Outside_OrdersEditComponent;
  let fixture: ComponentFixture<Outside_OrdersEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Outside_OrdersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Outside_OrdersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
