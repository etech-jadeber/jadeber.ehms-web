import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCIdMode, TCModalModes} from "../../tc/models";


import {Outside_OrdersDetail} from "../outside_orders.model";
import {Outside_OrdersPersist} from "../outside_orders.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PrescriptionDrugNavigator } from 'src/app/form_encounters/prescriptions-edit/prescription-drug-navigator';
import { lab_order_type, OrderType, OutsideOrderType } from 'src/app/app.enums';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { CameraNavigator } from 'src/app/patients/camera.navigator';
import { WebcamImage } from 'ngx-webcam';
import { HttpClient } from '@angular/common/http';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { FormControl } from '@angular/forms';
import { OrdersPersist as form_encounterOrderPersist, OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { Order } from 'src/app/lab_panels/lab_panel.model';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
import { ProcedureNavigator } from 'src/app/procedures/procedure.navigator';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { OrderedOtheServiceNavigator } from 'src/app/ordered_othe_service/ordered_othe_service.navigator';


@Component({
  selector: 'app-outside_orders-edit',
  templateUrl: './outside-orders-edit.component.html',
  styleUrls: ['./outside-orders-edit.component.css']
})
export class Outside_OrdersEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  outside_ordersDetail: Outside_OrdersDetail;
  birthDate:FormControl = new FormControl({disabled: true, value: new Date()});
  prescription_pic: string
  birthYear: number = 0;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Outside_OrdersEditComponent>,
              public  persist: Outside_OrdersPersist,
              private tCUtilsDate:TCUtilsDate,
              public prescriptionNavigator: Form_EncounterNavigator,
              public labOrderNavigator: OrdersNavigator,
              public formEncounterNavigator: Form_EncounterNavigator,
              public tcUtilsArray: TCUtilsArray,
              private cameraNav:CameraNavigator,
              private http:HttpClient,
              public filePersist:FilePersist,
              public orderPersisit: form_encounterOrderPersist,
              public labOrderPersist: OrdersPersist,
              public orderOtherServiceNavigator: OrderedOtheServiceNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                this.birthDate.valueChanges.subscribe(value => {
                  this.birthYear = new Date().getFullYear() - value._d.getFullYear()
                })

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  isWizard(): boolean {
    return this.idMode.mode === TCModalModes.WIZARD;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("outside_orders");
      this.title = this.appTranslation.getText("general","new") +  "  " + this.appTranslation.getText("patient", "outside_order");
      this.outside_ordersDetail = new Outside_OrdersDetail();
      this.outside_ordersDetail.sex = false;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("outside_orders");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "outside_order");
      this.isLoadingResults = true;
      this.persist.getOutside_Orders(this.idMode.id).subscribe(outside_ordersDetail => {
        this.outside_ordersDetail = outside_ordersDetail;
        this.transformDates(false)
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.transformDates(true);
    this.submitOrder()
  }

  submitOrder(): void {
    if (this.outside_ordersDetail.outside_order_type.some(value => value == OutsideOrderType.prescription)){
      this.addPrescription()
    }
    else if (this.outside_ordersDetail.outside_order_type.some(value => value == OutsideOrderType.other_service)){
      this.addOtherService()
    }
    else if (this.outside_ordersDetail.outside_order_type.some(value => value == OutsideOrderType.procedure)){
      this.addProcedure()
    }
    else if (this.outside_ordersDetail.outside_order_type.some(value => value == OutsideOrderType.physiotherapy)){
      this.addPhysiotherapy()
    }
     else {
      this.addOrder(this.outside_ordersDetail.outside_order_type)
    }
  }

  addPrescription(): void {
    let dialogRef = this.prescriptionNavigator.wizardPrescription(this.outside_ordersDetail.id)
    dialogRef.afterClosed().subscribe((newPrescriptions) => {
      console.log(newPrescriptions)
      if (newPrescriptions) {
        this.persist.addOutside_Orders({...this.outside_ordersDetail, prescriptions: newPrescriptions}).subscribe(
          (value) => {
            this.dialogRef.close(this.outside_ordersDetail)
        this.tcNotification.success('added');
          }
        )
      }
    });
  }

  addProcedure(): void {
    let dialogRef = this.formEncounterNavigator.wizardPatient_Procedure()
    dialogRef.afterClosed().subscribe((newPatientProcedure) => {
      if (newPatientProcedure) {
        this.persist.addOutside_Orders({...this.outside_ordersDetail, patientProcedures: newPatientProcedure}).subscribe(
          (value) => {
            this.dialogRef.close(this.outside_ordersDetail)
        this.tcNotification.success('added');
          }
        )
      }
    });
  }

  addPhysiotherapy(): void {
    let dialogRef = this.formEncounterNavigator.wizardRehabilitation_Order(null)
    dialogRef.afterClosed().subscribe((physiotherapy) => {
      if (physiotherapy) {
        this.persist.addOutside_Orders({...this.outside_ordersDetail, physiotherapy: physiotherapy}).subscribe(
          (value) => {
            this.dialogRef.close(this.outside_ordersDetail)
        this.tcNotification.success('added');
          }
        )
      }
    });
  }

  addOtherService(): void {
    let dialogRef = this.orderOtherServiceNavigator.wizardOrderedOtheService()
    dialogRef.afterClosed().subscribe((newOtherServices) => {
      if (newOtherServices) {
        this.persist.addOutside_Orders({...this.outside_ordersDetail, otherServices: newOtherServices}).subscribe(
          (value) => {
            this.dialogRef.close(this.outside_ordersDetail)
        this.tcNotification.success('added');
          }
        )
      }
    });
  }

  addOrder(
    orderType: number[],
    isWizard: boolean = true,
    date: number = this.tCUtilsDate.toTimeStamp(new Date())
  ): void {
      let dialogRef = this.formEncounterNavigator.addLab(
        isWizard,
        this.outside_ordersDetail.id,
        null,
        date,
        orderType
      );
      dialogRef.afterClosed().subscribe((result: Order[]) => {
        if (result) {
          this.isLoadingResults = true;
          this.persist.addOutside_Orders({...this.outside_ordersDetail, orders: result}).subscribe(value => {
            this.outside_ordersDetail.id = value.id;
              this.dialogRef.close(this.outside_ordersDetail)
              this.tcNotification.success('Successfully Added');
          }, error => {
            this.isLoadingResults = false;
            console.error(error)
          })
        }
      });
  }


  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateOutside_Orders(this.outside_ordersDetail).subscribe(value => {
      this.tcNotification.success("Outside_Orders updated");
      this.dialogRef.close(this.outside_ordersDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  takePic(){
    this.cameraNav.takePic().afterClosed().subscribe(
      {
        next:(res:WebcamImage)=>{

         let fd: FormData = new FormData();

         const rawData = atob(res.imageAsBase64);
         const bytes = new Array(rawData.length);
         for (var x = 0; x < rawData.length; x++) {
           bytes[x] = rawData.charCodeAt(x);
          }
          const arr = new Uint8Array(bytes);
          const blob = new Blob([arr], {type: 'image/png'});


         fd.append('file', blob);

         this.http.post(this.persist.documentUploadUri(), fd).subscribe({
          next: (response) => {
            this.outside_ordersDetail.file_id = (<TCId>response).id;
           this.filePersist.getDownloadKey(this.outside_ordersDetail.file_id).subscribe(
              (res:any)=>{


                this.prescription_pic = this.filePersist.downloadLink(res.id);

              }
            );


          },

          error:(err)=>{
            console.log(err);

          }
        });

        },
        error:(err)=>{

        }
      }
    )
  }

  documentUpload(fileInputEvent: any): void {

    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.persist.documentUploadUri(), fd).subscribe({
      next: (response) => {
        this.outside_ordersDetail.file_id = (<TCId>response).id;
       this.filePersist.getDownloadKey(this.outside_ordersDetail.file_id).subscribe(
          (res:any)=>{

            this.prescription_pic = this.filePersist.downloadLink(res.id);

          }
        );


      },

      error:(err)=>{
        console.log(err);

      }
    }


    );
  }


  canSubmit():boolean{
    if (this.isWizard()){
      if (this.outside_ordersDetail.outside_order_type == null){
        return false;
      }
      return true;
    }
        if (this.outside_ordersDetail == null){
            return false;
          }

          if (this.outside_ordersDetail.outside_order_type == null && this.isNew()){
            return false;
          }

        if (this.outside_ordersDetail.patient_name == null || this.outside_ordersDetail.patient_name  == "") {
                      return false;
                    }
                    if (this.outside_ordersDetail.sex == null){
                      return false;
                    }
                    if (this.outside_ordersDetail.phone_no && !this.tcUtilsString.isValidPhone(this.outside_ordersDetail.phone_no)){
                      return false;
                    }
        return true;
      }
      transformDates(todate: boolean = true) {
        if (todate) {
          this.outside_ordersDetail.birth_date = this.tCUtilsDate.toTimeStamp(new Date(this.birthDate.value._d || this.birthDate.value));
        } else {
          this.birthDate.setValue(this.tCUtilsDate.toDate(this.outside_ordersDetail.birth_date), {emitEvent: false})
        } }

        updateBirthDate(){
          const birthdate = new Date()
          birthdate.setFullYear(birthdate.getFullYear() - this.birthYear)
          this.birthDate.setValue(birthdate, {emitEvent: false})
        }
}
