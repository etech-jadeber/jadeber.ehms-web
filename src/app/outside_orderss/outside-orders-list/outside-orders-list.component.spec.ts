import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsideOrdersListComponent } from './outside-orders-list.component';

describe('OutsideOrdersListComponent', () => {
  let component: OutsideOrdersListComponent;
  let fixture: ComponentFixture<OutsideOrdersListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideOrdersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideOrdersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
