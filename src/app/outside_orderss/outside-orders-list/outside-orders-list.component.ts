import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Outside_OrdersPersist} from "../outside_orders.persist";
import {Outside_OrdersNavigator} from "../outside_orders.navigator";
import {Outside_OrdersDetail, Outside_OrdersSummary, Outside_OrdersSummaryPartialList} from "../outside_orders.model";
import { TCUtilsString } from 'src/app/tc/utils-string';
import { FilePersist } from 'src/app/tc/files/file.persist';


@Component({
  selector: 'app-outside_orders-list',
  templateUrl: './outside-orders-list.component.html',
  styleUrls: ['./outside-orders-list.component.css']
})
export class Outside_OrdersListComponent implements OnInit {

  outside_orderssData: Outside_OrdersSummary[] = [];
  outside_orderssTotalCount: number = 0;
  outside_orderssSelectAll:boolean = false;
  outside_orderssSelection: Outside_OrdersSummary[] = [];

  outside_orderssDisplayedColumns: string[] = ["select","action", "patient_name", "phone_no","birth_date","sex","file_id","serial_id", ];
  outside_orderssSearchTextBox: FormControl = new FormControl();
  outside_orderssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) outside_orderssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) outside_orderssSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public outside_ordersPersist: Outside_OrdersPersist,
                public outside_ordersNavigator: Outside_OrdersNavigator,
                public jobPersist: JobPersist,
                public tcUtilsString: TCUtilsString,
                public filePersist: FilePersist,
    ) {

        this.tcAuthorization.requireRead("outside_orders");
       this.outside_orderssSearchTextBox.setValue(outside_ordersPersist.outside_ordersSearchText);
      //delay subsequent keyup events
      this.outside_orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.outside_ordersPersist.outside_ordersSearchText = value;
        this.searchOutside_Orderss();
      });

      this.outside_ordersPersist.date.setValue(new Date(new Date().toDateString()),{emitEvent:false})
      this.outside_ordersPersist.date.valueChanges.subscribe((value)=>{
        this.searchOutside_Orderss()
    });
    }

    ngOnInit() {

      this.outside_orderssSort.sortChange.subscribe(() => {
        this.outside_orderssPaginator.pageIndex = 0;
        this.searchOutside_Orderss(true);
      });

      this.outside_orderssPaginator.page.subscribe(() => {
        this.searchOutside_Orderss(true);
      });
      //start by loading items
      this.searchOutside_Orderss();
    }

    viewPrescription(fileId: string) {
      this.filePersist.getDownloadKey(fileId).subscribe(value => {
        this.tcNavigator.openUrl(this.filePersist.downloadLink(value['id']), true)
      })
    }

  searchOutside_Orderss(isPagination:boolean = false): void {


    let paginator = this.outside_orderssPaginator;
    let sorter = this.outside_orderssSort;

    this.outside_orderssIsLoading = true;
    this.outside_orderssSelection = [];

    this.outside_ordersPersist.searchOutside_Orders(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Outside_OrdersSummaryPartialList) => {
      this.outside_orderssData = partialList.data;
      if (partialList.total != -1) {
        this.outside_orderssTotalCount = partialList.total;
      }
      this.outside_orderssIsLoading = false;
    }, error => {
      this.outside_orderssIsLoading = false;
    });

  }

  downloadOutside_Orderss(): void {
    if(this.outside_orderssSelectAll){
         this.outside_ordersPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download outside_orderss", true);
      });
    }
    else{
        this.outside_ordersPersist.download(this.tcUtilsArray.idsList(this.outside_orderssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download outside_orderss",true);
            });
        }
  }



  addOutside_Orders(): void {
    let dialogRef = this.outside_ordersNavigator.addOutside_Orders();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchOutside_Orderss();
      }
    });
  }

  editOutside_Orders(item: Outside_OrdersSummary) {
    let dialogRef = this.outside_ordersNavigator.editOutside_Orders(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteOutside_Orders(item: Outside_OrdersSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Outside_Orders");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.outside_ordersPersist.deleteOutside_Orders(item.id).subscribe(response => {
          this.tcNotification.success("Outside_Orders deleted");
          this.searchOutside_Orderss();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }


  

}
