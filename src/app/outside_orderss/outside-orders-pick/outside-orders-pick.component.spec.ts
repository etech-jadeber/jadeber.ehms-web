import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsideOrdersPickComponent } from './outside-orders-pick.component';

describe('OutsideOrdersPickComponent', () => {
  let component: OutsideOrdersPickComponent;
  let fixture: ComponentFixture<OutsideOrdersPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsideOrdersPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsideOrdersPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
