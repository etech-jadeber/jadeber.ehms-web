import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Outside_OrdersDetail, Outside_OrdersSummary, Outside_OrdersSummaryPartialList} from "../outside_orders.model";
import {Outside_OrdersPersist} from "../outside_orders.persist";


@Component({
  selector: 'app-outside_orders-pick',
  templateUrl: './outside-orders-pick.component.html',
  styleUrls: ['./outside-orders-pick.component.css']
})
export class Outside_OrdersPickComponent implements OnInit {

  outside_orderssData: Outside_OrdersSummary[] = [];
  outside_orderssTotalCount: number = 0;
  outside_orderssSelection: Outside_OrdersSummary[] = [];
  outside_orderssDisplayedColumns: string[] = ["select", 'patient_name','birth_date','sex','file_id','serial_id' ];

  outside_orderssSearchTextBox: FormControl = new FormControl();
  outside_orderssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) outside_orderssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) outside_orderssSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public outside_ordersPersist: Outside_OrdersPersist,
              public dialogRef: MatDialogRef<Outside_OrdersPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("outside_orders");
    this.outside_orderssSearchTextBox.setValue(outside_ordersPersist.outside_ordersSearchText);
    //delay subsequent keyup events
    this.outside_orderssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.outside_ordersPersist.outside_ordersSearchText = value;
      this.searchOutside_Orderss();
    });
  }

  ngOnInit() {

    this.outside_orderssSort.sortChange.subscribe(() => {
      this.outside_orderssPaginator.pageIndex = 0;
      this.searchOutside_Orderss();
    });

    this.outside_orderssPaginator.page.subscribe(() => {
      this.searchOutside_Orderss();
    });

    //set initial picker list to 5
    this.outside_orderssPaginator.pageSize = 5;

    //start by loading items
    this.searchOutside_Orderss();
  }

  searchOutside_Orderss(): void {
    this.outside_orderssIsLoading = true;
    this.outside_orderssSelection = [];

    this.outside_ordersPersist.searchOutside_Orders(this.outside_orderssPaginator.pageSize,
        this.outside_orderssPaginator.pageIndex,
        this.outside_orderssSort.active,
        this.outside_orderssSort.direction).subscribe((partialList: Outside_OrdersSummaryPartialList) => {
      this.outside_orderssData = partialList.data;
      if (partialList.total != -1) {
        this.outside_orderssTotalCount = partialList.total;
      }
      this.outside_orderssIsLoading = false;
    }, error => {
      this.outside_orderssIsLoading = false;
    });

  }

  markOneItem(item: Outside_OrdersSummary) {
    if(!this.tcUtilsArray.containsId(this.outside_orderssSelection,item.id)){
          this.outside_orderssSelection = [];
          this.outside_orderssSelection.push(item);
        }
        else{
          this.outside_orderssSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.outside_orderssSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
