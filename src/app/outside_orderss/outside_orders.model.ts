import { Patient_ProcedureDetail } from "../form_encounters/form_encounter.model";
import { PrescriptionsDetailForList } from "../form_encounters/prescriptions-edit/prescriptions-edit.component";
import { Order } from "../lab_panels/lab_panel.model";
import { OtherServicesDetail } from "../other_services/other-services.model";
import { Physiotherapy } from "../physiotherapy/physiotherapy";
import {TCId} from "../tc/models";

export class Outside_OrdersSummary extends TCId {
  id:string;
  patient_name : string;
birth_date : number;
sex : boolean;
outside_id:string;
file_id : string;
serial_id : number;
outside_order_type: number[];
target_id: number;
phone_no: string;
orders: {[type: number]: {}};
prescriptions: PrescriptionsDetailForList;
patientProcedures: Patient_ProcedureDetail;
otherServices: OtherServicesDetail;
physiotherapy: Physiotherapy;
}

export class Outside_OrdersSummaryPartialList {
  data: Outside_OrdersSummary[];
  total: number;
}

export class Outside_OrdersDetail extends Outside_OrdersSummary {
  patient_name : string;
birth_date : number;
sex : boolean;
file_id : string;
serial_id : number;
}

export class Outside_OrdersDashboard {
  total: number;
}
