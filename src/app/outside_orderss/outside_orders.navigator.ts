import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import { Outside_OrdersEditComponent } from "./outside-orders-edit/outside-orders-edit.component";
import { Outside_OrdersPickComponent } from "./outside-orders-pick/outside-orders-pick.component";
import { LabOrderComponent } from "./lab-order/lab-order.component";


@Injectable({
  providedIn: 'root'
})

export class Outside_OrdersNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  outside_orderssUrl(): string {
    return "/outside_orders";
  }

  outside_ordersUrl(id: string): string {
    return "/outside_orders/" + id;
  }

  viewOutside_Orderss(): void {
    this.router.navigateByUrl(this.outside_orderssUrl());
  }

  viewOutside_Orders(id: string): void {
    this.router.navigateByUrl(this.outside_ordersUrl(id));
  }

  editOutside_Orders(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Outside_OrdersEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOutside_Orders(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Outside_OrdersEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  additionalOrders(outside_id: string): MatDialogRef<unknown,any>{
    const dialogRef = this.dialog.open(Outside_OrdersEditComponent, {
      data: new TCIdMode(outside_id, TCModalModes.WIZARD),
      width: TCModalWidths.medium
});
return dialogRef;
  }
  
   pickOutside_Orderss(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Outside_OrdersPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }

    addLab(outsideId:string): MatDialogRef<unknown,any> {
      let params: TcDictionary<string> = new TcDictionary();
      const dialogRef = this.dialog.open(LabOrderComponent, {
        data: new TCParentChildIds(outsideId, null, params),
        width: TCModalWidths.medium,
      });
      return dialogRef;
  
  
    }
  
}
