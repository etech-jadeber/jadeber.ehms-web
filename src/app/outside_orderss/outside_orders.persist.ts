import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Outside_OrdersDashboard, Outside_OrdersDetail, Outside_OrdersSummaryPartialList} from "./outside_orders.model";
import { OutsideOrderType } from '../app.enums';
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Outside_OrdersPersist {

  outside_ordersSearchText: string = "";

  outsideOrderTypeFilter: number ;
  date : FormControl=new FormControl({disabled: true, value: new Date()});

  OutsideOrderType: TCEnum[] = [
  new TCEnum( OutsideOrderType.prescription, 'Prescription'),
  new TCEnum(OutsideOrderType.physiotherapy, 'Physiotherapy'),
  new TCEnum( OutsideOrderType.procedure, 'Procedure'),
  new TCEnum( OutsideOrderType.other_service, 'Other Service'),
  new TCEnum( OutsideOrderType.lab_order, 'Laboratory Order'),
  new TCEnum( OutsideOrderType.rad_order, 'Radiology Order'),
  new TCEnum( OutsideOrderType.pat_order, "Pathology Order")
  ];

  constructor(private http: HttpClient) {
  }

  documentUploadUri() {
    return environment.tcApiBaseUri + 'patients/upload-image/';
  }

  searchOutside_Orders(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Outside_OrdersSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("outside_orders", this.outside_ordersSearchText, pageSize, pageIndex, sort, order);
    if (this.date.value)
      url = TCUtilsString.appendUrlParameter(url, "date", (new Date(this.date.value).getTime()/1000).toString())
    return this.http.get<Outside_OrdersSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.outside_ordersSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outside_orders/do", new TCDoParam("download_all", this.filters()));
  }

  outside_ordersDashboard(): Observable<Outside_OrdersDashboard> {
    return this.http.get<Outside_OrdersDashboard>(environment.tcApiBaseUri + "outside_orders/dashboard");
  }

  getOutside_Orders(id: string): Observable<Outside_OrdersDetail> {
    return this.http.get<Outside_OrdersDetail>(environment.tcApiBaseUri + "outside_orders/" + id);
  }

  addOutside_Orders(item: Outside_OrdersDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outside_orders/", item);
  }

  updateOutside_Orders(item: Outside_OrdersDetail): Observable<Outside_OrdersDetail> {
    return this.http.patch<Outside_OrdersDetail>(environment.tcApiBaseUri + "outside_orders/" + item.id, item);
  }

  deleteOutside_Orders(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "outside_orders/" + id);
  }

  outside_orderssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "outside_orders/do", new TCDoParam(method, payload));
  }

  outside_ordersDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "outside_orders/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "outside_orders/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "outside_orders/" + id + "/do", new TCDoParam("print", {}));
  }


}
