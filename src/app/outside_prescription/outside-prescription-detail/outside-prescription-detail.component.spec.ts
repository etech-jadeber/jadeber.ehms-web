import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsidePrescriptionDetailComponent } from './outside-prescription-detail.component';

describe('OutsidePrescriptionDetailComponent', () => {
  let component: OutsidePrescriptionDetailComponent;
  let fixture: ComponentFixture<OutsidePrescriptionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutsidePrescriptionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsidePrescriptionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
