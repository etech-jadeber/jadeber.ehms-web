import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { OutsidePrescriptionDetail } from '../outside_prescription.model';
import { OutsidePrescriptionPersist } from '../outside_prescription.persist';
import { OutsidePrescriptionNavigator } from '../outside_prescription.navigator';
import { PrescriptionRequestStatus, dispatch_status } from 'src/app/app.enums';
import { ItemDispatchNavigator } from 'src/app/item_dispatch/item_dispatch.navigator';
import { MatTabChangeEvent } from '@angular/material/tabs';

export enum OutsidePrescriptionTabs {
  overview,
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-outside-prescription-detail',
  templateUrl: './outside-prescription-detail.component.html',
  styleUrls: ['./outside-prescription-detail.component.scss']
})
export class OutsidePrescriptionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  outsidePrescriptionIsLoading:boolean = false;
  dispatch_status = dispatch_status;
  OutsidePrescriptionTabs: typeof OutsidePrescriptionTabs = OutsidePrescriptionTabs;
  activeTab: OutsidePrescriptionTabs = OutsidePrescriptionTabs.overview;
  //basics
  outsidePrescriptionDetail: OutsidePrescriptionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public itemDispatchNavigator: ItemDispatchNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public outsidePrescriptionNavigator: OutsidePrescriptionNavigator,
              public  outsidePrescriptionPersist: OutsidePrescriptionPersist) {
    this.tcAuthorization.requireRead("outside_prescriptions");
    this.outsidePrescriptionDetail = new OutsidePrescriptionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("outside_prescriptions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.outsidePrescriptionIsLoading = true;
    this.outsidePrescriptionPersist.getOutsidePrescription(id).subscribe(outsidePrescriptionDetail => {
          this.outsidePrescriptionDetail = outsidePrescriptionDetail;
          this.outsidePrescriptionIsLoading = false;
        }, error => {
          console.error(error);
          this.outsidePrescriptionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.outsidePrescriptionDetail.id);
  }
  editOutsidePrescription(): void {
    let modalRef = this.outsidePrescriptionNavigator.editOutsidePrescription(this.outsidePrescriptionDetail.id);
    modalRef.afterClosed().subscribe(outsidePrescriptionDetail => {
      TCUtilsAngular.assign(this.outsidePrescriptionDetail, outsidePrescriptionDetail);
    }, error => {
      console.error(error);
    });
  }

  printBirth():void{
    this.outsidePrescriptionPersist.print(this.outsidePrescriptionDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print outside_prescription", true);
    });
  }

  back():void{
      this.location.back();
  }
  onTabChanged(matTab: MatTabChangeEvent){
    this.outsidePrescriptionPersist.tab_index=matTab.index;
  }

  addDispatch() {
      let dialogRef = this.itemDispatchNavigator.addItemDispatch(this.outsidePrescriptionDetail, PrescriptionRequestStatus.accept);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.loadDetails(this.outsidePrescriptionDetail.id);
      }
    })
  }

}