import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsidePrescriptionEditComponent } from './outside-prescription-edit.component';

describe('OutsidePrescriptionEditComponent', () => {
  let component: OutsidePrescriptionEditComponent;
  let fixture: ComponentFixture<OutsidePrescriptionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutsidePrescriptionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsidePrescriptionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
