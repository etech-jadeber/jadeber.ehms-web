import {Component, OnInit, Inject, ViewChild} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { OutsidePrescriptionDetail } from '../outside_prescription.model';import { OutsidePrescriptionPersist } from '../outside_prescription.persist';import { ItemSummary } from 'src/app/items/item.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { TransferNavigator } from 'src/app/transfers/transfer.navigator';
import { TransferSummary } from 'src/app/transfers/transfer.model';
import { ItemDispatchEditChildComponent } from 'src/app/item_dispatch/item-dispatch-edit/item-dispatch-edit.component';
import { PrescriptionRequestStatus } from 'src/app/app.enums';
@Component({
  selector: 'app-outside-prescription-edit',
  templateUrl: './outside-prescription-edit.component.html',
  styleUrls: ['./outside-prescription-edit.component.scss']
})

export class OutsidePrescriptionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  outsidePrescriptionDetail: OutsidePrescriptionDetail;
  outsidePrescriptionDetailList: any;
markupPricePersist: any;
service_type: any;
prescriptionRequestStatus = PrescriptionRequestStatus;

@ViewChild( 'itemDispatch', { static: false }) itemDispatch: ItemDispatchEditChildComponent;

constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public itemNavigator: ItemNavigator,
              public transferNavigator: TransferNavigator,
              public dialogRef: MatDialogRef<OutsidePrescriptionEditComponent>,
              public  persist: OutsidePrescriptionPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("outside_prescriptions");
      this.title = this.appTranslation.getText("general","new") +  " " + "outside_prescription";
      this.outsidePrescriptionDetail = new OutsidePrescriptionDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("outside_prescriptions");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "outside_prescription";
      this.isLoadingResults = true;
      this.persist.getOutsidePrescription(this.idMode.id).subscribe(outsidePrescriptionDetail => {
        this.outsidePrescriptionDetail = outsidePrescriptionDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.outsidePrescriptionDetail.itemDispatch = this.itemDispatch.itemDispatchDetailList;
    this.persist.addOutsidePrescription(this.outsidePrescriptionDetail).subscribe(value => {
      this.tcNotification.success("outsidePrescription added");
      this.outsidePrescriptionDetail.id = value.id;
      this.dialogRef.close(this.outsidePrescriptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateOutsidePrescription(this.outsidePrescriptionDetail).subscribe(value => {
      this.tcNotification.success("outside_prescription updated");
      this.dialogRef.close(this.outsidePrescriptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }



  

  canSubmit():boolean{
      if (this.outsidePrescriptionDetail == null){
          return false;
        }

        if(this.outsidePrescriptionDetail.patient_name == null ){
          return false;
        }

        // if(this.outsidePrescriptionDetail.quantity == null || this.outsidePrescriptionDetail.quantity <=0){
        //   return false;
        // }

      return true;

    }
 }