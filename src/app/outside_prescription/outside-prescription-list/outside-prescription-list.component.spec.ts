import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsidePrescriptionListComponent } from './outside-prescription-list.component';

describe('OutsidePrescriptionListComponent', () => {
  let component: OutsidePrescriptionListComponent;
  let fixture: ComponentFixture<OutsidePrescriptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutsidePrescriptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsidePrescriptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
