import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { OutsidePrescriptionSummary, OutsidePrescriptionSummaryPartialList } from '../outside_prescription.model';
import { OutsidePrescriptionNavigator } from '../outside_prescription.navigator';
import { OutsidePrescriptionPersist } from '../outside_prescription.persist';
import { PrescriptionRequestSummary } from 'src/app/prescription_request/prescription_request.model';
import { PrescriptionRequestStatus } from 'src/app/app.enums';
import { ItemDispatchNavigator } from 'src/app/item_dispatch/item_dispatch.navigator';
import { RequestPersist } from 'src/app/requests/request.persist';
import { PrescriptionRequestPersist } from 'src/app/prescription_request/prescription_request.persist';
@Component({
  selector: 'app-outside-prescription-list',
  templateUrl: './outside-prescription-list.component.html',
  styleUrls: ['./outside-prescription-list.component.scss']
})

export class OutsidePrescriptionListComponent implements OnInit {
  outsidePrescriptionsData: OutsidePrescriptionSummary[] = [];
  outsidePrescriptionsTotalCount: number = 0;
  outsidePrescriptionSelectAll:boolean = false;
  outsidePrescriptionSelection: OutsidePrescriptionSummary[] = [];
  prescriptionRequestStatus = PrescriptionRequestStatus;
 outsidePrescriptionsDisplayedColumns: string[] = ["select","action","mrn","patient_name", "request_date","response_date","status" ];
  outsidePrescriptionSearchTextBox: FormControl = new FormControl();
  outsidePrescriptionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) outsidePrescriptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) outsidePrescriptionsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public outsidePrescriptionPersist: OutsidePrescriptionPersist,
                public outsidePrescriptionNavigator: OutsidePrescriptionNavigator,
                public jobPersist: JobPersist,
                public prescriptionRequestPersist: PrescriptionRequestPersist,
                public itemDispatchNavigator: ItemDispatchNavigator,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("outside_prescriptions");

        this.outsidePrescriptionPersist.date.setValue(new Date(new Date().toDateString()));
       this.outsidePrescriptionSearchTextBox.setValue(outsidePrescriptionPersist.outsidePrescriptionSearchText);
      //delay subsequent keyup events
      this.outsidePrescriptionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.outsidePrescriptionPersist.outsidePrescriptionSearchText = value;
        this.searchOutside_prescriptions();
      });


      this.outsidePrescriptionPersist.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.searchOutside_prescriptions();
      });

    } ngOnInit() {
   
      this.outsidePrescriptionsSort.sortChange.subscribe(() => {
        this.outsidePrescriptionsPaginator.pageIndex = 0;
        this.searchOutside_prescriptions(true);
      });

      this.outsidePrescriptionsPaginator.page.subscribe(() => {
        this.searchOutside_prescriptions(true);
      });
      //start by loading items
      this.searchOutside_prescriptions();
    }

  searchOutside_prescriptions(isPagination:boolean = false): void {


    let paginator = this.outsidePrescriptionsPaginator;
    let sorter = this.outsidePrescriptionsSort;

    this.outsidePrescriptionIsLoading = true;
    this.outsidePrescriptionSelection = [];

    this.outsidePrescriptionPersist.searchOutsidePrescription(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OutsidePrescriptionSummaryPartialList) => {
      this.outsidePrescriptionsData = partialList.data;
      if (partialList.total != -1) {
        this.outsidePrescriptionsTotalCount = partialList.total;
      }
      this.outsidePrescriptionIsLoading = false;
    }, error => {
      this.outsidePrescriptionIsLoading = false;
    });

  } downloadOutsidePrescriptions(): void {
    if(this.outsidePrescriptionSelectAll){
         this.outsidePrescriptionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download outside_prescription", true);
      });
    }
    else{
        this.outsidePrescriptionPersist.download(this.tcUtilsArray.idsList(this.outsidePrescriptionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download outside_prescription",true);
            });
        }
  }
addOutside_prescription(): void {
    let dialogRef = this.outsidePrescriptionNavigator.addOutsidePrescription();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchOutside_prescriptions();
      }
    });
  }

  dispatchItems(item: PrescriptionRequestSummary){
    let dialogRef = this.itemDispatchNavigator.addItemDispatch(item, PrescriptionRequestStatus.dispatch);
  dialogRef.afterClosed().subscribe(result => {
    if(result){
      this.searchOutside_prescriptions();
      
    }
  })
  }
  editOutsidePrescription(item: OutsidePrescriptionSummary) {
    let dialogRef = this.outsidePrescriptionNavigator.editOutsidePrescription(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteOutsidePrescription(item: OutsidePrescriptionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("outside_prescription");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.outsidePrescriptionPersist.deleteOutsidePrescription(item.id).subscribe(response => {
          this.tcNotification.success("outside_prescription deleted");
          this.searchOutside_prescriptions();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}