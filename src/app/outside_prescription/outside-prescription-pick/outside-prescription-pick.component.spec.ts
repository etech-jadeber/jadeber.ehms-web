import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsidePrescriptionPickComponent } from './outside-prescription-pick.component';

describe('OutsidePrescriptionPickComponent', () => {
  let component: OutsidePrescriptionPickComponent;
  let fixture: ComponentFixture<OutsidePrescriptionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutsidePrescriptionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsidePrescriptionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
