import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OutsidePrescriptionSummary, OutsidePrescriptionSummaryPartialList } from '../outside_prescription.model';
import { OutsidePrescriptionPersist } from '../outside_prescription.persist';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-outside-prescription-pick',
  templateUrl: './outside-prescription-pick.component.html',
  styleUrls: ['./outside-prescription-pick.component.scss']
})
export class OutsidePrescriptionPickComponent implements OnInit {
  outsidePrescriptionsData: OutsidePrescriptionSummary[] = [];
  outsidePrescriptionsTotalCount: number = 0;
  outsidePrescriptionSelectAll:boolean = false;
  outsidePrescriptionSelection: OutsidePrescriptionSummary[] = [];

 outsidePrescriptionsDisplayedColumns: string[] = ["select","action","quantity" ];
  outsidePrescriptionSearchTextBox: FormControl = new FormControl();
  outsidePrescriptionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) outsidePrescriptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) outsidePrescriptionsSort: MatSort;  
  constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public dialogRef: MatDialogRef<OutsidePrescriptionPickComponent>,
                public outsidePrescriptionPersist: OutsidePrescriptionPersist,
                @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("outside_prescriptions");
       this.outsidePrescriptionSearchTextBox.setValue(outsidePrescriptionPersist.outsidePrescriptionSearchText);
      //delay subsequent keyup events
      this.outsidePrescriptionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.outsidePrescriptionPersist.outsidePrescriptionSearchText = value;
        this.searchOutside_prescriptions();
      });
    } ngOnInit() {
   
      this.outsidePrescriptionsSort.sortChange.subscribe(() => {
        this.outsidePrescriptionsPaginator.pageIndex = 0;
        this.searchOutside_prescriptions(true);
      });

      this.outsidePrescriptionsPaginator.page.subscribe(() => {
        this.searchOutside_prescriptions(true);
      });
      //start by loading items_
      this.searchOutside_prescriptions();
    }

  searchOutside_prescriptions(isPagination:boolean = false): void {


    let paginator = this.outsidePrescriptionsPaginator;
    let sorter = this.outsidePrescriptionsSort;

    this.outsidePrescriptionIsLoading = true;
    this.outsidePrescriptionSelection = [];

    this.outsidePrescriptionPersist.searchOutsidePrescription(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OutsidePrescriptionSummaryPartialList) => {
      this.outsidePrescriptionsData = partialList.data;
      if (partialList.total != -1) {
        this.outsidePrescriptionsTotalCount = partialList.total;
      }
      this.outsidePrescriptionIsLoading = false;
    }, error => {
      this.outsidePrescriptionIsLoading = false;
    });

  }
  markOneItem(item: OutsidePrescriptionSummary) {
    if(!this.tcUtilsArray.containsId(this.outsidePrescriptionSelection,item.id)){
          this.outsidePrescriptionSelection = [];
          this.outsidePrescriptionSelection.push(item);
        }
        else{
          this.outsidePrescriptionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.outsidePrescriptionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }