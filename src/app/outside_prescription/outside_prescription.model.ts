import { ItemDispatchDetail } from "../item_dispatch/item_dispatch.model";
import {TCId} from "../tc/models";
export class OutsidePrescriptionSummary extends TCId {
  batch_no: string;
  remaining_of_transfer: number;
  price: number;
  patient_name: string;
  total_price: number;
  itemDispatch: ItemDispatchDetail[];
}
export class OutsidePrescriptionSummaryPartialList {
  data: OutsidePrescriptionSummary[];
  total: number;
}
export class OutsidePrescriptionDetail extends OutsidePrescriptionSummary {
}

export class OutsidePrescriptionDashboard {
  total: number;
}