import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { OutsidePrescriptionEditComponent } from "./outside-prescription-edit/outside-prescription-edit.component";
import { OutsidePrescriptionPickComponent } from "./outside-prescription-pick/outside-prescription-pick.component";


@Injectable({
  providedIn: 'root'
})

export class OutsidePrescriptionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  outside_prescriptionsUrl(): string {
    return "/outside_prescriptions";
  }

  outsidePrescriptionUrl(id: string): string {
    return "/outside_prescriptions/" + id;
  }

  viewOutsidePrescriptions(): void {
    this.router.navigateByUrl(this.outside_prescriptionsUrl());
  }

  viewOutsidePrescription(id: string): void {
    this.router.navigateByUrl(this.outsidePrescriptionUrl(id));
  }

  editOutsidePrescription(id: string): MatDialogRef<OutsidePrescriptionEditComponent> {
    const dialogRef = this.dialog.open(OutsidePrescriptionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOutsidePrescription(): MatDialogRef<OutsidePrescriptionEditComponent> {
    const dialogRef = this.dialog.open(OutsidePrescriptionEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickOutsidePrescriptions(selectOne: boolean=false): MatDialogRef<OutsidePrescriptionPickComponent> {
      const dialogRef = this.dialog.open(OutsidePrescriptionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}