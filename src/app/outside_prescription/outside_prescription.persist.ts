import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {OutsidePrescriptionDashboard, OutsidePrescriptionDetail, OutsidePrescriptionSummaryPartialList} from "./outside_prescription.model";
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class OutsidePrescriptionPersist {
  
 outsidePrescriptionSearchText: string = "";
  tab_index: number;
  date: FormControl = new FormControl({disabled: true, value: new Date()});
 
 constructor(private http: HttpClient) {
  }
  
  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.outsidePrescriptionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchOutsidePrescription(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OutsidePrescriptionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("outside_prescription", this.outsidePrescriptionSearchText, pageSize, pageIndex, sort, order);
    if(this.date.value){      
      url = TCUtilsString.appendUrlParameter(url, "request_date", (new Date(this.date.value).getTime()/1000).toString())
    } 
    return this.http.get<OutsidePrescriptionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outside_prescription/do", new TCDoParam("download_all", this.filters()));
  }

  outsidePrescriptionDashboard(): Observable<OutsidePrescriptionDashboard> {
    return this.http.get<OutsidePrescriptionDashboard>(environment.tcApiBaseUri + "outside_prescription/dashboard");
  }

  getOutsidePrescription(id: string): Observable<OutsidePrescriptionDetail> {
    return this.http.get<OutsidePrescriptionDetail>(environment.tcApiBaseUri + "outside_prescription/" + id);
  }

  addOutsidePrescription(item: OutsidePrescriptionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + 'outside_prescription/', item);
  }

  updateOutsidePrescription(item: OutsidePrescriptionDetail): Observable<OutsidePrescriptionDetail> {
    return this.http.patch<OutsidePrescriptionDetail>(
      environment.tcApiBaseUri + 'outside_prescription/' + item.id,
      item
    );
  }

  deleteOutsidePrescription(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'outside_prescription/' + id);
  }

  outsidePrescriptionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'outside_prescription/do',
      new TCDoParam(method, payload)
    );
  }

  outsidePrescriptionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'outside_prescription/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'outside_prescription/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outside_prescription/" + id + "/do", new TCDoParam("print", {}));
  }

  
 }