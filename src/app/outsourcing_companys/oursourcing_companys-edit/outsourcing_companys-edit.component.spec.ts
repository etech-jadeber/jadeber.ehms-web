import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsourcingCompanyEditComponent } from './outsourcing_companys-edit.component';

describe('OutsourcingCompanyEditComponent', () => {
  let component: OutsourcingCompanyEditComponent;
  let fixture: ComponentFixture<OutsourcingCompanyEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsourcingCompanyEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsourcingCompanyEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
