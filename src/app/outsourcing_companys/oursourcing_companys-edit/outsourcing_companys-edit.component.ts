import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {OutsourcingCompanyDetail} from "../outsourcing_company.model";
import {OutsourcingCompanyPersist} from "../outsourcing_company.persist";


@Component({
  selector: 'app-OutsourcingCompany-edit',
  templateUrl: './outsourcing_companys-edit.component.html',
  styleUrls: ['./outsourcing_companys-edit.component.css']
})
export class OutsourcingCompanyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  OutsourcingCompanyDetail: OutsourcingCompanyDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<OutsourcingCompanyEditComponent>,
              public  persist: OutsourcingCompanyPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("outsourcing_companys");
      this.title = this.appTranslation.getText("general","new") +  " " + "Outsourcing Company";
      this.OutsourcingCompanyDetail = new OutsourcingCompanyDetail();
      this.OutsourcingCompanyDetail.duration = -1;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("outsourcing_companys");
      this.title = this.appTranslation.getText("general","edit") +  " " + "Outsourcing Company";
      this.isLoadingResults = true;
      this.persist.getOutsourcingCompany(this.idMode.id).subscribe(OutsourcingCompanyDetail => {
        this.OutsourcingCompanyDetail = OutsourcingCompanyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addOutsourcingCompany(this.OutsourcingCompanyDetail).subscribe(value => {
      this.tcNotification.success("OutsourcingCompany added");
      this.OutsourcingCompanyDetail.id = value.id;
      this.dialogRef.close(this.OutsourcingCompanyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateOutsourcingCompany(this.OutsourcingCompanyDetail).subscribe(value => {
      this.tcNotification.success("OutsourcingCompany updated");
      this.dialogRef.close(this.OutsourcingCompanyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  onContinue(): void {
    this.isLoadingResults = true;
    this.persist.addOutsourcingCompany(this.OutsourcingCompanyDetail).subscribe(value => {
    this.OutsourcingCompanyDetail.id = value.id
    this.isLoadingResults = false;
        this.dialogRef.close({...this.OutsourcingCompanyDetail, next: true});
    }, error => {
      this.isLoadingResults = false;
      console.log(error)
    })
  }

  canSubmit():boolean{
        if (this.OutsourcingCompanyDetail == null){
            return false;
          }

        if (this.OutsourcingCompanyDetail.name == null || this.OutsourcingCompanyDetail.name  == "") {
                      return false;
                    }

if (this.OutsourcingCompanyDetail.phone_no == null || this.OutsourcingCompanyDetail.phone_no  == "") {
                      return false;
                    }

if (this.OutsourcingCompanyDetail.address == null || this.OutsourcingCompanyDetail.address  == "") {
                      return false;
                    }

        return true;
      }


}
