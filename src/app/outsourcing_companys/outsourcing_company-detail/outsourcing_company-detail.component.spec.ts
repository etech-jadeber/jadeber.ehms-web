import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsourcingCompanyDetailComponent } from './outsourcing_company-detail.component';

describe('OutsourcingCompanyDetailComponent', () => {
  let component: OutsourcingCompanyDetailComponent;
  let fixture: ComponentFixture<OutsourcingCompanyDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsourcingCompanyDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsourcingCompanyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
