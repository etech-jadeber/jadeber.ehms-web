import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {OutsourcingCompanyDetail} from "../outsourcing_company.model";
import {OutsourcingCompanyPersist} from "../outsourcing_company.persist";
import {OutsourcingCompanyNavigator} from "../outsourcing_company.navigator";
import { MenuState } from 'src/app/global-state-manager/global-state';
import { tabs } from 'src/app/app.enums';

export enum OutsourcingCompanyTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-OutsourcingCompany-detail',
  templateUrl: './outsourcing_company-detail.component.html',
  styleUrls: ['./outsourcing_company-detail.component.css']
})
export class OutsourcingCompanyDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  OutsourcingCompanyLoading:boolean = false;
  
  OutsourcingCompanyTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  outsourcingcompanyDetail: OutsourcingCompanyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  InsuredEmployeeTotalCount: number = 0;
  CreditHistoryTotalCount: number = 0;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public OutsourcingCompanyNavigator: OutsourcingCompanyNavigator,
              public  OutsourcingCompanyPersist: OutsourcingCompanyPersist,
              public menuState: MenuState,) {
    this.tcAuthorization.requireRead("outsourcingcompanys");
    this.outsourcingcompanyDetail = new OutsourcingCompanyDetail();
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    });
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("outsourcingcompanys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
   }

  loadDetails(id: string): void {
  this.OutsourcingCompanyLoading = true;
    this.OutsourcingCompanyPersist.getOutsourcingCompany(id).subscribe(OutsourcingCompanyDetail => {
          this.outsourcingcompanyDetail = OutsourcingCompanyDetail;
          this.menuState.getDataResponse(this.outsourcingcompanyDetail);
          this.OutsourcingCompanyLoading = false;
        }, error => {
          console.error(error);
          this.OutsourcingCompanyLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.outsourcingcompanyDetail.id);
  }

  editOutsourcingCompany(): void {
    let modalRef = this.OutsourcingCompanyNavigator.editOutsourcingCompany(this.outsourcingcompanyDetail.id);
    modalRef.afterClosed().subscribe(modifiedOutsourcingCompanyDetail => {
      TCUtilsAngular.assign(this.outsourcingcompanyDetail, modifiedOutsourcingCompanyDetail);
    }, error => {
      console.error(error);
    });
  }

   printOutsourcingCompany():void{
      this.OutsourcingCompanyPersist.print(this.outsourcingcompanyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print OutsourcingCompany", true);
      });
    }

  back():void{
      this.location.back();
    }

    onInsuredEmployeesResult(count: number){
      this.InsuredEmployeeTotalCount = count
    }

    onCreditHistoryResult(count: number){
      this.CreditHistoryTotalCount = count
    }

}
