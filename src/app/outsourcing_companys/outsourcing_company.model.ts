import {TCId} from "../tc/models";

export class OutsourcingCompanySummary extends TCId {
  name : string;
tin_no : string;
max_payment : number;
phone_no : string;
address : string;
duration : number;
percentage : number;
insurance_id : string;
status : number
}

export class OutsourcingCompanySummaryPartialList {
  data: OutsourcingCompanySummary[];
  total: number;
}

export class OutsourcingCompanyDetail extends OutsourcingCompanySummary {
  name : string;
tin_no : string;
max_payment : number;
phone_no : string;
address : string;
duration : number;
percentage : number;
insurance_id : string;
}

export class OutsourcingCompanyDashboard {
  total: number;
}
