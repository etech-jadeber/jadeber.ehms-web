import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {OutsourcingCompanyEditComponent} from "./oursourcing_companys-edit/outsourcing_companys-edit.component";
import {OutsourcingCompanyPickComponent} from "./outsourcing_companys-pick/outsourcing_company-pick.component";


@Injectable({
  providedIn: 'root'
})

export class OutsourcingCompanyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  OutsourcingCompanysUrl(): string {
    return "/outsourcing_companys";
  }

  OutsourcingCompanyUrl(id: string): string {
    return "/outsourcing_companys/" + id;
  }

  viewOutsourcingCompanys(): void {
    this.router.navigateByUrl(this.OutsourcingCompanysUrl());
  }

  viewOutsourcingCompany(id: string): void {
    this.router.navigateByUrl(this.OutsourcingCompanyUrl(id));
  }

  editOutsourcingCompany(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OutsourcingCompanyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addOutsourcingCompany(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(OutsourcingCompanyEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickOutsourcingCompanys(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(OutsourcingCompanyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
