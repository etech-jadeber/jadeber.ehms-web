import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation, TCEnum} from "../tc/models";
import {OutsourcingCompanyDashboard, OutsourcingCompanyDetail, OutsourcingCompanySummaryPartialList} from "./outsourcing_company.model";
import { ActivenessStatus } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class OutsourcingCompanyPersist {

  OutsourcingCompanySearchText: string = "";

  outsourcingcompany_insurance_payment_typeId: number ;

  outsourcingcompanyStatusFilter: number  = -1;

    Activeness_Status: TCEnum[] = [
     new TCEnum( ActivenessStatus.active, 'active'),
  new TCEnum( ActivenessStatus.freeze, 'freeze'),

  ];

  constructor(
      private http: HttpClient,
      private appTranslation: AppTranslation,
      ) {
  }

  searchOutsourcingCompany(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<OutsourcingCompanySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("outsourcing_companys", this.OutsourcingCompanySearchText, pageSize, pageIndex, sort, order);
    if (this.outsourcingcompanyStatusFilter != -1){
      url = TCUtilsString.appendUrlParameter(url, "status", this.outsourcingcompanyStatusFilter.toString())
    }
    return this.http.get<OutsourcingCompanySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.OutsourcingCompanySearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outsourcing_companys/do", new TCDoParam("download_all", this.filters()));
  }

  OutsourcingCompanyDashboard(): Observable<OutsourcingCompanyDashboard> {
    return this.http.get<OutsourcingCompanyDashboard>(environment.tcApiBaseUri + "outsourcing_companys/dashboard");
  }

  getOutsourcingCompany(id: string): Observable<OutsourcingCompanyDetail> {
    return this.http.get<OutsourcingCompanyDetail>(environment.tcApiBaseUri + "outsourcing_companys/" + id);
  }

  addOutsourcingCompany(item: OutsourcingCompanyDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "outsourcing_companys/", item);
  }

  updateOutsourcingCompany(item: OutsourcingCompanyDetail): Observable<OutsourcingCompanyDetail> {
    return this.http.patch<OutsourcingCompanyDetail>(environment.tcApiBaseUri + "outsourcing_companys/" + item.id, item);
  }

  deleteOutsourcingCompany(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "outsourcing_companys/" + id);
  }

  OutsourcingCompanysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "outsourcing_companys/do", new TCDoParam(method, payload));
  }

  OutsourcingCompanyDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "outsourcing_companys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "outsourcing_companys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "outsourcing_companys/" + id + "/do", new TCDoParam("print", {}));
  }


}
