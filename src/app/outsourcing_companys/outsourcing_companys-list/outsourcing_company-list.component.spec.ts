import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsourcingCompanyListComponent } from './outsourcing_company-list.component';

describe('OutsourcingCompanyListComponent', () => {
  let component: OutsourcingCompanyListComponent;
  let fixture: ComponentFixture<OutsourcingCompanyListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsourcingCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsourcingCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
