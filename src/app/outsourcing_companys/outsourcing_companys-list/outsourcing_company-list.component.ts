import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {OutsourcingCompanyPersist} from "../outsourcing_company.persist";
import {OutsourcingCompanyNavigator} from "../outsourcing_company.navigator";
import {OutsourcingCompanyDetail, OutsourcingCompanySummary, OutsourcingCompanySummaryPartialList} from "../outsourcing_company.model";
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { ActivenessStatus } from 'src/app/app.enums';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-OutsourcingCompany-list',
  templateUrl: './outsourcing_company-list.component.html',
  styleUrls: ['./outsourcing_company-list.component.css']
})
export class OutsourcingCompanyListComponent implements OnInit {

  OutsourcingCompanysData: OutsourcingCompanySummary[] = [];
  OutsourcingCompanysTotalCount: number = 0;
  OutsourcingCompanysSelectAll:boolean = false;
  OutsourcingCompanysSelection: OutsourcingCompanySummary[] = [];

  OutsourcingCompanysDisplayedColumns: string[] = ["select","action", "name","phone_no","address", "status"];
  OutsourcingCompanysSearchTextBox: FormControl = new FormControl();
  OutsourcingCompanysIsLoading: boolean = false;

  outsourcingcompany_status = ActivenessStatus

  @ViewChild(MatPaginator, {static: true}) OutsourcingCompanysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) OutsourcingCompanysSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public OutsourcingCompanyPersist: OutsourcingCompanyPersist,
                public OutsourcingCompanyNavigator: OutsourcingCompanyNavigator,
                public jobPersist: JobPersist,
                public patientNavigator: PatientNavigator,
    ) {

        this.tcAuthorization.requireRead("outsourcing_companys");
       this.OutsourcingCompanysSearchTextBox.setValue(OutsourcingCompanyPersist.OutsourcingCompanySearchText);
      //delay subsequent keyup events
      this.OutsourcingCompanysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.OutsourcingCompanyPersist.OutsourcingCompanySearchText = value;
        this.searchOutsourcingCompanys();
      });
    }

    ngOnInit() {
      this.OutsourcingCompanysSort.sortChange.subscribe(() => {
        this.OutsourcingCompanysPaginator.pageIndex = 0;
        this.searchOutsourcingCompanys(true);
      });

      this.OutsourcingCompanysPaginator.page.subscribe(() => {
        this.searchOutsourcingCompanys(true);
      });
      //start by loading items
      this.searchOutsourcingCompanys();
    }

  searchOutsourcingCompanys(isPagination:boolean = false): void {


    let paginator = this.OutsourcingCompanysPaginator;
    let sorter = this.OutsourcingCompanysSort;

    this.OutsourcingCompanysIsLoading = true;
    this.OutsourcingCompanysSelection = [];

    this.OutsourcingCompanyPersist.searchOutsourcingCompany(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: OutsourcingCompanySummaryPartialList) => {
      this.OutsourcingCompanysData = partialList.data;
      if (partialList.total != -1) {
        this.OutsourcingCompanysTotalCount = partialList.total;
      }
      this.OutsourcingCompanysIsLoading = false;
    }, error => {
      this.OutsourcingCompanysIsLoading = false;
    });

  }

  downloadOutsourcingCompanys(): void {
    if(this.OutsourcingCompanysSelectAll){
         this.OutsourcingCompanyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download OutsourcingCompanys", true);
      });
    }
    else{
        this.OutsourcingCompanyPersist.download(this.tcUtilsArray.idsList(this.OutsourcingCompanysSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download OutsourcingCompanys",true);
            });
        }
  }


  activeOutsourcingCompany(outsourcingcompany: OutsourcingCompanyDetail): void {
    this.OutsourcingCompanyPersist.OutsourcingCompanyDo(outsourcingcompany.id, "active", {}).subscribe(result => {
      outsourcingcompany.status = this.outsourcingcompany_status.active
    })
  }

  freezeOutsourcingCompany(outsourcingcompany: OutsourcingCompanyDetail): void {
    this.OutsourcingCompanyPersist.OutsourcingCompanyDo(outsourcingcompany.id, "freeze", {}).subscribe(result => {
      outsourcingcompany.status = this.outsourcingcompany_status.freeze
    })
  }


  addOutsourcingCompany(): void {
    let dialogRef = this.OutsourcingCompanyNavigator.addOutsourcingCompany();
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.next) {
        this.searchOutsourcingCompanys()
      }
      else if (result){
        this.searchOutsourcingCompanys();
      }
    });
  }

  editOutsourcingCompany(item: OutsourcingCompanySummary) {
    let dialogRef = this.OutsourcingCompanyNavigator.editOutsourcingCompany(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }


  deleteOutsourcingCompany(item: OutsourcingCompanySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("OutsourcingCompany");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.OutsourcingCompanyPersist.deleteOutsourcingCompany(item.id).subscribe(response => {
          this.tcNotification.success("OutsourcingCompany deleted");
          this.searchOutsourcingCompanys();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
