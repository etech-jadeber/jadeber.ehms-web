import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OutsourcingCompanyPickComponent } from './outsourcing_company-pick.component';

describe('OutsourcingCompanyPickComponent', () => {
  let component: OutsourcingCompanyPickComponent;
  let fixture: ComponentFixture<OutsourcingCompanyPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsourcingCompanyPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsourcingCompanyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
