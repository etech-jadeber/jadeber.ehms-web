import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {OutsourcingCompanyDetail, OutsourcingCompanySummary, OutsourcingCompanySummaryPartialList} from "../outsourcing_company.model";
import {OutsourcingCompanyPersist} from "../outsourcing_company.persist";


@Component({
  selector: 'app-OutsourcingCompany-pick',
  templateUrl: './outsourcing_company-pick.component.html',
  styleUrls: ['./outsourcing_company-pick.component.css']
})
export class OutsourcingCompanyPickComponent implements OnInit {

  OutsourcingCompanysData: OutsourcingCompanySummary[] = [];
  OutsourcingCompanysTotalCount: number = 0;
  OutsourcingCompanysSelection: OutsourcingCompanySummary[] = [];
  OutsourcingCompanysDisplayedColumns: string[] = ["select", 'name','tin_no','max_payment','phone_no','address','duration','percentage','insurance_id' ];

  OutsourcingCompanysSearchTextBox: FormControl = new FormControl();
  OutsourcingCompanysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) OutsourcingCompanysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) OutsourcingCompanysSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public OutsourcingCompanyPersist: OutsourcingCompanyPersist,
              public dialogRef: MatDialogRef<OutsourcingCompanyPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("outsourcingcompanys");
    this.OutsourcingCompanysSearchTextBox.setValue(OutsourcingCompanyPersist.OutsourcingCompanySearchText);
    //delay subsequent keyup events
    this.OutsourcingCompanysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.OutsourcingCompanyPersist.OutsourcingCompanySearchText = value;
      this.searchOutsourcingCompanys();
    });
  }

  ngOnInit() {

    this.OutsourcingCompanysSort.sortChange.subscribe(() => {
      this.OutsourcingCompanysPaginator.pageIndex = 0;
      this.searchOutsourcingCompanys();
    });

    this.OutsourcingCompanysPaginator.page.subscribe(() => {
      this.searchOutsourcingCompanys();
    });

    //set initial picker list to 5
    this.OutsourcingCompanysPaginator.pageSize = 5;

    //start by loading items
    this.searchOutsourcingCompanys();
  }

  searchOutsourcingCompanys(): void {
    this.OutsourcingCompanysIsLoading = true;
    this.OutsourcingCompanysSelection = [];

    this.OutsourcingCompanyPersist.searchOutsourcingCompany(this.OutsourcingCompanysPaginator.pageSize,
        this.OutsourcingCompanysPaginator.pageIndex,
        this.OutsourcingCompanysSort.active,
        this.OutsourcingCompanysSort.direction).subscribe((partialList: OutsourcingCompanySummaryPartialList) => {
      this.OutsourcingCompanysData = partialList.data;
      if (partialList.total != -1) {
        this.OutsourcingCompanysTotalCount = partialList.total;
      }
      this.OutsourcingCompanysIsLoading = false;
    }, error => {
      this.OutsourcingCompanysIsLoading = false;
    });

  }

  markOneItem(item: OutsourcingCompanySummary) {
    if(!this.tcUtilsArray.containsId(this.OutsourcingCompanysSelection,item.id)){
          this.OutsourcingCompanysSelection = [];
          this.OutsourcingCompanysSelection.push(item);
        }
        else{
          this.OutsourcingCompanysSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.OutsourcingCompanysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
