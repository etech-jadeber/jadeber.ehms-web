import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidOperationComponent } from './invalid-operation.component';

describe('InvalidOperationComponent', () => {
  let component: InvalidOperationComponent;
  let fixture: ComponentFixture<InvalidOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvalidOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
