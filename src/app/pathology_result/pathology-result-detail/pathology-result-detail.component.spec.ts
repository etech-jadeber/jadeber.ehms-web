import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathologyResultDetailComponent } from './pathology-result-detail.component';

describe('PathologyResultDetailComponent', () => {
  let component: PathologyResultDetailComponent;
  let fixture: ComponentFixture<PathologyResultDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PathologyResultDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PathologyResultDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
