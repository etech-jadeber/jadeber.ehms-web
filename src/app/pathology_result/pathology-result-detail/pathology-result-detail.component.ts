import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PathologyResultDetail} from "../pathology_result.model";
import {PathologyResultPersist} from "../pathology_result.persist";
import {PathologyResultNavigator} from "../pathology_result.navigator";

export enum PathologyResultTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-pathology_result-detail',
  templateUrl: './pathology-result-detail.component.html',
  styleUrls: ['./pathology-result-detail.component.css']
})
export class PathologyResultDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  pathologyResultIsLoading:boolean = false;
  
  PathologyResultTabs: typeof PathologyResultTabs = PathologyResultTabs;
  activeTab: PathologyResultTabs = PathologyResultTabs.overview;
  //basics
  pathologyResultDetail: PathologyResultDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public pathologyResultNavigator: PathologyResultNavigator,
              public  pathologyResultPersist: PathologyResultPersist) {
    this.tcAuthorization.requireRead("pathology_results");
    this.pathologyResultDetail = new PathologyResultDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("pathology_results");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.pathologyResultIsLoading = true;
    this.pathologyResultPersist.getPathologyResult(id).subscribe(pathologyResultDetail => {
          this.pathologyResultDetail = pathologyResultDetail;
          this.pathologyResultIsLoading = false;
        }, error => {
          console.error(error);
          this.pathologyResultIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.pathologyResultDetail.id);
  }
  editPathologyResult(): void {
    let modalRef = this.pathologyResultNavigator.editPathologyResult(this.pathologyResultDetail);
    modalRef.afterClosed().subscribe(pathologyResultDetail => {
      TCUtilsAngular.assign(this.pathologyResultDetail, pathologyResultDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.pathologyResultPersist.print(this.pathologyResultDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print pathology_result", true);
      });
    }

  back():void{
      this.location.back();
    }

}