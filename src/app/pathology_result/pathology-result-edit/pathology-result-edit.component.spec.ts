import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathologyResultEditComponent } from './pathology-result-edit.component';

describe('PathologyResultEditComponent', () => {
  let component: PathologyResultEditComponent;
  let fixture: ComponentFixture<PathologyResultEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PathologyResultEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PathologyResultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
