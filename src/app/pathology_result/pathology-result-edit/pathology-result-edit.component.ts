import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCIdMode, TCModalModes } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PathologyResultDetail } from '../pathology_result.model'; import { PathologyResultPersist } from '../pathology_result.persist';import { selected_orders_status } from 'src/app/app.enums';
import { Selected_OrdersSummary } from 'src/app/form_encounters/orderss/orders.model';
 @Component({
  selector: 'app-pathology_result-edit',
  templateUrl: './pathology-result-edit.component.html',
  styleUrls: ['./pathology-result-edit.component.css']
}) export class PathologyResultEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  showResult: boolean = false;
  pathologyResultDetail: PathologyResultDetail | Selected_OrdersSummary; 
  
  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PathologyResultEditComponent>,
    public persist: PathologyResultPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: {result: PathologyResultDetail, encounter_id: string, mode: string}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  } 
  
  ngOnInit() {
    if(this.idMode.mode == TCModalModes.WIZARD){
      this.title = "Pathology Result";
      // this.pathologyResultDetail = new Selected_OrdersSummary();
      this.pathologyResultDetail = this.idMode.result as Selected_OrdersSummary
      this.showResult = true;
    }

    else if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("pathology_results");
      this.title = this.appTranslation.getText("general", "new") + " " + "pathology_result";
      this.pathologyResultDetail = new Selected_OrdersSummary();
      this.pathologyResultDetail = this.idMode.result
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("pathology_results");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "pathology_result";
      this.pathologyResultDetail = this.idMode.result;
      // this.isLoadingResults = true;
      // this.persist.getPathologyResult(this.idMode.id as string).subscribe(pathologyResultDetail => {
      //   this.pathologyResultDetail = pathologyResultDetail;
      //   if ([selected_orders_status.approved, selected_orders_status.ready_to_approve].some(res => res == this.pathologyResultDetail.status)){
      //     this.showResult = true;
      //   }
      //   this.isLoadingResults = false;
      // }, error => {
      //   console.log(error);
      //   this.isLoadingResults = false;
      // })

    }
  } onAdd(): void {
    // this.isLoadingResults = true;

    // this.persist.addPathologyResult(this.idMode.id, this.pathologyResultDetail).subscribe(value => {
    //   this.tcNotification.success("pathologyResult added");
    //   this.pathologyResultDetail.id = value.id;
      this.dialogRef.close(this.pathologyResultDetail);
    // }, error => {
    //   this.isLoadingResults = false;
    // })
  }

  onUpdate(): void {
    // this.isLoadingResults = true;


    // this.persist.updatePathologyResult(this.pathologyResultDetail).subscribe(value => {
    //   this.tcNotification.success("pathology_result updated");
      this.dialogRef.close(this.pathologyResultDetail);
    // }, error => {
    //   this.isLoadingResults = false;
    // })

  } canSubmit(): boolean {
    if (this.pathologyResultDetail == null) {
      return false;
    }

    if (this.pathologyResultDetail.clinical_finding == null || this.pathologyResultDetail.clinical_finding == "") {
      return false;
    }
    if (this.pathologyResultDetail.gross_examination == null || this.pathologyResultDetail.gross_examination == "") {
      return false;
    }
    if (this.pathologyResultDetail.microscopic_evaluation == null || this.pathologyResultDetail.microscopic_evaluation == "") {
      return false;
    }
    if (this.pathologyResultDetail.conclusion == null || this.pathologyResultDetail.conclusion == "") {
      return false;
    }
    if (this.pathologyResultDetail.comment == null || this.pathologyResultDetail.comment == "") {
      return false;
    }
    return true;

  }
}