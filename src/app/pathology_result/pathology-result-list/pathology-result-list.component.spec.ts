import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathologyResultListComponent } from './pathology-result-list.component';

describe('PathologyResultListComponent', () => {
  let component: PathologyResultListComponent;
  let fixture: ComponentFixture<PathologyResultListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PathologyResultListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PathologyResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
