import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PathologyResultDetail, PathologyResultSummary, PathologyResultSummaryPartialList } from '../pathology_result.model';
import { PathologyResultPersist } from '../pathology_result.persist';
import { PathologyResultNavigator } from '../pathology_result.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Selected_OrdersSummary } from 'src/app/form_encounters/orderss/orders.model';
@Component({
  selector: 'app-pathology_result-list',
  templateUrl: './pathology-result-list.component.html',
  styleUrls: ['./pathology-result-list.component.css']
})export class PathologyResultListComponent implements OnInit {
  pathologyResultsData: PathologyResultSummary[] = [];
  pathologyResultsTotalCount: number = 0;
  pathologyResultSelectAll:boolean = false;
  pathologyResultSelection: PathologyResultSummary[] = [];

 pathologyResultsDisplayedColumns: string[] = ["select","action","link" ,"clinical_finding","gross_examination","microscopic_evaluation","conclusion","comment","selected_order_id","result_file_id","result_date","status","result_by","confirm_by","reject_description" ];
  pathologyResultSearchTextBox: FormControl = new FormControl();
  pathologyResultIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) pathologyResultsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) pathologyResultsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public pathologyResultPersist: PathologyResultPersist,
                public pathologyResultNavigator: PathologyResultNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString

    ) {

        this.tcAuthorization.requireRead("pathology_results");
       this.pathologyResultSearchTextBox.setValue(pathologyResultPersist.pathologyResultSearchText);
      //delay subsequent keyup events
      this.pathologyResultSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.pathologyResultPersist.pathologyResultSearchText = value;
        this.searchPathologyResults();
      });
    } ngOnInit() {
   
      this.pathologyResultsSort.sortChange.subscribe(() => {
        this.pathologyResultsPaginator.pageIndex = 0;
        this.searchPathologyResults(true);
      });

      this.pathologyResultsPaginator.page.subscribe(() => {
        this.searchPathologyResults(true);
      });
      //start by loading items
      this.searchPathologyResults();
    }

  searchPathologyResults(isPagination:boolean = false): void {


    let paginator = this.pathologyResultsPaginator;
    let sorter = this.pathologyResultsSort;

    this.pathologyResultIsLoading = true;
    this.pathologyResultSelection = [];

    this.pathologyResultPersist.searchPathologyResult(this.tcUtilsString.invalid_id, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PathologyResultSummaryPartialList) => {
      this.pathologyResultsData = partialList.data;
      if (partialList.total != -1) {
        this.pathologyResultsTotalCount = partialList.total;
      }
      this.pathologyResultIsLoading = false;
    }, error => {
      this.pathologyResultIsLoading = false;
    });

  } downloadPathologyResults(): void {
    if(this.pathologyResultSelectAll){
         this.pathologyResultPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download pathology_result", true);
      });
    }
    else{
        this.pathologyResultPersist.download(this.tcUtilsArray.idsList(this.pathologyResultSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download pathology_result",true);
            });
        }
  }
addPathologyResult(): void {
    let dialogRef = this.pathologyResultNavigator.addPathologyResult(new Selected_OrdersSummary(), "");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPathologyResults();
      }
    });
  }

  editPathologyResult(item: PathologyResultSummary) {
    let dialogRef = this.pathologyResultNavigator.editPathologyResult(item);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePathologyResult(item: PathologyResultSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("pathology_result");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.pathologyResultPersist.deletePathologyResult(item.id).subscribe(response => {
          this.tcNotification.success("pathology_result deleted");
          this.searchPathologyResults();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}