import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PathologyResultPickComponent } from './pathology-result-pick.component';

describe('PathologyResultPickComponent', () => {
  let component: PathologyResultPickComponent;
  let fixture: ComponentFixture<PathologyResultPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PathologyResultPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PathologyResultPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
