import { TCId } from "../tc/models"; export class PathologyResultSummary extends TCId {
    clinical_finding: string;
    gross_examination: string;
    microscopic_evaluation: string;
    conclusion: string;
    comment: string;
    selected_order_id: string;
    lab_test_id: string;
    result_file_id: string;
    result_date: number;
    status: number;
    result_by: string;
    confirm_by: string;
    reject_description: string;
    key: string;
    test_unit_id: string;
    company_id: string;
    lab_order_id: string;

}
export class PathologyResultSummaryPartialList {
    data: PathologyResultSummary[];
    total: number;
}
export class PathologyResultDetail extends PathologyResultSummary {
  result_id: string;
}

export class PathologyResultDashboard {
    total: number;
}