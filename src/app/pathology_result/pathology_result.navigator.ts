import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from "../tc/utils-angular";
import { TCIdMode, TCModalModes } from "../tc/models";

import { PathologyResultEditComponent } from "./pathology-result-edit/pathology-result-edit.component";
import { PathologyResultPickComponent } from "./pathology-result-pick/pathology-result-pick.component";
import { Selected_OrdersSummary } from "../form_encounters/orderss/orders.model";
import { PathologyResultDetail, PathologyResultSummary } from "./pathology_result.model";


@Injectable({
    providedIn: 'root'
})

export class PathologyResultNavigator {

    constructor(private router: Router,
        public dialog: MatDialog) {
    }
    pathologyResultsUrl(): string {
        return "/pathology_results";
    }

    pathologyResultUrl(id: string): string {
        return "/pathology_results/" + id;
    }

    viewPathologyResults(): void {
        this.router.navigateByUrl(this.pathologyResultsUrl());
    }

    viewPathologyResult(id: string): void {
        this.router.navigateByUrl(this.pathologyResultUrl(id));
    }

      editPathologyResult(result: PathologyResultSummary): MatDialogRef<PathologyResultEditComponent> {
        const dialogRef = this.dialog.open(PathologyResultEditComponent, {
          data: {result, mode: TCModalModes.EDIT},
          disableClose: true
        });
        return dialogRef;
      }
    
      addPathologyResult(result: PathologyResultDetail, encounter_id: string): MatDialogRef<PathologyResultEditComponent> {
        const dialogRef = this.dialog.open(PathologyResultEditComponent, {
              data: {result, encounter_id, mode: TCModalModes.NEW},
              width: TCModalWidths.medium,
              disableClose: true
        });
        return dialogRef;
      }

    showPathologyResult(parent: PathologyResultDetail): MatDialogRef<PathologyResultEditComponent> {
        const dialogRef = this.dialog.open(PathologyResultEditComponent, {
            // data: new TCIdMode(parnetId, TCModalModes.NEW),
            data: {result: parent, mode: TCModalModes.WIZARD},
            width: TCModalWidths.medium
        });
        return dialogRef;
    }

    pickPathologyResults(selectOne: boolean = false): MatDialogRef<PathologyResultPickComponent> {
        const dialogRef = this.dialog.open(PathologyResultPickComponent, {
            data: selectOne,
            width: TCModalWidths.large
        });
        return dialogRef;
    }
}
