import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";


import { TCDoParam, TCUrlParams, TCUtilsHttp } from "../tc/utils-http";
import { TCId, TcDictionary } from "../tc/models";
import { PathologyResultDashboard, PathologyResultDetail, PathologyResultSummaryPartialList } from "./pathology_result.model";


@Injectable({
    providedIn: 'root'
})
export class PathologyResultPersist {
    pathologyResultSearchText: string = ""; constructor(private http: HttpClient) {
    }
    filters(): any {
        let fltrs: TcDictionary<string> = new TcDictionary<string>();
        fltrs[TCUrlParams.searchText] = this.pathologyResultSearchText;
        //add custom filters
        return fltrs;
    }

    searchPathologyResult = (parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PathologyResultSummaryPartialList> => {

        let url = TCUtilsHttp.buildSearchUrl("selected_orders/" + parentId + "/pathology_result", this.pathologyResultSearchText, pageSize, pageIndex, sort, order);
        return this.http.get<PathologyResultSummaryPartialList>(url);

    }
     downloadAll(): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "pathology_result/do", new TCDoParam("download_all", this.filters()));
    }

    pathologyResultDashboard(): Observable<PathologyResultDashboard> {
        return this.http.get<PathologyResultDashboard>(environment.tcApiBaseUri + "pathology_result/dashboard");
    }

    getPathologyResult(id: string): Observable<PathologyResultDetail> {
        return this.http.get<PathologyResultDetail>(environment.tcApiBaseUri + "pathology_result/" + id);
    }
    addPathologyResult(parentId: string, item: PathologyResultDetail): Observable<TCId> {
        if (item.id != null){
            return this.updatePathologyResult(item)
        }
        return this.http.post<TCId>(environment.tcApiBaseUri + "selected_orders/" + parentId + "/pathology_result", item);
    }

    updatePathologyResult(item: PathologyResultDetail): Observable<PathologyResultDetail> {
        return this.http.patch<PathologyResultDetail>(environment.tcApiBaseUri + "pathology_result/" + item.id, item);
    }

    deletePathologyResult(id: string): Observable<{}> {
        return this.http.delete(environment.tcApiBaseUri + "pathology_result/" + id);
    }
    pathologyResultsDo(method: string, payload: any): Observable<{}> {
        return this.http.post<{}>(environment.tcApiBaseUri + "pathology_result/do", new TCDoParam(method, payload));
    }

    pathologyResultDo(id: string, method: string, payload: any): Observable<{}> {
        return this.http.post<{}>(environment.tcApiBaseUri + "pathology_result/" + id + "/do", new TCDoParam(method, payload));
    }

    download(ids: string[]): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "pathology_result/do", new TCDoParam("download", ids));
    }

    print(id: string): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "pathology_result/" + id + "/do", new TCDoParam("print", {}));
    }


}