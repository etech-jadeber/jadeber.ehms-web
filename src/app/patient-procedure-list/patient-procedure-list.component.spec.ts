import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientProcedureListComponent } from './patient-procedure-list.component';

describe('PatientProcedureListComponent', () => {
  let component: PatientProcedureListComponent;
  let fixture: ComponentFixture<PatientProcedureListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientProcedureListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientProcedureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
