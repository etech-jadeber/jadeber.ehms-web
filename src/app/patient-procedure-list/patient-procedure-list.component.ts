import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCUtilsAngular } from '../tc/utils-angular';
import { Patient_ProcedureDetail, Patient_ProcedureSummary } from '../form_encounters/form_encounter.model';
import { ProcedurePersist } from '../procedures/procedure.persist';
import { Router } from '@angular/router';
import { Consent_FormNavigator } from '../consent_forms/consent_form.navigator';
import { Consent_FormPersist } from '../consent_forms/consent_form.persist';
import { Consent_FormSummary } from '../consent_forms/consent_form.model';
import { PaymentPersist } from '../payments/payment.persist';
import { payment_status, procedure_for } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { ProcedureNavigator } from '../procedures/procedure.navigator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { TCId } from '../tc/models';
import { ProcedureDetail } from '../procedures/procedure.model';
@Component({
  selector: 'app-patient-procedure-list',
  templateUrl: './patient-procedure-list.component.html',
  styleUrls: ['./patient-procedure-list.component.css']
})
export class PatientProcedureListComponent implements OnInit {
//patient_procedures
patient_proceduresData: Patient_ProcedureSummary[] = [];
patient_proceduresTotalCount: number = 0;
patient_proceduresSelectAll:boolean = false;
patient_proceduresSelected: Patient_ProcedureSummary[] = [];
patient_proceduresDisplayedColumns: string[] = ['select','action', 'pid', 'patient_name', 'name', 'quantity', 'created_at','procedure_status'];
patient_proceduresSearchTextBox: FormControl = new FormControl();
patient_proceduresLoading:boolean = false;
consentForm: Consent_FormSummary;
start_date: FormControl = new FormControl({disabled: true, value: new Date()})
procedures: {[id: string]: ProcedureDetail} = {}



@Input() encounterId: string=TCUtilsString.getInvalidId();
@Input() patient_id: string;
@Input() is_dialog: boolean = false;
@Input() procedure_for: number = procedure_for.diagnostic;
@Output() onResult = new EventEmitter<number>( );

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public procedurePersist: ProcedurePersist,
    private router: Router,
    public consetNav: Consent_FormNavigator,
    public consentFormPersist: Consent_FormPersist,
    public paymentPersist: PaymentPersist,
    public procedureNavigator: ProcedureNavigator,
    public tcUtilsString: TCUtilsString,
  ) {

    //patient_procedures filter
    this.patient_proceduresSearchTextBox.setValue(
      form_encounterPersist.patientProcedureEncounterSearchHistory.search_text
    );
    this.patient_proceduresSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.patientProcedureEncounterSearchHistory.search_text = value.trim();
        this.searchPatient_Procedures();
      });
      this.form_encounterPersist.patientProcedureEncounterSearchHistory.start_date = this.tcUtilsDate.toTimeStamp(new Date());
      this.start_date.valueChanges.subscribe(value => {
        this.form_encounterPersist.patientProcedureEncounterSearchHistory.start_date = value ? this.tcUtilsDate.toTimeStamp(value._d) : null;
        this.searchPatient_Procedures();
      });
   }

   handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.form_encounterPersist.patientProcedureEncounterSearchHistory.encounter_id = this.encounterId;
    } else {
      this.form_encounterPersist.patientProcedureEncounterSearchHistory.encounter_id = null;
    }
    this.searchPatient_Procedures()
  }

  ngOnChanges() {
    // create header using child_id
    this.form_encounterPersist.patientProcedureEncounterSearchHistory.encounter_id = this.encounterId
    this.searchPatient_Procedures(true);
  }

  getPatientStatus(patient_procedure: Patient_ProcedureDetail){
    if (patient_procedure.payment_status != payment_status.paid ){
      return 'red'
    }else {
      return ''
    }
  }
  
  ngOnInit(): void {
    this.form_encounterPersist.patientProcedureEncounterSearchHistory.encounter_id = this.encounterId
    this.form_encounterPersist.patientProcedureEncounterSearchHistory.procedure_for = this.procedure_for
    this.form_encounterPersist.patientProcedureEncounterSearchHistory.patient_id = this.patient_id
    if ( (!this.encounterId || this.encounterId != this.tcUtilsString.invalid_id )){
      this.start_date.setValue('')
      this.form_encounterPersist.patientProcedureEncounterSearchHistory.start_date = null;
    }
      this.sorter.active = 'created_at';
      this.sorter.direction = 'desc';
        // patient_procedures sorter
        this.tcAuthorization.requireRead('patient_procedures')
        this.sorter.sortChange.subscribe(() => {
          this.paginator.pageIndex = 0;
          this.searchPatient_Procedures(true);
        });
      // patient_procedures paginator
      this.paginator.page.subscribe(() => {
          this.searchPatient_Procedures(true);
        });
        this.searchPatient_Procedures(true);
  }

  loadAdditional(data: Patient_ProcedureSummary[]) {
    data.forEach((d) => {
      if (!this.procedures[d.procedure_id]){
        this.procedures[d.procedure_id] = new ProcedureDetail()
        this.procedurePersist.getProcedure(d.procedure_id).subscribe(
          procedure => this.procedures[d.procedure_id] = procedure
        )
      }
      this.paymentPersist.getPayment(d.payment_id).subscribe((payment) => {
        d.payment_status = payment.payment_status
      })
    })
  }

    //patient_procedures methods
    searchPatient_Procedures(isPagination: boolean = false): void {
      let paginator =
        this.paginator;
      let sorter = this.sorter;
      this.patient_proceduresSelected = [];
      this.patient_proceduresLoading = true;
      this.form_encounterPersist
        .searchPatient_Procedure(
          paginator.pageSize,
          isPagination ? paginator.pageIndex : 0,
          sorter.active,
          sorter.direction
        )
        .subscribe(
          (response) => {
            this.patient_proceduresData = response.data;
  
            if (response.total != -1) {
              this.patient_proceduresTotalCount = response.total;
              this.loadAdditional(this.patient_proceduresData)
              // this.patient_proceduresData.forEach(value =>{
              //   !value.consent_id ? this.procedurePersist.getProcedure(value.procedure_id).subscribe((procedure)=>{
              //     value.needs_consent = procedure.needs_consent_form;
              //     value.procedure_name = procedure.name
              //   }): this.consentFormPersist.getConsent_Form(value.consent_id).subscribe((consent)=>{
              //     value.consent_form = consent
              //   })
              // })
            }
            this.patient_proceduresLoading = false;
          },
          (error) => {
            this.patient_proceduresLoading = false;
          }
        );
    }

    addPatient_Procedure(): void {
      let dialogRef = this.form_encounterNavigator.addPatient_Procedure(
        this.encounterId
      );
      dialogRef.afterClosed().subscribe((newPatient_Procedure) => {
        this.searchPatient_Procedures();
      });
    }
  
    editPatient_Procedure(item: Patient_ProcedureDetail): void {
      let dialogRef = this.form_encounterNavigator.editPatient_Procedure(
        this.encounterId,
        item.id
      );
      dialogRef.afterClosed().subscribe((updatedPatient_Procedure) => {
        if (updatedPatient_Procedure) {
          this.searchPatient_Procedures();
        }
      });
    }
    paymentCheck(element: Patient_ProcedureDetail): boolean {
      element.payment_id ? this.paymentPersist.getPayment(element.payment_id).subscribe((result)=> {
        if(result){
          if(result.payment_status == payment_status.paid || result.payment_status == payment_status.covered){
              return true;
          }
          else {
            this.tcNotification.error("Payment for this procedure is not paid")
            return false;
          }
        }
      }): this.tcNotification.error("payment id is not added"); return false;
    }
    deletePatient_Procedure(item: Patient_ProcedureDetail): void {
      let dialogRef = this.tcNavigator.confirmDeletion('Patient_Procedure');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist
            .deletePatient_Procedure(item.id)
            .subscribe(
              (response) => {
                this.tcNotification.success('patient_procedure deleted');
                this.searchPatient_Procedures();
              },
              (error) => {}
            );
        }
      });
    }
  
    downloadPatient_Procedures(): void {
      this.tcNotification.info(
        'Download patient_procedures : ' + this.patient_proceduresSelected.length
      );
    }

    printPatient_Procedures(element: Patient_ProcedureDetail): void {
      this.form_encounterPersist.patient_procedureDo(element.id, "print_result", {} ).subscribe((downloadJob: TCId) => {

        this.jobPersist.followJob(downloadJob.id, "printing Result", true);
        let success_state = 3;
        this.jobPersist.getJob(downloadJob.id).subscribe(
          job => {
            if (job.job_state == success_state) {
            }
          }
  
        )
      }, error => {
      })
    }
    goToNotes(element:Patient_ProcedureSummary): void {
      this.router.navigateByUrl(this.procedureNavigator.procedureNoteUrl(element.id))
      // this.procedurePersist.procedureDo(element.procedure_id, "validate_consent",{encounter_id: this.encounterId, id: element.id})
      //     .subscribe((response) => {
      //       if (!response.can_see_notes){
      //         this.tcNotification.error("The patient is not consent for this procedure.");
      //       } else {
      //         element.payment_id ? this.paymentPersist.getPayment(element.payment_id).subscribe((result)=> {
      //           if(result){
      //             if(result.payment_status == payment_status.paid || result.payment_status == payment_status.covered){
      //                 return this.router.navigateByUrl(`/patient_procedure_notes/${element.id}`);
      //             }
      //             else {
      //               this.tcNotification.error("Payment for this procedure is not paid")
      //             }
      //           }
      //         }): this.tcNotification.error("payment id is not added");;
      //       }
      //     })
    }


  addConsetForm(patientPro: Patient_ProcedureSummary): void {
    this.consetNav
      .addConsent_Form(patientPro.id, patientPro.patient_id)
      .afterClosed()
      .subscribe((res) => {
        if(res){
          patientPro.consent_id = res.id;
          this.form_encounterPersist.updatePatient_Procedure(this.encounterId ,patientPro).subscribe((result)=>{
            this.tcNotification.success("consent form add success");
            this.searchPatient_Procedures();
          })
        }

      });
  }

  getConsentForm(){
    this.consentFormPersist.consent_formDo(this.encounterId,"donloaded_data", {}).subscribe((result: Consent_FormSummary) => {
        this.consentForm = result;
    })
  }

    back(): void {
      this.location.back();
    } 

    getProcedure(id: string){
      if(this.procedures[id]){
      return this.procedures[id]?.name || ""
      }
    }
  

}
