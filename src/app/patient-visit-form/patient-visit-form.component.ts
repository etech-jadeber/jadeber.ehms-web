


import { Component, Input, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';
import { AppTranslation } from '../app.translation';
import { Form_EncounterDetail } from '../form_encounters/form_encounter.model';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryEditListComponent } from '../historys/history-edit/history-edit.component';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsAngular } from '../tc/utils-angular';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import { AppointmentNavigator } from '../appointments/appointment.navigator';
import { TCWizardProgress, TCWizardProgressModes } from '../tc/models';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Admission_FormNavigator } from '../form_encounters/admission_forms/admission_form.navigator';
import { Admission_FormPersist } from '../form_encounters/admission_forms/admission_form.persist';
import { deposit_status, lab_order_type, ServiceType, user_context } from '../app.enums';
import { DepositPersist } from '../deposits/deposit.persist';
import { EmergencyObservationDetail } from '../emergency_observation/emergency_observation.model';
import { EmergencyObservationPersist } from '../emergency_observation/emergency_observation.persist';
import { DiagnosisEDitListComponent } from '../form_encounters/diagnosiss/diagnosis-edit/diagnosis-edit.component';
import { FinalAssessmentEditListComponent } from '../final_assessment/final-assessment-edit/final-assessment-edit.component';
import { HistoryDetail } from '../historys/history.model';
import { DiagnosisDetail } from '../form_encounters/diagnosiss/diagnosis.model';
import { MatAccordion } from '@angular/material/expansion';

@Component({
  selector: 'app-patient-visit-form',
  templateUrl: './patient-visit-form.component.html',
  styleUrls: ['./patient-visit-form.component.scss']
})
export class PatientVisitFormComponent implements OnInit {

  profileUrl = "";
  @Input() form_encounterDetail: Form_EncounterDetail
  @Input() self = true;
  form_encounterLoading: boolean = false;
  remaining_deposit: number;
  user_context = user_context;
  serviceType = ServiceType;
  patientAge : number ;

  new_history = false;
  new_assessment = false ;
  new_final_assessment = false;  
  new_risk_factor = false;
  new_physical_examination = false;

  new_diagnosis = new DiagnosisDetail()
  importTime = 10 * 24 * 60 * 60;
  @ViewChildren('result') results = new QueryList<{saveResult: () => void}>();
  @ViewChild('history') history : HistoryEditListComponent;
  @ViewChild('assessment') assessment : DiagnosisEDitListComponent;
  @ViewChild('finalAssessment') finalAssessment : FinalAssessmentEditListComponent;
  @ViewChild('hisAc',{static: true}) hisAc : MatAccordion;
  date: number
  constructor(public form_encounterPersist: Form_EncounterPersist,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public location: Location,
                public appointmentNavigator : AppointmentNavigator,
                public form_encounterNavigator: Form_EncounterNavigator,
                public admissionFormNavigator: Admission_FormNavigator,
                public admissionFormPersist: Admission_FormPersist,
                public emeregencyObservationPersist: EmergencyObservationPersist,
                private deposiPersist: DepositPersist,
                ) { }

  ngOnInit(): void {
    this.tcAuthorization.requireRead('form_encounters'); 
    if(this.form_encounterDetail){
      this.profileUrl = `${environment.tcApiBaseUri}tc/files/download?fk=${this.form_encounterDetail.profile_pic}`;
      this.calculateAge(this.form_encounterDetail.birth_date)
    }
    if(this.form_encounterDetail.service_type == ServiceType.inpatient){
      this.searchDeposit();
    }
  }

  getCurrentTime(){
    return this.tcUtilsDate.toTimeStamp(new Date())
  }

  createFutureAppointment(): void {
    this.tcAuthorization.canCreate('appointments');
    let dialogRef = this.appointmentNavigator.addAppointment(
      this.form_encounterDetail.pid,
      this.form_encounterDetail.department_id,
      this.form_encounterDetail.provider_id,
      undefined,
      true,
      this.form_encounterDetail.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        let wizardProgress: TCWizardProgress = <TCWizardProgress>result;
        if (wizardProgress.progressMode == TCWizardProgressModes.NEXT) {
          this.addLab(true, result.item.schedule_start_time);
        } else if (
          wizardProgress.progressMode == TCWizardProgressModes.FINISH
        ) {
          this.tcNotification.success('Appointment sucessfully added');
        }
      }
    });
  }



  assignDiagnosis(data: any):void {
      this.new_diagnosis = data.new_diagnosis;
  }
  
  assignHistory(data: any): void {
    this.new_history = data.new_history; 
  }

  assignAssesment(data: any):void {
    this.new_assessment = data.new_assessment; 
  }
  assignNewPhysicalExamination(data: any) {
    this.new_physical_examination = data.new_physical_examination
  }
  assignNewFinalExamination(data: any ){
    this.new_final_assessment = data.new_final_assessment;

  }
  calculateAge(dob: number) {
    const current = new Date().getFullYear()   
    this.patientAge = current - new Date(dob*1000).getFullYear()  
  }

  assignRiskFactor(data:any){
    this.new_risk_factor = data.new_risk_factor;
  }
  // ngOnChanges(changes: SimpleChanges) {
  //   console.log("hello")
  //   if(this.history){
  //     this.new_history = this.history.is_new;
  //   }
      
  // }


  closeEncounter(): void {
    this.tcAuthorization.canUpdate('form_encounters');
    let dialogRef = this.tcNavigator.confirmAction(
      'Close',
      'Form Encounter',
      'Are you sure you want to close this encounter?',
      'cancel'
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.form_encounterPersist
          .form_encounterDo(
            this.form_encounterDetail.id,
            'close_encounter',
            this.form_encounterDetail
          )
          .subscribe((response) => {
            this.tcNotification.success('encounter is closed');
            this.back();
          });
      }
    });
  }


  
  ngAfterViewInit(): void {
    console.log("gebichalehu",this.history.is_new)

      this.new_history = this.history.is_new;
      // this.hisAc.closeAll();
    
  }

  addLab(
    isWizard: boolean = false,
    date: number = this.tcUtilsDate.toTimeStamp(new Date())
  ): void {
    let dialogRef = this.form_encounterNavigator.addLab(
      isWizard,
      this.form_encounterDetail.id,
      null,
      date,
      [lab_order_type.imaging, lab_order_type.pathology, lab_order_type.lab]
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.tcNotification.success('sucess');
      }
    });
  }
  admitPatient() {
    this.tcAuthorization.canCreate('admission_forms');
    this.admissionFormPersist.admission_formsDo("can_create",{encounter_id: this.form_encounterDetail.id}).subscribe((result:any)=>{
      if(result){
        if(result.can_create){
            let dialogRef = this.admissionFormNavigator.admission_order(this.form_encounterDetail.id);
            dialogRef.afterClosed().subscribe(result => {
              if (result) {
                this.reload();
              }
          });
        }
      }
    }
    )

  }

  dateTime(date: number): void {
    this.date =date;
  }

  emergency_observation() {
    this.tcAuthorization.canCreate('emergency_observations');
    let emergencyObservationDetail: EmergencyObservationDetail = new EmergencyObservationDetail();
    emergencyObservationDetail.encounter_id = this.form_encounterDetail.id;
    emergencyObservationDetail.patient_id = this.form_encounterDetail.pid;
    this.emeregencyObservationPersist.addEmergencyObservation(emergencyObservationDetail).subscribe(value => {
      this.tcNotification.success("emergencyObservation added");
      emergencyObservationDetail.id = value.id;
      this.form_encounterDetail.service_type = ServiceType.emergency;
    }
    )
    
  }

  back():void{
    this.location.back();
  }

  canImport(): boolean {
    return (this.getCurrentTime() - this.form_encounterDetail.date < this.importTime) && this.form_encounterPersist.encounter_is_active && this.form_encounterDetail.service_type == this.serviceType.inpatient
  }
  loadDetails(id: string): void {
    this.tcAuthorization.requireRead('form_encounters');
    this.form_encounterLoading = true;
    this.form_encounterPersist.getForm_Encounter(id).subscribe(
      (form_encounterDetail) => {
        this.form_encounterDetail = form_encounterDetail;
        this.form_encounterLoading = false;
      });
    }
  reload() {
    this.loadDetails(this.form_encounterDetail.id);
  }
  saveAll(){
    // if(!this.history.canSubmit()){
    //   this.tcNotification.error("HPI and Chief Complaint is required")
    //   return;
    // }
    // if(!this.assessment.canSubmit() && !this.finalAssessment.canSubmit()){
    //   this.tcNotification.error("Final Diagnosis or Impression Preliminary Diagnosis is required")
    //   return;
    // }
    for (let result of this.results.toArray()){
      result.saveResult()
    }
    this.reload()
  }

  depositSearchHistory = { ...this.deposiPersist.depositSearchHistory }

  searchDeposit(): void {
    this.depositSearchHistory.status = deposit_status.opened;
    this.depositSearchHistory.patient_id = this.form_encounterDetail.pid
    this.deposiPersist.searchDeposit(1, 0, 'id', 'asc').subscribe(deposit => {
      if(deposit.data.length){
        this.remaining_deposit = deposit.data[0].current_amount
      }
    })
  }

  ngOnDestroy() {
    // this.deposiPersist.statusFilter = null;
    // this.deposiPersist.patientId = null;
  }

  importEncounter(): void {
    this.form_encounterPersist.form_encounterDo(this.form_encounterDetail.id, "import_encounter", {}).subscribe(
      res => this.reload()
    )
  }

}


