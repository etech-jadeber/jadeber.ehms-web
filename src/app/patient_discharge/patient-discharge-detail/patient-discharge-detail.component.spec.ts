import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDischargeDetailComponent } from './patient-discharge-detail.component';

describe('PatientDischargeDetailComponent', () => {
  let component: PatientDischargeDetailComponent;
  let fixture: ComponentFixture<PatientDischargeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientDischargeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDischargeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
