import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PatientDischargeDetail} from "../patient_discharge.model";
import {PatientDischargePersist} from "../patient_discharge.persist";
import {PatientDischargeNavigator} from "../patient_discharge.navigator";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { PatientSummary } from 'src/app/patients/patients.model';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { BedSummary } from 'src/app/bed/beds/bed.model';
import { BedPersist } from 'src/app/bed/beds/bed.persist';

export enum PatientDischargeTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-patient_discharge-detail',
  templateUrl: './patient-discharge-detail.component.html',
  styleUrls: ['./patient-discharge-detail.component.css']
})
export class PatientDischargeDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  patientDischargeIsLoading:boolean = false;
  
  PatientDischargeTabs: typeof PatientDischargeTabs = PatientDischargeTabs;
  activeTab: PatientDischargeTabs = PatientDischargeTabs.overview;
  patients: PatientSummary[] = [];
  specialities: Department_SpecialtySummary [] = [];
  beds: BedSummary[] = [];
  //basics
  patientDischargeDetail: PatientDischargeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public patientDischargeNavigator: PatientDischargeNavigator,
              public  patientDischargePersist: PatientDischargePersist,
              public patientPersist: PatientPersist,
              public specialityPersist: Department_SpecialtyPersist,
              public tcUtilsDate: TCUtilsDate,
              public bedPersist: BedPersist,
                            ) {
    this.tcAuthorization.requireRead("patient_discharges");
    this.patientDischargeDetail = new PatientDischargeDetail();
  } ngOnInit() {
    this.searchBeds();
    this.searchPatients();
    this.searchSpecialities();
    this.tcAuthorization.requireRead("patient_discharges");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.patientDischargeIsLoading = true;
    this.patientDischargePersist.getPatientDischarge(id).subscribe(patientDischargeDetail => {
          this.patientDischargeDetail = patientDischargeDetail;
          this.patientDischargeIsLoading = false;
        }, error => {
          console.error(error);
          this.patientDischargeIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.patientDischargeDetail.id);
  }
  editPatientDischarge(): void {
    let modalRef = this.patientDischargeNavigator.editPatientDischarge(this.patientDischargeDetail.id, "edit");
    modalRef.afterClosed().subscribe(patientDischargeDetail => {
      TCUtilsAngular.assign(this.patientDischargeDetail, patientDischargeDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.patientDischargePersist.print(this.patientDischargeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print patient_discharge", true);
      });
    
  }
  searchBeds(): void {
    this.bedPersist.searchBed(50, 0, "", "").subscribe(result => {
      this.beds = result.data;
    })
  }
    searchPatients(): void {
      this.patientPersist.searchPatient(50, 0, "", "").subscribe(result => {
        console.log("the patients are ", this.patients);
        this.patients = result.data;
      })
    }
    searchSpecialities(): void {
      this.specialityPersist.searchDepartment_Specialty(50, 0, "", "").subscribe(result => {
        console.log("the patients are ", this.patients);
        this.specialities = result.data;
      })
    }
    

  back():void{
      this.location.back();
    }

}