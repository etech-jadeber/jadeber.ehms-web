import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDischargeEditComponent } from './patient-discharge-edit.component';

describe('PatientDischargeEditComponent', () => {
  let component: PatientDischargeEditComponent;
  let fixture: ComponentFixture<PatientDischargeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientDischargeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDischargeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
