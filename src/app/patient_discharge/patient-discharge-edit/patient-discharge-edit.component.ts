import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientDischargeDetail } from '../patient_discharge.model';import { PatientDischargePersist } from '../patient_discharge.persist';import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { Department_SpecialtyNavigator } from 'src/app/department_specialtys/department_specialty.navigator';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { BedNavigator } from 'src/app/bed/beds/bed.navigator';
import { BedSummary } from 'src/app/bed/beds/bed.model';
@Component({
  selector: 'app-patient_discharge-edit',
  templateUrl: './patient-discharge-edit.component.html',
  styleUrls: ['./patient-discharge-edit.component.css']
})export class PatientDischargeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientDischargeDetail: PatientDischargeDetail;
  patientFullName: string;
  departmentSpeciality:string;
  bedNumber: number;
  dateOfAdmission: Date = new Date();
  followupDate: Date;
  date: Date = new Date();
  constructor(
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PatientDischargeEditComponent>,
              public  persist: PatientDischargePersist,
              public patientNavigator: PatientNavigator,
              public tcUtilsDate: TCUtilsDate,
              public departmentSpecialityNavigator: Department_SpecialtyNavigator,
              public bedNavigator: BedNavigator,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_discharges");
      this.title = this.appTranslation.getText("general","new") +  " " + "patient_discharge";
      this.patientDischargeDetail = new PatientDischargeDetail();
      this.patientDischargeDetail.bed_no = "123e4567-e89b-12d3-a456-426614174000";
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_discharges");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "patient_discharge";
      this.isLoadingResults = true;
      this.persist.getPatientDischarge(this.idMode.id).subscribe(patientDischargeDetail => {
        this.patientDischargeDetail = patientDischargeDetail;
        this.transformDates(true)
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(false);
    this.persist.addPatientDischarge(this.idMode.id, this.patientDischargeDetail).subscribe(value => {
      this.tcNotification.success("patientDischarge added");
      this.patientDischargeDetail.id = value.id;
      this.dialogRef.close(this.patientDischargeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(false);
    this.persist.updatePatientDischarge(this.patientDischargeDetail).subscribe(value => {
      this.tcNotification.success("patient_discharge updated");
      this.dialogRef.close(this.patientDischargeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.patientDischargeDetail.encounter_id = result[0].id;
      }
    });
  }

  searchDepartmentSpeciality() {
    let dialogRef = this.departmentSpecialityNavigator.pick_all_department_Specialtys(true);
    dialogRef.afterClosed().subscribe((result: Department_SpecialtySummary[]) => {
      if (result) {
        this.patientDischargeDetail.speciality = result[0].id;
        this.departmentSpeciality = result[0].name
      }
    });
  }

  searchBed() {
    let dialogRef = this.bedNavigator.pickBeds(true);
    dialogRef.afterClosed().subscribe((result: BedSummary[]) => {
      if (result) {
        this.patientDischargeDetail.bed_no = result[0].id;
        this.bedNumber = result[0].serial_id;
      }
    });
  }
  
  transformDates(todate: boolean) {
    if (todate) {
      this.dateOfAdmission = this.tcUtilsDate.toDate(this.patientDischargeDetail.date_of_admission);
      this.followupDate = this.tcUtilsDate.toDate(this.patientDischargeDetail.followup_date);
      this.date = this.tcUtilsDate.toDate(this.patientDischargeDetail.date);
    } else {
      this.patientDischargeDetail.date_of_admission = this.tcUtilsDate.toTimeStamp(this.dateOfAdmission);
     if(this.patientDischargeDetail.followup_date){
      this.patientDischargeDetail.followup_date = this.tcUtilsDate.toTimeStamp(this.followupDate);
     }
      this.patientDischargeDetail.date = this.tcUtilsDate.toTimeStamp(this.date);
    }
  }

  canSubmit():boolean{
        if (this.patientDischargeDetail == null){
            return false;
          }

// if (this.patientDischargeDetail.hospital_course == null || this.patientDischargeDetail.hospital_course  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.history_and_examination_notes == null || this.patientDischargeDetail.history_and_examination_notes  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.lab_and_investigation_notes == null || this.patientDischargeDetail.lab_and_investigation_notes  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.radiology_notes == null || this.patientDischargeDetail.radiology_notes  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.surgeries_and_procedure_notes == null || this.patientDischargeDetail.surgeries_and_procedure_notes == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.operative_procedure_notes == null || this.patientDischargeDetail.operative_procedure_notes  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.other_notes == null || this.patientDischargeDetail.other_notes  == "") {
//             return false;
//         } 
if (this.patientDischargeDetail.condition_status == null) {
            return false;
        }
// if (this.patientDischargeDetail.advice == null || this.patientDischargeDetail.advice  == "") {
//             return false;
//         } 
// if (this.patientDischargeDetail.plan_for_followup == null || this.patientDischargeDetail.plan_for_followup  == "") {
//             return false;
//         }   
  return true;
 }
 }