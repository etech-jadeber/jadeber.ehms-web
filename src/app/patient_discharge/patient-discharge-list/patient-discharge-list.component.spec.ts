import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDischargeListComponent } from './patient-discharge-list.component';

describe('PatientDischargeListComponent', () => {
  let component: PatientDischargeListComponent;
  let fixture: ComponentFixture<PatientDischargeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientDischargeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDischargeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
