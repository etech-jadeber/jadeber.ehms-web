import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientDischargeSummary, PatientDischargeSummaryPartialList } from '../patient_discharge.model';
import { PatientDischargePersist } from '../patient_discharge.persist';
import { PatientDischargeNavigator } from '../patient_discharge.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientSummary } from 'src/app/patients/patients.model';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { Department_SpecialtySummary } from 'src/app/department_specialtys/department_specialty.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { UserSummary, UserSummaryPartialList } from 'src/app/tc/users/user.model';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Discharge_status } from 'src/app/app.enums';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { encounterFilterColumn } from 'src/app/form_encounters/form_encounter.model';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { BedDetail } from 'src/app/bed/beds/bed.model';
import { BedroomDetail } from 'src/app/bed/bedrooms/bedroom.model';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
import { BedroomPersist } from 'src/app/bed/bedrooms/bedroom.persist';
@Component({
  selector: 'app-patient_discharge-list',
  templateUrl: './patient-discharge-list.component.html',
  styleUrls: ['./patient-discharge-list.component.css']
})
export class PatientDischargeListComponent implements OnInit {

  patientDischargesData: PatientDischargeSummary[] = [];
  patientDischargesTotalCount: number = 0;
  patientDischargeSelectAll:boolean = false;
  patientDischargeSelection: PatientDischargeSummary[] = [];
  patients: PatientSummary[] = [];
  patientDischargeFilterColumn = encounterFilterColumn;
  Discharge_status = Discharge_status;


  specialities: {[id: string]: Department_SpecialtySummary} = {}
  beds: { [id: string]: BedDetail } = {}
  bedrooms: { [id: string]: BedroomDetail } = {}
  wards: { [id: string]: BedroomtypeDetail } = {}

  patientDischargesDisplayedColumns: string[] = ["select", "action", 'p.pid', "patient_name" ,"date", "speciality", "no_of_days","discharged_by", "condition_status"];
  patientDischargeSearchTextBox: FormControl = new FormControl();
  patientDischargeIsLoading: boolean = false;

  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();
  @ViewChild(MatPaginator, {static: true}) patientDischargesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientDischargesSort: MatSort;
  inEncounter: boolean = true;
  usersData: UserSummary[] = [];
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientDischargePersist: PatientDischargePersist,
                public patientDischargeNavigator: PatientDischargeNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public wardPersist: BedroomtypePersist,
                public tcAsyncJob: TCAsyncJob,
                public patientPersist: PatientPersist,
                public specialityPersist: Department_SpecialtyPersist,
                public userPersist: UserPersist,
                public form_encounterPersist: Form_EncounterPersist,
                public bedPersist: BedPersist,
                public bedRoomPersist: BedroomPersist,
                public tcUtilsString: TCUtilsString,

    ) {

        this.tcAuthorization.requireRead("patient_discharges");
       this.patientDischargeSearchTextBox.setValue(patientDischargePersist.patientDischargeSearchHistory.search_text);
      //delay subsequent keyup events
      this.patientDischargeSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientDischargePersist.patientDischargeSearchHistory.search_text = value;
        this.searchPatientDischarges();
      });
    }
     ngOnInit() {
      // this.searchSpecialities();
      // this.searchPatients();
      this.patientDischargePersist.patientDischargeSearchHistory.encounter_id = this.encounterId
      this.patientDischargePersist.patientDischargeSearchHistory.patient_id = this.patientId
      this.patientDischargesSort.sortChange.subscribe(() => {
        this.patientDischargesPaginator.pageIndex = 0;
        this.searchPatientDischarges(true);
      });
      if (!this.encounterId){
        this.patientDischargePersist.patientDischargeSearchHistory.discharge_status = Discharge_status.ordered;
      }

      this.patientDischargesPaginator.page.subscribe(() => {
        this.searchPatientDischarges(true);
      });
      //start by loading items
      this.searchPatientDischarges();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.patientDischargePersist.patientDischargeSearchHistory.encounter_id = this.encounterId;
      } else {
        this.patientDischargePersist.patientDischargeSearchHistory.patient_id = null;
      }
      this.searchPatientDischarges()
    }

    ngOnDestroy() {
      this.patientDischargePersist.patientDischargeSearchHistory.discharge_status = null;
    }

  searchPatientDischarges(isPagination:boolean = false): void {
    if (this.encounterId == undefined) {
      this.encounterId = TCUtilsString.getInvalidId();
      this.inEncounter = false;
    }
    let paginator = this.patientDischargesPaginator;
    let sorter = this.patientDischargesSort;

    this.patientDischargeIsLoading = true;
    this.patientDischargeSelection = [];

    this.patientDischargePersist.searchPatientDischarge(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientDischargeSummaryPartialList) => {
      this.patientDischargesData = partialList.data;
      this.patientDischargesData.forEach(discharge => {
        this.searchSpecialities(discharge)
        this.searchPatient(discharge)
        this.searchUser(discharge)
        // if (!this.wards[discharge.ward_id]) {
        //   this.wards[discharge.ward_id] = new BedroomtypeDetail()
        //   this.wardPersist.getBedroomtype(discharge.ward_id).subscribe(
        //     ward => this.wards[discharge.ward_id] = ward
        //   )
        // }
        // if (!this.beds[discharge.bed_no]) {
        //   this.beds[discharge.bed_no] = new BedDetail()
        //   discharge.bed_no && discharge.bed_no != this.tcUtilsString.invalid_id && this.bedPersist.getBed(discharge.bed_no).subscribe(
        //     bed => {
        //       this.beds[discharge.bed_no] = bed;
        //     }
        //   )
        // }
        // if (!this.bedrooms[discharge.beedroom_id]) {

        //   this.bedrooms[discharge.beedroom_id] = new BedroomDetail()
        //   discharge.beedroom_id && discharge.beedroom_id != this.tcUtilsString.invalid_id && this.bedRoomPersist.getBedroom(discharge.beedroom_id).subscribe(
        //     bedroom => {
        //       this.bedrooms[discharge.beedroom_id] = bedroom;
        //     }
        //   )
        // }
      })
      if (partialList.total != -1) {
        this.patientDischargesTotalCount = partialList.total;
      }
      this.patientDischargeIsLoading = false;
    }, error => {
      this.patientDischargeIsLoading = false;
    });

  }

  searchPatient(discharge: PatientDischargeSummary){
    this.form_encounterPersist.getForm_Encounter(discharge.encounter_id).subscribe((encounter) => {
      this.patientPersist.getPatient(encounter.pid).subscribe(patient => {
        discharge.patient_name = patient.fname + " " + patient.mname + " " + patient.lname;
      })
    })
  }

  downloadPatientDischarges(): void {
    if(this.patientDischargeSelectAll){
         this.patientDischargePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patient_discharge", true);
      });
    }
    else{
        this.patientDischargePersist.download(this.tcUtilsArray.idsList(this.patientDischargeSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download patient_discharge",true);
            });
        }
  }

  searchPatients(): void {
    this.patientPersist.searchPatient(50, 0, "", "").subscribe(result => {
      this.patients = result.data;
    })
  }
  searchSpecialities(element: PatientDischargeSummary): void {
    if(!this.specialities[element.speciality]){
      this.specialities[element.speciality] = new Department_SpecialtySummary()
      this.specialityPersist.getDepartment_Specialty(element.speciality).subscribe((department) => {
      this.specialities[element.speciality] = department
    });
    }
  }

  getSpeciality(id: string){
    if(this.specialities[id]){
    return this.specialities[id]?.name || ""
    }
  }


addPatientDischarge(): void {
    let dialogRef = this.patientDischargeNavigator.addPatientDischarge(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPatientDischarges();
      }
    });
  }

  editPatientDischarge(item: PatientDischargeSummary) {
    let dialogRef = this.patientDischargeNavigator.editPatientDischarge(item.id, "edit");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  searchUser(discharge: PatientDischargeSummary): void {
    this.userPersist.getUser(discharge.discharged_by).subscribe((user) => {
      discharge.discharged_username = user.name;
    });

    // this.userPersist.getUser(discharge.order_by).subscribe((user) => {
    //   discharge.order_by_username = user.name;
    // });

  }

  deletePatientDischarge(item: PatientDischargeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("patient_discharge");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.patientDischargePersist.deletePatientDischarge(item.id).subscribe(response => {
          this.tcNotification.success("patient_discharge deleted");
          this.searchPatientDischarges();
        }, error => {
        });
      }

    });
  }

  discharge(patientDischargeDetail: PatientDischargeSummary): void {
    this.patientDischargePersist.patientDischargeDo(patientDischargeDetail.id, "discharge_patient", {}).subscribe(result => {
      if (result) {
        this.searchPatientDischarges();
        this.tcNotification.success("Discharged.");
      }
    })
  }

  rejectDischarge(patientDischargeDetail: PatientDischargeSummary): void {
    this.patientDischargePersist.patientDischargeDo(patientDischargeDetail.id, "reverse_order", {}).subscribe(result => {
        this.searchPatientDischarges();
        this.tcNotification.success("Discharge Rejected.");
    })
  }

  reverseDischarge(patientDischargeDetail: PatientDischargeSummary): void {
    this.patientDischargePersist.patientDischargeDo(patientDischargeDetail.id, "reverse_discharge", {}).subscribe(result => {
      this.searchPatientDischarges();
      this.tcNotification.success("Discharge Reversed.");
    })
  }

  closeEncounter(encounter_id: string): void {
    this.tcAuthorization.requireUpdate('form_encounters');
    let dialogRef = this.tcNavigator.confirmAction(
      'Close',
      'Form Encounter',
      'Are you sure you want to close this encounter?',
      'cancel'
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.form_encounterPersist
          .form_encounterDo(
            encounter_id,
            'close_encounter',
            null
          )
          .subscribe((response) => {
            this.tcNotification.success('encounter is closed');
            this.back();
          });
      }
    });
  }

  back():void{
      this.location.back();
    }

  getWard(ward_id: string): string {
    const ward = this.wards[ward_id]
    return ward.name ? `${ward.name}` : ''
  }

  getBedroom(bed_room_id: string): string {
    if (!this.bedrooms[bed_room_id]) {
      return ''
    }
    return this.bedrooms[bed_room_id].name
  }

  getBed(bed_id: string): string {
    if (!this.beds[bed_id]?.serial_id) {
      return ''
    }
    return `${this.beds[bed_id]?.serial_id}`
  }

}
