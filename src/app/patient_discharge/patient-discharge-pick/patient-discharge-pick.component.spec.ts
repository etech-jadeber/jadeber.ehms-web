import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDischargePickComponent } from './patient-discharge-pick.component';

describe('PatientDischargePickComponent', () => {
  let component: PatientDischargePickComponent;
  let fixture: ComponentFixture<PatientDischargePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientDischargePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDischargePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
