import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSort, } from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientDischargeSummary, PatientDischargeSummaryPartialList } from '../patient_discharge.model';
import { PatientDischargePersist } from '../patient_discharge.persist';
import { PatientDischargeNavigator } from '../patient_discharge.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { encounterFilterColumn } from 'src/app/form_encounters/form_encounter.model';
@Component({
  selector: 'app-patient_discharge-pick',
  templateUrl: './patient-discharge-pick.component.html',
  styleUrls: ['./patient-discharge-pick.component.css']
})export class PatientDischargePickComponent implements OnInit {
  patientDischargesData: PatientDischargeSummary[] = [];
  patientDischargesTotalCount: number = 0;
  patientDischargeSelectAll:boolean = false;
  patientDischargeSelection: PatientDischargeSummary[] = [];
  patientDischargeFilterColumn = encounterFilterColumn;

 patientDischargesDisplayedColumns: string[] = ["select", ,"date","pid","speciality","bed_no","date_of_admission","no_of_days","discharged_by","hospital_course","history_and_examination_notes","lab_and_investigation_notes","radiology_notes","surgeries and procedure","operative_procedure","other_notes","condition_status","advice","plan_for_followup","followup_date" ];
  patientDischargeSearchTextBox: FormControl = new FormControl();
  patientDischargeIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) patientDischargesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientDischargesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientDischargePersist: PatientDischargePersist,
                public patientDischargeNavigator: PatientDischargeNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PatientDischargePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("patient_discharges");
       this.patientDischargeSearchTextBox.setValue(patientDischargePersist.patientDischargeSearchHistory.search_text);
      //delay subsequent keyup events
      this.patientDischargeSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientDischargePersist.patientDischargeSearchHistory.search_text = value;
        this.searchPatient_discharges();
      });
    } ngOnInit() {
   
      this.patientDischargesSort.sortChange.subscribe(() => {
        this.patientDischargesPaginator.pageIndex = 0;
        this.searchPatient_discharges(true);
      });

      this.patientDischargesPaginator.page.subscribe(() => {
        this.searchPatient_discharges(true);
      });
      //start by loading items
      this.searchPatient_discharges();
    }

  searchPatient_discharges(isPagination:boolean = false): void {


    let paginator = this.patientDischargesPaginator;
    let sorter = this.patientDischargesSort;

    this.patientDischargeIsLoading = true;
    this.patientDischargeSelection = [];

    this.patientDischargePersist.searchPatientDischarge(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientDischargeSummaryPartialList) => {
      this.patientDischargesData = partialList.data;
      if (partialList.total != -1) {
        this.patientDischargesTotalCount = partialList.total;
      }
      this.patientDischargeIsLoading = false;
    }, error => {
      this.patientDischargeIsLoading = false;
    });

  }
  markOneItem(item: PatientDischargeSummary) {
    if(!this.tcUtilsArray.containsId(this.patientDischargeSelection,item.id)){
          this.patientDischargeSelection = [];
          this.patientDischargeSelection.push(item);
        }
        else{
          this.patientDischargeSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.patientDischargeSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }