import {TCId} from "../tc/models";export class PatientDischargeSummary extends TCId {
    date:number;
    encounter_id:string;
    speciality:string;
    bed_no:string;
    date_of_admission:number;
    no_of_days:number;
    discharged_by:string;
    hospital_course:string;
    history_and_examination_notes:string;
    lab_and_investigation_notes:string;
    radiology_notes:string;
    surgeries_and_procedure_notes:string;
    operative_procedure_notes:string;
    other_notes:string;
    condition_status:number;
    advice:string;
    plan_for_followup:string;
    followup_date:number;
    discharge_status: number;
    patient_name: string;
    discharged_username: string;
     
    }
    export class PatientDischargeSummaryPartialList {
      data: PatientDischargeSummary[];
      total: number;
    }
    export class PatientDischargeDetail extends PatientDischargeSummary {
    }
    
    export class PatientDischargeDashboard {
      total: number;
    }