import {Router} from "@angular/router";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Injectable } from "@angular/core";

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PatientDischargeEditComponent} from "./patient-discharge-edit/patient-discharge-edit.component";
import {PatientDischargePickComponent} from "./patient-discharge-pick/patient-discharge-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PatientDischargeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  patientDischargesUrl(): string {
    return "/patient_discharges";
  }

  patientDischargeUrl(id: string): string {
    return "/patient_discharges/" + id;
  }

  viewPatientDischarges(): void {
    this.router.navigateByUrl(this.patientDischargesUrl());
  }

  viewPatientDischarge(id: string): void {
    this.router.navigateByUrl(this.patientDischargeUrl(id));
  }

  editPatientDischarge(id: string, mode: string): MatDialogRef<PatientDischargeEditComponent> {
    const dialogRef = this.dialog.open(PatientDischargeEditComponent, {
      data: new TCIdMode(id, mode),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPatientDischarge(parentId: string): MatDialogRef<PatientDischargeEditComponent> {
    return this.editPatientDischarge(parentId, TCModalModes.NEW);
  }

   pickPatientDischarges(selectOne: boolean=false): MatDialogRef<PatientDischargePickComponent> {
      const dialogRef = this.dialog.open(PatientDischargePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}