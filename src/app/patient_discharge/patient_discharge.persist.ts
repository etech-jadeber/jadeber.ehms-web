import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PatientDischargeDashboard, PatientDischargeDetail, PatientDischargeSummaryPartialList} from "./patient_discharge.model";
import { Condition_status, Discharge_status } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PatientDischargePersist {
//  patientDischargeSearchText: string = "";
//  discharge_status: number;
//  encounterId: string;
//  patientId: string;
 constructor(private http: HttpClient) {
  }
  filters(searchHistory = this.patientDischargeSearchHistory): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  condition_statuses: TCEnum[] = [
    new TCEnum(Condition_status.cured, 'cured'),
    new TCEnum(Condition_status.improved, 'Improved'),
    new TCEnum(Condition_status.worsened, 'worsened'),
    new TCEnum(Condition_status.against_medical, 'Against Medical'),
    new TCEnum(Condition_status.no_change, 'no change'),
    new TCEnum(Condition_status.died, 'died'),
  ];

  discharge_statuses: TCEnum[] = [
    new TCEnum(Discharge_status.ordered, 'Ordered'),
    new TCEnum(Discharge_status.discharged, 'Discharged'),
  ];

  patientDischargeSearchHistory = {
    "discharge_status": undefined,
    "encounter_id": "",
    "patient_id": "",
    "search_text": "",
    "search_column": "",
}

defaultPatientDischargeSearchHistory = {...this.patientDischargeSearchHistory}

resetPatientDischargeSearchHistory(){
  this.patientDischargeSearchHistory = {...this.defaultPatientDischargeSearchHistory}
}

  searchPatientDischarge(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.patientDischargeSearchHistory): Observable<PatientDischargeSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(  "patient_discharge/", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.discharge_status){
    //   url = TCUtilsString.appendUrlParameter(url, "discharge_status", this.discharge_status.toString())
    // }
    // if (this.encounterId){
    //   url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
    // }
    // if (this.patientId){
    //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    // }
    return this.http.get<PatientDischargeSummaryPartialList>(url);

  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_discharge/do", new TCDoParam("download_all", this.filters()));
  }

  patientDischargeDashboard(): Observable<PatientDischargeDashboard> {
    return this.http.get<PatientDischargeDashboard>(environment.tcApiBaseUri + "patient_discharge/dashboard");
  }

  getPatientDischarge(id: string): Observable<PatientDischargeDetail> {
    return this.http.get<PatientDischargeDetail>(environment.tcApiBaseUri + "patient_discharge/" + id);
  }

  addPatientDischarge(parentId: string, item: PatientDischargeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/patient_discharge/", item);
  }

  updatePatientDischarge(item: PatientDischargeDetail): Observable<PatientDischargeDetail> {
    return this.http.patch<PatientDischargeDetail>(environment.tcApiBaseUri + "patient_discharge/" + item.id, item);
  }

  deletePatientDischarge(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_discharge/" + id);
  }
 patientDischargesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_discharge/do", new TCDoParam(method, payload));
  }

  patientDischargeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_discharge/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_discharge/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patient_discharge/" + id + "/do", new TCDoParam("print", {}));
  }


}
