import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientHistoryDetailComponent } from './patient-history-detail.component';

describe('PatientHistoryDetailComponent', () => {
  let component: PatientHistoryDetailComponent;
  let fixture: ComponentFixture<PatientHistoryDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientHistoryDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientHistoryDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
