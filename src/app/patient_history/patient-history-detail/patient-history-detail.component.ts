import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { PatientHistoryDetail } from '../patient_history.model';
import { PatientHistoryNavigator } from '../patient_history.navigator';
import { PatientHistoryPersist } from '../patient_history.persist';


export enum PatientHistoryTabs {
  overview,
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-patient-history-detail',
  templateUrl: './patient-history-detail.component.html',
  styleUrls: ['./patient-history-detail.component.scss']
})
export class PatientHistoryDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  patientHistoryIsLoading:boolean = false;
  
  PatientHistoryTabs: typeof PatientHistoryTabs = PatientHistoryTabs;
  activeTab: PatientHistoryTabs = PatientHistoryTabs.overview;
  //basics
  patientHistoryDetail: PatientHistoryDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public patientHistoryNavigator: PatientHistoryNavigator,
              public  patientHistoryPersist: PatientHistoryPersist) {
    this.tcAuthorization.requireRead("patient_historys");
    this.patientHistoryDetail = new PatientHistoryDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("patient_historys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.patientHistoryIsLoading = true;
    this.patientHistoryPersist.getPatientHistory(id).subscribe(patientHistoryDetail => {
          this.patientHistoryDetail = patientHistoryDetail;
          this.patientHistoryIsLoading = false;
        }, error => {
          console.error(error);
          this.patientHistoryIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.patientHistoryDetail.id);
  }
  
  editPatientHistory(): void {
    let modalRef = this.patientHistoryNavigator.editPatientHistory(this.patientHistoryDetail.id);
    modalRef.afterClosed().subscribe(patientHistoryDetail => {
      TCUtilsAngular.assign(this.patientHistoryDetail, patientHistoryDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.patientHistoryPersist.print(this.patientHistoryDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print patient_history", true);
      });
    }

  back():void{
      this.location.back();
    }

}