import {Component, OnInit, Inject, Injectable, Input, ViewChild, HostListener, Output, EventEmitter} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {map, startWith} from 'rxjs/operators';
import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientHistoryDetail } from '../patient_history.model';
import { PatientHistoryPersist } from '../patient_history.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { Icd11Persist } from 'src/app/icd_11/icd_11.persist';
import {Observable} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import { Icd11Detail, Icd11Summary, Icd11SummaryPartialList } from 'src/app/icd_11/icd_11.model';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { DiagnosisPersist } from 'src/app/form_encounters/diagnosiss/diagnosis.persist';
import { DiagnosisDetail } from 'src/app/form_encounters/diagnosiss/diagnosis.model';
import { QueuesPersist } from 'src/app/queues/queues.persist';

// import { Location } from '@angular/common';

@Injectable()
abstract class PatientHistoryEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  isModal:boolean
  illness_date: string;
  accidents_date: string;
  operation_date: string;
  icd11sData: Icd11Summary[] = [];
  icdValue :string = "";
  icdCount :number;
  diagnosisDetail :DiagnosisDetail;




  myControl = new FormControl('');
  options: string[] = [];
  filteredOptions: Observable<string[]>;

  icd11_diagnosis_options: Icd11Summary[];

  @ViewChild(MatAutocompleteTrigger) autocompleteTrigger: MatAutocompleteTrigger;
  @Input() encounterId: string;
  patientHistoryDetail: PatientHistoryDetail;
  constructor(
              // public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: PatientHistoryPersist,

              public icd11Persist : Icd11Persist,
              public form_encounterPersist: Form_EncounterPersist,

              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode,
              public  diagnosisPersist: DiagnosisPersist,
              public queuesPersist: QueuesPersist,

              ) {
                // this.myControl.valueChanges()

                this.myControl.valueChanges.subscribe((newValue,) =>{
                     console.log("==00",newValue)

                     this.icd11Persist.icd11SearchText = newValue;
                     this.patientHistoryDetail.provisional_psychiatric_diagnosis = newValue;


                    this.icd11Persist.searchIcd11( 5, 0, "id", "ASC").subscribe((partialList: Icd11SummaryPartialList) => {
                      this.icd11sData = partialList.data;
                      this.icdCount = partialList.total


                    }, error => {
                    });
                  })

  }

  onCancel(): void {
  }

  action(patientHistoryDetail:PatientHistoryDetail):void{
    // console.log(patientHistoryDetail)
   
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  updateHistory(){
    this.persist.updatePatientHistory(this.patientHistoryDetail).subscribe(value => {
      this.tcNotification.success("patient_history updated");
      console.log("updated")

      this.action(this.patientHistoryDetail)
    }, error => {
      this.isLoadingResults = false;
    })
  }

  addHistory(){
    this.patientHistoryDetail.encounter_id = this.encounterId
    this.persist.addPatientHistory(this.patientHistoryDetail).subscribe(value => {
      this.tcNotification.success("patient_history updated");
      this.patientHistoryDetail.id = value.id

      this.action(this.patientHistoryDetail)
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onBlur(){
    if(this.patientHistoryDetail.id){
      this.updateHistory();
    } else {
      this.addHistory();
    }

  }
  ngOnInit() {
    this.diagnosisDetail = new DiagnosisDetail();
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_historys");
      this.title = this.appTranslation.getText("general","new") +  " " + "patient_history";
      this.patientHistoryDetail = new PatientHistoryDetail();
      this.patientHistoryDetail.encounter_id = this.idMode.id;
      this.patientHistoryDetail.chief_compliant = "";
      this.patientHistoryDetail.hpi = "";
      this.patientHistoryDetail.risk_assessment = "";
      this.patientHistoryDetail.family_history = "";
      this.patientHistoryDetail.personal_history = "";
      this.patientHistoryDetail.pregnancy_birth = "";
      this.patientHistoryDetail.mode_of_delivery = "";
      this.patientHistoryDetail.history_of_abortion = "";
      this.patientHistoryDetail.health_during_childhood = "";
      this.patientHistoryDetail.education = "";
      this.patientHistoryDetail.work = "";
      this.patientHistoryDetail.sexual_history = "";
      this.patientHistoryDetail.menustrial_history = "";
      this.patientHistoryDetail.maritual_history = "";
      this.patientHistoryDetail.childhood_psychological_history = "";
      this.patientHistoryDetail.physical_sexual_history = "";
      this.patientHistoryDetail.illness = "";
      this.patientHistoryDetail.operation = "";
      this.patientHistoryDetail.accidents = "";
      // this.patientHistoryDetail.accidents_date: number;
      // this.patientHistoryDetail.illness_date: number;
      // this.patientHistoryDetail.operation_date: number;
      this.patientHistoryDetail.any_current_medication = "";
      this.patientHistoryDetail.previous_psychiatric_history = "";
      this.patientHistoryDetail.forensic_history = "";
      this.patientHistoryDetail.substance_history = "";
      this.patientHistoryDetail.premorbid_personality = "";
      this.patientHistoryDetail.attention_given = "";

    //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("patient_historys");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "patient_history";
      this.isLoadingResults = true;
      this.persist.getPatientHistory(this.idMode.id).subscribe(patientHistoryDetail => {
        this.patientHistoryDetail = patientHistoryDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }
  onAdd(encounter_id:string = ""): void {
    this.transformDates()
    this.isLoadingResults = true;
    const x = this.getDiagnosis(this.patientHistoryDetail.provisional_psychiatric_diagnosis)
    // for( let i of x){
      this.diagnosisDetail.name = x.join("_")

      this.diagnosisPersist.addDiagnosis(encounter_id,this.diagnosisDetail).subscribe(result=>{
        console.log("happy",result)
     })
    // }
    console.log("new",this.patientHistoryDetail.provisional_psychiatric_diagnosis)

    this.persist.addPatientHistory(this.patientHistoryDetail).subscribe(value => {
      this.tcNotification.success("patientHistory added");
      this.patientHistoryDetail.id = value.id;
      this.queuesPersist.updateQueues({queue_status:"opened"},this.patientHistoryDetail.encounter_id).subscribe((res)=>{
        console.log("queues upateded",res)
       })
      this.action(this.patientHistoryDetail)
    }, error => {
      this.isLoadingResults = false;
    })
  }
  onKeyDown(event:string ,input):void{}
  onUpdate(encounter_id:string = ""): void {
    console.log("updated -----------------------")

    this.isLoadingResults = true;
    this.transformDates()

   console.log("updated",this.patientHistoryDetail.provisional_psychiatric_diagnosis)
   const x = this.getDiagnosis(this.patientHistoryDetail.provisional_psychiatric_diagnosis)
   console.log("********************",x)
    // for( let i of x){
      this.diagnosisDetail.name = x.join("_")
      // console.log("----------",i,this.diagnosisDetail,encounter_id)

      this.diagnosisPersist.addDiagnosis(encounter_id,this.diagnosisDetail).subscribe(result=>{
         console.log("happy",result)
      })
    // }
    this.persist.updatePatientHistory(this.patientHistoryDetail).subscribe(value => {
      this.tcNotification.success("patient_history updated");
      
      console.log("updated -----------------------")
      this.queuesPersist.updateQueues({queue_status:"opened"},this.patientHistoryDetail.encounter_id).subscribe((res)=>{
        console.log("queues upateded",res)
       })
      this.action(this.patientHistoryDetail)

    }, error => {
      this.isLoadingResults = false;
    })

  }
  getDiagnosis(z){
   const y  = []
   const x = z.split('\u2022')

    for (let i of x){
      if(i.trim()!=""){
        y.push(i.trim())
      }
    }
    return y;
  }
  appendString(value: string):string{
    value = value.trim();
    if (!this.patientHistoryDetail.provisional_psychiatric_diagnosis)
      return "\u2022  " + value;
    if(this.patientHistoryDetail.provisional_psychiatric_diagnosis.endsWith("\n"))
      return this.patientHistoryDetail.provisional_psychiatric_diagnosis + "\u2022  " + value;
    let x = this.patientHistoryDetail.provisional_psychiatric_diagnosis.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return  y ?  y+"\u2022  " +value : "\u2022  " + value;
  }

  isEmpty(value:string):boolean {
    return value == null || value == "";
  }

   load_icd11Diagnois(){
      this.icd11Persist.searchIcd11(5,0,"","").subscribe((result)=>{
        if(result)
          this.icd11_diagnosis_options = result.data;
      })
    }
  transformDates(todate: boolean = true) {
    if (todate) {
      this.patientHistoryDetail.operation_date = this.operation_date ?
        new Date(this.operation_date).getTime() / 1000 : null;
      this.patientHistoryDetail.illness_date = this.illness_date ?
        new Date(this.illness_date).getTime() / 1000 : null;
        this.patientHistoryDetail.accidents_date = this.accidents_date ?
          new Date(this.accidents_date).getTime() / 1000 : null;
    } else {
      this.operation_date = this.patientHistoryDetail.operation_date &&  this.getStartTime(this.tcUtilsDate.toDate(this.patientHistoryDetail.operation_date));
      this.illness_date = this.patientHistoryDetail.illness_date && this.getStartTime(this.tcUtilsDate.toDate(this.patientHistoryDetail.illness_date));
      this.accidents_date = this.patientHistoryDetail.accidents_date && this.getStartTime(this.tcUtilsDate.toDate(this.patientHistoryDetail.accidents_date));
    }
  }


  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}`;
  }

  isDisabled():boolean {
    return !(this.form_encounterPersist.encounter_is_active);
  }



  canSubmit():boolean{
    if (this.patientHistoryDetail == null){
        return false;
      }

    // if(this.patientHistoryDetail.chief_compliant == null || this.patientHistoryDetail.chief_compliant == ""){
    //   return false;
    // }
    // if(this.patientHistoryDetail.hpi == null || this.patientHistoryDetail.hpi == ""){
    //   return false;
    // }
    return true;

 }
 }

@Component({
  selector: 'app-patient-history-edit',
  templateUrl: './patient-history-edit.component.html',
  styleUrls: ['./patient-history-edit.component.scss']
})
export class PatientHistoryEditComponent extends PatientHistoryEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  isModal: boolean = true;

  patientHistoryDetail: PatientHistoryDetail;
  constructor(
              // public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public icd11Persist : Icd11Persist,
              public form_encounterPersist: Form_EncounterPersist,
              public dialogRef: MatDialogRef<PatientHistoryEditComponent>,
              public  persist: PatientHistoryPersist,


              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode,
              public  diagnosisPersist: DiagnosisPersist,
              public queuesPersist: QueuesPersist,
              
              ) {

                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist, icd11Persist, form_encounterPersist,tcUtilsDate,idMode,diagnosisPersist,queuesPersist)

  }

  onCancel(): void {
    this.dialogRef.close();
  }
  action():void{
    this.dialogRef.close(this.patientHistoryDetail);
  }

 }

 @Component({
  selector: 'app-patient-history-edit-list',
  templateUrl: './patient-history-edit.component.html',
  styleUrls: ['./patient-history-edit.component.scss']
})

 export class PatientHistoryEditListComponent extends PatientHistoryEditMainComponent {

    isModal: boolean = false;
    @Input() encounterId: string;
    @Input() self:boolean = true;
    @ViewChild(MatPaginator, {static: true}) icd11sPaginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) icd11sSort: MatSort;

    @Output() assign_value = new EventEmitter<boolean>();

    constructor(
              // public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public icd11Persist : Icd11Persist,
              public form_encounterPersist: Form_EncounterPersist,
              public  persist: PatientHistoryPersist,

              public tcUtilsDate: TCUtilsDate,
              public  diagnosisPersist: DiagnosisPersist,
              public queuesPersist: QueuesPersist,

              ) {

                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist, icd11Persist, form_encounterPersist,tcUtilsDate,{id:"", mode: TCModalModes.NEW},diagnosisPersist,queuesPersist)
  }


    action(){
      this.isLoadingResults=false;
    }


    ngOnInit(): void {








      this.searchPatientHistory();
      this.icd11Persist.icd11SearchText = ''

      this.icd11Persist.searchIcd11( 5, 0, "id", "ASC").subscribe((partialList: Icd11SummaryPartialList) => {
        this.icd11sData = partialList.data;
        this.options = this.icd11sData.map(value=>value.category)
      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value || '')),
      );

      }, error => {
      });



      }
      private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();

        return this.options.filter(option => option.toLowerCase().includes(filterValue));
      }


      searchPatientHistory(): void {
        this.persist.encounter_id = this.encounterId
        this.persist.searchPatientHistory(1, 0, 'date', 'desc').subscribe(
          patientHistorys => {
            if(patientHistorys.data.length){
            this.patientHistoryDetail =  patientHistorys.data[0]
            this.myControl.setValue(this.patientHistoryDetail.provisional_psychiatric_diagnosis)
            this.icdValue = this.patientHistoryDetail.provisional_psychiatric_diagnosis
            this.transformDates(false);
            }
            else{
                this.assign_value.emit(false);
                this.patientHistoryDetail = new PatientHistoryDetail();
                this.patientHistoryDetail.encounter_id = this.idMode.id;
                this.patientHistoryDetail.chief_compliant = "";
                this.patientHistoryDetail.hpi = "";
                this.patientHistoryDetail.risk_assessment = "";
                this.patientHistoryDetail.family_history = "";
                this.patientHistoryDetail.personal_history = "";
                this.patientHistoryDetail.pregnancy_birth = "";
                this.patientHistoryDetail.mode_of_delivery = "";
                this.patientHistoryDetail.history_of_abortion = "";
                this.patientHistoryDetail.health_during_childhood = "";
                this.patientHistoryDetail.education = "";
                this.patientHistoryDetail.work = "";
                this.patientHistoryDetail.sexual_history = "";
                this.patientHistoryDetail.menustrial_history = "";
                this.patientHistoryDetail.maritual_history = "";
                this.patientHistoryDetail.childhood_psychological_history = "";
                this.patientHistoryDetail.physical_sexual_history = "";
                this.patientHistoryDetail.illness = "";
                this.patientHistoryDetail.operation = "";
                this.patientHistoryDetail.accidents = "";
                this.patientHistoryDetail.any_current_medication = "";
                this.patientHistoryDetail.previous_psychiatric_history = "";
                this.patientHistoryDetail.forensic_history = "";
                this.patientHistoryDetail.substance_history = "";
                this.patientHistoryDetail.premorbid_personality = "";
                this.patientHistoryDetail.appearance_and_behavior= "";


                this.patientHistoryDetail.sensorium= "";
                this.patientHistoryDetail.attention_and_concentration= "";
                this.patientHistoryDetail.orientation= "";
                this.patientHistoryDetail.perception= "";
                this.patientHistoryDetail.mood= "";
                this.patientHistoryDetail.memory= "";
                this.patientHistoryDetail.speech= "";
                this.patientHistoryDetail.thought= "";
                this.patientHistoryDetail.general_information_and_intelligence= "";
                this.patientHistoryDetail.insight_and_judjment= "";
                this.patientHistoryDetail.psychiatric_treatment= "";
                this.patientHistoryDetail.laboratory_and_radiological_examination= "";
                this.patientHistoryDetail.physical_examination_in_short= "";
                this.patientHistoryDetail.provisional_psychiatric_diagnosis= "";
                this.patientHistoryDetail.current_medical_treatment= "";
                this.patientHistoryDetail.referred_to_and_reason_for_referral= "";

            }})


   }


   saveResult():void{
    if (!this.canSubmit()){
      return;

    }console.log("*******************************","------------------------")
    this.diagnosisDetail = new DiagnosisDetail();
    this.diagnosisDetail.occurance =100
    this.diagnosisDetail.diagnosis_order =100
    this.diagnosisDetail.certainity =100
    this.diagnosisDetail.status =100
    this.diagnosisDetail.note =""
    this.diagnosisDetail.pertinent_investigation_finding =""
    this.diagnosisDetail.plan =""
    this.diagnosisDetail.written_by =100
    this.patientHistoryDetail.encounter_id = this.encounterId;
    if (this.patientHistoryDetail.id){
      this.onUpdate(this.encounterId)
    } else {
      this.onAdd(this.encounterId)
    }
    // if(this.icdCount<1){
    //   console.log("ppp--"+this.myControl.value,)
    //   let item :Icd11Detail= new Icd11Detail();
    //   item.category=this.myControl.value
    //   this.icd11Persist.addIcd11(item).subscribe((res)=>{
    //     console.log("***********************************************",res)
    //   })
    //   console.log("==============================----------------",item)

    // }
   }

   isDisabled(): boolean {
       return !(this.self && this.form_encounterPersist.encounter_is_active)
   }

   onKeyDown(event:string,input):void {
    console.log(event,input.value)
    if(event && event.length == 1 && !event.endsWith('\u2022'))
        input.value = "\u2022  " + event
      else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
        input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
      if (event && !input.value.endsWith("\n"))
      this.icd11Persist.icd11SearchText =input.value.split('\u2022').pop().trim();
      else
        this.icd11Persist.icd11SearchText = "";
      this.load_icd11Diagnois();
   }
   onBlur(){
    console.log("lll",this.patientHistoryDetail)
       if (this.patientHistoryDetail.id){
        this.persist.updatePatientHistory(this.patientHistoryDetail).subscribe(value => {
          this.tcNotification.success("patient_history updated");
          console.log("updated",this.patientHistoryDetail)
          this.queuesPersist.updateQueues({queue_status:"opened"},this.patientHistoryDetail.encounter_id).subscribe((res)=>{
            console.log("queues upateded",res)
           })
          // this.action(this.patientHistoryDetail)
        }, error => {
          this.isLoadingResults = false;
        })
       }
       else{
        this.diagnosisDetail = new DiagnosisDetail();
        this.diagnosisDetail.occurance =100
        this.diagnosisDetail.diagnosis_order =100
        this.diagnosisDetail.certainity =100
        this.diagnosisDetail.status =100
        this.diagnosisDetail.note =""
        this.diagnosisDetail.pertinent_investigation_finding =""
        this.diagnosisDetail.plan =""
        this.diagnosisDetail.written_by =100
        this.patientHistoryDetail.encounter_id = this.encounterId;
        this.transformDates()
        // this.isLoadingResults = true;
        const x = this.getDiagnosis(this.patientHistoryDetail.provisional_psychiatric_diagnosis)
        // for( let i of x){
          this.diagnosisDetail.name = x.join("_")
    
          this.diagnosisPersist.addDiagnosis(this.encounterId,this.diagnosisDetail).subscribe(result=>{
            console.log("happy",result)
         })
        // }
        console.log("=======---new  ---- new  ---",this.patientHistoryDetail.provisional_psychiatric_diagnosis)
        this.persist.addPatientHistory(this.patientHistoryDetail).subscribe(value => {
          this.tcNotification.success("patientHistory added");
          this.patientHistoryDetail.id = value.id;
          this.queuesPersist.updateQueues({queue_status:"opened"},this.patientHistoryDetail.encounter_id).subscribe((res)=>{
            console.log("queues upateded",res)
           })
          
        }, error => {
          this.isLoadingResults = false;
        })
       }
    
       
      }
   
 }
