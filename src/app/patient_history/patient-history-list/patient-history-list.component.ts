import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientHistorySummary, PatientHistorySummaryPartialList } from '../patient_history.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { PatientHistoryNavigator } from '../patient_history.navigator';
import { PatientHistoryPersist } from '../patient_history.persist';
@Component({
  selector: 'app-patient-history-list',
  templateUrl: './patient-history-list.component.html',
  styleUrls: ['./patient-history-list.component.scss']
})

export class PatientHistoryListComponent implements OnInit {
  patientHistorysData: PatientHistorySummary[] = [];
  patientHistorysTotalCount: number = 0;
  patientHistorySelectAll:boolean = false;
  patientHistorySelection: PatientHistorySummary[] = [];

 patientHistorysDisplayedColumns: string[] = ["select","action" ,"premorbid_personality" ];
  patientHistorySearchTextBox: FormControl = new FormControl();
  patientHistoryIsLoading: boolean = false;  
  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) patientHistorysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientHistorysSort: MatSort;  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientHistoryPersist: PatientHistoryPersist,
                public patientHistoryNavigator: PatientHistoryNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("patient_historys");
       this.patientHistorySearchTextBox.setValue(patientHistoryPersist.patientHistorySearchText);
      //delay subsequent keyup events
      this.patientHistorySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientHistoryPersist.patientHistorySearchText = value;
        this.searchPatient_historys();
      });
    } ngOnInit() {
   
      this.patientHistorysSort.sortChange.subscribe(() => {
        this.patientHistorysPaginator.pageIndex = 0;
        this.searchPatient_historys(true);
      });

      this.patientHistorysPaginator.page.subscribe(() => {
        this.searchPatient_historys(true);
      });
      //start by loading items
      this.searchPatient_historys();
    }

  searchPatient_historys(isPagination:boolean = false): void {


    let paginator = this.patientHistorysPaginator;
    let sorter = this.patientHistorysSort;

    this.patientHistoryIsLoading = true;
    this.patientHistorySelection = [];

    this.patientHistoryPersist.searchPatientHistory(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientHistorySummaryPartialList) => {
      this.patientHistorysData = partialList.data;
      if (partialList.total != -1) {
        this.patientHistorysTotalCount = partialList.total;
      }
      this.patientHistoryIsLoading = false;
    }, error => {
      this.patientHistoryIsLoading = false;
    });

  } downloadPatientHistorys(): void {
    if(this.patientHistorySelectAll){
         this.patientHistoryPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patient_history", true);
      });
    }
    else{
        this.patientHistoryPersist.download(this.tcUtilsArray.idsList(this.patientHistorySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download patient_history",true);
            });
        }
  }
addPatient_history(): void {
    let dialogRef = this.patientHistoryNavigator.addPatientHistory(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPatient_historys();
      }
    });
  }

  editPatientHistory(item: PatientHistorySummary) {
    let dialogRef = this.patientHistoryNavigator.editPatientHistory(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePatientHistory(item: PatientHistorySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("patient_history");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.patientHistoryPersist.deletePatientHistory(item.id).subscribe(response => {
          this.tcNotification.success("patient_history deleted");
          this.searchPatient_historys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}