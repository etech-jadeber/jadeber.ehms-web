import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientHistoryPickComponent } from './patient-history-pick.component';

describe('PatientHistoryPickComponent', () => {
  let component: PatientHistoryPickComponent;
  let fixture: ComponentFixture<PatientHistoryPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientHistoryPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientHistoryPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
