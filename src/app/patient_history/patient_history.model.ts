import { TCId } from '../tc/models';
export class PatientHistorySummary extends TCId {
  encounter_id: string;
  patient_id: string;
  chief_compliant: string;
  hpi: string;
  risk_assessment: string;
  family_history: string;
  personal_history: string;
  pregnancy_birth: string;
  mode_of_delivery: string;
  history_of_abortion: string;
  health_during_childhood: string;
  education: string;
  work: string;
  sexual_history: string;
  menustrial_history: string;
  maritual_history: string;
  childhood_psychological_history: string;
  physical_sexual_history: string;
  illness: string;
  operation: string;
  accidents: string;
  accidents_date: number;
  illness_date: number;
  operation_date: number;
  any_current_medication: string;
  previous_psychiatric_history: string;
  forensic_history: string;
  substance_history: string;
  premorbid_personality: string;
  date: number;
  appearance_and_behavior:string;

  sensorium:string;
  attention_and_concentration:string;
  orientation:string;
  perception:string;
  mood:string;
  memory:string;
  speech:string;
  thought:string;
  general_information_and_intelligence:string;
  insight_and_judjment:string;
  psychiatric_treatment:string;
  laboratory_and_radiological_examination:string;
  physical_examination_in_short:string;
  provisional_psychiatric_diagnosis:string;
  current_medical_treatment:string;
  referred_to_and_reason_for_referral:string;
  attention_given:string;


}
export class PatientHistorySummaryPartialList {
  data: PatientHistorySummary[];
  total: number;
}
export class PatientHistoryDetail extends PatientHistorySummary {}

export class PatientHistoryDashboard {
  total: number;
}
