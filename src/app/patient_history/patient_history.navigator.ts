import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { PatientHistoryEditComponent } from './patient-history-edit/patient-history-edit.component';
import { PatientHistoryPickComponent } from './patient-history-pick/patient-history-pick.component';

@Injectable({
  providedIn: 'root',
})
export class PatientHistoryNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  patientHistorysUrl(): string {
    return '/patient_historys';
  }

  patientHistoryUrl(id: string): string {
    return '/patient_historys/' + id;
  }

  viewPatientHistorys(): void {
    this.router.navigateByUrl(this.patientHistorysUrl());
  }

  viewPatientHistory(id: string): void {
    this.router.navigateByUrl(this.patientHistoryUrl(id));
  }

  editPatientHistory(id: string): MatDialogRef<PatientHistoryEditComponent> {
    const dialogRef = this.dialog.open(PatientHistoryEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addPatientHistory(encounter_id: string): MatDialogRef<PatientHistoryEditComponent> {
    const dialogRef = this.dialog.open(PatientHistoryEditComponent, {
      data: new TCIdMode(encounter_id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickPatientHistorys(
    selectOne: boolean = false
  ): MatDialogRef<PatientHistoryPickComponent> {
    const dialogRef = this.dialog.open(PatientHistoryPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
