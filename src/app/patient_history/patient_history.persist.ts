
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PatientHistoryDashboard, PatientHistoryDetail, PatientHistorySummaryPartialList} from "./patient_history.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PatientHistoryPersist {
 patientHistorySearchText: string = "";
 encounter_id: string;
constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patientHistorySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPatientHistory(pageSize: number, pageIndex: number, sort: string, order: string): Observable<PatientHistorySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patient_history", this.patientHistorySearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "encounter_id",this.encounter_id.toString());

    return this.http.get<PatientHistorySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_history/do", new TCDoParam("download_all", this.filters()));
  }

  patientHistoryDashboard(): Observable<PatientHistoryDashboard> {
    return this.http.get<PatientHistoryDashboard>(environment.tcApiBaseUri + "patient_history/dashboard");
  }

  getPatientHistory(id: string): Observable<PatientHistoryDetail> {
    return this.http.get<PatientHistoryDetail>(environment.tcApiBaseUri + "patient_history/" + id);
  }

  addPatientHistory(item: PatientHistoryDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_history/", item);
  }

  updatePatientHistory(item: PatientHistoryDetail): Observable<PatientHistoryDetail> {
    return this.http.patch<PatientHistoryDetail>(environment.tcApiBaseUri + "patient_history/" + item.id, item);
  }

  deletePatientHistory(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_history/" + id);
  }

  patientHistorysDo(method: string, payload: any): Observable<{}> {
    
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_history/do", new TCDoParam(method, payload));
  }

  patientHistoryDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_history/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_history/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_history/" + id + "/do", new TCDoParam("print", {}));
  }
 }
