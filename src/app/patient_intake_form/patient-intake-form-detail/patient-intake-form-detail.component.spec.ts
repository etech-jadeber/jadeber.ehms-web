import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientIntakeFormDetailComponent } from './patient-intake-form-detail.component';

describe('PatientIntakeFormDetailComponent', () => {
  let component: PatientIntakeFormDetailComponent;
  let fixture: ComponentFixture<PatientIntakeFormDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientIntakeFormDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientIntakeFormDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
