import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PatientIntakeFormDetail} from "../patient_intake_form.model";
import {PatientIntakeFormPersist} from "../patient_intake_form.persist";
import {PatientIntakeFormNavigator} from "../patient_intake_form.navigator";
import { DialysisSessionPersist } from 'src/app/dialysis_session/dialysis_session.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';


@Component({
  selector: 'app-patient_intake_form-detail',
  templateUrl: './patient-intake-form-detail.component.html',
  styleUrls: ['./patient-intake-form-detail.component.scss']
})

export class PatientIntakeFormDetailComponent implements OnInit {
  patientIntakeFormIsLoading:boolean = false;
  size:number = 0;
  patientIntakeFormDetail: PatientIntakeFormDetail;
  @Input() session_id:string;
  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public patientIntakeFormNavigator: PatientIntakeFormNavigator,
              public  patientIntakeFormPersist: PatientIntakeFormPersist,
              public dialysisSessionPersist:DialysisSessionPersist,
              ) {
    this.tcAuthorization.requireRead("patient_intake_forms");
    this.patientIntakeFormDetail = new PatientIntakeFormDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("patient_intake_forms");
    this.loadDetails();
  }

  loadDetails(): void {
  this.patientIntakeFormIsLoading = true;
  this.dialysisSessionPersist.getDialysisSession(this.session_id).subscribe((session)=>{
    if(session){
      this.patientIntakeFormPersist.searchPatientIntakeForm(session.session_id).subscribe((result)=>{
        if(result){
          this.size=result.data.length;
          this.patientIntakeFormDetail=result.data[0];
          this.patientIntakeFormIsLoading = false;
        }
      });
    }

  })
  
    
  }

  addPatientIntakeForm(): void {
    let dialogRef = this.patientIntakeFormNavigator.addPatientIntakeForm(this.session_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadDetails();
      }
    });
  }
  
  reload(){
    this.loadDetails();
  }
  editPatientIntakeForm(): void {
    let modalRef = this.patientIntakeFormNavigator.editPatientIntakeForm(this.patientIntakeFormDetail.id);
    modalRef.afterClosed().subscribe(patientIntakeFormDetail => {
      TCUtilsAngular.assign(this.patientIntakeFormDetail, patientIntakeFormDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.patientIntakeFormPersist.print(this.patientIntakeFormDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print patient_intake_form", true);
      });
    }

  back():void{
      this.location.back();
    }

}