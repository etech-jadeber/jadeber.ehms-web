import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientIntakeFormEditComponent } from './patient-intake-form-edit.component';

describe('PatientIntakeFormEditComponent', () => {
  let component: PatientIntakeFormEditComponent;
  let fixture: ComponentFixture<PatientIntakeFormEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientIntakeFormEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientIntakeFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
