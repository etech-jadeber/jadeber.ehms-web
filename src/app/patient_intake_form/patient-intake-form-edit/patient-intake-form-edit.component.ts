import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientIntakeFormDetail } from '../patient_intake_form.model';import { PatientIntakeFormPersist } from '../patient_intake_form.persist';
import { DialysisSessionPersist } from 'src/app/dialysis_session/dialysis_session.persist';
import { DialysisMachinePersist } from 'src/app/dialysis_machine/dialysis_machine.persist';
@Component({
  selector: 'app-patient_intake_form-edit',
  templateUrl: './patient-intake-form-edit.component.html',
  styleUrls: ['./patient-intake-form-edit.component.scss']
})
export class PatientIntakeFormEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  startingTime:string;
  finishingTime:string;
  patientIntakeFormDetail: PatientIntakeFormDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PatientIntakeFormEditComponent>,
              public  persist: PatientIntakeFormPersist,
              
              public tcUtilsDate: TCUtilsDate,

              public dialysisSessionPersist:DialysisSessionPersist,
              public dialysisMachinePersist: DialysisMachinePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_intake_forms");
      this.title = this.appTranslation.getText("general","new") +  " " + "patient_intake_form";
      this.patientIntakeFormDetail = new PatientIntakeFormDetail();
      this.dialysisSessionPersist.getDialysisSession(this.idMode.id).subscribe((result)=>{
        if(result){
          this.patientIntakeFormDetail.session_id=result.session_id;
          this.dialysisMachinePersist.getDialysisMachine(result.machine_no).subscribe((res)=>{
            if(result){
              this.patientIntakeFormDetail.dialysis_machine_no=res.machine_no;
            }
          })
          
        }
      })
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_intake_forms");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "patient_intake_form";
      this.isLoadingResults = true;
      this.persist.getPatientIntakeForm(this.idMode.id).subscribe(patientIntakeFormDetail => {
        this.patientIntakeFormDetail = patientIntakeFormDetail;
        this.transformTime(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.transformTime();
   this.patientIntakeFormDetail
    this.persist.addPatientIntakeForm(this.patientIntakeFormDetail).subscribe(value => {
      this.tcNotification.success("patientIntakeForm added");
      this.patientIntakeFormDetail.id = value.id;
      this.dialogRef.close(this.patientIntakeFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   
    this.transformTime();
    this.persist.updatePatientIntakeForm(this.patientIntakeFormDetail).subscribe(value => {
      this.tcNotification.success("patient_intake_form updated");
      this.dialogRef.close(this.patientIntakeFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  transformTime(isNew:boolean=true):void{
    if(isNew){
      let sTime=this.startingTime.split(':');
      let fTime=this.finishingTime.split(':');
      this.patientIntakeFormDetail.starting_time=parseInt(sTime[0])*3600+ parseInt(sTime[1])*60;
      this.patientIntakeFormDetail.finishing_time=parseInt(fTime[0])*3600+ parseInt(fTime[1])*60;
      let duration=this.tcUtilsDate.toDate( this.patientIntakeFormDetail.finishing_time-this.patientIntakeFormDetail.starting_time);
      this.patientIntakeFormDetail.duration_of_dialysis=this.formate(duration.getHours()-3)+':'+this.formate(duration.getMinutes());
    }
    else{
      this.startingTime=this.formate(Math.floor(this.patientIntakeFormDetail.starting_time/3600))+":"+this.formate((this.patientIntakeFormDetail.starting_time/60)%60);
      this.finishingTime=this.formate(Math.floor(this.patientIntakeFormDetail.finishing_time/3600))+":"+this.formate((this.patientIntakeFormDetail.finishing_time/60)%60);
    }
  }

  formate(num):string{
    let value=num.toString();
    if(value.length==1){
      value='0'+value;
    }
    return value;
  }
  
  canSubmit():boolean{
        if (this.patientIntakeFormDetail == null){
            return false;
          }

if (this.patientIntakeFormDetail.blood_groop == null || this.patientIntakeFormDetail.blood_groop  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.dry_weight == null) {
            return false;
        }
if (this.patientIntakeFormDetail.dialyzer_type == null || this.patientIntakeFormDetail.dialyzer_type  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.dialysate_no == null || this.patientIntakeFormDetail.dialysate_no  == "") {
            return false;
        } 

if (this.patientIntakeFormDetail.blood_flow == null || this.patientIntakeFormDetail.blood_flow  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.uf_volume == null || this.patientIntakeFormDetail.uf_volume  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.heparin_intial == null || this.patientIntakeFormDetail.heparin_intial  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.during_dialysis == null || this.patientIntakeFormDetail.during_dialysis  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.dialysate_fluid == null || this.patientIntakeFormDetail.dialysate_fluid  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.bicarbonate_acetate == null || this.patientIntakeFormDetail.bicarbonate_acetate  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.weight_before_hd == null) {
            return false;
        }
if (this.patientIntakeFormDetail.bp_before_hd == null) {
            return false;
        }
if (this.patientIntakeFormDetail.pulse_before_hd == null) {
            return false;
        }
if (this.patientIntakeFormDetail.weight_after_hd == null) {
            return false;
        }
if (this.patientIntakeFormDetail.bp_after_hd == null) {
            return false;
        }
if (this.patientIntakeFormDetail.pulse_after_hd == null) {
            return false;
        }
if (this.startingTime == null) {
            return false;
        }
if (this.finishingTime == null) {
            return false;
        }
if (this.patientIntakeFormDetail.iv_fluid == null || this.patientIntakeFormDetail.iv_fluid  == "") {
            return false;
        } 
if (this.patientIntakeFormDetail.other == null || this.patientIntakeFormDetail.other  == "") {
            return false;
        } 
 return true;

 }
 }