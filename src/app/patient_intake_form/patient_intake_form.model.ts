import {TCId} from "../tc/models";export class PatientIntakeFormSummary extends TCId {
    blood_groop:string;
    dry_weight:number;
    session_id:number;
    dialysis_machine_no:string;
    dialyzer_type:string;
    dialysate_no:string;
    duration_of_dialysis:string;
    blood_flow:string;
    uf_volume:string;
    heparin_intial:string;
    during_dialysis:string;
    dialysate_fluid:string;
    bicarbonate_acetate:string;
    weight_before_hd:number;
    bp_before_hd:number;
    pulse_before_hd:number;
    weight_after_hd:number;
    bp_after_hd:number;
    pulse_after_hd:number;
    starting_time:number;
    finishing_time:number;
    iv_fluid:string;
    other:string;
     
    }
    export class PatientIntakeFormSummaryPartialList {
      data: PatientIntakeFormSummary[];
      total: number;
    }
    export class PatientIntakeFormDetail extends PatientIntakeFormSummary {
    }
    
    export class PatientIntakeFormDashboard {
      total: number;
    }