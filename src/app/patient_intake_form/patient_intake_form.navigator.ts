import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PatientIntakeFormEditComponent} from "./patient-intake-form-edit/patient-intake-form-edit.component";


@Injectable({
  providedIn: 'root'
})

export class PatientIntakeFormNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  patientIntakeFormsUrl(): string {
    return "/patient_intake_forms";
  }

  patientIntakeFormUrl(id: string): string {
    return "/patient_intake_forms/" + id;
  }

  viewPatientIntakeForms(): void {
    this.router.navigateByUrl(this.patientIntakeFormsUrl());
  }

  viewPatientIntakeForm(id: string): void {
    this.router.navigateByUrl(this.patientIntakeFormUrl(id));
  }

  editPatientIntakeForm(id: string): MatDialogRef<PatientIntakeFormEditComponent> {
    const dialogRef = this.dialog.open(PatientIntakeFormEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addPatientIntakeForm(session_id:string): MatDialogRef<PatientIntakeFormEditComponent> {
    const dialogRef = this.dialog.open(PatientIntakeFormEditComponent, {
          data: new TCIdMode(session_id, TCModalModes.NEW),
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
}