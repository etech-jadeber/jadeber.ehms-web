import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PatientIntakeFormDashboard, PatientIntakeFormDetail, PatientIntakeFormSummaryPartialList} from "./patient_intake_form.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PatientIntakeFormPersist {
 patientIntakeFormSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patientIntakeFormSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPatientIntakeForm(session_id:number): Observable<PatientIntakeFormSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patient_intake_form", this.patientIntakeFormSearchText, 1, 0, "", "asc");
    url=TCUtilsString.appendUrlParameter(url,"session_id",session_id.toString());
    return this.http.get<PatientIntakeFormSummaryPartialList>(url);

  } 
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_intake_form/do", new TCDoParam("download_all", this.filters()));
  }

  patientIntakeFormDashboard(): Observable<PatientIntakeFormDashboard> {
    return this.http.get<PatientIntakeFormDashboard>(environment.tcApiBaseUri + "patient_intake_form/dashboard");
  }

  getPatientIntakeForm(id: string): Observable<PatientIntakeFormDetail> {
    return this.http.get<PatientIntakeFormDetail>(environment.tcApiBaseUri + "patient_intake_form/" + id);
  } addPatientIntakeForm(item: PatientIntakeFormDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_intake_form/", item);
  }

  updatePatientIntakeForm(item: PatientIntakeFormDetail): Observable<PatientIntakeFormDetail> {
    return this.http.patch<PatientIntakeFormDetail>(environment.tcApiBaseUri + "patient_intake_form/" + item.id, item);
  }

  deletePatientIntakeForm(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_intake_form/" + id);
  }
 patientIntakeFormsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_intake_form/do", new TCDoParam(method, payload));
  }

  patientIntakeFormDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_intake_forms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_intake_form/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patient_intake_form/" + id + "/do", new TCDoParam("print", {}));
  }


}