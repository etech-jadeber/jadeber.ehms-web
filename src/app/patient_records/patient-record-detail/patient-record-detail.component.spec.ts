import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientRecordDetailComponent } from './patient-record-detail.component';

describe('PatientRecordDetailComponent', () => {
  let component: PatientRecordDetailComponent;
  let fixture: ComponentFixture<PatientRecordDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRecordDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRecordDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
