import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientRecordEditComponent } from './patient-record-edit.component';

describe('PatientRecordEditComponent', () => {
  let component: PatientRecordEditComponent;
  let fixture: ComponentFixture<PatientRecordEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRecordEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRecordEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
