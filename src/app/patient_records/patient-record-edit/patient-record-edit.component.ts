import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Patient_RecordDetail} from "../patient_record.model";
import {Patient_RecordPersist} from "../patient_record.persist";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-patient_record-edit',
  templateUrl: './patient-record-edit.component.html',
  styleUrls: ['./patient-record-edit.component.css']
})
export class Patient_RecordEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patient_recordDetail: Patient_RecordDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Patient_RecordEditComponent>,
              public  persist: Patient_RecordPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_records");
      this.title = this.appTranslation.getText("general","new") +  " Patient_Record";
      this.patient_recordDetail = new Patient_RecordDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_records");
      this.title = this.appTranslation.getText("general","edit") +  " Patient_Record";
      this.isLoadingResults = true;
      this.persist.getPatient_Record(this.idMode.id).subscribe(patient_recordDetail => {
        this.patient_recordDetail = patient_recordDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addPatient_Record(this.idMode.id, this.patient_recordDetail).subscribe(value => {
      this.tcNotification.success("Patient_Record added");
      this.patient_recordDetail.id = value.id;
      this.dialogRef.close(this.patient_recordDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updatePatient_Record(this.patient_recordDetail).subscribe(value => {
      this.tcNotification.success("Patient_Record updated");
      this.dialogRef.close(this.patient_recordDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.patient_recordDetail == null){
            return false;
          }

        if (this.patient_recordDetail.record == null || this.patient_recordDetail.record  == "") {
                      return false;
                    }


        return true;
      }


}
