import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { Patient_RecordDetail, Patient_RecordSummary, Patient_RecordSummaryPartialList } from '../patient_record.model';
import { Patient_RecordNavigator } from '../patient_record.navigator';
import { Patient_RecordPersist } from '../patient_record.persist';
import {debounceTime} from "rxjs/operators";
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsArray } from 'src/app/tc/utils-array';

@Component({
  selector: 'app-patient-record-list',
  templateUrl: './patient-record-list.component.html',
  styleUrls: ['./patient-record-list.component.css']
})
export class PatientRecordListComponent implements OnInit {

  patient_recordsIsLoading: boolean = false;
  patient_recordsSelection: Patient_RecordDetail[] = [];
  patient_recordsData: Patient_RecordDetail[] = [];
  patient_recordsSelectAll:boolean = false;
  patient_recordsTotalCount : number = 0;
  patient_recordsDisplayedColumns: string[] = [
    'select',
    'action',
    'record_date',
    'record',
  ];
  patient_recordsSearchTextBox: FormControl = new FormControl()

  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public patient_recordPersist: Patient_RecordPersist,
    public patient_recordNavigator: Patient_RecordNavigator,
    public tcUtilsAngular: TCUtilsAngular,
    public tcNavigator: TCNavigator,
    public tcNotification: TCNotification,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
  ) {
    this.patient_recordsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.patient_recordPersist.patient_recordSearchText = value;
      this.searchPatient_Records();
    });
   }

   ngOnInit(): void {
     
   }

  ngAfterViewInit() {
    // patient_procedures sorter
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchPatient_Records(true);
    });
  // patient_procedures paginator
  this.paginator.page.subscribe(() => {
      this.searchPatient_Records(true);
    });
    this.searchPatient_Records();
  }

  searchPatient_Records(isPagination: boolean = false): void {
    let paginator = this.paginator;
    let sorter = this.sorter;

    this.patient_recordsIsLoading = true;
    this.patient_recordsSelection = [];
    

    this.patient_recordPersist
      .searchPatient_Record(
        this.encounterId,
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: Patient_RecordSummaryPartialList) => {
          this.patient_recordsData = partialList.data;
          if (partialList.total != -1) {
            this.patient_recordsTotalCount = partialList.total;
          }
          this.patient_recordsIsLoading = false;
        },
        (error) => {
          this.patient_recordsIsLoading = false;
        }
      );
  }

  addPatient_Record(id: string): void {
    let dialogRef = this.patient_recordNavigator.addPatient_Record(id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchPatient_Records();
      }
    });
  }

  editPatient_Record(item: Patient_RecordSummary) {
    let dialogRef = this.patient_recordNavigator.editPatient_Record(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deletePatient_Record(item: Patient_RecordSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Patient_Record');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.patient_recordPersist.deletePatient_Record(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Patient_Record deleted');
            this.searchPatient_Records();
          },
          (error) => {}
        );
      }
    });
  }

}
