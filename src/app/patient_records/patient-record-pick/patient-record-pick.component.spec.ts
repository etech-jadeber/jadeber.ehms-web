import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientRecordPickComponent } from './patient-record-pick.component';

describe('PatientRecordPickComponent', () => {
  let component: PatientRecordPickComponent;
  let fixture: ComponentFixture<PatientRecordPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientRecordPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientRecordPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
