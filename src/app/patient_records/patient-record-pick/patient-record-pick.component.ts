import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Patient_RecordDetail, Patient_RecordSummary, Patient_RecordSummaryPartialList} from "../patient_record.model";
import {Patient_RecordPersist} from "../patient_record.persist";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TCUtilsString } from 'src/app/tc/utils-string';


@Component({
  selector: 'app-patient_record-pick',
  templateUrl: './patient-record-pick.component.html',
  styleUrls: ['./patient-record-pick.component.css']
})
export class Patient_RecordPickComponent implements OnInit {

  patient_recordsData: Patient_RecordSummary[] = [];
  patient_recordsTotalCount: number = 0;
  patient_recordsSelection: Patient_RecordSummary[] = [];
  patient_recordsDisplayedColumns: string[] = ["select", 'record_date','record','encounter_id' ];

  patient_recordsSearchTextBox: FormControl = new FormControl();
  patient_recordsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) patient_recordsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patient_recordsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public patient_recordPersist: Patient_RecordPersist,
              public dialogRef: MatDialogRef<Patient_RecordPickComponent>,
              public tcUtilsString: TCUtilsString,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("patient_records");
    this.patient_recordsSearchTextBox.setValue(patient_recordPersist.patient_recordSearchText);
    //delay subsequent keyup events
    this.patient_recordsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.patient_recordPersist.patient_recordSearchText = value;
      this.searchPatient_Records();
    });
  }

  ngOnInit() {

    this.patient_recordsSort.sortChange.subscribe(() => {
      this.patient_recordsPaginator.pageIndex = 0;
      this.searchPatient_Records();
    });

    this.patient_recordsPaginator.page.subscribe(() => {
      this.searchPatient_Records();
    });

    //set initial picker list to 5
    this.patient_recordsPaginator.pageSize = 5;

    //start by loading items
    this.searchPatient_Records();
  }

  searchPatient_Records(): void {
    this.patient_recordsIsLoading = true;
    this.patient_recordsSelection = [];

    this.patient_recordPersist.searchPatient_Record(this.tcUtilsString.invalid_id, this.patient_recordsPaginator.pageSize,
        this.patient_recordsPaginator.pageIndex,
        this.patient_recordsSort.active,
        this.patient_recordsSort.direction).subscribe((partialList: Patient_RecordSummaryPartialList) => {
      this.patient_recordsData = partialList.data;
      if (partialList.total != -1) {
        this.patient_recordsTotalCount = partialList.total;
      }
      this.patient_recordsIsLoading = false;
    }, error => {
      this.patient_recordsIsLoading = false;
    });

  }

  markOneItem(item: Patient_RecordSummary) {
    if(!this.tcUtilsArray.containsId(this.patient_recordsSelection,item.id)){
          this.patient_recordsSelection = [];
          this.patient_recordsSelection.push(item);
        }
        else{
          this.patient_recordsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.patient_recordsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
