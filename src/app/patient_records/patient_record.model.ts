import {TCId} from "../tc/models";

export class Patient_RecordSummary extends TCId {
  record_date : number;
record : string;
encounter_id : string;
}

export class Patient_RecordSummaryPartialList {
  data: Patient_RecordSummary[];
  total: number;
}

export class Patient_RecordDetail extends Patient_RecordSummary {
  record_date : number;
record : string;
encounter_id : string;
}

export class Patient_RecordDashboard {
  total: number;
}
