import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Patient_RecordEditComponent} from "./patient-record-edit/patient-record-edit.component";
import {Patient_RecordPickComponent} from "./patient-record-pick/patient-record-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Patient_RecordNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  patient_recordsUrl(): string {
    return "/patient_records";
  }

  patient_recordUrl(id: string): string {
    return "/patient_records/" + id;
  }

  viewPatient_Records(): void {
    this.router.navigateByUrl(this.patient_recordsUrl());
  }

  viewPatient_Record(id: string): void {
    this.router.navigateByUrl(this.patient_recordUrl(id));
  }

  editPatient_Record(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Patient_RecordEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPatient_Record(id:string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Patient_RecordEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPatient_Records(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Patient_RecordPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
