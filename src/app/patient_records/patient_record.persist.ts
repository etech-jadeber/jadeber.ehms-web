import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Patient_RecordDashboard, Patient_RecordDetail, Patient_RecordSummaryPartialList} from "./patient_record.model";


@Injectable({
  providedIn: 'root'
})
export class Patient_RecordPersist {

  patient_recordSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchPatient_Record(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Patient_RecordSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/patient_records", this.patient_recordSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Patient_RecordSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patient_recordSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_records/do", new TCDoParam("download_all", this.filters()));
  }

  patient_recordDashboard(): Observable<Patient_RecordDashboard> {
    return this.http.get<Patient_RecordDashboard>(environment.tcApiBaseUri + "patient_records/dashboard");
  }

  getPatient_Record(id: string): Observable<Patient_RecordDetail> {
    return this.http.get<Patient_RecordDetail>(environment.tcApiBaseUri + "patient_records/" + id);
  }

  addPatient_Record(id: string,item: Patient_RecordDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/" + id + "/patient_records", item);
  }

  updatePatient_Record(item: Patient_RecordDetail): Observable<Patient_RecordDetail> {
    return this.http.patch<Patient_RecordDetail>(environment.tcApiBaseUri + "patient_records/" + item.id, item);
  }

  deletePatient_Record(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_records/" + id);
  }

  patient_recordsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_records/do", new TCDoParam(method, payload));
  }

  patient_recordDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_records/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_records/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patient_records/" + id + "/do", new TCDoParam("print", {}));
  }


}
