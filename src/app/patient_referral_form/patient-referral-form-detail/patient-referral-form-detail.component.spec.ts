import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralFormDetailComponent } from './patient-referral-form-detail.component';

describe('PatientReferralFormDetailComponent', () => {
  let component: PatientReferralFormDetailComponent;
  let fixture: ComponentFixture<PatientReferralFormDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralFormDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralFormDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
