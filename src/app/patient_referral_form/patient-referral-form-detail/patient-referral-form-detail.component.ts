import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PatientReferralFormDetail} from "../patient_referral_form.model";
import {PatientReferralFormPersist} from "../patient_referral_form.persist";
import {PatientReferralFormNavigator} from "../patient_referral_form.navigator";
import { PatientPersist } from 'src/app/patients/patients.persist';

export enum PatientReferralFormTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-patient_referral_form-detail',
  templateUrl: './patient-referral-form-detail.component.html',
  styleUrls: ['./patient-referral-form-detail.component.css']
})
export class PatientReferralFormDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  patientReferralFormIsLoading:boolean = false;
  
  PatientReferralFormTabs: typeof PatientReferralFormTabs = PatientReferralFormTabs;
  activeTab: PatientReferralFormTabs = PatientReferralFormTabs.overview;
  //basics
  patientReferralFormDetail: PatientReferralFormDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public patientReferralFormNavigator: PatientReferralFormNavigator,
              public  patientReferralFormPersist: PatientReferralFormPersist,
              public patientPersist: PatientPersist
              ) {
    this.tcAuthorization.requireRead("patient_referrals");
    this.patientReferralFormDetail = new PatientReferralFormDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("patient_referrals");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');

      this.loadDetails(id);

    });
   }

  loadPatient(id:string):void{
      this.patientPersist.getPatient(id).subscribe(result=>{
        this.patientReferralFormDetail.patient = result.fname+ " "+ result.mname + " " + result.lname
      })
  } 

  loadDetails(id: string): void {
  this.patientReferralFormIsLoading = true;
    this.patientReferralFormPersist.getPatientReferralForm(id).subscribe(patientReferralFormDetail => {
          this.patientReferralFormDetail = patientReferralFormDetail;
          this.patientReferralFormIsLoading = false;
          this.loadPatient(patientReferralFormDetail.patient)
        }, error => {
          console.error(error);
          this.patientReferralFormIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.patientReferralFormDetail.id);
  }
  editPatientReferralForm(): void {
    let modalRef = this.patientReferralFormNavigator.editPatientReferralForm(this.patientReferralFormDetail.id);
    modalRef.afterClosed().subscribe(patientReferralFormDetail => {
      TCUtilsAngular.assign(this.patientReferralFormDetail, patientReferralFormDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.patientReferralFormPersist.print(this.patientReferralFormDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print patient_referral_form", true);
      });
    }

  back():void{
      this.location.back();
    }

}