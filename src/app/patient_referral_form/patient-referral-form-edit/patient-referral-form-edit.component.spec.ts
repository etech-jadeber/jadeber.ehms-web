import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralFormEditComponent } from './patient-referral-form-edit.component';

describe('PatientReferralFormEditComponent', () => {
  let component: PatientReferralFormEditComponent;
  let fixture: ComponentFixture<PatientReferralFormEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralFormEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralFormEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
