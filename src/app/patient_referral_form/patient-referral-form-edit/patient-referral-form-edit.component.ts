import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientReferralFormDetail } from '../patient_referral_form.model';import { PatientReferralFormPersist } from '../patient_referral_form.persist';import { PatientHistoryDetail } from 'src/app/patient_history/patient_history.model';
import { PatientHistoryPersist } from 'src/app/patient_history/patient_history.persist';
@Component({
  selector: 'app-patient_referral_form-edit',
  templateUrl: './patient-referral-form-edit.component.html',
  styleUrls: ['./patient-referral-form-edit.component.css']
})export class PatientReferralFormEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientReferralFormDetail: PatientReferralFormDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PatientReferralFormEditComponent>,
              public  persist: PatientReferralFormPersist,
              public  patientHistoryPersist: PatientHistoryPersist,

              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_referrals");
      this.title = this.appTranslation.getText("general","new") +  " " + "patient_referral_form";
      this.patientReferralFormDetail = new PatientReferralFormDetail();
      this.loadPatientHistory()
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_referrals");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "patient_referral_form";
      this.isLoadingResults = true;
      this.persist.getPatientReferralForm(this.idMode.id).subscribe(patientReferralFormDetail => {
        this.patientReferralFormDetail = patientReferralFormDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    console.log("this.patientReferralFormDetail",this.patientReferralFormDetail)
    this.persist.addPatientReferralForm(this.idMode.id,this.patientReferralFormDetail).subscribe(value => {
      this.tcNotification.success("patientReferralForm added");
      this.patientReferralFormDetail.id = value.id;
      this.dialogRef.close(this.patientReferralFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  loadPatientHistory(){
    this.patientHistoryPersist.patientHistorysDo("find_by_encounter",{encounter_id:this.idMode.id}).subscribe((result:PatientHistoryDetail)=>{
      console.log("result",result)
      this.patientReferralFormDetail.diagnosis = result.provisional_psychiatric_diagnosis || ""
      this.patientReferralFormDetail.hpi = result.hpi || ""
      this.patientReferralFormDetail.past_psychiatric_history = result.previous_psychiatric_history || ""
      this.patientReferralFormDetail.family_psychiatric_history = result.family_history || ""


      
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePatientReferralForm(this.patientReferralFormDetail).subscribe(value => {
      this.tcNotification.success("patient_referral_form updated");
      this.dialogRef.close(this.patientReferralFormDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.patientReferralFormDetail == null){
            return false;
          }

// if (this.patientReferralFormDetail.from_hospital == null || this.patientReferralFormDetail.from_hospital  == "") {
//             return false;
//         } 
if (this.patientReferralFormDetail.to_hospital == null || this.patientReferralFormDetail.to_hospital  == "") {
            return false;
        } 
// if (this.patientReferralFormDetail.clinical_findings == null || this.patientReferralFormDetail.clinical_findings  == "") {
//             return false;
//         } 
// if (this.patientReferralFormDetail.diagnosis == null || this.patientReferralFormDetail.diagnosis  == "") {
//             return false;
//         } 
// if (this.patientReferralFormDetail.investigation_results == null || this.patientReferralFormDetail.investigation_results  == "") {
//             return false;
//         } 
// if (this.patientReferralFormDetail.treatment_given == null || this.patientReferralFormDetail.treatment_given  == "") {
//             return false;
//         } 
if (this.patientReferralFormDetail.reason_for_referral == null || this.patientReferralFormDetail.reason_for_referral  == "") {
            return false;
        } 
// if (this.patientReferralFormDetail.referring_physician == null || this.patientReferralFormDetail.referring_physician  == "") {
//             return false;
//         } 
// if (this.patientReferralFormDetail.phone_no == null || this.patientReferralFormDetail.phone_no  == "") {
//             return false;
//         } 
        return true;
 }
 }