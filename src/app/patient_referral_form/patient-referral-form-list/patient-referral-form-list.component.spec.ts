import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralFormListComponent } from './patient-referral-form-list.component';

describe('PatientReferralFormListComponent', () => {
  let component: PatientReferralFormListComponent;
  let fixture: ComponentFixture<PatientReferralFormListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralFormListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
