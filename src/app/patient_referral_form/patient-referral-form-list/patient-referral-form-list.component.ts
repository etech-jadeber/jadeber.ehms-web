import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientReferralFormDetail, PatientReferralFormSummary, PatientReferralFormSummaryPartialList } from '../patient_referral_form.model';
import { PatientReferralFormPersist } from '../patient_referral_form.persist';
import { PatientReferralFormNavigator } from '../patient_referral_form.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { TCId } from 'src/app/tc/models';
@Component({
  selector: 'app-patient_referral_form-list',
  templateUrl: './patient-referral-form-list.component.html',
  styleUrls: ['./patient-referral-form-list.component.css']
})export class PatientReferralFormListComponent implements OnInit {
  patientReferralFormsData: PatientReferralFormSummary[] = [];
  patientReferralFormsTotalCount: number = 0;
  patientReferralFormSelectAll:boolean = false;
  patientReferralFormSelection: PatientReferralFormSummary[] = [];

  @Input() encounterId: any;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>()
 patientReferralFormsDisplayedColumns: string[] = ["select","action", "date", "to_hospital","reason_for_referral","referring_physician", ];
 doctors: {[id: string]: UserDetail } = {}
  patientReferralFormSearchTextBox: FormControl = new FormControl();
  patientReferralFormIsLoading: boolean = false;  
  success_state = 3;

  @ViewChild(MatPaginator, {static: true}) patientReferralFormsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientReferralFormsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientReferralFormPersist: PatientReferralFormPersist,
                public patientReferralFormNavigator: PatientReferralFormNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public form_encounterPersist: Form_EncounterPersist,
                public userPersist: UserPersist,

    ) {

        this.tcAuthorization.requireRead("patient_referrals");
       this.patientReferralFormSearchTextBox.setValue(patientReferralFormPersist.patientReferralFormSearchText);
      //delay subsequent keyup events
      this.patientReferralFormSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientReferralFormPersist.patientReferralFormSearchText = value;
        this.searchPatientReferralForms();
      });
    } ngOnInit() {
this.patientReferralFormPersist.patientId = this.patientId
      
      this.patientReferralFormsSort.sortChange.subscribe(() => {
        this.patientReferralFormsPaginator.pageIndex = 0;
        this.searchPatientReferralForms(true);
      });

      this.patientReferralFormsPaginator.page.subscribe(() => {
        this.searchPatientReferralForms(true);
      });
      //start by loading items
      this.searchPatientReferralForms();
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
      this.patientReferralFormPersist.encounterId = this.encounterId;
      } else {
      this.patientReferralFormPersist.encounterId = null;
      }
      this.searchPatientReferralForms()
      }

  searchPatientReferralForms(isPagination:boolean = false): void {
    if (this.encounterId == undefined){
      this.encounterId = TCUtilsString.getInvalidId();
    }

    let paginator = this.patientReferralFormsPaginator;
    let sorter = this.patientReferralFormsSort;

    this.patientReferralFormIsLoading = true;
    this.patientReferralFormSelection = [];

    this.patientReferralFormPersist.searchPatientReferralForm( paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active,sorter.direction ).subscribe((partialList: PatientReferralFormSummaryPartialList) => {
      this.patientReferralFormsData = partialList.data;
      this.patientReferralFormsData.forEach(
        referral => {
          if (!this.doctors[referral.referring_physician]){
            this.doctors[referral.referring_physician] = new UserDetail()
            this.userPersist.getUser(referral.referring_physician).subscribe(
              doctor => {
                this.doctors[referral.referring_physician] = doctor;
              }
            )
          }
        }
      )
      if (partialList.total != -1) {
        this.patientReferralFormsTotalCount = partialList.total;
      }
      this.patientReferralFormIsLoading = false;
    }, error => {
      this.patientReferralFormIsLoading = false;
    });

  } downloadPatientReferralForms(): void {
    if(this.patientReferralFormSelectAll){
         this.patientReferralFormPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patient_referral_form", true);
      });
    }
    else{
        this.patientReferralFormPersist.download(this.tcUtilsArray.idsList(this.patientReferralFormSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download patient_referral_form",true);
            });
        }
  }
addPatientReferralForm(): void {
    let dialogRef = this.patientReferralFormNavigator.addPatientReferralForm(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPatientReferralForms();
      }
    });
  }

  editPatientReferralForm(item: PatientReferralFormSummary) {
    let dialogRef = this.patientReferralFormNavigator.editPatientReferralForm(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePatientReferralForm(item: PatientReferralFormSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("patient_referral_form");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.patientReferralFormPersist.deletePatientReferralForm(item.id).subscribe(response => {
          this.tcNotification.success("patient_referral_form deleted");
          this.searchPatientReferralForms();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getDoctor(id: string){
      return this.doctors[id] ? `${this.doctors[id]?.name}` : ""
    }
    printReferral(patientReferralFormDetail: PatientReferralFormDetail, isSelected: boolean = false) {
      let bulk_prints;
      if(isSelected){
        bulk_prints = this.patientReferralFormSelection.map(data => data.id).join(',')
      } else {
        bulk_prints = patientReferralFormDetail.id
      }
      this.patientReferralFormPersist.patientReferralFormsDo( this.encounterId, 'print_referral', {bulk_prints} ).subscribe(
          (downloadJob: TCId) => {
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              this.jobPersist.followJob( downloadJob.id,'printing prescription', true);
              if (job.job_state == this.success_state) {
                // item.print_status = PrintStatus.printed
                // this.printRequestPersist.updatePrintRequest(item)
              }
            });
          },
          (error) => {}
        );
    }

}