import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReferralFormPickComponent } from './patient-referral-form-pick.component';

describe('PatientReferralFormPickComponent', () => {
  let component: PatientReferralFormPickComponent;
  let fixture: ComponentFixture<PatientReferralFormPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReferralFormPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReferralFormPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
