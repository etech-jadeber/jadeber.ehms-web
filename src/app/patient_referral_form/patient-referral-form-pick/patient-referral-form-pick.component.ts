import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSort} from "@angular/material/sort"
import {MatPaginator} from "@angular/material/paginator"
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientReferralFormSummary, PatientReferralFormSummaryPartialList } from '../patient_referral_form.model';
import { PatientReferralFormPersist } from '../patient_referral_form.persist';
import { PatientReferralFormNavigator } from '../patient_referral_form.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-patient_referral_form-pick',
  templateUrl: './patient-referral-form-pick.component.html',
  styleUrls: ['./patient-referral-form-pick.component.css']
})export class PatientReferralFormPickComponent implements OnInit {
  patientReferralFormsData: PatientReferralFormSummary[] = [];
  patientReferralFormsTotalCount: number = 0;
  patientReferralFormSelectAll:boolean = false;
  patientReferralFormSelection: PatientReferralFormSummary[] = [];

 patientReferralFormsDisplayedColumns: string[] = ["select", ,"from","to","patient","clinical_findings","diagnosis","investigation_results","treatment_given","reason_for_referral","referring physician","phpne_no" ];
  patientReferralFormSearchTextBox: FormControl = new FormControl();
  patientReferralFormIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) patientReferralFormsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientReferralFormsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientReferralFormPersist: PatientReferralFormPersist,
                public patientReferralFormNavigator: PatientReferralFormNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PatientReferralFormPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {
   
        this.tcAuthorization.requireRead("patient_referrals");
       this.patientReferralFormSearchTextBox.setValue(patientReferralFormPersist.patientReferralFormSearchText);
      //delay subsequent keyup events
      this.patientReferralFormSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientReferralFormPersist.patientReferralFormSearchText = value;
        this.searchPatient_referral_forms();
      });
    } ngOnInit() {
   
      this.patientReferralFormsSort.sortChange.subscribe(() => {
        this.patientReferralFormsPaginator.pageIndex = 0;
        this.searchPatient_referral_forms(true);
      });

      this.patientReferralFormsPaginator.page.subscribe(() => {
        this.searchPatient_referral_forms(true);
      });
      //start by loading items
      this.searchPatient_referral_forms();
    }

  searchPatient_referral_forms(isPagination:boolean = false): void {


    let paginator = this.patientReferralFormsPaginator;
    let sorter = this.patientReferralFormsSort;

    this.patientReferralFormIsLoading = true;
    this.patientReferralFormSelection = [];

    this.patientReferralFormPersist.searchPatientReferralForm(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientReferralFormSummaryPartialList) => {
      this.patientReferralFormsData = partialList.data;
      if (partialList.total != -1) {
        this.patientReferralFormsTotalCount = partialList.total;
      }
      this.patientReferralFormIsLoading = false;
    }, error => {
      this.patientReferralFormIsLoading = false;
    });

  }
  markOneItem(item: PatientReferralFormSummary) {
    if(!this.tcUtilsArray.containsId(this.patientReferralFormSelection,item.id)){
          this.patientReferralFormSelection = [];
          this.patientReferralFormSelection.push(item);
        }
        else{
          this.patientReferralFormSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.patientReferralFormSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }