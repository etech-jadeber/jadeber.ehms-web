import {TCId} from "../tc/models";export class PatientReferralFormSummary extends TCId {
    from_hospital:string;
    to_hospital:string;
    patient:string;
    clinical_findings:string;    
    diagnosis:string;
    investigation_results:string;
    treatment_given:string;
    reason_for_referral:string;
    referring_physician:string;
    phone_no:string;
    hpi:string;
    past_psychiatric_history:string;
    family_psychiatric_history:string;
    medical_history:string;
    physical_findings:string
     
    }
    export class PatientReferralFormSummaryPartialList {
      data: PatientReferralFormSummary[];
      total: number;
    }
    export class PatientReferralFormDetail extends PatientReferralFormSummary {
    }
    
    export class PatientReferralFormDashboard {
      total: number;
    }