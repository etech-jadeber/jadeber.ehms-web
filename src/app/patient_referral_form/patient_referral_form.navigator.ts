import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PatientReferralFormEditComponent} from "./patient-referral-form-edit/patient-referral-form-edit.component";
import {PatientReferralFormPickComponent} from "./patient-referral-form-pick/patient-referral-form-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PatientReferralFormNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  patientReferralFormsUrl(): string {
    return "/patient_referral_forms";
  }

  patientReferralFormUrl(id: string): string {
    return "/patient_referral_forms/" + id;
  }

  viewPatientReferralForms(): void {
    this.router.navigateByUrl(this.patientReferralFormsUrl());
  }

  viewPatientReferralForm(id: string): void {
    this.router.navigateByUrl(this.patientReferralFormUrl(id));
  }

  editPatientReferralForm(id: string): MatDialogRef<PatientReferralFormEditComponent> {
    const dialogRef = this.dialog.open(PatientReferralFormEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPatientReferralForm(parentId: string): MatDialogRef<PatientReferralFormEditComponent> {
    const dialogRef = this.dialog.open(PatientReferralFormEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickPatientReferralForms(selectOne: boolean=false): MatDialogRef<PatientReferralFormPickComponent> {
      const dialogRef = this.dialog.open(PatientReferralFormPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}