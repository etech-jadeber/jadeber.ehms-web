import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PatientReferralFormDashboard, PatientReferralFormDetail, PatientReferralFormSummaryPartialList} from "./patient_referral_form.model";
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root'
})
export class PatientReferralFormPersist {
 patientReferralFormSearchText: string = "";
 from_date: number;
 to_date: number;
 encounterId: string;
 patientId: string;
 
 
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patientReferralFormSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPatientReferralForm(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PatientReferralFormSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "patient_referral_form/", this.patientReferralFormSearchText, pageSize, pageIndex, sort, order);

    if(this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient", this.patientId)
      }
      if(this.encounterId){
      url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
      }
    return this.http.get<PatientReferralFormSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_referral_form/do", new TCDoParam("download_all", this.filters()));
  }

  patientReferralFormDashboard(): Observable<PatientReferralFormDashboard> {
    return this.http.get<PatientReferralFormDashboard>(environment.tcApiBaseUri + "patient_referral_form/dashboard");
  }

  getPatientReferralForm(id: string): Observable<PatientReferralFormDetail> {
    return this.http.get<PatientReferralFormDetail>(environment.tcApiBaseUri + "patient_referral_form/" + id);
  } 
  addPatientReferralForm(parentId: string, item: PatientReferralFormDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId +"/patient_referral_form/", item);
  }

  updatePatientReferralForm(item: PatientReferralFormDetail): Observable<PatientReferralFormDetail> {
    return this.http.patch<PatientReferralFormDetail>(environment.tcApiBaseUri + "patient_referral_form/" + item.id, item);
  }

  deletePatientReferralForm(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_referral_form/" + id);
  }
 patientReferralFormsDo(parentId:string,method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("form_encounters/"+parentId+"/patient_referral_form/do", this.patientReferralFormSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  patientReferralFormDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_referral_forms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_referral_form/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patient_referral_form/" + id + "/do", new TCDoParam("print", {}));
  }


}