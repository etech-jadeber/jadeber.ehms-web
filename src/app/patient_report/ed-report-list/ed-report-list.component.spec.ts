import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdReportListComponent } from './ed-report-list.component';

describe('EdReportListComponent', () => {
  let component: EdReportListComponent;
  let fixture: ComponentFixture<EdReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
