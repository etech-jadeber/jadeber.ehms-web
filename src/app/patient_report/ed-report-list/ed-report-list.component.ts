import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../../app.translation';
import { TCAsyncJob } from '../../tc/asyncjob';
import { TCAuthorization } from '../../tc/authorization';
import { FilePersist } from '../../tc/files/file.persist';
import { JobData } from '../../tc/jobs/job.model';
import { JobPersist } from '../../tc/jobs/job.persist';
import { TCNavigator } from '../../tc/navigator';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import {Location} from '@angular/common';
import { EmergencyReportSummary, IpdReportSummary } from '../report_models';
import {debounceTime} from "rxjs/operators";
import { MatDatepicker } from '@angular/material/datepicker';
import { PatientReportPersist } from '../report_persist';
@Component({
  selector: 'app-ed-report-list',
  templateUrl: './ed-report-list.component.html',
  styleUrls: ['./ed-report-list.component.scss']
})
export class EdReportListComponent implements OnInit {
  edReportDisplayedColumns:string[]=["date","total","repeat"];
  edReportsSelectAll: boolean = false;
  edReportsData:EmergencyReportSummary[]=[];
  edReportsIsLoading:boolean=false;
  ed_reportTotalCount:number=0;
  st:number=this.tcUtilsDate.toTimeStamp(new Date());
  et:number=this.tcUtilsDate.toTimeStamp(new Date());
  edReportSelection:EmergencyReportSummary[]=[];
  start_date: FormControl = new FormControl({disabled:true , value:new Date()});
  end_date: FormControl = new FormControl({disabled:true , value:new Date()});
  end_time_picker: MatDatepicker<any>;
  start_time_picker: MatDatepicker<any>
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public persist:PatientReportPersist,

  ) { 
  
    this.start_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.st = this.tcUtilsDate.toTimeStamp(value._d);
      this.searchEdReports()
     
    });
    this.end_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.et= this.tcUtilsDate.toTimeStamp(value._d);
      this.searchEdReports()
    }); 

  }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchEdReports(true);
    });

    this.paginator.page.subscribe(() => {
      this.searchEdReports(true);
    });
    this.searchEdReports();
  }
  searchEdReports(isPagination: boolean = false){
    let paginator = this.paginator;
    let sorter = this.sorter;

    this.edReportsIsLoading = true;
    this.edReportSelection = [];
      this.persist.edReportsDo("ed_report",{start_date:this.st,end_date:this.et,name:'Emergency'},paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((result)=>{
        this.edReportsData=result.data;
        this.ed_reportTotalCount=result.total;
        this.edReportsIsLoading=false;
      });    
    
  }
  back():void{
    this.location.back();
  }
}