import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IpdReportListComponent } from './ipd-report-list.component';

describe('IpdReportListComponent', () => {
  let component: IpdReportListComponent;
  let fixture: ComponentFixture<IpdReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IpdReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IpdReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
