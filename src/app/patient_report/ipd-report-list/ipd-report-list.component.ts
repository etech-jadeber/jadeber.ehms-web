import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../../app.translation';
import { TCAsyncJob } from '../../tc/asyncjob';
import { TCAuthorization } from '../../tc/authorization';
import { FilePersist } from '../../tc/files/file.persist';
import { JobData } from '../../tc/jobs/job.model';
import { JobPersist } from '../../tc/jobs/job.persist';
import { TCNavigator } from '../../tc/navigator';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import {Location} from '@angular/common';

import { IpdReportSummary } from '../report_models';
import { MatDatepicker } from '@angular/material/datepicker';
import {debounceTime} from "rxjs/operators";
import { PatientReportPersist } from '../report_persist';
import { PatientDischargePersist } from 'src/app/patient_discharge/patient_discharge.persist';
@Component({
  selector: 'app-ipd-report-list',
  templateUrl: './ipd-report-list.component.html',
  styleUrls: ['./ipd-report-list.component.scss']
})
export class IpdReportListComponent implements OnInit {

  ipdReportDisplayedColumns:string[]=["patient","admission","readmission","bed_occupancy","condition_status"];
  ipd_reportTotalcount:number=0;
  ipdReportsData:IpdReportSummary[]=[];
  ipdReportsIsLoading:boolean=false;
  ipd_reportTotalCount:number=0;
  start_date: FormControl = new FormControl({disabled:true , value:new Date()});
  end_date: FormControl = new FormControl({disabled:true , value:new Date()});
  ipdReportSelection:IpdReportSummary[]=[];
  st:number=this.tcUtilsDate.toTimeStamp(new Date());
  et:number=this.tcUtilsDate.toTimeStamp(new Date());
  end_time_picker: MatDatepicker<any>;
  start_time_picker: MatDatepicker<any>
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public persist:PatientReportPersist,
    public patientDischargePersist:PatientDischargePersist,
  ) {
    this.start_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.st = this.tcUtilsDate.toTimeStamp(value._d);
      this.searchIpdReports()
     
    });
    this.end_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.et= this.tcUtilsDate.toTimeStamp(value._d);
      this.searchIpdReports()
    }); 
   }

   ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchIpdReports(true);
    });

    this.paginator.page.subscribe(() => {
      this.searchIpdReports(true);
    });
    this.searchIpdReports();
  }
  searchIpdReports(isPagination: boolean = false){
    let paginator = this.paginator;
    let sorter = this.sorter;

    this.ipdReportsIsLoading = true;
    this.ipdReportSelection = [];

      this.persist.ipdReportsDo("ipd_report",{start_date:this.st,end_date:this.et},paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((result)=>{
        this.ipdReportsData=result.data;
        console.log(this.ipdReportsData);
        this.ipd_reportTotalCount=result.total;
        this.ipdReportsIsLoading =false;
      });    
    
  }
  back():void{
    this.location.back();
  }
}

