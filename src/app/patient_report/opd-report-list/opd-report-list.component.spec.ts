import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpdReportListComponent } from './opd-report-list.component';

describe('OpdReportListComponent', () => {
  let component: OpdReportListComponent;
  let fixture: ComponentFixture<OpdReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpdReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpdReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
