import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../../app.translation';
import { TCAsyncJob } from '../../tc/asyncjob';
import { TCAuthorization } from '../../tc/authorization';
import { FilePersist } from '../../tc/files/file.persist';
import { JobData } from '../../tc/jobs/job.model';
import { JobPersist } from '../../tc/jobs/job.persist';
import { TCNavigator } from '../../tc/navigator';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import {Location} from '@angular/common';
import { OpdReportSummary } from '../report_models';
import { PatientReportPersist } from '../report_persist';
import {debounceTime} from "rxjs/operators";
import { MatDatepicker } from '@angular/material/datepicker';
@Component({
  selector: 'app-opd-report-list',
  templateUrl: './opd-report-list.component.html',
  styleUrls: ['./opd-report-list.component.scss']
})
export class OpdReportListComponent implements OnInit {
  opdReportDisplayedColumns:string[]=["name","new","repeat","new_repeat","total"];
  opdReportsSelectAll: boolean = false;
  opdReportsData:OpdReportSummary[]=[];
  opdReportsIsLoading:boolean=false;
  opd_reportTotalCount:number=0;
  st:number=this.tcUtilsDate.toTimeStamp(new Date());
  et:number=this.tcUtilsDate.toTimeStamp(new Date());
  opdReportSelection:OpdReportSummary[]=[];
  start_date: FormControl = new FormControl({disabled:true , value:new Date()});
  end_date: FormControl = new FormControl({disabled:true , value:new Date()});
  end_time_picker: MatDatepicker<any>;
  start_time_picker: MatDatepicker<any>
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public persist:PatientReportPersist,

  ) { 
  
    this.start_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.st = this.tcUtilsDate.toTimeStamp(value._d);
      this.searchOpdReports()
     
    });
    this.end_date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.et= this.tcUtilsDate.toTimeStamp(value._d);
      this.searchOpdReports()
    }); 

  }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchOpdReports(true);
    });

    this.paginator.page.subscribe(() => {
      this.searchOpdReports(true);
    });
    this.searchOpdReports();
  }
  searchOpdReports(isPagination: boolean = false){
    let paginator = this.paginator;
    let sorter = this.sorter;

    this.opdReportsIsLoading = true;
    this.opdReportSelection = [];

      this.persist.opdReportsDo("opd_report",{start_date:this.st,end_date:this.et,name:'OutPatient'},paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((result)=>{
        this.opdReportsData=result.data;
        this.opd_reportTotalCount=result.total;
        this.opdReportsIsLoading =false;
      });    
    
  }
  back():void{
    this.location.back();
  }
}
