import { TCId } from "../tc/models";

export class OpdReportSummary extends TCId{
    name:string;
    new:number;
    new_repeat:number;
    repeat:number;
    total:number;
    date:number;

}
export class OpdReportSummaryPartialList {
    data: OpdReportSummary[];
    total: number;
  }
export class IpdReportSummary extends TCId{
    patient:string;
    admission:number;
    readmission:number;
    bed_occupancy:number;
    condition_status:number;
    total:number;

}
export class IpdReportSummaryPartialList{
    data:IpdReportSummary[];
    total:number;
}

export class EmergencyReportSummary extends TCId{
    date:number;
    no_of_emergency:number;
    repeat:number;
}
export class EmergencyReportSummaryPartialList {
    data: EmergencyReportSummary[];
    total: number;
  }