import { Injectable } from "@angular/core";
import { Router } from "@angular/router";


@Injectable({
    providedIn: 'root'
  })
export class PatientReportNavigator{

 constructor(private router:Router){

 }

    view_opd_report_url(){
        return "report/opd_report"

    }

    view_opd_report(){
        this.router.navigateByUrl(this.view_opd_report_url())
    }
    view_ipd_report_url(){
        return "report/ipd_report"

    }

    view_ipd_report(){
        this.router.navigateByUrl(this.view_ipd_report_url())
    }
    view_ed_report_url(){
        return "report/ed_report"

    }

    view_ed_report(){
        this.router.navigateByUrl(this.view_ed_report_url())
    }

}


