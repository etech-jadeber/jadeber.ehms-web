import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";
import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import { EmergencyReportSummaryPartialList, IpdReportSummaryPartialList, OpdReportSummary, OpdReportSummaryPartialList } from './report_models';




@Injectable({
  providedIn: 'root'
})
export class PatientReportPersist {
  
 searchText: string = "";
 
constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.searchText;
    //add custom filters
    return fltrs;
  }
 
  searchOpdReports(parentId:string,pageSize: number, pageIndex: number, sort: string, order?: string): Observable<OpdReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("report/opd_report", this.searchText, pageSize, pageIndex, sort, order);
    return this.http.get<OpdReportSummaryPartialList>(url);

  } 
  


 opdReportsDo(method: string, payload: any,pageSize: number, pageIndex: number, sort: string, order?: string): Observable<OpdReportSummaryPartialList> {
  let url = TCUtilsHttp.buildSearchUrl("report/opd_report/do", this.searchText, pageSize, pageIndex, sort, order);
    return this.http.post<OpdReportSummaryPartialList>(url, new TCDoParam(method, payload));
  }
  edReportsDo(method: string, payload: any,pageSize: number, pageIndex: number, sort: string, order?: string): Observable<EmergencyReportSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("report/opd_report/do", this.searchText, pageSize, pageIndex, sort, order);
    return this.http.post<EmergencyReportSummaryPartialList>(url, new TCDoParam(method, payload));
  }
  ipdReportsDo(method: string, payload: any,pageSize: number, pageIndex: number, sort: string, order?: string): Observable<IpdReportSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("patient_discharge/do", this.searchText, pageSize, pageIndex, sort, order);
    return this.http.post<IpdReportSummaryPartialList>(url, new TCDoParam(method, payload));
  }



 
  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "report/opd_report/" + id + "/do", new TCDoParam("print", {}));
  }


}