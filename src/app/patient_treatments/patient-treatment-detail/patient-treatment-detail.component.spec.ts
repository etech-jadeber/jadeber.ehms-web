import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientTreatmentDetailComponent } from './patient-treatment-detail.component';

describe('PatientTreatmentDetailComponent', () => {
  let component: PatientTreatmentDetailComponent;
  let fixture: ComponentFixture<PatientTreatmentDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientTreatmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientTreatmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
