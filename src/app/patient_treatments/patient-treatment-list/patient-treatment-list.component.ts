import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { Patient_TreatmentDetail } from '../patient_treatment.model';
import { Patient_TreatmentNavigator } from '../patient_treatment.navigator';
import { Patient_TreatmentPersist } from '../patient_treatment.persist';

@Component({
  selector: 'app-patient-treatment-list',
  templateUrl: './patient-treatment-list.component.html',
  styleUrls: ['./patient-treatment-list.component.css']
})
export class PatientTreatmentListComponent implements OnInit {

  patient_treatmentsIsLoading: boolean = false;
  patient_treatmentsData: Patient_TreatmentDetail[] = [];
  patient_treatmentsTotalCount: number = 0;

  @Input() encounterId: string;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(public patient_treatmentNavigator: Patient_TreatmentNavigator,
              public patient_treatmentPersist: Patient_TreatmentPersist,
              public appTranslation: AppTranslation,
              public tcAuthorization: TCAuthorization,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsArray: TCUtilsArray,
              public userPersist: UserPersist,) { }

  ngOnInit(): void {
    
  }
  ngAfterViewInit() {
  // patient_procedures paginator
  this.paginator.page.subscribe(() => {
      this.searchPatient_Treatments(true);
    });
    this.searchPatient_Treatments();
  }

  addPatient_Treatment(id: string): void {
    let dialogRef = this.patient_treatmentNavigator.addPatient_Treatment(id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchPatient_Treatments();
      }
    });
  }

  searchPatient_Treatments(isPagination: boolean = false): void{
    let paginator = this.paginator;
    this.patient_treatmentsIsLoading = true;
    this.patient_treatmentPersist.searchPatient_Treatment(this.encounterId, paginator.pageSize,
      isPagination ? paginator.pageIndex : 0,
      "record_date",
      "desc").subscribe((result) => {
      this.patient_treatmentsData = result.data;
      this.patient_treatmentsData.forEach(treatment => {
          this.userPersist.getUser(treatment.assessed_by).subscribe((user) => {
            treatment.assessed_name = user.name
          })
      })
      if (result.total != -1) {
        this.patient_treatmentsTotalCount = result.total;
      }
      this.patient_treatmentsIsLoading = false;
    }, (error) => {
      this.patient_treatmentsIsLoading = false;
    })
  }

}
