import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Patient_TreatmentEditComponent } from './patient-treatment-edit.component';

describe('Patient_TreatmentEditComponent', () => {
  let component: Patient_TreatmentEditComponent;
  let fixture: ComponentFixture<Patient_TreatmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Patient_TreatmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Patient_TreatmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
