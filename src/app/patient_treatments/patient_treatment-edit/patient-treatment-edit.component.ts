import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Patient_TreatmentDetail} from "../patient_treatment.model";
import {Patient_TreatmentPersist} from "../patient_treatment.persist";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-patient_treatment-edit',
  templateUrl: './patient_treatment-edit.component.html',
  styleUrls: ['./patient-treatment-edit.component.css']
})
export class Patient_TreatmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patient_treatmentDetail: Patient_TreatmentDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Patient_TreatmentEditComponent>,
              public  persist: Patient_TreatmentPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_treatments");
      this.title = this.appTranslation.getText("general","new") +  " Patient_Treatment";
      this.patient_treatmentDetail = new Patient_TreatmentDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_treatments");
      this.title = this.appTranslation.getText("general","edit") +  " Patient_Treatment";
      this.isLoadingResults = true;
      this.persist.getPatient_Treatment(this.idMode.id).subscribe(patient_treatmentDetail => {
        this.patient_treatmentDetail = patient_treatmentDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addPatient_Treatment(this.idMode.id, this.patient_treatmentDetail).subscribe(value => {
      this.tcNotification.success("Patient_Treatment added");
      this.patient_treatmentDetail.id = value.id;
      this.dialogRef.close(this.patient_treatmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updatePatient_Treatment(this.patient_treatmentDetail).subscribe(value => {
      this.tcNotification.success("Patient_Treatment updated");
      this.dialogRef.close(this.patient_treatmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.patient_treatmentDetail == null){
            return false;
          }

        if (this.patient_treatmentDetail.treatment == null || this.patient_treatmentDetail.treatment  == "") {
                      return false;
                    }


        return true;
      }


}
