import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Patient_TreatmentPickComponent } from './patient_treatment-pick.component';

describe('Patient_TreatmentPickComponent', () => {
  let component: Patient_TreatmentPickComponent;
  let fixture: ComponentFixture<Patient_TreatmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Patient_TreatmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Patient_TreatmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
