import {TCId} from "../tc/models";

export class Patient_TreatmentSummary extends TCId {
  treatment : string;
record_date : number;
encounter_id : string;
assessed_by: string;
assessed_name: string;
}

export class Patient_TreatmentSummaryPartialList {
  data: Patient_TreatmentSummary[];
  total: number;
}

export class Patient_TreatmentDetail extends Patient_TreatmentSummary {
  treatment : string;
record_date : number;
encounter_id : string;
}

export class Patient_TreatmentDashboard {
  total: number;
}
