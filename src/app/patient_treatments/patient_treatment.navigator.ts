import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Patient_TreatmentEditComponent} from "./patient_treatment-edit/patient-treatment-edit.component";
import {Patient_TreatmentPickComponent} from "./patient_treatment-pick/patient_treatment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Patient_TreatmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  patient_treatmentsUrl(): string {
    return "/patient_treatments";
  }

  patient_treatmentUrl(id: string): string {
    return "/patient_treatments/" + id;
  }

  viewPatient_Treatments(): void {
    this.router.navigateByUrl(this.patient_treatmentsUrl());
  }

  viewPatient_Treatment(id: string): void {
    this.router.navigateByUrl(this.patient_treatmentUrl(id));
  }

  editPatient_Treatment(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Patient_TreatmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPatient_Treatment(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Patient_TreatmentEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPatient_Treatments(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Patient_TreatmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
