import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Patient_TreatmentDashboard, Patient_TreatmentDetail, Patient_TreatmentSummaryPartialList} from "./patient_treatment.model";


@Injectable({
  providedIn: 'root'
})
export class Patient_TreatmentPersist {

  patient_treatmentSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchPatient_Treatment(parentId: string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Patient_TreatmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounters/" + parentId + "/patient_treatments", this.patient_treatmentSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Patient_TreatmentSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patient_treatmentSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_treatments/do", new TCDoParam("download_all", this.filters()));
  }

  patient_treatmentDashboard(): Observable<Patient_TreatmentDashboard> {
    return this.http.get<Patient_TreatmentDashboard>(environment.tcApiBaseUri + "patient_treatments/dashboard");
  }

  getPatient_Treatment(id: string): Observable<Patient_TreatmentDetail> {
    return this.http.get<Patient_TreatmentDetail>(environment.tcApiBaseUri + "patient_treatments/" + id);
  }

  addPatient_Treatment(id:string, item: Patient_TreatmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounter/" + id + "/patient_treatments", item);
  }

  updatePatient_Treatment(item: Patient_TreatmentDetail): Observable<Patient_TreatmentDetail> {
    return this.http.patch<Patient_TreatmentDetail>(environment.tcApiBaseUri + "patient_treatments/" + item.id, item);
  }

  deletePatient_Treatment(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patient_treatments/" + id);
  }

  patient_treatmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_treatments/do", new TCDoParam(method, payload));
  }

  patient_treatmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_treatments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patient_treatments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patient_treatments/" + id + "/do", new TCDoParam("print", {}));
  }


}
