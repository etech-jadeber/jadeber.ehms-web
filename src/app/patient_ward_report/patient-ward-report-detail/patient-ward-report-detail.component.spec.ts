import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientWardReportDetailComponent } from './patient-ward-report-detail.component';

describe('PatientWardReportDetailComponent', () => {
  let component: PatientWardReportDetailComponent;
  let fixture: ComponentFixture<PatientWardReportDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientWardReportDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientWardReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
