import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PatientWardReportDetail} from "../patient_ward_report.model";
import {PatientWardReportPersist} from "../patient_ward_report.persist";
import {PatientWardReportNavigator} from "../patient_ward_report.navigator";

export enum PatientWardReportTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-patient_ward_report-detail',
  templateUrl: './patient-ward-report-detail.component.html',
  styleUrls: ['./patient-ward-report-detail.component.css']
})
export class PatientWardReportDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  patientWardReportIsLoading:boolean = false;
  
  PatientWardReportTabs: typeof PatientWardReportTabs = PatientWardReportTabs;
  activeTab: PatientWardReportTabs = PatientWardReportTabs.overview;
  //basics
  patientWardReportDetail: PatientWardReportDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public patientWardReportNavigator: PatientWardReportNavigator,
              public  patientWardReportPersist: PatientWardReportPersist) {
    this.tcAuthorization.requireRead("patient_ward_report");
    this.patientWardReportDetail = new PatientWardReportDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("patient_ward_report");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.patientWardReportIsLoading = true;
    this.patientWardReportPersist.getPatientWardReport(id).subscribe(patientWardReportDetail => {
          this.patientWardReportDetail = patientWardReportDetail;
          this.patientWardReportIsLoading = false;
        }, error => {
          console.error(error);
          this.patientWardReportIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.patientWardReportDetail.id);
  }
  editPatientWardReport(): void {
    let modalRef = this.patientWardReportNavigator.editPatientWardReport(this.patientWardReportDetail.id);
    modalRef.afterClosed().subscribe(patientWardReportDetail => {
      TCUtilsAngular.assign(this.patientWardReportDetail, patientWardReportDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.patientWardReportPersist.print(this.patientWardReportDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print patient_ward_report", true);
      });
    }

  back():void{
      this.location.back();
    }

}