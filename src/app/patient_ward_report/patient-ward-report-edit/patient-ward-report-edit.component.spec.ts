import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientWardReportEditComponent } from './patient-ward-report-edit.component';

describe('PatientWardReportEditComponent', () => {
  let component: PatientWardReportEditComponent;
  let fixture: ComponentFixture<PatientWardReportEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientWardReportEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientWardReportEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
