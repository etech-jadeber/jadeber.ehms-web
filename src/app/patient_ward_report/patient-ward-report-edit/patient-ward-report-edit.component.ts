import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientWardReportDetail } from '../patient_ward_report.model';import { PatientWardReportPersist } from '../patient_ward_report.persist';

@Component({
  selector: 'app-patient_ward_report-edit',
  templateUrl: './patient-ward-report-edit.component.html',
  styleUrls: ['./patient-ward-report-edit.component.css']
})



export class PatientWardReportEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientWardReportDetail: PatientWardReportDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PatientWardReportEditComponent>,
              public  persist: PatientWardReportPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("patient_ward_report");
      this.title = this.appTranslation.getText("general","new") +  " " + "patient_ward_report";
      this.patientWardReportDetail = new PatientWardReportDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("patient_ward_report");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "patient_ward_report";
      this.isLoadingResults = true;
      this.persist.getPatientWardReport(this.idMode.id).subscribe(patientWardReportDetail => {
        this.patientWardReportDetail = patientWardReportDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    console.log(this.patientWardReportDetail);
   
    this.persist.addPatient_ward_report(this.patientWardReportDetail).subscribe(value => {
      this.tcNotification.success("patientWardReport added");
      this.patientWardReportDetail.id = value.id;
      this.dialogRef.close(this.patientWardReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePatientWardReport(this.patientWardReportDetail).subscribe(value => {
      this.tcNotification.success("patient_ward_report updated");
      this.dialogRef.close(this.patientWardReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.patientWardReportDetail.ward_id == null){
            return false;
          }

          return true;

 }
 }