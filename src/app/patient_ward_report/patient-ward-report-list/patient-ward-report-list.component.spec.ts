import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientWardReportListComponent } from './patient-ward-report-list.component';

describe('PatientWardReportListComponent', () => {
  let component: PatientWardReportListComponent;
  let fixture: ComponentFixture<PatientWardReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientWardReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientWardReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
