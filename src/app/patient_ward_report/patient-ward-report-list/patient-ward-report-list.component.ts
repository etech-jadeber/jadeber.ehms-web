import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {JobData} from "../../tc/jobs/job.model";
import { PatientPersist } from "../../patients/patients.persist";
import { WardReportPersist } from "../../ward_report/ward_report.persist"
import {TCAsyncJob} from "../../tc/asyncjob";
import {FilePersist} from "../../tc/files/file.persist";
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';

import { PatientDetail } from "../../patients/patients.model"
import { WardReportDetail } from "../../ward_report/ward_report.model"
import { BedroomtypeDetail } from "../../bed/bedroomtypes/bedroomtype.model"
import { PatientWardReportDetail } from "../patient_ward_report.model"

import {AppTranslation} from "../../app.translation";
import { PatientWardReportSummary, PatientWardReportSummaryPartialList } from '../patient_ward_report.model';
import { PatientWardReportNavigator } from '../patient_ward_report.navigator';
import { PatientWardReportPersist } from '../patient_ward_report.persist';



@Component({
  selector: 'app-patient_ward_report-list',
  templateUrl: './patient-ward-report-list.component.html',
  styleUrls: ['./patient-ward-report-list.component.css']
})

export class PatientWardReportListComponent implements OnInit {
  patientWardReportsData: PatientWardReportSummary[] = [];
  patientWardReportsTotalCount: number = 0;
  patientWardReportSelectAll:boolean = false;
  patientWardReportSelection: PatientWardReportSummary[] = [];

  patients : {[id: string] : PatientDetail} = {}
  wards : {[id: string] : BedroomtypeDetail} = {}
  reports : {[id: string] : WardReportDetail} = {}


 patientWardReportsDisplayedColumns: string[] = ["select","action" ,"encounter_id", "patient_id", "ward_id", "report_id" ];
  patientWardReportSearchTextBox: FormControl = new FormControl();
  patientWardReportIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) patientWardReportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientWardReportsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientWardReportPersist: PatientWardReportPersist,
                public patientPersist: PatientPersist,
                public reportPersist: WardReportPersist,
                public patientWardReportNavigator: PatientWardReportNavigator,
                public jobPersist: JobPersist,
                public wardPersist: BedroomtypePersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("patient_ward_report");
       this.patientWardReportSearchTextBox.setValue(patientWardReportPersist.patientWardReportSearchText);
      //delay subsequent keyup events
      this.patientWardReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientWardReportPersist.patientWardReportSearchText = value;
        this.searchPatient_ward_reports();
      });
    } 
    
    
    ngOnInit() {
   
      this.patientWardReportsSort.sortChange.subscribe(() => {
        this.patientWardReportsPaginator.pageIndex = 0;
        this.searchPatient_ward_reports(true);
      });

      this.patientWardReportsPaginator.page.subscribe(() => {
        this.searchPatient_ward_reports(true);
      });
      //start by loading items
      this.searchPatient_ward_reports();
    }

  searchPatient_ward_reports(isPagination:boolean = false): void {


    let paginator = this.patientWardReportsPaginator;
    let sorter = this.patientWardReportsSort;

    this.patientWardReportIsLoading = true;
    this.patientWardReportSelection = [];

    this.patientWardReportPersist.searchPatientWardReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientWardReportSummaryPartialList) => {
      this.patientWardReportsData = partialList.data;
      this.loadAdditional(this.patientWardReportsData)
      if (partialList.total != -1) {
        this.patientWardReportsTotalCount = partialList.total;
      }
      this.patientWardReportIsLoading = false;
    }, error => {
      this.patientWardReportIsLoading = false;
    });

  } 


loadAdditional(data: PatientWardReportDetail[]) {
    data.forEach((d, idx) => {
      if (!this.patients[d.patient_id]){
        this.patients[d.patient_id] = new PatientDetail()
        this.patientPersist.getPatient(d.patient_id).subscribe(
          patient => this.patients[d.patient_id] = patient
        )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
      }

      if (!this.wards[d.ward_id]){
            this.wards[d.ward_id] = new BedroomtypeDetail()
            this.wardPersist.getBedroomtype(d.ward_id).subscribe(
              ward => {
                this.wards[d.ward_id] = ward;
              }
            )
      }

       if (!this.reports[d.report_id]){
            this.reports[d.report_id] = new WardReportDetail()
            this.reportPersist.getWardReport(d.report_id).subscribe(
              report => {
                this.reports[d.report_id] = report;
              }
            )
      }


    })
  }
      
  
  downloadPatientWardReports(): void {
    if(this.patientWardReportSelectAll){
         this.patientWardReportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patient_ward_report", true);
      });
    }
    else{
        this.patientWardReportPersist.download(this.tcUtilsArray.idsList(this.patientWardReportSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download patient_ward_report",true);
            });
        }
  }
addPatient_ward_report(): void {
    let dialogRef = this.patientWardReportNavigator.addPatientWardReport();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPatient_ward_reports();
      }
    });
  }

  editPatientWardReport(item: PatientWardReportSummary) {
    let dialogRef = this.patientWardReportNavigator.editPatientWardReport(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePatientWardReport(item: PatientWardReportSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("patient_ward_report");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.patientWardReportPersist.deletePatientWardReport(item.id).subscribe(response => {
          this.tcNotification.success("patient_ward_report deleted");
          this.searchPatient_ward_reports();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getPatientName(id)
    {
      // this.patientPersist.getPatient(id).subscribe(patient => {
      //   return `${patient.fname} ${patient.lname}`
      // })
      if(this.patients[id]){
        const {fname, lname} = this.patients[id]
        return fname ? `${fname} ${lname}` : ""
    }
    }

    getWardName(id)
    {
      // this.patientPersist.getPatient(id).subscribe(patient => {
      //   return `${patient.fname} ${patient.lname}`
      // })
      if(this.wards[id]){
        const {name} = this.wards[id]
        return name ? name : ""
    }
    }


    getReport(id)
    {
          if(this.reports[id]){
            console.log(this.reports[id])
        const {report} = this.reports[id]
        return report ? report : ""
    }
    }
}
