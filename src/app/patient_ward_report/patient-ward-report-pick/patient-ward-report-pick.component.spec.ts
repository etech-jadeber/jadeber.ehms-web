import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientWardReportPickComponent } from './patient-ward-report-pick.component';

describe('PatientWardReportPickComponent', () => {
  let component: PatientWardReportPickComponent;
  let fixture: ComponentFixture<PatientWardReportPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientWardReportPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientWardReportPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
