import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientWardReportSummary, PatientWardReportSummaryPartialList } from '../patient_ward_report.model';
import { PatientWardReportPersist } from '../patient_ward_report.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-patient_ward_report-list',
  templateUrl: './patient-ward-report-pick.component.html',
  styleUrls: ['./patient-ward-report-pick.component.css']
})


export class PatientWardReportPickComponent implements OnInit {
  patientWardReportsData: PatientWardReportSummary[] = [];
  patientWardReportsTotalCount: number = 0;
  patientWardReportSelectAll:boolean = false;
  patientWardReportSelection: PatientWardReportSummary[] = [];

 patientWardReportsDisplayedColumns: string[] = ["select","action" ,"encounter_id", "patient_id", "ward_id", "report_id" ];
  patientWardReportSearchTextBox: FormControl = new FormControl();
  patientWardReportIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) patientWardReportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientWardReportsSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientWardReportPersist: PatientWardReportPersist,
                public dialogRef: MatDialogRef<PatientWardReportPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("patient_ward_report");
       this.patientWardReportSearchTextBox.setValue(patientWardReportPersist.patientWardReportSearchText);
      //delay subsequent keyup events
      this.patientWardReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.patientWardReportPersist.patientWardReportSearchText = value;
        this.searchPatient_ward_reports();
      });
    } ngOnInit() {
   
      this.patientWardReportsSort.sortChange.subscribe(() => {
        this.patientWardReportsPaginator.pageIndex = 0;
        this.searchPatient_ward_reports(true);
      });

      this.patientWardReportsPaginator.page.subscribe(() => {
        this.searchPatient_ward_reports(true);
      });
      //start by loading items
      this.searchPatient_ward_reports();
    }

  searchPatient_ward_reports(isPagination:boolean = false): void {


    let paginator = this.patientWardReportsPaginator;
    let sorter = this.patientWardReportsSort;

    this.patientWardReportIsLoading = true;
    this.patientWardReportSelection= [];

    this.patientWardReportPersist.searchPatientWardReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientWardReportSummaryPartialList) => {
      this.patientWardReportsData = partialList.data;
      if (partialList.total != -1) {
        this.patientWardReportsTotalCount = partialList.total;
      }
      this.patientWardReportIsLoading = false;
    }, error => {
      this.patientWardReportIsLoading = false;
    });

  }

  editPatientWardReport()
  {

  }


  markOneItem(item: PatientWardReportSummary) {
    if(!this.tcUtilsArray.containsId(this.patientWardReportSelection,item.id)){
          this.patientWardReportSelection = [];
          this.patientWardReportSelection.push(item);
        }
        else{
          this.patientWardReportSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.patientWardReportSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }