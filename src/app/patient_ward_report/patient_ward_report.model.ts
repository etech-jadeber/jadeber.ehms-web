import {TCId} from "../tc/models";


export class PatientWardReportSummary extends TCId {
    patient_id:string;
    ward_id:string;
    report_id:string;
    encounter_id:string;
    
}
export class PatientWardReportSummaryPartialList {
  data: PatientWardReportSummary[];
  total: number;
}
export class PatientWardReportDetail extends PatientWardReportSummary {
}

export class PatientWardReportDashboard {
  total: number;
}