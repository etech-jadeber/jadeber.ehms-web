import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PatientWardReportEditComponent} from "./patient-ward-report-edit/patient-ward-report-edit.component";
import {PatientWardReportPickComponent} from "./patient-ward-report-pick/patient-ward-report-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PatientWardReportNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  patient_ward_reportsUrl(): string {
    return "/patient_ward_reports";
  }

  patient_ward_reportUrl(id: string): string {
    return "/patient_ward_reports/" + id;
  }

  viewPatientWardReports(): void {
    this.router.navigateByUrl(this.patient_ward_reportsUrl());
  }

  viewPatientWardReport(id: string): void {
    this.router.navigateByUrl(this.patient_ward_reportUrl(id));
  }

  editPatientWardReport(id: string): MatDialogRef<PatientWardReportEditComponent> {
    const dialogRef = this.dialog.open(PatientWardReportEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPatientWardReport(): MatDialogRef<PatientWardReportEditComponent> {
    const dialogRef = this.dialog.open(PatientWardReportEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPatientWardReports(selectOne: boolean=false): MatDialogRef<PatientWardReportPickComponent> {
      const dialogRef = this.dialog.open(PatientWardReportPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}