import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PatientWardReportDashboard, PatientWardReportDetail, PatientWardReportSummaryPartialList} from "./patient_ward_report.model";


@Injectable({
  providedIn: 'root'
})
export class PatientWardReportPersist {
 patientWardReportSearchText: string = "";
 
 constructor(private http: HttpClient) {
  }


 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_ward_report/" + id + "/do", new TCDoParam("print", {}));
  }  
  
  
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.patientWardReportSearchText;
    //add custom filters
    return fltrs;
  }

    addPatient_ward_report(item: PatientWardReportDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_ward_report/", item);
  }
 
  searchPatientWardReport(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PatientWardReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patient_ward_report", this.patientWardReportSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<PatientWardReportSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_ward_report/do", new TCDoParam("download_all", this.filters()));
  }

  patientWardReportDashboard(): Observable<PatientWardReportDashboard> {
    return this.http.get<PatientWardReportDashboard>(environment.tcApiBaseUri + "patient_ward_report/dashboard");
  }

  getPatientWardReport(id: string): Observable<PatientWardReportDetail> {
    return this.http.get<PatientWardReportDetail>(environment.tcApiBaseUri + "patient_ward_report/" + id);
  }

 download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_ward_report/do", new TCDoParam("download", ids));
  }

  deletePatientWardReport(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "patient_ward_report/" + id);
  }

   updatePatientWardReport(item: PatientWardReportDetail): Observable<PatientWardReportDetail> {
    return this.http.patch<PatientWardReportDetail>(environment.tcApiBaseUri + "patient_ward_report/" + item.id, item);
  }
 }