
import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../tc/utils-http";
import {CaseDashboard, CaseDetail, CaseSummaryPartialList} from "../tc/cases/case.model";
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class PatientCasePersisit {

    caseSearchText: string = "";

    constructor(private http: HttpClient) {
    }

    searchCase(caseTypeId:number, pageSize: number, pageIndex: number, sort: string, order: string): Observable<CaseSummaryPartialList> {

      let url = TCUtilsHttp.buildSearchUrl("cases", this.caseSearchText, pageSize, pageIndex, sort, order);
      url = TCUtilsString.appendUrlParameter(url,"case_type",caseTypeId.toString());
      
      return this.http.get<CaseSummaryPartialList>(url);

    }

    caseDashboard(): Observable<CaseDashboard> {
      return this.http.get<CaseDashboard>(environment.tcApiBaseUri + "cases/dashboard");
    }

    getCase(id: string): Observable<CaseDetail> {
      return this.http.get<CaseDetail>(environment.tcApiBaseUri + "cases/" + id);
    }


    caseDo(id: string, method: string, payload: any): Observable<{}> {
      return this.http.post<{}>(environment.tcApiBaseUri + "cases/" + id + "/do", new TCDoParam(method, payload));
    }


  }
