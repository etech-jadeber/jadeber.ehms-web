import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraTakeComponent } from './camera-take.component';

describe('CameraTakeComponent', () => {
  let component: CameraTakeComponent;
  let fixture: ComponentFixture<CameraTakeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CameraTakeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraTakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
