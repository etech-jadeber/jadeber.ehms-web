import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { WebcamImage } from 'ngx-webcam';
import { Observable, Subject } from 'rxjs';
@Component({
  selector: 'app-camera-take',
  templateUrl: './camera-take.component.html',
  styleUrls: ['./camera-take.component.css']
})
export class CameraTakeComponent implements OnInit {

  constructor(  public dialogRef: MatDialogRef<CameraTakeComponent>) { }

  ngOnInit(): void {
  }




  
  public webcamImage: WebcamImage = null;
  private trigger: Subject<void> = new Subject<void>();
  triggerSnapshot(): void {
   this.trigger.next();
  }
  handleImage(webcamImage: WebcamImage): void {
    this.dialogRef.close(webcamImage);
  }
   
  public get triggerObservable(): Observable<void> {
   return this.trigger.asObservable();
  }


}
