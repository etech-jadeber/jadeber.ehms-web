import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

import {MatDialogRef,MatDialog} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {PatientPersist} from "./patients.persist";

import { CameraTakeComponent } from "./camera-take/camera-take.component";


@Injectable({
  providedIn: 'root'
})

export class CameraNavigator {

  constructor(private router: Router,
              public dialog: MatDialog,
              private patientPersisit: PatientPersist) {
  }




  takePic(): MatDialogRef<unknown,any> {
    return this.dialog.open(CameraTakeComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
  }


}
