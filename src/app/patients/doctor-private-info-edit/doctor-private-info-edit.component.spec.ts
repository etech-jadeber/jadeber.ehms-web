import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DoctorPrivateInfoEditComponent } from './doctor-private-info-edit.component';

describe('DoctorPrivateInfoEditComponent', () => {
  let component: DoctorPrivateInfoEditComponent;
  let fixture: ComponentFixture<DoctorPrivateInfoEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DoctorPrivateInfoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorPrivateInfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
