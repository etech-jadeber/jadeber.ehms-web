import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";
import { PatientPersist } from '../patients.persist';
import { Doctor_Private_InfoDetail } from '../patients.model';



@Component({
  selector: 'app-doctor_private_info-edit',
  templateUrl: './doctor-private-info-edit.component.html',
  styleUrls: ['./doctor-private-info-edit.component.css']
})
export class Doctor_Private_InfoEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  doctor_private_infoDetail: Doctor_Private_InfoDetail = new Doctor_Private_InfoDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<Doctor_Private_InfoEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: PatientPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.isNew()) {
      this.title = this.appTranslation.getText("general","new") + " "+ this.appTranslation.getText("patient","private_info");
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general","edit") + " "+ this.appTranslation.getText("patient","private_info");
      this.isLoadingResults = true;
      this.persist.getDoctor_Private_Info(this.ids.parentId, this.ids.childId).subscribe(doctor_private_infoDetail => {
        this.doctor_private_infoDetail = doctor_private_infoDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }



  canSubmit():boolean{
      if (this.doctor_private_infoDetail == null){
          return false;
        }

     if (this.doctor_private_infoDetail.note == null || this.doctor_private_infoDetail.note  == "") {
                      return false;
                    }

      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addDoctor_Private_Info(this.ids.parentId, this.doctor_private_infoDetail).subscribe(value => {
      this.tcNotification.success("Doctor_Private_Info added");
      this.doctor_private_infoDetail.id = value.id;
      this.dialogRef.close(this.doctor_private_infoDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateDoctor_Private_Info(this.ids.parentId, this.doctor_private_infoDetail).subscribe(value => {
      this.tcNotification.success("Doctor_Private_Info updated");
      this.dialogRef.close(this.doctor_private_infoDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
