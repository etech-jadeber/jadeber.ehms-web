import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HistoryDataEditComponent } from './history-data-edit.component';

describe('HistoryDataEditComponent', () => {
  let component: HistoryDataEditComponent;
  let fixture: ComponentFixture<HistoryDataEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryDataEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryDataEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
