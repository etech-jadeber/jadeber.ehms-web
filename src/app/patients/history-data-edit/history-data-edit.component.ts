import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../tc/notification";
import {TCUtilsString}  from "../../tc/utils-string";
import {TCParentChildIds} from "../../tc/models";
import {AppTranslation} from "../../app.translation";
import { History_DataDetail } from '../patients.model';
import { PatientPersist } from '../patients.persist';



@Component({
  selector: 'app-history_data-edit',
  templateUrl: './history-data-edit.component.html',
  styleUrls: ['./history-data-edit.component.css']
})
export class History_DataEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  history_dataDetail: History_DataDetail = new History_DataDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public dialogRef: MatDialogRef<History_DataEditComponent>,
              public appTranslation:AppTranslation,
              public  persist: PatientPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  isNew(): boolean {
    return this.ids.childId === null;
  }

  isEdit(): boolean {
    return this.ids.childId != null;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {

    if (this.isNew()) {
      this.title = this.appTranslation.getText("general","new") + " "+ this.appTranslation.getText("patient","history");
      this.history_dataDetail.pid = this.ids.parentId;
      this.history_dataDetail.alcohol = "";
      this.history_dataDetail.coffee = "";
      this.history_dataDetail.history_father = "";
      this.history_dataDetail.history_mother = "";
      this.history_dataDetail.history_offspring = "";
      this.history_dataDetail.history_siblings = "";
      this.history_dataDetail.history_spouse = "";

      //set necessary defaults
    } else if(this.isEdit()){
      this.title = this.appTranslation.getText("general","edit") + " "+ this.appTranslation.getText("patient","history");
      this.isLoadingResults = true;
      this.persist.getHistory_Data(this.ids.parentId, this.ids.childId).subscribe(history_dataDetail => {
        this.history_dataDetail = history_dataDetail;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })
    }
  }



  canSubmit():boolean{
      if (this.history_dataDetail == null){
          return false;
        }


      return true;
    }


  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addHistory_Data(this.ids.parentId, this.history_dataDetail).subscribe(value => {
      this.tcNotification.success("History_Data added");
      this.history_dataDetail.id = value.id;
      this.dialogRef.close(this.history_dataDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateHistory_Data(this.ids.parentId, this.history_dataDetail).subscribe(value => {
      this.tcNotification.success("History_Data updated");
      this.dialogRef.close(this.history_dataDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
