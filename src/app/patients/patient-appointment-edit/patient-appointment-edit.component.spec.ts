import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientAppointmentEditComponent } from './patient-appointment-edit.component';

describe('PatientAppointmentEditComponent', () => {
  let component: PatientAppointmentEditComponent;
  let fixture: ComponentFixture<PatientAppointmentEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientAppointmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientAppointmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
