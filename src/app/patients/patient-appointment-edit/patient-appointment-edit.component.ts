import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import {AppointmentDetail} from "../../appointments/appointment.model";
import {AppointmentEditComponent} from "../../appointments/appointment-edit/appointment-edit.component";
import {AppointmentPersist} from "../../appointments/appointment.persist";
import {DoctorSummary} from "../../doctors/doctor.model";
import {DoctorNavigator} from "../../doctors/doctor.navigator";
import {TCUtilsDate} from "../../tc/utils-date";


@Component({
  selector: 'app-patient-appointment-edit',
  templateUrl: './patient-appointment-edit.component.html',
  styleUrls: ['./patient-appointment-edit.component.css']
})
export class PatientAppointmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  appointmentDetail: AppointmentDetail;
  userFullName: string;
  schedule_start_time = new Date();
  schedule_end_time = new Date();

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<AppointmentEditComponent>,
              public persist: AppointmentPersist,
              public tcUtilsDate: TCUtilsDate,
              private doctorNavigator: DoctorNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("schedule", "appointment");
    this.appointmentDetail = new AppointmentDetail();
    //set necessary defaults
    this.appointmentDetail.schedule_type = -1;
  }

  onAdd(): void {
    this.transformDates(false);
    this.dialogRef.close(this.appointmentDetail);
  }


  canSubmit(): boolean {
    if (this.appointmentDetail == null) {
      return false;
    }

    if (this.appointmentDetail.schedule_type == -1) {
      return false;
    }
    if (this.appointmentDetail.scheduler_user_id == null) {
      return false;
    }

    if (this.schedule_end_time == null || this.schedule_start_time == null) {
      return false;
    }

    return true;
  }


  searchUser() {
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.userFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
        this.appointmentDetail.scheduler_user_id = result[0].id;
      }
    });
  }

  transformDates(todate: boolean) {
    if (todate) {
      this.schedule_start_time = this.tcUtilsDate.toDate(this.appointmentDetail.schedule_start_time);
      this.schedule_end_time = this.tcUtilsDate.toDate(this.appointmentDetail.schedule_end_time);
    } else {
      this.appointmentDetail.schedule_start_time = new Date(this.schedule_start_time).getTime() / 1000;
      this.appointmentDetail.schedule_end_time = new Date(this.schedule_end_time).getTime() / 1000;
    }
  }
}
