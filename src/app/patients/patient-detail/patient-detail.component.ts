import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { PatientDetail, ReceptionNoteSummary, SummarySheetSummary, SummarySheetSummaryPartialList } from "../patients.model";
import { PatientPersist } from "../patients.persist";
import { PatientNavigator } from "../patients.navigator";
import { CommentDetail, CommentSummary, TCId } from 'src/app/tc/models';
import { FormControl } from '@angular/forms';
import { TCAuthentication } from 'src/app/tc/authentication';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { tabs } from 'src/app/app.enums';
import { TCAppInit } from 'src/app/tc/app-init';
import { environment } from 'src/environments/environment';
import { ReceptionNotePersist } from '../receptionNote.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { AppointmentSummary, AppointmentSummaryPartialList } from 'src/app/appointments/appointment.model';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';


export enum PaginatorIndexes {
  summary,
  notes,
}

export enum PatientTabs {
  overview
}


@Component({
  selector: 'app-patient-detail',
  templateUrl: './patient-detail.component.html',
  styleUrls: ['./patient-detail.component.css']
})
export class PatientDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  patientLoading: boolean = false;
  //basics
  patientDetail: PatientDetail;
  //patientTabs: typeof PatientTabs = PatientTabs;
  // activeTab: PatientTabs = PatientTabs.overview;
  tabs:typeof tabs=tabs;
  activeTab:number;

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  //notes
  profileUrl = "";
  notesData: ReceptionNoteSummary[] = [];
  notesTotalCount: number = 0;
  notesSelectAll: boolean = false;
  notesSelected: CommentSummary[] = [];
  users: {[id: string]: UserDetail} = {}
  invalid_uuid = TCUtilsString.getInvalidId();
  displayedColumns: string[] = ['date', 'service', 'chief_complaint', 'diagnosis','schedule_type','doctor'];

  notesDisplayedColumns: string[] = ['select', 'action', 'comment_text'];
  notesSearchTextBox: FormControl = new FormControl();
  notesLoading: boolean = false;
  summarySheetsTotalCount: number = 0;
  summarySheetsData: SummarySheetSummary[] = [];
  featureAppointmentData: AppointmentSummary[] = [];
  appointmentsDisplayedColumns: string[] = [  "scheduled_user_id", 
  "service_type",
  "schedule_start_time", "status","schedule_type","appointed_for" ];

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public userPersist: UserPersist,
    public tcAuthentication: TCAuthentication,
    public appTranslation: AppTranslation,
    public tcUtilsString: TCUtilsString,
    public patientNavigator: PatientNavigator,
    public patientPersist: PatientPersist,
    public menuState: MenuState,
    public tcUtilString:TCUtilsString,

    public appointmentPersist:AppointmentPersist,

    public receptionNotePersist: ReceptionNotePersist) {
    this.tcAuthorization.requireRead("patients");
    this.patientDetail = new PatientDetail();

    //notes filter
    this.notesSearchTextBox.setValue(receptionNotePersist.noteSearchText);
    this.notesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.receptionNotePersist.noteSearchText = value.trim();
      this.searchNotes();
    });

    // //tab state manager
    // this.menuState.currentTabResponseState.subscribe((res) => {
    //   this.activeTab =  res;
    // })

  }




  ngOnInit() {
    this.tcAuthorization.requireRead("patients");
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.receptionNotePersist.patientId = id;
      this.loadDetails(id);
      // this.menuState.changeMenuState(id);
    });



    this.sorters
    .toArray()
    [PaginatorIndexes.summary].sortChange.subscribe(() => {
    this.paginators.toArray()[PaginatorIndexes.summary].pageIndex = 0;
    this.loadSummary(true);
  });

  this.paginators
  .toArray()
  [PaginatorIndexes.summary].page.subscribe(() => {
  this.loadSummary(true);
});

     // notes sorter
    this.sorters
     .toArray()
     [PaginatorIndexes.notes].sortChange.subscribe(() => {
     this.paginators.toArray()[PaginatorIndexes.notes].pageIndex = 0;
     this.searchNotes(true);
   });
   // notes paginator
   this.paginators
     .toArray()
     [PaginatorIndexes.notes].page.subscribe(() => {
     this.searchNotes(true);
   });
  }

  loadDetails(id: string): void {
    this.patientLoading = true;
    this.patientPersist.getPatient(id).subscribe(patientDetail => {
      this.patientDetail = patientDetail;
      this.profileUrl = `${environment.tcApiBaseUri}tc/files/download?fk=${patientDetail.profile_pic}`;
      this.menuState.getDataResponse(this.patientDetail);
      this.searchNotes();
      this.loadSummary();
      this.loadAppointment();
      this.patientLoading = false;
    }, error => {
      console.error(error);
      this.patientLoading = false;
    });
  }
  loadAppointment(isPagination:boolean = false){
    // let paginator = this.paginators.toArray()[PaginatorIndexes.summary];
    // let sorter = this.sorters.toArray()[PaginatorIndexes.summary];
    // console.log("paginator and sorder",this.patientDetail.id)

    this.patientPersist.patientDo2(20, 0, 'schedule_start_time', 'desc',this.patientDetail.id,"get_feature_appointment",{}).subscribe((res:AppointmentSummaryPartialList)=>{
      this.featureAppointmentData = res.data
      // if (res.total != -1) {
      //   this.summarySheetsTotalCount = res.total;
      // }
   })
  }
 loadSummary(isPagination:boolean = false){
    let paginator = this.paginators.toArray()[PaginatorIndexes.summary];
    let sorter = this.sorters.toArray()[PaginatorIndexes.summary];
    console.log("paginator and sorder",this.patientDetail.id)

    this.patientPersist.patientDo2(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction,this.patientDetail.id,"get_summary_detail",{}).subscribe((res:SummarySheetSummaryPartialList)=>{
      console.log("res ------ res ----- res ---- ",res.data)
      this.summarySheetsData = res.data
      if (res.total != -1) {
        this.summarySheetsTotalCount = res.total;
      }
   })
  }
  reload() {
    this.loadDetails(this.patientDetail.id);
  }

  editPatient(): void {
    let modalRef = this.patientNavigator.editPatient(this.patientDetail.id);
    modalRef.afterClosed().subscribe(modifiedPatientDetail => {
      TCUtilsAngular.assign(this.patientDetail, modifiedPatientDetail);
    }, error => {
      console.error(error);
    });
  }

  printPatient(): void {
    this.patientPersist.print(this.patientDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print patient", true);
    });
  }

  back(): void {
    this.location.back();
  }

  //notes methods
  searchNotes(isPagination: boolean = false): void {
    let paginator = this.paginators.toArray()[PaginatorIndexes.notes];
    let sorter = this.sorters.toArray()[PaginatorIndexes.notes];
    this.notesSelected = [];
    this.notesLoading = true;
    this.receptionNotePersist.searchReceptionNote( paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
      this.notesData = response.data;
      this.notesData.forEach(note => {
        if (!this.users[note.user_id]) {
          this.users[note.user_id] = new UserDetail()
          this.userPersist.getUser(note.user_id).subscribe(
            user => {
              this.users[note.user_id] = user;
            }
          )
        }
      })
      if (response.total != -1) {
        this.notesTotalCount = response.total;
      }
      this.notesLoading = false;
    }, error => {
      this.notesLoading = false;
    });
  }


  addNote(): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), "", true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const receptionNote = new ReceptionNoteSummary()
        receptionNote.patient_id = this.patientDetail.id
        receptionNote.note = note;
        this.receptionNotePersist.addReceptionNote(receptionNote).subscribe(response => {
          this.searchNotes();
        });
      }
    });
  }

  editNote(item: ReceptionNoteSummary): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), item.note, true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        item.note = note;
        this.receptionNotePersist.updateReceptionNote(item).subscribe(response => {
          this.searchNotes();
        });
      }
    });
  }

  canUpdateNote(item: ReceptionNoteSummary): boolean {
    return this.tcAuthorization.isSysAdmin() || item.user_id == TCAppInit.userInfo.user_id
  }

  deleteNote(item: ReceptionNoteSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.receptionNotePersist.deleteReceptionNote(item.id).subscribe(response => {
          this.tcNotification.success("note deleted");
          this.searchNotes();
        }, error => {
        });
      }

    });
  }

  createUser(): void {
    this.patientPersist
      .patientDo(this.patientDetail.id, "account_create", {})
      .subscribe((response) => {
        this.patientDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }

  removeUser(): void {
    this.patientPersist
      .patientDo(this.patientDetail.id, "remove_account", {})
      .subscribe((response) => {
        this.tcNotification.success("account removed");
        this.reload();
      });

  }

  getUser(id: string): string {
    return (this.users[id]?.name || "Unknown User")
  }

}
