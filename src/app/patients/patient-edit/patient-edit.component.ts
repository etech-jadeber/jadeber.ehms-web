import {Component, Inject, Input, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCId, TCModalModes, TCParentChildIds, TCWizardProgress, TCWizardProgressModes} from "../../tc/models";


import {PatientDetail, PatientSummary} from "../patients.model";
import {PatientPersist} from "../patients.persist";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {ProcedureStatus} from "../../form_encounters/procedure_orders/procedure_order.persist";
import { FilePersist } from 'src/app/tc/files/file.persist';
import { HttpClient } from '@angular/common/http';
import { WebcamImage } from 'ngx-webcam';

import { CameraNavigator } from '../camera.navigator';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Insured_EmployeeDetail } from 'src/app/insured_employees/insured_employee.model';
import { CompanyCreditServicePackageDetail } from 'src/app/company_credit_service_package/companyCreditServicePackage.model';
import { CompanyCreditServicePackageNavigator } from 'src/app/company_credit_service_package/companyCreditServicePackage.navigator';
import { CompanyNavigator } from 'src/app/Companys/Company.navigator';
import { CompanyDetail } from 'src/app/Companys/Company.model';
import { PatientNavigator } from '../patients.navigator';
import { Insured_EmployeeNavigator } from 'src/app/insured_employees/insured_employee.navigator';
import { CompanyCreditServicePackagePersist } from 'src/app/company_credit_service_package/companyCreditServicePackage.persist';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.css']
})
export class PatientEditComponent implements OnInit {

// ********************
// separateDialCode = false;
	SearchCountryField = SearchCountryField;
	CountryISO = CountryISO;
	PhoneNumberFormat = PhoneNumberFormat;
	preferredCountries: CountryISO[] = [
		CountryISO.UnitedStates,
		CountryISO.UnitedKingdom,
	];
	phoneForm = new FormGroup({
		phone: new FormControl(undefined, [Validators.required]),
		phoneEmg: new FormControl(undefined, [Validators.required]),

	});

  year: number = 0;
  month: number = 0;
  day: number = 0;
  minDate:Date;
  maxDate:Date;


  guardianRelationShip: ProcedureStatus[] = [
    {name: 'brother', value: 'brother'},
    {name: "care_giver", value: "care_giver"},
    {name: "child", value: "child"},
    {name: "life_partner", value: "life_partner"},
    {name: "emergency_contact", value: "emergency_contact"},
    {name: "employee", value: "employee"},
    {name: "employer", value: "employer"},
    {name: "extended_family", value: "extended_family"},
    {name: "friend", value: "friend"},
    {name: "father", value: "father"},
    {name: "grandchild", value: "grandchild"},
    {name: "guardian", value: "guardian"},
    {name: "grandparent", value: "grandparent"},
    {name: "mother", value: "mother"},
    {name: "parent", value: "parent"},
    {name: "sister", value: "sister"},
    {name: "other", value: "other"},
  ];

  isLoadingResults: boolean = false;
  title: string;
  patientDetail: PatientDetail;
  birthDate: Date = new Date();
  personIsUnder18: boolean = false;
  insured_employeeDetail: Insured_EmployeeDetail;
  patientFullName: string;
  selectedCountryISO: string;
  constructor(private router:Router,
              public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              private tcStringUtil:TCUtilsString,
              public filePersist:FilePersist,
              private http:HttpClient,
              public tcUtilsString: TCUtilsString,
              private cameraNav:CameraNavigator,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<PatientEditComponent>,
              public persist: PatientPersist,
              private tCUtilsDate: TCUtilsDate,
              public packageNavigator: CompanyCreditServicePackageNavigator,
              public packagePersist: CompanyCreditServicePackagePersist,
              public insuredEmployeeNavigator: Insured_EmployeeNavigator,
              public companyNavigator: CompanyNavigator,
              public patientNavigator: PatientNavigator,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {
                this.maxDate = new Date();

  }


  profile_pic:string = 'assets/images/profile.png';


  public url:string = TCUtilsString.getInvalidId(); ;

  changePreferredCountries() {
		this.preferredCountries = [CountryISO.India, CountryISO.Canada];
	}

  onCancel(): void {
    this.dialogRef.close();
  }

  isWizard(): boolean {
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew(): boolean {
    console.log("this.ids.context['mode']",this.ids.context['mode'])
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.ids.context['mode'] == TCModalModes.EDIT;

  }

  ngOnInit() {
    if (this.isNew() || this.isWizard()) {
      this.tcAuthorization.requireCreate("patients");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText('patient', 'patient');
      this.patientDetail = new PatientDetail();
      this.patientDetail.phone_cell = "";
      this.patientDetail.email = "";
      this.patientDetail.mname = "";
      this.patientDetail.sex = false;
      this.patientDetail.lname = "";
      this.patientDetail.profile_pic = TCUtilsString.getInvalidId(); ;
      this.patientDetail.guardiansex = false;
      this.patientDetail.profile_pic = this.tcStringUtil.invalid_id;
      this.patientDetail.state = -1;
      this.patientDetail.subcity = "";
      this.patientDetail.wereda = "";
      this.patientDetail.kebele = "";
      this.patientDetail.house_no = "";
      this.patientDetail.emergency_contact_name = "";
      this.patientDetail.emergency_contact_relationship = "";
      this.patientDetail.emergency_contact_phone = "";
      this.patientDetail.occupation = "";
      this.patientDetail.tin = "";
      this.patientDetail.language_preference = "";
      this.patientDetail.driving_license = "";
      this.patientDetail.maritial_status = -1;
      this.patientDetail.educational_status = -1;
      this.patientDetail.is_free = false;
      //set necessary defaults
      this.insured_employeeDetail = new Insured_EmployeeDetail();

    } else {
      this.tcAuthorization.requireUpdate("patients");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText('patient', 'patient');
      this.isLoadingResults = true;
     
      this.persist.getPatient(this.ids.childId).subscribe(patientDetail => {
        this.patientDetail = patientDetail;
        console.log("ti",this.patientDetail)
        this.phoneForm = new FormGroup({
          phone: new FormControl(this.patientDetail.phone_cell, [Validators.required]),
		      phoneEmg: new FormControl(this.patientDetail.emergency_contact_phone, [Validators.required]),

        });
        if(patientDetail.profile_pic != this.tcStringUtil.invalid_id){
          this.patientDetail.profile_url = this.filePersist.downloadLink(patientDetail.profile_pic);
        }
        
        this.transformDates(false);
        this.birthDateChange();
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }
  onCountryChange(event: any) {
    this.selectedCountryISO = event.iso2; // Capture the selected country ISO code
    console.log('Selected country ISO code:', this.selectedCountryISO);

    // Perform any additional actions based on the selected country change
}
  ageChange(){
    this.birthDate =  new Date();

    if(!(this.year <0)&&!(this.month <0)&&!(this.day <0)){
      this.birthDate.setFullYear(  this.birthDate.getFullYear() - this.year); 
    this.birthDate.setMonth(  this.birthDate.getMonth() - this.month); 
    this.birthDate.setDate(  this.birthDate.getDate() - this.day); 
    }
  }

  birthDateChange(){
    const {year, months, day} = this.tCUtilsDate.calculateAge(this.birthDate)
    this.year = year;
    this.month = months
    this.day = day
  }

  onAdd(isWizard: boolean = false, closeDialog: boolean = true): void {
    console.log("000000000000000000000000")
    this.transformDates()
    // if(this.patientDetail.phone_cell.length > 0 && this.patientDetail.phone_cell.charAt(0) != '0'){
    //   this.patientDetail.phone_cell = '0' + this.patientDetail.phone_cell; 
    // }
    console.log("***************************************",this.phoneForm.value)
    this.patientDetail.phone_cell = this.phoneForm.value.phone.e164Number
    this.patientDetail.emergency_contact_phone = this.phoneForm.value.phoneEmg.e164Number


    this.isLoadingResults = true;
    this.patientDetail.insured_employee = this.insured_employeeDetail
    this.persist.addPatient(this.patientDetail).subscribe(value => {
      if (closeDialog) {
        if (isWizard) {
          this.dialogRef.close(new TCWizardProgress(TCWizardProgressModes.NEXT, this.patientDetail));

        } else {
          this.dialogRef.close(this.patientDetail);
        }
      }

    }, error => {
      this.isLoadingResults = false;
    })
  }

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + result[0].mname + ' ' + result[0].lname;
        this.patientDetail.guardian_id = result[0].id;
      }
    });
  }

  onContinue(): void {
    this.isLoadingResults = true;
    
    if(this.patientDetail.phone_cell.length > 0 && this.patientDetail.phone_cell.charAt(0) != '0'){
      this.patientDetail.phone_cell = '0' + this.patientDetail.phone_cell; 
    }
    this.transformDates(true);
    this.patientDetail.insured_employee = this.insured_employeeDetail
    this.persist.addPatient(this.patientDetail).subscribe(
      (value) => {
        this.tcNotification.success("Patient added");
        this.patientDetail.id = value.id;

        this.dialogRef.close(
          new TCWizardProgress(TCWizardProgressModes.NEXT, value)
        );
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }


    transformDates(todate: boolean = true) {
      if (todate) {
        this.patientDetail.dob = this.tCUtilsDate.toTimeStamp(this.birthDate);


      } else {
        this.birthDate = this.tCUtilsDate.toDate(this.patientDetail.dob);


      }
    }


  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    console.log("*******************************************************************")
    console.log()
    this.patientDetail.phone_cell = this.phoneForm.value.phone.e164Number
    this.patientDetail.emergency_contact_phone = this.phoneForm.value.phoneEmg.e164Number

    console.log(this.patientDetail);
    
    this.persist.updatePatient(this.patientDetail).subscribe(value => {
      this.tcNotification.success("Patient updated");
      this.dialogRef.close(this.patientDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {

    if (this.patientDetail == null) {

      return false;
    }

    if (this.patientDetail.fname == null || this.patientDetail.fname == "") {

      return false;
    }
    if (this.patientDetail.mname == null || this.patientDetail.mname == "") {

      return false;
    }
    // if (this.patientDetail.phone_cell == null || this.patientDetail.phone_cell == "") {
    //   console.log("this.patientDetail.phone_cell ")

    //   return false;
    // }
    if (this.phoneForm.controls['phone'].invalid) {
      return false;
    }

    // if (this.age <  18) {
    //   //validate the guardian information
    //   if (this.patientDetail.guardiansname == null || this.patientDetail.guardianrelationship == null) {
    //     return false;
    //   }
    //   if (this.patientDetail.guardiansex == null || this.patientDetail.guardianaddress == null) {
    //     return false;
    //   }
    //   if (this.patientDetail.guardianphone == null) {
    //     return false;
    //   }
    // }

    if(this.patientDetail.emergency_contact_name == null || this.patientDetail.emergency_contact_name == ""){

      return false;
    }
    if (this.phoneForm.controls['phoneEmg'].invalid) {
      return false;
    }
    // if(this.patientDetail.emergency_contact_phone == null || this.patientDetail.emergency_contact_phone == ""){
    //   console.log("555555555555555")

    //   return false;
    // }
    if(this.patientDetail.mname == null || this.patientDetail.mname == ""){

      return false;
    }

    if(this.patientDetail.emergency_contact_relationship == null || this.patientDetail.emergency_contact_relationship == ""){

      return false;
    }

    // if(this.patientDetail.maritial_status == -1 || this.patientDetail.maritial_status == null){
    //   return false;
    // }

    // if(this.patientDetail.educational_status == -1 || this.patientDetail.educational_status == null){
    //   return false;
    // }
    if(this.patientDetail.state == -1 || this.patientDetail.state == null){

      return false;
    }
    if(this.year<0||this.month<0||this.day<0){
      return false;
    }
    if(this.patientDetail.email !="" && !this.validateEmail(this.patientDetail.email)){
return false
    }

    return true;
  }


  // subscribe(next?: (value: Object) => void, error?: (error: any) => void, complete?: () => void): Subscription




  takePic(key: string, pic: string){
    this.cameraNav.takePic().afterClosed().subscribe(
      {
        next:(res:WebcamImage)=>{

         let fd: FormData = new FormData();
         const rawData = atob(res.imageAsBase64);
         const bytes = new Array(rawData.length);
         for (var x = 0; x < rawData.length; x++) {
           bytes[x] = rawData.charCodeAt(x);
          }
          const arr = new Uint8Array(bytes);
          const blob = new Blob([arr], {type: 'image/png'});


         fd.append('file', blob);

         this.http.post(this.persist.documentUploadUri(), fd).subscribe({
          next: (response) => {
            if (pic == 'profile_url'){
              this.patientDetail.profile_pic = (<TCId>response).id;
              this.filePersist.getDownloadKey(this.patientDetail.profile_pic).subscribe(
                (res:any)=>{
                    this.patientDetail.profile_url = this.filePersist.downloadLink(res.id);
                }
              );
            } else {
              this.insured_employeeDetail[key] = (<TCId>response).id;
            
           this.filePersist.getDownloadKey(this.insured_employeeDetail[key]).subscribe(
              (res:any)=>{
                  this.insured_employeeDetail[pic] = this.filePersist.downloadLink(res.id);
              }
            );
            }

          },

          error:(err)=>{
            console.log(err);

          }
        });
        },
        error:(err)=>{

        }
      }
    )
  }

  searchPackage() {
    let dialogRef = this.packageNavigator.pickCompanyCreditServicePackages(this.insured_employeeDetail.company_id, true)
    dialogRef.afterClosed().subscribe((result: CompanyCreditServicePackageDetail[]) => {
      if(result){
        this.insured_employeeDetail.package_name = result[0].name
        this.insured_employeeDetail.package_id = result[0].id;
      }
    })
  }

  searchEmployee() {
    let dialogRef = this.insuredEmployeeNavigator.pickInsured_Employees(this.insured_employeeDetail.company_id, true)
    dialogRef.afterClosed().subscribe((result: Insured_EmployeeDetail[]) => {
      if(result){
        this.insured_employeeDetail.full_name = result[0].full_name
        this.insured_employeeDetail.id = result[0].id;
        this.insured_employeeDetail.employee_id = result[0].employee_id
        this.packagePersist.getCompanyCreditServicePackage(result[0].package_id).subscribe(
          (value) => {
            this.insured_employeeDetail.package_name = value.name
            this.insured_employeeDetail.package_id = value.id
          }
        )
      }
    })
  }

  searchCompany() {
    let dialogRef = this.companyNavigator.pickCompanys(true)
    dialogRef.afterClosed().subscribe((result: CompanyDetail[]) => {
      if(result){
        this.insured_employeeDetail.company_name = result[0].name
        this.insured_employeeDetail.company_id = result[0].id;
      }
    })
  }


  documentUpload(fileInputEvent: any, key: string, pic: string): void {

    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.persist.documentUploadUri(), fd).subscribe({
      next: (response) => {
        if (pic == 'profile_url'){
          this.patientDetail.profile_pic = (<TCId>response).id;
          this.filePersist.getDownloadKey(this.patientDetail.profile_pic).subscribe(
            (res:any)=>{
  
              this.patientDetail.profile_url = this.filePersist.downloadLink(res.id);
  
            }
          );
        } else {
          this.insured_employeeDetail[key] = (<TCId>response).id;
          this.filePersist.getDownloadKey(this.insured_employeeDetail[key]).subscribe(
            (res:any)=>{
  
              this.insured_employeeDetail[pic] = this.filePersist.downloadLink(res.id);
              console.log(this.insured_employeeDetail)
  
            }
          );
        }


      },

      error:(err)=>{
        console.log(err);

      }
    }


    );
  }
  isActiveTab(routeStart: string): boolean {
    return this.router.url.startsWith(routeStart);
  }
  validateEmail(email: string): boolean {
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(email);
  }
}
