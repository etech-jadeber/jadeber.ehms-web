import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";


import { PatientDetail, PatientSummary, PatientSummaryPartialList, ReceptionNoteSummary, patientFilterColumn } from '../patients.model';
import { PatientPersist } from '../patients.persist';
import { PatientNavigator } from '../patients.navigator';
import { PaymentNavigator } from 'src/app/payments/payment.navigator';
import { TCId, TCWizardProgress, TCWizardProgressModes } from 'src/app/tc/models';
import { payment_type } from 'src/app/app.enums';
import { AppointmentNavigator } from 'src/app/appointments/appointment.navigator';
import { AppointmentDetail } from 'src/app/appointments/appointment.model';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { interval } from 'rxjs';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { MatDialogRef } from '@angular/material/dialog';
import { ReportEditComponent } from 'src/app/reports/report-edit/report-edit.component';
import { ReceptionNotePersist } from '../receptionNote.persist';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  patientsData: PatientSummary[] = [];
  patientsTotalCount: number = 0;
  patientsSelectAll: boolean = false;
  patientsSelection: PatientSummary[] = [];
  patientFilterColumn = patientFilterColumn

  patientsDisplayedColumns: string[] = ["select", "action","pid", "fname", "mname", "lname", "sex", "dob", "email", "phone_cell"];
  patientsSearchTextBox: FormControl = new FormControl();
  pidSearchBox:FormControl  = new FormControl(this.patientPersist.patientSearchHistory.pid);
  patientsIsLoading: boolean = false;
  success_state = 3;
  message: string = "";
  @ViewChild(MatPaginator, { static: true }) patientsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) patientsSort: MatSort;

  constructor(private router: Router,
    private filePersist: FilePersist,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    private paymentNavigator:PaymentNavigator,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public patientPersist: PatientPersist,
    public patientNavigator: PatientNavigator,
    private appointmentNavigator:AppointmentNavigator,
    public jobPersist: JobPersist,
    public receptionNotePersist: ReceptionNotePersist,
    private http:HttpClient,
  ) {

    this.tcAuthorization.requireRead("patients");
    this.patientsSearchTextBox.setValue(patientPersist.patientSearchHistory.search_text);
    //delay subsequent keyup events
    this.patientsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.patientPersist.patientSearchHistory.search_text = value;
      this.searchPatients();
    });

    this.pidSearchBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.patientPersist.patientSearchHistory.pid = value;
      this.searchPatients();
    });

  }

  ngOnInit() {

    this.patientsSort.sortChange.subscribe(() => {
      this.patientsPaginator.pageIndex = 0;
      this.searchPatients(true);
    });

    this.patientsPaginator.page.subscribe(() => {
      this.searchPatients(true);
    });
    //start by loading items
    this.searchPatients();
  }

  searchPatients(isPagination: boolean = false): void {

    let paginator = this.patientsPaginator;
    let sorter = this.patientsSort;

    if (!sorter.direction){
      sorter.active = "pid";
      sorter.direction = "desc";
    }

    this.patientsIsLoading = true;
    this.patientsSelection = [];

    this.patientPersist.searchPatient(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: PatientSummaryPartialList) => {
      this.patientsData = partialList.data;
      if (partialList.total != -1) {
        this.patientsTotalCount = partialList.total;
      }
      this.patientsIsLoading = false;
    }, error => {
      this.patientsIsLoading = false;
    });

  }


  downloadPatients(): void {
    if (this.patientsSelectAll) {
      this.patientPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patients", true);
      });
    }
    else {
      this.patientPersist.download(this.tcUtilsArray.idsList(this.patientsSelection)).subscribe(downloadJob => {

        this.jobPersist.followJob(downloadJob.id, "download patients", true);
      });
    }
  }



  payScheduleFee(apptId:string){
    let dialogRef = this.paymentNavigator.addAppointmentPayment(apptId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }



    });


  }
  addAppointment(patientId:string = null): void {


    let dialogRef = this.appointmentNavigator.wizardAppointment(patientId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let wizardProgress: TCWizardProgress = <TCWizardProgress>result;
        if ((wizardProgress.progressMode == TCWizardProgressModes.NEXT)  ) {

         this.payScheduleFee(wizardProgress.item.item.id);

        }
        else if(wizardProgress.item.payment_type != payment_type.card_fee){

        }
        else if (wizardProgress.progressMode == TCWizardProgressModes.FINISH) {

        }
      }

    });
  }

  addPatient(): void {
    let dialogRef = this.patientNavigator.addPatient(true);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let wizardProgress: TCWizardProgress = <TCWizardProgress>result;
        if ((wizardProgress.progressMode == TCWizardProgressModes.NEXT) || (wizardProgress.progressMode == TCWizardProgressModes.SKIP)
        ) {


         this.addAppointment(wizardProgress.item.id);
        }

        else if (wizardProgress.progressMode == TCWizardProgressModes.FINISH) {

        }
        this.searchPatients();
      }
    });
  }



  editPatient(item: PatientSummary) {
    let dialogRef = this.patientNavigator.editPatient(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePatient(item: PatientSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Patient");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.patientPersist.deletePatient(item.id).subscribe(response => {
          this.tcNotification.success("Patient deleted");
          this.searchPatients();
        }, error => {
        });
      }

    });
  }


  downloadPatient(): void {
    if (this.patientsSelectAll) {
      this.patientPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download members", true);
      });
    } else {
      this.patientPersist.download(this.tcUtilsArray.idsList(this.patientsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download members", true);
      });
    }
  }
  addNote(item: PatientSummary): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), "", true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const receptionNote = new ReceptionNoteSummary()
        receptionNote.patient_id = item.id;
        receptionNote.note = note
        this.receptionNotePersist.addReceptionNote(receptionNote).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("notes", "note_added"));
          this.searchPatients();
        });
      }
    });
  }

  printIndexCard(element: PatientDetail): void {
    this.patientPersist.patientDo(element.id, "print_index_card", {}).subscribe((downloadJob: {id: string}) => {
      this.jobPersist.followJob(downloadJob.id, "Printing Index Card", true);
    })
  }

  sendSms(item: PatientSummary): void {
    let modalRef = this.tcNavigator.textInput("Sms","", true, "Send");
    modalRef.afterClosed().subscribe(sms => {
      if (sms) {
        this.patientPersist.smsDo(item.id,"single_sms",{"message":sms}).subscribe(response => {
          this.tcNotification.success("sms is sent successfully");
        });
      }
    })
  }

  sendBulkSms(item: PatientSummary[]): void {
    let modalRef = this.tcNavigator.textInput("Bulk Sms","", true);
    if (this.patientsSelectAll) {
      this.patientPersist.sendSmsAll().subscribe(response => {
        this.tcNotification.success("sms is sent successfully");
      });
    }
    else {
      modalRef.afterClosed().subscribe(sms => {
        if (sms) {
          this.patientPersist.sendSmsSelected(this.tcUtilsArray.idsList(this.patientsSelection).concat([sms])).subscribe(response => {
            this.tcNotification.success("sms is sent successfully");
          });
        }
      })

    }
  }

  importPatient(fileInputEvent: any): void {
    if (fileInputEvent.target.files[0].size > 20000000) {
      this.tcNotification.error("File too big");
      return;
    }

    if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
      this.tcNotification.error("Unsupported file type");
      return;
    }

    let fd: FormData = new FormData();
    fd.append('file', fileInputEvent.target.files[0]);
    this.http.post(this.patientPersist.uploadPatient(), fd).subscribe({
      next: (uplodJob: TCId) => {
        this.jobPersist.followJob(uplodJob.id, "uploading patients", false, () => this.searchPatients());
        
      },

      error:(err)=>{
        console.log(err);
        
      }
    }
    

    );
  }

  back(): void {
    this.location.back();
  }

}
