import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientPickComponent } from './patient-pick.component';

describe('PatientPickComponent', () => {
  let component: PatientPickComponent;
  let fixture: ComponentFixture<PatientPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
