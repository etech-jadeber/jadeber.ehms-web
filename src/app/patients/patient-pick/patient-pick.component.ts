import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {PatientDetail, PatientSummary, PatientSummaryPartialList} from "../patients.model";
import {PatientPersist} from "../patients.persist";


@Component({
  selector: 'app-patient-pick',
  templateUrl: './patient-pick.component.html',
  styleUrls: ['./patient-pick.component.css']
})
export class PatientPickComponent implements OnInit {

  patientsData: PatientSummary[] = [];
  patientsTotalCount: number = 0;
  patientsSelection: PatientSummary[] = [];
  patientsDisplayedColumns: string[] = ["select",  'pid', 'fname','mname','lname','sex','dob','email','phone_cell' ];

  patientsSearchTextBox: FormControl = new FormControl();
  patientsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) patientsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public patientPersist: PatientPersist,
              public dialogRef: MatDialogRef<PatientPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
   // this.tcAuthorization.requireRead("patients");
    this.patientsSearchTextBox.setValue(patientPersist.patientSearchHistory.search_text);
    //delay subsequent keyup events
    this.patientsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.patientPersist.patientSearchHistory.search_text = value;
      this.searchPatients();
    });
  }

  ngOnInit() {

    this.patientsSort.sortChange.subscribe(() => {
      this.patientsPaginator.pageIndex = 0;
      this.searchPatients();
    });

    this.patientsPaginator.page.subscribe(() => {
      this.searchPatients();
    });

    //set initial picker list to 5
    this.patientsPaginator.pageSize = 5;

    //start by loading items
    this.searchPatients();
  }

  searchPatients(): void {
    this.patientsIsLoading = true;
    this.patientsSelection = [];

    this.patientPersist.searchPatient(this.patientsPaginator.pageSize,
        this.patientsPaginator.pageIndex,
        this.patientsSort.active,
        this.patientsSort.direction).subscribe((partialList: PatientSummaryPartialList) => {
      this.patientsData = partialList.data;
      if (partialList.total != -1) {
        this.patientsTotalCount = partialList.total;
      }
      this.patientsIsLoading = false;
    }, error => {
      this.patientsIsLoading = false;
    });

  }

  markOneItem(item: PatientSummary) {
    if(!this.tcUtilsArray.containsId(this.patientsSelection,item.id)){
          this.patientsSelection = [];
          this.patientsSelection.push(item);
        }
        else{
          this.patientsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.patientsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}