import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { CommentDetail, CommentSummaryPartialList, TCId } from "../tc/models";
import { TCDoParam, TCUtilsHttp } from "../tc/utils-http";
import { TCUtilsString } from "../tc/utils-string";
import { PatientNoteSummary, PatientNoteSummaryPartialList } from "./patients.model";

@Injectable({
    providedIn: 'root'
  })
  export class PatientNotePersist {

    constructor(private http: HttpClient) {
    }
    noteSearchText: string = "";
    patientId: string = "";
    encounterId: string = "";

  searchPatientNote(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PatientNoteSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patient_notes/", this.noteSearchText, pageSize, pageIndex, sort, order);
    if (this.patientId){
        url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId)
    }
    if (this.encounterId){
        url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
    }
    return this.http.get<PatientNoteSummaryPartialList>(url);

  }

  getPatientNote( id: string): Observable<PatientNoteSummary> {
    return this.http.get<PatientNoteSummary>(environment.tcApiBaseUri + "patient_notes/" + id);
  }


  addPatientNote(item: PatientNoteSummary): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patient_notes/", item);
  }

  updatePatientNote(item: PatientNoteSummary): Observable<PatientNoteSummary> {
    return this.http.patch<PatientNoteSummary>(environment.tcApiBaseUri + "patient_notes/" + item.id, item);
  }

  deletePatientNote(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "patient_notes/" + id);
  }

  patientNotesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient_notes/do", new TCDoParam(method, payload));
  }

  patientNoteDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patients_notes/" + id + "/do", new TCDoParam(method, payload));
  }
}