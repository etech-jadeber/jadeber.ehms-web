import {Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {FormControl} from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {AppTranslation} from 'src/app/app.translation';
import {UserContexts} from 'src/app/tc/app.enums';
import {TCAuthentication} from 'src/app/tc/authentication';
import {TCAuthorization} from 'src/app/tc/authorization';
import {TCNavigator} from 'src/app/tc/navigator';
import {TCNotification} from 'src/app/tc/notification';
import {UserPersist} from 'src/app/tc/users/user.persist';
import {TCUtilsAngular} from 'src/app/tc/utils-angular';
import {TCUtilsArray} from 'src/app/tc/utils-array';
import {TCUtilsCountry} from 'src/app/tc/utils-country';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {TCUtilsString} from 'src/app/tc/utils-string';
import {environment} from 'src/environments/environment';
import {PatientSelfDashboard} from '../patients.model';
import {PatientPersist} from '../patients.persist';
import {Location} from '@angular/common';
import {AppointmentSummary, AppointmentSummaryPartialList} from "../../appointments/appointment.model";
import {AppointmentPersist} from "../../appointments/appointment.persist";
import {DoctorSummary} from "../../doctors/doctor.model";
import {DoctorPersist} from "../../doctors/doctor.persist";
import {debounceTime} from "rxjs/operators";
import {PatientNavigator} from "../patients.navigator";
import { TCAppInit } from 'src/app/tc/app-init';


export enum PatientTabs {
  overview,
  profile,
  appointment

}

export enum PaginatorIndexes {


}

@Component({
  selector: 'app-patients-home',
  templateUrl: './patients-home.component.html',
  styleUrls: ['./patients-home.component.css']
})
export class PatientsHomeComponent implements OnInit, OnDestroy {

  title = environment.appName;
  allowRoute: boolean = false;
  patientLoading: boolean = false;
  message: string = "";
  patientdetail: PatientSelfDashboard;

  patientsAppointmentSummary: AppointmentSummary[] = [];
  patientsAppointmentCount: number = 0;
  patientAppointmentDisplayedColumns = [];
  appointmentsDisplayedColumns: string[] = ["schedule_type", "scheduled_user_id", "schedule_start_time", "schedule_end_time", "closed"];
  doctorSummary: DoctorSummary [] = [];
  dateSearchBox: Date = new Date();

  profileTab: typeof PatientTabs = PatientTabs;
  activeTab: PatientTabs = PatientTabs.overview;


  coopsavingsDisplayedColumns: string[] = ['saving_amount', 'transaction_reference', 'reg_date', 'remark'];
  coopsavingsSearchTextBox: FormControl = new FormControl();
  coopsavingsLoading: boolean = false;

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  TCAppInit  = TCAppInit;

  constructor(
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsAngular: TCUtilsAngular,
    public appTranslation: AppTranslation,
    public tcUtilsDate: TCUtilsDate,
    public userPersist: UserPersist,
    public tcUtilsCountry: TCUtilsCountry,
    public tcUtilsArray: TCUtilsArray,
    public tcNavigator: TCNavigator,
    public tcAuthentication: TCAuthentication,
    public appointmentPersist: AppointmentPersist,
    public patientPersisit: PatientPersist,
    public doctorPersist: DoctorPersist,
    public patientNavigation: PatientNavigator,
  ) {
    this.patientdetail = new PatientSelfDashboard();
  }

  ngOnInit() {
    this.tcAuthorization.requireLogin();
    this.patientPersisit.initPatientSelf();
  }

  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }


  ngAfterViewInit(): void {

    let patientId: string = this.tcAuthorization.getUserContexts(UserContexts.patient)[0];
    this.message = "member id: " + patientId;
    this.loadDetails(patientId);
    this.getAllDoctors();


  };

  fullName(patient: PatientSelfDashboard): string {
    return patient.fname + " " + patient.mname + " " + patient.lname;

  }

  loadDetails(patientId: string) {
    this.patientPersisit.getPatientSelf().subscribe(patientSelfDashboard => {
        this.patientdetail = patientSelfDashboard;
        this.patientLoading = false;
      }
      , error => {
        console.error(error);
        this.patientLoading = false;
      });
  }

  getPatientSelfSchedule() {
    this.appointmentPersist.searchAppointment(100, 0, "schedule_start_time", "desc").subscribe(
      (appointments: AppointmentSummaryPartialList) => {
        this.patientsAppointmentSummary = appointments.data;

        this.patientsAppointmentCount = appointments.total;
      },
      err => {
        console.log(err);

      }
    )
  }

  requestGeneral(): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("case", "write_your_request"), "", true, this.appTranslation.getText("general", "submit"), this.appTranslation.getText("general", "cancel"));
    modalRef.afterClosed().subscribe(request => {
      if (request) {
        this.patientPersisit.patientSelfDo("request_general", {"request": request}).subscribe(response => {
          this.tcNotification.success("Request submitted");
          this.patientdetail.has_general_case_open = true;
        });
      }
    });
  }


  requestAppointment(): void {
    let dialogRef = this.patientNavigation.addAppointment();
    dialogRef.afterClosed().subscribe(appointment => {
      if (appointment) {
        // this.getPatientSelfSchedule();
        this.patientPersisit.patientSelfDo("appointment_case", {"request": appointment}).subscribe(response => {
          this.tcNotification.success("Appointment Request submitted");
          this.patientdetail.has_appointment_case_open = true;
        });
      }
    });
  }

  comingSoon() {
    this.tcNotification.info("coming soon");
  }

  getTime(date: any) {
    return `${date.hour}:${date.minute}`
  }

  back(): void {
    this.location.back();
  }

  getDoctorFullName(doctorId: string): string {
    let doctors: DoctorSummary = this.tcUtilsArray.getById(
      this.doctorSummary,
      doctorId
    );
    if (doctors) {
      return `${doctors.first_name} ${doctors.middle_name} ${doctors.last_name}`;
    }
  }

  getAllDoctors(): void {
    this.doctorPersist.doctorsDo("get_all_doctors", {}).subscribe((doctors: DoctorSummary[]) => {
      this.doctorSummary = doctors;
    });
  }

  ngOnDestroy(): void {
    // this.appointmentPersist.clearFilters();
  }

  resetFields(isDate: boolean, isScheduleType: boolean, isClosed: boolean) {
    if (isDate){
      this.appointmentPersist.appointmentSearchHistory.schedule_type = 0;
      this.appointmentPersist.closed = null;
      this.dateSearchBox = new Date(this.dateSearchBox);
    }
    else if (isScheduleType){
      this.appointmentPersist.closed = null;
      this.dateSearchBox = null;
    }
    else if (isClosed){
      this.appointmentPersist.appointmentSearchHistory.schedule_type = 0;
      this.dateSearchBox = null;
    }
  }

  logout(){
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }
}
