import { Insured_EmployeeDetail } from '../insured_employees/insured_employee.model';
import { TCId, TCString } from '../tc/models';
import { CommentSummary } from '../tc/models';

export type Prefixed<T> = {
  [K in keyof T as `patient_${string & K}`]: T[K];
};

export type PrefixedPatient = Prefixed<PatientDetail>;

export class PatientSummary extends TCId {
  fname: string;
  mname: string;
  lname: string;
  sex: boolean;
  dob: number;
  email: string;
  pid: number;
  phone_cell: string;
  guardiansname: string;
  guardianrelationship: string;
  guardianaddress: string;
  guardiansex: boolean;
  guardianphone: string;
  login_id: string;
  date: any;
  kebele: string;
  emergency_contact_name: string;
  emergency_contact_relationship: string;
  emergency_contact_phone: string;
  language_preference: string;
  driving_license: string;
  house_no: string;
  tin: string;
  reg_date: number;
  uuidd:string;
  educational_status:number;
  maritial_status:number;
  occupation: string;
  state: number;
  subcity: string;
  wereda: string;
  profile_pic: string;
  last_visit: string;
  profile_url: string;
  is_credit: boolean;
  insured_employee: Insured_EmployeeDetail;
  guardian_id: string;
  is_free:boolean


}
export class ReceptionNoteSummaryPartialList {
  data: ReceptionNoteSummary[];
  total: number;
}

export class ReceptionNoteSummary extends TCId {
  user_id: string;
  patient_id: string;
  note: string;
  date: number;

}

export class PatientNoteSummaryPartialList {
  data: PatientNoteSummary[];
  total: number;
}

export class PatientNoteSummary extends TCId {
  provider_id: string;
  patient_id: string;
  encounter_id: string;
  note: string;
  date: number;

}

export class GeneralPatientDetail {
  patient_name: string;
  dob: number;
  sex: boolean
  pid: number;
}

export class PatientSummaryPartialList {
  data: PatientSummary[];
  total: number;
}

export class PatientDetail extends PatientSummary {
  fname: string;
  mname: string;
  lname: string;
  sex: boolean;
  dob: number;
  email: string;
  pid: number;
  phone_cell: string;
  guardiansname: string;
  guardianrelationship: string;
  guardianaddress: string;
  guardiansex: boolean;
  guardianphone: string;
  state: number;
  subcity: string;
  wereda: string;
  kebele: string;
  occupation: string;
  emergency_contact_name: string;
  emergency_contact_relationship: string;
  emergency_contact_phone: string;
  language_preference: string;
  driving_license: string;
  guardian_id: string;
}

export class PatientDashboard {
  total: number;
  male_count;
  female_count;
}

export class Stat {
  sex: string;
  infant: number;
  child: number;
  young: number;
  old: number;
}
export class PatientStatistics {
  data: Stat[];
}

export class PatientSelfDashboard extends PatientSummary {
  has_general_case_open: boolean;
  has_appointment_case_open: boolean;
}

export class Openemr_Postcalendar_EventsSummary extends TCId {
  pc_pid: number;
  pc_time: string;
  pc_eventDate: string;
}

export class Openemr_Postcalendar_EventsSummaryPartialList {
  data: Openemr_Postcalendar_EventsSummary[];
  total: number;
}

export class Openemr_Postcalendar_EventsDetail extends Openemr_Postcalendar_EventsSummary {}

export class Openemr_Postcalendar_EventsDashboard {
  total: number;
}

export class History_DataSummary extends TCId {
  history_siblings: string;
  history_father: string;
  history_mother: string;
  history_offspring: string;
  history_spouse: string;
  coffee: string;
  alcohol: string;
  tobacco: string;
  pid: string;
}

export class History_DataSummaryPartialList {
  data: History_DataSummary[];
  total: number;
}

export class History_DataDetail extends History_DataSummary {}

export class Doctor_Private_InfoSummary extends TCId {
  doctor_id: string;
  patient_id: string;
  note: string;
}

export class Doctor_Private_InfoSummaryPartialList {
  data: Doctor_Private_InfoSummary[];
  total: number;
}

export class Doctor_Private_InfoDetail extends Doctor_Private_InfoSummary {}

export class ICD10_report {
  name: string;
  infant: number;
  child: number;
  teen: number;
  man: number;
  total: number;
}


export class SummarySheetSummary extends TCId {
  date:number;
  chief_compliant :string;
  diagnosis:string;
  doctor:string;
  service_type:number;
  schedule_type:number;
   
  }
  export class SummarySheetSummaryPartialList {
    data: SummarySheetSummary[];
    total: number;
  }
  export class SummarySheetDetail extends SummarySheetSummary {
  }
  
  export class SummarySheetDashboard {
    total: number;
  }

  export const prefixPatientFilterColumn: TCString[] = [
    new TCString( "patient.pid", 'MRN'),
    new TCString( "patient.fname", 'First Name'),
    new TCString("patient.mname", 'Middle Name'),
    new TCString( "patient.lname", 'Last Name'),
    new TCString( "patient.phone_cell", 'Phone Number'),
    ];
  
  export const prefixTriageFilterColumn: TCString[] = [
    new TCString( "patient.pid", 'MRN'),
    new TCString( "patient.full_name", 'Patient Name'),
    new TCString("physician.full_name", 'Doctor Name'),
  ];
  
  export const patientFilterColumn: TCString[] = [
    new TCString( "pid", 'MRN'),
    new TCString( "fname", 'First Name'),
    new TCString("mname", 'Middle Name'),
    new TCString( "lname", 'Last Name'),
    new TCString( "phone_cell", 'Phone Number'),
    ];
  
  export function getPrefixPatientName(patient: PrefixedPatient){
    return `${patient.patient_fname} ${patient.patient_mname} ${patient.patient_lname}`
  }
  
  export function getPatientName(patient: PatientDetail){
    return `${patient.fname} ${patient.mname} ${patient.lname}`
  }