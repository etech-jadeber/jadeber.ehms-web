import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {PatientEditComponent} from "./patient-edit/patient-edit.component";
import {PatientPickComponent} from "./patient-pick/patient-pick.component";
import {GeneralCaseComponent} from "../cases/general-case/general-case.component";
import {PatientPersist} from "./patients.persist";
import {PatientAppointmentEditComponent} from "./patient-appointment-edit/patient-appointment-edit.component";
import {PatientAppointmentCaseComponent} from "../cases/patient-appointment-case/patient-appointment-case.component";
import { History_DataEditComponent } from "./history-data-edit/history-data-edit.component";
import { Doctor_Private_InfoEditComponent } from "./doctor-private-info-edit/doctor-private-info-edit.component";


@Injectable({
  providedIn: 'root'
})

export class PatientNavigator {

  constructor(private router: Router,
              public dialog: MatDialog,
              private patientPersisit: PatientPersist) {
  }


  processCaseGeneral(caseId: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(GeneralCaseComponent, {
      data: caseId,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  processAppointmentCase(caseId: string): MatDialogRef<unknown,any> {
    return this.dialog.open(PatientAppointmentCaseComponent, {
      data: caseId,
      width: TCModalWidths.large,
    });
  }


  goHome(): void {
    this.router.navigateByUrl("/patient-home");
  }

  patientsUrl(): string {
    return "/patients";
  }

  patientUrl(id: string): string {
    return "/patients/" + id;
  }

  diagnosisReportUrl(): string {
    return "/diagnosis_report";
  }

  viewPatients(): void {
    this.router.navigateByUrl(this.patientsUrl());
  }


  viewMales(): void {
    this.viewPatients();
    // this.patientPersisit.genderFilter = '1';
  }


  viewFemales(): void {
    this.viewPatients();
    // this.patientPersisit.genderFilter = '0';
  }


  viewPatient(id: string): void {
    this.router.navigateByUrl(this.patientUrl(id));
  }

  editPatient(id: string, isWizard: boolean = false): MatDialogRef<unknown,any> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = isWizard
      ? TCModalModes.WIZARD
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
    const dialogRef = this.dialog.open(PatientEditComponent, {
      data: new TCParentChildIds(null, id, params),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  addAppointment(): MatDialogRef<PatientAppointmentEditComponent> {
    return this.dialog.open(PatientAppointmentEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
  }

  addPatient(isWizard: boolean = false): MatDialogRef<unknown,any> {
    return this.editPatient(null, true);
  }

  pickPatients(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PatientPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  editHistory_Data(parentId:string, id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(History_DataEditComponent, {
      data:  new TCParentChildIds(parentId, id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addHistory_Data(parentId:string, ): MatDialogRef<unknown,any> {
    return this.editHistory_Data(parentId,null);
  }


//doctor_private_infos
editDoctor_Private_Info(parentId:string, id: string): MatDialogRef<unknown,any> {
  const dialogRef = this.dialog.open(Doctor_Private_InfoEditComponent, {
    data:  new TCParentChildIds(parentId, id),
    width: TCModalWidths.medium
  });
  return dialogRef;
}

addDoctor_Private_Info(parentId:string, ): MatDialogRef<unknown,any> {
  return this.editDoctor_Private_Info(parentId,null);
}
}
