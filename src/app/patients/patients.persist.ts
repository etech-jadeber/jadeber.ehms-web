import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TCEnum} from "../tc/models";
import {Doctor_Private_InfoDetail, Doctor_Private_InfoSummaryPartialList, History_DataDetail, History_DataSummaryPartialList, Openemr_Postcalendar_EventsSummary, PatientDashboard, PatientDetail, PatientSelfDashboard, PatientSummaryPartialList} from './patients.model';
import { TCAuthorization } from '../tc/authorization';
import { UserContexts } from '../tc/app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { AppointmentSummaryPartialList} from "../appointments/appointment.model";
import { MaritalStatus, PatientEducationalStatus, schedule_type, ServiceType, State } from '../app.enums';
import { TCUtilsArray } from '../tc/utils-array';


@Injectable({
  providedIn: 'root'
})
export class PatientPersist {

  // patientSearchText: string = "";
  // patientSearchColumn: string = "";
  // genderFilter:string = '-1';
  // pid:string = '';
  infant: number = 1;
  child: number = 5;
  teen: number = 15;
  // from_date: number;
  // to_date: number;
  // start_date: number;
  // end_date: number;
  // sex: number;
  // diagnosisReportSearchText:string =""
  patient_sms_type: number ;

  constructor(private http: HttpClient, private tcAuthorization:TCAuthorization, public tcUtilsArray: TCUtilsArray) {
  }

  getMartialStatus(maritial_status: number){
    if(!maritial_status)
      return ''
    return maritial_status != -1 ? `Martial Status: ${this.tcUtilsArray.getEnum(this.maritalStatus, maritial_status)?.name}` : ''
  }

  uploadPatient() {
    return environment.tcApiBaseUri + 'patients/upload_patient';
  }


  documentUploadUri() {
    return environment.tcApiBaseUri + 'patients/upload-image/';
  }

  patientSearchHistory = {
    "sex": "-1",
    "pid": "",
    "search_text": "",
    "search_column": "",
    "is_exact": false,
}

defaultPatientSearchHistory = {...this.patientSearchHistory}

resetPatientSearchHistory(){
  this.patientSearchHistory = {...this.defaultPatientSearchHistory}
}

  searchPatient(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.patientSearchHistory): Observable<PatientSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("patients", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory);
    return this.http.get<PatientSummaryPartialList>(url);

  }

  // clearFilters(): void {
  //   this.patientSearchText = "";
  //   this.genderFilter = '-1';

  // }




  filters(searchHistory = this.patientSearchHistory): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/do", new TCDoParam("download_all", this.filters()));
  }

  patientDashboard(): Observable<PatientDashboard> {
    return this.http.get<PatientDashboard>(environment.tcApiBaseUri + "patients/dashboard");
  }

  getPatient(id: any): Observable<PatientDetail> {
    return this.http.get<PatientDetail>(environment.tcApiBaseUri + "patients/" + id);
  }

  addPatient(item: PatientDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/", item);
  }

  updatePatient(item: PatientDetail): Observable<PatientDetail> {
    return this.http.patch<PatientDetail>(environment.tcApiBaseUri + "patients/" + item.id, item);
  }

  deletePatient(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patients/" + id);
  }

  patientReportSearchHistory = {
    "from_date": undefined,
    "to_date": undefined,
    "search_text": "",
    "search_column": "",
  }

  defaultPatientReportSearchHistory = {...this.patientReportSearchHistory}

  resetPatientReportSearchHistory(){
  this.patientReportSearchHistory = {...this.defaultPatientReportSearchHistory}
  }

  patientsDo(method: string, payload: any,pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.patientReportSearchHistory): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("patients/do", searchHistory.search_text, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  patientDo2(pageSize: number, pageIndex: number, sort: string, order: string,id: string, method: string, payload: any): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("patients/"+id+"/do", "", pageSize, pageIndex, sort, order);
    console.log("url",url,"id",id)

    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  patientDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patients/" + id + "/do", new TCDoParam(method, payload));
  }

  diagnosisReportSearchHistory = {
    "start_date": undefined,
    "end_date": undefined,
    "sex": "-1",
    "search_text": "",
    "search_column": "",
  }

  defaultDiagnosisReportSearchHistory = {...this.diagnosisReportSearchHistory}

  resetDiagnosisReportSearchHistory(){
  this.diagnosisReportSearchHistory = {...this.defaultDiagnosisReportSearchHistory}
  }

  diagnosisReportDo(method: string, payload: any,pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.diagnosisReportSearchHistory): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("diagnosis_report/do", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "patients/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "patients/" + id + "/do", new TCDoParam("print", {}));
  }

  //sms
  smsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patients/" + parentId + "/sms/do", new TCDoParam(method, payload));
  }

  myPatientId :string = null;

  initPatientSelf(): void {
    let patientIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.patient);
    if (patientIds.length == 1) {
      this.myPatientId = patientIds[0];
    }
  }

  patientSelfDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patient-self/do", new TCDoParam(method, payload));
  }

  getPatientSelf(): Observable<PatientSelfDashboard> {
    return this.http.get<PatientSelfDashboard>(environment.tcApiBaseUri + "patient-self");
  }

  getPatientScheduleSelf(): Observable<AppointmentSummaryPartialList> {
    return this.http.get<AppointmentSummaryPartialList>(environment.tcApiBaseUri + "patient-self/searchAppointment");
  }
  sendSmsAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/sms/do", new TCDoParam("bulk_sms", this.filters()));
  }

  sendSmsSelected(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/sms/do", new TCDoParam("selected_sms", ids));
}


  //history_datas
  history_dataSearchText: string = "";
  searchHistory_Data(parentId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<History_DataSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "patients/" + parentId+  "/history_datas/", this.history_dataSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<History_DataSummaryPartialList>(url);

  }

  getHistory_Data(parentId:string, id: string): Observable<History_DataDetail> {
      return this.http.get<History_DataDetail>(environment.tcApiBaseUri + "patients/" + parentId+  "/history_datas/" + id);
   }


  addHistory_Data(parentId:string,item: History_DataDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "/history_datas/", item);
  }

  updateHistory_Data(parentId:string, item: History_DataDetail): Observable<History_DataDetail> {
    return this.http.patch<History_DataDetail>(environment.tcApiBaseUri + "history_datas/" + item.id, item);
  }

  deleteHistory_Data(parentId:string, id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri +"/history_datas/" + id);
  }

  history_datasDo(parentId:string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patients/" + parentId+ "/history_datas/do", new TCDoParam(method, payload));
  }

  history_dataDo(parentId:string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>( environment.tcApiBaseUri + "patients/" + parentId+ "/history_datas/" + id + "/do", new TCDoParam(method, payload));
  }



  //doctor_private_infos
  doctor_private_infoSearchText: string = "";
  searchDoctor_Private_Info(parentId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Doctor_Private_InfoSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl( "patients/" + parentId+  "/doctor_private_infos/", this.doctor_private_infoSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Doctor_Private_InfoSummaryPartialList>(url);

  }

  getDoctor_Private_Info(parentId:string, id: string): Observable<Doctor_Private_InfoDetail> {
      return this.http.get<Doctor_Private_InfoDetail>(environment.tcApiBaseUri + "patients/" + parentId+  "/doctor_private_infos/" + id);
   }


  addDoctor_Private_Info(parentId:string,item: Doctor_Private_InfoDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "patients/" + parentId+ "/doctor_private_infos/", item);
  }

  updateDoctor_Private_Info(parentId:string, item: Doctor_Private_InfoDetail): Observable<Doctor_Private_InfoDetail> {
    return this.http.patch<Doctor_Private_InfoDetail>(environment.tcApiBaseUri + "patients/" + parentId+ "/doctor_private_infos/" + item.id, item);
  }

  deleteDoctor_Private_Info(parentId:string, id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "patients/" + parentId+ "/doctor_private_infos/" + id);
  }

  doctor_private_infosDo(parentId:string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "patients/" + parentId+ "/doctor_private_infos/do", new TCDoParam(method, payload));
  }

  doctor_private_infoDo(parentId:string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>( environment.tcApiBaseUri + "patients/" + parentId+ "/doctor_private_infos/" + id + "/do", new TCDoParam(method, payload));
  }

gender:string[] = ["Male","Female"];



state: TCEnum[] = [
  new TCEnum(State.addisAbaba, 'Addis Ababa'),
  new TCEnum(State.diredawa, 'Dire Dawa'),
  new TCEnum(State.sidama, 'Sidama'),
  new TCEnum(State.somalia, 'Somalia'),
  new TCEnum(State.south, 'South'),
  new TCEnum(State.south_west, 'South West'),
  new TCEnum(State.harar, 'Harrar'),
  new TCEnum(State.gambella, 'Gambella'),
  new TCEnum(State.tigray, 'Tigray'),
  new TCEnum(State.amhara, 'Amhara'),
  new TCEnum(State.afar, 'Afar'),
  new TCEnum(State.oromia, 'Oromia'),
  new TCEnum(State.benishangul, 'Benishangul'),

];

educationalStatus: TCEnum[] = [
  new TCEnum(PatientEducationalStatus.uneducated, 'Uneducated'),
  new TCEnum(PatientEducationalStatus.elementary1, 'Elementary [<4] '),
  new TCEnum(PatientEducationalStatus.elementary2, 'Elementary [5:8]'),
  new TCEnum(PatientEducationalStatus.highschool, 'Highschool [9:10]  '),
  new TCEnum(PatientEducationalStatus.preparatory, 'Preparatory [11:12]'),
  new TCEnum(PatientEducationalStatus.tvet, 'TVET'),
  new TCEnum(PatientEducationalStatus.university, 'University'),
  new TCEnum(PatientEducationalStatus.degree, 'Degree'),
  new TCEnum(PatientEducationalStatus.master, 'Masters'),
  new TCEnum(PatientEducationalStatus.phd, 'Phd'),

];


maritalStatus: TCEnum[] = [
  new TCEnum( MaritalStatus.single, 'single'),
  new TCEnum( MaritalStatus.married, 'married'),
  new TCEnum( MaritalStatus.widowed, 'widowed'),
  new TCEnum( MaritalStatus.divorced, 'divorced'),
  new TCEnum( MaritalStatus.separated, 'separated'),
];

schedule_type: TCEnum[] = [
  new TCEnum(schedule_type.new_patient, "New Patient"),
  new TCEnum(schedule_type.revisit, "Revisit"),
  new TCEnum(schedule_type.repayment, "Repayment"),
  new TCEnum(schedule_type.repeat, "Repeat"),
  new TCEnum(schedule_type.follow_up, "Follow Up"),
];

service_type: TCEnum[] = [
  new TCEnum(ServiceType.emergency, "Emergency"),
  new TCEnum(ServiceType.inpatient, "Inpatient"),
  new TCEnum(ServiceType.outpatient, "Outpatient"),
  new TCEnum(ServiceType.outsideOrder,"Outpatient"),
];
}




