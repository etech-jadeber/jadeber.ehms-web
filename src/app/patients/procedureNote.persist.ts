import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { ProcedureNoteSummary, ProcedureNoteSummaryPartialList } from "../procedures/procedure.model";
import { TCId } from "../tc/models";
import { TCDoParam, TCUtilsHttp } from "../tc/utils-http";
import { TCUtilsString } from "../tc/utils-string";

@Injectable({
    providedIn: 'root'
  })
  export class ProcedureNotePersist {

    constructor(private http: HttpClient) {
    }
    noteSearchText: string = "";
    procedureId: string = "";

  searchProcedureNote(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ProcedureNoteSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_notes/", this.noteSearchText, pageSize, pageIndex, sort, order);
    if (this.procedureId){
      url = TCUtilsString.appendUrlParameter(url, "patient_procedure_id", this.procedureId);
    }
    return this.http.get<ProcedureNoteSummaryPartialList>(url);

  }

  getProcedureNote( id: string): Observable<ProcedureNoteSummary> {
    return this.http.get<ProcedureNoteSummary>(environment.tcApiBaseUri + "procedure_notes/" + id);
  }


  addProcedureNote(item: ProcedureNoteSummary): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_notes/", item);
  }

  updateProcedureNote(item: ProcedureNoteSummary): Observable<ProcedureNoteSummary> {
    return this.http.patch<ProcedureNoteSummary>(environment.tcApiBaseUri + "procedure_notes/" + item.id, item);
  }

  deleteProcedureNote(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "procedure_notes/" + id);
  }

  procedureNotesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_notes/do", new TCDoParam(method, payload));
  }

  procedureNoteDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedures_notes/" + id + "/do", new TCDoParam(method, payload));
  }
}