import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { TCId } from "../tc/models";
import { TCDoParam, TCUtilsHttp } from "../tc/utils-http";
import { TCUtilsString } from "../tc/utils-string";
import { ReceptionNoteSummary, ReceptionNoteSummaryPartialList } from "./patients.model";

@Injectable({
    providedIn: 'root'
  })
  export class ReceptionNotePersist {

    constructor(private http: HttpClient) {
    }
    noteSearchText: string = "";
    patientId: string = "";

  searchReceptionNote(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ReceptionNoteSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("reception_notes/", this.noteSearchText, pageSize, pageIndex, sort, order);
    if (this.patientId){
      url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patientId);
    }
    return this.http.get<ReceptionNoteSummaryPartialList>(url);

  }

  getReceptionNote( id: string): Observable<ReceptionNoteSummary> {
    return this.http.get<ReceptionNoteSummary>(environment.tcApiBaseUri + "reception_notes/" + id);
  }


  addReceptionNote(item: ReceptionNoteSummary): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "reception_notes/", item);
  }

  updateReceptionNote(item: ReceptionNoteSummary): Observable<ReceptionNoteSummary> {
    return this.http.patch<ReceptionNoteSummary>(environment.tcApiBaseUri + "reception_notes/" + item.id, item);
  }

  deleteReceptionNote(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "reception_notes/" + id);
  }

  receptionNotesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "reception_notes/do", new TCDoParam(method, payload));
  }

  receptionNoteDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "receptions_notes/" + id + "/do", new TCDoParam(method, payload));
  }
}