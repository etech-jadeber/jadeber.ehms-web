import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDiscountRequestDetailComponent } from './payment-discount-request-detail.component';

describe('PaymentDiscountRequestDetailComponent', () => {
  let component: PaymentDiscountRequestDetailComponent;
  let fixture: ComponentFixture<PaymentDiscountRequestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentDiscountRequestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDiscountRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
