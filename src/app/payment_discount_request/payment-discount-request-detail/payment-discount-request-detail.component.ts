import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PaymentDiscountRequestDetail} from "../payment_discount_request.model";
import {PaymentDiscountRequestPersist} from "../payment_discount_request.persist";
import {PaymentDiscountRequestNavigator} from "../payment_discount_request.navigator";

export enum PaymentDiscountRequestTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-payment_discount_request-detail',
  templateUrl: './payment-discount-request-detail.component.html',
  styleUrls: ['./payment-discount-request-detail.component.scss']
})
export class PaymentDiscountRequestDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  paymentDiscountRequestIsLoading:boolean = false;
  
  PaymentDiscountRequestTabs: typeof PaymentDiscountRequestTabs = PaymentDiscountRequestTabs;
  activeTab: PaymentDiscountRequestTabs = PaymentDiscountRequestTabs.overview;
  //basics
  paymentDiscountRequestDetail: PaymentDiscountRequestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public paymentDiscountRequestNavigator: PaymentDiscountRequestNavigator,
              public  paymentDiscountRequestPersist: PaymentDiscountRequestPersist) {
    this.tcAuthorization.requireRead("payment_discount_requests");
    this.paymentDiscountRequestDetail = new PaymentDiscountRequestDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("payment_discount_requests");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.paymentDiscountRequestIsLoading = true;
    this.paymentDiscountRequestPersist.getPaymentDiscountRequest(id).subscribe(paymentDiscountRequestDetail => {
          this.paymentDiscountRequestDetail = paymentDiscountRequestDetail;
          this.paymentDiscountRequestIsLoading = false;
        }, error => {
          console.error(error);
          this.paymentDiscountRequestIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.paymentDiscountRequestDetail.id);
  }
  editPaymentDiscountRequest(): void {
    let modalRef = this.paymentDiscountRequestNavigator.editPaymentDiscountRequest(this.paymentDiscountRequestDetail.id);
    modalRef.afterClosed().subscribe(paymentDiscountRequestDetail => {
      TCUtilsAngular.assign(this.paymentDiscountRequestDetail, paymentDiscountRequestDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.paymentDiscountRequestPersist.print(this.paymentDiscountRequestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print payment_discount_request", true);
      });
    }

  back():void{
      this.location.back();
    }

}
