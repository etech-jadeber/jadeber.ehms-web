import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDiscountRequestEditComponent } from './payment-discount-request-edit.component';

describe('PaymentDiscountRequestEditComponent', () => {
  let component: PaymentDiscountRequestEditComponent;
  let fixture: ComponentFixture<PaymentDiscountRequestEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentDiscountRequestEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDiscountRequestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
