import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PaymentDiscountRequestDetail } from '../payment_discount_request.model';import { PaymentDiscountRequestPersist } from '../payment_discount_request.persist';@Component({
  selector: 'app-payment_discount_request-edit',
  templateUrl: './payment-discount-request-edit.component.html',
  styleUrls: ['./payment-discount-request-edit.component.scss']
})

export class PaymentDiscountRequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  paymentDiscountRequestDetail: PaymentDiscountRequestDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PaymentDiscountRequestEditComponent>,
              public  persist: PaymentDiscountRequestPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: {id: string, patientId: string, paymentId: string, mode: string}) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isApprove(): boolean {
    return this.idMode.mode === 'Approve'
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("payment_discount_requests");
      this.title = this.appTranslation.getText("general","new") +  " " + "payment_discount_request";
      this.paymentDiscountRequestDetail = new PaymentDiscountRequestDetail();
      //set necessary defaults
      this.paymentDiscountRequestDetail.patient_id = this.idMode.patientId
      this.paymentDiscountRequestDetail.payment_id = this.idMode.paymentId

    } else {
     this.tcAuthorization.requireUpdate("payment_discount_requests");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "payment_discount_request";
      this.isLoadingResults = true;
      this.persist.getPaymentDiscountRequest(this.idMode.id).subscribe(paymentDiscountRequestDetail => {
        this.paymentDiscountRequestDetail = paymentDiscountRequestDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPaymentDiscountRequest(this.paymentDiscountRequestDetail).subscribe(value => {
      this.tcNotification.success("paymentDiscountRequest added");
      this.paymentDiscountRequestDetail.id = value.id;
      this.dialogRef.close(this.paymentDiscountRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePaymentDiscountRequest(this.paymentDiscountRequestDetail).subscribe(value => {
      this.tcNotification.success("payment_discount_request updated");
      this.dialogRef.close(this.paymentDiscountRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  onApprove(): void {
    this.isLoadingResults = true;
   

    this.persist.paymentDiscountRequestDo(this.paymentDiscountRequestDetail.id, "approve", this.paymentDiscountRequestDetail).subscribe(value => {
      this.tcNotification.success("payment_discount_request updated");
      this.dialogRef.close(this.paymentDiscountRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }
  
  canSubmit():boolean{
        if (this.paymentDiscountRequestDetail == null){
            return false;
          }
if (this.paymentDiscountRequestDetail.patient_id == null || this.paymentDiscountRequestDetail.patient_id  == "") {
            return false;
        }
        if (this.paymentDiscountRequestDetail.payment_id == null || this.paymentDiscountRequestDetail.payment_id  == "") {
          return false;
      }
if (this.isNew() && this.paymentDiscountRequestDetail.remark == null || this.paymentDiscountRequestDetail.remark  == "") {
            return false;
        }
if (this.isApprove() && this.paymentDiscountRequestDetail.discounted_amount == null) {
          return false;
      }
 return true;

 }
 }
