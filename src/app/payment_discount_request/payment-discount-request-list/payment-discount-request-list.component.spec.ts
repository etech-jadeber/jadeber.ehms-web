import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDiscountRequestListComponent } from './payment-discount-request-list.component';

describe('PaymentDiscountRequestListComponent', () => {
  let component: PaymentDiscountRequestListComponent;
  let fixture: ComponentFixture<PaymentDiscountRequestListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentDiscountRequestListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDiscountRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
