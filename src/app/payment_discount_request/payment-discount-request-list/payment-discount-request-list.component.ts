import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PaymentDiscountRequestDetail, PaymentDiscountRequestSummary, PaymentDiscountRequestSummaryPartialList } from '../payment_discount_request.model';
import { PaymentDiscountRequestPersist } from '../payment_discount_request.persist';
import { PaymentDiscountRequestNavigator } from '../payment_discount_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PaymentDetail } from 'src/app/payments/payment.model';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { ReceiveStatus } from 'src/app/app.enums';
@Component({
  selector: 'app-payment_discount_request-list',
  templateUrl: './payment-discount-request-list.component.html',
  styleUrls: ['./payment-discount-request-list.component.scss']
})export class PaymentDiscountRequestListComponent implements OnInit {
  paymentDiscountRequestsData: PaymentDiscountRequestSummary[] = [];
  paymentDiscountRequestsTotalCount: number = 0;
  paymentDiscountRequestSelectAll:boolean = false;
  paymentDiscountRequestSelection: PaymentDiscountRequestSummary[] = [];

 paymentDiscountRequestsDisplayedColumns: string[] = ["select","action","patient_id","request_date","respond_date","remark","payment_id","status","discounted_amount" ];
  paymentDiscountRequestSearchTextBox: FormControl = new FormControl();
  paymentDiscountRequestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) paymentDiscountRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentDiscountRequestsSort: MatSort;
  patients : {[id: string]: PatientDetail} = {};
  payments : {[id: string] : PaymentDetail} = {};
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public paymentDiscountRequestPersist: PaymentDiscountRequestPersist,
                public paymentDiscountRequestNavigator: PaymentDiscountRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public paymentPersist: PaymentPersist,
                public patientPersist: PatientPersist,

    ) {
      this.paymentDiscountRequestPersist.discount_status_filter = ReceiveStatus.sent

        this.tcAuthorization.requireRead("payment_discount_requests");
       this.paymentDiscountRequestSearchTextBox.setValue(paymentDiscountRequestPersist.paymentDiscountRequestSearchText);
      //delay subsequent keyup events
      this.paymentDiscountRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.paymentDiscountRequestPersist.paymentDiscountRequestSearchText = value;
        this.searchPaymentDiscountRequests();
      });
    } ngOnInit() {
   
      this.paymentDiscountRequestsSort.sortChange.subscribe(() => {
        this.paymentDiscountRequestsPaginator.pageIndex = 0;
        this.searchPaymentDiscountRequests(true);
      });

      this.paymentDiscountRequestsPaginator.page.subscribe(() => {
        this.searchPaymentDiscountRequests(true);
      });
      //start by loading items
      this.searchPaymentDiscountRequests();
    }

  searchPaymentDiscountRequests(isPagination:boolean = false): void {


    let paginator = this.paymentDiscountRequestsPaginator;
    let sorter = this.paymentDiscountRequestsSort;

    this.paymentDiscountRequestIsLoading = true;
    this.paymentDiscountRequestSelection = [];

    this.paymentDiscountRequestPersist.searchPaymentDiscountRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PaymentDiscountRequestSummaryPartialList) => {
      this.paymentDiscountRequestsData = partialList.data;
      this.paymentDiscountRequestsData.forEach(request => {
        this.patients[request.patient_id] = new PatientDetail()
        this.payments[request.payment_id] = new PaymentDetail()
        this.patientPersist.getPatient(request.patient_id).subscribe(
          patient => {
            this.patients[request.patient_id] = patient
          }
        )
        this.paymentPersist.getPayment(request.payment_id).subscribe(
          payment => {
            this.payments[request.payment_id] = payment
          }
        )
      })
      if (partialList.total != -1) {
        this.paymentDiscountRequestsTotalCount = partialList.total;
      }
      this.paymentDiscountRequestIsLoading = false;
    }, error => {
      this.paymentDiscountRequestIsLoading = false;
    });

  } downloadPaymentDiscountRequests(): void {
    if(this.paymentDiscountRequestSelectAll){
         this.paymentDiscountRequestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download payment_discount_request", true);
      });
    }
    else{
        this.paymentDiscountRequestPersist.download(this.tcUtilsArray.idsList(this.paymentDiscountRequestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download payment_discount_request",true);
            });
        }
  }
addPaymentDiscountRequest(): void {
    let dialogRef = this.paymentDiscountRequestNavigator.addPaymentDiscountRequest("", "");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPaymentDiscountRequests();
      }
    });
  }

  editPaymentDiscountRequest(item: PaymentDiscountRequestSummary) {
    let dialogRef = this.paymentDiscountRequestNavigator.editPaymentDiscountRequest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePaymentDiscountRequest(item: PaymentDiscountRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("payment_discount_request");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.paymentDiscountRequestPersist.deletePaymentDiscountRequest(item.id).subscribe(response => {
          this.tcNotification.success("payment_discount_request deleted");
          this.searchPaymentDiscountRequests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getPatientName(id: string): string {
      const patient = this.patients[id];
      return patient ? `${patient.fname} ${patient.mname} ${patient.lname}` : ""
    }

    getPayment(id: string): number {
      const payment = this.payments[id];
      return payment.price + payment.additional_payment - payment.discount_payment
    }

    approveRequest(element: PaymentDiscountRequestDetail){
      let dialogRef = this.paymentDiscountRequestNavigator.approvePaymentDiscountRequest(element.id)
      dialogRef.afterClosed().subscribe(
        reqeust => {
          this.searchPaymentDiscountRequests()
        }
      )
    }

    rejectRequest(element: PaymentDiscountRequestDetail){
      this.paymentDiscountRequestPersist.paymentDiscountRequestDo(element.id, "reject", {}).subscribe(
        request => {
          this.searchPaymentDiscountRequests()
        }
      )
    }
}
