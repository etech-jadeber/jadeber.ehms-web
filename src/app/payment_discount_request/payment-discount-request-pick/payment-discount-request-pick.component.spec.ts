import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentDiscountRequestPickComponent } from './payment-discount-request-pick.component';

describe('PaymentDiscountRequestPickComponent', () => {
  let component: PaymentDiscountRequestPickComponent;
  let fixture: ComponentFixture<PaymentDiscountRequestPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentDiscountRequestPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDiscountRequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
