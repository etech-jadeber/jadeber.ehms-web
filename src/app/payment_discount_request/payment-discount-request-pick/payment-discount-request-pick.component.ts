import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PaymentDiscountRequestSummary, PaymentDiscountRequestSummaryPartialList } from '../payment_discount_request.model';
import { PaymentDiscountRequestPersist } from '../payment_discount_request.persist';
import { PaymentDiscountRequestNavigator } from '../payment_discount_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-payment_discount_request-pick',
  templateUrl: './payment-discount-request-pick.component.html',
  styleUrls: ['./payment-discount-request-pick.component.scss']
})export class PaymentDiscountRequestPickComponent implements OnInit {
  paymentDiscountRequestsData: PaymentDiscountRequestSummary[] = [];
  paymentDiscountRequestsTotalCount: number = 0;
  paymentDiscountRequestSelectAll:boolean = false;
  paymentDiscountRequestSelection: PaymentDiscountRequestSummary[] = [];

 paymentDiscountRequestsDisplayedColumns: string[] = ["select" ,"requestor_id","patient_id","responsed_id","request_date","respond_date","remark","target_id","status","discounted_amount" ];
  paymentDiscountRequestSearchTextBox: FormControl = new FormControl();
  paymentDiscountRequestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) paymentDiscountRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentDiscountRequestsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public paymentDiscountRequestPersist: PaymentDiscountRequestPersist,
                public paymentDiscountRequestNavigator: PaymentDiscountRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PaymentDiscountRequestPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("payment_discount_requests");
       this.paymentDiscountRequestSearchTextBox.setValue(paymentDiscountRequestPersist.paymentDiscountRequestSearchText);
      //delay subsequent keyup events
      this.paymentDiscountRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.paymentDiscountRequestPersist.paymentDiscountRequestSearchText = value;
        this.searchPayment_discount_requests();
      });
    } ngOnInit() {
   
      this.paymentDiscountRequestsSort.sortChange.subscribe(() => {
        this.paymentDiscountRequestsPaginator.pageIndex = 0;
        this.searchPayment_discount_requests(true);
      });

      this.paymentDiscountRequestsPaginator.page.subscribe(() => {
        this.searchPayment_discount_requests(true);
      });
      //start by loading items
      this.searchPayment_discount_requests();
    }

  searchPayment_discount_requests(isPagination:boolean = false): void {


    let paginator = this.paymentDiscountRequestsPaginator;
    let sorter = this.paymentDiscountRequestsSort;

    this.paymentDiscountRequestIsLoading = true;
    this.paymentDiscountRequestSelection = [];

    this.paymentDiscountRequestPersist.searchPaymentDiscountRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PaymentDiscountRequestSummaryPartialList) => {
      this.paymentDiscountRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.paymentDiscountRequestsTotalCount = partialList.total;
      }
      this.paymentDiscountRequestIsLoading = false;
    }, error => {
      this.paymentDiscountRequestIsLoading = false;
    });

  }
  markOneItem(item: PaymentDiscountRequestSummary) {
    if(!this.tcUtilsArray.containsId(this.paymentDiscountRequestSelection,item.id)){
          this.paymentDiscountRequestSelection = [];
          this.paymentDiscountRequestSelection.push(item);
        }
        else{
          this.paymentDiscountRequestSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.paymentDiscountRequestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
