import {TCId} from "../tc/models";export class PaymentDiscountRequestSummary extends TCId {
    requester_id:string;
    patient_id:string;
    responsed_id:string;
    request_date:number;
    respond_date:number;
    remark:string;
    payment_id:string;
    status:number;
    discounted_amount:number;
     
    }
    export class PaymentDiscountRequestSummaryPartialList {
      data: PaymentDiscountRequestSummary[];
      total: number;
    }
    export class PaymentDiscountRequestDetail extends PaymentDiscountRequestSummary {
    }
    
    export class PaymentDiscountRequestDashboard {
      total: number;
    }
    