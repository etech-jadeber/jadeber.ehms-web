import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {PaymentDiscountRequestEditComponent} from "./payment-discount-request-edit/payment-discount-request-edit.component";
import {PaymentDiscountRequestPickComponent} from "./payment-discount-request-pick/payment-discount-request-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PaymentDiscountRequestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  paymentDiscountRequestsUrl(): string {
    return "/payment_discount_requests";
  }

  paymentDiscountRequestUrl(id: string): string {
    return "/payment_discount_requests/" + id;
  }

  viewPaymentDiscountRequests(): void {
    this.router.navigateByUrl(this.paymentDiscountRequestsUrl());
  }

  viewPaymentDiscountRequest(id: string): void {
    this.router.navigateByUrl(this.paymentDiscountRequestUrl(id));
  }

  editPaymentDiscountRequest(id: string): MatDialogRef<PaymentDiscountRequestEditComponent> {
    const dialogRef = this.dialog.open(PaymentDiscountRequestEditComponent, {
      data: {id, mode: TCModalModes.EDIT},
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPaymentDiscountRequest(patientId: string, paymentId: string): MatDialogRef<PaymentDiscountRequestEditComponent> {
    const dialogRef = this.dialog.open(PaymentDiscountRequestEditComponent, {
          data: {patientId, paymentId, mode: TCModalModes.NEW},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

  approvePaymentDiscountRequest(id: string): MatDialogRef<PaymentDiscountRequestEditComponent> {
    const dialogRef = this.dialog.open(PaymentDiscountRequestEditComponent, {
          data: {id, mode: 'Approve'},
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPaymentDiscountRequests(selectOne: boolean=false): MatDialogRef<PaymentDiscountRequestPickComponent> {
      const dialogRef = this.dialog.open(PaymentDiscountRequestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
