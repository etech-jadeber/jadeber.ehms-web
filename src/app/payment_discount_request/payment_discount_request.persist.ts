import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PaymentDiscountRequestDashboard, PaymentDiscountRequestDetail, PaymentDiscountRequestSummaryPartialList} from "./payment_discount_request.model";
import { ReceiveStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PaymentDiscountRequestPersist {
 paymentDiscountRequestSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.paymentDiscountRequestSearchText;
    //add custom filters
    return fltrs;
  }
  discount_status_filter: number;

  DiscountStatus: TCEnum[] = [
    new TCEnum( ReceiveStatus.sent, 'Sent'),
 new TCEnum( ReceiveStatus.received, 'Approved'),
 new TCEnum( ReceiveStatus.rejected, 'Rejected'),

 ];
 
  searchPaymentDiscountRequest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PaymentDiscountRequestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("payment_discount_request", this.paymentDiscountRequestSearchText, pageSize, pageIndex, sort, order);
    if (this.discount_status_filter){
        url = TCUtilsString.appendUrlParameter(url, "status", this.discount_status_filter.toString())
    }
    return this.http.get<PaymentDiscountRequestSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payment_discount_request/do", new TCDoParam("download_all", this.filters()));
  }

  paymentDiscountRequestDashboard(): Observable<PaymentDiscountRequestDashboard> {
    return this.http.get<PaymentDiscountRequestDashboard>(environment.tcApiBaseUri + "payment_discount_request/dashboard");
  }

  getPaymentDiscountRequest(id: string): Observable<PaymentDiscountRequestDetail> {
    return this.http.get<PaymentDiscountRequestDetail>(environment.tcApiBaseUri + "payment_discount_request/" + id);
  } addPaymentDiscountRequest(item: PaymentDiscountRequestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payment_discount_request/", item);
  }

  updatePaymentDiscountRequest(item: PaymentDiscountRequestDetail): Observable<PaymentDiscountRequestDetail> {
    return this.http.patch<PaymentDiscountRequestDetail>(environment.tcApiBaseUri + "payment_discount_request/" + item.id, item);
  }

  deletePaymentDiscountRequest(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "payment_discount_request/" + id);
  }
 paymentDiscountRequestsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "payment_discount_request/do", new TCDoParam(method, payload));
  }

  paymentDiscountRequestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "payment_discount_request/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "payment_discount_request/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "payment_discount_request/" + id + "/do", new TCDoParam("print", {}));
  }


}
