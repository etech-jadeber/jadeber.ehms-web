import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaymentDueDetailComponent } from './payment-due-detail.component';

describe('PaymentDueDetailComponent', () => {
  let component: PaymentDueDetailComponent;
  let fixture: ComponentFixture<PaymentDueDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDueDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDueDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
