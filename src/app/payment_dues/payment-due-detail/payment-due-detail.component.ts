import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Payment_DueDetail} from "../payment_due.model";
import {Payment_DuePersist} from "../payment_due.persist";
import {Payment_DueNavigator} from "../payment_due.navigator";

export enum Payment_DueTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-payment-due-detail',
  templateUrl: './payment-due-detail.component.html',
  styleUrls: ['./payment-due-detail.component.css']
})
export class PaymentDueDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  payment_dueLoading:boolean = false;
  
  Payment_DueTabs: typeof Payment_DueTabs = Payment_DueTabs;
  activeTab: Payment_DueTabs = Payment_DueTabs.overview;
  //basics
  payment_dueDetail: Payment_DueDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public payment_dueNavigator: Payment_DueNavigator,
              public  payment_duePersist: Payment_DuePersist) {
    this.tcAuthorization.requireRead("payment_dues");
    this.payment_dueDetail = new Payment_DueDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("payment_dues");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.payment_dueLoading = true;
    this.payment_duePersist.getPayment_Due(id).subscribe(payment_dueDetail => {
          this.payment_dueDetail = payment_dueDetail;
          this.payment_dueLoading = false;
        }, error => {
          console.error(error);
          this.payment_dueLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.payment_dueDetail.id);
  }

  editPayment_Due(): void {
    let modalRef = this.payment_dueNavigator.editPayment_Due(this.payment_dueDetail.id);
    modalRef.afterClosed().subscribe(modifiedPayment_DueDetail => {
      TCUtilsAngular.assign(this.payment_dueDetail, modifiedPayment_DueDetail);
    }, error => {
      console.error(error);
    });
  }

   printPayment_Due():void{
      this.payment_duePersist.print(this.payment_dueDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print payment_due", true);
      });
    }

  back():void{
      this.location.back();
    }

}
