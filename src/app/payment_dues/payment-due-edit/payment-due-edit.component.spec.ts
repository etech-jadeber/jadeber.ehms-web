import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaymentDueEditComponent } from './payment-due-edit.component';

describe('PaymentDueEditComponent', () => {
  let component: PaymentDueEditComponent;
  let fixture: ComponentFixture<PaymentDueEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDueEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDueEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
