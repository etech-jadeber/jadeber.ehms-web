import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Payment_DueDetail} from "../payment_due.model";
import {Payment_DuePersist} from "../payment_due.persist";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { EmployeePersist } from 'src/app/storeemployees/employee.persist';
import { EmployeeSummary, EmployeeSummaryPartialList } from 'src/app/storeemployees/employee.model';
import { Insured_EmployeePersist } from 'src/app/insured_employees/insured_employee.persist';
import { Insured_EmployeeSummary, Insured_EmployeeSummaryPartialList } from 'src/app/insured_employees/insured_employee.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';


@Component({
  selector: 'app-payment-due-edit',
  templateUrl: './payment-due-edit.component.html',
  styleUrls: ['./payment-due-edit.component.css']
})
export class PaymentDueEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  payment_dueDetail: Payment_DueDetail;

  patients: PatientSummary[] = [];
  employees: Insured_EmployeeSummary[] = [];
  date: Date = new Date();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PaymentDueEditComponent>,
              public  persist: Payment_DuePersist,
              public patientPersist: PatientPersist,
              public employeePersist: Insured_EmployeePersist,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    this.getPatients();
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("payment_dues");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("inventory","payment_due");
      this.payment_dueDetail = new Payment_DueDetail();
      this.payment_dueDetail.status = -1;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("payment_dues");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("inventory","payment_due");
      this.isLoadingResults = true;
      this.persist.getPayment_Due(this.idMode.id).subscribe(payment_dueDetail => {
        this.payment_dueDetail = payment_dueDetail;
        this.transformDates(true);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(false);
    this.persist.addPayment_Due(this.payment_dueDetail).subscribe(value => {
      this.tcNotification.success("Payment_Due added");
      this.payment_dueDetail.id = value.id;
      this.dialogRef.close(this.payment_dueDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  getPatients():void{
    this.patientPersist.searchPatient(50, 0, "", "").subscribe(
      (partialList:PatientSummaryPartialList)=>{
        this.patients =  partialList.data;
      }
    )
  }

  onUpdate(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.updatePayment_Due(this.payment_dueDetail).subscribe(value => {
      this.tcNotification.success("Payment_Due updated");
      this.dialogRef.close(this.payment_dueDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  transformDates(todate: boolean) {
    if (todate) {
      this.date = this.tcUtilsDate.toDate(this.payment_dueDetail.time_stamp);
    } else {
      this.payment_dueDetail.time_stamp = this.tcUtilsDate.toTimeStamp(
        this.date
      );
    }
  }


  canSubmit():boolean{
        if (this.payment_dueDetail == null){
            return false;
          }
          if (this.payment_dueDetail.status == -1) {
            return false;
          }



        return true;
      }


}
