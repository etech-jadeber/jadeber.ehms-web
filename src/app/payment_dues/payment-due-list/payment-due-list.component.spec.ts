import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaymentDueListComponent } from './payment-due-list.component';

describe('PaymentDueListComponent', () => {
  let component: PaymentDueListComponent;
  let fixture: ComponentFixture<PaymentDueListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
