import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Payment_DuePersist} from "../payment_due.persist";
import {Payment_DueNavigator} from "../payment_due.navigator";
import {Payment_DueDetail, Payment_DueSummary, Payment_DueSummaryPartialList} from "../payment_due.model";
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Insured_EmployeePersist } from 'src/app/insured_employees/insured_employee.persist';
import { Insured_EmployeeSummary, Insured_EmployeeSummaryPartialList } from 'src/app/insured_employees/insured_employee.model';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';


@Component({
  selector: 'app-payment-due-list',
  templateUrl: './payment-due-list.component.html',
  styleUrls: ['./payment-due-list.component.css']
})
export class PaymentDueListComponent implements OnInit {

  payment_duesData: Payment_DueSummary[] = [];
  payment_duesTotalCount: number = 0;
  payment_duesSelectAll:boolean = false;
  payment_duesSelection: Payment_DueSummary[] = [];

  payment_duesDisplayedColumns: string[] = ["select","action", "employee_id","amount","patient_id","time_stamp","status", ];
  payment_duesSearchTextBox: FormControl = new FormControl();
  payment_duesIsLoading: boolean = false;

  employees: Insured_EmployeeSummary[] = [];
  patients: PatientSummary[] = [];

  @ViewChild(MatPaginator, {static: true}) payment_duesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) payment_duesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public payment_duePersist: Payment_DuePersist,
                public payment_dueNavigator: Payment_DueNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public employeePersist: Insured_EmployeePersist,
    ) {

        this.tcAuthorization.requireRead("payment_dues");
       this.payment_duesSearchTextBox.setValue(payment_duePersist.payment_dueSearchText);
      //delay subsequent keyup events
      this.payment_duesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.payment_duePersist.payment_dueSearchText = value;
        this.searchPayment_Dues();
      });
    }

    ngOnInit() {
      // this.getEmployees();
      this.getPatients();
      this.payment_duesSort.sortChange.subscribe(() => {
        this.payment_duesPaginator.pageIndex = 0;
        this.searchPayment_Dues(true);
      });

      this.payment_duesPaginator.page.subscribe(() => {
        this.searchPayment_Dues(true);
      });
      //start by loading items
      this.searchPayment_Dues();
    }

  searchPayment_Dues(isPagination:boolean = false): void {


    let paginator = this.payment_duesPaginator;
    let sorter = this.payment_duesSort;

    this.payment_duesIsLoading = true;
    this.payment_duesSelection = [];

    this.payment_duePersist.searchPayment_Due(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Payment_DueSummaryPartialList) => {
      this.payment_duesData = partialList.data;
      if (partialList.total != -1) {
        this.payment_duesTotalCount = partialList.total;
      }
      this.payment_duesIsLoading = false;
    }, error => {
      this.payment_duesIsLoading = false;
    });

  }

  downloadPayment_Dues(): void {
    if(this.payment_duesSelectAll){
         this.payment_duePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download payment_dues", true);
      });
    }
    else{
        this.payment_duePersist.download(this.tcUtilsArray.idsList(this.payment_duesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download payment_dues",true);
            });
        }
  }


  getPatient(id: string): string {
    let patientFullName = "";
    let patients = this.patients.filter((patient) => (patient.uuidd == id));
    if (patients && patients.length > 0){
      let patient = patients[0];
      patientFullName = patient.fname + " " + patient.lname;
    }
    return patientFullName;
  }

  getPatients():void{
    this.patientPersist.searchPatient(50, 0, "", "").subscribe(
      (partialList:PatientSummaryPartialList)=>{
        this.patients =  partialList.data; 
      }
    )
  }

  // getEmployees():void{
  //   this.employeePersist.searchInsured_Employee(50, 0, "", "").subscribe(
  //     (partialList:Insured_EmployeeSummaryPartialList)=>{
  //       this.employees =  partialList.data; 
  //     }
  //   )
  // }

  addPayment_Due(): void {
    let dialogRef = this.payment_dueNavigator.addPayment_Due();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPayment_Dues();
      }
    });
  }

  editPayment_Due(item: Payment_DueSummary) {
    let dialogRef = this.payment_dueNavigator.editPayment_Due(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  covor(element: Payment_DueDetail): void {
    let dialogRef = this.tcNavigator.confirmAction("Cover payment", "Payment due", "Cover payment", "Cancel");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
    this.payment_duePersist.payment_dueDo(element.id, "cover", {})
    .subscribe(result => {
      if(result) {
        this.tcNotification.success("Payment_Due covered.");
        this.searchPayment_Dues();
      }
    }), error => {
      this.tcNotification.error("Payment_Due not covered.");   
    }}});


  }

  deletePayment_Due(item: Payment_DueSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Payment_Due");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.payment_duePersist.deletePayment_Due(item.id).subscribe(response => {
          this.tcNotification.success("Payment_Due deleted");
          this.searchPayment_Dues();
        }, error => {
        });
      }

    });
  }


  back():void{
      this.location.back();
    }

}
