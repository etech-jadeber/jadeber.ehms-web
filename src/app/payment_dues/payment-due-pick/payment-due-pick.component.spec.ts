import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaymentDuePickComponent } from './payment-due-pick.component';

describe('PaymentDuePickComponent', () => {
  let component: PaymentDuePickComponent;
  let fixture: ComponentFixture<PaymentDuePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentDuePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentDuePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
