import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Payment_DueDetail, Payment_DueSummary, Payment_DueSummaryPartialList} from "../payment_due.model";
import {Payment_DuePersist} from "../payment_due.persist";


@Component({
  selector: 'app-payment-due-pick',
  templateUrl: './payment-due-pick.component.html',
  styleUrls: ['./payment-due-pick.component.css']
})
export class PaymentDuePickComponent implements OnInit {

  payment_duesData: Payment_DueSummary[] = [];
  payment_duesTotalCount: number = 0;
  payment_duesSelection: Payment_DueSummary[] = [];
  payment_duesDisplayedColumns: string[] = ["select", 'employee_id','amount','patient_id','time_stamp','status' ];

  payment_duesSearchTextBox: FormControl = new FormControl();
  payment_duesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) payment_duesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) payment_duesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public payment_duePersist: Payment_DuePersist,
              public dialogRef: MatDialogRef<PaymentDuePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("payment_dues");
    this.payment_duesSearchTextBox.setValue(payment_duePersist.payment_dueSearchText);
    //delay subsequent keyup events
    this.payment_duesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.payment_duePersist.payment_dueSearchText = value;
      this.searchPayment_Dues();
    });
  }

  ngOnInit() {

    this.payment_duesSort.sortChange.subscribe(() => {
      this.payment_duesPaginator.pageIndex = 0;
      this.searchPayment_Dues();
    });

    this.payment_duesPaginator.page.subscribe(() => {
      this.searchPayment_Dues();
    });

    //set initial picker list to 5
    this.payment_duesPaginator.pageSize = 5;

    //start by loading items
    this.searchPayment_Dues();
  }

  searchPayment_Dues(): void {
    this.payment_duesIsLoading = true;
    this.payment_duesSelection = [];

    this.payment_duePersist.searchPayment_Due(this.payment_duesPaginator.pageSize,
        this.payment_duesPaginator.pageIndex,
        this.payment_duesSort.active,
        this.payment_duesSort.direction).subscribe((partialList: Payment_DueSummaryPartialList) => {
      this.payment_duesData = partialList.data;
      if (partialList.total != -1) {
        this.payment_duesTotalCount = partialList.total;
      }
      this.payment_duesIsLoading = false;
    }, error => {
      this.payment_duesIsLoading = false;
    });

  }

  markOneItem(item: Payment_DueSummary) {
    if(!this.tcUtilsArray.containsId(this.payment_duesSelection,item.id)){
          this.payment_duesSelection = [];
          this.payment_duesSelection.push(item);
        }
        else{
          this.payment_duesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.payment_duesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
