import {TCId} from "../tc/models";

export class Payment_DueSummary extends TCId {
  employee_id : string;
amount : string;
patient_id : string;
time_stamp : number;
status : number;
}

export class Payment_DueSummaryPartialList {
  data: Payment_DueSummary[];
  total: number;
}

export class Payment_DueDetail extends Payment_DueSummary {
  employee_id : string;
amount : string;
patient_id : string;
time_stamp : number;
status : number;
}

export class Payment_DueDashboard {
  total: number;
}
