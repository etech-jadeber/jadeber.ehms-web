import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PaymentDueEditComponent} from "./payment-due-edit/payment-due-edit.component";
import {PaymentDuePickComponent} from "./payment-due-pick/payment-due-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Payment_DueNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  payment_duesUrl(): string {
    return "/payment_dues";
  }

  payment_dueUrl(id: string): string {
    return "/payment_dues/" + id;
  }

  viewPayment_Dues(): void {
    this.router.navigateByUrl(this.payment_duesUrl());
  }

  viewPayment_Due(id: string): void {
    this.router.navigateByUrl(this.payment_dueUrl(id));
  }

  editPayment_Due(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PaymentDueEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPayment_Due(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PaymentDueEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPayment_Dues(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(PaymentDuePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
