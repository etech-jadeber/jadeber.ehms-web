import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {Payment_DueDashboard, Payment_DueDetail, Payment_DueSummaryPartialList} from "./payment_due.model";
import { payment_due_status } from '../app.enums';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class Payment_DuePersist {

  payment_dueSearchText: string = "";

  payment_due_statuId: number ;
    
  payment_due_status: TCEnumTranslation[] = [
  new TCEnumTranslation(payment_due_status.covered, this.appTranslation.getKey('patient', 'covered')),
  new TCEnumTranslation(payment_due_status.pending, this.appTranslation.getKey('patient', 'pending')),
];

  constructor(
      private http: HttpClient,
      private appTranslation: AppTranslation,
      ) {
  }

  searchPayment_Due(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Payment_DueSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("payment_dues", this.payment_dueSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Payment_DueSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.payment_dueSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payment_dues/do", new TCDoParam("download_all", this.filters()));
  }

  payment_dueDashboard(): Observable<Payment_DueDashboard> {
    return this.http.get<Payment_DueDashboard>(environment.tcApiBaseUri + "payment_dues/dashboard");
  }

  getPayment_Due(id: string): Observable<Payment_DueDetail> {
    return this.http.get<Payment_DueDetail>(environment.tcApiBaseUri + "payment_dues/" + id);
  }

  addPayment_Due(item: Payment_DueDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payment_dues/", item);
  }

  updatePayment_Due(item: Payment_DueDetail): Observable<Payment_DueDetail> {
    return this.http.patch<Payment_DueDetail>(environment.tcApiBaseUri + "payment_dues/" + item.id, item);
  }

  deletePayment_Due(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "payment_dues/" + id);
  }

  payment_duesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "payment_dues/do", new TCDoParam(method, payload));
  }

  payment_dueDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "payment_dues/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "payment_dues/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "payment_dues/" + id + "/do", new TCDoParam("print", {}));
  }


}
