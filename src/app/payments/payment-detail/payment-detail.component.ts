import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, elementAt } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";

import { CommentDetail, TCEnum, TCId } from 'src/app/tc/models';
import { FormControl } from '@angular/forms';
import { TCAuthentication } from 'src/app/tc/authentication';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { MenuState } from 'src/app/global-state-manager/global-state';
import {
  CreditServiceFor,
  deposit_status,
  fee_type,
  PaymentType,
  payment_status,
  payment_type,
  tabs,
  vat_status,
  openness_status
} from 'src/app/app.enums';
import { PaymentDetail, PaymentSummary, TotalPaymentSummery } from '../payment.model';
import { PaymentPersist } from '../payment.persist';
import { FeePersist } from 'src/app/fees/fee.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Insured_EmployeeSummary } from 'src/app/insured_employees/insured_employee.model';
import { Insured_EmployeePersist } from 'src/app/insured_employees/insured_employee.persist';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
import { PaymentDiscountRequestNavigator } from 'src/app/payment_discount_request/payment_discount_request.navigator';
import { CompanyCreditServicePersist } from 'src/app/company_credit_service/company_credit_service.persist';
import { CompanyCreditServiceDetail } from 'src/app/company_credit_service/company_credit_service.model';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { UserDetail } from 'src/app/tc/users/user.model';
import { FeeDetail } from 'src/app/fees/fee.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { CreditEmployeeFamilyPersist } from 'src/app/credit_employee_family/credit_employee_family.persist';
import { TransferDetail } from 'src/app/transfers/transfer.model';
import { TransferPersist } from 'src/app/transfers/transfer.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { DepositPersist } from 'src/app/deposits/deposit.persist';
import { PaymentNavigator } from '../payment.navigator';
import { PatientDetail } from 'src/app/patients/patients.model';
import { BankNavigator } from 'src/app/bank/bank.navigator';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { C } from '@angular/cdk/keycodes';
import { TCUtilsNumber } from 'src/app/tc/utils-number';
import {CreditPaymentPersist} from "../../credit_payment/credit_payment.persist";


export enum PaginatorIndexes {
  not_paid,
  paid,
}

export enum PatientTabs {
  overview
}


@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styleUrls: ['./payment-detail.component.css']
})
export class PaymentDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  paymentLoading: boolean = false;
  totalPaymentPrice: TotalPaymentSummery = { total_price: 0, total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_patient_price: 0};
  refundDate: number = 60 * 60 * 1;
  //basics
  paidData: PaymentSummary[] = [];
  not_paidData: PaymentSummary[] = [];
  paidSelectAll: boolean = false;
  not_paidSelectAll: boolean = false;
  paidSelection: PaymentSummary[] = [];
  not_paidSelection: PaymentDetail[] = [];
  paidTotalCount: number = 0;
  not_paidTotalCount: number = 0;
  paidDisplayColumns: String[] = ["select", "action", "crv", 'item_code', "fee_id", 'fee_type', "payment_date", "quantity", "price", "payment_type", "provider_id", "cashier_id"];
  not_paidDisplayColumns: String[] = ["select", "action", 'item_code', "fee_id", 'fee_type', "quantity", "ordered_date", "price", "additional_payment", "discount_payment", "net_price", "provider_id"]
  paymentSearchTextBox: FormControl = new FormControl();
  payment_type: number = -1;
  selectedTabIndex: number;
  openedUnconsciousDate: number;
  company: {insuredId: string, name: string, id: string, empId?: string, empName?: string;}[] = []
  paymentType = PaymentType
  selected_company: {insuredId: string, name: string, id: string, empId?: string, empName?: string;};
  paidStatus: number = payment_status.paid
  tabs: typeof tabs = tabs;
  activeTab: number;
  payment_status = payment_status
  fee_types: number[] = []
  companyCredit: {[fee: number]: CompanyCreditServiceDetail} = {}
  providers: {[id: string]: DoctorDetail} = {}
  cashiers: {[id: string]: UserDetail} = {}
  fees: {[id: string]: FeeDetail} = {}
  image_url: string;
  isCircle: boolean = true;
  transfer : {[id:string]:TransferDetail} = {}
  remaining_deposit: number;
  diposited: number;
  credit: number;
  used: number;
  patient: PatientDetail;
  patient_name: string;
  filteredPaymentType;
  bankName: string;
  bankId: string;
  tin_number: string;
  received_amount: number = 0;
  transaction_no: string;
  searchHistory = this.paymentPersist.notPaidPaymentSearchHistory;
  ordered_date: FormControl = new FormControl({disabled: true, value: ''});
  payment_date: FormControl = new FormControl({disabled: true, value: this.searchHistory.payment_date})
  payment_ordered_date: FormControl = new FormControl({disabled: true, value: ''});

  depositSearchHistory = {...this.deposiPersist.defaultDepositSearchHistory}

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public userPersist: UserPersist,
    public tcAuthentication: TCAuthentication,
    public appTranslation: AppTranslation,
    public tcUtilsString: TCUtilsString,
    public paymentPersist: PaymentPersist,
    public menuState: MenuState,
    public feePersist: FeePersist,
    public doctorPersist: DoctorPersist,
    public insuredEmployeePersist: Insured_EmployeePersist,
    public companyPersist: CompanyPersist,
    public discountNavigator: PaymentDiscountRequestNavigator,
    public companyCreditPersist: CompanyCreditServicePersist,
    public patientPersist: PatientPersist,
    public filePersist: FilePersist,
    public creditFamilyPersist: CreditEmployeeFamilyPersist,
    public transferPersist: TransferPersist,
    public itemPersist : ItemPersist,
    public deposiPersist: DepositPersist,
    public itemNavigator: ItemNavigator,
    public paymentNavigator: PaymentNavigator,
    public tcUtilsNumber: TCUtilsNumber,
    public creditPaymentPersist: CreditPaymentPersist,
    public bankNavigator: BankNavigator) {

    this.filteredPaymentType = this.paymentPersist.paymentType
    // payment filter
    this.paymentSearchTextBox.setValue(this.searchHistory.search_text);
    this.paymentSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.searchHistory.search_text = value.trim();
      this.searchPayment();
    });
    if (this.searchHistory.ordered_date){
      // this.ordered_date.setValue(this.tcUtilsDate.toDate(this.searchHistory.ordered_date))
      this.payment_ordered_date.setValue(this.tcUtilsDate.toDate(this.searchHistory.ordered_date))
    }
    this.payment_date.valueChanges.subscribe((value)=>{
      this.searchHistory.payment_date = value ? this.tcUtilsDate.toTimeStamp(value._d) : null
      this.searchPayment()
  });
//   this.ordered_date.valueChanges.subscribe((value)=>{
//     this.payment_ordered_date.setValue(value, {emitEvent: false})
//     this.paymentPersist.ordered_date =  value ? this.tcUtilsDate.toTimeStamp(value._d) : null
//     this.searchPayment()
// });
// this.payment_ordered_date.valueChanges.subscribe((value)=>{
//   this.ordered_date.setValue(value)
// });

    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })

  }


  downloadPayments(){
    this.paymentPersist.paymentsDo("print", {
      payment_status: payment_status.not_paid,
      patient_id: this.patient.id
    }).subscribe((downloadJob: TCId) => this.jobPersist.followJob(downloadJob.id, "download payments", true))
  }
  ngOnInit() {
    this.tcAuthorization.requireRead("payments");
    this.activeTab = tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
      // this.deposiPersist.statusFilter = null;
      // this.deposiPersist.patientId = null;
      this.creditPaymentPersist.status = null;
      this.creditPaymentPersist.patient_id = null;
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.searchHistory.patient_id = id;
      this.paymentPersist.notPaidPaymentSearchHistory.patient_id = id;
      this.paymentPersist.paidPaymentSearchHistory.patient_id = id;
      this.getImageUrl(id);
    });
    // paid sorter
    this.sorters
      .toArray()
    [PaginatorIndexes.paid].sortChange.subscribe(() => {
      console.log(this.paginators)
      this.paginators.toArray()[PaginatorIndexes.not_paid].pageIndex = 0;
      this.searchPayment(true);
    });
    // paid paginator
    this.paginators
      .toArray()
    [PaginatorIndexes.not_paid].page.subscribe(() => {
      this.searchPayment(true);
    });

    this.sorters.toArray()[PaginatorIndexes.paid].active = "ordered_date";
    this.sorters.toArray()[PaginatorIndexes.paid].direction = "desc";

    //  not_paid sorter
    this.sorters
      .toArray()
    [PaginatorIndexes.not_paid].sortChange.subscribe(() => {
      this.paginators.toArray()[PaginatorIndexes.not_paid].pageIndex = 0;
      this.searchPayment(true);
    });
    // not_paid paginator
    // this.paginators
    //   .toArray()
    // [PaginatorIndexes.not_paid].page.subscribe(() => {
    //   this.searchPayment(true);
    // });

    this.sorters.toArray()[PaginatorIndexes.not_paid].active = "ordered_date";
    this.sorters.toArray()[PaginatorIndexes.not_paid].direction = "desc";
    this.depositSearchHistory.status = deposit_status.opened;
    this.depositSearchHistory.patient_id = this.searchHistory.patient_id
    this.searchPayment()
    this.searchDeposit()
    this.searchCredit()
  }

  back(): void {
    this.location.back();
  }

  //notes methods
  searchPayment(isPagination: boolean = false): void {
    this.searchHistory.payment_status = this.selectedTabIndex ? this.paidStatus : payment_status.not_paid;
    let index = this.selectedTabIndex ? PaginatorIndexes.paid : PaginatorIndexes.not_paid
    let paginator = this.paginators.toArray()[0];
    let sorter = this.sorters.toArray()[index];
    this.paidData = [];
    this.not_paidData = [];
    this.paidSelection = [];
    this.not_paidSelection = [];
    this.paidSelectAll = false;
    this.not_paidSelectAll = false;
    this.paymentLoading = true;
    this.payment_type = -1;
    this.totalPaymentPrice = { total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_price: 0, total_patient_price: 0 }
    this.received_amount = 0;
    // this.paymentPersist.paymentsDo("opened_unconscious_date", {patient_id: this.paymentPersist.patient_id}).subscribe(date => {
    //   date ? this.openedUnconsciousDate = date[0] : null
    this.paymentPersist.searchPayment(paginator.pageSize, (this.selectedTabIndex ? (isPagination ? paginator.pageIndex : 0) : -1), sorter.active, sorter.direction, this.searchHistory).subscribe(response => {
      this.selectedTabIndex ? this.paidData = response.data : this.not_paidData = response.data;
      response.data.forEach(resp => {
        resp.net_price = this.tcUtilsNumber.convertToDecimalPlaces(this.tcUtilsNumber.convertToDecimalPlaces(resp.price) * resp.quantity + resp.additional_payment - resp.discount_payment);
        // if(resp.fee_type  == fee_type.item){
        //   if(!this.transfer[resp.fee_id]){
        //     this.transferPersist.getTransfer(resp.fee_id).subscribe((transfer)=>{
        //         this.transfer[resp.fee_id] = transfer;
        //         this.itemPersist.getItem(transfer.item_id).subscribe((item)=>{
        //           this.transfer[resp.fee_id].item = item;
        //         })
        //     });
        //   }
        //
        // }
        // else
          if (!this.fees[resp.fee_id]) {
          this.fees[resp.fee_id] = new FeeDetail()
          this.feePersist.getFee(resp.fee_id).subscribe(result => {
            this.fee_types.push(result.fee_type);
            this.fees[resp.fee_id] = result
          })
        }
        if(!this.providers[resp.provider_id]){
          this.providers[resp.provider_id] = new DoctorDetail();
          resp.provider_id !== this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(resp.provider_id).subscribe(result => {
          this.providers[resp.provider_id] = result;
        })}
        if (!this.cashiers[resp.cashier_id]){
          this.cashiers[resp.cashier_id] = new UserDetail()
          resp.cashier_id && this.userPersist.getUser(resp.cashier_id).subscribe(result => {
          this.cashiers[resp.cashier_id] = result
        })}
      })
      if (response.total != -1) {
        this.selectedTabIndex ? this.paidTotalCount = response.total : this.not_paidTotalCount = response.total;
      }
      this.paymentLoading = false;
    }, error => {
      this.paymentLoading = false;
    })
  // });
  }


  addPayments(payment_status: number): void {
    this.paymentLoading = true;
      const paymentIds = this.not_paidSelection.map(not_paid => not_paid.id)
      this.paymentPersist.paymentsDo("send_payments", {paymentIds, payment_type: this.payment_type, company_id: this.selected_company?.id,
        payment_status, patient_id: this.searchHistory.patient_id, tin_no: this.tin_number, bank_id: this.bankId, transaction_no: this.transaction_no, received_amount: this.received_amount}).subscribe(
        (value) => {
          this.searchPayment()
          this.searchDeposit()
          this.paymentLoading = false;
        }, err => {
          this.paymentLoading = false;
        }
      )
      // this.not_paidSelection.forEach(selection => {
      //   selection.payment_type = this.payment_type
      //   if (this.payment_type == this.paymentType.CreditByEmployee || this.payment_type == this.paymentType.CreditByEmployee){
      //     selection.company_id = this.selected_company.id
      //     selection.employee_id = this.selected_company.empId;
      //   }
      //   selection.payment_status = cancel ? payment_status.Cancelled : selection.payment_status;
      //   this.paymentPersist.updatePayment(selection).subscribe(() => {
      //     if(this.not_paidSelection[this.not_paidSelection.length - 1].id == selection.id){
      //       this.searchPayment()
      //       this.searchDeposit()
      //     }
      //   })
      // })
  }

  editPaid(item: CommentDetail): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), item.comment_text, true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        // this.patientPersist.noteDo(this.patientDetail.id, item.id, "edit", { "note": note }).subscribe(response => {
        //   this.searchNotes();
        // });
      }
    });
  }

  getPatientCompany(){
    this.insuredEmployeePersist.insured_employeesDo("patient_companys", {patient_id: this.searchHistory.patient_id}).subscribe(
      (companys: Insured_EmployeeSummary[]) => {
        this.company = []
        companys.forEach(company => {
          this.companyPersist.getCompany(company.company_id).subscribe(
            comp => {
              this.company = [...this.company, {insuredId: company.id, id:comp.id, name:comp.name}]
            }
          )
        })
      }
    )
  }

  searchBank(){
    let dialogRef = this.bankNavigator.pickBanks(true);
    dialogRef.afterClosed().subscribe((value) => {
      if (value.length){
        this.bankName = value[0].name;
        this.bankId = value[0].id
      }
    })
  }

  getFamilyCompany(){
    this.creditFamilyPersist.creditEmployeeFamilysDo("patient_companys", {patient_id: this.searchHistory.patient_id}).subscribe(
      (companys: Insured_EmployeeSummary[]) => {
        this.company = []
        companys.forEach(company => {
          this.companyPersist.getCompany(company.company_id).subscribe(
            comp => {
              this.insuredEmployeePersist.getInsured_Employee(company.employee_id).subscribe(
                employee => {
                  this.company = [...this.company, {insuredId: company.id, id: comp.id, name: comp.name, empId: employee.id, empName: employee.full_name}]
                }
              )
            }
          )
        })
      }
    )
  }

  deletePaid(item: CommentDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        // this.patientPersist.deleteNote(this.patientDetail.id, item.id).subscribe(response => {
        //   this.tcNotification.success("note deleted");
        //   this.searchNotes();
        // }, error => {
        // });
      }

    });
  }

  getImageUrl(patient_id: string){
      this.patientPersist.getPatient(patient_id).subscribe(
        patient => {
          this.patient = patient
          this.patient_name = `${patient.fname} ${patient.mname} ${patient.lname}`
          this.image_url = patient.profile_pic && patient.profile_pic != this.tcUtilsString.invalid_id && this.filePersist.downloadLink(patient.profile_pic)
          this.isCircle = true;
        }
      )
  }

  requireDiscount(element: PaymentDetail): void {
    let dialogRef = this.discountNavigator.addPaymentDiscountRequest(element.patient_id, element.id);
    dialogRef.afterClosed().subscribe(
      discount => {
        console.log(discount)
      }
    )

  }

  update(not_paidSelection:any, row:any){
    this.tcUtilsArray.toggleSelection(not_paidSelection,row) ;
    this.updateSummery();
  }

  updateSummery() {
    this.totalPaymentPrice = { total_additional_payment: 0, total_discount_payment: 0, total_net_price: 0, total_price: 0 , total_patient_price: 0}
    this.received_amount = 0;
    this.not_paidSelection.forEach((selection) => {
      this.totalPaymentPrice.total_price += this.tcUtilsNumber.convertToDecimalPlaces(this.tcUtilsNumber.convertToDecimalPlaces(selection.price) * selection.quantity);
      this.totalPaymentPrice.total_additional_payment += this.getAdditionalPayment(selection);
      this.totalPaymentPrice.total_discount_payment += this.getDiscountPayment(selection);
      this.totalPaymentPrice.total_net_price += this.getNetPayment(selection);
      this.received_amount += this.getNetPayment(selection);
      this.totalPaymentPrice.total_patient_price = parseFloat((this.totalPaymentPrice.total_patient_price + this.getPaymentByPatient(selection)).toFixed(2))
    })
  }

  paymentTypeUpdate(){
    if (this.payment_type == PaymentType.CreditByEmployee){
      this.getPatientCompany()
    }
    if (this.payment_type == PaymentType.CreditByFamily){
      this.getFamilyCompany()
    }
  }

  updateTab(){
   if(this.selectedTabIndex){
     this.searchPayment(false)
   }else {
     this.searchPayment()
     this.searchDeposit()
     this.searchCredit()
   }
  }

  refundPayment(payment: PaymentDetail){
    this.paymentLoading = true;
    const paymentIds = [payment.id]
      this.paymentPersist.paymentsDo("send_payments", {paymentIds, payment_type: this.payment_type, company_id: this.selected_company?.id,
        payment_status: payment_status.Refunded, patient_id: this.searchHistory.patient_id, tin_no: this.tin_number, bank_id: this.bankId, transaction_no: this.transaction_no}).subscribe(
        (value) => {
          this.searchPayment(true)
          this.searchDeposit()
          this.searchCredit()
          this.paymentLoading = false;
        }, err => {
          this.paymentLoading = false;
        }
      )
  }

  printAttachment(payment: PaymentDetail){
    this.paymentPersist.paymentDo(payment.id, "print_attachment", {}).subscribe((downloadJob: TCId) => {
      this.jobPersist.followJob(downloadJob.id, "download attachment", true);
    })
  }

  getCnetResponse(payment: PaymentDetail){
    this.paymentPersist.paymentDo(payment.id, "get_cnet_response", {}).subscribe((value) => {
      this.tcNotification.success("FS No is registered")
      this.updateTab()
    })
  }

  canSubmit(): boolean{
    if (this.payment_type == -1){
      return false;
    }
    if ((this.payment_type == PaymentType.CreditByEmployee || this.payment_type == PaymentType.CreditByFamily) && this.selected_company && this.selected_company.id == ''){
      return false;
    }
    if (this.payment_type == PaymentType.transfer && (!this.bankId || !this.transaction_no)){
      return false;
    }
    if (this.received_amount == null || this.received_amount < 0 || this.received_amount > this.totalPaymentPrice.total_net_price){
      return false;
    }
    return true;
  }

  canAddPaymentType():boolean{
    if (!this.not_paidSelection.length){
      return false;
    }
    return true;
  }

  canRefund(payment: PaymentDetail): boolean{
    return payment.payment_status != payment_status.Refunded && payment.payment_status != payment_status.Cancelled && (this.tcAuthorization.canUpdate('payments') || this.tcAuthorization.canDelete('payments')) && this.tcUtilsDate.toTimeStamp(new Date()) - payment.payment_date < this.refundDate
  }

  getCompanyCredit(){
    this.paymentLoading = true;
    this.companyCredit = {}
    this.companyCreditPersist.companyCreditServicesDo("credit_info",
    {company_id: this.selected_company.id, fee_types: this.fee_types,
      credit_service_for: this.payment_type == PaymentType.CreditByEmployee ? CreditServiceFor.employee : CreditServiceFor.family}).subscribe(
      (data: CompanyCreditServiceDetail[]) => {
        data.forEach(credit => {
          this.companyCredit[credit.service_type] = credit
        })
        this.updateSummery()
        this.paymentLoading = false;
      },
      error => {
        this.paymentLoading = false;
        console.error(error)
      }
    )
    if (this.payment_type == PaymentType.CreditByEmployee)
    {this.insuredEmployeePersist.getInsured_Employee(this.selected_company.insuredId).subscribe(
      data => {
        data.image_id != this.tcUtilsString.invalid_id && this.filePersist.getDownloadKey(data.image_id).subscribe(
         (download: any) => {this.image_url = this.filePersist.downloadLink(download.id)
         this.isCircle = false}
        )
      }
    )}
    else {
      this.creditFamilyPersist.getCreditEmployeeFamily(this.selected_company.insuredId).subscribe(
        data => {
         data.image_id != this.tcUtilsString.invalid_id && this.filePersist.getDownloadKey(data.image_id).subscribe(
           (download: any) => {this.image_url = this.filePersist.downloadLink(download.id)
           this.isCircle = false}
          )
        }
      )
    }
  }

  // getBatchNumber(transfer_id: string):string{
  //   return this.transfer[transfer_id] ? this.transfer[transfer_id].item_batch_no : "";
  // }
  getAdditionalPayment(element: PaymentDetail){
    return this.tcUtilsNumber.convertToDecimalPlaces(element.additional_payment || 0)
  }

  getDiscountPayment(element: PaymentDetail){
    return this.tcUtilsNumber.convertToDecimalPlaces(this.tcUtilsNumber.convertToDecimalPlaces(element.discount_payment || 0) + this.tcUtilsNumber.convertToDecimalPlaces((this.companyCredit[element.fee_type]?.discount_percent || 0) * element.price * 0.01))
  }

  getNetPayment(element: PaymentDetail){
    return this.tcUtilsNumber.convertToDecimalPlaces(this.tcUtilsNumber.convertToDecimalPlaces(this.tcUtilsNumber.convertToDecimalPlaces(element.price) * element.quantity) + this.getAdditionalPayment(element) - this.getDiscountPayment(element))
  }

  getPaymentByPatient(element: PaymentDetail){
    return this.tcUtilsNumber.convertToDecimalPlaces(this.getNetPayment(element) * (1 - (this.companyCredit[element.fee_type]?.service_percent || 0) * 0.01))
  }

  getFeeName(id: string){
    if (this.fees[id]){
      return this.fees[id].name
    }
    else if(this.transfer[id]){
      return this.itemNavigator.itemName(this.transfer[id].item)
    }
    return ""
  }

  getItemCode(id: string){
    if (this.fees[id]){
      return this.fees[id].item_code
    }
    else if(this.transfer[id]){
      return this.transfer[id].item.item_code
    }
    return ""
  }

  getProviderName(id: string){
    const {first_name, middle_name, last_name} = this.providers[id]
    return first_name ? `${first_name} ${middle_name} ${last_name}` : ''
  }

  getCashierName(id: string){
    if (this.cashiers[id]){
      return `${this.cashiers[id].name}`
    }
    return ""
  }

  searchDeposit(): void {
    // this.deposiPersist.statusFilter = deposit_status.opened;
    // this.deposiPersist.patientId = this.searchHistory.patient_id
    this.deposiPersist.searchDeposit(1, 0, 'id', 'asc', this.depositSearchHistory).subscribe(deposit => {
      if(deposit.data.length){
        this.filteredPaymentType = [new TCEnum( PaymentType.Deposit, 'Deposit')]
        this.remaining_deposit = deposit.data[0].current_amount * -1
        this.used = deposit.data[0].used *  -1
        this.diposited = deposit.data[0].deposited
        this.ordered_date.setValue('')
    } else if (this.patient.guardian_id){
      this.deposiPersist.depositSearchHistory.patient_id = this.patient.guardian_id
      this.deposiPersist.searchDeposit(1, 0, 'id', 'asc').subscribe(deposit => {
        if(deposit.data.length){
          this.filteredPaymentType = [new TCEnum( PaymentType.Deposit, 'Deposit')]
        this.remaining_deposit = deposit.data[0].current_amount * -1
        this.used = deposit.data[0].used *  -1
        this.diposited = deposit.data[0].deposited
        this.ordered_date.setValue('')
        }})
    } })
  }

  searchCredit(): void {
    this.creditPaymentPersist.status = openness_status.open;
    this.creditPaymentPersist.patient_id = this.searchHistory.patient_id
    this.creditPaymentPersist.searchCreditPayment(1, 0, 'id', 'asc').subscribe(credit => {
      if(credit.data.length){
        this.credit = credit.data[0].current_amount * -1
      }
    })
  }

  calculateAge(dob: number) {
    const current = new Date().getFullYear()
    return current - new Date(dob*1000).getFullYear()
  }

}
