import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds, TCWizardProgress, TCWizardProgressModes} from "../../tc/models";


import {PaymentDetail} from "../payment.model";
import {PaymentPersist} from "../payment.persist";
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { other_type, payment_type } from 'src/app/app.enums';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { FeeSummary } from 'src/app/fees/fee.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { DepositDetail, Deposit_HistoryDetail } from 'src/app/deposits/deposit.model';
import { DepositPersist } from 'src/app/deposits/deposit.persist';
import { deposit_history_type } from 'src/app/app.enums';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';

@Component({
  selector: 'app-payment-edit',
  templateUrl: './payment-edit.component.html',
  styleUrls: ['./payment-edit.component.css']
})
export class PaymentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  patientFullName = '';
  paymentDetail: PaymentDetail;
  payment_date :Date = new Date();
  other_type = other_type;
  wizard:boolean = false;

  depositDetail: DepositDetail;
  patient:PatientSummary[] = [];
  deposit_date :Date = new Date();
  deposit_historyDetail: Deposit_HistoryDetail = new Deposit_HistoryDetail();
  deposit_type: number=0;
  fees:FeeSummary[]= [];
  is_deposit_payment: boolean = false;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              private patientPersist:PatientPersist,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PaymentEditComponent>,
              public  persist: PaymentPersist,
              private appointmentPersist:AppointmentPersist,
              private feePersist:FeePersist,
              private tcUtilsDate:TCUtilsDate,
              public patientNavigator: PatientNavigator,
              public depositPersist: DepositPersist,
              @Inject(MAT_DIALOG_DATA) public ids: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }


  isWizard():boolean{
    return this.ids.context['mode'] == TCModalModes.WIZARD;
  }

  isNew():boolean{
    return this.ids.context['mode'] == TCModalModes.NEW;
  }

  isEdit():boolean{
    return this.ids.context['mode'] == TCModalModes.EDIT;

  }
  ngOnInit() {
    this.getFeeTypes();
    if (this.isNew() ) {

     //this.tcAuthorization.requireCreate("payments");
      this.title = this.appTranslation.getText("general","new") +  " "+this.appTranslation.getText('financial', 'payment');
      this.paymentDetail = new PaymentDetail();
      this.paymentDetail.vat_included = other_type.vat;
      this.paymentDetail.vat = 0;
      //set necessary defaults
      this.paymentDetail.target_id = this.tcUtilsString.invalid_id;

    }
    else if(this.isWizard()){

      this.title = this.appTranslation.getText("general","new") +  " "+this.appTranslation.getText('financial', 'payment');
      this.paymentDetail = new PaymentDetail();
      this.paymentDetail.vat_included = other_type.vat;
      this.paymentDetail.vat = 0;
      if( this.ids.parentId != null){
        this.paymentDetail.patient_id = this.ids.parentId;
        this.getPatientName(this.ids.parentId);
        this.wizard = true;
      }

      if( this.ids.context['appointmentId'] != null ){
        let apptID = this.ids.context['appointmentId'];
        this.appointmentPersist.getAppointment(apptID).subscribe(
          appt=>{
            this.paymentDetail.patient_id = appt.scheduled_user_id;
            this.getPatientName(appt.scheduled_user_id);
          }
        )
        this.persist.paymentsDo("get_payment_by_type", {appointmentId:apptID}).subscribe((fee:FeeSummary)=>{
          this.paymentDetail.fee_id = fee.id;
       
        });
        
      }


    }

    else {
    // this.tcAuthorization.requireUpdate("payments");
      this.title = this.appTranslation.getText("general","edit") +  " " +this.appTranslation.getText('financial', 'payment');
      this.isLoadingResults = true;
      this.persist.getPayment(this.ids.childId).subscribe(paymentDetail => {
        this.paymentDetail = paymentDetail;
        this.getPatientName(this.paymentDetail.patient_id);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }


  onAdd(isWizard:boolean = false, closeDialog:boolean = true): void {
    this.transformDates()
    this.isLoadingResults = true;
    if(this.paymentDetail)
    {

    }
    this.paymentDetail.target_id = this.tcUtilsString.invalid_id;

    if(this.is_deposit_payment){
      this.paymentDetail.deposit_type = deposit_history_type.medical_expense;
      this.persist.addPayment(this.paymentDetail).subscribe(value => {
          this.addDeposit_History(this.paymentDetail, value.id);
        if(closeDialog){
          if(isWizard){
            this.dialogRef.close(new TCWizardProgress(TCWizardProgressModes.NEXT, this.paymentDetail));
          }
          else{
            this.dialogRef.close(this.paymentDetail);
          }
        }

      }, error => {
        this.isLoadingResults = false;
      })
  }else{
    this.paymentDetail.deposit_type = deposit_history_type.take_out;
    this.persist.addPayment(this.paymentDetail).subscribe(value => {
    if(closeDialog){

      if(isWizard){
        this.dialogRef.close(new TCWizardProgress(TCWizardProgressModes.NEXT, this.paymentDetail));


      }
      else{
        this.dialogRef.close(this.paymentDetail);
      }
    }

  }, error => {
    this.isLoadingResults = false;
  })
  }

  }

  onContinue(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.addPayment(this.paymentDetail).subscribe(
      (value) => {
        this.tcNotification.success("Member added");
        this.paymentDetail.id = value.id;
        this.dialogRef.close(
          new TCWizardProgress(TCWizardProgressModes.FINISH, {item:this.paymentDetail, next:"bed"}));
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }



  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist.updatePayment(this.paymentDetail).subscribe(value => {
      this.tcNotification.success("Payment updated");
      this.dialogRef.close(this.paymentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }



  canSubmit():boolean{
        if (this.paymentDetail == null){
            return false;
          }
          if(this.paymentDetail.patient_id == null){
            return false;
          }
          if(this.paymentDetail.vat == null){
            return false;
          }
          if(this.paymentDetail.vat_included == null){
            return false;
          }
          if(this.paymentDetail.total == null){
            return false;
          }
          if(this.paymentDetail.crv == null){
            return false;
          }



        return true;
      }


  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.patientFullName = result[0].fname + ' ' + result[0].lname;
        this.paymentDetail.patient_id = result[0].id;
      }
    });
  }


  transformDates(todate:boolean = true){
    if(todate){
      this.paymentDetail.payment_date=this.tcUtilsDate.toTimeStamp(this.payment_date);

    }
    else{
      this.payment_date = this.tcUtilsDate.toDate(this.paymentDetail.payment_date);

    }

  }

  getPatientName(patientId:string){
    this.patientPersist.getPatient(patientId).subscribe((
      patient
    )=>{
      this.patientFullName =    `${patient.fname} ${patient.mname} ${patient.lname}` ;


    }

    )
  }

  getFeeTypes():void{
    this.feePersist.feesDo("get_all_fee",{}).subscribe(
      (fees:FeeSummary[])=>{
        this.fees= fees;

      }
    )
  }

  addDeposit_History(paymentDetail:PaymentDetail, parent_id:string): void {
    this.deposit_historyDetail.deposit_amount = this.paymentDetail.total;
    this.deposit_historyDetail.deposit_date = this.tcUtilsDate.toTimeStamp(this.deposit_date);
    this.deposit_historyDetail.deposit_id = parent_id;
    this.deposit_historyDetail.deposit_type = deposit_history_type.medical_expense;
    this.deposit_historyDetail.patient_id = paymentDetail.patient_id;

      this.depositPersist.addDeposit_History(this.deposit_historyDetail.deposit_id, this.deposit_historyDetail).subscribe(value => {
        this.tcNotification.success("Deposit_History added");
        this.deposit_historyDetail.id = value.id;
      });
    }

    is_deposit_payment_checked(){
      this.is_deposit_payment = !this.is_deposit_payment;
    }

  feeChanged(fee:FeeSummary){
 

  }



}
