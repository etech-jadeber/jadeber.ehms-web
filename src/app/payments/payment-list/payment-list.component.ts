import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {PaymentPersist} from "../payment.persist";
import {PaymentNavigator} from "../payment.navigator";
import {PaymentByPatient, PaymentByPatientPartialList, PaymentDetail, paymentFilterColumn, PaymentSummary, PaymentSummaryPartialList} from "../payment.model";
import {PatientPersist} from 'src/app/patients/patients.persist';
import {PatientSummary, PatientSummaryPartialList} from 'src/app/patients/patients.model';
import {TCWizardProgress, TCWizardProgressModes} from 'src/app/tc/models';
import {payment_type} from 'src/app/app.enums';
import {AppointmentNavigator} from 'src/app/appointments/appointment.navigator';
import { DateEnv } from '@fullcalendar/core';


@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.css']
})
export class PaymentListComponent implements OnInit {

  paymentsData: PaymentByPatient[] = [];
  paymentsTotalCount: number = 0;
  patientSummary: PaymentByPatient [] = []
  paymentsSelectAll: boolean = false;
  paymentsSelection: PaymentByPatient[] = [];
  paymentByPatientFilterOption = paymentFilterColumn

  paymentsDisplayedColumns: string[] = [ "pid", "patient_name", 'phone_cell', "not_paid", "paid",
                                        "remaining_price"];
  patientSearchText: FormControl = new FormControl();
  paymentsIsLoading: boolean = false;
  is_deposit_payment: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paymentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentsSort: MatSort;
  
  date: FormControl = new FormControl({disabled: true, value: new Date()});
  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              private appointmentNavigator: AppointmentNavigator,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              private patientPersisit: PatientPersist,
              public paymentPersist: PaymentPersist,
              public paymentNavigator: PaymentNavigator,
              public jobPersist: JobPersist,
  ) {
    // this.paymentPersist.paymentByPatientSearchHistory.search_text = "";
    this.tcAuthorization.requireRead("payments");
    this.patientSearchText.setValue(paymentPersist.paymentByPatientSearchHistory.search_text);
    //delay subsequent keyup events
    this.patientSearchText.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.paymentPersist.paymentByPatientSearchHistory.search_text = value;
        this.searchPayments();
    });
  }

  ngOnInit() {
    this.paymentsSort.sortChange.subscribe(() => {
      this.paymentsPaginator.pageIndex = 0;
      this.searchPayments(true);
    });

    this.paymentsPaginator.page.subscribe(() => {
      this.searchPayments(true);
    });
    //start by loading items
    this.searchPayments();
    this.paymentPersist.paymentByPatientSearchHistory.ordered_date = this.tcUtilsDate.toTimeStamp(new Date())
    this.date.valueChanges.subscribe((value)=>{
      this.paymentPersist.paymentByPatientSearchHistory.ordered_date = value ?  this.tcUtilsDate.toTimeStamp(value._d) : null;
      this.searchPayments()
  });
  }

  searchPayments(isPagination: boolean = false): void {
    let paginator = this.paymentsPaginator;
    let sorter = this.paymentsSort;
    this.paymentsIsLoading = true;
    this.paymentsSelection = [];
    this.paymentPersist.paymentsDo("get_payment_by_patient", {
      order_date: this.date.value ? Math.floor(this.tcUtilsDate.toTimeStamp(this.date.value)) : null,
      fee_type: this.paymentPersist.paymentByPatientSearchHistory.fee_type,
      ps: paginator.pageSize ? paginator.pageSize : 10, 
      pi: isPagination ? paginator.pageIndex : 0, 
      sc: sorter.active, 
      so: sorter.direction,
    st: this.paymentPersist.paymentByPatientSearchHistory.search_text}).subscribe((partialList: PaymentByPatientPartialList) => {
        this.paymentsData = partialList.data;
        if (partialList.total != -1){
          this.paymentsTotalCount = partialList.total;
        }
        this.paymentsIsLoading = false;
    }, error => {
      this.paymentsIsLoading = false;
    })
  }

  navigateToDetail(payment: PaymentByPatient): void {
    // this.paymentPersist.company = payment.company
    this.paymentNavigator.viewPayment(payment.patient_id)
  }

  downloadPayments(): void {
    if (this.paymentsSelectAll) {
      this.paymentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download payments", true);
      });
    } else {
      this.paymentPersist.download(this.tcUtilsArray.idsList(this.paymentsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download payments", true);
      });
    }
  }

  editPayment(item: PaymentSummary) {
    let dialogRef = this.paymentNavigator.editPayment(null, item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePayment(item: PaymentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Payment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.paymentPersist.deletePayment(item.id).subscribe(response => {
          this.tcNotification.success("Payment deleted");
          this.searchPayments();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }
}
