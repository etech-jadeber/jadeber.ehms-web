import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PaymentPickComponent } from './payment-pick.component';

describe('PaymentPickComponent', () => {
  let component: PaymentPickComponent;
  let fixture: ComponentFixture<PaymentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
