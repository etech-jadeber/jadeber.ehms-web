import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {PaymentDetail, PaymentSummary, PaymentSummaryPartialList} from "../payment.model";
import {PaymentPersist} from "../payment.persist";


@Component({
  selector: 'app-payment-pick',
  templateUrl: './payment-pick.component.html',
  styleUrls: ['./payment-pick.component.css']
})
export class PaymentPickComponent implements OnInit {

  paymentsData: PaymentSummary[] = [];
  paymentsTotalCount: number = 0;
  paymentsSelection: PaymentSummary[] = [];
  paymentsDisplayedColumns: string[] = ["select", 'crv','patient_id','fee_id','vat_included','price_before','vat','total','payment_date' ];

  paymentsSearchTextBox: FormControl = new FormControl();
  paymentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paymentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public paymentPersist: PaymentPersist,
              public dialogRef: MatDialogRef<PaymentPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("payments");
    this.paymentsSearchTextBox.setValue(paymentPersist.paymentSearchHistory.search_text);
    //delay subsequent keyup events
    this.paymentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.paymentPersist.paymentSearchHistory.search_text = value;
      this.searchPayments();
    });
  }

  ngOnInit() {

    this.paymentsSort.sortChange.subscribe(() => {
      this.paymentsPaginator.pageIndex = 0;
      this.searchPayments();
    });

    this.paymentsPaginator.page.subscribe(() => {
      this.searchPayments();
    });

    //set initial picker list to 5
    this.paymentsPaginator.pageSize = 5;

    //start by loading items
    this.searchPayments();
  }

  searchPayments(): void {
    this.paymentsIsLoading = true;
    this.paymentsSelection = [];

    this.paymentPersist.searchPayment(this.paymentsPaginator.pageSize,
        this.paymentsPaginator.pageIndex,
        this.paymentsSort.active,
        this.paymentsSort.direction).subscribe((partialList: PaymentSummaryPartialList) => {
      this.paymentsData = partialList.data;
      if (partialList.total != -1) {
        this.paymentsTotalCount = partialList.total;
      }
      this.paymentsIsLoading = false;
    }, error => {
      this.paymentsIsLoading = false;
    });

  }

  markOneItem(item: PaymentSummary) {
    if(!this.tcUtilsArray.containsId(this.paymentsSelection,item.id)){
          this.paymentsSelection = [];
          this.paymentsSelection.push(item);
        }
        else{
          this.paymentsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.paymentsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}

