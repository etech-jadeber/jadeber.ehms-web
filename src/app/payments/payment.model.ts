import {TCId, TCString} from "../tc/models";

export const paymentFilterColumn: TCString[] = [
  new TCString( "pid", 'MRN'),
  new TCString( "patient_name", 'Patient Name'),
  new TCString( "phone_cell", 'Phone Number'),
  ];

export type Prefixed<T> = {
  [K in keyof T as `payment_${string & K}`]: T[K];
};

export type PrefixedPayment = Prefixed<PaymentDetail>;

export class PaymentSummary extends TCId {
  target: string;
  name: string;
  amount: number;
  patient_id : string;
crv : string;
fee_id : string;
vat_included : number;
price : number;
vat : number;
total : number;
payment_date : number;
target_id : string;
payment_status : number;
fee_name: string
additional_payment: number;
discount_payment: number;
provider_id: string;
provider_name: string;
net_price: number;
payment_type: number;
cashier_id: string;
cashier_name: string;
patient_name: string;
company_id: string;
company_name: string;
fee_type: number;
quantity: number;
  item_id: string;
  bank_id: string;
  transaction_no: string;
  tin_no: string;
}

export class PaymentByPatient{
  patient_id: string;
  patient_name: string;
  not_paid: number;
  paid: number;
  remaining_price: number;
  company: string;
}

export class PaymentByPatientPartialList{
  data: PaymentByPatient[];
  total: number;
}

export class TotalPaymentSummery{
  total_price: number;
  total_additional_payment: number;
  total_discount_payment: number;
  total_net_price: number;
  total_patient_price?: number;
  total_company_price?: number;
}

export class PaymentSummaryPartialList {
  data: PaymentSummary[];
  total: number;
}

export class PaymentReportList {
  data: [PaymentSummary[], number];
  total: number
}

export class PaymentDetail extends PaymentSummary {
  target: string;
  patient_id : string;
crv : string;
fee_id : string;
vat_included : number;
price : number;
vat : number;
total : number;
payment_date : number;
deposit_type:number;
target_id : string;
payment_status : number;
fee_name: string;
additional_payment: number;
discount_payment: number;
provider_id: string;
provider_name: string;
net_price: number;
payment_type: number;
patient_name: string;
employee_payment: number;
company_payment: number;
employee_id: string;
quantity: number;
}

export class PaymentDashboard {
  total: number;
  free: number;
  paid: number;
  not_paid: number;
}
