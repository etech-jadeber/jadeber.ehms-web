import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {PaymentEditComponent} from "./payment-edit/payment-edit.component";
import {PaymentPickComponent} from "./payment-pick/payment-pick.component";
import { PaymentDetail } from "./payment.model";


@Injectable({
  providedIn: 'root'
})

export class PaymentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  paymentsUrl(): string {
    return "/payments";
  }

  paymentUrl(id: string): string {
    return "/payments/" + id;
  }

  viewPayments(): void {
    this.router.navigateByUrl(this.paymentsUrl());
  }

  viewPayment(id: string): void {
    this.router.navigateByUrl(this.paymentUrl(id));
  }

  editPayment(patientId:string=null,id: string,isWizard: boolean = false): MatDialogRef<unknown,any> {

    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = isWizard

      ? TCModalModes.WIZARD
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;

    const dialogRef = this.dialog.open(PaymentEditComponent, {
      data: new TCParentChildIds(patientId, id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;

  }

  addAppointmentPayment(appointmentId:string=null, ): MatDialogRef<unknown,any> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = TCModalModes.WIZARD;
    params["appointmentId"] = appointmentId;
    const dialogRef = this.dialog.open(PaymentEditComponent, {
      data: new TCParentChildIds(null, null, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addPayment(patientId:string=null,isWizard: boolean = false): MatDialogRef<unknown,any> {

    return this.editPayment(patientId,null,isWizard);
  }

   pickPayments(selectOne: boolean=false,): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(PaymentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
