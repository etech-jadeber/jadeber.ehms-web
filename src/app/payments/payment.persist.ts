import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";


import { TCDoParam, TCUrlParams, TCUtilsHttp } from "../tc/utils-http";
import { TCId, TcDictionary, TCEnum, TCEnumTranslation } from "../tc/models";
import { PaymentDashboard, PaymentDetail, PaymentSummaryPartialList } from "./payment.model";
import { AdditionalPaymentType, CreditStatus, DiscountPaymentType, fee_type, GraphBy, other_type, PaymentType, payment_report_group_by, payment_status, payment_type } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { AppTranslation } from '../app.translation';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class PaymentPersist {

  // paymentSearchText: string = "";
  // patientSearchText: string = "";
  // last_days :number = -1
  //above constructor
  // payment_status_Id: number;
  // patient_id: string;
  // company_id: string;
  // outsourcing_company_id: string;
  // company: String;
  // isReport: boolean = false;
  // provider_id: string;
  // cashier_id: string;
  // fee_id: string;
  // ordered_date: number;
  // payment_date: number;
  // report_display_type_filter: number;
  // service_type: number;
  // schedule_type: number;
  // ward_id: string;


  // creditStatusFilter: number;
  CreditStatus: TCEnum[] = [
    new TCEnum(CreditStatus.covered, 'Covered'),
    new TCEnum(CreditStatus.not_covered, "Not Covered")
  ]


  constructor(private http: HttpClient, public appTranslation: AppTranslation, public tcUtilsDate: TCUtilsDate) {
  }
  payment_status: TCEnumTranslation[] = [
    new TCEnumTranslation(payment_status.paid, this.appTranslation.getKey('financial', 'paid')),
    new TCEnumTranslation(payment_status.not_paid, this.appTranslation.getKey('financial', 'not_paid')),
    new TCEnumTranslation(payment_status.covered, this.appTranslation.getKey('financial', 'covered')),
    new TCEnumTranslation(payment_status.Cancelled, this.appTranslation.getKey('financial', 'cancel')),
    new TCEnumTranslation(payment_status.Refunded, this.appTranslation.getKey('financial', 'refund')),
  ];
  // fee_typeId: number;

  // fee_type: TCEnumTranslation[] = [
  //   new TCEnumTranslation(fee_type.bed, this.appTranslation.getKey('bed', 'bed')),
  //   new TCEnumTranslation(fee_type.item, this.appTranslation.getKey('inventory', 'item')),
  //   new TCEnumTranslation(fee_type.card, this.appTranslation.getKey('financial', 'card_fee')),
  //   new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure')),
  //   new TCEnumTranslation(fee_type.lab_test, this.appTranslation.getKey('laboratory', 'lab_test_id')),
  //   new TCEnumTranslation(fee_type.lab_panel, this.appTranslation.getKey('laboratory', 'lab_panel_id')),
  //   new TCEnumTranslation(fee_type.dialysis, this.appTranslation.getKey('laboratory', 'dialysis')),
  //   new TCEnumTranslation(fee_type.physio_theraphy, this.appTranslation.getKey('patient', 'physiotherapy')),
  //   new TCEnumTranslation(fee_type.rad_test, this.appTranslation.getKey('laboratory', 'rad_test')),
  //   new TCEnumTranslation(fee_type.rad_panel, this.appTranslation.getKey('laboratory', 'rad_panel')),
  //   new TCEnumTranslation(fee_type.other_service, this.appTranslation.getKey('laboratory', 'other_service')),
  //   new TCEnumTranslation(fee_type.oxygen_service, this.appTranslation.getKey('laboratory', 'oxygen_service')),
  //   new TCEnumTranslation(fee_type.ambulance_in_km, this.appTranslation.getKey('laboratory', 'ambulance_in_km')),
  //   new TCEnumTranslation(fee_type.ambulance_in_waiting, this.appTranslation.getKey('laboratory', 'ambulance_in_waiting')),
  //   new TCEnumTranslation(fee_type.medical_package, this.appTranslation.getKey('laboratory', 'medical_package')),
  //   new TCEnumTranslation(fee_type.operation_room_fee, this.appTranslation.getKey('laboratory', 'operation_room_fee')),
  // ];

  fee_type: TCEnumTranslation[] = [
    new TCEnumTranslation(fee_type.bed, this.appTranslation.getKey('bed', 'bed')),
    new TCEnumTranslation(fee_type.item, this.appTranslation.getKey('inventory', 'item')),
    new TCEnumTranslation(fee_type.card, this.appTranslation.getKey('financial', 'card_fee')),
    new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure')),
    new TCEnumTranslation(fee_type.lab_test, this.appTranslation.getKey('laboratory', 'lab_test_id')),
    new TCEnumTranslation(fee_type.lab_panel, this.appTranslation.getKey('laboratory', 'lab_panel_id')),
    new TCEnumTranslation(fee_type.dialysis, this.appTranslation.getKey('laboratory', 'dialysis')),
    new TCEnumTranslation(fee_type.physio_theraphy, this.appTranslation.getKey('patient', 'physiotherapy')),
    new TCEnumTranslation(fee_type.rad_test, this.appTranslation.getKey('laboratory', 'rad_test')),
    new TCEnumTranslation(fee_type.rad_panel, this.appTranslation.getKey('laboratory', 'rad_panel')),
    new TCEnumTranslation(fee_type.pat_test, this.appTranslation.getKey('laboratory', 'pat_test')),
    new TCEnumTranslation(fee_type.pat_panel, this.appTranslation.getKey('laboratory', 'pat_panel')),
    new TCEnumTranslation(fee_type.other_service, this.appTranslation.getKey('financial', 'other_fee')),
    new TCEnumTranslation(fee_type.ambulance_in_km, this.appTranslation.getKey('financial', 'ambulance_in_km')),
    new TCEnumTranslation(fee_type.ambulance_in_waiting, this.appTranslation.getKey('financial', 'ambulance_in_waiting')),
    new TCEnumTranslation(fee_type.doctor, this.appTranslation.getKey('employee', 'doctor')),
    new TCEnumTranslation(fee_type.operation_room_fee, this.appTranslation.getKey('financial', 'operation_room_fee')),
    new TCEnumTranslation(fee_type.oxygen_service, this.appTranslation.getKey('financial', 'oxygen_service')),
    new TCEnumTranslation(fee_type.medical_package, this.appTranslation.getKey('laboratory', 'medical_package')),
  ];

  ordered_fee_type: TCEnumTranslation[] = [
    new TCEnumTranslation(fee_type.card, this.appTranslation.getKey('financial', 'card_fee')),
    new TCEnumTranslation(fee_type.procedure, this.appTranslation.getKey('procedure', 'procedure')),
    new TCEnumTranslation(fee_type.lab_test, this.appTranslation.getKey('laboratory', 'lab_test_id')),
    new TCEnumTranslation(fee_type.lab_panel, this.appTranslation.getKey('laboratory', 'lab_panel_id')),
    new TCEnumTranslation(fee_type.dialysis, this.appTranslation.getKey('laboratory', 'dialysis')),
    new TCEnumTranslation(fee_type.physio_theraphy, this.appTranslation.getKey('patient', 'physiotherapy')),
    new TCEnumTranslation(fee_type.rad_test, this.appTranslation.getKey('laboratory', 'rad_test')),
    new TCEnumTranslation(fee_type.rad_panel, this.appTranslation.getKey('laboratory', 'rad_panel')),
    new TCEnumTranslation(fee_type.pat_test, this.appTranslation.getKey('laboratory', 'pat_test')),
    new TCEnumTranslation(fee_type.pat_panel, this.appTranslation.getKey('laboratory', 'pat_panel')),
    new TCEnumTranslation(fee_type.other_service, this.appTranslation.getKey('financial', 'other_fee'))
  ];

  creditFeeType: TCEnumTranslation[] = [
    ...this.fee_type,
  ]

  // paymentReportGroupByFilter: number;

  paymentReportGroupBy: TCEnum[] = [
    new TCEnum(payment_report_group_by.fee, "Fee"),
    new TCEnum(payment_report_group_by.doctor, "Doctor"),
    new TCEnum(payment_report_group_by.cashier, "Cashier"),
    new TCEnum(payment_report_group_by.company, "Company"),
    new TCEnum(payment_report_group_by.date, "Date"),
    new TCEnum(payment_report_group_by.patient, "Patient"),
    new TCEnum(payment_report_group_by.payment_status, "Payment Status"),
    new TCEnum(payment_report_group_by.payment_type, "Payment Type"),
    new TCEnum(payment_report_group_by.fee_type, "Fee Type"),
    new TCEnum(payment_report_group_by.ward, "Ward"),
  ];

  discountPaymentTypeFilter: number;

  DiscountPaymentType: TCEnum[] = [
    new TCEnum(DiscountPaymentType.cash, 'Cash'),
    new TCEnum(DiscountPaymentType.credit, 'Credit'),

  ];

  additionalPaymentTypeFilter: number;

  AdditionalPaymentType: TCEnum[] = [
    new TCEnum(AdditionalPaymentType.vat, 'Vat'),

  ];


  graph_by: number = GraphBy.doctor;

  Graph_By: TCEnum[] = [
    new TCEnum(GraphBy.patient, 'Patient'),
    new TCEnum(GraphBy.cashier, 'Cashier'),
    new TCEnum(GraphBy.company, 'Company'),
    new TCEnum(GraphBy.doctor, 'Provider'),
    new TCEnum(GraphBy.fee, 'Fee'),
    new TCEnum(GraphBy.fee_type, 'Fee Type'),
    new TCEnum(GraphBy.payment, 'Payment'),
  ];

  paymentSearchHistory = {
    "deposit_id": "",
    "patient_id": "",
    "patient_name": "",
    "is_report": false,
    "payment_status": undefined,
    "payment_type": undefined,
    "company_id": "",
    "company_name": "",
    "outsourcing_company_id": "",
    "outsourcing_company_name": "",
    "provider_id": "",
    "provider_name": "",
    "cashier_id": "",
    "cashier_name": "",
    "fee_id": "",
    "fee_name": "",
    "item_id": "",
    "item_name": "",
    "fee_type": undefined,
    "start_date": this.tcUtilsDate.toTimeStamp(new Date),
    "end_date": undefined,
    "payment_date": undefined,
    "ordered_date": undefined,
    "credit_status": undefined,
    "ward_id": "",
    "ward_name": "",
    "schedule_type": undefined,
    "service_type": undefined,
    "group_by": undefined,
    "search_text": "",
    "search_column": "",
  }

  defaultPaymentSearchHistory = {...this.paymentSearchHistory}

  resetPaymentSearchHistory(){
  this.paymentSearchHistory = {...this.defaultPaymentSearchHistory}
  }

  creditPaymentSearchHistory = {...this.paymentSearchHistory, payment_status: payment_status.paid}

  defaultCreditPaymentSearchHistory = {...this.creditPaymentSearchHistory}

  resetCreditPaymentSearchHistory(){
  this.creditPaymentSearchHistory = {...this.defaultCreditPaymentSearchHistory}
  }

  paymentReportSearchHistory = {...this.paymentSearchHistory, "is_report": true}

  defaultPaymentReportSearchHistory = {...this.paymentReportSearchHistory}

  resetPaymentReportSearchHistory(){
  this.paymentReportSearchHistory = {...this.defaultPaymentReportSearchHistory}
  }

  notPaidPaymentSearchHistory = {...this.paymentSearchHistory, "ordered_date": this.tcUtilsDate.toTimeStamp(new Date)}

  defaultNotPaidPaymentSearchHistory = {...this.notPaidPaymentSearchHistory}

  resetNotPaidPaymentSearchHistory(){
    this.notPaidPaymentSearchHistory = {...this.defaultNotPaidPaymentSearchHistory}
  }

  paidPaymentSearchHistory = {...this.paymentSearchHistory}

  defaultPaidPaymentSearchHistory = {...this.paidPaymentSearchHistory}

  resetPaidPaymentSearchHistory(){
    this.paidPaymentSearchHistory = {...this.defaultPaidPaymentSearchHistory}
  }

  searchPayment(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.paymentSearchHistory): Observable<PaymentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("payments", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // url = TCUtilsString.appendUrlParameter(url, "last_days", this.last_days.toString())
    // url = TCUtilsString.appendUrlParameter(url, "is_report", `${this.isReport}`)
    // if(this.patient_id){
    //   url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patient_id)
    // }
    // if(this.payment_status_Id){
    //   url = TCUtilsString.appendUrlParameter(url, "payment_status", this.payment_status_Id.toString())
    // }
    // if (this.paymentTypeFilter){
    //   url = TCUtilsString.appendUrlParameter(url, "payment_type", this.paymentTypeFilter.toString())
    // }
    // if (this.company_id){
    //   url = TCUtilsString.appendUrlParameter(url, "company_id", this.company_id)
    // }
    // if (this.outsourcing_company_id){
    //   url = TCUtilsString.appendUrlParameter(url, "outsourcing_company_id", this.outsourcing_company_id)
    // }
    // if (this.provider_id){
    //   url = TCUtilsString.appendUrlParameter(url, "provider_id", this.provider_id)
    // }
    // if (this.cashier_id){
    //   url = TCUtilsString.appendUrlParameter(url, "cashier_id", this.cashier_id)
    // }
    // if (this.fee_id){
    //   url = TCUtilsString.appendUrlParameter(url, "fee_id", this.fee_id.toString())
    // }
    // if (this.fee_typeId){
    //   url = TCUtilsString.appendUrlParameter(url, "fee_type", this.fee_typeId.toString())
    // }
    // if (this.start_date){
    //   url = TCUtilsString.appendUrlParameter(url, "start_date", this.start_date.toString())
    // }
    // if (this.ordered_date){
    //   url = TCUtilsString.appendUrlParameter(url, "ordered_date", this.ordered_date.toString())
    // }
    // if (this.payment_date){
    //   url = TCUtilsString.appendUrlParameter(url, "payment_date", this.payment_date.toString())
    // }
    // if (this.end_date){
    //   url = TCUtilsString.appendUrlParameter(url, "end_date", this.end_date.toString())
    // }
    // if (this.creditStatusFilter){
    //   url = TCUtilsString.appendUrlParameter(url, "credit_status", this.creditStatusFilter.toString())
    // }
    // if (this.ward_id){
    //   url = TCUtilsString.appendUrlParameter(url, "ward_id", this.ward_id)
    // }
    // if (this.schedule_type){
    //   url = TCUtilsString.appendUrlParameter(url, "schedule_type", this.schedule_type.toString())
    // }
    // if (this.service_type){
    //   url = TCUtilsString.appendUrlParameter(url, "service_type", this.service_type.toString())
    // }
    // if (this.paymentReportGroupByFilter){
    //   url = TCUtilsString.appendUrlParameter(url, "payment_report_group_by", this.paymentReportGroupByFilter.toString())
    // }
    return this.http.get<PaymentSummaryPartialList>(url);

  }

  searchByPatientName(patientName: string): Observable<PaymentSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("search-by-patient-name", patientName);
    return this.http.get<PaymentSummaryPartialList>(url);
  }

  filters(searchHistory = this.paymentSearchHistory): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payments/do", new TCDoParam("download_all", this.filters()));
  }

  paymentDashboard(): Observable<PaymentDashboard> {
    return this.http.get<PaymentDashboard>(environment.tcApiBaseUri + "payments/dashboard");
  }

  getPayment(id: string): Observable<PaymentDetail> {
    return this.http.get<PaymentDetail>(environment.tcApiBaseUri + "payments/" + id);
  }

  addPayment(item: { patient_id: string, provider_id: string, fee_type: number }): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payments/", item);
  }

  updatePayment(item: PaymentDetail): Observable<PaymentDetail> {
    return this.http.patch<PaymentDetail>(environment.tcApiBaseUri + "payments/" + item.id, item);
  }

  deletePayment(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "payments/" + id);
  }

  paymentByPatientSearchHistory = {
    "search_text": "",
    "search_column": "",
    "ordered_date": this.tcUtilsDate.toTimeStamp(new Date()),
    "fee_type": undefined,
  }

  defaultPaymentByPatientSearchHistory = {...this.paymentByPatientSearchHistory}

  resetPaymentByPatientSearchHistory(){
  this.paymentByPatientSearchHistory = {...this.defaultPaymentByPatientSearchHistory}
  }

  paymentsDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string, searchHistory = this.paymentByPatientSearchHistory): Observable<any> {
    let url = TCUtilsHttp.buildSearchUrl("payments/do", searchHistory.search_text, pageSize, pageIndex, sort, order)
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    return this.http.post<any>(url, new TCDoParam(method, payload));
  }

  paymentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "payments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "payments/" + id + "/do", new TCDoParam("print", {}));
  }

  other_typeId: number;

  other_type: TCEnum[] = [
    new TCEnum(other_type.no_type, 'no_vat'),
    new TCEnum(other_type.vat, 'vat'),
    new TCEnum(other_type.tot, 'tot'),
  ];

  // start_date: number;
  //   end_date: number;

  // paymentTypeFilter: number;

  paymentType: TCEnum[] = [
    new TCEnum(PaymentType.Cash, 'Cash'),
    new TCEnum(PaymentType.Deposit, 'Deposit'),
    new TCEnum(PaymentType.transfer, 'Transfer'),
    new TCEnum(PaymentType.CreditByEmployee, 'Credit By Employee'),
    new TCEnum(PaymentType.CreditByFamily, 'Credit By Family'),
    // new TCEnum(PaymentType.Card, 'Card'),
    // new TCEnum(PaymentType.Free, 'Free'),

  ];

  coPaymentType: TCEnum[] = [
    new TCEnum(PaymentType.Cash, 'Cash'),
    new TCEnum(PaymentType.Deposit, 'Deposit'),
    new TCEnum(PaymentType.transfer, 'Transfer'),

  ];

  banks: TCEnum[] = [
    new TCEnum(PaymentType.Cash, 'Ahadu Bank'),
    new TCEnum(PaymentType.Deposit, 'Abyssinia Bank'),
    new TCEnum(PaymentType.CreditByEmployee, 'National Bank of Ethiopia'),

  ];





}
