import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {AppTranslation} from '../app.translation';

import {PatientCasePersisit} from '../patients/PatientCase.persisit';
import {PatientDashboard, PatientDetail, PatientSummary, PatientSummaryPartialList,PatientStatistics, ICD10_report} from '../patients/patients.model';
import {PatientNavigator} from '../patients/patients.navigator';
import {PatientPersist} from '../patients/patients.persist';
import {UserContexts} from '../tc/app.enums';
import {TCAuthorization} from '../tc/authorization';
import {CaseDashboard, CaseSummary} from '../tc/cases/case.model';
import {TCNavigator} from '../tc/navigator';
import {TCNotification} from '../tc/notification';
import {TCUtilsArray} from '../tc/utils-array';
import {TCUtilsDate} from '../tc/utils-date';
import {WaitinglistPersist} from '../bed/waitinglists/waitinglist.persist';
import {ItemSummary, ItemSummaryPartialList} from "../items/item.model";
import {ItemPersist} from "../items/item.persist";
import {EmployeeNavigator} from "../storeemployees/employee.navigator";
import {ReceptionNavigator} from '../receptions/reception.navigator';
import {DoctorNavigator} from '../doctors/doctor.navigator';
import {PurchaseSummaryPartialList} from "../purchases/purchase.model";
import {PurchasePersist} from "../purchases/purchase.persist";
import { AdmitByWardReport } from '../form_encounters/admission_forms/admission_form.model';
import { PaymentDashboard } from '../payments/payment.model';
import { PrescriptionsSummary } from '../form_encounters/form_encounter.model';
import { FormControl } from '@angular/forms';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { StoreSummary } from '../stores/store.model';
import { ItemNavigator } from '../items/item.navigator';
import { StoreNavigator } from '../stores/store.navigator';
import { StorePersist } from '../stores/store.persist';
import { BedPersist } from '../bed/beds/bed.persist';
import { BedDashboard } from '../bed/beds/bed.model';
import { OrdersDashboard } from '../form_encounters/orderss/orders.model';
import { OrdersPersist } from '../form_encounters/orderss/orders.persist';
export enum PaginatorIndexes {
  prescriptionss,
}
@Component({
  selector: 'app-pharmacy-dashboard',
  templateUrl: './pharmacy-dashboard.component.html',
  styleUrls: ['./pharmacy-dashboard.component.css']
})
export class PharmacyDashboardComponent implements OnInit {


  bedTypes = [];
  itemTypes = [];
  itemsAboutToExpire = [];
  itemsExpired = [];
  patientDashboard: PatientDashboard;
  lab_order_dashboard: OrdersDashboard[];
  paymentDasboard: PaymentDashboard;
  caseDashboard: CaseDashboard;
  bedDashboard: BedDashboard;
  itemsToReorder = [];
  isPatientDashBoardLoading: boolean = false;
  isPatientDashBoardLoaded: boolean = false;
  isLab_order_dashboardLoading: boolean = false;
  isLab_order_dashboardLoaded: boolean = false;
  isPaymentDashboardLoading: boolean = false;
  isPaymentDashboardLoaded: boolean = false;
  casesByState = [];
  patientByState = [];
  patientCasesGeneralIsLoading: boolean = false;
  patientCasesGeneralData: CaseSummary[] = [];
  patientOfCase: PatientSummary[] = [];
  patientCasesGeneralTotalCount: number = 0
  chartColorScheme = {
    domain: ['#175D91', '#F79A20']
  };

  secondChartColorScheme = {
    domain: ['#F79A20']
  };
  CasesGeneralDisplayedColumns: string[] = [
    "action",
    "target_id"
  ];
  waitingListData = [];
  availableBedsForWaitingPeople: boolean = false;
  patientAppointmentCaseData: CaseSummary[];
  patientAppointmentCaseTotalCount: number = 0;
  ICD10_reportDisplayedColumns: String[] = ["name","infant", "child", "young", "old", "total"];
  ICD10_reportData: ICD10_report[] = [];
  ICD10_reportTotalCount: number = 0;
  admitByWardDisplayedColumns: String[] = ["name", "male", "female", "total"];
  admitByWardData: AdmitByWardReport[] = [];
  admitByWardTotalCount: number = 0;

  patientStatistics: PatientStatistics ;
  ageRangeDisplayColumn: string[] = ["sex","infant","child", "teen","young","old"];
  patientStatisticsBySex: PatientStatistics;

  //prescriptionss
  prescriptionssData: any[] = [];
  prescriptionssTotalCount: number = 0;
  prescriptionssSelectAll: boolean = false;
  prescriptionssSelected: PrescriptionsSummary[] = [];
  prescriptionssDisplayedColumns: string[] = ['select', 'action', 'drug', 'form', 'quantity', 'unit', 'note', 'in_patient'];
  prescriptionssSearchTextBox: FormControl = new FormControl();
  prescriptionssLoading: boolean = false;
  prescriptionssSelection: PrescriptionsSummary[] = [];

  //items
  itemsData: ItemSummary[] = [];
  itemsTotalCount: number = 0;
  itemsSelectAll: boolean = false;
  itemsSelection: ItemSummary[] = [];
  stores: StoreSummary[] = [];

  itemsDisplayedColumns: string[] = [
    'select',
    'action',
    'name',
    'item_type',
    'item_code',
    'quantity',
    "reorder_point"
  ];
  itemsSearchTextBox: FormControl = new FormControl();
  itemsIsLoading: boolean = false;
  showReorder: boolean = false;

  
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public tcUtilsArray: TCUtilsArray,
    public appTranslation: AppTranslation,
    private patientPersisit: PatientPersist,
    public casePersist: PatientCasePersisit,
    public patientNavigator: PatientNavigator,
    public employeeNavigator: EmployeeNavigator,
    public doctorNavigator: DoctorNavigator,
    public receptionNavigator: ReceptionNavigator,
    private bedPersist: BedPersist,
    private purchasePersist: PurchasePersist,
    public itemPersist: ItemPersist,
    private waitingListPersist: WaitinglistPersist,
    private lab_orders_persist: OrdersPersist,
    public form_encounterPersist: Form_EncounterPersist,
    public itemNavigator: ItemNavigator,
    public storePersist: StorePersist,
    public storeNavigator: StoreNavigator,

  ) {
    this.patientDashboard = new PatientDashboard();
    this.patientStatistics = new PatientStatistics();
    this.lab_order_dashboard = [];
    this.paymentDasboard = new PaymentDashboard();

    this.prescriptionssSearchTextBox.setValue(form_encounterPersist.prescriptionsSearchText);
    this.prescriptionssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.form_encounterPersist.prescriptionsSearchText = value.trim();
      this.searchPrescriptionss();
    });

        this.itemsSearchTextBox.setValue(itemPersist.itemSearchText);
        this.itemsSearchTextBox.valueChanges
          .pipe(debounceTime(500))
          .subscribe((value) => {
            this.itemPersist.itemSearchText = value;
            this.searchItems();
          });
  }


  ngOnInit() {
    //TO DO
    // needs to updated with real value
    this.percentageCalculator(200, 200);

    this.tcAuthorization.requireLogin();

    if (this.tcAuthorization.hasMandatorys()) {
      this.tcNavigator.mandatory();
      return;
    }
    if (
      this.tcAuthorization.getUserContexts(UserContexts.patient).length > 0
    ) {
      this.patientNavigator.goHome();
      return;
    }

    if (
      this.tcAuthorization.getUserContexts(UserContexts.employee).length > 0
    ) {
      this.employeeNavigator.goHome();
      return;
    }



    if (
      this.tcAuthorization.getUserContexts(UserContexts.nurse).length > 0
    ) {
      this.doctorNavigator.goNurseHome();
      return;
    }



    if (
      this.tcAuthorization.getUserContexts(UserContexts.lab).length > 0
    ) {
      this.doctorNavigator.goLabHome();
      return;
    }


    if (
      this.tcAuthorization.getUserContexts(UserContexts.front_desk).length > 0
    ) {
      this.receptionNavigator.goHome();
      return;
    }



    this.bedPersist.bedDashboard().subscribe(
      (res) => {
        this.bedTypes = [
          {
            name: this.appTranslation.getText("bed", "taken"),
            value: res.total_beds - res.available_beds,
          },
          {
            name: this.appTranslation.getText("bed", "free"),
            value: res.available_beds,
          },
        ];
      }
    )


    this.waitingListPersist.waitinglistDashboard().subscribe(
      (res) => {
        this.availableBedsForWaitingPeople = res.total > 0 && res.available_beds > 0;
        this.waitingListData = [
          {
            name: this.appTranslation.getText("patient", "people_waiting"),
            value: res.total,
          },
          {
            name: this.appTranslation.getText("bed", "available_beds_to_be_assigned"),
            value: Math.min(res.total, res.available_beds),
          },
        ];
      }
    );
    this.loadLabOrderDashboardInfo();
    this.loadMemberDashboardInfo();
    this.loadPatients();
    this.loadCloseExpireDate();
    this.loadToReorderItems();
    this.loadAllItems();
    this.loadExpiredItemsDate();
    this.searchPrescriptionss();
    this.searchItems();
  }
  ngAfterViewInit(): void {
      // prescriptionss sorter
      this.sorters.toArray()[PaginatorIndexes.prescriptionss].sortChange.subscribe(() => {
        this.paginators.toArray()[PaginatorIndexes.prescriptionss].pageIndex = 0;
        this.searchPrescriptionss(true);
      });
      // // prescriptionss paginator
      // this.paginators.toArray()[PaginatorIndexes.prescriptionss].page.subscribe(() => {
      //   this.searchPrescriptionss(true);
      // });
}
  loadMemberDashboardInfo() {
    if (this.tcAuthorization.canRead("patients")) {
      this.isPatientDashBoardLoading = true;
      this.patientPersisit.patientDashboard().subscribe(
        (data) => {
          this.patientDashboard = data;
          this.isPatientDashBoardLoaded = true;
          this.isPatientDashBoardLoading = false;


        },
        (error) => {
          this.isPatientDashBoardLoading = false;
        }
      );
      
    }
  }

  loadLabOrderDashboardInfo() {
      this.isLab_order_dashboardLoading = true;
      this.lab_orders_persist.ordersDashboard().subscribe(
        (data) => {
          this.lab_order_dashboard = data;
          this.isLab_order_dashboardLoaded = true;
          this.isLab_order_dashboardLoading = false;


        },
        (error) => {
          this.isLab_order_dashboardLoading = false;
        }
      );
      
  }  


  fullPatientName(patientId: string): string {

    let patient: PatientDetail = this.tcUtilsArray.getById(
      this.patientOfCase,
      patientId
    );
    return patient == undefined
      ? ""
      : patient.fname + " " + patient.mname + " " + patient.lname;
  }

  perctageFormat(value: number): string {
    const str = value.toFixed(2);
    return str;
  }


  loadPatients() {
    this.patientPersisit
      .patientsDo("get_all_patients", "")
      .subscribe((result) => {
        this.patientOfCase = (result as PatientSummaryPartialList).data;
        this.patientByState = [
          {
            name: this.appTranslation.getText("patient", "total_patient"),
            value: this.patientOfCase.length
          }
        ]
      });
  }

  getPatientFullName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patientOfCase,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }

  loadCloseExpireDate() {

    this.purchasePersist.purchasesDo("get_items_with_close_expire_date", {}).subscribe(
      (result: PurchaseSummaryPartialList) => {
        this.itemsAboutToExpire = [
          {
            name: this.appTranslation.getText("inventory", "items_about_to_expire"),
            value: result.total
          }
        ];
      }, error => {
        console.error(error);
      }
    );
  }
  loadExpiredItemsDate() {

    this.purchasePersist.purchasesDo("get_expired_items", {}).subscribe(
      (result: PurchaseSummaryPartialList) => {
        this.itemsExpired = [
          {
            name: "Expired Items",
            value: result.total
          }
        ];
      }, error => {
        console.error(error);
      }
    );
    }
  loadToReorderItems() {

    this.itemPersist.itemsDo("get_to_reorder_item", '').subscribe((result: ItemSummaryPartialList) => {
      this.itemsToReorder = [
        {
          name: this.appTranslation.getText("inventory", "items_to_reorder"),
          value: result.total,
        }
      ];
    }, error => {
      console.error(error)
    });
  }
  loadAllItems() {

    this.itemPersist.itemsDo("get_all_items", '').subscribe((result: any) => {
      this.itemTypes = [
  
        {
          name: "Drug",
          value: result.data[0]["drug"],
        },        {
          name: "Consumable",
          value: result.data[0]["consumable"],
        },        {
          name: "Equipment",
          value: result.data[0]["equipment"],
        },
        {
          name: "Total number of items",
          value: result.total,
        }
      ];
    }, error => {
      console.error(error)
    });
  }
 

  percentageCalculator(value1: number, value2: number): [string, string, string] {
    const total = value1 + value2;
    const value1_percentage = (value1 / total) * 100;
    const value2_percentage = 100 - value1_percentage;
    return [this.setValue(total.toString()), this.setValue(value1_percentage.toFixed(2)), this.setValue(value2_percentage.toFixed(2))];
  }
  itemPercentageCalculator(value1: number, value2: number, value3:number): [string, string, string,string] {
    const total = value1 + value2 + value3;
    const value1_percentage = (value1 / total) * 100;
    const value2_percentage = (value2 / total) * 100;
    const value3_percentage = (value2 / total) * 100;

    return [this.setValue(value1_percentage.toFixed(2)),this.setValue(value2_percentage.toFixed(2)), this.setValue(value3_percentage.toFixed(2)),this.setValue(total.toString())];
  }
  setValue(value) {
    return value == "NaN" ? 0 : value;
  }
  prescriptionssDatas :any[] = [];
  //prescriptionss methods
  searchPrescriptionss(isPagination: boolean = false): void {
    // let paginator = this.paginators.toArray()[PaginatorIndexes.prescriptionss];
    // let sorter = this.sorters.toArray()[PaginatorIndexes.prescriptionss];
    this.prescriptionssSelected = [];
    this.prescriptionssLoading = true;
    this.form_encounterPersist.prescriptionsDashboard().subscribe(response => {
      this.prescriptionssData = response.data;
      console.log(response.data);  
      if (response.total != -1) {
        this.prescriptionssTotalCount = response.total;
      }
      this.prescriptionssLoading = false;
    }, error => {
      this.prescriptionssLoading = false;
    });
  }


  acceptPrescriptionOrder(item: PrescriptionsSummary) {

    this.form_encounterPersist.prescriptionsStatusDo(item.id, "acept_prescription_order", item).subscribe(downloadJob => {
      this.searchPrescriptionss();

  })
}

  voidPrescriptionOrder(item: PrescriptionsSummary){
    this.form_encounterPersist.prescriptionsStatusDo(item.id, "void_prescription_order", item).subscribe(downloadJob => {
      this.searchPrescriptionss();

  })
  }

  dispensePrescriptionOrder(item: PrescriptionsSummary){
    this.form_encounterPersist.prescriptionsStatusDo(item.id, "dispense_prescription_order", item).subscribe(downloadJob => {
      this.searchPrescriptionss();
  })
  }

  downloadItems(){}
  
  reloadCases(){

  }
  downloadPrescriptionss(){
    
  }

  searchItems(isPagination: boolean = false): void {

    this.itemsIsLoading = true;
    this.showReorder = false;
    this.itemsSelection = [];

    this.itemPersist
      .itemDashboard().subscribe(
        (partialList: any) => {
          this.itemsData = partialList.data;
          console.log(partialList)
          if (partialList.total != -1) {
            this.itemsTotalCount = partialList.total;
          }
          this.itemsIsLoading = false;
        },
        (error) => {
          this.itemsIsLoading = false;
        }
      );
  }
}
