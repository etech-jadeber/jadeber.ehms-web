import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationListComponent } from './physical-examination-list.component';

describe('PhysicalExaminationListComponent', () => {
  let component: PhysicalExaminationListComponent;
  let fixture: ComponentFixture<PhysicalExaminationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
