import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, interval } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { Follow_Up_NoteDetail, Follow_Up_NoteSummary, Physical_ExaminationDetail, Physical_ExaminationSummary, Rehabilitation_OrderDetail, Rehabilitation_OrderSummary } from '../form_encounters/form_encounter.model';
import { CommentDetail, CommentSummary } from '../tc/models';
import { TCAuthentication } from '../tc/authentication';
import { UserPersist } from '../tc/users/user.persist';
import { PhysicalExaminationOptionsPersist } from '../physical_examination_options/physical_examination_options.persist';
import { PhysicalExaminationOptions, PhysicalExaminationState } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
@Component({
  selector: 'app-physical-examination-list',
  templateUrl: './physical-examination-list.component.html',
  styleUrls: ['./physical-examination-list.component.css']
})
export class PhysicalExaminationListComponent implements OnInit {

  physical_examinationsData: Physical_ExaminationSummary[] = [];
  physical_examinationsTotalCount: number = 0;
  physical_examinationsSelectAll: boolean = false;
  physical_examinationLoading: boolean = false;
  physical_examinationsSelected: Physical_ExaminationSummary[] = [];
  physical_examinationsDisplayedColumns: string[] = [
    'select',
    'action',
    'general_appearance',
    'ent',
    'eye',
    'head_and_neck',
    'glands',
    'chests',
    'cardiovascular',
    'abdominal',
    'genitourinary',
    'musculoskeletal',
    'integument',
    'neurology',
    'encounter',
    'vital_sign',
  ];


  physical_examinationsSearchTextBox: FormControl = new FormControl();
  physical_examinationsLoading: boolean = false;
  physical_examinationResult = {};
  physicalOptions = PhysicalExaminationOptions
  physicalState = PhysicalExaminationState

  @Input() encounterId: any;
  @Input() edit: boolean;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public physicalExaminationOptionPersist: PhysicalExaminationOptionsPersist,
    public tcUtilsString: TCUtilsString,
  ) {
        
    this.physical_examinationsSearchTextBox.setValue(
      form_encounterPersist.physical_examinationSearchText
    );
    this.physical_examinationsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.physical_examinationSearchText =
          value.trim();
        this.searchPhysical_Examinations();
      });
   }

   ngAfterViewInit(): void {
    this.searchPhysical_Examinations();
   }

  ngOnInit(): void {
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchPhysical_Examinations(true);
    });
  // physical_examinations paginator
  this.paginator.page.subscribe(() => {
      this.searchPhysical_Examinations(true);
    });
  }



  searchPhysical_Examinations(isPagination: boolean = false): void {
    this.tcAuthorization.canRead("physical_examinations");
    this.physical_examinationResult = {}
    let paginator =
      this.paginator;
    let sorter = this.sorter;
    this.physical_examinationsSelected = [];
    this.physical_examinationsData = []
    this.physical_examinationsLoading = true;
    this.form_encounterPersist
      .searchPhysical_Examination(
        this.encounterId,
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (response) => {
          this.physical_examinationsData = response.data;
          // response.data.forEach((res) => {
          //   this.addToOption(res)
          // })
          if (response.total != -1) {
            this.onResult.emit(response.total);
          }
          this.physical_examinationsLoading = false;
        },
        (error) => {
          this.physical_examinationsLoading = false;
        }
      );
  }

  checkOptionNormalityCreated(option: number){
    if (!this.physical_examinationResult[option][this.physicalState.abnormal]){
      this.physical_examinationResult[option][this.physicalState.abnormal] = []
    }
    if (!this.physical_examinationResult[option][this.physicalState.normal]){
      this.physical_examinationResult[option][this.physicalState.normal] = []
    }
  }

  addToDetail(option: number, value: Physical_ExaminationSummary):void{
    this.checkOptionNormalityCreated(option)
    if (option == this.physicalOptions.abdominal){
      value.abdominal.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.cardiovascular){
      value.cardiovascular.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.chests){
      value.chests.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.ent){
      value.ent.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.eye){
      value.eye.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.genitourinary){
      value.genitourinary.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.glands){
      value.glands.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.musculoskeletal){
      value.musculoskeletal.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.head_and_neck){
      value.head_and_neck.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.integument){
      value.integument.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
    else if (option == this.physicalOptions.neurology){
      value.neurology.split(/,(?=[^"])/).forEach(
        res => {
          this.searchOptionsFindings(res, option)
        }
      )
    }
  }

  searchOptionsFindings(res: string, option: number):void{
    if(this.tcUtilsString.isValidId(res)){
    this.physicalExaminationOptionPersist.getPhysicalExaminationOptions(res).subscribe((result => {
      let state = this.physical_examinationResult[option][result.state]
      if (state == null){
        state = []
      }
      this.physical_examinationResult[option][result.state] = [...state, result.findings]
    }))
  } else if (res) {
    this.physical_examinationResult[option]['detail'] = JSON.parse(res)
  }
  }
  
  canGive(option: number, isNormal: boolean) : boolean{
    if (!this.physical_examinationResult[option].detail){
      return false
    }else if(isNormal ? !this.physical_examinationResult[option].detail.normal : !this.physical_examinationResult[option].detail.abnormal){
      return false
    }
    return true
  }

  addToOption(data: Physical_ExaminationSummary){
    for (const option of this.physicalExaminationOptionPersist.PhysicalExaminationOptions) {
      this.checkOptionCreated(option.id)
      this.addToDetail(option.id, data)
    }
  }

  checkOptionCreated(option: number) {
    if (!this.physical_examinationResult[option]){
      this.physical_examinationResult[option] = {}
    }
  }

  addPhysical_Examination(): void {
    if (this.physical_examinationsData.length > 0) {
      this.editPhysical_Examination(this.physical_examinationsData[0]);
      return;
    }
    let dialogRef = this.form_encounterNavigator.addPhysical_Examination(
      this.encounterId
    );

    dialogRef.afterClosed().subscribe((newPhysical_Examination) => {
      this.searchPhysical_Examinations();
    });
  }

  editPhysical_Examination(item: Physical_ExaminationDetail): void {
    let dialogRef = this.form_encounterNavigator.editPhysical_Examination(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((updatedPhysical_Examination) => {
      if (updatedPhysical_Examination) {
        this.searchPhysical_Examinations();
      }
    });
  }

  deletePhysical_Examination(item: Physical_ExaminationDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Physical_Examination');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deletePhysical_Examination(this.encounterId, item.id)
          .subscribe((response) => {
            this.tcNotification.success('physical_examination deleted');
            this.searchPhysical_Examinations();
          });
      }
      });
    }

    downloadPhysical_Examinations(): void {
      this.tcNotification.info(
      'Download physical_examinations : ' +
        this.physical_examinationsSelected.length
    );
  }

  back(): void {
    this.location.back();
  }

}
