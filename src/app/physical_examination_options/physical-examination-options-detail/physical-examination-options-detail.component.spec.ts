import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationOptionsDetailComponent } from './physical-examination-options-detail.component';

describe('PhysicalExaminationOptionsDetailComponent', () => {
  let component: PhysicalExaminationOptionsDetailComponent;
  let fixture: ComponentFixture<PhysicalExaminationOptionsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationOptionsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationOptionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
