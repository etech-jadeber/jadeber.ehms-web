import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PhysicalExaminationOptionsDetail} from "../physical_examination_options.model";
import {PhysicalExaminationOptionsPersist} from "../physical_examination_options.persist";
import {PhysicalExaminationOptionsNavigator} from "../physical_examination_options.navigator";

export enum PhysicalExaminationOptionsTabs {
  overview,
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-physical-examination-options-detail',
  templateUrl: './physical-examination-options-detail.component.html',
  styleUrls: ['./physical-examination-options-detail.component.scss']
})
export class PhysicalExaminationOptionsDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  physicalExaminationOptionsIsLoading:boolean = false;
  
  PhysicalExaminationOptionsTabs: typeof PhysicalExaminationOptionsTabs = PhysicalExaminationOptionsTabs;
  activeTab: PhysicalExaminationOptionsTabs = PhysicalExaminationOptionsTabs.overview;
  //basics
  physicalExaminationOptionsDetail: PhysicalExaminationOptionsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public physicalExaminationOptionsNavigator: PhysicalExaminationOptionsNavigator,
              public  physicalExaminationOptionsPersist: PhysicalExaminationOptionsPersist) {
    this.tcAuthorization.requireRead("physical_examination_options");
    this.physicalExaminationOptionsDetail = new PhysicalExaminationOptionsDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("physical_examination_options");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.physicalExaminationOptionsIsLoading = true;
    this.physicalExaminationOptionsPersist.getPhysicalExaminationOptions(id).subscribe(physicalExaminationOptionsDetail => {
          this.physicalExaminationOptionsDetail = physicalExaminationOptionsDetail;
          this.physicalExaminationOptionsIsLoading = false;
        }, error => {
          console.error(error);
          this.physicalExaminationOptionsIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.physicalExaminationOptionsDetail.id);
  }
  editPhysicalExaminationOptions(): void {
    let modalRef = this.physicalExaminationOptionsNavigator.editPhysicalExaminationOptions(this.physicalExaminationOptionsDetail.id);
    modalRef.afterClosed().subscribe(physicalExaminationOptionsDetail => {
      TCUtilsAngular.assign(this.physicalExaminationOptionsDetail, physicalExaminationOptionsDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.physicalExaminationOptionsPersist.print(this.physicalExaminationOptionsDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print physical_examination_options", true);
      });
    }

  back():void{
      this.location.back();
    }

}