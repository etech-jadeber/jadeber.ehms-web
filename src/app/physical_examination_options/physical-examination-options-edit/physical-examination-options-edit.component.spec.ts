import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationOptionsEditComponent } from './physical-examination-options-edit.component';

describe('PhysicalExaminationOptionsEditComponent', () => {
  let component: PhysicalExaminationOptionsEditComponent;
  let fixture: ComponentFixture<PhysicalExaminationOptionsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationOptionsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationOptionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
