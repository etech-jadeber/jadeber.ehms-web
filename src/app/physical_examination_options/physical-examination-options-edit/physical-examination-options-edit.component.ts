import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PhysicalExaminationOptionsDetail } from '../physical_examination_options.model';
import { PhysicalExaminationOptionsPersist } from '../physical_examination_options.persist';
import { Physical_ExaminationDetail } from 'src/app/form_encounters/form_encounter.model';


@Component({
  selector: 'app-physical-examination-options-edit',
  templateUrl: './physical-examination-options-edit.component.html',
  styleUrls: ['./physical-examination-options-edit.component.scss']
})
export class PhysicalExaminationOptionsEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  physicalExaminationOptionsDetail: PhysicalExaminationOptionsDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PhysicalExaminationOptionsEditComponent>,
              public  persist: PhysicalExaminationOptionsPersist,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("physical_examination_options");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient","physical_examination_options");
      this.physicalExaminationOptionsDetail = new PhysicalExaminationOptionsDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("physical_examination_options");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "physical_examination_options");
      this.isLoadingResults = true;
      this.persist.getPhysicalExaminationOptions(this.idMode.id).subscribe(physicalExaminationOptionsDetail => {
        this.physicalExaminationOptionsDetail = physicalExaminationOptionsDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }
  
  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPhysicalExaminationOptions(this.physicalExaminationOptionsDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "physical_examination_options") + " added");
      this.physicalExaminationOptionsDetail.id = value.id;
      this.dialogRef.close(this.physicalExaminationOptionsDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePhysicalExaminationOptions(this.physicalExaminationOptionsDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "physical_examination_options") + " updated");
      this.dialogRef.close(this.physicalExaminationOptionsDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.physicalExaminationOptionsDetail == null){
            return false;
          }

        if (this.physicalExaminationOptionsDetail.findings == null || this.physicalExaminationOptionsDetail.findings  == "") {
            return false;
        } 

        if (this.physicalExaminationOptionsDetail.state == null) {
            return false;
        }

        if (this.physicalExaminationOptionsDetail.option == null) {
            return false;
        }
        return true;
    }
 }