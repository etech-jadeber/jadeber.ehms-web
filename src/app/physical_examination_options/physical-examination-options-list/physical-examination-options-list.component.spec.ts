import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationOptionsListComponent } from './physical-examination-options-list.component';

describe('PhysicalExaminationOptionsListComponent', () => {
  let component: PhysicalExaminationOptionsListComponent;
  let fixture: ComponentFixture<PhysicalExaminationOptionsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationOptionsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationOptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
