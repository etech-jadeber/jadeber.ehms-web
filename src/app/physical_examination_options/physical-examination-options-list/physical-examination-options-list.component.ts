import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PhysicalExaminationOptionsSummary, PhysicalExaminationOptionsSummaryPartialList } from '../physical_examination_options.model';
import { PhysicalExaminationOptionsPersist } from '../physical_examination_options.persist';
import { PhysicalExaminationOptionsNavigator } from '../physical_examination_options.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';


@Component({
  selector: 'app-physical-examination_options-list',
  templateUrl: './physical-examination-options-list.component.html',
  styleUrls: ['./physical-examination-options-list.component.scss']
})
export class PhysicalExaminationOptionsListComponent implements OnInit {
  physicalExaminationOptionssData: PhysicalExaminationOptionsSummary[] = [];
  physicalExaminationOptionssTotalCount: number = 0;
  physicalExaminationOptionsSelectAll:boolean = false;
  physicalExaminationOptionsSelection: PhysicalExaminationOptionsSummary[] = [];

  physicalExaminationOptionssDisplayedColumns: string[] = ["select","action","option","state","findings" ];
  physicalExaminationOptionsSearchTextBox: FormControl = new FormControl();
  physicalExaminationOptionsIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) physicalExaminationOptionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) physicalExaminationOptionssSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public physicalExaminationOptionsPersist: PhysicalExaminationOptionsPersist,
                public physicalExaminationOptionsNavigator: PhysicalExaminationOptionsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("physical_examination_options");
       this.physicalExaminationOptionsSearchTextBox.setValue(physicalExaminationOptionsPersist.physicalExaminationOptionsSearchText);
      //delay subsequent keyup events
      this.physicalExaminationOptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.physicalExaminationOptionsPersist.physicalExaminationOptionsSearchText = value;
        this.searchPhysicalExaminationOptionss();
      });
    } ngOnInit() {
   
      this.physicalExaminationOptionssSort.sortChange.subscribe(() => {
        this.physicalExaminationOptionssPaginator.pageIndex = 0;
        this.searchPhysicalExaminationOptionss(true);
      });

      this.physicalExaminationOptionssPaginator.page.subscribe(() => {
        this.searchPhysicalExaminationOptionss(true);
      });
      //start by loading items
      this.searchPhysicalExaminationOptionss();
    }

  searchPhysicalExaminationOptionss(isPagination:boolean = false): void {


    let paginator = this.physicalExaminationOptionssPaginator;
    let sorter = this.physicalExaminationOptionssSort;

    this.physicalExaminationOptionsIsLoading = true;
    this.physicalExaminationOptionsSelection = [];

    this.physicalExaminationOptionsPersist.searchPhysicalExaminationOptions(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PhysicalExaminationOptionsSummaryPartialList) => {
      this.physicalExaminationOptionssData = partialList.data;
      if (partialList.total != -1) {
        this.physicalExaminationOptionssTotalCount = partialList.total;
      }
      this.physicalExaminationOptionsIsLoading = false;
    }, error => {
      this.physicalExaminationOptionsIsLoading = false;
    });

  } downloadPhysicalExaminationOptionss(): void {
    if(this.physicalExaminationOptionsSelectAll){
         this.physicalExaminationOptionsPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download physical_examination_options", true);
      });
    }
    else{
        this.physicalExaminationOptionsPersist.download(this.tcUtilsArray.idsList(this.physicalExaminationOptionsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download physical_examination_options",true);
            });
        }
  }
addPhysicalExaminationOptions(): void {
    let dialogRef = this.physicalExaminationOptionsNavigator.addPhysicalExaminationOptions();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPhysicalExaminationOptionss();
      }
    });
  }

  editPhysicalExaminationOptions(item: PhysicalExaminationOptionsSummary) {
    let dialogRef = this.physicalExaminationOptionsNavigator.editPhysicalExaminationOptions(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePhysicalExaminationOptions(item: PhysicalExaminationOptionsSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("physical_examination_options");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.physicalExaminationOptionsPersist.deletePhysicalExaminationOptions(item.id).subscribe(response => {
          this.tcNotification.success("physical_examination_options deleted");
          this.searchPhysicalExaminationOptionss();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}