import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysicalExaminationOptionsPickComponent } from './physical-examination-options-pick.component';

describe('PhysicalExaminationOptionsPickComponent', () => {
  let component: PhysicalExaminationOptionsPickComponent;
  let fixture: ComponentFixture<PhysicalExaminationOptionsPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysicalExaminationOptionsPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysicalExaminationOptionsPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
