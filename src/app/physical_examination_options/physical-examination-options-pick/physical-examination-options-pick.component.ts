import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PhysicalExaminationOptionsSummary, PhysicalExaminationOptionsSummaryPartialList } from '../physical_examination_options.model';
import { PhysicalExaminationOptionsPersist } from '../physical_examination_options.persist';
import { PhysicalExaminationOptionsNavigator } from '../physical_examination_options.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';

@Component({
  selector: 'app-physical-examination-options-pick',
  templateUrl: './physical-examination-options-pick.component.html',
  styleUrls: ['./physical-examination-options-pick.component.scss']
})
export class PhysicalExaminationOptionsPickComponent implements OnInit {
  physicalExaminationOptionssData: PhysicalExaminationOptionsSummary[] = [];
  physicalExaminationOptionssTotalCount: number = 0;
  physicalExaminationOptionsSelectAll:boolean = false;
  physicalExaminationOptionsSelection: PhysicalExaminationOptionsSummary[] = [];

  physicalExaminationOptionssDisplayedColumns: string[] = ["select","option","state" , "findings"];
  physicalExaminationOptionsSearchTextBox: FormControl = new FormControl();
  selectOne: boolean;
  physicalExaminationOptionsIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) physicalExaminationOptionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) physicalExaminationOptionssSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public physicalExaminationOptionsPersist: PhysicalExaminationOptionsPersist,
                public physicalExaminationOptionsNavigator: PhysicalExaminationOptionsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PhysicalExaminationOptionsPickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, physical_option: number, status: number}

    ) {
        this.selectOne = data.selectOne;
        this.physicalExaminationOptionsPersist.physicalExaminationStateFilter = data.status;
        this.physicalExaminationOptionsPersist.physicalExaminationOptionsFilter = data.physical_option;
        this.tcAuthorization.requireRead("physical_examination_options");
       this.physicalExaminationOptionsSearchTextBox.setValue(physicalExaminationOptionsPersist.physicalExaminationOptionsSearchText);
      //delay subsequent keyup events
      this.physicalExaminationOptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.physicalExaminationOptionsPersist.physicalExaminationOptionsSearchText = value;
        this.searchPhysical_examination_optionss();
      });
    } 
    
    ngOnInit() {
   
      this.physicalExaminationOptionssSort.sortChange.subscribe(() => {
        this.physicalExaminationOptionssPaginator.pageIndex = 0;
        this.searchPhysical_examination_optionss(true);
      });

      this.physicalExaminationOptionssPaginator.page.subscribe(() => {
        this.searchPhysical_examination_optionss(true);
      });
      //start by loading items
      this.searchPhysical_examination_optionss();
    }

  searchPhysical_examination_optionss(isPagination:boolean = false): void {


    let paginator = this.physicalExaminationOptionssPaginator;
    let sorter = this.physicalExaminationOptionssSort;

    this.physicalExaminationOptionsIsLoading = true;
    this.physicalExaminationOptionsSelection = [];

    this.physicalExaminationOptionsPersist.searchPhysicalExaminationOptions(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PhysicalExaminationOptionsSummaryPartialList) => {
      this.physicalExaminationOptionssData = partialList.data;
      if (partialList.total != -1) {
        this.physicalExaminationOptionssTotalCount = partialList.total;
      }
      this.physicalExaminationOptionsIsLoading = false;
    }, error => {
      this.physicalExaminationOptionsIsLoading = false;
    });

  }

  markOneItem(item: PhysicalExaminationOptionsSummary) {
    if(!this.tcUtilsArray.containsId(this.physicalExaminationOptionsSelection,item.id)){
          this.physicalExaminationOptionsSelection = [];
          this.physicalExaminationOptionsSelection.push(item);
        }
        else{
          this.physicalExaminationOptionsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.physicalExaminationOptionsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }