import {TCId} from "../tc/models";

export class PhysicalExaminationOptionsSummary extends TCId {
    findings:string;
    state:number;
    option:number;
}

export class PhysicalExaminationOptionsSummaryPartialList {
    data: PhysicalExaminationOptionsSummary[];
    total: number;
}

export class PhysicalExaminationOptionsDetail extends PhysicalExaminationOptionsSummary {
}
    
export class PhysicalExaminationOptionsDashboard {
    total: number;
}