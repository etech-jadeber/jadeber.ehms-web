import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PhysicalExaminationOptionsEditComponent} from "./physical-examination-options-edit/physical-examination-options-edit.component";
import {PhysicalExaminationOptionsPickComponent} from "./physical-examination-options-pick/physical-examination-options-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PhysicalExaminationOptionsNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  physicalExaminationOptionssUrl(): string {
    return "/physical_examination_options";
  }

  physicalExaminationOptionsUrl(id: string): string {
    return "/physical_examination_options/" + id;
  }

  viewPhysicalExaminationOptionss(): void {
    this.router.navigateByUrl(this.physicalExaminationOptionssUrl());
  }

  viewPhysicalExaminationOptions(id: string): void {
    this.router.navigateByUrl(this.physicalExaminationOptionsUrl(id));
  }

  editPhysicalExaminationOptions(id: string): MatDialogRef<PhysicalExaminationOptionsEditComponent> {
    const dialogRef = this.dialog.open(PhysicalExaminationOptionsEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPhysicalExaminationOptions(): MatDialogRef<PhysicalExaminationOptionsEditComponent> {
    const dialogRef = this.dialog.open(PhysicalExaminationOptionsEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
  pickPhysicalExaminationOptionss(physical_option: number = null, status: number = null, selectOne: boolean=false): MatDialogRef<PhysicalExaminationOptionsPickComponent> {
      const dialogRef = this.dialog.open(PhysicalExaminationOptionsPickComponent, {
        data: {physical_option, status, selectOne},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}