import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PhysicalExaminationOptionsDashboard, PhysicalExaminationOptionsDetail, PhysicalExaminationOptionsSummaryPartialList} from "./physical_examination_options.model";
import { PhysicalExaminationOptions, PhysicalExaminationState } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class PhysicalExaminationOptionsPersist {
 physicalExaminationOptionsSearchText: string = "";

 physicalExaminationOptionsFilter: number;

    PhysicalExaminationOptions: TCEnum[] = [
  new TCEnum( PhysicalExaminationOptions.ent, this.appTranslation.getText('patient','ent')),
  new TCEnum( PhysicalExaminationOptions.eye, this.appTranslation.getText('patient','eye')),
  new TCEnum( PhysicalExaminationOptions.head_and_neck, this.appTranslation.getText('patient','head_and_neck')),
  new TCEnum( PhysicalExaminationOptions.glands, this.appTranslation.getText('patient','glands')),
  new TCEnum( PhysicalExaminationOptions.chests, this.appTranslation.getText('patient','chests')),
  new TCEnum( PhysicalExaminationOptions.cardiovascular, this.appTranslation.getText('patient','cardiovascular')),
  new TCEnum( PhysicalExaminationOptions.abdominal, this.appTranslation.getText('patient','abdominal')),
  new TCEnum( PhysicalExaminationOptions.genitourinary, this.appTranslation.getText('patient','genitourinary')),
  new TCEnum( PhysicalExaminationOptions.musculoskeletal, this.appTranslation.getText('patient','musculoskeletal')),
  new TCEnum( PhysicalExaminationOptions.integument, this.appTranslation.getText('patient','integumentary')),
  new TCEnum( PhysicalExaminationOptions.neurology, this.appTranslation.getText('patient','neurology')),

  ];

  physicalExaminationStateFilter: number;

    PhysicalExaminationState: TCEnum[] = [
     new TCEnum( PhysicalExaminationState.normal, 'Normal'),
  new TCEnum( PhysicalExaminationState.abnormal, 'Abnormal'),

  ];


 
 constructor(private http: HttpClient, public appTranslation: AppTranslation) {
 }
  
 filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.physicalExaminationOptionsSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPhysicalExaminationOptions(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PhysicalExaminationOptionsSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("physical_examination_options", this.physicalExaminationOptionsSearchText, pageSize, pageIndex, sort, order);
    if (this.physicalExaminationOptionsFilter){
        url = TCUtilsString.appendUrlParameter(url, "option", this.physicalExaminationOptionsFilter.toString())
    }
    if (this.physicalExaminationStateFilter){
        url = TCUtilsString.appendUrlParameter(url, "state", this.physicalExaminationStateFilter.toString())
    }
    return this.http.get<PhysicalExaminationOptionsSummaryPartialList>(url);
  } 
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physical_examination_options/do", new TCDoParam("download_all", this.filters()));
  }

  physicalExaminationOptionsDashboard(): Observable<PhysicalExaminationOptionsDashboard> {
    return this.http.get<PhysicalExaminationOptionsDashboard>(environment.tcApiBaseUri + "physical_examination_options/dashboard");
  }

  getPhysicalExaminationOptions(id: string): Observable<PhysicalExaminationOptionsDetail> {
    return this.http.get<PhysicalExaminationOptionsDetail>(environment.tcApiBaseUri + "physical_examination_options/" + id);
  }
  
  addPhysicalExaminationOptions(item: PhysicalExaminationOptionsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physical_examination_options/", item);
  }

  updatePhysicalExaminationOptions(item: PhysicalExaminationOptionsDetail): Observable<PhysicalExaminationOptionsDetail> {
    return this.http.patch<PhysicalExaminationOptionsDetail>(environment.tcApiBaseUri + "physical_examination_options/" + item.id, item);
  }

  deletePhysicalExaminationOptions(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "physical_examination_options/" + id);
  }
 
  physicalExaminationOptionssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physical_examination_options/do", new TCDoParam(method, payload));
  }

  physicalExaminationOptionsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physical_examination_optionss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "physical_examination_options/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "physical_examination_options/" + id + "/do", new TCDoParam("print", {}));
  }


}