import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Physiotherapy } from './physiotherapy';

@Component({
  selector: 'app-physiotherapy',
  templateUrl: './physiotherapy.component.html',
  styleUrls: ['./physiotherapy.component.scss']
})
export class PhysiotherapyComponent implements OnInit {

  constructor(
    public physiotherapyPersist: Physiotherapy,

  ) { }

  ngOnInit(): void {
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.physiotherapyPersist.selectedIndex=matTab.index;
  }
  
}
