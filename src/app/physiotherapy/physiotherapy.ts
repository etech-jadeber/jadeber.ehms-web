

import {Injectable} from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
  })
  
  export class Physiotherapy{
    selectedIndex:number=0;

    constructor(private router: Router) {
}
    physiotherapysUrl(): string {
        return "/physiotherapys";
      }
      viewPhysiotherapys(): void {
        this.router.navigateByUrl(this.physiotherapysUrl());
      }

      
  }