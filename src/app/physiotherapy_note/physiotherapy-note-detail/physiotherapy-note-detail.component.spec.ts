import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysiotherapyNoteDetailComponent } from './physiotherapy-note-detail.component';

describe('PhysiotherapyNoteDetailComponent', () => {
  let component: PhysiotherapyNoteDetailComponent;
  let fixture: ComponentFixture<PhysiotherapyNoteDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysiotherapyNoteDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyNoteDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
