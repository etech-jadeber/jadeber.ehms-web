import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";
import { PhysiotherapyNoteDetail } from '../physiotherapy_note.model';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCNotification } from 'src/app/tc/notification';
import { TCNavigator } from 'src/app/tc/navigator';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { AppTranslation } from 'src/app/app.translation';
import { PhysiotherapyNoteNavigator } from '../physiotherapy_note.navigator';
import { PhysiotherapyNotePersist } from '../physiotherapy_note.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { TCUtilsDate } from 'src/app/tc/utils-date';



@Component({
  selector: 'app-physiotherapy_note-detail',
  templateUrl: './physiotherapy-note-detail.component.html',
  styleUrls: ['./physiotherapy-note-detail.component.scss']
})
export class PhysiotherapyNoteDetailComponent implements OnInit {
  physiotherapyNoteIsLoading:boolean = false;
  size:number;
  //basics
  doctor_name:string
  physiotherapyNoteDetail: PhysiotherapyNoteDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();
  @Input() session_id:string;
  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public physiotherapyNoteNavigator: PhysiotherapyNoteNavigator,
              public  physiotherapyNotePersist: PhysiotherapyNotePersist,
              public doctorPersist: DoctorPersist
              ) {
    this.tcAuthorization.requireRead("physiotherapy_notes");
    this.physiotherapyNoteDetail = new PhysiotherapyNoteDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("physiotherapy_notes");
    this.loadDetails();
  }

  

  loadDetails(): void {
  this.physiotherapyNoteIsLoading = true;
    this.physiotherapyNotePersist.searchPhysiotherapyNote(this.session_id).subscribe(physiotherapyNotes => {
          this.physiotherapyNoteDetail = physiotherapyNotes.data[0];
          this.size=physiotherapyNotes.total;
          if(this.size){

          this.doctorPersist.getDoctor(this.physiotherapyNoteDetail.taken_by).subscribe((doctor)=>{
            if(doctor){
              this.doctor_name=doctor.first_name + " " + doctor.middle_name; 
            }
          })
          }
          this.physiotherapyNoteIsLoading = false;
        }, error => {
          console.error(error);
          this.physiotherapyNoteIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails();
  }
  addPhysiotherapyNote(): void {
    let dialogRef = this.physiotherapyNoteNavigator.addPhysiotherapyNote(this.session_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadDetails();
      }
    });
  }

  editPhysiotherapyNote(): void {
    let modalRef = this.physiotherapyNoteNavigator.editPhysiotherapyNote(this.physiotherapyNoteDetail.id);
    modalRef.afterClosed().subscribe(physiotherapyNoteDetail => {
      TCUtilsAngular.assign(this.physiotherapyNoteDetail, physiotherapyNoteDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.physiotherapyNotePersist.print(this.physiotherapyNoteDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print physiotherapy_note", true);
      });
    }

  back():void{
      this.location.back();
    }

}