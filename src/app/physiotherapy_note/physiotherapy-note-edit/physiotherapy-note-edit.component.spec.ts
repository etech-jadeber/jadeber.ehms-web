import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysiotherapyNoteEditComponent } from './physiotherapy-note-edit.component';

describe('PhysiotherapyNoteEditComponent', () => {
  let component: PhysiotherapyNoteEditComponent;
  let fixture: ComponentFixture<PhysiotherapyNoteEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysiotherapyNoteEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyNoteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
