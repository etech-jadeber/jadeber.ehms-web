import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCIdMode, TCModalModes } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PhysiotherapyNoteDetail } from '../physiotherapy_note.model'; import { PhysiotherapyNotePersist } from '../physiotherapy_note.persist'; @Component({
  selector: 'app-physiotherapy_note-edit',
  templateUrl: './physiotherapy-note-edit.component.html',
  styleUrls: ['./physiotherapy-note-edit.component.scss']
}) export class PhysiotherapyNoteEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  physiotherapyNoteDetail: PhysiotherapyNoteDetail; constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PhysiotherapyNoteEditComponent>,
    public persist: PhysiotherapyNotePersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  } ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("physiotherapy_notes");
      this.title = this.appTranslation.getText("general", "new") + " " + "physiotherapy_note";
      this.physiotherapyNoteDetail = new PhysiotherapyNoteDetail();
      this.physiotherapyNoteDetail.diagnosis_problem_list = "";
      this.physiotherapyNoteDetail.current_medications = "";
      this.physiotherapyNoteDetail.dermatomes = "";
      this.physiotherapyNoteDetail.interim_events_since_last_visit = "";
      this.physiotherapyNoteDetail.manual_muscle_test = "";
      this.physiotherapyNoteDetail.illness_understanding_by_the_patient = "";
      this.physiotherapyNoteDetail.illness_understanding_by_the_family_members = "";
      this.physiotherapyNoteDetail.neuro_screening_reflex_dir = "";
      this.physiotherapyNoteDetail.no_pain_symptoms = "";
      this.physiotherapyNoteDetail.social_issue = "";
      this.physiotherapyNoteDetail.pain_assessment_pain_type = "";
      this.physiotherapyNoteDetail.emotional_spritual_issue = "";
      this.physiotherapyNoteDetail.palliative_care_plan = "";//set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("physiotherapy_notes");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "physiotherapy_note";
      this.isLoadingResults = true;
      this.persist.getPhysiotherapyNote(this.idMode.id).subscribe(physiotherapyNoteDetail => {
        this.physiotherapyNoteDetail = physiotherapyNoteDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } onAdd(): void {
    this.isLoadingResults = true;
    this.physiotherapyNoteDetail.session_id=this.idMode.id;
    this.persist.addPhysiotherapyNote(this.physiotherapyNoteDetail).subscribe(value => {
      this.tcNotification.success("physiotherapyNote added");
      this.physiotherapyNoteDetail.id = value.id;
      this.dialogRef.close(this.physiotherapyNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updatePhysiotherapyNote(this.physiotherapyNoteDetail).subscribe(value => {
      this.tcNotification.success("physiotherapy_note updated");
      this.dialogRef.close(this.physiotherapyNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  } 
  canSubmit(): boolean {
    if (this.physiotherapyNoteDetail == null) {
      return false;
    }

// if (this.physiotherapyNoteDetail.diagnosis_problem_list == null || this.physiotherapyNoteDetail.diagnosis_problem_list  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.current_medications == null || this.physiotherapyNoteDetail.current_medications  == "") {
//       return false;
//   } 
if (this.physiotherapyNoteDetail.chief_complaint_today == null || this.physiotherapyNoteDetail.chief_complaint_today  == "") {
      return false;
  } 
// if (this.physiotherapyNoteDetail.dermatomes == null || this.physiotherapyNoteDetail.dermatomes  == "") {
//       return false;
//   } 
if (this.physiotherapyNoteDetail.brief_history_of_present_illness == null || this.physiotherapyNoteDetail.brief_history_of_present_illness  == "") {
      return false;
  } 
// if (this.physiotherapyNoteDetail.interim_events_since_last_visit == null || this.physiotherapyNoteDetail.interim_events_since_last_visit  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.manual_muscle_test == null || this.physiotherapyNoteDetail.manual_muscle_test  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.illness_understanding_by_the_patient == null || this.physiotherapyNoteDetail.illness_understanding_by_the_patient  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.illness_understanding_by_the_family_members == null || this.physiotherapyNoteDetail.illness_understanding_by_the_family_members  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.Neuro_screening_reflex_dir == null || this.physiotherapyNoteDetail.Neuro_screening_reflex_dir  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.pain_assessment_pain_severity == null) {
//       return false;
//   }
// if (this.physiotherapyNoteDetail.no_pain_symptoms == null || this.physiotherapyNoteDetail.no_pain_symptoms  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.social_issue == null || this.physiotherapyNoteDetail.social_issue  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.pain_assessment_pain_type == null || this.physiotherapyNoteDetail.pain_assessment_pain_type  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.emotional_spritual_issue == null || this.physiotherapyNoteDetail.emotional_spritual_issue  == "") {
//       return false;
//   } 
// if (this.physiotherapyNoteDetail.palliative_care_plan == null || this.physiotherapyNoteDetail.palliative_care_plan  == "") {
//       return false;
//   } 
    return true;

  }
}