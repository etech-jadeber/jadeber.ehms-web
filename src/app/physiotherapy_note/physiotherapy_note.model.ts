import {TCId} from "../tc/models";export class PhysiotherapyNoteSummary extends TCId {
    session_id:string;
    progress_note:string;
    recommendation:string;
    taken_by:string;
    date_of_progress_note:number;
    diagnosis_problem_list:string;
    current_medications:string;
    chief_complaint_today:string;
    dermatomes:string;
    brief_history_of_present_illness:string;
    interim_events_since_last_visit:string;
    manual_muscle_test:string;
    illness_understanding_by_the_patient:string;
    illness_understanding_by_the_family_members:string;
    neuro_screening_reflex_dir:string;
    pain_assessment_pain_severity:number;
    no_pain_symptoms:string;
    social_issue:string;
    pain_assessment_pain_type:string;
    emotional_spritual_issue:string;
    palliative_care_plan:string;
     
    }
    export class PhysiotherapyNoteSummaryPartialList {
      data: PhysiotherapyNoteSummary[];
      total: number;
    }
    export class PhysiotherapyNoteDetail extends PhysiotherapyNoteSummary {
    }
    
    export class PhysiotherapyNoteDashboard {
      total: number;
    }