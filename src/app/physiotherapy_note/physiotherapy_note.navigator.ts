import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PhysiotherapyNoteEditComponent} from "./physiotherapy-note-edit/physiotherapy-note-edit.component";


@Injectable({
  providedIn: 'root'
})

export class PhysiotherapyNoteNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  physiotherapyNotesUrl(): string {
    return "/physiotherapy_notes";
  }

  physiotherapyNoteUrl(id: string): string {
    return "/physiotherapy_notes/" + id;
  }

  viewPhysiotherapyNotes(): void {
    this.router.navigateByUrl(this.physiotherapyNotesUrl());
  }

  viewPhysiotherapyNote(id: string): void {
    this.router.navigateByUrl(this.physiotherapyNoteUrl(id));
  }

  editPhysiotherapyNote(id: string): MatDialogRef<PhysiotherapyNoteEditComponent> {
    const dialogRef = this.dialog.open(PhysiotherapyNoteEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPhysiotherapyNote(session_id:string): MatDialogRef<PhysiotherapyNoteEditComponent> {
    const dialogRef = this.dialog.open(PhysiotherapyNoteEditComponent, {
          data: new TCIdMode(session_id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
}