import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PhysiotherapyNoteDashboard, PhysiotherapyNoteDetail, PhysiotherapyNoteSummaryPartialList} from "./physiotherapy_note.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PhysiotherapyNotePersist {
 physiotherapyNoteSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.physiotherapyNoteSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPhysiotherapyNote(session_id:string): Observable<PhysiotherapyNoteSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("physiotherapy_note", this.physiotherapyNoteSearchText, 1, 0, "", "asc");
    url=TCUtilsString.appendUrlParameter(url,"session_id",session_id.toString());
    return this.http.get<PhysiotherapyNoteSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_note/do", new TCDoParam("download_all", this.filters()));
  }

  physiotherapyNoteDashboard(): Observable<PhysiotherapyNoteDashboard> {
    return this.http.get<PhysiotherapyNoteDashboard>(environment.tcApiBaseUri + "physiotherapy_note/dashboard");
  }

  getPhysiotherapyNote(id: string): Observable<PhysiotherapyNoteDetail> {
    return this.http.get<PhysiotherapyNoteDetail>(environment.tcApiBaseUri + "physiotherapy_note/" + id);
  } addPhysiotherapyNote(item: PhysiotherapyNoteDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_note/", item);
  }

  updatePhysiotherapyNote(item: PhysiotherapyNoteDetail): Observable<PhysiotherapyNoteDetail> {
    return this.http.patch<PhysiotherapyNoteDetail>(environment.tcApiBaseUri + "physiotherapy_note/" + item.id, item);
  }

  deletePhysiotherapyNote(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "physiotherapy_note/" + id);
  }
 physiotherapyNotesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_note/do", new TCDoParam(method, payload));
  }

  physiotherapyNoteDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_notes/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_note/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_note/" + id + "/do", new TCDoParam("print", {}));
  }


}