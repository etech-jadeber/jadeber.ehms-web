import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysiotherapySessionDetailComponent } from './physiotherapy-session-detail.component';

describe('PhysiotherapySessionDetailComponent', () => {
  let component: PhysiotherapySessionDetailComponent;
  let fixture: ComponentFixture<PhysiotherapySessionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysiotherapySessionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapySessionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
