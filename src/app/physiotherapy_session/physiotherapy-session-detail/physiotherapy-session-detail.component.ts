import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PhysiotherapySessionDetail} from "../physiotherapy_session.model";
import {PhysiotherapySessionPersist} from "../physiotherapy_session.persist";
import {PhysiotherapySessionNavigator} from "../physiotherapy_session.navigator";

export enum PhysiotherapySessionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-physiotherapy_session-detail',
  templateUrl: './physiotherapy-session-detail.component.html',
  styleUrls: ['./physiotherapy-session-detail.component.scss']
})
export class PhysiotherapySessionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  physiotherapySessionIsLoading:boolean = false;
  
  PhysiotherapySessionTabs: typeof PhysiotherapySessionTabs = PhysiotherapySessionTabs;
  activeTab: PhysiotherapySessionTabs = PhysiotherapySessionTabs.overview;
  //basics
  physiotherapySessionDetail: PhysiotherapySessionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public physiotherapySessionNavigator: PhysiotherapySessionNavigator,
              public  physiotherapySessionPersist: PhysiotherapySessionPersist) {
    this.tcAuthorization.requireRead("physiotherapy_sessions");
    this.physiotherapySessionDetail = new PhysiotherapySessionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("physiotherapy_sessions");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.physiotherapySessionIsLoading = true;
    this.physiotherapySessionPersist.getPhysiotherapySession(id).subscribe(physiotherapySessionDetail => {
          this.physiotherapySessionDetail = physiotherapySessionDetail;
          this.physiotherapySessionIsLoading = false;
        }, error => {
          console.error(error);
          this.physiotherapySessionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.physiotherapySessionDetail.id);
  }
  // editPhysiotherapySession(): void {
  //   let modalRef = this.physiotherapySessionNavigator.editPhysiotherapySession(this.physiotherapySessionDetail.id);
  //   modalRef.afterClosed().subscribe(physiotherapySessionDetail => {
  //     TCUtilsAngular.assign(this.physiotherapySessionDetail, physiotherapySessionDetail);
  //   }, error => {
  //     console.error(error);
  //   });
  // }

   printBirth():void{
      this.physiotherapySessionPersist.print(this.physiotherapySessionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print physiotherapy_session", true);
      });
    }

  back():void{
      this.location.back();
    }

}