import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PhysiotherapySessionListComponent } from './physiotherapy-session-list.component';

describe('PhysiotherapySessionListComponent', () => {
  let component: PhysiotherapySessionListComponent;
  let fixture: ComponentFixture<PhysiotherapySessionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhysiotherapySessionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapySessionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
