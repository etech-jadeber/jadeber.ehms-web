import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PhysiotherapySessionDetail, PhysiotherapySessionSummary, PhysiotherapySessionSummaryPartialList } from '../physiotherapy_session.model';
import { PhysiotherapySessionPersist } from '../physiotherapy_session.persist';
import { PhysiotherapySessionNavigator } from '../physiotherapy_session.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { payment_status } from 'src/app/app.enums';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { Physiotherapy_TypeSummary } from 'src/app/physiotherapy_types/physiotherapy_type.model';
import { Physiotherapy_TypePersist } from 'src/app/physiotherapy_types/physiotherapy_type.persist';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-physiotherapy_session-list',
  templateUrl: './physiotherapy-session-list.component.html',
  styleUrls: ['./physiotherapy-session-list.component.scss']
})export class PhysiotherapySessionListComponent implements OnInit {
  physiotherapySessionsData: PhysiotherapySessionSummary[] = [];
  physiotherapySessionsTotalCount: number = 0;
  physiotherapySessionSelectAll:boolean = false;
  physiotherapySessionSelection: PhysiotherapySessionSummary[] = [];
  physiotherapy_typesData: Physiotherapy_TypeSummary[] = [];
  invalid_id=TCUtilsString.getInvalidId();

 physiotherapySessionsDisplayedColumns: string[] = ["select","action","patient_name","physiotherapy_type","card_id","physiotherapy_id","session_id","date_of_session" ];
  physiotherapySessionSearchTextBox: FormControl = new FormControl();
  physiotherapySessionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) physiotherapySessionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) physiotherapySessionsSort: MatSort;
  @Input() encounter_id=TCUtilsString.getInvalidId();
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public physiotherapySessionPersist: PhysiotherapySessionPersist,
                public physiotherapySessionNavigator: PhysiotherapySessionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public paymentPersist: PaymentPersist,
                public physiotherapy_TypePersist: Physiotherapy_TypePersist,

    ) {

        this.tcAuthorization.requireRead("physiotherapy_sessions");
       this.physiotherapySessionSearchTextBox.setValue(physiotherapySessionPersist.physiotherapySessionSearchText);
      //delay subsequent keyup events
      this.physiotherapySessionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.physiotherapySessionPersist.physiotherapySessionSearchText = value;
        this.searchPhysiotherapySessions();
      });
    } ngOnInit() {
   
      this.physiotherapySessionsSort.sortChange.subscribe(() => {
        this.physiotherapySessionsPaginator.pageIndex = 0;
        this.searchPhysiotherapySessions(true);
      });

      this.physiotherapySessionsPaginator.page.subscribe(() => {
        this.searchPhysiotherapySessions(true);
      });
      //start by loading items
      this.searchPhysiotherapySessions();
    }

  searchPhysiotherapySessions(isPagination:boolean = false): void {


    let paginator = this.physiotherapySessionsPaginator;
    let sorter = this.physiotherapySessionsSort;

    this.physiotherapySessionIsLoading = true;
    this.physiotherapySessionSelection = [];
    this.physiotherapySessionPersist.encounterId=this.encounter_id;
    this.physiotherapySessionPersist.searchPhysiotherapySession(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PhysiotherapySessionSummaryPartialList) => {
      this.physiotherapySessionsData = partialList.data;
      if (partialList.total != -1) {
        this.physiotherapySessionsTotalCount = partialList.total;
        for(let physiotherapy_session of this.physiotherapySessionsData){
          this.physiotherapy_TypePersist.getPhysiotherapy_Type(physiotherapy_session.physiotherapy_type).subscribe((response)=>{
            this.physiotherapy_typesData.push(response)
          })
        }
        
      }
      this.physiotherapySessionIsLoading = false;
    }, error => {
      this.physiotherapySessionIsLoading = false;
    });

  } downloadPhysiotherapySessions(): void {
    if(this.physiotherapySessionSelectAll){
         this.physiotherapySessionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download physiotherapy_session", true);
      });
    }
    else{
        this.physiotherapySessionPersist.download(this.tcUtilsArray.idsList(this.physiotherapySessionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download physiotherapy_session",true);
            });
        }
  }
  loadPhysiotherapySessionDetail(session:PhysiotherapySessionDetail): void {
    this.paymentPersist.getPayment(session.payment_id).subscribe((result)=>{
      if(result){
        if(result.payment_status==payment_status.not_paid){
           this.tcNotification.error("physiotherapy session fee is not paid");
        }
        else{
          this.router.navigate([this.physiotherapySessionNavigator.physiotherapySessionUrl(session.id)]);
        }
      }
    })
  }
  deletePhysiotherapySession(item: PhysiotherapySessionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("physiotherapy_sessions");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.physiotherapySessionPersist.deletePhysiotherapySession(item.id).subscribe(response => {
          this.tcNotification.success("physiotherapy_session deleted");
          this.searchPhysiotherapySessions();
        }, error => {
        });
      }

    });
  }  
  ngOnDestroy():void{
    this.physiotherapySessionPersist.encounterId=TCUtilsString.getInvalidId();
  }
  back():void{
      this.location.back();
    }
}