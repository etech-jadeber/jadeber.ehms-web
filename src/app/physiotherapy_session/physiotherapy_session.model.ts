import {TCId} from "../tc/models";
export class PhysiotherapySessionSummary extends TCId {
    physiotherapy_id:number;
    session_id:number;
    card_id:number;
    encounter_id:string;
    patient_name:string;
    date_of_session:number;
    no_of_session_per_week:number;
    payment_id:string;
    physiotherapy_type:string;
     
    }
    export class PhysiotherapySessionSummaryPartialList {
      data: PhysiotherapySessionSummary[];
      total: number;
    }
    export class PhysiotherapySessionDetail extends PhysiotherapySessionSummary {
    }
    
    export class PhysiotherapySessionDashboard {
      total: number;
    }