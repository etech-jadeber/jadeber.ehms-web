import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';




@Injectable({
  providedIn: 'root'
})

export class PhysiotherapySessionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  physiotherapySessionsUrl(): string {
    return "/physiotherapy_sessions";
  }

  physiotherapySessionUrl(id: string): string {
    return "/physiotherapy_sessions/" + id;
  }

  viewPhysiotherapySessions(): void {
    this.router.navigateByUrl(this.physiotherapySessionsUrl());
  }

  viewPhysiotherapySession(id: string): void {
    this.router.navigateByUrl(this.physiotherapySessionUrl(id));
  }

   
}