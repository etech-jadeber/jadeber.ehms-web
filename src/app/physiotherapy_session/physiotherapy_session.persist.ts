import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PhysiotherapySessionDashboard, PhysiotherapySessionDetail, PhysiotherapySessionSummaryPartialList} from "./physiotherapy_session.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PhysiotherapySessionPersist {
 physiotherapySessionSearchText: string = ""; 
 encounterId:string=TCUtilsString.getInvalidId();

 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.physiotherapySessionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPhysiotherapySession(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PhysiotherapySessionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("physiotherapy_session", this.physiotherapySessionSearchText, pageSize, pageIndex, sort, order);
    url=TCUtilsString.appendUrlParameter(url,"encounter_id",this.encounterId);
    return this.http.get<PhysiotherapySessionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_session/do", new TCDoParam("download_all", this.filters()));
  }

  physiotherapySessionDashboard(): Observable<PhysiotherapySessionDashboard> {
    return this.http.get<PhysiotherapySessionDashboard>(environment.tcApiBaseUri + "physiotherapy_session/dashboard");
  }

  getPhysiotherapySession(id: string): Observable<PhysiotherapySessionDetail> {
    return this.http.get<PhysiotherapySessionDetail>(environment.tcApiBaseUri + "physiotherapy_session/" + id);
  } addPhysiotherapySession(item: PhysiotherapySessionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_session/", item);
  }

  updatePhysiotherapySession(item: PhysiotherapySessionDetail): Observable<PhysiotherapySessionDetail> {
    return this.http.patch<PhysiotherapySessionDetail>(environment.tcApiBaseUri + "physiotherapy_session/" + item.id, item);
  }

  deletePhysiotherapySession(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "physiotherapy_session/" + id);
  }
 physiotherapySessionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_session/do", new TCDoParam(method, payload));
  }

  physiotherapySessionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_sessions/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_session/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_session/" + id + "/do", new TCDoParam("print", {}));
  }


}