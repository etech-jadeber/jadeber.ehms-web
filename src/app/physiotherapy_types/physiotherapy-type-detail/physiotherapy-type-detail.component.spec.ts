import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PhysiotherapyTypeDetailComponent } from './physiotherapy-type-detail.component';

describe('PhysiotherapyTypeDetailComponent', () => {
  let component: PhysiotherapyTypeDetailComponent;
  let fixture: ComponentFixture<PhysiotherapyTypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysiotherapyTypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
