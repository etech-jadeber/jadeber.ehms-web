import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Physiotherapy_TypeDetail} from "../physiotherapy_type.model";
import {Physiotherapy_TypePersist} from "../physiotherapy_type.persist";
import {Physiotherapy_TypeNavigator} from "../physiotherapy_type.navigator";

export enum Physiotherapy_TypeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-physiotherapy-type-detail',
  templateUrl: './physiotherapy-type-detail.component.html',
  styleUrls: ['./physiotherapy-type-detail.component.css']
})
export class Physiotherapy_TypeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  physiotherapy_typeLoading:boolean = false;
  
  Physiotherapy_TypeTabs: typeof Physiotherapy_TypeTabs = Physiotherapy_TypeTabs;
  activeTab: Physiotherapy_TypeTabs = Physiotherapy_TypeTabs.overview;
  //basics
  physiotherapy_typeDetail: Physiotherapy_TypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public physiotherapy_typeNavigator: Physiotherapy_TypeNavigator,
              public  physiotherapy_typePersist: Physiotherapy_TypePersist) {
    this.tcAuthorization.requireRead("physiotherapy_types");
    this.physiotherapy_typeDetail = new Physiotherapy_TypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("physiotherapy_types");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.physiotherapy_typeLoading = true;
    this.physiotherapy_typePersist.getPhysiotherapy_Type(id).subscribe(physiotherapy_typeDetail => {
          this.physiotherapy_typeDetail = physiotherapy_typeDetail;
          this.physiotherapy_typeLoading = false;
        }, error => {
          console.error(error);
          this.physiotherapy_typeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.physiotherapy_typeDetail.id);
  }

  editPhysiotherapy_Type(): void {
    let modalRef = this.physiotherapy_typeNavigator.editPhysiotherapy_Type(this.physiotherapy_typeDetail.id);
    modalRef.afterClosed().subscribe(modifiedPhysiotherapy_TypeDetail => {
      TCUtilsAngular.assign(this.physiotherapy_typeDetail, modifiedPhysiotherapy_TypeDetail);
    }, error => {
      console.error(error);
    });
  }

   printPhysiotherapy_Type():void{
      this.physiotherapy_typePersist.print(this.physiotherapy_typeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print physiotherapy_type", true);
      });
    }

  back():void{
      this.location.back();
    }

}
