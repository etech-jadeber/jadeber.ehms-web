import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PhysiotherapyTypeEditComponent } from './physiotherapy-type-edit.component';

describe('PhysiotherapyTypeEditComponent', () => {
  let component: PhysiotherapyTypeEditComponent;
  let fixture: ComponentFixture<PhysiotherapyTypeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysiotherapyTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
