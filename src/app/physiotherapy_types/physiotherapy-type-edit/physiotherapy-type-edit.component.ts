import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Physiotherapy_TypeDetail} from "../physiotherapy_type.model";
import {Physiotherapy_TypePersist} from "../physiotherapy_type.persist";


@Component({
  selector: 'app-physiotherapy-type-edit',
  templateUrl: './physiotherapy-type-edit.component.html',
  styleUrls: ['./physiotherapy-type-edit.component.css']
})
export class Physiotherapy_TypeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  physiotherapy_typeDetail: Physiotherapy_TypeDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Physiotherapy_TypeEditComponent>,
              public  persist: Physiotherapy_TypePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("physiotherapy_types");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "physiotherapy_type");
      this.physiotherapy_typeDetail = new Physiotherapy_TypeDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("physiotherapy_types");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "physiotherapy_type");
      this.isLoadingResults = true;
      this.persist.getPhysiotherapy_Type(this.idMode.id).subscribe(physiotherapy_typeDetail => {
        this.physiotherapy_typeDetail = physiotherapy_typeDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addPhysiotherapy_Type(this.physiotherapy_typeDetail).subscribe(value => {
      this.tcNotification.success("Physiotherapy_Type added");
      this.physiotherapy_typeDetail.id = value.id;
      this.dialogRef.close(this.physiotherapy_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updatePhysiotherapy_Type(this.physiotherapy_typeDetail).subscribe(value => {
      this.tcNotification.success("Physiotherapy_Type updated");
      this.dialogRef.close(this.physiotherapy_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.physiotherapy_typeDetail == null){
            return false;
          }

        if (this.physiotherapy_typeDetail.name == null || this.physiotherapy_typeDetail.name  == "") {
                      return false;
                    }


        return true;
      }


}
