import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PhysiotherapyTypeListComponent } from './physiotherapy-type-list.component';

describe('PhysiotherapyTypeListComponent', () => {
  let component: PhysiotherapyTypeListComponent;
  let fixture: ComponentFixture<PhysiotherapyTypeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysiotherapyTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
