import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Physiotherapy_TypePersist} from "../physiotherapy_type.persist";
import {Physiotherapy_TypeNavigator} from "../physiotherapy_type.navigator";
import {Physiotherapy_TypeDetail, Physiotherapy_TypeSummary, Physiotherapy_TypeSummaryPartialList} from "../physiotherapy_type.model";


@Component({
  selector: 'app-physiotherapy-type-list',
  templateUrl: './physiotherapy-type-list.component.html',
  styleUrls: ['./physiotherapy-type-list.component.css']
})
export class Physiotherapy_TypeListComponent implements OnInit {

  physiotherapy_typesData: Physiotherapy_TypeSummary[] = [];
  physiotherapy_typesTotalCount: number = 0;
  physiotherapy_typesSelectAll:boolean = false;
  physiotherapy_typesSelection: Physiotherapy_TypeSummary[] = [];

  physiotherapy_typesDisplayedColumns: string[] = ["select","action", "name","created_at","updated_at", ];
  physiotherapy_typesSearchTextBox: FormControl = new FormControl();
  physiotherapy_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) physiotherapy_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) physiotherapy_typesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public physiotherapy_typePersist: Physiotherapy_TypePersist,
                public physiotherapy_typeNavigator: Physiotherapy_TypeNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("physiotherapy_types");
       this.physiotherapy_typesSearchTextBox.setValue(physiotherapy_typePersist.physiotherapy_typeSearchText);
      //delay subsequent keyup events
      this.physiotherapy_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.physiotherapy_typePersist.physiotherapy_typeSearchText = value;
        this.searchPhysiotherapy_Types();
      });
    }

    ngOnInit() {

      this.physiotherapy_typesSort.sortChange.subscribe(() => {
        this.physiotherapy_typesPaginator.pageIndex = 0;
        this.searchPhysiotherapy_Types(true);
      });

      this.physiotherapy_typesPaginator.page.subscribe(() => {
        this.searchPhysiotherapy_Types(true);
      });
      //start by loading items
      this.searchPhysiotherapy_Types();
    }

  searchPhysiotherapy_Types(isPagination:boolean = false): void {


    let paginator = this.physiotherapy_typesPaginator;
    let sorter = this.physiotherapy_typesSort;

    this.physiotherapy_typesIsLoading = true;
    this.physiotherapy_typesSelection = [];

    this.physiotherapy_typePersist.searchPhysiotherapy_Type(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Physiotherapy_TypeSummaryPartialList) => {
      this.physiotherapy_typesData = partialList.data;
      if (partialList.total != -1) {
        this.physiotherapy_typesTotalCount = partialList.total;
      }
      this.physiotherapy_typesIsLoading = false;
    }, error => {
      this.physiotherapy_typesIsLoading = false;
    });

  }

  downloadPhysiotherapy_Types(): void {
    if(this.physiotherapy_typesSelectAll){
         this.physiotherapy_typePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download physiotherapy_types", true);
      });
    }
    else{
        this.physiotherapy_typePersist.download(this.tcUtilsArray.idsList(this.physiotherapy_typesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download physiotherapy_types",true);
            });
        }
  }



  addPhysiotherapy_Type(): void {
    let dialogRef = this.physiotherapy_typeNavigator.addPhysiotherapy_Type();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPhysiotherapy_Types();
      }
    });
  }

  editPhysiotherapy_Type(item: Physiotherapy_TypeSummary) {
    let dialogRef = this.physiotherapy_typeNavigator.editPhysiotherapy_Type(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePhysiotherapy_Type(item: Physiotherapy_TypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Physiotherapy_Type");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.physiotherapy_typePersist.deletePhysiotherapy_Type(item.id).subscribe(response => {
          this.tcNotification.success("Physiotherapy_Type deleted");
          this.searchPhysiotherapy_Types();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
