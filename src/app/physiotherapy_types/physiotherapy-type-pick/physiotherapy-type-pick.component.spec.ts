import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PhysiotherapyTypePickComponent } from './physiotherapy-type-pick.component';

describe('PhysiotherapyTypePickComponent', () => {
  let component: PhysiotherapyTypePickComponent;
  let fixture: ComponentFixture<PhysiotherapyTypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PhysiotherapyTypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhysiotherapyTypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
