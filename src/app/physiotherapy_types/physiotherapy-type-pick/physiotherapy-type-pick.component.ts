import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Physiotherapy_TypeDetail, Physiotherapy_TypeSummary, Physiotherapy_TypeSummaryPartialList} from "../physiotherapy_type.model";
import {Physiotherapy_TypePersist} from "../physiotherapy_type.persist";


@Component({
  selector: 'app-physiotherapy-type-pick',
  templateUrl: './physiotherapy-type-pick.component.html',
  styleUrls: ['./physiotherapy-type-pick.component.css']
})
export class Physiotherapy_TypePickComponent implements OnInit {

  physiotherapy_typesData: Physiotherapy_TypeSummary[] = [];
  physiotherapy_typesTotalCount: number = 0;
  physiotherapy_typesSelection: Physiotherapy_TypeSummary[] = [];
  physiotherapy_typesDisplayedColumns: string[] = ["select", 'name','created_at','updated_at' ];

  physiotherapy_typesSearchTextBox: FormControl = new FormControl();
  physiotherapy_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) physiotherapy_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) physiotherapy_typesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public physiotherapy_typePersist: Physiotherapy_TypePersist,
              public dialogRef: MatDialogRef<Physiotherapy_TypePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("physiotherapy_types");
    this.physiotherapy_typesSearchTextBox.setValue(physiotherapy_typePersist.physiotherapy_typeSearchText);
    //delay subsequent keyup events
    this.physiotherapy_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.physiotherapy_typePersist.physiotherapy_typeSearchText = value;
      this.searchPhysiotherapy_Types();
    });
  }

  ngOnInit() {

    this.physiotherapy_typesSort.sortChange.subscribe(() => {
      this.physiotherapy_typesPaginator.pageIndex = 0;
      this.searchPhysiotherapy_Types();
    });

    this.physiotherapy_typesPaginator.page.subscribe(() => {
      this.searchPhysiotherapy_Types();
    });

    //set initial picker list to 5
    this.physiotherapy_typesPaginator.pageSize = 5;

    //start by loading items
    this.searchPhysiotherapy_Types();
  }

  searchPhysiotherapy_Types(): void {
    this.physiotherapy_typesIsLoading = true;
    this.physiotherapy_typesSelection = [];

    this.physiotherapy_typePersist.searchPhysiotherapy_Type(this.physiotherapy_typesPaginator.pageSize,
        this.physiotherapy_typesPaginator.pageIndex,
        this.physiotherapy_typesSort.active,
        this.physiotherapy_typesSort.direction).subscribe((partialList: Physiotherapy_TypeSummaryPartialList) => {
      this.physiotherapy_typesData = partialList.data;
      if (partialList.total != -1) {
        this.physiotherapy_typesTotalCount = partialList.total;
      }
      this.physiotherapy_typesIsLoading = false;
    }, error => {
      this.physiotherapy_typesIsLoading = false;
    });

  }

  markOneItem(item: Physiotherapy_TypeSummary) {
    if(!this.tcUtilsArray.containsId(this.physiotherapy_typesSelection,item.id)){
          this.physiotherapy_typesSelection = [];
          this.physiotherapy_typesSelection.push(item);
        }
        else{
          this.physiotherapy_typesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.physiotherapy_typesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
