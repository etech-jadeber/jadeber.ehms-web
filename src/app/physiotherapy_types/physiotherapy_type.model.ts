import {TCId} from "../tc/models";

export class Physiotherapy_TypeSummary extends TCId {
  name : string;
created_at : number;
updated_at : number;
}

export class Physiotherapy_TypeSummaryPartialList {
  data: Physiotherapy_TypeSummary[];
  total: number;
}

export class Physiotherapy_TypeDetail extends Physiotherapy_TypeSummary {

}

export class Physiotherapy_TypeDashboard {
  total: number;
}
