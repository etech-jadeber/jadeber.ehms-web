import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Physiotherapy_TypeEditComponent} from "./physiotherapy-type-edit/physiotherapy-type-edit.component";
import {Physiotherapy_TypePickComponent} from "./physiotherapy-type-pick/physiotherapy-type-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Physiotherapy_TypeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  physiotherapy_typesUrl(): string {
    return "/physiotherapy_types";
  }

  physiotherapy_typeUrl(id: string): string {
    return "/physiotherapy_types/" + id;
  }

  viewPhysiotherapy_Types(): void {
    this.router.navigateByUrl(this.physiotherapy_typesUrl());
  }

  viewPhysiotherapy_Type(id: string): void {
    this.router.navigateByUrl(this.physiotherapy_typeUrl(id));
  }

  editPhysiotherapy_Type(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Physiotherapy_TypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPhysiotherapy_Type(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Physiotherapy_TypeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPhysiotherapy_Types(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Physiotherapy_TypePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
