import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Physiotherapy_TypeDashboard, Physiotherapy_TypeDetail, Physiotherapy_TypeSummaryPartialList} from "./physiotherapy_type.model";


@Injectable({
  providedIn: 'root'
})
export class Physiotherapy_TypePersist {

  physiotherapy_typeSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchPhysiotherapy_Type(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Physiotherapy_TypeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("physiotherapy_types", this.physiotherapy_typeSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Physiotherapy_TypeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.physiotherapy_typeSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_types/do", new TCDoParam("download_all", this.filters()));
  }

  physiotherapy_typeDashboard(): Observable<Physiotherapy_TypeDashboard> {
    return this.http.get<Physiotherapy_TypeDashboard>(environment.tcApiBaseUri + "physiotherapy_types/dashboard");
  }

  getPhysiotherapy_Type(id: string): Observable<Physiotherapy_TypeDetail> {
    return this.http.get<Physiotherapy_TypeDetail>(environment.tcApiBaseUri + "physiotherapy_types/" + id);
  }

  addPhysiotherapy_Type(item: Physiotherapy_TypeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_types/", item);
  }

  updatePhysiotherapy_Type(item: Physiotherapy_TypeDetail): Observable<Physiotherapy_TypeDetail> {
    return this.http.patch<Physiotherapy_TypeDetail>(environment.tcApiBaseUri + "physiotherapy_types/" + item.id, item);
  }

  deletePhysiotherapy_Type(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "physiotherapy_types/" + id);
  }

  physiotherapy_typesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_types/do", new TCDoParam(method, payload));
  }

  physiotherapy_typeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "physiotherapy_types/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_types/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "physiotherapy_types/" + id + "/do", new TCDoParam("print", {}));
  }


}
