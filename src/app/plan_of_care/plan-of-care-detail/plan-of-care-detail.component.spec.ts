import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOfCareDetailComponent } from './plan-of-care-detail.component';

describe('PlanOfCareDetailComponent', () => {
  let component: PlanOfCareDetailComponent;
  let fixture: ComponentFixture<PlanOfCareDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanOfCareDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOfCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
