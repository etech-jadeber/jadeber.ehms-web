import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import { PlanOfCareDetail } from '../plan_of_care.model';
import { PlanOfCarePersist } from '../plan_of_care.persist';
import { PlanOfCareNavigator } from '../plan_of_care.navigator';

export enum PlanOfCareTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-plan-of-care-detail',
  templateUrl: './plan-of-care-detail.component.html',
  styleUrls: ['./plan-of-care-detail.component.scss']
})
export class PlanOfCareDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  planOfCareIsLoading:boolean = false;
  
  PlanOfCareTabs: typeof PlanOfCareTabs = PlanOfCareTabs;
  activeTab: PlanOfCareTabs = PlanOfCareTabs.overview;
  //basics
  planOfCareDetail: PlanOfCareDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public planOfCareNavigator: PlanOfCareNavigator,
              public  planOfCarePersist: PlanOfCarePersist) {
    this.tcAuthorization.requireRead("plan_of_cares");
    this.planOfCareDetail = new PlanOfCareDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("plan_of_cares");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.planOfCareIsLoading = true;
    this.planOfCarePersist.getPlanOfCare(id).subscribe(planOfCareDetail => {
          this.planOfCareDetail = planOfCareDetail;
          this.planOfCareIsLoading = false;
        }, error => {
          console.error(error);
          this.planOfCareIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.planOfCareDetail.id);
  }
  editPlanOfCare(): void {
    let modalRef = this.planOfCareNavigator.editPlanOfCare(this.planOfCareDetail.id);
    modalRef.afterClosed().subscribe(planOfCareDetail => {
      TCUtilsAngular.assign(this.planOfCareDetail, planOfCareDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.planOfCarePersist.print(this.planOfCareDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print plan_of_care", true);
      });
    }

  back():void{
      this.location.back();
    }

}