import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOfCareEditComponent } from './plan-of-care-edit.component';

describe('PlanOfCareEditComponent', () => {
  let component: PlanOfCareEditComponent;
  let fixture: ComponentFixture<PlanOfCareEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanOfCareEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOfCareEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
