import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PlanOfCareDetail } from '../plan_of_care.model';
import { PlanOfCarePersist } from '../plan_of_care.persist';
@Component({
  selector: 'app-plan-of-care-edit',
  templateUrl: './plan-of-care-edit.component.html',
  styleUrls: ['./plan-of-care-edit.component.scss']
})

export class PlanOfCareEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  alert_date: string;
  planOfCareDetail: PlanOfCareDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PlanOfCareEditComponent>,
              public  persist: PlanOfCarePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("plan_of_cares");
      this.title = this.appTranslation.getText("general","new") +  " " + "plan_of_care";
      this.planOfCareDetail = new PlanOfCareDetail();
      this.planOfCareDetail.required = false;
      this.planOfCareDetail.need_alert = false;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("plan_of_cares");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "plan_of_care";
      this.isLoadingResults = true;
      this.persist.getPlanOfCare(this.idMode.id).subscribe(planOfCareDetail => {
        this.planOfCareDetail = planOfCareDetail;
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  resetAlertDate():void{
    this.alert_date = undefined;
  }
  
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist.addPlanOfCare(this.planOfCareDetail).subscribe(value => {
      this.tcNotification.success("planOfCare added");
      this.planOfCareDetail.id = value.id;
      this.dialogRef.close(this.planOfCareDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.planOfCareDetail.alert_date = new Date(this.alert_date).getTime() / 1000;
    } else {
      this.alert_date=this.getStartTime(this.tcUtilsDate.toDate(this.planOfCareDetail.alert_date));
    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates()
    this.persist.updatePlanOfCare(this.planOfCareDetail).subscribe(value => {
      this.tcNotification.success("plan_of_care updated");
      this.dialogRef.close(this.planOfCareDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  canSubmit():boolean{
        if (this.planOfCareDetail == null){
            return false;
          }
        if(this.planOfCareDetail.name == null || this.planOfCareDetail.name == ""){
          return false;
        }
        if(this.planOfCareDetail.required == null){
          return false;
        }
        if(this.planOfCareDetail.need_alert == null){
          return false;
        }
        if(this.planOfCareDetail.need_alert){
          if(this.alert_date == null){
            return false;
          }
        }

        return true;

 }


 getStartTime(date: Date) {
  let now = date;
  let month = ('0' + (now.getMonth() + 1)).slice(-2);
  let day = ('0' + now.getDate()).slice(-2);
  let min = ('0' + now.getMinutes()).slice(-2);
  let hour = ('0' + now.getHours()).slice(-2);
  return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }


 }