import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOfCareListComponent } from './plan-of-care-list.component';

describe('PlanOfCareListComponent', () => {
  let component: PlanOfCareListComponent;
  let fixture: ComponentFixture<PlanOfCareListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanOfCareListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOfCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
