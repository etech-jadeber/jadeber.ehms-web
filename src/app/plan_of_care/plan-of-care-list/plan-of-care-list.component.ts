import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PlanOfCareSummary, PlanOfCareSummaryPartialList } from '../plan_of_care.model';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { PlanOfCareNavigator } from '../plan_of_care.navigator';
import { PlanOfCarePersist } from '../plan_of_care.persist';
@Component({
  selector: 'app-plan-of-care-list',
  templateUrl: './plan-of-care-list.component.html',
  styleUrls: ['./plan-of-care-list.component.scss']
})export class PlanOfCareListComponent implements OnInit {
  planOfCaresData: PlanOfCareSummary[] = [];
  planOfCaresTotalCount: number = 0;
  planOfCareSelectAll:boolean = false;
  planOfCareSelection: PlanOfCareSummary[] = [];

 planOfCaresDisplayedColumns: string[] = ["select","action" ,"name", "required", "need_alert","alert_date" ];
  planOfCareSearchTextBox: FormControl = new FormControl();
  planOfCareIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) planOfCaresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) planOfCaresSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public planOfCarePersist: PlanOfCarePersist,
                public planOfCareNavigator: PlanOfCareNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("plan_of_cares");
       this.planOfCareSearchTextBox.setValue(planOfCarePersist.planOfCareSearchText);
      //delay subsequent keyup events
      this.planOfCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.planOfCarePersist.planOfCareSearchText = value;
        this.searchPlan_of_cares();
      });
    } ngOnInit() {
   
      this.planOfCaresSort.sortChange.subscribe(() => {
        this.planOfCaresPaginator.pageIndex = 0;
        this.searchPlan_of_cares(true);
      });

      this.planOfCaresPaginator.page.subscribe(() => {
        this.searchPlan_of_cares(true);
      });
      //start by loading items
      this.searchPlan_of_cares();
    }

  searchPlan_of_cares(isPagination:boolean = false): void {


    let paginator = this.planOfCaresPaginator;
    let sorter = this.planOfCaresSort;

    this.planOfCareIsLoading = true;
    this.planOfCareSelection = [];

    this.planOfCarePersist.searchPlanOfCare(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PlanOfCareSummaryPartialList) => {
      this.planOfCaresData = partialList.data;
      if (partialList.total != -1) {
        this.planOfCaresTotalCount = partialList.total;
      }
      this.planOfCareIsLoading = false;
    }, error => {
      this.planOfCareIsLoading = false;
    });

  } downloadPlanOfCares(): void {
    if(this.planOfCareSelectAll){
         this.planOfCarePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download plan_of_care", true);
      });
    }
    else{
        this.planOfCarePersist.download(this.tcUtilsArray.idsList(this.planOfCareSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download plan_of_care",true);
            });
        }
  }
addPlan_of_care(): void {
    let dialogRef = this.planOfCareNavigator.addPlanOfCare();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPlan_of_cares();
      }
    });
  }

  editPlanOfCare(item: PlanOfCareSummary) {
    let dialogRef = this.planOfCareNavigator.editPlanOfCare(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePlanOfCare(item: PlanOfCareSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("plan_of_care");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.planOfCarePersist.deletePlanOfCare(item.id).subscribe(response => {
          this.tcNotification.success("plan_of_care deleted");
          this.searchPlan_of_cares();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}