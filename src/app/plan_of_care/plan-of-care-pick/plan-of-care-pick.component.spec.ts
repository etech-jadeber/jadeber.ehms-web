import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanOfCarePickComponent } from './plan-of-care-pick.component';

describe('PlanOfCarePickComponent', () => {
  let component: PlanOfCarePickComponent;
  let fixture: ComponentFixture<PlanOfCarePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanOfCarePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanOfCarePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
