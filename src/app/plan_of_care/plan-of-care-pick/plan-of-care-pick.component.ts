import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime, elementAt} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlanOfCareSummary, PlanOfCareSummaryPartialList } from '../plan_of_care.model';
import { PlanOfCarePersist } from '../plan_of_care.persist';
import { PlanOfCareNavigator } from '../plan_of_care.navigator';
import { NursePlanOfCarePersist } from 'src/app/nurse_plan_of_care/nurse_plan_of_care.persist';
import { NurseHistoryDetail } from 'src/app/nurse_history/nurse_history.model';
import { NursePlanOfCareDetail } from 'src/app/nurse_plan_of_care/nurse_plan_of_care.model';
@Component({
  selector: 'app-plan-of-care-pick',
  templateUrl: './plan-of-care-pick.component.html',
  styleUrls: ['./plan-of-care-pick.component.scss']
})export class PlanOfCarePickComponent implements OnInit {
  planOfCaresData: PlanOfCareSummary[] = [];
  planOfCaresTotalCount: number = 0;
  planOfCareSelectAll:boolean = false;
  planOfCareSelection: PlanOfCareSummary[] = [];

  planOfCaresDisplayedColumns: string[] = ["select","action" ,"name", "required", "need_alert","alert_date" ];
  planOfCareSearchTextBox: FormControl = new FormControl();
  planOfCareIsLoading: boolean = false;  
  alreadyAddedList: NursePlanOfCareDetail[] = []
  // @ViewChild(MatPaginator, {static: true}) planOfCaresPaginator: MatPaginator;
  // @ViewChild(MatSort, {static: true}) planOfCaresSort: MatSort;  
  
  constructor(                
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public dialogRef: MatDialogRef<PlanOfCarePickComponent>,
                public appTranslation:AppTranslation,
                public planOfCarePersist: PlanOfCarePersist,
                public planeOfCareNavigator: PlanOfCareNavigator,
                public nursePlaneOfCarePersist:NursePlanOfCarePersist,
                @Inject(MAT_DIALOG_DATA) public data:{selectOne: boolean,encounterId:string}
    ) {

        this.tcAuthorization.requireRead("plan_of_cares");
       this.planOfCareSearchTextBox.setValue(planOfCarePersist.planOfCareSearchText);
      //delay subsequent keyup events
      this.planOfCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.planOfCarePersist.planOfCareSearchText = value;
        this.searchPlan_of_cares();
      });
    } ngOnInit() {
   
      // this.planOfCaresSort.sortChange.subscribe(() => {
      //   this.planOfCaresPaginator.pageIndex = 0;
      //   this.searchPlan_of_cares(true);
      // });

      // this.planOfCaresPaginator.page.subscribe(() => {
      //   this.searchPlan_of_cares(true);
      // });
      //start by loading items
      this.searchPlan_of_cares();
    }

  searchPlan_of_cares(isPagination:boolean = false): void {


    // let paginator = this.planOfCaresPaginator;
    // let sorter = this.planOfCaresSort;

    this.planOfCareIsLoading = true;
    // this.planOfCareSelection = [];

    this.planOfCarePersist.searchPlanOfCare(5,0, "", "").subscribe((partialList: PlanOfCareSummaryPartialList) => {
      this.planOfCaresData = partialList.data;
      if(this.data.encounterId){
      this.planOfCaresData.forEach(element =>{
        if(element.required){
          element.is_disabled = true;
          this.tcUtilsArray.toggleSelection(this.planOfCareSelection, element)
        }
      })
      this.loadNursesPlanOfCare();
    }
      if (partialList.total != -1) {
        this.planOfCaresTotalCount = partialList.total;
      }
      this.planOfCareIsLoading = false;
    }, error => {
      this.planOfCareIsLoading = false;
    });

  }

  loadNursesPlanOfCare():void{
    this.nursePlaneOfCarePersist.nursePlanOfCaresDo("nurse_plane_of_care",{encounter_id:this.data.encounterId}).subscribe((response:NursePlanOfCareDetail[])=>{
      if(response){
        this.alreadyAddedList = response;
        response.forEach((element) => {
          if(!this.tcUtilsArray.containsId(this.planOfCareSelection, element.plan_of_care_id)){
            let plan_of_care = this.planOfCaresData.find(value=> value.id == element.plan_of_care_id);
            plan_of_care.is_disabled = true;
            this.tcUtilsArray.toggleSelection(this.planOfCareSelection, plan_of_care);
          }

        });
      }
    })
  }


  markOneItem(item: PlanOfCareSummary) {
    if(!this.tcUtilsArray.containsId(this.planOfCareSelection,item.id)){
          this.planOfCareSelection = [];
          this.planOfCareSelection.push(item);
        }
        else{
          this.planOfCareSelection = [];
        }
  }

  returnSelected(): void {
    this.alreadyAddedList.forEach(element=>{
      this.tcUtilsArray.removeItemById(this.planOfCareSelection,element.plan_of_care_id)
    })
    this.dialogRef.close(this.planOfCareSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}