import { TCId } from '../tc/models';

export class PlanOfCareSummary extends TCId {
  name: string;
  required: boolean;
  need_alert: boolean;
  alert_date: number;
  is_disabled:boolean;
}
export class PlanOfCareSummaryPartialList {
  data: PlanOfCareSummary[];
  total: number;
}
export class PlanOfCareDetail extends PlanOfCareSummary {}

export class PlanOfCareDashboard {
  total: number;
}
