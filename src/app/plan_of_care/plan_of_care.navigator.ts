import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { PlanOfCareEditComponent } from './plan-of-care-edit/plan-of-care-edit.component';
import { PlanOfCarePickComponent } from './plan-of-care-pick/plan-of-care-pick.component';

@Injectable({
  providedIn: 'root',
})
export class PlanOfCareNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  plan_of_caresUrl(): string {
    return '/plan_of_cares';
  }

  plan_of_careUrl(id: string): string {
    return '/plan_of_cares/' + id;
  }

  viewPlanOfCares(): void {
    this.router.navigateByUrl(this.plan_of_caresUrl());
  }

  viewPlanOfCare(id: string): void {
    this.router.navigateByUrl(this.plan_of_careUrl(id));
  }

  editPlanOfCare(id: string): MatDialogRef<PlanOfCareEditComponent> {
    const dialogRef = this.dialog.open(PlanOfCareEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addPlanOfCare(): MatDialogRef<PlanOfCareEditComponent> {
    const dialogRef = this.dialog.open(PlanOfCareEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickPlanOfCares(encounterId:string,
    selectOne: boolean = false
  ): MatDialogRef<PlanOfCarePickComponent> {
    const dialogRef = this.dialog.open(PlanOfCarePickComponent, {
      data: {selectOne,encounterId},
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
