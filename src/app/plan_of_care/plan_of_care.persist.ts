import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PlanOfCareDashboard, PlanOfCareDetail, PlanOfCareSummaryPartialList} from "./plan_of_care.model";


@Injectable({
  providedIn: 'root'
})
export class PlanOfCarePersist {
 planOfCareSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "plan_of_care/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.planOfCareSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPlanOfCare(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PlanOfCareSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("plan_of_care", this.planOfCareSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<PlanOfCareSummaryPartialList>(url);

  }

  planOfCareDashboard(): Observable<PlanOfCareDashboard> {
    return this.http.get<PlanOfCareDashboard>(environment.tcApiBaseUri + "plan_of_care/dashboard");
  }


  getPlanOfCare(id: string): Observable<PlanOfCareDetail> {
    return this.http.get<PlanOfCareDetail>(environment.tcApiBaseUri + "plan_of_care/" + id);
  }

  addPlanOfCare(item: PlanOfCareDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "plan_of_care",
      item
    );
  }

  updatePlanOfCare(item: PlanOfCareDetail): Observable<PlanOfCareDetail> {
    return this.http.patch<PlanOfCareDetail>(
      environment.tcApiBaseUri + 'plan_of_care/' + item.id,
      item
    );
  }

  deletePlanOfCare(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'plan_of_care/' + id);
  }
  planOfCaresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'plan_of_care/do',
      new TCDoParam(method, payload)
    );
  }

  planOfCareDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'plan_of_care/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "plan_of_care/do", new TCDoParam("download_all", this.filters()));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'plan_of_care/do',
      new TCDoParam('download', ids)
    );
  }


 }