import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineDetailComponent } from './pos-machine-detail.component';

describe('PosMachineDetailComponent', () => {
  let component: PosMachineDetailComponent;
  let fixture: ComponentFixture<PosMachineDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
