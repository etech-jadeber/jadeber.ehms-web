import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PosMachineDetail} from "../pos-machine.model";
import {PosMachinePersist} from "../pos-machine.persist";
import {PosMachineNavigator} from "../pos-machine.navigator";

export enum PosMachineTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-pos_machine-detail',
  templateUrl: './pos-machine-detail.component.html',
  styleUrls: ['./pos-machine-detail.component.scss']
})
export class PosMachineDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  posMachineIsLoading:boolean = false;
  
  PosMachineTabs: typeof PosMachineTabs = PosMachineTabs;
  activeTab: PosMachineTabs = PosMachineTabs.overview;
  //basics
  posMachineDetail: PosMachineDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public posMachineNavigator: PosMachineNavigator,
              public  posMachinePersist: PosMachinePersist) {
    this.tcAuthorization.requireRead("pos_machines");
    this.posMachineDetail = new PosMachineDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("pos_machines");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.posMachineIsLoading = true;
    this.posMachinePersist.getPosMachine(id).subscribe(posMachineDetail => {
          this.posMachineDetail = posMachineDetail;
          this.posMachineIsLoading = false;
        }, error => {
          console.error(error);
          this.posMachineIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.posMachineDetail.id);
  }
  editPosMachine(): void {
    let modalRef = this.posMachineNavigator.editPosMachine(this.posMachineDetail.id);
    modalRef.afterClosed().subscribe(posMachineDetail => {
      TCUtilsAngular.assign(this.posMachineDetail, posMachineDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.posMachinePersist.print(this.posMachineDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print pos_machine", true);
      });
    }

  back():void{
      this.location.back();
    }

}