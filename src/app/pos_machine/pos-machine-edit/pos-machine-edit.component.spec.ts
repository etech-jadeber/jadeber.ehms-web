import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineEditComponent } from './pos-machine-edit.component';

describe('PosMachineEditComponent', () => {
  let component: PosMachineEditComponent;
  let fixture: ComponentFixture<PosMachineEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
