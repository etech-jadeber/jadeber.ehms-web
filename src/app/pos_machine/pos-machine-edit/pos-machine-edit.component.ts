import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PosMachineDetail } from '../pos-machine.model';
import { PosMachinePersist } from '../pos-machine.persist';

@Component({
  selector: 'app-pos_machine-edit',
  templateUrl: './pos-machine-edit.component.html',
  styleUrls: ['./pos-machine-edit.component.scss']
})export class PosMachineEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  posMachineDetail: PosMachineDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PosMachineEditComponent>,
              public  persist: PosMachinePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("pos_machines");
      this.title = this.appTranslation.getText("general","new") +  " " + "pos_machine";
      this.posMachineDetail = new PosMachineDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("pos_machines");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "pos_machine";
      this.isLoadingResults = true;
      this.persist.getPosMachine(this.idMode.id).subscribe(posMachineDetail => {
        this.posMachineDetail = posMachineDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPosMachine(this.posMachineDetail).subscribe(value => {
      this.tcNotification.success("posMachine added");
      this.posMachineDetail.id = value.id;
      this.dialogRef.close(this.posMachineDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePosMachine(this.posMachineDetail).subscribe(value => {
      this.tcNotification.success("pos_machine updated");
      this.dialogRef.close(this.posMachineDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.posMachineDetail == null){
            return false;
          }
          if (!this.posMachineDetail.name){
            return false;
          }
          if (!this.posMachineDetail.mrc){
            return false;
          }
          return true;

 }
 }