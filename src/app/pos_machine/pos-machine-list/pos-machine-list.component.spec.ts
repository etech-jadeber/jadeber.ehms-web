import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineListComponent } from './pos-machine-list.component';

describe('PosMachineListComponent', () => {
  let component: PosMachineListComponent;
  let fixture: ComponentFixture<PosMachineListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
