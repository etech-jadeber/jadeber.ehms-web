import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { PosMachineSummary, PosMachineSummaryPartialList } from '../pos-machine.model';
import { PosMachineNavigator } from '../pos-machine.navigator';
import { PosMachinePersist } from '../pos-machine.persist';
@Component({
  selector: 'app-pos_machine-list',
  templateUrl: './pos-machine-list.component.html',
  styleUrls: ['./pos-machine-list.component.scss']
})export class PosMachineListComponent implements OnInit {
  posMachinesData: PosMachineSummary[] = [];
  posMachinesTotalCount: number = 0;
  posMachineSelectAll:boolean = false;
  posMachineSelection: PosMachineSummary[] = [];

 posMachinesDisplayedColumns: string[] = ["select","action", "name", "mrc" ,"added_date" ];
  posMachineSearchTextBox: FormControl = new FormControl();
  posMachineIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) posMachinesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) posMachinesSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public posMachinePersist: PosMachinePersist,
                public posMachineNavigator: PosMachineNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("pos_machines");
       this.posMachineSearchTextBox.setValue(posMachinePersist.posMachineSearchText);
      //delay subsequent keyup events
      this.posMachineSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.posMachinePersist.posMachineSearchText = value;
        this.searchPos_machines();
      });
    } ngOnInit() {
   
      this.posMachinesSort.sortChange.subscribe(() => {
        this.posMachinesPaginator.pageIndex = 0;
        this.searchPos_machines(true);
      });

      this.posMachinesPaginator.page.subscribe(() => {
        this.searchPos_machines(true);
      });
      //start by loading items
      this.searchPos_machines();
    }

  searchPos_machines(isPagination:boolean = false): void {


    let paginator = this.posMachinesPaginator;
    let sorter = this.posMachinesSort;

    this.posMachineIsLoading = true;
    this.posMachineSelection = [];

    this.posMachinePersist.searchPosMachine(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PosMachineSummaryPartialList) => {
      this.posMachinesData = partialList.data;
      if (partialList.total != -1) {
        this.posMachinesTotalCount = partialList.total;
      }
      this.posMachineIsLoading = false;
    }, error => {
      this.posMachineIsLoading = false;
    });

  } downloadPosMachines(): void {
    if(this.posMachineSelectAll){
         this.posMachinePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download pos_machine", true);
      });
    }
    else{
        this.posMachinePersist.download(this.tcUtilsArray.idsList(this.posMachineSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download pos_machine",true);
            });
        }
  }
addPos_machine(): void {
    let dialogRef = this.posMachineNavigator.addPosMachine();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPos_machines();
      }
    });
  }

  editPosMachine(item: PosMachineSummary) {
    let dialogRef = this.posMachineNavigator.editPosMachine(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePosMachine(item: PosMachineSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("pos_machine");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.posMachinePersist.deletePosMachine(item.id).subscribe(response => {
          this.tcNotification.success("pos_machine deleted");
          this.searchPos_machines();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}