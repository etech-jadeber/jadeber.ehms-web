import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachinePickComponent } from './pos-machine-pick.component';

describe('PosMachinePickComponent', () => {
  let component: PosMachinePickComponent;
  let fixture: ComponentFixture<PosMachinePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachinePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachinePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
