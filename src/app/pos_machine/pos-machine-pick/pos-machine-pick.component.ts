import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  PosMachineSummary,
  PosMachineSummaryPartialList,
} from '../pos-machine.model';
import { PosMachinePersist } from '../pos-machine.persist';
@Component({
  selector: 'app-pos_machine-pick',
  templateUrl: './pos-machine-pick.component.html',
  styleUrls: ['./pos-machine-pick.component.scss'],
})
export class PosMachinePickComponent implements OnInit {
  posMachinesData: PosMachineSummary[] = [];
  posMachinesTotalCount: number = 0;
  posMachineSelectAll: boolean = false;
  posMachineSelection: PosMachineSummary[] = [];

  posMachinesDisplayedColumns: string[] = [
    'select',
    'name',
    'mrc',
  ];
  posMachineSearchTextBox: FormControl = new FormControl();
  posMachineIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true }) posMachinesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) posMachinesSort: MatSort;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public posMachinePersist: PosMachinePersist,
    public dialogRef: MatDialogRef<PosMachinePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead('pos_machines');
    this.posMachineSearchTextBox.setValue(
      posMachinePersist.posMachineSearchText
    );
    //delay subsequent keyup events
    this.posMachineSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.posMachinePersist.posMachineSearchText = value;
        this.searchPos_machines();
      });
  }
  ngOnInit() {
    this.posMachinesSort.sortChange.subscribe(() => {
      this.posMachinesPaginator.pageIndex = 0;
      this.searchPos_machines(true);
    });

    this.posMachinesPaginator.page.subscribe(() => {
      this.searchPos_machines(true);
    });
    //start by loading items
    this.searchPos_machines();
  }

  searchPos_machines(isPagination: boolean = false): void {
    let paginator = this.posMachinesPaginator;
    let sorter = this.posMachinesSort;

    this.posMachineIsLoading = true;
    this.posMachineSelection = [];

    this.posMachinePersist
      .searchPosMachine(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: PosMachineSummaryPartialList) => {
          this.posMachinesData = partialList.data;
          if (partialList.total != -1) {
            this.posMachinesTotalCount = partialList.total;
          }
          this.posMachineIsLoading = false;
        },
        (error) => {
          this.posMachineIsLoading = false;
        }
      );
  }
  markOneItem(item: PosMachineSummary) {
    if (!this.tcUtilsArray.containsId(this.posMachineSelection, item.id)) {
      this.posMachineSelection = [];
      this.posMachineSelection.push(item);
    } else {
      this.posMachineSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.posMachineSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
