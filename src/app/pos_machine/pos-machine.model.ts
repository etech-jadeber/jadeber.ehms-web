import { TCId } from '../tc/models';
export class PosMachineSummary extends TCId {
  name: string;
  mrc: string;
  added_date: number;
}
export class PosMachineSummaryPartialList {
  data: PosMachineSummary[];
  total: number;
}
export class PosMachineDetail extends PosMachineSummary {}

export class PosMachineDashboard {
  total: number;
}
