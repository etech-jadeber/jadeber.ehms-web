import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { PosMachineEditComponent } from './pos-machine-edit/pos-machine-edit.component';
import { PosMachinePickComponent } from './pos-machine-pick/pos-machine-pick.component';

@Injectable({
  providedIn: 'root',
})
export class PosMachineNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  pos_machinesUrl(): string {
    return '/pos_machines';
  }

  pos_machineUrl(id: string): string {
    return '/pos_machines/' + id;
  }

  viewPosMachines(): void {
    this.router.navigateByUrl(this.pos_machinesUrl());
  }

  viewPosMachine(id: string): void {
    this.router.navigateByUrl(this.pos_machineUrl(id));
  }

  editPosMachine(id: string): MatDialogRef<PosMachineEditComponent> {
    const dialogRef = this.dialog.open(PosMachineEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addPosMachine(): MatDialogRef<PosMachineEditComponent> {
    const dialogRef = this.dialog.open(PosMachineEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickPosMachines(
    selectOne: boolean = false
  ): MatDialogRef<PosMachinePickComponent> {
    const dialogRef = this.dialog.open(PosMachinePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
