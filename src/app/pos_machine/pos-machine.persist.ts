import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  PosMachineDashboard,
  PosMachineDetail,
  PosMachineSummaryPartialList,
} from './pos-machine.model';

@Injectable({
  providedIn: 'root',
})
export class PosMachinePersist {
  posMachineSearchText: string = '';
  constructor(private http: HttpClient) {}
  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.posMachineSearchText;
    //add custom filters
    return fltrs;
  }

  searchPosMachine(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<PosMachineSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'pos_machine',
      this.posMachineSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<PosMachineSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  posMachineDashboard(): Observable<PosMachineDashboard> {
    return this.http.get<PosMachineDashboard>(
      environment.tcApiBaseUri + 'pos_machine/dashboard'
    );
  }

  getPosMachine(id: string): Observable<PosMachineDetail> {
    return this.http.get<PosMachineDetail>(
      environment.tcApiBaseUri + 'pos_machine/' + id
    );
  }

//   this is not included in code-gen

  addPosMachine(item: PosMachineDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine/',
      item
    );
  }

  updatePosMachine(item: PosMachineDetail): Observable<PosMachineDetail> {
    return this.http.patch<PosMachineDetail>(
      environment.tcApiBaseUri + 'pos_machine/' + item.id,
      item
    );
  }

  deletePosMachine(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'pos_machine/' + id);
  }
  posmachinesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pos_machine/do',
      new TCDoParam(method, payload)
    );
  }

  posmachineDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pos_machine/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine/do',
      new TCDoParam('download', ids)
    );
  }
}
