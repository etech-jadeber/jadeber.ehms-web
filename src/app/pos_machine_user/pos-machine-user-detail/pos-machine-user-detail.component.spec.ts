import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineUserDetailComponent } from './pos-machine-user-detail.component';

describe('PosMachineUserDetailComponent', () => {
  let component: PosMachineUserDetailComponent;
  let fixture: ComponentFixture<PosMachineUserDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineUserDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
