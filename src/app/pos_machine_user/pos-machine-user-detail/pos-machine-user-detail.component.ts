import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { PosMachineUserDetail } from '../pos-machine-user.model';
import { PosMachineUserNavigator } from '../pos-machine-user.navigator';
import { PosMachineUserPersist } from '../pos-machine-user.persist';


export enum PosMachineUserTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-pos_machine_user-detail',
  templateUrl: './pos-machine-user-detail.component.html',
  styleUrls: ['./pos-machine-user-detail.component.scss']
})
export class PosMachineUserDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  posMachineUserIsLoading:boolean = false;
  
  PosMachineUserTabs: typeof PosMachineUserTabs = PosMachineUserTabs;
  activeTab: PosMachineUserTabs = PosMachineUserTabs.overview;
  //basics
  posMachineUserDetail: PosMachineUserDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public posMachineUserNavigator: PosMachineUserNavigator,
              public  posMachineUserPersist: PosMachineUserPersist) {
    this.tcAuthorization.requireRead("pos_machine_users");
    this.posMachineUserDetail = new PosMachineUserDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("pos_machine_users");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.posMachineUserIsLoading = true;
    this.posMachineUserPersist.getPosMachineUser(id).subscribe(posMachineUserDetail => {
          this.posMachineUserDetail = posMachineUserDetail;
          this.posMachineUserIsLoading = false;
        }, error => {
          console.error(error);
          this.posMachineUserIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.posMachineUserDetail.id);
  }
  editPosMachineUser(): void {
    let modalRef = this.posMachineUserNavigator.editPosMachineUser(this.posMachineUserDetail.id);
    modalRef.afterClosed().subscribe(posMachineUserDetail => {
      TCUtilsAngular.assign(this.posMachineUserDetail, posMachineUserDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.posMachineUserPersist.print(this.posMachineUserDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print pos_machine_user", true);
      });
    }

  back():void{
      this.location.back();
    }

}