import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineUserEditComponent } from './pos-machine-user-edit.component';

describe('PosMachineUserEditComponent', () => {
  let component: PosMachineUserEditComponent;
  let fixture: ComponentFixture<PosMachineUserEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineUserEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
