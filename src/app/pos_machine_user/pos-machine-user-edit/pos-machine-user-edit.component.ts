import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PosMachineUserDetail } from '../pos-machine-user.model';
import { PosMachineUserPersist } from '../pos-machine-user.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { UserNavigator } from 'src/app/tc/users/user.navigator';
import { UserDetail } from 'src/app/tc/users/user.model';
import { PosMachineNavigator } from 'src/app/pos_machine/pos-machine.navigator';
import { PosMachinePersist } from 'src/app/pos_machine/pos-machine.persist';
import { PosMachineDetail } from 'src/app/pos_machine/pos-machine.model';
@Component({
  selector: 'app-pos_machine_user-edit',
  templateUrl: './pos-machine-user-edit.component.html',
  styleUrls: ['./pos-machine-user-edit.component.scss']
})export class PosMachineUserEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  userName: string;
  machineName: string;
  posMachineUserDetail: PosMachineUserDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PosMachineUserEditComponent>,
              public  persist: PosMachineUserPersist,
              public userPersist: UserPersist,
              public userNavigator: UserNavigator,
              public posMachineNavigator: PosMachineNavigator,
              public posMachinePersist: PosMachinePersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("pos_machine_users");
      this.title = this.appTranslation.getText("general","new") +  " " + "pos_machine_user";
      this.posMachineUserDetail = new PosMachineUserDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("pos_machine_users");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "pos_machine_user";
      this.isLoadingResults = true;
      this.persist.getPosMachineUser(this.idMode.id).subscribe(posMachineUserDetail => {
        this.posMachineUserDetail = posMachineUserDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPosMachineUser(this.posMachineUserDetail).subscribe(value => {
      this.tcNotification.success("posMachineUser added");
      this.posMachineUserDetail.id = value.id;
      this.dialogRef.close(this.posMachineUserDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePosMachineUser(this.posMachineUserDetail).subscribe(value => {
      this.tcNotification.success("pos_machine_user updated");
      this.dialogRef.close(this.posMachineUserDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.posMachineUserDetail == null){
            return false;
          }
          if (!this.posMachineUserDetail.pos_machine_id){
            return false;
          }
          if (!this.posMachineUserDetail.user_id){
            return false;
          }
          return true;

 }

 searchUser() {
  let dialogRef = this.userNavigator.pickUsers(true);
  dialogRef.afterClosed().subscribe((result: UserDetail[]) => {
    if (result) {
      this.userName = result[0].name ;
      this.posMachineUserDetail.user_id = result[0].id;
    }
  });
}

searchMachine() {
  let dialogRef = this.posMachineNavigator.pickPosMachines(true);
  dialogRef.afterClosed().subscribe((result: PosMachineDetail[]) => {
    if (result) {
      this.machineName = result[0].name ;
      this.posMachineUserDetail.pos_machine_id = result[0].id;
    }
  });
}
 }