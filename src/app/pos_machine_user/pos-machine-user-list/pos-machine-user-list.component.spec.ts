import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineUserListComponent } from './pos-machine-user-list.component';

describe('PosMachineUserListComponent', () => {
  let component: PosMachineUserListComponent;
  let fixture: ComponentFixture<PosMachineUserListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineUserListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineUserListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
