import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { PosMachineUserSummary, PosMachineUserSummaryPartialList } from '../pos-machine-user.model';
import { PosMachineUserNavigator } from '../pos-machine-user.navigator';
import { PosMachineUserPersist } from '../pos-machine-user.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { PosMachineDetail } from 'src/app/pos_machine/pos-machine.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { PosMachinePersist } from 'src/app/pos_machine/pos-machine.persist';
@Component({
  selector: 'app-pos_machine_user-list',
  templateUrl: './pos-machine-user-list.component.html',
  styleUrls: ['./pos-machine-user-list.component.scss']
})export class PosMachineUserListComponent implements OnInit {
  posMachineUsersData: PosMachineUserSummary[] = [];
  posMachineUsersTotalCount: number = 0;
  posMachineUserSelectAll:boolean = false;
  posMachineUserSelection: PosMachineUserSummary[] = [];
  user : {[id:string]:UserDetail} = {}
  machine : {[id:string]:PosMachineDetail} = {}

 posMachineUsersDisplayedColumns: string[] = ["select","action" , "user_id", "pos_machine_id", "start_date", "end_date", "status" ];
  posMachineUserSearchTextBox: FormControl = new FormControl();
  posMachineUserIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) posMachineUsersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) posMachineUsersSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public posMachineUserPersist: PosMachineUserPersist,
                public posMachineUserNavigator: PosMachineUserNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public userPersist: UserPersist,
                public posMachinePersist: PosMachinePersist

    ) {

        this.tcAuthorization.requireRead("pos_machine_users");
       this.posMachineUserSearchTextBox.setValue(posMachineUserPersist.posMachineUserSearchText);
      //delay subsequent keyup events
      this.posMachineUserSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.posMachineUserPersist.posMachineUserSearchText = value;
        this.searchPos_machine_users();
      });
    } ngOnInit() {
   
      this.posMachineUsersSort.sortChange.subscribe(() => {
        this.posMachineUsersPaginator.pageIndex = 0;
        this.searchPos_machine_users(true);
      });

      this.posMachineUsersPaginator.page.subscribe(() => {
        this.searchPos_machine_users(true);
      });
      //start by loading items
      this.searchPos_machine_users();
    }

  searchPos_machine_users(isPagination:boolean = false): void {


    let paginator = this.posMachineUsersPaginator;
    let sorter = this.posMachineUsersSort;

    this.posMachineUserIsLoading = true;
    this.posMachineUserSelection = [];

    this.posMachineUserPersist.searchPosMachineUser(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PosMachineUserSummaryPartialList) => {
      this.posMachineUsersData = partialList.data;
      this.posMachineUsersData.forEach(machineUser =>{
        if(machineUser.user_id && !this.user[machineUser.user_id]){
          this.userPersist.getUser(machineUser.user_id).subscribe((_user)=>{
            if(_user){
              this.user[machineUser.user_id] = _user;
            }
          })
        }
        if(machineUser.pos_machine_id && !this.machine[machineUser.pos_machine_id]){
          this.posMachinePersist.getPosMachine(machineUser.pos_machine_id).subscribe((data)=>{
            if(data){
              this.machine[machineUser.pos_machine_id] = data;
            }
          })
        }
      })
      if (partialList.total != -1) {
        this.posMachineUsersTotalCount = partialList.total;
      }
      this.posMachineUserIsLoading = false;
    }, error => {
      this.posMachineUserIsLoading = false;
    });

  } downloadPosMachineUsers(): void {
    if(this.posMachineUserSelectAll){
         this.posMachineUserPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download pos_machine_user", true);
      });
    }
    else{
        this.posMachineUserPersist.download(this.tcUtilsArray.idsList(this.posMachineUserSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download pos_machine_user",true);
            });
        }
  }
addPos_machine_user(): void {
    let dialogRef = this.posMachineUserNavigator.addPosMachineUser();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPos_machine_users();
      }
    });
  }

  editPosMachineUser(item: PosMachineUserSummary) {
    let dialogRef = this.posMachineUserNavigator.editPosMachineUser(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  endPosMachineUser(item: PosMachineUserSummary) {
    let dialogRef = this.posMachinePersist.posmachineDo(item.id, "end", {}).subscribe(
      value => {
        this.searchPos_machine_users();
      }
    )
  }

  deletePosMachineUser(item: PosMachineUserSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("pos_machine_user");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.posMachineUserPersist.deletePosMachineUser(item.id).subscribe(response => {
          this.tcNotification.success("pos_machine_user deleted");
          this.searchPos_machine_users();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    getUser(user_id: string): string {
      if (!this.user[user_id]){
        return ''
      }
      return this.user[user_id].name
    }

    getMachine(pos_machine_id: string): string {
      if (!this.machine[pos_machine_id]){
        return ''
      }
      return this.machine[pos_machine_id].name
    }
}