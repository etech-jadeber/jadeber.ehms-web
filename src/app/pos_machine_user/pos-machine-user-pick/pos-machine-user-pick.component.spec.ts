import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMachineUserPickComponent } from './pos-machine-user-pick.component';

describe('PosMachineUserPickComponent', () => {
  let component: PosMachineUserPickComponent;
  let fixture: ComponentFixture<PosMachineUserPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PosMachineUserPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMachineUserPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
