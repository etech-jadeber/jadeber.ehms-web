import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PosMachineUserSummary, PosMachineUserSummaryPartialList } from '../pos-machine-user.model';
import { PosMachineUserPersist } from '../pos-machine-user.persist';
@Component({
  selector: 'app-pos_machine_user-pick',
  templateUrl: './pos-machine-user-pick.component.html',
  styleUrls: ['./pos-machine-user-pick.component.scss']
})export class PosMachineUserPickComponent implements OnInit {
  posMachineUsersData: PosMachineUserSummary[] = [];
  posMachineUsersTotalCount: number = 0;
  posMachineUserSelectAll:boolean = false;
  posMachineUserSelection: PosMachineUserSummary[] = [];

 posMachineUsersDisplayedColumns: string[] = ["select","action", "user", 'machine', 'start_date', 'end_date',"status" ];
  posMachineUserSearchTextBox: FormControl = new FormControl();
  posMachineUserIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) posMachineUsersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) posMachineUsersSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public posMachineUserPersist: PosMachineUserPersist,
                public dialogRef: MatDialogRef<PosMachineUserPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("pos_machine_users");
       this.posMachineUserSearchTextBox.setValue(posMachineUserPersist.posMachineUserSearchText);
      //delay subsequent keyup events
      this.posMachineUserSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.posMachineUserPersist.posMachineUserSearchText = value;
        this.searchPos_machine_users();
      });
    } ngOnInit() {
   
      this.posMachineUsersSort.sortChange.subscribe(() => {
        this.posMachineUsersPaginator.pageIndex = 0;
        this.searchPos_machine_users(true);
      });

      this.posMachineUsersPaginator.page.subscribe(() => {
        this.searchPos_machine_users(true);
      });
      //start by loading items
      this.searchPos_machine_users();
    }

  searchPos_machine_users(isPagination:boolean = false): void {


    let paginator = this.posMachineUsersPaginator;
    let sorter = this.posMachineUsersSort;

    this.posMachineUserIsLoading = true;
    this.posMachineUserSelection = [];

    this.posMachineUserPersist.searchPosMachineUser(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PosMachineUserSummaryPartialList) => {
      this.posMachineUsersData = partialList.data;
      if (partialList.total != -1) {
        this.posMachineUsersTotalCount = partialList.total;
      }
      this.posMachineUserIsLoading = false;
    }, error => {
      this.posMachineUserIsLoading = false;
    });

  }
  markOneItem(item: PosMachineUserSummary) {
    if(!this.tcUtilsArray.containsId(this.posMachineUserSelection,item.id)){
          this.posMachineUserSelection = [];
          this.posMachineUserSelection.push(item);
        }
        else{
          this.posMachineUserSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.posMachineUserSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }