import { TCId } from '../tc/models';
export class PosMachineUserSummary extends TCId {
  start_date: number;
  end_date: number;
  pos_machine_id: string;
  user_id: string;
  status: number;
}
export class PosMachineUserSummaryPartialList {
  data: PosMachineUserSummary[];
  total: number;
}
export class PosMachineUserDetail extends PosMachineUserSummary {}

export class PosMachineUserDashboard {
  total: number;
}
