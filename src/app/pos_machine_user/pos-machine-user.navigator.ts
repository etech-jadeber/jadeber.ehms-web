import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PosMachineUserEditComponent} from "./pos-machine-user-edit/pos-machine-user-edit.component";
import {PosMachineUserPickComponent} from "./pos-machine-user-pick/pos-machine-user-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PosMachineUserNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  pos_machine_usersUrl(): string {
    return "/pos_machine_users";
  }

  pos_machine_userUrl(id: string): string {
    return "/pos_machine_users/" + id;
  }

  viewPosMachineUsers(): void {
    this.router.navigateByUrl(this.pos_machine_usersUrl());
  }

  viewPosMachineUser(id: string): void {
    this.router.navigateByUrl(this.pos_machine_userUrl(id));
  }

  editPosMachineUser(id: string): MatDialogRef<PosMachineUserEditComponent> {
    const dialogRef = this.dialog.open(PosMachineUserEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPosMachineUser(): MatDialogRef<PosMachineUserEditComponent> {
    const dialogRef = this.dialog.open(PosMachineUserEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPosMachineUsers(selectOne: boolean=false): MatDialogRef<PosMachineUserPickComponent> {
      const dialogRef = this.dialog.open(PosMachineUserPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}