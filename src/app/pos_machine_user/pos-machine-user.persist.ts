import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCEnum, TCId, TcDictionary} from "../tc/models";
import {PosMachineUserDashboard, PosMachineUserDetail, PosMachineUserSummaryPartialList} from "./pos-machine-user.model";
import { progress_status } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class PosMachineUserPersist {
 posMachineUserSearchText: string = "";constructor(private http: HttpClient) {
  }

  progressStatus: TCEnum[] = [
    new TCEnum(progress_status.start, 'Start'),
    new TCEnum(progress_status.end, 'End')
  ]
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "pos_machine_user/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.posMachineUserSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPosMachineUser(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PosMachineUserSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("pos_machine_user", this.posMachineUserSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<PosMachineUserSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "pos_machine_user/do", new TCDoParam("download_all", this.filters()));
  }

  posMachineUserDashboard(): Observable<PosMachineUserDashboard> {
    return this.http.get<PosMachineUserDashboard>(environment.tcApiBaseUri + "pos_machine_user/dashboard");
  }

  getPosMachineUser(id: string): Observable<PosMachineUserDetail> {
    return this.http.get<PosMachineUserDetail>(environment.tcApiBaseUri + "pos_machine_user/" + id);
  }

  addPosMachineUser(item: PosMachineUserDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine_user/',
      item
    );
  }

  updatePosMachineUser(item: PosMachineUserDetail): Observable<PosMachineUserDetail> {
    return this.http.patch<PosMachineUserDetail>(
      environment.tcApiBaseUri + 'pos_machine_user/' + item.id,
      item
    );
  }

  deletePosMachineUser(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'pos_machine_user/' + id);
  }
  posmachineusersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pos_machine_user/do',
      new TCDoParam(method, payload)
    );
  }

  posmachineuserDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pos_machine_user/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pos_machine_user/do',
      new TCDoParam('download', ids)
    );
  }
 }