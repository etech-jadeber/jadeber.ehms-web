import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostNatalCareDetailComponent } from './post-natal-care-detail.component';

describe('PostNatalCareDetailComponent', () => {
  let component: PostNatalCareDetailComponent;
  let fixture: ComponentFixture<PostNatalCareDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostNatalCareDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostNatalCareDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
