import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PostNatalCareDetail} from "../post_natal_care.model";
import {PostNatalCarePersist} from "../post_natal_care.persist";
import {PostNatalCareNavigator} from "../post_natal_care.navigator";
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';

export enum PostNatalCareTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-post_natal_care-detail',
  templateUrl: './post-natal-care-detail.component.html',
  styleUrls: ['./post-natal-care-detail.component.css']
})
export class PostNatalCareDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  postNatalCareIsLoading:boolean = false;
  
  PostNatalCareTabs: typeof PostNatalCareTabs = PostNatalCareTabs;
  activeTab: PostNatalCareTabs = PostNatalCareTabs.overview;
  //basics
  postNatalCareDetail: PostNatalCareDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public postNatalCareNavigator: PostNatalCareNavigator,
              public  postNatalCarePersist: PostNatalCarePersist,
              public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,
              ) {
    this.tcAuthorization.requireRead("post_natal_cares");
    this.postNatalCareDetail = new PostNatalCareDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("post_natal_cares");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.postNatalCareIsLoading = true;
    this.postNatalCarePersist.getPostNatalCare(id).subscribe(postNatalCareDetail => {
          this.postNatalCareDetail = postNatalCareDetail;
          this.postNatalCareIsLoading = false;
        }, error => {
          console.error(error);
          this.postNatalCareIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.postNatalCareDetail.id);
  }
  editPostNatalCare(): void {
    let modalRef = this.postNatalCareNavigator.editPostNatalCare(this.postNatalCareDetail.id);
    modalRef.afterClosed().subscribe(postNatalCareDetail => {
      TCUtilsAngular.assign(this.postNatalCareDetail, postNatalCareDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.postNatalCarePersist.print(this.postNatalCareDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print post_natal_care", true);
      });
    }

  back():void{
      this.location.back();
    }

}
