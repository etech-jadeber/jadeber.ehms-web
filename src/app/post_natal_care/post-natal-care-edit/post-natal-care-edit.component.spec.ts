import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostNatalCareEditComponent } from './post-natal-care-edit.component';

describe('PostNatalCareEditComponent', () => {
  let component: PostNatalCareEditComponent;
  let fixture: ComponentFixture<PostNatalCareEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostNatalCareEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostNatalCareEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
