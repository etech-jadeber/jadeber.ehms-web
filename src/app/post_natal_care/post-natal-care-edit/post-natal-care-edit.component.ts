import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes, TCParentChildIds } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PostNatalCareDetail } from '../post_natal_care.model';
import { PostNatalCarePersist } from '../post_natal_care.persist';
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';
@Component({
  selector: 'app-post_natal_care-edit',
  templateUrl: './post-natal-care-edit.component.html',
  styleUrls: ['./post-natal-care-edit.component.css'],
})
export class PostNatalCareEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  postNatalCareDetail: PostNatalCareDetail;
  dateTime: string;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PostNatalCareEditComponent>,
    public persist: PostNatalCarePersist,
    public tCUtilsDate: TCUtilsDate,
    public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,

    @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }

  dateAndTime(timedate: Date): [string, string] {
    const date = `${timedate.getFullYear()}-${(
      '0' +
      (timedate.getMonth() + 1)
    ).slice(-2)}-${('0' + timedate.getDate()).slice(-2)}`;
    const time = `${('0' + timedate.getHours()).slice(-2)}:${(
      '0' + timedate.getMinutes()
    ).slice(-2)}`;
    return [date, time];
  }

  ngOnInit() {
    const today = new Date();
    const [date, time] = this.dateAndTime(today);
    this.dateTime = `${date}`;
    if (this.idMode.context['mode'] == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('post_natal_cares');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'Post Natal Care';
      this.postNatalCareDetail = new PostNatalCareDetail();
      this.postNatalCareDetail.post_natal_visit = this.idMode.context['visits'] + 1;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('post_natal_cares');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'Post Natal Care';
      this.isLoadingResults = true;
      this.transformDates(false);
      this.persist.getPostNatalCare(this.idMode.childId).subscribe(
        (postNatalCareDetail) => {
          this.postNatalCareDetail = postNatalCareDetail;
          const [date, time] = this.dateAndTime(
            new Date(this.postNatalCareDetail.date_checked * 1000)
          );
          this.dateTime = `${date}`;
          this.isLoadingResults = false;
      },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addPostNatalCare(this.idMode.parentId,this.postNatalCareDetail).subscribe(
      (value) => {
        this.tcNotification.success('postNatalCare added');
        this.postNatalCareDetail.id = value.id;
        this.dialogRef.close(this.postNatalCareDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updatePostNatalCare(this.postNatalCareDetail).subscribe(
      (value) => {
        this.tcNotification.success('post_natal_care updated');
        this.dialogRef.close(this.postNatalCareDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.postNatalCareDetail.date_checked =
        new Date(this.dateTime).getTime() / 1000;
    } else {
      // this.postNatalCareDetail.date_checked = this.tCUtilsDate.toDate(this.postNatalCareDetail.date_checked);
    }
  }

  canSubmit(): boolean {
    if (this.postNatalCareDetail == null) {
      return false;
    }

    if (this.postNatalCareDetail.post_natal_visit == -1) {
      return false;
    }
    if (this.postNatalCareDetail.bp == null) {
      return false;
    }
    if (this.postNatalCareDetail.tpr == null) {
      return false;
    }
    // if (
    //   this.postNatalCareDetail.uterus_contracted_look_for_pph == null ||
    //   this.postNatalCareDetail.uterus_contracted_look_for_pph == ''
    // ) {
    //   return false;
    // }
    if (this.dateTime == null) {
      return false;
    }
    if (this.postNatalCareDetail.temp_value == null) {
      return false;
    }
    // if (
    //   this.postNatalCareDetail.dribbling_leaking_urine == null ||
    //   this.postNatalCareDetail.dribbling_leaking_urine == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.postNatalCareDetail.anemia == null ||
    //   this.postNatalCareDetail.anemia == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.postNatalCareDetail.vaginal_discharge == null ||
    //   this.postNatalCareDetail.vaginal_discharge == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.postNatalCareDetail.pelvic_exam == null ||
    //   this.postNatalCareDetail.pelvic_exam == ''
    // ) {
    //   return false;
    // }
    // if (
    //   this.postNatalCareDetail.breast == null ||
    //   this.postNatalCareDetail.breast == ''
    // ) {
    //   return false;
    // }
    return true;
  }
}
