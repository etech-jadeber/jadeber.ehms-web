import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostNatalCareListComponent } from './post-natal-care-list.component';

describe('PostNatalCareListComponent', () => {
  let component: PostNatalCareListComponent;
  let fixture: ComponentFixture<PostNatalCareListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostNatalCareListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostNatalCareListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
