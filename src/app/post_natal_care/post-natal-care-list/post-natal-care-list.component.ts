import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  PostNatalCareSummary,
  PostNatalCareSummaryPartialList,
} from '../post_natal_care.model';
import { PostNatalCarePersist } from '../post_natal_care.persist';
import { PostNatalCareNavigator } from '../post_natal_care.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PresentPregnancyFollowUpPersist } from 'src/app/present_pregnancy_follow_up/present_pregnancy_follow_up.persist';
@Component({
  selector: 'app-post_natal_care-list',
  templateUrl: './post-natal-care-list.component.html',
  styleUrls: ['./post-natal-care-list.component.css'],
})
export class PostNatalCareListComponent implements OnInit {
  postNatalCaresData: PostNatalCareSummary[] = [];
  postNatalCaresTotalCount: number = 0;
  postNatalCareSelectAll: boolean = false;
  postNatalCareSelection: PostNatalCareSummary[] = [];
  postNatalCaresDisplayedColumns: string[] = [
    'action',
    'post_natal_visit',
    'date_checked',
    'bp',
    'tpr',
    'temp_value',
    'anemia',
    'breast',
  ];
  postNatalCareSearchTextBox: FormControl = new FormControl();
  postNatalCareIsLoading: boolean = false;

  @Input() ancHistoryId: any;
  @Output() onResult = new EventEmitter<number>();
  
  @ViewChild(MatPaginator, { static: true })
  postNatalCaresPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) postNatalCaresSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public postNatalCarePersist: PostNatalCarePersist,
    public postNatalCareNavigator: PostNatalCareNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public persentPregenancyFollowUp: PresentPregnancyFollowUpPersist,

  ) {
    this.tcAuthorization.requireRead('post_natal_cares');
    this.postNatalCareSearchTextBox.setValue(
      postNatalCarePersist.postNatalCareSearchText
    );
    //delay subsequent keyup events
    this.postNatalCareSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.postNatalCarePersist.postNatalCareSearchText = value;
        this.searchPostNatalCares();
      });
  }
  ngOnInit() {
    this.postNatalCaresSort.sortChange.subscribe(() => {
      this.postNatalCaresPaginator.pageIndex = 0;
      this.searchPostNatalCares(true);
    });

    this.postNatalCaresPaginator.page.subscribe(() => {
      this.searchPostNatalCares(true);
    });
    // start by loading items
    this.searchPostNatalCares();
  }

  searchPostNatalCares(isPagination: boolean = false): void {
    let paginator = this.postNatalCaresPaginator;
    let sorter = this.postNatalCaresSort;

    this.postNatalCareIsLoading = true;
    this.postNatalCareSelection = [];

    this.postNatalCarePersist.searchPostNatalCare(this.ancHistoryId,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe(
      (partialList: PostNatalCareSummaryPartialList) => {
        this.postNatalCaresData = partialList.data;
        if (partialList.total != -1) {
          this.postNatalCaresTotalCount = partialList.total;
        }
        this.postNatalCareIsLoading = false;
      },
      (error) => {
        this.postNatalCareIsLoading = false;
      }
    );
  }
  downloadPostNatalCares(): void {
    if (this.postNatalCareSelectAll) {
      this.postNatalCarePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download post_natal_care',
          true
        );
      });
    } else {
      this.postNatalCarePersist
        .download(this.tcUtilsArray.idsList(this.postNatalCareSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download post_natal_care',
            true
          );
        });
    }
  }
  addPostNatalCare(): void {
    let dialogRef = this.postNatalCareNavigator.addPostNatalCare(this.ancHistoryId,this.postNatalCaresTotalCount);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchPostNatalCares();
      }
    });
  }

  editPostNatalCare(item: PostNatalCareSummary) {
    let dialogRef = this.postNatalCareNavigator.editPostNatalCare(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deletePostNatalCare(item: PostNatalCareSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('post_natal_care');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.postNatalCarePersist.deletePostNatalCare(item.id).subscribe(
          (response) => {
            this.tcNotification.success('post_natal_care deleted');
            this.searchPostNatalCares();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
