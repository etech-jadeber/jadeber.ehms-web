import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostNatalCarePickComponent } from './post-natal-care-pick.component';

describe('PostNatalCarePickComponent', () => {
  let component: PostNatalCarePickComponent;
  let fixture: ComponentFixture<PostNatalCarePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostNatalCarePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostNatalCarePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
