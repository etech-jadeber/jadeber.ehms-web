import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator'
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PostNatalCareSummary, PostNatalCareSummaryPartialList } from '../post_natal_care.model';
import { PostNatalCarePersist } from '../post_natal_care.persist';
import { PostNatalCareNavigator } from '../post_natal_care.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-post_natal_care-pick',
  templateUrl: './post-natal-care-pick.component.html',
  styleUrls: ['./post-natal-care-pick.component.css']
})export class PostNatalCarePickComponent implements OnInit {
  postNatalCaresData: PostNatalCareSummary[] = [];
  postNatalCaresTotalCount: number = 0;
  postNatalCareSelectAll:boolean = false;
  postNatalCareSelection: PostNatalCareSummary[] = [];

 postNatalCaresDisplayedColumns: string[] = ["select", ,"bp","tpr","uterus_contracted_look_for_pph","date","date_checked","temp","temp_value","dribbling_leaking_urine","anemia","vaginal_discharge","pelvic_exam","breast","vitamin_a","counsenig_danger_signs_epi","baby_breathing","baby_breast_feeding","baby_weight","immunization","hiv_tested","hiv_test_result","arv_px_for_mother","arv_px_for_newborn","feeding_option","mother_referred_to_c_and_sup","new_born_referred_to_chronic_hiv_infant_care","fb_counseled_and_provided","remark","action_taken","attendant_name_and_signature" ];
  postNatalCareSearchTextBox: FormControl = new FormControl();
  postNatalCareIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) postNatalCaresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) postNatalCaresSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public postNatalCarePersist: PostNatalCarePersist,
                public postNatalCareNavigator: PostNatalCareNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PostNatalCarePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("post_natal_cares");
       this.postNatalCareSearchTextBox.setValue(postNatalCarePersist.postNatalCareSearchText);
      //delay subsequent keyup events
      this.postNatalCareSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.postNatalCarePersist.postNatalCareSearchText = value;
        this.searchPost_natal_cares();
      });
    } ngOnInit() {
   
      this.postNatalCaresSort.sortChange.subscribe(() => {
        this.postNatalCaresPaginator.pageIndex = 0;
        this.searchPost_natal_cares(true);
      });

      this.postNatalCaresPaginator.page.subscribe(() => {
        this.searchPost_natal_cares(true);
      });
      //start by loading items
      this.searchPost_natal_cares();
    }

  searchPost_natal_cares(isPagination:boolean = false): void {


    let paginator = this.postNatalCaresPaginator;
    let sorter = this.postNatalCaresSort;

    this.postNatalCareIsLoading = true;
    this.postNatalCareSelection = [];

    this.postNatalCarePersist.searchPostNatalCare(TCUtilsString.getInvalidId(), paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PostNatalCareSummaryPartialList) => {
      this.postNatalCaresData = partialList.data;
      if (partialList.total != -1) {
        this.postNatalCaresTotalCount = partialList.total;
      }
      this.postNatalCareIsLoading = false;
    }, error => {
      this.postNatalCareIsLoading = false;
    });

  }
  markOneItem(item: PostNatalCareSummary) {
    if(!this.tcUtilsArray.containsId(this.postNatalCareSelection,item.id)){
          this.postNatalCareSelection = [];
          this.postNatalCareSelection.push(item);
        }
        else{
          this.postNatalCareSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.postNatalCareSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
