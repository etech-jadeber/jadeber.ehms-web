import { TCId } from '../tc/models';
export class PostNatalCareSummary extends TCId {
  post_natal_visit: number;
  bp: number;
  tpr: number;
  date_checked: number;
  uterus_contracted_look_for_pph: string;
  temp_value: number;
  dribbling_leaking_urine: string;
  anemia: string;
  vaginal_discharge: string;
  pelvic_exam: string;
  breast: string;
  vitamin_a: string;
  counsenig_danger_signs_epi: string;
  baby_breathing: string;
  baby_breast_feeding: string;
  baby_weight: number;
  immunization: string;
  hiv_tested: string;
  hiv_test_result: string;
  arv_px_for_mother: string;
  arv_px_for_newborn: string;
  feeding_option: string;
  mother_referred_to_c_and_sup: string;
  new_born_referred_to_chronic_hiv_infant_care: string;
  fb_counseled_and_provided: string;
  remark: string;
  action_taken: string;
  attendant_name_and_signature: string;
}
export class PostNatalCareSummaryPartialList {
  data: PostNatalCareSummary[];
  total: number;
}
export class PostNatalCareDetail extends PostNatalCareSummary {}

export class PostNatalCareDashboard {
  total: number;
}
