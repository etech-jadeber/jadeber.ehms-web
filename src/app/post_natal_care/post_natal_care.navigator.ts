import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TcDictionary, TCIdMode, TCModalModes, TCParentChildIds } from '../tc/models';

import { PostNatalCareEditComponent } from './post-natal-care-edit/post-natal-care-edit.component';
import { PostNatalCarePickComponent } from './post-natal-care-pick/post-natal-care-pick.component';

@Injectable({
  providedIn: 'root',
})
export class PostNatalCareNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  postNatalCaresUrl(): string {
    return '/post_natal_cares';
  }

  postNatalCareUrl(id: string): string {
    return '/post_natal_cares/' + id;
  }

  viewPostNatalCares(): void {
    this.router.navigateByUrl(this.postNatalCaresUrl());
  }

  viewPostNatalCare(id: string): void {
    this.router.navigateByUrl(this.postNatalCareUrl(id));
  }

  editPostNatalCare(id: string): MatDialogRef<PostNatalCareEditComponent> {
    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = TCModalModes.EDIT;
    const dialogRef = this.dialog.open(PostNatalCareEditComponent, {
      data: new TCParentChildIds(null,id, params),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  addPostNatalCare(parentId:string,visits:number): MatDialogRef<PostNatalCareEditComponent> {
    let params: TcDictionary<any> = new TcDictionary();
    params["mode"] = TCModalModes.NEW;
    params["visits"] = visits;
    
    const dialogRef = this.dialog.open(PostNatalCareEditComponent, {
      data: new TCParentChildIds(parentId, null,params),
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  pickPostNatalCares(
    selectOne: boolean = false
  ): MatDialogRef<PostNatalCarePickComponent> {
    const dialogRef = this.dialog.open(PostNatalCarePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
