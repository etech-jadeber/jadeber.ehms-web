import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  PostNatalCareDashboard,
  PostNatalCareDetail,
  PostNatalCareSummaryPartialList,
} from './post_natal_care.model';
import { PostNatalCareStatus, PostNatalVisit } from '../app.enums';

@Injectable({
  providedIn: 'root',
})
export class PostNatalCarePersist {
  postNatalVisitFilter: number;

  PostNatalVisit: TCEnum[] = [
    new TCEnum(PostNatalVisit.First_Visit, 'First Visit'),
    new TCEnum(PostNatalVisit.Second_Visit, 'Second Visit'),
    new TCEnum(PostNatalVisit.Third_Visit, 'Third Visit'),
  ];

  PostNatalCareStatusFilter: number;

  PostNatalCareStatus: TCEnum[] = [
    new TCEnum(PostNatalCareStatus.completed, 'Completed'),
    new TCEnum(PostNatalCareStatus.incomplete, 'Incomplete'),
  ];

  postNatalCareSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.postNatalCareSearchText;
    //add custom filters
    return fltrs;
  }

  searchPostNatalCare(
    parentId:string,
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<PostNatalCareSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'anc_history/'+parentId+'/post_natal_care',
      this.postNatalCareSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<PostNatalCareSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'post_natal_care/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  postNatalCareDashboard(): Observable<PostNatalCareDashboard> {
    return this.http.get<PostNatalCareDashboard>(
      environment.tcApiBaseUri + 'post_natal_care/dashboard'
    );
  }

  getPostNatalCare(id: string): Observable<PostNatalCareDetail> {
    return this.http.get<PostNatalCareDetail>(
      environment.tcApiBaseUri + 'post_natal_care/' + id
    );
  }
  addPostNatalCare(parentID:string,item: PostNatalCareDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'anc_history/'+ parentID +'/post_natal_care',
      item
    );
  }

  updatePostNatalCare(
    item: PostNatalCareDetail
  ): Observable<PostNatalCareDetail> {
    return this.http.patch<PostNatalCareDetail>(
      environment.tcApiBaseUri + 'post_natal_care/' + item.id,
      item
    );
  }

  deletePostNatalCare(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'post_natal_care/' + id);
  }
  postNatalCaresDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'post_natal_care/do',
      new TCDoParam(method, payload)
    );
  }

  postNatalCareDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'post_natal_cares/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'post_natal_care/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'post_natal_care/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
