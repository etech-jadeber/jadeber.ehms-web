import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreAnsthesiaEvaluationDetailComponent } from './pre-ansthesia-evaluation-detail.component';

describe('PreAnsthesiaEvaluationDetailComponent', () => {
  let component: PreAnsthesiaEvaluationDetailComponent;
  let fixture: ComponentFixture<PreAnsthesiaEvaluationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreAnsthesiaEvaluationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreAnsthesiaEvaluationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
