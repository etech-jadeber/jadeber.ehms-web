import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";
import { PreAnsthesiaEvaluationDetail } from '../pre_ansthesia_evaluation.model';
import { PreAnsthesiaEvaluationNavigator } from '../pre_ansthesia_evaluation.navigator';
import { PreAnsthesiaEvaluationPersist } from '../pre_ansthesia_evaluation.persist';


export enum PreAnsthesiaEvaluationTabs {
  overview,
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-pre-ansthesia-evaluation-detail',
  templateUrl: './pre-ansthesia-evaluation-detail.component.html',
  styleUrls: ['./pre-ansthesia-evaluation-detail.component.scss']
})
export class PreAnsthesiaEvaluationDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  preAnsthesiaEvaluationIsLoading:boolean = false;
  
  PreAnsthesiaEvaluationTabs: typeof PreAnsthesiaEvaluationTabs = PreAnsthesiaEvaluationTabs;
  activeTab: PreAnsthesiaEvaluationTabs = PreAnsthesiaEvaluationTabs.overview;
  //basics
  preAnsthesiaEvaluationDetail: PreAnsthesiaEvaluationDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public preAnsthesiaEvaluationNavigator: PreAnsthesiaEvaluationNavigator,
              public  preAnsthesiaEvaluationPersist: PreAnsthesiaEvaluationPersist) {
    this.tcAuthorization.requireRead("pre_ansthesia_evaluations");
    this.preAnsthesiaEvaluationDetail = new PreAnsthesiaEvaluationDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("pre_ansthesia_evaluations");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.preAnsthesiaEvaluationIsLoading = true;
    this.preAnsthesiaEvaluationPersist.getPreAnsthesiaEvaluation(id).subscribe(preAnsthesiaEvaluationDetail => {
          this.preAnsthesiaEvaluationDetail = preAnsthesiaEvaluationDetail;
          this.preAnsthesiaEvaluationIsLoading = false;
        }, error => {
          console.error(error);
          this.preAnsthesiaEvaluationIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.preAnsthesiaEvaluationDetail.id);
  }
  editPreAnsthesiaEvaluation(): void {
    let modalRef = this.preAnsthesiaEvaluationNavigator.editPreAnsthesiaEvaluation(this.preAnsthesiaEvaluationDetail.id);
    modalRef.afterClosed().subscribe(preAnsthesiaEvaluationDetail => {
      TCUtilsAngular.assign(this.preAnsthesiaEvaluationDetail, preAnsthesiaEvaluationDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.preAnsthesiaEvaluationPersist.print(this.preAnsthesiaEvaluationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print pre_ansthesia_evaluation", true);
      });
    }

  back():void{
      this.location.back();
    }

}