import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreAnsthesiaEvaluationEditComponent } from './pre-ansthesia-evaluation-edit.component';

describe('PreAnsthesiaEvaluationEditComponent', () => {
  let component: PreAnsthesiaEvaluationEditComponent;
  let fixture: ComponentFixture<PreAnsthesiaEvaluationEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreAnsthesiaEvaluationEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreAnsthesiaEvaluationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
