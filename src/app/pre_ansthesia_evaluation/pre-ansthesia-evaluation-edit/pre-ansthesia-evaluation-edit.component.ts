import {Component, OnInit, Inject, Injectable, Input} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PreAnsthesiaEvaluationDetail } from '../pre_ansthesia_evaluation.model';
import { PreAnsthesiaEvaluationPersist } from '../pre_ansthesia_evaluation.persist';
import { Location } from '@angular/common';
import { Icd11Summary } from 'src/app/icd_11/icd_11.model';
import { Icd11Persist } from 'src/app/icd_11/icd_11.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { ProcedureSummary } from 'src/app/procedures/procedure.model';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';

@Injectable()
abstract class PreAnsthesiaEvaluationEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  encounterId: string;
  icd11_diagnosis_options: Icd11Summary[];
  procedures_options: ProcedureSummary[];
  preAnsthesiaEvaluationDetail: PreAnsthesiaEvaluationDetail;
  constructor(
              public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: PreAnsthesiaEvaluationPersist,
              public icd11Persist : Icd11Persist,
              public form_encounterPersist: Form_EncounterPersist,
              public procedurePersist: ProcedurePersist,
              public tcUtilsDate: TCUtilsDate,
              public idMode: TCIdMode) {

  }

 
  action():void{

  }
  onCancel(): void {
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("pre_ansthesia_evaluations");
      this.title = this.appTranslation.getText("general","new") +  " " + "Pre ansthesia evaluation";
      this.preAnsthesiaEvaluationDetail = new PreAnsthesiaEvaluationDetail();
      this.preAnsthesiaEvaluationDetail.encounter_id = this.idMode.id;
      this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis = "";
      this.preAnsthesiaEvaluationDetail.planned_procedure = "";
      this.preAnsthesiaEvaluationDetail.history = "";
      this.preAnsthesiaEvaluationDetail.pervious_surgery_anesthesia_exposure = "";
      this.preAnsthesiaEvaluationDetail.cvd = "";
      this.preAnsthesiaEvaluationDetail.htn = "";
      this.preAnsthesiaEvaluationDetail.oncologic_disease = "";
      this.preAnsthesiaEvaluationDetail.pulmonary = "";
      this.preAnsthesiaEvaluationDetail.dm = "";
      this.preAnsthesiaEvaluationDetail.other_endocrine = "";
      this.preAnsthesiaEvaluationDetail.hematologic = "";
      this.preAnsthesiaEvaluationDetail.gi_liver = "";
      this.preAnsthesiaEvaluationDetail.renal_disease = "";
      this.preAnsthesiaEvaluationDetail.neurologic_disease = "";
      this.preAnsthesiaEvaluationDetail.pyschiatric_disease = "";
      this.preAnsthesiaEvaluationDetail.infectious_disease = "";
      this.preAnsthesiaEvaluationDetail.rheumatic_disease = "";
      this.preAnsthesiaEvaluationDetail.allergy_disease = "";
      this.preAnsthesiaEvaluationDetail.smoking = "";
      this.preAnsthesiaEvaluationDetail.alcohol = "";
      this.preAnsthesiaEvaluationDetail.other = "";
      this.preAnsthesiaEvaluationDetail.functional_status = "";
      this.preAnsthesiaEvaluationDetail.g_a = "";
      this.preAnsthesiaEvaluationDetail.bp = "";
      this.preAnsthesiaEvaluationDetail.hr = "";
      this.preAnsthesiaEvaluationDetail.rr = "";
      this.preAnsthesiaEvaluationDetail.d_to = "";
      this.preAnsthesiaEvaluationDetail.so2 = "";
      this.preAnsthesiaEvaluationDetail.wt = "";
      this.preAnsthesiaEvaluationDetail.bmi = "";
      this.preAnsthesiaEvaluationDetail.heent = "";
      this.preAnsthesiaEvaluationDetail.mallampatic_class = "";
      this.preAnsthesiaEvaluationDetail.iid_text = "";
      this.preAnsthesiaEvaluationDetail.thyromental_distance = "";
      this.preAnsthesiaEvaluationDetail.tmj_mobility = "";
      this.preAnsthesiaEvaluationDetail.neck_mobility = "";
      this.preAnsthesiaEvaluationDetail.other_2 = "";
      this.preAnsthesiaEvaluationDetail.loose_incisor_teeth = "";
      this.preAnsthesiaEvaluationDetail.artificial_denture = "";
      this.preAnsthesiaEvaluationDetail.lgs = "";
      this.preAnsthesiaEvaluationDetail.abdomen = "";
      this.preAnsthesiaEvaluationDetail.chest = "";
      this.preAnsthesiaEvaluationDetail.mss_is = "";
      this.preAnsthesiaEvaluationDetail.cvs = "";
      this.preAnsthesiaEvaluationDetail.cvn = "";
      this.preAnsthesiaEvaluationDetail.medication = "";
      this.preAnsthesiaEvaluationDetail.investigation = "";
      this.preAnsthesiaEvaluationDetail.ecg = "";
      this.preAnsthesiaEvaluationDetail.echo = "";
      this.preAnsthesiaEvaluationDetail.cxr = "";
      this.preAnsthesiaEvaluationDetail.imaging_other = "";
      this.preAnsthesiaEvaluationDetail.consent_for_blood_transfusion = "";
      this.preAnsthesiaEvaluationDetail.functional_capacity = "";
      this.preAnsthesiaEvaluationDetail.asp_ps_class = "";
      this.preAnsthesiaEvaluationDetail.assessment = "";
      this.preAnsthesiaEvaluationDetail.anesthetic_plan_concern = "";

      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("pre_ansthesia_evaluations");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "pre_ansthesia_evaluation";
      this.isLoadingResults = true;
      this.persist.getPreAnsthesiaEvaluation(this.idMode.id).subscribe(preAnsthesiaEvaluationDetail => {
        this.preAnsthesiaEvaluationDetail = preAnsthesiaEvaluationDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
     console.log("on add")
    this.persist.addPreAnsthesiaEvaluation(this.preAnsthesiaEvaluationDetail).subscribe(value => {
      this.tcNotification.success("preAnsthesiaEvaluation added");
      this.preAnsthesiaEvaluationDetail.id = value.id;
      this.action()
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    console.log("on update")

    this.persist.updatePreAnsthesiaEvaluation(this.preAnsthesiaEvaluationDetail).subscribe(value => {
      this.tcNotification.success("pre_ansthesia_evaluation updated");
      this.action()
    }, error => {
      this.isLoadingResults = false;
    })

  } 
  
  canSubmit():boolean{
        if (this.preAnsthesiaEvaluationDetail == null){
            return false;
          }

          return true;
 }


 appendString(value: string):string{
  value = value.trim();
  if (!this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis)
    return "\u2022  " + value;
  if(this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis.endsWith("\n"))
    return this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis + "\u2022  " + value;
  let x = this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis.split('\u2022  ');
  x.pop();
  let y = x.join("\u2022  ")
  return  y ?  y+"\u2022  " +value : "\u2022  " + value;
}
handleInput(event: string, input) {
  if(event && event.length == 1 && !event.endsWith('\u2022'))
    input.value = "\u2022  " + event
  else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
    input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
}
updateIcdDiagnosis(event:string,input):void{
  if (event && !input.value.endsWith("\n"))
  this.icd11Persist.icd11SearchText =input.value.split('\u2022').pop().trim();
  else 
    this.icd11Persist.icd11SearchText = "";
  this.load_icd11Diagnois();
}
load_icd11Diagnois(){
  this.icd11Persist.searchIcd11(5,0,"category","asc").subscribe((result)=>{
    if(result)
      this.icd11_diagnosis_options = result.data;
  })
}

updateProcedure(event:string,input):void{
  if (event && !input.value.endsWith("\n"))
  this.procedurePersist.procedureSearchText =input.value.split('\u2022').pop().trim();
  else 
  this.procedurePersist.procedureSearchText = "";
  this.load_Procedures();
}

load_Procedures(){
  this.procedurePersist.searchProcedure(5,0,"name","asc").subscribe((result)=>{
    if(result)
    this.procedures_options = result.data;
    console.log(this.procedures_options);
  })
}

appendStringProcedure(value: string):string{
  value = value.trim();
  if (!this.preAnsthesiaEvaluationDetail.planned_procedure)
    return "\u2022  " + value;
  if(this.preAnsthesiaEvaluationDetail.planned_procedure.endsWith("\n"))
    return this.preAnsthesiaEvaluationDetail.planned_procedure + "\u2022  " + value;
  let x = this.preAnsthesiaEvaluationDetail.planned_procedure.split('\u2022  ');
  x.pop();
  let y = x.join("\u2022  ")
  return  y ?  y+"\u2022  " +value : "\u2022  " + value;
}

 back():void{
  this.location.back();
}

searchPreAnesthesialEvaluation():void {

}

isDisabled():boolean {
  return !this.form_encounterPersist.encounter_is_active || !this.tcAuthorization.canCreate("pre_ansthesia_evaluations");
}

 }

 @Component({
  selector: 'app-pre-ansthesia-evaluation-edit',
  templateUrl: './pre-ansthesia-evaluation-edit.component.html',
  styleUrls: ['./pre-ansthesia-evaluation-edit.component.scss']
})

export class PreAnsthesiaEvaluationEditComponent extends PreAnsthesiaEvaluationEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  preAnsthesiaEvaluationDetail: PreAnsthesiaEvaluationDetail;
  constructor(
              public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PreAnsthesiaEvaluationEditComponent>,
              public  persist: PreAnsthesiaEvaluationPersist,
              public icd11Persist : Icd11Persist,              
              public form_encounterPersist: Form_EncounterPersist,
              public tcUtilsDate: TCUtilsDate,
              public procedurePersist: ProcedurePersist,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(location,tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist, icd11Persist, form_encounterPersist,procedurePersist,tcUtilsDate,idMode)


  }

  
  onCancel(): void {
    this.dialogRef.close();
  }

  action(){
    this.dialogRef.close(this.preAnsthesiaEvaluationDetail);
  }
 }


 @Component({
  selector: 'app-pre-ansthesia-evaluation-edit-list',
  templateUrl: './pre-ansthesia-evaluation-edit.component.html',
  styleUrls: ['./pre-ansthesia-evaluation-edit.component.scss']
})

export class PreAnsthesiaEvaluationEditListComponent extends PreAnsthesiaEvaluationEditMainComponent {

  isLoadingResults: boolean = false;
  title: string;
  preAnsthesiaEvaluationDetail: PreAnsthesiaEvaluationDetail;

  @Input() encounterId:string;
  constructor(public location: Location,
              public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public icd11Persist : Icd11Persist,
              public  persist: PreAnsthesiaEvaluationPersist,

              public form_encounterPersist: Form_EncounterPersist,
              public procedurePersist: ProcedurePersist,
              public tcUtilsDate: TCUtilsDate,
              ) {

                super(location,tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist,icd11Persist, form_encounterPersist, procedurePersist,tcUtilsDate,{id:null, mode: TCModalModes.NEW})
  }

  action(){
    this.isLoadingResults=false;
  }


  ngOnInit(): void {

    this.searchPreAnesthesialEvaluation();

    }

    searchPreAnesthesialEvaluation(): void {
      this.persist.encounter_id = this.encounterId
      this.persist.searchPreAnsthesiaEvaluation( 1, 0, 'date', 'desc').subscribe(
        preAnsthesiaEvaluation => {
          if(preAnsthesiaEvaluation.data.length){
          this.preAnsthesiaEvaluationDetail =  preAnsthesiaEvaluation.data[0] 
          }
          else{
            this.preAnsthesiaEvaluationDetail = new PreAnsthesiaEvaluationDetail();
            this.preAnsthesiaEvaluationDetail.encounter_id = this.encounterId;
           this.preAnsthesiaEvaluationDetail.pre_operative_diagnosis = "";
           this.preAnsthesiaEvaluationDetail.planned_procedure = "";
           this.preAnsthesiaEvaluationDetail.history = "";
           this.preAnsthesiaEvaluationDetail.pervious_surgery_anesthesia_exposure = "";
           this.preAnsthesiaEvaluationDetail.cvd = "";
           this.preAnsthesiaEvaluationDetail.htn = "";
           this.preAnsthesiaEvaluationDetail.oncologic_disease = "";
           this.preAnsthesiaEvaluationDetail.pulmonary = "";
           this.preAnsthesiaEvaluationDetail.dm = "";
           this.preAnsthesiaEvaluationDetail.other_endocrine = "";
           this.preAnsthesiaEvaluationDetail.hematologic = "";
           this.preAnsthesiaEvaluationDetail.gi_liver = "";
           this.preAnsthesiaEvaluationDetail.renal_disease = "";
           this.preAnsthesiaEvaluationDetail.neurologic_disease = "";
           this.preAnsthesiaEvaluationDetail.pyschiatric_disease = "";
           this.preAnsthesiaEvaluationDetail.infectious_disease = "";
           this.preAnsthesiaEvaluationDetail.rheumatic_disease = "";
           this.preAnsthesiaEvaluationDetail.allergy_disease = "";
           this.preAnsthesiaEvaluationDetail.smoking = "";
           this.preAnsthesiaEvaluationDetail.alcohol = "";
           this.preAnsthesiaEvaluationDetail.other = "";
           this.preAnsthesiaEvaluationDetail.functional_status = "";
           this.preAnsthesiaEvaluationDetail.g_a = "";
           this.preAnsthesiaEvaluationDetail.bp = "";
           this.preAnsthesiaEvaluationDetail.hr = "";
           this.preAnsthesiaEvaluationDetail.rr = "";
           this.preAnsthesiaEvaluationDetail.d_to = "";
           this.preAnsthesiaEvaluationDetail.so2 = "";
           this.preAnsthesiaEvaluationDetail.wt = "";
           this.preAnsthesiaEvaluationDetail.bmi = "";
           this.preAnsthesiaEvaluationDetail.heent = "";
           this.preAnsthesiaEvaluationDetail.mallampatic_class = "";
           this.preAnsthesiaEvaluationDetail.iid_text = "";
           this.preAnsthesiaEvaluationDetail.thyromental_distance = "";
           this.preAnsthesiaEvaluationDetail.tmj_mobility = "";
           this.preAnsthesiaEvaluationDetail.neck_mobility = "";
           this.preAnsthesiaEvaluationDetail.other_2 = "";
           this.preAnsthesiaEvaluationDetail.loose_incisor_teeth = "";
           this.preAnsthesiaEvaluationDetail.artificial_denture = "";
           this.preAnsthesiaEvaluationDetail.lgs = "";
           this.preAnsthesiaEvaluationDetail.abdomen = "";
           this.preAnsthesiaEvaluationDetail.chest = "";
           this.preAnsthesiaEvaluationDetail.mss_is = "";
           this.preAnsthesiaEvaluationDetail.cvs = "";
           this.preAnsthesiaEvaluationDetail.cvn = "";
           this.preAnsthesiaEvaluationDetail.medication = "";
           this.preAnsthesiaEvaluationDetail.investigation = "";
           this.preAnsthesiaEvaluationDetail.ecg = "";
           this.preAnsthesiaEvaluationDetail.echo = "";
           this.preAnsthesiaEvaluationDetail.cxr = "";
           this.preAnsthesiaEvaluationDetail.imaging_other = "";
           this.preAnsthesiaEvaluationDetail.consent_for_blood_transfusion = "";
           this.preAnsthesiaEvaluationDetail.functional_capacity = "";
           this.preAnsthesiaEvaluationDetail.asp_ps_class = "";
           this.preAnsthesiaEvaluationDetail.assessment = "";
           this.preAnsthesiaEvaluationDetail.anesthetic_plan_concern = "";
          }
        })
    }
 }