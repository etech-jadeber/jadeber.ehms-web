import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreAnsthesiaEvaluationListComponent } from './pre-ansthesia-evaluation-list.component';

describe('PreAnsthesiaEvaluationListComponent', () => {
  let component: PreAnsthesiaEvaluationListComponent;
  let fixture: ComponentFixture<PreAnsthesiaEvaluationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreAnsthesiaEvaluationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreAnsthesiaEvaluationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
