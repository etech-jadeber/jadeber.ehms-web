import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { PreAnsthesiaEvaluationSummary, PreAnsthesiaEvaluationSummaryPartialList } from '../pre_ansthesia_evaluation.model';
import { PreAnsthesiaEvaluationNavigator } from '../pre_ansthesia_evaluation.navigator';
import { PreAnsthesiaEvaluationPersist } from '../pre_ansthesia_evaluation.persist';
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-pre-ansthesia-evaluation-list',
  templateUrl: './pre-ansthesia-evaluation-list.component.html',
  styleUrls: ['./pre-ansthesia-evaluation-list.component.scss']
})

export class PreAnsthesiaEvaluationListComponent implements OnInit {
  preAnsthesiaEvaluationsData: PreAnsthesiaEvaluationSummary[] = [];
  preAnsthesiaEvaluationsTotalCount: number = 0;
  preAnsthesiaEvaluationSelectAll:boolean = false;
  preAnsthesiaEvaluationSelection: PreAnsthesiaEvaluationSummary[] = [];
  users: {[id: string]: UserDetail} = {}

 preAnsthesiaEvaluationsDisplayedColumns: string[] = ["select","action","date", 'anesthesiologist_id' ];
  @Input() encounterId:string;
  @Input() patientId: string;
  preAnsthesiaEvaluationSearchTextBox: FormControl = new FormControl();
  preAnsthesiaEvaluationIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) preAnsthesiaEvaluationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) preAnsthesiaEvaluationsSort: MatSort;  
  
  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public preAnsthesiaEvaluationPersist: PreAnsthesiaEvaluationPersist,
                public preAnsthesiaEvaluationNavigator: PreAnsthesiaEvaluationNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public userPersist: UserPersist,

    ) {

        this.tcAuthorization.requireRead("pre_ansthesia_evaluations");
       this.preAnsthesiaEvaluationSearchTextBox.setValue(preAnsthesiaEvaluationPersist.preAnsthesiaEvaluationSearchText);
      //delay subsequent keyup events
      this.preAnsthesiaEvaluationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.preAnsthesiaEvaluationPersist.preAnsthesiaEvaluationSearchText = value;
        this.searchPre_ansthesia_evaluations();
      });
    } ngOnInit() {
      this.preAnsthesiaEvaluationPersist.patient_id = this.patientId;
    if (!this.patientId){
      this.preAnsthesiaEvaluationPersist.encounter_id = this.encounterId;
    }
      this.preAnsthesiaEvaluationsSort.active = 'date';
      this.preAnsthesiaEvaluationsSort.direction = "desc";
      this.preAnsthesiaEvaluationsSort.sortChange.subscribe(() => {
        this.preAnsthesiaEvaluationsPaginator.pageIndex = 0;
        this.searchPre_ansthesia_evaluations(true);
      });

      this.preAnsthesiaEvaluationsPaginator.page.subscribe(() => {
        this.searchPre_ansthesia_evaluations(true);
      });
      //start by loading items
      this.searchPre_ansthesia_evaluations();
    }

    ngOnDestroy(): void {
      this.preAnsthesiaEvaluationPersist.encounter_id = null;
      this.preAnsthesiaEvaluationPersist.patient_id = null;
    }

    handleToggle(event: MatSlideToggleChange){
      if (event.checked){
        this.preAnsthesiaEvaluationPersist.encounter_id = this.encounterId;
      } else {
        this.preAnsthesiaEvaluationPersist.encounter_id = null;
      }
      this.searchPre_ansthesia_evaluations()
    }

  searchPre_ansthesia_evaluations(isPagination:boolean = false): void {


    let paginator = this.preAnsthesiaEvaluationsPaginator;
    let sorter = this.preAnsthesiaEvaluationsSort;

    this.preAnsthesiaEvaluationIsLoading = true;
    this.preAnsthesiaEvaluationSelection = [];

    this.preAnsthesiaEvaluationPersist.searchPreAnsthesiaEvaluation(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PreAnsthesiaEvaluationSummaryPartialList) => {
      this.preAnsthesiaEvaluationsData = partialList.data;
      this.preAnsthesiaEvaluationsData.forEach(data =>{
        if(data.anesthesiologist_id && !this.users[data.anesthesiologist_id]){
          this.userPersist.getUser(data.anesthesiologist_id).subscribe((_user)=>{
            if(_user){
              this.users[data.anesthesiologist_id] = _user;
            }
          })
        }
      })
      if (partialList.total != -1) {
        this.preAnsthesiaEvaluationsTotalCount = partialList.total;
      }
      this.preAnsthesiaEvaluationIsLoading = false;
    }, error => {
      this.preAnsthesiaEvaluationIsLoading = false;
    });

  } downloadPreAnsthesiaEvaluations(): void {
    if(this.preAnsthesiaEvaluationSelectAll){
         this.preAnsthesiaEvaluationPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download pre_ansthesia_evaluation", true);
      });
    }
    else{
        this.preAnsthesiaEvaluationPersist.download(this.tcUtilsArray.idsList(this.preAnsthesiaEvaluationSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download pre_ansthesia_evaluation",true);
            });
        }
  }
addPre_ansthesia_evaluation(): void {
    let dialogRef = this.preAnsthesiaEvaluationNavigator.addPreAnsthesiaEvaluation(this.encounterId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPre_ansthesia_evaluations();
      }
    });
  }

  editPreAnsthesiaEvaluation(item: PreAnsthesiaEvaluationSummary) {
    let dialogRef = this.preAnsthesiaEvaluationNavigator.editPreAnsthesiaEvaluation(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePreAnsthesiaEvaluation(item: PreAnsthesiaEvaluationSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("pre_ansthesia_evaluation");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.preAnsthesiaEvaluationPersist.deletePreAnsthesiaEvaluation(item.id).subscribe(response => {
          this.tcNotification.success("pre_ansthesia_evaluation deleted");
          this.searchPre_ansthesia_evaluations();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}