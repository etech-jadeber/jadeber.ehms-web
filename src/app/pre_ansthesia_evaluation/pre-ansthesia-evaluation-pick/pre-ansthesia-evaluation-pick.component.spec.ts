import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreAnsthesiaEvaluationPickComponent } from './pre-ansthesia-evaluation-pick.component';

describe('PreAnsthesiaEvaluationPickComponent', () => {
  let component: PreAnsthesiaEvaluationPickComponent;
  let fixture: ComponentFixture<PreAnsthesiaEvaluationPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreAnsthesiaEvaluationPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreAnsthesiaEvaluationPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
