import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PreAnsthesiaEvaluationSummary, PreAnsthesiaEvaluationSummaryPartialList } from '../pre_ansthesia_evaluation.model';
import { PreAnsthesiaEvaluationPersist } from '../pre_ansthesia_evaluation.persist';

@Component({
  selector: 'app-pre-ansthesia-evaluation-pick',
  templateUrl: './pre-ansthesia-evaluation-pick.component.html',
  styleUrls: ['./pre-ansthesia-evaluation-pick.component.scss']
})

export class PreAnsthesiaEvaluationPickComponent implements OnInit {
  preAnsthesiaEvaluationsData: PreAnsthesiaEvaluationSummary[] = [];
  preAnsthesiaEvaluationsTotalCount: number = 0;
  preAnsthesiaEvaluationSelectAll:boolean = false;
  preAnsthesiaEvaluationSelection: PreAnsthesiaEvaluationSummary[] = [];

  preAnsthesiaEvaluationsDisplayedColumns: string[] = ["select","action" ,"date" ];
  preAnsthesiaEvaluationSearchTextBox: FormControl = new FormControl();
  preAnsthesiaEvaluationIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) preAnsthesiaEvaluationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) preAnsthesiaEvaluationsSort: MatSort; 
   constructor(                  
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public dialogRef: MatDialogRef<PreAnsthesiaEvaluationPickComponent>,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public preAnsthesiaEvaluationPersist: PreAnsthesiaEvaluationPersist,
               @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("pre_ansthesia_evaluations");
       this.preAnsthesiaEvaluationSearchTextBox.setValue(preAnsthesiaEvaluationPersist.preAnsthesiaEvaluationSearchText);
      //delay subsequent keyup events
      this.preAnsthesiaEvaluationSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.preAnsthesiaEvaluationPersist.preAnsthesiaEvaluationSearchText = value;
        this.searchPre_ansthesia_evaluations();
      });
    } 
    ngOnInit() {
   
      this.preAnsthesiaEvaluationsSort.sortChange.subscribe(() => {
        this.preAnsthesiaEvaluationsPaginator.pageIndex = 0;
        this.searchPre_ansthesia_evaluations(true);
      });

      this.preAnsthesiaEvaluationsPaginator.page.subscribe(() => {
        this.searchPre_ansthesia_evaluations(true);
      });
      //start by loading items
      this.searchPre_ansthesia_evaluations();
    }

  searchPre_ansthesia_evaluations(isPagination:boolean = false): void {


    let paginator = this.preAnsthesiaEvaluationsPaginator;
    let sorter = this.preAnsthesiaEvaluationsSort;

    this.preAnsthesiaEvaluationIsLoading = true;
    this.preAnsthesiaEvaluationSelection = [];

    this.preAnsthesiaEvaluationPersist.searchPreAnsthesiaEvaluation(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PreAnsthesiaEvaluationSummaryPartialList) => {
      this.preAnsthesiaEvaluationsData = partialList.data;
      if (partialList.total != -1) {
        this.preAnsthesiaEvaluationsTotalCount = partialList.total;
      }
      this.preAnsthesiaEvaluationIsLoading = false;
    }, error => {
      this.preAnsthesiaEvaluationIsLoading = false;
    });

  }
  markOneItem(item: PreAnsthesiaEvaluationSummary) {
    if(!this.tcUtilsArray.containsId(this.preAnsthesiaEvaluationSelection,item.id)){
          this.preAnsthesiaEvaluationSelection = [];
          this.preAnsthesiaEvaluationSelection.push(item);
        }
        else{
          this.preAnsthesiaEvaluationSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.preAnsthesiaEvaluationSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }