import {TCId} from "../tc/models";
export class PreAnsthesiaEvaluationSummary extends TCId {
    encounter_id:string;
    patient_id:string;
    pre_operative_diagnosis:string;
    planned_procedure:string;
    history:string;
    pervious_surgery_anesthesia_exposure:string;
    cvd:string;
    htn:string;
    oncologic_disease:string;
    pulmonary:string;
    dm:string;
    other_endocrine:string;
    hematologic:string;
    gi_liver:string;
    renal_disease:string;
    neurologic_disease:string;
    pyschiatric_disease:string;
    infectious_disease:string;
    rheumatic_disease:string;
    allergy_disease:string;
    smoking:string;
    alcohol:string;
    other:string;
    functional_status:string;
    g_a:string;
    bp:string;
    hr:string;
    rr:string;
    d_to:string;
    so2:string;
    wt:string;
    bmi:string;
    heent:string;
    mallampatic_class:string;
    iid_text:string;
    thyromental_distance:string;
    tmj_mobility:string;
    neck_mobility:string;
    other_2: string;
    loose_incisor_teeth:string;
    loose_incisor_teeth_remark: string;
    artificial_denture:string;
    artificial_denture_remark:string;
    lgs:string;
    abdomen:string;
    chest:string;
    mss_is:string;
    cvs:string;
    cvn:string;
    medication:string;
    investigation:string;
    ecg:string;
    echo:string;
    cxr: string;
    imaging_other:string;
    consent_for_blood_transfusion:string;
    functional_capacity:string;
    asp_ps_class:string;
    assessment:string;
    anesthetic_plan_concern:string;
    anesthesiologist_id:string;
    date:number;
     
    }
    export class PreAnsthesiaEvaluationSummaryPartialList {
      data: PreAnsthesiaEvaluationSummary[];
      total: number;
    }
    export class PreAnsthesiaEvaluationDetail extends PreAnsthesiaEvaluationSummary {
    }
    
    export class PreAnsthesiaEvaluationDashboard {
      total: number;
    }