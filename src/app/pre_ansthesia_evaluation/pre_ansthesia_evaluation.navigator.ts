import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import { PreAnsthesiaEvaluationEditComponent } from "./pre-ansthesia-evaluation-edit/pre-ansthesia-evaluation-edit.component";
import { PreAnsthesiaEvaluationPickComponent } from "./pre-ansthesia-evaluation-pick/pre-ansthesia-evaluation-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PreAnsthesiaEvaluationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  pre_ansthesia_evaluationsUrl(): string {
    return "/pre_ansthesia_evaluations";
  }

  pre_ansthesia_evaluationUrl(id: string): string {
    return "/pre_ansthesia_evaluations/" + id;
  }

  viewPreAnsthesiaEvaluations(): void {
    this.router.navigateByUrl(this.pre_ansthesia_evaluationsUrl());
  }

  viewPreAnsthesiaEvaluation(id: string): void {
    this.router.navigateByUrl(this.pre_ansthesia_evaluationUrl(id));
  }

  editPreAnsthesiaEvaluation(id: string): MatDialogRef<PreAnsthesiaEvaluationEditComponent> {
    const dialogRef = this.dialog.open(PreAnsthesiaEvaluationEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }

  addPreAnsthesiaEvaluation(encounterId: string): MatDialogRef<PreAnsthesiaEvaluationEditComponent> {
    const dialogRef = this.dialog.open(PreAnsthesiaEvaluationEditComponent, {
          data: new TCIdMode(encounterId, TCModalModes.NEW),
          width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }
  
   pickPreAnsthesiaEvaluations(selectOne: boolean=false): MatDialogRef<PreAnsthesiaEvaluationPickComponent> {
      const dialogRef = this.dialog.open(PreAnsthesiaEvaluationPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}