import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {PreAnsthesiaEvaluationDashboard, PreAnsthesiaEvaluationDetail, PreAnsthesiaEvaluationSummaryPartialList} from "./pre_ansthesia_evaluation.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class PreAnsthesiaEvaluationPersist {
 preAnsthesiaEvaluationSearchText: string = "";
 encounter_id: string;
 patient_id: string;
 
 constructor(private http: HttpClient) {
  }
 
 
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.preAnsthesiaEvaluationSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPreAnsthesiaEvaluation(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PreAnsthesiaEvaluationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("pre_ansthesia_evaluation", this.preAnsthesiaEvaluationSearchText, pageSize, pageIndex, sort, order);
    if(this.encounter_id){
      url = TCUtilsString.appendUrlParameter(url, 'encounter_id',this.encounter_id);
    }
    if(this.patient_id){
      url = TCUtilsString.appendUrlParameter(url, 'patient_id',this.patient_id);
    }
    return this.http.get<PreAnsthesiaEvaluationSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "pre_ansthesia_evaluation/do", new TCDoParam("download_all", this.filters()));
  }

  preAnsthesiaEvaluationDashboard(): Observable<PreAnsthesiaEvaluationDashboard> {
    return this.http.get<PreAnsthesiaEvaluationDashboard>(environment.tcApiBaseUri + "pre_ansthesia_evaluation/dashboard");
  }

  getPreAnsthesiaEvaluation(id: string): Observable<PreAnsthesiaEvaluationDetail> {
    return this.http.get<PreAnsthesiaEvaluationDetail>(environment.tcApiBaseUri + "pre_ansthesia_evaluation/" + id);
  }
  addPreAnsthesiaEvaluation(item: PreAnsthesiaEvaluationDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "pre_ansthesia_evaluation",
      item
    );
  }

  updatePreAnsthesiaEvaluation(item: PreAnsthesiaEvaluationDetail): Observable<PreAnsthesiaEvaluationDetail> {
    return this.http.patch<PreAnsthesiaEvaluationDetail>(
      environment.tcApiBaseUri + 'pre_ansthesia_evaluation/' + item.id,
      item
    );
  }

  deletePreAnsthesiaEvaluation(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'pre_ansthesia_evaluation/' + id);
  }
  preAnsthesiaEvaluationsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pre_ansthesia_evaluation/do',
      new TCDoParam(method, payload)
    );
  }

  preAnsthesiaEvaluationDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'pre_ansthesia_evaluation/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'pre_ansthesia_evaluation/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "pre_ansthesia_evaluation/" + id + "/do", new TCDoParam("print", {}));
  }
 }