import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../app.translation';
import { PrescriptionsDetail, PrescriptionsSummary } from '../form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryNavigator } from '../historys/history.navigator';
import { HistoryPersist } from '../historys/history.persist';
import { TCAuthorization } from '../tc/authorization';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { debounceTime, interval } from 'rxjs';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { TCAsyncJob } from '../tc/asyncjob';
import { FilePersist } from '../tc/files/file.persist';
import { TCUtilsString } from '../tc/utils-string';
import { TCAppInit } from '../tc/app-init';
import { Item_In_StoreNavigator } from '../item_in_stores/item_in_store.navigator';
import { ItemDetail } from '../items/item.model';
import { ItemPersist } from '../items/item.persist';
import { MedicationAdminstrationChartPersist } from '../medication_adminstration_chart/medication_adminstration_chart.persist';
import { calculateQuantity } from '../form_encounters/prescriptions-edit/prescriptions-edit.component';
import { TCId, TCModalModes } from '../tc/models';
import { category, encounter_status, prescription_type } from '../app.enums';
import { ItemCategoryPersist } from '../item_category/item_category.persist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { PatientDetail } from '../patients/patients.model';
import { PatientPersist } from '../patients/patients.persist';
@Component({
  selector: 'app-prescription-list',
  templateUrl: './prescription-list.component.html',
  styleUrls: ['./prescription-list.component.css']
})
export class PrescriptionListComponent implements OnInit {
  calculateQuantity=calculateQuantity;
  prescriptionssData: PrescriptionsSummary[] = [];
  prescriptionssTotalCount: number = 0;
  prescriptionssSelectAll: boolean = false;
  prescriptionssSelected: PrescriptionsSummary[] = [];
  prescriptionssDisplayedColumns: string[] = [
    'select',
    'action',
    'date_added',
    "prescription",
    // 'drug',
    // 'dosage',
    // 'quantity',
    'diagnosis',
    // 'route',
    // 'how_to_use',
  ];
  prescriptionssSearchTextBox: FormControl = new FormControl();
  prescriptionssLoading: boolean = false;
  prescriptionssSelection: PrescriptionsSummary[] = [];
  success_state = 3;
  message: string = ''; 
  isSysAdmin: boolean= TCAppInit.userInfo.is_sysadmin;
  prescription_type = prescription_type;
  
  
  @Input() store_type: number;
  @Input() encounterId: string;
  @Input() patient_id: string;
  @Input() type: number;
  @Input() prescribe_type: number = prescription_type.doctor_prescription;
  @Input() is_surgery: boolean = false;
  @Output() onResult = new EventEmitter<number>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  add_date: FormControl = new FormControl({disabled: true, value: new Date()});

  patients: {[id: string] : PatientDetail} = {}

  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public patientPersist: PatientPersist,
    public jobPersist: JobPersist,
    public categoryPersist: ItemCategoryPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist, 
    public itemPersist: ItemPersist,
    public itemsInstoreNavigator: Item_In_StoreNavigator,
    public tcUtilsString: TCUtilsString,
    public medicationAdminstrationChartPersist : MedicationAdminstrationChartPersist,
  ) { 

    this.form_encounterPersist.add_date = Math.floor(new Date(this.add_date.value).getTime() / 1000)
    this.prescriptionssSearchTextBox.setValue(
      form_encounterPersist.prescriptionsSearchText
    );
    this.prescriptionssSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.prescriptionsSearchText = value;
        this.searchPrescriptionss();
      });


      this.add_date.valueChanges.pipe(debounceTime(500))
      .subscribe((value)=>{
        this.form_encounterPersist.add_date =new Date(value._d).getTime()/1000;
        this.searchPrescriptionss();
      });
  }

  ngOnInit(): void {
    this.form_encounterPersist.prescriptionEncounterId = this.encounterId
    this.form_encounterPersist.patient_id = this.patient_id

    if(!this.encounterId){
      this.prescriptionssDisplayedColumns = [
        'select',
        'action',
        'pid',
        'patient_id',
        'date_added',
        "prescription",
        // 'drug',
        // 'dosage',
        // 'quantity',
        'diagnosis',
        // 'route',
        // 'how_to_use',
      ];
    }
    this.form_encounterPersist.category = this.type ;
    if(this.form_encounterPersist.encounter_is_active == null){
      this.form_encounterPersist.getForm_Encounter(this.encounterId).subscribe((encounter)=>{
        this.form_encounterPersist.encounter_is_active = encounter.status == encounter_status.opened;
      })
    }
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchPrescriptionss(true);
    });
  // prescriptionss paginator
  this.paginator.page.subscribe(() => {
    this.searchPrescriptionss(true);
  });
  this.sorter.active = 'date_added'
  this.sorter.direction = 'desc'
  this.searchPrescriptionss(true);

  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.form_encounterPersist.prescriptionEncounterId = this.encounterId;
    } else {
      this.form_encounterPersist.prescriptionEncounterId = null;
    }
    this.searchPrescriptionss()
  }

    //prescriptionss methods
    searchPrescriptionss(isPagination: boolean = false): void {
      if (this.encounterId == undefined) {
        this.encounterId = TCUtilsString.getInvalidId();
      }
      let paginator = this.paginator;
      let sorter = this.sorter;
      this.prescriptionssLoading = true;
      this.form_encounterPersist
        .searchPrescriptions(
          paginator.pageSize,
          isPagination ? paginator.pageIndex : 0,
          sorter.active,
          sorter.direction
        )
        .subscribe(
          (response) => {
            this.prescriptionssData = response.data;
            this.prescriptionssData.forEach(prescription=>{
              if(!this.patients[prescription.patient_id]){
                this.patientPersist.getPatient(prescription.patient_id).subscribe(patient=>{
                  this.patients[prescription.patient_id] = patient;
                })
              }

            })
            if (response.total != -1) {
              this.prescriptionssTotalCount=response.total;          
            }
            this.prescriptionssLoading = false;
          },
          (error) => {
            this.prescriptionssLoading = false;
          }
        );
    }
  
    addPrescriptions(is_surgery: boolean): void {
      let dialogRef = this.form_encounterNavigator.addPrescriptions(
        this.encounterId,
        is_surgery,
        this.prescribe_type
      );
      dialogRef.afterClosed().subscribe((newPrescriptions) => {
        if (newPrescriptions) {
          this.searchPrescriptionss();
        }
      });
    }
  
    editPrescriptions(item: PrescriptionsDetail): void {
      let dialogRef = this.form_encounterNavigator.editPrescriptions(
        this.encounterId,
        item.id,
        TCModalModes.EDIT,
        this.is_surgery,
        this.prescribe_type
      );
      dialogRef.afterClosed().subscribe((updatedPrescriptions) => {
        if (updatedPrescriptions) {
          this.searchPrescriptionss();
        }
      });
    }
  

    ngOnDestroy():void {
      this.form_encounterPersist.category = category.drug;
    }

    deletePrescriptions(item: PrescriptionsDetail): void {
      let dialogRef = this.tcNavigator.confirmDeletion('');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist
            .deletePrescriptions(this.encounterId, item.id)
            .subscribe(
              (response) => {
                this.tcNotification.success('prescriptions deleted');
                this.searchPrescriptionss();
              },
              (error) => {}
            );
        }
      });
    }

    // printPrescriptions(item: PrescriptionsSummary) {
    //   this.form_encounterPersist
    //     .prescriptionsDo(
    //       this.encounterId,
    //       item.id,
    //       'print_prescription',
    //       item
    //     )
    //     .subscribe(
    //       (downloadJob) => {
    //         this.followJob(downloadJob.id, 'printing prescription', true);
    //         let success_state = 3;
    //         this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
    //           if (job.job_state == success_state) {
    //           }
    //         });
    //       },
    //       (error) => {}
    //     );
    // }

    printPrescriptions(prescriptionDetail: PrescriptionsDetail, isSelected: boolean = false) {
      let bulk_prints;
      if(isSelected){
        bulk_prints = this.prescriptionssSelection.map(data => data.id).join(',')
      } else {
        bulk_prints = prescriptionDetail.id
      }
      this.form_encounterPersist.prescriptionssDo( this.tcUtilsString.invalid_id, 'print_prescription', {bulk_prints} ).subscribe(
          (downloadJob: TCId) => {
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              this.jobPersist.followJob( downloadJob.id,'printing prescription', true);
              if (job.job_state == this.success_state) {
                // item.print_status = PrintStatus.printed
                // this.printRequestPersist.updatePrintRequest(item)
              }
            });
          },
          (error) => {}
        );
    }


    followJob(
      id: string,
      descrption: string,
      is_download_job: boolean = true
    ): AsyncJob {
      if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
        return;
      }
  
      let jb: AsyncJob = new AsyncJob();
      jb.id = id;
      jb.is_terminated = false;
      jb.is_download_job = is_download_job;
      jb.title = descrption;
  
      this.jobData.unfinishedJobs.push(jb);
  
      if (this.jobData.unfinishedJobs.length > 0) {
        this.tcAsyncJob.show();
      }
  
      const attemptsCounter = interval(3000); //every 3 second
      jb.subscription = attemptsCounter.subscribe((n) => {
        this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
          if (jobstate.job_state == this.success_state) {
            this.jobPersist.unfollowJob(id, jobstate);
  
            if (is_download_job) {
              this.jobPersist.downloadFileKey(id).subscribe((jobFile) => {
                this.tcNavigator.openUrl(
                  this.filePersist.downloadLink(jobFile.download_key)
                );
              });
            }
          } else {
            this.message = jobstate.message;
            this.jobPersist.unfollowJob(id, jobstate);
            this.tcNotification.error(this.message);
          }
        });
      });
  
      return jb;
    }

  downloadPrescriptionss(): void {
    this.tcNotification.info(
      'Download prescriptionss : ' + this.prescriptionssSelected.length
    );
  }

  dispatch(element: PrescriptionsDetail): void {
    const dialogRef =  this.itemsInstoreNavigator.drugs(element);
    dialogRef.afterClosed().subscribe((newPrescriptions: PrescriptionsSummary[]) => {
      if (newPrescriptions) {
        this.form_encounterPersist.prescriptionssDo(element.id, "sell_medicine", newPrescriptions).subscribe(result => {
          this.tcNotification.success('dispatched');
          this.searchPrescriptionss();
        })
      }
    });
  }

  back(): void {
    this.location.back();
  }
}
