import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionRequestByPatientDetailComponent } from './prescription-request-by-patient-detail.component';

describe('PrescriptionRequestByPatientDetailComponent', () => {
  let component: PrescriptionRequestByPatientDetailComponent;
  let fixture: ComponentFixture<PrescriptionRequestByPatientDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionRequestByPatientDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionRequestByPatientDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
