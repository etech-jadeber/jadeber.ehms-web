import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PrescriptionRequestStatus } from 'src/app/app.enums';
import { PrescriptionRequestPersist } from '../prescription_request.persist';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { PatientDetail } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';

@Component({
  selector: 'app-prescription-request-by-patient-detail',
  templateUrl: './prescription-request-by-patient-detail.component.html',
  styleUrls: ['./prescription-request-by-patient-detail.component.scss']
})
export class PrescriptionRequestByPatientDetailComponent implements OnInit {
  paramsSubscription: Subscription;
  patientId: string;
  patient_name: string;
  patient: PatientDetail;
  PrescriptionRequestStatus = PrescriptionRequestStatus

  constructor(
    private route: ActivatedRoute,
    public patientPersist: PatientPersist,
    public prescriptionRequestPersist: PrescriptionRequestPersist,
  ) { }

  ngOnInit(): void {
  }
  onTabChanged(matTab: MatTabChangeEvent){
    this.prescriptionRequestPersist.selectedTab=matTab.index;
  }
  
  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
       this.patientId = this.route.snapshot.paramMap.get('id');
       this.patientPersist.getPatient(this.patientId).subscribe(
        patient => {
          this.patient = patient
          this.patient_name = `${patient.fname} ${patient.mname} ${patient.lname}`
        }
      )
    });
   }

   calculateAge(dob: number) {
    const current = new Date().getFullYear()   
    return current - new Date(dob*1000).getFullYear() 
  }

}
