import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionRequestByPatientListComponent } from './prescription-request-by-patient-list.component';

describe('PrescriptionRequestByPatientListComponent', () => {
  let component: PrescriptionRequestByPatientListComponent;
  let fixture: ComponentFixture<PrescriptionRequestByPatientListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionRequestByPatientListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionRequestByPatientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
