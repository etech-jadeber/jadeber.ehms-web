import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PrescriptionRequestSummary, PrescriptionRequestSummaryPartialList } from '../prescription_request.model';
import { PrescriptionRequestPersist } from '../prescription_request.persist';
import { PrescriptionRequestNavigator } from '../prescription_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
@Component({
  selector: 'prescription-request-by-patient-list',
  templateUrl: './prescription-request-by-patient-list.component.html',
  styleUrls: ['./prescription-request-by-patient-list.component.scss']
})export class PrescriptionRequestByPatientListComponent implements OnInit {
  prescriptionRequestsData: PrescriptionRequestSummary[] = [];
  prescriptionRequestsTotalCount: number = 0;
  prescriptionRequestSelectAll:boolean = false;
  prescriptionRequestSelection: PrescriptionRequestSummary[] = [];

 prescriptionRequestsDisplayedColumns: string[] = ["select" ,"id","patient_name","requested","dispatched" ];
 prescriptionRequestByPatientSearchText: FormControl = new FormControl();
  prescriptionRequestIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) prescriptionRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) prescriptionRequestsSort: MatSort;


constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public storeNavigator: StoreNavigator,
                public prescriptionRequestPersist: PrescriptionRequestPersist,
                public prescriptionRequestNavigator: PrescriptionRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("prescription_requests");
        this.prescriptionRequestPersist.date.setValue(new Date(new Date().toDateString()));
       this.prescriptionRequestByPatientSearchText.setValue(prescriptionRequestPersist.prescriptionRequestByPatientSearchText);
        //delay subsequent keyup events
        this.prescriptionRequestByPatientSearchText.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.prescriptionRequestPersist.prescriptionRequestByPatientSearchText = value;
        this.searchPrescriptionRequests();
      });

      this.prescriptionRequestPersist.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.searchPrescriptionRequests();
      });
    } ngOnInit() {
   
      this.prescriptionRequestsSort.sortChange.subscribe(() => {
        this.prescriptionRequestsPaginator.pageIndex = 0;
        this.searchPrescriptionRequests(true);
      });

      this.prescriptionRequestsPaginator.page.subscribe(() => {
        this.searchPrescriptionRequests(true);
      });
      //start by loading items
      this.searchPrescriptionRequests();
    }

  searchPrescriptionRequests(isPagination:boolean = false): void {


    let paginator = this.prescriptionRequestsPaginator;
    let sorter = this.prescriptionRequestsSort;

    this.prescriptionRequestIsLoading = true;
    this.prescriptionRequestSelection = [];

    this.prescriptionRequestPersist.prescriptionRequestsDo("prescription_requests_by_patient", {ps: paginator.pageSize, pi: isPagination? paginator.pageIndex:0, sc: sorter.active, so: sorter.direction}).subscribe((partialList: PrescriptionRequestSummaryPartialList) => {
      this.prescriptionRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.prescriptionRequestsTotalCount = partialList.total;
      }
      this.prescriptionRequestIsLoading = false;
    }, error => {
      this.prescriptionRequestIsLoading = false;
    });

  } downloadPrescriptionRequests(): void {
    if(this.prescriptionRequestSelectAll){
         this.prescriptionRequestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download prescription_request", true);
      });
    }
    else{
        this.prescriptionRequestPersist.download(this.tcUtilsArray.idsList(this.prescriptionRequestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download prescription_request",true);
            });
        }
  }
addPrescriptionRequest(): void {
    let dialogRef = this.prescriptionRequestNavigator.addPrescriptionRequest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPrescriptionRequests();
      }
    });
  }

  editPrescriptionRequest(item: PrescriptionRequestSummary) {
    let dialogRef = this.prescriptionRequestNavigator.editPrescriptionRequest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.prescriptionRequestPersist.store_name = result[0].name;
        this.prescriptionRequestPersist.store_id = result[0].id;
        this.searchPrescriptionRequests();
      }
    });
  }
  deletePrescriptionRequest(item: PrescriptionRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("prescription_request");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.prescriptionRequestPersist.deletePrescriptionRequest(item.id).subscribe(response => {
          this.tcNotification.success("prescription_request deleted");
          this.searchPrescriptionRequests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
