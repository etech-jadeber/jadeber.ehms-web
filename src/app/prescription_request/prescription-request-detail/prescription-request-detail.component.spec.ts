import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionRequestDetailComponent } from './prescription-request-detail.component';

describe('PrescriptionRequestDetailComponent', () => {
  let component: PrescriptionRequestDetailComponent;
  let fixture: ComponentFixture<PrescriptionRequestDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionRequestDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionRequestDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
