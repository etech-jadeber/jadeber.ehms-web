import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PrescriptionRequestDetail} from "../prescription_request.model";
import {PrescriptionRequestPersist} from "../prescription_request.persist";
import {PrescriptionRequestNavigator} from "../prescription_request.navigator";
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MedicationAdminstrationChartPersist } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.persist';
import { EmployeePersist } from 'src/app/storeemployees/employee.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { calculateQuantity } from 'src/app/form_encounters/prescriptions-edit/prescriptions-edit.component';
import { UserPersist } from 'src/app/tc/users/user.persist';

export enum PrescriptionRequestTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-prescription_request-detail',
  templateUrl: './prescription-request-detail.component.html',
  styleUrls: ['./prescription-request-detail.component.scss']
})
export class PrescriptionRequestDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  prescriptionRequestIsLoading:boolean = false;
  pharmasist_name:string;
  item_name:string;
  interchanged_item_name:string;
  PrescriptionRequestTabs: typeof PrescriptionRequestTabs = PrescriptionRequestTabs;
  activeTab: PrescriptionRequestTabs = PrescriptionRequestTabs.overview;
  //basics
  prescriptionRequestDetail: PrescriptionRequestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public medicationAdminstrationChartPersist: MedicationAdminstrationChartPersist,
              public prescriptionsPersist: Form_EncounterPersist,
              public employeePersist: EmployeePersist,
              public itemPersist: ItemPersist,
              public userPersist: UserPersist,
              public prescriptionRequestNavigator: PrescriptionRequestNavigator,
              public  prescriptionRequestPersist: PrescriptionRequestPersist) {
    this.tcAuthorization.requireRead("prescription_requests");
    this.prescriptionRequestDetail = new PrescriptionRequestDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("prescription_requests");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.prescriptionRequestIsLoading = true;
    this.prescriptionRequestPersist.getPrescriptionRequest(id).subscribe(prescriptionRequestDetail => {
          this.prescriptionRequestDetail = prescriptionRequestDetail;
          this.itemPersist.getItem(prescriptionRequestDetail.item_id).subscribe((value)=>{
            this.item_name =value.name;
          })
          if(this.prescriptionRequestDetail.interchange_id){
            this.itemPersist.getItem(this.prescriptionRequestDetail.interchange_id).subscribe((value)=>{
              this.interchanged_item_name=value.name;
            })
          }
          this.userPersist.getUser(this.prescriptionRequestDetail.pharmacist_id).subscribe((user)=>{
            this.pharmasist_name=user.name;
          })
          this.prescriptionsPersist.getPrescriptions(this.prescriptionRequestDetail.prescription_id).subscribe((prescription)=>{
            if(prescription){
              this.prescriptionRequestDetail.dose=prescription.dose;
              this.prescriptionRequestDetail.frequency =prescription.frequency;
              this.prescriptionRequestDetail.duration = prescription.duration;
              this.prescriptionRequestDetail.route =prescription.route;
              this.prescriptionRequestDetail.duration_unit =prescription.duration_unit;
              this.prescriptionRequestDetail.total=calculateQuantity(prescription.dose,prescription.duration,prescription.duration_unit,prescription.frequency);
            }

          })
          this.prescriptionRequestIsLoading = false;
        }, error => {
          console.error(error);
          this.prescriptionRequestIsLoading = false;
        });
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.prescriptionRequestPersist.selectedIndex=matTab.index;
  }

  reload(){
    this.loadDetails(this.prescriptionRequestDetail.id);
  }
  editPrescriptionRequest(): void {
    let modalRef = this.prescriptionRequestNavigator.editPrescriptionRequest(this.prescriptionRequestDetail.id);
    modalRef.afterClosed().subscribe(prescriptionRequestDetail => {
      TCUtilsAngular.assign(this.prescriptionRequestDetail, prescriptionRequestDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.prescriptionRequestPersist.print(this.prescriptionRequestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print prescription_request", true);
      });
    }

  back():void{
      this.location.back();
    }

}
