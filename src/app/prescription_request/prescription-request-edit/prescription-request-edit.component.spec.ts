import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionRequestEditComponent } from './prescription-request-edit.component';

describe('PrescriptionRequestEditComponent', () => {
  let component: PrescriptionRequestEditComponent;
  let fixture: ComponentFixture<PrescriptionRequestEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionRequestEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionRequestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
