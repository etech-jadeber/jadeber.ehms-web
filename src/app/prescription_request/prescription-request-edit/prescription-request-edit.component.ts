import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PrescriptionRequestDetail } from '../prescription_request.model';import { PrescriptionRequestPersist } from '../prescription_request.persist';@Component({
  selector: 'app-prescription_request-edit',
  templateUrl: './prescription-request-edit.component.html',
  styleUrls: ['./prescription-request-edit.component.scss']
})export class PrescriptionRequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  prescriptionRequestDetail: PrescriptionRequestDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PrescriptionRequestEditComponent>,
              public  persist: PrescriptionRequestPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("prescription_requests");
      this.title = this.appTranslation.getText("general","new") +  " " + "prescription_request";
      this.prescriptionRequestDetail = new PrescriptionRequestDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("prescription_requests");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "prescription_request";
      this.isLoadingResults = true;
      this.persist.getPrescriptionRequest(this.idMode.id).subscribe(prescriptionRequestDetail => {
        this.prescriptionRequestDetail = prescriptionRequestDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPrescriptionRequest(this.prescriptionRequestDetail).subscribe(value => {
      this.tcNotification.success("prescriptionRequest added");
      this.prescriptionRequestDetail.id = value.id;
      this.dialogRef.close(this.prescriptionRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePrescriptionRequest(this.prescriptionRequestDetail).subscribe(value => {
      this.tcNotification.success("prescription_request updated");
      this.dialogRef.close(this.prescriptionRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.prescriptionRequestDetail == null){
            return false;
          }

if (this.prescriptionRequestDetail.prescription_id == null || this.prescriptionRequestDetail.prescription_id  == "") {
            return false;
        } 
if (this.prescriptionRequestDetail.store_id == null || this.prescriptionRequestDetail.store_id  == "") {
            return false;
        } 
if (this.prescriptionRequestDetail.pharmacist_id == null || this.prescriptionRequestDetail.pharmacist_id  == "") {
            return false;
        } 
if (this.prescriptionRequestDetail.interchange_id == null || this.prescriptionRequestDetail.interchange_id  == "") {
            return false;
        } 
if (this.prescriptionRequestDetail.status == null) {
            return false;
        }
if (this.prescriptionRequestDetail.patient_type == null) {
            return false;
        }
if (this.prescriptionRequestDetail.selector_id == null || this.prescriptionRequestDetail.selector_id  == "") {
            return false;
        } 
if (this.prescriptionRequestDetail.payment_id == null || this.prescriptionRequestDetail.payment_id  == "") {
            return false;
        } 
 return true;

 }
 }
