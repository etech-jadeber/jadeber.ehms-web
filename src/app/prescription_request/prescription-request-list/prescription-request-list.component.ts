import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PrescriptionRequestDetail, PrescriptionRequestSummary, PrescriptionRequestSummaryPartialList } from '../prescription_request.model';
import { PrescriptionRequestPersist } from '../prescription_request.persist';
import { PrescriptionRequestNavigator } from '../prescription_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { InterchangeableItemNavigator } from 'src/app/interchangeable_item/interchangeable_item.navigator';
import { TransferNavigator } from 'src/app/transfers/transfer.navigator';
import { calculateQuantity } from 'src/app/form_encounters/prescriptions-edit/prescriptions-edit.component';
import { payment_status, prescription_type, PrescriptionRequestStatus } from 'src/app/app.enums';
import { ItemDispatchNavigator } from 'src/app/item_dispatch/item_dispatch.navigator';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { MedicationAdminstrationChartPersist } from 'src/app/medication_adminstration_chart/medication_adminstration_chart.persist';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { PrescriptionsDetail } from 'src/app/form_encounters/form_encounter.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TCId } from 'src/app/tc/models';
import { TCEnum, TCModalModes } from 'src/app/tc/models';
import { ItemDispatchDetail } from 'src/app/item_dispatch/item_dispatch.model';
import { EmployeeNavigator } from 'src/app/storeemployees/employee.navigator';
import { EmployeeDetail } from 'src/app/storeemployees/employee.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
@Component({
  selector: 'app-prescription_request-list',
  templateUrl: './prescription-request-list.component.html',
  styleUrls: ['./prescription-request-list.component.scss']
})
export class PrescriptionRequestListComponent implements OnInit {
  prescriptionRequestsData: PrescriptionRequestSummary[] = [];
  prescriptionRequestsTotalCount: number = 0;
  prescriptionRequestSelectAll:boolean = false;
  prescriptionRequestSelection: PrescriptionRequestSummary[] = [];

//  prescriptionRequestsDisplayedColumns: string[] = ["select","action","item_name" ,"interchangeable_item","dose","frequency","route","duration","duration_unit","total","patient_type", "request_date" ];

  prescriptionRequestsDisplayedColumns: string[] = ["select","action","treatment","patient_type", "request_date","prescribed"];
  prescriptionRequestSearchTextBox: FormControl = new FormControl();
  prescriptionRequestIsLoading: boolean = false;
  itemDispatchDetailList: ItemDispatchDetail[]=[];
  total: number;
  filterType: number[];
  filter : number [];
  prescription_request:TCEnum[];
  requestor_name: string;
  
  @ViewChild(MatPaginator, {static: true}) prescriptionRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) prescriptionRequestsSort: MatSort;
  @Input() patientId : string;
  @Input() requestType : number;
  @Input() prescribed: boolean = false;
  user_id : string;
  prescriptionRequestStatus = PrescriptionRequestStatus;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public doctorNavigator: DoctorNavigator,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public prescriptionRequestPersist: PrescriptionRequestPersist,
                public prescriptionRequestNavigator: PrescriptionRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public storeEmployeeNavigator: EmployeeNavigator,
                public interChangeItemNavigator: InterchangeableItemNavigator,
                public itemDispatchNavigator: ItemDispatchNavigator,
                public formEncounterPersist: Form_EncounterPersist,
                public medicationPersist: MedicationAdminstrationChartPersist,
                public form_encounterNavigator: Form_EncounterNavigator,
                public tcUtilsString: TCUtilsString,
                
    ) {

        this.tcAuthorization.requireRead("prescription_requests");
       this.prescriptionRequestSearchTextBox.setValue(prescriptionRequestPersist.prescriptionRequestSearchText);
      //delay subsequent keyup events
      this.prescriptionRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.prescriptionRequestPersist.prescriptionRequestSearchText = value;
        this.searchPrescriptionRequests();
      });

      this.prescriptionRequestPersist.date.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.searchPrescriptionRequests();
      });
    } 
    
    ngOnInit() {

      if(this.prescribed){

        // this.prescriptionRequestsDisplayedColumns =  ["select","action","item_name" ,"interchangeable_item","dosage","dose","frequency","route","duration","duration_unit","total","requestor","patient_type", "request_date" ]; 


        this.prescriptionRequestsDisplayedColumns =  ["select","action","prescription","item_name" ,"total","requestor","patient_type", "request_date" ]; 
        
      }  
      
      if(this.requestType == PrescriptionRequestStatus.dispatch){
        this.prescriptionRequestsDisplayedColumns =  ["select","action","prescription","item_name" ,"total","requestor","patient_type", "request_date","response_date" ]; 
        
      }
   
      this.prescriptionRequestsSort.sortChange.subscribe(() => {
        this.prescriptionRequestsPaginator.pageIndex = 0;
        this.searchPrescriptionRequests(true);
      });

      this.prescriptionRequestsPaginator.page.subscribe(() => {
        this.searchPrescriptionRequests(true);
      });
    
      if(this.requestType == PrescriptionRequestStatus.requested){
        this.filterType = [PrescriptionRequestStatus.requested, PrescriptionRequestStatus.accept]
        this.prescription_request = this.prescriptionRequestPersist.PrescriptionRequestStatus.filter((value)=>  value.id == PrescriptionRequestStatus.accept || value.id == PrescriptionRequestStatus.requested );
      } else {
        this.prescription_request = this.prescriptionRequestPersist.PrescriptionRequestStatus.filter((value)=> value.id != PrescriptionRequestStatus.accept && value.id != PrescriptionRequestStatus.requested);
        this.filterType = [PrescriptionRequestStatus.cancel, PrescriptionRequestStatus.dispatch, PrescriptionRequestStatus.reject]
      }
      
      //start by loading items
      this.searchPrescriptionRequests();
    }

  searchPrescriptionRequests(isPagination:boolean = false): void {


    let paginator = this.prescriptionRequestsPaginator;
    let sorter = this.prescriptionRequestsSort;

    this.prescriptionRequestIsLoading = true;
    this.prescriptionRequestSelection = [];

    this.prescriptionRequestPersist.prescriptionRequestsDo(this.prescribed ?"patient_prescription_requests" :"patient_prescription_requests_treatment", {patient_id: this.patientId, user_id : this.user_id , st: this.prescriptionRequestSearchTextBox.value, ps: paginator.pageSize, pi: isPagination? paginator.pageIndex:0, sc: sorter.active, so: sorter.direction, request_type: this.filter ? this.filter : this.filterType}).subscribe((partialList: PrescriptionRequestSummaryPartialList) => {
      this.prescriptionRequestsData = partialList.data;
      // this.prescriptionRequestsData.forEach(
      //   request => {
      //     this.prescribed ? request.total = calculateQuantity(request.dose, request.duration, request.duration_unit, request.frequency || 0) : request.treatments = request.treatments.replace(/\n/g," <br /> ");
          
      //   }
      // )
      // this.prescriptionRequestsData.
      if (partialList.total != -1) {
        this.prescriptionRequestsTotalCount = partialList.total;
      }
      this.prescriptionRequestIsLoading = false;
    }, error => {
      this.prescriptionRequestIsLoading = false;
      console.log(error)
    });

  } downloadPrescriptionRequests(): void {
    if(this.prescriptionRequestSelectAll){
         this.prescriptionRequestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download prescription_request", true);
      });
    }
    else{
        this.prescriptionRequestPersist.download(this.tcUtilsArray.idsList(this.prescriptionRequestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download prescription_request",true);
            });
        }
  }
addPrescriptionRequest(): void {
    let dialogRef = this.prescriptionRequestNavigator.addPrescriptionRequest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPrescriptionRequests();
      }
    });
  }

  editPrescriptionRequest(item: PrescriptionRequestSummary) {
    let dialogRef = this.prescriptionRequestNavigator.editPrescriptionRequest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  printPrescriptions(prescriptionDetail: PrescriptionsDetail, isSelected: boolean = false) {
    let bulk_prints;
    if(isSelected){
      bulk_prints = this.prescriptionRequestSelection.map(data => data.prescription_id).join(',')
    } else {
      bulk_prints = prescriptionDetail.id
    }
    this.formEncounterPersist.prescriptionssDo( this.tcUtilsString.invalid_id, 'print_prescription', {bulk_prints} ).subscribe(
        (downloadJob: TCId) => {
          this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
            this.jobPersist.followJob( downloadJob.id,'printing prescription', true);
          });
        },
        (error) => {}
      );
  }


  batchEditPrescripstions():void {
    this.prescriptionRequestSelection = this.prescriptionRequestSelection.filter(value => this.canAccept(value))
    if(!this.prescriptionRequestSelection.length){
      this.tcNotification.error("you can accept all the prescriptions")
      return;
    }
    let dialogRef = this.form_encounterNavigator.editBatchPrescriptions(this.prescriptionRequestSelection);
    dialogRef.afterClosed().subscribe(prescirtion =>{
      if(prescirtion){
        this.searchPrescriptionRequests();
      }
    })
  }

  interchangeDrug(item: PrescriptionRequestSummary) {
    let dialogRef = this.interChangeItemNavigator.pickInterchangeableItems(item.drug_id);
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.prescriptionRequestIsLoading = true;
        this.prescriptionRequestPersist.prescriptionRequestDo(item.id, "interchange_item", {interchange_id: result[0].interchangeable_id}).subscribe(
          result => {
              this.searchPrescriptionRequests()
              this.prescriptionRequestIsLoading = false;
          }, error => {
            console.error(error)
            this.prescriptionRequestIsLoading = false;
          }
        )
      }
    })
  }


  deletePrescriptionRequest(item: PrescriptionRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("prescription_request");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.prescriptionRequestPersist.deletePrescriptionRequest(item.id).subscribe(response => {
          this.tcNotification.success("prescription_request deleted");
          this.searchPrescriptionRequests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    responseToItems(method: string){
      this.prescriptionRequestIsLoading = true;
      !this.prescriptionRequestSelection.length && (this.prescriptionRequestIsLoading = false)
      this.prescriptionRequestSelection.forEach(
        (request, idx, arr) => {
          this.prescriptionRequestPersist.prescriptionRequestDo(request.id, method, {}).subscribe(
            result => {
              if (idx == arr.length -1){
                this.tcNotification.success("the prescription request accepted");
                this.searchPrescriptionRequests()
                this.prescriptionRequestIsLoading = false
              }
            }, error => {
              console.error(error)
              this.prescriptionRequestIsLoading = false;
            }
          )
        }
      )
    }

    async iterate():Promise<void> {
      for (let prescriptionRequest of  this.prescriptionRequestSelection){
        await this.acceptOrDispatchItems(prescriptionRequest,PrescriptionRequestStatus.accept,TCModalModes.WIZARD);
        
      }
      this.itemDispatchDetailList = [];

    }

    accept(request_status: number):void {
      this.prescriptionRequestSelection = this.prescriptionRequestSelection.filter((value) => value.status != PrescriptionRequestStatus.accept);

      this.acceptOrDispatchItems(this.prescriptionRequestSelection,request_status,TCModalModes.WIZARD);
    }

    acceptOrDispatchItems(item: PrescriptionRequestSummary | PrescriptionRequestSummary[], request_status: number,mode: string = TCModalModes.NEW){
      let dialogRef = this.itemDispatchNavigator.addItemDispatch(item, request_status,mode);
    dialogRef.afterClosed().subscribe((result : ItemDispatchDetail[]) => {
      if(result){
        this.searchPrescriptionRequests();
        
      }
    })
    }



    canAccept(element: PrescriptionRequestDetail): boolean {
        if (!element.quantity ){
            return true;
        }
        if(element.drug_id == null || element.drug_id == TCUtilsString.getInvalidId()){
          return true;
        }
        return this.canEdit(element)
  }


    canEdit(element: PrescriptionRequestDetail): boolean {
    
        if (element.prescription_id && element.status == PrescriptionRequestStatus.requested){
          return false;
        }
   
      return true;
    }

    select(row : PrescriptionRequestDetail):void{
      if(this.tcUtilsArray.contains(this.prescriptionRequestSelection, row, "id")) {
        this.total = this.total - (row.total || 0) ;
        this.tcUtilsArray.removeItem(this.prescriptionRequestSelection, row, "id");
      } else{
        this.total = this.total + (row.total || 0) ;
        this.prescriptionRequestSelection.push(row);
      }  
    }

  editPrescriptions(item: any): void {
    let dialogRef = this.form_encounterNavigator.editPrescriptions(
      item.encounter_id,
      item.prescription_id,
      TCModalModes.EDIT,
      false,
      prescription_type.pharmacist_prescription
    );
    dialogRef.afterClosed().subscribe((updatedPrescriptions) => {
      if (updatedPrescriptions) {
        this.searchPrescriptionRequests();
      }
    });
  } 
    canDispatch(element: PrescriptionRequestDetail): boolean {
    
      if (element.prescription_id && element.status == PrescriptionRequestStatus.accept){
        return false;
      }
    return true;
  }
    addPrescriptions(element :any): void {
      let dialogRef = this.form_encounterNavigator.addPrescriptions(
        element.encounter_id
      );
      dialogRef.afterClosed().subscribe((newPrescriptions) => {
        if (newPrescriptions) {
          this.searchPrescriptionRequests();
        }
      });
    }

    searchStoreEmployee():void {
      let dialogRef = this.storeEmployeeNavigator.pickEmployees(true)
      dialogRef.afterClosed().subscribe((result: EmployeeDetail[]) => {
        if (result) {
          this.user_id = result[0].login_id;
          this.requestor_name = result[0].first_name + " " + result[0].middle_name + result[0].last_name;
          this.searchPrescriptionRequests();
        }
      });
    }
  
    searchPhysicianEmployee():void {
      let dialogRef = this.doctorNavigator.pickDoctors(true)
      dialogRef.afterClosed().subscribe((result: EmployeeDetail[]) => {
        if (result) {
          this.user_id = result[0].login_id;
          this.requestor_name = result[0].first_name + " " + result[0].middle_name + result[0].last_name;
          this.searchPrescriptionRequests();
        }
      });
    }
    // canDispatch(element: PrescriptionRequestSummary): boolean {
    //   return(element.payment_status) && ((element.payment_status == payment_status.paid||(element.payment_status == payment_status.covered)) && element.status==PrescriptionRequestStatus.accept);
    // }

    canVoid(): boolean {
      this.prescriptionRequestSelection.forEach(
        request => {
          if (request.remaining >= request.total){
            return false;
          }
        }
      )
      return true;
    }
}
