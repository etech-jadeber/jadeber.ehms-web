import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionRequestPickComponent } from './prescription-request-pick.component';

describe('PrescriptionRequestPickComponent', () => {
  let component: PrescriptionRequestPickComponent;
  let fixture: ComponentFixture<PrescriptionRequestPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionRequestPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionRequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
