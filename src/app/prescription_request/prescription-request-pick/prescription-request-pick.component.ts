import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PrescriptionRequestSummary, PrescriptionRequestSummaryPartialList } from '../prescription_request.model';
import { PrescriptionRequestPersist } from '../prescription_request.persist';
import { PrescriptionRequestNavigator } from '../prescription_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-prescription_request-pick',
  templateUrl: './prescription-request-pick.component.html',
  styleUrls: ['./prescription-request-pick.component.scss']
})export class PrescriptionRequestPickComponent implements OnInit {
  prescriptionRequestsData: PrescriptionRequestSummary[] = [];
  prescriptionRequestsTotalCount: number = 0;
  prescriptionRequestSelectAll:boolean = false;
  prescriptionRequestSelection: PrescriptionRequestSummary[] = [];

 prescriptionRequestsDisplayedColumns: string[] = ["select", ,"prescription_id","store_id","pharmacist_id","interchange_id","status","store_type","selector_id","payment_id" ];
  prescriptionRequestSearchTextBox: FormControl = new FormControl();
  prescriptionRequestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) prescriptionRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) prescriptionRequestsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public prescriptionRequestPersist: PrescriptionRequestPersist,
                public prescriptionRequestNavigator: PrescriptionRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PrescriptionRequestPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("prescription_requests");
       this.prescriptionRequestSearchTextBox.setValue(prescriptionRequestPersist.prescriptionRequestSearchText);
      //delay subsequent keyup events
      this.prescriptionRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.prescriptionRequestPersist.prescriptionRequestSearchText = value;
        this.searchPrescription_requests();
      });
    } ngOnInit() {
   
      this.prescriptionRequestsSort.sortChange.subscribe(() => {
        this.prescriptionRequestsPaginator.pageIndex = 0;
        this.searchPrescription_requests(true);
      });

      this.prescriptionRequestsPaginator.page.subscribe(() => {
        this.searchPrescription_requests(true);
      });
      //start by loading items
      this.searchPrescription_requests();
    }

  searchPrescription_requests(isPagination:boolean = false): void {


    let paginator = this.prescriptionRequestsPaginator;
    let sorter = this.prescriptionRequestsSort;

    this.prescriptionRequestIsLoading = true;
    this.prescriptionRequestSelection = [];

    this.prescriptionRequestPersist.searchPrescriptionRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PrescriptionRequestSummaryPartialList) => {
      this.prescriptionRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.prescriptionRequestsTotalCount = partialList.total;
      }
      this.prescriptionRequestIsLoading = false;
    }, error => {
      this.prescriptionRequestIsLoading = false;
    });

  }
  markOneItem(item: PrescriptionRequestSummary) {
    if(!this.tcUtilsArray.containsId(this.prescriptionRequestSelection,item.id)){
          this.prescriptionRequestSelection = [];
          this.prescriptionRequestSelection.push(item);
        }
        else{
          this.prescriptionRequestSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.prescriptionRequestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
