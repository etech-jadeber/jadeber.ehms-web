import {TCId} from "../tc/models";

export class PrescriptionRequestSummary extends TCId {
    prescription_id:string;
    store_id:string;
    pharmacist_id:string;
    interchange_id:string;
    status:number;
    patient_type:number;
    selector_id:string;
    payment_id:string;
    drug_id:string;
    remaining: number;
    dose: number;
    quantity: number;
    duration: number;
    duration_unit: number;
    frequency: number;
    total: number;
    payment_status: number;
    item_id: string;
    total_request: number;
    route: number;
    treatments: string;
    }
    export class PrescriptionRequestSummaryPartialList {
      data: PrescriptionRequestSummary[];
      total: number;
    }
    export class PrescriptionRequestDetail extends PrescriptionRequestSummary {
    }
    
    export class PrescriptionRequestDashboard {
      total: number;
    }
    