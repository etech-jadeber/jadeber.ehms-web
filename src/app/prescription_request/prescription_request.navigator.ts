import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PrescriptionRequestEditComponent} from "./prescription-request-edit/prescription-request-edit.component";
import {PrescriptionRequestPickComponent} from "./prescription-request-pick/prescription-request-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PrescriptionRequestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  prescriptionRequestsUrl(): string {
    return "/prescription_requests";
  }

  prescriptionRequestUrl(id: string): string {
    return "/prescription_requests/" + id;
  }

  prescriptionRequestByPatientsUrl(): string {{
    return "/prescription_requests_by_patient"
  }}
  prescriptionRequestByPatientUrl(id: string): string {{
    return "/prescription_requests_by_patient/" + id;
  }}

  viewPrescriptionRequests(): void {
    this.router.navigateByUrl(this.prescriptionRequestsUrl());
  }

  viewPrescriptionRequest(id: string): void {
    this.router.navigateByUrl(this.prescriptionRequestUrl(id));
  }

  editPrescriptionRequest(id: string): MatDialogRef<PrescriptionRequestEditComponent> {
    const dialogRef = this.dialog.open(PrescriptionRequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPrescriptionRequest(): MatDialogRef<PrescriptionRequestEditComponent> {
    const dialogRef = this.dialog.open(PrescriptionRequestEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPrescriptionRequests(selectOne: boolean=false): MatDialogRef<PrescriptionRequestPickComponent> {
      const dialogRef = this.dialog.open(PrescriptionRequestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
