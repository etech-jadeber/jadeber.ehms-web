import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PrescriptionRequestDashboard, PrescriptionRequestDetail, PrescriptionRequestSummaryPartialList} from "./prescription_request.model";
import { PrescriptionRequestStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class PrescriptionRequestPersist {
 prescriptionRequestSearchText: string = "";
 prescriptionRequestByPatientSearchText: string = "";

 selectedIndex:number=0;
 selectedTab: number = 0;
 prescriptionRequestStatusFilter: number ;

    PrescriptionRequestStatus: TCEnum[] = [
     new TCEnum( PrescriptionRequestStatus.requested, 'Requested'),
  new TCEnum( PrescriptionRequestStatus.accept, 'Accepted'),
  new TCEnum( PrescriptionRequestStatus.reject, 'Rejected'),
  new TCEnum( PrescriptionRequestStatus.void, 'Void'),
  new TCEnum( PrescriptionRequestStatus.cancel, 'Cancelled'),
  new TCEnum( PrescriptionRequestStatus.dispatch, 'Dispatched'),

  ];
  store_name: string;
  store_id: string;
  date: FormControl = new FormControl({disabled: true, value: new Date()});

 
 
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.prescriptionRequestSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPrescriptionRequest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PrescriptionRequestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("prescription_request", this.prescriptionRequestSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<PrescriptionRequestSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "prescription_request/do", new TCDoParam("download_all", this.filters()));
  }

  prescriptionRequestDashboard(): Observable<PrescriptionRequestDashboard> {
    return this.http.get<PrescriptionRequestDashboard>(environment.tcApiBaseUri + "prescription_request/dashboard");
  }

  getPrescriptionRequest(id: string): Observable<PrescriptionRequestDetail> {
    return this.http.get<PrescriptionRequestDetail>(environment.tcApiBaseUri + "prescription_request/" + id);
  } addPrescriptionRequest(item: PrescriptionRequestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "prescription_request/", item);
  }

  updatePrescriptionRequest(item: PrescriptionRequestDetail): Observable<PrescriptionRequestDetail> {
    return this.http.patch<PrescriptionRequestDetail>(environment.tcApiBaseUri + "prescription_request/" + item.id, item);
  }

  deletePrescriptionRequest(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "prescription_request/" + id);
  }
 prescriptionRequestsDo(method: string, payload: any): Observable<{}> {
    let url = environment.tcApiBaseUri + "prescription_request/do";
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if(this.date.value){      
      url = TCUtilsString.appendUrlParameter(url, "date", (new Date(this.date.value).getTime()/1000).toString())
    }   
    if(this.prescriptionRequestByPatientSearchText){
      url = TCUtilsString.appendUrlParameter(url, "st", this.prescriptionRequestByPatientSearchText);
    }
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  prescriptionRequestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "prescription_request/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "prescription_request/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "prescription_request/" + id + "/do", new TCDoParam("print", {}));
  }


}
