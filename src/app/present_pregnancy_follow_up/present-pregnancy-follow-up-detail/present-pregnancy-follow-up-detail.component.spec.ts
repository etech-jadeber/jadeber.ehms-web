import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentPregnancyFollowUpDetailComponent } from './present-pregnancy-follow-up-detail.component';

describe('PresentPregnancyFollowUpDetailComponent', () => {
  let component: PresentPregnancyFollowUpDetailComponent;
  let fixture: ComponentFixture<PresentPregnancyFollowUpDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentPregnancyFollowUpDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentPregnancyFollowUpDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
