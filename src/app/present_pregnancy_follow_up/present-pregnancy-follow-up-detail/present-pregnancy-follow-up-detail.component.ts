import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PresentPregnancyFollowUpDetail} from "../present_pregnancy_follow_up.model";
import {PresentPregnancyFollowUpPersist} from "../present_pregnancy_follow_up.persist";
import {PresentPregnancyFollowUpNavigator} from "../present_pregnancy_follow_up.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';

export enum PresentPregnancyFollowUpTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-present_pregnancy_follow_up-detail',
  templateUrl: './present-pregnancy-follow-up-detail.component.html',
  styleUrls: ['./present-pregnancy-follow-up-detail.component.css']
})
export class PresentPregnancyFollowUpDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  presentPregnancyFollowUpIsLoading:boolean = false;
  
  PresentPregnancyFollowUpTabs: typeof PresentPregnancyFollowUpTabs = PresentPregnancyFollowUpTabs;
  activeTab: PresentPregnancyFollowUpTabs = PresentPregnancyFollowUpTabs.overview;
  //basics
  presentPregnancyFollowUpDetail: PresentPregnancyFollowUpDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public tcUtilsDate: TCUtilsDate,
              public presentPregnancyFollowUpNavigator: PresentPregnancyFollowUpNavigator,
              public  presentPregnancyFollowUpPersist: PresentPregnancyFollowUpPersist) {
    this.tcAuthorization.requireRead("present_pregnancy_follow_ups");
    this.presentPregnancyFollowUpDetail = new PresentPregnancyFollowUpDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("present_pregnancy_follow_ups");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.presentPregnancyFollowUpIsLoading = true;
    this.presentPregnancyFollowUpPersist.getPresentPregnancyFollowUp(id).subscribe(presentPregnancyFollowUpDetail => {
          this.presentPregnancyFollowUpDetail = presentPregnancyFollowUpDetail;
          this.presentPregnancyFollowUpIsLoading = false;
        }, error => {
          console.error(error);
          this.presentPregnancyFollowUpIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.presentPregnancyFollowUpDetail.id);
  }
  editPresentPregnancyFollowUp(): void {
    let modalRef = this.presentPregnancyFollowUpNavigator.editPresentPregnancyFollowUp(this.presentPregnancyFollowUpDetail.id);
    modalRef.afterClosed().subscribe(presentPregnancyFollowUpDetail => {
      TCUtilsAngular.assign(this.presentPregnancyFollowUpDetail, presentPregnancyFollowUpDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.presentPregnancyFollowUpPersist.print(this.presentPregnancyFollowUpDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print present_pregnancy_follow_up", true);
      });
    }

  back():void{
      this.location.back();
    }

}