import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentPregnancyFollowUpEditComponent } from './present-pregnancy-follow-up-edit.component';

describe('PresentPregnancyFollowUpEditComponent', () => {
  let component: PresentPregnancyFollowUpEditComponent;
  let fixture: ComponentFixture<PresentPregnancyFollowUpEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentPregnancyFollowUpEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentPregnancyFollowUpEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
