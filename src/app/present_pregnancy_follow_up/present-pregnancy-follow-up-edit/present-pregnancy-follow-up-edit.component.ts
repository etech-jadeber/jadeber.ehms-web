import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCEnum, TCIdMode, TCModalModes, TCParentChildIds } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PresentPregnancyFollowUpDetail } from '../present_pregnancy_follow_up.model'; import { PresentPregnancyFollowUpPersist } from '../present_pregnancy_follow_up.persist'; import { TCUtilsArray } from 'src/app/tc/utils-array';
import { E } from '@angular/cdk/keycodes';
@Component({
  selector: 'app-present_pregnancy_follow_up-edit',
  templateUrl: './present-pregnancy-follow-up-edit.component.html',
  styleUrls: ['./present-pregnancy-follow-up-edit.component.css']
}) 
export class PresentPregnancyFollowUpEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  age: number = 0;


  title: string;
  gestation_age: Date = new Date();
  date_of_visit: Date = new Date();
  presentPregnancyFollowUpDetail: PresentPregnancyFollowUpDetail;
  isSecondary = false;
  constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PresentPregnancyFollowUpEditComponent>,
    public persist: PresentPregnancyFollowUpPersist,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    @Inject(MAT_DIALOG_DATA) public idMode: {id:string,mode:string,visits:number}){

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  
  
  } 
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("present_pregnancy_follow_ups");
      this.title = this.appTranslation.getText("general", "new") + " " + "Present Pregnancy Follow Up";
      this.presentPregnancyFollowUpDetail = new PresentPregnancyFollowUpDetail();
      this.presentPregnancyFollowUpDetail.visit_status = this.idMode.visits + 1;
      //set necessary defaults


    } 
    else if(this.idMode.mode == TCModalModes.CASE){
      this.isSecondary = true;
      this.tcAuthorization.requireCreate("present_pregnancy_follow_ups");
      this.title = this.appTranslation.getText("general", "new") + " " + "Present Pregnancy Follow Up";
      this.presentPregnancyFollowUpDetail = new PresentPregnancyFollowUpDetail();
      this.presentPregnancyFollowUpDetail.visit_status = this.idMode.visits + 1;
    }
    
    else {
      this.tcAuthorization.requireUpdate("present_pregnancy_follow_ups");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "Present Pregnancy Follow Up";
      this.isLoadingResults = true;
      this.persist.getPresentPregnancyFollowUp(this.idMode.id).subscribe(presentPregnancyFollowUpDetail => {
        this.presentPregnancyFollowUpDetail = presentPregnancyFollowUpDetail;
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void { 
    this.isLoadingResults = true;
    this.transformDates();

    this.persist.addPresentPregnancyFollowUp(this.idMode.id, this.presentPregnancyFollowUpDetail).subscribe(value => {
      this.tcNotification.success("presentPregnancyFollowUp added");
      this.presentPregnancyFollowUpDetail.id = value.id;
      this.dialogRef.close(this.presentPregnancyFollowUpDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.presentPregnancyFollowUpDetail.gestation_age = this.tcUtilsDate.toTimeStamp(this.gestation_age);
      this.presentPregnancyFollowUpDetail.date_of_visit = this.tcUtilsDate.toTimeStamp(this.date_of_visit);

    } else {
      this.gestation_age = this.tcUtilsDate.toDate(this.presentPregnancyFollowUpDetail.gestation_age);
      this.date_of_visit = this.tcUtilsDate.toDate(this.presentPregnancyFollowUpDetail.date_of_visit);


    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();

    this.persist.updatePresentPregnancyFollowUp(this.presentPregnancyFollowUpDetail).subscribe(value => {
      this.tcNotification.success("present pregnancy follow up updated");
      this.dialogRef.close(this.presentPregnancyFollowUpDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
  canSubmit(): boolean {
    
    if (this.presentPregnancyFollowUpDetail == null) {
      return false;
    }
    if (this.presentPregnancyFollowUpDetail.visit_status == null) {
      return false;
    }
    if (this.presentPregnancyFollowUpDetail.name_and_signs_of_health_care_provider == null || this.presentPregnancyFollowUpDetail.name_and_signs_of_health_care_provider == "") {
      return false;
    }

    

    return true;
  }
}