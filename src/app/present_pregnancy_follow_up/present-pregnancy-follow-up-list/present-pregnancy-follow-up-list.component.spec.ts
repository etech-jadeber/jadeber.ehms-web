import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentPregnancyFollowUpListComponent } from './present-pregnancy-follow-up-list.component';

describe('PresentPregnancyFollowUpListComponent', () => {
  let component: PresentPregnancyFollowUpListComponent;
  let fixture: ComponentFixture<PresentPregnancyFollowUpListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentPregnancyFollowUpListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentPregnancyFollowUpListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
