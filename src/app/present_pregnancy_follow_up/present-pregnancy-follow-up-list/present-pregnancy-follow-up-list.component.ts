import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { patientPrescriptionWithPregnancy, PresentPregnancyFollowUpSummary, PresentPregnancyFollowUpSummaryPartialList } from '../present_pregnancy_follow_up.model';
import { PresentPregnancyFollowUpPersist } from '../present_pregnancy_follow_up.persist';
import { PresentPregnancyFollowUpNavigator } from '../present_pregnancy_follow_up.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-present_pregnancy_follow_up-list',
  templateUrl: './present-pregnancy-follow-up-list.component.html',
  styleUrls: ['./present-pregnancy-follow-up-list.component.css']
})export class PresentPregnancyFollowUpListComponent implements OnInit {
  presentPregnancyFollowUpsData: PresentPregnancyFollowUpSummary[] = [];
  presentPregnancyFollowUpsTotalCount: number = 0;
  presentPregnancyFollowUpSelectAll:boolean = false;
  presentPregnancyFollowUpSelection: PresentPregnancyFollowUpSummary[] = [];

 presentPregnancyFollowUpsDisplayedColumns: string[] = ["select","action","visit_status","date_of_visit","bp","weight","gestation_age","uterine" , "presentation", "fhb","remarks","appontiment" ];
  presentPregnancyFollowUpSearchTextBox: FormControl = new FormControl();
  presentPregnancyFollowUpIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) presentPregnancyFollowUpsPaginator: MatPaginator;
  @Input() ancHistoryId: any;
  @Output() onResult = new EventEmitter<number>()
  @ViewChild(MatSort, {static: true}) presentPregnancyFollowUpsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public presentPregnancyFollowUpPersist: PresentPregnancyFollowUpPersist,
                public presentPregnancyFollowUpNavigator: PresentPregnancyFollowUpNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("present_pregnancy_follow_ups");
       this.presentPregnancyFollowUpSearchTextBox.setValue(presentPregnancyFollowUpPersist.presentPregnancyFollowUpSearchText);
      //delay subsequent keyup events
      this.presentPregnancyFollowUpSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.presentPregnancyFollowUpPersist.presentPregnancyFollowUpSearchText = value;
        this.searchPresentPregnancyFollowUps();
      });
    } ngOnInit() {
   
      this.presentPregnancyFollowUpsSort.sortChange.subscribe(() => {
        this.presentPregnancyFollowUpsPaginator.pageIndex = 0;
        this.searchPresentPregnancyFollowUps(true);
      });

      this.presentPregnancyFollowUpsPaginator.page.subscribe(() => {
        this.searchPresentPregnancyFollowUps(true);
      });
      //start by loading items
      this.searchPresentPregnancyFollowUps();
    }

  searchPresentPregnancyFollowUps(isPagination:boolean = false): void {


    let paginator = this.presentPregnancyFollowUpsPaginator;
    let sorter = this.presentPregnancyFollowUpsSort;

    this.presentPregnancyFollowUpIsLoading = true;
    this.presentPregnancyFollowUpSelection = [];

    this.presentPregnancyFollowUpPersist.searchPresentPregnancyFollowUp(this.ancHistoryId,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PresentPregnancyFollowUpSummaryPartialList) => {
      this.presentPregnancyFollowUpsData = partialList.data;
      if (partialList.total != -1) {
        this.presentPregnancyFollowUpsTotalCount = partialList.total;
      }
      this.presentPregnancyFollowUpIsLoading = false;
    }, error => {
      this.presentPregnancyFollowUpIsLoading = false;
    });

  }
   downloadPresentPregnancyFollowUps(): void {
    if(this.presentPregnancyFollowUpSelectAll){
         this.presentPregnancyFollowUpPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download present_pregnancy_follow_up", true);
      });
    }
    else{
        this.presentPregnancyFollowUpPersist.download(this.tcUtilsArray.idsList(this.presentPregnancyFollowUpSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download present_pregnancy_follow_up",true);
            });
        }
  }
addPresentPregnancyFollowUp(): void {
    let dialogRef = this.presentPregnancyFollowUpNavigator.addPresentPregnancyFollowUp(this.ancHistoryId,this.presentPregnancyFollowUpsData.length);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPresentPregnancyFollowUps();
      }
    });
  }

  editPresentPregnancyFollowUp(item: PresentPregnancyFollowUpSummary) {
    let dialogRef = this.presentPregnancyFollowUpNavigator.editPresentPregnancyFollowUp(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  // canCreate():boolean{
  //   if(this.presentPregnancyFollowUpsData.length<4)
  //     return true;
  //   else
  //     return false;
  // }

  deletePresentPregnancyFollowUp(item: PresentPregnancyFollowUpSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("present_pregnancy_follow_up");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.presentPregnancyFollowUpPersist.deletePresentPregnancyFollowUp(item.id).subscribe(response => {
          this.tcNotification.success("present_pregnancy_follow_up deleted");
          this.searchPresentPregnancyFollowUps();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
