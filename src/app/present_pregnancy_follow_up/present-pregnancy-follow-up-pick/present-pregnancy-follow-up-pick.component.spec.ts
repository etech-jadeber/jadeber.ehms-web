import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentPregnancyFollowUpPickComponent } from './present-pregnancy-follow-up-pick.component';

describe('PresentPregnancyFollowUpPickComponent', () => {
  let component: PresentPregnancyFollowUpPickComponent;
  let fixture: ComponentFixture<PresentPregnancyFollowUpPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentPregnancyFollowUpPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentPregnancyFollowUpPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
