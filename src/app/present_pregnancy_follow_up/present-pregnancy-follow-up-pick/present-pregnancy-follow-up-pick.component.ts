import {Component, OnInit, Inject,ViewChild} from '@angular/core';
// import {FormControl} from "@angular/forms";
// import {Router} from '@angular/router';

// import {MatSort} from '@angular/material/sort';
// import {MatPaginator} from '@angular/material/paginator';
// import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
// import {debounceTime} from "rxjs/operators";
// import {Location} from '@angular/common';

// import {TCAuthorization} from "../../tc/authorization";
// import {TCUtilsAngular} from "../../tc/utils-angular";
// import {TCUtilsArray} from "../../tc/utils-array";
// import {TCUtilsDate} from "../../tc/utils-date";
// import {TCNotification} from "../../tc/notification";
// import {TCNavigator} from "../../tc/navigator";
// import {JobPersist} from "../../tc/jobs/job.persist";

// import {AppTranslation} from "../../app.translation";
// import { PresentPregnancyFollowUpSummary, PresentPregnancyFollowUpSummaryPartialList } from '../present_pregnancy_follow_up.model';
// import { PresentPregnancyFollowUpPersist } from '../present_pregnancy_follow_up.persist';
// import { PresentPregnancyFollowUpNavigator } from '../present_pregnancy_follow_up.navigator';
// import { JobData } from 'src/app/tc/jobs/job.model';
// import { FilePersist } from 'src/app/tc/files/file.persist';
// import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-present_pregnancy_follow_up-pick',
  templateUrl: './present-pregnancy-follow-up-pick.component.html',
  styleUrls: ['./present-pregnancy-follow-up-pick.component.css']
})export class PresentPregnancyFollowUpPickComponent implements OnInit {
//   presentPregnancyFollowUpsData: PresentPregnancyFollowUpSummary[] = [];
//   presentPregnancyFollowUpsTotalCount: number = 0;
//   presentPregnancyFollowUpSelectAll:boolean = false;
//   presentPregnancyFollowUpSelection: PresentPregnancyFollowUpSummary[] = [];

//  presentPregnancyFollowUpsDisplayedColumns: string[] = ["select","encounter","visit_status" ,"date_of_visit","gestation_age","gravda","para","number_of_children_alive","name_and_signs_of_health_care_provider","marital_status" ];
//   presentPregnancyFollowUpSearchTextBox: FormControl = new FormControl();
//   presentPregnancyFollowUpIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) presentPregnancyFollowUpsPaginator: MatPaginator;
//   @ViewChild(MatSort, {static: true}) presentPregnancyFollowUpsSort: MatSort;
 
constructor(
  // private router: Router,
//                 private location: Location,
//                 public tcAuthorization:TCAuthorization,
//                 public tcUtilsAngular: TCUtilsAngular,
//                 public tcUtilsArray: TCUtilsArray,
//                 public tcUtilsDate:TCUtilsDate,
//                 public tcNotification: TCNotification,
//                 public tcNavigator: TCNavigator,
//                 public appTranslation:AppTranslation,
//                 public presentPregnancyFollowUpPersist: PresentPregnancyFollowUpPersist,
//                 public presentPregnancyFollowUpNavigator: PresentPregnancyFollowUpNavigator,
//                 public jobPersist: JobPersist,
//                 public  jobData: JobData,
//                 public filePersist: FilePersist,
//                 public tcAsyncJob: TCAsyncJob,
//  public dialogRef: MatDialogRef<PresentPregnancyFollowUpPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

      //   this.tcAuthorization.requireRead("present_pregnancy_follow_ups");
      //  this.presentPregnancyFollowUpSearchTextBox.setValue(presentPregnancyFollowUpPersist.presentPregnancyFollowUpSearchText);
      // //delay subsequent keyup events
      // this.presentPregnancyFollowUpSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      //   this.presentPregnancyFollowUpPersist.presentPregnancyFollowUpSearchText = value;
      //   this.searchPresent_pregnancy_follow_ups();
      // });
    } ngOnInit() {
   
      // this.presentPregnancyFollowUpsSort.sortChange.subscribe(() => {
      //   this.presentPregnancyFollowUpsPaginator.pageIndex = 0;
      //   this.searchPresent_pregnancy_follow_ups(true);
      // });

      // this.presentPregnancyFollowUpsPaginator.page.subscribe(() => {
      //   this.searchPresent_pregnancy_follow_ups(true);
      // });
      // //start by loading items
      // this.searchPresent_pregnancy_follow_ups();
    }

  // searchPresent_pregnancy_follow_ups(isPagination:boolean = false): void {


  //   let paginator = this.presentPregnancyFollowUpsPaginator;
  //   let sorter = this.presentPregnancyFollowUpsSort;

  //   this.presentPregnancyFollowUpIsLoading = true;
  //   this.presentPregnancyFollowUpSelection = [];

  //   this.presentPregnancyFollowUpPersist.searchPresentPregnancyFollowUp(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PresentPregnancyFollowUpSummaryPartialList) => {
  //     this.presentPregnancyFollowUpsData = partialList.data;
  //     if (partialList.total != -1) {
  //       this.presentPregnancyFollowUpsTotalCount = partialList.total;
  //     }
  //     this.presentPregnancyFollowUpIsLoading = false;
  //   }, error => {
  //     this.presentPregnancyFollowUpIsLoading = false;
  //   });

  // }
  // markOneItem(item: PresentPregnancyFollowUpSummary) {
  //   if(!this.tcUtilsArray.containsId(this.presentPregnancyFollowUpSelection,item.id)){
  //         this.presentPregnancyFollowUpSelection = [];
  //         this.presentPregnancyFollowUpSelection.push(item);
  //       }
  //       else{
  //         this.presentPregnancyFollowUpSelection = [];
  //       }
  // }

  // returnSelected(): void {
  //   this.dialogRef.close(this.presentPregnancyFollowUpSelection);
  // }

  // onCancel(): void {
  //   this.dialogRef.close();
  // }
  }