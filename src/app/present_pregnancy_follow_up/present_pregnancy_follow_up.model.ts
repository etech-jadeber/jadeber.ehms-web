import {TCId} from "../tc/models";
export class PresentPregnancyFollowUpSummary extends TCId {
    visit_status:number;
    date_of_visit:number;
    gestation_age:number;
    bp:number;
    weight:number;
    pallor:string;
    uterine_height:number;
    fatal_heart_beat:number;
    presentation:string;
    urine_test_protein:string;
    repid_syphilis_test:string;
    hemoglobin:string;
    blood_group_and_rh:string;
    tt:number;
    iron_folic_add:string;
    mebendazole:string;
    use_of_itn:string;
    arv_px:string;
    remarks:string;
    danger_signs_identified_and_investigation:string;
    action_advice_counseling:string;
    appointment_for_next_follow_up:string;
    name_and_signs_of_health_care_provider:string;
    urine_test_for_infection:string;
     
    }
    export class patientPrescriptionWithPregnancy{
      name: string;
      id: string;
      medication: PresentPregnancyFollowUpSummary[];
  }
    export class PresentPregnancyFollowUpSummaryPartialList {
      data: PresentPregnancyFollowUpSummary[];
      total: number;
    }
    export class PresentPregnancyFollowUpDetail extends PresentPregnancyFollowUpSummary {
sbp: any;
dbp: any;
    }
    
    export class PresentPregnancyFollowUpDashboard {
      total: number;
    }