import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {PresentPregnancyFollowUpEditComponent} from "./present-pregnancy-follow-up-edit/present-pregnancy-follow-up-edit.component";
import {PresentPregnancyFollowUpPickComponent} from "./present-pregnancy-follow-up-pick/present-pregnancy-follow-up-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PresentPregnancyFollowUpNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  presentPregnancyFollowUpsUrl(): string {
    return "/present_pregnancy_follow_ups";
  }

  presentPregnancyFollowUpUrl(id: string): string {
    return "/present_pregnancy_follow_ups/" + id;
  }

  viewPresentPregnancyFollowUps(): void {
    this.router.navigateByUrl(this.presentPregnancyFollowUpsUrl());
  }

  viewPresentPregnancyFollowUp(id: string): void {
    this.router.navigateByUrl(this.presentPregnancyFollowUpUrl(id));
  }

  editPresentPregnancyFollowUp(id: string): MatDialogRef<PresentPregnancyFollowUpEditComponent> {
    const dialogRef = this.dialog.open(PresentPregnancyFollowUpEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addPresentPregnancyFollowUp(encounterId:string,visits:number): MatDialogRef<PresentPregnancyFollowUpEditComponent> {
    const dialogRef = this.dialog.open(PresentPregnancyFollowUpEditComponent, {
          data: {id : encounterId, mode:visits?TCModalModes.CASE:TCModalModes.NEW, visits:visits},
          width: TCModalWidths.large
    });
    return dialogRef;
  }

  // secondaryAddPresentPregnancyFollowUp(encounterId:string,exists:boolean): MatDialogRef<PresentPregnancyFollowUpEditComponent> {
  //   const dialogRef = this.dialog.open(PresentPregnancyFollowUpEditComponent, {
  //         data: new TCIdMode(encounterId, TCModalModes.CASE),
  //         width: TCModalWidths.medium
  //   });
  //   return dialogRef;
  // }
  
   pickPresentPregnancyFollowUps(selectOne: boolean=false): MatDialogRef<PresentPregnancyFollowUpPickComponent> {
      const dialogRef = this.dialog.open(PresentPregnancyFollowUpPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}