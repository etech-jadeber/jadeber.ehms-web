import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PresentPregnancyFollowUpDashboard, PresentPregnancyFollowUpDetail, PresentPregnancyFollowUpSummaryPartialList} from "./present_pregnancy_follow_up.model";
import { MaritalStatus, Status, VisitStatus } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class PresentPregnancyFollowUpPersist {
    maritalStatusFilter: number ;

    maritalStatus: TCEnum[] = [
     new TCEnum( MaritalStatus.single, 'single'),
  new TCEnum( MaritalStatus.married, 'married'),
  new TCEnum( MaritalStatus.widowed, 'widowed'),
  new TCEnum( MaritalStatus.divorced, 'divorced'),
  new TCEnum( MaritalStatus.separated, 'separated'),

  ];

  level ={
    1:"1st",
    2:"2nd",
    3:"3rd",
  };

  getLevel(num: number):string{ 
    return (this.level[num]|| num +"th") + " Visit";
  }

  visitStatusFilter: number ;

    visitStatus: TCEnum[] = [
     new TCEnum( VisitStatus.firstVisit, '1st Visit (better before 16 wks)'),
  new TCEnum( VisitStatus.secondVisit, '2nd Visit (better 24-28 wks)'),
  new TCEnum( VisitStatus.thirdVisit, '3rd Visit (better 30-32 wks)'),
  new TCEnum( VisitStatus.fourthVisit, '4th Visit ( better 36-40 wks)'),

  ];

  statusFilter: number ;

    Status: TCEnum[] = [
     new TCEnum( Status.incompleted, 'incompleted'),
  new TCEnum( Status.completed, 'completed'),

  ];


 presentPregnancyFollowUpSearchText: string = "";
 
constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.presentPregnancyFollowUpSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPresentPregnancyFollowUp(parentId:string,pageSize: number, pageIndex: number, sort: string, order?: string): Observable<PresentPregnancyFollowUpSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("anc_history/" + parentId + "/present_pregnancy_follow_up", this.presentPregnancyFollowUpSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<PresentPregnancyFollowUpSummaryPartialList>(url);

  } 
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "present_pregnancy_follow_up/do", new TCDoParam("download_all", this.filters()));
  }

  presentPregnancyFollowUpDashboard(): Observable<PresentPregnancyFollowUpDashboard> {
    return this.http.get<PresentPregnancyFollowUpDashboard>(environment.tcApiBaseUri + "present_pregnancy_follow_up/dashboard");
  }

  getPresentPregnancyFollowUp(id: string): Observable<PresentPregnancyFollowUpDetail> {
    return this.http.get<PresentPregnancyFollowUpDetail>(environment.tcApiBaseUri + "present_pregnancy_follow_up/" + id);
  } 
  addPresentPregnancyFollowUp(parentId:string,item: PresentPregnancyFollowUpDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "form_encounters/" + parentId + "/present_pregnancy_follow_up/", item);
  }

  updatePresentPregnancyFollowUp(item: PresentPregnancyFollowUpDetail): Observable<PresentPregnancyFollowUpDetail> {
    return this.http.patch<PresentPregnancyFollowUpDetail>(environment.tcApiBaseUri + "present_pregnancy_follow_up/" + item.id, item);
  }

  deletePresentPregnancyFollowUp(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "present_pregnancy_follow_up/" + id);
  }
 presentPregnancyFollowUpsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "present_pregnancy_follow_up/do", new TCDoParam(method, payload));
  }

  presentPregnancyFollowUpDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "present_pregnancy_follow_ups/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "present_pregnancy_follow_up/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "present_pregnancy_follow_up/" + id + "/do", new TCDoParam("print", {}));
  }


}