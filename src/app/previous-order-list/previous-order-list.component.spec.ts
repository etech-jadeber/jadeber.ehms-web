import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousOrderListComponent } from './previous-order-list.component';

describe('PreviousOrderListComponent', () => {
  let component: PreviousOrderListComponent;
  let fixture: ComponentFixture<PreviousOrderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviousOrderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
