import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { OrdersDetail, OrdersSummary, Selected_OrdersDetail, Selected_OrdersSummaryPartialList } from '../form_encounters/orderss/orders.model';
import { OrdersPersist } from '../form_encounters/orderss/orders.persist';
import { Lab_PanelDetail, Order, Test_PanelSummary } from '../lab_panels/lab_panel.model';
import { Lab_PanelPersist } from '../lab_panels/lab_panel.persist';
import { Lab_TestDetail } from '../lab_tests/lab_test.model';
import { Lab_TestPersist } from '../lab_tests/lab_test.persist';
import { TCAuthorization } from '../tc/authorization';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import { TCUtilsString } from '../tc/utils-string';

@Component({
  selector: 'app-previous-order-list',
  templateUrl: './previous-order-list.component.html',
  styleUrls: ['./previous-order-list.component.scss']
})
export class PreviousOrderListComponent implements OnInit {

  @Input() encounterId: string;
  ordersDetail: OrdersSummary[] = [];
  ordersTotalCount: number = 0;
  panels: {[id: string]: Lab_PanelDetail}= {}
  tests: {[id: string]: Lab_TestDetail}= {}
  isLoading: boolean = false;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  constructor(
    public ordersPersist: OrdersPersist,
    public tcUtilsArray: TCUtilsArray,
    public lab_panelPersist: Lab_PanelPersist,
    public lab_testPersist: Lab_TestPersist,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public formEncounterNavigator: Form_EncounterNavigator,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator
  ) { }

  ngOnInit(): void {
    this.paginator.page.subscribe(() => {
      this.getOrders(true);
    });
    // this.getAllLabTestPanels()
    this.getOrders();
  }

  getOrders(isPagination: boolean = false){
    let paginator = this.paginator;
    this.ordersPersist.searchOrders(this.encounterId, paginator.pageSize,
      isPagination ? paginator.pageIndex : 0,
      'created_at',
      'desc').subscribe(
        (orders) => {
          this.ordersDetail = orders.data;
          
          if (orders.total != -1){
            this.ordersTotalCount = orders.total
          }
        }
      )

  }

  getDetail(order: OrdersDetail){
      !order.selected_orders && this.ordersPersist.selected_orderssDo(order.id, "get_by_order", {}).subscribe(
        (selected_order: Selected_OrdersSummaryPartialList) => {
          order.selected_orders = selected_order.data;
          selected_order.data.forEach(
            selected => {
              if (!this.panels[selected.lab_panel_id]){
                this.panels[selected.lab_panel_id] = new Lab_PanelDetail()
                selected.lab_panel_id != this.tcUtilsString.invalid_id && this.lab_panelPersist.getLab_Panel(selected.lab_panel_id).subscribe(
                  panel => {
                    this.panels[selected.lab_panel_id] = panel;
                  }
                )
              }
              if (!this.tests[selected.lab_test_id]){
                this.tests[selected.lab_test_id] = new Lab_TestDetail()
                selected.lab_test_id != this.tcUtilsString.invalid_id && this.lab_testPersist.getLab_Test(selected.lab_test_id).subscribe(
                  test => {
                    this.tests[selected.lab_test_id] = test;
                  }
                )
              }
            }
          )
        }
      )
  }

  filterOrders(selectedOrders: Selected_OrdersDetail[], key: string){
    return selectedOrders?.filter(selected => selected[key] != this.tcUtilsString.invalid_id)
  }

  getlabTest(lab_test_id: string){
    return this.tests[lab_test_id]?.name
  }

  getlabPanel(lab_panel_id: string){
    return this.panels[lab_panel_id]?.name
  }

  addOrder(
    orderType: number[],
    order: OrdersDetail,
    isWizard: boolean = true,
    date: number = this.tcUtilsDate.toTimeStamp(new Date()),
    
  ): void {
      let dialogRef = this.formEncounterNavigator.addLab(
        isWizard,
        null,
        null,
        date,
        orderType,
        order.id,
        order.selected_orders
      );
      dialogRef.afterClosed().subscribe((result: Order[]) => {
        if (result) {
         this.tcNotification.success("Order is added")
         this.getOrders()
          // this.persist.addOutside_Orders({...order, orders: result}).subscribe(value => {
          //   this.outside_ordersDetail.id = value.id;
          //     this.tcNotification.success('Successfully Added');
          // }
          // , error => {
          //   this.isLoading = false;
          //   console.error(error)
          // })
        }
      });
  }

  deleteSelected(id: string){
    let dialogRef = this.tcNavigator.confirmDeletion("additional_service");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.ordersPersist.deleteSelected_Orders(id).subscribe((value) => {
          this.tcNotification.success("The order is deleted successfully")
          this.getOrders()
        });
      }

    });
    
  }

}
