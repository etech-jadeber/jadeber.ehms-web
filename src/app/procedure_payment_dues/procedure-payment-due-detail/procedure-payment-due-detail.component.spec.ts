import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedurePaymentDueDetailComponent } from './procedure-payment-due-detail.component';

describe('ProcedurePaymentDueDetailComponent', () => {
  let component: ProcedurePaymentDueDetailComponent;
  let fixture: ComponentFixture<ProcedurePaymentDueDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurePaymentDueDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurePaymentDueDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
