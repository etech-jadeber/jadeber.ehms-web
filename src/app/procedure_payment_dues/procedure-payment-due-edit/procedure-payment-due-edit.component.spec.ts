import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedurePaymentDueEditComponent } from './procedure-payment-due-edit.component';

describe('ProcedurePaymentDueEditComponent', () => {
  let component: ProcedurePaymentDueEditComponent;
  let fixture: ComponentFixture<ProcedurePaymentDueEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurePaymentDueEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurePaymentDueEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
