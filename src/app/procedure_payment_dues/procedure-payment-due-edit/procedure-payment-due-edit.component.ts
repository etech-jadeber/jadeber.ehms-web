import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Procedure_Payment_DueDetail} from "../procedure_payment_due.model";
import {Procedure_Payment_DuePersist} from "../procedure_payment_due.persist";


@Component({
  selector: 'app-procedure_payment_due-edit',
  templateUrl: './procedure-payment-due-edit.component.html',
  styleUrls: ['./procedure-payment-due-edit.component.css']
})
export class Procedure_Payment_DueEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedure_payment_dueDetail: Procedure_Payment_DueDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Procedure_Payment_DueEditComponent>,
              public  persist: Procedure_Payment_DuePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("procedure_payment_dues");
      this.title = this.appTranslation.getText("general","new") +  " " +  this.appTranslation.getText("procedure", "procedure_payment_due");
      this.procedure_payment_dueDetail = new Procedure_Payment_DueDetail();
      this.procedure_payment_dueDetail.status = -1;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("procedure_payment_dues");
      this.title = this.appTranslation.getText("general","edit") +  " "  +  this.appTranslation.getText("procedure", "procedure_payment_due");;
      this.isLoadingResults = true;
      this.persist.getProcedure_Payment_Due(this.idMode.id).subscribe(procedure_payment_dueDetail => {
        this.procedure_payment_dueDetail = procedure_payment_dueDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addProcedure_Payment_Due(this.procedure_payment_dueDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Payment_Due added");
      this.procedure_payment_dueDetail.id = value.id;
      this.dialogRef.close(this.procedure_payment_dueDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateProcedure_Payment_Due(this.procedure_payment_dueDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Payment_Due updated");
      this.dialogRef.close(this.procedure_payment_dueDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.procedure_payment_dueDetail == null){
            return false;
          }

        if (this.procedure_payment_dueDetail.status == null || this.procedure_payment_dueDetail.status  == -1) {
                      return false;
                    }


        return true;
      }


}
