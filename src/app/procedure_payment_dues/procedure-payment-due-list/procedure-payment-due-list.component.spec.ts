import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedurePaymentDueListComponent } from './procedure-payment-due-list.component';

describe('ProcedurePaymentDueListComponent', () => {
  let component: ProcedurePaymentDueListComponent;
  let fixture: ComponentFixture<ProcedurePaymentDueListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurePaymentDueListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurePaymentDueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
