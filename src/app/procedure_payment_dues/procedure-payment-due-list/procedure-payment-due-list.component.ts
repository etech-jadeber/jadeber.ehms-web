import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Procedure_Payment_DuePersist} from "../procedure_payment_due.persist";
import {Procedure_Payment_DueNavigator} from "../procedure_payment_due.navigator";
import {Procedure_Payment_DueDetail, Procedure_Payment_DueSummary, Procedure_Payment_DueSummaryPartialList} from "../procedure_payment_due.model";
import { procedure_status } from 'src/app/app.enums';


@Component({
  selector: 'app-procedure_payment_due-list',
  templateUrl: './procedure-payment-due-list.component.html',
  styleUrls: ['./procedure-payment-due-list.component.css']
})
export class Procedure_Payment_DueListComponent implements OnInit {

  procedure_payment_duesData: Procedure_Payment_DueSummary[] = [];
  procedure_status:typeof procedure_status = procedure_status
  procedure_payment_duesTotalCount: number = 0;
  procedure_payment_duesSelectAll:boolean = false;
  procedure_payment_duesSelection: Procedure_Payment_DueSummary[] = [];

  procedure_payment_duesDisplayedColumns: string[] = ["select","action","procedure_id","status","patient_id", "amount"];
  procedure_payment_duesSearchTextBox: FormControl = new FormControl()
  procedure_payment_duesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_payment_duesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_payment_duesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedure_payment_duePersist: Procedure_Payment_DuePersist,
                public procedure_payment_dueNavigator: Procedure_Payment_DueNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("procedure_payment_dues");
       this.procedure_payment_duesSearchTextBox.setValue(procedure_payment_duePersist.procedure_payment_dueSearchText);
      //delay subsequent keyup events
      this.procedure_payment_duesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedure_payment_duePersist.procedure_payment_dueSearchText = value;
        this.searchProcedure_Payment_Dues();
      });
    }

    ngOnInit() {

      this.procedure_payment_duesSort.sortChange.subscribe(() => {
        this.procedure_payment_duesPaginator.pageIndex = 0;
        this.searchProcedure_Payment_Dues(true);
      });

      this.procedure_payment_duesPaginator.page.subscribe(() => {
        this.searchProcedure_Payment_Dues(true);
      });
      //start by loading items
      this.searchProcedure_Payment_Dues();
    }

  searchProcedure_Payment_Dues(isPagination:boolean = false): void {


    let paginator = this.procedure_payment_duesPaginator;
    let sorter = this.procedure_payment_duesSort;

    this.procedure_payment_duesIsLoading = true;
    this.procedure_payment_duesSelection = [];

    this.procedure_payment_duePersist.searchProcedure_Payment_Due(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Procedure_Payment_DueSummaryPartialList) => {
      this.procedure_payment_duesData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_payment_duesTotalCount = partialList.total;
      }
      this.procedure_payment_duesIsLoading = false;
    }, error => {
      this.procedure_payment_duesIsLoading = false;
    });

  }

  downloadProcedure_Payment_Dues(): void {
    if(this.procedure_payment_duesSelectAll){
         this.procedure_payment_duePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_payment_dues", true);
      });
    }
    else{
        this.procedure_payment_duePersist.download(this.tcUtilsArray.idsList(this.procedure_payment_duesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download procedure_payment_dues",true);
            });
        }
  }



  addProcedure_Payment_Due(): void {
    let dialogRef = this.procedure_payment_dueNavigator.addProcedure_Payment_Due();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Payment_Dues();
      }
    });
  }

  editProcedure_Payment_Due(item: Procedure_Payment_DueSummary) {
    let dialogRef = this.procedure_payment_dueNavigator.editProcedure_Payment_Due(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteProcedure_Payment_Due(item: Procedure_Payment_DueSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Payment_Due");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedure_payment_duePersist.deleteProcedure_Payment_Due(item.id).subscribe(response => {
          this.tcNotification.success("Procedure_Payment_Due deleted");
          this.searchProcedure_Payment_Dues();
        }, error => {
        });
      }

    });
  }

  payNow(due:Procedure_Payment_DueSummary):void{
    this.procedure_payment_duePersist.procedure_payment_dueDo(due.id, "pay_now", due).subscribe(
      res=>{
        this.tcNotification.success("Succesfull paid");
        this.searchProcedure_Payment_Dues();
      }

    )
    
  }


  back():void{
      this.location.back();
    }

}
