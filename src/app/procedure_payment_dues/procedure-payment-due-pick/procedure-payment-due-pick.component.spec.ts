import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedurePaymentDuePickComponent } from './procedure-payment-due-pick.component';

describe('ProcedurePaymentDuePickComponent', () => {
  let component: ProcedurePaymentDuePickComponent;
  let fixture: ComponentFixture<ProcedurePaymentDuePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurePaymentDuePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurePaymentDuePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
