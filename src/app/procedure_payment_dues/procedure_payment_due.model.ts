import {TCId} from "../tc/models";

export class Procedure_Payment_DueSummary extends TCId {
  procedure_tests_id : string;
procedure_id : string;
status : number;
patient_id : string;
sequence:number;
amount:number
patient_name:string;
}

export class Procedure_Payment_DueSummaryPartialList {
  data: Procedure_Payment_DueSummary[];
  total: number;
}

export class Procedure_Payment_DueDetail extends Procedure_Payment_DueSummary {

}

export class Procedure_Payment_DueDashboard {
  total: number;
}
