import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import { Procedure_Payment_DueEditComponent } from "./procedure-payment-due-edit/procedure-payment-due-edit.component";
import { ProcedurePaymentDuePickComponent } from "./procedure-payment-due-pick/procedure-payment-due-pick.component";

@Injectable({
  providedIn: 'root'
})

export class Procedure_Payment_DueNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  procedure_payment_duesUrl(): string {
    return "/procedure_payment_dues";
  }

  procedure_payment_dueUrl(id: string): string {
    return "/procedure_payment_dues/" + id;
  }

  viewProcedure_Payment_Dues(): void {
    this.router.navigateByUrl(this.procedure_payment_duesUrl());
  }

  viewProcedure_Payment_Due(id: string): void {
    this.router.navigateByUrl(this.procedure_payment_dueUrl(id));
  }

  editProcedure_Payment_Due(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_Payment_DueEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure_Payment_Due(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_Payment_DueEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickProcedure_Payment_Dues(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(ProcedurePaymentDuePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
