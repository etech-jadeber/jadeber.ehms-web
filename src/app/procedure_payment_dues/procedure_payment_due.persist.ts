import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {Procedure_Payment_DueDashboard, Procedure_Payment_DueDetail, Procedure_Payment_DueSummaryPartialList} from "./procedure_payment_due.model";
import { AppTranslation } from '../app.translation';
import { procedure_status } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class Procedure_Payment_DuePersist {

  procedure_payment_dueSearchText: string = "";

  constructor(private http: HttpClient, private appTranslation:AppTranslation) {
  }

  searchProcedure_Payment_Due(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_Payment_DueSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_payment_dues", this.procedure_payment_dueSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Procedure_Payment_DueSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_payment_dueSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_payment_dues/do", new TCDoParam("download_all", this.filters()));
  }

  procedure_payment_dueDashboard(): Observable<Procedure_Payment_DueDashboard> {
    return this.http.get<Procedure_Payment_DueDashboard>(environment.tcApiBaseUri + "procedure_payment_dues/dashboard");
  }

  getProcedure_Payment_Due(id: string): Observable<Procedure_Payment_DueDetail> {
    return this.http.get<Procedure_Payment_DueDetail>(environment.tcApiBaseUri + "procedure_payment_dues/" + id);
  }

  addProcedure_Payment_Due(item: Procedure_Payment_DueDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_payment_dues/", item);
  }

  updateProcedure_Payment_Due(item: Procedure_Payment_DueDetail): Observable<Procedure_Payment_DueDetail> {
    return this.http.patch<Procedure_Payment_DueDetail>(environment.tcApiBaseUri + "procedure_payment_dues/" + item.id, item);
  }

  deleteProcedure_Payment_Due(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "procedure_payment_dues/" + id);
  }

  procedure_payment_duesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_payment_dues/do", new TCDoParam(method, payload));
  }

  procedure_payment_dueDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_payment_dues/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_payment_dues/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_payment_dues/" + id + "/do", new TCDoParam("print", {}));
  }

  procedure_statuId: number ;
    
    procedure_status: TCEnumTranslation[] = [
    new TCEnumTranslation(procedure_status.free, this.appTranslation.getKey('general', 'free')),
    new TCEnumTranslation(procedure_status.paid, this.appTranslation.getKey('financial', 'paid')),
    new TCEnumTranslation(procedure_status.not_paid, this.appTranslation.getKey('financial', 'not_paid')),
    // new TCEnumTranslation(procedure_status.credit, this.appTranslation.getKey('financial', 'credit')),
    new TCEnumTranslation(procedure_status.referal, this.appTranslation.getKey('financial', 'referral')),
  ];


}
