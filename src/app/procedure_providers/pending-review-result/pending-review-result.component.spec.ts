import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PendingReviewResultComponent } from './pending-review-result.component';

describe('PendingReviewResultComponent', () => {
  let component: PendingReviewResultComponent;
  let fixture: ComponentFixture<PendingReviewResultComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingReviewResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingReviewResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
