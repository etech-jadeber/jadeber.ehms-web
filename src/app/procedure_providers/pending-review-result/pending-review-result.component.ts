import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Procedure_ProviderEditComponent} from "../procedure-provider-edit/procedure-provider-edit.component";
import {AppTranslation} from "../../app.translation";
import {PendingReview} from "../procedure_provider.model";
import {ProcedureStatus} from "../../form_encounters/procedure_orders/procedure_order.persist";
import {Procedure_Order_CodeDetail} from "../../form_encounters/procedure_order_codes/procedure_order_code.model";
import {ReportResultUnion} from "../procedure_report.model";
import {Procedure_resultPersist} from "./procedure_result.persist";
import {TCUtilsDate} from "../../tc/utils-date";

@Component({
  selector: 'app-pending-review-result',
  templateUrl: './pending-review-result.component.html',
  styleUrls: ['./pending-review-result.component.css']
})
export class PendingReviewResultComponent implements OnInit {

  status: ProcedureStatus[] = [
    {value: "final", name: this.appTranslation.getText("procedure", "final")},
    {value: "review", name: this.appTranslation.getText("procedure", "reviewed")},
    {value: "prelim", name: this.appTranslation.getText("procedure", "preliminary")},
    {value: "cancel", name: this.appTranslation.getText("procedure", "canceled")},
    {value: "error", name: this.appTranslation.getText("procedure", "error")},
    {value: "correct", name: this.appTranslation.getText("procedure", "corrected")},
  ]

  abns: ProcedureStatus[] = [
    {value: "no", name: this.appTranslation.getText("general", "no")},
    {value: "yes", name: this.appTranslation.getText("general", "yes")},
    {value: "high", name: this.appTranslation.getText("procedure", "high")},
    {value: "low", name: this.appTranslation.getText("procedure", "low")},
    {value: "vhigh", name: this.appTranslation.getText("procedure", "above_upper_panic_limits")},
    {value: "vlow", name: this.appTranslation.getText("procedure", "below_lower_panic_limits")},
  ];

  procedure_order_code: Procedure_Order_CodeDetail;

  reportResultUnion: ReportResultUnion;

  constructor(@Inject(MAT_DIALOG_DATA) public data: PendingReview,
              public appTranslation: AppTranslation,
              public tcUtilsDate: TCUtilsDate,
              private procedureResultPersist: Procedure_resultPersist,
              public dialogRef: MatDialogRef<Procedure_ProviderEditComponent>,) {
    this.procedure_order_code = this.data.procedure_order_code[0];
    this.reportResultUnion = new ReportResultUnion();
    this.reportResultUnion.procedure_order_id = this.procedure_order_code.procedure_order_id;
    this.reportResultUnion.procedure_order_seq = this.procedure_order_code.procedure_order_seq;
    this.reportResultUnion.date_collected = this.data.date_ordered;
    this.reportResultUnion.result_code = this.procedure_order_code.procedure_code;
  }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onAdd() {
    this.procedureResultPersist.addProcedure_ResultAndReport(this.reportResultUnion).subscribe(
      result => {
        this.dialogRef.close(result);
      }, error => {
        console.error(error);
      }
    );
  }

  canSubmit() {

    if (this.reportResultUnion.specimen_num == null) {
      return false;
    }

    if (this.reportResultUnion.result_status == null) {
      return false;
    }
    if (this.reportResultUnion.result_code == null) {
      return false;
    }

    if (this.reportResultUnion.abnormal == null) {
      return false;
    }

    if (this.reportResultUnion.result == null) {
      return false;
    }

    if (this.reportResultUnion.units == null) {
      return false;
    }

    if (this.reportResultUnion.range == null) {
      return false;
    }

    if (this.reportResultUnion.result_notes == null) {
      return false;
    }

    if (this.reportResultUnion.comments == null) {
      return false;
    }

    return true;
  }

  getDate(date_collected) {
    return this.tcUtilsDate.toDate(date_collected).toDateString();
  }
}
