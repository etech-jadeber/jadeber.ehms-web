import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ReportResultUnion} from "../procedure_report.model";
import {Observable} from "rxjs";
import {TCId} from "../../tc/models";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class Procedure_resultPersist {

  constructor(private http: HttpClient) {
  }

  /**
   * Adds both procedure result and procedure report to the database
   * @param item
   */
  addProcedure_ResultAndReport(item: ReportResultUnion): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_results/", item);
  }

  getProcedureOrderResult(procedure_order_seq: string, procedure_order_id: string) {
    return this.http.get<ReportResultUnion>(environment.tcApiBaseUri + "procedure_results/" + procedure_order_seq + "/" + procedure_order_id);
  }

}
