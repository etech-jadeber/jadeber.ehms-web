import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNotification} from "../../tc/notification";
import {AppTranslation} from "../../app.translation";
import {Location} from "@angular/common";
import {Procedure_ProviderPersist} from "../procedure_provider.persist";
import {PendingReview} from "../procedure_provider.model";
import {Procedure_ProviderNavigator} from "../procedure_provider.navigator";
import {TCUtilsDate} from "../../tc/utils-date";
import {PatientPersist} from "../../patients/patients.persist";


@Component({
  selector: 'app-pending-review',
  templateUrl: './pending-review.component.html',
  styleUrls: ['./pending-review.component.css']
})
export class PendingReviewComponent implements OnInit, AfterViewInit {

  @Input() labId: string;

  pendingReviewData: PendingReview[] = [];
  displayedColumns: string[] = ["diagnoses", "procedure_code", "procedure_name", "procedure_order_title", "procedure_type", "action"];
  patients = [];

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              private location: Location,
              private patientPersist: PatientPersist,
              private procedureProviderNavigator: Procedure_ProviderNavigator,
              public appTranslation: AppTranslation,
              public procedure_providerPersist: Procedure_ProviderPersist,
              public tcNotification: TCNotification,) {
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_providers");
    this.loadProcedureOrderCodes(this.labId.toString());
  }

  ngAfterViewInit(): void {

  }

  getAndSavePatients(patientId: string) {
    this.patientPersist.getPatient(patientId).subscribe(value => {
      this.patients.push(value);
    })
  }

  reload() {
    this.loadProcedureOrderCodes(this.labId.toString());
  }

  private loadProcedureOrderCodes(id: string) {
    this.procedure_providerPersist.getProcedureOrderCodes(id).subscribe(result => {
      this.pendingReviewData = result;

      result.forEach(value => {
        this.getAndSavePatients(value.patient_id);
      })
    }, error => {
      console.error(error);
    });
  }

  back(): void {
    this.location.back();
  }

  openReviewDialog(element, patientId: string, date_ordered: number, is_paid: boolean = false) {
    const dataToSend: PendingReview = {
      procedure_order_code: [element],
      patient_id: patientId,
      is_paid,
      date_ordered: date_ordered
    };
    const dialogRef = this.procedureProviderNavigator.addProcedureOrderResult(dataToSend);

    dialogRef.afterClosed().subscribe(
      result => {
        this.loadProcedureOrderCodes(this.labId.toString());
      },
      error => {
        console.error(error);
      }
    );

  }

  getPatientFullName(patient_id: string) {
    const patient = this.tcUtilsArray.getById(this.patients, patient_id);
    if (patient) {
      return patient.fname + " " + patient.mname;
    }
  }
}
