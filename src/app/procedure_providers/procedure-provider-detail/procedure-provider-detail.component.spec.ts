import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureProviderDetailComponent } from './procedure-provider-detail.component';

describe('ProcedureProviderDetailComponent', () => {
  let component: ProcedureProviderDetailComponent;
  let fixture: ComponentFixture<ProcedureProviderDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureProviderDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureProviderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
