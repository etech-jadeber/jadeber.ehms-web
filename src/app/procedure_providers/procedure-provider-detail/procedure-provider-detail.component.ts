import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Procedure_ProviderDetail} from "../procedure_provider.model";
import {Procedure_ProviderPersist} from "../procedure_provider.persist";
import {Procedure_ProviderNavigator} from "../procedure_provider.navigator";
import {MenuState} from "../../global-state-manager/global-state";
import {tabs} from "../../app.enums";

export enum Procedure_ProviderTabs {
  overview,
  pending_review
}

export enum PaginatorIndexes {

}

@Component({
  selector: 'app-procedure_provider-detail',
  templateUrl: './procedure-provider-detail.component.html',
  styleUrls: ['./procedure-provider-detail.component.css']
})
export class Procedure_ProviderDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  procedure_providerLoading: boolean = false;

  Procedure_ProviderTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;
  //basics
  procedure_providerDetail: Procedure_ProviderDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public jobPersist: JobPersist,
              public appTranslation: AppTranslation,
              public menuState: MenuState,
              public procedure_providerNavigator: Procedure_ProviderNavigator,
              public procedure_providerPersist: Procedure_ProviderPersist) {
    this.tcAuthorization.requireRead("procedure_providers");
    this.procedure_providerDetail = new Procedure_ProviderDetail();

    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_providers");
    this.activeTab = tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.procedure_providerLoading = true;
    this.procedure_providerPersist.getProcedure_Provider(id).subscribe(procedure_providerDetail => {
      this.procedure_providerDetail = procedure_providerDetail;
      this.procedure_providerLoading = false;
      this.menuState.getDataResponse(this.procedure_providerDetail);
    }, error => {
      console.error(error);
      this.procedure_providerLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.procedure_providerDetail.id);
  }

  editProcedure_Provider(): void {
    let modalRef = this.procedure_providerNavigator.editProcedure_Provider(this.procedure_providerDetail.id.toString());
    modalRef.afterClosed().subscribe(modifiedProcedure_ProviderDetail => {
      TCUtilsAngular.assign(this.procedure_providerDetail, modifiedProcedure_ProviderDetail);
    }, error => {
      console.error(error);
    });
  }

  printProcedure_Provider(): void {
    this.procedure_providerPersist.print(this.procedure_providerDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print procedure_provider", true);
    });
  }

  back(): void {
    this.location.back();
  }


}
