import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureProviderEditComponent } from './procedure-provider-edit.component';

describe('ProcedureProviderEditComponent', () => {
  let component: ProcedureProviderEditComponent;
  let fixture: ComponentFixture<ProcedureProviderEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureProviderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureProviderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
