import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Procedure_ProviderDetail} from "../procedure_provider.model";
import {Procedure_ProviderPersist} from "../procedure_provider.persist";


@Component({
  selector: 'app-procedure_provider-edit',
  templateUrl: './procedure-provider-edit.component.html',
  styleUrls: ['./procedure-provider-edit.component.css']
})
export class Procedure_ProviderEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedure_providerDetail: Procedure_ProviderDetail;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<Procedure_ProviderEditComponent>,
              public persist: Procedure_ProviderPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("procedure_providers");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("procedure", "procedure_provider");
      this.procedure_providerDetail = new Procedure_ProviderDetail();
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("procedure_providers");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("procedure", "procedure_provider");
      this.isLoadingResults = true;
      this.persist.getProcedure_Provider(this.idMode.id).subscribe(procedure_providerDetail => {
        this.procedure_providerDetail = procedure_providerDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addProcedure_Provider(this.procedure_providerDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Provider added");
      this.procedure_providerDetail.id = value.id;
      this.dialogRef.close(this.procedure_providerDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateProcedure_Provider(this.procedure_providerDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Provider updated");
      this.dialogRef.close(this.procedure_providerDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.procedure_providerDetail == null) {
      return false;
    }

    if (this.procedure_providerDetail.notes == null || this.procedure_providerDetail.notes == "") {
      return false;
    }

    if (this.procedure_providerDetail.type == null || this.procedure_providerDetail.type == "") {
      return false;
    }

    return true;
  }


}
