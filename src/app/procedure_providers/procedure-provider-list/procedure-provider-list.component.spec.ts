import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import {Procedure_ProviderListComponent} from "./procedure-provider-list.component";


describe('ProcedureProviderListComponent', () => {
  let component: Procedure_ProviderListComponent;
  let fixture: ComponentFixture<Procedure_ProviderListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Procedure_ProviderListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Procedure_ProviderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
