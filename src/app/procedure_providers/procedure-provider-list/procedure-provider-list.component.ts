import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Procedure_ProviderNavigator} from "../procedure_provider.navigator";
import {Procedure_ProviderSummary, Procedure_ProviderSummaryPartialList} from "../procedure_provider.model";
import {Procedure_ProviderPersist} from "../procedure_provider.persist";


@Component({
  selector: 'app-procedure_provider-list',
  templateUrl: './procedure-provider-list.component.html',
  styleUrls: ['./procedure-provider-list.component.css']
})
export class Procedure_ProviderListComponent implements OnInit {

  procedure_providersData: Procedure_ProviderSummary[] = [];
  procedure_providersTotalCount: number = 0;
  procedure_providersSelectAll: boolean = false;
  procedure_providersSelection: Procedure_ProviderSummary[] = [];

  procedure_providersDisplayedColumns: string[] = ["select", "action", "name", "notes", "active", "type",];
  procedure_providersSearchTextBox: FormControl = new FormControl();
  procedure_providersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_providersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_providersSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public procedure_providerPersist: Procedure_ProviderPersist,
              public procedure_providerNavigator: Procedure_ProviderNavigator,
              public jobPersist: JobPersist,
  ) {

    this.tcAuthorization.requireRead("procedure_providers");
    this.procedure_providersSearchTextBox.setValue(procedure_providerPersist.procedure_providerSearchText);
    //delay subsequent keyup events
    this.procedure_providersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_providerPersist.procedure_providerSearchText = value;
      this.searchProcedure_Providers();
    });
  }

  ngOnInit() {

    this.procedure_providersSort.sortChange.subscribe(() => {
      this.procedure_providersPaginator.pageIndex = 0;
      this.searchProcedure_Providers(true);
    });

    this.procedure_providersPaginator.page.subscribe(() => {
      this.searchProcedure_Providers(true);
    });
    //start by loading items
    this.searchProcedure_Providers();
  }

  searchProcedure_Providers(isPagination: boolean = false): void {


    let paginator = this.procedure_providersPaginator;
    let sorter = this.procedure_providersSort;

    this.procedure_providersIsLoading = true;
    this.procedure_providersSelection = [];

    this.procedure_providerPersist.searchProcedure_Provider(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: Procedure_ProviderSummaryPartialList) => {
      this.procedure_providersData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_providersTotalCount = partialList.total;
      }
      this.procedure_providersIsLoading = false;
    }, error => {
      this.procedure_providersIsLoading = false;
    });

  }

  downloadProcedure_Providers(): void {
    if (this.procedure_providersSelectAll) {
      this.procedure_providerPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_providers", true);
      });
    } else {
      this.procedure_providerPersist.download(this.tcUtilsArray.idsList(this.procedure_providersSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_providers", true);
      });
    }
  }


  addProcedure_Provider(): void {
    let dialogRef = this.procedure_providerNavigator.addProcedure_Provider();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Providers();
      }
    });
  }

  editProcedure_Provider(item: Procedure_ProviderSummary) {
    let dialogRef = this.procedure_providerNavigator.editProcedure_Provider(item.id.toString());
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteProcedure_Provider(item: Procedure_ProviderSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Provider");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedure_providerPersist.deleteProcedure_Provider(item.id.toString()).subscribe(response => {
          this.tcNotification.success("Procedure_Provider deleted");
          this.searchProcedure_Providers();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

}
