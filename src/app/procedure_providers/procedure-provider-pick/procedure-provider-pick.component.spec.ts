import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureProviderPickComponent } from './procedure-provider-pick.component';

describe('ProcedureProviderPickComponent', () => {
  let component: ProcedureProviderPickComponent;
  let fixture: ComponentFixture<ProcedureProviderPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureProviderPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureProviderPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
