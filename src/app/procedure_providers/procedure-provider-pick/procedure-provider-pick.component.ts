import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Procedure_ProviderSummary, Procedure_ProviderSummaryPartialList} from "../procedure_provider.model";
import {Procedure_ProviderPersist} from "../procedure_provider.persist";


@Component({
  selector: 'app-procedure_provider-pick',
  templateUrl: './procedure-provider-pick.component.html',
  styleUrls: ['./procedure-provider-pick.component.css']
})
export class Procedure_ProviderPickComponent implements OnInit {

  procedure_providersData: Procedure_ProviderSummary[] = [];
  procedure_providersTotalCount: number = 0;
  procedure_providersSelection: Procedure_ProviderSummary[] = [];
  procedure_providersDisplayedColumns: string[] = ["select", 'name', 'notes', 'active', 'type'];

  procedure_providersSearchTextBox: FormControl = new FormControl();
  procedure_providersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_providersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_providersSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public procedure_providerPersist: Procedure_ProviderPersist,
              public dialogRef: MatDialogRef<Procedure_ProviderPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("procedure_providers");
    this.procedure_providersSearchTextBox.setValue(procedure_providerPersist.procedure_providerSearchText);
    //delay subsequent keyup events
    this.procedure_providersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_providerPersist.procedure_providerSearchText = value;
      this.searchProcedure_Providers();
    });
  }

  ngOnInit() {

    this.procedure_providersSort.sortChange.subscribe(() => {
      this.procedure_providersPaginator.pageIndex = 0;
      this.searchProcedure_Providers();
    });

    this.procedure_providersPaginator.page.subscribe(() => {
      this.searchProcedure_Providers();
    });

    //set initial picker list to 5
    this.procedure_providersPaginator.pageSize = 5;

    //start by loading items
    this.searchProcedure_Providers();
  }

  searchProcedure_Providers(): void {
    this.procedure_providersIsLoading = true;
    this.procedure_providersSelection = [];

    this.procedure_providerPersist.searchProcedure_Provider(this.procedure_providersPaginator.pageSize,
      this.procedure_providersPaginator.pageIndex,
      this.procedure_providersSort.active,
      this.procedure_providersSort.direction).subscribe((partialList: Procedure_ProviderSummaryPartialList) => {
      this.procedure_providersData = partialList.data;
      partialList.data.forEach(value => {
        value.id = value.id.toString();
      })
      if (partialList.total != -1) {
        this.procedure_providersTotalCount = partialList.total;
      }
      this.procedure_providersIsLoading = false;
    }, error => {
      this.procedure_providersIsLoading = false;
    });

  }

  markOneItem(item: Procedure_ProviderSummary) {
    if (!this.tcUtilsArray.containsId(this.procedure_providersSelection, item.id.toString())) {
      this.procedure_providersSelection = [];
      this.procedure_providersSelection.push(item);
    } else {
      this.procedure_providersSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.procedure_providersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
