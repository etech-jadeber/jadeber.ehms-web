import {TCId} from "../tc/models";
import {Procedure_Order_CodeDetail} from "../form_encounters/procedure_order_codes/procedure_order_code.model";

export class Procedure_ProviderSummary extends TCId {
  name: string;
  notes: string;
  active: boolean;
  type: string;
}

export class Procedure_ProviderSummaryPartialList {
  data: Procedure_ProviderSummary[];
  total: number;
}

export class Procedure_ProviderDetail extends Procedure_ProviderSummary {
  name: string;
  notes: string;
  active: boolean;
  type: string;
}

export class Procedure_ProviderDashboard {
  total: number;
}

export interface PendingReview {
  patient_id: string,
  date_ordered: number,
  is_paid:boolean,
  procedure_order_code: Procedure_Order_CodeDetail[],
}
