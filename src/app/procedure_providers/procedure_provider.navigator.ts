import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import {Procedure_ProviderEditComponent} from "./procedure-provider-edit/procedure-provider-edit.component";
import {Procedure_ProviderPickComponent} from "./procedure-provider-pick/procedure-provider-pick.component";
import {PendingReviewResultComponent} from "./pending-review-result/pending-review-result.component";
import {PendingReview} from "./procedure_provider.model";


@Injectable({
  providedIn: 'root'
})

export class Procedure_ProviderNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  procedure_providersUrl(): string {
    return "/procedure_providers";
  }

  procedure_providerUrl(id: string): string {
    return "/procedure_providers/" + id;
  }

  viewProcedure_Providers(): void {
    this.router.navigateByUrl(this.procedure_providersUrl());
  }

  viewProcedure_Provider(id: string): void {
    this.router.navigateByUrl(this.procedure_providerUrl(id));
  }

  editProcedure_Provider(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_ProviderEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure_Provider(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_ProviderEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedureOrderResult(pendingReview: PendingReview): MatDialogRef<unknown,any> {
    return this.dialog.open(PendingReviewResultComponent, {
      data: pendingReview,
      width: TCModalWidths.medium
    });
  }

  pickProcedure_Providers(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_ProviderPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
