import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TcDictionary, TCId} from "../tc/models";
import {
  PendingReview,
  Procedure_ProviderDashboard,
  Procedure_ProviderDetail,
  Procedure_ProviderSummaryPartialList
} from "./procedure_provider.model";


@Injectable({
  providedIn: 'root'
})
export class Procedure_ProviderPersist {

  procedure_providerSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchProcedure_Provider(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_ProviderSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_providers", this.procedure_providerSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Procedure_ProviderSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_providerSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_providers/do", new TCDoParam("download_all", this.filters()));
  }

  procedure_providerDashboard(): Observable<Procedure_ProviderDashboard> {
    return this.http.get<Procedure_ProviderDashboard>(environment.tcApiBaseUri + "procedure_providers/dashboard");
  }

  getProcedure_Provider(id: string): Observable<Procedure_ProviderDetail> {
    return this.http.get<Procedure_ProviderDetail>(environment.tcApiBaseUri + "procedure_providers/" + id);
  }

  getProcedureOrderCodes(id: string): Observable<PendingReview[]> {
    return this.http.get<PendingReview[]>(environment.tcApiBaseUri + "procedure_providers/procedure_order_codes/" + id);
  }

  addProcedure_Provider(item: Procedure_ProviderDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_providers/", item);
  }

  updateProcedure_Provider(item: Procedure_ProviderDetail): Observable<Procedure_ProviderDetail> {
    return this.http.patch<Procedure_ProviderDetail>(environment.tcApiBaseUri + "procedure_providers/" + item.id, item);
  }

  deleteProcedure_Provider(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "procedure_providers/" + id);
  }

  procedure_providersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_providers/do", new TCDoParam(method, payload));
  }

  procedure_providerDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_providers/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_providers/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_providers/" + id + "/do", new TCDoParam("print", {}));
  }

}
