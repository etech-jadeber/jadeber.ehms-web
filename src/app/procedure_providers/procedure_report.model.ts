import {TCId} from "../tc/models";
import {Procedure_ResultDetail} from "./procedure_result.model";

export class Procedure_ReportSummary extends TCId {
  procedure_order_id: number;
  procedure_order_seq: number;
  date_collected: number;
  date_report: number;
  source: number;
  specimen_num: string;
  result_status: string;
  result_notes: string;
}

export class Procedure_ReportSummaryPartialList {
  data: Procedure_ReportSummary[];
  total: number;
}


export class Procedure_ReportDetail extends Procedure_ReportSummary {
  procedure_order_id: number;
  procedure_order_seq: number;
  date_collected: number;
  date_report: number;
  source: number;
  specimen_num: string;
  result_status: string;
  result_notes: string;
}

export class Procedure_ReportDashboard {
  total: number;
}


export class ReportResultUnion implements Procedure_ReportDetail, Procedure_ResultDetail {
  date: number;
  date_collected: number;
  date_report: number;
  facility: number;
  id: string;
  procedure_order_id: number;
  procedure_order_seq: number;
  procedure_report_id: number;
  procedure_result_id: number;
  result_notes: string;
  result_status: string;
  result_code: string;
  result_data_type: string;
  result_text: string;
  source: number;
  specimen_num: string;
  units: number;
  abnormal: string;
  comments: string;
  document_id: number;
  range: string;
  result: string;
}

