import {TCId} from "../tc/models";

export class Procedure_ResultSummary extends TCId {
  procedure_result_id : number;
  procedure_report_id : number;
  result_data_type : string;
  result_code : string;
  result_text : string;
  date : number;
  facility : number;
  units : number;
  result: string;
  range: string;
  abnormal: string;
  comments: string;
  document_id: number;
  result_status: string;
}

export class Procedure_ResultSummaryPartialList {
  data: Procedure_ResultSummary[];
  total: number;
}

export class Procedure_ResultDetail extends Procedure_ResultSummary {
  procedure_result_id : number;
  procedure_report_id : number;
  result_data_type : string;
  result_code : string;
  result_text : string;
  date : number;
  facility : number;
  units : number;
}

export class Procedure_ResultDashboard {
  total: number;
}
