import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ProcedureTemplateDetail} from "../procedure_template.model";
import {ProcedureTemplatePersist} from "../procedure_template.persist";
import {ProcedureTemplateNavigator} from "../procedure_template.navigator";
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCId } from 'src/app/tc/models';
import { CameraNavigator } from 'src/app/patients/camera.navigator';
import { WebcamImage } from 'ngx-webcam';
import { HttpClient } from '@angular/common/http';

export enum ProcedureTemplateTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-procedure-template-detail',
  templateUrl: './procedure_template-detail.component.html',
  styleUrls: ['./procedure-template-detail.component.scss']
})
export class ProcedureTemplateDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  procedureTemplateIsLoading:boolean = false;
  Editor = ClassicEditor;

  procedureTemplateTabs: typeof ProcedureTemplateTabs = ProcedureTemplateTabs;
  activeTab: ProcedureTemplateTabs = ProcedureTemplateTabs.overview;
  image_url: string;
  //basics
  procedureTemplateDetail: ProcedureTemplateDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public filePersist: FilePersist,
              public cameraNav: CameraNavigator,
              public http: HttpClient,
              public ProcedureTemplateNavigator: ProcedureTemplateNavigator,
              public  ProcedureTemplatePersist: ProcedureTemplatePersist) {
    this.tcAuthorization.requireRead("procedure_templates");
    this.procedureTemplateDetail = new ProcedureTemplateDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("procedure_templates");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.procedureTemplateIsLoading = true;
    this.ProcedureTemplatePersist.getProcedureTemplate(id).subscribe(ProcedureTemplateDetail => {
          this.procedureTemplateDetail = ProcedureTemplateDetail;
          this.procedureTemplateDetail.download_key = this.filePersist.downloadLink(this.procedureTemplateDetail.download_key)
          this.procedureTemplateIsLoading = false;
        }, error => {
          console.error(error);
          this.procedureTemplateIsLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.procedureTemplateDetail.id);
  }
  editProcedureTemplate(): void {
    let modalRef = this.ProcedureTemplateNavigator.editProcedureTemplate(this.procedureTemplateDetail.id);
    modalRef.afterClosed().subscribe(ProcedureTemplateDetail => {
      TCUtilsAngular.assign(this.procedureTemplateDetail, ProcedureTemplateDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.ProcedureTemplatePersist.print(this.procedureTemplateDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print procedure-template", true);
      });
    }

  back():void{
      this.location.back();
    }

    takePic(){
      this.cameraNav.takePic().afterClosed().subscribe(
        {
          next:(res:WebcamImage)=>{
           const rawData = atob(res.imageAsBase64);
           const bytes = new Array(rawData.length);
           for (var x = 0; x < rawData.length; x++) {
             bytes[x] = rawData.charCodeAt(x);
            }
            const arr = new Uint8Array(bytes);
            const blob = new Blob([arr], {type: 'image/png'});

            this.uploadFile(blob)

          },
          error:(err)=>{

          }
        }
      )
    }



    documentUpload(fileInputEvent: any): void {

      if (fileInputEvent.target.files[0].size > 20000000) {
        this.tcNotification.error("File too big");
        return;
      }

      if (!this.filePersist.isAllowedDocumentType(fileInputEvent.target.files[0].name)) {
        this.tcNotification.error("Unsupported file type");
        return;
      }
        this.uploadFile(fileInputEvent.target.files[0])

      
    }

    uploadFile(file: any){let fd: FormData = new FormData();
      fd.append('file', file);
      this.http.post(this.ProcedureTemplatePersist.documentUploadUri(this.procedureTemplateDetail.id), fd).subscribe({
        next: (response) => {
          this.procedureTemplateDetail.file_id = (<TCId>response).id;
         this.filePersist.getDownloadKey(this.procedureTemplateDetail.file_id).subscribe(
            (res:any)=>{
              this.procedureTemplateDetail.download_key = this.filePersist.downloadLink(res.id);
            }
          );


        },

        error:(err)=>{
          console.log(err);

        }
      }


      );}

}
