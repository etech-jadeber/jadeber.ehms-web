import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureTemplateDetailComponent } from './prcedure-template-detail.component';

describe('ProcedureTemplateDetailComponent', () => {
  let component: ProcedureTemplateDetailComponent;
  let fixture: ComponentFixture<ProcedureTemplateDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcedureTemplateDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTemplateDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
