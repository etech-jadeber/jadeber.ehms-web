import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureTemplateEditComponent } from './procedure-template-edit.component';

describe('ProcedureTemplateEditComponent', () => {
  let component: ProcedureTemplateEditComponent;
  let fixture: ComponentFixture<ProcedureTemplateEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcedureTemplateEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTemplateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
