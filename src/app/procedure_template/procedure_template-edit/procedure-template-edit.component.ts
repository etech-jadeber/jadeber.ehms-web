import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { ProcedureTemplateDetail } from '../procedure_template.model';import { ProcedureTemplatePersist } from '../procedure_template.persist';import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { lab_order_type } from 'src/app/app.enums';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestDetail } from 'src/app/lab_tests/lab_test.model';

@Component({
  selector: 'app-radiology_result_settings-edit',
  templateUrl: './procedure-template-edit.component.html',
  styleUrls: ['./procedure-template-edit.component.scss']
})export class ProcedureTemplateEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  orderName: string;
  Editor = ClassicEditor;
  config = {}

  procedureTemplateDetail: ProcedureTemplateDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ProcedureTemplateEditComponent>,
              public  persist: ProcedureTemplatePersist,
              public labOrderNavigator: Lab_TestNavigator,
              public physiocalExaminationPersist : PhysicalExaminationOptionsPersist,
              public tcUtilsDate: TCUtilsDate,
              public lab_testNavigator: Lab_TestNavigator,
              public lab_testPersist: Lab_TestPersist,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("procedure_templates");
      this.title = this.appTranslation.getText("general","new") +  " " + "procedure_templates";
      this.procedureTemplateDetail = new ProcedureTemplateDetail();
      this.procedureTemplateDetail.procedure_id = this.idMode.id
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("procedure_templates");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "procedure_templates";
      this.isLoadingResults = true;
      this.persist.getProcedureTemplate(this.idMode.id).subscribe(procedureTemplateDetail => {
        this.procedureTemplateDetail = procedureTemplateDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addProcedureTemplate(this.procedureTemplateDetail).subscribe(value => {
      this.tcNotification.success("procedureTemplate added");
      this.procedureTemplateDetail.id = value.id;
      this.dialogRef.close(this.procedureTemplateDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateProcedureTemplate(this.procedureTemplateDetail).subscribe(value => {
      this.tcNotification.success("radiology_result_settings updated");
      this.dialogRef.close(this.procedureTemplateDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.procedureTemplateDetail == null){
            return false;
          }

if (this.procedureTemplateDetail.template_name == null || this.procedureTemplateDetail.template_name  == "") {
            return false;
        }
 return true;

 }
 }
