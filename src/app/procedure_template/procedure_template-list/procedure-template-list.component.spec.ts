import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultSettingsListComponent } from './procedure-template-list.component';

describe('RadiologyResultSettingsListComponent', () => {
  let component: RadiologyResultSettingsListComponent;
  let fixture: ComponentFixture<RadiologyResultSettingsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultSettingsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultSettingsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
