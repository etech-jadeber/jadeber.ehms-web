import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ProcedureTemplateSummary, ProcedureTemplateSummaryPartialList } from '../procedure_template.model';
import { ProcedureTemplatePersist } from '../procedure_template.persist';
import { ProcedureTemplateNavigator } from '../procedure_template.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { lab_order_type } from 'src/app/app.enums';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
@Component({
  selector: 'app-procedure-template-list',
  templateUrl: './procedure-template-list.component.html',
  styleUrls: ['./procedure-template-list.component.scss']
})export class ProcedureTemplateListComponent implements OnInit {
  procedureTemplatesData: ProcedureTemplateSummary[] = [];
  procedureTemplatesTotalCount: number = 0;
  procedureTemplateSelectAll:boolean = false;
  procedureTemplateSelection: ProcedureTemplateSummary[] = [];
  orderName: string;
 procedureTemplatesDisplayedColumns: string[] = ["select","action","template_name" ];
  procedureTemplateSearchTextBox: FormControl = new FormControl();
  procedureTemplateIsLoading: boolean = false;

  @Input() procedure_id: string;
  @ViewChild(MatPaginator, {static: true}) procedureTemplatesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedureTemplatesSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedureTemplatePersist: ProcedureTemplatePersist,
                public procedureTemplateNavigator: ProcedureTemplateNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public physicalExaminationPersist: PhysicalExaminationOptionsPersist,
                public labOrderNavigator: Lab_TestNavigator,
                public labOrderPersist: Lab_TestPersist,

    ) {

        this.tcAuthorization.requireRead("procedure_templates");
       this.procedureTemplateSearchTextBox.setValue(procedureTemplatePersist.procedureTemplateSearchText);
      //delay subsequent keyup events
      this.procedureTemplateSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedureTemplatePersist.procedureTemplateSearchText = value;
        this.searchProcedureTemplates();
      });
    } ngOnInit() {

      this.procedureTemplatePersist.procedure_id = this.procedure_id
      this.procedureTemplatesSort.sortChange.subscribe(() => {
        this.procedureTemplatesPaginator.pageIndex = 0;
        this.searchProcedureTemplates(true);
      });

      this.procedureTemplatesPaginator.page.subscribe(() => {
        this.searchProcedureTemplates(true);
      });
      //start by loading items
      this.searchProcedureTemplates();
    }

  searchProcedureTemplates(isPagination:boolean = false): void {


    let paginator = this.procedureTemplatesPaginator;
    let sorter = this.procedureTemplatesSort;

    this.procedureTemplateIsLoading = true;
    this.procedureTemplateSelection = [];

    this.procedureTemplatePersist.searchProcedureTemplate(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ProcedureTemplateSummaryPartialList) => {
      this.procedureTemplatesData = partialList.data;
      if (partialList.total != -1) {
        this.procedureTemplatesTotalCount = partialList.total;
      }
      this.procedureTemplateIsLoading = false;
    }, error => {
      this.procedureTemplateIsLoading = false;
    });

  } downloadProcedureTemplates(): void {
    if(this.procedureTemplateSelectAll){
         this.procedureTemplatePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure-template", true);
      });
    }
    else{
        this.procedureTemplatePersist.download(this.tcUtilsArray.idsList(this.procedureTemplateSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download procedure-template",true);
            });
        }
  }
  ngOnDestroy(){
    this.procedureTemplatePersist.status = null;
    this.procedureTemplatePersist.procedure_id = null;
  }
addProcedureTemplate(): void {
    let dialogRef = this.procedureTemplateNavigator.addProcedureTemplate(this.procedure_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedureTemplates();
      }
    });
  }

  editProcedureTemplate(item: ProcedureTemplateSummary) {
    let dialogRef = this.procedureTemplateNavigator.editProcedureTemplate(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  searchOrder(){
    let dialogRef = this.labOrderNavigator.pickLab_Tests(true, null, lab_order_type.imaging)
    dialogRef.afterClosed().subscribe(
      (value : Lab_OrderDetail[]) => {
        if (value){
          this.procedureTemplatePersist.procedure_id = value[0].id
          this.orderName = value[0].name
          this.searchProcedureTemplates()
        }
      }
    )
  }
  deleteProcedureTemplate(item: ProcedureTemplateSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("procedure-template");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedureTemplatePersist.deleteProcedureTemplate(item.id).subscribe(response => {
          this.tcNotification.success("procedure-template deleted");
          this.searchProcedureTemplates();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
