import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ProcedureTemplateSummary, ProcedureTemplateSummaryPartialList } from '../procedure_template.model';
import { ProcedureTemplatePersist } from '../procedure_template.persist';
import { ProcedureTemplateNavigator } from '../procedure_template.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
@Component({
  selector: 'app-procedure-template-pick',
  templateUrl: './procedure-template-pick.component.html',
  styleUrls: ['./procedure-template-pick.component.scss']
})export class ProcedureTemplatePickComponent implements OnInit {
  procedureTemplatesData: ProcedureTemplateSummary[] = [];
  procedureTemplatesTotalCount: number = 0;
  procedureTemplateSelectAll:boolean = false;
  procedureTemplateSelection: ProcedureTemplateSummary[] = [];

 procedureTemplatesDisplayedColumns: string[] = ["select","template_name" ];
  procedureTemplateSearchTextBox: FormControl = new FormControl();
  procedureTemplateIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) procedureTemplatesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedureTemplatesSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedureTemplatePersist: ProcedureTemplatePersist,
                public procedureTemplateNavigator: ProcedureTemplateNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public physicalExaminationPersist: PhysicalExaminationOptionsPersist,
 public dialogRef: MatDialogRef<ProcedureTemplatePickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, order_id: string, status: number, resultList: string}

    ) {

        this.tcAuthorization.requireRead("procedure-template");
        this.procedureTemplatePersist.procedure_id = data.order_id,
        this.procedureTemplatePersist.status = data.status,
       this.procedureTemplateSearchTextBox.setValue(procedureTemplatePersist.procedureTemplateSearchText);
      //delay subsequent keyup events
      this.procedureTemplateSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedureTemplatePersist.procedureTemplateSearchText = value;
        this.searchProceduretemplates();
      });
    } ngOnInit() {

      this.procedureTemplatesSort.sortChange.subscribe(() => {
        this.procedureTemplatesPaginator.pageIndex = 0;
        this.searchProceduretemplates(true);
      });

      this.procedureTemplatesPaginator.page.subscribe(() => {
        this.searchProceduretemplates(true);
      });
      //start by loading items
      this.searchProceduretemplates();
    }

  searchProceduretemplates(isPagination:boolean = false): void {


    let paginator = this.procedureTemplatesPaginator;
    let sorter = this.procedureTemplatesSort;

    this.procedureTemplateIsLoading = true;
    this.procedureTemplateSelection = [];

    this.procedureTemplatePersist.searchProcedureTemplate(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ProcedureTemplateSummaryPartialList) => {
      this.procedureTemplatesData = partialList.data;
      if (partialList.total != -1) {
        this.procedureTemplatesTotalCount = partialList.total;
      }
      this.procedureTemplateIsLoading = false;
    }, error => {
      this.procedureTemplateIsLoading = false;
    });

  }
  markOneItem(item: ProcedureTemplateSummary) {
    if(!this.tcUtilsArray.containsId(this.procedureTemplateSelection,item.id)){
          this.procedureTemplateSelection = [];
          this.procedureTemplateSelection.push(item);
        }
        else{
          this.procedureTemplateSelection = [];
        }
  }

  ngOnDestroy(){
    this.procedureTemplatePersist.status = null;
    this.procedureTemplatePersist.procedure_id = null;
  }

  returnSelected(): void {
    this.dialogRef.close(this.procedureTemplateSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  }
