import {TCId} from "../tc/models";

export class ProcedureTemplateSummary extends TCId {
    template_name: string;
    template: string;
    procedure_id: string;
    status: number;
    file_id: string;
    download_key: string;

    }
    export class ProcedureTemplateSummaryPartialList {
      data: ProcedureTemplateSummary[];
      total: number;
    }
    export class ProcedureTemplateDetail extends ProcedureTemplateSummary {
    }

    export class ProcedureTemplateDashboard {
      total: number;
    }
