import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ProcedureTemplateEditComponent} from "./procedure_template-edit/procedure-template-edit.component";
import {ProcedureTemplatePickComponent} from "./procedure_template-pick/procedure-template-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ProcedureTemplateNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  procedureTemplatesUrl(): string {
    return "/procedure-templates";
  }

  procedureTemplateUrl(id: string): string {
    return "/procedure-templates/" + id;
  }

  viewProcedureTemplates(): void {
    this.router.navigateByUrl(this.procedureTemplatesUrl());
  }

  viewProcedureTemplate(id: string): void {
    this.router.navigateByUrl(this.procedureTemplateUrl(id));
  }

  editProcedureTemplate(id: string): MatDialogRef<ProcedureTemplateEditComponent> {
    const dialogRef = this.dialog.open(ProcedureTemplateEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }

  addProcedureTemplate(lab_test_id: string): MatDialogRef<ProcedureTemplateEditComponent> {
    const dialogRef = this.dialog.open(ProcedureTemplateEditComponent, {
          data: new TCIdMode(lab_test_id, TCModalModes.NEW),
          width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }

   pickProcedureTemplates(selectOne: boolean, order_id: string): MatDialogRef<ProcedureTemplatePickComponent> {
      const dialogRef = this.dialog.open(ProcedureTemplatePickComponent, {
        data: {selectOne, order_id},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
