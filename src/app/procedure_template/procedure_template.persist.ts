import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ProcedureTemplateDashboard, ProcedureTemplateDetail, ProcedureTemplateSummaryPartialList} from "./procedure_template.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class ProcedureTemplatePersist {
 procedureTemplateSearchText: string = "";
 procedure_id: string;

onReady(editor) {
  editor.ui
    .getEditableElement()
    .parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
}
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedureTemplateSearchText;
    //add custom filters
    return fltrs;
  }
  status: number;

  searchProcedureTemplate(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ProcedureTemplateSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_template", this.procedureTemplateSearchText, pageSize, pageIndex, sort, order);
    if (this.procedure_id){
      url = TCUtilsString.appendUrlParameter(url, "procedure_id", this.procedure_id)
  }
    if (this.status){
        url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }

    return this.http.get<ProcedureTemplateSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_template/do", new TCDoParam("download_all", this.filters()));
  }

  documentUploadUri(template_id: string) {
    return environment.tcApiBaseUri + `procedure_template/${template_id}/upload-image/`;
  }

  ProcedureTemplateDashboard(): Observable<ProcedureTemplateDashboard> {
    return this.http.get<ProcedureTemplateDashboard>(environment.tcApiBaseUri + "procedure_template/dashboard");
  }

  getProcedureTemplate(id: string): Observable<ProcedureTemplateDetail> {
    return this.http.get<ProcedureTemplateDetail>(environment.tcApiBaseUri + "procedure_template/" + id);
  } addProcedureTemplate(item: ProcedureTemplateDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_template/", item);
  }

  updateProcedureTemplate(item: ProcedureTemplateDetail): Observable<ProcedureTemplateDetail> {
    return this.http.patch<ProcedureTemplateDetail>(environment.tcApiBaseUri + "procedure_template/" + item.id, item);
  }

  deleteProcedureTemplate(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "procedure_template/" + id);
  }
 ProcedureTemplatesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_template/do", new TCDoParam(method, payload));
  }

  ProcedureTemplateDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_templates/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_template/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_template/" + id + "/do", new TCDoParam("print", {}));
  }


}
