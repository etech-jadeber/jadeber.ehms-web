import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTestsDetailComponent } from './procedure-tests-detail.component';

describe('ProcedureTestsDetailComponent', () => {
  let component: ProcedureTestsDetailComponent;
  let fixture: ComponentFixture<ProcedureTestsDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTestsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTestsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
