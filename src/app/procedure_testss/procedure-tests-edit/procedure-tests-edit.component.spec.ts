import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTestsEditComponent } from './procedure-tests-edit.component';

describe('ProcedureTestsEditComponent', () => {
  let component: ProcedureTestsEditComponent;
  let fixture: ComponentFixture<ProcedureTestsEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTestsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTestsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
