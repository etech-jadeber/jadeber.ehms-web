import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Procedure_TestsDetail} from "../procedure_tests.model";
import {Procedure_TestsPersist} from "../procedure_tests.persist";
import { Procedure_Order_CodeDetail } from 'src/app/form_encounters/procedure_order_codes/procedure_order_code.model';
import { ProcedureStatus } from 'src/app/form_encounters/procedure_orders/procedure_order.persist';


@Component({
  selector: 'app-procedure_tests-edit',
  templateUrl: './procedure-tests-edit.component.html',
  styleUrls: ['./procedure-tests-edit.component.css']
})
export class Procedure_TestsEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedureType: ProcedureStatus[];
  procedure_testsDetail: Procedure_TestsDetail;
  
  procedure_order_codeDetail: Procedure_Order_CodeDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Procedure_TestsEditComponent>,
              public  persist: Procedure_TestsPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                this.procedureType = this.persist.procedureType;

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("procedure_testss");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("procedure", "procedure_tests");
      this.procedure_testsDetail = new Procedure_TestsDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("procedure_testss");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("procedure", "procedure_tests");
      this.isLoadingResults = true;
      this.persist.getProcedure_Tests(this.idMode.id).subscribe(procedure_testsDetail => {
        this.procedure_testsDetail = procedure_testsDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addProcedure_Tests(this.procedure_testsDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Tests added");
      this.procedure_testsDetail.id = value.id;
      this.dialogRef.close(this.procedure_testsDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateProcedure_Tests(this.procedure_testsDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Tests updated");
      this.dialogRef.close(this.procedure_testsDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.procedure_testsDetail == null){
            return false;
          }

        if (this.procedure_testsDetail.type == null || this.procedure_testsDetail.type  == "") {
                      return false;
                    }

if (this.procedure_testsDetail.name == null || this.procedure_testsDetail.name  == "") {
                      return false;
                    }

if (this.procedure_testsDetail.code == null || this.procedure_testsDetail.code  == "") {
                      return false;
                    }


        return true;
      }


}
