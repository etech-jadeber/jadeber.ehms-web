import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTestsListComponent } from './procedure-tests-list.component';

describe('ProcedureTestsListComponent', () => {
  let component: ProcedureTestsListComponent;
  let fixture: ComponentFixture<ProcedureTestsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTestsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTestsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
