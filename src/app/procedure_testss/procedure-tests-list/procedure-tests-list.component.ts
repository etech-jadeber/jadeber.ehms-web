import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Procedure_TestsPersist} from "../procedure_tests.persist";
import {Procedure_TestsNavigator} from "../procedure_tests.navigator";
import {Procedure_TestsDetail, Procedure_TestsSummary, Procedure_TestsSummaryPartialList} from "../procedure_tests.model";


@Component({
  selector: 'app-procedure_tests-list',
  templateUrl: './procedure-tests-list.component.html',
  styleUrls: ['./procedure-tests-list.component.css']
})
export class Procedure_TestsListComponent implements OnInit {

  procedure_testssData: Procedure_TestsSummary[] = [];
  procedure_testssTotalCount: number = 0;
  procedure_testssSelectAll:boolean = false;
  procedure_testssSelection: Procedure_TestsSummary[] = [];

  procedure_testssDisplayedColumns: string[] = ["select","action", "type","name","code","amount", ];
  procedure_testssSearchTextBox: FormControl = new FormControl();
  procedure_testssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_testssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_testssSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedure_testsPersist: Procedure_TestsPersist,
                public procedure_testsNavigator: Procedure_TestsNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("procedure_testss");
       this.procedure_testssSearchTextBox.setValue(procedure_testsPersist.procedure_testsSearchText);
      //delay subsequent keyup events
      this.procedure_testssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedure_testsPersist.procedure_testsSearchText = value;
        this.searchProcedure_Testss();
      });
    }

    ngOnInit() {

      this.procedure_testssSort.sortChange.subscribe(() => {
        this.procedure_testssPaginator.pageIndex = 0;
        this.searchProcedure_Testss(true);
      });

      this.procedure_testssPaginator.page.subscribe(() => {
        this.searchProcedure_Testss(true);
      });
      //start by loading items
      this.searchProcedure_Testss();
    }

  searchProcedure_Testss(isPagination:boolean = false): void {


    let paginator = this.procedure_testssPaginator;
    let sorter = this.procedure_testssSort;

    this.procedure_testssIsLoading = true;
    this.procedure_testssSelection = [];

    this.procedure_testsPersist.searchProcedure_Tests(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Procedure_TestsSummaryPartialList) => {
      this.procedure_testssData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_testssTotalCount = partialList.total;
      }
      this.procedure_testssIsLoading = false;
    }, error => {
      this.procedure_testssIsLoading = false;
    });

  }

  downloadProcedure_Testss(): void {
    if(this.procedure_testssSelectAll){
         this.procedure_testsPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_testss", true);
      });
    }
    else{
        this.procedure_testsPersist.download(this.tcUtilsArray.idsList(this.procedure_testssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download procedure_testss",true);
            });
        }
  }



  addProcedure_Tests(): void {
    let dialogRef = this.procedure_testsNavigator.addProcedure_Tests();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Testss();
      }
    });
  }

  editProcedure_Tests(item: Procedure_TestsSummary) {
    let dialogRef = this.procedure_testsNavigator.editProcedure_Tests(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteProcedure_Tests(item: Procedure_TestsSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Tests");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedure_testsPersist.deleteProcedure_Tests(item.id).subscribe(response => {
          this.tcNotification.success("Procedure_Tests deleted");
          this.searchProcedure_Testss();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
