import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTestsPickComponent } from './procedure-tests-pick.component';

describe('ProcedureTestsPickComponent', () => {
  let component: ProcedureTestsPickComponent;
  let fixture: ComponentFixture<ProcedureTestsPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTestsPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTestsPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
