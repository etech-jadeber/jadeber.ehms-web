import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Procedure_TestsDetail, Procedure_TestsSummary, Procedure_TestsSummaryPartialList} from "../procedure_tests.model";
import {Procedure_TestsPersist} from "../procedure_tests.persist";


@Component({
  selector: 'app-procedure_tests-pick',
  templateUrl: './procedure-tests-pick.component.html',
  styleUrls: ['./procedure-tests-pick.component.css']
})
export class Procedure_TestsPickComponent implements OnInit {

  procedure_testssData: Procedure_TestsSummary[] = [];
  procedure_testssTotalCount: number = 0;
  procedure_testssSelection: Procedure_TestsSummary[] = [];
  procedure_testssDisplayedColumns: string[] = ["select", 'type','name','code','amount' ];

  procedure_testssSearchTextBox: FormControl = new FormControl();
  procedure_testssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_testssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_testssSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public procedure_testsPersist: Procedure_TestsPersist,
              public dialogRef: MatDialogRef<Procedure_TestsPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("procedure_testss");
    this.procedure_testssSearchTextBox.setValue(procedure_testsPersist.procedure_testsSearchText);
    //delay subsequent keyup events
    this.procedure_testssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_testsPersist.procedure_testsSearchText = value;
      this.searchProcedure_Testss();
    });
  }

  ngOnInit() {

    this.procedure_testssSort.sortChange.subscribe(() => {
      this.procedure_testssPaginator.pageIndex = 0;
      this.searchProcedure_Testss();
    });

    this.procedure_testssPaginator.page.subscribe(() => {
      this.searchProcedure_Testss();
    });

    //set initial picker list to 5
    this.procedure_testssPaginator.pageSize = 5;

    //start by loading items
    this.searchProcedure_Testss();
  }

  searchProcedure_Testss(): void {
    this.procedure_testssIsLoading = true;
    this.procedure_testssSelection = [];

    this.procedure_testsPersist.searchProcedure_Tests(this.procedure_testssPaginator.pageSize,
        this.procedure_testssPaginator.pageIndex,
        this.procedure_testssSort.active,
        this.procedure_testssSort.direction).subscribe((partialList: Procedure_TestsSummaryPartialList) => {
      this.procedure_testssData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_testssTotalCount = partialList.total;
      }
      this.procedure_testssIsLoading = false;
    }, error => {
      this.procedure_testssIsLoading = false;
    });

  }

  markOneItem(item: Procedure_TestsSummary) {
    if(!this.tcUtilsArray.containsId(this.procedure_testssSelection,item.id)){
          this.procedure_testssSelection = [];
          this.procedure_testssSelection.push(item);
        }
        else{
          this.procedure_testssSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.procedure_testssSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
