import {TCId} from "../tc/models";

export class Procedure_TestsSummary extends TCId {
  type : string;
name : string;
code : string;
amount : number;
}

export class Procedure_TestsSummaryPartialList {
  data: Procedure_TestsSummary[];
  total: number;
}

export class Procedure_TestsDetail extends Procedure_TestsSummary {
  type : string;
name : string;
code : string;
amount : number;
}

export class Procedure_TestsDashboard {
  total: number;
}
