import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";
import { Procedure_TestsEditComponent } from "./procedure-tests-edit/procedure-tests-edit.component";
import { Procedure_TestsPickComponent } from "./procedure-tests-pick/procedure-tests-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Procedure_TestsNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  procedure_testssUrl(): string {
    return "/procedure_testss";
  }

  procedure_testsUrl(id: string): string {
    return "/procedure_testss/" + id;
  }

  viewProcedure_Testss(): void {
    this.router.navigateByUrl(this.procedure_testssUrl());
  }

  viewProcedure_Tests(id: string): void {
    this.router.navigateByUrl(this.procedure_testsUrl(id));
  }

  editProcedure_Tests(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_TestsEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure_Tests(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Procedure_TestsEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickProcedure_Testss(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Procedure_TestsPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
