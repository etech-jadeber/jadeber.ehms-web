import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Procedure_TestsDashboard, Procedure_TestsDetail, Procedure_TestsSummaryPartialList} from "./procedure_tests.model";
import { ProcedureStatus } from '../form_encounters/procedure_orders/procedure_order.persist';


@Injectable({
  providedIn: 'root'
})
export class Procedure_TestsPersist {

  procedure_testsSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchProcedure_Tests(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_TestsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_testss", this.procedure_testsSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Procedure_TestsSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_testsSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_testss/do", new TCDoParam("download_all", this.filters()));
  }

  procedure_testsDashboard(): Observable<Procedure_TestsDashboard> {
    return this.http.get<Procedure_TestsDashboard>(environment.tcApiBaseUri + "procedure_testss/dashboard");
  }

  getProcedure_Tests(id: string): Observable<Procedure_TestsDetail> {
    return this.http.get<Procedure_TestsDetail>(environment.tcApiBaseUri + "procedure_testss/" + id);
  }

  addProcedure_Tests(item: Procedure_TestsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_testss/", item);
  }

  updateProcedure_Tests(item: Procedure_TestsDetail): Observable<Procedure_TestsDetail> {
    return this.http.patch<Procedure_TestsDetail>(environment.tcApiBaseUri + "procedure_testss/" + item.id, item);
  }

  deleteProcedure_Tests(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "procedure_testss/" + id);
  }

  procedure_testssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_testss/do", new TCDoParam(method, payload));
  }

  procedure_testsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_testss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_testss/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_testss/" + id + "/do", new TCDoParam("print", {}));
  }
  procedureType: ProcedureStatus[] = [
    {name: "intervention", value: "intervention"},
    {name: "laboratory_test", value: "laboratory_test"},
    {name: "physical_exam", value: "physical_exam"},
    {name: "risk_category", value: "risk_category"},
    {name: "patient_characteristics", value: "patient_characteristics"},
    {name: "imaging", value: "imaging"},
    {name: "encounter_checkup_procedure", value: "enc_checkup_procedure"},
  ];

}
