import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTypeDetailComponent } from './procedure-type-detail.component';

describe('ProcedureTypeDetailComponent', () => {
  let component: ProcedureTypeDetailComponent;
  let fixture: ComponentFixture<ProcedureTypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
