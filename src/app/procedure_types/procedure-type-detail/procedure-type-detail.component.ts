import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Procedure_TypeDetail} from "../procedure_type.model";
import {Procedure_TypePersist} from "../procedure_type.persist";
import {Procedure_TypeNavigator} from "../procedure_type.navigator";

export enum Procedure_TypeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-procedure-type-detail',
  templateUrl: './procedure-type-detail.component.html',
  styleUrls: ['./procedure-type-detail.component.css']
})
export class ProcedureTypeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  procedure_typeLoading:boolean = false;
  
  Procedure_TypeTabs: typeof Procedure_TypeTabs = Procedure_TypeTabs;
  activeTab: Procedure_TypeTabs = Procedure_TypeTabs.overview;
  //basics
  procedure_typeDetail: Procedure_TypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public procedure_typeNavigator: Procedure_TypeNavigator,
              public  procedure_typePersist: Procedure_TypePersist) {
    this.tcAuthorization.requireRead("procedure_types");
    this.procedure_typeDetail = new Procedure_TypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("procedure_types");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.procedure_typeLoading = true;
    this.procedure_typePersist.getProcedure_Type(id).subscribe(procedure_typeDetail => {
          this.procedure_typeDetail = procedure_typeDetail;
          this.procedure_typeLoading = false;
        }, error => {
          console.error(error);
          this.procedure_typeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.procedure_typeDetail.id);
  }

  editProcedure_Type(): void {
    let modalRef = this.procedure_typeNavigator.editProcedure_Type(this.procedure_typeDetail.id);
    modalRef.afterClosed().subscribe(modifiedProcedure_TypeDetail => {
      TCUtilsAngular.assign(this.procedure_typeDetail, modifiedProcedure_TypeDetail);
    }, error => {
      console.error(error);
    });
  }

   printProcedure_Type():void{
      this.procedure_typePersist.print(this.procedure_typeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print procedure_type", true);
      });
    }

  back():void{
      this.location.back();
    }

}

