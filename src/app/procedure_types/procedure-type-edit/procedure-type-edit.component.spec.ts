import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTypeEditComponent } from './procedure-type-edit.component';

describe('ProcedureTypeEditComponent', () => {
  let component: ProcedureTypeEditComponent;
  let fixture: ComponentFixture<ProcedureTypeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
