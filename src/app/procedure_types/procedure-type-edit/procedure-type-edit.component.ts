import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Procedure_TypeDetail} from "../procedure_type.model";
import {Procedure_TypePersist} from "../procedure_type.persist";


@Component({
  selector: 'app-procedure-type-edit',
  templateUrl: './procedure-type-edit.component.html',
  styleUrls: ['./procedure-type-edit.component.css']
})
export class ProcedureTypeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedure_typeDetail: Procedure_TypeDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ProcedureTypeEditComponent>,
              public  persist: Procedure_TypePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("procedure_types");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("patient","procedure_type");
      this.procedure_typeDetail = new Procedure_TypeDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("procedure_types");
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("patient","procedure_type");
      this.isLoadingResults = true;
      this.persist.getProcedure_Type(this.idMode.id).subscribe(procedure_typeDetail => {
        this.procedure_typeDetail = procedure_typeDetail;
        this.procedure_typeDetail.type = -1;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addProcedure_Type(this.procedure_typeDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Type added");
      this.procedure_typeDetail.id = value.id;
      this.dialogRef.close(this.procedure_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateProcedure_Type(this.procedure_typeDetail).subscribe(value => {
      this.tcNotification.success("Procedure_Type updated");
      this.dialogRef.close(this.procedure_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.procedure_typeDetail == null){
            return false;
          }

        if (this.procedure_typeDetail.name == null || this.procedure_typeDetail.name  == "") {
                      return false;
                    }
        if (this.procedure_typeDetail.type == -1) {
          return false;
        }

        return true;
      }


}
