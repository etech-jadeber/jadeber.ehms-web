import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTypeListComponent } from './procedure-type-list.component';

describe('ProcedureTypeListComponent', () => {
  let component: ProcedureTypeListComponent;
  let fixture: ComponentFixture<ProcedureTypeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
