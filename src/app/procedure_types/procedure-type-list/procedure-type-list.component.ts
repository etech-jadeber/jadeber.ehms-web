import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Procedure_TypePersist} from "../procedure_type.persist";
import {Procedure_TypeNavigator} from "../procedure_type.navigator";
import {Procedure_TypeDetail, Procedure_TypeSummary, Procedure_TypeSummaryPartialList} from "../procedure_type.model";


@Component({
  selector: 'app-procedure-type-list',
  templateUrl: './procedure-type-list.component.html',
  styleUrls: ['./procedure-type-list.component.css']
})
export class ProcedureTypeListComponent implements OnInit {

  procedure_typesData: Procedure_TypeSummary[] = [];
  procedure_typesTotalCount: number = 0;
  procedure_typesSelectAll:boolean = false;
  procedure_typesSelection: Procedure_TypeSummary[] = [];

  procedure_typesDisplayedColumns: string[] = ["select","action", "name","type", ];
  procedure_typesSearchTextBox: FormControl = new FormControl();
  procedure_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_typesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedure_typePersist: Procedure_TypePersist,
                public procedure_typeNavigator: Procedure_TypeNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("procedure_types");
       this.procedure_typesSearchTextBox.setValue(procedure_typePersist.procedure_typeSearchText);
      //delay subsequent keyup events
      this.procedure_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedure_typePersist.procedure_typeSearchText = value;
        this.searchProcedure_Types();
      });
    }

    ngOnInit() {

      this.procedure_typesSort.sortChange.subscribe(() => {
        this.procedure_typesPaginator.pageIndex = 0;
        this.searchProcedure_Types(true);
      });

      this.procedure_typesPaginator.page.subscribe(() => {
        this.searchProcedure_Types(true);
      });
      //start by loading items
      this.searchProcedure_Types();
    }

  searchProcedure_Types(isPagination:boolean = false): void {


    let paginator = this.procedure_typesPaginator;
    let sorter = this.procedure_typesSort;

    this.procedure_typesIsLoading = true;
    this.procedure_typesSelection = [];

    this.procedure_typePersist.searchProcedure_Type(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Procedure_TypeSummaryPartialList) => {
      this.procedure_typesData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_typesTotalCount = partialList.total;
      }
      this.procedure_typesIsLoading = false;
    }, error => {
      this.procedure_typesIsLoading = false;
    });

  }

  downloadProcedure_Types(): void {
    if(this.procedure_typesSelectAll){
         this.procedure_typePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedure_types", true);
      });
    }
    else{
        this.procedure_typePersist.download(this.tcUtilsArray.idsList(this.procedure_typesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download procedure_types",true);
            });
        }
  }



  addProcedure_Type(): void {
    let dialogRef = this.procedure_typeNavigator.addProcedure_Type();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedure_Types();
      }
    });
  }

  editProcedure_Type(item: Procedure_TypeSummary) {
    let dialogRef = this.procedure_typeNavigator.editProcedure_Type(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteProcedure_Type(item: Procedure_TypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure_Type");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedure_typePersist.deleteProcedure_Type(item.id).subscribe(response => {
          this.tcNotification.success("Procedure_Type deleted");
          this.searchProcedure_Types();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
