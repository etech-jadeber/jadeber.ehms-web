import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedureTypePickComponent } from './procedure-type-pick.component';

describe('ProcedureTypePickComponent', () => {
  let component: ProcedureTypePickComponent;
  let fixture: ComponentFixture<ProcedureTypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedureTypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureTypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
