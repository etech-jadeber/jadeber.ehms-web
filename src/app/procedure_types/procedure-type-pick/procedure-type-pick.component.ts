import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Procedure_TypeDetail, Procedure_TypeSummary, Procedure_TypeSummaryPartialList} from "../procedure_type.model";
import {Procedure_TypePersist} from "../procedure_type.persist";


@Component({
  selector: 'app-procedure-type-pick',
  templateUrl: './procedure-type-pick.component.html',
  styleUrls: ['./procedure-type-pick.component.css']
})
export class ProcedureTypePickComponent implements OnInit {

  procedure_typesData: Procedure_TypeSummary[] = [];
  procedure_typesTotalCount: number = 0;
  procedure_typesSelection: Procedure_TypeSummary[] = [];
  procedure_typesDisplayedColumns: string[] = ["select", 'name','type' ];

  procedure_typesSearchTextBox: FormControl = new FormControl();
  procedure_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) procedure_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) procedure_typesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public procedure_typePersist: Procedure_TypePersist,
              public dialogRef: MatDialogRef<ProcedureTypePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("procedure_types");
    this.procedure_typesSearchTextBox.setValue(procedure_typePersist.procedure_typeSearchText);
    //delay subsequent keyup events
    this.procedure_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedure_typePersist.procedure_typeSearchText = value;
      this.searchProcedure_Types();
    });
  }

  ngOnInit() {

    this.procedure_typesSort.sortChange.subscribe(() => {
      this.procedure_typesPaginator.pageIndex = 0;
      this.searchProcedure_Types();
    });

    this.procedure_typesPaginator.page.subscribe(() => {
      this.searchProcedure_Types();
    });

    //set initial picker list to 5
    this.procedure_typesPaginator.pageSize = 5;

    //start by loading items
    this.searchProcedure_Types();
  }

  searchProcedure_Types(): void {
    this.procedure_typesIsLoading = true;
    this.procedure_typesSelection = [];

    this.procedure_typePersist.searchProcedure_Type(this.procedure_typesPaginator.pageSize,
        this.procedure_typesPaginator.pageIndex,
        this.procedure_typesSort.active,
        this.procedure_typesSort.direction).subscribe((partialList: Procedure_TypeSummaryPartialList) => {
      this.procedure_typesData = partialList.data;
      if (partialList.total != -1) {
        this.procedure_typesTotalCount = partialList.total;
      }
      this.procedure_typesIsLoading = false;
    }, error => {
      this.procedure_typesIsLoading = false;
    });

  }

  markOneItem(item: Procedure_TypeSummary) {
    if(!this.tcUtilsArray.containsId(this.procedure_typesSelection,item.id)){
          this.procedure_typesSelection = [];
          this.procedure_typesSelection.push(item);
        }
        else{
          this.procedure_typesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.procedure_typesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
