import {TCId} from "../tc/models";

export class Procedure_TypeSummary extends TCId {
  name : string;
type : number;
}

export class Procedure_TypeSummaryPartialList {
  data: Procedure_TypeSummary[];
  total: number;
}

export class Procedure_TypeDetail extends Procedure_TypeSummary {
  name : string;
type : number;
}

export class Procedure_TypeDashboard {
  total: number;
}
