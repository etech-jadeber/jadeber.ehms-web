import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ProcedureTypeEditComponent} from "./procedure-type-edit/procedure-type-edit.component";
import {ProcedureTypePickComponent} from "./procedure-type-pick/procedure-type-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Procedure_TypeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  procedure_typesUrl(): string {
    return "/procedure_types";
  }

  procedure_typeUrl(id: string): string {
    return "/procedure_types/" + id;
  }

  viewProcedure_Types(): void {
    this.router.navigateByUrl(this.procedure_typesUrl());
  }

  viewProcedure_Type(id: string): void {
    this.router.navigateByUrl(this.procedure_typeUrl(id));
  }
  
  editProcedure_Type(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ProcedureTypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure_Type(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ProcedureTypeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickProcedure_Types(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(ProcedureTypePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
