import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Procedure_TypeDashboard, Procedure_TypeDetail, Procedure_TypeSummaryPartialList} from "./procedure_type.model";
import { procedure_type } from '../app.enums';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class Procedure_TypePersist {

  procedure_typeSearchText: string = "";

  procedure_typeId: number ;
    
  procedure_types: TCEnum[] = [
  new TCEnum(procedure_type.major, this.appTranslation.getKey('patient', 'major')),
  new TCEnum(procedure_type.minor, this.appTranslation.getKey('patient', 'minor')),
];

  constructor(
      private http: HttpClient,
      private appTranslation: AppTranslation,
      ) {
  }

  searchProcedure_Type(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Procedure_TypeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedure_types", this.procedure_typeSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Procedure_TypeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedure_typeSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_types/do", new TCDoParam("download_all", this.filters()));
  }

  procedure_typeDashboard(): Observable<Procedure_TypeDashboard> {
    return this.http.get<Procedure_TypeDashboard>(environment.tcApiBaseUri + "procedure_types/dashboard");
  }

  getProcedure_Type(id: string): Observable<Procedure_TypeDetail> {
    return this.http.get<Procedure_TypeDetail>(environment.tcApiBaseUri + "procedure_types/" + id);
  }

  addProcedure_Type(item: Procedure_TypeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_types/", item);
  }

  updateProcedure_Type(item: Procedure_TypeDetail): Observable<Procedure_TypeDetail> {
    return this.http.patch<Procedure_TypeDetail>(environment.tcApiBaseUri + "procedure_types/" + item.id, item);
  }

  deleteProcedure_Type(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "procedure_types/" + id);
  }

  procedure_typesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_types/do", new TCDoParam(method, payload));
  }

  procedure_typeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "procedure_types/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_types/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "procedure_types/" + id + "/do", new TCDoParam("print", {}));
  }


}
