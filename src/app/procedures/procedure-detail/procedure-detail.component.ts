import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ProcedureDetail, ProcedureNoteSummary, ProcedureSummary} from "../procedure.model";
import {ProcedurePersist} from "../procedure.persist";
import {ProcedureNavigator} from "../procedure.navigator";
import { Procedure_TypePersist } from 'src/app/procedure_types/procedure_type.persist';
import { Procedure_TypeSummary, Procedure_TypeSummaryPartialList } from 'src/app/procedure_types/procedure_type.model';
import { CommentDetail, CommentSummary } from 'src/app/tc/models';
import { FormControl } from '@angular/forms';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCAppInit } from 'src/app/tc/app-init';
import { ProcedureNotePersist } from 'src/app/patients/procedureNote.persist';

export enum ProcedureTabs {
  overview,
}

export enum PaginatorIndexes {
  notes
}


@Component({
  selector: 'app-procedure-detail',
  templateUrl: './procedure-detail.component.html',
  styleUrls: ['./procedure-detail.component.css']
})
export class ProcedureDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  procedureLoading:boolean = false;
  
  ProcedureTabs: typeof ProcedureTabs = ProcedureTabs;
  activeTab: ProcedureTabs = ProcedureTabs.overview;
  //basics
  procedureDetail: ProcedureDetail;
  procedureTypes: Procedure_TypeSummary [] = [];

    //notes
    notesData: ProcedureNoteSummary[] = [];
    notesTotalCount: number = 0;
    notesSelectAll: boolean = false;
    notesSelected: ProcedureNoteSummary[] = [];
    notesDisplayedColumns: string[] = ['select', 'action', 'comment_text'];
    notesSearchTextBox: FormControl = new FormControl();
    notesLoading: boolean = false;

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public procedureNavigator: ProcedureNavigator,
              public  procedurePersist: ProcedurePersist,
              public patientPersist: PatientPersist,
              public userPersist: UserPersist,
              public tcAuthentication: TCAuthentication,
              public tcUtilsDate: TCUtilsDate,
              public procedure_typePersist: Procedure_TypePersist,
              public procedureNotePersist: ProcedureNotePersist) {
    this.tcAuthorization.requireRead("procedures");
    this.procedureDetail = new ProcedureDetail();
        //notes filter
        this.notesSearchTextBox.setValue(procedureNotePersist.noteSearchText);
        this.notesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
          this.procedureNotePersist.noteSearchText = value.trim();
          this.searchNotes();
        });

  }

  ngOnInit() {
    this.loadProcedureTypes();
    this.tcAuthorization.requireRead("procedures");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.procedureNotePersist.procedureId = id;
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.procedureLoading = true;
    this.procedurePersist.getProcedure(id).subscribe(procedureDetail => {
          this.procedureDetail = procedureDetail;
          this.procedureLoading = false;
          this.searchNotes();
        }, error => {
          console.error(error);
          this.procedureLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.procedureDetail.id);
  }

  editProcedure(): void {
    let modalRef = this.procedureNavigator.editProcedure(this.procedureDetail.id);
    modalRef.afterClosed().subscribe(modifiedProcedureDetail => {
      TCUtilsAngular.assign(this.procedureDetail, modifiedProcedureDetail);
    }, error => {
      console.error(error);
    });
  }

   printProcedure():void{
      this.procedurePersist.print(this.procedureDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print procedure", true);
      });
    }
    loadProcedureTypes(): void {
      this.procedure_typePersist.searchProcedure_Type(50,0, "", "").subscribe((result: Procedure_TypeSummaryPartialList) => {
        this.procedureTypes = result.data;
      }, error => {
      })
    }

      //notes methods
  searchNotes(): void {
    let paginator = this.paginators.toArray()[PaginatorIndexes.notes];
    let sorter = this.sorters.toArray()[PaginatorIndexes.notes];
    this.notesLoading = true;
    this.procedureNotePersist.searchProcedureNote(paginator.pageSize, 0, sorter.active, sorter.direction).subscribe(response => {
      this.notesData = response.data;
      if (response.total != -1) {
        this.notesTotalCount = response.total;
      }
      this.notesLoading = false;
    }, error => {

    });
  }


  addNote(): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), "", true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const procedureNote = new ProcedureNoteSummary()
        this.procedureNotePersist.addProcedureNote(procedureNote).subscribe(response => {
          this.searchNotes();
        });
      }
    });
  }

  editNote(item: CommentDetail): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), item.comment_text, true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const procedureNote = new ProcedureNoteSummary()
        this.procedureNotePersist.updateProcedureNote(procedureNote).subscribe(response => {
          this.searchNotes();
        });
      }
    });
  }

  canUpdateNote(item: CommentDetail): boolean {
    return this.tcAuthorization.isSysAdmin() || item.commented_by == TCAppInit.userInfo.user_id
  }

  deleteNote(item: CommentDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedureNotePersist.deleteProcedureNote(item.id).subscribe(response => {
          this.tcNotification.success("note deleted");
          this.searchNotes();
        }, error => {
        });
      }

    });
  }

  archiveNote(item: CommentDetail): void {
    this.procedureNotePersist.procedureNoteDo(item.id, "archive", {}).subscribe(response => {
      this.tcNotification.success("note archived");
      this.searchNotes();
    }, error => {
    });
  }

  back():void{
      this.location.back();
    }

}
