import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {ProcedureDetail, ProcedureSummaryPartialList} from "../procedure.model";
import {ProcedurePersist} from "../procedure.persist";
import { Procedure_TypePersist } from 'src/app/procedure_types/procedure_type.persist';
import { Procedure_TypeSummary, Procedure_TypeSummaryPartialList } from 'src/app/procedure_types/procedure_type.model';

import { Procedure_TypeNavigator } from 'src/app/procedure_types/procedure_type.navigator';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { procedure_type } from '../../app.enums'
import { FeeByServiceType, FeeDetail } from 'src/app/fees/fee.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';


@Component({
  selector: 'app-procedure-edit',
  templateUrl: './procedure-edit.component.html',
  styleUrls: ['./procedure-edit.component.css']
})
export class ProcedureEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  procedureDetail: ProcedureDetail;
  procedureTypes: Procedure_TypeSummary[] = [];
  procedureType: string = "";
  takeDuration: boolean = false;
  timeType: number = 100;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ProcedureEditComponent>,
              public  persist: ProcedurePersist,
              public procedure_typePersist: Procedure_TypePersist,
              public procedureTypeNavigator: Procedure_TypeNavigator,
              public tcUtilsArray: TCUtilsArray,
              public feePersist: FeePersist,
              public encounterPersist: Form_EncounterPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("procedures");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("procedure","procedure");
      this.procedureDetail = new ProcedureDetail();
      this.procedureDetail.fees = new FeeByServiceType()
      this.procedureDetail.needs_consent_form = false;
      this.procedureDetail.duration = 0;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("procedures");
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("procedure","procedure");
      this.isLoadingResults = true;
      this.persist.getProcedure(this.idMode.id).subscribe(procedureDetail => {
        this.procedureDetail = procedureDetail;
        const feeSearchHistory = {...this.feePersist.defaultFeeSearchHistory}
        feeSearchHistory.target_id = this.procedureDetail.id
        this.feePersist.searchFee(5, 0, 'id', 'asc').subscribe(
          fee => {
            const fees = new FeeByServiceType()
            fee.data.forEach(
              fee => {
                fees[this.tcUtilsArray.getEnum(this.encounterPersist.serviceTypeWithAll, fee.service_type )?.name.toLowerCase() ] = fee.price
              }
            )
            this.procedureDetail.fees = fees
          }
        )
        if(this.procedureDetail.duration){
          this.takeDuration = true;
        }
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    if (this.takeDuration && this.timeType == 101){
        this.procedureDetail.duration *= 60;
    }
    this.persist.addProcedure(this.procedureDetail).subscribe(value => {
      this.tcNotification.success("Procedure added");
      this.procedureDetail.id = value.id;
      this.dialogRef.close(this.procedureDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  ngOnDestroy(){
    // this.feePersist.targetId = null;
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    if (this.takeDuration && this.timeType == 101){
      this.procedureDetail.duration *= 60;
  }
    this.persist.updateProcedure(this.procedureDetail).subscribe(value => {
      this.tcNotification.success("Procedure updated");
      this.dialogRef.close(this.procedureDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  searchProcedureType() {
    let dialogRef = this.procedureTypeNavigator.pickProcedure_Types(true);
    dialogRef.afterClosed().subscribe((result: Procedure_TypeSummary[]) => {
      if (result) {
        if(result[0].type = procedure_type.major){
          this.takeDuration = true;
        }
        this.procedureType = result[0].name
        this.procedureDetail.procedure_type_id = result[0].id;
      }
    });
  }


  canSubmit():boolean{
        if (this.procedureDetail == null){
            return false;
          }

        if (this.procedureDetail.name == null || this.procedureDetail.name  == "") {
                      return false;
                    }
                    
        if (this.takeDuration && this.procedureDetail.duration == 0){
          return false;
        }

        return true;
      }


}
