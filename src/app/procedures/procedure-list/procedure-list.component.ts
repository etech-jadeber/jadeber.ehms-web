import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {ProcedurePersist} from "../procedure.persist";
import {ProcedureNavigator} from "../procedure.navigator";
import {ProcedureDetail, ProcedureNoteSummary, ProcedureSummary, ProcedureSummaryPartialList} from "../procedure.model";
import { Procedure_TypePersist } from 'src/app/procedure_types/procedure_type.persist';
import { Procedure_TypeSummary, Procedure_TypeSummaryPartialList } from 'src/app/procedure_types/procedure_type.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { ProcedureNotePersist } from 'src/app/patients/procedureNote.persist';


@Component({
  selector: 'app-procedure-list',
  templateUrl: './procedure-list.component.html',
  styleUrls: ['./procedure-list.component.css']
})
export class ProcedureListComponent implements OnInit {

  proceduresData: ProcedureSummary[] = [];
  proceduresTotalCount: number = 0;
  proceduresSelectAll:boolean = false;
  proceduresSelection: ProcedureSummary[] = [];

  proceduresDisplayedColumns: string[] = ["select","action", 'name', "procedure_type_id","needs_consent_form", ];
  proceduresSearchTextBox: FormControl = new FormControl();
  proceduresIsLoading: boolean = false;
  procedureTypes: Procedure_TypeSummary[] = [];

  @ViewChild(MatPaginator, {static: true}) proceduresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) proceduresSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedurePersist: ProcedurePersist,
                public procedureNavigator: ProcedureNavigator,
                public jobPersist: JobPersist,
                public procedure_typePersist:Procedure_TypePersist,
                public patientPersist: PatientPersist,
                public procudureNotePersist: ProcedureNotePersist,
    ) {

        this.tcAuthorization.requireRead("procedures");
       this.proceduresSearchTextBox.setValue(procedurePersist.procedureSearchText);
      //delay subsequent keyup events
      this.proceduresSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.procedurePersist.procedureSearchText = value;
        this.searchProcedures();
      });
    }

    ngOnInit() {
      this.loadProcedureTypes();
      this.proceduresSort.sortChange.subscribe(() => {
        this.proceduresPaginator.pageIndex = 0;
        this.searchProcedures(true);
      });

      this.proceduresPaginator.page.subscribe(() => {
        this.searchProcedures(true);
      });
      //start by loading items
      this.searchProcedures();
    }

  searchProcedures(isPagination:boolean = false): void {


    let paginator = this.proceduresPaginator;
    let sorter = this.proceduresSort;

    this.proceduresIsLoading = true;
    this.proceduresSelection = [];

    this.procedurePersist.searchProcedure(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ProcedureSummaryPartialList) => {
      this.proceduresData = partialList.data;
      if (partialList.total != -1) {
        this.proceduresTotalCount = partialList.total;
      }
      this.proceduresIsLoading = false;
    }, error => {
      this.proceduresIsLoading = false;
    });

  }

  downloadProcedures(): void {
    if(this.proceduresSelectAll){
         this.procedurePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download procedures", true);
      });
    }
    else{
        this.procedurePersist.download(this.tcUtilsArray.idsList(this.proceduresSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download procedures",true);
            });
        }
  }

  addProcedure(): void {
    let dialogRef = this.procedureNavigator.addProcedure();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchProcedures();
      }
    });
  }

  editProcedure(item: ProcedureSummary) {
    let dialogRef = this.procedureNavigator.editProcedure(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteProcedure(item: ProcedureSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Procedure");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.procedurePersist.deleteProcedure(item.id).subscribe(response => {
          this.tcNotification.success("Procedure deleted");
          this.searchProcedures();
        }, error => {
        });
      }

    });
  }

  loadProcedureTypes(): void {
    this.procedure_typePersist.searchProcedure_Type(50,0, "", "").subscribe((result: Procedure_TypeSummaryPartialList) => {
      this.procedureTypes = result.data;
    }, error => {
    })
  }

  addNote(item: ProcedureSummary): void {
    let modalRef = this.tcNavigator.textInput(this.appTranslation.getText("notes", "note"), "", true);
    modalRef.afterClosed().subscribe(note => {
      if (note) {
        const procedureNote = new ProcedureNoteSummary()
        this.procudureNotePersist.addProcedureNote(procedureNote).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("notes", "note_added"));
          this.searchProcedures();
        });
      }
    });
  }


  back():void{
      this.location.back();
    }

}
