import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ProcedurePickComponent } from './procedure-pick.component';

describe('ProcedurePickComponent', () => {
  let component: ProcedurePickComponent;
  let fixture: ComponentFixture<ProcedurePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcedurePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedurePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
