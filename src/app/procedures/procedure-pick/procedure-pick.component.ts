import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {ProcedureDetail, ProcedureSummary, ProcedureSummaryPartialList} from "../procedure.model";
import {ProcedurePersist} from "../procedure.persist";
import { Procedure_TypePersist } from 'src/app/procedure_types/procedure_type.persist';
import { Procedure_TypeDetail } from 'src/app/procedure_types/procedure_type.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { FeeDetail } from 'src/app/fees/fee.model';
import { fee_type } from 'src/app/app.enums';
import { TCUtilsNumber } from 'src/app/tc/utils-number';


@Component({
  selector: 'app-procedure-pick',
  templateUrl: './procedure-pick.component.html',
  styleUrls: ['./procedure-pick.component.css']
})
export class ProcedurePickComponent implements OnInit {

  proceduresData: ProcedureSummary[] = [];
  proceduresTotalCount: number = 0;
  proceduresSelection: ProcedureSummary[] = [];
  proceduresDisplayedColumns: string[] = ["select", 'name',  'procedure_type_id','needs_consent_form', 'price' ];

  proceduresSearchTextBox: FormControl = new FormControl();
  proceduresIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) proceduresPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) proceduresSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public procedurePersist: ProcedurePersist,
              public procedureTypePersist: Procedure_TypePersist,
              public feePersist: FeePersist,
              public tcUtilsNumber: TCUtilsNumber,
              public dialogRef: MatDialogRef<ProcedurePickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, service_type: number, status: number}
  ) {
    this.tcAuthorization.requireRead("procedures");
    this.proceduresSearchTextBox.setValue(procedurePersist.procedureSearchText);
    //delay subsequent keyup events
    this.proceduresSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.procedurePersist.procedureSearchText = value;
      this.searchProcedures();
    });
    this.procedurePersist.status = this.data.status
  }

  ngOnInit() {

    this.proceduresSort.sortChange.subscribe(() => {
      this.proceduresPaginator.pageIndex = 0;
      this.searchProcedures();
    });

    this.proceduresPaginator.page.subscribe(() => {
      this.searchProcedures();
    });

    //set initial picker list to 5
    this.proceduresPaginator.pageSize = 5;

    //start by loading items
    this.searchProcedures();
  }

  searchProcedures(): void {
    this.proceduresIsLoading = true;

    this.procedurePersist.searchProcedure(this.proceduresPaginator.pageSize,
        this.proceduresPaginator.pageIndex,
        this.proceduresSort.active,
        this.proceduresSort.direction).subscribe((partialList: ProcedureSummaryPartialList) => {
      this.proceduresData = partialList.data;
      this.proceduresData.forEach((procedure: ProcedureDetail, idx) => {
        this.procedureTypePersist.getProcedure_Type(procedure.procedure_type_id).subscribe((proceduretype: Procedure_TypeDetail) => {
          this.proceduresData[idx].procedure_type_name = proceduretype.name;
        })
        this.feePersist.feesDo("get_fee", {target_id: procedure.id, fee_type: fee_type.procedure, service_type: this.data.service_type}).subscribe((fee: FeeDetail) => {
          this.proceduresData[idx].price = this.tcUtilsNumber.convertToDecimalPlaces(fee.price);
        })
      })
      if (partialList.total != -1) {
        this.proceduresTotalCount = partialList.total;
      }
      this.proceduresIsLoading = false;
    }, error => {
      this.proceduresIsLoading = false;
    });

  }

  markOneItem(item: ProcedureSummary) {
    if(!this.tcUtilsArray.containsId(this.proceduresSelection,item.id)){
          this.proceduresSelection = [];
          this.proceduresSelection.push(item);
        }
        else{
          this.proceduresSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.proceduresSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
