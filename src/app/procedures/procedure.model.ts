import { FeeByServiceType, FeeDetail } from "../fees/fee.model";
import {TCId} from "../tc/models";

export class ProcedureSummary extends TCId {
  procedure_type_id : string;
name : string;
needs_consent_form : boolean;
procedure_type_name: string;
duration: number;
total: number;
hospital: number;
referral: number;
fees: FeeByServiceType;
procedure_for: number;
price: number;
status: number;
}

export class ProcedureNoteSummaryPartialList {
  data: ProcedureNoteSummary[];
  total: number;
}

export class ProcedureNoteSummary extends TCId {
  provider_id : string;
patient_id : string;
encounter_id : string;
patient_procedure_id: string;
note: string;
date: number;
}

export class ProcedureSummaryPartialList {
  data: ProcedureSummary[];
  total: number;
}

export class ProcedureDetail extends ProcedureSummary {
  procedure_type_id : string;
name : string;
needs_consent_form : boolean;
procedure_type_name: string;
}

export class ProcedureDashboard {
  total: number;
}
