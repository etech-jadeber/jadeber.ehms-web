import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog,MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ProcedureEditComponent} from "./procedure-edit/procedure-edit.component";
import {ProcedurePickComponent} from "./procedure-pick/procedure-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ProcedureNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  proceduresUrl(): string {
    return "/procedures";
  }

  procedureUrl(id: string): string {
    return "/procedures/" + id;
  }
  procedureNoteUrl(id:string):string{
    return "/patient_procedure_notes/" + id;
  }
  viewProcedures(): void {
    this.router.navigateByUrl(this.proceduresUrl());
  }

  viewProcedure(id: string): void {
    this.router.navigateByUrl(this.procedureUrl(id));
  }

  editProcedure(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ProcedureEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addProcedure(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ProcedureEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickProcedures(selectOne: boolean=false, service_type: number = null, status: number = null): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(ProcedurePickComponent, {
        data: {selectOne, service_type, status},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
