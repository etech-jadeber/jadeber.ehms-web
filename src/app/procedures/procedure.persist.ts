import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {ProcedureDashboard, ProcedureDetail, ProcedureSummaryPartialList} from "./procedure.model";
import { procedure_for } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class ProcedurePersist {

  procedureSearchText: string = "";
  from_date: number;
  to_date: number;
  procedureType: string;
  status: number;

  time_type: TCEnum[] = [
    new TCEnum(100, "Min"),
    new TCEnum(101, "Hr"),
  ];

  procedureFor: TCEnum[] = [
    new TCEnum(procedure_for.diagnostic, "Diagnostic"),
    new TCEnum(procedure_for.therapeutic, "Therapeutic"),
  ];

  constructor(private http: HttpClient) {
  }

  searchProcedure(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ProcedureSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("procedures", this.procedureSearchText, pageSize, pageIndex, sort, order);
    if (this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    return this.http.get<ProcedureSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.procedureSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedures/do", new TCDoParam("download_all", this.filters()));
  }

  procedureDashboard(): Observable<ProcedureDashboard> {
    return this.http.get<ProcedureDashboard>(environment.tcApiBaseUri + "procedures/dashboard");
  }

  getProcedure(id: string): Observable<ProcedureDetail> {
    return this.http.get<ProcedureDetail>(environment.tcApiBaseUri + "procedures/" + id);
  }

  addProcedure(item: ProcedureDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "procedures/", item);
  }

  updateProcedure(item: ProcedureDetail): Observable<ProcedureDetail> {
    return this.http.patch<ProcedureDetail>(environment.tcApiBaseUri + "procedures/" + item.id, item);
  }

  deleteProcedure(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "procedures/" + id);
  }

  proceduresDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("procedures/do", this.procedureSearchText, pageSize, pageIndex, sort, order)
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  procedureDo(id: string, method: string, payload: any): Observable<{can_see_notes: boolean}> {
    return this.http.post<{can_see_notes: boolean}>(environment.tcApiBaseUri + "procedures/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "procedures/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "procedures/" + id + "/do", new TCDoParam("print", {}));
  }


}
