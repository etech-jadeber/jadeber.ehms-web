import {Component, OnInit} from '@angular/core';
import {TCAuthorization} from "../../tc/authorization";
import {TCAuthentication} from "../../tc/authentication";
import {AppTranslation} from "../../app.translation";
import {TCNotification} from "../../tc/notification";
import {Passwords} from "../../tc/users/user.model";
import {UserPersist} from "../../tc/users/user.persist";
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.css']
})
export class PasswordChangeComponent implements OnInit {

  pwds: Passwords = new Passwords();

  constructor(
    public tcNotification: TCNotification,
    public dialogRef: MatDialogRef<PasswordChangeComponent>,
    public appTranslation: AppTranslation,
    public persist: UserPersist) {
  }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  pwdChange(): void {
    if (this.pwds.pwd_new != this.pwds.pwd_repeat) {
      this.tcNotification.error(this.appTranslation.getText("users", "password_notmatch"));
      return;
    }
    this.persist.pwdChange(this.pwds).subscribe(result => {
      this.tcNotification.success(this.appTranslation.getText('users', 'password_change_success'));
      this.dialogRef.close();
    });
  }
}
