import {Component, OnInit} from '@angular/core';
import {AppTranslation} from "../app.translation";
import {TCNotification} from "../tc/notification";
import {IdentitySources, TCAuthentication, TCMandatory} from "../tc/authentication";
import {TCAuthorization} from "../tc/authorization";
import {TCNavigator} from "../tc/navigator";
import {TCUtilsArray} from "../tc/utils-array";
import {UserPersist} from "../tc/users/user.persist";
import {Location} from '@angular/common';
import { TCId } from '../tc/models';
import { UserDetail } from '../tc/users/user.model';
import { TCUtilsDate } from '../tc/utils-date';
import { TCAppInit } from '../tc/app-init';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  TCMandatory: typeof TCMandatory = TCMandatory;
  IdentitySources: typeof IdentitySources = IdentitySources;
  userLoaded:boolean  = false;
  userDetail:UserDetail;
  TCAppInit = TCAppInit;

  constructor(private location: Location,
              public appTranslation: AppTranslation,
              public tcNotification: TCNotification,
              public tcAuthentication: TCAuthentication,
              public tcNavigator: TCNavigator,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate:TCUtilsDate,
              public tcAuthorization: TCAuthorization,
              public userPersist: UserPersist) {

    this.tcAuthorization.requireLogin();
  }

  ngOnInit() {

  }



  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }

  passwordChange(): void {
    let dialogRef = this.tcNavigator.passwordChange();
    dialogRef.afterClosed().subscribe(response => {

    });
  }

  back(): void {
    this.location.back();
  }

  reload() {

  }

  otpEnable(): void {
    this.userPersist.otpEnable(TCAppInit.userInfo.user_id).subscribe(response => {
      if (response) {
        this.userDetail.otp = (<TCId>response).id;
      }
    });
  }

  otpReset(): void {
    this.userPersist.otpReset(TCAppInit.userInfo.user_id).subscribe(response => {
      if (response) {
        this.userDetail.otp = (<TCId>response).id;
      }
    });
  }

  otpDisable(): void {
    this.userPersist.otpDisable(TCAppInit.userInfo.user_id).subscribe(response => {
      this.userDetail.otp = "";
    });
  }



}