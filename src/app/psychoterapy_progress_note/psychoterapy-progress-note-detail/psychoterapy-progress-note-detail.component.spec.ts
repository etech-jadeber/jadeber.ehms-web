import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsychoterapyProgressNoteDetailComponent } from './psychoterapy-progress-note-detail.component';

describe('PsychoterapyProgressNoteDetailComponent', () => {
  let component: PsychoterapyProgressNoteDetailComponent;
  let fixture: ComponentFixture<PsychoterapyProgressNoteDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsychoterapyProgressNoteDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsychoterapyProgressNoteDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
