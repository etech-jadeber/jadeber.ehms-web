import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatSort } from "@angular/material/sort";
import { MatPaginator } from "@angular/material/paginator";
import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { PsychoterapyProgressNoteDetail } from "../psychoterapy_progress_note.model";
import { PsychoterapyProgressNotePersist } from "../psychoterapy_progress_note.persist";
import { PsychoterapyProgressNoteNavigator } from "../psychoterapy_progress_note.navigator";

export enum PsychoterapyProgressNoteTabs {
  overview,
}

export enum PaginatorIndexes {

} @Component({
  selector: 'app-psychoterapy_progress_note-detail',
  templateUrl: './psychoterapy-progress-note-detail.component.html',
  styleUrls: ['./psychoterapy-progress-note-detail.component.css']
})
export class PsychoterapyProgressNoteDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  psychoterapyProgressNoteIsLoading: boolean = false;

  PsychoterapyProgressNoteTabs: typeof PsychoterapyProgressNoteTabs = PsychoterapyProgressNoteTabs;
  activeTab: PsychoterapyProgressNoteTabs = PsychoterapyProgressNoteTabs.overview;
  //basics
  psychoterapyProgressNoteDetail: PsychoterapyProgressNoteDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public psychoterapyProgressNoteNavigator: PsychoterapyProgressNoteNavigator,
    public psychoterapyProgressNotePersist: PsychoterapyProgressNotePersist) {
    this.tcAuthorization.requireRead("psychotherapy_progress_notes");
    this.psychoterapyProgressNoteDetail = new PsychoterapyProgressNoteDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("psychotherapy_progress_notes");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.psychoterapyProgressNoteIsLoading = true;
    this.psychoterapyProgressNotePersist.getPsychoterapyProgressNote(id).subscribe(psychoterapyProgressNoteDetail => {
      this.psychoterapyProgressNoteDetail = psychoterapyProgressNoteDetail;
      this.psychoterapyProgressNoteIsLoading = false;
    }, error => {
      console.error(error);
      this.psychoterapyProgressNoteIsLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.psychoterapyProgressNoteDetail.id);
  }
  editPsychoterapyProgressNote(): void {
    let modalRef = this.psychoterapyProgressNoteNavigator.editPsychoterapyProgressNote(this.psychoterapyProgressNoteDetail.id);
    modalRef.afterClosed().subscribe(psychoterapyProgressNoteDetail => {
      TCUtilsAngular.assign(this.psychoterapyProgressNoteDetail, psychoterapyProgressNoteDetail);
    }, error => {
      console.error(error);
    });
  }

  printBirth(): void {
    this.psychoterapyProgressNotePersist.print(this.psychoterapyProgressNoteDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print psychoterapy_progress_note", true);
    });
  }

  back(): void {
    this.location.back();
  }

}
