import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsychoterapyProgressNoteEditComponent } from './psychoterapy-progress-note-edit.component';

describe('PsychoterapyProgressNoteEditComponent', () => {
  let component: PsychoterapyProgressNoteEditComponent;
  let fixture: ComponentFixture<PsychoterapyProgressNoteEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsychoterapyProgressNoteEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsychoterapyProgressNoteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
