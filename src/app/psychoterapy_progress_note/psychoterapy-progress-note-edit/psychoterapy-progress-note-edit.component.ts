import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from "../../tc/authorization";
import { TCNotification } from "../../tc/notification";
import { TCUtilsString } from "../../tc/utils-string";
import { AppTranslation } from "../../app.translation";

import { TCIdMode, TCModalModes } from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PsychoterapyProgressNoteDetail } from '../psychoterapy_progress_note.model'; import { PsychoterapyProgressNotePersist } from '../psychoterapy_progress_note.persist'; @Component({
  selector: 'app-psychoterapy_progress_note-edit',
  templateUrl: './psychoterapy-progress-note-edit.component.html',
  styleUrls: ['./psychoterapy-progress-note-edit.component.css']
}) export class PsychoterapyProgressNoteEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  psychoterapyProgressNoteDetail: PsychoterapyProgressNoteDetail; constructor(public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<PsychoterapyProgressNoteEditComponent>,
    public persist: PsychoterapyProgressNotePersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  } ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("psychotherapy_progress_notes");
      this.title = this.appTranslation.getText("general", "new") + " " + "psychoterapy_progress_note";
      this.psychoterapyProgressNoteDetail = new PsychoterapyProgressNoteDetail();
      //set necessary defaults

    } else {
      this.tcAuthorization.requireUpdate("psychotherapy_progress_notes");
      this.title = this.appTranslation.getText("general", "edit") + " " + " " + "psychoterapy_progress_note";
      this.isLoadingResults = true;
      this.persist.getPsychoterapyProgressNote(this.idMode.id).subscribe(psychoterapyProgressNoteDetail => {
        this.psychoterapyProgressNoteDetail = psychoterapyProgressNoteDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  } onAdd(): void {
    this.isLoadingResults = true;

    this.psychoterapyProgressNoteDetail.patient_id = this.persist.patient_id
    this.psychoterapyProgressNoteDetail.provider_id = this.persist.provider_id
    console.log(this.psychoterapyProgressNoteDetail)
    this.persist.addPsychoterapy_progress_note(this.psychoterapyProgressNoteDetail).subscribe(value => {
      this.tcNotification.success("psychoterapyProgressNote added");
      this.psychoterapyProgressNoteDetail.id = value.id;
      this.dialogRef.close(this.psychoterapyProgressNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updatePsychoterapyProgressNote(this.psychoterapyProgressNoteDetail).subscribe(value => {
      this.tcNotification.success("psychoterapy_progress_note updated");
      this.dialogRef.close(this.psychoterapyProgressNoteDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  } canSubmit(): boolean {
    if (this.psychoterapyProgressNoteDetail == null) {
      return false;
    }

    return true

  }
}
