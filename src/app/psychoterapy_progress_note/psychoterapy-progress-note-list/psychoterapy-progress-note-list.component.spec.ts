import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsychoterapyProgressNoteListComponent } from './psychoterapy-progress-note-list.component';

describe('PsychoterapyProgressNoteListComponent', () => {
  let component: PsychoterapyProgressNoteListComponent;
  let fixture: ComponentFixture<PsychoterapyProgressNoteListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsychoterapyProgressNoteListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsychoterapyProgressNoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
