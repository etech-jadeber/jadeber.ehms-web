import { Component, Input ,OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { PsychoterapyProgressNoteSummary, PsychoterapyProgressNoteSummaryPartialList } from '../psychoterapy_progress_note.model';
import { PsychoterapyProgressNotePersist } from '../psychoterapy_progress_note.persist';
import { PsychoterapyProgressNoteNavigator } from '../psychoterapy_progress_note.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
@Component({
  selector: 'app-psychoterapy_progress_note-list',
  templateUrl: './psychoterapy-progress-note-list.component.html',
  styleUrls: ['./psychoterapy-progress-note-list.component.css']
}) export class PsychoterapyProgressNoteListComponent implements OnInit {
  psychoterapyProgressNotesData: PsychoterapyProgressNoteSummary[] = [];
  psychoterapyProgressNotesTotalCount: number = 0;
  psychoterapyProgressNoteSelectAll: boolean = false;
  psychoterapyProgressNoteSelection: PsychoterapyProgressNoteSummary[] = [];
  @Input() encounterId: string;
  @Input() patient_id: string;
  @Input() provider_id: string;
  doctors: { [id: string]: DoctorDetail } = {}


  psychoterapyProgressNotesDisplayedColumns: string[] = ["select", "action", "provider_id", "date", "diagnosis", "thoughts", "therapy_given", "home_work" ,"appointment"];
  psychoterapyProgressNoteSearchTextBox: FormControl = new FormControl();
  psychoterapyProgressNoteIsLoading: boolean = false; @ViewChild(MatPaginator, { static: true }) psychoterapyProgressNotesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) psychoterapyProgressNotesSort: MatSort; constructor(private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public doctorPersist: DoctorPersist,
    public psychoterapyProgressNotePersist: PsychoterapyProgressNotePersist,
    public psychoterapyProgressNoteNavigator: PsychoterapyProgressNoteNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,

  ) {

    this.tcAuthorization.requireRead("psychotherapy_progress_notes");
    this.psychoterapyProgressNoteSearchTextBox.setValue(psychoterapyProgressNotePersist.psychoterapyProgressNoteSearchText);
    //delay subsequent keyup events
    this.psychoterapyProgressNoteSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.psychoterapyProgressNotePersist.psychoterapyProgressNoteSearchText = value;
      this.searchPsychoterapy_progress_notes();
    });
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
      this.psychoterapyProgressNotePersist.encounterId = this.encounterId;
    } else {
      this.psychoterapyProgressNotePersist.encounterId = null;
    }
    this.searchPsychoterapy_progress_notes()
  }

  ngOnInit() {
    this.psychoterapyProgressNotePersist.provider_id = this.provider_id
    this.psychoterapyProgressNotePersist.patient_id = this.patient_id

    this.psychoterapyProgressNotesSort.sortChange.subscribe(() => {
      this.psychoterapyProgressNotesPaginator.pageIndex = 0;
      this.searchPsychoterapy_progress_notes(true);
    });

    this.psychoterapyProgressNotesPaginator.page.subscribe(() => {
      this.searchPsychoterapy_progress_notes(true);
    });
    //start by loading items
    this.searchPsychoterapy_progress_notes();
  }

  searchPsychoterapy_progress_notes(isPagination: boolean = false): void {


    let paginator = this.psychoterapyProgressNotesPaginator;
    let sorter = this.psychoterapyProgressNotesSort;

    this.psychoterapyProgressNoteIsLoading = true;
    this.psychoterapyProgressNoteSelection = [];

    this.psychoterapyProgressNotePersist.searchPsychoterapyProgressNote(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: PsychoterapyProgressNoteSummaryPartialList) => {
      this.psychoterapyProgressNotesData = partialList.data;
      this.psychoterapyProgressNotesData.forEach((d) => {
        if (!this.doctors[d.provider_id]) {
          this.doctors[d.provider_id] = new DoctorDetail()
          this.doctorPersist.getDoctor(d.provider_id).subscribe(
            doctor => this.doctors[d.provider_id] = doctor
          )
        }
      })
      if (partialList.total != -1) {
        this.psychoterapyProgressNotesTotalCount = partialList.total;
      }
      this.psychoterapyProgressNoteIsLoading = false;
    }, error => {
      this.psychoterapyProgressNoteIsLoading = false;
    });

  } downloadPsychoterapyProgressNotes(): void {
    if (this.psychoterapyProgressNoteSelectAll) {
      this.psychoterapyProgressNotePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download psychoterapy_progress_note", true);
      });
    }
    else {
      this.psychoterapyProgressNotePersist.download(this.tcUtilsArray.idsList(this.psychoterapyProgressNoteSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download psychoterapy_progress_note", true);
      });
    }
  }
  addPsychoterapy_progress_note(): void {
    let dialogRef = this.psychoterapyProgressNoteNavigator.addPsychoterapyProgressNote();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPsychoterapy_progress_notes();
      }
    });
  }

  editPsychoterapyProgressNote(item: PsychoterapyProgressNoteSummary) {
    let dialogRef = this.psychoterapyProgressNoteNavigator.editPsychoterapyProgressNote(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePsychoterapyProgressNote(item: PsychoterapyProgressNoteSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("psychoterapy_progress_note");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.psychoterapyProgressNotePersist.deletePsychoterapyProgressNote(item.id).subscribe(response => {
          this.tcNotification.success("psychoterapy_progress_note deleted");
          this.searchPsychoterapy_progress_notes();
        }, error => {
        });
      }

    });
  } back(): void {
    this.location.back();
  }

  getDoctor(id: string)
  {
    if (this.doctors[id]) {
      const { first_name, middle_name, last_name } = this.doctors[id]
      return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
    }
  }
}
