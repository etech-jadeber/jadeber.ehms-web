import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PsychoterapyProgressNotePickComponent } from './psychoterapy-progress-note-pick.component';

describe('PsychoterapyProgressNotePickComponent', () => {
  let component: PsychoterapyProgressNotePickComponent;
  let fixture: ComponentFixture<PsychoterapyProgressNotePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PsychoterapyProgressNotePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PsychoterapyProgressNotePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
