import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from "@angular/forms";
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { debounceTime } from "rxjs/operators";
import { Location } from '@angular/common';

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCUtilsDate } from "../../tc/utils-date";
import { TCNotification } from "../../tc/notification";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";

import { AppTranslation } from "../../app.translation";
import { PsychoterapyProgressNoteSummary, PsychoterapyProgressNoteSummaryPartialList } from '../psychoterapy_progress_note.model';
import { PsychoterapyProgressNotePersist } from '../psychoterapy_progress_note.persist';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PsychoterapyProgressNoteNavigator } from '../psychoterapy_progress_note.navigator';
@Component({
  selector: 'app-psychoterapy_progress_note-pick',
  templateUrl: './psychoterapy-progress-note-pick.component.html',
  styleUrls: ['./psychoterapy-progress-note-pick.component.css']
})

export class PsychoterapyProgressNotePickComponent implements OnInit {
  psychoterapyProgressNotesData: PsychoterapyProgressNoteSummary[] = [];
  psychoterapyProgressNotesTotalCount: number = 0;
  psychoterapyProgressNoteSelectAll: boolean = false;
  psychoterapyProgressNoteSelection: PsychoterapyProgressNoteSummary[] = [];

  psychoterapyProgressNotesDisplayedColumns: string[] = ["select", "patient_id", "provider_id", "date", "diagnosis", "thoughts", "therapy_given", "home_work", "appointment"];
  psychoterapyProgressNoteSearchTextBox: FormControl = new FormControl();
  psychoterapyProgressNoteIsLoading: boolean = false; @ViewChild(MatPaginator, { static: true }) psychoterapyProgressNotesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) psychoterapyProgressNotesSort: MatSort; constructor(public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public psychoterapyProgressNotePersist: PsychoterapyProgressNotePersist,
    public psychoterapyProgressNoteNavigator: PsychoterapyProgressNoteNavigator,
    public dialogRef: MatDialogRef<PsychoterapyProgressNotePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    this.tcAuthorization.requireRead("psychotherapy_progress_notes");
    this.psychoterapyProgressNoteSearchTextBox.setValue(psychoterapyProgressNotePersist.psychoterapyProgressNoteSearchText);
    //delay subsequent keyup events
    this.psychoterapyProgressNoteSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.psychoterapyProgressNotePersist.psychoterapyProgressNoteSearchText = value;
      this.searchPsychoterapy_progress_notes();
    });
  } ngOnInit() {

    this.psychoterapyProgressNotesSort.sortChange.subscribe(() => {
      this.psychoterapyProgressNotesPaginator.pageIndex = 0;
      this.searchPsychoterapy_progress_notes(true);
    });

    this.psychoterapyProgressNotesPaginator.page.subscribe(() => {
      this.searchPsychoterapy_progress_notes(true);
    });
    //start by loading items
    this.searchPsychoterapy_progress_notes();
  }

  searchPsychoterapy_progress_notes(isPagination: boolean = false): void {


    let paginator = this.psychoterapyProgressNotesPaginator;
    let sorter = this.psychoterapyProgressNotesSort;

    this.psychoterapyProgressNoteIsLoading = true;
    this.psychoterapyProgressNoteSelection = [];

    this.psychoterapyProgressNotePersist.searchPsychoterapyProgressNote(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: PsychoterapyProgressNoteSummaryPartialList) => {
      this.psychoterapyProgressNotesData = partialList.data;
      if (partialList.total != -1) {
        this.psychoterapyProgressNotesTotalCount = partialList.total;
      }
      this.psychoterapyProgressNoteIsLoading = false;
    }, error => {
      this.psychoterapyProgressNoteIsLoading = false;
    });

  }
  markOneItem(item: PsychoterapyProgressNoteSummary) {
    if (!this.tcUtilsArray.containsId(this.psychoterapyProgressNoteSelection, item.id)) {
      this.psychoterapyProgressNoteSelection = [];
      this.psychoterapyProgressNoteSelection.push(item);
    }
    else {
      this.psychoterapyProgressNoteSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.psychoterapyProgressNoteSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
}
