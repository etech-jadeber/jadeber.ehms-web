import { TCId } from "../tc/models"; export class PsychoterapyProgressNoteSummary extends TCId {
    patient_id: string;
    provider_id: string;
    date?: number;
    diagnosis: string;
    thoughts: string;
    therapy_given: string;
    home_work: string;
    appointment: string;

}
export class PsychoterapyProgressNoteSummaryPartialList {
    data: PsychoterapyProgressNoteSummary[];
    total: number;
}
export class PsychoterapyProgressNoteDetail extends PsychoterapyProgressNoteSummary {
}

export class PsychoterapyProgressNoteDashboard {
    total: number;
}