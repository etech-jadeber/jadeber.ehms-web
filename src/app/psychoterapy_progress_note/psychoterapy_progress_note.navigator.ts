import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from "../tc/utils-angular";
import { TCIdMode, TCModalModes } from "../tc/models";

import { PsychoterapyProgressNoteEditComponent } from "./psychoterapy-progress-note-edit/psychoterapy-progress-note-edit.component";
import { PsychoterapyProgressNotePickComponent } from "./psychoterapy-progress-note-pick/psychoterapy-progress-note-pick.component";


@Injectable({
    providedIn: 'root'
})

export class PsychoterapyProgressNoteNavigator {

    constructor(private router: Router,
        public dialog: MatDialog) {
    }
    psychoterapy_progress_notesUrl(): string {
        return "/psychoterapy_progress_notes";
    }

    psychoterapy_progress_noteUrl(id: string): string {
        return "/psychoterapy_progress_notes/" + id;
    }

    viewPsychoterapyProgressNotes(): void {
        this.router.navigateByUrl(this.psychoterapy_progress_notesUrl());
    }

    viewPsychoterapyProgressNote(id: string): void {
        this.router.navigateByUrl(this.psychoterapy_progress_noteUrl(id));
    }

    editPsychoterapyProgressNote(id: string): MatDialogRef<PsychoterapyProgressNoteEditComponent> {
        const dialogRef = this.dialog.open(PsychoterapyProgressNoteEditComponent, {
            data: new TCIdMode(id, TCModalModes.EDIT),
            width: TCModalWidths.medium
        });
        return dialogRef;
    }

    addPsychoterapyProgressNote(): MatDialogRef<PsychoterapyProgressNoteEditComponent> {
        const dialogRef = this.dialog.open(PsychoterapyProgressNoteEditComponent, {
            data: new TCIdMode(null, TCModalModes.NEW),
            width: TCModalWidths.medium
        });
        return dialogRef;
    }

    pickPsychoterapyProgressNotes(selectOne: boolean = false): MatDialogRef<PsychoterapyProgressNotePickComponent> {
        const dialogRef = this.dialog.open(PsychoterapyProgressNotePickComponent, {
            data: selectOne,
            width: TCModalWidths.large
        });
        return dialogRef;
    }
}