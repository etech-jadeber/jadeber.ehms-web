import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";


import { TCDoParam, TCUrlParams, TCUtilsHttp } from "../tc/utils-http";
import { TCId, TcDictionary } from "../tc/models";
import { PsychoterapyProgressNoteDashboard, PsychoterapyProgressNoteDetail, PsychoterapyProgressNoteSummaryPartialList } from "./psychoterapy_progress_note.model";
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
    providedIn: 'root'
})
export class PsychoterapyProgressNotePersist {
    psychoterapyProgressNoteSearchText: string = "";
    encounterId: string;
    patient_id: string;
    provider_id: string;


    constructor(private http: HttpClient) {
    }
    print(id: string): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "psychoterapy_progress_note/" + id + "/do", new TCDoParam("print", {}));
    } filters(): any {
        let fltrs: TcDictionary<string> = new TcDictionary<string>();
        fltrs[TCUrlParams.searchText] = this.psychoterapyProgressNoteSearchText;
        //add custom filters
        return fltrs;
    }

    searchPsychoterapyProgressNote(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PsychoterapyProgressNoteSummaryPartialList> {

        let url = TCUtilsHttp.buildSearchUrl("psychoterapy_progress_note", this.psychoterapyProgressNoteSearchText, pageSize, pageIndex, sort, order);
        if(this.encounterId){
          url = TCUtilsString.appendUrlParameter(url, "encounter_id", this.encounterId)
        }
        if(this.patient_id){
          url = TCUtilsString.appendUrlParameter(url, "patient_id", this.patient_id)
        }
        return this.http.get<PsychoterapyProgressNoteSummaryPartialList>(url);

    } downloadAll(): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "psychoterapy_progress_note/do", new TCDoParam("download_all", this.filters()));
    }

    psychoterapyProgressNoteDashboard(): Observable<PsychoterapyProgressNoteDashboard> {
        return this.http.get<PsychoterapyProgressNoteDashboard>(environment.tcApiBaseUri + "psychoterapy_progress_note/dashboard");
    }

    getPsychoterapyProgressNote(id: string): Observable<PsychoterapyProgressNoteDetail> {
        return this.http.get<PsychoterapyProgressNoteDetail>(environment.tcApiBaseUri + "psychoterapy_progress_note/" + id);
    }

    download(ids: string[]): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "psychoterapy_progress_note/do", new TCDoParam("download", ids));
    }

    addPsychoterapy_progress_note(item: PsychoterapyProgressNoteDetail): Observable<TCId> {
        return this.http.post<TCId>(environment.tcApiBaseUri + "psychoterapy_progress_note/", item);
    }

    updatePsychoterapyProgressNote(item: PsychoterapyProgressNoteDetail): Observable<PsychoterapyProgressNoteDetail> {
        return this.http.patch<PsychoterapyProgressNoteDetail>(environment.tcApiBaseUri + "psychoterapy_progress_note/" + item.id, item);
    }

    deletePsychoterapyProgressNote(id: string): Observable<{}> {
        return this.http.delete(environment.tcApiBaseUri + "psychoterapy_progress_note/" + id);
    }
}
