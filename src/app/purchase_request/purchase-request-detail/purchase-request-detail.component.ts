import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PurchaseRequestDetail} from "../purchase_request.model";
import {PurchaseRequestPersist} from "../purchase_request.persist";
import {PurchaseRequestNavigator} from "../purchase_request.navigator";
import { UserPersist } from 'src/app/tc/users/user.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { RequestPersist } from 'src/app/requests/request.persist';

export enum PurchaseRequestTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-purchase_request-detail',
  templateUrl: './purchase-request-detail.component.html',
  styleUrls: ['./purchase-request-detail.component.scss']
})
export class PurchaseRequestDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  purchaseRequestIsLoading:boolean = false;
  
  PurchaseRequestTabs: typeof PurchaseRequestTabs = PurchaseRequestTabs;
  activeTab: PurchaseRequestTabs = PurchaseRequestTabs.overview;
  //basics
  purchaseRequestDetail: PurchaseRequestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public purchaseRequestNavigator: PurchaseRequestNavigator,
              public  purchaseRequestPersist: PurchaseRequestPersist,
              public requestPersist: RequestPersist,
              public userPersist: UserPersist,
              public itemPersist: ItemPersist) {
    this.tcAuthorization.requireRead("purchase_requests");
    this.purchaseRequestDetail = new PurchaseRequestDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("purchase_requests");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.purchaseRequestIsLoading = true;
    this.purchaseRequestPersist.getPurchaseRequest(id).subscribe(purchaseRequestDetail => {
          this.purchaseRequestDetail = purchaseRequestDetail;
          this.itemPersist.getItem(this.purchaseRequestDetail.item_id).subscribe(
            (item) => {
              this.purchaseRequestDetail.item_name = item.name;
            }
          )
          this.userPersist.getUser(this.purchaseRequestDetail.approver).subscribe(
            (user) => {
              this.purchaseRequestDetail.approver_name = user.name;
            }
          )
          this.userPersist.getUser(this.purchaseRequestDetail.requestor).subscribe(
            (user) => {
              this.purchaseRequestDetail.requester_name = user.name;
            }
          )
          this.purchaseRequestIsLoading = false;
        }, error => {
          console.error(error);
          this.purchaseRequestIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.purchaseRequestDetail.id);
  }
  editPurchaseRequest(): void {
    let modalRef = this.purchaseRequestNavigator.editPurchaseRequest(this.purchaseRequestDetail.id);
    modalRef.afterClosed().subscribe(purchaseRequestDetail => {
      TCUtilsAngular.assign(this.purchaseRequestDetail, purchaseRequestDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.purchaseRequestPersist.print(this.purchaseRequestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print purchase_request", true);
      });
    }

  back():void{
      this.location.back();
    }

}
