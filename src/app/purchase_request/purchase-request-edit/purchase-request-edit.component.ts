import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PurchaseRequestDetail } from '../purchase_request.model';import { PurchaseRequestPersist } from '../purchase_request.persist';import { ItemPersist } from 'src/app/items/item.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemDetail } from 'src/app/items/item.model';
import { RequestPersist } from 'src/app/requests/request.persist';
import { units } from 'src/app/app.enums';
import { StoreStockLevelPersist } from 'src/app/store_stock_level/store_stock_level.persist';
import { TCUtilsArray } from 'src/app/tc/utils-array';
@Component({
  selector: 'app-purchase_request-edit',
  templateUrl: './purchase-request-edit.component.html',
  styleUrls: ['./purchase-request-edit.component.scss']
})export class PurchaseRequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  max_quantity: string;

  purchaseRequestsDisplayedColumns: string[] = ["action", "edit", "item_id","quantity","unit"];
  purchaseRequestDetailList: PurchaseRequestDetail[] = []
  purchaseRequestDetail: PurchaseRequestDetail;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PurchaseRequestEditComponent>,
              public  persist: PurchaseRequestPersist,
              public itemPersist: ItemPersist,
              public itemNavigator: ItemNavigator,
              public tcUtilsDate: TCUtilsDate,
              public requestPersist: RequestPersist,
              public store_stoke_level_persist: StoreStockLevelPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("purchase_requests");
      this.title = this.appTranslation.getText("general","new") +  " " + "purchase_request";
      this.purchaseRequestDetail = new PurchaseRequestDetail();
      this.purchaseRequestDetail.unit = units.piece;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("purchase_requests");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "purchase_request";
      this.isLoadingResults = true;
      this.persist.getPurchaseRequest(this.idMode.id).subscribe(purchaseRequestDetail => {
        this.purchaseRequestDetail = purchaseRequestDetail;
        this.itemPersist.getItem(this.purchaseRequestDetail.item_id).subscribe((item)=>{
          if(item)
          this.purchaseRequestDetail.item_name = item.name
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {

    if(this.purchaseRequestDetailList.some(value=> (value.quantity == null || value.quantity <1))){
      this.tcNotification.error("please enter the quantity for all request")
      return;
    }
    
    this.isLoadingResults = true;
    for(let purchaseRequest of this.purchaseRequestDetailList)
    this.persist.addPurchaseRequest(purchaseRequest).subscribe(value => {
      this.tcNotification.success("purchaseRequest added");
      purchaseRequest.id = value.id;
      this.dialogRef.close(purchaseRequest);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
  

    this.persist.updatePurchaseRequest(this.purchaseRequestDetail).subscribe(value => {
      this.tcNotification.success("purchase_request updated");
      this.dialogRef.close(this.purchaseRequestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  canSubmit():boolean{
        if (this.purchaseRequestDetail == null){
            return false;
          }
if (this.purchaseRequestDetail.item_id == null || this.purchaseRequestDetail.item_id  == "") {
            return false;
        } 
if (this.purchaseRequestDetail.quantity == null) {
            return false;
        }
if (this.purchaseRequestDetail.unit == null) {
            return false;
        }
        return true
 }


      removeFromList(item:PurchaseRequestDetail):void{
        this.purchaseRequestDetailList=this.purchaseRequestDetailList.filter((value)=> value != item);
      }
      editFromList(item:PurchaseRequestDetail){
        this.removeFromList(item);
        this.purchaseRequestDetail=item;
      }


      AddToList():void{
        this.purchaseRequestDetailList=[this.purchaseRequestDetail,...this.purchaseRequestDetailList];
        this.purchaseRequestDetail=new PurchaseRequestDetail();
        this.purchaseRequestDetail.unit = units.piece
      }

 searchItem(){
  let dialogRef = this.itemNavigator.pickItems(false)
  dialogRef.afterClosed().subscribe((results: ItemDetail[]) => {
    if(results.length > 1){

      for( let result of results){
        if(!(this.purchaseRequestDetailList.some(value => value.item_id == result.id))){
          let purchaseRequestDetail = new PurchaseRequestDetail(); 
          purchaseRequestDetail.unit = units.piece;
          
          purchaseRequestDetail.item_name = this.itemNavigator.itemName(result);
          purchaseRequestDetail.item_id = result.id
      
      
      
          this.purchaseRequestDetailList = [...this.purchaseRequestDetailList,purchaseRequestDetail]
        }

      } 
    }
    else{

      if(!(this.purchaseRequestDetailList.some(value => value.item_id == results[0].id))){
      this.purchaseRequestDetail.unit = units.piece;
      this.purchaseRequestDetail.item_name = this.itemNavigator.itemName(results[0]);
      this.purchaseRequestDetail.item_id = results[0].id
      }else{
        this.tcNotification.error("You have already selected this item please update quanitty if you need")
      }
    }
  })
}
 }
