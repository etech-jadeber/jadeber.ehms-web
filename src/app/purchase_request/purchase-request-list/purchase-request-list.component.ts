import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PurchaseRequestDetail, PurchaseRequestSummary, PurchaseRequestSummaryPartialList } from '../purchase_request.model';
import { PurchaseRequestPersist } from '../purchase_request.persist';
import { PurchaseRequestNavigator } from '../purchase_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { RequestPersist } from 'src/app/requests/request.persist';
import { PurchaseNavigator } from 'src/app/purchases/purchase.navigator';
import { PurchaseRequestStatus } from 'src/app/app.enums';
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { TCModalModes } from 'src/app/tc/models';
@Component({
  selector: 'app-purchase_request-list',
  templateUrl: './purchase-request-list.component.html',
  styleUrls: ['./purchase-request-list.component.scss']
})

export class PurchaseRequestListComponent implements OnInit {
  purchaseRequestsData: PurchaseRequestSummary[] = [];
  purchaseRequestsTotalCount: number = 0;
  purchaseRequestSelectAll:boolean = false;
  purchaseRequestSelection: PurchaseRequestSummary[] = [];
  purchaseRequestStatus = PurchaseRequestStatus;
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
 purchaseRequestsDisplayedColumns: string[] = ["select","action", "item_id","quantity","unit","request_date", "response_date","status" ];
  purchaseRequestSearchTextBox: FormControl = new FormControl();
  purchaseRequestIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) purchaseRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) purchaseRequestsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public purchaseRequestPersist: PurchaseRequestPersist,
                public purchaseRequestNavigator: PurchaseRequestNavigator,
                public userPersist: UserPersist,
                public storeNavigator: StoreNavigator,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public requestPersist: RequestPersist,
                public purchaseNavigator: PurchaseNavigator,
                public requestNavigator: RequestNavigator,
                public tcUtilsString: TCUtilsString,
                public categoryPersist: ItemCategoryPersist,
                public categoryNavigator: ItemCategoryNavigator,

    ) {

        this.tcAuthorization.requireRead("purchase_requests");
       this.purchaseRequestSearchTextBox.setValue(purchaseRequestPersist.purchaseRequestSearchText);
      //delay subsequent keyup events
      this.purchaseRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.purchaseRequestPersist.purchaseRequestSearchText = value;
        this.searchPurchaseRequests();
      });
      this.purchaseRequestPersist.from_date.valueChanges.pipe().subscribe(value => {
        this.searchPurchaseRequests();
      });
  
      this.purchaseRequestPersist.to_date.valueChanges.pipe().subscribe(value => {
        this.searchPurchaseRequests();
      });
    } ngOnInit() {
   
      this.purchaseRequestsSort.sortChange.subscribe(() => {
        this.purchaseRequestsPaginator.pageIndex = 0;
        this.searchPurchaseRequests(true);
      });

      this.purchaseRequestsPaginator.page.subscribe(() => {
        this.searchPurchaseRequests(true);
      });
      //start by loading items
      this.searchPurchaseRequests();
    }

    
  searchPurchaseRequests(isPagination:boolean = false): void {


    let paginator = this.purchaseRequestsPaginator;
    let sorter = this.purchaseRequestsSort;

    this.purchaseRequestIsLoading = true;
    this.purchaseRequestSelection = [];

    this.purchaseRequestPersist.searchPurchaseRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PurchaseRequestSummaryPartialList) => {
      this.purchaseRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.purchaseRequestsTotalCount = partialList.total;
      }
      this.purchaseRequestIsLoading = false;
    }, error => {
      this.purchaseRequestIsLoading = false;
    });

  } downloadPurchaseRequests(): void {
    if(this.purchaseRequestSelectAll){
         this.purchaseRequestPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download purchase_request", true);
      });
    }
    else{
        this.purchaseRequestPersist.download(this.tcUtilsArray.idsList(this.purchaseRequestSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download purchase_request",true);
            });
        }
  }
addPurchaseRequest(): void {
    let dialogRef = this.purchaseRequestNavigator.addPurchaseRequest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPurchaseRequests();
      }
    });
  }

  rejectRequest(request: PurchaseRequestDetail){
    let dialogRef = this.requestNavigator.addReason();
    dialogRef.afterClosed().subscribe(
      result => {
        if (result){
          this.respondRequest(request, 'reject', {reject_description : result.reason})
        }
      }
    )
  }

  acceptAll():void{
    this.purchaseRequestSelection = this.purchaseRequestSelection.filter(value => value.status ==this.purchaseRequestStatus.submitted);
    for( let request of this.purchaseRequestSelection){
      this.respondRequest(request,"accept");
    }
  }

  purchaseAll():void{
    this.purchaseRequestSelection = this.purchaseRequestSelection.filter(value => value.status ==this.purchaseRequestStatus.accepted);
    this.addPurchase(this.purchaseRequestSelection, TCModalModes.WIZARD)

  }

  respondRequest(request: PurchaseRequestDetail, respond: string, payload = {}) {
    this.purchaseRequestIsLoading = true;
    this.purchaseRequestPersist.purchaseRequestDo(request.id, respond, payload).subscribe(
      result => {
          this.purchaseRequestIsLoading = false;
          this.searchPurchaseRequests()
      }, error => {
        this.purchaseRequestIsLoading = false;
        console.error(error)
      }
    )
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.purchaseRequestPersist.store_name = result[0].name;
        this.purchaseRequestPersist.store_id = result[0].id;
        this.searchPurchaseRequests();
      }
    });
  }
  searchPurchases() {
    throw new Error('Method not implemented.');
  }



  addPurchase(request: any, mode:string = TCModalModes.NEW): void {
    let dialogRef = this.purchaseNavigator.addPurchase(request, mode);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPurchaseRequests();
      }
    });
  }

  editPurchaseRequest(item: PurchaseRequestSummary) {
    let dialogRef = this.purchaseRequestNavigator.editPurchaseRequest(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchPurchaseRequests();
      }

    });
  }


  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.purchaseRequestPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.purchaseRequestPersist.categoryId = result[0].id;
      }
      this.searchPurchaseRequests()
    })
  }

  deletePurchaseRequest(item: PurchaseRequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("purchase_request");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.purchaseRequestPersist.deletePurchaseRequest(item.id).subscribe(response => {
          this.tcNotification.success("purchase_request deleted");
          this.searchPurchaseRequests();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
