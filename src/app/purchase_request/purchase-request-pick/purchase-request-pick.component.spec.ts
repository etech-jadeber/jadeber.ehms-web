import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseRequestPickComponent } from './purchase-request-pick.component';

describe('PurchaseRequestPickComponent', () => {
  let component: PurchaseRequestPickComponent;
  let fixture: ComponentFixture<PurchaseRequestPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseRequestPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseRequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
