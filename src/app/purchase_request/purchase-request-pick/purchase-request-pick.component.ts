import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PurchaseRequestSummary, PurchaseRequestSummaryPartialList } from '../purchase_request.model';
import { PurchaseRequestPersist } from '../purchase_request.persist';
import { PurchaseRequestNavigator } from '../purchase_request.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-purchase_request-pick',
  templateUrl: './purchase-request-pick.component.html',
  styleUrls: ['./purchase-request-pick.component.scss']
})export class PurchaseRequestPickComponent implements OnInit {
  purchaseRequestsData: PurchaseRequestSummary[] = [];
  purchaseRequestsTotalCount: number = 0;
  purchaseRequestSelectAll:boolean = false;
  purchaseRequestSelection: PurchaseRequestSummary[] = [];

 purchaseRequestsDisplayedColumns: string[] = ["select", ,"request_date","response_date","status","item_id","quantity","unit","requestor","approver" ];
  purchaseRequestSearchTextBox: FormControl = new FormControl();
  purchaseRequestIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) purchaseRequestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) purchaseRequestsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public purchaseRequestPersist: PurchaseRequestPersist,
                public purchaseRequestNavigator: PurchaseRequestNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PurchaseRequestPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("purchase_requests");
       this.purchaseRequestSearchTextBox.setValue(purchaseRequestPersist.purchaseRequestSearchText);
      //delay subsequent keyup events
      this.purchaseRequestSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.purchaseRequestPersist.purchaseRequestSearchText = value;
        this.searchPurchase_requests();
      });
    } ngOnInit() {
   
      this.purchaseRequestsSort.sortChange.subscribe(() => {
        this.purchaseRequestsPaginator.pageIndex = 0;
        this.searchPurchase_requests(true);
      });

      this.purchaseRequestsPaginator.page.subscribe(() => {
        this.searchPurchase_requests(true);
      });
      //start by loading items
      this.searchPurchase_requests();
    }

  searchPurchase_requests(isPagination:boolean = false): void {


    let paginator = this.purchaseRequestsPaginator;
    let sorter = this.purchaseRequestsSort;

    this.purchaseRequestIsLoading = true;
    this.purchaseRequestSelection = [];

    this.purchaseRequestPersist.searchPurchaseRequest(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PurchaseRequestSummaryPartialList) => {
      this.purchaseRequestsData = partialList.data;
      if (partialList.total != -1) {
        this.purchaseRequestsTotalCount = partialList.total;
      }
      this.purchaseRequestIsLoading = false;
    }, error => {
      this.purchaseRequestIsLoading = false;
    });

  }
  markOneItem(item: PurchaseRequestSummary) {
    if(!this.tcUtilsArray.containsId(this.purchaseRequestSelection,item.id)){
          this.purchaseRequestSelection = [];
          this.purchaseRequestSelection.push(item);
        }
        else{
          this.purchaseRequestSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.purchaseRequestSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
