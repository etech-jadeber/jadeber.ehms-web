import {TCId} from "../tc/models";

export class PurchaseRequestSummary extends TCId {
    request_date:number;
    response_date:number;
    status:number;
    item_id:string;
    quantity:number;
    unit:number;
    requestor:string;
    approver:string;
    item_name: string;
    requester_name: string;
    approver_name: string;
    name: string;
     
    }
    
export class PurchaseRequestSummaryPartialList {
      data: PurchaseRequestSummary[];
      total: number;
    }
    
export class PurchaseRequestDetail extends PurchaseRequestSummary {
    }
    
export class PurchaseRequestDashboard {
      total: number;
    }
    