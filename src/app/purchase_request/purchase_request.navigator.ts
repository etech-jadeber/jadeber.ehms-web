import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PurchaseRequestEditComponent} from "./purchase-request-edit/purchase-request-edit.component";
import {PurchaseRequestPickComponent} from "./purchase-request-pick/purchase-request-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PurchaseRequestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  purchaseRequestsUrl(): string {
    return "/purchase_requests";
  }

  purchaseRequestUrl(id: string): string {
    return "/purchase_requests/" + id;
  }

  viewPurchaseRequests(): void {
    this.router.navigateByUrl(this.purchaseRequestsUrl());
  }

  viewPurchaseRequest(id: string): void {
    this.router.navigateByUrl(this.purchaseRequestUrl(id));
  }

  editPurchaseRequest(id: string): MatDialogRef<PurchaseRequestEditComponent> {
    const dialogRef = this.dialog.open(PurchaseRequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addPurchaseRequest(): MatDialogRef<PurchaseRequestEditComponent> {
    const dialogRef = this.dialog.open(PurchaseRequestEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickPurchaseRequests(selectOne: boolean=false): MatDialogRef<PurchaseRequestPickComponent> {
      const dialogRef = this.dialog.open(PurchaseRequestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
