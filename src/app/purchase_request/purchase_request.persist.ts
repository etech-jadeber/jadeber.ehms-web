import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PurchaseRequestDashboard, PurchaseRequestDetail, PurchaseRequestSummaryPartialList} from "./purchase_request.model";
import { PurchaseRequestStatus } from '../app.enums';
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class PurchaseRequestPersist {
 purchaseRequestSearchText: string = "";
 categoryId: string;
 category: number;

 purchaseRequestStatusFilter: number ;

    PurchaseRequestStatus: TCEnum[] = [
  new TCEnum( PurchaseRequestStatus.submitted, 'submitted'),
  new TCEnum( PurchaseRequestStatus.accepted, 'accepted'),
  new TCEnum( PurchaseRequestStatus.rejected, 'rejected'),
  new TCEnum( PurchaseRequestStatus.purchased, 'purchased'),

  ];
  store_name: any;
  store_id: any;

 
 constructor(private http: HttpClient,
  public tcUtilsDate: TCUtilsDate) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.purchaseRequestSearchText;
    //add custom filters
    return fltrs;
  }

  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""}) 
 
  searchPurchaseRequest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<PurchaseRequestSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("purchase_request", this.purchaseRequestSearchText, pageSize, pageIndex, sort, order);
    if (this.purchaseRequestStatusFilter){
      url = TCUtilsString.appendUrlParameter(url, "status", this.purchaseRequestStatusFilter.toString())
    }
    if (this.from_date.value){
      url = TCUtilsString.appendUrlParameter(url, "from_date", this.tcUtilsDate.toTimeStamp(this.from_date.value).toString())
    }
    if (this.to_date.value){
      url = TCUtilsString.appendUrlParameter(url, "to_date", this.tcUtilsDate.toTimeStamp(this.to_date.value).toString())
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    return this.http.get<PurchaseRequestSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "purchase_request/do", new TCDoParam("download_all", this.filters()));
  }

  purchaseRequestDashboard(): Observable<PurchaseRequestDashboard> {
    return this.http.get<PurchaseRequestDashboard>(environment.tcApiBaseUri + "purchase_request/dashboard");
  }

  getPurchaseRequest(id: string): Observable<PurchaseRequestDetail> {
    return this.http.get<PurchaseRequestDetail>(environment.tcApiBaseUri + "purchase_request/" + id);
  } addPurchaseRequest(item: PurchaseRequestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "purchase_request/", item);
  }

  updatePurchaseRequest(item: PurchaseRequestDetail): Observable<PurchaseRequestDetail> {
    return this.http.patch<PurchaseRequestDetail>(environment.tcApiBaseUri + "purchase_request/" + item.id, item);
  }

  deletePurchaseRequest(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "purchase_request/" + id);
  }
 purchaseRequestsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "purchase_request/do", new TCDoParam(method, payload));
  }

  purchaseRequestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "purchase_request/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "purchase_request/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "purchase_request/" + id + "/do", new TCDoParam("print", {}));
  }


}
