import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {PurchaseDetail} from "../purchase.model";
import {PurchasePersist} from "../purchase.persist";
import {PurchaseNavigator} from "../purchase.navigator";

export enum PurchaseTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-purchase-detail',
  templateUrl: './purchase-detail.component.html',
  styleUrls: ['./purchase-detail.component.css']
})
export class PurchaseDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  purchaseIsLoading:boolean = false;
  
  PurchaseTabs: typeof PurchaseTabs = PurchaseTabs;
  activeTab: PurchaseTabs = PurchaseTabs.overview;
  //basics
  purchaseDetail: PurchaseDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public purchaseNavigator: PurchaseNavigator,
              public  purchasePersist: PurchasePersist) {
    this.tcAuthorization.requireRead("purchases");
    this.purchaseDetail = new PurchaseDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("purchases");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.purchaseIsLoading = true;
    this.purchasePersist.getPurchase(id).subscribe(purchaseDetail => {
          this.purchaseDetail = purchaseDetail;
          this.purchaseIsLoading = false;
        }, error => {
          console.error(error);
          this.purchaseIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.purchaseDetail.id);
  }
  editPurchase(): void {
    let modalRef = this.purchaseNavigator.editPurchase(this.purchaseDetail.id);
    modalRef.afterClosed().subscribe(purchaseDetail => {
      TCUtilsAngular.assign(this.purchaseDetail, purchaseDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.purchasePersist.print(this.purchaseDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print purchase", true);
      });
    }

  back():void{
      this.location.back();
    }

}
