import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PurchaseDetail } from '../purchase.model';import { PurchasePersist } from '../purchase.persist';import { PurchaseRequestPersist } from 'src/app/purchase_request/purchase_request.persist';
import { SupplierPersist } from 'src/app/suppliers/supplier.persist';
import { SupplierNavigator } from 'src/app/suppliers/supplier.navigator';
import { SupplierDetail } from 'src/app/suppliers/supplier.model';
import { FormControl } from '@angular/forms';
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { RequestPersist } from 'src/app/requests/request.persist';
import { units } from 'src/app/app.enums';
import { StoreStockLevelPersist } from 'src/app/store_stock_level/store_stock_level.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
@Component({
  selector: 'app-purchase-edit',
  templateUrl: './purchase-edit.component.html',
  styleUrls: ['./purchase-edit.component.css']
})export class PurchaseEditComponent implements OnInit {

  isLoadingResults: boolean = false;


  purchasesDisplayedColumns: string[] = ["action","edit","item_name","batch_no", "quantity",'total_price', 'expire_date'];
  title: string;
  purchaseDetail: PurchaseDetail;
  expire_date: FormControl = new FormControl({disabled: true, value: null})
  min_date : Date = new Date(new Date().setDate(new Date().getDate()+1));

  purchaseDetailList : PurchaseDetail[] = [];
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<PurchaseEditComponent>,
              public  persist: PurchasePersist,
              public perchaseRequestPersist: PurchaseRequestPersist,
              public tcUtilsDate: TCUtilsDate,
              public supplierPersist: SupplierPersist,
              public supplierNavigator: SupplierNavigator,
              public itemNav: ItemNavigator,
              public requestPersist: RequestPersist,
              public store_stoke_level_persist: StoreStockLevelPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: {id:any, mode:string}) {
              this.expire_date.valueChanges.subscribe(value => {
                this.purchaseDetail.expire_date = this.tcUtilsDate.toTimeStamp(value._d)
              })
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isWizard(): boolean {
    return this.idMode.mode == TCModalModes.WIZARD;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("purchases");
      this.title = this.appTranslation.getText("general","new") +  " " + "purchase";
      this.purchaseDetail = new PurchaseDetail();
      //set necessary defaults
      this.purchaseDetail.purchase_request_id = this.idMode.id.id;
      this.purchaseDetail.requested_quantity = this.idMode.id.quantity;
      this.purchaseDetail.item_name = this.itemNav.itemName(this.idMode.id);
      this.purchaseDetail.strength = 250;
      this.purchaseDetail.form = units.piece

    }else if(this.idMode.mode == TCModalModes.WIZARD){

      this.tcAuthorization.requireCreate("purchases");
      this.title = this.appTranslation.getText("general","new") +  " " + "purchase";
      this.purchaseDetail = new PurchaseDetail();
      //set necessary defaults
      this.purchaseDetail.purchase_request_id = this.idMode.id[0].id;
      this.purchaseDetail.requested_quantity = this.idMode.id[0].quantity;
      this.purchaseDetail.item_name = this.itemNav.itemName(this.idMode.id[0]);
      this.purchaseDetail.strength = 250;
      this.purchaseDetail.form = units.piece

    } else {
     this.tcAuthorization.requireUpdate("purchases");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "purchase";
      this.isLoadingResults = true;
      this.persist.getPurchase(this.idMode.id).subscribe(purchaseDetail => {
        this.purchaseDetail = purchaseDetail;
        this.supplierPersist.getSupplier(this.purchaseDetail.supplier_id).subscribe(supplier=>{
          if(supplier){
            this.purchaseDetail.supplier_name = supplier.name;
          }
        });
        this.expire_date.setValue(this.tcUtilsDate.toDate(this.purchaseDetail.expire_date))
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addPurchase(this.purchaseDetailList).subscribe(value => {
      this.tcNotification.success("purchase added");
      this.purchaseDetail.id = value.id;
      this.purchaseDetail.remaining = this.purchaseDetail.quantity;
      this.dialogRef.close(this.purchaseDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updatePurchase(this.purchaseDetail).subscribe(value => {
      this.tcNotification.success("purchase updated");
      this.purchaseDetail.remaining = this.purchaseDetail.quantity;
      this.dialogRef.close(this.purchaseDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.purchaseDetail == null){
            return false;
          }
if (this.purchaseDetail.purchase_request_id == null || this.purchaseDetail.purchase_request_id  == "") {
            return false;
        } 
// if (this.purchaseDetail.supplier_id == null || this.purchaseDetail.supplier_id  == "") {
//             return false;
//         } 
if (this.purchaseDetail.quantity == null) {
            return false;
        }
// if (this.purchaseDetail.purchase_unit == null) {
//             return false;
//         }
// if (this.purchaseDetail.crv_number == null || this.purchaseDetail.crv_number  == "") {
//             return false;
//         } 
if (this.purchaseDetail.total_price == null) {
            return false;
        }
if (this.purchaseDetail.expire_date == null){
  return false;
}
 return true;

 }
 AddToList():void{
  let purchase_detail = this.purchaseDetail
  this.expire_date.setValue(null);
  this.purchaseDetailList=[this.purchaseDetail,...this.purchaseDetailList];
  this.purchaseDetail=new PurchaseDetail();
  this.purchaseDetail.crv_number = purchase_detail.crv_number;
  this.purchaseDetail.supplier_id = purchase_detail.supplier_id;
  this.purchaseDetail.supplier_name = purchase_detail.supplier_name;
  this.purchaseDetail.purchase_unit = purchase_detail.purchase_unit
  this.purchaseDetail.purchase_request_id = purchase_detail.purchase_request_id;
  this.purchaseDetail.requested_quantity = purchase_detail.requested_quantity - purchase_detail.quantity;
  this.purchaseDetail.item_name = purchase_detail.item_name;
  this.purchaseDetail.strength = 250;
  this.purchaseDetail.form = units.piece
}

next():void{
  let purchase_detail = this.purchaseDetail
  this.idMode.id = this.idMode.id.filter(value => value != this.idMode.id[0])
  this.purchaseDetail= new PurchaseDetail();
  this.purchaseDetail.crv_number = purchase_detail.crv_number;
  this.purchaseDetail.supplier_id = purchase_detail.supplier_id;
  this.purchaseDetail.supplier_name = purchase_detail.supplier_name;
  this.purchaseDetail.purchase_unit = purchase_detail.purchase_unit
  this.purchaseDetail.purchase_request_id = this.idMode.id[0].id;
  this.purchaseDetail.requested_quantity = this.idMode.id[0].quantity;
  this.purchaseDetail.item_name = this.itemNav.itemName(this.idMode.id[0]);
  this.purchaseDetail.strength = 250;
  this.purchaseDetail.form = units.piece;

}


removeFromList(item:PurchaseDetail):void{
  this.purchaseDetailList=this.purchaseDetailList.filter((value)=> value != item);
}
editFromList(item:PurchaseDetail){
  this.removeFromList(item);
  this.purchaseDetail=item;
}

  onTotalPriceChange(event : any):void{
    this.purchaseDetail.unit_price = parseFloat(((event || 0) / this.purchaseDetail.quantity).toFixed(2));
  }

 onUnitPriceChange(event: any):void{
  this.purchaseDetail.total_price = this.purchaseDetail.quantity * (event || 0);
 }
 onQuantityChange(event :any ):void{
  this.purchaseDetail.total_price = this.purchaseDetail.unit_price * (event || 0);
 }

 searchSupplier(){
  let dialogRef = this.supplierNavigator.pickSuppliers(true)
  dialogRef.afterClosed().subscribe((result: SupplierDetail[]) => {
    if(result){
    this.purchaseDetail.supplier_name = result[0].name;
    this.purchaseDetail.supplier_id = result[0].id
    }
  })
}
 }
