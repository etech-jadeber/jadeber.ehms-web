import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PurchaseSummary, PurchaseSummaryPartialList } from '../purchase.model';
import { PurchasePersist } from '../purchase.persist';
import { PurchaseNavigator } from '../purchase.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { SupplierPersist } from 'src/app/suppliers/supplier.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { PurchaseRequestPersist } from 'src/app/purchase_request/purchase_request.persist';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ItemReceiveNavigator } from 'src/app/item_receive/item_receive.navigator';
import { SupplierDetail } from 'src/app/suppliers/supplier.model';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { PurchaseStatus, units } from 'src/app/app.enums';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemReceiveDetail } from 'src/app/item_receive/item_receive.model';
import { ItemReceivePersist } from 'src/app/item_receive/item_receive.persist';
@Component({
  selector: 'app-purchase-list',
  templateUrl: './purchase-list.component.html',
  styleUrls: ['./purchase-list.component.css']
})export class PurchaseListComponent implements OnInit {
  purchasesData: PurchaseSummary[] = [];
  purchasesTotalCount: number = 0;
  purchaseSelectAll:boolean = false;
  purchaseSelection: PurchaseSummary[] = [];
  categoryName: string;
  subCategoryName: string;
  purchaseStatus = PurchaseStatus;
  categoryId: string;
  refNoBox: FormControl = new FormControl();

 purchasesDisplayedColumns: string[] = ["select","action","item",'batch_no','ref_no',"supplier_id","quantity",'remaining',"crv_number","total_price","purchase_date", "expire_date","status" ];
  purchaseSearchTextBox: FormControl = new FormControl();
  purchaseIsLoading: boolean = false;  
  supplierIds: SupplierDetail[] = [];
  @ViewChild(MatPaginator, {static: true}) purchasesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) purchasesSort: MatSort;
  @Input() requestId: string;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public purchasePersist: PurchasePersist,
                public purchaseNavigator: PurchaseNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public supplierPersist: SupplierPersist,
                public storeNavigator: StoreNavigator,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public purchaseRequestPersist: PurchaseRequestPersist,
                public requestPersist: RequestPersist,
                public itemReceiveNavigator: ItemReceiveNavigator,
                public ItemReceivePersist: ItemReceivePersist,
                public tcUtilsString: TCUtilsString,
                public categoryPersist: ItemCategoryPersist,
                public categoryNavigator: ItemCategoryNavigator,

    ) {

        this.tcAuthorization.requireRead("purchases");
       this.purchaseSearchTextBox.setValue(purchasePersist.purchaseSearchText);
      //delay subsequent keyup events
      this.purchaseSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.purchasePersist.purchaseSearchText = value;
        this.searchPurchases();
      });
      this.purchasePersist.from_date.valueChanges.pipe().subscribe(value => {
        this.searchPurchases();
      });
  
      this.refNoBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.purchasePersist.ref_no = value;
        this.searchPurchases();
      });
      this.purchasePersist.to_date.valueChanges.pipe().subscribe(value => {
        this.searchPurchases();
      });
    } ngOnInit() {
   
      this.purchasesSort.sortChange.subscribe(() => {
        this.purchasesPaginator.pageIndex = 0;
        this.searchPurchases(true);
      });

      this.purchasesPaginator.page.subscribe(() => {
        this.searchPurchases(true);
      });
      //start by loading items
      this.searchPurchases();
    }

  searchPurchases(isPagination:boolean = false): void {


    let paginator = this.purchasesPaginator;
    let sorter = this.purchasesSort;

    this.purchaseIsLoading = true;
    this.purchaseSelection = [];

    this.purchasePersist.searchPurchase(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.requestId).subscribe((partialList: PurchaseSummaryPartialList) => {
      this.purchasesData = partialList.data;
      this.purchasesData.forEach((purchase) => {
        if (!this.tcUtilsArray.containsId(this.supplierIds, purchase.supplier_id)){
          this.supplierIds.push(new SupplierDetail(purchase.supplier_id))
          this.supplierPersist.getSupplier(purchase.supplier_id).subscribe(
            supplier => {
              this.tcUtilsArray.removeItemById(this.supplierIds, purchase.supplier_id);
              this.supplierIds = [...this.supplierIds, supplier]
            }
          )
        }
      })
      if (partialList.total != -1) {
        this.purchasesTotalCount = partialList.total;
      }
      this.purchaseIsLoading = false;
    }, error => {
      this.purchaseIsLoading = false;
    });

  } downloadPurchases(): void {
    if(this.purchaseSelectAll){
         this.purchasePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download purchase", true);
      });
    }
    else{
        this.purchasePersist.download(this.tcUtilsArray.idsList(this.purchaseSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download purchase",true);
            });
        }
  }
addPurchase(): void {
    let dialogRef = this.purchaseNavigator.addPurchase(this.requestId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPurchases();
      }
    });
  }

  sendToStore():void {
    this.purchaseSelection = this.purchaseSelection.filter(value => value.status == PurchaseStatus.purchased);
    for( let purchase of this.purchaseSelection){
      let itemReceiveDetail = new ItemReceiveDetail();
      itemReceiveDetail.purchase_id = purchase.id;
      itemReceiveDetail.quantity = purchase.remaining;
      itemReceiveDetail.unit = units.piece;

      this.ItemReceivePersist.addItemReceive(itemReceiveDetail).subscribe(value => {
        this.tcNotification.success("itemReceive added");
        purchase.status = PurchaseStatus.sent
        purchase.remaining = 0;
        itemReceiveDetail.id = value.id
      });
    }
  }

  sentMainStore(element: PurchaseSummary){
    console.log(element)
    let dialogRef = this.itemReceiveNavigator.addItemReceive(element.id, element.remaining);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchPurchases()
      }
    })
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.purchasePersist.store_name = result[0].name;
        this.purchasePersist.store_id = result[0].id;
        this.searchPurchases();
      }
    });
  }

  editPurchase(item: PurchaseSummary) {
    let dialogRef = this.purchaseNavigator.editPurchase(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.purchasePersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.purchasePersist.categoryId = result[0].id;
      }
      this.searchPurchases()
    })
  }
  deletePurchase(item: PurchaseSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("purchase");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.purchasePersist.deletePurchase(item.id).subscribe(response => {
          this.tcNotification.success("purchase deleted");
          this.searchPurchases();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
