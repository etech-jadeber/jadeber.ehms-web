import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PurchasePickComponent } from './purchase-pick.component';

describe('PurchasePickComponent', () => {
  let component: PurchasePickComponent;
  let fixture: ComponentFixture<PurchasePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
