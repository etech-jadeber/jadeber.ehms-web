import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PurchaseSummary, PurchaseSummaryPartialList } from '../purchase.model';
import { PurchasePersist } from '../purchase.persist';
import { PurchaseNavigator } from '../purchase.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-purchase-pick',
  templateUrl: './purchase-pick.component.html',
  styleUrls: ['./purchase-pick.component.css']
})export class PurchasePickComponent implements OnInit {
  purchasesData: PurchaseSummary[] = [];
  purchasesTotalCount: number = 0;
  purchaseSelectAll:boolean = false;
  purchaseSelection: PurchaseSummary[] = [];

 purchasesDisplayedColumns: string[] = ["select", ,"item_type","purchase_request_id","supplier_id","quantity","unit","crv_number","total_price","purchase_date","status","receiver_id","sender_id","reject_description" ];
  purchaseSearchTextBox: FormControl = new FormControl();
  purchaseIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) purchasesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) purchasesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public purchasePersist: PurchasePersist,
                public purchaseNavigator: PurchaseNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<PurchasePickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("purchases");
       this.purchaseSearchTextBox.setValue(purchasePersist.purchaseSearchText);
      //delay subsequent keyup events
      this.purchaseSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.purchasePersist.purchaseSearchText = value;
        this.searchPurchases();
      });
    } ngOnInit() {
   
      this.purchasesSort.sortChange.subscribe(() => {
        this.purchasesPaginator.pageIndex = 0;
        this.searchPurchases(true);
      });

      this.purchasesPaginator.page.subscribe(() => {
        this.searchPurchases(true);
      });
      //start by loading items
      this.searchPurchases();
    }

  searchPurchases(isPagination:boolean = false): void {


    let paginator = this.purchasesPaginator;
    let sorter = this.purchasesSort;

    this.purchaseIsLoading = true;
    this.purchaseSelection = [];

    this.purchasePersist.searchPurchase(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PurchaseSummaryPartialList) => {
      this.purchasesData = partialList.data;
      if (partialList.total != -1) {
        this.purchasesTotalCount = partialList.total;
      }
      this.purchaseIsLoading = false;
    }, error => {
      this.purchaseIsLoading = false;
    });

  }
  markOneItem(item: PurchaseSummary) {
    if(!this.tcUtilsArray.containsId(this.purchaseSelection,item.id)){
          this.purchaseSelection = [];
          this.purchaseSelection.push(item);
        }
        else{
          this.purchaseSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.purchaseSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
