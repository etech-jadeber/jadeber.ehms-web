import {TCId} from "../tc/models";

export class PurchaseSummary extends TCId {
  purchase_request_id:string;
  requested_quantity: number;
  supplier_id:string;
  quantity:number;
  purchase_unit:number;
  crv_number:string;
  total_price:number;
  purchase_date:number;
  status:number;
  receiver_id:string;
  sender_id:string;
  reject_description:string;
  supplier_name: string;
  sender_name: string;
  receiver_name: string;
  item_name: string;
  expire_date: number;
  strength: number;
  form: number;
  unit_price: number;
  remaining: number;
  batch_no: string;
  }
  export class PurchaseSummaryPartialList {
    data: PurchaseSummary[];
    total: number;
  }
  export class PurchaseDetail extends PurchaseSummary {
  }
  
  export class PurchaseDashboard {
    total: number;
  }
  