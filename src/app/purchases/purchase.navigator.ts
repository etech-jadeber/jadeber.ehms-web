import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {PurchaseEditComponent} from "./purchase-edit/purchase-edit.component";
import {PurchasePickComponent} from "./purchase-pick/purchase-pick.component";


@Injectable({
  providedIn: 'root'
})

export class PurchaseNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  purchasesUrl(): string {
    return "/purchases";
  }

  purchaseUrl(id: string): string {
    return "/purchases/" + id;
  }

  viewPurchases(): void {
    this.router.navigateByUrl(this.purchasesUrl());
  }

  viewPurchase(id: string): void {
    this.router.navigateByUrl(this.purchaseUrl(id));
  }

  editPurchase(id: string): MatDialogRef<PurchaseEditComponent> {
    const dialogRef = this.dialog.open(PurchaseEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  addPurchase(parent:any, mode:string= TCModalModes.NEW): MatDialogRef<PurchaseEditComponent> {
    const dialogRef = this.dialog.open(PurchaseEditComponent, {
          data: {id:parent,  mode:mode},
          width: TCModalWidths.large
    });
    return dialogRef;
  }
  
   pickPurchases(selectOne: boolean=false): MatDialogRef<PurchasePickComponent> {
      const dialogRef = this.dialog.open(PurchasePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
