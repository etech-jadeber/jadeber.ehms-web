import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {PurchaseDashboard, PurchaseDetail, PurchaseSummaryPartialList} from "./purchase.model";
import { item_units, PurchaseStatus } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class PurchasePersist {
 purchaseSearchText: string = "";
 categoryId: string;
 category: number;
 
 purchaseStatusFilter: number ;

    PurchaseStatus: TCEnum[] = [
     new TCEnum( PurchaseStatus.purchased, 'purchased'),
  new TCEnum( PurchaseStatus.sent, 'sent'),
  // new TCEnum( PurchaseStatus.received, 'received'),
  // new TCEnum( PurchaseStatus.rejected, 'rejected'),
  new TCEnum( PurchaseStatus.onprogress, 'on progress'),

  ];

  item_units : TCEnum[] = [
    new TCEnum(item_units.bottle, "Bottle" ),
  new TCEnum(item_units.sachet, "Sachet" ),
  new TCEnum(item_units.vial, "Vial" ),
  new TCEnum(item_units.tab, "Tab" ),
  new TCEnum(item_units.amp, "Amp" ),
  new TCEnum(item_units.supp, "SUPP" ),
  new TCEnum(item_units.each , "each" ),
  new TCEnum(item_units.cap, "Cap" ),
  new TCEnum(item_units.jar, "Jar" ),
  ]

  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""}) 
  store_id: string;
  store_name: string;
  ref_no: string;

 constructor(private http: HttpClient,
  public tcUtilsDate: TCUtilsDate) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.purchaseSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchPurchase(pageSize: number, pageIndex: number, sort: string, order: string, request_id: string = null): Observable<PurchaseSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("purchase", this.purchaseSearchText, pageSize, pageIndex, sort, order);
    if (request_id){
      url = TCUtilsString.appendUrlParameter(url, "request_id", request_id);
    }
    if (this.purchaseStatusFilter){
      url = TCUtilsString.appendUrlParameter(url, "status", this.purchaseStatusFilter.toString())
    }
    if (this.from_date.value){
      url = TCUtilsString.appendUrlParameter(url, "from_date", this.tcUtilsDate.toTimeStamp(this.from_date.value).toString())
    }
    if (this.to_date.value){
      url = TCUtilsString.appendUrlParameter(url, "to_date", this.tcUtilsDate.toTimeStamp(this.to_date.value).toString())
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }


    if(this.ref_no){
      url = TCUtilsString.appendUrlParameter(url, "ref_no", this.ref_no);
    }
    
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }    
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    return this.http.get<PurchaseSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "purchase/do", new TCDoParam("download_all", this.filters()));
  }

  purchaseDashboard(): Observable<PurchaseDashboard> {
    return this.http.get<PurchaseDashboard>(environment.tcApiBaseUri + "purchase/dashboard");
  }

  getPurchase(id: string): Observable<PurchaseDetail> {
    return this.http.get<PurchaseDetail>(environment.tcApiBaseUri + "purchase/" + id);
  } addPurchase(item: PurchaseDetail[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "purchase/", item);
  }

  updatePurchase(item: PurchaseDetail): Observable<PurchaseDetail> {
    return this.http.patch<PurchaseDetail>(environment.tcApiBaseUri + "purchase/" + item.id, item);
  }

  deletePurchase(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "purchase/" + id);
  }
 purchasesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "purchase/do", new TCDoParam(method, payload));
  }

  purchaseDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "purchases/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "purchase/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "purchase/" + id + "/do", new TCDoParam("print", {}));
  }


}
