import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {QueueDetail} from "../queue.model";
import {QueuePersist} from "../queue.persist";
import {QueueNavigator} from "../queue.navigator";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

export enum QueueTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-queue-detail',
  templateUrl: './queue-detail.component.html',
  styleUrls: ['./queue-detail.component.scss']
})
export class QueueDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  queueIsLoading:boolean = false;
  
  QueueTabs: typeof QueueTabs = QueueTabs;
  activeTab: QueueTabs = QueueTabs.overview;
  //basics
  queueDetail: QueueDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public queueNavigator: QueueNavigator,
              public  queuePersist: QueuePersist) {
    this.tcAuthorization.requireRead("queues");
    this.queueDetail = new QueueDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("queues");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.queueIsLoading = true;
    this.queuePersist.getQueue(id).subscribe(queueDetail => {
          this.queueDetail = queueDetail;
          this.queueIsLoading = false;
        }, error => {
          console.error(error);
          this.queueIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.queueDetail.id);
  }
  editQueue(): void {
    let modalRef = this.queueNavigator.editQueue(this.queueDetail.id);
    modalRef.afterClosed().subscribe(queueDetail => {
      TCUtilsAngular.assign(this.queueDetail, queueDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.queuePersist.print(this.queueDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print queue", true);
      });
    }

  back():void{
      this.location.back();
    }

}