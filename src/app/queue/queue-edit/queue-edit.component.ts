import {Component, OnInit, Inject} from '@angular/core';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { QueueDetail } from '../queue.model';import { QueuePersist } from '../queue.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-queue-edit',
  templateUrl: './queue-edit.component.html',
  styleUrls: ['./queue-edit.component.scss']
})export class QueueEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  queueDetail: QueueDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<QueueEditComponent>,
              public  persist: QueuePersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("queues");
      this.title = this.appTranslation.getText("general","new") +  " " + "queue";
      this.queueDetail = new QueueDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("queues");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "queue";
      this.isLoadingResults = true;
      this.persist.getQueue(this.idMode.id).subscribe(queueDetail => {
        this.queueDetail = queueDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addQueue(this.queueDetail).subscribe(value => {
      this.tcNotification.success("queue added");
      this.queueDetail.id = value.id;
      this.dialogRef.close(this.queueDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateQueue(this.queueDetail).subscribe(value => {
      this.tcNotification.success("queue updated");
      this.dialogRef.close(this.queueDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.queueDetail == null){
            return false;
          }

if (this.queueDetail.encounter_id == null || this.queueDetail.encounter_id  == "") {
            return false;
        } 

if (this.queueDetail.patient_id == null || this.queueDetail.patient_id  == "") {
            return false;
        } 

if (this.queueDetail.room_id == null || this.queueDetail.room_id  == "") {
            return false;
        } 

if (this.queueDetail.doctor_id == null || this.queueDetail.doctor_id  == "") {
            return false;
}

if (this.queueDetail.type == null) {
            return false;
        }

        return true;

 }
 }