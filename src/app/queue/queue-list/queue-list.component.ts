import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { QueueSummary, QueueSummaryPartialList } from '../queue.model';
import { QueuePersist } from '../queue.persist';
import { QueueNavigator } from '../queue.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { VitalNavigator } from 'src/app/vitals/vital.navigator';
import { OrdersNavigator } from 'src/app/form_encounters/orderss/orders.navigator';
@Component({
  selector: 'app-queue-list',
  templateUrl: './queue-list.component.html',
  styleUrls: ['./queue-list.component.scss']
})export class QueueListComponent implements OnInit {
  queuesData: QueueSummary[] = [];
  queuesTotalCount: number = 0;
  queueSelectAll:boolean = false;
  queueSelection: QueueSummary[] = [];

 queuesDisplayedColumns: string[] = ["select","action" ,"patient_id","room_id","doctor_id","type","date","service_time","completed" ];
  queueSearchTextBox: FormControl = new FormControl();
  queueIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) queuesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) queuesSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public vitalNavigator:VitalNavigator,
                public appTranslation:AppTranslation,
                public queuePersist: QueuePersist,
                public queueNavigator: QueueNavigator,
                public encounteNav:Form_EncounterNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public ordersNavigator: OrdersNavigator

    ) 
    
    {

      

        this.tcAuthorization.requireRead("queues");
       this.queueSearchTextBox.setValue(queuePersist.queueSearchText);
      //delay subsequent keyup events
      this.queueSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.queuePersist.queueSearchText = value;
        this.searchQueues();
      });
    } ngOnInit() {
   
      this.queuesSort.sortChange.subscribe(() => {
        this.queuesPaginator.pageIndex = 0;
        this.searchQueues(true);
      });

      this.queuesPaginator.page.subscribe(() => {
        this.searchQueues(true);
      });
      //start by loading items
      this.searchQueues();
    }

  searchQueues(isPagination:boolean = false): void {


    let paginator = this.queuesPaginator;
    let sorter = this.queuesSort;

    this.queueIsLoading = true;
    this.queueSelection = [];

    this.queuePersist.searchQueue(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: QueueSummaryPartialList) => {
      this.queuesData = partialList.data;
      if (partialList.total != -1) {
        this.queuesTotalCount = partialList.total;
      }
      this.queueIsLoading = false;
    }, error => {
      this.queueIsLoading = false;
    });

  } downloadQueues(): void {
    if(this.queueSelectAll){
         this.queuePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download queue", true);
      });
    }
    else{
        this.queuePersist.download(this.tcUtilsArray.idsList(this.queueSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download queue",true);
            });
        }
  }
addQueue(): void {
    let dialogRef = this.queueNavigator.addQueue();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchQueues();
      }
    });
  }

  editQueue(item: QueueSummary) {
    let dialogRef = this.queueNavigator.editQueue(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteQueue(item: QueueSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("queue");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.queuePersist.deleteQueue(item.id).subscribe(response => {
          this.tcNotification.success("queue deleted");
          this.searchQueues();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }

    loadProcedure(id: string): string{
      return `/patient_procedure_notes/${id}`;
    }
}