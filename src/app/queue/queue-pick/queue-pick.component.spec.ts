import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuePickComponent } from './queue-pick.component';

describe('QueuePickComponent', () => {
  let component: QueuePickComponent;
  let fixture: ComponentFixture<QueuePickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueuePickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
