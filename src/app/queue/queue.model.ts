import {TCId} from "../tc/models";export class QueueSummary extends TCId {
    encounter_id:string;
    patient_id:string;
    room_id:string;
    doctor_id:string;
    type:number;
    date:number;
    service_time:number;
    completed:boolean;
    doctor_name:string;
    patient_name:string;
    room_no:string;
     
    }
    export class QueueSummaryPartialList {
      data: QueueSummary[];
      total: number;
    }
    export class QueueDetail extends QueueSummary {
    }
    
    export class QueueDashboard {
      total: number;
    }