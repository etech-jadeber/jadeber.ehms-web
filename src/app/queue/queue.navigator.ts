import {Router} from "@angular/router";
import {Injectable} from "@angular/core";


import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {QueueEditComponent} from "./queue-edit/queue-edit.component";
import {QueuePickComponent} from "./queue-pick/queue-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class QueueNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  queuesUrl(): string {
    return "/queues";
  }

  queueUrl(id: string): string {
    return "/queues/" + id;
  }

  viewQueues(): void {
    this.router.navigateByUrl(this.queuesUrl());
  }

  viewQueue(id: string): void {
    this.router.navigateByUrl(this.queueUrl(id));
  }

  editQueue(id: string): MatDialogRef<QueueEditComponent> {
    const dialogRef = this.dialog.open(QueueEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addQueue(): MatDialogRef<QueueEditComponent> {
    const dialogRef = this.dialog.open(QueueEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickQueues(selectOne: boolean=false): MatDialogRef<QueuePickComponent> {
      const dialogRef = this.dialog.open(QueuePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}