import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {QueueDashboard, QueueDetail, QueueSummaryPartialList} from "./queue.model";
import { QueueType } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class QueuePersist {
 queueSearchText: string = "";
 queueTypeId:number = -1;
 completed:boolean = false;
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.queueSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchQueue(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<QueueSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("queue", this.queueSearchText, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameter(url, "queueTypeId", this.queueTypeId.toString());

    url = TCUtilsString.appendUrlParameter(url, "completed", this.completed.toString());


    return this.http.get<QueueSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "queue/do", new TCDoParam("download_all", this.filters()));
  }

  queueDashboard(): Observable<QueueDashboard> {
    return this.http.get<QueueDashboard>(environment.tcApiBaseUri + "queue/dashboard");
  }

  getQueue(id: string): Observable<QueueDetail> {
    return this.http.get<QueueDetail>(environment.tcApiBaseUri + "queue/" + id);
  } addQueue(item: QueueDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "queue/", item);
  }

  updateQueue(item: QueueDetail): Observable<QueueDetail> {
    return this.http.patch<QueueDetail>(environment.tcApiBaseUri + "queue/" + item.id, item);
  }

  deleteQueue(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "queue/" + id);
  }
 queuesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "queue/do", new TCDoParam(method, payload));
  }

  queueDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "queues/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "queue/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "queue/" + id + "/do", new TCDoParam("print", {}));
  }



  queueType:TCEnumTranslation[] = [
    new TCEnumTranslation(QueueType.doctor, "Doctor"),
    new TCEnumTranslation(QueueType.nurse, "Nurse"),
    new TCEnumTranslation(QueueType.lab, "Lab"),
    new TCEnumTranslation(QueueType.rad, "Rad"),
    new TCEnumTranslation(QueueType.procedure, "Procedure"),
  ]



}