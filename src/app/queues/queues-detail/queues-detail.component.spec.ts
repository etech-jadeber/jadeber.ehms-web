import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuesDetailComponent } from './queues-detail.component';

describe('QueuesDetailComponent', () => {
  let component: QueuesDetailComponent;
  let fixture: ComponentFixture<QueuesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueuesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
