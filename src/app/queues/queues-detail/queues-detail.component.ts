import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {QueuesDetail} from "../queues.model";
import {QueuesPersist} from "../queues.persist";
import {QueuesNavigator} from "../queues.navigator";

export enum QueuesTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-queues-detail',
  templateUrl: './queues-detail.component.html',
  styleUrls: ['./queues-detail.component.scss']
})
export class QueuesDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  queuesIsLoading:boolean = false;
  
  QueuesTabs: typeof QueuesTabs = QueuesTabs;
  activeTab: QueuesTabs = QueuesTabs.overview;
  //basics
  queuesDetail: QueuesDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public queuesNavigator: QueuesNavigator,
              public  queuesPersist: QueuesPersist) {
    this.tcAuthorization.requireRead("queues");
    this.queuesDetail = new QueuesDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("queues");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.queuesIsLoading = true;
    this.queuesPersist.getQueues(id).subscribe(queuesDetail => {
          this.queuesDetail = queuesDetail;
          this.queuesIsLoading = false;
        }, error => {
          console.error(error);
          this.queuesIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.queuesDetail.id);
  }
  editQueues(): void {
    let modalRef = this.queuesNavigator.editQueues(this.queuesDetail.id);
    modalRef.afterClosed().subscribe(queuesDetail => {
      TCUtilsAngular.assign(this.queuesDetail, queuesDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.queuesPersist.print(this.queuesDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print queues", true);
      });
    }

  back():void{
      this.location.back();
    }

}