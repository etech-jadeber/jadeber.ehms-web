import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuesEditComponent } from './queues-edit.component';

describe('QueuesEditComponent', () => {
  let component: QueuesEditComponent;
  let fixture: ComponentFixture<QueuesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueuesEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
