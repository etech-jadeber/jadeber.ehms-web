import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { QueuesDetail } from '../queues.model';import { QueuesPersist } from '../queues.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-queues-edit',
  templateUrl: './queues-edit.component.html',
  styleUrls: ['./queues-edit.component.scss']
})export class QueuesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  queuesDetail: QueuesDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<QueuesEditComponent>,
              public  persist: QueuesPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("queues");
      this.title = this.appTranslation.getText("general","new") +  " " + "queues";
      this.queuesDetail = new QueuesDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("queues");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "queues";
      this.isLoadingResults = true;
      this.persist.getQueues(this.idMode.id).subscribe(queuesDetail => {
        this.queuesDetail = queuesDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addQueues(this.queuesDetail).subscribe(value => {
      this.tcNotification.success("queues added");
      this.queuesDetail.id = value;
      this.dialogRef.close(this.queuesDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateQueues(this.queuesDetail,this.queuesDetail.encounter_id).subscribe(value => {
      this.tcNotification.success("queues updated");
      this.dialogRef.close(this.queuesDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.queuesDetail == null){
            return false;
          }

 }
 }