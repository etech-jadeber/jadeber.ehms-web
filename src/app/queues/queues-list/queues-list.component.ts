import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { QueuesSummary, QueuesSummaryPartialList } from '../queues.model';
import { QueuesPersist } from '../queues.persist';
import { QueuesNavigator } from '../queues.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { DoctorDetail } from 'src/app/doctors/doctor.model';
import { E } from '@angular/cdk/keycodes';
@Component({
  selector: 'app-queues-list',
  templateUrl: './queues-list.component.html',
  styleUrls: ['./queues-list.component.scss']
})export class QueuesListComponent implements OnInit {
  queuessData: QueuesSummary[] = [];
  queuessTotalCount: number = 0;
  queuesSelectAll:boolean = false;
  queuesSelection: QueuesSummary[] = [];
  timer:any;
  timer2:any;
  name:string;


 queuessDisplayedColumns: string[] = [ "number","pid","queue_type","queue_status" ];
  queuesSearchTextBox: FormControl = new FormControl();
  queuesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) queuessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) queuessSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public queuesPersist: QueuesPersist,
                public queuesNavigator: QueuesNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("queues");
       this.queuesSearchTextBox.setValue(queuesPersist.queuesSearchText);
      //delay subsequent keyup events
      this.queuesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.queuesPersist.queuesSearchText = value;
        // this.searchQueuess();
      });
    } ngOnInit() {

      this.queuesPersist.getQueueDoctor().subscribe((res:DoctorDetail[])=>{
        console.log("doctores",res)
        let val = [...res]
        let y= 0
        let  currentIndex = 1;
        this.queuessPaginator.length = val[y].count
        let x = Math.ceil(val[y].count /10)
        // if(val[y].id=="radiology"||val[y].id=='labratory'){
        //   this.name = val[y].id
        // }else{
          this.name= val[y].first_name + " " +val[y].middle_name + " "+val[y].last_name

        // }

        this.searchQueuess(val[y].id,false)
 

          this.timer = setInterval(() => {
            
            

            if(y<val.length){
              
              if(x>1){
                console.log("===================",y,x)
                console.log(y,"x = ",x,"this.queuessPaginator.pageIndex ",this.queuessPaginator.pageIndex,val[y])
                this.queuessPaginator.pageIndex = this.queuessPaginator.pageIndex+1;
                this.searchQueuess(val[y].id,true)
                
              
              x = x -1;
              }else {
                y = y+1
                if (y<val.length){
                  this.queuessPaginator.pageIndex = 0;
                  this.queuessPaginator.length = val[y].count
  
                  x = Math.ceil(val[y].count /10)
                  // if(val[y].id=="radiology"||val[y].id=='labratory'){
                  //   this.name = val[y].id
                  // }else{
                    this.name= val[y].first_name + " " +val[y].middle_name + " "+val[y].last_name
          
                  // }
                  this.searchQueuess(val[y].id,false)
                }else{
                  console.log("ppppppppppppppppppppppppppppppppppppppppppppppppppppppp")
    
                  this.queuesPersist.getQueueDoctor().subscribe(((res2:DoctorDetail[])=>{
    
                    y = 0 
    
                    val = [...res2]
    
                    console.log("***************/////////////////////",y,x,val)
                    
                    this.queuessPaginator.length = val[y].count
                    this.queuessPaginator.pageIndex = 0;
    
                    x = Math.ceil(val[y].count /10)
            // if(val[y].id=="radiology"||val[y].id=='labratory'){
            //   this.name = val[y].id
            // }else{
              this.name= val[y].first_name + " " +val[y].middle_name + " "+val[y].last_name
    
            // }
    
            this.searchQueuess(val[y].id,false)
    
    
                  }))
    
                }
                

              
              }
              
            }

            
        //     if (currentIndex < res.length) {
        //       const nextElement = res[currentIndex];
        //       console.log(nextElement); 
        // // this.searchQueuess(true);

        //       this.queuesPersist.searchQueues
        //       currentIndex++;
        //     }
          }, 5000);
        
      })
   
      this.queuessSort.sortChange.subscribe(() => {
        this.queuessPaginator.pageIndex = 0;
        // this.searchQueuess(true);
      });

      this.queuessPaginator.page.subscribe(() => {
        console.log("paginator",this.queuessPaginator.pageIndex)

        // this.searchQueuess(true);
      });
      //start by loading items
      // this.searchQueuess();
    }

    ngOnDestroy() {
      clearInterval(this.timer); 
    }
  

  searchQueuess(id:string,isPagination:boolean = false): void {


    let paginator = this.queuessPaginator;
    let sorter = this.queuessSort;

    this.queuesIsLoading = true;
    this.queuesSelection = [];

    this.queuesPersist.searchQueues(id,paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: QueuesSummaryPartialList) => {
      console.log("queues",partialList)
      let no = paginator.pageIndex *10 
      this.queuessData =partialList.data.map(data=>{
        no = no + 1
         return {...data,number:this.formatNumberWithLeadingZeros(no,3)}

      }) ;
      if (partialList.total != -1) {
        this.queuessTotalCount = partialList.total;
      }
      this.queuesIsLoading = false;
    }, error => {
      this.queuesIsLoading = false;
    });

  }

   formatNumberWithLeadingZeros(num: number, length: number): string {
    return num.toString().padStart(length, '0');
  }
   downloadQueuess(): void {
    if(this.queuesSelectAll){
         this.queuesPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download queues", true);
      });
    }
    else{
        this.queuesPersist.download(this.tcUtilsArray.idsList(this.queuesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download queues",true);
            });
        }
  }



addQueues(): void {
    let dialogRef = this.queuesNavigator.addQueues();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // this.searchQueuess();
      }
    });
  }

  editQueues(item: QueuesSummary) {
    let dialogRef = this.queuesNavigator.editQueues(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteQueues(item: QueuesSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("queues");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.queuesPersist.deleteQueues(item.id).subscribe(response => {
          this.tcNotification.success("queues deleted");
          // this.searchQueuess();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}