import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QueuesPickComponent } from './queues-pick.component';

describe('QueuesPickComponent', () => {
  let component: QueuesPickComponent;
  let fixture: ComponentFixture<QueuesPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QueuesPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QueuesPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
