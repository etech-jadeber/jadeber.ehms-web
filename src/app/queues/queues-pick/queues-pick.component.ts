import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { QueuesSummary, QueuesSummaryPartialList } from '../queues.model';
import { QueuesPersist } from '../queues.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-queues-pick',
  templateUrl: './queues-pick.component.html',
  styleUrls: ['./queues-pick.component.scss']
})export class QueuesPickComponent implements OnInit {
  queuessData: QueuesSummary[] = [];
  queuessTotalCount: number = 0;
  queuesSelectAll:boolean = false;
  queuesSelection: QueuesSummary[] = [];

 queuessDisplayedColumns: string[] = ["select","action","no" ];
  queuesSearchTextBox: FormControl = new FormControl();
  queuesIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) queuessPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) queuessSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public queuesPersist: QueuesPersist,
                public dialogRef: MatDialogRef<QueuesPickComponent>,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("queues");
       this.queuesSearchTextBox.setValue(queuesPersist.queuesSearchText);
      //delay subsequent keyup events
      this.queuesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.queuesPersist.queuesSearchText = value;
        // this.searchQueuess();
      });
    } ngOnInit() {
   
      this.queuessSort.sortChange.subscribe(() => {
        this.queuessPaginator.pageIndex = 0;
        // this.searchQueuess(true);
      });

      this.queuessPaginator.page.subscribe(() => {
        // this.searchQueuess(true);
      });
      //start by loading items
      // this.searchQueuess();
    }

  searchQueuess(isPagination:boolean = false): void {


    let paginator = this.queuessPaginator;
    let sorter = this.queuessSort;

    this.queuesIsLoading = true;
    this.queuesSelection = [];

    // this.queuesPersist.searchQueues(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: QueuesSummaryPartialList) => {
    //   this.queuessData = partialList.data;
    //   if (partialList.total != -1) {
    //     this.queuessTotalCount = partialList.total;
    //   }
    //   this.queuesIsLoading = false;
    // }, error => {
    //   this.queuesIsLoading = false;
    // });

  }
  markOneItem(item: QueuesSummary) {
    if(!this.tcUtilsArray.containsId(this.queuesSelection,item.id)){
          this.queuesSelection = [];
          this.queuesSelection.push(item);
        }
        else{
          this.queuesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.queuesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }