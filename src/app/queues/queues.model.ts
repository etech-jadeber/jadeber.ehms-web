import {TCId} from "../tc/models";export class QueuesSummary extends TCId {
    date:number;
    pid:string;
    provider_id:string;
    queue_type:string;
    queue_status:string;
    number:string;
    encounter_id:string;
    patient:string;
    doctor:string;
    order_id:string;
    
     
    }
    export class QueuesSummaryPartialList {
      data: QueuesSummary[];
      total: number;
    }
    export class QueuesDetail extends QueuesSummary {
    }
    
    export class QueuesDashboard {
      total: number;
    }