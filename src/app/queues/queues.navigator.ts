import {Router} from "@angular/router";
import {Injectable} from "@angular/core";


import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {QueuesEditComponent} from "./queues-edit/queues-edit.component";
import {QueuesPickComponent} from "./queues-pick/queues-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class QueuesNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  queuessUrl(): string {
    return "/queuess";
  }

  queuesUrl(id: string): string {
    return "/queuess/" + id;
  }

  viewQueuess(): void {
    this.router.navigateByUrl(this.queuessUrl());
  }

  viewQueues(id: string): void {
    this.router.navigateByUrl(this.queuesUrl(id));
  }

  editQueues(id: string): MatDialogRef<QueuesEditComponent> {
    const dialogRef = this.dialog.open(QueuesEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addQueues(): MatDialogRef<QueuesEditComponent> {
    const dialogRef = this.dialog.open(QueuesEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickQueuess(selectOne: boolean=false): MatDialogRef<QueuesPickComponent> {
      const dialogRef = this.dialog.open(QueuesPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}