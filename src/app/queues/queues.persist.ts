import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {QueuesDashboard, QueuesDetail, QueuesSummaryPartialList} from "./queues.model";


@Injectable({
  providedIn: 'root'
})
export class QueuesPersist {
 queuesSearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "queues/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.queuesSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchQueues(id:string,pageSize: number, pageIndex: number, sort: string, order: string,): Observable<QueuesSummaryPartialList> {

    let url = TCUtilsHttp.buildSocketSearchUrl("queues/get/"+id, this.queuesSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<QueuesSummaryPartialList>(url);

  } 
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "queues/do", new TCDoParam("download_all", this.filters()));
  }

  queuesDashboard(): Observable<QueuesDashboard> {
    return this.http.get<QueuesDashboard>(environment.socketUrl + "queues/dashboard");
  }

  getQueues(id: string): Observable<QueuesDetail> {
    return this.http.get<QueuesDetail>(environment.socketUrl + "queues/" + id);
  }
  getQueueDoctor(): Observable<any> {
    return this.http.get<any>(environment.socketUrl + "queues/getDoctors");
  }

  deleteQueues(id: string): Observable<any> {
    return this.http.delete(environment.socketUrl + "queues/" + id);
  }

  addQueues(item: any): Observable<any> {
    return this.http.post<any>(environment.socketUrl + "queues", item);
  }

  updateQueues(item: any,id:string): Observable<any> {
    return this.http.patch<any>(environment.socketUrl + "queues/" + id, item);
  }

  preEclampsiaChartsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "queues/do", new TCDoParam(method, payload));
  }

  preEclampsiaChartDo(item: QueuesDetail, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "queues/" + item.id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "queues/do", new TCDoParam("download", ids));
}
 }