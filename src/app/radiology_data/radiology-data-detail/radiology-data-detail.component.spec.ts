import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyDataDetailComponent } from './radiology-data-detail.component';

describe('RadiologyDataDetailComponent', () => {
  let component: RadiologyDataDetailComponent;
  let fixture: ComponentFixture<RadiologyDataDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyDataDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyDataDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
