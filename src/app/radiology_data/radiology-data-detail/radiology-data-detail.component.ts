import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { RadiologyDataDetail } from '../radiology_data.model';
import { RadiologyDataPersist } from '../radiology_data.persist';
import { RadiologyDataNavigator } from '../radiology_data.navigator';

export enum RadiologyDataTabs {
  overview,
}

export enum PaginatorIndexes {}
@Component({
  selector: 'app-radiology_data-detail',
  templateUrl: './radiology-data-detail.component.html',
  styleUrls: ['./radiology-data-detail.component.css'],
})
export class RadiologyDataDetailComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  paramsSubscription: Subscription;
  radiologyDataIsLoading: boolean = false;

  RadiologyDataTabs: typeof RadiologyDataTabs = RadiologyDataTabs;
  activeTab: RadiologyDataTabs = RadiologyDataTabs.overview;
  //basics
  radiologyDataDetail: RadiologyDataDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public radiologyDataNavigator: RadiologyDataNavigator,
    public radiologyDataPersist: RadiologyDataPersist
  ) {
    this.tcAuthorization.requireRead('radiology_data');
    this.radiologyDataDetail = new RadiologyDataDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('radiology_data');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.radiologyDataIsLoading = true;
    this.radiologyDataPersist.getRadiologyData(id).subscribe(
      (radiologyDataDetail) => {
        this.radiologyDataDetail = radiologyDataDetail;
        this.radiologyDataIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.radiologyDataIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.radiologyDataDetail.id);
  }
  editRadiologyData(): void {
    let modalRef = this.radiologyDataNavigator.editRadiologyData(
      this.radiologyDataDetail.id
    );
    modalRef.afterClosed().subscribe(
      (radiologyDataDetail) => {
        TCUtilsAngular.assign(this.radiologyDataDetail, radiologyDataDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.radiologyDataPersist
      .print(this.radiologyDataDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print radiology_data', true);
      });
  }

  back(): void {
    this.location.back();
  }
}
