import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyDataEditComponent } from './radiology-data-edit.component';

describe('RadiologyDataEditComponent', () => {
  let component: RadiologyDataEditComponent;
  let fixture: ComponentFixture<RadiologyDataEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyDataEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyDataEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
