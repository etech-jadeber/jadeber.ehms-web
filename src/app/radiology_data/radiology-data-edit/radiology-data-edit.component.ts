import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RadiologyDataDetail } from '../radiology_data.model';
import { RadiologyDataPersist } from '../radiology_data.persist';
@Component({
  selector: 'app-radiology_data-edit',
  templateUrl: './radiology-data-edit.component.html',
  styleUrls: ['./radiology-data-edit.component.css'],
})
export class RadiologyDataEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  radiologyDataDetail: RadiologyDataDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<RadiologyDataEditComponent>,
    public persist: RadiologyDataPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('radiology_data');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'radiology_data';
      this.radiologyDataDetail = new RadiologyDataDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('radiology_data');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'radiology_data';
      this.isLoadingResults = true;
      this.persist.getRadiologyData(this.idMode.id).subscribe(
        (radiologyDataDetail) => {
          this.radiologyDataDetail = radiologyDataDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addRadiologyData(this.radiologyDataDetail).subscribe(
      (value) => {
        this.tcNotification.success('radiologyData added');
        this.radiologyDataDetail.id = value.id;
        this.dialogRef.close(this.radiologyDataDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateRadiologyData(this.radiologyDataDetail).subscribe(
      (value) => {
        this.tcNotification.success('radiology_data updated');
        this.dialogRef.close(this.radiologyDataDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }
  canSubmit(): boolean {
    if (this.radiologyDataDetail == null) {
      return false;
    }

    if (
      this.radiologyDataDetail.encounter_id == null ||
      this.radiologyDataDetail.encounter_id == ''
    ) {
      return false;
    }
    return true;
    if (
      this.radiologyDataDetail.patient_id == null ||
      this.radiologyDataDetail.patient_id == ''
    ) {
      return false;
    }
    return true;
    if (
      this.radiologyDataDetail.rad_order_id == null ||
      this.radiologyDataDetail.rad_order_id == ''
    ) {
      return false;
    }
    return true;
    if (
      this.radiologyDataDetail.description == null ||
      this.radiologyDataDetail.description == ''
    ) {
      return false;
    }
    return true;
    if (this.radiologyDataDetail.ordered_date == null) {
      return false;
    }
    if (
      this.radiologyDataDetail.radiographer_id == null ||
      this.radiologyDataDetail.radiographer_id == ''
    ) {
      return false;
    }
    return true;
    if (this.radiologyDataDetail.result_date == null) {
      return false;
    }
    if (
      this.radiologyDataDetail.radiologist_id == null ||
      this.radiologyDataDetail.radiologist_id == ''
    ) {
      return false;
    }
    return true;
    if (
      this.radiologyDataDetail.result_description == null ||
      this.radiologyDataDetail.result_description == ''
    ) {
      return false;
    }
    return true;
    if (
      this.radiologyDataDetail.result_conclusion == null ||
      this.radiologyDataDetail.result_conclusion == ''
    ) {
      return false;
    }
    return true;
    if (this.radiologyDataDetail.approved_date == null) {
      return false;
    }
    if (this.radiologyDataDetail.status == null) {
      return false;
    }
  }
}
