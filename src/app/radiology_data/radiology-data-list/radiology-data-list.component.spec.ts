import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyDataListComponent } from './radiology-data-list.component';

describe('RadiologyDataListComponent', () => {
  let component: RadiologyDataListComponent;
  let fixture: ComponentFixture<RadiologyDataListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyDataListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyDataListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
