import {
  Component,
  OnInit,
  ViewChild,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator'
import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';
import {
  RadiologyDataSummary,
  RadiologyDataSummaryPartialList,
} from '../radiology_data.model';
import { RadiologyDataPersist } from '../radiology_data.persist';
import { RadiologyDataNavigator } from '../radiology_data.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-radiology_data-list',
  templateUrl: './radiology-data-list.component.html',
  styleUrls: ['./radiology-data-list.component.css'],
})
export class RadiologyDataListComponent implements OnInit {
  radiologyDatasData: RadiologyDataSummary[] = [];
  radiologyDatasTotalCount: number = 0;
  radiologyDataSelectAll: boolean = false;
  radiologyDataSelection: RadiologyDataSummary[] = [];

  radiologyDatasDisplayedColumns: string[] = [
    'select',
    'action',
    'link',
    'encounter_id',
    'patient_id',
    'rad_order_id',
    'description',
    'ordered_date',
    'radiographer_id',
    'result_date',
    'radiologist_id',
    'result_description',
    'result_conclusion',
    'approved_date',
    'status',
  ];
  radiologyDataSearchTextBox: FormControl = new FormControl();
  radiologyDataIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  radiologyDatasPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) radiologyDatasSort: MatSort;
  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public radiologyDataPersist: RadiologyDataPersist,
    public radiologyDataNavigator: RadiologyDataNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob
  ) {
    this.tcAuthorization.requireRead('radiology_data');
    this.radiologyDataSearchTextBox.setValue(
      radiologyDataPersist.radiologyDataSearchText
    );
    //delay subsequent keyup events
    this.radiologyDataSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.radiologyDataPersist.radiologyDataSearchText = value;
        this.searchRadiologyDatas();
      });
  }
  ngOnInit() {
    this.radiologyDatasSort.sortChange.subscribe(() => {
      this.radiologyDatasPaginator.pageIndex = 0;
      this.searchRadiologyDatas(true);
    });

    this.radiologyDatasPaginator.page.subscribe(() => {
      this.searchRadiologyDatas(true);
    });
    //start by loading items
    this.searchRadiologyDatas();
  }

  searchRadiologyDatas(isPagination: boolean = false): void {
    let paginator = this.radiologyDatasPaginator;
    let sorter = this.radiologyDatasSort;

    this.radiologyDataIsLoading = true;
    this.radiologyDataSelection = [];

    this.radiologyDataPersist
      .searchRadiologyData(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: RadiologyDataSummaryPartialList) => {
          this.radiologyDatasData = partialList.data;
          if (partialList.total != -1) {
            this.radiologyDatasTotalCount = partialList.total;
          }
          this.radiologyDataIsLoading = false;
        },
        (error) => {
          this.radiologyDataIsLoading = false;
        }
      );
  }
  downloadRadiologyDatas(): void {
    if (this.radiologyDataSelectAll) {
      this.radiologyDataPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download radiology_data',
          true
        );
      });
    } else {
      this.radiologyDataPersist
        .download(this.tcUtilsArray.idsList(this.radiologyDataSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download radiology_data',
            true
          );
        });
    }
  }
  addRadiologyData(): void {
    let dialogRef = this.radiologyDataNavigator.addRadiologyData();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchRadiologyDatas();
      }
    });
  }

  editRadiologyData(item: RadiologyDataSummary) {
    let dialogRef = this.radiologyDataNavigator.editRadiologyData(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteRadiologyData(item: RadiologyDataSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('radiology_data');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.radiologyDataPersist.deleteRadiologyData(item.id).subscribe(
          (response) => {
            this.tcNotification.success('radiology_data deleted');
            this.searchRadiologyDatas();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }
}
