import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyDataPickComponent } from './radiology-data-pick.component';

describe('RadiologyDataPickComponent', () => {
  let component: RadiologyDataPickComponent;
  let fixture: ComponentFixture<RadiologyDataPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyDataPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyDataPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
