import { TCId } from '../tc/models';
export class RadiologyDataSummary extends TCId {
  encounter_id: string;
  patient_id: string;
  rad_order_id: string;
  description: string;
  ordered_date: number;
  radiographer_id: string;
  result_date: number;
  radiologist_id: string;
  result_description: string;
  result_conclusion: string;
  approved_date: number;
  status: number;
}
export class RadiologyDataSummaryPartialList {
  data: RadiologyDataSummary[];
  total: number;
}
export class RadiologyDataDetail extends RadiologyDataSummary {}

export class RadiologyDataDashboard {
  total: number;
}
