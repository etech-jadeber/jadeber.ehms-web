import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { RadiologyDataEditComponent } from './radiology-data-edit/radiology-data-edit.component';
import { RadiologyDataPickComponent } from './radiology-data-pick/radiology-data-pick.component';

@Injectable({
  providedIn: 'root',
})
export class RadiologyDataNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  radiologyDatasUrl(): string {
    return '/radiology_datas';
  }

  radiologyDataUrl(id: string): string {
    return '/radiology_datas/' + id;
  }

  viewRadiologyDatas(): void {
    this.router.navigateByUrl(this.radiologyDatasUrl());
  }

  viewRadiologyData(id: string): void {
    this.router.navigateByUrl(this.radiologyDataUrl(id));
  }

  editRadiologyData(id: string): MatDialogRef<RadiologyDataEditComponent> {
    const dialogRef = this.dialog.open(RadiologyDataEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addRadiologyData(): MatDialogRef<RadiologyDataEditComponent> {
    const dialogRef = this.dialog.open(RadiologyDataEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickRadiologyDatas(
    selectOne: boolean = false
  ): MatDialogRef<RadiologyDataPickComponent> {
    const dialogRef = this.dialog.open(RadiologyDataPickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
