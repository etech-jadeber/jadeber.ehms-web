import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary } from '../tc/models';
import {
  RadiologyDataDashboard,
  RadiologyDataDetail,
  RadiologyDataSummaryPartialList,
} from './radiology_data.model';

@Injectable({
  providedIn: 'root',
})
export class RadiologyDataPersist {
  radiologyDataSearchText: string = '';
  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.radiologyDataSearchText;
    //add custom filters
    return fltrs;
  }

  searchRadiologyData(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<RadiologyDataSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'radiology_data',
      this.radiologyDataSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    return this.http.get<RadiologyDataSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'radiology_data/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  radiologyDataDashboard(): Observable<RadiologyDataDashboard> {
    return this.http.get<RadiologyDataDashboard>(
      environment.tcApiBaseUri + 'radiology_data/dashboard'
    );
  }

  getRadiologyData(id: string): Observable<RadiologyDataDetail> {
    return this.http.get<RadiologyDataDetail>(
      environment.tcApiBaseUri + 'radiology_data/' + id
    );
  }
  addRadiologyData(item: RadiologyDataDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'radiology_data/',
      item
    );
  }

  updateRadiologyData(
    item: RadiologyDataDetail
  ): Observable<RadiologyDataDetail> {
    return this.http.patch<RadiologyDataDetail>(
      environment.tcApiBaseUri + 'radiology_data/' + item.id,
      item
    );
  }

  deleteRadiologyData(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'radiology_data/' + id);
  }
  radiologyDatasDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'radiology_data/do',
      new TCDoParam(method, payload)
    );
  }

  radiologyDataDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'radiology_datas/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'radiology_data/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'radiology_data/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
