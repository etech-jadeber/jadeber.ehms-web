import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultDetailComponent } from './radiology-result-detail.component';

describe('RadiologyResultDetailComponent', () => {
  let component: RadiologyResultDetailComponent;
  let fixture: ComponentFixture<RadiologyResultDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
