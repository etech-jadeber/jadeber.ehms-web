import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {RadiologyResultDetail} from "../radiology_result.model";
import {RadiologyResultPersist} from "../radiology_result.persist";
import {RadiologyResultNavigator} from "../radiology_result.navigator";

export enum RadiologyResultTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-radiology_result-detail',
  templateUrl: './radiology-result-detail.component.html',
  styleUrls: ['./radiology-result-detail.component.scss']
})
export class RadiologyResultDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  radiologyResultIsLoading:boolean = false;

  RadiologyResultTabs: typeof RadiologyResultTabs = RadiologyResultTabs;
  activeTab: RadiologyResultTabs = RadiologyResultTabs.overview;
  //basics
  radiologyResultDetail: RadiologyResultDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public radiologyResultNavigator: RadiologyResultNavigator,
              public  radiologyResultPersist: RadiologyResultPersist) {
    this.tcAuthorization.requireRead("radiology_results");
    this.radiologyResultDetail = new RadiologyResultDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("radiology_results");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.radiologyResultIsLoading = true;
    this.radiologyResultPersist.getRadiologyResult(id).subscribe(radiologyResultDetail => {
          this.radiologyResultDetail = radiologyResultDetail;
          this.radiologyResultIsLoading = false;
        }, error => {
          console.error(error);
          this.radiologyResultIsLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.radiologyResultDetail.id);
  }
  editRadiologyResult(): void {
    let modalRef = this.radiologyResultNavigator.editRadiologyResult(this.radiologyResultDetail);
    modalRef.afterClosed().subscribe(radiologyResultDetail => {
      TCUtilsAngular.assign(this.radiologyResultDetail, radiologyResultDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.radiologyResultPersist.print(this.radiologyResultDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print radiology_result", true);
      });
    }

  back():void{
      this.location.back();
    }

}
