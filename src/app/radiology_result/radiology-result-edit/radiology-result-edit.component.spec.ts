import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultEditComponent } from './radiology-result-edit.component';

describe('RadiologyResultEditComponent', () => {
  let component: RadiologyResultEditComponent;
  let fixture: ComponentFixture<RadiologyResultEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
