import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RadiologyResultDetail } from '../radiology_result.model';import { RadiologyResultPersist } from '../radiology_result.persist';import { RadiologyResultSettingsNavigator } from 'src/app/radiology_result_settings/radiology_result_settings.navigator';
import { PhysicalExaminationState, selected_orders_status } from 'src/app/app.enums';
import { RadiologyResultSettingsDetail, RadiologyResultSettingsSummaryPartialList } from 'src/app/radiology_result_settings/radiology_result_settings.model';
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { HistoryPersist } from 'src/app/historys/history.persist';
import { DiagnosisPersist } from 'src/app/form_encounters/diagnosiss/diagnosis.persist';
import { TestRadTemplateNavigator } from 'src/app/test-rad-templates/test-rad-templates.navigator';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';

@Component({
  selector: 'app-radiology_result-edit',
  templateUrl: './radiology-result-edit.component.html',
  styleUrls: ['./radiology-result-edit.component.scss']
})export class RadiologyResultEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  physicalExaminationStatus = PhysicalExaminationState
  title: string;
  selectedOrderStatus = selected_orders_status
  radiologyResultDetail: RadiologyResultDetail;
  isOnlyShow: boolean;
  Editor = ClassicEditor;
  chief_complaint: string;
  impression_primary_diagnosis: string;
  canImport = false;
  config = {};


  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<RadiologyResultEditComponent>,
              public  persist: RadiologyResultPersist,
              public testRadTemplateNavigator: TestRadTemplateNavigator,
              public tcUtilsDate: TCUtilsDate,
              public historyPersist: HistoryPersist,
              public diagnosisPersist: DiagnosisPersist,
              public radiologyResultSettingPersist: RadiologyResultSettingsPersist,
              public radiologyResultSettingNavigator: RadiologyResultSettingsNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: {result: RadiologyResultDetail, encounter_id: string, mode: string}) {
                this.radiologyResultSettingPersist.lab_test_id = this.idMode.result.lab_test_id
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.WIZARD){
      this.title = "Radiology Result";
      this.radiologyResultDetail = this.idMode.result;
      this.radiologyResultDetail.finding = this.radiologyResultDetail.finding || ""
      this.radiologyResultDetail.technique = this.radiologyResultDetail.technique || ""
      this.radiologyResultDetail.conclusion = this.radiologyResultDetail.conclusion || ""
      this.radiologyResultDetail.recommendation = this.radiologyResultDetail.recommendation || ""
      //set necessary defaults
      this.isOnlyShow = true
      this.config['toolbar'] = []
    }
    else if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("radiology_results");
      this.title = this.appTranslation.getText("general","new") +  " " + "radiology_result";
      this.radiologyResultDetail = this.idMode.result;
      this.radiologyResultDetail.finding = this.radiologyResultDetail.finding || ""
      this.radiologyResultDetail.technique = this.radiologyResultDetail.technique || ""
      this.radiologyResultDetail.conclusion = this.radiologyResultDetail.conclusion || ""
      this.radiologyResultDetail.recommendation = this.radiologyResultDetail.recommendation || ""
      //set necessary defaults
      this.isOnlyShow = false
      this.getClinicalFindings()
      this.getTestFilter()

    } else {
     this.tcAuthorization.requireUpdate("radiology_results");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "radiology_result";
      this.radiologyResultDetail = this.idMode.result;

    }


  }
  searchRadResultTemplate() {
    let dialogRef = this.radiologyResultSettingNavigator.pickRadiologyResultSettingss(true, this.radiologyResultDetail.lab_test_id)
    dialogRef.afterClosed().subscribe(
      (value: RadiologyResultSettingsDetail []) => {
        if (value) {
          delete value[0].id
          this.radiologyResultDetail = {...this.radiologyResultDetail, ...value[0]}
          // if (status == PhysicalExaminationState.abnormal){
          //   this.radiologyResultDetail.abnormal_result += (this.radiologyResultDetail.abnormal_result ? "\n" : "") + value.map(val => "\u2022" + " " + val.value).join('\n')
          // } else {
          //   this.radiologyResultDetail.normal_result += (this.radiologyResultDetail.normal_result ? "\n" : "") + value.map(val => "\u2022" + " " + val.value).join('\n')
          // }
        }
      }
    )
  }

  getTestFilter() {
    this.radiologyResultSettingPersist.searchRadiologyResultSettings(1, 0, 'id', 'asc').subscribe(
      (radiologySetting: RadiologyResultSettingsSummaryPartialList) => {
        delete radiologySetting.data[0].id
        this.radiologyResultDetail = {...this.radiologyResultDetail, ...radiologySetting.data[0]}
        if(radiologySetting.total > 1){
          this.canImport = true;
        }
      }
    )
  }

  getClinicalFindings() {
    this.historyPersist.searchHistory(this.idMode.encounter_id, 1, 0, 'date_of_history', 'desc').subscribe(
      history => {
        this.radiologyResultDetail.chief_complaint = (history.data[0]?.chief_complaint || "")
      }
    )
    this.diagnosisPersist.searchDiagnosis(this.idMode.encounter_id, 1, 0, 'date', 'desc').subscribe(
      diagnosis => {
          this.radiologyResultDetail.impression_preliminary_diagnosis = (diagnosis.data[0].name || "");
      }
    )
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addRadiologyResult(this.radiologyResultDetail).subscribe(value => {
      this.tcNotification.success("radiologyResult added");
      this.radiologyResultDetail.id = value.id;
      this.dialogRef.close(this.radiologyResultDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateRadiologyResult(this.radiologyResultDetail).subscribe(value => {
      this.tcNotification.success("radiology_result updated");
      this.dialogRef.close(this.radiologyResultDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.radiologyResultDetail == null){
            return false;
          }


if (this.radiologyResultDetail.finding == null || this.radiologyResultDetail.finding  == "") {
            return false;
        }
if (this.radiologyResultDetail.technique == null || this.radiologyResultDetail.technique  == "") {
            return false;
        }
 return true;

 }

 handleInput(event: string, input) {
  if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
  else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
    input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
  }
 }
