import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultListComponent } from './radiology-result-list.component';

describe('RadiologyResultListComponent', () => {
  let component: RadiologyResultListComponent;
  let fixture: ComponentFixture<RadiologyResultListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
