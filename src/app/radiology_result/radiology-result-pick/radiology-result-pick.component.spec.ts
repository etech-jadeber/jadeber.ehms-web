import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultPickComponent } from './radiology-result-pick.component';

describe('RadiologyResultPickComponent', () => {
  let component: RadiologyResultPickComponent;
  let fixture: ComponentFixture<RadiologyResultPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
