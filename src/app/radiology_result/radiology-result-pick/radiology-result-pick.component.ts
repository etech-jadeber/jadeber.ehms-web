import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RadiologyResultSummary, RadiologyResultSummaryPartialList } from '../radiology_result.model';
import { RadiologyResultPersist } from '../radiology_result.persist';
import { RadiologyResultNavigator } from '../radiology_result.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-radiology_result-pick',
  templateUrl: './radiology-result-pick.component.html',
  styleUrls: ['./radiology-result-pick.component.scss']
})export class RadiologyResultPickComponent implements OnInit {
  radiologyResultsData: RadiologyResultSummary[] = [];
  radiologyResultsTotalCount: number = 0;
  radiologyResultSelectAll:boolean = false;
  radiologyResultSelection: RadiologyResultSummary[] = [];

 radiologyResultsDisplayedColumns: string[] = ["select", ,"selected_order_id","result_by","confirmed_by","status","result_date","lab_test_id","result_file_id","normal_result","abnormal_result","conclusion" ];
  radiologyResultSearchTextBox: FormControl = new FormControl();
  radiologyResultIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) radiologyResultsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) radiologyResultsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public radiologyResultPersist: RadiologyResultPersist,
                public radiologyResultNavigator: RadiologyResultNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<RadiologyResultPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("radiology_result");
       this.radiologyResultSearchTextBox.setValue(radiologyResultPersist.radiologyResultSearchText);
      //delay subsequent keyup events
      this.radiologyResultSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.radiologyResultPersist.radiologyResultSearchText = value;
        this.searchRadiology_results();
      });
    } ngOnInit() {
   
      this.radiologyResultsSort.sortChange.subscribe(() => {
        this.radiologyResultsPaginator.pageIndex = 0;
        this.searchRadiology_results(true);
      });

      this.radiologyResultsPaginator.page.subscribe(() => {
        this.searchRadiology_results(true);
      });
      //start by loading items
      this.searchRadiology_results();
    }

  searchRadiology_results(isPagination:boolean = false): void {


    let paginator = this.radiologyResultsPaginator;
    let sorter = this.radiologyResultsSort;

    this.radiologyResultIsLoading = true;
    this.radiologyResultSelection = [];

    this.radiologyResultPersist.searchRadiologyResult(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RadiologyResultSummaryPartialList) => {
      this.radiologyResultsData = partialList.data;
      if (partialList.total != -1) {
        this.radiologyResultsTotalCount = partialList.total;
      }
      this.radiologyResultIsLoading = false;
    }, error => {
      this.radiologyResultIsLoading = false;
    });

  }
  markOneItem(item: RadiologyResultSummary) {
    if(!this.tcUtilsArray.containsId(this.radiologyResultSelection,item.id)){
          this.radiologyResultSelection = [];
          this.radiologyResultSelection.push(item);
        }
        else{
          this.radiologyResultSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.radiologyResultSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
