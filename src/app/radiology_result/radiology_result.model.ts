import {TCId} from "../tc/models";

export class RadiologyResultSummary extends TCId {
    selected_order_id:string;
    result_by:string;
    confirmed_by:string;
    status:number;
    result_date:number;
    lab_test_id:string;
    result_file_id:string;
    finding:string;
    technique:string;
    recommendation:string;
    conclusion:string;
    chief_complaint: string;
    impression_preliminary_diagnosis: string;
    test_unit_id: string;
    company_id: string;

    }
    export class RadiologyResultSummaryPartialList {
      data: RadiologyResultSummary[];
      total: number;
    }
    export class RadiologyResultDetail extends RadiologyResultSummary {
    }

    export class RadiologyResultDashboard {
      total: number;
    }
