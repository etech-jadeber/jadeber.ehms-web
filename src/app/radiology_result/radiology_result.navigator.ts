import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {RadiologyResultEditComponent} from "./radiology-result-edit/radiology-result-edit.component";
import {RadiologyResultPickComponent} from "./radiology-result-pick/radiology-result-pick.component";
import { RadiologyResultDetail } from "./radiology_result.model";


@Injectable({
  providedIn: 'root'
})

export class RadiologyResultNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  radiologyResultsUrl(): string {
    return "/radiology_results";
  }

  radiologyResultUrl(id: string): string {
    return "/radiology_results/" + id;
  }

  viewRadiologyResults(): void {
    this.router.navigateByUrl(this.radiologyResultsUrl());
  }

  viewRadiologyResult(id: string): void {
    this.router.navigateByUrl(this.radiologyResultUrl(id));
  }

  editRadiologyResult(result: RadiologyResultDetail): MatDialogRef<RadiologyResultEditComponent> {
    const dialogRef = this.dialog.open(RadiologyResultEditComponent, {
      data: {result, mode: TCModalModes.EDIT},
      width: TCModalWidths.xtra_large,
      disableClose: true
    });
    return dialogRef;
  }

  addRadiologyResult(result: RadiologyResultDetail, encounter_id: string): MatDialogRef<RadiologyResultEditComponent> {
    const dialogRef = this.dialog.open(RadiologyResultEditComponent, {
          data: {result, encounter_id, mode: TCModalModes.NEW},
          width: TCModalWidths.xtra_large,
          disableClose: true
    });
    return dialogRef;
  }

  showRadiologyResult(result: RadiologyResultDetail): MatDialogRef<RadiologyResultEditComponent> {
    const dialogRef = this.dialog.open(RadiologyResultEditComponent, {
          data: {result, mode: TCModalModes.WIZARD},
          width: TCModalWidths.xtra_large,
          disableClose: true,
    });
    return dialogRef;
  }

   pickRadiologyResults(selectOne: boolean=false): MatDialogRef<RadiologyResultPickComponent> {
      const dialogRef = this.dialog.open(RadiologyResultPickComponent, {
        data: selectOne,
        width: TCModalWidths.large,
        disableClose: true,
      });
      return dialogRef;
    }
}
