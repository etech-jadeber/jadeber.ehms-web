import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {RadiologyResultDashboard, RadiologyResultDetail, RadiologyResultSummaryPartialList} from "./radiology_result.model";


@Injectable({
  providedIn: 'root'
})
export class RadiologyResultPersist {
 radiologyResultSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.radiologyResultSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchRadiologyResult(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RadiologyResultSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("radiology_result", this.radiologyResultSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<RadiologyResultSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result/do", new TCDoParam("download_all", this.filters()));
  }

  radiologyResultDashboard(): Observable<RadiologyResultDashboard> {
    return this.http.get<RadiologyResultDashboard>(environment.tcApiBaseUri + "radiology_result/dashboard");
  }

  getRadiologyResult(id: string): Observable<RadiologyResultDetail> {
    return this.http.get<RadiologyResultDetail>(environment.tcApiBaseUri + "radiology_result/" + id);
  } addRadiologyResult(item: RadiologyResultDetail): Observable<TCId> {
    if (item.id != null){
      return this.updateRadiologyResult(item)
  }
    return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result/", item);
  }

  updateRadiologyResult(item: RadiologyResultDetail): Observable<RadiologyResultDetail> {
    return this.http.patch<RadiologyResultDetail>(environment.tcApiBaseUri + "radiology_result/" + item.id, item);
  }

  deleteRadiologyResult(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "radiology_result/" + id);
  }
 radiologyResultsDo(method: string, payload: any, pageSize?: number, pageIndex?: number, sort?: string, order?: string,): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("radiology_result/do", this.radiologyResultSearchText, pageSize, pageIndex, sort, order);
    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }

  radiologyResultDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "radiology_result/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result/" + id + "/do", new TCDoParam("print", {}));
  }


}
