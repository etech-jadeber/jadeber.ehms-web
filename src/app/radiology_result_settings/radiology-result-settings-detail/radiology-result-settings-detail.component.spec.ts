import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultSettingsDetailComponent } from './radiology-result-settings-detail.component';

describe('RadiologyResultSettingsDetailComponent', () => {
  let component: RadiologyResultSettingsDetailComponent;
  let fixture: ComponentFixture<RadiologyResultSettingsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultSettingsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultSettingsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
