import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {RadiologyResultSettingsDetail} from "../radiology_result_settings.model";
import {RadiologyResultSettingsPersist} from "../radiology_result_settings.persist";
import {RadiologyResultSettingsNavigator} from "../radiology_result_settings.navigator";
// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';

export enum RadiologyResultSettingsTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-radiology_result_settings-detail',
  templateUrl: './radiology-result-settings-detail.component.html',
  styleUrls: ['./radiology-result-settings-detail.component.scss']
})
export class RadiologyResultSettingsDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  radiologyResultSettingsIsLoading:boolean = false;
  Editor = ClassicEditor;

  RadiologyResultSettingsTabs: typeof RadiologyResultSettingsTabs = RadiologyResultSettingsTabs;
  activeTab: RadiologyResultSettingsTabs = RadiologyResultSettingsTabs.overview;
  //basics
  radiologyResultSettingsDetail: RadiologyResultSettingsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public radiologyResultSettingsNavigator: RadiologyResultSettingsNavigator,
              public  radiologyResultSettingsPersist: RadiologyResultSettingsPersist) {
    this.tcAuthorization.requireRead("radiology_result_settings");
    this.radiologyResultSettingsDetail = new RadiologyResultSettingsDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("radiology_result_settings");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.radiologyResultSettingsIsLoading = true;
    this.radiologyResultSettingsPersist.getRadiologyResultSettings(id).subscribe(radiologyResultSettingsDetail => {
          this.radiologyResultSettingsDetail = radiologyResultSettingsDetail;
          this.radiologyResultSettingsIsLoading = false;
        }, error => {
          console.error(error);
          this.radiologyResultSettingsIsLoading = false;
        });
  }

  reload(){
    this.loadDetails(this.radiologyResultSettingsDetail.id);
  }
  editRadiologyResultSettings(): void {
    let modalRef = this.radiologyResultSettingsNavigator.editRadiologyResultSettings(this.radiologyResultSettingsDetail.id);
    modalRef.afterClosed().subscribe(radiologyResultSettingsDetail => {
      TCUtilsAngular.assign(this.radiologyResultSettingsDetail, radiologyResultSettingsDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.radiologyResultSettingsPersist.print(this.radiologyResultSettingsDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print radiology_result_settings", true);
      });
    }

  back():void{
      this.location.back();
    }

}
