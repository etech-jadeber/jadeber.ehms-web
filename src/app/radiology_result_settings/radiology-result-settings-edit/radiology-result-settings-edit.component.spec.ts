import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultSettingsEditComponent } from './radiology-result-settings-edit.component';

describe('RadiologyResultSettingsEditComponent', () => {
  let component: RadiologyResultSettingsEditComponent;
  let fixture: ComponentFixture<RadiologyResultSettingsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultSettingsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultSettingsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
