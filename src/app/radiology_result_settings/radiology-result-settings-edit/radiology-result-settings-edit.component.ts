import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RadiologyResultSettingsDetail } from '../radiology_result_settings.model';import { RadiologyResultSettingsPersist } from '../radiology_result_settings.persist';import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { lab_order_type } from 'src/app/app.enums';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestDetail } from 'src/app/lab_tests/lab_test.model';

@Component({
  selector: 'app-radiology_result_settings-edit',
  templateUrl: './radiology-result-settings-edit.component.html',
  styleUrls: ['./radiology-result-settings-edit.component.scss']
})export class RadiologyResultSettingsEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  orderName: string;
  Editor = ClassicEditor;
  config = {}

  radiologyResultSettingsDetail: RadiologyResultSettingsDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<RadiologyResultSettingsEditComponent>,
              public  persist: RadiologyResultSettingsPersist,
              public labOrderNavigator: Lab_TestNavigator,
              public physiocalExaminationPersist : PhysicalExaminationOptionsPersist,
              public tcUtilsDate: TCUtilsDate,
              public lab_testNavigator: Lab_TestNavigator,
              public lab_testPersist: Lab_TestPersist,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("radiology_result_settings");
      this.title = this.appTranslation.getText("general","new") +  " " + "radiology_result_settings";
      this.radiologyResultSettingsDetail = new RadiologyResultSettingsDetail();
      this.radiologyResultSettingsDetail.lab_test_id = this.idMode.id
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("radiology_result_settings");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "radiology_result_settings";
      this.isLoadingResults = true;
      this.persist.getRadiologyResultSettings(this.idMode.id).subscribe(radiologyResultSettingsDetail => {
        this.radiologyResultSettingsDetail = radiologyResultSettingsDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addRadiologyResultSettings(this.radiologyResultSettingsDetail).subscribe(value => {
      this.tcNotification.success("radiologyResultSettings added");
      this.radiologyResultSettingsDetail.id = value.id;
      this.dialogRef.close(this.radiologyResultSettingsDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateRadiologyResultSettings(this.radiologyResultSettingsDetail).subscribe(value => {
      this.tcNotification.success("radiology_result_settings updated");
      this.dialogRef.close(this.radiologyResultSettingsDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.radiologyResultSettingsDetail == null){
            return false;
          }

if (this.radiologyResultSettingsDetail.template_name == null || this.radiologyResultSettingsDetail.template_name  == "") {
            return false;
        }
 return true;

 }
 }
