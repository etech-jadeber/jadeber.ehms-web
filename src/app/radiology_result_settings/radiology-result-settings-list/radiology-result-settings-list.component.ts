import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RadiologyResultSettingsSummary, RadiologyResultSettingsSummaryPartialList } from '../radiology_result_settings.model';
import { RadiologyResultSettingsPersist } from '../radiology_result_settings.persist';
import { RadiologyResultSettingsNavigator } from '../radiology_result_settings.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
import { Lab_OrderNavigator } from 'src/app/lab_orders/lab_order.navigator';
import { Lab_OrderDetail } from 'src/app/lab_orders/lab_order.model';
import { lab_order_type } from 'src/app/app.enums';
import { Lab_OrderPersist } from 'src/app/lab_orders/lab_order.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
@Component({
  selector: 'app-radiology_result_settings-list',
  templateUrl: './radiology-result-settings-list.component.html',
  styleUrls: ['./radiology-result-settings-list.component.scss']
})export class RadiologyResultSettingsListComponent implements OnInit {
  radiologyResultSettingssData: RadiologyResultSettingsSummary[] = [];
  radiologyResultSettingssTotalCount: number = 0;
  radiologyResultSettingsSelectAll:boolean = false;
  radiologyResultSettingsSelection: RadiologyResultSettingsSummary[] = [];
  orderName: string;
 radiologyResultSettingssDisplayedColumns: string[] = ["select","action","template_name" ];
  radiologyResultSettingsSearchTextBox: FormControl = new FormControl();
  radiologyResultSettingsIsLoading: boolean = false;

  @Input() lab_test_id: string;
  @ViewChild(MatPaginator, {static: true}) radiologyResultSettingssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) radiologyResultSettingssSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public radiologyResultSettingsPersist: RadiologyResultSettingsPersist,
                public radiologyResultSettingsNavigator: RadiologyResultSettingsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public physicalExaminationPersist: PhysicalExaminationOptionsPersist,
                public labOrderNavigator: Lab_TestNavigator,
                public labOrderPersist: Lab_TestPersist,

    ) {

        this.tcAuthorization.requireRead("radiology_result_settings");
       this.radiologyResultSettingsSearchTextBox.setValue(radiologyResultSettingsPersist.radiologyResultSettingsSearchText);
      //delay subsequent keyup events
      this.radiologyResultSettingsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.radiologyResultSettingsPersist.radiologyResultSettingsSearchText = value;
        this.searchRadiologyResultSettingss();
      });
    } ngOnInit() {

      this.radiologyResultSettingsPersist.lab_test_id = this.lab_test_id
      this.radiologyResultSettingssSort.sortChange.subscribe(() => {
        this.radiologyResultSettingssPaginator.pageIndex = 0;
        this.searchRadiologyResultSettingss(true);
      });

      this.radiologyResultSettingssPaginator.page.subscribe(() => {
        this.searchRadiologyResultSettingss(true);
      });
      //start by loading items
      this.searchRadiologyResultSettingss();
    }

  searchRadiologyResultSettingss(isPagination:boolean = false): void {


    let paginator = this.radiologyResultSettingssPaginator;
    let sorter = this.radiologyResultSettingssSort;

    this.radiologyResultSettingsIsLoading = true;
    this.radiologyResultSettingsSelection = [];

    this.radiologyResultSettingsPersist.searchRadiologyResultSettings(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RadiologyResultSettingsSummaryPartialList) => {
      this.radiologyResultSettingssData = partialList.data;
      if (partialList.total != -1) {
        this.radiologyResultSettingssTotalCount = partialList.total;
      }
      this.radiologyResultSettingsIsLoading = false;
    }, error => {
      this.radiologyResultSettingsIsLoading = false;
    });

  } downloadRadiologyResultSettingss(): void {
    if(this.radiologyResultSettingsSelectAll){
         this.radiologyResultSettingsPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download radiology_result_settings", true);
      });
    }
    else{
        this.radiologyResultSettingsPersist.download(this.tcUtilsArray.idsList(this.radiologyResultSettingsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download radiology_result_settings",true);
            });
        }
  }
  ngOnDestroy(){
    this.radiologyResultSettingsPersist.status = null;
    this.radiologyResultSettingsPersist.lab_test_id = null;
  }
addRadiologyResultSettings(): void {
    let dialogRef = this.radiologyResultSettingsNavigator.addRadiologyResultSettings(this.lab_test_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchRadiologyResultSettingss();
      }
    });
  }

  editRadiologyResultSettings(item: RadiologyResultSettingsSummary) {
    let dialogRef = this.radiologyResultSettingsNavigator.editRadiologyResultSettings(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  searchOrder(){
    let dialogRef = this.labOrderNavigator.pickLab_Tests(true, null, lab_order_type.imaging)
    dialogRef.afterClosed().subscribe(
      (value : Lab_OrderDetail[]) => {
        if (value){
          this.radiologyResultSettingsPersist.lab_test_id = value[0].id
          this.orderName = value[0].name
          this.searchRadiologyResultSettingss()
        }
      }
    )
  }
  deleteRadiologyResultSettings(item: RadiologyResultSettingsSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("radiology_result_settings");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.radiologyResultSettingsPersist.deleteRadiologyResultSettings(item.id).subscribe(response => {
          this.tcNotification.success("radiology_result_settings deleted");
          this.searchRadiologyResultSettingss();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
