import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiologyResultSettingsPickComponent } from './radiology-result-settings-pick.component';

describe('RadiologyResultSettingsPickComponent', () => {
  let component: RadiologyResultSettingsPickComponent;
  let fixture: ComponentFixture<RadiologyResultSettingsPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RadiologyResultSettingsPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiologyResultSettingsPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
