import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RadiologyResultSettingsSummary, RadiologyResultSettingsSummaryPartialList } from '../radiology_result_settings.model';
import { RadiologyResultSettingsPersist } from '../radiology_result_settings.persist';
import { RadiologyResultSettingsNavigator } from '../radiology_result_settings.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { PhysicalExaminationOptionsPersist } from 'src/app/physical_examination_options/physical_examination_options.persist';
@Component({
  selector: 'app-radiology_result_settings-pick',
  templateUrl: './radiology-result-settings-pick.component.html',
  styleUrls: ['./radiology-result-settings-pick.component.scss']
})export class RadiologyResultSettingsPickComponent implements OnInit {
  radiologyResultSettingssData: RadiologyResultSettingsSummary[] = [];
  radiologyResultSettingssTotalCount: number = 0;
  radiologyResultSettingsSelectAll:boolean = false;
  radiologyResultSettingsSelection: RadiologyResultSettingsSummary[] = [];

 radiologyResultSettingssDisplayedColumns: string[] = ["select","template_name" ];
  radiologyResultSettingsSearchTextBox: FormControl = new FormControl();
  radiologyResultSettingsIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) radiologyResultSettingssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) radiologyResultSettingssSort: MatSort;

constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public radiologyResultSettingsPersist: RadiologyResultSettingsPersist,
                public radiologyResultSettingsNavigator: RadiologyResultSettingsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public physicalExaminationPersist: PhysicalExaminationOptionsPersist,
 public dialogRef: MatDialogRef<RadiologyResultSettingsPickComponent>,@Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, order_id: string, status: number, resultList: string}

    ) {

        this.tcAuthorization.requireRead("radiology_result_settings");
        this.radiologyResultSettingsPersist.lab_test_id = data.order_id,
        this.radiologyResultSettingsPersist.status = data.status,
       this.radiologyResultSettingsSearchTextBox.setValue(radiologyResultSettingsPersist.radiologyResultSettingsSearchText);
      //delay subsequent keyup events
      this.radiologyResultSettingsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.radiologyResultSettingsPersist.radiologyResultSettingsSearchText = value;
        this.searchRadiology_result_settingss();
      });
    } ngOnInit() {

      this.radiologyResultSettingssSort.sortChange.subscribe(() => {
        this.radiologyResultSettingssPaginator.pageIndex = 0;
        this.searchRadiology_result_settingss(true);
      });

      this.radiologyResultSettingssPaginator.page.subscribe(() => {
        this.searchRadiology_result_settingss(true);
      });
      //start by loading items
      this.searchRadiology_result_settingss();
    }

  searchRadiology_result_settingss(isPagination:boolean = false): void {


    let paginator = this.radiologyResultSettingssPaginator;
    let sorter = this.radiologyResultSettingssSort;

    this.radiologyResultSettingsIsLoading = true;
    this.radiologyResultSettingsSelection = [];

    this.radiologyResultSettingsPersist.searchRadiologyResultSettings(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RadiologyResultSettingsSummaryPartialList) => {
      this.radiologyResultSettingssData = partialList.data;
      if (partialList.total != -1) {
        this.radiologyResultSettingssTotalCount = partialList.total;
      }
      this.radiologyResultSettingsIsLoading = false;
    }, error => {
      this.radiologyResultSettingsIsLoading = false;
    });

  }
  markOneItem(item: RadiologyResultSettingsSummary) {
    if(!this.tcUtilsArray.containsId(this.radiologyResultSettingsSelection,item.id)){
          this.radiologyResultSettingsSelection = [];
          this.radiologyResultSettingsSelection.push(item);
        }
        else{
          this.radiologyResultSettingsSelection = [];
        }
  }

  ngOnDestroy(){
    this.radiologyResultSettingsPersist.status = null;
    this.radiologyResultSettingsPersist.lab_test_id = null;
  }

  returnSelected(): void {
    this.dialogRef.close(this.radiologyResultSettingsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  }
