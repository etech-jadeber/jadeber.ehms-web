import {TCId} from "../tc/models";

export class RadiologyResultSettingsSummary extends TCId {
    template_name: string;
    finding: string;
    recommendation: string;
    technique: string;
    conclusion: string;
    lab_test_id: string;

    }
    export class RadiologyResultSettingsSummaryPartialList {
      data: RadiologyResultSettingsSummary[];
      total: number;
    }
    export class RadiologyResultSettingsDetail extends RadiologyResultSettingsSummary {
    }

    export class RadiologyResultSettingsDashboard {
      total: number;
    }
