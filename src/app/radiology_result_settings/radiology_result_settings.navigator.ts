import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {RadiologyResultSettingsEditComponent} from "./radiology-result-settings-edit/radiology-result-settings-edit.component";
import {RadiologyResultSettingsPickComponent} from "./radiology-result-settings-pick/radiology-result-settings-pick.component";


@Injectable({
  providedIn: 'root'
})

export class RadiologyResultSettingsNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  radiologyResultSettingssUrl(): string {
    return "/radiology_result_settingss";
  }

  radiologyResultSettingsUrl(id: string): string {
    return "/radiology_result_settingss/" + id;
  }

  viewRadiologyResultSettingss(): void {
    this.router.navigateByUrl(this.radiologyResultSettingssUrl());
  }

  viewRadiologyResultSettings(id: string): void {
    this.router.navigateByUrl(this.radiologyResultSettingsUrl(id));
  }

  editRadiologyResultSettings(id: string): MatDialogRef<RadiologyResultSettingsEditComponent> {
    const dialogRef = this.dialog.open(RadiologyResultSettingsEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }

  addRadiologyResultSettings(lab_test_id: string): MatDialogRef<RadiologyResultSettingsEditComponent> {
    const dialogRef = this.dialog.open(RadiologyResultSettingsEditComponent, {
          data: new TCIdMode(lab_test_id, TCModalModes.NEW),
          width: TCModalWidths.xtra_large
    });
    return dialogRef;
  }

   pickRadiologyResultSettingss(selectOne: boolean, order_id: string): MatDialogRef<RadiologyResultSettingsPickComponent> {
      const dialogRef = this.dialog.open(RadiologyResultSettingsPickComponent, {
        data: {selectOne, order_id},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
