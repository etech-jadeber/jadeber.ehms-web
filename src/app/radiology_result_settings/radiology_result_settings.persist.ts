import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {RadiologyResultSettingsDashboard, RadiologyResultSettingsDetail, RadiologyResultSettingsSummaryPartialList} from "./radiology_result_settings.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class RadiologyResultSettingsPersist {
 radiologyResultSettingsSearchText: string = "";
 lab_test_id: string;

onReady(editor) {
  editor.ui
    .getEditableElement()
    .parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
}
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.radiologyResultSettingsSearchText;
    //add custom filters
    return fltrs;
  }
  status: number;

  searchRadiologyResultSettings(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RadiologyResultSettingsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("radiology_result_settings", this.radiologyResultSettingsSearchText, pageSize, pageIndex, sort, order);
    if (this.lab_test_id){
      url = TCUtilsString.appendUrlParameter(url, "lab_test_id", this.lab_test_id)
  }
    if (this.status){
        url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }

    return this.http.get<RadiologyResultSettingsSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result_settings/do", new TCDoParam("download_all", this.filters()));
  }

  radiologyResultSettingsDashboard(): Observable<RadiologyResultSettingsDashboard> {
    return this.http.get<RadiologyResultSettingsDashboard>(environment.tcApiBaseUri + "radiology_result_settings/dashboard");
  }

  getRadiologyResultSettings(id: string): Observable<RadiologyResultSettingsDetail> {
    return this.http.get<RadiologyResultSettingsDetail>(environment.tcApiBaseUri + "radiology_result_settings/" + id);
  } addRadiologyResultSettings(item: RadiologyResultSettingsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result_settings/", item);
  }

  updateRadiologyResultSettings(item: RadiologyResultSettingsDetail): Observable<RadiologyResultSettingsDetail> {
    return this.http.patch<RadiologyResultSettingsDetail>(environment.tcApiBaseUri + "radiology_result_settings/" + item.id, item);
  }

  deleteRadiologyResultSettings(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "radiology_result_settings/" + id);
  }
 radiologyResultSettingssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "radiology_result_settings/do", new TCDoParam(method, payload));
  }

  radiologyResultSettingsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "radiology_result_settingss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result_settings/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "radiology_result_settings/" + id + "/do", new TCDoParam("print", {}));
  }


}
