import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { ReceptionDetail } from "../reception.model";
import { ReceptionPersist } from "../reception.persist";
import { ReceptionNavigator } from "../reception.navigator";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TCId } from 'src/app/tc/models';
import { tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';

export enum ReceptionTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-reception-detail',
  templateUrl: './reception-detail.component.html',
  styleUrls: ['./reception-detail.component.css']
})
export class ReceptionDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  receptionLoading: boolean = false;

  receptionTabs: typeof tabs = tabs;
  activeTab: number;
  //basics
  receptionDetail: ReceptionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsString: TCUtilsString,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public receptionNavigator: ReceptionNavigator,
    public receptionPersist: ReceptionPersist,
    public menuState: MenuState) {
    this.tcAuthorization.requireRead("receptions");
    this.receptionDetail = new ReceptionDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("receptions");
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.receptionLoading = true;
    this.receptionPersist.getReception(id).subscribe(receptionDetail => {
      this.receptionDetail = receptionDetail;      
      this.menuState.getDataResponse(this.receptionDetail);
      this.receptionLoading = false;
    }, error => {
      console.error(error);
      this.receptionLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.receptionDetail.id);
  }

  editReception(): void {
    let modalRef = this.receptionNavigator.editReception(this.receptionDetail.id);
    modalRef.afterClosed().subscribe(modifiedReceptionDetail => {
      TCUtilsAngular.assign(this.receptionDetail, modifiedReceptionDetail);
    }, error => {
      console.error(error);
    });
  }

  printReception(): void {
    this.receptionPersist.print(this.receptionDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print reception", true);
    });
  }

  back(): void {
    this.location.back();
  }


  createUser() {
    this.receptionPersist
      .receptionDo(this.receptionDetail.id, "account_create", {})
      .subscribe((response) => {
        this.receptionDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }

  removeUser() {
    this.receptionPersist
      .receptionDo(this.receptionDetail.id, "remove_account", {})
      .subscribe((response) => {
        this.tcNotification.success("account removed");
        this.reload();
      });
  }
  activateUser() {
    this.receptionPersist
      .receptionDo(this.receptionDetail.id, "activate_user", {})
      .subscribe((response) => {
        this.tcNotification.success("account Activated");
        this.reload();
      });
  }
}
