import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {ReceptionDetail} from "../reception.model";
import {ReceptionPersist} from "../reception.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';


@Component({
  selector: 'app-reception-edit',
  templateUrl: './reception-edit.component.html',
  styleUrls: ['./reception-edit.component.css']
})
export class ReceptionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  receptionDetail: ReceptionDetail;
  birth_date: Date;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ReceptionEditComponent>,
              public  persist: ReceptionPersist,
              private tcUtilsDate:TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("receptions");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("employee", "receptionist");
      this.receptionDetail = new ReceptionDetail();
      this.receptionDetail.email = "";
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("receptions");
      this.title = this.appTranslation.getText("general","edit") + " " + this.appTranslation.getText("employee", "receptionist");
      this.isLoadingResults = true;
      this.persist.getReception(this.idMode.id).subscribe(receptionDetail => {
        this.receptionDetail = receptionDetail;
        this.transformDates(true);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.addReception(this.receptionDetail).subscribe(value => {
      this.tcNotification.success("Reception added");
      this.receptionDetail.id = value.id;
      this.dialogRef.close(this.receptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.updateReception(this.receptionDetail).subscribe(value => {
      this.tcNotification.success("Reception updated");
      this.dialogRef.close(this.receptionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }



  transformDates(todate: boolean) {
    if (todate) {
      this.birth_date = this.tcUtilsDate.toDate(this.receptionDetail.birth_date);
    } else {
      this.receptionDetail.birth_date = this.tcUtilsDate.toTimeStamp(
        this.birth_date
      );
    }
  }


  canSubmit():boolean{
        if (this.receptionDetail == null){
            return false;
          }

        if (this.receptionDetail.first_name == null || this.receptionDetail.first_name  == "") {
                      return false;
                    }

if (this.receptionDetail.middle_name == null || this.receptionDetail.middle_name  == "") {
                      return false;
                    }

if (this.receptionDetail.last_name == null || this.receptionDetail.last_name  == "") {
                      return false;
                    }

// if (this.receptionDetail.email == null || this.receptionDetail.email  == "") {
//                       return false;
//                     }

if (this.receptionDetail.telephone == null || this.receptionDetail.telephone  == "") {
                      return false;
                    }

if (this.receptionDetail.address == null || this.receptionDetail.address  == "") {
                      return false;
                    }


        return true;
      }


}
