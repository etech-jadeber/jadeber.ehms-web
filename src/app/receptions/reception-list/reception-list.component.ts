import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {ReceptionPersist} from "../reception.persist";
import {ReceptionNavigator} from "../reception.navigator";
import {ReceptionDetail, ReceptionSummary, ReceptionSummaryPartialList, storeEmployeeFilterColumn} from "../reception.model";


@Component({
  selector: 'app-reception-list',
  templateUrl: './reception-list.component.html',
  styleUrls: ['./reception-list.component.css']
})
export class ReceptionListComponent implements OnInit {

  receptionsData: ReceptionSummary[] = [];
  receptionsTotalCount: number = 0;
  receptionsSelectAll:boolean = false;
  receptionsSelection: ReceptionSummary[] = [];

  receptionsDisplayedColumns: string[] = ["select","action", "first_name","middle_name","last_name","email","telephone","is_male","birth_date","address", ];
  receptionsSearchTextBox: FormControl = new FormControl();
  receptionsIsLoading: boolean = false;
  receptionFilterColumn = storeEmployeeFilterColumn 

  @ViewChild(MatPaginator, {static: true}) receptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) receptionsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public receptionPersist: ReceptionPersist,
                public receptionNavigator: ReceptionNavigator,
                public jobPersist: JobPersist,
    ) {

       this.receptionsSearchTextBox.setValue(receptionPersist.receptionSearchHistory.search_text);
      //delay subsequent keyup events
      this.receptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.receptionPersist.receptionSearchHistory.search_text = value;
        this.searchReceptions();
      });
    }

    ngOnInit() {

      this.receptionsSort.sortChange.subscribe(() => {
        this.receptionsPaginator.pageIndex = 0;
        this.searchReceptions(true);
      });

      this.receptionsPaginator.page.subscribe(() => {
        this.searchReceptions(true);
      });
      //start by loading items
      this.searchReceptions();
    }

  searchReceptions(isPagination:boolean = false): void {


    let paginator = this.receptionsPaginator;
    let sorter = this.receptionsSort;

    this.receptionsIsLoading = true;
    this.receptionsSelection = [];

    this.receptionPersist.searchReception(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ReceptionSummaryPartialList) => {
      this.receptionsData = partialList.data;
      if (partialList.total != -1) {
        this.receptionsTotalCount = partialList.total;
      }
      this.receptionsIsLoading = false;
    }, error => {
      this.receptionsIsLoading = false;
    });

  }

  downloadReceptions(): void {
    if(this.receptionsSelectAll){
         this.receptionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download receptions", true);
      });
    }
    else{
        this.receptionPersist.download(this.tcUtilsArray.idsList(this.receptionsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download receptions",true);
            });
        }
  }



  addReception(): void {
    let dialogRef = this.receptionNavigator.addReception();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchReceptions();
      }
    });
  }

  editReception(item: ReceptionSummary) {
    let dialogRef = this.receptionNavigator.editReception(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteReception(item: ReceptionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Reception");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.receptionPersist.deleteReception(item.id).subscribe(response => {
          this.tcNotification.success("Reception deleted");
          this.searchReceptions();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
