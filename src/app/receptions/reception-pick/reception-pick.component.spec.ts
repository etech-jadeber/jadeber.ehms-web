import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReceptionPickComponent } from './reception-pick.component';

describe('ReceptionPickComponent', () => {
  let component: ReceptionPickComponent;
  let fixture: ComponentFixture<ReceptionPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
