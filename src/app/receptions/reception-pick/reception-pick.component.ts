import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {ReceptionDetail, ReceptionSummary, ReceptionSummaryPartialList} from "../reception.model";
import {ReceptionPersist} from "../reception.persist";


@Component({
  selector: 'app-reception-pick',
  templateUrl: './reception-pick.component.html',
  styleUrls: ['./reception-pick.component.css']
})
export class ReceptionPickComponent implements OnInit {

  receptionsData: ReceptionSummary[] = [];
  receptionsTotalCount: number = 0;
  receptionsSelection: ReceptionSummary[] = [];
  receptionsDisplayedColumns: string[] = ["select", 'first_name','middle_name','last_name','email','telephone','is_male','birth_date','address','login_id' ];

  receptionsSearchTextBox: FormControl = new FormControl();
  receptionsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) receptionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) receptionsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public receptionPersist: ReceptionPersist,
              public dialogRef: MatDialogRef<ReceptionPickComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, disabled:boolean}
  ) {
    this.tcAuthorization.requireRead("receptions");
    this.receptionsSearchTextBox.setValue(receptionPersist.receptionSearchHistory.search_text);
    //delay subsequent keyup events
    this.receptionPersist.receptionSearchHistory.disabled = data.disabled;
    this.receptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.receptionPersist.receptionSearchHistory.search_text = value;
      this.searchReceptions();
    });
  }

  ngOnInit() {

    this.receptionsSort.sortChange.subscribe(() => {
      this.receptionsPaginator.pageIndex = 0;
      this.searchReceptions();
    });

    this.receptionsPaginator.page.subscribe(() => {
      this.searchReceptions();
    });

    //set initial picker list to 5
    this.receptionsPaginator.pageSize = 5;

    //start by loading items
    this.searchReceptions();
  }

  searchReceptions(): void {
    this.receptionsIsLoading = true;
    this.receptionsSelection = [];

    this.receptionPersist.searchReception(this.receptionsPaginator.pageSize,
        this.receptionsPaginator.pageIndex,
        this.receptionsSort.active,
        this.receptionsSort.direction).subscribe((partialList: ReceptionSummaryPartialList) => {
      this.receptionsData = partialList.data;
      if (partialList.total != -1) {
        this.receptionsTotalCount = partialList.total;
      }
      this.receptionsIsLoading = false;
    }, error => {
      this.receptionsIsLoading = false;
    });

  }

  markOneItem(item: ReceptionSummary) {
    if(!this.tcUtilsArray.containsId(this.receptionsSelection,item.id)){
          this.receptionsSelection = [];
          this.receptionsSelection.push(item);
        }
        else{
          this.receptionsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.receptionsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
