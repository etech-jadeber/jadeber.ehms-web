import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReceptionSelfComponent } from './reception-self.component';

describe('ReceptionSelfComponent', () => {
  let component: ReceptionSelfComponent;
  let fixture: ComponentFixture<ReceptionSelfComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceptionSelfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceptionSelfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
