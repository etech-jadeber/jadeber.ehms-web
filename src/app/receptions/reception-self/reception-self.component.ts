import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { payment_type } from 'src/app/app.enums';
import { AppTranslation } from 'src/app/app.translation';
import { AppointmentSummary, AppointmentSummaryPartialList } from 'src/app/appointments/appointment.model';
import { AppointmentNavigator } from 'src/app/appointments/appointment.navigator';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { PatientSummary, PatientSummaryPartialList } from 'src/app/patients/patients.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { PaymentSummary, PaymentSummaryPartialList } from 'src/app/payments/payment.model';
import { PaymentNavigator } from 'src/app/payments/payment.navigator';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { TCAppInit } from 'src/app/tc/app-init';
import { UserContexts } from 'src/app/tc/app.enums';
import { TCAuthentication } from 'src/app/tc/authentication';
import { TCAuthorization } from 'src/app/tc/authorization';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCModalModes, TCWizardProgress, TCWizardProgressModes } from 'src/app/tc/models';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { environment } from 'src/environments/environment';
import { ReceptionSummary } from '../reception.model';
import { ReceptionPersist } from '../reception.persist';


export enum ReceptionTabs {
  overview,
  finance,
  schedule,
  profile
}



@Component({
  selector: 'app-reception-self',
  templateUrl: './reception-self.component.html',
  styleUrls: ['./reception-self.component.css']
})
export class ReceptionSelfComponent implements OnInit {

  receptionTabs: typeof ReceptionTabs = ReceptionTabs;
  activeTab: ReceptionTabs = ReceptionTabs.overview;


  doctorSummary:DoctorSummary [] = [];

  title = environment.appName;
  receptionLoading: boolean = true;
  message: string = "";
  receptionSummary: ReceptionSummary;
  TCAppInit = TCAppInit;

  paymentsData: PaymentSummary[] = [];
  paymentsTotalCount: number = 0;
  patientSummary:PatientSummary [] = []
  paymentsSelectAll:boolean = false;
  paymentsSelection: PaymentSummary[] = [];
  lastDaysSearchBox: FormControl = new FormControl();

  paymentsDisplayedColumns: string[] = ["select","action", "patient_id","payment_type","other_type_included","price_before_other","other","total_price","payment_date", ];
  paymentsSearchTextBox: FormControl = new FormControl();
  paymentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) paymentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentsSort: MatSort;


  fullName(){
    return this.receptionSummary.first_name + " " + this.receptionSummary.middle_name + " " + this.receptionSummary.last_name;
  }



  ngAfterViewInit(): void {
    let employeeId: string = this.tcAuthorization.getUserContexts(UserContexts.front_desk)[0];
  
    this.loadDetails();
  };


  loadDetails() {
    this.receptionPersisit.getReceptionelf().subscribe(result => {
      
        this.receptionSummary = result;
        this.receptionLoading = false;
      }
      , error => {
        console.error(error);
        this.receptionLoading = false;
      });
  }

  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }


  appointmentsData: AppointmentSummary[] = [];
  appointmentsTotalCount: number = 0;
  appointmentsSelectAll: boolean = false;
  appointmentsSelection: AppointmentSummary[] = [];
  patients:PatientSummary[] = [];

  appointmentsDisplayedColumns: string[] = ["select", "action", "schedule_type", "scheduled_user_id", "scheduler_user_id", "schedule_start_time", "schedule_end_time",];
  appointmentsSearchTextBox: FormControl = new FormControl();
  appointmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) appointmentsSort: MatSort;


  dateSearchBox: FormControl = new FormControl();

  constructor(public router: Router,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public appointmentPersist: AppointmentPersist,
    public appointmentNavigator: AppointmentNavigator,
    public patientPersist:PatientPersist,
    public jobPersist: JobPersist,
    public tcAuthorization:TCAuthorization, 
    public receptionPersisit:ReceptionPersist, 
    public userPersist:UserPersist, 
    public tcUtilsDate:TCUtilsDate,
    public tcNotification:TCNotification, 
    public tcAuthentication:TCAuthentication,
     public tcNavigator:TCNavigator, 
    public appTranslation:AppTranslation,
    public paymentPersist: PaymentPersist,
    public paymentNavigator: PaymentNavigator,
  ) {

    this.appointmentsSearchTextBox.setValue(appointmentPersist.appointmentSearchHistory.search_text);
    //delay subsequent keyup events
    this.appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.appointmentPersist.appointmentSearchHistory.search_text = value;
      this.searchAppointments();
    });

    this.paymentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.paymentPersist.paymentSearchHistory.search_text = value;
      this.searchPayments();
    });
    this.lastDaysSearchBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      // this.paymentPersist.last_days = value;
      this.searchPayments();
    });

    this.dateSearchBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {  
      // this.paymentPersist.last_days = value;
      this.searchAppointments();
    });
  }

  ngOnInit() {
    this.tcAuthorization.requireLogin();
    this.receptionPersisit.initReceptionSelf();
    this.loadPatients();

    this.appointmentsSort.sortChange.subscribe(() => {
      this.appointmentsPaginator.pageIndex = 0;
      this.searchAppointments(true);
    });

    this.appointmentsPaginator.page.subscribe(() => {
      this.searchAppointments(true);
    });
    //start by loading items
    this.searchAppointments();
    this.paymentsSort.sortChange.subscribe(() => {
      this.paymentsPaginator.pageIndex = 0;
      this.searchPayments(true);
    });

 

    this.paymentsPaginator.page.subscribe(() => {
      this.searchPayments(true);
    });
    //start by loading items
    this.loadPatients();
    this.searchPayments();
  }

  searchAppointments(isPagination: boolean = false): void {


    let paginator = this.appointmentsPaginator;
    let sorter = this.appointmentsSort;

    this.appointmentsIsLoading = true;
    this.appointmentsSelection = [];

    this.appointmentPersist.searchAppointment(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: AppointmentSummaryPartialList) => {
      this.appointmentsData = partialList.data;
      if (partialList.total != -1) {
        
        this.appointmentsTotalCount = partialList.total;
      }
      this.appointmentsIsLoading = false;
    }, error => {
      this.appointmentsIsLoading = false;
    });

  }

  downloadAppointments(): void {
    if (this.appointmentsSelectAll) {
      this.appointmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download appointments", true);
      });
    }
    else {
      this.appointmentPersist.download(this.tcUtilsArray.idsList(this.appointmentsSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download appointments", true);
      });
    }
  }



  addAppointment(patientId:string = null): void {
    let dialogRef = this.appointmentNavigator.addAppointment(patientId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchAppointments();
      }
    });
  }

  editAppointment(item: AppointmentSummary) {
    let dialogRef = this.appointmentNavigator.editAppointment(null,item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }



  getDoctorFullName(doctorId:string):string{
    let doctors:DoctorSummary= this.tcUtilsArray.getById(
      this.doctorSummary,
      doctorId
    );
    if (doctors) {
      return `${doctors.first_name} ${doctors.middle_name} ${doctors.last_name}`;
    }

}


  deleteAppointment(item: AppointmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Appointment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.appointmentPersist.deleteAppointment(item.id).subscribe(response => {
          this.tcNotification.success("Appointment deleted");
          this.searchAppointments();
        }, error => {
        });
      }

    });
  }


  loadPatients() {
    this.patientPersist
      .patientsDo("get_all_patients","")
      .subscribe((result) => {
        this.patients =  (result as PatientSummaryPartialList).data;
      });
  }

  getPatientFullName(patientId): string {
    let patient: PatientSummary = this.tcUtilsArray.getById(
      this.patients,
      patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
  }


  searchPayments(isPagination:boolean = false): void {


    let paginator = this.paymentsPaginator;
    let sorter = this.paymentsSort;

    this.paymentsIsLoading = true;
    this.paymentsSelection = [];

    this.paymentPersist.searchPayment(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PaymentSummaryPartialList) => {
      this.paymentsData = partialList.data;
      if (partialList.total != -1) {
        this.paymentsTotalCount = partialList.total;
      }
      this.paymentsIsLoading = false;
    }, error => {
      this.paymentsIsLoading = false;
    });

  }

  downloadPayments(): void {
    if(this.paymentsSelectAll){
         this.paymentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download payments", true);
      });
    }
    else{
        this.paymentPersist.download(this.tcUtilsArray.idsList(this.paymentsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download payments",true);
            });
        }
  }





  addPayment(): void {
    let dialogRef = this.paymentNavigator.addPayment(null,true);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let wizardProgress: TCWizardProgress = <TCWizardProgress>result;
        if ((wizardProgress.progressMode == TCWizardProgressModes.NEXT) || (wizardProgress.progressMode == TCWizardProgressModes.SKIP) 
        && wizardProgress.item.payment_type == payment_type.card_fee ) {
         this.addAppointment(wizardProgress.item.patient_id);
        }
        else if(wizardProgress.item.payment_type != payment_type.card_fee){

        }
        else if (wizardProgress.progressMode == TCWizardProgressModes.FINISH) {

        }
      }



    });
  }

  editPayment(item: PaymentSummary) {
    let dialogRef = this.paymentNavigator.editPayment(null,item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deletePayment(item: PaymentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Payment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.paymentPersist.deletePayment(item.id).subscribe(response => {
          this.tcNotification.success("Payment deleted");
          this.searchPayments();
        }, error => {
        });
      }

    });
  }

  logout(){
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }
  

}
