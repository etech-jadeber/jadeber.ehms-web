import {TCId, TCString} from "../tc/models";

export const storeEmployeeFilterColumn: TCString[] = [
  new TCString( "first_name", 'First Name'),
  new TCString( "middle_name", 'Middle Name'),
  new TCString( "last_name", 'Last Name'),
  new TCString( "telephone", 'Phone Number'),
  ];

export class ReceptionSummary extends TCId {
  first_name : string;
middle_name : string;
last_name : string;
email : string;
telephone : string;
is_male : boolean = false;
birth_date : number;
address : string;
login_id : string;
disabled:boolean;
}

export class ReceptionSummaryPartialList {
  data: ReceptionSummary[];
  total: number;
}

export class ReceptionDetail extends ReceptionSummary {
  first_name : string;
middle_name : string;
last_name : string;
email : string;
telephone : string;

birth_date : number;
address : string;
login_id : string;
}

export class ReceptionDashboard {
  total: number;
}
