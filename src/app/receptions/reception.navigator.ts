import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ReceptionEditComponent} from "./reception-edit/reception-edit.component";
import {ReceptionPickComponent} from "./reception-pick/reception-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ReceptionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  receptionsUrl(): string {
    return "/receptions";
  }

  receptionUrl(id: string): string {
    return "/receptions/" + id;
  }

  viewReceptions(): void {
    this.router.navigateByUrl(this.receptionsUrl());
  }

  viewReception(id: string): void {
    this.router.navigateByUrl(this.receptionUrl(id));
  }

  editReception(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ReceptionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addReception(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ReceptionEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickReceptions(selectOne: boolean=false, disabled:boolean = false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(ReceptionPickComponent, {
        data: {selectOne,disabled},
        width: TCModalWidths.large
      });
      return dialogRef;
    }

    goHome(): void {
      this.router.navigateByUrl("/reception-home");
    }
}
