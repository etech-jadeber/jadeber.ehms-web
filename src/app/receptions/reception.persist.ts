import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {ReceptionDashboard, ReceptionDetail, ReceptionSummaryPartialList} from "./reception.model";
import { TCAuthorization } from '../tc/authorization';
import { UserContexts } from '../tc/app.enums';
import { DoctorSummary } from '../doctors/doctor.model';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class ReceptionPersist {

  // receptionSearchText: string = "";
  
  // receptionId: string = null;
  // disabled: boolean;

  constructor(private http: HttpClient, private tcAuthorization:TCAuthorization) {
  }

  receptionSearchHistory = {
    "disabled": false,
    "search_text": "",
    "search_column": "",
  }
  
  defaultReceptionSearchHistory = {...this.receptionSearchHistory}
  
  resetReceptionSearchHistory(){
  this.receptionSearchHistory = {...this.defaultReceptionSearchHistory}
  }

  searchReception(pageSize: number, pageIndex: number, sort: string, order: string, searchHistory = this.receptionSearchHistory): Observable<ReceptionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("receptions", searchHistory.search_text, pageSize, pageIndex, sort, order);
    url = TCUtilsString.appendUrlParameters(url, searchHistory)
    // if (this.disabled != null) {
    //   url = TCUtilsString.appendUrlParameter(url, "disabled", this.disabled.toString());
    // }
   
    return this.http.get<ReceptionSummaryPartialList>(url);

  }

  filters(searchHistory = this.receptionSearchHistory): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = searchHistory.search_text;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "receptions/do", new TCDoParam("download_all", this.filters()));
  }

  receptionDashboard(): Observable<ReceptionDashboard> {
    return this.http.get<ReceptionDashboard>(environment.tcApiBaseUri + "receptions/dashboard");
  }

  getReception(id: string): Observable<ReceptionDetail> {
    return this.http.get<ReceptionDetail>(environment.tcApiBaseUri + "receptions/" + id);
  }

  addReception(item: ReceptionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "receptions/", item);
  }

  updateReception(item: ReceptionDetail): Observable<ReceptionDetail> {
    return this.http.patch<ReceptionDetail>(environment.tcApiBaseUri + "receptions/" + item.id, item);
  }

  deleteReception(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "receptions/" + id);
  }

  receptionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "receptions/do", new TCDoParam(method, payload));
  }

  receptionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "receptions/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "receptions/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "receptions/" + id + "/do", new TCDoParam("print", {}));
  }



  initReceptionSelf(): void {
    // let receptionIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.front_desk);
    // if (receptionIds.length == 1) {
    //   this.receptionId = receptionIds[0];
    // }
  }

  getReceptionelf(): Observable<DoctorSummary>{
    return this.http.get<DoctorSummary>(environment.tcApiBaseUri + "reception-self");

  }

}
