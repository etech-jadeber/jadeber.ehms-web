import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RehabilitationOrderDetailComponent } from './rehabilitation-order-detail.component';

describe('RehabilitationOrderDetailComponent', () => {
  let component: RehabilitationOrderDetailComponent;
  let fixture: ComponentFixture<RehabilitationOrderDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RehabilitationOrderDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RehabilitationOrderDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
