import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../tc/authorization";
import {TCUtilsAngular} from "../tc/utils-angular";
import {TCNotification} from "../tc/notification";
import {TCUtilsArray} from "../tc/utils-array";
import {TCNavigator} from "../tc/navigator";
import {JobPersist} from "../tc/jobs/job.persist";
import {AppTranslation} from "../app.translation";


import {  Rehabilitation_OrderDetail, Rehabilitation_OrderSummary } from '../form_encounters/form_encounter.model';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';


export enum RehabilitationOrderTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-rehabilitation_order-detail',
  templateUrl: './rehabilitation-order-detail.component.html',
  styleUrls: ['./rehabilitation-order-detail.component.scss']
})
export class RehabilitationOrderDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  rehabilitationOrderIsLoading:boolean = false;
  
  RehabilitationOrderTabs: typeof RehabilitationOrderTabs = RehabilitationOrderTabs;
  activeTab: RehabilitationOrderTabs = RehabilitationOrderTabs.overview;
  //basics
  rehabilitationOrderDetail: Rehabilitation_OrderDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
              public form_encounterPersist: Form_EncounterPersist,
              private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation) {
    this.tcAuthorization.requireRead("rehabilitation_orders");
    this.rehabilitationOrderDetail = new Rehabilitation_OrderDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("rehabilitation_orders");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.rehabilitationOrderIsLoading = true;
    this.form_encounterPersist.getRehabilitation_Order(id).subscribe(rehabilitationOrderDetail => {
          this.rehabilitationOrderDetail = rehabilitationOrderDetail;
          this.rehabilitationOrderIsLoading = false;
        }, error => {
          console.error(error);
          this.rehabilitationOrderIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.rehabilitationOrderDetail.id);
  }

   printBirth():void{
      this.form_encounterPersist.print(this.rehabilitationOrderDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print rehabilitation_order", true);
      });
    }

  back():void{
      this.location.back();
    }

}
