import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RehabilitationOrderListComponent } from './rehabilitation-order-list.component';

describe('RehabilitationOrderListComponent', () => {
  let component: RehabilitationOrderListComponent;
  let fixture: ComponentFixture<RehabilitationOrderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RehabilitationOrderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RehabilitationOrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
