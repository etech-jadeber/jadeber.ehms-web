import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime, interval } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { AsyncJob, JobData, JobState } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { Follow_Up_NoteDetail, Follow_Up_NoteSummary, Rehabilitation_OrderDetail, Rehabilitation_OrderSummary } from '../form_encounters/form_encounter.model';
import { CommentDetail, CommentSummary } from '../tc/models';
import { TCAuthentication } from '../tc/authentication';
import { UserPersist } from '../tc/users/user.persist';
import { Physiotherapy_TypeDetail, Physiotherapy_TypeSummary } from '../physiotherapy_types/physiotherapy_type.model';
import { Physiotherapy_TypePersist } from '../physiotherapy_types/physiotherapy_type.persist';
import { DoctorDetail, DoctorSummary } from '../doctors/doctor.model';
import { DoctorPersist } from '../doctors/doctor.persist';
import { TCUtilsString } from '../tc/utils-string';
import { PhysiotherapySessionPersist } from '../physiotherapy_session/physiotherapy_session.persist';
import { PhysiotherapySessionDetail } from '../physiotherapy_session/physiotherapy_session.model';
import { PatientPersist } from '../patients/patients.persist';
import { PhysiotherapyStatus } from '../app.enums';
import { UserDetail } from '../tc/users/user.model';
import { PatientDetail } from '../patients/patients.model';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

class Patient_name{
  id:string;
  name:string;
}
@Component({
  selector: 'app-rehabilitation-order-list',
  templateUrl: './rehabilitation-order-list.component.html',
  styleUrls: ['./rehabilitation-order-list.component.css']
})
export class RehabilitationOrderListComponent implements OnInit {
  //rehabilitation_orders
  
  physiotherapyStatus = PhysiotherapyStatus;
  rehabilitation_ordersData: Rehabilitation_OrderSummary[] = [];
  doctorsData: DoctorSummary[]=[];
  rehabilitation_ordersTotalCount: number = 0;
  rehabilitation_ordersSelectAll: boolean = false;
  patientNames:Patient_name[]=[];
  rehabilitation_ordersSelected: Rehabilitation_OrderSummary[] = [];
  rehabilitation_ordersDisplayedColumns: string[] = [
    'select',
    'action',
    'pid',
    'patient_id',
    'status',
    'physiotherapy_type_id',
    'note',
    'created_at',
    'treatment_per_week',
    "doctor"
  ];
  size:number;
  rehabilitation_ordersSearchTextBox: FormControl = new FormControl();
  rehabilitation_ordersLoading: boolean = false;
  isLoadingResults: boolean = false;
  physiotherapy_typesData: Physiotherapy_TypeSummary[] = [];

  @Input() encounterId: string;
  @Input() patientId: string;
  @Input() isSession: boolean = false;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  doctors: {[id: string] : DoctorDetail} = {}
  patients: {[id: string] : Patient_name} = {}
  physiotherapyType: {[id: string] : Physiotherapy_TypeDetail} = {}


  
  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public doctorPersist: DoctorPersist,
    public physiotherapy_TypePersist: Physiotherapy_TypePersist,
    public physiotherapySessionPersist: PhysiotherapySessionPersist,
    public patientPersist: PatientPersist,
    public tcUtilsString: TCUtilsString,
  ) {
        this.rehabilitation_ordersSearchTextBox.setValue(
          form_encounterPersist.rehabilationSearchHistory.search_text
        );
        this.rehabilitation_ordersSearchTextBox.valueChanges
          .pipe(debounceTime(500))
          .subscribe((value) => {
            this.form_encounterPersist.rehabilationSearchHistory.search_text =
              value.trim();
            this.searchRehabilitation_Orders();
          });
   }

  ngOnInit(): void {
this.form_encounterPersist.rehabilationSearchHistory.patient_id = this.patientId
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.searchRehabilitation_Orders(true);
    });

    this.paginator.page.subscribe(() => {
      this.searchRehabilitation_Orders(true);
    });
    this.searchRehabilitation_Orders();
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.form_encounterPersist.rehabilationSearchHistory.encounter_id = this.encounterId;
    } else {
    this.form_encounterPersist.rehabilationSearchHistory.encounter_id = null;
    }
    this.searchRehabilitation_Orders()
    }

    //rehabilitation_orders methods
    searchRehabilitation_Orders(isPagination: boolean = false): void {
      this.rehabilitation_ordersSelected = [];
      this.rehabilitation_ordersLoading = true;
      let paginator = this.paginator;
    let sorter = this.sorter;
      this.form_encounterPersist
        .searchRehabilitation_Order(
          paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction
        )
        .subscribe(
          (response) => {
            this.rehabilitation_ordersData = response.data;
            this.size= this.rehabilitation_ordersData.length;
            if (response.total != -1) {
              this.rehabilitation_ordersTotalCount = response.total;
              
               for (let rehabilitation_order of this.rehabilitation_ordersData){
                if(!this.physiotherapyType[rehabilitation_order.physiotherapy_type_id]){
                  this.physiotherapy_TypePersist.getPhysiotherapy_Type(rehabilitation_order.physiotherapy_type_id).subscribe((response)=>{
                    // this.physiotherapy_typesData.push(response)
                    this.physiotherapyType[rehabilitation_order.physiotherapy_type_id] = response
                  });
                }
                if(!this.doctors[rehabilitation_order.doctor_id]){
                  rehabilitation_order.doctor_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(rehabilitation_order.doctor_id).subscribe((response)=>
                  {
                    this.doctors[rehabilitation_order.doctor_id];
                  });
                }
                // if(!this.patients[rehabilitation_order.patient_id]){
                //   this.patientPersist.getPatient(rehabilitation_order.patient_id).subscribe((result)=>{
                //     let patientName= new Patient_name();
                //     patientName.name=result.fname + " " + result.mname +" " +result.lname;
                //     patientName.id=rehabilitation_order.patient_id;
                //     this.patients[rehabilitation_order.patient_id] = patientName;
                //   })
                // }
              }
            }
            this.rehabilitation_ordersLoading = false;
          },
          (error) => {
            this.rehabilitation_ordersLoading = false;
          }
        );
    }
  
    addRehabilitation_Order(): void {
      let dialogRef = this.form_encounterNavigator.addRehabilitation_Order(
        this.encounterId
      );
      dialogRef.afterClosed().subscribe((newRehabilitation_Order) => {
        this.searchRehabilitation_Orders();
      });
    }
  
    editRehabilitation_Order(item: Rehabilitation_OrderDetail): void {
      let dialogRef = this.form_encounterNavigator.editRehabilitation_Order(
        item.id
      );
      dialogRef.afterClosed().subscribe((updatedRehabilitation_Order) => {
        if (updatedRehabilitation_Order) {
          this.searchRehabilitation_Orders();
        }
      });
    }
    createPhysiotherapySession(physiotherapy: Rehabilitation_OrderDetail): void {
      this.isLoadingResults = true;
      let physiotherapySessionDetail= new PhysiotherapySessionDetail();
      physiotherapySessionDetail.encounter_id=physiotherapy.encounter_id;
      physiotherapySessionDetail.no_of_session_per_week=107-physiotherapy.treatment_per_week;
      physiotherapySessionDetail.physiotherapy_id=physiotherapy.physiotherapy_id;
      physiotherapySessionDetail.physiotherapy_type=physiotherapy.physiotherapy_type_id;
    this.physiotherapySessionPersist.addPhysiotherapySession(physiotherapySessionDetail).subscribe(value => {
      this.tcNotification.success("physiotherapySession added");
      physiotherapySessionDetail.id = value.id;
      this.searchRehabilitation_Orders()
    }, error => {
      this.isLoadingResults = false;
    })
    }
    
    completePhysiotherapyOrder(item: Rehabilitation_OrderDetail):void{
      let dialogRef = this.tcNavigator.confirmAction("complete",'Rehabilation_order',"Do You want to complet the physiotherapy you can not create session latter", "Cancel")
      dialogRef.afterClosed().subscribe((result)=>{
        if(result){
          this.form_encounterPersist.rehabilitation_orderDo(item.id,"complete_session",{}).subscribe((res)=>{
            this.tcNotification.success("You have success fully completed the session");
            this.searchRehabilitation_Orders();
          })
        }
      })
    }

    deleteRehabilitation_Order(item: Rehabilitation_OrderDetail): void {
      let dialogRef = this.tcNavigator.confirmDeletion('Rehabilitation_Order');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist
            .deleteRehabilitation_Order(this.encounterId, item.id)
            .subscribe(
              (response) => {
                this.tcNotification.success('rehabilitation_order deleted');
                this.searchRehabilitation_Orders();
              },
              (error) => {}
            );
        }
      });
    }
  
    downloadRehabilitation_Orders(): void {
      this.tcNotification.info(
        'Download rehabilitation_orders : ' +
          this.rehabilitation_ordersSelected.length
      );
    }

    getPhysiotherapyTypes(Id: string) {
      let type: Physiotherapy_TypeSummary = this.tcUtilsArray.getById(
        this.physiotherapy_typesData,
        Id
      );
      if (type) {
        return type.name;
      }
    }
  
    back(): void {
      this.location.back();
    } 

}
