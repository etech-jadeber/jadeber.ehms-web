import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EncounterReportListComponent } from './encounter-report-list.component';

describe('EncounterReportListComponent', () => {
  let component: EncounterReportListComponent;
  let fixture: ComponentFixture<EncounterReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EncounterReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EncounterReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
