import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { Form_EncounterSummary, Form_EncounterSummaryPartialList } from '../../form_encounters/form_encounter.model';
import { Form_EncounterPersist } from '../../form_encounters/form_encounter.persist';
import { Form_EncounterNavigator } from '../../form_encounters/form_encounter.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';

@Component({
  selector: 'app-encounter-report-list',
  templateUrl: './encounter-report-list.component.html',
  styleUrls: ['./encounter-report-list.component.scss']
})
export class EncounterReportListComponent implements OnInit {

  encounterReportData: Form_EncounterSummary[] = [];
  encounterReportTotalCount: number = 0;
  encounterReportselectAll:boolean = false;
  encounterReportselection: Form_EncounterSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 encounterReportDisplayedColumns: string[] = ["select", "id", "patient_name", "encounter_count" ,"last_visit" ];
  encounterReportearchTextBox: FormControl = new FormControl();
  encounterReportIsLoading: boolean = false;  
  encounterType: string;

  @ViewChild(MatPaginator, {static: true}) encounterReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) encounterReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public encounterPersist: Form_EncounterPersist,
                public encounterNavigator: Form_EncounterNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,) {
                this.tcAuthorization.requireRead("encounter_reports");
                this.encounterReportearchTextBox.setValue(encounterPersist.encounterSearchHistory.search_text);
                //delay subsequent keyup events
                this.encounterReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.encounterPersist.encounterSearchHistory.search_text = value;
                this.searchencounterPersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.encounterPersist.encounterSearchHistory.from_date = new Date(value._d).getTime()/1000;
                  this.searchencounterPersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.encounterPersist.encounterSearchHistory.to_date = new Date(value._d).getTime()/1000;
                  this.searchencounterPersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.encounterReportSort.sortChange.subscribe(() => {
      this.encounterReportPaginator.pageIndex = 0;
      this.searchencounterPersistReport(true);
    });

    this.encounterReportPaginator.page.subscribe(() => {
      this.searchencounterPersistReport(true);
    });
    //start by loading items
    this.searchencounterPersistReport();
  }

  ngOnDestroy(): void {
    this.encounterPersist.encounterSearchHistory.from_date = null;
    this.encounterPersist.encounterSearchHistory.to_date = null;
    this.encounterPersist.encounterSearchHistory.search_text = "";
  }

  searchencounterPersistReport(isPagination:boolean = false): void {

    let paginator = this.encounterReportPaginator;
    let sorter = this.encounterReportSort;

    this.encounterReportIsLoading = true;
    this.encounterReportselection = [];

    this.encounterPersist.form_encountersDo("encounterReport",
    {start_date: this.encounterPersist.encounterSearchHistory.from_date,
     end_date: this.encounterPersist.encounterSearchHistory.to_date, 
    }, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Form_EncounterSummaryPartialList) => {
      this.encounterReportData = partialList.data;
      if (partialList.total != -1) {
        this.encounterReportTotalCount = partialList.total;
      }
      this.encounterReportIsLoading = false;
    }, error => {
      this.encounterReportIsLoading = false;
    });
  }

  downloadencounterReport(): void {
    if(this.encounterReportselectAll){
         this.encounterPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download encounter-report", true);
      });
    }
    else{
        this.encounterPersist.download(this.tcUtilsArray.idsList(this.encounterReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download encounter-report",true);
            });
        }
  }

  back():void{
    this.location.back();
  }

}
