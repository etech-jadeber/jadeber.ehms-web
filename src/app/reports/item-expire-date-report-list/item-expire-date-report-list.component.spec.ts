import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemExpireDateReportListComponent } from './item-expire-date-report-list.component';

describe('ItemExpireDateReportListComponent', () => {
  let component: ItemExpireDateReportListComponent;
  let fixture: ComponentFixture<ItemExpireDateReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemExpireDateReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemExpireDateReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
