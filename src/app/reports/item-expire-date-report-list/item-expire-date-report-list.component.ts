import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemExpireDateReport, ItemExpireDateReportPartialList } from 'src/app/items/item.model';
import { StoreSummary } from 'src/app/stores/store.model';
import { StoreNavigator } from 'src/app/stores/store.navigator';

@Component({
  selector: 'item-expire-date-report-list',
  templateUrl: './item-expire-date-report-list.component.html',
  styleUrls: ['./item-expire-date-report-list.component.scss']
})
export class ItemExpireDateReportListComponent implements OnInit {

  itemExpireDateReportData: ItemExpireDateReport[] = [];
  itemExpireDateReportTotalCount: number = 0;
  itemExpireDateReportselectAll:boolean = false;
  itemExpireDateReportselection: ItemExpireDateReport[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 itemExpireDateReportDisplayedColumns: string[] = ["select", "item_name", "batch_nos" , "unit", "quantities", "expire_dates", "expand"];
  itemExpireDateReportearchTextBox: FormControl = new FormControl();
  itemExpireDateReportIsLoading: boolean = false;  
  encounterType: string;

  @ViewChild(MatPaginator, {static: true}) itemExpireDateReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) itemExpireDateReportSort: MatSort;

  expandedElement: ItemExpireDateReport

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemPersist: ItemPersist,
                public itemNavigator: ItemNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public storeNavigator: StoreNavigator,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,) {
                this.tcAuthorization.requireRead("item_expire_reports");
                this.itemExpireDateReportearchTextBox.setValue(itemPersist.itemSearchText);
                //delay subsequent keyup events
                this.itemExpireDateReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.itemPersist.itemSearchText = value;
                this.searchencounterPersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.itemPersist.from_date = new Date(value._d).getTime()/1000;
                  this.searchencounterPersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.itemPersist.to_date = new Date(value._d).getTime()/1000;
                  this.searchencounterPersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.itemExpireDateReportSort.sortChange.subscribe(() => {
      this.itemExpireDateReportPaginator.pageIndex = 0;
      this.searchencounterPersistReport(true);
    });

    this.itemExpireDateReportPaginator.page.subscribe(() => {
      this.searchencounterPersistReport(true);
    });
    //start by loading items
    this.searchencounterPersistReport();
  }

  ngOnDestroy(): void {
    this.itemPersist.from_date = null;
    this.itemPersist.to_date = null;
    this.itemPersist.itemSearchText = "";
  }

  searchencounterPersistReport(isPagination:boolean = false): void {

    let paginator = this.itemExpireDateReportPaginator;
    let sorter = this.itemExpireDateReportSort;

    this.itemExpireDateReportIsLoading = true;
    this.itemExpireDateReportselection = [];

    this.itemPersist.searchItemExpireDateReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ItemExpireDateReportPartialList) => {
      this.itemExpireDateReportData = partialList.data;
      if (partialList.total != -1) {
        this.itemExpireDateReportTotalCount = partialList.total;
      }
      this.itemExpireDateReportIsLoading = false;
      this.itemExpireDateReportData.forEach(
        data => this.decodeDetail(data)
       )
    }, error => {
      this.itemExpireDateReportIsLoading = false;
    });
  }

  downloaditemExpireDateReport(): void {
    if(this.itemExpireDateReportselectAll){
         this.itemPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download encounter-report", true);
      });
    }
    else{
        this.itemPersist.download(this.tcUtilsArray.idsList(this.itemExpireDateReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download encounter-report",true);
            });
        }
  }

  back():void{
    this.location.back();
  }
  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.itemPersist.store_name = result[0].name;
        this.itemPersist.store_id = result[0].id;
        this.searchencounterPersistReport();
      }
    });
  }
 

  decodeDetail(data: ItemExpireDateReport){
    const outerRe = /\{[^\{\}]+\}/g;
    data.convertedDetail = []
  data.details.match(outerRe).forEach(
    detail => {
      const converted = detail.replace(/\\/g, '')
      data.convertedDetail = [...data.convertedDetail, JSON.parse(converted)]
    }
  )
  data.convertedDetail = [...data.convertedDetail.sort((a, b) => a.expire_date - b.expire_date)]
  }

}