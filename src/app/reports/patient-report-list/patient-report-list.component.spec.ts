import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientReportList } from './patient-report-list.component';

describe('PatientReportList', () => {
  let component: PatientReportList;
  let fixture: ComponentFixture<PatientReportList>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientReportList ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientReportList);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
