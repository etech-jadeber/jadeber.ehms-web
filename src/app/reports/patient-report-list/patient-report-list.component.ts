import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientSummary, PatientSummaryPartialList } from '../../patients/patients.model';
import { PatientPersist } from '../../patients/patients.persist';
import { PatientNavigator } from '../../patients/patients.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';

@Component({
  selector: 'app-patient-report-list',
  templateUrl: './patient-report-list.component.html',
  styleUrls: ['./patient-report-list.component.scss']
})
export class PatientReportList implements OnInit {

  patientListReportData: PatientSummary[] = [];
  patientListReportTotalCount: number = 0;
  patientListReportselectAll:boolean = false;
  patientListReportselection: PatientSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 patientListReportDisplayedColumns: string[] = ["select", "pid", "patient_name", "sex" , "age", "phone_cell", "state", "subcity", "wereda", "last_visit" ];
  patientListReportearchTextBox: FormControl = new FormControl();
  patientListReportIsLoading: boolean = false;  
  patientListType: string;

  @ViewChild(MatPaginator, {static: true}) patientListReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) patientListReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public patientListPersist: PatientPersist,
                public patientListNavigator: PatientNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,) {
                this.tcAuthorization.requireRead("patient_list_reports");
                this.patientListReportearchTextBox.setValue(patientListPersist.patientReportSearchHistory.search_text);
                //delay subsequent keyup events
                this.patientListReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.patientListPersist.patientReportSearchHistory.search_text = value;
                this.searchpatientListPersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.patientListPersist.patientReportSearchHistory.from_date = new Date(value._d).getTime()/1000;
                  this.searchpatientListPersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.patientListPersist.patientReportSearchHistory.to_date = new Date(value._d).getTime()/1000;
                  this.searchpatientListPersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.patientListReportSort.sortChange.subscribe(() => {
      this.patientListReportPaginator.pageIndex = 0;
      this.searchpatientListPersistReport(true);
    });

    this.patientListReportPaginator.page.subscribe(() => {
      this.searchpatientListPersistReport(true);
    });
    //start by loading items

    this.searchpatientListPersistReport();
  }

  ngOnDestroy(): void {
    this.patientListPersist.patientReportSearchHistory.from_date = null;
    this.patientListPersist.patientReportSearchHistory.to_date = null;
    this.patientListPersist.patientReportSearchHistory.search_text = "";
  }

  searchpatientListPersistReport(isPagination:boolean = false): void {

    let paginator = this.patientListReportPaginator;
    let sorter = this.patientListReportSort;

    this.patientListReportIsLoading = true;
    this.patientListReportselection = [];

    this.patientListPersist.patientsDo("patientListReport",
    {start_date: this.patientListPersist.patientReportSearchHistory.from_date,
     end_date: this.patientListPersist.patientReportSearchHistory.to_date, 
    }, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientSummaryPartialList) => {
      this.patientListReportData = partialList.data;
      if (partialList.total != -1) {
        this.patientListReportTotalCount = partialList.total;
      }
      this.patientListReportIsLoading = false;
    }, error => {
      this.patientListReportIsLoading = false;
    });
  }

  downloadpatientListReport(): void {
    if(this.patientListReportselectAll){
         this.patientListPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download patientList-report", true);
      });
    }
    else{
        this.patientListPersist.download(this.tcUtilsArray.idsList(this.patientListReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download patientList-report",true);
            });
        }
  }

  back():void{
    this.location.back();
  }

}
