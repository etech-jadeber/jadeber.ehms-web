import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentReportListComponent } from './payment-report-list.component';

describe('PaymentReportListComponent', () => {
  let component: PaymentReportListComponent;
  let fixture: ComponentFixture<PaymentReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaymentReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
