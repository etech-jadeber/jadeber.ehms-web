import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PaymentDetail, PaymentReportList, PaymentSummary, PaymentSummaryPartialList } from '../../payments/payment.model';
import { PaymentPersist } from '../../payments/payment.persist';
import { PaymentNavigator } from '../../payments/payment.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { PatientDetail, PatientSummary } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { fee_type, GraphBy, payment_report_group_by, payment_status, physican_type, physician_type } from 'src/app/app.enums';
import { DoctorDetail, DoctorSummary } from 'src/app/doctors/doctor.model';
import { UserNavigator } from 'src/app/tc/users/user.navigator';
import { UserDetail, UserSummary } from 'src/app/tc/users/user.model';
import { CompanyNavigator } from 'src/app/Companys/Company.navigator';
import { CompanyDetail, CompanySummary } from 'src/app/Companys/Company.model';
import { FeeNavigator } from 'src/app/fees/fee.navigator';
import { FeeDetail, FeeSummary } from 'src/app/fees/fee.model';
import { FeePersist } from 'src/app/fees/fee.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
import { TransferPersist } from 'src/app/transfers/transfer.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemDetail } from 'src/app/items/item.model';
import { TCId } from 'src/app/tc/models';
import { OutsourcingCompanyNavigator } from 'src/app/outsourcing_companys/outsourcing_company.navigator';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { TCUtilsNumber } from 'src/app/tc/utils-number';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { AppointmentPersist } from 'src/app/appointments/appointment.persist';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
import { BedroomtypeDetail, BedroomtypeSummary } from 'src/app/bed/bedroomtypes/bedroomtype.model';


@Component({
  selector: 'app-payment-report-list',
  templateUrl: './payment-report-list.component.html',
  styleUrls: ['./payment-report-list.component.scss']
})
export class PaymentReportListComponent implements OnInit {

  paymentsReportData: PaymentSummary[] = [];
  paymentsReportTotalCount: number = 0;
  paymentsReportselectAll:boolean = false;
  paymentsReportselection: PaymentSummary[] = [];
  showGraph: boolean = false;
  multi: any[] = [];
  view: any[] = [900, 400];
  colorScheme = {
    domain: [],
  };
  // options
  legend: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Month';
  yAxisLabel: string = 'Value';
  timeline: boolean = true;

  targetNameFunction = {
    [payment_report_group_by.date]: this.getDate.bind(this),
    [payment_report_group_by.patient]: this.getPatient.bind(this),
    [payment_report_group_by.doctor]: this.getDoctor.bind(this),
    [payment_report_group_by.cashier]: this.getCashier.bind(this),
    [payment_report_group_by.payment_status]: this.getPaymentStatus.bind(this),
    [payment_report_group_by.fee_type]: this.getFeeType.bind(this),
    [payment_report_group_by.payment_type]: this.getPaymentType.bind(this),
    [payment_report_group_by.fee]: this.getFee.bind(this),
    [payment_report_group_by.company]: this.getCompany.bind(this),
    [payment_report_group_by.ward]: this.getWard.bind(this),
  }

  paymentNameFunction = {
    [payment_report_group_by.patient]: this.fetchPatient.bind(this),
    [payment_report_group_by.doctor]: this.fetchdoctor.bind(this),
    [payment_report_group_by.cashier]: this.fetchCashier.bind(this),
    [payment_report_group_by.fee]: this.fetchFee.bind(this),
    [payment_report_group_by.company]: this.fetchCompany.bind(this),
    [payment_report_group_by.ward]: this.fetchWard.bind(this),
  }

  detailDisplayedColumns: string[] = ["select", "provider_id" ,"cashier_id", 'patient_id',"fee_id", "fee_type", "payment_date", "amount", "payment_type", "company_id" ]
  groupedDisplayedColumns: string[] = ["select", "target", 'price', 'quantity', "amount"]
 paymentsReportDisplayedColumns: string[] = this.detailDisplayedColumns;
  paymentsReportearchTextBox: FormControl = new FormControl();

  paymentsReportIsLoading: boolean = false;  
  paymentType: string;
  total: number;
  GraphBy = GraphBy;
  graphBy = GraphBy.doctor

  doctors: {[id: string]: DoctorDetail} = {}
  cashiers: {[id: string]: UserDetail} = {}
  fees: {[id: string]: FeeDetail} = {}
  company: {[id: string]: CompanyDetail} = {}
  wards: {[id: string]: BedroomtypeDetail} = {}
  items: {[id: string]: ItemDetail} = {}
  patients: {[id: string]: any} = {}
  itemfees: {[fee_id:string]: {item_id:string}} = {}
  payment_report_group_by = payment_report_group_by;



  @ViewChild(MatPaginator, {static: true}) paymentsReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) paymentsReportSort: MatSort;
  tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
  localISOTime = (new Date(Date.now() - this.tzoffset)).toISOString().slice(0, -1);
  start_date: FormControl = new FormControl((new Date(Date.now() - this.tzoffset)).toISOString().slice(0, 16))
  end_date: FormControl = new FormControl()

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public paymentPersist: PaymentPersist,
                public paymentNavigator: PaymentNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
                public patientNavigator: PatientNavigator,
                public doctorNavigator: DoctorNavigator,
                public userNavigator: UserNavigator,
                public companyNavigator: CompanyNavigator,
                public feeNavigator: FeeNavigator,
                public feePersist: FeePersist,
                public userPersist: UserPersist,
                public doctorPersist: DoctorPersist,
                public patientPersist: PatientPersist,
                public companyPersist: CompanyPersist,
                public transferPersist:  TransferPersist,
                public itemNavigator: ItemNavigator,
                public tcUtilsNumber: TCUtilsNumber,
                public encounterPersist: Form_EncounterPersist,
                public appointmentPersist: AppointmentPersist,
                public bedRoomTypePersist: BedroomtypePersist,
                public bedRoomTypeNavigator: BedroomtypeNavigator,
                public outsourcingCompanyNavigator: OutsourcingCompanyNavigator,
                public itemPersist: ItemPersist,) {
                this.tcAuthorization.requireRead("payment_reports");
                this.paymentsReportearchTextBox.setValue(paymentPersist.paymentReportSearchHistory.search_text);
                //delay subsequent keyup events
                this.paymentsReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.paymentPersist.paymentReportSearchHistory.search_text = value;
                this.searchpaymentPersistReport();
              });
              this.paymentsReportDisplayedColumns = this.paymentPersist.paymentReportSearchHistory.group_by ? this.groupedDisplayedColumns : this.detailDisplayedColumns
              // this.paymentPersist.start_date = tcUtilsDate.toTimeStamp(new Date());
              this.paymentPersist.paymentReportSearchHistory.is_report = true;
                this.start_date.valueChanges.pipe().subscribe(value => {
                  this.paymentPersist.paymentReportSearchHistory.start_date = value && this.tcUtilsDate.toTimeStamp(new Date(value))
                  this.searchpaymentPersistReport();
                });

                this.end_date.valueChanges.pipe().subscribe(value => {
                  this.paymentPersist.paymentReportSearchHistory.end_date = value && this.tcUtilsDate.toTimeStamp(new Date(value))
                  this.searchpaymentPersistReport();
                });

                 }

  ngOnInit(): void {
   this.reset()
    this.paymentPersist.paymentReportSearchHistory.payment_status = payment_status.paid;
    this.paymentsReportSort.sortChange.subscribe(() => {
      this.paymentsReportPaginator.pageIndex = 0;
      this.searchpaymentPersistReport(true);
    });

    this.paymentsReportPaginator.page.subscribe(() => {
      this.searchpaymentPersistReport(true);
    });
    this.paymentsReportSort.active = "payment_date"
    //start by loading items
    this.searchpaymentPersistReport();
  }

  ngOnDestroy(): void {
    // this.paymentPersist.start_date = null;
    // this.paymentPersist.end_date = null;
    // this.paymentPersist.paymentSearchText = "";
    // this.paymentPersist.isReport = false;
    // this.showGraph = false
    // this.reset()
  }

  getDate(data: PaymentSummary) {
    data.name = this.tcUtilsDate.toDate(+data.target).toString()
  }

  getDoctor(id: string){
    const {first_name, middle_name, last_name} = this.doctors[id]
    return first_name ? `${first_name} ${middle_name} ${last_name}` : ""
  }

  getPatient(id: string){
    const {patient_name} = this.patients[id]
    return patient_name ? `${patient_name}` : ""
  }

  getFee(id: string, feetype: number){
    if (feetype == fee_type.item){
      return this.itemNavigator.itemName(this.items[this.itemfees[id]?.item_id])
    }
    return this.fees[id]?.name ||  ""
  }

  getCompany(id: string){
    return this.company[id]?.name ||  ""
  }

  getWard(id: string){
    return this.wards[id]?.name ||  ""
  }

  getCashier(id: string){
    return this.cashiers[id]?.name ||  ""
  }

  getPaymentStatus(target: number) {
   return this.appTranslation.getTextByIdentifier(this.tcUtilsArray.getEnum(this.paymentPersist.payment_status, target)?.name)
  }

  getFeeType(target: number) {
    return this.appTranslation.getTextByIdentifier(this.tcUtilsArray.getEnum(this.paymentPersist.fee_type, target)?.name)
   }

  getPaymentType(target: number) {
    return this.tcUtilsArray.getEnum(this.paymentPersist.paymentType, target)?.name
  }

  updateColumn(){
    this.paymentsReportDisplayedColumns = this.paymentPersist.paymentReportSearchHistory.group_by ? this.groupedDisplayedColumns : this.detailDisplayedColumns
    this.paymentsReportSort.active = this.paymentPersist.paymentReportSearchHistory.group_by ? "target" : "id"
    this.searchpaymentPersistReport()
  }

  reset(): void {
    // this.paymentPersist.cashier_id = null;
    // this.paymentPersist.provider_id = null;
    // this.paymentPersist.patient_id = null;
    // this.paymentPersist.company_id = null;
    // this.paymentPersist.fee_id = null;
    // this.paymentPersist.fee_typeId = null;
    // this.paymentPersist.payment_status_Id = null;
    // this.paymentPersist.paymentTypeFilter = null;
  }

  searchPatient() {
    let dialogRef = this.patientNavigator.pickPatients(true);
    dialogRef.afterClosed().subscribe((result: PatientSummary) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.patient_name = result[0].fname + ' ' + result[0].lname;
        this.paymentPersist.paymentReportSearchHistory.patient_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchFee(){
    let dialogRef = this.feeNavigator.pickFees(true, this.paymentPersist.paymentReportSearchHistory.fee_type);
    dialogRef.afterClosed().subscribe((result: FeeSummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.fee_name = result[0].name;
        this.paymentPersist.paymentReportSearchHistory.fee_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchCompany() {
    let dialogRef = this.companyNavigator.pickCompanys(true);
    dialogRef.afterClosed().subscribe((result: CompanySummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.company_name = result[0].name;
        this.paymentPersist.paymentReportSearchHistory.company_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchWard() {
    let dialogRef = this.bedRoomTypeNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.ward_name = result[0].name;
        this.paymentPersist.paymentReportSearchHistory.ward_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchOutsourcingCompany() {
    let dialogRef = this.outsourcingCompanyNavigator.pickOutsourcingCompanys(true);
    dialogRef.afterClosed().subscribe((result: CompanySummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.outsourcing_company_name = result[0].name;
        this.paymentPersist.paymentReportSearchHistory.outsourcing_company_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchProvider() {
    let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
    dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.provider_name = result[0].first_name + ' ' + result[0].last_name;
        this.paymentPersist.paymentReportSearchHistory.provider_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  searchCashier(){
    let dialogRef = this.userNavigator.pickUsers(true);
    dialogRef.afterClosed().subscribe((result: UserSummary[]) => {
      if (result) {
        this.paymentPersist.paymentReportSearchHistory.cashier_name = result[0].name;
        this.paymentPersist.paymentReportSearchHistory.cashier_id = result[0].id
        this.searchpaymentPersistReport()
      }
    });
  }

  changeGraph(){
    this.reset()
    this.searchpaymentPersistReport()
  }

  searchpaymentPersistReport(isPagination:boolean = false): void {

    let paginator = this.paymentsReportPaginator;
    let sorter = this.paymentsReportSort;

    this.paymentsReportIsLoading = true;
    this.paymentsReportselection = [];

    this.paymentPersist.searchPayment(this.showGraph ? -1 : paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.paymentPersist.paymentReportSearchHistory).subscribe((partialList:unknown) => {
      const parital = partialList as PaymentReportList
      this.paymentsReportData = parital.data[0];
      if(this.paymentPersist.paymentReportSearchHistory.group_by)
       {this.paymentsReportData.forEach(
        data => {
          const fun = this.paymentNameFunction[this.paymentPersist.paymentReportSearchHistory.group_by]
          fun && fun(data.target, data.fee_type)
        }
      )} else {
        this.paymentsReportData.forEach(
          payment => {
            this.fetchdoctor(payment.provider_id)
            this.fetchPatient(payment.patient_id)
            this.fetchFee(payment.fee_id, payment.fee_type)
            this.fetchCompany(payment.company_id)
            // else{
            //   payment.item_id = this.itemfees[payment.fee_id].item_id
            // }
            this.fetchCashier(payment.cashier_id)
          }
        )
      }
      this.showGraph && this.changeToReportGraph()
      this.total = parital.data[1];
      if (parital.total != -1) {
        this.paymentsReportTotalCount = parital.total;
      }
      this.paymentsReportIsLoading = false;
    }, error => {
      this.paymentsReportIsLoading = false;
    });
  }

  getTargetName(target: string, fee_type: number): string{
    const fun = this.targetNameFunction[this.paymentPersist.paymentReportSearchHistory.group_by]
    return fun ? (this.paymentPersist.paymentReportSearchHistory.group_by == payment_report_group_by.fee ? fun(target, fee_type) : fun(target) ): ''
  }

  changeToReportGraph(){
    this.multi = [];
    const [filterdData, report_key] = this.getFilteredData();
    for (const data in filterdData) {
      this.colorScheme.domain = [...this.colorScheme.domain, `#${data.substring(0, 3)}`]
      this.multi = [...this.multi, {
        name: filterdData[data],
        series: this.paymentsReportData.filter(report => `${report[report_key]}` === data).map(report => ({name: new Date(this.tcUtilsDate.toDate(report.payment_date).setHours(0, 0, 0, 0)), value: report.amount}))
      }]
    }
  }

  fetchdoctor(provider_id: string){
    if (!this.doctors[provider_id]){
      this.doctors[provider_id] = new DoctorDetail()
      provider_id != this.tcUtilsString.invalid_id && this.doctorPersist.getDoctor(provider_id).subscribe(
        doctor => {
          this.doctors[provider_id] = doctor;
        }
      )
    }
  }

  fetchPatient(patient_id: string) {
    if (!this.patients[patient_id]){
      this.patients[patient_id] = new PatientDetail()
      patient_id != this.tcUtilsString.invalid_id && this.patientPersist.patientDo(patient_id, "get_patient",{}).subscribe(
        patient => {
          this.patients[patient_id] = patient;
        }
      )
    }
  }

  fetchFee(fee_id: string, feetype: number){
    if (!this.fees[fee_id]){
      this.fees[fee_id] = new FeeDetail()
      if (feetype == fee_type.item){
        fee_id != this.tcUtilsString.invalid_id && this.transferPersist.getTransfer(fee_id).subscribe(
          transfer => {
            this.itemfees[fee_id]={item_id :transfer.item_id};
            if (!this.items[transfer.item_id]){
              this.items[transfer.item_id] = new ItemDetail()
              this.itemPersist.getItem(transfer.item_id).subscribe(
                item => {
                  this.items[transfer.item_id] = item
                }
              )
            }
          }
        )
      } else {
        fee_id != this.tcUtilsString.invalid_id && this.feePersist.getFee(fee_id).subscribe(
          fee => {
            this.fees[fee_id] = fee;
          }
        )
      }
    }
  }

  fetchCompany(company_id: string){
    if (!this.company[company_id]){
      this.company[company_id] = new CompanyDetail()
      company_id != this.tcUtilsString.invalid_id && this.companyPersist.getCompany(company_id).subscribe(
        company => {
          this.company[company_id] = company;
        }
      )
    }
  }

  fetchWard(ward_id: string){
    if (!this.wards[ward_id]){
      this.wards[ward_id] = new BedroomtypeDetail()
      ward_id != this.tcUtilsString.invalid_id && this.bedRoomTypePersist.getBedroomtype(ward_id).subscribe(
        company => {
          this.wards[ward_id] = company;
        }
      )
    }
  }

  fetchCashier(cashier_id: string){
    if (!this.cashiers[cashier_id]){
      this.cashiers[cashier_id] = new UserDetail()
      cashier_id != this.tcUtilsString.invalid_id && this.userPersist.getUser(cashier_id).subscribe(
        cashier => {
          this.cashiers[cashier_id] = cashier;
        }
      )
    }
  }

  getFilteredData(): [any, string]{
    const filterdData = {}
    let report_key = ""
    if (this.paymentPersist.graph_by == GraphBy.patient){
      this.paymentsReportData.forEach(report => report.patient_id !== null && (filterdData[report.patient_id] = report.patient_name));
      report_key = 'patient_id';
    }
    if (this.paymentPersist.graph_by == GraphBy.cashier){
      this.paymentsReportData.forEach(report => report.cashier_id !== null && (filterdData[report.cashier_id] = report.cashier_name));
      report_key = 'cashier_id'
    }
    if (this.paymentPersist.graph_by == GraphBy.company){
      this.paymentsReportData.forEach(report => report.company_id !== null && (filterdData[report.company_id] = report.company_name));
      report_key = 'company_id'
    }
    if (this.paymentPersist.graph_by == GraphBy.doctor){
      this.paymentsReportData.forEach(report => report.provider_id !== null &&  (filterdData[report.provider_id] = report.provider_name));
      report_key = 'provider_id'
    }
    if (this.paymentPersist.graph_by == GraphBy.fee){
      this.paymentsReportData.forEach(report => report.fee_id !== null &&  (filterdData[report.fee_id] = report.fee_name))
      report_key = 'fee_id'
    }
    if (this.paymentPersist.graph_by == GraphBy.fee_type){
      this.paymentsReportData.forEach(report => report.fee_type !== null && (filterdData[report.fee_type] = this.appTranslation.getTextByIdentifier(this.tcUtilsArray.getEnum(this.paymentPersist.fee_type, report.fee_type).name)))
      report_key = 'fee_type'
    }
    if (this.paymentPersist.graph_by == GraphBy.payment){
      this.paymentsReportData.forEach(report => report.payment_type !== null && (filterdData[report.payment_type] = this.tcUtilsArray.getEnum(this.paymentPersist.paymentType, report.payment_type)?.name))
      report_key = 'payment_type'
    }
    return [filterdData, report_key]
  }

  downloadpaymentReport(): void {
    this.paymentPersist.paymentsDo("print_report", {
      ...this.paymentPersist.paymentReportSearchHistory
  //     patient_id: (this.paymentPersist.patient_id || ""),
  //      payment_status : (this.paymentPersist.payment_status_Id || ""), 
  //      payment_type: (this.paymentPersist.paymentTypeFilter || ""),
  //   company_id: (this.paymentPersist.company_id || ""), 
  //   provider_id: (this.paymentPersist.provider_id || ""), 
  //   cashier_id: (this.paymentPersist.cashier_id || ""), 
  //   fee_id: (this.paymentPersist.fee_id || ""),
  //    fee_type: (this.paymentPersist.fee_typeId || ""),
  // start_date: (this.paymentPersist.start_date || ""), 
  // end_date: (this.paymentPersist.end_date || ""),
  // group_by: (this.paymentPersist.paymentReportGroupByFilter || "")
}).subscribe(
    (printJob: TCId) => {
    this.jobPersist.followJob(printJob.id, "print additional_service", true);
  }
  )
  }

  back():void{
    this.location.back();
  }

}
