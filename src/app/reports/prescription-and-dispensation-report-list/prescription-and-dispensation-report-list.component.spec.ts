import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrescriptionAndDispensationReportListComponent } from './prescription-and-dispensation-report-list.component';

describe('PrescriptionAndDispensationReportListComponent', () => {
  let component: PrescriptionAndDispensationReportListComponent;
  let fixture: ComponentFixture<PrescriptionAndDispensationReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrescriptionAndDispensationReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrescriptionAndDispensationReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
