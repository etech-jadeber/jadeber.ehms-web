import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PrescriptionsSummary, PrescriptionsSummaryPartialList } from '../../form_encounters/form_encounter.model';
import { Form_EncounterPersist } from '../../form_encounters/form_encounter.persist';
import { Form_EncounterNavigator } from '../../form_encounters/form_encounter.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';

@Component({
  selector: 'app-prescription-and-dispensation-report-list',
  templateUrl: './prescription-and-dispensation-report-list.component.html',
  styleUrls: ['./prescription-and-dispensation-report-list.component.scss']
})
export class PrescriptionAndDispensationReportListComponent implements OnInit {

  prescriptionDispensesReportData: PrescriptionsSummary[] = [];
  prescriptionDispensesReportTotalCount: number = 0;
  prescriptionDispensesReportselectAll:boolean = false;
  prescriptionDispensesReportselection: PrescriptionsSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 prescriptionDispensesReportDisplayedColumns: string[] = ["select", "id", "patient_name", "drug", "provider", "quantity", "dispatch_status", "batch_no" ];
  prescriptionDispensesReportearchTextBox: FormControl = new FormControl();
  prescriptionDispensesReportIsLoading: boolean = false;  
  prescriptionDispenseType: string;

  @ViewChild(MatPaginator, {static: true}) prescriptionDispensesReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) prescriptionDispensesReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public prescriptionDispensePersist: Form_EncounterPersist,
                public prescriptionDispenseNavigator: Form_EncounterNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString) {
                this.tcAuthorization.requireRead("prescription_and_dispensation_reports");
                this.prescriptionDispensesReportearchTextBox.setValue(prescriptionDispensePersist.prescriptionsSearchText);
                //delay subsequent keyup events
                this.prescriptionDispensesReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.prescriptionDispensePersist.prescriptionsSearchText = value;
                this.searchprescriptionDispensePersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.from_date = new Date(value._d).getTime()/1000;
                  this.searchprescriptionDispensePersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.to_date = new Date(value._d).getTime()/1000;
                  this.searchprescriptionDispensePersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.prescriptionDispensesReportSort.sortChange.subscribe(() => {
      this.prescriptionDispensesReportPaginator.pageIndex = 0;
      this.searchprescriptionDispensePersistReport(true);
    });

    this.prescriptionDispensesReportPaginator.page.subscribe(() => {
      this.searchprescriptionDispensePersistReport(true);
    });
    //start by loading items
    this.searchprescriptionDispensePersistReport();
  }

  ngOnDestroy(): void {
    this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.from_date = null;
    this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.to_date = null;
  }

  searchprescriptionDispensePersistReport(isPagination:boolean = false): void {

    let paginator = this.prescriptionDispensesReportPaginator;
    let sorter = this.prescriptionDispensesReportSort;

    this.prescriptionDispensesReportIsLoading = true;
    this.prescriptionDispensesReportselection = [];

    this.prescriptionDispensePersist.prescriptionssDo(this.tcUtilsString.invalid_id,"prescriptionDispenseReport",
    {start_date: this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.from_date,
     end_date: this.prescriptionDispensePersist.prescriptionDispenseSearchHistory.to_date,
    dispatch_status: this.prescriptionDispensePersist.dispatch_status}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PrescriptionsSummaryPartialList) => {
      this.prescriptionDispensesReportData = partialList.data;
      if (partialList.total != -1) {
        this.prescriptionDispensesReportTotalCount = partialList.total;
      }
      this.prescriptionDispensesReportIsLoading = false;
    }, error => {
      this.prescriptionDispensesReportIsLoading = false;
    });
  }

  downloadprescriptionDispenseReport(): void {
    if(this.prescriptionDispensesReportselectAll){
         this.prescriptionDispensePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "prescriptionDispense_report", true);
      });
    }
    else{
        this.prescriptionDispensePersist.download(this.tcUtilsArray.idsList(this.prescriptionDispensesReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "prescriptionDispense_report",true);
            });
        }
  }

  back():void{
    this.location.back();
  }

  clearStatus(){
    this.prescriptionDispensePersist.dispatch_status = null;
    this.searchprescriptionDispensePersistReport()
  }

}
