import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcedureReportListComponent } from './procedure-report-list.component';

describe('ProcedureReportListComponent', () => {
  let component: ProcedureReportListComponent;
  let fixture: ComponentFixture<ProcedureReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProcedureReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcedureReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
