import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { ProcedureSummary, ProcedureSummaryPartialList } from '../../procedures/procedure.model';
import { ProcedurePersist } from '../../procedures/procedure.persist';
import { ProcedureNavigator } from '../../procedures/procedure.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Procedure_TypeNavigator } from 'src/app/procedure_types/procedure_type.navigator';

@Component({
  selector: 'app-procedure-report-list',
  templateUrl: './procedure-report-list.component.html',
  styleUrls: ['./procedure-report-list.component.scss']
})
export class ProcedureReportListComponent implements OnInit {

  proceduresReportData: ProcedureSummary[] = [];
  proceduresReportTotalCount: number = 0;
  proceduresReportselectAll:boolean = false;
  proceduresReportselection: ProcedureSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 proceduresReportDisplayedColumns: string[] = ["select", "pr.name", "hospital" ,"referral", "total" ];
  proceduresReportearchTextBox: FormControl = new FormControl();
  proceduresReportIsLoading: boolean = false;  
  procedureType: string;

  @ViewChild(MatPaginator, {static: true}) proceduresReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) proceduresReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public procedurePersist: ProcedurePersist,
                public procedureNavigator: ProcedureNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public procedureTypeNavigator: Procedure_TypeNavigator) {
                this.tcAuthorization.requireRead("procedure_reports");
                this.proceduresReportearchTextBox.setValue(procedurePersist.procedureSearchText);
                //delay subsequent keyup events
                this.proceduresReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.procedurePersist.procedureSearchText = value;
                this.searchProcedurePersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.procedurePersist.from_date = new Date(value._d).getTime()/1000;
                  this.searchProcedurePersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.procedurePersist.to_date = new Date(value._d).getTime()/1000;
                  this.searchProcedurePersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.proceduresReportSort.sortChange.subscribe(() => {
      this.proceduresReportPaginator.pageIndex = 0;
      this.searchProcedurePersistReport(true);
    });

    this.proceduresReportPaginator.page.subscribe(() => {
      this.searchProcedurePersistReport(true);
    });
    //start by loading items
    this.searchProcedurePersistReport();
  }

  ngOnDestroy(): void {
    this.procedurePersist.from_date = null;
    this.procedurePersist.to_date = null;
    this.procedurePersist.procedureSearchText = "";
  }

  searchProcedurePersistReport(isPagination:boolean = false): void {

    let paginator = this.proceduresReportPaginator;
    let sorter = this.proceduresReportSort;

    this.proceduresReportIsLoading = true;
    this.proceduresReportselection = [];

    this.procedurePersist.proceduresDo("procedureReport",
    {start_date: this.procedurePersist.from_date,
     end_date: this.procedurePersist.to_date, 
    procedure_type_id: this.procedurePersist.procedureType}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ProcedureSummaryPartialList) => {
      this.proceduresReportData = partialList.data;
      if (partialList.total != -1) {
        this.proceduresReportTotalCount = partialList.total;
      }
      this.proceduresReportIsLoading = false;
    }, error => {
      this.proceduresReportIsLoading = false;
    });
  }

  downloadProcedureReport(): void {
    if(this.proceduresReportselectAll){
         this.procedurePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "procedure_report", true);
      });
    }
    else{
        this.procedurePersist.download(this.tcUtilsArray.idsList(this.proceduresReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "procedure_report",true);
            });
        }
  }

  searchProcedureType(): void {
    let dialogRef = this.procedureTypeNavigator.pickProcedure_Types(true)
    dialogRef.afterClosed().subscribe(value => {
      this.procedureType = value[0].name;
      this.procedurePersist.procedureType = value[0].id
      this.searchProcedurePersistReport()
    })
  }

  back():void{
    this.location.back();
  }
}
