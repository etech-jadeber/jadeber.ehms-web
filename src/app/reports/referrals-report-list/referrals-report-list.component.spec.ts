import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralsReportListComponent } from './referrals-report-list.component';

describe('ReferralsReportListComponent', () => {
  let component: ReferralsReportListComponent;
  let fixture: ComponentFixture<ReferralsReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReferralsReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralsReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
