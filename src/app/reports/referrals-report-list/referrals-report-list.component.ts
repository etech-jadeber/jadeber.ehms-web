import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { PatientReferralFormSummary, PatientReferralFormSummaryPartialList } from '../../patient_referral_form/patient_referral_form.model';
import { PatientReferralFormPersist } from '../../patient_referral_form/patient_referral_form.persist';
import { PatientReferralFormNavigator } from '../../patient_referral_form/patient_referral_form.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';

@Component({
  selector: 'app-referrals-report-list',
  templateUrl: './referrals-report-list.component.html',
  styleUrls: ['./referrals-report-list.component.scss']
})
export class ReferralsReportListComponent implements OnInit {

  referralsReportData: PatientReferralFormSummary[] = [];
  referralsReportTotalCount: number = 0;
  referralsReportselectAll:boolean = false;
  referralsReportselection: PatientReferralFormSummary[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 referralsReportDisplayedColumns: string[] = ["select", "id", "patient_name" ,"refer_to", "refer_date", "reply_date", "reason" ];
  referralsReportearchTextBox: FormControl = new FormControl();
  referralsReportIsLoading: boolean = false;  
  referralType: string;

  @ViewChild(MatPaginator, {static: true}) referralsReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) referralsReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public referralPersist: PatientReferralFormPersist,
                public referralNavigator: PatientReferralFormNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString) {
                this.tcAuthorization.requireRead("referral_reports");
                this.referralsReportearchTextBox.setValue(referralPersist.patientReferralFormSearchText);
                //delay subsequent keyup events
                this.referralsReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.referralPersist.patientReferralFormSearchText = value;
                this.searchreferralPersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.referralPersist.from_date = new Date(value._d).getTime()/1000;
                  this.searchreferralPersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.referralPersist.to_date = new Date(value._d).getTime()/1000;
                  this.searchreferralPersistReport();
                });
                 }

  ngOnInit(): void {
   
    this.referralsReportSort.sortChange.subscribe(() => {
      this.referralsReportPaginator.pageIndex = 0;
      this.searchreferralPersistReport(true);
    });

    this.referralsReportPaginator.page.subscribe(() => {
      this.searchreferralPersistReport(true);
    });
    //start by loading items
    this.searchreferralPersistReport();
  }

  ngOnDestroy(): void {
    this.referralPersist.from_date = null;
    this.referralPersist.to_date = null;
    this.referralPersist.patientReferralFormSearchText = "";
  }

  searchreferralPersistReport(isPagination:boolean = false): void {

    let paginator = this.referralsReportPaginator;
    let sorter = this.referralsReportSort;

    this.referralsReportIsLoading = true;
    this.referralsReportselection = [];

    this.referralPersist.patientReferralFormsDo("","referralReport",
    {start_date: this.referralPersist.from_date,
     end_date: this.referralPersist.to_date}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: PatientReferralFormSummaryPartialList) => {
      this.referralsReportData = partialList.data;
      if (partialList.total != -1) {
        this.referralsReportTotalCount = partialList.total;
      }
      this.referralsReportIsLoading = false;
    }, error => {
      this.referralsReportIsLoading = false;
    });
  }

  downloadreferralReport(): void {
    if(this.referralsReportselectAll){
         this.referralPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "referral_report", true);
      });
    }
    else{
        this.referralPersist.download(this.tcUtilsArray.idsList(this.referralsReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "referral_report",true);
            });
        }
  }

  back():void{
    this.location.back();
  }

}
