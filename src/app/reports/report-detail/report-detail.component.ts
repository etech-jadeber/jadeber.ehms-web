import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {ReportDetail} from "../report.model";
import {ReportPersist} from "../report.persist";
import {ReportNavigator} from "../report.navigator";

export enum ReportTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  reportLoading:boolean = false;
  
  ReportTabs: typeof ReportTabs = ReportTabs;
  activeTab: ReportTabs = ReportTabs.overview;
  //basics
  reportDetail: ReportDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public reportNavigator: ReportNavigator,
              public  reportPersist: ReportPersist) {
    this.tcAuthorization.requireRead("inventory_reports");
    this.reportDetail = new ReportDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("inventory_reports");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.reportLoading = true;
    this.reportPersist.getReport(id).subscribe(reportDetail => {
          this.reportDetail = reportDetail;
          this.reportLoading = false;
        }, error => {
          console.error(error);
          this.reportLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.reportDetail.id);
  }

  editReport(): void {
    let modalRef = this.reportNavigator.editReport(this.reportDetail.id);
    modalRef.afterClosed().subscribe(modifiedReportDetail => {
      TCUtilsAngular.assign(this.reportDetail, modifiedReportDetail);
    }, error => {
      console.error(error);
    });
  }

   printReport():void{
      this.reportPersist.print(this.reportDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print report", true);
      });
    }

  back():void{
      this.location.back();
    }

}
