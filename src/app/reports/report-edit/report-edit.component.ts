import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';

import { ReportDetail, ReportEntryType } from '../report.model';
import { ReportPersist } from '../report.persist';
import { now } from 'moment';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { AsyncJob, JobData, JobState } from 'src/app/tc/jobs/job.model';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { interval } from 'rxjs';
import { entry_type, report_type } from 'src/app/app.enums';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';



@Component({
  selector: 'app-report-edit',
  templateUrl: './report-edit.component.html',
  styleUrls: ['./report-edit.component.css'],
})
export class ReportEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  reportType = report_type;
  message: string = '';
  success_state = 3;
  entry_type:entry_type
  ReportEntryType:typeof ReportEntryType = ReportEntryType;
  userFullName:string = "";
  report_type:report_type;
  reportDetail: ReportDetail;
  from_date: Date = new Date();
  to_date: Date = new Date();
  ledgerEntry = {
    entry_type: 0,
    from_date: 0,
    to_date: 0,
    description:""
  };

  doctorEncounter = {
    provider_id:"",
    start_date: 0,
    end_date: 0,
  }
  minDate:Date = new Date();
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<ReportEditComponent>,
    public persist: ReportPersist,
    public tcUtilsArray: TCUtilsArray,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public tcNavigator: TCNavigator,
    public filePersist: FilePersist,
    public doctorNavigator:DoctorNavigator,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('inventory_reports');
      this.title =
        this.appTranslation.getText('general', 'new') + " " +
        this.appTranslation.getText('report', 'report');


       this.report_type = new report_type();

      this.reportDetail = new ReportDetail();
      this.reportDetail.upload_date = this.tcUtilsDate.toTimeStamp(
        this.tcUtilsDate.now()
      );
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('inventory_reports');
      this.title =
        this.appTranslation.getText('general', 'edit') + " " +
        this.appTranslation.getText('report', 'report');
      this.isLoadingResults = true;
      this.persist.getReport(this.idMode.id).subscribe(
        (reportDetail) => {
          this.reportDetail = reportDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    if (this.reportDetail.report_type == report_type.purchase) {
      // item ledger entry report

      this.ledgerEntry.to_date = this.tcUtilsDate.toTimeStamp(
        this.to_date
      );
      this.ledgerEntry.from_date = this.tcUtilsDate.toTimeStamp(
        this.from_date
      );
      this.ledgerEntry.description = this.reportDetail.description;
      this.ledgerEntry.entry_type = this.reportDetail.report_type;
      let success_state = 3;

      this.persist
        .reportsDo('item_ledger_entry_report', this.ledgerEntry)
        .subscribe(
          (downloadJob) => {
            this.followJob(
              downloadJob.id,
              'download itemLdger entry reports',
              true
            );
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              if (job.job_state == success_state) {
              }
            });
            this.dialogRef.close(this.reportDetail);
          },
          (error) => {
            this.isLoadingResults = false;
          }
        );
    }else if (this.reportDetail.report_type == report_type.transfer) {
      // item ledger entry report

      this.ledgerEntry.to_date = this.tcUtilsDate.toTimeStamp(
        this.to_date
      );
      this.ledgerEntry.from_date = this.tcUtilsDate.toTimeStamp(
        this.from_date
      );
      this.ledgerEntry.description = this.reportDetail.description;
      this.ledgerEntry.entry_type = this.reportDetail.report_type;
      let success_state = 3;

      this.persist
        .reportsDo('item_ledger_entry_report', this.ledgerEntry)
        .subscribe(
          (downloadJob) => {
            this.followJob(
              downloadJob.id,
              'download itemLdger entry reports',
              true
            );
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              if (job.job_state == success_state) {
              }
            });
            this.dialogRef.close(this.reportDetail);
          },
          (error) => {
            this.isLoadingResults = false;
          }
        );
    } else if (this.reportDetail.report_type == report_type.sales) {
      // item ledger entry report

      this.ledgerEntry.to_date = this.tcUtilsDate.toTimeStamp(
        this.to_date
      );
      this.ledgerEntry.from_date = this.tcUtilsDate.toTimeStamp(
        this.from_date
      );
      this.ledgerEntry.description = this.reportDetail.description;
      this.ledgerEntry.entry_type = this.reportDetail.report_type;
      let success_state = 3;

      this.persist
        .reportsDo('item_ledger_entry_report', this.ledgerEntry)
        .subscribe(
          (downloadJob) => {
            this.followJob(
              downloadJob.id,
              'download itemLdger entry reports',
              true
            );
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              if (job.job_state == success_state) {
              }
            });
            this.dialogRef.close(this.reportDetail);
          },
          (error) => {
            this.isLoadingResults = false;
          }
        );
    }else if (this.reportDetail.report_type == report_type.all) {
      // item ledger entry report

      this.ledgerEntry.to_date = this.tcUtilsDate.toTimeStamp(
        this.to_date
      );
      this.ledgerEntry.from_date = this.tcUtilsDate.toTimeStamp(
        this.from_date
      );
      this.ledgerEntry.description = this.reportDetail.description;
      this.ledgerEntry.entry_type = this.reportDetail.report_type;
      let success_state = 3;

      this.persist
        .reportsDo('item_ledger_entry_report', this.ledgerEntry)
        .subscribe(
          (downloadJob) => {
            this.followJob(
              downloadJob.id,
              'download itemLdger entry reports',
              true
            );
            this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
              if (job.job_state == success_state) {
              }
            });
            this.dialogRef.close(this.reportDetail);
          },
          (error) => {
            this.isLoadingResults = false;
          }
        );
      }else if (this.reportDetail.report_type == report_type.doctor_encounter) {
        // item ledger entry report

        this.doctorEncounter.start_date = this.tcUtilsDate.toTimeStamp(
          this.from_date
        );
        this.doctorEncounter.end_date = this.tcUtilsDate.toTimeStamp(
          this.to_date
        );
        let success_state = 3;


        this.persist
          .reportsDo('doctor_report', this.doctorEncounter)
          .subscribe(
            (downloadJob) => {
              this.followJob(
                downloadJob.id,
                'download doctor encounter',
                true
              );
              this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
                if (job.job_state == success_state) {
                }
              });
              this.dialogRef.close(this.reportDetail);
            },
            (error) => {
              this.isLoadingResults = false;
            }
          );
    }else {
      this.tcNotification.error('Report type is not correctly selected.');
      this.isLoadingResults = false;
    }
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateReport(this.reportDetail).subscribe(
      (value) => {
        this.tcNotification.success('Report updated');
        this.dialogRef.close(this.reportDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.reportDetail == null) {
      return false;
    }
    if (
      this.reportDetail.description == null ||
      this.reportDetail.description == ''
    ) {
      return false;
    }

    return true;
  }

  searchUser(){
    let dialogRef = this.doctorNavigator.pickDoctors(true);
    dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
      if (result) {
        this.userFullName =  `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}` ;
        this.doctorEncounter.provider_id = result[0].id;
      }
    });
  }

  followJob(
    id: string,
    descrption: string,
    is_download_job: boolean = true
  ): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }

    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe((n) => {
      this.jobPersist.jobState(id).subscribe((jobstate: JobState) => {
        if (jobstate.job_state == this.success_state) {
          this.jobPersist.unfollowJob(id, jobstate);

          if (is_download_job) {
            this.jobPersist.downloadFileKey(id).subscribe((jobFile) => {
              this.tcNavigator.openUrl(
                this.filePersist.downloadLink(jobFile.download_key)
              );
            });
          }
        } else {
          this.message = jobstate.message;
          this.jobPersist.unfollowJob(id, jobstate);

          this.tcNotification.error(this.message);
        }
      });
    });

    return jb;
  }
}
