import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {ReportPersist} from "../report.persist";
import {ReportNavigator} from "../report.navigator";
import {ReportDetail, ReportSummary, ReportSummaryPartialList} from "../report.model";
import { Item_Ledger_EntryPersist } from 'src/app/item_ledger_entrys/item_ledger_entry.persist';


@Component({
  selector: 'app-report-list',
  templateUrl: './report-list.component.html',
  styleUrls: ['./report-list.component.css']
})
export class ReportListComponent implements OnInit {

  reportsData: ReportSummary[] = [];
  reportsTotalCount: number = 0;
  reportsSelectAll:boolean = false;
  reportsSelection: ReportSummary[] = [];

  reportsDisplayedColumns: string[] = ["select","action","report_type","file_name","file_id","description","upload_date", ];
  reportsSearchTextBox: FormControl = new FormControl();
  reportsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) reportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) reportsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public reportPersist: ReportPersist,
                public reportNavigator: ReportNavigator,
                public jobPersist: JobPersist,
                public item_ledger_entryPersist: Item_Ledger_EntryPersist,

    ) {

        this.tcAuthorization.requireRead("inventory_reports");
       this.reportsSearchTextBox.setValue(reportPersist.reportSearchText);
      //delay subsequent keyup events
      this.reportsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.reportPersist.reportSearchText = value;
        this.searchReports();
      });
    }

    ngOnInit() {

      this.reportsSort.sortChange.subscribe(() => {
        this.reportsPaginator.pageIndex = 0;
        this.searchReports(true);
      });

      this.reportsPaginator.page.subscribe(() => {
        this.searchReports(true);
      });
      //start by loading items
      this.searchReports();
    }

  searchReports(isPagination:boolean = false): void {


    let paginator = this.reportsPaginator;
    let sorter = this.reportsSort;

    this.reportsIsLoading = true;
    this.reportsSelection = [];

    this.reportPersist.searchReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ReportSummaryPartialList) => {
      this.reportsData = partialList.data;
      if (partialList.total != -1) {
        this.reportsTotalCount = partialList.total;
      }
      this.reportsIsLoading = false;
    }, error => {
      this.reportsIsLoading = false;
    });

  }

  downloadReports(): void {
    if(this.reportsSelectAll){
         this.reportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download reports", true);
      });
    }
    else{
        this.reportPersist.download(this.tcUtilsArray.idsList(this.reportsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download reports",true);
            });
        }
  }



  addReport(): void {
    let dialogRef = this.reportNavigator.addReport();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchReports();
      }
    });
  }

  editReport(item: ReportSummary) {
    let dialogRef = this.reportNavigator.editReport(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteReport(item: ReportSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Report");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.reportPersist.deleteReport(item.id).subscribe(response => {
          this.tcNotification.success("Report deleted");
          this.searchReports();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
