import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ReportPickComponent } from './report-pick.component';

describe('ReportPickComponent', () => {
  let component: ReportPickComponent;
  let fixture: ComponentFixture<ReportPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
