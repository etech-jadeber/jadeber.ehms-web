import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {ReportDetail, ReportSummary, ReportSummaryPartialList} from "../report.model";
import {ReportPersist} from "../report.persist";


@Component({
  selector: 'app-report-pick',
  templateUrl: './report-pick.component.html',
  styleUrls: ['./report-pick.component.css']
})
export class ReportPickComponent implements OnInit {

  reportsData: ReportSummary[] = [];
  reportsTotalCount: number = 0;
  reportsSelection: ReportSummary[] = [];
  reportsDisplayedColumns: string[] = ["select", 'report_type','file_name','file_id','description','upload_date' ];

  reportsSearchTextBox: FormControl = new FormControl();
  reportsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) reportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) reportsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public reportPersist: ReportPersist,
              public dialogRef: MatDialogRef<ReportPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("reports");
    this.reportsSearchTextBox.setValue(reportPersist.reportSearchText);
    //delay subsequent keyup events
    this.reportsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.reportPersist.reportSearchText = value;
      this.searchReports();
    });
  }

  ngOnInit() {

    this.reportsSort.sortChange.subscribe(() => {
      this.reportsPaginator.pageIndex = 0;
      this.searchReports();
    });

    this.reportsPaginator.page.subscribe(() => {
      this.searchReports();
    });

    //set initial picker list to 5
    this.reportsPaginator.pageSize = 5;

    //start by loading items
    this.searchReports();
  }

  searchReports(): void {
    this.reportsIsLoading = true;
    this.reportsSelection = [];

    this.reportPersist.searchReport(this.reportsPaginator.pageSize,
        this.reportsPaginator.pageIndex,
        this.reportsSort.active,
        this.reportsSort.direction).subscribe((partialList: ReportSummaryPartialList) => {
      this.reportsData = partialList.data;
      if (partialList.total != -1) {
        this.reportsTotalCount = partialList.total;
      }
      this.reportsIsLoading = false;
    }, error => {
      this.reportsIsLoading = false;
    });

  }

  markOneItem(item: ReportSummary) {
    if(!this.tcUtilsArray.containsId(this.reportsSelection,item.id)){
          this.reportsSelection = [];
          this.reportsSelection.push(item);
        }
        else{
          this.reportsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.reportsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
