import { entry_type } from "../app.enums";
import {TCId} from "../tc/models";

export class ReportSummary extends TCId {
  report_type : number;
file_name : string;
file_id : string;
description : string;
upload_date : number;
}

export class ReportSummaryPartialList {
  data: ReportSummary[];
  total: number;
}

export class ReportDetail extends ReportSummary {
  report_type : any;
file_name : string;
file_id : string;
description : string;
upload_date : number;
}

export class ReportDashboard {
  total: number;
}

export class ReportEntryType extends entry_type{
   sales: number = 102;
	 purchase: number = 100;
	 transfer: number = 101;
	 all: number = 103;
 
  // get sales() {
  //   return this.sales;
  // }

  // get purchase() {
  //   return this.purchase;
  // }

  // get transfer() {
  //   return this.transfer;
  // }
} 

export class SellReportSummary extends TCId{
  name:string;
  quantity:number;
  unit: number;
  total_price: number
}

export  class SellReportSummaryPartialList{
  data: SellReportSummary[];
  total_price:number;
  total: number;
}