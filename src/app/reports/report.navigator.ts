import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog,MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {ReportEditComponent} from "./report-edit/report-edit.component";
import {ReportPickComponent} from "./report-pick/report-pick.component";


@Injectable({
  providedIn: 'root'
})

export class ReportNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  reportsUrl(): string {
    return "/inventory_reports";
  }

  tatReportUril(): string {
    return "/tat_reports";
  }

  reportUrl(id: string): string {
    return "/inventory_reports/" + id;
  }
  labReportsUrl(): any {
    return "/laboratory_reports"
  }
  patReportsUrl(): any {
    return "/pathology_reports";
  }
  radReportsUrl(): any {
    return "/radiology_reports";
  }
  viewLabReports(): void {
    this.router.navigateByUrl(this.labReportsUrl());
    //this._router.navigate(['SecondComponent', {p1: this.property1, p2: property2 }]);

  }
  viewPatReports(): void {
    this.router.navigateByUrl(this.patReportsUrl());
    //this._router.navigate(['SecondComponent', {p1: this.property1, p2: property2 }]);

  }
  viewRadReports(): void {
    this.router.navigateByUrl(this.radReportsUrl());
    //this._router.navigate(['SecondComponent', {p1: this.property1, p2: property2 }]);

  }
  sellReportsUrl(): string {
    return "/sell_reports";
  }
  viewSellReports():void {
    this.router.navigateByUrl(this.sellReportsUrl());
  }

  paymentReportUrl(): string {
    return "/payment_reports"
  }

  itemExpireReportUrl(): string {
    return "/item_expire_date_reports"
  }

  viewPaymentReports(): void {
    this.router.navigateByUrl(this.paymentReportUrl());
  }

  viewItemExpireReports(): void {
    this.router.navigateByUrl(this.itemExpireReportUrl());
  }

  viewReports(): void {
    this.router.navigateByUrl(this.reportsUrl());
  }

  viewReport(id: string): void {
    this.router.navigateByUrl(this.reportUrl(id));
  }

  procedureReportsUrl(): string {
    return "/procedure_reports"
  }

  encounterReportsUrl(): string {
    return "/encounter_reports"
  }

  patientListReportsUrl(): string {
    return "/patient_list_reports"
  }

  prescriptionAndDispensationReportsUrl(): string {
    return "/prescription_and_dispensation_reports"
  }

  refferalReportsUrl(): string {
    return "/referral_reports"
  }

  viewProcedureReports(): void {
    this.router.navigateByUrl(this.procedureReportsUrl())
  }

  viewEncounterReports(): void {
    this.router.navigateByUrl(this.encounterReportsUrl())
  }

  viewPatientListReports(): void {
    this.router.navigateByUrl(this.patientListReportsUrl())
  }

  viewPrescriptionAndDispensationReports(): void {
    this.router.navigateByUrl(this.prescriptionAndDispensationReportsUrl())
  }

  viewReferralReports(): void {
    this.router.navigateByUrl(this.refferalReportsUrl())
  }

  editReport(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ReportEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addReport(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ReportEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }

   pickReports(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(ReportPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
