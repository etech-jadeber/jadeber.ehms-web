import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {ReportDashboard, ReportDetail, ReportSummaryPartialList, SellReportSummaryPartialList} from "./report.model";
import { entry_type, report_type } from '../app.enums';
import { AppTranslation } from '../app.translation';
import { FormControl } from '@angular/forms';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class ReportPersist {
  sellReportSearchText :string = "";
  start_date: FormControl = new FormControl({disabled: true, value: ""});
  end_date: FormControl = new FormControl({disabled: true, value: ""});
  reportSearchText: string = "";
  store_id: string;
  store_name: string;

  constructor(private http: HttpClient,public appTranslation: AppTranslation) {
  }
  entry_type: TCEnumTranslation[] = [
    new TCEnumTranslation(entry_type.sales, 'sales'),
    new TCEnumTranslation(entry_type.purchase, 'purchase'),
    new TCEnumTranslation(entry_type.transfer,'transfer'),
  ];

  report_type: TCEnumTranslation[] = [
    new TCEnumTranslation(report_type.sales, this.appTranslation.getKey('inventory', 'sales')),
    new TCEnumTranslation(report_type.purchase, this.appTranslation.getKey('inventory', 'purchase')),
    new TCEnumTranslation(report_type.transfer, this.appTranslation.getKey('inventory', 'transfer')),
    new TCEnumTranslation(report_type.all, this.appTranslation.getKey('general', 'all')),
    new TCEnumTranslation(report_type.doctor_encounter, this.appTranslation.getKey('schedule', 'encounter_report')),
  ];
  searchReport(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("reports", this.reportSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<ReportSummaryPartialList>(url);

  }

  searchsellReports(pageSize: number, pageIndex: number, sort: string, order: string): Observable<SellReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("ite", this.reportSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<SellReportSummaryPartialList>(url);

  }
  searchsellReport(pageSize: number, pageIndex: number, sort: string, order: string,method: string, payload: any): Observable<{}> {
    let url = TCUtilsHttp.buildSearchUrl("item_dispatch/do", this.sellReportSearchText, pageSize, pageIndex, sort, order);
    
      if (this.start_date.value){
        url = TCUtilsString.appendUrlParameter(url, "start_date", (new Date(this.start_date.value).getTime()/1000).toString())
      }
      if (this.end_date.value){
        url = TCUtilsString.appendUrlParameter(url, "end_date", (new Date(this.end_date.value).getTime()/1000).toString())
      }

    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }


    return this.http.post<{}>(url, new TCDoParam(method, payload));
  }


  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.reportSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "reports/do", new TCDoParam("download_all", this.filters()));
  }

  reportDashboard(): Observable<ReportDashboard> {
    return this.http.get<ReportDashboard>(environment.tcApiBaseUri + "reports/dashboard");
  }

  getReport(id: string): Observable<ReportDetail> {
    return this.http.get<ReportDetail>(environment.tcApiBaseUri + "reports/" + id);
  }

  addReport(item: ReportDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "reports/", item);
  }

  updateReport(item: ReportDetail): Observable<ReportDetail> {
    return this.http.patch<ReportDetail>(environment.tcApiBaseUri + "reports/" + item.id, item);
  }

  deleteReport(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "reports/" + id);
  }

  reportsDo(method: string, payload: any): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "reports/do", new TCDoParam(method, payload));
  }

  reportDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "reports/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "reports/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "reports/" + id + "/do", new TCDoParam("print", {}));
  }


}
