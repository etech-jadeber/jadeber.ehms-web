import {Component, OnInit, ViewChild,Input} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ReportPersist } from '../report.persist';
import { SellReportSummary, SellReportSummaryPartialList } from '../report.model';
import { StoreSummary } from 'src/app/stores/store.model';
import { StoreNavigator } from 'src/app/stores/store.navigator';

@Component({
  selector: 'sell-report',
  templateUrl: './sell-report.component.html',
  styleUrls: ['./sell-report.component.scss']
})export class SellReportComponent implements OnInit {
  sellReportsData: SellReportSummary[] = [];
  sellReportsTotalCount: number = 0;
  sellReportSelectAll:boolean = false;
  sellReportSelection: SellReportSummary[] = [];
  total_price :number;

  sellReportsDisplayedColumns: string[] = ["select","name" ,"quantity","total_price","store_name"];
  sellReportSearchTextBox: FormControl = new FormControl();
  sellReportIsLoading: boolean = false;  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public reportPersist : ReportPersist,
                public storeNavigator: StoreNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                

    ) {

        this.tcAuthorization.requireRead("sale_reports");
       this.sellReportSearchTextBox.setValue(reportPersist.sellReportSearchText);
      //delay subsequent keyup events
      this.sellReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.reportPersist.sellReportSearchText = value;
        this.searchSellReports();
      });
      this.reportPersist.start_date.valueChanges.subscribe((value)=>{

          this.searchSellReports()
      });
      this.reportPersist.end_date.valueChanges.subscribe((value)=>{
          this.searchSellReports()
      });
    } ngOnInit() {
   
      this.sort.sortChange.subscribe(() => {
        this.paginator.pageIndex = 0;
        this.searchSellReports(true);
      });

      this.paginator.page.subscribe(() => {
        this.searchSellReports(true);
      });
      //start by loading items
      this.searchSellReports();
    }

    searchSellReports(isPagination:boolean = false): void {


    let paginator = this.paginator;
    let sorter = this.sort;

    this.sellReportIsLoading = true;
    this.sellReportSelection = [];
    this.reportPersist.searchsellReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction,"sell_report",{}).subscribe((partialList: SellReportSummaryPartialList) => {

      this.sellReportsData = partialList.data;
      this.total_price=partialList.total_price;
      if (partialList.total != -1) {
        this.sellReportsTotalCount = partialList.total;

      }
      this.sellReportIsLoading = false;
    }, error => {
      this.sellReportIsLoading = false;
    });

  } downloadsellReports(): void {
    if(this.sellReportSelectAll){
         this.reportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download sell report", true);
      });
    }
    else{
        this.reportPersist.download(this.tcUtilsArray.idsList(this.sellReportSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download sell report",true);
            });
        }
  }
  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.reportPersist.store_name = result[0].name;
        this.reportPersist.store_id = result[0].id;
        this.searchSellReports();
      }
    });
  }

  back():void{
      this.location.back();
    }
}