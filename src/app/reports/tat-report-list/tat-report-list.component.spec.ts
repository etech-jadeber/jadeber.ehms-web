import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TatReportListComponent } from './tat-report-list.component';

describe('TatReportListComponent', () => {
  let component: TatReportListComponent;
  let fixture: ComponentFixture<TatReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TatReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TatReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
