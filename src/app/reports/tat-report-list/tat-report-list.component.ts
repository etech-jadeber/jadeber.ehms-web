import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
import { Lab_TestDetail } from 'src/app/lab_tests/lab_test.model';
import { Lab_TestPersist } from 'src/app/lab_tests/lab_test.persist';
import { Lab_TestNavigator } from 'src/app/lab_tests/lab_test.navigator';
import { Lab_PanelNavigator } from 'src/app/lab_panels/lab_panel.navigator';
import { lab_order_type } from 'src/app/app.enums';

interface TatRportModel {
  style: string;
  generated_sample_id: string;
  lab_test_id: string;
  lab_test_name: string;
  future_date: number;
  date_of_collection: number;
  result_date: number;
}

@Component({
  selector: 'app-tat-report-list',
  templateUrl: './tat-report-list.component.html',
  styleUrls: ['./tat-report-list.component.scss']
})
export class TatReportListComponent implements OnInit {

  tatReportData: TatRportModel[] = [];
  tatReportTotalCount: number = 0;
  tatReportselectAll:boolean = false;
  tatReportselection: TatRportModel[] = [];
  from_date: FormControl = new FormControl({disabled: true, value: ""});
  to_date: FormControl = new FormControl({disabled: true, value: ""})

 tatReportDisplayedColumns: string[] = ["select", "name", "tat_time", "tat_in" ,"tat_out"];
  tatReportearchTextBox: FormControl = new FormControl();
  tatReportIsLoading: boolean = false;
  lab_tests: {[id: string]: Lab_TestDetail} = {}
  lab_panel_id: string;
  lab_panel_name: string
  lab_test_id: string;
  lab_test_name: string;
  order_type: number = lab_order_type.lab;

  @ViewChild(MatPaginator, {static: true}) tatReportPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) tatReportSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public tatPersist: OrdersPersist,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public labTestPersist: Lab_TestPersist,
                public labTestNavigator: Lab_TestNavigator,
                public labPanelNavigator: Lab_PanelNavigator,
                public ordersPersist: OrdersPersist,
                public tcAsyncJob: TCAsyncJob,) {
                this.tcAuthorization.requireRead("tat_reports");
                this.tatReportearchTextBox.setValue(tatPersist.tatReportSearchHistory.search_text);
                //delay subsequent keyup events
                this.tatReportearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
                this.tatPersist.tatReportSearchHistory.search_text = value;
                this.searchtatPersistReport();
              });
                this.from_date.valueChanges.pipe().subscribe(value => {
                  this.tatPersist.tatReportSearchHistory.ordered_start_date = value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null;
                  this.searchtatPersistReport();
                });

                this.to_date.valueChanges.pipe().subscribe(value => {
                  this.tatPersist.tatReportSearchHistory.ordered_end_date = value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null;
                  this.searchtatPersistReport();
                });
                 }

  ngOnInit(): void {

    this.tatReportSort.sortChange.subscribe(() => {
      this.tatReportPaginator.pageIndex = 0;
      this.searchtatPersistReport(true);
    });

    this.tatReportPaginator.page.subscribe(() => {
      this.searchtatPersistReport(true);
    });
    //start by loading items
    this.searchtatPersistReport();
  }

  ngOnDestroy(): void {
    // this.tatPersist.orderedStartDate = null;
    // this.tatPersist.orderedEndDate = null;
    // this.tatPersist.ordersSearchText = "";
  }

  searchtatPersistReport(isPagination:boolean = false): void {

    let paginator = this.tatReportPaginator;
    let sorter = this.tatReportSort;

    this.tatReportIsLoading = true;
    this.tatReportselection = [];

    this.tatPersist.orderssDo("tat_report", {lab_test_id:  this.lab_test_id, lab_panel_id: this.lab_panel_id, order_type: this.order_type}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: {data: TatRportModel[]; total: number}) => {
      this.tatReportData = partialList.data;
      this.tatReportData.forEach(
        (tatReport) => {
          if (!this.lab_tests[tatReport.lab_test_id]){
            this.lab_tests[tatReport.lab_test_id] = new Lab_TestDetail()
          this.labTestPersist.getLab_Test(tatReport.lab_test_id).subscribe(
            lab_test => {
              this.lab_tests[tatReport.lab_test_id] = lab_test
            }
          )
        }
      }
      )
      if (partialList.total != -1) {
        this.tatReportTotalCount = partialList.total;
      }
      this.tatReportIsLoading = false;
    }, error => {
      this.tatReportIsLoading = false;
    });
  }

  downloadtatReport(): void {
    if(this.tatReportselectAll){
         this.tatPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download tat-report", true);
      });
    }
    else{
        this.tatPersist.download(this.tcUtilsArray.idsList(this.tatReportselection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download tat-report",true);
            });
        }
  }

  getTestName(id: string): string {
    return this.lab_tests[id]?.name || ""
  }

  searchTest(): void {
    let dialogRef = this.labTestNavigator.pickLab_Tests(true, null, this.order_type)
    dialogRef.afterClosed().subscribe(
      (result) => {
        if(result){
          this.lab_test_name = result[0].name
          this.lab_test_id = result[0].id
          this.searchtatPersistReport()
        }
      }
    )
  }

  zeroPad = (num, places) => String(num).padStart(places, '0')

  getDateFormat(time: number) : string {
    // if (!element.result_date){
    //   return "";
    // }
    // const diff = element.result_date - element.future_date
    // const test = this.lab_tests[element.lab_test_id]
    // if (test && test.tat_time && test.tat_time >= diff){
    //   element.style = 'green'
    // }
    // else if (test && test.tat_time && test.tat_time < diff){
    //   element.style = 'red'
    // } else {
    //   element.style = 'black'
    // }
    if (time == null || time <= 0){
      return ""
    }
    const day = Math.floor(time / (24 * 3600))
    const dayRem = time % (24 * 3600)
    const hour = Math.floor(dayRem / 3600)
    const hourRem = dayRem % 3600
    const min = Math.floor(hourRem / 60)
    return `${this.zeroPad(day, 2)}:${this.zeroPad(hour, 2)}:${this.zeroPad(min, 2)}`
  }

  searchPanel(): void {
    let dialogRef = this.labPanelNavigator.pickLab_Panels(true, this.order_type)
    dialogRef.afterClosed().subscribe(
      (result) => {
        if(result){
          this.lab_panel_name = result[0].name
          this.lab_panel_id = result[0].id
          this.searchtatPersistReport()
        }
      }
    )
  }

  back():void{
    this.location.back();
  }

}
