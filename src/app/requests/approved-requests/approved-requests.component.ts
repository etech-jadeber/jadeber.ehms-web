import { Component, OnInit, ViewChild } from '@angular/core';
import { TCAuthorization } from '../../tc/authorization';
import { AppTranslation } from '../../app.translation';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RequestSummary, RequestSummaryPartialList } from '../request.model';
import { RequestPersist } from '../request.persist';
import {TCUtilsArray} from "../../tc/utils-array";
import { FormControl } from '@angular/forms';
import { ItemSummary } from 'src/app/items/item.model';
import { StoreSummary } from 'src/app/stores/store.model';
import { request_status } from 'src/app/app.enums';
import { EmployeeSummary } from 'src/app/storeemployees/employee.model';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { EmployeeNavigator } from 'src/app/storeemployees/employee.navigator';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { StorePersist } from 'src/app/stores/store.persist';
import {
  EmployeeDetail,
  EmployeeSummaryPartialList,
} from '../../storeemployees/employee.model';
import { EmployeePersist } from '../../storeemployees/employee.persist';
import { ItemPersist } from 'src/app/items/item.persist';

@Component({
  selector: 'app-approved-requests',
  templateUrl: './approved-requests.component.html',
  styleUrls: ['./approved-requests.component.css'],
})
export class ApprovedRequestsComponent implements OnInit {
  constructor(
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public requestPersist: RequestPersist,
    public tcUtilsArray: TCUtilsArray,
    public itemNavigator: ItemNavigator,
    public storeNavigator: StoreNavigator,
    public employeeNavigator: EmployeeNavigator,
    public tcUtilsDate: TCUtilsDate,
    public employeePersist: EmployeePersist,
    public storePersist: StorePersist,
    public itemPersist: ItemPersist
  ) {}

  ngOnInit(): void {
  }
}