import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectReasonEditComponent } from './reject-reason-edit.component';

describe('RejectReasonEditComponent', () => {
  let component: RejectReasonEditComponent;
  let fixture: ComponentFixture<RejectReasonEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejectReasonEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectReasonEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
