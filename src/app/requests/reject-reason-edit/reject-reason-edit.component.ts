import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { RequestEditComponent } from '../request-edit/request-edit.component';
import { ReasonDetail, RequestDetail } from '../request.model';

@Component({
  selector: 'app-reject-reason-edit',
  templateUrl: './reject-reason-edit.component.html',
  styleUrls: ['./reject-reason-edit.component.scss']
})
export class RejectReasonEditComponent implements OnInit {

  isLoadingReason: boolean = false;
  title: string;
  reasonDetail: ReasonDetail;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<RequestEditComponent>,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("requests");
      this.title = this.appTranslation.getText("general", "new") + " Request";
      this.reasonDetail = new ReasonDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate("requests");
      this.title = this.appTranslation.getText("general", "edit") + " Request";
      this.reasonDetail = new ReasonDetail();
      this.reasonDetail.reason = this.idMode.id

    }
  }

  onAdd(): void {
    this.dialogRef.close(this.reasonDetail);
  }

  onUpdate(): void {
      this.dialogRef.close(this.reasonDetail);
  }


  canSubmit(): boolean {
    if (this.reasonDetail == null) {
      return false;
    }
    if (this.reasonDetail.reason == null || this.reasonDetail.reason == "") {
      return false;
    }

    return true;
  }

}
