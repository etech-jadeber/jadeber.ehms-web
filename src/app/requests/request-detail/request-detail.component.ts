import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {RequestDetail} from "../request.model";
import {RequestPersist} from "../request.persist";
import {RequestNavigator} from "../request.navigator";


export enum PaginatorIndexes {

}


@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  requestLoading:boolean = false;
  //basics
  requestDetail: RequestDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public requestNavigator: RequestNavigator,
              public  requestPersist: RequestPersist) {
    this.tcAuthorization.requireRead("requests");
    this.requestDetail = new RequestDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("requests");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.requestLoading = true;
    this.requestPersist.getRequest(id).subscribe(requestDetail => {
          this.requestDetail = requestDetail;
          this.requestLoading = false;
        }, error => {
          console.error(error);
          this.requestLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.requestDetail.id);
  }

  editRequest(): void {
    let modalRef = this.requestNavigator.editRequest(this.requestDetail.id);
    modalRef.afterClosed().subscribe(modifiedRequestDetail => {
      TCUtilsAngular.assign(this.requestDetail, modifiedRequestDetail);
    }, error => {
      console.error(error);
    });
  }

   printRequest():void{
      this.requestPersist.print(this.requestDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print request", true);
      });
    }

  back():void{
      this.location.back();
    }

}