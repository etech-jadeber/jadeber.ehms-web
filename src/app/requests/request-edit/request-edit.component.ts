import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {RequestDetail} from "../request.model";
import {RequestPersist} from "../request.persist";
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {request_status, units} from "../../app.enums"
import {ItemSummary} from 'src/app/items/item.model';
import {ItemNavigator} from 'src/app/items/item.navigator';
import {TCUtilsArray} from 'src/app/tc/utils-array';
import {StoreDetail, StoreSummary} from 'src/app/stores/store.model';
import {StorePersist} from 'src/app/stores/store.persist';
import {StoreSpecialNavigator} from 'src/app/stores/store.specialnavigator';
import {EmployeeNavigator} from "../../storeemployees/employee.navigator";
import {EmployeeSummary} from "../../storeemployees/employee.model";
import { ItemPersist } from 'src/app/items/item.persist';

@Component({
  selector: 'app-request-edit',
  templateUrl: './request-edit.component.html',
  styleUrls: ['./request-edit.component.css']
})
export class RequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  requestDetail: RequestDetail;
  request_day: Date = new Date();
  requestDetailList: RequestDetail[]=[]

  requestsDisplayedColumns: string[] = [
    'edit',
    'action',
    'item_id',
    'quantity',
    'unit',
  ];
  itemList: ItemSummary[] = [];
  storeList: StoreSummary[] = [];
  storeDetail: StoreDetail;
  requesterStoreName: string = "";
  requesteeStoreName: string = "";
  employeeFullName: string = "";
  itemName: string;
  permitted_quantity: number;

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public itemPersist: ItemPersist,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<RequestEditComponent>,
              public persist: RequestPersist,
              public storePersist: StorePersist,
              public tcUtilsDate: TCUtilsDate,
              public storeNavigator: StoreSpecialNavigator,
              public itemNavigator: ItemNavigator,
              public employeeNavigator: EmployeeNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("requests");
      this.title = this.appTranslation.getText("general", "new") + " Request";
      this.requestDetail = new RequestDetail();
      this.requestDetail.request_unit = units.piece;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate("requests");
      this.title = this.appTranslation.getText("general", "edit") + " Request";
      this.isLoadingResults = true;

      this.persist.getRequest(this.idMode.id).subscribe(requestDetail => {
        this.requestDetail = requestDetail;
        this.itemPersist.getItem(requestDetail.item_id).subscribe((item)=>{
          if(item)
            this.requestDetail.item_name = this.itemNavigator.itemName(item)
        })
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    for(let requestDetail of this.requestDetailList){
      this.persist.addRequest(requestDetail).subscribe(value => {
        this.tcNotification.success("Request dispatch case added");
        this.requestDetail.id = value.id
        this.dialogRef.close(this.requestDetail);
      }, error => {
        this.isLoadingResults = false;
      })
    }
    
  }

  removeFromList(item:RequestDetail):void{
    this.requestDetailList=this.requestDetailList.filter((value)=> value != item);
  }
  editFromList(item:RequestDetail){
    this.removeFromList(item);
    this.requestDetail=item;
  }


  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateRequest(this.requestDetail).subscribe(value => {
      this.tcNotification.success("Request updated");
      this.dialogRef.close(this.requestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.requestDetail == null) {
      return false;
    }
    if (this.requestDetail.item_id == null) {
      return false;
    }
    if (this.requestDetail.quantity == null) {
      return false;
    }
    if (this.requestDetail.request_unit == null) {
      return false;
    }

    return true;
  }

  getItem(itemId: string): string {
    let item = this.tcUtilsArray.getById(
      this.itemList,
      itemId
    );
    if (item) {
      return item.item_type;
    }
  }
  AddToList():void{
    this.requestDetailList=[this.requestDetail,...this.requestDetailList];
    this.requestDetail=new RequestDetail();
    this.requestDetail.request_unit = units.piece;
  }


  searchRequesteeStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.requesteeStoreName = result[0].name;
        this.requestDetail.requestee_store_id = result[0].id;
      }
    });
  }

  searchStore() {
    let dialogRef = this.itemNavigator.pickItems(true);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.requestDetail.item_name = this.itemNavigator.itemName(result[0])
        this.requestDetail.item_id = result[0].id;
        // this.persist.requestsDo("permitted_amount",{item_id:this.requestDetail.item_id}).subscribe((value:any)=>{
        //   this.permitted_quantity = value.permitted_amount;
        //   this.requestDetail.quantity = value.permitted_amount;
        // })
      }
    });
  }

  searchEmployee() {
    let dialogRef = this.employeeNavigator.pickEmployees(true);
    dialogRef.afterClosed().subscribe((result: EmployeeSummary[]) => {
      if (result) {
        this.employeeFullName = `${result[0].first_name} ${result[0].last_name}`;
        this.requestDetail.requester_store_manager_id = result[0].id;
      }
    });
  }

}
