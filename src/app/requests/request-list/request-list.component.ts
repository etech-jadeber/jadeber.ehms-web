import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {RequestPersist} from "../request.persist";
import {RequestNavigator} from "../request.navigator";
import {RequestDetail, RequestSummary, RequestSummaryPartialList} from "../request.model";
import {request_status} from 'src/app/app.enums';
import {ItemPersist} from 'src/app/items/item.persist';
import {StorePersist} from 'src/app/stores/store.persist';

import {EmployeeDetail, EmployeeSummary, EmployeeSummaryPartialList} from "../../storeemployees/employee.model";
import {EmployeePersist} from "../../storeemployees/employee.persist";
import {EmployeeNavigator} from "../../storeemployees/employee.navigator";
import {StoreNavigator} from "../../stores/store.navigator";
import {ItemNavigator} from "../../items/item.navigator";
import {EmployeeSelfNavigator} from "../../storeemployees/employee-self.navigator";
import {EmployeeSelfPersist} from "../../storeemployees/employee-self.persist";
import {Item_In_StorePersist} from "../../item_in_stores/item_in_store.persist";
import { DispatchOrRejectNavigator } from 'src/app/dispatch_or_reject/dispatch_or_reject.navigator';
import {
  MatDialog,
} from '@angular/material/dialog';
import { DispatchOrRejectPersist } from 'src/app/dispatch_or_reject/dispatch_or_reject.persist';
import { TransferNavigator } from 'src/app/transfers/transfer.navigator';
import { PurchaseRequestPersist } from 'src/app/purchase_request/purchase_request.persist';
import { PurchaseRequestStatus } from 'src/app/app.enums';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { TransferPersist } from 'src/app/transfers/transfer.persist';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreSummary } from 'src/app/stores/store.model';
import { TCId } from 'src/app/tc/models';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css'],
})
export class RequestListComponent implements OnInit {
  requestsData: RequestSummary[] = [];
  purchaseRequestStatus= PurchaseRequestStatus;
  requestsTotalCount: number = 0;
  requestsSelectAll: boolean = false;
  requestsSelection: RequestSummary[] = [];
  item_batch: any = [];

  employees: EmployeeSummary[] = [];

  requestsDisplayedColumns: string[] = [
    'select',
    'action',
    'item_id',
    'quantity',
    'unit',
    'request_day',
    'ref_no',
    'status',
    'sent',
    'accepted',
    'rejected',
    'remark'
  ];
  requestsSearchTextBox: FormControl = new FormControl();
  refNoBox: FormControl = new FormControl();
  requestsIsLoading: boolean = false;
  request_status = request_status;
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
  to_from_store_name: string;

  isDispatch: boolean;
  animal: string;
  name: string;
  @ViewChild(MatPaginator, { static: true }) requestsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) requestsSort: MatSort;
  @Input() isRequested: boolean
  success_state: number= 1;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public requestPersist: RequestPersist,
    public requestNavigator: RequestNavigator,
    public employeeNavigator: EmployeeNavigator,
    public storeNavigator: StoreNavigator,
    public employeeSelfNavigator: EmployeeSelfNavigator,
    public itemNavigator: ItemNavigator,
    public categoryPersist: ItemCategoryPersist,
    public jobPersist: JobPersist,
    public itemPersist: ItemPersist,
    public employeePersist: EmployeePersist,
    public storePersist: StorePersist,
    public item_In_StorePersist: Item_In_StorePersist,
    public dispatchOrRejectNavigator: DispatchOrRejectNavigator,
    public dialog: MatDialog,
    public dispatchOrRejectPersist: DispatchOrRejectPersist,
    public transferNavigator: TransferNavigator,
    public purchaseRequestPersist:  PurchaseRequestPersist,
    public tcUtilsString: TCUtilsString,
    public categoryNavigator: ItemCategoryNavigator,
    public transferPersist: TransferPersist,
  ) {
    this.tcAuthorization.requireRead('requests');
    this.requestsSearchTextBox.setValue(requestPersist.requestSearchText);
    //delay subsequent keyup events
    this.requestsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.requestPersist.requestSearchText = value;
        this.searchRequests();
      });

      this.refNoBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.requestPersist.ref_no = value;
        this.searchRequests();
      });

      this.requestPersist.start_date.valueChanges.subscribe((value)=>{

        this.searchRequests()
    });
    this.requestPersist.end_date.valueChanges.subscribe((value)=>{
        this.searchRequests()
    });
  }

  ngOnInit() {
    this.requestsSort.sortChange.subscribe(() => {
      this.requestsPaginator.pageIndex = 0;
      this.searchRequests(true);
    });

    this.requestsPaginator.page.subscribe(() => {
      this.searchRequests(true);
    });

    if(this.isRequested){
      this.requestsDisplayedColumns = [...this.requestsDisplayedColumns.slice(0, 3), 'store', ...this.requestsDisplayedColumns.slice(3)]
    }
    //start by loading items
    this.searchRequests();
  }

  searchRequests(isPagination: boolean = false): void {
    let paginator = this.requestsPaginator;
    let sorter = this.requestsSort;

    this.requestsIsLoading = true;
    this.requestsSelection = [];

    this.requestPersist
      .searchRequest(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction,
        this.isRequested
      )
      .subscribe(
        (partialList: RequestSummaryPartialList) => {
          this.requestsData = partialList.data
          if (partialList.total != -1) {
            this.requestsTotalCount = partialList.total;
          }
          this.requestsData.forEach(request=>{
            this.transferPersist.transfersDo("total_transfer",{"transfer_request_id":request.id}).subscribe((result:any)=>{
              if(result){
                request.remaining=+request.quantity-(+result.total_transfer)
                console.log(request.remaining)
              }
            })
          })
            
          this.requestsIsLoading = false;
        },
        (error) => {
          this.requestsIsLoading = false;
        }
      );
  }

  downloadRequests(): void {
    if (this.requestsSelectAll) {
      this.requestPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download requests', true);
      });
    } else {
      this.requestPersist
        .download(this.tcUtilsArray.idsList(this.requestsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'download requests', true);
        });
    }
  }

  addRequest(): void {
    let dialogRef = this.requestNavigator.addRequest();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchRequests();
      }
    });
  }

  editRequest(item: RequestSummary) {
    let dialogRef = this.requestNavigator.editRequest(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchRequests()
      }
    });
  }

  acceptAll():void{
    this.requestsSelection = this.requestsSelection.filter(value => value.status ==this.purchaseRequestStatus.submitted);
    for( let request of this.requestsSelection){
      this.acceptRequest(request);
    }
  }

  acceptRequest(item: RequestSummary){
    this.requestPersist.requestDo(item.id, "accept", {}).subscribe(
      result => {
          this.tcNotification.success("The request is accepted")
          this.searchRequests();
      }
    )
  }
  deleteRequest(item: RequestSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Request');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.requestPersist.deleteRequest(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Request deleted');
            this.searchRequests();
          },
          (error) => {}
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }

  getEmployeeFullName(employeeId: string) {
    const employee: EmployeeDetail = this.tcUtilsArray.getById(
      this.employees,
      employeeId
    );
    if (employee) {
      return `${employee.first_name} ${employee.last_name}`;
    }
  }

  dispatchRequest(request: RequestDetail) {


    let dialogRef = this.transferNavigator.addTransfer(
      request.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchRequests();
      }
    });

  }

  firstPrint():void {
    if(!this.requestPersist.to_from_store){
      this.tcNotification.error("please select store from")
      return;
    }  if(this.requestPersist.request_statusId != this.purchaseRequestStatus.dispatched){
      this.tcNotification.error("please select dispatched status")
      return;
    }
    this.requestsSelection = this.requestsSelection.filter(value => value.ref_no == null  && value.status == this.purchaseRequestStatus.dispatched)
    if (this.requestsSelection.length)  
      this.printTransfers()
    else 
      this.tcNotification.error("no selected request without Ref No.")
  }

  secondPrint():void {
    if(!this.requestPersist.ref_no){
      this.tcNotification.error("please enter the refernce no")
      return;
    }
    this.requestsSelection = this.requestsSelection.filter(value => value.ref_no == this.requestPersist.ref_no && value.status == this.purchaseRequestStatus.dispatched)
    if (this.requestsSelection.length)  
      this.printTransfers()
    else 
      this.tcNotification.error("no selected request with Ref No.")
  }

  printTransfers():void {
    let ids = ""
    this.requestsSelection.forEach((value, idx)=>{
      ids += ids =="" ? value.id : ","+ value.id;
    })

    let params = {ids : ids,ref_no: this.requestPersist.ref_no, start_date: this.requestPersist.start_date.value && (new Date(this.requestPersist.start_date.value).getTime()/1000).toString(), end_date: this.requestPersist.end_date.value && (new Date(this.requestPersist.end_date.value).getTime()/1000).toString(), store_id : this.requestPersist.to_from_store }
    this.requestPersist.requestsDo("print_request",params).subscribe((downloadJob: TCId) => {
      this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
        this.jobPersist.followJob( downloadJob.id,'printing request', true);
        if (job.job_state == this.success_state) {
        }
      });
    });
  }


  searchStore(is_to_from_store: boolean = false) {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        if (is_to_from_store){
          this.requestPersist.to_from_store = result[0].id
          this.to_from_store_name = result[0].name
        } else {
          this.requestPersist.store_name = result[0].name;
          this.requestPersist.store_id = result[0].id;
        }
        this.searchRequests();
      }
    });
  }

  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.requestPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.requestPersist.categoryId = result[0].id;
      }
      this.searchRequests()
    })
  }

  rejectRequest(item: RequestDetail) {

    let dialogRef = this.requestNavigator.addReason();
    dialogRef.afterClosed().subscribe(
      result => {
        if (result){
          this.requestPersist.requestDo(item.id, 'reject', {reject_description: result.reason}).subscribe(
            result => {
              this.tcNotification.success("You have rejected successfully")
              this.searchRequests()
            }
          )
        }
      }
    )
  }
}