import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RequestPickComponent } from './request-pick.component';

describe('RequestPickComponent', () => {
  let component: RequestPickComponent;
  let fixture: ComponentFixture<RequestPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
