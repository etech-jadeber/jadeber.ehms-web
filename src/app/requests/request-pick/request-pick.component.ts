import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {RequestDetail, RequestSummary, RequestSummaryPartialList} from "../request.model";
import {RequestPersist} from "../request.persist";


@Component({
  selector: 'app-request-pick',
  templateUrl: './request-pick.component.html',
  styleUrls: ['./request-pick.component.css']
})
export class RequestPickComponent implements OnInit {

  requestsData: RequestSummary[] = [];
  requestsTotalCount: number = 0;
  requestsSelection: RequestSummary[] = [];
  requestsDisplayedColumns: string[] = ["select", 'item_type','item_id','unit_id','quantity','requester_store_manager_id','requester_store_id','requestee_store_id','request_day','status' ];

  requestsSearchTextBox: FormControl = new FormControl();
  requestsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) requestsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) requestsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public requestPersist: RequestPersist,
              public dialogRef: MatDialogRef<RequestPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("requests");
    this.requestsSearchTextBox.setValue(requestPersist.requestSearchText);
    //delay subsequent keyup events
    this.requestsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.requestPersist.requestSearchText = value;
      this.searchRequests();
    });
  }

  ngOnInit() {

    this.requestsSort.sortChange.subscribe(() => {
      this.requestsPaginator.pageIndex = 0;
      this.searchRequests();
    });

    this.requestsPaginator.page.subscribe(() => {
      this.searchRequests();
    });

    //set initial picker list to 5
    this.requestsPaginator.pageSize = 5;

    //start by loading items
    this.searchRequests();
  }

  searchRequests(): void {
    this.requestsIsLoading = true;
    this.requestsSelection = [];

    this.requestPersist.searchRequest(this.requestsPaginator.pageSize,
        this.requestsPaginator.pageIndex,
        this.requestsSort.active,
        this.requestsSort.direction).subscribe((partialList: RequestSummaryPartialList) => {
      this.requestsData = partialList.data;
      if (partialList.total != -1) {
        this.requestsTotalCount = partialList.total;
      }
      this.requestsIsLoading = false;
    }, error => {
      this.requestsIsLoading = false;
    });

  }

  markOneItem(item: RequestSummary) {
    if(!this.tcUtilsArray.containsId(this.requestsSelection,item.id)){
          this.requestsSelection = [];
          this.requestsSelection.push(item);
        }
        else{
          this.requestsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.requestsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}