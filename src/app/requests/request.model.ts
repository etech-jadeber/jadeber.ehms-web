import { TCId } from "../tc/models";

export class RequestSummary extends TCId {
  id:string;
  item_id: string;
  request_unit: number;
  quantity: number;
  requester_store_manager_id: string;
  requester_store_id: string;
  requestee_store_id: string;
  request_day: number;
  reject_description: string;
  status: number;
  item_name: string;
  remaining: number;
  sent: number;
  accepted: number;
  rejected: number;
  ref_no: any;
}

export class ReasonDetail {
  reason: string;
}

export class RequestSummaryPartialList {
  data: RequestSummary[];
  total: number;
}

export class RequestDetail extends RequestSummary {
  item_type: number;
  item_id: string;
  unit_id: number;
  case_id:string;
  quantity: number;
  requester_store_manager_id: string;
  requester_store_id: string;
  requestee_store_id: string;
  request_day: number;
  status: number;
}

export class RequestDashboard {
  total: number;
}
