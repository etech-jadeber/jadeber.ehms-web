import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {RequestEditComponent} from "./request-edit/request-edit.component";
import {RequestPickComponent} from "./request-pick/request-pick.component";
import { RejectReasonEditComponent } from "./reject-reason-edit/reject-reason-edit.component";


@Injectable({
  providedIn: 'root'
})

export class RequestNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  requestsUrl(): string {
    return "/requests";
  }

  requestUrl(id: string): string {
    return "/requests/" + id;
  }

  viewRequests(): void {
    this.router.navigateByUrl(this.requestsUrl());
  }

  viewRequest(id: string): void {
    this.router.navigateByUrl(this.requestUrl(id));
  }

  editRequest(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(RequestEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addReason(id: string = ""): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(RejectReasonEditComponent, {
      data: new TCIdMode(id, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addRequest(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(RequestEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  addRequestStatus(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(RequestEditComponent, {
          data: new TCIdMode(id, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickRequests(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(RequestPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
