import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation, TCEnum} from "../tc/models";
import {RequestDashboard, RequestDetail, RequestSummaryPartialList} from "./request.model";
import { AppTranslation } from '../app.translation';

import {PurchaseRequestStatus, item_type, request_status, units} from "../app.enums";
import {TCUtilsString} from "../tc/utils-string";
import { FormControl } from "@angular/forms";


@Injectable({
  providedIn: 'root',
})
export class RequestPersist {
  start_date: FormControl = new FormControl({disabled: true, value: ""});
  end_date: FormControl = new FormControl({disabled: true, value: ""});
  requestSearchText: string = '';
  item_typeId: number = -1;
  request_statusId: number;
  unitId: number = -1;
  categoryId: string;
  category: number;
  store_name: string;
  store_id: string;
  to_from_store: string;
  ref_no: string;

  constructor(
    private http: HttpClient,
    public appTranslation: AppTranslation
  ) {}

  PurchaseRequestStatus: TCEnum[] = [
    new TCEnum( PurchaseRequestStatus.submitted, 'submitted'),
    new TCEnum( PurchaseRequestStatus.accepted, 'accepted'),
    new TCEnum( PurchaseRequestStatus.rejected, 'rejected'),
    // new TCEnum( PurchaseRequestStatus.onprogress, 'on progress'),
    new TCEnum( PurchaseRequestStatus.dispatched, 'dispatched'),
    ];

  item_type: TCEnumTranslation[] = [
    new TCEnumTranslation(
      item_type.drug,
      this.appTranslation.getKey('inventory', 'drug')
    ),
    new TCEnumTranslation(
      item_type.equipment,
      this.appTranslation.getKey('inventory', 'equipment')
    ),
    new TCEnumTranslation(
      item_type.consumable,
      this.appTranslation.getKey('inventory', 'consumable')
    ),
  ];

  request_status: TCEnumTranslation[] = [
    new TCEnumTranslation(
      request_status.firstrejected,
      this.appTranslation.getKey('general', 'rejected')
    ),
    new TCEnumTranslation(
      request_status.secondrejected,
      this.appTranslation.getKey('general', 'rejected')
    ),

    new TCEnumTranslation(
      request_status.submitted,
      this.appTranslation.getKey('general', 'submitted')
    ),
    new TCEnumTranslation(
      request_status.approved,
      this.appTranslation.getKey('general', 'approved')
    ),
    new TCEnumTranslation(
      request_status.dispatched,
      this.appTranslation.getKey('inventory', 'dispatched')
    ),
  ];

  units: TCEnumTranslation[] = [
    new TCEnumTranslation(
      units.piece,
      this.appTranslation.getKey('inventory', 'piece')
    ),
    new TCEnumTranslation(
      units.box,
      this.appTranslation.getKey('inventory', 'box')
    ),
    new TCEnumTranslation(
      units.pack,
      this.appTranslation.getKey('inventory', 'pack')
    ),
    new TCEnumTranslation(
      units.vial,
      this.appTranslation.getKey('inventory', 'vial')
    ),
  ];

  searchRequest(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string,
    isRequested: boolean = true
  ): Observable<RequestSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'requests',
      this.requestSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    url = TCUtilsString.appendUrlParameter(url, 'isRequested', `${isRequested}`)
    if (this.request_statusId) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'status',
        this.request_statusId.toString()
      );
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if(this.to_from_store){
      url = TCUtilsString.appendUrlParameter(url, "to_from_store", this.to_from_store)
    }
    if (this.item_typeId) {
      url = TCUtilsString.appendUrlParameter(
        url,
        'item_type',
        this.item_typeId.toString()
      );
    }
    if (this.start_date.value){
      url = TCUtilsString.appendUrlParameter(url, "start_date", (new Date(this.start_date.value).getTime()/1000).toString())
    }
    if (this.end_date.value){
      url = TCUtilsString.appendUrlParameter(url, "end_date", (new Date(this.end_date.value).getTime()/1000).toString())
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if(this.ref_no){
      url = TCUtilsString.appendUrlParameter(url, "ref_no", this.ref_no);
    }
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
    return this.http.get<RequestSummaryPartialList>(url);
  }

  filters(): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = this.requestSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'requests/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  requestDashboard(): Observable<RequestDashboard> {
    return this.http.get<RequestDashboard>(
      environment.tcApiBaseUri + 'requests/dashboard'
    );
  }

  getRequest(id: string): Observable<RequestDetail> {
    return this.http.get<RequestDetail>(
      environment.tcApiBaseUri + 'requests/' + id
    );
  }

  addRequest(item: RequestDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'requests/',
      item
    );
  }

  updateRequest(item: RequestDetail): Observable<RequestDetail> {
    return this.http.patch<RequestDetail>(
      environment.tcApiBaseUri + 'requests/' + item.id,
      item
    );
  }

  deleteRequest(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'requests/' + id);
  }

  requestsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'requests/do',
      new TCDoParam(method, payload)
    );
  }

  requestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'requests/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'requests/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'requests/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
