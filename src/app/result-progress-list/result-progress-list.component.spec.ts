import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultProgressListComponent } from './result-progress-list.component';

describe('ResultProgressListComponent', () => {
  let component: ResultProgressListComponent;
  let fixture: ComponentFixture<ResultProgressListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultProgressListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultProgressListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
