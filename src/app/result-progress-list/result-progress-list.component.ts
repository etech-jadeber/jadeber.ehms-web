import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../tc/authorization";
import {TCUtilsAngular} from "../tc/utils-angular";
import {TCUtilsArray} from "../tc/utils-array";
import {TCUtilsDate} from "../tc/utils-date";
import {TCNotification} from "../tc/notification";
import {TCNavigator} from "../tc/navigator";
import {JobPersist} from "../tc/jobs/job.persist";

import {AppTranslation} from "../app.translation";
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { lab_order_type, OrderStatus, selected_orders_status } from 'src/app/app.enums';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { ResultProgressDetail, ResultProgressPartialList } from '../form_encounters/orderss/lab_results/lab_result.model';
import { Lab_ResultPersist } from '../form_encounters/orderss/lab_results/lab_result.persist';
import { OrdersPersist } from '../form_encounters/orderss/orders.persist';

@Component({
  selector: 'app-result-progress-list',
  templateUrl: './result-progress-list.component.html',
  styleUrls: ['./result-progress-list.component.scss']
})
export class ResultProgressListComponent implements OnInit {
  resultProgressData: ResultProgressDetail[] = [];
  resultProgressTotalCount: number = 0;
  resultProgressSelectAll:boolean = false;
  resultProgressSelection: ResultProgressDetail[] = [];
  resultProgressDisplayedColumns: string[] = ["pid", "patient_name", "generated_sample_id", "created_at", "result_date", "total_order", "completed", "rejected", "requested" ];
  resultProgressSearchTextBox: FormControl = new FormControl();
  resultProgressIsLoading: boolean = false;
  intervalId
  startDateTextField: FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  endDateTextField: FormControl = new FormControl({disabled: true, value: null});
  resultDateTextField: FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});


  @ViewChild(MatPaginator, {static: true}) resultProgressPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) resultProgressSort: MatSort;

  constructor(
    private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
                public labPersist: OrdersPersist,
                public patientPersist: PatientPersist,
                public userPersist: UserPersist,
                public orderssPersist: OrdersPersist,
  ) {
    this.tcAuthorization.requireRead("result_progress");
       this.resultProgressSearchTextBox.setValue(orderssPersist.orderSearchHistory.search_text);
      //delay subsequent keyup events
      this.resultProgressSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.orderssPersist.orderSearchHistory.search_text = value;
        this.searchResultProgress();
      });
    
      this.resultDateTextField.valueChanges.subscribe(value => {
        this.orderssPersist.orderSearchHistory.result_date = value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        this.searchResultProgress()
      })
      this.startDateTextField.valueChanges.subscribe(value => {
        this.orderssPersist.orderSearchHistory.ordered_start_date = value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        this.searchResultProgress()
      })
      this.endDateTextField.valueChanges.subscribe(value => {
        this.orderssPersist.orderSearchHistory.ordered_end_date = value._d ? this.tcUtilsDate.toTimeStamp(value._d) : null
        this.searchResultProgress()
      })
      this.orderssPersist.orderSearchHistory.order_status = OrderStatus.completed;
   }

  ngOnInit(): void {
    this.orderssPersist.orderSearchHistory.result_date = this.tcUtilsDate.toTimeStamp(new Date())
    this.resultProgressSort.sortChange.subscribe(() => {
      this.resultProgressPaginator.pageIndex = 0;
      this.searchResultProgress(true);
    });

    this.resultProgressPaginator.page.subscribe(() => {
      this.searchResultProgress(true);
    });
    //start by loading items
    // this.searchResultProgress();
    this.intervalId = setInterval(this.searchResultProgress.bind(this), 60000);
  }

  ngOnDestroy(): void {
    // this.orderssPersist.resultDate = null
    // this.orderssPersist.orderedStartDate = null;
    // this.orderssPersist.orderedEndDate = null;
    // this.orderssPersist.show_only_mine = false;
    clearInterval(this.intervalId);
  }

  searchResultProgress(isPagination:boolean = false): void {


    let paginator = this.resultProgressPaginator;
    let sorter = this.resultProgressSort;

    this.resultProgressIsLoading = true;
    this.resultProgressSelection = [];

    this.orderssPersist.orderssDo("get_all_order", {}, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: ResultProgressPartialList) => {
      this.resultProgressData = partialList.data;
      if (partialList.total != -1) {
        this.resultProgressTotalCount = partialList.total;
      }
      this.resultProgressIsLoading = false;
    }, error => {
      this.resultProgressIsLoading = false;
    });

  }

  downloadResultCollection(): void {
    if(this.resultProgressSelectAll){
         this.orderssPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download result progress", true);
      });
    }
    else{
        this.orderssPersist.download(this.tcUtilsArray.idsList(this.resultProgressSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download result progress",true);
            });
        }
  }

  back():void{
      this.location.back();
    }

}
