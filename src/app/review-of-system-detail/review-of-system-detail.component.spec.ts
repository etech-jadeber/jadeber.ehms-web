import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewOfSystemDetailComponent } from './review-of-system-detail.component';

describe('ReviewOfSystemDetailComponent', () => {
  let component: ReviewOfSystemDetailComponent;
  let fixture: ComponentFixture<ReviewOfSystemDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewOfSystemDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewOfSystemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
