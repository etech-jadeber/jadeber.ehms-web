import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTranslation } from '../app.translation';
import { TCAuthorization } from '../tc/authorization';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsAngular } from '../tc/utils-angular';
import { TCUtilsArray } from '../tc/utils-array';
import {Location} from '@angular/common';
import { Subscription } from 'rxjs';
import { Review_Of_SystemDetail } from '../form_encounters/form_encounter.model';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';

@Component({
  selector: 'app-review-of-system-detail',
  templateUrl: './review-of-system-detail.component.html',
  styleUrls: ['./review-of-system-detail.component.scss']
})
export class ReviewOfSystemDetailComponent implements OnInit {

  paramsSubscription: Subscription;
  review_of_systemLoading:boolean = false;
  //basics
  review_of_systemDetail: Review_Of_SystemDetail;
  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization:TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator:TCNavigator,
    public jobPersist:JobPersist,
    public appTranslation:AppTranslation,
    public review_of_systemNavigator: Form_EncounterNavigator,
    public  review_of_systemPersist: Form_EncounterPersist) {
      this.tcAuthorization.requireRead("review_of_systems");
    this.review_of_systemDetail = new Review_Of_SystemDetail();
     }

  ngOnInit() {
    this.tcAuthorization.requireRead("review_of_systems");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.review_of_systemLoading = true;
    this.review_of_systemPersist.getReview_Of_System(id).subscribe(review_of_systemDetail => {
          this.review_of_systemDetail = review_of_systemDetail;
          this.review_of_systemLoading = false;
        }, error => {
          console.error(error);
          this.review_of_systemLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.review_of_systemDetail.id);
  }

  editReview_Of_System(): void {
    let modalRef = this.review_of_systemNavigator.editReview_Of_System(this.review_of_systemDetail.id);
    modalRef.afterClosed().subscribe(modifiedReview_Of_SystemDetail => {
      TCUtilsAngular.assign(this.review_of_systemDetail, modifiedReview_Of_SystemDetail);
    }, error => {
      console.error(error);
    });
  }

   printReview_Of_System():void{
      this.review_of_systemPersist.print(this.review_of_systemDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print review_of_system", true);
      });
    }

  back():void{
      this.location.back();
    }

}
