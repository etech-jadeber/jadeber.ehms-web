import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewOfSystemListComponent } from './review-of-system-list.component';

describe('ReviewOfSystemListComponent', () => {
  let component: ReviewOfSystemListComponent;
  let fixture: ComponentFixture<ReviewOfSystemListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewOfSystemListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewOfSystemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
