import { Review_Of_SystemSummary, Review_Of_SystemSummaryPartialList } from '../form_encounters/form_encounter.model';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList } from '../form_encounters/form_encounter.model';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { TCUtilsAngular } from '../tc/utils-angular';
import { DoctorPersist } from '../doctors/doctor.persist';
import { D } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-review-of-system-list',
  templateUrl: './review-of-system-list.component.html',
  styleUrls: ['./review-of-system-list.component.css']
})
export class ReviewOfSystemListComponent implements OnInit {
  review_of_systemsIsLoading: boolean = false;
  review_of_systemsSelection: Review_Of_SystemSummary[] = [];
  review_of_systemsData: Review_Of_SystemSummary[] = [];
  review_of_systemsTotalCount: number = 0;
  review_of_systemsSelectAll: boolean = false;
  review_of_systemsSearchTextBox: FormControl = new FormControl();
  review_of_systemsDisplayedColumns: string[] = [
    'select',
    'date_of_review',
    'provider_id',
    'general',
    'head_and_neck',
    'ent',
    'eye',
    'chest_lungs_breasts',
    'cardiovascular',
    'gastrointestinal',
    'musculoskeletal',
    'neurological'
  ];

  @Input() encounterId: any;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;


  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public doctorPersist: DoctorPersist,
  ) { 

    this.review_of_systemsSearchTextBox.setValue(
      form_encounterPersist.review_of_systemSearchText
    );
    this.review_of_systemsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.review_of_systemSearchText = value.trim();
        this.searchReview_Of_Systems();
      });
  }

  ngOnInit(): void {
    this.searchReview_Of_Systems();
  }

    // review_of_system
    searchReview_Of_Systems(isPagination: boolean = false): void {
      let paginator =
        this.paginator;
      let sorter = this.sorter;
  
      this.review_of_systemsIsLoading = true;
      this.review_of_systemsSelection = [];
  
      this.form_encounterPersist
        .searchReview_Of_System(
          this.encounterId,
          paginator.pageSize,
          isPagination ? paginator.pageIndex : 0,
          sorter.active,
          sorter.direction
        )
        .subscribe(
          (partialList: Review_Of_SystemSummaryPartialList) => {
            this.review_of_systemsData = partialList.data;
            this.review_of_systemsData.forEach(
              data => {
                this.doctorPersist.getDoctor(data.provider_id).subscribe(doctor => {
                  data.provider_name = doctor.first_name + " " + doctor.middle_name + " " + doctor.last_name
                })
              }
            )
            if (partialList.total != -1) {
              this.review_of_systemsTotalCount = partialList.total;
            }
            this.review_of_systemsIsLoading = false;
          },
          (error) => {
            this.review_of_systemsIsLoading = false;
          }
        );
    }
  
    downloadReview_Of_Systems(): void {
      if (this.review_of_systemsSelectAll) {
        this.form_encounterPersist.downloadAll().subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download  review_of_systems',
            true
          );
        });
      } else {
        this.form_encounterPersist
          .download(this.tcUtilsArray.idsList(this.review_of_systemsSelection))
          .subscribe((downloadJob) => {
            this.jobPersist.followJob(
              downloadJob.id,
              'download  review_of_systems',
              true
            );
          });
      }
    }
  
    addReview_Of_System(): void {
      let dialogRef = this.form_encounterNavigator.addReview_Of_System(
        this.encounterId
      );
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.searchReview_Of_Systems();
        }
      });
    }
  
    editReview_Of_System() {
      let dialogRef = this.form_encounterNavigator.editReview_Of_System(this.review_of_systemsData[0].id);
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.searchReview_Of_Systems();
        }
      });
    }
  
    deleteReview_Of_System(item: Review_Of_SystemSummary): void {
      let dialogRef = this.tcNavigator.confirmDeletion(' Review_Of_System');
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          //do the deletion
          this.form_encounterPersist.deleteReview_Of_System(item.id).subscribe(
            (response) => {
              this.tcNotification.success(' Review_Of_System deleted');
              this.searchReview_Of_Systems();
            },
            (error) => {}
          );
        }
      });
    }

    back(): void {
      this.location.back();
    } 

}
