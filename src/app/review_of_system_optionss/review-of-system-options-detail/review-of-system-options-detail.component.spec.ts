import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Review_Of_System_OptionsDetailComponent } from './review-of-system-options-detail.component';

describe('ReviewOfSystemOptionsDetailComponent', () => {
  let component: Review_Of_System_OptionsDetailComponent;
  let fixture: ComponentFixture<Review_Of_System_OptionsDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Review_Of_System_OptionsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Review_Of_System_OptionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
