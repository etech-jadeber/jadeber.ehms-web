import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Review_Of_System_OptionsDetail} from "../review_of_system_options.model";
import {Review_Of_System_OptionsPersist} from "../review_of_system_options.persist";
import {Review_Of_System_OptionsNavigator} from "../review_of_system_options.navigator";

export enum Review_Of_System_OptionsTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-review-of-system-options-detail',
  templateUrl: './review-of-system-options-detail.component.html',
  styleUrls: ['./review-of-system-options-detail.component.css']
})
export class Review_Of_System_OptionsDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  review_of_system_optionsLoading:boolean = false;
  
  Review_Of_System_OptionsTabs: typeof Review_Of_System_OptionsTabs = Review_Of_System_OptionsTabs;
  activeTab: Review_Of_System_OptionsTabs = Review_Of_System_OptionsTabs.overview;
  //basics
  review_of_system_optionsDetail: Review_Of_System_OptionsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public review_of_system_optionsNavigator: Review_Of_System_OptionsNavigator,
              public  review_of_system_optionsPersist: Review_Of_System_OptionsPersist) {
    this.tcAuthorization.requireRead("review_of_system_options");
    this.review_of_system_optionsDetail = new Review_Of_System_OptionsDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("review_of_system_options");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.review_of_system_optionsLoading = true;
    this.review_of_system_optionsPersist.getReview_Of_System_Options(id).subscribe(review_of_system_optionsDetail => {
          this.review_of_system_optionsDetail = review_of_system_optionsDetail;
          this.review_of_system_optionsLoading = false;
        }, error => {
          console.error(error);
          this.review_of_system_optionsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.review_of_system_optionsDetail.id);
  }

  editReview_Of_System_Options(): void {
    let modalRef = this.review_of_system_optionsNavigator.editReview_Of_System_Options(this.review_of_system_optionsDetail.id);
    modalRef.afterClosed().subscribe(modifiedReview_Of_System_OptionsDetail => {
      TCUtilsAngular.assign(this.review_of_system_optionsDetail, modifiedReview_Of_System_OptionsDetail);
    }, error => {
      console.error(error);
    });
  }

   printReview_Of_System_Options():void{
      this.review_of_system_optionsPersist.print(this.review_of_system_optionsDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print review_of_system_options", true);
      });
    }

  back():void{
      this.location.back();
    }

}
