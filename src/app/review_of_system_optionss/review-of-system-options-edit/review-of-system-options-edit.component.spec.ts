import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Review_Of_System_OptionsEditComponent } from './review-of-system-options-edit.component';

describe('Review_Of_System_OptionsEditComponent', () => {
  let component: Review_Of_System_OptionsEditComponent;
  let fixture: ComponentFixture<Review_Of_System_OptionsEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Review_Of_System_OptionsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Review_Of_System_OptionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
