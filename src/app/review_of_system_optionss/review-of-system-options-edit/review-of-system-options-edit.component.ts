import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Review_Of_System_OptionsDetail} from "../review_of_system_options.model";
import {Review_Of_System_OptionsPersist} from "../review_of_system_options.persist";


@Component({
  selector: 'app-review-of-system-options-edit',
  templateUrl: './review-of-system-options-edit.component.html',
  styleUrls: ['./review-of-system-options-edit.component.css']
})
export class Review_Of_System_OptionsEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  review_of_system_optionsDetail: Review_Of_System_OptionsDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Review_Of_System_OptionsEditComponent>,
              public  persist: Review_Of_System_OptionsPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate(this.appTranslation.getText("patient", "review_of_system_options"));
      this.title = this.appTranslation.getText("general","new") +  " " +  this.appTranslation.getText("patient", "review_of_system_option");
      this.review_of_system_optionsDetail = new Review_Of_System_OptionsDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate(this.appTranslation.getText("patient", "review_of_system_options"));
      this.title = this.appTranslation.getText("general","edit") +  " " +  this.appTranslation.getText("patient", "review_of_system_option");
      this.isLoadingResults = true;
      this.persist.getReview_Of_System_Options(this.idMode.id).subscribe(review_of_system_optionsDetail => {
        this.review_of_system_optionsDetail = review_of_system_optionsDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addReview_Of_System_Options(this.review_of_system_optionsDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "review_of_system_option") + this.appTranslation.getText("general", "added"));
      this.review_of_system_optionsDetail.id = value.id;
      this.dialogRef.close(this.review_of_system_optionsDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateReview_Of_System_Options(this.review_of_system_optionsDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "review_of_system_option") + this.appTranslation.getText("general", "updated"));
      this.dialogRef.close(this.review_of_system_optionsDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.review_of_system_optionsDetail == null){
            return false;
          }

        if (this.review_of_system_optionsDetail.name == null || this.review_of_system_optionsDetail.name  == "") {
                      return false;
                    }

if (this.review_of_system_optionsDetail.type == null || this.review_of_system_optionsDetail.type  == "") {
                      return false;
                    }


        return true;
      }


}
