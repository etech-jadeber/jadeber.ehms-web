import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Review_Of_System_OptionsListComponent } from './review-of-system-options-list.component';

describe('Review_Of_System_OptionsListComponent', () => {
  let component: Review_Of_System_OptionsListComponent;
  let fixture: ComponentFixture<Review_Of_System_OptionsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Review_Of_System_OptionsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Review_Of_System_OptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
