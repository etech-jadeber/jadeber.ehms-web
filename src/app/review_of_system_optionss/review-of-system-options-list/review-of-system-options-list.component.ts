import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Review_Of_System_OptionsPersist} from "../review_of_system_options.persist";
import {Review_Of_System_OptionsNavigator} from "../review_of_system_options.navigator";
import {Review_Of_System_OptionsDetail, Review_Of_System_OptionsSummary, Review_Of_System_OptionsSummaryPartialList} from "../review_of_system_options.model";


@Component({
  selector: 'app-review-of-system-options-list',
  templateUrl: './review-of-system-options-list.component.html',
  styleUrls: ['./review-of-system-options-list.component.css']
})
export class Review_Of_System_OptionsListComponent implements OnInit {

  review_of_system_optionssData: Review_Of_System_OptionsSummary[] = [];
  review_of_system_optionssTotalCount: number = 0;
  review_of_system_optionssSelectAll:boolean = false;
  review_of_system_optionssSelection: Review_Of_System_OptionsSummary[] = [];

  review_of_system_optionssDisplayedColumns: string[] = ["select","action", "name","type", ];
  review_of_system_optionssSearchTextBox: FormControl = new FormControl();
  review_of_system_optionssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) review_of_system_optionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) review_of_system_optionssSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public review_of_system_optionsPersist: Review_Of_System_OptionsPersist,
                public review_of_system_optionsNavigator: Review_Of_System_OptionsNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("review_of_system_options");
       this.review_of_system_optionssSearchTextBox.setValue(review_of_system_optionsPersist.review_of_system_optionsSearchText);
      //delay subsequent keyup events
      this.review_of_system_optionssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.review_of_system_optionsPersist.review_of_system_optionsSearchText = value;
        this.searchReview_Of_System_Optionss();
      });
    }

    ngOnInit() {

      this.review_of_system_optionssSort.sortChange.subscribe(() => {
        this.review_of_system_optionssPaginator.pageIndex = 0;
        this.searchReview_Of_System_Optionss(true);
      });

      this.review_of_system_optionssPaginator.page.subscribe(() => {
        this.searchReview_Of_System_Optionss(true);
      });
      //start by loading items
      this.searchReview_Of_System_Optionss();
    }

  searchReview_Of_System_Optionss(isPagination:boolean = false): void {


    let paginator = this.review_of_system_optionssPaginator;
    let sorter = this.review_of_system_optionssSort;

    this.review_of_system_optionssIsLoading = true;
    this.review_of_system_optionssSelection = [];

    this.review_of_system_optionsPersist.searchReview_Of_System_Options(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Review_Of_System_OptionsSummaryPartialList) => {
      this.review_of_system_optionssData = partialList.data;
      if (partialList.total != -1) {
        this.review_of_system_optionssTotalCount = partialList.total;
      }
      this.review_of_system_optionssIsLoading = false;
    }, error => {
      this.review_of_system_optionssIsLoading = false;
    });

  }

  downloadReview_Of_System_Optionss(): void {
    if(this.review_of_system_optionssSelectAll){
         this.review_of_system_optionsPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download review_of_system_optionss", true);
      });
    }
    else{
        this.review_of_system_optionsPersist.download(this.tcUtilsArray.idsList(this.review_of_system_optionssSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download review_of_system_optionss",true);
            });
        }
  }



  addReview_Of_System_Options(): void {
    let dialogRef = this.review_of_system_optionsNavigator.addReview_Of_System_Options();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchReview_Of_System_Optionss();
      }
    });
  }

  editReview_Of_System_Options(item: Review_Of_System_OptionsSummary) {
    let dialogRef = this.review_of_system_optionsNavigator.editReview_Of_System_Options(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteReview_Of_System_Options(item: Review_Of_System_OptionsSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion(this.appTranslation.getText("patient", "review_of_system_option"));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.review_of_system_optionsPersist.deleteReview_Of_System_Options(item.id).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("patient", "review_of_system_option") + this.appTranslation.getText("general", "deleted"));
          this.searchReview_Of_System_Optionss();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}

