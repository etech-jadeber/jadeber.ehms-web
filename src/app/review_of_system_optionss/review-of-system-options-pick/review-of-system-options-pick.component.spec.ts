import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { Review_Of_System_OptionsPickComponent } from './review-of-system-options-pick.component';

describe('ReviewOfSystemOptionsPickComponent', () => {
  let component: Review_Of_System_OptionsPickComponent;
  let fixture: ComponentFixture<Review_Of_System_OptionsPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Review_Of_System_OptionsPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Review_Of_System_OptionsPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
