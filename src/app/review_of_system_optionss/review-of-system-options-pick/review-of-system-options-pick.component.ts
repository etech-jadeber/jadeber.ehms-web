import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Review_Of_System_OptionsDetail, Review_Of_System_OptionsSummary, Review_Of_System_OptionsSummaryPartialList} from "../review_of_system_options.model";
import {Review_Of_System_OptionsPersist} from "../review_of_system_options.persist";


@Component({
  selector: 'app-review-of-system-options-pick',
  templateUrl: './review-of-system-options-pick.component.html',
  styleUrls: ['./review-of-system-options-pick.component.css']
})
export class Review_Of_System_OptionsPickComponent implements OnInit {

  review_of_system_optionssData: Review_Of_System_OptionsSummary[] = [];
  review_of_system_optionssTotalCount: number = 0;
  review_of_system_optionssSelection: Review_Of_System_OptionsSummary[] = [];
  review_of_system_optionssDisplayedColumns: string[] = ["select", 'name','type' ];

  review_of_system_optionssSearchTextBox: FormControl = new FormControl();
  review_of_system_optionssIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) review_of_system_optionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) review_of_system_optionssSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public review_of_system_optionsPersist: Review_Of_System_OptionsPersist,
              public dialogRef: MatDialogRef<Review_Of_System_OptionsPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("review_of_system_options");
    this.review_of_system_optionssSearchTextBox.setValue(review_of_system_optionsPersist.review_of_system_optionsSearchText);
    //delay subsequent keyup events
    this.review_of_system_optionssSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.review_of_system_optionsPersist.review_of_system_optionsSearchText = value;
      this.searchReview_Of_System_Optionss();
    });
  }

  ngOnInit() {

    this.review_of_system_optionssSort.sortChange.subscribe(() => {
      this.review_of_system_optionssPaginator.pageIndex = 0;
      this.searchReview_Of_System_Optionss();
    });

    this.review_of_system_optionssPaginator.page.subscribe(() => {
      this.searchReview_Of_System_Optionss();
    });

    //set initial picker list to 5
    this.review_of_system_optionssPaginator.pageSize = 5;

    //start by loading items
    this.searchReview_Of_System_Optionss();
  }

  searchReview_Of_System_Optionss(): void {
    this.review_of_system_optionssIsLoading = true;
    this.review_of_system_optionssSelection = [];

    this.review_of_system_optionsPersist.searchReview_Of_System_Options(this.review_of_system_optionssPaginator.pageSize,
        this.review_of_system_optionssPaginator.pageIndex,
        this.review_of_system_optionssSort.active,
        this.review_of_system_optionssSort.direction).subscribe((partialList: Review_Of_System_OptionsSummaryPartialList) => {
      this.review_of_system_optionssData = partialList.data;
      if (partialList.total != -1) {
        this.review_of_system_optionssTotalCount = partialList.total;
      }
      this.review_of_system_optionssIsLoading = false;
    }, error => {
      this.review_of_system_optionssIsLoading = false;
    });

  }

  markOneItem(item: Review_Of_System_OptionsSummary) {
    if(!this.tcUtilsArray.containsId(this.review_of_system_optionssSelection,item.id)){
          this.review_of_system_optionssSelection = [];
          this.review_of_system_optionssSelection.push(item);
        }
        else{
          this.review_of_system_optionssSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.review_of_system_optionssSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}

