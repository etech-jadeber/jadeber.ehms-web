import {TCId} from "../tc/models";

export class Review_Of_System_OptionsSummary extends TCId {
  name : string;
type : string;
checked :boolean;
}

export class Review_Of_System_OptionsSummaryPartialList {
  data: Review_Of_System_OptionsSummary[];
  total: number;
}

export class Review_Of_System_OptionsDetail extends Review_Of_System_OptionsSummary {
  name : string;
type : string;
}

export class Review_Of_System_OptionsDashboard {
  total: number;
}
