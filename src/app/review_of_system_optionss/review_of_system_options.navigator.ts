import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Review_Of_System_OptionsEditComponent} from "./review-of-system-options-edit/review-of-system-options-edit.component";
import {Review_Of_System_OptionsPickComponent} from "./review-of-system-options-pick/review-of-system-options-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Review_Of_System_OptionsNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  review_of_system_optionssUrl(): string {
    return "/review_of_system_options";
  }

  review_of_system_optionsUrl(id: string): string {
    return "/review_of_system_options/" + id;
  }

  viewReview_Of_System_Optionss(): void {
    this.router.navigateByUrl(this.review_of_system_optionssUrl());
  }

  viewReview_Of_System_Options(id: string): void {
    this.router.navigateByUrl(this.review_of_system_optionsUrl(id));
  }

  editReview_Of_System_Options(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Review_Of_System_OptionsEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addReview_Of_System_Options(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Review_Of_System_OptionsEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickReview_Of_System_Optionss(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Review_Of_System_OptionsPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
