import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Review_Of_System_OptionsDashboard, Review_Of_System_OptionsDetail, Review_Of_System_OptionsSummaryPartialList} from "./review_of_system_options.model";


@Injectable({
  providedIn: 'root'
})
export class Review_Of_System_OptionsPersist {

  review_of_system_optionsSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchReview_Of_System_Options(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Review_Of_System_OptionsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("review_of_system_optionss", this.review_of_system_optionsSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Review_Of_System_OptionsSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.review_of_system_optionsSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "review_of_system_optionss/do", new TCDoParam("download_all", this.filters()));
  }

  review_of_system_optionsDashboard(): Observable<Review_Of_System_OptionsDashboard> {
    return this.http.get<Review_Of_System_OptionsDashboard>(environment.tcApiBaseUri + "review_of_system_optionss/dashboard");
  }

  getReview_Of_System_Options(id: string): Observable<Review_Of_System_OptionsDetail> {
    return this.http.get<Review_Of_System_OptionsDetail>(environment.tcApiBaseUri + "review_of_system_optionss/" + id);
  }

  addReview_Of_System_Options(item: Review_Of_System_OptionsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "review_of_system_optionss/", item);
  }

  updateReview_Of_System_Options(item: Review_Of_System_OptionsDetail): Observable<Review_Of_System_OptionsDetail> {
    return this.http.patch<Review_Of_System_OptionsDetail>(environment.tcApiBaseUri + "review_of_system_optionss/" + item.id, item);
  }

  deleteReview_Of_System_Options(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "review_of_system_optionss/" + id);
  }

  review_of_system_optionssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "review_of_system_optionss/do", new TCDoParam(method, payload));
  }

  review_of_system_optionsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "review_of_system_optionss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "review_of_system_optionss/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "review_of_system_optionss/" + id + "/do", new TCDoParam("print", {}));
  }


}
