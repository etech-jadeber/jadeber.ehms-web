import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorDetailComponent } from './risk-factor-detail.component';

describe('RiskFactorDetailComponent', () => {
  let component: RiskFactorDetailComponent;
  let fixture: ComponentFixture<RiskFactorDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
