import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import {MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {RiskFactorDetail} from "../risk_factor.model";
import {RiskFactorPersist} from "../risk_factor.persist";
import {RiskFactorNavigator} from "../risk_factor.navigator";

export enum RiskFactorTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-risk_factor-detail',
  templateUrl: './risk-factor-detail.component.html',
  styleUrls: ['./risk-factor-detail.component.scss']
})
export class RiskFactorDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  riskFactorIsLoading:boolean = false;
  
  RiskFactorTabs: typeof RiskFactorTabs = RiskFactorTabs;
  activeTab: RiskFactorTabs = RiskFactorTabs.overview;
  //basics
  riskFactorDetail: RiskFactorDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public riskFactorNavigator: RiskFactorNavigator,
              public  riskFactorPersist: RiskFactorPersist) {
    this.tcAuthorization.requireRead("risk_factor");
    this.riskFactorDetail = new RiskFactorDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("risk_factor");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.riskFactorIsLoading = true;
    this.riskFactorPersist.getRiskFactor(id).subscribe(riskFactorDetail => {
          this.riskFactorDetail = riskFactorDetail;
          this.riskFactorIsLoading = false;
        }, error => {
          console.error(error);
          this.riskFactorIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.riskFactorDetail.id);
  }
  editRiskFactor(): void {
    let modalRef = this.riskFactorNavigator.editRiskFactor(this.riskFactorDetail.id);
    modalRef.afterClosed().subscribe(riskFactorDetail => {
      TCUtilsAngular.assign(this.riskFactorDetail, riskFactorDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.riskFactorPersist.print(this.riskFactorDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print risk_factor", true);
      });
    }

  back():void{
      this.location.back();
    }

}