import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorEditComponent } from './risk-factor-edit.component';

describe('RiskFactorEditComponent', () => {
  let component: RiskFactorEditComponent;
  let fixture: ComponentFixture<RiskFactorEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
