import {Component, OnInit, Inject, Input, Injectable, EventEmitter, Output} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RiskFactorDetail, RiskFactorSummary } from '../risk_factor.model';import { RiskFactorPersist } from '../risk_factor.persist';
import { RiskFactorsOptionsSummary } from 'src/app/risk_factors_options/risk_factors_options.model';
import { RiskFactorsOptionsPersist } from 'src/app/risk_factors_options/risk_factors_options.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';

@Injectable()
abstract class RiskFactorEditMainComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  riskFactorDetail: RiskFactorDetail;
  riskFactorOptionsValue: string;
  riskFactorOptions: RiskFactorsOptionsSummary[];
  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: RiskFactorPersist,
              public form_encounterPersist: Form_EncounterPersist,
              public tcUtilsDate: TCUtilsDate,
              public riskFactorOptionsPersist: RiskFactorsOptionsPersist,
              public isDialog: Boolean,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

            this.loadRiskFactorOptions();

  }

  onCancel(): void {
  }

  action(risk_factorSummary : RiskFactorSummary){

  }
  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("risk_factor");
      this.title = this.appTranslation.getText("general","new") +  " " + "risk_factor";
      this.riskFactorDetail = new RiskFactorDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("risk_factor");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "risk_factor";
      this.isLoadingResults = true;
      this.persist.getRiskFactor(this.idMode.id).subscribe(riskFactorDetail => {
        this.riskFactorDetail = riskFactorDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addRiskFactor(this.riskFactorDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success("riskFactor added");
      this.riskFactorDetail.id = value.id;
      this.action(this.riskFactorDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateRiskFactor(this.riskFactorDetail).subscribe(value => {
      this.isDialog && this.tcNotification.success("risk_factor updated");
      this.action(this.riskFactorDetail);
      this.isLoadingResults = false;
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  appendValue():void{
    if(this.riskFactorOptionsValue)
    this.riskFactorDetail.risk_factor = this.riskFactorDetail.risk_factor ?  this.riskFactorDetail.risk_factor +  ", " + this.riskFactorOptionsValue :  this.riskFactorOptionsValue;
    this.riskFactorOptionsPersist.riskFactorsOptionsSearchText = "";
    this.loadRiskFactorOptions();
  }
  searchValue(event:any):void{
    this.riskFactorOptionsPersist.riskFactorsOptionsSearchText = event.term;
    this.loadRiskFactorOptions();
  }

  appendString(value: string):string{
    value = value.trim();
    if (!this.riskFactorDetail.risk_factor)
      return "\u2022  " + value;
    if(this.riskFactorDetail.risk_factor.endsWith("\n"))
      return this.riskFactorDetail.risk_factor + "\u2022  " + value;
    let x = this.riskFactorDetail.risk_factor.split('\u2022  ');
    x.pop();
    let y = x.join("\u2022  ")
    return  y ?  y+"\u2022  " +value : "\u2022  " + value;
  }
  handleInput(event: string, input) {
    if(event && event.length == 1 && !event.endsWith('\u2022'))
      input.value = "\u2022  " + event
    else if(event && !event.endsWith("\n") && event.match(/\n[^(\u2022)]/))
      input.value =event.replace(/\n[^(\u2022)]/g, '\n\u2022  '+ event.match(/\n[^(\u2022)]/)[0][1])
    if (event && !input.value.endsWith("\n"))
    this.riskFactorOptionsPersist.riskFactorsOptionsSearchText =input.value.split('\u2022').pop().trim();
    else 
      this.riskFactorOptionsPersist.riskFactorsOptionsSearchText = "";
    this.loadRiskFactorOptions();
  }

  
  loadRiskFactorOptions():void{
      this.riskFactorOptionsPersist.searchRiskFactorsOptions(5,0,"name","asc").subscribe((value)=>{
        if(value){
            this.riskFactorOptions = value.data;
        }
      })
  }
  
  canSubmit():boolean{
        if (this.riskFactorDetail == null){
            return false;
          }

        if (this.riskFactorDetail.risk_factor == null || this.riskFactorDetail.risk_factor  == "") {
                    return false;
                } 
        return true;

 }
 }

 @Component({
  selector: 'app-risk_factor-edit',
  templateUrl: './risk-factor-edit.component.html',
  styleUrls: ['./risk-factor-edit.component.scss']
})
export class RiskFactorEditComponent extends RiskFactorEditMainComponent {
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<RiskFactorEditComponent>,
              public  persist: RiskFactorPersist,
              public form_encounter_persisit: Form_EncounterPersist,
              public tcUtilsDate: TCUtilsDate,
              public riskFactorOptionsPersist: RiskFactorsOptionsPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist, form_encounter_persisit, tcUtilsDate,riskFactorOptionsPersist,true,idMode)

  }

  onCancel(): void {
    this.dialogRef.close();
  }
  action(risk_factorSummary: RiskFactorSummary): void {
    this.tcNotification.success("riskFactor added");
    this.dialogRef.close(risk_factorSummary);
  }
}

@Component({
  selector: 'app-risk_factor-edit-list',
  templateUrl: './risk-factor-edit.component.html',
  styleUrls: ['./risk-factor-edit.component.scss']
})
export class RiskFactorEditListComponent extends RiskFactorEditMainComponent {


  @Input() encounterId :any
  @Output() assign_value = new EventEmitter<any>();

  dialogRef = null;
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public  persist: RiskFactorPersist,
              public form_encounter_persisit: Form_EncounterPersist,
              public tcUtilsDate: TCUtilsDate,
              public riskFactorOptionsPersist: RiskFactorsOptionsPersist,
              ) {
                super(tcAuthorization,tcNotification,tcUtilsString,appTranslation,persist, form_encounter_persisit,tcUtilsDate, riskFactorOptionsPersist,false,{id:null, mode: TCModalModes.NEW})

  }
  ngOnInit(): void {
    this.persist.searchRiskFactor(this.encounterId,1, 0, 'date', 'desc').subscribe(
      risk_factor => {
        if(risk_factor.data.length){
          this.riskFactorDetail =  risk_factor.data[0] 
        }
        else{
          this.assign_value.emit({new_risk_factor: true })
          this.riskFactorDetail = new RiskFactorDetail()
        } 
      }
    )
  }

  onCancel(): void {
  }
  saveResult(){
    if (!this.canSubmit()){
      return;
    }
    this.riskFactorDetail.encounter_id = this.encounterId;
    if (this.riskFactorDetail.id){
      this.onUpdate()
    } else {
      this.onAdd()
    }
  }
}