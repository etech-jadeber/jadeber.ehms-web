import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RiskFactorSummary, RiskFactorSummaryPartialList } from '../risk_factor.model';
import { RiskFactorPersist } from '../risk_factor.persist';
import { RiskFactorNavigator } from '../risk_factor.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
@Component({
  selector: 'app-risk_factor-list',
  templateUrl: './risk-factor-list.component.html',
  styleUrls: ['./risk-factor-list.component.scss']
})export class RiskFactorListComponent implements OnInit {
  riskFactorsData: RiskFactorSummary[] = [];
  riskFactorsTotalCount: number = 0;
  riskFactorSelectAll:boolean = false;
  riskFactorSelection: RiskFactorSummary[] = [];

 riskFactorsDisplayedColumns: string[] = ["select","action","link" ,"risk_factor" ];
  riskFactorSearchTextBox: FormControl = new FormControl();
  riskFactorIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) riskFactorsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) riskFactorsSort: MatSort;

  @Input() encounterId: string;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public riskFactorPersist: RiskFactorPersist,
                public riskFactorNavigator: RiskFactorNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public form_encounterPersist: Form_EncounterPersist,

    ) {

        this.tcAuthorization.requireRead("risk_factor");
       this.riskFactorSearchTextBox.setValue(riskFactorPersist.riskFactorSearchText);
      //delay subsequent keyup events
      this.riskFactorSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.riskFactorPersist.riskFactorSearchText = value;
        this.searchRiskFactors();
      });
    } ngOnInit() {
   
      this.riskFactorsSort.sortChange.subscribe(() => {
        this.riskFactorsPaginator.pageIndex = 0;
        this.searchRiskFactors(true);
      });

      this.riskFactorsPaginator.page.subscribe(() => {
        this.searchRiskFactors(true);
      });
      //start by loading items
      this.searchRiskFactors();
    }

  searchRiskFactors(isPagination:boolean = false): void {


    let paginator = this.riskFactorsPaginator;
    let sorter = this.riskFactorsSort;

    this.riskFactorIsLoading = true;
    this.riskFactorSelection = [];

    this.riskFactorPersist.searchRiskFactor(this.encounterId, paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RiskFactorSummaryPartialList) => {
      this.riskFactorsData = partialList.data;
      if (partialList.total != -1) {
        this.riskFactorsTotalCount = partialList.total;
      }
      this.riskFactorIsLoading = false;
    }, error => {
      this.riskFactorIsLoading = false;
    });

  } downloadRiskFactors(): void {
    if(this.riskFactorSelectAll){
         this.riskFactorPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download risk_factor", true);
      });
    }
    else{
        this.riskFactorPersist.download(this.tcUtilsArray.idsList(this.riskFactorSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download risk_factor",true);
            });
        }
  }
addRiskFactor(): void {
    let dialogRef = this.riskFactorNavigator.addRiskFactor();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchRiskFactors();
      }
    });
  }

  editRiskFactor(item: RiskFactorSummary) {
    let dialogRef = this.riskFactorNavigator.editRiskFactor(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteRiskFactor(item: RiskFactorSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("risk_factor");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.riskFactorPersist.deleteRiskFactor(item.id).subscribe(response => {
          this.tcNotification.success("risk_factor deleted");
          this.searchRiskFactors();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}