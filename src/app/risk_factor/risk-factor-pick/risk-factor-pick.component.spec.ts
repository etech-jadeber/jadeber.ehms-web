import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorPickComponent } from './risk-factor-pick.component';

describe('RiskFactorPickComponent', () => {
  let component: RiskFactorPickComponent;
  let fixture: ComponentFixture<RiskFactorPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
