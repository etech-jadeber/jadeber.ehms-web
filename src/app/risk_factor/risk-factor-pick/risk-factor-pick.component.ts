import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSort} from '@angular/material/sort';
import {MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RiskFactorSummary, RiskFactorSummaryPartialList } from '../risk_factor.model';
import { RiskFactorPersist } from '../risk_factor.persist';
import { RiskFactorNavigator } from '../risk_factor.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
@Component({
  selector: 'app-risk_factor-pick',
  templateUrl: './risk-factor-pick.component.html',
  styleUrls: ['./risk-factor-pick.component.scss']
})export class RiskFactorPickComponent implements OnInit {
  riskFactorsData: RiskFactorSummary[] = [];
  riskFactorsTotalCount: number = 0;
  riskFactorSelectAll:boolean = false;
  riskFactorSelection: RiskFactorSummary[] = [];

 riskFactorsDisplayedColumns: string[] = ["select", ,"risk_factor" ];
  riskFactorSearchTextBox: FormControl = new FormControl();
  riskFactorIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) riskFactorsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) riskFactorsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public riskFactorPersist: RiskFactorPersist,
                public riskFactorNavigator: RiskFactorNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<RiskFactorPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("risk_factor");
       this.riskFactorSearchTextBox.setValue(riskFactorPersist.riskFactorSearchText);
      //delay subsequent keyup events
      this.riskFactorSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.riskFactorPersist.riskFactorSearchText = value;
        this.searchRisk_factors();
      });
    } ngOnInit() {
   
      this.riskFactorsSort.sortChange.subscribe(() => {
        this.riskFactorsPaginator.pageIndex = 0;
        this.searchRisk_factors(true);
      });

      this.riskFactorsPaginator.page.subscribe(() => {
        this.searchRisk_factors(true);
      });
      //start by loading items
      this.searchRisk_factors();
    }

  searchRisk_factors(isPagination:boolean = false): void {


    let paginator = this.riskFactorsPaginator;
    let sorter = this.riskFactorsSort;

    this.riskFactorIsLoading = true;
    this.riskFactorSelection = [];

    this.riskFactorPersist.searchRiskFactor(TCUtilsString.getInvalidId(),paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RiskFactorSummaryPartialList) => {
      this.riskFactorsData = partialList.data;
      if (partialList.total != -1) {
        this.riskFactorsTotalCount = partialList.total;
      }
      this.riskFactorIsLoading = false;
    }, error => {
      this.riskFactorIsLoading = false;
    });

  }
  markOneItem(item: RiskFactorSummary) {
    if(!this.tcUtilsArray.containsId(this.riskFactorSelection,item.id)){
          this.riskFactorSelection = [];
          this.riskFactorSelection.push(item);
        }
        else{
          this.riskFactorSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.riskFactorSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }