import {TCId} from "../tc/models";
export class RiskFactorSummary extends TCId {
    risk_factor:string;
    encounter_id: string;
     
    }
    export class RiskFactorSummaryPartialList {
      data: RiskFactorSummary[];
      total: number;
    }
    export class RiskFactorDetail extends RiskFactorSummary {
    }
    
    export class RiskFactorDashboard {
      total: number;
    }