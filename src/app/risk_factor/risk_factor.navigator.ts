import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {RiskFactorEditComponent} from "./risk-factor-edit/risk-factor-edit.component";
import {RiskFactorPickComponent} from "./risk-factor-pick/risk-factor-pick.component";


@Injectable({
  providedIn: 'root'
})

export class RiskFactorNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  riskFactorsUrl(): string {
    return "/risk_factors";
  }

  riskFactorUrl(id: string): string {
    return "/risk_factors/" + id;
  }

  viewRiskFactors(): void {
    this.router.navigateByUrl(this.riskFactorsUrl());
  }

  viewRiskFactor(id: string): void {
    this.router.navigateByUrl(this.riskFactorUrl(id));
  }

  editRiskFactor(id: string): MatDialogRef<RiskFactorEditComponent> {
    const dialogRef = this.dialog.open(RiskFactorEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addRiskFactor(): MatDialogRef<RiskFactorEditComponent> {
    const dialogRef = this.dialog.open(RiskFactorEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickRiskFactors(selectOne: boolean=false): MatDialogRef<RiskFactorPickComponent> {
      const dialogRef = this.dialog.open(RiskFactorPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}