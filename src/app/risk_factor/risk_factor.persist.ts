import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {RiskFactorDashboard, RiskFactorDetail, RiskFactorSummaryPartialList} from "./risk_factor.model";


@Injectable({
  providedIn: 'root'
})
export class RiskFactorPersist {
 riskFactorSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.riskFactorSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchRiskFactor(encounterId:string, pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RiskFactorSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("form_encounter/" + encounterId + "/risk_factor", this.riskFactorSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<RiskFactorSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factor/do", new TCDoParam("download_all", this.filters()));
  }

  riskFactorDashboard(): Observable<RiskFactorDashboard> {
    return this.http.get<RiskFactorDashboard>(environment.tcApiBaseUri + "risk_factor/dashboard");
  }

  getRiskFactor(id: string): Observable<RiskFactorDetail> {
    return this.http.get<RiskFactorDetail>(environment.tcApiBaseUri + "risk_factor/" + id);
  } addRiskFactor(item: RiskFactorDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factor/", item);
  }

  updateRiskFactor(item: RiskFactorDetail): Observable<RiskFactorDetail> {
    return this.http.patch<RiskFactorDetail>(environment.tcApiBaseUri + "risk_factor/" + item.id, item);
  }

  deleteRiskFactor(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "risk_factor/" + id);
  }
 riskFactorsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "risk_factor/do", new TCDoParam(method, payload));
  }

  riskFactorDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "risk_factors/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factor/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factor/" + id + "/do", new TCDoParam("print", {}));
  }


}