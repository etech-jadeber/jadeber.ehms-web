import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorsOptionsDetailComponent } from './risk-factors-options-detail.component';

describe('RiskFactorsOptionsDetailComponent', () => {
  let component: RiskFactorsOptionsDetailComponent;
  let fixture: ComponentFixture<RiskFactorsOptionsDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorsOptionsDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorsOptionsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
