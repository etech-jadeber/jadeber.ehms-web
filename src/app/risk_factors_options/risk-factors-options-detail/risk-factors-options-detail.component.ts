import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {RiskFactorsOptionsDetail} from "../risk_factors_options.model";
import {RiskFactorsOptionsPersist} from "../risk_factors_options.persist";
import {RiskFactorsOptionsNavigator} from "../risk_factors_options.navigator";

export enum RiskFactorsOptionsTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-risk_factors_options-detail',
  templateUrl: './risk-factors-options-detail.component.html',
  styleUrls: ['./risk-factors-options-detail.component.scss']
})
export class RiskFactorsOptionsDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  riskFactorsOptionsIsLoading:boolean = false;
  
  RiskFactorsOptionsTabs: typeof RiskFactorsOptionsTabs = RiskFactorsOptionsTabs;
  activeTab: RiskFactorsOptionsTabs = RiskFactorsOptionsTabs.overview;
  //basics
  riskFactorsOptionsDetail: RiskFactorsOptionsDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public riskFactorsOptionsNavigator: RiskFactorsOptionsNavigator,
              public  riskFactorsOptionsPersist: RiskFactorsOptionsPersist) {
    this.tcAuthorization.requireRead("risk_factors_options");
    this.riskFactorsOptionsDetail = new RiskFactorsOptionsDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("risk_factors_options");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.riskFactorsOptionsIsLoading = true;
    this.riskFactorsOptionsPersist.getRiskFactorsOptions(id).subscribe(riskFactorsOptionsDetail => {
          this.riskFactorsOptionsDetail = riskFactorsOptionsDetail;
          this.riskFactorsOptionsIsLoading = false;
        }, error => {
          console.error(error);
          this.riskFactorsOptionsIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.riskFactorsOptionsDetail.id);
  }
  editRiskFactorsOptions(): void {
    let modalRef = this.riskFactorsOptionsNavigator.editRiskFactorsOptions(this.riskFactorsOptionsDetail.id);
    modalRef.afterClosed().subscribe(riskFactorsOptionsDetail => {
      TCUtilsAngular.assign(this.riskFactorsOptionsDetail, riskFactorsOptionsDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.riskFactorsOptionsPersist.print(this.riskFactorsOptionsDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print risk_factors_options", true);
      });
    }

  back():void{
      this.location.back();
    }

}
