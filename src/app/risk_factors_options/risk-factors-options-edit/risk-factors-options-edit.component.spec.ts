import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorsOptionsEditComponent } from './risk-factors-options-edit.component';

describe('RiskFactorsOptionsEditComponent', () => {
  let component: RiskFactorsOptionsEditComponent;
  let fixture: ComponentFixture<RiskFactorsOptionsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorsOptionsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorsOptionsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
