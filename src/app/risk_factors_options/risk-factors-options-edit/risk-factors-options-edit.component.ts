import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RiskFactorsOptionsDetail } from '../risk_factors_options.model';
import { RiskFactorsOptionsPersist } from '../risk_factors_options.persist';
@Component({
  selector: 'app-risk_factors_options-edit',
  templateUrl: './risk-factors-options-edit.component.html',
  styleUrls: ['./risk-factors-options-edit.component.scss'],
})
export class RiskFactorsOptionsEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  riskFactorsOptionsDetail: RiskFactorsOptionsDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<RiskFactorsOptionsEditComponent>,
    public persist: RiskFactorsOptionsPersist,

    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('risk_factors_options');
      this.title =
        this.appTranslation.getText('general', 'new') +
        ' ' +
        'risk_factors_options';
      this.riskFactorsOptionsDetail = new RiskFactorsOptionsDetail();
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('risk_factors_options');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'risk_factors_options';
      this.isLoadingResults = true;
      this.persist.getRiskFactorsOptions(this.idMode.id).subscribe(
        (riskFactorsOptionsDetail) => {
          this.riskFactorsOptionsDetail = riskFactorsOptionsDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addRiskFactorsOptions(this.riskFactorsOptionsDetail).subscribe(
      (value) => {
        this.tcNotification.success('riskFactorsOptions added');
        this.riskFactorsOptionsDetail.id = value.id;
        this.dialogRef.close(this.riskFactorsOptionsDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist
      .updateRiskFactorsOptions(this.riskFactorsOptionsDetail)
      .subscribe(
        (value) => {
          this.tcNotification.success('risk_factors_options updated');
          this.dialogRef.close(this.riskFactorsOptionsDetail);
        },
        (error) => {
          this.isLoadingResults = false;
        }
      );
  }
  canSubmit(): boolean {
    if (this.riskFactorsOptionsDetail == null) {
      return false;
    }

    if (
      this.riskFactorsOptionsDetail.name == null ||
      this.riskFactorsOptionsDetail.name == ''
    ) {
      return false;
    }
    if (this.riskFactorsOptionsDetail.type == null) {
      return false;
    }
    return true;
  }
}
