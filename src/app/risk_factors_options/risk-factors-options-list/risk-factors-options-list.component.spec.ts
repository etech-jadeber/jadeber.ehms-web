import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorsOptionsListComponent } from './risk-factors-options-list.component';

describe('RiskFactorsOptionsListComponent', () => {
  let component: RiskFactorsOptionsListComponent;
  let fixture: ComponentFixture<RiskFactorsOptionsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorsOptionsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorsOptionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
