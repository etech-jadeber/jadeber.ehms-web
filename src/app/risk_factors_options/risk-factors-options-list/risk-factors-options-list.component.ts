import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RiskFactorsOptionsSummary, RiskFactorsOptionsSummaryPartialList } from '../risk_factors_options.model';
import { RiskFactorsOptionsPersist } from '../risk_factors_options.persist';
import { RiskFactorsOptionsNavigator } from '../risk_factors_options.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-risk_factors_options-list',
  templateUrl: './risk-factors-options-list.component.html',
  styleUrls: ['./risk-factors-options-list.component.scss']
})export class RiskFactorsOptionsListComponent implements OnInit {
  riskFactorsOptionssData: RiskFactorsOptionsSummary[] = [];
  riskFactorsOptionssTotalCount: number = 0;
  riskFactorsOptionsSelectAll:boolean = false;
  riskFactorsOptionsSelection: RiskFactorsOptionsSummary[] = [];

 riskFactorsOptionssDisplayedColumns: string[] = ["select","action" ,"name","type" ];
  riskFactorsOptionsSearchTextBox: FormControl = new FormControl();
  riskFactorsOptionsIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) riskFactorsOptionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) riskFactorsOptionssSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public riskFactorsOptionsPersist: RiskFactorsOptionsPersist,
                public riskFactorsOptionsNavigator: RiskFactorsOptionsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("risk_factors_options");
       this.riskFactorsOptionsSearchTextBox.setValue(riskFactorsOptionsPersist.riskFactorsOptionsSearchText);
      //delay subsequent keyup events
      this.riskFactorsOptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.riskFactorsOptionsPersist.riskFactorsOptionsSearchText = value;
        this.searchRiskFactorsOptionss();
      });
    } ngOnInit() {
   
      this.riskFactorsOptionssSort.sortChange.subscribe(() => {
        this.riskFactorsOptionssPaginator.pageIndex = 0;
        this.searchRiskFactorsOptionss(true);
      });

      this.riskFactorsOptionssPaginator.page.subscribe(() => {
        this.searchRiskFactorsOptionss(true);
      });
      //start by loading items
      this.searchRiskFactorsOptionss();
    }

  searchRiskFactorsOptionss(isPagination:boolean = false): void {


    let paginator = this.riskFactorsOptionssPaginator;
    let sorter = this.riskFactorsOptionssSort;

    this.riskFactorsOptionsIsLoading = true;
    this.riskFactorsOptionsSelection = [];

    this.riskFactorsOptionsPersist.searchRiskFactorsOptions(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RiskFactorsOptionsSummaryPartialList) => {
      this.riskFactorsOptionssData = partialList.data;
      if (partialList.total != -1) {
        this.riskFactorsOptionssTotalCount = partialList.total;
      }
      this.riskFactorsOptionsIsLoading = false;
    }, error => {
      this.riskFactorsOptionsIsLoading = false;
    });

  } downloadRiskFactorsOptionss(): void {
    if(this.riskFactorsOptionsSelectAll){
         this.riskFactorsOptionsPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download risk_factors_options", true);
      });
    }
    else{
        this.riskFactorsOptionsPersist.download(this.tcUtilsArray.idsList(this.riskFactorsOptionsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download risk_factors_options",true);
            });
        }
  }
addRiskFactorsOptions(): void {
    let dialogRef = this.riskFactorsOptionsNavigator.addRiskFactorsOptions();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchRiskFactorsOptionss();
      }
    });
  }

  editRiskFactorsOptions(item: RiskFactorsOptionsSummary) {
    let dialogRef = this.riskFactorsOptionsNavigator.editRiskFactorsOptions(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteRiskFactorsOptions(item: RiskFactorsOptionsSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("risk_factors_options");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.riskFactorsOptionsPersist.deleteRiskFactorsOptions(item.id).subscribe(response => {
          this.tcNotification.success("risk_factors_options deleted");
          this.searchRiskFactorsOptionss();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
