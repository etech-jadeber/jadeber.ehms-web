import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskFactorsOptionsPickComponent } from './risk-factors-options-pick.component';

describe('RiskFactorsOptionsPickComponent', () => {
  let component: RiskFactorsOptionsPickComponent;
  let fixture: ComponentFixture<RiskFactorsOptionsPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskFactorsOptionsPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskFactorsOptionsPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
