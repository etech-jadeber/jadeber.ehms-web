import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from "@angular/material/sort";
import {MatPaginator} from "@angular/material/paginator";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RiskFactorsOptionsSummary, RiskFactorsOptionsSummaryPartialList } from '../risk_factors_options.model';
import { RiskFactorsOptionsPersist } from '../risk_factors_options.persist';
import { RiskFactorsOptionsNavigator } from '../risk_factors_options.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-risk_factors_options-pick',
  templateUrl: './risk-factors-options-pick.component.html',
  styleUrls: ['./risk-factors-options-pick.component.scss']
})export class RiskFactorsOptionsPickComponent implements OnInit {
  riskFactorsOptionssData: RiskFactorsOptionsSummary[] = [];
  riskFactorsOptionssTotalCount: number = 0;
  riskFactorsOptionsSelectAll:boolean = false;
  riskFactorsOptionsSelection: RiskFactorsOptionsSummary[] = [];

 riskFactorsOptionssDisplayedColumns: string[] = ["select", ,"name","type" ];
  riskFactorsOptionsSearchTextBox: FormControl = new FormControl();
  riskFactorsOptionsIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) riskFactorsOptionssPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) riskFactorsOptionssSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public riskFactorsOptionsPersist: RiskFactorsOptionsPersist,
                public riskFactorsOptionsNavigator: RiskFactorsOptionsNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<RiskFactorsOptionsPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("risk_factors_options");
       this.riskFactorsOptionsSearchTextBox.setValue(riskFactorsOptionsPersist.riskFactorsOptionsSearchText);
      //delay subsequent keyup events
      this.riskFactorsOptionsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.riskFactorsOptionsPersist.riskFactorsOptionsSearchText = value;
        this.searchRisk_factors_optionss();
      });
    } ngOnInit() {
   
      this.riskFactorsOptionssSort.sortChange.subscribe(() => {
        this.riskFactorsOptionssPaginator.pageIndex = 0;
        this.searchRisk_factors_optionss(true);
      });

      this.riskFactorsOptionssPaginator.page.subscribe(() => {
        this.searchRisk_factors_optionss(true);
      });
      //start by loading items
      this.searchRisk_factors_optionss();
    }

  searchRisk_factors_optionss(isPagination:boolean = false): void {


    let paginator = this.riskFactorsOptionssPaginator;
    let sorter = this.riskFactorsOptionssSort;

    this.riskFactorsOptionsIsLoading = true;
    this.riskFactorsOptionsSelection = [];

    this.riskFactorsOptionsPersist.searchRiskFactorsOptions(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RiskFactorsOptionsSummaryPartialList) => {
      this.riskFactorsOptionssData = partialList.data;
      if (partialList.total != -1) {
        this.riskFactorsOptionssTotalCount = partialList.total;
      }
      this.riskFactorsOptionsIsLoading = false;
    }, error => {
      this.riskFactorsOptionsIsLoading = false;
    });

  }
  markOneItem(item: RiskFactorsOptionsSummary) {
    if(!this.tcUtilsArray.containsId(this.riskFactorsOptionsSelection,item.id)){
          this.riskFactorsOptionsSelection = [];
          this.riskFactorsOptionsSelection.push(item);
        }
        else{
          this.riskFactorsOptionsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.riskFactorsOptionsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
