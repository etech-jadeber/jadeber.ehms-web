import {TCId} from "../tc/models";
export class RiskFactorsOptionsSummary extends TCId {
    name:string;
    type:number;
     
    }
    export class RiskFactorsOptionsSummaryPartialList {
      data: RiskFactorsOptionsSummary[];
      total: number;
    }
    export class RiskFactorsOptionsDetail extends RiskFactorsOptionsSummary {
    }
    
    export class RiskFactorsOptionsDashboard {
      total: number;
    }
    