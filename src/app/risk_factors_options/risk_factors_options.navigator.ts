import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {RiskFactorsOptionsEditComponent} from "./risk-factors-options-edit/risk-factors-options-edit.component";
import {RiskFactorsOptionsPickComponent} from "./risk-factors-options-pick/risk-factors-options-pick.component";


@Injectable({
  providedIn: 'root'
})

export class RiskFactorsOptionsNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  riskFactorsOptionssUrl(): string {
    return "/risk_factors_optionss";
  }

  riskFactorsOptionsUrl(id: string): string {
    return "/risk_factors_optionss/" + id;
  }

  viewRiskFactorsOptionss(): void {
    this.router.navigateByUrl(this.riskFactorsOptionssUrl());
  }

  viewRiskFactorsOptions(id: string): void {
    this.router.navigateByUrl(this.riskFactorsOptionsUrl(id));
  }

  editRiskFactorsOptions(id: string): MatDialogRef<RiskFactorsOptionsEditComponent> {
    const dialogRef = this.dialog.open(RiskFactorsOptionsEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addRiskFactorsOptions(): MatDialogRef<RiskFactorsOptionsEditComponent> {
    const dialogRef = this.dialog.open(RiskFactorsOptionsEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickRiskFactorsOptionss(selectOne: boolean=false): MatDialogRef<RiskFactorsOptionsPickComponent> {
      const dialogRef = this.dialog.open(RiskFactorsOptionsPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
