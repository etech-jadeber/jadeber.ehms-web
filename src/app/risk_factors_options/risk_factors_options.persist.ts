import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {RiskFactorsOptionsDashboard, RiskFactorsOptionsDetail, RiskFactorsOptionsSummaryPartialList} from "./risk_factors_options.model";
import { ModificationStatus } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class RiskFactorsOptionsPersist {
 riskFactorsOptionsSearchText: string = "";
 modificationStatus: TCEnum[] = [
    new TCEnum( ModificationStatus.modifiable, 'Modifiable'),
    new TCEnum( ModificationStatus.non_modifiable, 'Non Modifiable'),
    new TCEnum( ModificationStatus.medical_conditions_contribute_to_cad, 'Medical Conditions Contribute to CAD'),
 ];
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.riskFactorsOptionsSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchRiskFactorsOptions(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RiskFactorsOptionsSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("risk_factors_options", this.riskFactorsOptionsSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<RiskFactorsOptionsSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factors_options/do", new TCDoParam("download_all", this.filters()));
  }

  riskFactorsOptionsDashboard(): Observable<RiskFactorsOptionsDashboard> {
    return this.http.get<RiskFactorsOptionsDashboard>(environment.tcApiBaseUri + "risk_factors_options/dashboard");
  }

  getRiskFactorsOptions(id: string): Observable<RiskFactorsOptionsDetail> {
    return this.http.get<RiskFactorsOptionsDetail>(environment.tcApiBaseUri + "risk_factors_options/" + id);
  } addRiskFactorsOptions(item: RiskFactorsOptionsDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factors_options/", item);
  }

  updateRiskFactorsOptions(item: RiskFactorsOptionsDetail): Observable<RiskFactorsOptionsDetail> {
    return this.http.patch<RiskFactorsOptionsDetail>(environment.tcApiBaseUri + "risk_factors_options/" + item.id, item);
  }

  deleteRiskFactorsOptions(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "risk_factors_options/" + id);
  }
 riskFactorsOptionssDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "risk_factors_options/do", new TCDoParam(method, payload));
  }

  riskFactorsOptionsDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "risk_factors_optionss/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factors_options/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "risk_factors_options/" + id + "/do", new TCDoParam("print", {}));
  }


}
