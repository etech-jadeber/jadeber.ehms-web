import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorInRoomEditComponent } from './doctor-in-room-edit.component';

describe('DoctorInRoomEditComponent', () => {
  let component: DoctorInRoomEditComponent;
  let fixture: ComponentFixture<DoctorInRoomEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorInRoomEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorInRoomEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
