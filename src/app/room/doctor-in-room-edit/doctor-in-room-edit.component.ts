import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { DoctorInRoomDetail } from '../room.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RoomPersist } from '../room.persist';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { physican_type } from 'src/app/app.enums';
@Component({
  selector: 'app-doctor_in_room-edit',
  templateUrl: './doctor-in-room-edit.component.html',
  styleUrls: ['./doctor-in-room-edit.component.scss']
})export class DoctorInRoomEditComponent implements OnInit {

  userFullName:string = "";
  start_time: string;
  end_time:  string;

  isLoadingResults: boolean = false;
  title: string;
  doctorInRoomDetail: DoctorInRoomDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public doctorNavigator:DoctorNavigator,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<DoctorInRoomEditComponent>,
              public  persist: RoomPersist,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.context['mode'] === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context['mode'] === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context['mode'] === TCModalModes.CASE;
  }ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("doctor_in_room");
      this.title = this.appTranslation.getText("general","new") +  " " + "Doctor In Room";
      this.doctorInRoomDetail = new DoctorInRoomDetail();
this.doctorInRoomDetail.room_id = this.idMode.parentId;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("doctor_in_room");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "doctor_in_room";
      this.isLoadingResults = true;
      this.persist.getDoctorInRoom(this.idMode.parentId,this.idMode.childId).subscribe(doctorInRoomDetail => {
        this.doctorInRoomDetail = doctorInRoomDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addDoctorInRoom(this.idMode.parentId,this.doctorInRoomDetail).subscribe(value => {
      this.tcNotification.success("doctorInRoom added");
      this.doctorInRoomDetail.id = value.id;
      this.dialogRef.close(this.doctorInRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();

    this.persist.updateDoctorInRoom(this.idMode.parentId,this.doctorInRoomDetail).subscribe(value => {
      this.tcNotification.success("doctor_in_room updated");
      this.dialogRef.close(this.doctorInRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.doctorInRoomDetail == null){
            return false;
          }

if (this.doctorInRoomDetail.room_id == null || this.doctorInRoomDetail.room_id  == "") {
            return false;
        }

if (this.doctorInRoomDetail.doctor_id == null) {
            return false;
        }

        return true;

 }


 transformDates(todate: boolean = true) {
  if (todate) {
    let ed = new Date();
    let end_times:string[] = this.end_time.split(":");
    ed.setHours(parseInt( end_times[0]))
    ed.setMinutes(parseInt( end_times[1]))
    let sd = new Date();
    let start_times:string[] = this.start_time.split(":");
    sd.setHours(parseInt( start_times[0]))
    sd.setMinutes(parseInt( start_times[1]))

   
    this.doctorInRoomDetail.end_time = this.tcUtilsDate.toTimeStamp(ed);
    this.doctorInRoomDetail.start_time = this.tcUtilsDate.toTimeStamp(sd);
    
   
  
  } else {

  }
}




 searchDoctor() {
  let dialogRef = this.doctorNavigator.pickDoctors(true, physican_type.doctor);
  dialogRef.afterClosed().subscribe((result: DoctorSummary) => {
    if (result) {
      this.userFullName = `${result[0].first_name} ${result[0].middle_name} ${result[0].last_name}`;
      this.doctorInRoomDetail.doctor_id = result[0].id;
    }
  });
}


 }