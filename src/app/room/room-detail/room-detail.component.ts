import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {DoctorInRoomDetail, DoctorInRoomSummary, RoomDetail} from "../room.model";
import {RoomPersist} from "../room.persist";
import {RoomNavigator} from "../room.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { FormControl } from '@angular/forms';
import { TCUtilsDate } from 'src/app/tc/utils-date';

export enum RoomTabs {
  overview,doctor_in_room
}

export enum PaginatorIndexes {
  doctor_in_room

}

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.scss']
})
export class RoomDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  roomIsLoading:boolean = false;
  
  RoomTabs: typeof RoomTabs = RoomTabs;
  activeTab: RoomTabs = RoomTabs.overview;
  //basics
  roomDetail: RoomDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();


  doctorInRoomsData: DoctorInRoomSummary[] = [];
  doctorInRoomsTotalCount: number = 0;
  doctorInRoomSelectAll:boolean = false;
  doctorInRoomSelection: DoctorInRoomSummary[] = [];


  doctorInRoomsDisplayedColumns: string[] = ["select","action" ,"start_time","end_time","doctor_id","status" ];
  doctorInRoomSearchTextBox: FormControl = new FormControl();
  doctorInRoomIsLoading: boolean = false;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public tcUtilDate:TCUtilsDate,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public roomNavigator: RoomNavigator,
              public  roomPersist: RoomPersist) {
    this.tcAuthorization.requireRead("rooms");
    this.roomDetail = new RoomDetail();

    this.doctorInRoomSearchTextBox.setValue(roomPersist.doctorInRoomSearchText);
    this.doctorInRoomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.roomPersist.doctorInRoomSearchText = value.trim();
      this.searchDoctorInRoom();
    });


  } 
  
  ngOnInit() {
    this.tcAuthorization.requireRead("rooms");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });

    this.sorters.toArray()[PaginatorIndexes.doctor_in_room].sortChange.subscribe(() => {
      this.paginators.toArray()[PaginatorIndexes.doctor_in_room].pageIndex = 0;
      this.searchDoctorInRoom(true);
    });

   }

  loadDetails(id: string): void {
  this.roomIsLoading = true;
    this.roomPersist.getRoom(id).subscribe(roomDetail => {
          this.roomDetail = roomDetail;
          this.searchDoctorInRoom();
          this.roomIsLoading = false;
        }, error => {
          console.error(error);
          this.roomIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.roomDetail.id);
  }
  editRoom(): void {
    let modalRef = this.roomNavigator.editRoom(this.roomDetail.id);
    modalRef.afterClosed().subscribe(roomDetail => {
      TCUtilsAngular.assign(this.roomDetail, roomDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.roomPersist.print(this.roomDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print room", true);
      });
    }

  back():void{
      this.location.back();
    }



    searchDoctorInRoom(isPagination: boolean = false): void {
      let paginator = this.paginators.toArray()[PaginatorIndexes.doctor_in_room];
      let sorter = this.sorters.toArray()[PaginatorIndexes.doctor_in_room];
      this.doctorInRoomSelection = [];
      this.doctorInRoomIsLoading = true;
      this.roomPersist.searchDoctorInRoom(this.roomDetail.id, paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe(response => {
        this.doctorInRoomsData = response.data;
        if (response.total != -1) {
          this.doctorInRoomsTotalCount = response.total;
        }
        this.doctorInRoomIsLoading = false;
      }, error => { this.doctorInRoomIsLoading = false; });
    }  deleteDoctorInRoom(item: DoctorInRoomDetail): void {
      let dialogRef = this.tcNavigator.confirmDeletion("doctor_in_room");
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          //do the deletion
          this.roomPersist.deleteDoctorInRoom(this.roomDetail.id, item.id).subscribe(response => {
            this.tcNotification.success("doctor_in_room deleted");
            this.searchDoctorInRoom();
          }, error => {
          });
        }
  
      });
    }  addDoctorInRoom(): void {
      let dialogRef = this.roomNavigator.addDoctorInRoom(this.roomDetail.id);
      dialogRef.afterClosed().subscribe(res => {
        this.searchDoctorInRoom();
      });
    }
  
    editDoctorInRoom(item: DoctorInRoomDetail): void {
      let dialogRef = this.roomNavigator.editDoctorInRoom(this.roomDetail.id, item.id);
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.searchDoctorInRoom();
        }
      });
    } downloadDoctorInRooms(): void {
  this.tcNotification.info("Download doctor_in_room" );  
  }

}