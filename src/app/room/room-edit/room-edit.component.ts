import {Component, OnInit, Inject} from '@angular/core';


import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { RoomDetail } from '../room.model';import { RoomPersist } from '../room.persist';import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BedroomtypeSummary } from 'src/app/bed/bedroomtypes/bedroomtype.model';
import { BedroomtypeNavigator } from 'src/app/bed/bedroomtypes/bedroomtype.navigator';
@Component({
  selector: 'app-room-edit',
  templateUrl: './room-edit.component.html',
  styleUrls: ['./room-edit.component.scss']
})export class RoomEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  wardName:string = "";
  roomDetail: RoomDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<RoomEditComponent>,
              public  persist: RoomPersist,
              public wardNavigator:BedroomtypeNavigator,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }


  assignWard():void{
    let dialogRef = this.wardNavigator.pickBedroomtypes(true);
    dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
      if (result) {
        this.wardName = result[0].name;
        this.roomDetail.ward_id = result[0].id;
      }
    });
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("rooms");
      this.title = this.appTranslation.getText("general","new") +  " " + "room";
      this.roomDetail = new RoomDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("rooms");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "room";
      this.isLoadingResults = true;
      this.persist.getRoom(this.idMode.id).subscribe(roomDetail => {
        this.roomDetail = roomDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addRoom(this.roomDetail).subscribe(value => {
      this.tcNotification.success("room added");
      this.roomDetail.id = value.id;
      this.dialogRef.close(this.roomDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateRoom(this.roomDetail).subscribe(value => {
      this.tcNotification.success("room updated");
      this.dialogRef.close(this.roomDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.roomDetail == null){
            return false;
          }

if (this.roomDetail.room_no == null || this.roomDetail.room_no  == "") {
            return false;
        } 
 return true;
if (this.roomDetail.ward_id == null || this.roomDetail.ward_id  == "") {
            return false;
        } 
 return true;
if (this.roomDetail.status == null) {
            return false;
        }

 }
 }