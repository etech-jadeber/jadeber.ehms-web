import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { RoomSummary, RoomSummaryPartialList } from '../room.model';

import { RoomNavigator } from '../room.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { RoomPersist } from '../room.persist';
@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss']
})export class RoomListComponent implements OnInit {
  roomsData: RoomSummary[] = [];
  roomsTotalCount: number = 0;
  roomSelectAll:boolean = false;
  roomSelection: RoomSummary[] = [];

 roomsDisplayedColumns: string[] = ["select","action" ,"room_no","ward_id","status" ];
  roomSearchTextBox: FormControl = new FormControl();
  roomIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) roomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) roomsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public roomPersist: RoomPersist,
                public roomNavigator: RoomNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("rooms");
       this.roomSearchTextBox.setValue(roomPersist.roomSearchText);
      //delay subsequent keyup events
      this.roomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.roomPersist.roomSearchText = value;
        this.searchRooms();
      });
    } ngOnInit() {
   
      this.roomsSort.sortChange.subscribe(() => {
        this.roomsPaginator.pageIndex = 0;
        this.searchRooms(true);
      });

      this.roomsPaginator.page.subscribe(() => {
        this.searchRooms(true);
      });
      //start by loading items
      this.searchRooms();
    }

  searchRooms(isPagination:boolean = false): void {


    let paginator = this.roomsPaginator;
    let sorter = this.roomsSort;

    this.roomIsLoading = true;
    this.roomSelection = [];

    this.roomPersist.searchRoom(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: RoomSummaryPartialList) => {
      this.roomsData = partialList.data;
      if (partialList.total != -1) {
        this.roomsTotalCount = partialList.total;
      }
      this.roomIsLoading = false;
    }, error => {
      this.roomIsLoading = false;
    });

  } downloadRooms(): void {
    if(this.roomSelectAll){
         this.roomPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download room", true);
      });
    }
    else{
        this.roomPersist.download(this.tcUtilsArray.idsList(this.roomSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download room",true);
            });
        }
  }
addRoom(): void {
    let dialogRef = this.roomNavigator.addRoom();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchRooms();
      }
    });
  }

  editRoom(item: RoomSummary) {
    let dialogRef = this.roomNavigator.editRoom(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteRoom(item: RoomSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("room");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.roomPersist.deleteRoom(item.id).subscribe(response => {
          this.tcNotification.success("room deleted");
          this.searchRooms();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}