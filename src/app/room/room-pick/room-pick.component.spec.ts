import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomPickComponent } from './room-pick.component';

describe('RoomPickComponent', () => {
  let component: RoomPickComponent;
  let fixture: ComponentFixture<RoomPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoomPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
