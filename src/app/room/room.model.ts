import {TCId} from "../tc/models";export class RoomSummary extends TCId {
    room_no:string;
    ward_id:string;
    status:number;
    name:string;
     
    }
    export class RoomSummaryPartialList {
      data: RoomSummary[];
      total: number;
    }
    export class RoomDetail extends RoomSummary {
    }
    
    export class RoomDashboard {
      total: number;
    }


    export class DoctorInRoomSummary extends TCId {
      room_id:string;
      start_time:number;
      end_time:number;
      doctor_id:string;
      status:number;
      doc_name:string;
       
      }
      export class DoctorInRoomSummaryPartialList {
        data: DoctorInRoomSummary[];
        total: number;
      }
      export class DoctorInRoomDetail extends DoctorInRoomSummary {
      }
      
      export class DoctorInRoomDashboard {
        total: number;
      }