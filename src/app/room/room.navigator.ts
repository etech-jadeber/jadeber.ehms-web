import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";

import {RoomEditComponent} from "./room-edit/room-edit.component";
import {RoomPickComponent} from "./room-pick/room-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { DoctorInRoomEditComponent } from "./doctor-in-room-edit/doctor-in-room-edit.component";


@Injectable({
  providedIn: 'root'
})

export class RoomNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  roomsUrl(): string {
    return "/rooms";
  }

  roomUrl(id: string): string {
    return "/rooms/" + id;
  }

  viewRooms(): void {
    this.router.navigateByUrl(this.roomsUrl());
  }

  viewRoom(id: string): void {
    this.router.navigateByUrl(this.roomUrl(id));
  }

  editRoom(id: string): MatDialogRef<RoomEditComponent> {
    const dialogRef = this.dialog.open(RoomEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addRoom(): MatDialogRef<RoomEditComponent> {
    const dialogRef = this.dialog.open(RoomEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickRooms(selectOne: boolean=false): MatDialogRef<RoomPickComponent> {
      const dialogRef = this.dialog.open(RoomPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }

    editDoctorInRoom(parentId:string, id: string): MatDialogRef<DoctorInRoomEditComponent> {
      let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
        
      const dialogRef = this.dialog.open(DoctorInRoomEditComponent, {
        data:  new TCParentChildIds(parentId, id, params),
        width: TCModalWidths.medium
      });
      return dialogRef;
    }
    addDoctorInRoom(parentId:string, ): MatDialogRef<DoctorInRoomEditComponent> {
      return this.editDoctorInRoom(parentId,null);
    }
}