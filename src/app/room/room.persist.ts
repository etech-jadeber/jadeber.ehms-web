import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {DoctorInRoomDetail, DoctorInRoomSummaryPartialList, RoomDashboard, RoomDetail, RoomSummaryPartialList} from "./room.model";
import { DoctorInRoomStatus, RoomStatus } from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class RoomPersist {
 roomSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.roomSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchRoom(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RoomSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("room", this.roomSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<RoomSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "room/do", new TCDoParam("download_all", this.filters()));
  }

  roomDashboard(): Observable<RoomDashboard> {
    return this.http.get<RoomDashboard>(environment.tcApiBaseUri + "room/dashboard");
  }

  getRoom(id: string): Observable<RoomDetail> {
    return this.http.get<RoomDetail>(environment.tcApiBaseUri + "room/" + id);
  } addRoom(item: RoomDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "room/", item);
  }

  updateRoom(item: RoomDetail): Observable<RoomDetail> {
    return this.http.patch<RoomDetail>(environment.tcApiBaseUri + "room/" + item.id, item);
  }

  deleteRoom(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "room/" + id);
  }
 roomsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "room/do", new TCDoParam(method, payload));
  }

  roomDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "rooms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "room/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "room/" + id + "/do", new TCDoParam("print", {}));
  }


  roomStatus: TCEnumTranslation[] = [
    new TCEnumTranslation(RoomStatus.empty, "Empty"),
    new TCEnumTranslation(RoomStatus.occupied, "Occupied"),
   
  ];


  doctorInRoomStatus:TCEnumTranslation[] = [
    new TCEnumTranslation(DoctorInRoomStatus.empty, "Empty"),
    new TCEnumTranslation(DoctorInRoomStatus.occupied, "Occupied"),
  ]


  doctorInRoomSearchText:string = "";

  searchDoctorInRoom(parentId:string,pageSize: number, pageIndex: number, sort: string, order: string,): Observable<DoctorInRoomSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("room/"+parentId+"/doctor_in_room/", this.doctorInRoomSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<DoctorInRoomSummaryPartialList>(url);

  }
  getDoctorInRoom(parentId:string,id: string): Observable<DoctorInRoomDetail> {
    return this.http.get<DoctorInRoomDetail>(environment.tcApiBaseUri + "room/"+parentId+"/doctor_in_room/" + id);
  }
 addDoctorInRoom(parentId:string,item: DoctorInRoomDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "room/"+parentId+"/doctor_in_room/", item);
  }

  updateDoctorInRoom(parentId:string,item: DoctorInRoomDetail): Observable<DoctorInRoomDetail> {
    return this.http.patch<DoctorInRoomDetail>(environment.tcApiBaseUri + "room/"+parentId+"/doctor_in_room/" + item.id, item);
  }

  deleteDoctorInRoom(parentId:string,id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri +"room/"+parentId+"/doctor_in_room/" + id);
  }
 doctorInRoomsDo(parentId:string,method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "room/"+parentId+"/doctor_in_room/", new TCDoParam(method, payload));
  }

  doctorInRoomDo(parentId:string,id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "room/"+parentId+"/doctor_in_room/" + id + "/do", new TCDoParam(method, payload));
  }


}