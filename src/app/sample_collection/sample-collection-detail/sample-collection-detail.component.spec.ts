import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleCollectionDetailComponent } from './sample-collection-detail.component';

describe('SampleCollectionDetailComponent', () => {
  let component: SampleCollectionDetailComponent;
  let fixture: ComponentFixture<SampleCollectionDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleCollectionDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleCollectionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
