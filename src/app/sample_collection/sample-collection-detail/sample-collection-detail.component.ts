import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {SampleCollectionDetail} from "../sample_collection.model";
import {SampleCollectionPersist} from "../sample_collection.persist";
import {SampleCollectionNavigator} from "../sample_collection.navigator";

export enum SampleCollectionTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-sample_collection-detail',
  templateUrl: './sample-collection-detail.component.html',
  styleUrls: ['./sample-collection-detail.component.css']
})
export class SampleCollectionDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  sampleCollectionIsLoading:boolean = false;
  
  SampleCollectionTabs: typeof SampleCollectionTabs = SampleCollectionTabs;
  activeTab: SampleCollectionTabs = SampleCollectionTabs.overview;
  //basics
  sampleCollectionDetail: SampleCollectionDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public sampleCollectionNavigator: SampleCollectionNavigator,
              public  sampleCollectionPersist: SampleCollectionPersist) {
    this.tcAuthorization.requireRead("sample_collection");
    this.sampleCollectionDetail = new SampleCollectionDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("sample_collection");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.sampleCollectionIsLoading = true;
    this.sampleCollectionPersist.getSampleCollection(id).subscribe(sampleCollectionDetail => {
          this.sampleCollectionDetail = sampleCollectionDetail;
          this.sampleCollectionIsLoading = false;
        }, error => {
          console.error(error);
          this.sampleCollectionIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.sampleCollectionDetail.id);
  }
  editSampleCollection(): void {
    let modalRef = this.sampleCollectionNavigator.editSampleCollection(this.sampleCollectionDetail.id);
    modalRef.afterClosed().subscribe(sampleCollectionDetail => {
      TCUtilsAngular.assign(this.sampleCollectionDetail, sampleCollectionDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.sampleCollectionPersist.print(this.sampleCollectionDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print sample_collection", true);
      });
    }

  back():void{
      this.location.back();
    }

}