import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleCollectionEditComponent } from './sample-collection-edit.component';

describe('SampleCollectionEditComponent', () => {
  let component: SampleCollectionEditComponent;
  let fixture: ComponentFixture<SampleCollectionEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleCollectionEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleCollectionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
