import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { SampleCollectionDetail } from '../sample_collection.model';import { SampleCollectionPersist } from '../sample_collection.persist';import { selected_orders_status } from 'src/app/app.enums';
@Component({
  selector: 'app-sample_collection-edit',
  templateUrl: './sample-collection-edit.component.html',
  styleUrls: ['./sample-collection-edit.component.css']
})export class SampleCollectionEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  reject: boolean = false;
  sampleCollectionDetail: SampleCollectionDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<SampleCollectionEditComponent>,
              public  persist: SampleCollectionPersist,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode !== TCModalModes.EDIT && this.idMode.mode !== TCModalModes.CASE;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.isNew()) {
     this.tcAuthorization.requireCreate("sample_collections");
      this.title = this.appTranslation.getText("general","new") +  " " + "sample_collection";
      this.sampleCollectionDetail = new SampleCollectionDetail();
      //set necessary defaults
      this.sampleCollectionDetail.sample_type = -1;
      this.sampleCollectionDetail.order_id = this.idMode.mode;
      this.sampleCollectionDetail.selected_order_id = this.idMode.id;

    }
    else {
     this.tcAuthorization.requireUpdate("sample_collections");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "sample_collection";
      this.isLoadingResults = true;
      this.persist.getSampleCollection(this.idMode.id).subscribe(sampleCollectionDetail => {
        this.sampleCollectionDetail = sampleCollectionDetail;
        this.isLoadingResults = false;
        this.sampleCollectionDetail.sample_status = selected_orders_status.ready_to_approve;
        if(this.isCase()){
          this.reject = true;
          this.title = "Reject Reason"
          this.sampleCollectionDetail.sample_status = selected_orders_status.rejected
        }
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addSampleCollection(this.sampleCollectionDetail).subscribe(value => {
      this.tcNotification.success("sampleCollection added");
      this.sampleCollectionDetail.id = value.id;
      this.dialogRef.close(this.sampleCollectionDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateSampleCollection(this.sampleCollectionDetail).subscribe(value => {
      this.tcNotification.success("sample_collection updated");
      this.dialogRef.close(this.sampleCollectionDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  
  
  canSubmit():boolean{
        if (this.sampleCollectionDetail == null){
            return false;
          }

if (this.sampleCollectionDetail.sample_type == null) {
            return false;
        }
if (this.sampleCollectionDetail.anatomic_site == null) {
            return false;
        }
if (this.sampleCollectionDetail.selected_order_id == null || this.sampleCollectionDetail.selected_order_id  == "") {
            return false;
        }
if (this.sampleCollectionDetail.order_id == null || this.sampleCollectionDetail.selected_order_id == ""){
  return false;
}
if (this.reject && (this.sampleCollectionDetail.reject_reason == null || this.sampleCollectionDetail.reject_reason == "")){
  return false;
}
 return true;

 }
 }