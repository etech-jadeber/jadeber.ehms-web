import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleCollectionListComponent } from './sample-collection-list.component';

describe('SampleCollectionListComponent', () => {
  let component: SampleCollectionListComponent;
  let fixture: ComponentFixture<SampleCollectionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleCollectionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleCollectionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
