import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SampleCollectionSummary, SampleCollectionSummaryPartialList } from '../sample_collection.model';
import { SampleCollectionPersist } from '../sample_collection.persist';
import { SampleCollectionNavigator } from '../sample_collection.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { lab_order_type, selected_orders_status } from 'src/app/app.enums';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { OrdersPersist } from 'src/app/form_encounters/orderss/orders.persist';
@Component({
  selector: 'app-sample_collection-list',
  templateUrl: './sample-collection-list.component.html',
  styleUrls: ['./sample-collection-list.component.css']
})export class SampleCollectionListComponent implements OnInit {
  sampleCollectionsData: SampleCollectionSummary[] = [];
  sampleCollectionsTotalCount: number = 0;
  sampleCollectionSelectAll:boolean = false;
  sampleCollectionSelection: SampleCollectionSummary[] = [];
  selected_orders_status = selected_orders_status
  lab_order_type = lab_order_type;
 sampleCollectionsDisplayedColumns: string[] = ["select","generated_sample_id" ,"sample_type","date_of_collection","anatomic_site","patient_id","collector_id","sample_status","reject_reason" ];
  sampleCollectionSearchTextBox: FormControl = new FormControl();
  dateTextField: FormControl = new FormControl({disabled: true, value: new Date(new Date().toDateString())});
  sampleCollectionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) sampleCollectionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sampleCollectionsSort: MatSort;
  @Input() order_type: number;
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public sampleCollectionPersist: SampleCollectionPersist,
                public sampleCollectionNavigator: SampleCollectionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public tcUtilsString: TCUtilsString,
                public labPersist: OrdersPersist,
                public patientPersist: PatientPersist,
                public userPersist: UserPersist,

    ) {

        this.tcAuthorization.requireRead("sample_collections");
       this.sampleCollectionSearchTextBox.setValue(sampleCollectionPersist.sampleCollectionSearchText);
      //delay subsequent keyup events
      this.sampleCollectionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.sampleCollectionPersist.sampleCollectionSearchText = value;
        this.searchSampleCollections();
      });
      this.sampleCollectionPersist.date = this.dateTextField.value
      this.dateTextField.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.sampleCollectionPersist.date = value;
        this.searchSampleCollections();
      });
    } ngOnInit() {
      this.sampleCollectionPersist.order_type = this.order_type
      this.sampleCollectionsSort.sortChange.subscribe(() => {
        this.sampleCollectionsPaginator.pageIndex = 0;
        this.searchSampleCollections(true);
      });

      this.sampleCollectionsPaginator.page.subscribe(() => {
        this.searchSampleCollections(true);
      });
      //start by loading items
      this.searchSampleCollections();
    }

  searchSampleCollections(isPagination:boolean = false): void {


    let paginator = this.sampleCollectionsPaginator;
    let sorter = this.sampleCollectionsSort;

    this.sampleCollectionIsLoading = true;
    this.sampleCollectionSelection = [];

    this.sampleCollectionPersist.searchSampleCollection(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SampleCollectionSummaryPartialList) => {
      this.sampleCollectionsData = partialList.data;
      this.sampleCollectionsData.forEach(sampleCollection => {
        this.userPersist.getUser(sampleCollection.collector_id).subscribe(user => {
          sampleCollection.collector_name = user.name;
        })
          this.labPersist.getOrders(sampleCollection.order_id).subscribe(order => {
            sampleCollection.generated_sample_id = order.generated_sample_id
            order.patient_id != this.tcUtilsString.invalid_id && this.patientPersist.getPatient(order.patient_id).subscribe(patient => {
              sampleCollection.patient_name = patient.fname + patient.lname
            })
          })
      })
      if (partialList.total != -1) {
        this.sampleCollectionsTotalCount = partialList.total;
      }
      this.sampleCollectionIsLoading = false;
    }, error => {
      this.sampleCollectionIsLoading = false;
    });

  } downloadSampleCollections(): void {
    if(this.sampleCollectionSelectAll){
         this.sampleCollectionPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download sample_collection", true);
      });
    }
    else{
        this.sampleCollectionPersist.download(this.tcUtilsArray.idsList(this.sampleCollectionSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download sample_collection",true);
            });
        }
  }
addSampleCollection(): void {
    let dialogRef = this.sampleCollectionNavigator.addSampleCollection(this.tcUtilsString.invalid_id, this.tcUtilsString.invalid_id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSampleCollections();
      }
    });
  }

  editSampleCollection(item: SampleCollectionSummary) {
    let dialogRef = this.sampleCollectionNavigator.editSampleCollection(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSampleCollection(item: SampleCollectionSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("sample_collection");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.sampleCollectionPersist.deleteSampleCollection(item.id).subscribe(response => {
          this.tcNotification.success("sample_collection deleted");
          this.searchSampleCollections();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}