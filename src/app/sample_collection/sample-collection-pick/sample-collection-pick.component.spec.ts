import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleCollectionPickComponent } from './sample-collection-pick.component';

describe('SampleCollectionPickComponent', () => {
  let component: SampleCollectionPickComponent;
  let fixture: ComponentFixture<SampleCollectionPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampleCollectionPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleCollectionPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
