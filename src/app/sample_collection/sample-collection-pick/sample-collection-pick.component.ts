import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SampleCollectionSummary, SampleCollectionSummaryPartialList } from '../sample_collection.model';
import { SampleCollectionPersist } from '../sample_collection.persist';
import { SampleCollectionNavigator } from '../sample_collection.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-sample_collection-pick',
  templateUrl: './sample-collection-pick.component.html',
  styleUrls: ['./sample-collection-pick.component.css']
})export class SampleCollectionPickComponent implements OnInit {
  sampleCollectionsData: SampleCollectionSummary[] = [];
  sampleCollectionsTotalCount: number = 0;
  sampleCollectionSelectAll:boolean = false;
  sampleCollectionSelection: SampleCollectionSummary[] = [];

 sampleCollectionsDisplayedColumns: string[] = ["select", ,"sample_type","date_of_collection","anatomic_site","selected_order_id","collector_id","sample_status","reject_reason" ];
  sampleCollectionSearchTextBox: FormControl = new FormControl();
  sampleCollectionIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) sampleCollectionsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sampleCollectionsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public sampleCollectionPersist: SampleCollectionPersist,
                public sampleCollectionNavigator: SampleCollectionNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<SampleCollectionPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("sample_collection");
       this.sampleCollectionSearchTextBox.setValue(sampleCollectionPersist.sampleCollectionSearchText);
      //delay subsequent keyup events
      this.sampleCollectionSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.sampleCollectionPersist.sampleCollectionSearchText = value;
        this.searchSample_collections();
      });
    } ngOnInit() {
   
      this.sampleCollectionsSort.sortChange.subscribe(() => {
        this.sampleCollectionsPaginator.pageIndex = 0;
        this.searchSample_collections(true);
      });

      this.sampleCollectionsPaginator.page.subscribe(() => {
        this.searchSample_collections(true);
      });
      //start by loading items
      this.searchSample_collections();
    }

  searchSample_collections(isPagination:boolean = false): void {


    let paginator = this.sampleCollectionsPaginator;
    let sorter = this.sampleCollectionsSort;

    this.sampleCollectionIsLoading = true;
    this.sampleCollectionSelection = [];

    this.sampleCollectionPersist.searchSampleCollection(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SampleCollectionSummaryPartialList) => {
      this.sampleCollectionsData = partialList.data;
      if (partialList.total != -1) {
        this.sampleCollectionsTotalCount = partialList.total;
      }
      this.sampleCollectionIsLoading = false;
    }, error => {
      this.sampleCollectionIsLoading = false;
    });

  }
  markOneItem(item: SampleCollectionSummary) {
    if(!this.tcUtilsArray.containsId(this.sampleCollectionSelection,item.id)){
          this.sampleCollectionSelection = [];
          this.sampleCollectionSelection.push(item);
        }
        else{
          this.sampleCollectionSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.sampleCollectionSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }