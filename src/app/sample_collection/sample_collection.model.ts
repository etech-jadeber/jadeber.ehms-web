import {TCId} from "../tc/models";export class SampleCollectionSummary extends TCId {
    sample_type:number;
    date_of_collection:number;
    anatomic_site:number;
    selected_order_id:string;
    collector_id:string;
    sample_status:number;
    reject_reason:string;
    order_id: string;
    patient_name: string;
    collector_name: string;
    generated_sample_id: string;
    }
    export class SampleCollectionSummaryPartialList {
      data: SampleCollectionSummary[];
      total: number;
    }
    export class SampleCollectionDetail extends SampleCollectionSummary {
    }

    export class SampleCollectionDashboardInfo{
      samples_collected: number;
      samples_to_be_collected: number;
      total: number;
      order_type: number
    }