import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {SampleCollectionEditComponent} from "./sample-collection-edit/sample-collection-edit.component";
import {SampleCollectionPickComponent} from "./sample-collection-pick/sample-collection-pick.component";


@Injectable({
  providedIn: 'root'
})

export class SampleCollectionNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  sampleCollectionsUrl(): string {
    return "/sample_collections";
  }

  sampleCollectionUrl(id: string): string {
    return "/sample_collections/" + id;
  }

  viewSampleCollections(): void {
    this.router.navigateByUrl(this.sampleCollectionsUrl());
  }

  viewSampleCollection(id: string): void {
    this.router.navigateByUrl(this.sampleCollectionUrl(id));
  }

  editSampleCollection(id: string): MatDialogRef<SampleCollectionEditComponent> {
    const dialogRef = this.dialog.open(SampleCollectionEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  rejectSampleCollection(id: string): MatDialogRef<SampleCollectionEditComponent> {
    const dialogRef = this.dialog.open(SampleCollectionEditComponent, {
      data: new TCIdMode(id, TCModalModes.CASE),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSampleCollection(selectedOrderId: string, orderId: string): MatDialogRef<SampleCollectionEditComponent> {
    const dialogRef = this.dialog.open(SampleCollectionEditComponent, {
          data: new TCIdMode(orderId, selectedOrderId),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSampleCollections(selectOne: boolean=false): MatDialogRef<SampleCollectionPickComponent> {
      const dialogRef = this.dialog.open(SampleCollectionPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}