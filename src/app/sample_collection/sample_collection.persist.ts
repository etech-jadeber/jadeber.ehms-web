import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {SampleCollectionDashboardInfo, SampleCollectionDetail, SampleCollectionSummaryPartialList} from "./sample_collection.model";
import { AnatomicCite, SampleType } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsDate } from '../tc/utils-date';


@Injectable({
  providedIn: 'root'
})
export class SampleCollectionPersist {
 sampleCollectionSearchText: string = "";

 sampleTypeFilter: number ;
 date:Date;
 order_type: number;

    SampleType: TCEnum[] = [
     new TCEnum( SampleType.blood, 'blood'),
  new TCEnum( SampleType.urine, 'urine'),
  new TCEnum( SampleType.serum, 'serum'),
  new TCEnum( SampleType.semen, 'semen'),
  new TCEnum( SampleType.stool, 'stool'),
  new TCEnum( SampleType.csf, 'csf'),
  new TCEnum( SampleType.tissue, 'tissue'),
  new TCEnum( SampleType.sputum, 'sputum'),
  new TCEnum( SampleType.viginal_fluid, 'viginal_fluid'),
  new TCEnum( SampleType.pleural_fluid, 'pleural_fluid'),
  new TCEnum( SampleType.other_body_fluid, 'other_body_fluid'),
  new TCEnum( SampleType.knee_joint_fluid, 'knee_joint_fluid'),
  new TCEnum( SampleType.peritoneal_fluid, 'peritoneal_fluid'),
  new TCEnum( SampleType.pus, 'pus'),
  new TCEnum( SampleType.others, 'others'),

  ];

  anatomicCiteFilter: number ;

  AnatomicCite: TCEnum[] = [
   new TCEnum( AnatomicCite.Arm, 'arm'),
new TCEnum( AnatomicCite.Buttock, 'buttock'),
new TCEnum( AnatomicCite.Other, 'other'),

];
 
 constructor(private http: HttpClient,public tcUtilsDate: TCUtilsDate) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.sampleCollectionSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchSampleCollection(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<SampleCollectionSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("sample_collection", this.sampleCollectionSearchText, pageSize, pageIndex, sort, order);
    if (this.order_type){
      url = TCUtilsString.appendUrlParameter(url, "order_type", this.order_type.toString())
    }
    if (this.date){
      url = TCUtilsString.appendUrlParameter(url, "date_of_collection", (this.tcUtilsDate.toTimeStamp(this.date)).toString())
    }
    return this.http.get<SampleCollectionSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "sample_collection/do", new TCDoParam("download_all", this.filters()));
  }

  sampleCollectionDashboard(): Observable<SampleCollectionDashboardInfo[]> {
    return this.http.get<SampleCollectionDashboardInfo[]>(environment.tcApiBaseUri + "sample_collection/dashboard");
  }

  getSampleCollection(id: string): Observable<SampleCollectionDetail> {
    return this.http.get<SampleCollectionDetail>(environment.tcApiBaseUri + "sample_collection/" + id);
  } addSampleCollection(item: SampleCollectionDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "sample_collection/", item);
  }

  updateSampleCollection(item: SampleCollectionDetail): Observable<SampleCollectionDetail> {
    return this.http.patch<SampleCollectionDetail>(environment.tcApiBaseUri + "sample_collection/" + item.id, item);
  }

  deleteSampleCollection(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "sample_collection/" + id);
  }
 sampleCollectionsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "sample_collection/do", new TCDoParam(method, payload));
  }

  sampleCollectionDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "sample_collections/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "sample_collection/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "sample_collection/" + id + "/do", new TCDoParam("print", {}));
  }


}