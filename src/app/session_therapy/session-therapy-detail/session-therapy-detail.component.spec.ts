import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionTherapyDetailComponent } from './session-therapy-detail.component';

describe('SessionTherapyDetailComponent', () => {
  let component: SessionTherapyDetailComponent;
  let fixture: ComponentFixture<SessionTherapyDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionTherapyDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionTherapyDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
