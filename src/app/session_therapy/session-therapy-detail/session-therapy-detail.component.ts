import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {SessionTherapyDetail} from "../session_therapy.model";
import {SessionTherapyPersist} from "../session_therapy.persist";
import {SessionTherapyNavigator} from "../session_therapy.navigator";

export enum SessionTherapyTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-session_therapy-detail',
  templateUrl: './session-therapy-detail.component.html',
  styleUrls: ['./session-therapy-detail.component.scss']
})
export class SessionTherapyDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  sessionTherapyIsLoading:boolean = false;
  
  SessionTherapyTabs: typeof SessionTherapyTabs = SessionTherapyTabs;
  activeTab: SessionTherapyTabs = SessionTherapyTabs.overview;
  //basics
  sessionTherapyDetail: SessionTherapyDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public sessionTherapyNavigator: SessionTherapyNavigator,
              public  sessionTherapyPersist: SessionTherapyPersist) {
    this.tcAuthorization.requireRead("session_therapys");
    this.sessionTherapyDetail = new SessionTherapyDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("session_therapys");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.sessionTherapyIsLoading = true;
    this.sessionTherapyPersist.getSessionTherapy(id).subscribe(sessionTherapyDetail => {
          this.sessionTherapyDetail = sessionTherapyDetail;
          this.sessionTherapyIsLoading = false;
        }, error => {
          console.error(error);
          this.sessionTherapyIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.sessionTherapyDetail.id);
  }
  editSessionTherapy(): void {
    let modalRef = this.sessionTherapyNavigator.editSessionTherapy(this.sessionTherapyDetail.id);
    modalRef.afterClosed().subscribe(sessionTherapyDetail => {
      TCUtilsAngular.assign(this.sessionTherapyDetail, sessionTherapyDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.sessionTherapyPersist.print(this.sessionTherapyDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print session_therapy", true);
      });
    }

  back():void{
      this.location.back();
    }

}