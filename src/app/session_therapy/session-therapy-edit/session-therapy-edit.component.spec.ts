import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionTherapyEditComponent } from './session-therapy-edit.component';

describe('SessionTherapyEditComponent', () => {
  let component: SessionTherapyEditComponent;
  let fixture: ComponentFixture<SessionTherapyEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionTherapyEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionTherapyEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
