import {Component, OnInit, Inject} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { SessionTherapyDetail } from '../session_therapy.model';
import { SessionTherapyPersist } from '../session_therapy.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OtherServicesNavigator } from 'src/app/other_services/other-services.navigator';
import { OtherServicesSummary } from 'src/app/other_services/other-services.model';

@Component({
  selector: 'app-session_therapy-edit',
  templateUrl: './session-therapy-edit.component.html',
  styleUrls: ['./session-therapy-edit.component.scss']
})export class SessionTherapyEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  session_name:string;
  sessionTherapyDetail: SessionTherapyDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<SessionTherapyEditComponent>,
              public  persist: SessionTherapyPersist,
              public  otherServicesNavigator: OtherServicesNavigator,




              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("session_therapys");
      this.title = this.appTranslation.getText("general","new") +  " " + "session_therapy";
      this.sessionTherapyDetail = new SessionTherapyDetail();
      this.sessionTherapyDetail.status = 100
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("session_therapys");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "session_therapy";
      this.isLoadingResults = true;
      this.persist.getSessionTherapy(this.idMode.id).subscribe(sessionTherapyDetail => {
        this.sessionTherapyDetail = sessionTherapyDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;

    this.persist.addSessionTherapy(this.sessionTherapyDetail).subscribe(value => {
      this.tcNotification.success("sessionTherapy added");
      this.sessionTherapyDetail.id = value.id;

      this.dialogRef.close(this.sessionTherapyDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }
  searchOtherService() {
    let dialogRef = this.otherServicesNavigator.pickOtherServicess(null,true);
    dialogRef.afterClosed().subscribe((result: OtherServicesSummary) => {
console.log("earch",result)
      if (result) {
        this.session_name = result[0].name
        this.sessionTherapyDetail.other_service_id = result[0].id;
      }
    });
  }

  onUpdate(): void {
    this.isLoadingResults = true;


    this.persist.updateSessionTherapy(this.sessionTherapyDetail).subscribe(value => {
      this.tcNotification.success("session_therapy updated");
      this.dialogRef.close(this.sessionTherapyDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.sessionTherapyDetail == null){
            return false;
          }

         if(this.sessionTherapyDetail.status == null ){
          return false
         }
         if(this.sessionTherapyDetail.other_service_id == null || this.sessionTherapyDetail.other_service_id==""){

          return false
         }
         if(this.sessionTherapyDetail.name == "" || this.sessionTherapyDetail.name == null){
            return false
         }

          return true;

 }
 }
