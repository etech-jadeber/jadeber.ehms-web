import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionTherapyListComponent } from './session-therapy-list.component';

describe('SessionTherapyListComponent', () => {
  let component: SessionTherapyListComponent;
  let fixture: ComponentFixture<SessionTherapyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionTherapyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionTherapyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
