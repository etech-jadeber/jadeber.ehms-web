import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';

import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SessionTherapySummary, SessionTherapySummaryPartialList } from '../session_therapy.model';
import { SessionTherapyPersist } from '../session_therapy.persist';
import { SessionTherapyNavigator } from '../session_therapy.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { GroupMemberNavigator } from 'src/app/group_member/groupMember.navigator';
import { OtherServicesPersist } from 'src/app/other_services/other-services.persist';
import { OtherServicesDetail } from 'src/app/other_services/other-services.model';


@Component({
  selector: 'app-session_therapy-list',
  templateUrl: './session-therapy-list.component.html',
  styleUrls: ['./session-therapy-list.component.scss']
})export class SessionTherapyListComponent implements OnInit {
  sessionTherapysData: SessionTherapySummary[] = [];
  sessionTherapysTotalCount: number = 0;
  sessionTherapySelectAll:boolean = false;
  sessionTherapySelection: SessionTherapySummary[] = [];

 sessionTherapysDisplayedColumns: string[] = ["select","action" ,"name","status","session_type","date" ];
  sessionTherapySearchTextBox: FormControl = new FormControl();
  sessionTherapyIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) sessionTherapysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sessionTherapysSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public sessionTherapyPersist: SessionTherapyPersist,
                public otherServicesPersist:OtherServicesPersist,
                public sessionTherapyNavigator: SessionTherapyNavigator,
                public groupMemberNavigator: GroupMemberNavigator,

                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
    


    ) {

        this.tcAuthorization.requireRead("session_therapys");
       this.sessionTherapySearchTextBox.setValue(sessionTherapyPersist.sessionTherapySearchText);
      //delay subsequent keyup events
      this.sessionTherapySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.sessionTherapyPersist.sessionTherapySearchText = value;
        this.searchSession_therapys();
      });
    } ngOnInit() {
      
      
   
      this.sessionTherapysSort.sortChange.subscribe(() => {
        this.sessionTherapysPaginator.pageIndex = 0;
        this.searchSession_therapys(true);
      });

      this.sessionTherapysPaginator.page.subscribe(() => {
        this.searchSession_therapys(true);
      });
      //start by loading items
      this.searchSession_therapys();
    }

  searchSession_therapys(isPagination:boolean = false): void {


    let paginator = this.sessionTherapysPaginator;
    let sorter = this.sessionTherapysSort;

    this.sessionTherapyIsLoading = true;
    this.sessionTherapySelection = [];

    this.sessionTherapyPersist.searchSessionTherapy(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SessionTherapySummaryPartialList) => {
      
      this.sessionTherapysData = partialList.data;
      console.log("**********************************",this.sessionTherapysData)

      if (partialList.total != -1) {
        this.sessionTherapysTotalCount = partialList.total;
      }

      for(let data of this.sessionTherapysData){
         this.otherServicesPersist.getOtherServices(data.other_service_id).subscribe((otherServiceDetail:OtherServicesDetail)=>{
            data.session_type = otherServiceDetail.name
         })
      }

      this.sessionTherapyIsLoading = false;
    }, error => {
      this.sessionTherapyIsLoading = false;
    });

  } downloadSessionTherapys(): void {
    if(this.sessionTherapySelectAll){
         this.sessionTherapyPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download session_therapy", true);
      });
    }
    else{
        this.sessionTherapyPersist.download(this.tcUtilsArray.idsList(this.sessionTherapySelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download session_therapy",true);
            });
        }
  }
addSession_therapy(): void {
    let dialogRef = this.sessionTherapyNavigator.addSessionTherapy();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSession_therapys();
      }
    });
  }

  editSessionTherapy(item: SessionTherapySummary) {
    let dialogRef = this.sessionTherapyNavigator.editSessionTherapy(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSessionTherapy(item: SessionTherapySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("session_therapy");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.sessionTherapyPersist.deleteSessionTherapy(item.id).subscribe(response => {
          this.tcNotification.success("session_therapy deleted");
          this.searchSession_therapys();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}