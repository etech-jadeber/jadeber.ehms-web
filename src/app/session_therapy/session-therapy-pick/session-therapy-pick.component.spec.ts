import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionTherapyPickComponent } from './session-therapy-pick.component';

describe('SessionTherapyPickComponent', () => {
  let component: SessionTherapyPickComponent;
  let fixture: ComponentFixture<SessionTherapyPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessionTherapyPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionTherapyPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
