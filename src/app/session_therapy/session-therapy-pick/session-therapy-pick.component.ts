import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SessionTherapySummary, SessionTherapySummaryPartialList } from '../session_therapy.model';
import { SessionTherapyPersist } from '../session_therapy.persist';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-session_therapy-pick',
  templateUrl: './session-therapy-pick.component.html',
  styleUrls: ['./session-therapy-pick.component.scss']
})export class SessionTherapyPickComponent implements OnInit {
  sessionTherapysData: SessionTherapySummary[] = [];
  sessionTherapysTotalCount: number = 0;
  sessionTherapySelectAll:boolean = false;
  sessionTherapySelection: SessionTherapySummary[] = [];

 sessionTherapysDisplayedColumns: string[] = ["select","action" ,"name" ];
  sessionTherapySearchTextBox: FormControl = new FormControl();
  sessionTherapyIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) sessionTherapysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sessionTherapysSort: MatSort;  constructor(                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public dialogRef: MatDialogRef<SessionTherapyPickComponent>,

                public appTranslation:AppTranslation,
                public sessionTherapyPersist: SessionTherapyPersist,
 @Inject(MAT_DIALOG_DATA) public selectOne: boolean
    ) {

        this.tcAuthorization.requireRead("session_therapys");
       this.sessionTherapySearchTextBox.setValue(sessionTherapyPersist.sessionTherapySearchText);
      //delay subsequent keyup events
      this.sessionTherapySearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.sessionTherapyPersist.sessionTherapySearchText = value;
        this.searchSession_therapys();
      });
    } ngOnInit() {
   
      this.sessionTherapysSort.sortChange.subscribe(() => {
        this.sessionTherapysPaginator.pageIndex = 0;
        this.searchSession_therapys(true);
      });

      this.sessionTherapysPaginator.page.subscribe(() => {
        this.searchSession_therapys(true);
      });
      //start by loading items
      this.searchSession_therapys();
    }

  searchSession_therapys(isPagination:boolean = false): void {


    let paginator = this.sessionTherapysPaginator;
    let sorter = this.sessionTherapysSort;

    this.sessionTherapyIsLoading = true;
    this.sessionTherapySelection = [];

    this.sessionTherapyPersist.searchSessionTherapy(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SessionTherapySummaryPartialList) => {
      this.sessionTherapysData = partialList.data;
      if (partialList.total != -1) {
        this.sessionTherapysTotalCount = partialList.total;
      }
      this.sessionTherapyIsLoading = false;
    }, error => {
      this.sessionTherapyIsLoading = false;
    });

  }
  markOneItem(item: SessionTherapySummary) {
    if(!this.tcUtilsArray.containsId(this.sessionTherapySelection,item.id)){
          this.sessionTherapySelection = [];
          this.sessionTherapySelection.push(item);
        }
        else{
          this.sessionTherapySelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.sessionTherapySelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }