import {TCId} from "../tc/models";export class SessionTherapySummary extends TCId {
    date:number;
    other_service_id:string;
    status:number;
    name:string;
    session_type:string;
     
    }
    export class SessionTherapySummaryPartialList {
      data: SessionTherapySummary[];
      total: number;
    }
    export class SessionTherapyDetail extends SessionTherapySummary {
    }
    
    export class SessionTherapyDashboard {
      total: number;
    }