import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatSort} from '@angular/material/sort';
import { MatPaginator} from '@angular/material/paginator';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {SessionTherapyEditComponent} from "./session-therapy-edit/session-therapy-edit.component";
import {SessionTherapyPickComponent} from "./session-therapy-pick/session-therapy-pick.component";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";


@Injectable({
  providedIn: 'root'
})

export class SessionTherapyNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  session_therapysUrl(): string {
    return "/session_therapys";
  }

  session_therapyUrl(id: string): string {
    return "/session_therapys/" + id;
  }

  viewSessionTherapys(): void {
    this.router.navigateByUrl(this.session_therapysUrl());
  }

  viewSessionTherapy(id: string): void {
    this.router.navigateByUrl(this.session_therapyUrl(id));
  }

  editSessionTherapy(id: string): MatDialogRef<SessionTherapyEditComponent> {
    const dialogRef = this.dialog.open(SessionTherapyEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSessionTherapy(): MatDialogRef<SessionTherapyEditComponent> {
    const dialogRef = this.dialog.open(SessionTherapyEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSessionTherapys(selectOne: boolean=false): MatDialogRef<SessionTherapyPickComponent> {
      const dialogRef = this.dialog.open(SessionTherapyPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}