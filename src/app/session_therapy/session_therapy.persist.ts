import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnumTranslation} from "../tc/models";
import {SessionTherapyDashboard, SessionTherapyDetail, SessionTherapySummaryPartialList} from "./session_therapy.model";
import {session_therapy_session_type,session_therapy_status} from '../app.enums';


@Injectable({
  providedIn: 'root'
})
export class SessionTherapyPersist {
 sessionTherapySearchText: string = "";constructor(private http: HttpClient) {
  }
 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "session_therapy/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.sessionTherapySearchText;
    //add custom filters
    return fltrs;
  }
 
  searchSessionTherapy(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<SessionTherapySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("session_therapy", this.sessionTherapySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<SessionTherapySummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "session_therapy/do", new TCDoParam("download_all", this.filters()));
  }

  sessionTherapyDashboard(): Observable<SessionTherapyDashboard> {
    return this.http.get<SessionTherapyDashboard>(environment.tcApiBaseUri + "session_therapy/dashboard");
  }

  getSessionTherapy(id: string): Observable<SessionTherapyDetail> {
    return this.http.get<SessionTherapyDetail>(environment.tcApiBaseUri + "session_therapy/" + id);
  }
  sessionTherapynDashboard(): Observable<SessionTherapyDashboard> {
    return this.http.get<SessionTherapyDashboard>(environment.tcApiBaseUri + "session_therapy/dashboard");
  }

 
  addSessionTherapy(item: SessionTherapyDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + "session_therapy",
      item
    );
  }

  updateSessionTherapy(item: SessionTherapyDetail): Observable<SessionTherapyDetail> {
    return this.http.patch<SessionTherapyDetail>(
      environment.tcApiBaseUri + 'session_therapy/' + item.id,
      item
    );
  }

  deleteSessionTherapy(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'session_therapy/' + id);
  }
  sessionTherapynsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'session_therapy/do',
      new TCDoParam(method, payload)
    );
  }

  preAnsthesiaEvaluationDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'session_therapy/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'session_therapy/do',
      new TCDoParam('download', ids)
    );
  }

  session_therapy_session_type: TCEnumTranslation[] = [
    new TCEnumTranslation(session_therapy_session_type.group_therapy, "Group therapy"),
  ];
  session_therapy_status: TCEnumTranslation[] = [
    new TCEnumTranslation(session_therapy_status.active, "Active"),
    new TCEnumTranslation(session_therapy_status.deactive, "Deactive"),

  ];

 }