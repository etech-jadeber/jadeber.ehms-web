import { Injectable } from '@angular/core';
import { io } from "socket.io-client";

@Injectable({
    providedIn: 'root'
  })
export class SocketService {
  public socket:any;

  constructor() {
    this.socket = io('http://localhost:5000');

  }

  public startListening(): void {
    this.socket.on('roomUsers', (message) => {
      console.log('Received message from server:', message);
      // Do something with the received message
    });
  }

  public joinRoom(message: string): void {
    this.socket.emit('joinRoom', message);
  }

  public sendNotfication(message: any): void {
    this.socket.emit('msg', message);
  }
  public updateCount(room:string): void {
    this.socket.emit('updateCount',room);
  }
  public groupMemebrUpdate(message: any): void {
    this.socket.emit('groupMamber_update', message);
  }

  public listenToMsg() {
    this.socket.on('msg', (data) => {
      console.log('Received message from server:', data);
      return data
      // Do something with the received message
    });
  }


  public disconnect(): void {
    this.socket.disconnect();
  }
}
