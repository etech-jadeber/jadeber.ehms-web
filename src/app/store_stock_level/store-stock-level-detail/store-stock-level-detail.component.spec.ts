import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreStockLevelDetailComponent } from './store-stock-level-detail.component';

describe('StoreStockLevelDetailComponent', () => {
  let component: StoreStockLevelDetailComponent;
  let fixture: ComponentFixture<StoreStockLevelDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreStockLevelDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreStockLevelDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
