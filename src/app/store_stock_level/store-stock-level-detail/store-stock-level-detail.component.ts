import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {StoreStockLevelDetail} from "../store_stock_level.model";
import {StoreStockLevelPersist} from "../store_stock_level.persist";
import {StoreStockLevelNavigator} from "../store_stock_level.navigator";

export enum StoreStockLevelTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-store_stock_level-detail',
  templateUrl: './store-stock-level-detail.component.html',
  styleUrls: ['./store-stock-level-detail.component.scss']
})
export class StoreStockLevelDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  storeStockLevelIsLoading:boolean = false;
  
  StoreStockLevelTabs: typeof StoreStockLevelTabs = StoreStockLevelTabs;
  activeTab: StoreStockLevelTabs = StoreStockLevelTabs.overview;
  //basics
  storeStockLevelDetail: StoreStockLevelDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public storeStockLevelNavigator: StoreStockLevelNavigator,
              public  storeStockLevelPersist: StoreStockLevelPersist) {
    this.tcAuthorization.requireRead("store_stock_levels");
    this.storeStockLevelDetail = new StoreStockLevelDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("store_stock_levels");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.storeStockLevelIsLoading = true;
    this.storeStockLevelPersist.getStoreStockLevel(id).subscribe(storeStockLevelDetail => {
          this.storeStockLevelDetail = storeStockLevelDetail;
          this.storeStockLevelIsLoading = false;
        }, error => {
          console.error(error);
          this.storeStockLevelIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.storeStockLevelDetail.id);
  }
  editStoreStockLevel(): void {
    let modalRef = this.storeStockLevelNavigator.editStoreStockLevel(this.storeStockLevelDetail.id);
    modalRef.afterClosed().subscribe(storeStockLevelDetail => {
      TCUtilsAngular.assign(this.storeStockLevelDetail, storeStockLevelDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.storeStockLevelPersist.print(this.storeStockLevelDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print store_stock_level", true);
      });
    }

  back():void{
      this.location.back();
    }

}