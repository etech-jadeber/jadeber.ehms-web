import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreStockLevelEditComponent } from './store-stock-level-edit.component';

describe('StoreStockLevelEditComponent', () => {
  let component: StoreStockLevelEditComponent;
  let fixture: ComponentFixture<StoreStockLevelEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreStockLevelEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreStockLevelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
