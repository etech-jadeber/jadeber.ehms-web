import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { StoreStockLevelDetail } from '../store_stock_level.model';import { StoreStockLevelPersist } from '../store_stock_level.persist';import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemDetail } from 'src/app/items/item.model';
@Component({
  selector: 'app-store_stock_level-edit',
  templateUrl: './store-stock-level-edit.component.html',
  styleUrls: ['./store-stock-level-edit.component.scss']
})export class StoreStockLevelEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  storeStockLevelDetail: StoreStockLevelDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<StoreStockLevelEditComponent>,
              public  persist: StoreStockLevelPersist,
              public itemNavigator: ItemNavigator,
              public itemPersist: ItemPersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("store_stock_levels");
      this.title = this.appTranslation.getText("general","new") +  " " + "store_stock_level";
      this.storeStockLevelDetail = new StoreStockLevelDetail();
      this.storeStockLevelDetail.store_id = this.idMode.id
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("store_stock_levels");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "store_stock_level";
      this.isLoadingResults = true;
      this.persist.getStoreStockLevel(this.idMode.id).subscribe(storeStockLevelDetail => {
        this.storeStockLevelDetail = storeStockLevelDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addStoreStockLevel(this.storeStockLevelDetail).subscribe(value => {
      this.tcNotification.success("storeStockLevel added");
      this.storeStockLevelDetail.id = value.id;
      this.dialogRef.close(this.storeStockLevelDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateStoreStockLevel(this.storeStockLevelDetail).subscribe(value => {
      this.tcNotification.success("store_stock_level updated");
      this.dialogRef.close(this.storeStockLevelDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.storeStockLevelDetail == null){
            return false;
          }

        if (this.storeStockLevelDetail.store_id == null || this.storeStockLevelDetail.store_id  == "") {
            return false;
        } 
        if (this.storeStockLevelDetail.item_id == null || this.storeStockLevelDetail.item_id  == "") {
            return false;
        } 
        if (this.storeStockLevelDetail.min == null) {
            return false;
        }
        if (this.storeStockLevelDetail.max == null) {
            return false;
        }
        if (this.storeStockLevelDetail.max < this.storeStockLevelDetail.min){
          return false;
        }
return true
 }

 searchItem() {
  let dialogRef = this.itemNavigator.pickItems(true)
    dialogRef.afterClosed().subscribe((result: ItemDetail[]) => {
      if(result){
        this.storeStockLevelDetail.item_name = this.itemNavigator.itemName(result[0]);
        this.storeStockLevelDetail.item_id = result[0].id;
      }
    })
 }
 }