import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreStockLevelListComponent } from './store-stock-level-list.component';

describe('StoreStockLevelListComponent', () => {
  let component: StoreStockLevelListComponent;
  let fixture: ComponentFixture<StoreStockLevelListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreStockLevelListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreStockLevelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
