import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { StoreStockLevelSummary, StoreStockLevelSummaryPartialList } from '../store_stock_level.model';
import { StoreStockLevelPersist } from '../store_stock_level.persist';
import { StoreStockLevelNavigator } from '../store_stock_level.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
@Component({
  selector: 'app-store_stock_level-list',
  templateUrl: './store-stock-level-list.component.html',
  styleUrls: ['./store-stock-level-list.component.scss']
})

export class StoreStockLevelListComponent implements OnInit {
  storeStockLevelsData: StoreStockLevelSummary[] = [];
  storeStockLevelsTotalCount: number = 0;
  storeStockLevelSelectAll:boolean = false;
  storeStockLevelSelection: StoreStockLevelSummary[] = [];

 storeStockLevelsDisplayedColumns: string[] = ["select","action","item_id","min","max" ];
  storeStockLevelSearchTextBox: FormControl = new FormControl();
  storeStockLevelIsLoading: boolean = false; 
  
  @ViewChild(MatPaginator, {static: true}) storeStockLevelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) storeStockLevelsSort: MatSort;
  @Input() storeId: string;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemNavigator: ItemNavigator,
                public storeStockLevelPersist: StoreStockLevelPersist,
                public storeStockLevelNavigator: StoreStockLevelNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemPersist: ItemPersist,

    ) {
       this.tcAuthorization.requireRead("store_stock_levels");
       this.storeStockLevelSearchTextBox.setValue(storeStockLevelPersist.storeStockLevelSearchText);
      //delay subsequent keyup events
      this.storeStockLevelSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.storeStockLevelPersist.storeStockLevelSearchText = value;
        this.searchStoreStockLevels();
      });
    } 
    
    ngOnInit() {
      this.storeStockLevelsSort.sortChange.subscribe(() => {
        this.storeStockLevelsPaginator.pageIndex = 0;
        this.searchStoreStockLevels(true);
      });

      this.storeStockLevelsPaginator.page.subscribe(() => {
        this.searchStoreStockLevels(true);
      });
      //start by loading items
      this.searchStoreStockLevels();
    }

  searchStoreStockLevels(isPagination:boolean = false): void {


    let paginator = this.storeStockLevelsPaginator;
    let sorter = this.storeStockLevelsSort;

    this.storeStockLevelIsLoading = true;
    this.storeStockLevelSelection = [];

    this.storeStockLevelPersist.searchStoreStockLevel(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction, this.storeId).subscribe((partialList: StoreStockLevelSummaryPartialList) => {
      this.storeStockLevelsData = partialList.data;
      this.storeStockLevelsData.forEach(
        store => {
          this.itemPersist.getItem(store.item_id).subscribe(
            item => {
              store.item_name = this.itemNavigator.itemName(item);
            }
          )
        }
      )
      if (partialList.total != -1) {
        this.storeStockLevelsTotalCount = partialList.total;
      }
      this.storeStockLevelIsLoading = false;
    }, error => {
      this.storeStockLevelIsLoading = false;
    });

  } 
  
  downloadStoreStockLevels(): void {
    if(this.storeStockLevelSelectAll){
         this.storeStockLevelPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download store_stock_level", true);
      });
    }
    else{
        this.storeStockLevelPersist.download(this.tcUtilsArray.idsList(this.storeStockLevelSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download store_stock_level",true);
            });
        }
  }
addStoreStockLevel(): void {
    let dialogRef = this.storeStockLevelNavigator.addStoreStockLevel(this.storeId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchStoreStockLevels();
      }
    });
  }

  editStoreStockLevel(item: StoreStockLevelSummary) {
    let dialogRef = this.storeStockLevelNavigator.editStoreStockLevel(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteStoreStockLevel(item: StoreStockLevelSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("store_stock_level");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.storeStockLevelPersist.deleteStoreStockLevel(item.id).subscribe(response => {
          this.tcNotification.success("store_stock_level deleted");
          this.searchStoreStockLevels();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}