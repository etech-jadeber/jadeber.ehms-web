import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreStockLevelPickComponent } from './store-stock-level-pick.component';

describe('StoreStockLevelPickComponent', () => {
  let component: StoreStockLevelPickComponent;
  let fixture: ComponentFixture<StoreStockLevelPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreStockLevelPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreStockLevelPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
