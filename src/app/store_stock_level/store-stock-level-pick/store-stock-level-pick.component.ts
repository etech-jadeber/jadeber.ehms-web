import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { StoreStockLevelSummary, StoreStockLevelSummaryPartialList } from '../store_stock_level.model';
import { StoreStockLevelPersist } from '../store_stock_level.persist';
import { StoreStockLevelNavigator } from '../store_stock_level.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-store_stock_level-pick',
  templateUrl: './store-stock-level-pick.component.html',
  styleUrls: ['./store-stock-level-pick.component.scss']
})export class StoreStockLevelPickComponent implements OnInit {
  storeStockLevelsData: StoreStockLevelSummary[] = [];
  storeStockLevelsTotalCount: number = 0;
  storeStockLevelSelectAll:boolean = false;
  storeStockLevelSelection: StoreStockLevelSummary[] = [];

 storeStockLevelsDisplayedColumns: string[] = ["select", ,"store_id","item_id","min","max" ];
  storeStockLevelSearchTextBox: FormControl = new FormControl();
  storeStockLevelIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) storeStockLevelsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) storeStockLevelsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public storeStockLevelPersist: StoreStockLevelPersist,
                public storeStockLevelNavigator: StoreStockLevelNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<StoreStockLevelPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("store_stock_levels");
       this.storeStockLevelSearchTextBox.setValue(storeStockLevelPersist.storeStockLevelSearchText);
      //delay subsequent keyup events
      this.storeStockLevelSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.storeStockLevelPersist.storeStockLevelSearchText = value;
        this.searchStore_stock_levels();
      });
    } ngOnInit() {
   
      this.storeStockLevelsSort.sortChange.subscribe(() => {
        this.storeStockLevelsPaginator.pageIndex = 0;
        this.searchStore_stock_levels(true);
      });

      this.storeStockLevelsPaginator.page.subscribe(() => {
        this.searchStore_stock_levels(true);
      });
      //start by loading items
      this.searchStore_stock_levels();
    }

  searchStore_stock_levels(isPagination:boolean = false): void {


    let paginator = this.storeStockLevelsPaginator;
    let sorter = this.storeStockLevelsSort;

    this.storeStockLevelIsLoading = true;
    this.storeStockLevelSelection = [];

    this.storeStockLevelPersist.searchStoreStockLevel(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: StoreStockLevelSummaryPartialList) => {
      this.storeStockLevelsData = partialList.data;
      if (partialList.total != -1) {
        this.storeStockLevelsTotalCount = partialList.total;
      }
      this.storeStockLevelIsLoading = false;
    }, error => {
      this.storeStockLevelIsLoading = false;
    });

  }
  markOneItem(item: StoreStockLevelSummary) {
    if(!this.tcUtilsArray.containsId(this.storeStockLevelSelection,item.id)){
          this.storeStockLevelSelection = [];
          this.storeStockLevelSelection.push(item);
        }
        else{
          this.storeStockLevelSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.storeStockLevelSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }