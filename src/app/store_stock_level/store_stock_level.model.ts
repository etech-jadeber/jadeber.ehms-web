import {TCId} from "../tc/models";

export class StoreStockLevelSummary extends TCId {
    store_id:string;
    item_id:string;
    min:number;
    max:number;
    store_name: string;
    item_name: string;
     
    }
    export class StoreStockLevelSummaryPartialList {
      data: StoreStockLevelSummary[];
      total: number;
    }
    export class StoreStockLevelDetail extends StoreStockLevelSummary {
    }
    
    export class StoreStockLevelDashboard {
      total: number;
    }