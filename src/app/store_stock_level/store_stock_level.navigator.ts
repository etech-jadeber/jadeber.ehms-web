import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {StoreStockLevelEditComponent} from "./store-stock-level-edit/store-stock-level-edit.component";
import {StoreStockLevelPickComponent} from "./store-stock-level-pick/store-stock-level-pick.component";


@Injectable({
  providedIn: 'root'
})

export class StoreStockLevelNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  storeStockLevelsUrl(): string {
    return "/store_stock_levels";
  }

  storeStockLevelUrl(id: string): string {
    return "/store_stock_levels/" + id;
  }

  viewStoreStockLevels(): void {
    this.router.navigateByUrl(this.storeStockLevelsUrl());
  }

  viewStoreStockLevel(id: string): void {
    this.router.navigateByUrl(this.storeStockLevelUrl(id));
  }

  editStoreStockLevel(id: string): MatDialogRef<StoreStockLevelEditComponent> {
    const dialogRef = this.dialog.open(StoreStockLevelEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addStoreStockLevel(parentId: string): MatDialogRef<StoreStockLevelEditComponent> {
    const dialogRef = this.dialog.open(StoreStockLevelEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickStoreStockLevels(selectOne: boolean=false): MatDialogRef<StoreStockLevelPickComponent> {
      const dialogRef = this.dialog.open(StoreStockLevelPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}