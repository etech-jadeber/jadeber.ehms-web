import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {StoreStockLevelDashboard, StoreStockLevelDetail, StoreStockLevelSummaryPartialList} from "./store_stock_level.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class StoreStockLevelPersist {
 storeStockLevelSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.storeStockLevelSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchStoreStockLevel(pageSize: number, pageIndex: number, sort: string, order: string, parentId: string = null): Observable<StoreStockLevelSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("store_stock_level", this.storeStockLevelSearchText, pageSize, pageIndex, sort, order);
    if (parentId){
        url = TCUtilsString.appendUrlParameter(url, "store_id", parentId)
    }
    return this.http.get<StoreStockLevelSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "store_stock_level/do", new TCDoParam("download_all", this.filters()));
  }

  storeStockLevelDashboard(): Observable<StoreStockLevelDashboard> {
    return this.http.get<StoreStockLevelDashboard>(environment.tcApiBaseUri + "store_stock_level/dashboard");
  }

  getStoreStockLevel(id: string): Observable<StoreStockLevelDetail> {
    return this.http.get<StoreStockLevelDetail>(environment.tcApiBaseUri + "store_stock_level/" + id);
  } addStoreStockLevel(item: StoreStockLevelDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "store_stock_level/", item);
  }

  updateStoreStockLevel(item: StoreStockLevelDetail): Observable<StoreStockLevelDetail> {
    return this.http.patch<StoreStockLevelDetail>(environment.tcApiBaseUri + "store_stock_level/" + item.id, item);
  }

  deleteStoreStockLevel(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "store_stock_level/" + id);
  }
 storeStockLevelsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "store_stock_level/do", new TCDoParam(method, payload));
  }

  storeStockLevelDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "store_stock_levels/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "store_stock_level/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "store_stock_level/" + id + "/do", new TCDoParam("print", {}));
  }


}