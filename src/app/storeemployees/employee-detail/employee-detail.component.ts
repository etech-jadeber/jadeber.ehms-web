import { AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../tc/authorization";
import { TCUtilsAngular } from "../../tc/utils-angular";
import { TCNotification } from "../../tc/notification";
import { TCUtilsArray } from "../../tc/utils-array";
import { TCNavigator } from "../../tc/navigator";
import { JobPersist } from "../../tc/jobs/job.persist";
import { AppTranslation } from "../../app.translation";


import { EmployeeDetail } from "../employee.model";
import { EmployeePersist } from "../employee.persist";
import { EmployeeNavigator } from "../employee.navigator";
import { PatientTabs } from "../../patients/patient-detail/patient-detail.component";
import { TCUtilsDate } from "../../tc/utils-date";
import { StoreSummary, StoreSummaryPartialList } from "../../stores/store.model";
import { StorePersist } from "../../stores/store.persist";
import { StoreNavigator } from "../../stores/store.navigator";
import { TCUtilsString } from "../../tc/utils-string";
import { TCId } from "../../tc/models";
import { tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';


export enum PaginatorIndexes {

}

export enum EmployeeTabs {
  overview
}


@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit, OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  employeeLoading: boolean = false;
  stores: StoreSummary[] = [];
  //basics
  employeeDetail: EmployeeDetail;
  employeeTabs:typeof tabs=tabs;
  activeTab:number=tabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsString: TCUtilsString,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public employeeNavigator: EmployeeNavigator,
    private storePersist: StorePersist,
    public storeNavigator: StoreNavigator,
    public employeePersist: EmployeePersist,
    public menuState: MenuState) {

    this.tcAuthorization.requireRead("store_employees");
    this.employeeDetail = new EmployeeDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("store_employees");
    this.activeTab=tabs.overview;
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
      this.loadStores();
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.employeeLoading = true;
    this.employeePersist.getEmployee(id).subscribe(employeeDetail => {
      this.employeeDetail = employeeDetail;
      this.menuState.getDataResponse(this.employeeDetail);

      this.employeeLoading = false;
    }, error => {
      console.error(error);
      this.employeeLoading = false;
    });
  }

  reload() {
    this.loadDetails(this.employeeDetail.id);
  }

  editEmployee(): void {
    let modalRef = this.employeeNavigator.editEmployee(this.employeeDetail.id);
    modalRef.afterClosed().subscribe(modifiedEmployeeDetail => {
      TCUtilsAngular.assign(this.employeeDetail, modifiedEmployeeDetail);
    }, error => {
      console.error(error);
    });
  }

  printEmployee(): void {
    this.employeePersist.print(this.employeeDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print employee", true);
    });
  }

  back(): void {
    this.location.back();
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

  createUser() {
    this.employeePersist
      .employeeDo(this.employeeDetail.id, "account_create", {})
      .subscribe((response) => {
        this.employeeDetail.login_id = (<TCId>response).id;
        this.tcNotification.success("account created");
        this.reload();
      });
  }

  removeUser() {
    this.employeePersist
      .employeeDo(this.employeeDetail.id, "remove_account", {})
      .subscribe((response) => {
        this.tcNotification.success("account removed");
        this.reload();
      });
  }
  activateUser() {
    this.employeePersist
      .employeeDo(this.employeeDetail.id, "activate_user", {})
      .subscribe((response) => {
        this.tcNotification.success("account Activated");
        this.reload();
      });
  }

}
