import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {EmployeeDetail} from "../employee.model";
import {EmployeePersist} from "../employee.persist";
import {TCUtilsDate} from "../../tc/utils-date";
import {StoreSummary} from "../../stores/store.model";
import {StoreNavigator} from "../../stores/store.navigator";
import { StorePersist } from 'src/app/stores/store.persist';


@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  employeeDetail: EmployeeDetail;
  birth_date: Date;
  storeName: String;
  gender: string = 'male';

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<EmployeeEditComponent>,
              public persist: EmployeePersist,
              public storePersist: StorePersist,

              public tcUtilsDate: TCUtilsDate,
              private storeNavigator: StoreNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate("store_employees");
      this.title = this.appTranslation.getText("general", "new") + " " + this.appTranslation.getText("employee", "store_employee");
      this.employeeDetail = new EmployeeDetail();
      this.employeeDetail.email = "";
      //set necessary defaults
      this.employeeDetail.is_male = this.gender == 'male';
      this.employeeDetail.is_store_manager = false;
    } else {
      this.tcAuthorization.requireUpdate("store_employees");
      this.title = this.appTranslation.getText("general", "edit") + " " + this.appTranslation.getText("general", "store_employee");
      this.isLoadingResults = true;
      this.persist.getEmployee(this.idMode.id).subscribe(employeeDetail => {
        this.employeeDetail = employeeDetail;

        this.gender = this.employeeDetail.is_male ? 'male' : 'female';
        this.storePersist.getStore(employeeDetail.store_id).subscribe(value=>{
            this.storeName=value.name
        })
        this.transformDates(true);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {

    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.addEmployee(this.employeeDetail).subscribe(value => {
      this.tcNotification.success("Employee added");
      this.employeeDetail.id = value.id;
      this.dialogRef.close(this.employeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    console.log("store",this.employeeDetail)

    this.transformDates(false);
    this.isLoadingResults = true;
    this.persist.updateEmployee(this.employeeDetail).subscribe(value => {
      this.tcNotification.success("Employee updated");
      this.dialogRef.close(this.employeeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit(): boolean {
    if (this.employeeDetail == null) {
      return false;
    }

    if (this.employeeDetail.first_name == null || this.employeeDetail.first_name == "") {
      return false;
    }

    if (this.employeeDetail.last_name == null || this.employeeDetail.last_name == "") {
      return false;
    }

    if (this.employeeDetail.middle_name == null || this.employeeDetail.middle_name == "") {
      return false;
    }

    // if (this.employeeDetail.email == null || this.employeeDetail.email == "") {
    //   return false;
    // }

    if (this.employeeDetail.telephone == null || this.employeeDetail.telephone == "") {
      return false;
    }

    if (this.employeeDetail.address == null || this.employeeDetail.address == "") {
      return false;
    }

    if(this.employeeDetail.store_id == null || this.birth_date == null){
      return false;
    }


    return true;
  }

  transformDates(todate: boolean) {
    if (todate) {
      this.birth_date = this.tcUtilsDate.toDate(this.employeeDetail.birth_date);
    } else {
      this.employeeDetail.birth_date = this.tcUtilsDate.toTimeStamp(
        this.birth_date
      );
    }
  }

  searchStore(){
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.storeName = result[0].name;
        this.employeeDetail.store_id = result[0].id;
      }
    });
  }

  changeGender(newGender: string){
    this.employeeDetail.is_male = newGender==='male';
  }


}
