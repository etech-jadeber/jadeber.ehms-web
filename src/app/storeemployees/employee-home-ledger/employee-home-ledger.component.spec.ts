import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmployeeHomeLedgerComponent } from './employee-home-ledger.component';

describe('EmployeeHomeLedgerComponent', () => {
  let component: EmployeeHomeLedgerComponent;
  let fixture: ComponentFixture<EmployeeHomeLedgerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeHomeLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeHomeLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
