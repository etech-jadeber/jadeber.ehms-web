import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {
  Item_Ledger_EntrySummary,
  Item_Ledger_EntrySummaryPartialList
} from "../../item_ledger_entrys/item_ledger_entry.model";
import {EmployeeSelfPersist} from "../employee-self.persist";
import {ItemPersist} from "../../items/item.persist";
import {Item_Ledger_EntryPersist} from "../../item_ledger_entrys/item_ledger_entry.persist";
import {ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {StorePersist} from "../../stores/store.persist";

@Component({
  selector: 'app-employee-home-ledger',
  templateUrl: './employee-home-ledger.component.html',
  styleUrls: ['./employee-home-ledger.component.css']
})
export class EmployeeHomeLedgerComponent implements OnInit {

  item_ledger_entrysData: Item_Ledger_EntrySummary[] = [];
  item_ledger_entrysTotalCount: number = 0;
  item_ledger_entrysSelectAll: boolean = false;
  item_ledger_entrysSelection: Item_Ledger_EntrySummary[] = [];
  items: ItemSummary[] = [];
  stores: StoreSummary[] = [];

  item_ledger_entrysDisplayedColumns: string[] = ["select", "entry_type", "item_type", "item_id", "quantity", "store_id", "batch", "expire_date", "description", "registration_date"];
  item_ledger_entrysSearchTextBox: FormControl = new FormControl();
  item_ledger_entrysIsLoading: boolean = true;

  @ViewChild(MatPaginator, {static: true}) item_ledger_entrysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) item_ledger_entrysSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public employeeSelfPersist: EmployeeSelfPersist,
              public ledgerEntryPersist: Item_Ledger_EntryPersist,
              public itemsPersist: ItemPersist,
              public storePersist: StorePersist,
              public jobPersist: JobPersist,
  ) {
    this.item_ledger_entrysSearchTextBox.setValue(employeeSelfPersist.item_ledger_entrySearchText);
    //delay subsequent keyup events
    this.item_ledger_entrysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.employeeSelfPersist.item_ledger_entrySearchText = value;
      this.searchItem_Ledger_Entrys();
    });
  }

  ngOnInit() {

    this.item_ledger_entrysSort.sortChange.subscribe(() => {
      this.item_ledger_entrysPaginator.pageIndex = 0;
      this.searchItem_Ledger_Entrys(true);
    });

    this.item_ledger_entrysPaginator.page.subscribe(() => {
      this.searchItem_Ledger_Entrys(true);
    });
    //start by loading items
    this.searchItem_Ledger_Entrys();
    this.loadStores();
  }

  searchItem_Ledger_Entrys(isPagination: boolean = false): void {

    let paginator = this.item_ledger_entrysPaginator;
    let sorter = this.item_ledger_entrysSort;

    this.item_ledger_entrysIsLoading = true;
    this.item_ledger_entrysSelection = [];

    this.employeeSelfPersist.searchItem_Ledger_Entry(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: Item_Ledger_EntrySummaryPartialList) => {
      this.item_ledger_entrysData = partialList.data;
      this.item_ledger_entrysData.forEach((data, idx)=>{
        this.itemsPersist.getItem(data.item_id).subscribe(result=>{
          this.items.push(result);
        })
      });
      if (partialList.total != -1) {
        this.item_ledger_entrysTotalCount = partialList.total;
      }
      this.item_ledger_entrysIsLoading = false;
    }, error => {
      this.item_ledger_entrysIsLoading = false;
    });

  }

  downloadItem_Ledger_Entrys(): void {

  }

  back(): void {
    this.location.back();
  }


  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

}
