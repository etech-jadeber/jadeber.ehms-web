import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmployeeHomeRequestEditComponent } from './employee-home-request-edit.component';

describe('EmployeeHomeRequestEditComponent', () => {
  let component: EmployeeHomeRequestEditComponent;
  let fixture: ComponentFixture<EmployeeHomeRequestEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeHomeRequestEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeHomeRequestEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
