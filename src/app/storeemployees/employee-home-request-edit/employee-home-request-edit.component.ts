import {Component, Inject, OnInit} from '@angular/core';
import {RequestDetail} from "../../requests/request.model";
import {ItemSummary} from "../../items/item.model";
import {StoreDetail, StoreSummary} from "../../stores/store.model";
import {TCAuthorization} from "../../tc/authorization";
import {TCNotification} from "../../tc/notification";
import {TCUtilsString} from "../../tc/utils-string";
import {TCUtilsArray} from "../../tc/utils-array";
import {AppTranslation} from "../../app.translation";
import {RequestPersist} from "../../requests/request.persist";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {TCUtilsDate} from "../../tc/utils-date";
import {StoreSpecialNavigator} from "../../stores/store.specialnavigator";
import {ItemNavigator} from "../../items/item.navigator";
import {TCIdMode, TCModalModes, TCParentChildIds} from "../../tc/models";
import {request_status} from "../../app.enums";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {EmployeeSelfPersist} from "../employee-self.persist";
import { EmployeeSummary, RequestCaseDetail } from '../employee.model';
import { EmployeePersist } from '../employee.persist';
import { EmployeeCasePersist } from 'src/app/cases/EmployeeCasePersist';
import { DispatchOrRejectNavigator } from 'src/app/dispatch_or_reject/dispatch_or_reject.navigator';

@Component({
  selector: 'app-employee-home-request-edit',
  templateUrl: './employee-home-request-edit.component.html',
  styleUrls: ['./employee-home-request-edit.component.css']
})
export class EmployeeHomeRequestEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  requestDetail: RequestDetail;
  fullnameOfRequester:string = "";
  request_day: Date = new Date();
  itemList: ItemSummary[] = [];
  storeList: StoreSummary[] = [];
  storeDetail: StoreDetail;
  requesterStoreName: string = "";
  requesteeStoreName: string = "";
  itemName: string = "";
  itemType: string = "";
  caseDetail:RequestCaseDetail
  employeeDetail: EmployeeSummary;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public tcUtilsArray: TCUtilsArray,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<EmployeeHomeRequestEditComponent>,
              public employeeSelfPersist: EmployeeSelfPersist,
              public requestPersist: RequestPersist,
              public itemPersist:ItemPersist,
              public storePersist: StorePersist,
              public tcUtilsDate: TCUtilsDate,
              public storeNavigator: StoreSpecialNavigator,
              public itemNavigator: ItemNavigator,
              public employeePersist: EmployeePersist,
              private empoyeeCasePersist :EmployeeCasePersist,
              private dispatchOrRejectNavigator: DispatchOrRejectNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCParentChildIds) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

 
  isNew(): boolean {
    return this.idMode.context["mode"]  === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.context["mode"]  === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.context["mode"]  === TCModalModes.CASE;
  }

  ngOnInit() {
    this.loadDetails();
    if(this.isCase()){
      
      
      this.title = this.appTranslation.getText("general","new") +  " Request";
      this.requestDetail = new RequestDetail(); 
     
      this.employeePersist
      .getCaseDetail(this.idMode.childId)
      .subscribe((caseDetail:RequestCaseDetail) => 
      {
        this.caseDetail = caseDetail;
        this.requestDetail = caseDetail.payload;   
      
        
        this.searchRequesterEmployee(this.requestDetail.requester_store_manager_id);
        this.itemPersist.getItem(this.requestDetail.item_id).subscribe(res=>{
          this.itemName  = res.name;
        });
      },
      err=>{
        console.log(err);
        
      });
      
    }
    else if (this.isNew()) {
      this.title = this.appTranslation.getText("general","new") +  " Request";
      this.requestDetail = new RequestDetail();
      //set necessary defaults
      this.requestDetail.request_day = this.tcUtilsDate.toTimeStamp(this.tcUtilsDate.now());
      this.requestDetail.status = request_status.submitted;
      this.transformDates(false);
    } else {
      this.title = this.appTranslation.getText("general","edit") +  " Request";
      this.isLoadingResults = true;
      this.requestPersist.getRequest(this.idMode.childId).subscribe(requestDetail => {
        this.requestDetail = requestDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(true);

    this.employeeSelfPersist.addRequest(this.requestDetail).subscribe(value => {
      this.tcNotification.success("Request case added");
      this.dialogRef.close(this.requestDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.employeeSelfPersist.updateRequest(this.requestDetail).subscribe(value => {
      this.tcNotification.success("Request updated");
      this.dialogRef.close(this.requestDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
    if (this.requestDetail == null){
      return false;
    }
    if(this.requestDetail.item_id == null){
      return false;
    }
    if(this.requestDetail.quantity == null){
      return false;
    }
    if(this.requestDetail.unit_id == null){
      return false;
    }
    if(this.requestDetail.status == null){
      return false;
    }

    return true;
  }

  transformDates(todate: boolean) {
    if (todate) {
      this.requestDetail.request_day = this.tcUtilsDate.toTimeStamp(this.request_day);
    } else {
      this.request_day = this.tcUtilsDate.toDate(this.requestDetail.request_day);
    }
  }


  getItem(itemId: string): string{
    let item = this.tcUtilsArray.getById(
      this.itemList,
      itemId
    );
    if(item){
      return item.item_type;
    }
  }

  searchItem(){
    let dialogRef = this.itemNavigator.pickItems(true);
    dialogRef.afterClosed().subscribe((result: ItemSummary[]) => {
      if (result) {
        this.itemName = result[0].name;
        this.requestDetail.item_id = result[0].id;
      }
    });
  }

  loadDetails() {
    this.employeePersist.getEmployeeSelf().subscribe(result => {
        this.employeeDetail = result;
        this.loadStore();
      }
      , error => {
        console.error(error);
      });
  }

  loadStore() {
    this.storePersist.storeDo(this.employeeDetail.store_id, "get_store", {}).subscribe(result => {
      this.requestDetail.requestee_store_id = (<StoreDetail>result).parent_store_id;
    })
  }

 requsterEmployeeSum :EmployeeSummary 
  searchRequesterEmployee(id:string):void{
    this.employeePersist.getEmployee(id).subscribe(result=>{
      this.requsterEmployeeSum =result;
    },
    err=>{
      console.log(err);
      
    })

  }


  acceptRequest():void{

    let dialogRef = this.dispatchOrRejectNavigator.editDispatchOrReject(
      this.requestDetail.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
         this.requestDetail.case_id = this.caseDetail.id;
    this.employeePersist.acceptRequest(this.requestDetail,"accept_request").subscribe(
      res=>{
        this.tcNotification.success("Sucessfully added");
        this.dialogRef.close();
      }
    )
      }
    });

  }

  rejectRequest():void{
    this.requestDetail.case_id = this.caseDetail.id
    this.employeePersist.acceptRequest(this.requestDetail,"reject_request").subscribe(
      res=>{
        this.tcNotification.success("Sucessfully Rejected");
        this.dialogRef.close();
      }
    )
  }
}
