import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {RequestSummary, RequestSummaryPartialList} from "../../requests/request.model";
import {EmployeeSelfPersist} from "../employee-self.persist";
import {EmployeeDetail, EmployeeSummary, EmployeeSummaryPartialList} from "../employee.model";
import {EmployeeSelfNavigator} from "../employee-self.navigator";
import {RequestPersist} from "../../requests/request.persist";
import {ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {EmployeePersist} from "../employee.persist";
import {request_status} from "../../app.enums";
import { RequestDetail } from '../../requests/request.model';
import { DispatchOrRejectNavigator } from 'src/app/dispatch_or_reject/dispatch_or_reject.navigator';
import { DispatchOrRejectDetail } from 'src/app/dispatch_or_reject/dispatch_or_reject.model';
import { TCAppInit } from 'src/app/tc/app-init';
import { DispatchOrRejectPersist } from 'src/app/dispatch_or_reject/dispatch_or_reject.persist';


@Component({
  selector: 'app-employee-home-request-list',
  templateUrl: './employee-home-request-list.component.html',
  styleUrls: ['./employee-home-request-list.component.css'],
})
export class EmployeeHomeRequestListComponent implements OnInit {
  requestsData: RequestSummary[] = [];
  requestsTotalCount: number = 0;
  requestsSelectAll: boolean = false;
  requestsSelection: RequestSummary[] = [];
  employeeDetail: EmployeeSummary;
  request_status = request_status;
  dispatchOrRejectDetail: DispatchOrRejectDetail;

  requestsDisplayedColumns: string[] = [
    'select',
    'action',
    'item_type',
    'item_id',
    'unit_id',
    'quantity',
    'requester_store_manager_id',
    'requestee_store_id',
    'request_day',
    'status',
  ];
  requestsSearchTextBox: FormControl = new FormControl();
  requestsIsLoading: boolean = false;
  items: ItemSummary[] = [];
  stores: StoreSummary[] = [];
  employees: EmployeeSummary[] = [];

  @ViewChild(MatPaginator, { static: true }) requestsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) requestsSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public employeeSelfPersist: EmployeeSelfPersist,
    public employeeSelfNavigator: EmployeeSelfNavigator,
    public requestPersist: RequestPersist,
    public jobPersist: JobPersist,
    public itemPersist: ItemPersist,
    public storePersist: StorePersist,
    public employeePersist: EmployeePersist,
    public dispatchOrRejectNavigator: DispatchOrRejectNavigator,
    public dispatchOrRejectPersist: DispatchOrRejectPersist
  ) {
    // this.tcAuthorization.requireRead("requests");
    this.requestsSearchTextBox.setValue(employeeSelfPersist.requestSearchText);
    //delay subsequent keyup events
    this.requestsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.employeeSelfPersist.requestSearchText = value;
        this.searchRequests();
      });
  }

  ngOnInit() {
    this.requestsSort.sortChange.subscribe(() => {
      this.requestsPaginator.pageIndex = 0;
      this.searchRequests(true);
    });

    this.requestsPaginator.page.subscribe(() => {
      this.searchRequests(true);
    });
          this.dispatchOrRejectDetail = new DispatchOrRejectDetail();

    //start by loading items
    this.searchRequests();
    this.loadDetails();

    this.loadItems();
    this.loadStores();
    this.loadEmployees();
  }

  searchRequests(isPagination: boolean = false): void {

    let paginator = this.requestsPaginator;
    let sorter = this.requestsSort;

    this.requestsIsLoading = true;
    this.requestsSelection = [];

    this.employeeSelfPersist
      .searchRequest(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: RequestSummaryPartialList) => {
          this.requestsData = partialList.data;
          this.requestsData.forEach((data, idx)=>{
            this.itemPersist.getItem(data.item_id).subscribe(result=>{
              this.items.push(result);
            })
          });
          if (partialList.total != -1) {
            this.requestsTotalCount = partialList.total;
          }
          this.requestsIsLoading = false;
        },
        (error) => {
          this.requestsIsLoading = false;
        }
      );
  }

  loadDetails() {
    this.employeeSelfPersist.getEmployeeSelf().subscribe(
      (result) => {
        this.employeeDetail = result;
      },
      (error) => {
        console.error(error);
      }
    );
  }

  downloadRequests(): void {}

  addRequest(): void {
    let dialogRef = this.employeeSelfNavigator.addRequest();
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchRequests();
      }
    });
  }

  editRequest(item: RequestSummary) {
    let dialogRef = this.employeeSelfNavigator.editRequest(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
        this.searchRequests();
      }
    });
  }

  deleteRequest(request: RequestDetail): void {
    let dialogRef = this.dispatchOrRejectNavigator.addDispatchOrReject(
      request.id
    );
  
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchRequests();
      }
    });

    // request.status = 102;

    // let dialogRef = this.tcNavigator.confirmDeletion('Request');
    // dialogRef.afterClosed().subscribe((result) => {
    //   if (result) {

    //     // this.requestPersist.deleteRequest(request.id).subscribe(
    //     //   (response) => {
    //     //     this.tcNotification.success('Request deleted');
    //     //     this.searchRequests();
    //     //   },
    //     //   (error) => {}
    //     // );
    //   }
    // });
  }

  back(): void {
    this.location.back();
  }

  loadEmployees() {
    this.employeePersist
      .employeesDo('get_all_employees', '')
      .subscribe((result) => {
        this.employees = (result as EmployeeSummaryPartialList).data;
      });
  }

  getEmployeeFullName(employeeId: string) {
    const employee: EmployeeDetail = this.tcUtilsArray.getById(
      this.employees,
      employeeId
    );
    if (employee) {
      return `${employee.first_name} ${employee.last_name}`;
    }
  }

  loadItems() {
    this.itemPersist.itemsDo('get_all_items', '').subscribe((result) => {
      this.items = (result as ItemSummaryPartialList).data;
    });
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

  approveRequest(request: RequestDetail) {

    this.dispatchOrRejectDetail.item_id = request.item_id;
    this.dispatchOrRejectDetail.request_id = request.id;
    this.dispatchOrRejectDetail.store_id = request.requestee_store_id;
    this.dispatchOrRejectDetail.batch_no = 0;
    this.dispatchOrRejectDetail.employee_id = TCAppInit.userInfo.user_id;
    this.dispatchOrRejectDetail.status = 101;
    this.dispatchOrRejectDetail.reason = '';

    this.dispatchOrRejectPersist
      .addDispatchOrReject(this.dispatchOrRejectDetail)
      .subscribe(
        (value) => {
          // this.tcNotification.success('dispatchOrReject added');
          // this.dispatchOrRejectDetail.id = value.id;
        },
        (error) => {}
      );

      request.status = 101;

    this.employeeSelfPersist.updateRequest(request).subscribe(
      (value) => {
        this.tcNotification.success('Request approved');
        // this.dialogRef.close(this.requestDetail);
      },
      (error) => {
        console.error("error is ", error)
        // this.isLoadingResults = false;
      }
    );
  }
}
