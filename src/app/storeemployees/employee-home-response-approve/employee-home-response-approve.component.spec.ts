import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmployeeHomeResponseApproveComponent } from './employee-home-response-approve.component';

describe('EmployeeHomeResponseApproveComponent', () => {
  let component: EmployeeHomeResponseApproveComponent;
  let fixture: ComponentFixture<EmployeeHomeResponseApproveComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeHomeResponseApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeHomeResponseApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
