import {Component, OnInit} from '@angular/core';
import {ItemLedgerEntryPartial} from "../../item_ledger_entrys/item_ledger_entry.model";
import {AppTranslation} from "../../app.translation";
import {Item_Ledger_EntryPersist} from "../../item_ledger_entrys/item_ledger_entry.persist";
import {MatDialogRef} from "@angular/material/dialog";
import {TCUtilsDate} from "../../tc/utils-date";

@Component({
  selector: 'app-employee-home-response-approve',
  templateUrl: './employee-home-response-approve.component.html',
  styleUrls: ['./employee-home-response-approve.component.css']
})
export class EmployeeHomeResponseApproveComponent implements OnInit {


  itemLedgerEntryPartial: ItemLedgerEntryPartial;
  minDate: any = new Date();
  expireDate;

  constructor(
    public appTranslation: AppTranslation,
    public persist: Item_Ledger_EntryPersist,
    public tcDateUtils: TCUtilsDate,
    public dialogRef: MatDialogRef<EmployeeHomeResponseApproveComponent>,
  ) {
  }

  ngOnInit() {
    this.itemLedgerEntryPartial = new ItemLedgerEntryPartial();
    this.itemLedgerEntryPartial.entry_type = -1;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  canSubmit(): boolean {
    if (this.itemLedgerEntryPartial == null) {
      return false;
    }

    if(this.expireDate == null){
      return false;
    }

    if (this.itemLedgerEntryPartial.entry_type == -1) {
      return false;
    }

    if (this.itemLedgerEntryPartial.entry_type == -1) {
      return false;
    }

    if (this.itemLedgerEntryPartial.description == null || this.itemLedgerEntryPartial.description == "") {
      return false;
    }

    if (this.itemLedgerEntryPartial.batch == null) {
      return false;
    }

    return true;
  }

  onSubmit() {
    this.itemLedgerEntryPartial.expire_date = this.tcDateUtils.toTimeStamp(this.expireDate);
    this.dialogRef.close(this.itemLedgerEntryPartial);
  }

}
