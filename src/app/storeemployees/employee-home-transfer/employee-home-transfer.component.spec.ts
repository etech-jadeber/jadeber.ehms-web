import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmployeeHomeTransferComponent } from './employee-home-transfer.component';

describe('EmployeeHomeTransferComponent', () => {
  let component: EmployeeHomeTransferComponent;
  let fixture: ComponentFixture<EmployeeHomeTransferComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeHomeTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeHomeTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
