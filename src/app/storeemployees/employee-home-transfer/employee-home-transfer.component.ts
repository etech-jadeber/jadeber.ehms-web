import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import {TransferSummary, TransferSummaryPartialList} from "../../transfers/transfer.model";
import {EmployeeSelfPersist} from "../employee-self.persist";
import {ItemSummary, ItemSummaryPartialList} from "../../items/item.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {
  Item_Ledger_EntrySummary,
  Item_Ledger_EntrySummaryPartialList
} from "../../item_ledger_entrys/item_ledger_entry.model";
import {ItemPersist} from "../../items/item.persist";
import {StorePersist} from "../../stores/store.persist";
import {Item_Ledger_EntryPersist} from "../../item_ledger_entrys/item_ledger_entry.persist";


@Component({
  selector: 'app-employee-home-transfer',
  templateUrl: './employee-home-transfer.component.html',
  styleUrls: ['./employee-home-transfer.component.css']
})
export class EmployeeHomeTransferComponent implements OnInit {

  transfersData: TransferSummary[] = [];
  transfersTotalCount: number = 0;
  transfersSelectAll: boolean = false;
  transfersSelection: TransferSummary[] = [];
  items: ItemSummary[] = [];
  stores: StoreSummary[] = [];
  ledgerEntries: Item_Ledger_EntrySummary[] = [];

  transfersDisplayedColumns: string[] = ["select", "transfer_to_store_id", "transfer_from_store_id", "item_id", "ledger_entry_id",];
  transfersSearchTextBox: FormControl = new FormControl();
  transfersIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) transfersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) transfersSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public employeeSelfPersist: EmployeeSelfPersist,
              public itemsPersist: ItemPersist,
              public storePersist: StorePersist,
              public ledgerPersist: Item_Ledger_EntryPersist,
              public jobPersist: JobPersist,
  ) {

    this.transfersSearchTextBox.setValue(employeeSelfPersist.transferSearchText);
    //delay subsequent keyup events
    this.transfersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.employeeSelfPersist.transferSearchText = value;
      this.searchTransfers();
    });
  }

  ngOnInit() {

    this.transfersSort.sortChange.subscribe(() => {
      this.transfersPaginator.pageIndex = 0;
      this.searchTransfers(true);
    });

    this.transfersPaginator.page.subscribe(() => {
      this.searchTransfers(true);
    });
    //start by loading items
    this.searchTransfers();
    this.loadItems();
    this.loadStores();
    this.loadLedgerEntry();
  }

  searchTransfers(isPagination: boolean = false): void {

    let paginator = this.transfersPaginator;
    let sorter = this.transfersSort;

    this.transfersIsLoading = true;
    this.transfersSelection = [];

    this.employeeSelfPersist.searchTransfer(paginator.pageSize, isPagination ? paginator.pageIndex : 0, sorter.active, sorter.direction).subscribe((partialList: TransferSummaryPartialList) => {
      this.transfersData = partialList.data;
      if (partialList.total != -1) {
        this.transfersTotalCount = partialList.total;
      }
      this.transfersIsLoading = false;
    }, error => {
      this.transfersIsLoading = false;
    });

  }

  downloadTransfers(): void {

  }

  back(): void {
    this.location.back();
  }

  loadItems() {
    this.itemsPersist.itemsDo('get_all_items', '').subscribe((result) => {
      this.items = (result as ItemSummaryPartialList).data;
    });
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

  loadLedgerEntry() {
    this.ledgerPersist.item_ledger_entrysDo('get_all_item_ledger_entry', '').subscribe((result) => {
      this.ledgerEntries = (result as Item_Ledger_EntrySummaryPartialList).data;
    });
  }

}
