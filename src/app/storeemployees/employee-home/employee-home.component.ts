import {Component, OnInit} from '@angular/core';
import {TCAuthorization} from "../../tc/authorization";
import {EmployeePersist} from "../employee.persist";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {AppTranslation} from "../../app.translation";
import {UserContexts} from "../../tc/app.enums";
import {EmployeeDetail, EmployeeSummary, RequestCaseDetail} from "../employee.model";
import {environment} from "../../../environments/environment";
import {TCNavigator} from "../../tc/navigator";
import {TCAuthentication} from "../../tc/authentication";
import {UserPersist} from "../../tc/users/user.persist";
import {TCNotification} from "../../tc/notification";
import {EmployeeSelfPersist} from "../employee-self.persist";
import {EmployeeSelfNavigator} from "../employee-self.navigator";
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {StoreNavigator} from 'src/app/stores/store.navigator';
import {TCUtilsDate} from 'src/app/tc/utils-date';
import {TCUtilsArray} from 'src/app/tc/utils-array';

import {StorePersist} from 'src/app/stores/store.persist';
import {StoreSummary, StoreSummaryPartialList} from 'src/app/stores/store.model';
import {CaseDetail, CaseSummary} from "../../tc/cases/case.model";
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { TCAppInit } from 'src/app/tc/app-init';
import { RequestDetail } from 'src/app/requests/request.model';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { ItemSummary } from 'src/app/items/item.model';


export enum EmployeeTabs {
  overview,
  profile,
  items,
  requests,
  responses,
  transfer,
  ledger,
  purchase,
  medicines,
  lost_damage,
}


@Component({
  selector: 'app-employee-home',
  templateUrl: './employee-home.component.html',
  styleUrls: ['./employee-home.component.css']
})
export class EmployeeHomeComponent implements OnInit {
  item_detail:ItemSummary[];
  paramsSubscription: Subscription;
  title = environment.appName;
  TCAppInit = TCAppInit;
  employeeLoading: boolean = true;
  message: string = "";
  employeeSummary: EmployeeSummary;
  employeeTab: typeof EmployeeTabs = EmployeeTabs;
  activeTab: EmployeeTabs = EmployeeTabs.overview;
  stores: StoreSummary[] = [];
  requestDisplayedColumns: string[] = [
    "action",
    "target_id",
    "creation_time",
  ];
  requestToApprove: CaseSummary[] = [];
  requestToDispatch: CaseDetail[] = [];

  constructor(
    private route: ActivatedRoute,
    public tcUtilsAngular: TCUtilsAngular,
    public tcAuthorization: TCAuthorization,
    public tcUtilsDate: TCUtilsDate,
    public tcUtilsArray: TCUtilsArray,
    public employeeSelfPersist: EmployeeSelfPersist,
    public employeePersist: EmployeePersist,
    public appTranslation: AppTranslation,
    public tcNavigator: TCNavigator,
    public tcAuthentication: TCAuthentication,
    public userPersist: UserPersist,
    public tcNotification: TCNotification,
    public employeeSelfNavigator: EmployeeSelfNavigator,
    public storeNavigator: StoreNavigator,
    public storePersist: StorePersist,
    private requestNavigator:EmployeeSelfNavigator,
    private itemPersist: ItemPersist
  ) {
    this.employeeSummary = new EmployeeSummary();

  }

  ngOnInit() {
    this.tcAuthorization.requireLogin();
    this.employeeSelfPersist.initEmployeeSelf();
    this.loadItem();
  }

  ngAfterViewInit(): void {
    let employeeId: string = this.tcAuthorization.getUserContexts(UserContexts.employee)[0];
    this.message = "member id: " + employeeId;
    this.loadSelfDetails();
    this.loadStores();
  };

  loadItem(){
    this.itemPersist.searchItem(100,0,"","").subscribe((result)=>{
      this.item_detail=result.data;
    });
  }
  loadSelfDetails() {
    this.employeeSelfPersist.getEmployeeSelf().subscribe(result => {
        this.employeeSummary = result;
        this.employeeLoading = false;

        if (this.employeeSummary.is_main_store_employee && this.employeeSummary.is_store_manager) {
          this.loadRequestsToDispatch();
        }
        if (this.employeeSummary.is_store_manager) {
          this.loadRequestsToApprove();
        }
      }
      , error => {
        console.error(error);
        this.employeeLoading = false;
      });
  }

  loadRequestsToApprove() {
    this.employeeSelfPersist.requestsToApprove().subscribe(
      result => {
        this.requestToApprove = result;
      }, error => {
        console.error(error);
      }
    )
  }


  reload() {
    this.loadSelfDetails();
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

  languageChanged(): void {
    this.userPersist.updateLanguage(TCAppInit.userInfo.lang).subscribe(result => {
      this.tcAuthentication.reloadUserInfo(() => {
        this.tcNotification.success("Language profile updated");
      });
    });
  }

  addRequest(): void {
    let dialogRef = this.employeeSelfNavigator.addRequest();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.tcNotification.success("New request has been added");
      }
    });
  }

  private loadRequestsToDispatch() {
    this.employeeSelfPersist.requestsToDispatch().subscribe(
      result => {
        this.requestToDispatch = result;
      }, error => {
        console.error(error);
      }
    )
  }

  processdisPatchCase(element:CaseSummary){
    let ref =this.requestNavigator.editRequest(element.id,true);
    ref.afterClosed().subscribe(
      res=>{
        this.loadRequestsToDispatch();
      }
    )

  }

  logout(){
    this.tcAuthentication.logout();
    this.tcNavigator.home();
  }
}
