import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {EmployeePersist} from "../employee.persist";
import {EmployeeNavigator} from "../employee.navigator";
import {EmployeeDetail, EmployeeSummary, EmployeeSummaryPartialList} from "../employee.model";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {StoreNavigator} from "../../stores/store.navigator";
import {StorePersist} from "../../stores/store.persist";


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employeesData: EmployeeSummary[] = [];
  employeesTotalCount: number = 0;
  employeesSelectAll:boolean = false;
  employeesSelection: EmployeeSummary[] = [];
  stores: StoreSummary[] = []

  employeesDisplayedColumns: string[] = ["select","action", "first_name","middle_name","last_name","telephone","is_male","birth_date","address","store_id","is_store_manager", ];
  employeesSearchTextBox: FormControl = new FormControl();
  employeesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) employeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) employeesSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate:TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public employeePersist: EmployeePersist,
              public employeeNavigator: EmployeeNavigator,
              public storeNavigator: StoreNavigator,
              private storePersist: StorePersist,
              public jobPersist: JobPersist,
  ) {

    this.tcAuthorization.requireRead("store_employees");
    this.employeesSearchTextBox.setValue(employeePersist.employeeSearchText);
    //delay subsequent keyup events
    this.employeesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.employeePersist.employeeSearchText = value;
      this.searchEmployees();
    });
  }

  ngOnInit() {

    this.employeesSort.sortChange.subscribe(() => {
      this.employeesPaginator.pageIndex = 0;
      this.searchEmployees(true);
    });

    this.employeesPaginator.page.subscribe(() => {
      this.searchEmployees(true);
    });
    //start by loading items
    this.searchEmployees();
    this.loadStores();
  }

  searchEmployees(isPagination:boolean = false): void {


    let paginator = this.employeesPaginator;
    let sorter = this.employeesSort;

    this.employeesIsLoading = true;
    this.employeesSelection = [];

    this.employeePersist.searchEmployee(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: EmployeeSummaryPartialList) => {
      this.employeesData = partialList.data;
      if (partialList.total != -1) {
        this.employeesTotalCount = partialList.total;
      }
      this.employeesIsLoading = false;
    }, error => {
      this.employeesIsLoading = false;
    });

  }

  downloadEmployees(): void {
    if(this.employeesSelectAll){
      this.employeePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download employees", true);
      });
    }
    else{
      this.employeePersist.download(this.tcUtilsArray.idsList(this.employeesSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download employees",true);
      });
    }
  }



  addEmployee(): void {
    let dialogRef = this.employeeNavigator.addEmployee();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchEmployees();
      }
    });
  }

  editEmployee(item: EmployeeSummary) {
    let dialogRef = this.employeeNavigator.editEmployee(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteEmployee(item: EmployeeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Employee");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.employeePersist.deleteEmployee(item.id).subscribe(response => {
          this.tcNotification.success("Employee deleted");
          this.searchEmployees();
        }, error => {
        });
      }

    });
  }

  back():void{
    this.location.back();
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

}
