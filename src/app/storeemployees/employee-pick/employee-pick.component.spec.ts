import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmployeePickComponent } from './employee-pick.component';

describe('EmployeePickComponent', () => {
  let component: EmployeePickComponent;
  let fixture: ComponentFixture<EmployeePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
