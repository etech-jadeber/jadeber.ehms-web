import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {EmployeeDetail, EmployeeSummary, EmployeeSummaryPartialList} from "../employee.model";
import {EmployeePersist} from "../employee.persist";
import {StoreSummary, StoreSummaryPartialList} from "../../stores/store.model";
import {StorePersist} from "../../stores/store.persist";


@Component({
  selector: 'app-employee-pick',
  templateUrl: './employee-pick.component.html',
  styleUrls: ['./employee-pick.component.css']
})
export class EmployeePickComponent implements OnInit {

  employeesData: EmployeeSummary[] = [];
  employeesTotalCount: number = 0;
  employeesSelection: EmployeeSummary[] = [];
  employeesDisplayedColumns: string[] = ["select", 'first_name','last_name','middle_name','email','telephone','store_id','is_store_manager' ];
  stores: StoreSummary[] = [];

  employeesSearchTextBox: FormControl = new FormControl();
  employeesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) employeesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) employeesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public employeePersist: EmployeePersist,
              public storePersist: StorePersist,
              public dialogRef: MatDialogRef<EmployeePickComponent>,
              @Inject(MAT_DIALOG_DATA) public data : {selectOne: boolean, disabled:boolean}
  ) {
    this.tcAuthorization.requireRead("store_employees");
    this.employeesSearchTextBox.setValue(employeePersist.employeeSearchText);
    //delay subsequent keyup events
    this.employeePersist.disabled = data.disabled;
    this.employeesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.employeePersist.employeeSearchText = value;
      this.searchEmployees();
    });
  }

  ngOnInit() {

    this.employeesSort.sortChange.subscribe(() => {
      this.employeesPaginator.pageIndex = 0;
      this.searchEmployees();
    });

    this.employeesPaginator.page.subscribe(() => {
      this.searchEmployees();
    });

    //set initial picker list to 5
    this.employeesPaginator.pageSize = 5;

    //start by loading items
    this.searchEmployees();
    this.loadStores();
  }

  searchEmployees(): void {
    this.employeesIsLoading = true;
    this.employeesSelection = [];

    this.employeePersist.searchEmployee(this.employeesPaginator.pageSize,
      this.employeesPaginator.pageIndex,
      this.employeesSort.active,
      this.employeesSort.direction).subscribe((partialList: EmployeeSummaryPartialList) => {
      this.employeesData = partialList.data;
      if (partialList.total != -1) {
        this.employeesTotalCount = partialList.total;
      }
      this.employeesIsLoading = false;
    }, error => {
      this.employeesIsLoading = false;
    });

  }

  markOneItem(item: EmployeeSummary) {
    if(!this.tcUtilsArray.containsId(this.employeesSelection,item.id)){
      this.employeesSelection = [];
      this.employeesSelection.push(item);
    }
    else{
      this.employeesSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.employeesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  loadStores() {
    this.storePersist.storesDo('get_all_stores', '').subscribe((result) => {
      this.stores = (result as StoreSummaryPartialList).data;
    });
  }

}
