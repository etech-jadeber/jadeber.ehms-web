import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TcDictionary, TCIdMode, TCModalModes, TCParentChildIds} from "../tc/models";
import {EmployeeHomeRequestEditComponent} from "./employee-home-request-edit/employee-home-request-edit.component";
import {
  EmployeeHomeResponseApproveComponent
} from "./employee-home-response-approve/employee-home-response-approve.component";


@Injectable({
  providedIn: 'root'
})

export class EmployeeSelfNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  editRequest(id: string, isCase:boolean = false): MatDialogRef<unknown,any> {

    let params: TcDictionary<string> = new TcDictionary();
    params["mode"] = isCase
      ? TCModalModes.CASE
      : id == null
        ? TCModalModes.NEW
        : TCModalModes.EDIT;
    const dialogRef = this.dialog.open(EmployeeHomeRequestEditComponent, {
      data: new TCParentChildIds(null, id, params),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }


  

  approveResponse(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(EmployeeHomeResponseApproveComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addRequest(): MatDialogRef<unknown,any> {
    return this.editRequest(null,false)
    
  }
}
