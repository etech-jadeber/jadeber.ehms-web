import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TcDictionary, TCEnum, TCEnumTranslation, TCId} from "../tc/models";
import {EmployeeSummary} from "./employee.model";
import {UserContexts} from "../tc/app.enums";
import {TCAuthorization} from "../tc/authorization";
import {RequestDetail, RequestSummaryPartialList} from "../requests/request.model";
import {Purchase_type, request_status, transfer_type, units} from '../app.enums';
import {AppTranslation} from "../app.translation";
import {TCUtilsString} from "../tc/utils-string";
import {TransferSummaryPartialList} from "../transfers/transfer.model";
import {Item_Ledger_EntrySummaryPartialList} from "../item_ledger_entrys/item_ledger_entry.model";
import {PurchaseSummaryPartialList} from '../purchases/purchase.model';
import {Item_In_StoreSummaryPartialList} from "../item_in_stores/item_in_store.model";
import {CaseDetail} from "../tc/cases/case.model";


@Injectable({
  providedIn: 'root'
})
export class EmployeeSelfPersist {

  item_in_storeSearchText: string = "";
  requestSearchText: string = "";
  responseSearchText: string = "";
  transferSearchText: string = "";
  item_ledger_entrySearchText: string = "";
  purchaseSearchText: string = '';

  myEmployeeId: string = null;
  request_statusId: number;
  response_statusId: number;
  transfer_typeId: number;
  item_typeId: number;
  entry_typeId: number;
  purchaseTypefilter: string = "-1";
  unitFilter: string = "-1";

  response_status: TCEnumTranslation[] = [
    new TCEnumTranslation(request_status.firstrejected, this.appTranslation.getKey('general', 'rejected')),
    new TCEnumTranslation(request_status.approved, this.appTranslation.getKey('general', 'approved')),
    new TCEnumTranslation(request_status.dispatched, this.appTranslation.getKey('inventory', 'dispatched')),
  ];

  transfer_types: TCEnumTranslation[] = [
    new TCEnumTranslation(transfer_type.in, this.appTranslation.getKey('inventory', 'in')),
    new TCEnumTranslation(transfer_type.out, this.appTranslation.getKey('inventory', 'out')),
  ];
  Purchase_type: TCEnum[] = [
    new TCEnum(Purchase_type.annual, 'annual'),
    new TCEnum(Purchase_type.stock_out, 'stock_out'),
    new TCEnum(Purchase_type.other, 'other'),
  ];

  units: TCEnum[] = [
    new TCEnum(units.piece, 'piece'),
    new TCEnum(units.box, 'box'),
    new TCEnum(units.pack, 'pack'),
    new TCEnum(units.vial, 'vial'),
  ];

  constructor(private http: HttpClient, private tcAuthorization: TCAuthorization, private appTranslation: AppTranslation) {
  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.requestSearchText;
    //add custom filters
    return fltrs;
  }

  searchRequest(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<RequestSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("employee-self/requests", this.requestSearchText, pageSize, pageIndex, sort, order);
    if (this.request_statusId) {
      url = TCUtilsString.appendUrlParameter(url, "status", this.request_statusId.toString());
    }
    return this.http.get<RequestSummaryPartialList>(url);
  }

  searchTransfer(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<TransferSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("employee-self/transfers", this.transferSearchText, pageSize, pageIndex, sort, order);
    if (this.transfer_typeId) {
      url = TCUtilsString.appendUrlParameter(url, "transfer_type", this.transfer_typeId.toString());
    }
    return this.http.get<TransferSummaryPartialList>(url);
  }

  clearFilters(): void {
    this.purchaseSearchText = "";
    this.purchaseTypefilter = "-1";
    this.unitFilter = "-1";
  }

  searchPurchase(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<PurchaseSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      '/employee-self/purchases',
      this.purchaseSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );

    url = TCUtilsString.appendUrlParameter(url, 'purchase_type', this.purchaseTypefilter.toString());
    url = TCUtilsString.appendUrlParameter(url, "unit", this.unitFilter.toString());
    return this.http.get<PurchaseSummaryPartialList>(url);
  }

  searchItem_Ledger_Entry(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Item_Ledger_EntrySummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl("employee-self/item_ledger_entrys", this.item_ledger_entrySearchText, pageSize, pageIndex, sort, order);
    if (this.item_typeId) {
      url = TCUtilsString.appendUrlParameter(url, "item_type", this.item_typeId.toString());
    }
    if (this.entry_typeId) {
      url = TCUtilsString.appendUrlParameter(url, "entry_type", this.entry_typeId.toString());
    }
    return this.http.get<Item_Ledger_EntrySummaryPartialList>(url);
  }

  requestDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "employee-self/requests/" + id + "/do", new TCDoParam(method, payload));
  }

  responseDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "employee-self/responses/" + id + "/do", new TCDoParam(method, payload));
  }

  initEmployeeSelf(): void {
    let employeeIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.employee);
    if (employeeIds.length == 1) {
      this.myEmployeeId = employeeIds[0];
    }
  }

  getEmployeeSelf(): Observable<EmployeeSummary> {
    return this.http.get<EmployeeSummary>(environment.tcApiBaseUri + "employee-self");
  }

  addRequest(item: RequestDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "employee-self/requests/", item);
  }

  updateRequest(item: RequestDetail): Observable<RequestDetail> {
    return this.http.patch<RequestDetail>(environment.tcApiBaseUri + "employee-self/requests/" + item.id, item);
  }

  requestsToApprove(): Observable<CaseDetail[]> {
    return this.http.get<CaseDetail[]>(environment.tcApiBaseUri + "employee-self/requests-to-approve");
  }

  requestsToDispatch(): Observable<CaseDetail[]> {
    return this.http.get<CaseDetail[]>(environment.tcApiBaseUri + "employee-self/requests-to-dispatch");
  }

}
