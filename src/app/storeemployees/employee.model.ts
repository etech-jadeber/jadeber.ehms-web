import { RequestDetail } from "../requests/request.model";
import { CaseSummary } from "../tc/cases/case.model";
import {CommentDetail, TCId} from "../tc/models";

export class EmployeeSummary extends TCId {
  first_name : string;
  last_name : string;
  middle_name : string;
  email : string;
  telephone : string;
  is_male : boolean;
  birth_date : number;
  address : string;
  store_id : string;
  is_store_manager : boolean;
  login_id : string;
  is_main_store_employee: boolean;
  disabled:boolean;
}


export class EmployeeSummaryPartialList {
  data: EmployeeSummary[];
  total: number;
}

export class EmployeeDetail extends EmployeeSummary {
  first_name : string;
  last_name : string;
  middle_name : string;
  email : string;
  telephone : string;
  is_male : boolean;
  birth_date : number;
  address : string;
  store_id : string;
  is_store_manager : boolean;
  login_id : string;
}

export class EmployeeDashboard {
  total: number;
}


export class RequestCaseDetail extends CaseSummary {
  payload: RequestDetail;
  comments:CommentDetail[];
}

