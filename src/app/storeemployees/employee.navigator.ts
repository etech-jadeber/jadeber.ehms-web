import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {EmployeeEditComponent} from "./employee-edit/employee-edit.component";
import {EmployeePickComponent} from "./employee-pick/employee-pick.component";


@Injectable({
  providedIn: 'root'
})

export class EmployeeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  employeesUrl(): string {
    return "/store_employees";
  }

  employeeUrl(id: string): string {
    return "/store_employees/" + id;
  }

  viewEmployees(): void {
    this.router.navigateByUrl(this.employeesUrl());
  }

  viewEmployee(id: string): void {
    this.router.navigateByUrl(this.employeeUrl(id));
  }

  editEmployee(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(EmployeeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addEmployee(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(EmployeeEditComponent, {
      data: new TCIdMode(null, TCModalModes.NEW),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  pickEmployees(selectOne: boolean=false,disabled:boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(EmployeePickComponent, {
      data: {selectOne,disabled},
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  goHome(): void {
    this.router.navigateByUrl("/employee-home");
  }
}
