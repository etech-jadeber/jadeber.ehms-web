import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {EmployeeDashboard, EmployeeDetail, EmployeeSummary, EmployeeSummaryPartialList} from "./employee.model";
import {UserContexts} from "../tc/app.enums";
import {TCAuthorization} from "../tc/authorization";
import {PatientSelfDashboard} from "../patients/patients.model";
import { CaseDetail } from '../tc/cases/case.model';
import { RequestSummary } from '../requests/request.model';
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class EmployeePersist {

  employeeSearchText: string = "";
  myEmployeeId :string = null;
  disabled: boolean;

  constructor(private http: HttpClient, private tcAuthorization:TCAuthorization) {
  }

  searchEmployee(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<EmployeeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("employees", this.employeeSearchText, pageSize, pageIndex, sort, order);    
    if (this.disabled != null) {
      url = TCUtilsString.appendUrlParameter(url, "disabled", this.disabled.toString());
    }
    return this.http.get<EmployeeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.employeeSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "employees/do", new TCDoParam("download_all", this.filters()));
  }

  employeeDashboard(): Observable<EmployeeDashboard> {
    return this.http.get<EmployeeDashboard>(environment.tcApiBaseUri + "employees/dashboard");
  }

  getEmployee(id: string): Observable<EmployeeDetail> {
    return this.http.get<EmployeeDetail>(environment.tcApiBaseUri + "employees/" + id);
  }


  acceptRequest(reqSummary:RequestSummary, method:string):Observable<TCId>{
    return this.http.post<TCId>(environment.tcApiBaseUri + "employee-self/requests/do/", new TCDoParam(method, reqSummary));
  }
  addEmployee(item: EmployeeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "employees/", item);
  }

  updateEmployee(item: EmployeeDetail): Observable<EmployeeDetail> {
    return this.http.patch<EmployeeDetail>(environment.tcApiBaseUri + "employees/" + item.id, item);
  }

  deleteEmployee(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "employees/" + id);
  }

  employeesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "employees/do", new TCDoParam(method, payload));
  }

  employeeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "employees/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "employees/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "employees/" + id + "/do", new TCDoParam("print", {}));
  }

  initEmployeeSelf(): void {
    let employeeIds: string[] = this.tcAuthorization.getUserContexts(UserContexts.employee);
    if (employeeIds.length == 1) {
      this.myEmployeeId = employeeIds[0];
    }
  }

  getEmployeeSelf(): Observable<EmployeeSummary> {
    return this.http.get<EmployeeSummary>(environment.tcApiBaseUri + "employee-self");
  }


  getCaseDetail(id:string):Observable<CaseDetail>{
    return this.http.get<CaseDetail>(environment.tcApiBaseUri + "employee-self/requests/"+id);
  
  }

}
