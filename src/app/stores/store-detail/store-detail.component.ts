import {
  AfterViewInit,
  Component,
  OnInit,
  OnDestroy,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from 'rxjs';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCNotification } from '../../tc/notification';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';
import { AppTranslation } from '../../app.translation';

import { StoreDetail } from '../store.model';
import { StorePersist } from '../store.persist';
import { StoreNavigator } from '../store.navigator';
import { store_type, tabs } from 'src/app/app.enums';
import { MenuState } from 'src/app/global-state-manager/global-state';
import { TCUtilsString } from 'src/app/tc/utils-string';

export enum PaginatorIndexes { }

export enum StoreTabs {
  overview
}

@Component({
  selector: 'app-store-detail',
  templateUrl: './store-detail.component.html',
  styleUrls: ['./store-detail.component.css'],
})
export class StoreDetailComponent implements OnInit, OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  storeLoading: boolean = false;
  //basics
  storeDetail: StoreDetail;
  storeType = store_type;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();


  storeTabs: typeof tabs = tabs;
  activeTab: number = tabs.overview;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public storeNavigator: StoreNavigator,
    public storePersist: StorePersist,
    public menuState: MenuState,
    public tcUtilsString: TCUtilsString,
  ) {
    this.tcAuthorization.requireRead('stores');
    this.storeDetail = new StoreDetail();
    //tab state manager
    this.menuState.currentTabResponseState.subscribe((res) => {
      this.activeTab = res;
    })
  }

  ngOnInit() {
    this.tcAuthorization.requireRead('stores');
    this.activeTab=tabs.overview; 
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.storeDetail.id = null;
      this.loadDetails(id);
      this.menuState.changeMenuState(id);
    });
  }

  loadDetails(id: string): void {
    this.storeLoading = true;
    this.storePersist.getStore(id).subscribe(
      (storeDetail) => {
        this.storeDetail = storeDetail;
        this.menuState.getDataResponse(this.storeDetail);
        this.storeDetail.parent_store_id != this.tcUtilsString.invalid_id && this.loadParentStoreDetail(this.storeDetail.parent_store_id);
        this.storeLoading = false;
      },
      (error) => {
        console.error(error);
        this.storeLoading = false;
      }
    );
  }

  loadParentStoreDetail(id: string) {
    this.storePersist.getStore(id).subscribe(
      (storeDetail) => {
        this.storeDetail.parent_store_name = storeDetail.name;
      },
      (error) => {
        console.error(error);
      }
    );
  }


  reload() {
    this.loadDetails(this.storeDetail.id);
  }

  editStore(): void {
    let modalRef = this.storeNavigator.editStore(this.storeDetail.id);
    modalRef.afterClosed().subscribe(
      (modifiedStoreDetail) => {
        TCUtilsAngular.assign(this.storeDetail, modifiedStoreDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printStore(): void {
    this.storePersist.print(this.storeDetail.id).subscribe((printJob) => {
      this.jobPersist.followJob(printJob.id, 'print store', true);
    });
  }

  back(): void {
    this.location.back();
  }
}
