import { Component, OnInit, Inject } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCAuthorization } from '../../tc/authorization';
import { TCNotification } from '../../tc/notification';
import { TCUtilsString } from '../../tc/utils-string';
import { AppTranslation } from '../../app.translation';

import { TCIdMode, TCModalModes } from '../../tc/models';

import { StoreDetail, StoreSummary } from '../store.model';
import { StorePersist } from '../store.persist';
import { StoreSpecialNavigator } from '../store.specialnavigator';
import { store_service_type } from 'src/app/app.enums';

@Component({
  selector: 'app-store-edit',
  templateUrl: './store-edit.component.html',
  styleUrls: ['./store-edit.component.css'],
})
export class StoreEditComponent implements OnInit {
  isLoadingResults: boolean = false;
  title: string;
  storeDetail: StoreDetail;
  storeName: string = "";

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<StoreEditComponent>,
    public persist: StorePersist,
    public storeNavigator: StoreSpecialNavigator,
    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    console.log(this.idMode.id)
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('stores');
      this.title = this.appTranslation.getText('general', 'new') + ' ' + this.appTranslation.getText("inventory", "store");
      this.storeDetail = new StoreDetail();
      this.storeDetail.store_type = -1;
      console.log(this.idMode.id)
      this.storeDetail.parent_store_id = this.idMode.id;
      this.storeDetail.store_service_type = store_service_type.pharmacy;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate('stores');
      this.title = this.appTranslation.getText('general', 'edit') + ' ' + this.appTranslation.getText("inventory", "store");
      this.isLoadingResults = true;
      this.persist.getStore(this.idMode.id).subscribe(
        (storeDetail) => {
          this.storeDetail = storeDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addStore(this.storeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Store added');
        this.storeDetail.id = value.id;
        this.dialogRef.close(this.storeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateStore(this.storeDetail).subscribe(
      (value) => {
        this.tcNotification.success('Store updated');
        this.dialogRef.close(this.storeDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.storeDetail == null) {
      return false;
    }

    if (this.storeDetail.name == null || this.storeDetail.name == '') {
      return false;
    }

    if (!this.idMode.id && this.storeDetail.store_type == -1){
      return false;
    }

    return true;
  }

  searchStore(){
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.storeName = result[0].name;
        this.storeDetail.parent_store_id = result[0].id;
      }
    });
  }

}
