import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { debounceTime } from 'rxjs/operators';
import { Location } from '@angular/common';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { JobPersist } from '../../tc/jobs/job.persist';

import { AppTranslation } from '../../app.translation';

import { StorePersist } from '../store.persist';
import { StoreNavigator } from '../store.navigator';
import {
  StoreDetail,
  StoreSummary,
  StoreSummaryPartialList,
} from '../store.model';
import { store_service_type } from 'src/app/app.enums';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.css'],
})
export class StoreListComponent implements OnInit {
  storesData: StoreSummary[] = [];
  storesTotalCount: number = 0;
  storesSelectAll: boolean = false;
  storesSelection: StoreSummary[] = [];

  storesDisplayedColumns: string[] = [
    'select',
    'action',
    'name',
    'store_type',
    'start_date',
    'end_date'
  ];
  storesSearchTextBox: FormControl = new FormControl();
  storesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) storesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) storesSort: MatSort;
  @Input() parentId : string;
  @Input() isMain: boolean = true;
  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public storePersist: StorePersist,
    public storeNavigator: StoreNavigator,
    public jobPersist: JobPersist
  ) {
    this.tcAuthorization.requireRead('stores');
    this.storePersist.store_service_type = store_service_type.pharmacy;
    this.storesSearchTextBox.setValue(storePersist.storeSearchText);
    //delay subsequent keyup events
    this.storesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.storePersist.storeSearchText = value;
        this.searchStores();
      });
  }

  ngOnInit() {
    if(this.parentId){
      this.storesDisplayedColumns.splice(3, 1)
    }
    this.storesSort.sortChange.subscribe(() => {
      this.storesPaginator.pageIndex = 0;
      this.searchStores(true);
    });

    this.storesPaginator.page.subscribe(() => {
      this.searchStores(true);
    });
    //start by loading items
    this.searchStores();
  }

  searchStores(isPagination: boolean = false): void {
    let paginator = this.storesPaginator;
    let sorter = this.storesSort;

    this.storesIsLoading = true;
    this.storesSelection = [];

    this.storePersist
      .searchStore(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction,
        this.parentId
      )
      .subscribe(
        (partialList: StoreSummaryPartialList) => {
          this.storesData = partialList.data;
          if (partialList.total != -1) {
            this.storesTotalCount = partialList.total;
          }
          this.storesIsLoading = false;
        },
        (error) => {
          this.storesIsLoading = false;
        }
      );
  }

  downloadStores(): void {
    if (this.storesSelectAll) {
      this.storePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(downloadJob.id, 'download stores', true);
      });
    } else {
      this.storePersist
        .download(this.tcUtilsArray.idsList(this.storesSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(downloadJob.id, 'download stores', true);
        });
    }
  }

  addStore(): void {
    let dialogRef = this.storeNavigator.addStore(this.parentId);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchStores();
      }
    });
  }

  editStore(item: StoreSummary) {
    let dialogRef = this.storeNavigator.editStore(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteStore(item: StoreSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Store');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.storePersist.deleteStore(item.id).subscribe(
          (response) => {
            this.tcNotification.success('Store deleted');
            this.searchStores();
          },
          (error) => {}
        );
      }
    });
  }

  back(): void {
    this.location.back();
  }
}
