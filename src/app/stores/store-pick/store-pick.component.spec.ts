import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StorePickComponent } from './store-pick.component';

describe('StorePickComponent', () => {
  let component: StorePickComponent;
  let fixture: ComponentFixture<StorePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StorePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
