import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import { debounceTime } from 'rxjs/operators';

import { TCAuthorization } from '../../tc/authorization';
import { TCUtilsAngular } from '../../tc/utils-angular';
import { TCUtilsArray } from '../../tc/utils-array';
import { TCUtilsDate } from '../../tc/utils-date';
import { TCNotification } from '../../tc/notification';
import { TCNavigator } from '../../tc/navigator';
import { AppTranslation } from '../../app.translation';

import {
  StoreDetail,
  StoreSummary,
  StoreSummaryPartialList,
} from '../store.model';
import { StorePersist } from '../store.persist';
import { store_service_type, store_type } from 'src/app/app.enums';

@Component({
  selector: 'app-store-pick',
  templateUrl: './store-pick.component.html',
  styleUrls: ['./store-pick.component.css'],
})
export class StorePickComponent implements OnInit {
  storesData: StoreSummary[] = [];
  storesTotalCount: number = 0;
  storesSelection: StoreSummary[] = [];
  storesDisplayedColumns: string[] = ['select', 'name', 'store_type'];

  storesSearchTextBox: FormControl = new FormControl();
  storesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) storesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) storesSort: MatSort;

  constructor(
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public storePersist: StorePersist,
    public dialogRef: MatDialogRef<StorePickComponent>,
    @Inject(MAT_DIALOG_DATA) public selectOne: boolean | string,
  ) {
    // this.tcAuthorization.requireRead('stores');
    this.storePersist.store_service_type = undefined;
    this.storesSearchTextBox.setValue(storePersist.storeSearchText);
    //delay subsequent keyup events
    this.storesSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.storePersist.storeSearchText = value;
        this.searchStores();
      });
  }

  ngOnInit() {
    this.storesSort.sortChange.subscribe(() => {
      this.storesPaginator.pageIndex = 0;
      this.searchStores();
    });

    this.storesPaginator.page.subscribe(() => {
      this.searchStores();
    });

    //set initial picker list to 5
    this.storesPaginator.pageSize = 5;

    //start by loading items
    this.searchStores();
  }

  searchStores(): void {
    this.storesIsLoading = true;
    this.storesSelection = [];

    this.storePersist
      .searchStore(
        this.storesPaginator.pageSize,
        this.storesPaginator.pageIndex,
        this.storesSort.active,
        this.storesSort.direction
      )
      .subscribe(
        (partialList: StoreSummaryPartialList) => {
          this.storesData = partialList.data;
          if(typeof this.selectOne == "string" && this.selectOne == "pharmacies"){
            this.storesData = this.storesData.filter((singlestore) =>(singlestore.store_type == 102));
          }
          if (partialList.total != -1) {
            this.storesTotalCount = partialList.total;
          }
          this.storesIsLoading = false;
        },
        (error) => {
          this.storesIsLoading = false;
        }
      );
  }

  markOneItem(item: StoreSummary) {
    if (!this.tcUtilsArray.containsId(this.storesSelection, item.id)) {
      this.storesSelection = [];
      this.storesSelection.push(item);
    } else {
      this.storesSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.storesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnDestroy():void {
    this.storePersist.store_service_type = store_service_type.pharmacy;
  }

}
