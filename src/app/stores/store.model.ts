import { TCId } from '../tc/models';

export class StoreSummary extends TCId {
  name: string;
  parent_store_id: string;
  store_type : number;
  store_service_type: number;
}

export class StoreSummaryPartialList {
  data: StoreSummary[];
  total: number;
}

export class StoreDetail extends StoreSummary {
  name: string;
  parent_store_id: string;
  store_type : number;
  parent_store_name: string;
}

export class StoreDashboard {
  total: number;
}
