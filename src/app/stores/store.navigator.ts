import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';


import { StorePickComponent } from './store-pick/store-pick.component';
import { StoreEditComponent } from './store-edit/store-edit.component';

@Injectable({
  providedIn: 'root',
})
export class StoreNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}

  storesUrl(): string {
    return '/stores';
  }

  storeUrl(id: string): string {
    return '/stores/' + id;
  }

  viewStores(): void {
    this.router.navigateByUrl(this.storesUrl());
  }

  viewStore(id: string): void {
    this.router.navigateByUrl(this.storeUrl(id));
  }

  editStore(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StoreEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addStore(parentId: string = null): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StoreEditComponent, {
      data: new TCIdMode(parentId, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  pickStores(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StorePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

  pickPharmacies(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StorePickComponent, {
      data: "pharmacies",
      width: TCModalWidths.large,
    });
    return dialogRef;
  }

}
