import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {StoreDashboard, StoreDetail, StoreSummaryPartialList} from "./store.model";
import {store_service_type, store_type} from '../app.enums';
import {TCUtilsString} from "../tc/utils-string";


@Injectable({
  providedIn: 'root'
})
export class StorePersist {

  storeSearchText: string = "";

  store_typeId: number;

  store_type: TCEnum[] = [
    new TCEnum(store_type.main, 'main_store'),
    new TCEnum(store_type.outpatient, 'outpatient'),
    new TCEnum(store_type.inpatient, 'inpatient'),
    new TCEnum(store_type.emergency, 'emergency'),
    new TCEnum(store_type.ward_store, 'ward_store'),
    new TCEnum(store_type.lab_store, 'lab_store'),
    new TCEnum(store_type.rad_store, 'rad_store'),
    new TCEnum(store_type.anesthesia_store, 'anesthesia_store'),
    new TCEnum(store_type.or_store, 'or_store'),
    new TCEnum(store_type.opd_store, 'opd_store'),
    new TCEnum(store_type.endoscopy_store, 'endoscopy_store'),
  ];

  storeServiceType: TCEnum[] = [
    new TCEnum(store_service_type.pharmacy, 'Pharmacy'),
    new TCEnum(store_service_type.logistic, 'Logistic'),
  ];

  store_service_type :number  = store_service_type.pharmacy;
  constructor(private http: HttpClient) {
  }

  searchStore(pageSize: number, pageIndex: number, sort: string, order: string, parentId: string = null): Observable<StoreSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("stores", this.storeSearchText, pageSize, pageIndex, sort, order);
    if (parentId){
      url = TCUtilsString.appendUrlParameter(url, "parent_store_id", parentId);
    }
    if (this.store_typeId) {
      url = TCUtilsString.appendUrlParameter(url, "store_type", this.store_typeId.toString());
    }
    if(this.store_service_type)
        url = TCUtilsString.appendUrlParameter(url, "store_service_type", this.store_service_type.toString())

    return this.http.get<StoreSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.storeSearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "stores/do", new TCDoParam("download_all", this.filters()));
  }

  storeDashboard(): Observable<StoreDashboard> {
    return this.http.get<StoreDashboard>(environment.tcApiBaseUri + "stores/dashboard");
  }

  getStore(id: string): Observable<StoreDetail> {
    return this.http.get<StoreDetail>(environment.tcApiBaseUri + "stores/" + id);
  }

  addStore(item: StoreDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "stores/", item);
  }

  updateStore(item: StoreDetail): Observable<StoreDetail> {
    return this.http.patch<StoreDetail>(environment.tcApiBaseUri + "stores/" + item.id, item);
  }

  deleteStore(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "stores/" + id);
  }

  storesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "stores/do", new TCDoParam(method, payload));
  }

  storeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "stores/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "stores/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "stores/" + id + "/do", new TCDoParam("print", {}));
  }


}
