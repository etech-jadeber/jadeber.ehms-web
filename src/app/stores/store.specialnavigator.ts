import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { StorePickComponent } from './store-pick/store-pick.component';

@Injectable({
  providedIn: 'root',
})
export class StoreSpecialNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}

  pickStores(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StorePickComponent, {
      data: selectOne,
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
