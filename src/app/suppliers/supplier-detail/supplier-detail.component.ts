import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {SupplierDetail} from "../supplier.model";
import {SupplierPersist} from "../supplier.persist";
import {SupplierNavigator} from "../supplier.navigator";

export enum SupplierTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.component.html',
  styleUrls: ['./supplier-detail.component.css']
})
export class SupplierDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  supplierIsLoading:boolean = false;
  
  SupplierTabs: typeof SupplierTabs = SupplierTabs;
  activeTab: SupplierTabs = SupplierTabs.overview;
  //basics
  supplierDetail: SupplierDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public supplierNavigator: SupplierNavigator,
              public  supplierPersist: SupplierPersist) {
    this.tcAuthorization.requireRead("suppliers");
    this.supplierDetail = new SupplierDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("suppliers");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.supplierIsLoading = true;
    this.supplierPersist.getSupplier(id).subscribe(supplierDetail => {
          this.supplierDetail = supplierDetail;
          this.supplierIsLoading = false;
        }, error => {
          console.error(error);
          this.supplierIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.supplierDetail.id);
  }
  editSupplier(): void {
    let modalRef = this.supplierNavigator.editSupplier(this.supplierDetail.id);
    modalRef.afterClosed().subscribe(supplierDetail => {
      TCUtilsAngular.assign(this.supplierDetail, supplierDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.supplierPersist.print(this.supplierDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print supplier", true);
      });
    }

  back():void{
      this.location.back();
    }

}
