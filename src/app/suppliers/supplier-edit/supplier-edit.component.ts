import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { SupplierDetail } from '../supplier.model';import { SupplierPersist } from '../supplier.persist';@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.css']
})export class SupplierEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  supplierDetail: SupplierDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<SupplierEditComponent>,
              public  persist: SupplierPersist,
              
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("suppliers");
      this.title = this.appTranslation.getText("general","new") +  " " + "supplier";
      this.supplierDetail = new SupplierDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("suppliers");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "supplier";
      this.isLoadingResults = true;
      this.persist.getSupplier(this.idMode.id).subscribe(supplierDetail => {
        this.supplierDetail = supplierDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addSupplier(this.supplierDetail).subscribe(value => {
      this.tcNotification.success("supplier added");
      this.supplierDetail.id = value.id;
      this.dialogRef.close(this.supplierDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateSupplier(this.supplierDetail).subscribe(value => {
      this.tcNotification.success("supplier updated");
      this.dialogRef.close(this.supplierDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.supplierDetail == null){
            return false;
          }

if (this.supplierDetail.name == null || this.supplierDetail.name  == "") {
            return false;
        } 
if (this.supplierDetail.tin_number == null || this.supplierDetail.tin_number  == "") {
            return false;
        } 
if ((this.supplierDetail.phone_number_1 == null || this.supplierDetail.phone_number_1  == "")
&& (this.supplierDetail.phone_number_2 == null || this.supplierDetail.phone_number_2  == "")
&& (this.supplierDetail.phone_number_3 == null || this.supplierDetail.phone_number_3  == "")) {
            return false;
        } 
if (this.supplierDetail.email == null || this.supplierDetail.email  == "") {
            return false;
        } 
if (this.supplierDetail.address == null || this.supplierDetail.address  == "") {
            return false;
        } 
 return true;

 }
 }
