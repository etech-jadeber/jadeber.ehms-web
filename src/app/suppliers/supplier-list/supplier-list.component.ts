import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SupplierSummary, SupplierSummaryPartialList } from '../supplier.model';
import { SupplierPersist } from '../supplier.persist';
import { SupplierNavigator } from '../supplier.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { CompanyPersist } from 'src/app/Companys/Company.persist';
@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.css']
})export class SupplierListComponent implements OnInit {
  suppliersData: SupplierSummary[] = [];
  suppliersTotalCount: number = 0;
  supplierSelectAll:boolean = false;
  supplierSelection: SupplierSummary[] = [];

 suppliersDisplayedColumns: string[] = ["select","action","name","tin_number","phone_number_1","phone_number_2","phone_number_3","email","address", "status" ];
  supplierSearchTextBox: FormControl = new FormControl();
  supplierIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) suppliersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) suppliersSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public supplierPersist: SupplierPersist,
                public supplierNavigator: SupplierNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public companyPersist: CompanyPersist,

    ) {

        this.tcAuthorization.requireRead("suppliers");
       this.supplierSearchTextBox.setValue(supplierPersist.supplierSearchText);
      //delay subsequent keyup events
      this.supplierSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.supplierPersist.supplierSearchText = value;
        this.searchSuppliers();
      });
    } ngOnInit() {
   
      this.suppliersSort.sortChange.subscribe(() => {
        this.suppliersPaginator.pageIndex = 0;
        this.searchSuppliers(true);
      });

      this.suppliersPaginator.page.subscribe(() => {
        this.searchSuppliers(true);
      });
      //start by loading items
      this.searchSuppliers();
    }

  searchSuppliers(isPagination:boolean = false): void {


    let paginator = this.suppliersPaginator;
    let sorter = this.suppliersSort;

    this.supplierIsLoading = true;
    this.supplierSelection = [];

    this.supplierPersist.searchSupplier(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SupplierSummaryPartialList) => {
      this.suppliersData = partialList.data;
      if (partialList.total != -1) {
        this.suppliersTotalCount = partialList.total;
      }
      this.supplierIsLoading = false;
    }, error => {
      this.supplierIsLoading = false;
    });

  } downloadSuppliers(): void {
    if(this.supplierSelectAll){
         this.supplierPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download supplier", true);
      });
    }
    else{
        this.supplierPersist.download(this.tcUtilsArray.idsList(this.supplierSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download supplier",true);
            });
        }
  }
addSupplier(): void {
    let dialogRef = this.supplierNavigator.addSupplier();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSuppliers();
      }
    });
  }

  editSupplier(item: SupplierSummary) {
    let dialogRef = this.supplierNavigator.editSupplier(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSupplier(item: SupplierSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("supplier");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.supplierPersist.deleteSupplier(item.id).subscribe(response => {
          this.tcNotification.success("supplier deleted");
          this.searchSuppliers();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}
