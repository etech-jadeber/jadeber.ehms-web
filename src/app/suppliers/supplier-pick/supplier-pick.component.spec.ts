import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SupplierPickComponent } from './supplier-pick.component';

describe('SupplierPickComponent', () => {
  let component: SupplierPickComponent;
  let fixture: ComponentFixture<SupplierPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
