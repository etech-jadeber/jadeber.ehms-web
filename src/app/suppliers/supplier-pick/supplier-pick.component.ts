import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SupplierSummary, SupplierSummaryPartialList } from '../supplier.model';
import { SupplierPersist } from '../supplier.persist';
import { SupplierNavigator } from '../supplier.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
@Component({
  selector: 'app-supplier-pick',
  templateUrl: './supplier-pick.component.html',
  styleUrls: ['./supplier-pick.component.css']
})export class SupplierPickComponent implements OnInit {
  suppliersData: SupplierSummary[] = [];
  suppliersTotalCount: number = 0;
  supplierSelectAll:boolean = false;
  supplierSelection: SupplierSummary[] = [];

 suppliersDisplayedColumns: string[] = ["select" ,"name","tin_number","phone_number_1","phone_number_2","phone_number_3","email","address" ];
  supplierSearchTextBox: FormControl = new FormControl();
  supplierIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) suppliersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) suppliersSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public supplierPersist: SupplierPersist,
                public supplierNavigator: SupplierNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
 public dialogRef: MatDialogRef<SupplierPickComponent>,@Inject(MAT_DIALOG_DATA) public selectOne: boolean

    ) {

        this.tcAuthorization.requireRead("suppliers");
       this.supplierSearchTextBox.setValue(supplierPersist.supplierSearchText);
      //delay subsequent keyup events
      this.supplierSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.supplierPersist.supplierSearchText = value;
        this.searchSuppliers();
      });
    } ngOnInit() {
   
      this.suppliersSort.sortChange.subscribe(() => {
        this.suppliersPaginator.pageIndex = 0;
        this.searchSuppliers(true);
      });

      this.suppliersPaginator.page.subscribe(() => {
        this.searchSuppliers(true);
      });
      //start by loading items
      this.searchSuppliers();
    }

  searchSuppliers(isPagination:boolean = false): void {


    let paginator = this.suppliersPaginator;
    let sorter = this.suppliersSort;

    this.supplierIsLoading = true;
    this.supplierSelection = [];

    this.supplierPersist.searchSupplier(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SupplierSummaryPartialList) => {
      this.suppliersData = partialList.data;
      if (partialList.total != -1) {
        this.suppliersTotalCount = partialList.total;
      }
      this.supplierIsLoading = false;
    }, error => {
      this.supplierIsLoading = false;
    });

  }
  markOneItem(item: SupplierSummary) {
    if(!this.tcUtilsArray.containsId(this.supplierSelection,item.id)){
          this.supplierSelection = [];
          this.supplierSelection.push(item);
        }
        else{
          this.supplierSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.supplierSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }
