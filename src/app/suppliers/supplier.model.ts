import {TCId} from "../tc/models";

export class SupplierSummary extends TCId {
  name:string;
  tin_number:string;
  phone_number_1:string;
  phone_number_2:string;
  phone_number_3:string;
  email:string;
  address:string;
  status: number;
   
  }
  export class SupplierSummaryPartialList {
    data: SupplierSummary[];
    total: number;
  }
  export class SupplierDetail extends SupplierSummary {
  }
  
  export class SupplierDashboard {
    total: number;
  }