import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {SupplierEditComponent} from "./supplier-edit/supplier-edit.component";
import {SupplierPickComponent} from "./supplier-pick/supplier-pick.component";


@Injectable({
  providedIn: 'root'
})

export class SupplierNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  suppliersUrl(): string {
    return "/suppliers";
  }

  supplierUrl(id: string): string {
    return "/suppliers/" + id;
  }

  viewSuppliers(): void {
    this.router.navigateByUrl(this.suppliersUrl());
  }

  viewSupplier(id: string): void {
    this.router.navigateByUrl(this.supplierUrl(id));
  }

  editSupplier(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(SupplierEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSupplier(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(SupplierEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSuppliers(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(SupplierPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}