import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {SupplierDashboard, SupplierDetail, SupplierSummaryPartialList} from "./supplier.model";


@Injectable({
  providedIn: 'root'
})
export class SupplierPersist {

  supplierSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchSupplier(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<SupplierSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("suppliers", this.supplierSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<SupplierSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = this.supplierSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "suppliers/do", new TCDoParam("download_all", this.filters()));
  }

  supplierDashboard(): Observable<SupplierDashboard> {
    return this.http.get<SupplierDashboard>(environment.tcApiBaseUri + "suppliers/dashboard");
  }

  getSupplier(id: string): Observable<SupplierDetail> {
    return this.http.get<SupplierDetail>(environment.tcApiBaseUri + "suppliers/" + id);
  }

  addSupplier(item: SupplierDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "suppliers/", item);
  }

  updateSupplier(item: SupplierDetail): Observable<SupplierDetail> {
    return this.http.patch<SupplierDetail>(environment.tcApiBaseUri + "suppliers/" + item.id, item);
  }

  deleteSupplier(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "suppliers/" + id);
  }

  suppliersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "suppliers/do", new TCDoParam(method, payload));
  }

  supplierDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "suppliers/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "suppliers/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "suppliers/" + id + "/do", new TCDoParam("print", {}));
  }


}