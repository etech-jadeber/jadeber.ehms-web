import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryAppointmentListComponent } from './surgery-appointment-list.component';

describe('SurgeryAppointmentListComponent', () => {
  let component: SurgeryAppointmentListComponent;
  let fixture: ComponentFixture<SurgeryAppointmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryAppointmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryAppointmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
