import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList } from '../form_encounters/form_encounter.model';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { AppTranslation } from '../app.translation';
import { Form_EncounterNavigator } from '../form_encounters/form_encounter.navigator';
import { Form_EncounterPersist } from '../form_encounters/form_encounter.persist';
import { HistoryPersist } from '../historys/history.persist';
import { TCAsyncJob } from '../tc/asyncjob';
import { TCAuthorization } from '../tc/authorization';
import { FilePersist } from '../tc/files/file.persist';
import { JobData } from '../tc/jobs/job.model';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';

import { TCUtilsString } from '../tc/utils-string';
import { TCUtilsAngular } from '../tc/utils-angular';
import { Surgery_TypeSummary } from '../surgery_types/surgery_type.model';
import { Surgery_AppointmentPersist } from '../surgery_appointments/surgery_appointment.persist';
import { ProcedurePersist } from '../procedures/procedure.persist';
import { DoctorDetail, DoctorSummary } from '../doctors/doctor.model';
import { DoctorPersist } from '../doctors/doctor.persist';
import { surgery_status } from '../app.enums';
import { PatientSummary } from '../patients/patients.model';
import { PatientPersist } from '../patients/patients.persist';
import { PatientNavigator } from '../patients/patients.navigator';
import { OperationNotePersist } from '../operation_note/operation_note.persist';
import { SurgeryRoomPersist } from '../surgery_room/surgery_room.persist';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SurgicalSafetyCheckPersist } from '../surgical-safety-checklist/surgical-sefety-checklist';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-surgery-appointments-list',
  templateUrl: './surgery-appointment-list.component.html',
  styleUrls: ['./surgery-appointment-list.component.css']
})
export class SurgeryAppointmentListComponent implements OnInit {

  surgery_appointmentsIsLoading: boolean = false;
  surgery_appointmentsSelection: Surgery_AppointmentSummary[] = [];
  surgery_appointmentsData: Surgery_AppointmentSummary[] = [];
  surgery_appointmentsTotalCount: number = 0;
  surgery_appointmentsSelectAll: boolean = false;
  surgery_appointmentsSearchTextBox: FormControl = new FormControl();
  surgeryTypes: Surgery_TypeSummary[] = [];
  surgery_appointmentsDisplayedColumns: string[] = [
    'select',
    'action',
    'pid',
    'patient',
    'surgery_room',
    'surgery_date',
    'sergeon',
    'surgery_type_id',
    'surgery_status',
    'consent_id',
  ];
  doctors: DoctorSummary[] = [];
  surgery_status = surgery_status;
  patients: PatientSummary[] = [];

  @Input() encounterId: string;
  @Input() patientId: string;
  @Output() onResult = new EventEmitter<number>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;


  surgery_date: FormControl = new FormControl({disabled: true, value: new Date()});


  constructor(
    public form_encounterPersist: Form_EncounterPersist,
    public tcNotification: TCNotification,
    public tcUtilsDate: TCUtilsDate,
    public form_encounterNavigator: Form_EncounterNavigator,
    public tcNavigator: TCNavigator,
    public location: Location,
    public appTranslation: AppTranslation,
    public tcAuthorization: TCAuthorization,
    public tcUtilsArray: TCUtilsArray,
    public historyPersist: HistoryPersist,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public tcAsyncJob: TCAsyncJob,
    public filePersist: FilePersist,
    public surgery_appointmentPersist: Surgery_AppointmentPersist,
    public procedurePersist: ProcedurePersist,
    public doctorPersist: DoctorPersist,
    public patientPersist: PatientPersist,
    public patientNavigator: PatientNavigator,
    public operation_notePersist: OperationNotePersist,
    public surgerySafetyChecklistNavigator: SurgicalSafetyCheckPersist,
    public surgeryRoomPersist: SurgeryRoomPersist,

  ) { 

    this.surgery_appointmentsSearchTextBox.setValue(
      form_encounterPersist.surgery_appointmentSearchText
    );
    //delay subsequent keyup events
    this.surgery_appointmentsSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.form_encounterPersist.surgery_appointmentSearchText = value;
        this.searchSurgery_Appointments();
      });

      this.form_encounterPersist.surgery_date = Math.floor(new Date(this.surgery_date.value).getTime() / 1000)
      this.surgery_date.valueChanges.pipe(debounceTime(500))
      .subscribe((value)=>{
        this.form_encounterPersist.surgery_date =new Date(value._d).getTime()/1000;
        this.searchSurgery_Appointments();
      });
  }

  ngOnInit(): void {
        this.form_encounterPersist.surgeryAppointmentPatientId = this.patientId
        // surgery appointments sorter
        this.sorter.sortChange.subscribe(() => {
          this.paginator.pageIndex = 0;
          this.searchSurgery_Appointments(true);
        });
  
      // surgery appointments paginator
      this.paginator.page.subscribe(() => {
          this.searchSurgery_Appointments(true);
        });
        this.searchSurgery_Appointments()
  }

  handleToggle(event: MatSlideToggleChange){
    if (event.checked){
    this.form_encounterPersist.surgeryAppointmentEncounterId = this.encounterId;
    } else {
    this.form_encounterPersist.surgeryAppointmentEncounterId = null;
    }
    this.searchSurgery_Appointments()
    }


  // surgery appointment methods
  searchSurgery_Appointments(isPagination: boolean = false): void {
    let paginator =
      this.paginator;
    let sorter = this.sorter;

    this.surgery_appointmentsIsLoading = true;
    this.surgery_appointmentsSelection = [];

    this.form_encounterPersist
      .searchSurgery_Appointment(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: Surgery_AppointmentSummaryPartialList) => {
          this.surgery_appointmentsData = partialList.data;
          // this.surgery_appointmentsData.forEach((appointment, idx) => {
          //   this.procedurePersist
          //     .getProcedure(appointment.planned_procedure)
          //     .subscribe((procedure) => {
          //       this.surgery_appointmentsData[idx].procedure_name =
          //         procedure.name;
          //     });
          //     // this.operation_notePersist.getOperationNote(appointment.operation_note_id).subscribe((operation_note)=>{
          //       this.doctorPersist.getDoctor(appointment.sergeon_id).subscribe((doctor)=>{
          //         appointment.sergeon_name = doctor.first_name + " " + doctor.middle_name + " " + doctor.last_name;
          //       })
          //     // })
          //     this.form_encounterPersist.getForm_Encounter(appointment.encounter_id).subscribe((encounter)=>{
          //       this.patientPersist.getPatient(encounter.pid).subscribe((patient)=>{
          //         appointment.patient_name = patient.fname + " " + patient.mname + " " + patient.lname;
          //       })
          //     })
          //     this.surgeryRoomPersist.getSurgeryRoom(appointment.surgery_room).subscribe((surgery_room)=>{
          //       appointment.surgery_room_name = surgery_room.name;
          //     })
          // });
          if (partialList.total != -1) {
            this.onResult.emit(partialList.total);

          }
          this.surgery_appointmentsIsLoading = false;
        },
        (error) => {
          this.surgery_appointmentsIsLoading = false;
        }
      );
  }

  addSurgery_Appointment(): void {
    let dialogRef = this.form_encounterNavigator.addSurgery_Appointment(
      this.encounterId
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchSurgery_Appointments();
      }
    });
  }
  downloadFile(id:string) :void {
    this.filePersist.getDownloadKey(id).subscribe((key:any)=>{
      if(key){
        this.tcNavigator.openUrl(this.filePersist.downloadLink(key.id),true);
      }
    })
  }
  editSurgery_Appointment(item: Surgery_AppointmentSummary) {
    let dialogRef = this.form_encounterNavigator.editSurgery_Appointment(
      this.encounterId,
      item.id
    );
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchSurgery_Appointments();
      }
    });
  }

  deleteSurgery_Appointment(item: Surgery_AppointmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('Surgery_Appointment');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.form_encounterPersist
          .deleteSurgery_Appointment(this.encounterId, item.id)
          .subscribe(
            (response) => {
              this.tcNotification.success(
                this.appTranslation.getText('patient', 'surgery_appointment') +
                  ' ' +
                  this.appTranslation.getText('general', 'deleted')
              );
              this.searchSurgery_Appointments();
            },
            (error) => {}
          );
      }
    });
  }


  downloadSurgery_Appointments(): void {
    if (this.surgery_appointmentsSelectAll) {
      this.surgery_appointmentPersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download surgery_appointments',
          true
        );
      });
    } else {
      this.surgery_appointmentPersist
        .download(this.tcUtilsArray.idsList(this.surgery_appointmentsSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download surgery_appointments',
            true
          );
        });
    }
  }

  getDoctors() {
    this.doctorPersist.searchDoctor(50, 0, '', 'asc').subscribe((result) => {
      this.doctors = result.data;
    });
  }

  getPatients() {
    this.patientPersist.searchPatient(50, 0, '', 'asc').subscribe((result) => {
      this.patients = result.data;
    });
  }
  

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(
      (value) => value.uuidd == patientId
    );
    if (patient) {
      return `${patient.fname} ${patient.lname}`;
    }
    return '';
  }

  getDoctorFullName(doctorId: string) {
    const doctor: DoctorDetail = this.tcUtilsArray.getById(
      this.doctors,
      doctorId
    );

    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`;
    }
    return '';
  }
  
  setStartSurgery(item: Surgery_AppointmentSummary) {
    this.form_encounterPersist
      .surgery_appointmentDo(item.id, 'start_time', item)
      .subscribe(
        (value) => {
          this.tcNotification.success('Surgery started');
          this.searchSurgery_Appointments();
        },
        (error) => {
          console.error(error);
        }
      );
  }

  setEndtSurgery(item: Surgery_AppointmentSummary) {
    this.form_encounterPersist
      .surgery_appointmentDo(item.id, 'end_time', item)
      .subscribe(
        (value) => {
          this.tcNotification.success('Surgery eneded');
          this.searchSurgery_Appointments();
        },
        (error) => {
          console.error(error);
        }
      );
  }

  cancelAppointment(item: Surgery_AppointmentSummary) {
    this.form_encounterPersist
      .surgery_appointmentDo(item.id, 'cancelled', item)
      .subscribe(
        (value) => {
          this.tcNotification.success('Surgery cancelled');
          this.searchSurgery_Appointments();
        },
        (error) => {
          console.error(error);
        }
      );
  }
  back(): void {
    this.location.back();
  } 
}
