import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryAppointmentDetailComponent } from './surgery-appointment-detail.component';

describe('SurgeryAppointmentDetailComponent', () => {
  let component: SurgeryAppointmentDetailComponent;
  let fixture: ComponentFixture<SurgeryAppointmentDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryAppointmentDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryAppointmentDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
