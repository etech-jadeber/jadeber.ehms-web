import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Surgery_AppointmentDetail} from "../surgery_appointment.model";
import {Surgery_AppointmentPersist} from "../surgery_appointment.persist";
import {Surgery_AppointmentNavigator} from "../surgery_appointment.navigator";

export enum Surgery_AppointmentTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-surgery-appointment-detail',
  templateUrl: './surgery-appointment-detail.component.html',
  styleUrls: ['./surgery-appointment-detail.component.css']
})
export class Surgery_AppointmentDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  surgery_appointmentLoading:boolean = false;
  
  Surgery_AppointmentTabs: typeof Surgery_AppointmentTabs = Surgery_AppointmentTabs;
  activeTab: Surgery_AppointmentTabs = Surgery_AppointmentTabs.overview;
  //basics
  surgery_appointmentDetail: Surgery_AppointmentDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public surgery_appointmentNavigator: Surgery_AppointmentNavigator,
              public  surgery_appointmentPersist: Surgery_AppointmentPersist) {
    this.tcAuthorization.requireRead("surgery_appointments");
    this.surgery_appointmentDetail = new Surgery_AppointmentDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("surgery_appointments");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.surgery_appointmentLoading = true;
    this.surgery_appointmentPersist.getSurgery_Appointment(id).subscribe(surgery_appointmentDetail => {
          this.surgery_appointmentDetail = surgery_appointmentDetail;
          this.surgery_appointmentLoading = false;
        }, error => {
          console.error(error);
          this.surgery_appointmentLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.surgery_appointmentDetail.id);
  }

  editSurgery_Appointment(): void {
    let modalRef = this.surgery_appointmentNavigator.editSurgery_Appointment(this.surgery_appointmentDetail.id);
    modalRef.afterClosed().subscribe(modifiedSurgery_AppointmentDetail => {
      TCUtilsAngular.assign(this.surgery_appointmentDetail, modifiedSurgery_AppointmentDetail);
    }, error => {
      console.error(error);
    });
  }

   printSurgery_Appointment():void{
      this.surgery_appointmentPersist.print(this.surgery_appointmentDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print surgery_appointment", true);
      });
    }

  back():void{
      this.location.back();
    }

}

