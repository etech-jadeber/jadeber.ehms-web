import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Surgery_AppointmentDetail} from "../surgery_appointment.model";
import {Surgery_AppointmentPersist} from "../surgery_appointment.persist";
import { Surgery_TypeNavigator } from 'src/app/surgery_types/surgery_type.navigator';
import { Surgery_TypeSummary } from 'src/app/surgery_types/surgery_type.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { DoctorNavigator } from 'src/app/doctors/doctor.navigator';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { TCUtilsDate } from 'src/app/tc/utils-date';


@Component({
  selector: 'app-surgery-appointment-edit',
  templateUrl: './surgery-appointment-edit.component.html',
  styleUrls: ['./surgery-appointment-edit.component.css']
})
export class Surgery_AppointmentEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  surgery_appointmentDetail: Surgery_AppointmentDetail;
  surgeryTypeName: string;
  patientName: string;
  doctorName: string;
  start_time:Date;
  end_time:Date;
  appointed_time:Date;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Surgery_AppointmentEditComponent>,
              public  persist: Surgery_AppointmentPersist,
              public surgeryTypeNavigator: Surgery_TypeNavigator,
              public patientNavigator: PatientNavigator,
              public doctorNavigator: DoctorNavigator,
              public tCUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("surgery_appointments");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "surgery_appointment");
      this.surgery_appointmentDetail = new Surgery_AppointmentDetail();
      //set necessary defaults
      this.surgery_appointmentDetail.surgery_status = 100;
    } else {
     this.tcAuthorization.requireUpdate("surgery_appointments");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "surgery_appointment");
      this.isLoadingResults = true;
      this.persist.getSurgery_Appointment(this.idMode.id).subscribe(surgery_appointmentDetail => {
      this.surgery_appointmentDetail = surgery_appointmentDetail;
      this.surgery_appointmentDetail.surgery_status = 100;
      this.transformDates(false);
      this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.addSurgery_Appointment(this.surgery_appointmentDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "added"));
      this.surgery_appointmentDetail.id = value.id;
      this.dialogRef.close(this.surgery_appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates();
    this.persist.updateSurgery_Appointment(this.surgery_appointmentDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "updated"));
      this.dialogRef.close(this.surgery_appointmentDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.surgery_appointmentDetail == null){
            return false;
          }

        if (this.surgery_appointmentDetail.patient_id == null){
          return false;
        }

        if (this.surgery_appointmentDetail.doctor_id == null){
          return false;
        }

        if (this.appointed_time == null){
          return false;
        }

        if (this.surgery_appointmentDetail.surgery_type_id == null || this.surgery_appointmentDetail.surgery_type_id == ""){
          return false;
        }

        return true;
      }

      searchSurgeryType() {
        let dialogRef = this.surgeryTypeNavigator.pickSurgery_Types(true);
        dialogRef.afterClosed().subscribe((result: Surgery_TypeSummary[]) => {
          if (result) {
            this.surgeryTypeName = result[0].surgery_name;
            this.surgery_appointmentDetail.surgery_type_id = result[0].id;
          }
        });
      }

      searchPatient(){
        let dialogRef = this.patientNavigator.pickPatients(true);
        dialogRef.afterClosed().subscribe((result: PatientSummary[]) => {
          if (result){
            this.patientName = result[0].fname + " " + result[0].lname;
            this.surgery_appointmentDetail.patient_id = result[0].uuidd;
          }
        })
      }

      searchDoctor(){
        let dialogRef = this.doctorNavigator.pickDoctors(true);
        dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
          if (result){
            this.doctorName = result[0].first_name + " " + result[0].last_name;
            this.surgery_appointmentDetail.doctor_id = result[0].id;
          }
        })
      }

      transformDates(todate: boolean = true) {
        if (todate) {
          this.surgery_appointmentDetail.surgery_date = new Date(this.appointed_time).getTime()/1000;
        } else {
          this.start_time = this.tCUtilsDate.toDate(this.surgery_appointmentDetail.start_time);
          this.end_time = this.tCUtilsDate.toDate(this.surgery_appointmentDetail.end_time);
          this.appointed_time = this.tCUtilsDate.toDate(this.surgery_appointmentDetail.surgery_date)
        }
      }


}

