
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Surgery_AppointmentPersist} from "../surgery_appointment.persist";
import {Surgery_AppointmentNavigator} from "../surgery_appointment.navigator";
import {Surgery_AppointmentDetail, Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList} from "../surgery_appointment.model";
import { PatientSummary } from 'src/app/patients/patients.model';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { Surgery_TypeSummary } from 'src/app/surgery_types/surgery_type.model';
import { PatientPersist } from 'src/app/patients/patients.persist';
import { Surgery_TypePersist } from 'src/app/surgery_types/surgery_type.persist';
import { DoctorSummary } from 'src/app/doctors/doctor.model';
import { DoctorPersist } from 'src/app/doctors/doctor.persist';
import { Form_EncounterPersist } from 'src/app/form_encounters/form_encounter.persist';
import { ProcedurePersist } from 'src/app/procedures/procedure.persist';
import { Department_SpecialtyPersist } from 'src/app/department_specialtys/department_specialty.persist';
import { Form_EncounterNavigator } from 'src/app/form_encounters/form_encounter.navigator';
import { payment_status, surgery_status } from 'src/app/app.enums';
import { PaymentPersist } from 'src/app/payments/payment.persist';
import { SurgeryRoomPersist } from 'src/app/surgery_room/surgery_room.persist';
import { OperationNotePersist } from 'src/app/operation_note/operation_note.persist';
import { OperationNoteNavigator } from 'src/app/operation_note/operation_note.navigator';
import { SurgicalSafetyCheckPersist } from 'src/app/surgical-safety-checklist/surgical-sefety-checklist';


@Component({
  selector: 'app-surgery-appointment-list',
  templateUrl: './surgery-appointment-list.component.html',
  styleUrls: ['./surgery-appointment-list.component.css']
})
export class Surgery_AppointmentListComponent implements OnInit {

  surgery_appointmentsData: Surgery_AppointmentSummary[] = [];
  surgery_appointmentsTotalCount: number = 0;
  surgery_appointmentsSelectAll:boolean = false;
  surgery_appointmentsSelection: Surgery_AppointmentSummary[] = [];
  patients: PatientSummary[] = [];
  surgeryTypes: Surgery_TypeSummary[] = [];
  doctors: DoctorSummary[] = [];

  surgery_status = surgery_status;

  surgery_appointmentsDisplayedColumns: string[] = ["select","action",'surgery_room',"surgery_date", "patient_id",'sergeon',"procedure","surgery_status" ];
  surgery_appointmentsSearchTextBox: FormControl = new FormControl();
  surgery_appointmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) surgery_appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgery_appointmentsSort: MatSort;
  @Input() status: number;
  surgery_date: FormControl = new FormControl({disabled: true, value: new Date()});

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public surgery_appointmentPersist: Surgery_AppointmentPersist,
                public surgery_appointmentNavigator: Surgery_AppointmentNavigator,
                public jobPersist: JobPersist,
                public patientPersist: PatientPersist,
                public surgeryTypePersist: Surgery_TypePersist,
                public patientNavigator: PatientNavigator,
                public tCUtilsDate: TCUtilsDate,
                public doctorPersist: DoctorPersist,
                public formEncounterPersist: Form_EncounterPersist,
                public procedurePersist: ProcedurePersist,
                public departmentSpecialityPersist: Department_SpecialtyPersist,
                public formEncounterNavigator: Form_EncounterNavigator,
                public paymentPersist: PaymentPersist,
                public surgeryRoomPersist: SurgeryRoomPersist,
                public operation_notePersist: OperationNotePersist,
                public surgerySafetyChecklistNavigator: SurgicalSafetyCheckPersist,
                public operationNoteNavigator: OperationNoteNavigator,
    ) {

        this.tcAuthorization.requireRead("surgery_appointments");
       this.surgery_appointmentsSearchTextBox.setValue(surgery_appointmentPersist.surgery_appointmentSearchText);
      //delay subsequent keyup events
      this.surgery_appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.surgery_appointmentPersist.surgery_appointmentSearchText = value;
        this.searchSurgery_Appointments();
      });
      this.surgery_appointmentPersist.surgery_date = Math.floor(new Date(this.surgery_date.value).getTime() / 1000)
      this.surgery_date.valueChanges.pipe(debounceTime(500))
      .subscribe((value)=>{
        this.surgery_appointmentPersist.surgery_date =new Date(value._d).getTime()/1000;
        this.searchSurgery_Appointments();
      });
    }

    ngOnInit() {
      this.surgery_appointmentsSort.sortChange.subscribe(() => {
        this.surgery_appointmentsPaginator.pageIndex = 0;
        this.searchSurgery_Appointments(true);
      });

      this.surgery_appointmentsPaginator.page.subscribe(() => {
        this.searchSurgery_Appointments(true);
      });
      if(this.status == surgery_status.sent){
        this.surgery_appointmentsDisplayedColumns.splice(2, 1)
      }
      //start by loading items
      this.searchSurgery_Appointments();
    }

  searchSurgery_Appointments(isPagination:boolean = false): void {


    let paginator = this.surgery_appointmentsPaginator;
    let sorter = this.surgery_appointmentsSort;

    this.surgery_appointmentsIsLoading = true;
    this.surgery_appointmentsSelection = [];
    this.surgery_appointmentPersist.surgery_statuId = this.status
    this.surgery_appointmentPersist.searchSurgery_Appointment(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Surgery_AppointmentSummaryPartialList) => {
      this.surgery_appointmentsData = partialList.data;
      // this.surgery_appointmentsData.forEach((data, idx) => {
      //   this.formEncounterPersist.getForm_Encounter(data.encounter_id).subscribe(encounter => {
      //     data.patient_name = encounter.full_name
      //     // this.operation_notePersist.getOperationNote(data.operation_note_id).subscribe((operation_note)=>{
      //       this.doctorPersist.getDoctor(data.sergeon_id).subscribe((doctor)=>{
      //         data.sergeon_name = doctor.first_name + " " + doctor.middle_name + " " + doctor.last_name;
      //       })
      //     // })
      //     this.surgeryRoomPersist.getSurgeryRoom(data.surgery_room).subscribe((surgery_room)=>{
      //       data.surgery_room_name = surgery_room.name;
      //     })
      //   });
      //   this.procedurePersist.getProcedure(data.planned_procedure).subscribe(procedure => {
      //     data.procedure_name = procedure.name;
      //   });
      // })
      if (partialList.total != -1) {
        this.surgery_appointmentsTotalCount = partialList.total;
      }
      this.surgery_appointmentsIsLoading = false;
    }, error => {
      this.surgery_appointmentsIsLoading = false;
    });

  }

  downloadSurgery_Appointments(): void {
    if(this.surgery_appointmentsSelectAll){
         this.surgery_appointmentPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download surgery_appointments", true);
      });
    }
    else{
        this.surgery_appointmentPersist.download(this.tcUtilsArray.idsList(this.surgery_appointmentsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download surgery_appointments",true);
            });
        }
  }



  addSurgery_Appointment(): void {
    let dialogRef = this.surgery_appointmentNavigator.addSurgery_Appointment();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSurgery_Appointments();
      }
    });
  }

  editSurgery_Appointment(item: Surgery_AppointmentSummary) {
    // if(item.payment_id==null){
    //   this.tcNotification.error("surgery appointment payment is not created");
    // } else {
    // this.paymentPersist.getPayment(item.payment_id).subscribe((result)=>{
    //   if(result){
    //     if(result.payment_status==payment_status.not_paid){
    //        this.tcNotification.error("surgery appointment fee is not paid");
    //     }
    //     else{
            let dialogRef = this.formEncounterNavigator.editSurgery_Appointment(item.encounter_id, item.id);
            dialogRef.afterClosed().subscribe(result => {
            if (result) {
              TCUtilsAngular.assign(item, result);
              this.searchSurgery_Appointments();
            }

            });
    //     }
    //   }
    // })
  // }
    
  }

  navigateToChecklist(item: Surgery_AppointmentSummary) {
    this.surgerySafetyChecklistNavigator.viewSurgicalSafetyChecklist(item.operation_note_id)
    // this.paymentPersist.getPayment(item.payment_id).subscribe((value) => {
    //   if (value.payment_status == payment_status.paid){
    //     this.paymentPersist.getPayment(item.surgery_room_payment_id).subscribe((pay) => {
    //       if (pay.payment_status == payment_status.paid){
    //         this.surgerySafetyChecklistNavigator.viewSurgicalSafetyChecklist(item.operation_note_id)
    //       } else {
    //         this.tcNotification.error("The patient did not pay")
    //       }
    //     })
    //   } else {
    //     this.tcNotification.error("The patient did not pay")
    //   }
    // })
  }

  deleteSurgery_Appointment(item: Surgery_AppointmentSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Surgery_Appointment");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.surgery_appointmentPersist.deleteSurgery_Appointment(item.id).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("patient", "surgery_appointment") + " " + this.appTranslation.getText("general" , "deleted"));
          this.searchSurgery_Appointments();
        }, error => {
        });
      }

    });
  }

  setStartSurgery(item: Surgery_AppointmentSummary){
    item.start_time = Math.floor(new Date().getTime()/1000);
    this.surgery_appointmentPersist.surgery_appointmentDo(item.id,"start_time",item).subscribe(value => {
      this.tcNotification.success("Surgery started");
      this.searchSurgery_Appointments();
    }, error => {
      console.error(error);
    })
  }

  setEndtSurgery(item: Surgery_AppointmentSummary){
    item.end_time = Math.floor(new Date().getTime()/1000);
    this.surgery_appointmentPersist.surgery_appointmentDo(item.id,"end_time",item).subscribe(value => {
      this.tcNotification.success("Surgery eneded");
      this.searchSurgery_Appointments();
    }, error => {
      console.error(error);
    })
  }

  cancelAppointment(item: Surgery_AppointmentSummary){
    this.surgery_appointmentPersist.surgery_appointmentDo(item.id,"cancelled",item).subscribe(value => {
      this.tcNotification.success("Surgery cancelled");
      this.searchSurgery_Appointments();
    }, error => {
      console.error(error);
    })
  }

  back():void{
      this.location.back();
    }

  getPatientFullName(patientId: string) {
    const patient: PatientSummary = this.patients.find(value => value.uuidd == patientId);
    if (patient) {
      return `${patient.fname} ${patient.lname}`
    }
    return "";
  }

  getDoctorFullName(doctorId: string) {
    const doctor: DoctorSummary = this.doctors.find(value => value.id == doctorId);
    if (doctor) {
      return `${doctor.first_name} ${doctor.last_name}`
    }
    return "";
  }

  ngOnDestroy():void{
    this.surgery_appointmentPersist.surgery_statuId = undefined;
  }

}

