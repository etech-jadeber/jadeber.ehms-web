import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryAppointmentPickComponent } from './surgery-appointment-pick.component';

describe('SurgeryAppointmentPickComponent', () => {
  let component: SurgeryAppointmentPickComponent;
  let fixture: ComponentFixture<SurgeryAppointmentPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryAppointmentPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryAppointmentPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
