import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Surgery_AppointmentDetail, Surgery_AppointmentSummary, Surgery_AppointmentSummaryPartialList} from "../surgery_appointment.model";
import {Surgery_AppointmentPersist} from "../surgery_appointment.persist";


@Component({
  selector: 'app-surgery-appointment-pick',
  templateUrl: './surgery-appointment-pick.component.html',
  styleUrls: ['./surgery-appointment-pick.component.css']
})
export class Surgery_AppointmentPickComponent implements OnInit {

  surgery_appointmentsData: Surgery_AppointmentSummary[] = [];
  surgery_appointmentsTotalCount: number = 0;
  surgery_appointmentsSelection: Surgery_AppointmentSummary[] = [];
  surgery_appointmentsDisplayedColumns: string[] = ["select", 'patient_id','surgery_type_id','doctor_id','surgery_date','start_time','end_time','surgery_status' ];

  surgery_appointmentsSearchTextBox: FormControl = new FormControl();
  surgery_appointmentsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) surgery_appointmentsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgery_appointmentsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public surgery_appointmentPersist: Surgery_AppointmentPersist,
              public dialogRef: MatDialogRef<Surgery_AppointmentPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("surgery_appointments");
    this.surgery_appointmentsSearchTextBox.setValue(surgery_appointmentPersist.surgery_appointmentSearchText);
    //delay subsequent keyup events
    this.surgery_appointmentsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.surgery_appointmentPersist.surgery_appointmentSearchText = value;
      this.searchSurgery_Appointments();
    });
  }

  ngOnInit() {

    this.surgery_appointmentsSort.sortChange.subscribe(() => {
      this.surgery_appointmentsPaginator.pageIndex = 0;
      this.searchSurgery_Appointments();
    });

    this.surgery_appointmentsPaginator.page.subscribe(() => {
      this.searchSurgery_Appointments();
    });

    //set initial picker list to 5
    this.surgery_appointmentsPaginator.pageSize = 5;

    //start by loading items
    this.searchSurgery_Appointments();
  }

  searchSurgery_Appointments(): void {
    this.surgery_appointmentsIsLoading = true;
    this.surgery_appointmentsSelection = [];

    this.surgery_appointmentPersist.searchSurgery_Appointment(this.surgery_appointmentsPaginator.pageSize,
        this.surgery_appointmentsPaginator.pageIndex,
        this.surgery_appointmentsSort.active,
        this.surgery_appointmentsSort.direction).subscribe((partialList: Surgery_AppointmentSummaryPartialList) => {
      this.surgery_appointmentsData = partialList.data;
      if (partialList.total != -1) {
        this.surgery_appointmentsTotalCount = partialList.total;
      }
      this.surgery_appointmentsIsLoading = false;
    }, error => {
      this.surgery_appointmentsIsLoading = false;
    });

  }

  markOneItem(item: Surgery_AppointmentSummary) {
    if(!this.tcUtilsArray.containsId(this.surgery_appointmentsSelection,item.id)){
          this.surgery_appointmentsSelection = [];
          this.surgery_appointmentsSelection.push(item);
        }
        else{
          this.surgery_appointmentsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.surgery_appointmentsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
