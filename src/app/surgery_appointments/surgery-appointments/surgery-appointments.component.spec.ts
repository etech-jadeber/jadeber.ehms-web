import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryAppointmentsComponent } from './surgery-appointments.component';

describe('SurgeryAppointmentsComponent', () => {
  let component: SurgeryAppointmentsComponent;
  let fixture: ComponentFixture<SurgeryAppointmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryAppointmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryAppointmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
