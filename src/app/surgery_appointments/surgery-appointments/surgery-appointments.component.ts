import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { surgery_status } from 'src/app/app.enums';
import { Surgery_AppointmentPersist } from '../surgery_appointment.persist';

@Component({
  selector: 'app-surgery-appointments',
  templateUrl: './surgery-appointments.component.html',
  styleUrls: ['./surgery-appointments.component.scss']
})
export class SurgeryAppointmentsComponent implements OnInit {
Surgery_status: any;

  
  constructor(
    // public surgery_status: surgery_status,
    public surgery_appointementPersist: Surgery_AppointmentPersist,

  ) { }

  ngOnInit(): void {
    this.Surgery_status =surgery_status
  }
  onTabChanged(matTab: MatTabChangeEvent){
    this.surgery_appointementPersist.selectedIndex=matTab.index;
  }
}
