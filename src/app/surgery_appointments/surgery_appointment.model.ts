import {TCId} from "../tc/models";

export class Surgery_AppointmentSummary extends TCId {
patient_id : string;
surgery_type_id : string;
doctor_id : string;
surgery_date : number;
start_time : number;
end_time : number;
surgery_status : number;
encounter_id: string;
patient_name: string;
department: string;
planned_procedure: string;
procedure_name: string;
operation_note_id: string;
payment_id: string;
surgery_room_name: string;
sergeon_name : string;
surgery_room: string;
duration:number;
sergeon_id: string;
surgery_room_payment_id: string;
}

export class Surgery_AppointmentSummaryPartialList {
  data: Surgery_AppointmentSummary[];
  total: number;
}

export class Surgery_AppointmentDetail extends Surgery_AppointmentSummary {
patient_id : string;
surgery_type_id : string;
doctor_id : string;
surgery_date : number;
start_time : number;
end_time : number;
surgery_status : number;
}

export class Surgery_AppointmentDashboard {
  total: number;
}
