import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Surgery_AppointmentEditComponent} from "./surgery-appointment-edit/surgery-appointment-edit.component";
import {Surgery_AppointmentPickComponent} from "./surgery-appointment-pick/surgery-appointment-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Surgery_AppointmentNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  surgery_appointmentsUrl(): string {
    return "/surgery_appointments";
  }
  surgeryAppointemntUrl():string {
    return "/surgery_appointment"
  }

  surgery_appointmentUrl(id: string): string {
    return "/surgery_appointments/" + id;
  }

  viewSurgery_Appointments(): void {
    this.router.navigateByUrl(this.surgery_appointmentsUrl());
  }

  viewSurgery_Appointment(id: string): void {
    this.router.navigateByUrl(this.surgery_appointmentUrl(id));
  }

  editSurgery_Appointment(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Surgery_AppointmentEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSurgery_Appointment(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Surgery_AppointmentEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSurgery_Appointments(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Surgery_AppointmentPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
