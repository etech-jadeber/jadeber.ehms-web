import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {Surgery_AppointmentDashboard, Surgery_AppointmentDetail, Surgery_AppointmentSummaryPartialList} from "./surgery_appointment.model";
import {surgery_status} from "../app.enums"
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class Surgery_AppointmentPersist {

  surgery_appointmentSearchText: string = "";
  surgery_statuId: number ;
  selectedIndex: number;
  surgery_date: number;

  surgery_status: TCEnum[] = [
    new TCEnum(surgery_status.sent, 'appointed'),
    new TCEnum(surgery_status.appointed, 'booked'),
    new TCEnum(surgery_status.on_surgery, 'on_surgery'),
    new TCEnum(surgery_status.cancelled, 'cancelled'),
    new TCEnum(surgery_status.completed, 'completed'),
    new TCEnum(surgery_status.rejected, 'rejected')
  ];

  constructor(private http: HttpClient) {
  }

  searchSurgery_Appointment(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Surgery_AppointmentSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("surgery_appointments", this.surgery_appointmentSearchText, pageSize, pageIndex, sort, order);
    if (this.surgery_statuId) {
      url = TCUtilsString.appendUrlParameter(url, "surgery_status", this.surgery_statuId.toString());
    }

    url = TCUtilsString.appendUrlParameter(url, "surgery_date",this.surgery_date ? this.surgery_date.toString(): '')
    return this.http.get<Surgery_AppointmentSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.surgery_appointmentSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_appointments/do", new TCDoParam("download_all", this.filters()));
  }

  surgery_appointmentDashboard(): Observable<Surgery_AppointmentDashboard> {
    return this.http.get<Surgery_AppointmentDashboard>(environment.tcApiBaseUri + "surgery_appointments/dashboard");
  }

  getSurgery_Appointment(id: string): Observable<Surgery_AppointmentDetail> {
    return this.http.get<Surgery_AppointmentDetail>(environment.tcApiBaseUri + "surgery_appointments/" + id);
  }

  addSurgery_Appointment(item: Surgery_AppointmentDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_appointments/", item);
  }

  updateSurgery_Appointment(item: Surgery_AppointmentDetail): Observable<Surgery_AppointmentDetail> {
    return this.http.patch<Surgery_AppointmentDetail>(environment.tcApiBaseUri + "surgery_appointments/" + item.id, item);
  }

  deleteSurgery_Appointment(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "surgery_appointments/" + id);
  }

  surgery_appointmentsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_appointments/do", new TCDoParam(method, payload));
  }

  surgery_appointmentDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_appointments/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_appointments/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_appointments/" + id + "/do", new TCDoParam("print", {}));
  }


}
