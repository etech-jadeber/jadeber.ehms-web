import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryRoomDetailComponent } from './surgery-room-detail.component';

describe('SurgeryRoomDetailComponent', () => {
  let component: SurgeryRoomDetailComponent;
  let fixture: ComponentFixture<SurgeryRoomDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryRoomDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryRoomDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
