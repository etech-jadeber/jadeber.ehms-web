import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";

import {MatSort} from "@angular/material/sort";
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {SurgeryRoomDetail} from "../surgery_room.model";
import {SurgeryRoomPersist} from "../surgery_room.persist";
import {SurgeryRoomNavigator} from "../surgery_room.navigator";

export enum SurgeryRoomTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-surgery_room-detail',
  templateUrl: './surgery-room-detail.component.html',
  styleUrls: ['./surgery-room-detail.component.scss']
})
export class SurgeryRoomDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  surgeryRoomIsLoading:boolean = false;
  
  SurgeryRoomTabs: typeof SurgeryRoomTabs = SurgeryRoomTabs;
  activeTab: SurgeryRoomTabs = SurgeryRoomTabs.overview;
  //basics
  surgeryRoomDetail: SurgeryRoomDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public surgeryRoomNavigator: SurgeryRoomNavigator,
              public  surgeryRoomPersist: SurgeryRoomPersist) {
    this.tcAuthorization.requireRead("surgery_rooms");
    this.surgeryRoomDetail = new SurgeryRoomDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("surgery_rooms");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.surgeryRoomIsLoading = true;
    this.surgeryRoomPersist.getSurgeryRoom(id).subscribe(surgeryRoomDetail => {
          this.surgeryRoomDetail = surgeryRoomDetail;
          this.surgeryRoomIsLoading = false;
        }, error => {
          console.error(error);
          this.surgeryRoomIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.surgeryRoomDetail.id);
  }
  editSurgeryRoom(): void {
    let modalRef = this.surgeryRoomNavigator.editSurgeryRoom(this.surgeryRoomDetail.id);
    modalRef.afterClosed().subscribe(surgeryRoomDetail => {
      TCUtilsAngular.assign(this.surgeryRoomDetail, surgeryRoomDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.surgeryRoomPersist.print(this.surgeryRoomDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print surgery_room", true);
      });
    }

  back():void{
      this.location.back();
    }

}