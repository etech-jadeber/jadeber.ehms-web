import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryRoomEditComponent } from './surgery-room-edit.component';

describe('SurgeryRoomEditComponent', () => {
  let component: SurgeryRoomEditComponent;
  let fixture: ComponentFixture<SurgeryRoomEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryRoomEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryRoomEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
