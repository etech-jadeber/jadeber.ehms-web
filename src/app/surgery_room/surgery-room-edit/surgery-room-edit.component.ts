import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { SurgeryRoomDetail } from '../surgery_room.model';
import { SurgeryRoomPersist } from '../surgery_room.persist';
import { bed_status } from 'src/app/app.enums';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
@Component({
  selector: 'app-surgery_room-edit',
  templateUrl: './surgery-room-edit.component.html',
  styleUrls: ['./surgery-room-edit.component.scss']
})export class SurgeryRoomEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  surgeryRoomDetail: SurgeryRoomDetail;constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<SurgeryRoomEditComponent>,
              public  persist: SurgeryRoomPersist,
              public bed_persist: BedPersist,
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("surgery_rooms");
      this.title = this.appTranslation.getText("general","new") +  " " + "surgery_room";
      this.surgeryRoomDetail = new SurgeryRoomDetail();
      this.surgeryRoomDetail.status=bed_status.available;
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("surgery_rooms");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "surgery_room";
      this.isLoadingResults = true;
      this.persist.getSurgeryRoom(this.idMode.id).subscribe(surgeryRoomDetail => {
        this.surgeryRoomDetail = surgeryRoomDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addSurgeryRoom(this.surgeryRoomDetail).subscribe(value => {
      this.tcNotification.success("surgeryRoom added");
      this.surgeryRoomDetail.id = value.id;
      this.dialogRef.close(this.surgeryRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateSurgeryRoom(this.surgeryRoomDetail).subscribe(value => {
      this.tcNotification.success("surgery_room updated");
      this.dialogRef.close(this.surgeryRoomDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  
  canSubmit():boolean{
        if (this.surgeryRoomDetail == null){
            return false;
          }

if (this.surgeryRoomDetail.name == null || this.surgeryRoomDetail.name  == "") {
            return false;
        } 
if (this.surgeryRoomDetail.status == null) {
            return false;
        }

        return true;
 }
 }