import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryRoomListComponent } from './surgery-room-list.component';

describe('SurgeryRoomListComponent', () => {
  let component: SurgeryRoomListComponent;
  let fixture: ComponentFixture<SurgeryRoomListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryRoomListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryRoomListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
