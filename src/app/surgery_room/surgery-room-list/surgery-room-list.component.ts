import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SurgeryRoomSummary, SurgeryRoomSummaryPartialList } from '../surgery_room.model';
import { SurgeryRoomPersist } from '../surgery_room.persist';
import { SurgeryRoomNavigator } from '../surgery_room.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { BedPersist } from 'src/app/bed/beds/bed.persist';
@Component({
  selector: 'app-surgery_room-list',
  templateUrl: './surgery-room-list.component.html',
  styleUrls: ['./surgery-room-list.component.scss']
})export class SurgeryRoomListComponent implements OnInit {
  surgeryRoomsData: SurgeryRoomSummary[] = [];
  surgeryRoomsTotalCount: number = 0;
  surgeryRoomSelectAll:boolean = false;
  surgeryRoomSelection: SurgeryRoomSummary[] = [];

 surgeryRoomsDisplayedColumns: string[] = ["select","action" ,"name","status" ];
  surgeryRoomSearchTextBox: FormControl = new FormControl();
  surgeryRoomIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) surgeryRoomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgeryRoomsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public surgeryRoomPersist: SurgeryRoomPersist,
                public surgeryRoomNavigator: SurgeryRoomNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public bedPersist: BedPersist,

    ) {

        this.tcAuthorization.requireRead("surgery_rooms");
       this.surgeryRoomSearchTextBox.setValue(surgeryRoomPersist.surgeryRoomSearchText);
      //delay subsequent keyup events
      this.surgeryRoomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.surgeryRoomPersist.surgeryRoomSearchText = value;
        this.searchSurgeryRooms();
      });
    } ngOnInit() {
   
      this.surgeryRoomsSort.sortChange.subscribe(() => {
        this.surgeryRoomsPaginator.pageIndex = 0;
        this.searchSurgeryRooms(true);
      });

      this.surgeryRoomsPaginator.page.subscribe(() => {
        this.searchSurgeryRooms(true);
      });
      //start by loading items
      this.searchSurgeryRooms();
    }

  searchSurgeryRooms(isPagination:boolean = false): void {


    let paginator = this.surgeryRoomsPaginator;
    let sorter = this.surgeryRoomsSort;

    this.surgeryRoomIsLoading = true;
    this.surgeryRoomSelection = [];

    this.surgeryRoomPersist.searchSurgeryRoom(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SurgeryRoomSummaryPartialList) => {
      this.surgeryRoomsData = partialList.data;
      if (partialList.total != -1) {
        this.surgeryRoomsTotalCount = partialList.total;
      }
      this.surgeryRoomIsLoading = false;
    }, error => {
      this.surgeryRoomIsLoading = false;
    });

  } downloadSurgeryRooms(): void {
    if(this.surgeryRoomSelectAll){
         this.surgeryRoomPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download surgery_room", true);
      });
    }
    else{
        this.surgeryRoomPersist.download(this.tcUtilsArray.idsList(this.surgeryRoomSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download surgery_room",true);
            });
        }
  }
addSurgeryRoom(): void {
    let dialogRef = this.surgeryRoomNavigator.addSurgeryRoom();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSurgeryRooms();
      }
    });
  }

  editSurgeryRoom(item: SurgeryRoomSummary) {
    let dialogRef = this.surgeryRoomNavigator.editSurgeryRoom(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSurgeryRoom(item: SurgeryRoomSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("surgery_room");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.surgeryRoomPersist.deleteSurgeryRoom(item.id).subscribe(response => {
          this.tcNotification.success("surgery_room deleted");
          this.searchSurgeryRooms();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}