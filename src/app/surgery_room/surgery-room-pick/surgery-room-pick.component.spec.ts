import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurgeryRoomPickComponent } from './surgery-room-pick.component';

describe('SurgeryRoomPickComponent', () => {
  let component: SurgeryRoomPickComponent;
  let fixture: ComponentFixture<SurgeryRoomPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurgeryRoomPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryRoomPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
