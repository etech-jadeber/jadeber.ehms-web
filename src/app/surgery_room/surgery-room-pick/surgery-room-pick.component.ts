import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { SurgeryRoomSummary, SurgeryRoomSummaryPartialList } from '../surgery_room.model';
import { SurgeryRoomPersist } from '../surgery_room.persist';
import { SurgeryRoomNavigator } from '../surgery_room.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCIdMode } from 'src/app/tc/models';
@Component({
  selector: 'app-surgery_room-pick',
  templateUrl: './surgery-room-pick.component.html',
  styleUrls: ['./surgery-room-pick.component.scss']
})export class SurgeryRoomPickComponent implements OnInit {
  surgeryRoomsData: SurgeryRoomSummary[] = [];
  surgeryRoomsTotalCount: number = 0;
  surgeryRoomSelectAll:boolean = false;
  surgeryRoomSelection: SurgeryRoomSummary[] = [];
  selectOne:boolean=true;
  surgeryRoomsDisplayedColumns: string[] = ["select" ,"name","status" ];
  surgeryRoomSearchTextBox: FormControl = new FormControl();
  surgeryRoomIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) surgeryRoomsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgeryRoomsSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public surgeryRoomPersist: SurgeryRoomPersist,
                public surgeryRoomNavigator: SurgeryRoomNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public dialogRef: MatDialogRef<SurgeryRoomPickComponent>,
                @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode

    ) {

        this.tcAuthorization.requireRead("surgery_rooms");
       this.surgeryRoomSearchTextBox.setValue(surgeryRoomPersist.surgeryRoomSearchText);
      //delay subsequent keyup events
      this.surgeryRoomSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.surgeryRoomPersist.surgeryRoomSearchText = value;
        this.searchSurgery_rooms();
      });
    } ngOnInit() {
   
      this.surgeryRoomsSort.sortChange.subscribe(() => {
        this.surgeryRoomsPaginator.pageIndex = 0;
        this.searchSurgery_rooms(true);
      });

      this.surgeryRoomsPaginator.page.subscribe(() => {
        this.searchSurgery_rooms(true);
      });
      //start by loading items
      this.searchSurgery_rooms();
    }

  searchSurgery_rooms(isPagination:boolean = false): void {


    let paginator = this.surgeryRoomsPaginator;
    let sorter = this.surgeryRoomsSort;

    this.surgeryRoomIsLoading = true;
    this.surgeryRoomSelection = [];

    this.surgeryRoomPersist.duration_time=parseInt(this.idMode.mode)*60;
    this.surgeryRoomPersist.surgery_date=new Date(this.idMode.id).getTime() / 1000;
    this.surgeryRoomPersist.searchSurgeryRoom(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: SurgeryRoomSummaryPartialList) => {
      this.surgeryRoomsData = partialList.data;
      if (partialList.total != -1) {
        this.surgeryRoomsTotalCount = partialList.total;
      }
      this.surgeryRoomIsLoading = false;
    }, error => {
      this.surgeryRoomIsLoading = false;
    });

  }
  markOneItem(item: SurgeryRoomSummary) {
    if(!this.tcUtilsArray.containsId(this.surgeryRoomSelection,item.id)){
          this.surgeryRoomSelection = [];
          this.surgeryRoomSelection.push(item);
        }
        else{
          this.surgeryRoomSelection = [];
        }
  }
  ngOnDestroy():void{
    this.surgeryRoomPersist.duration_time=0;
    this.surgeryRoomPersist.surgery_date=0;

  }

  returnSelected(): void {
    this.dialogRef.close(this.surgeryRoomSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }