import {TCId} from "../tc/models";export class SurgeryRoomSummary extends TCId {
    name:string;
    status:number;
     
    }
    export class SurgeryRoomSummaryPartialList {
      data: SurgeryRoomSummary[];
      total: number;
    }
    export class SurgeryRoomDetail extends SurgeryRoomSummary {
    }
    
    export class SurgeryRoomDashboard {
      total: number;
    }