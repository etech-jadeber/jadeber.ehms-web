import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {SurgeryRoomEditComponent} from "./surgery-room-edit/surgery-room-edit.component";
import {SurgeryRoomPickComponent} from "./surgery-room-pick/surgery-room-pick.component";


@Injectable({
  providedIn: 'root'
})

export class SurgeryRoomNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  surgeryRoomsUrl(): string {
    return "/surgery_rooms";
  }

  surgeryRoomUrl(id: string): string {
    return "/surgery_rooms/" + id;
  }

  viewSurgeryRooms(): void {
    this.router.navigateByUrl(this.surgeryRoomsUrl());
  }

  viewSurgeryRoom(id: string): void {
    this.router.navigateByUrl(this.surgeryRoomUrl(id));
  }

  editSurgeryRoom(id: string): MatDialogRef<SurgeryRoomEditComponent> {
    const dialogRef = this.dialog.open(SurgeryRoomEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSurgeryRoom(): MatDialogRef<SurgeryRoomEditComponent> {
    const dialogRef = this.dialog.open(SurgeryRoomEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSurgeryRooms(surgery_date:string,duration_time:string): MatDialogRef<SurgeryRoomPickComponent> {
      const dialogRef = this.dialog.open(SurgeryRoomPickComponent, {
        data: new TCIdMode(surgery_date, duration_time),
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}