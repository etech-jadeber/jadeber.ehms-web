import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {SurgeryRoomDashboard, SurgeryRoomDetail, SurgeryRoomSummaryPartialList} from "./surgery_room.model";
import { TCUtilsString } from '../tc/utils-string';


@Injectable({
  providedIn: 'root'
})
export class SurgeryRoomPersist {
    surgery_date:number;
    duration_time:number;
 surgeryRoomSearchText: string = "";constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.surgeryRoomSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchSurgeryRoom(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<SurgeryRoomSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("surgery_room", this.surgeryRoomSearchText, pageSize, pageIndex, sort, order);
    if(this.duration_time){
        url = TCUtilsString.appendUrlParameter(url, "duration_time", this.duration_time.toString());
    }
    if(this.surgery_date){
        url = TCUtilsString.appendUrlParameter(url,'surgery_date',this.surgery_date.toString());
    }

    return this.http.get<SurgeryRoomSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_room/do", new TCDoParam("download_all", this.filters()));
  }

  surgeryRoomDashboard(): Observable<SurgeryRoomDashboard> {
    return this.http.get<SurgeryRoomDashboard>(environment.tcApiBaseUri + "surgery_room/dashboard");
  }

  getSurgeryRoom(id: string): Observable<SurgeryRoomDetail> {
    return this.http.get<SurgeryRoomDetail>(environment.tcApiBaseUri + "surgery_room/" + id);
  } addSurgeryRoom(item: SurgeryRoomDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_room/", item);
  }

  updateSurgeryRoom(item: SurgeryRoomDetail): Observable<SurgeryRoomDetail> {
    return this.http.patch<SurgeryRoomDetail>(environment.tcApiBaseUri + "surgery_room/" + item.id, item);
  }

  deleteSurgeryRoom(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "surgery_room/" + id);
  }
 surgeryRoomsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_room/do", new TCDoParam(method, payload));
  }

  surgeryRoomDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_rooms/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_room/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_room/" + id + "/do", new TCDoParam("print", {}));
  }


}