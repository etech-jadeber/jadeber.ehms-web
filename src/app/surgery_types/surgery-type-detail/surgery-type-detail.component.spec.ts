import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryTypeDetailComponent } from './surgery-type-detail.component';

describe('SurgeryTypeDetailComponent', () => {
  let component: SurgeryTypeDetailComponent;
  let fixture: ComponentFixture<SurgeryTypeDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryTypeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
