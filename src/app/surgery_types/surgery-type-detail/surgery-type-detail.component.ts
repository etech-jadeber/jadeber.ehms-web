import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {Surgery_TypeDetail} from "../surgery_type.model";
import {Surgery_TypePersist} from "../surgery_type.persist";
import {Surgery_TypeNavigator} from "../surgery_type.navigator";

export enum Surgery_TypeTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-surgery-type-detail',
  templateUrl: './surgery-type-detail.component.html',
  styleUrls: ['./surgery-type-detail.component.css']
})
export class Surgery_TypeDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  surgery_typeLoading:boolean = false;
  
  Surgery_TypeTabs: typeof Surgery_TypeTabs = Surgery_TypeTabs;
  activeTab: Surgery_TypeTabs = Surgery_TypeTabs.overview;
  //basics
  surgery_typeDetail: Surgery_TypeDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public surgery_typeNavigator: Surgery_TypeNavigator,
              public  surgery_typePersist: Surgery_TypePersist) {
    this.tcAuthorization.requireRead("surgery_types");
    this.surgery_typeDetail = new Surgery_TypeDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("surgery_types");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.surgery_typeLoading = true;
    this.surgery_typePersist.getSurgery_Type(id).subscribe(surgery_typeDetail => {
          this.surgery_typeDetail = surgery_typeDetail;
          this.surgery_typeLoading = false;
        }, error => {
          console.error(error);
          this.surgery_typeLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.surgery_typeDetail.id);
  }

  editSurgery_Type(): void {
    let modalRef = this.surgery_typeNavigator.editSurgery_Type(this.surgery_typeDetail.id);
    modalRef.afterClosed().subscribe(modifiedSurgery_TypeDetail => {
      TCUtilsAngular.assign(this.surgery_typeDetail, modifiedSurgery_TypeDetail);
    }, error => {
      console.error(error);
    });
  }

   printSurgery_Type():void{
      this.surgery_typePersist.print(this.surgery_typeDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print surgery_type", true);
      });
    }

  back():void{
      this.location.back();
    }

}

