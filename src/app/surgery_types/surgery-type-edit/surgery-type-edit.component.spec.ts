import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryTypeEditComponent } from './surgery-type-edit.component';

describe('SurgeryTypeEditComponent', () => {
  let component: SurgeryTypeEditComponent;
  let fixture: ComponentFixture<SurgeryTypeEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryTypeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
