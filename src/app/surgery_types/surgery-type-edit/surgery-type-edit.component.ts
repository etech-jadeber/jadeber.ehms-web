import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {Surgery_TypeDetail} from "../surgery_type.model";
import {Surgery_TypePersist} from "../surgery_type.persist";


@Component({
  selector: 'app-surgery-type-edit',
  templateUrl: './surgery-type-edit.component.html',
  styleUrls: ['./surgery-type-edit.component.css']
})
export class Surgery_TypeEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  surgery_typeDetail: Surgery_TypeDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<Surgery_TypeEditComponent>,
              public  persist: Surgery_TypePersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("surgery_types");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient", "surgery_type");
      this.surgery_typeDetail = new Surgery_TypeDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("surgery_types");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient", "surgery_type");
      this.isLoadingResults = true;
      this.persist.getSurgery_Type(this.idMode.id).subscribe(surgery_typeDetail => {
        this.surgery_typeDetail = surgery_typeDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addSurgery_Type(this.surgery_typeDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "surgery_type") + " " + this.appTranslation.getText("general", "added"));
      this.surgery_typeDetail.id = value.id;
      this.dialogRef.close(this.surgery_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateSurgery_Type(this.surgery_typeDetail).subscribe(value => {
      this.tcNotification.success(this.appTranslation.getText("patient", "surgery_type") + " " +  this.appTranslation.getText("general", "updated"));
      this.dialogRef.close(this.surgery_typeDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.surgery_typeDetail == null){
            return false;
          }

        if (this.surgery_typeDetail.surgery_name == null || this.surgery_typeDetail.surgery_name  == "") {
                      return false;
                    }

        if (this.surgery_typeDetail.duration == 0 || this.surgery_typeDetail.duration == null){
          return false;
        }


        return true;
      }


}

