import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {Surgery_TypePersist} from "../surgery_type.persist";
import {Surgery_TypeNavigator} from "../surgery_type.navigator";
import {Surgery_TypeDetail, Surgery_TypeSummary, Surgery_TypeSummaryPartialList} from "../surgery_type.model";


@Component({
  selector: 'app-surgery-type-list',
  templateUrl: './surgery-type-list.component.html',
  styleUrls: ['./surgery-type-list.component.css']
})
export class Surgery_TypeListComponent implements OnInit {

  surgery_typesData: Surgery_TypeSummary[] = [];
  surgery_typesTotalCount: number = 0;
  surgery_typesSelectAll:boolean = false;
  surgery_typesSelection: Surgery_TypeSummary[] = [];

  surgery_typesDisplayedColumns: string[] = ["select","action","surgery_name","duration", ];
  surgery_typesSearchTextBox: FormControl = new FormControl();
  surgery_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) surgery_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgery_typesSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public surgery_typePersist: Surgery_TypePersist,
                public surgery_typeNavigator: Surgery_TypeNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("surgery_types");
       this.surgery_typesSearchTextBox.setValue(surgery_typePersist.surgery_typeSearchText);
      //delay subsequent keyup events
      this.surgery_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.surgery_typePersist.surgery_typeSearchText = value;
        this.searchSurgery_Types();
      });
    }

    ngOnInit() {

      this.surgery_typesSort.sortChange.subscribe(() => {
        this.surgery_typesPaginator.pageIndex = 0;
        this.searchSurgery_Types(true);
      });

      this.surgery_typesPaginator.page.subscribe(() => {
        this.searchSurgery_Types(true);
      });
      //start by loading items
      this.searchSurgery_Types();
    }

  searchSurgery_Types(isPagination:boolean = false): void {


    let paginator = this.surgery_typesPaginator;
    let sorter = this.surgery_typesSort;

    this.surgery_typesIsLoading = true;
    this.surgery_typesSelection = [];

    this.surgery_typePersist.searchSurgery_Type(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: Surgery_TypeSummaryPartialList) => {
      this.surgery_typesData = partialList.data;
      if (partialList.total != -1) {
        this.surgery_typesTotalCount = partialList.total;
      }
      this.surgery_typesIsLoading = false;
    }, error => {
      this.surgery_typesIsLoading = false;
    });

  }

  downloadSurgery_Types(): void {
    if(this.surgery_typesSelectAll){
         this.surgery_typePersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download surgery_types", true);
      });
    }
    else{
        this.surgery_typePersist.download(this.tcUtilsArray.idsList(this.surgery_typesSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download surgery_types",true);
            });
        }
  }



  addSurgery_Type(): void {
    let dialogRef = this.surgery_typeNavigator.addSurgery_Type();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSurgery_Types();
      }
    });
  }

  editSurgery_Type(item: Surgery_TypeSummary) {
    let dialogRef = this.surgery_typeNavigator.editSurgery_Type(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSurgery_Type(item: Surgery_TypeSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Surgery_Type");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.surgery_typePersist.deleteSurgery_Type(item.id).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("patient", "surgery_type") + " " + this.appTranslation.getText("general", "deleted"));
          this.searchSurgery_Types();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
