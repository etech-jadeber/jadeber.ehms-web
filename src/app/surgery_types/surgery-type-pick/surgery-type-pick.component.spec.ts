import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurgeryTypePickComponent } from './surgery-type-pick.component';

describe('SurgeryTypePickComponent', () => {
  let component: SurgeryTypePickComponent;
  let fixture: ComponentFixture<SurgeryTypePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurgeryTypePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurgeryTypePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
