import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {Surgery_TypeDetail, Surgery_TypeSummary, Surgery_TypeSummaryPartialList} from "../surgery_type.model";
import {Surgery_TypePersist} from "../surgery_type.persist";


@Component({
  selector: 'app-surgery-type-pick',
  templateUrl: './surgery-type-pick.component.html',
  styleUrls: ['./surgery-type-pick.component.css']
})
export class Surgery_TypePickComponent implements OnInit {

  surgery_typesData: Surgery_TypeSummary[] = [];
  surgery_typesTotalCount: number = 0;
  surgery_typesSelection: Surgery_TypeSummary[] = [];
  surgery_typesDisplayedColumns: string[] = ["select", 'surgery_name','duration' ];

  surgery_typesSearchTextBox: FormControl = new FormControl();
  surgery_typesIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) surgery_typesPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) surgery_typesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public surgery_typePersist: Surgery_TypePersist,
              public dialogRef: MatDialogRef<Surgery_TypePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("surgery_types");
    this.surgery_typesSearchTextBox.setValue(surgery_typePersist.surgery_typeSearchText);
    //delay subsequent keyup events
    this.surgery_typesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.surgery_typePersist.surgery_typeSearchText = value;
      this.searchSurgery_Types();
    });
  }

  ngOnInit() {

    this.surgery_typesSort.sortChange.subscribe(() => {
      this.surgery_typesPaginator.pageIndex = 0;
      this.searchSurgery_Types();
    });

    this.surgery_typesPaginator.page.subscribe(() => {
      this.searchSurgery_Types();
    });

    //set initial picker list to 5
    this.surgery_typesPaginator.pageSize = 5;

    //start by loading items
    this.searchSurgery_Types();
  }

  searchSurgery_Types(): void {
    this.surgery_typesIsLoading = true;
    this.surgery_typesSelection = [];

    this.surgery_typePersist.searchSurgery_Type(this.surgery_typesPaginator.pageSize,
        this.surgery_typesPaginator.pageIndex,
        this.surgery_typesSort.active,
        this.surgery_typesSort.direction).subscribe((partialList: Surgery_TypeSummaryPartialList) => {
      this.surgery_typesData = partialList.data;
      if (partialList.total != -1) {
        this.surgery_typesTotalCount = partialList.total;
      }
      this.surgery_typesIsLoading = false;
    }, error => {
      this.surgery_typesIsLoading = false;
    });

  }

  markOneItem(item: Surgery_TypeSummary) {
    if(!this.tcUtilsArray.containsId(this.surgery_typesSelection,item.id)){
          this.surgery_typesSelection = [];
          this.surgery_typesSelection.push(item);
        }
        else{
          this.surgery_typesSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.surgery_typesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
