import {TCId} from "../tc/models";

export class Surgery_TypeSummary extends TCId {
  surgery_name : string;
duration : number;
}

export class Surgery_TypeSummaryPartialList {
  data: Surgery_TypeSummary[];
  total: number;
}

export class Surgery_TypeDetail extends Surgery_TypeSummary {
  surgery_name : string;
duration : number;
}

export class Surgery_TypeDashboard {
  total: number;
}