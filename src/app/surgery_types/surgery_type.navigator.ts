import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {Surgery_TypeEditComponent} from "./surgery-type-edit/surgery-type-edit.component";
import {Surgery_TypePickComponent} from "./surgery-type-pick/surgery-type-pick.component";


@Injectable({
  providedIn: 'root'
})

export class Surgery_TypeNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  surgery_typesUrl(): string {
    return "/surgery_types";
  }

  surgery_typeUrl(id: string): string {
    return "/surgery_types/" + id;
  }

  viewSurgery_Types(): void {
    this.router.navigateByUrl(this.surgery_typesUrl());
  }

  viewSurgery_Type(id: string): void {
    this.router.navigateByUrl(this.surgery_typeUrl(id));
  }

  editSurgery_Type(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Surgery_TypeEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSurgery_Type(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(Surgery_TypeEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickSurgery_Types(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(Surgery_TypePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
