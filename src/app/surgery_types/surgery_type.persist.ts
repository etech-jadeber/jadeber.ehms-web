import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {Surgery_TypeDashboard, Surgery_TypeDetail, Surgery_TypeSummaryPartialList} from "./surgery_type.model";


@Injectable({
  providedIn: 'root'
})
export class Surgery_TypePersist {

  surgery_typeSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchSurgery_Type(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<Surgery_TypeSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("surgery_types", this.surgery_typeSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<Surgery_TypeSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.surgery_typeSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_types/do", new TCDoParam("download_all", this.filters()));
  }

  surgery_typeDashboard(): Observable<Surgery_TypeDashboard> {
    return this.http.get<Surgery_TypeDashboard>(environment.tcApiBaseUri + "surgery_types/dashboard");
  }

  getSurgery_Type(id: string): Observable<Surgery_TypeDetail> {
    return this.http.get<Surgery_TypeDetail>(environment.tcApiBaseUri + "surgery_types/" + id);
  }

  addSurgery_Type(item: Surgery_TypeDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_types/", item);
  }

  updateSurgery_Type(item: Surgery_TypeDetail): Observable<Surgery_TypeDetail> {
    return this.http.patch<Surgery_TypeDetail>(environment.tcApiBaseUri + "surgery_types/" + item.id, item);
  }

  deleteSurgery_Type(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "surgery_types/" + id);
  }

  surgery_typesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_types/do", new TCDoParam(method, payload));
  }

  surgery_typeDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "surgery_types/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_types/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "surgery_types/" + id + "/do", new TCDoParam("print", {}));
  }


}
