import { Component, Input, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
import { Subscription } from 'rxjs';
import { SurgicalSafetyCheckPersist } from './surgical-sefety-checklist';

@Component({
  selector: 'app-surgical-safety-checklist',
  templateUrl: './surgical-safety-checklist.component.html',
  styleUrls: ['./surgical-safety-checklist.component.scss']
})
export class SurgicalSafetyChecklistComponent implements OnInit {

   id:string;
   paramsSubscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public persist: SurgicalSafetyCheckPersist,
    ) {

   }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      this.id = this.route.snapshot.paramMap.get('id');
      
    });
  }

  onTabChanged(matTab: MatTabChangeEvent){
    this.persist.selected_index=matTab.index;
  }

 
  back():void{
    this.location.back();
  }
}  

