import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root',
})
export class SurgicalSafetyCheckPersist{
    selected_index: number;



    constructor(
        public router: Router,
    ){

    }

    surgicalSafetyChecklistUrl(id: string): string {
        return '/surgical_safety_checklist/' + id;
      }
    
    
      viewSurgicalSafetyChecklist(id: string): void {
        this.router.navigateByUrl(this.surgicalSafetyChecklistUrl(id));
      }
}