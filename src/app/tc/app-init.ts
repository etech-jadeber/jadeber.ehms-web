import {Injectable} from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { TCUserInfo } from "./authentication";

@Injectable({
  providedIn: 'root',
})

export class TCAppInit {
  public static appInitialized$: BehaviorSubject< boolean> =new BehaviorSubject(false);
  public static token: string = null;
  public static userInfo: TCUserInfo;
  public static isLoggedIn: boolean = false;
}
