//enums

import { TCMandatoryTypes } from "./models";


export class AppEventTypes {}

export class AppMandatoryTypes extends TCMandatoryTypes {}

//relation types
export class Relation_Types {
  static mother: number = 1;
  static father: number = 2;
  static child: number = 3;
  static sister: number = 4;
  static brother: number = 5;
  static spouse: number = 6;
  static other: number = 7;
}

//marital status
export class Marital_status {
  static Married: number = 1;
  static Single: number = 2;
  static Widowed: number = 3;
  static Divorced: number = 4;
}

//Educational level

export class Educational_level {
  static primery_school: number = 1;
  static secondary_school: number = 2;
  static certificate: number = 3;
  static diplmoa: number = 4;
  static first_degree: number = 5;
  static masters: number = 6;
  static phd: number = 7;
  static basic_reading_writing: number = 8;
}

export class Event_Type {
  static app_state_event = 1;
  static identity_management = 11;
  static user_management = 12;
  static user_login_success = 13;
  static user_login_failed = 14;
  static user_password_changed = 15;
  static case_management = 21;
  static case_state_change = 22;
  static data_add_event = 31;
  static data_update_event = 32;
  static data_delete_event = 33;
  static job_event = 41;
  static file_event = 51;
  static file_download_event = 52;
}

export class UserContexts {
  static patient: number = 101;
static employee: number = 102;
static doctor: number = 103;
static front_desk: number = 100;
static nurse: number = 104;
static lab: number = 105;
}


//Job States
export class Job_States {
  static queued = 1;
  static running = 2;
  static success = 3;
  static failure = 4;
  static aborted = 5;
}

//Job Types
export class Job_Types {
  static test = -1;
  static tc_download = 1;
  static tc_print = 2;
}

export class Setting_Type {
  static user_pwd_reset = 1;
  static user_language = 2;
  static user_activation = 3;
  static user_otp = 4;
  static user_otp_csrf = 5;
  static telegram_login = 6;
}

export class LoanStatus {
  static submitted: number = 1;
  static rejected: number = 3;
  static approved: number = 2;
}

export class AppCaseTypes {
  static patient_general: number = 101;
  static patient_appointment: number = 102;
  static item_request: number = 103;
  static item_dispatch: number = 104;

}

export class AccountType {
  static assets: number = 100;
  static liabilities: number = 101;
  static equity: number = 102;
  static revenue: number = 103;
  static expenses: number = 104;
}


export class savingType {
  static Mandatoryd : number = 100;
  static voluntary: number = 101;
}
