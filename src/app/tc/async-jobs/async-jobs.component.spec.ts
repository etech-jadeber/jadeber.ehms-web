import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AsyncJobsComponent } from './async-jobs.component';

describe('AsyncJobsComponent', () => {
  let component: AsyncJobsComponent;
  let fixture: ComponentFixture<AsyncJobsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AsyncJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsyncJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
