import {Component, OnInit} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import {interval} from "rxjs";
import {JobData} from "../jobs/job.model";
import {JobPersist} from "../jobs/job.persist";

@Component({
  selector: 'app-async-jobs',
  templateUrl: './async-jobs.component.html',
  styleUrls: ['./async-jobs.component.css']
})
export class AsyncJobsComponent implements OnInit {

  constructor(public snackBar: MatSnackBar, public jobData: JobData) {
  }

  ngOnInit() {
    const jobCheckTicks = interval(1000); //check every second
    jobCheckTicks.subscribe(n => {
      if (this.jobData.unfinishedJobs.length == 0) {
        this.snackBar.dismiss();
      }
    });
  }

  onOk(): void {
    this.snackBar.dismiss();
  }


}
