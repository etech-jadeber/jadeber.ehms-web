import {Injectable} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';


import {AsyncJobsComponent} from "./async-jobs/async-jobs.component";

@Injectable({
  providedIn: 'root',
})

export class TCAsyncJob {

  constructor(public snackBar: MatSnackBar) {

  }


  show() {
    this.snackBar.openFromComponent(AsyncJobsComponent, {
      panelClass: "tc-panel-info",
      verticalPosition: "bottom",
      horizontalPosition: "right"
    });
  }

}


