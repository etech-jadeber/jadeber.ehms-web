import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {TCId} from "./models";
import {TCAppInit} from "./app-init";
import { TCNavigator } from './navigator';

//models - start
export class TCAcl {
  id: string;
  uri: string;
  user_id: string;
  group_id: string;
  can_create: boolean;
  can_read: boolean;
  can_update: boolean;
  can_delete: boolean;
}

export class Tokens{
  access_token:string;
  refresh_token:string;
}

export class GroupContext extends TCId {
  group_id: string;
  context_type: number;
  target_id: string;
}

export class UserContext extends TCId {
  user_id: string;
  context_type: number;
  target_id: string;
}

export class TCMandatory extends TCId {

  user_id: string;
  mandatory_type: number;
  target_id: string;
  is_handled: boolean;
  reg_time: number;
  handle_time: number;

}

export class IdentitySources {
  static tc_local: number = 1;
  static tc_remote: number = 2;
  static telegram: number = 3;
}

export class TCToken {
  token: string;
}

export class TCUserInfo extends TCToken {
  user_id: string;
  name: string;
  username: string;
  email: string;
  lang: string;
  is_sysadmin: boolean;
  acl: TCAcl[];
  group_contexts: GroupContext[];
  user_contexts: UserContext[];
  mandatorys: TCMandatory[];
  identity_source: number;
}

//models - end

@Injectable({
  providedIn: 'root',
})

export class TCAuthentication {

  


  constructor(private http: HttpClient, private tcNavigator:TCNavigator) {

    if (localStorage.getItem(TCAuthentication.getAuthorizationDataKey()) != null) {
      try {
        let cachedToken: TCToken = JSON.parse(localStorage.getItem(TCAuthentication.getAuthorizationDataKey()));
        
      


        //fetch detail info from sever
        // using this
        this.login(cachedToken.token, () => {
          
        }, false);

      } catch {
        TCAppInit. appInitialized$.next(true);
        //this.logout();
      }
    }

    
    else {
      TCAppInit.userInfo = new TCUserInfo();
      TCAppInit.isLoggedIn = false;
      TCAppInit. appInitialized$.next(true);
    }
  }

  private static getAuthorizationDataKey(): string {
    return environment.tcApiBaseUri + ".authorizationData";
  }

  reloadUserInfo(afterReload: Function) {
    
    this.login(TCAppInit.userInfo.token, afterReload, false);
  }

  login(token: string, afterInfoFetch: Function, isFirstTime: boolean): void {    
    //call user info to getApp tcaclDetail and details
    let url = environment.tcApiBaseUri + "user-info";


    this.http.get<TCUserInfo>(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    }).subscribe(userInfo => {

      TCAppInit.userInfo = userInfo;
      console.log(userInfo);
      
      TCAppInit.userInfo.token = token;
      TCAppInit.isLoggedIn = true;
      if (isFirstTime) {
        this.persistToken(token);
      }

      TCAppInit. appInitialized$.next(true);
      TCAppInit.token = token;
      //call back
      afterInfoFetch();
    }, error => {
      //this.logout();
    });

    
  }

  getRefreshToken():string{
    return localStorage.getItem("refresh_token");
  }

  refreshToken():void{
    
  }

  persisitRefresh(token:string){
    localStorage.setItem("refresh_token", token);
  }
  updateUserInfo(updatedUserInfo: TCUserInfo): void {
    console.error("update user info - needs update, don't forget");
    //this.login(this.userInfo.token, updatedUserInfo);
  }

  persistToken(token: string): void {
    TCAppInit.token = token;
    //update local store
    localStorage.setItem(TCAuthentication.getAuthorizationDataKey(), JSON.stringify({"token": token}));
  }

  logout(): void {
  
    
    // // TCAppInit.userInfo = new TCUserInfo();
    // const url = environment.tcApiBaseUri + "logout";
    // console.log("why")
    // this.http.post(url, {}).subscribe((value) => {
    //   localStorage.removeItem(environment.tcApiBaseUri + ".authorizationData");
    //   localStorage.removeItem("refresh_token");
      
    //   // TCAppInit.token = null;
    //   this.tcNavigator.login();
    //   // TCAppInit.isLoggedIn = false;
    //   // TCAppInit.appInitialized$.next(true);

    TCAppInit.userInfo = new TCUserInfo();
    localStorage.removeItem(TCAuthentication.getAuthorizationDataKey());
    localStorage.removeItem("refresh_token");
    TCAppInit.isLoggedIn = false;
    TCAppInit.token = null;
    this.tcNavigator.login();
  }

}

