import {Injectable} from '@angular/core';
import {TCNavigator} from "./navigator";
import {GroupContext, TCAcl, TCAuthentication, TCMandatory, UserContext} from "./authentication";
import {TCUtilsArray} from "./utils-array";
import {TCMandatoryTypes} from "./models";
import { TCAppInit } from './app-init';
import { MatDialog } from '@angular/material/dialog';
import { TCNotification } from './notification';


@Injectable({
  providedIn: 'root',
})

export class TCAuthorization {
  constructor(private tcAuthentication: TCAuthentication, private tcUtilsArray: TCUtilsArray, private tcNavigator: TCNavigator, private dialogRef: MatDialog, private tcNotification: TCNotification) {
  }

  isLoggedIn(): boolean {
    return TCAppInit.isLoggedIn;
  }

  requireLogin(): void {
    if (!TCAppInit.isLoggedIn) {
      //this.tcNavigator.accessDenied();
    }
  }

  isSysAdmin(): boolean {
    return TCAppInit.isLoggedIn ? TCAppInit.userInfo.is_sysadmin : false;
  }

  requireSysAdmin(): void {
    if (!this.isSysAdmin()) {
      // this.tcNavigator.accessDenied();
    }
  }

  hasMandatorys(): boolean {
    if (!this.isLoggedIn()) {
      return false;
    }
    return this.getMandatorys().length > 0;
  }

  tosAccepted(): boolean {
    if (!this.isLoggedIn()) {
      return false;
    }

    return !this.hasMandatory(TCMandatoryTypes.tos_firsttime) && !this.hasMandatory(TCMandatoryTypes.tos_updated);
  }

  hasMandatory(mandatoryType: number): boolean {

    return this.getMandatorysByType(mandatoryType).length > 0;
  }

  getMandatorysByType(mandatoryType: number): TCMandatory[] {
    let mandatorys = this.tcUtilsArray.getByProperty(this.getMandatorys(), (man) => {
      return man.mandatory_type == mandatoryType;
    });
    return mandatorys;
  }

  getMandatorys(): TCMandatory[] {
    if (!this.isLoggedIn()) {
      return [];
    }
    if (TCAppInit.userInfo.mandatorys == undefined) {
      this.tcAuthentication.logout();
      return [];
    }
    return TCAppInit.userInfo.mandatorys;

  }

  canCreate(uri: string): boolean {

    if (this.hasMandatorys()) {
      return false;
    }

    if (this.isSysAdmin()) {
      return true;
    }
    return this.tcUtilsArray.getByProperty(TCAppInit.userInfo.acl, (tcAcl: TCAcl) => {
      return tcAcl.uri == uri && tcAcl.can_create;
    }).length > 0;
  }

  requireCreate(uri: string): boolean {
    if (!this.canCreate(uri)) {
      //this.tcNavigator.accessDenied();
      this.dialogRef.closeAll();
      this.tcNotification.error("You have not access to create")
      return false;
    }
    return true;
  }

  canRead(uri: string): boolean {
    if (this.hasMandatorys()) {
      return false;
    }

    if (this.isSysAdmin()) {
      return true;
    }
    return this.tcUtilsArray.getByProperty(TCAppInit.userInfo.acl, (tcAcl: TCAcl) => {
      return tcAcl.uri == uri && tcAcl.can_read;
    }).length > 0;
  }

  getResourceIds(resourceType: string) {
    let ids: string[] = [];
    let acls = this.tcUtilsArray.getByProperty(TCAppInit.userInfo.acl, (tcAcl: TCAcl) => {
      return tcAcl.uri.startsWith(resourceType) && tcAcl.can_read;
    });

    for (var acl of acls) {
      let stripped = acl.uri.replace(resourceType + "/", "");
      if (!stripped.includes("/")) {
        ids.push(stripped);
      }
    }
    return ids;

  }

  requireRead(uri: string): boolean {
    if (!this.canRead(uri)) {
      //this.tcNavigator.accessDenied();
      this.dialogRef.closeAll();
      this.tcNotification.error("You have not access to read")
      return false;
    }
    return true;
  }

  canUpdate(uri: string): boolean {
    if (this.hasMandatorys()) {
      return false;
    }

    if (this.isSysAdmin()) {
      return true;
    }
    return this.tcUtilsArray.getByProperty(TCAppInit.userInfo.acl, (tcAcl: TCAcl) => {
      return tcAcl.uri == uri && tcAcl.can_update;
    }).length > 0;
  }

  requireUpdate(uri: string): boolean {
    if (!this.canUpdate(uri)) {
      // this.tcNavigator.accessDenied();
      this.dialogRef.closeAll();
      this.tcNotification.error("You have not access to edit")
      return false
    }
    return true
  }

  canDelete(uri: string): boolean {
    if (this.hasMandatorys()) {
      return false;
    }

    if (this.isSysAdmin()) {
      return true;
    }
    return this.tcUtilsArray.getByProperty(TCAppInit.userInfo.acl, (tcAcl: TCAcl) => {
      return tcAcl.uri == uri && tcAcl.can_delete;
    }).length > 0;
  }

  requireDelete(uri: string): boolean {
    if (!this.canDelete(uri)) {
      //this.tcNavigator.accessDenied();
      this.dialogRef.closeAll();
      this.tcNotification.error("You have not access to delete")
      return false;
    }
    return true;
  }

  getGroupContexts(contextType: number): string[] {
    let targetIds: string[] = [];

    let items: GroupContext[] = this.tcUtilsArray.getByProperty(TCAppInit.userInfo.group_contexts, (group_context: GroupContext) => {
      return group_context.context_type == contextType;
    });

    items.forEach(item => {
      targetIds.push(item.target_id);
    });

    return targetIds;

  }

  getUserContexts(contextType: number): string[] {
    let targetIds: string[] = [];

    let items: GroupContext[] = this.tcUtilsArray.getByProperty(TCAppInit.userInfo.user_contexts, (user_context: UserContext) => {
      return user_context.context_type == contextType;
    });

    items.forEach(item => {
      targetIds.push(item.target_id);
    });

    return targetIds;

  }

}
