import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {CaseDetail} from "../case.model";
import {CasePersist} from "../case.persist";
import {CaseNavigator} from "../case.navigator";


export enum CaseTabs {
  overview,
}

@Component({
  selector: 'app-case-detail',
  templateUrl: './case-detail.component.html',
  styleUrls: ['./case-detail.component.css']
})
export class CaseDetailComponent implements OnInit {

  //basics
  caseDetail: CaseDetail;
  caseTabs: typeof CaseTabs = CaseTabs;
  activeTab: CaseTabs = CaseTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public caseNavigator: CaseNavigator,
              public  persist: CasePersist) {
    this.tcAuthorization.requireRead("cases");
    this.caseDetail = new CaseDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getCase(id.toString()).subscribe(caseDetail => {
      this.caseDetail = caseDetail;
    }, error => {
    });
  }

  editCase(): void {
    let modalRef = this.caseNavigator.editCase(this.caseDetail.id);
    modalRef.afterClosed().subscribe(modifiedCaseDetail => {
      TCUtilsAngular.assign(this.caseDetail, modifiedCaseDetail);
    }, error => {
      console.error(error);
    });
  }


}
