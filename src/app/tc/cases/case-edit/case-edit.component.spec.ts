import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CaseEditComponent } from './case-edit.component';

describe('CaseEditComponent', () => {
  let component: CaseEditComponent;
  let fixture: ComponentFixture<CaseEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CaseEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
