import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {CasePersist} from "../case.persist";
import {CaseNavigator} from "../case.navigator";
import {CaseDetail, CaseSummary, CaseSummaryPartialList} from "../case.model";

export enum CaseListTabs {
  overview
}

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.css']
})
export class CaseListComponent implements OnInit {
  activeTab: CaseListTabs = CaseListTabs.overview;
  tabs: typeof CaseListTabs = CaseListTabs;

  casesData: CaseSummary[] = [];
  casesTotalCount: number = 0;
  casesSelection: CaseSummary[] = [];
  casesDisplayedColumns: string[] = ["select", "action","link", "case_type_id","is_closed","case_state_id","target_id","creation_time","last_update"];

  casesSearchTextBox: FormControl = new FormControl();
  casesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) casesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) casesSort: MatSort;

  constructor(private router: Router,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public casePersist: CasePersist,
                public caseNavigator: CaseNavigator
    ) {

        this.tcAuthorization.requireRead("cases");
      //delay subsequent keyup events
      this.casesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.casePersist.caseSearchText = value;
        this.searchCases();
      });
    }

    ngOnInit() {

      this.casesSort.sortChange.subscribe(() => {
        this.casesPaginator.pageIndex = 0;
        this.searchCases();
      });

      this.casesPaginator.page.subscribe(() => {
        this.searchCases();
      });
      //start by loading items
      this.searchCases();
    }

  searchCases(): void {
    this.casesIsLoading = true;
    this.casesSelection = [];

    this.casePersist.searchCase(this.casesPaginator.pageSize, this.casesPaginator.pageIndex, this.casesSort.active, this.casesSort.direction).subscribe((partialList: CaseSummaryPartialList) => {
      this.casesData = partialList.data;
      if (partialList.total != -1) {
        this.casesTotalCount = partialList.total;
      }
      this.casesIsLoading = false;
    }, error => {
      this.casesIsLoading = false;
    });

  }

  downloadCases(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addCase(): void {
    let dialogRef = this.caseNavigator.addCase();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchCases();
      }
    });
  }

  editCase(item: CaseSummary) {
    let dialogRef = this.caseNavigator.editCase(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteCase(item: CaseSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Case");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.casePersist.deleteCase(item.id).subscribe(response => {
          this.tcNotification.success("Case deleted");
          this.searchCases();
        }, error => {
        });
      }

    });
  }

}
