import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CasePickComponent } from './case-pick.component';

describe('CasePickComponent', () => {
  let component: CasePickComponent;
  let fixture: ComponentFixture<CasePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CasePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CasePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
