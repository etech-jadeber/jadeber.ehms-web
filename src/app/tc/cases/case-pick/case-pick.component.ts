import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {CaseDetail, CaseSummary, CaseSummaryPartialList} from "../case.model";
import {CasePersist} from "../case.persist";


@Component({
  selector: 'app-case-pick',
  templateUrl: './case-pick.component.html',
  styleUrls: ['./case-pick.component.css']
})
export class CasePickComponent implements OnInit {

  casesData: CaseSummary[] = [];
  casesTotalCount: number = 0;
  casesSelection: CaseSummary[] = [];
  casesDisplayedColumns: string[] = ["select", 'case_type_id','is_closed','case_state_id','target_id','creation_time','last_update' ];

  casesSearchTextBox: FormControl = new FormControl();
  casesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) casesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) casesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public casePersist: CasePersist,
              public dialogRef: MatDialogRef<CasePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("cases");
    //delay subsequent keyup events
    this.casesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.casePersist.caseSearchText = value;
      this.searchCases();
    });
  }

  ngOnInit() {

    this.casesSort.sortChange.subscribe(() => {
      this.casesPaginator.pageIndex = 0;
      this.searchCases();
    });

    this.casesPaginator.page.subscribe(() => {
      this.searchCases();
    });
    //start by loading items
    this.searchCases();
  }

  searchCases(): void {
    this.casesIsLoading = true;
    this.casesSelection = [];

    this.casePersist.searchCase(this.casesPaginator.pageSize,
        this.casesPaginator.pageIndex,
        this.casesSort.active,
        this.casesSort.direction).subscribe((partialList: CaseSummaryPartialList) => {
      this.casesData = partialList.data;
      if (partialList.total != -1) {
        this.casesTotalCount = partialList.total;
      }
      this.casesIsLoading = false;
    }, error => {
      this.casesIsLoading = false;
    });

  }

  markOneItem(item: CaseSummary) {
    this.casesSelection = [];
    this.casesSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.casesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
