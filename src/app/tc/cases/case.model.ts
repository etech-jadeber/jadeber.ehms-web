import {CommentDetail, TCId} from "../../tc/models";

export class CaseStates {
  static accepted: number = 0;
  static rejected: number = 1;
  static unknown: number = -1;
  static accepted_partially: number = 2;
  static awaiting_response: number = 3;
  static response_provided: number = 4;
}

export class CaseSummary extends TCId {
  case_type_id: number;
  is_closed: boolean;
  case_state_id: number;
  target_id: string;
  creation_time: number;
  last_update: number;
}

export class CaseSummaryPartialList {
  data: CaseSummary[];
  total: number;
}

export class CaseDetail extends CaseSummary {
  payload: object;
  comments:CommentDetail[];
}

export class CaseDashboard {
  total: number;
  open: number;
}

export class ModificationRequestFld extends TCId {
  fld_name: string;
  current_value: Object;
  requested_update: Object;

  constructor(fld_name: string, current_value: Object, requested_update: Object) {
    super();
    this.id = fld_name;
    this.fld_name = fld_name;
    this.current_value = current_value;
    this.requested_update = requested_update;
  }

  static acceptedChanges(correctionsSelected: ModificationRequestFld[]): Object {
    let accepatedChanges = {};
    correctionsSelected.forEach(fld => {
      accepatedChanges[fld.fld_name] = fld.requested_update;
    });
    return accepatedChanges;
  }
}

