import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {CaseEditComponent} from "./case-edit/case-edit.component";
import {CasePickComponent} from "./case-pick/case-pick.component";


@Injectable({
  providedIn: 'root'
})

export class CaseNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  casesUrl(): string {
    return "/cases";
  }

  caseUrl(id: string): string {
    return "/cases/" + id;
  }

  viewCases(): void {
    this.router.navigateByUrl(this.casesUrl());
  }

  viewCase(id: string): void {
    this.router.navigateByUrl(this.caseUrl(id));
  }

  editCase(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(CaseEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addCase(): MatDialogRef<unknown,any> {
    return this.editCase(null);
  }
  
   pickCases(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(CasePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
