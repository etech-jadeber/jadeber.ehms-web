import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {CaseDashboard, CaseDetail, CaseSummaryPartialList} from "./case.model";


@Injectable({
  providedIn: 'root'
})
export class CasePersist {

  caseSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchCase(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<CaseSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/cases", this.caseSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<CaseSummaryPartialList>(url);

  }

  caseDashboard(): Observable<CaseDashboard> {
    return this.http.get<CaseDashboard>(environment.tcApiBaseUri + "tc/cases/dashboard");
  }

  getCase(id: string): Observable<CaseDetail> {
    return this.http.get<CaseDetail>(environment.tcApiBaseUri + "tc/cases/" + id);
  }

  addCase(item: CaseDetail): Observable<TCId> {
    return this.http.post<CaseDetail>(environment.tcApiBaseUri + "tc/cases/", item);
  }

  updateCase(item: CaseDetail): Observable<CaseDetail> {
    return this.http.patch<CaseDetail>(environment.tcApiBaseUri + "tc/cases/" + item.id, item);
  }

  deleteCase(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/cases/" + id);
  }

  casesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/cases/do", new TCDoParam(method, payload));
  }

  caseDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/cases/" + id + "/do", new TCDoParam(method, payload));
  }


}
