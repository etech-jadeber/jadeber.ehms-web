import {Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export class ConfirmParameter {
  action: string;
  itemType: string;
  description: string;
  cancelLabel: string;
  actionReturn: any = true;
  cancelReturn: any;
}

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {

  title: string = "Confirm Action";

  constructor(public dialogRef: MatDialogRef<ConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmParameter) {
  }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onCancel(): void {
    this.dialogRef.close(this.data.cancelReturn);
  }

  onOk(): void {
    this.dialogRef.close(this.data.actionReturn);
  }

}
