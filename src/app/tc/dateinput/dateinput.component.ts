import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TCUtilsDate} from "../utils-date";
import {FormGroup, FormControl} from '@angular/forms';

export class DateinputParameter {
  currentValue: number;
  prompt: string;
  cancelLabel: string;
  action: string;
  is_range: boolean;
}

@Component({
  selector: 'app-dateinput',
  templateUrl: './dateinput.component.html',
  styleUrls: ['./dateinput.component.css']
})
export class DateinputComponent implements OnInit {

  dt: Date = new Date();
  range = new FormGroup({
    start: new FormControl(),
    end: new FormControl(),
  });

  constructor(public dialogRef: MatDialogRef<DateinputComponent>,
              public tcUtilsDate: TCUtilsDate,
              @Inject(MAT_DIALOG_DATA) public data: DateinputParameter) {
  }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onOk(): void {
    if(this.data.is_range){
      const obj = {start: this.tcUtilsDate.toTimeStamp(this.range.value.start._d),
      end: this.tcUtilsDate.toTimeStamp(this.range.value.end._d) }
      this.dialogRef.close(obj)
    } else {
      this.dialogRef.close(this.tcUtilsDate.toTimeStamp(this.dt));

    }
  }
}
