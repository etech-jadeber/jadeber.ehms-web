import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ethiopianBirrFormat'
})
export class EthiopianBirrFormatPipe implements PipeTransform {
  transform(value: number): string {
    // Convert the number to Ethiopian Birr format
    const formattedValue = value && parseFloat(value.toFixed(2)).toLocaleString('en-ET');

    // Return the formatted value
    return formattedValue + ' ETB';
  }
}