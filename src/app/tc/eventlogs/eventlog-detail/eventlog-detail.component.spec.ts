import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EventlogDetailComponent } from './eventlog-detail.component';

describe('EventlogDetailComponent', () => {
  let component: EventlogDetailComponent;
  let fixture: ComponentFixture<EventlogDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EventlogDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventlogDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
