import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";

import {EventlogDetail} from "../eventlog.model";
import {EventlogPersist} from "../eventlog.persist";
import {EventlogNavigator} from "../eventlog.navigator";
import {TCNavigator} from "../../navigator";
import {AppTranslation} from "../../../app.translation";


export enum EventlogTabs {
  overview,
}

@Component({
  selector: 'app-eventlog-detail',
  templateUrl: './eventlog-detail.component.html',
  styleUrls: ['./eventlog-detail.component.css']
})
export class EventlogDetailComponent implements OnInit {

  //basics
  eventlogDetail: EventlogDetail;
  eventlogTabs: typeof EventlogTabs = EventlogTabs;
  activeTab: EventlogTabs = EventlogTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public eventlogNavigator: EventlogNavigator,
              public  persist: EventlogPersist) {
    this.tcAuthorization.requireRead("eventlogs");
    this.eventlogDetail = new EventlogDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getEventlog(id.toString()).subscribe(eventlogDetail => {
      this.eventlogDetail = eventlogDetail;
    }, error => {
    });
  }

  editEventlog(): void {
    let modalRef = this.eventlogNavigator.editEventlog(this.eventlogDetail.id);
    modalRef.afterClosed().subscribe(modifiedEventlogDetail => {
      TCUtilsAngular.assign(this.eventlogDetail, modifiedEventlogDetail);
    }, error => {
      console.error(error);
    });
  }


}
