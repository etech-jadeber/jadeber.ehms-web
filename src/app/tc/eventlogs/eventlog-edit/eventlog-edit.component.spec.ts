import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EventlogEditComponent } from './eventlog-edit.component';

describe('EventlogEditComponent', () => {
  let component: EventlogEditComponent;
  let fixture: ComponentFixture<EventlogEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EventlogEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventlogEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
