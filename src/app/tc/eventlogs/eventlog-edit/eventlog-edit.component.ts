import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";




import {EventlogDetail} from "../eventlog.model";
import {EventlogPersist} from "../eventlog.persist";
import {TCAuthorization} from "../../authorization";
import {TCNotification} from "../../notification";
import {AppTranslation} from "../../../app.translation";


@Component({
  selector: 'app-eventlog-edit',
  templateUrl: './eventlog-edit.component.html',
  styleUrls: ['./eventlog-edit.component.css']
})
export class EventlogEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: EventlogDetail = new EventlogDetail();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<EventlogEditComponent>,
              public  persist: EventlogPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
     this.tcAuthorization.requireCreate("eventlogs");
      this.title = this.appTranslation.getText("general","new") +  " Eventlog";
      this.isNew = true;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("eventlogs");
      this.title = this.appTranslation.getText("general","edit") +  " Eventlog";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getEventlog(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addEventlog(this.item).subscribe(value => {
      this.tcNotification.success("Eventlog added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateEventlog(this.item).subscribe(value => {
      this.tcNotification.success("Eventlog updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
