import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EventlogListComponent } from './eventlog-list.component';

describe('EventlogListComponent', () => {
  let component: EventlogListComponent;
  let fixture: ComponentFixture<EventlogListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EventlogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventlogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
