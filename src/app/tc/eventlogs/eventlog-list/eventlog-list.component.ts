import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";


import {EventlogPersist} from "../eventlog.persist";
import {EventlogNavigator} from "../eventlog.navigator";
import {EventlogSummary, EventlogSummaryPartialList} from "../eventlog.model";
import {TCUtilsDate} from "../../utils-date";
import {TCAuthorization} from "../../authorization";
import {TCUtilsArray} from "../../utils-array";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {AppTranslation} from "../../../app.translation";
import {TCUtilsAngular} from "../../utils-angular";
import {JobPersist} from "../../jobs/job.persist";

export enum EventlogListTabs {
  overview
}

@Component({
  selector: 'app-eventlog-list',
  templateUrl: './eventlog-list.component.html',
  styleUrls: ['./eventlog-list.component.css']
})
export class EventlogListComponent implements OnInit {
  activeTab: EventlogListTabs = EventlogListTabs.overview;
  tabs: typeof EventlogListTabs = EventlogListTabs;

  eventlogsData: EventlogSummary[] = [];
  eventlogsTotalCount: number = 0;
  eventlogsSelection: EventlogSummary[] = [];
  eventlogsDisplayedColumns: string[] = ["select", "action", "link", "event_type", "log_text", "has_more", "target_id", "operator_id", "log_time"];

  eventlogsSearchTextBox: FormControl = new FormControl();
  eventlogsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) eventlogsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) eventlogsSort: MatSort;

  constructor(private router: Router,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public jobPersist: JobPersist,
              public eventlogPersist: EventlogPersist,
              public eventlogNavigator: EventlogNavigator
  ) {

    this.tcAuthorization.requireRead("eventlogs");
    //delay subsequent keyup events
    this.eventlogsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.eventlogPersist.eventlogSearchText = value;
      this.searchEventlogs();
    });
  }

  ngOnInit() {

    this.eventlogsSort.sortChange.subscribe(() => {
      this.eventlogsPaginator.pageIndex = 0;
      this.searchEventlogs();
    });

    this.eventlogsPaginator.page.subscribe(() => {
      this.searchEventlogs();
    });
    //start by loading items
    this.searchEventlogs();
  }

  searchEventlogs(): void {
    this.eventlogsIsLoading = true;
    this.eventlogsSelection = [];

    this.eventlogPersist.searchEventlog(this.eventlogsPaginator.pageSize, this.eventlogsPaginator.pageIndex, this.eventlogsSort.active, this.eventlogsSort.direction).subscribe((partialList: EventlogSummaryPartialList) => {
      this.eventlogsData = partialList.data;
      if (partialList.total != -1) {
        this.eventlogsTotalCount = partialList.total;
      }
      this.eventlogsIsLoading = false;
    }, error => {
      this.eventlogsIsLoading = false;
    });

  }

  downloadEventlogs(): void {
    this.eventlogPersist.download(this.tcUtilsArray.idsList(this.eventlogsSelection)).subscribe(downloadJob => {
      this.jobPersist.followJob(downloadJob.id, "download eventlogs", true);
    });
  }


  addEventlog(): void {
    let dialogRef = this.eventlogNavigator.addEventlog();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchEventlogs();
      }
    });
  }

  editEventlog(item: EventlogSummary) {
    let dialogRef = this.eventlogNavigator.editEventlog(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteEventlog(item: EventlogSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Eventlog");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.eventlogPersist.deleteEventlog(item.id).subscribe(response => {
          this.tcNotification.success("Eventlog deleted");
          this.searchEventlogs();
        }, error => {
        });
      }

    });
  }

}
