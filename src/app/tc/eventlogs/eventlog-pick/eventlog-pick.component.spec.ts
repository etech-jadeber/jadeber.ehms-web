import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EventlogPickComponent } from './eventlog-pick.component';

describe('EventlogPickComponent', () => {
  let component: EventlogPickComponent;
  let fixture: ComponentFixture<EventlogPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EventlogPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventlogPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
