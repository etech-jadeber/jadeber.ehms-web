import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {EventlogDetail, EventlogSummary, EventlogSummaryPartialList} from "../eventlog.model";
import {EventlogPersist} from "../eventlog.persist";
import {TCUtilsDate} from "../../utils-date";
import {TCAuthorization} from "../../authorization";
import {TCUtilsArray} from "../../utils-array";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {AppTranslation} from "../../../app.translation";
import {TCUtilsAngular} from "../../utils-angular";


@Component({
  selector: 'app-eventlog-pick',
  templateUrl: './eventlog-pick.component.html',
  styleUrls: ['./eventlog-pick.component.css']
})
export class EventlogPickComponent implements OnInit {

  eventlogsData: EventlogSummary[] = [];
  eventlogsTotalCount: number = 0;
  eventlogsSelection: EventlogSummary[] = [];
  eventlogsDisplayedColumns: string[] = ["select", 'event_type','log_text','has_more','target_id','operator_id','log_time' ];

  eventlogsSearchTextBox: FormControl = new FormControl();
  eventlogsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) eventlogsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) eventlogsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public eventlogPersist: EventlogPersist,
              public dialogRef: MatDialogRef<EventlogPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("eventlogs");
    //delay subsequent keyup events
    this.eventlogsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.eventlogPersist.eventlogSearchText = value;
      this.searchEventlogs();
    });
  }

  ngOnInit() {

    this.eventlogsSort.sortChange.subscribe(() => {
      this.eventlogsPaginator.pageIndex = 0;
      this.searchEventlogs();
    });

    this.eventlogsPaginator.page.subscribe(() => {
      this.searchEventlogs();
    });
    //start by loading items
    this.searchEventlogs();
  }

  searchEventlogs(): void {
    this.eventlogsIsLoading = true;
    this.eventlogsSelection = [];

    this.eventlogPersist.searchEventlog(this.eventlogsPaginator.pageSize,
        this.eventlogsPaginator.pageIndex,
        this.eventlogsSort.active,
        this.eventlogsSort.direction).subscribe((partialList: EventlogSummaryPartialList) => {
      this.eventlogsData = partialList.data;
      if (partialList.total != -1) {
        this.eventlogsTotalCount = partialList.total;
      }
      this.eventlogsIsLoading = false;
    }, error => {
      this.eventlogsIsLoading = false;
    });

  }

  markOneItem(item: EventlogSummary) {
    this.eventlogsSelection = [];
    this.eventlogsSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.eventlogsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
