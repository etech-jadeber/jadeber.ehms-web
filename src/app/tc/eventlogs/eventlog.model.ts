import {TCId} from "../../tc/models";

export class EventlogSummary extends TCId {
  event_type: number;
  log_text: string;
  has_more: boolean;
  target_id: string;
  operator_id: string;
  log_time: number;
}

export class EventlogSummaryPartialList {
  data: EventlogSummary[];
  total: number;
}

export class EventlogDetail extends EventlogSummary {

}

export class EventlogDashboard {
  total: number;
}
