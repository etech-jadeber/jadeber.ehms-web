import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MatDialog,MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {EventlogEditComponent} from "./eventlog-edit/eventlog-edit.component";
import {EventlogPickComponent} from "./eventlog-pick/eventlog-pick.component";


@Injectable({
  providedIn: 'root'
})

export class EventlogNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  eventlogsUrl(): string {
    return "/eventlogs";
  }

  eventlogUrl(id: string): string {
    return "/eventlogs/" + id;
  }

  viewEventlogs(): void {
    this.router.navigateByUrl(this.eventlogsUrl());
  }

  viewEventlog(id: string): void {
    this.router.navigateByUrl(this.eventlogUrl(id));
  }

  editEventlog(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(EventlogEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addEventlog(): MatDialogRef<unknown,any> {
    return this.editEventlog(null);
  }
  
   pickEventlogs(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(EventlogPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
