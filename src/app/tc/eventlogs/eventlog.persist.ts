import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {EventlogDashboard, EventlogDetail, EventlogSummaryPartialList} from "./eventlog.model";


@Injectable({
  providedIn: 'root'
})
export class EventlogPersist {

  eventlogSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchEventlog(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<EventlogSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/eventlogs", this.eventlogSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<EventlogSummaryPartialList>(url);

  }

  eventlogDashboard(): Observable<EventlogDashboard> {
    return this.http.get<EventlogDashboard>(environment.tcApiBaseUri + "tc/eventlogs/dashboard");
  }

  getEventlog(id: string): Observable<EventlogDetail> {
    return this.http.get<EventlogDetail>(environment.tcApiBaseUri + "tc/eventlogs/" + id);
  }

  addEventlog(item: EventlogDetail): Observable<TCId> {
    return this.http.post<EventlogDetail>(environment.tcApiBaseUri + "tc/eventlogs/", item);
  }

  updateEventlog(item: EventlogDetail): Observable<EventlogDetail> {
    return this.http.patch<EventlogDetail>(environment.tcApiBaseUri + "tc/eventlogs/" + item.id, item);
  }

  deleteEventlog(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/eventlogs/" + id);
  }

  eventlogsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/eventlogs/do", new TCDoParam(method, payload));
  }

  eventlogDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/eventlogs/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "tc/eventlogs/do", new TCDoParam("download", ids));
  }

}
