import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {FileDetail} from "../file.model";
import {FilePersist} from "../file.persist";
import {FileNavigator} from "../file.navigator";


export enum FileTabs {
  overview,
}

@Component({
  selector: 'app-file-detail',
  templateUrl: './file-detail.component.html',
  styleUrls: ['./file-detail.component.css']
})
export class FileDetailComponent implements OnInit {

  //basics
  fileDetail: FileDetail;
  fileTabs: typeof FileTabs = FileTabs;
  activeTab: FileTabs = FileTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public fileNavigator: FileNavigator,
              public  persist: FilePersist) {
    this.tcAuthorization.requireRead("files");
    this.fileDetail = new FileDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getFile(id.toString()).subscribe(fileDetail => {
      this.fileDetail = fileDetail;
    }, error => {
    });
  }

  editFile(): void {
    let modalRef = this.fileNavigator.editFile(this.fileDetail.id);
    modalRef.afterClosed().subscribe(modifiedFileDetail => {
      TCUtilsAngular.assign(this.fileDetail, modifiedFileDetail);
    }, error => {
      console.error(error);
    });
  }


}
