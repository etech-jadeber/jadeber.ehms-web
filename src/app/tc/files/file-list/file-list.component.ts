import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {FilePersist} from "../file.persist";
import {FileNavigator} from "../file.navigator";
import {FileSummary, FileSummaryPartialList} from "../file.model";
import {HttpClient} from "@angular/common/http";

export enum FileListTabs {
  overview
}

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {

  activeTab: FileListTabs = FileListTabs.overview;
  tabs: typeof FileListTabs = FileListTabs;

  filesData: FileSummary[] = [];
  filesTotalCount: number = 0;
  filesSelection: FileSummary[] = [];
  filesDisplayedColumns: string[] = ["select", "action", "link", "file_type", "file_name", "creation_time", "expiration_time", "content_type", "is_permanent", "target_id"];

  filesSearchTextBox: FormControl = new FormControl();
  filesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) filesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) filesSort: MatSort;

  constructor(private router: Router,
              private http: HttpClient,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public filePersist: FilePersist,
              public fileNavigator: FileNavigator
  ) {

    this.tcAuthorization.requireRead("files");
    //delay subsequent keyup events
    this.filesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.filePersist.fileSearchText = value;
      this.searchFiles();
    });
  }

  ngOnInit() {

    this.filesSort.sortChange.subscribe(() => {
      this.filesPaginator.pageIndex = 0;
      this.searchFiles();
    });

    this.filesPaginator.page.subscribe(() => {
      this.searchFiles();
    });
    //start by loading items
    this.searchFiles();
  }

  searchFiles(): void {
    this.filesIsLoading = true;
    this.filesSelection = [];

    this.filePersist.searchFile(this.filesPaginator.pageSize, this.filesPaginator.pageIndex, this.filesSort.active, this.filesSort.direction).subscribe((partialList: FileSummaryPartialList) => {
      this.filesData = partialList.data;
      if (partialList.total != -1) {
        this.filesTotalCount = partialList.total;
      }
      this.filesIsLoading = false;
    }, error => {
      this.filesIsLoading = false;
    });

  }

  downloadFiles(): void {
    this.tcNotification.info("Download selected items . . . .");
  }


  addFile(): void {
    let dialogRef = this.fileNavigator.addFile();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchFiles();
      }
    });
  }

  editFile(item: FileSummary) {
    let dialogRef = this.fileNavigator.editFile(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteFile(item: FileSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("File");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.filePersist.deleteFile(item.id).subscribe(response => {
          this.tcNotification.success("File deleted");
          this.searchFiles();
        }, error => {
        });
      }

    });
  }


}
