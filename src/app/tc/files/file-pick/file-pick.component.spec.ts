import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FilePickComponent } from './file-pick.component';

describe('FilePickComponent', () => {
  let component: FilePickComponent;
  let fixture: ComponentFixture<FilePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FilePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
