import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {FileDetail, FileSummary, FileSummaryPartialList} from "../file.model";
import {FilePersist} from "../file.persist";


@Component({
  selector: 'app-file-pick',
  templateUrl: './file-pick.component.html',
  styleUrls: ['./file-pick.component.css']
})
export class FilePickComponent implements OnInit {

  filesData: FileSummary[] = [];
  filesTotalCount: number = 0;
  filesSelection: FileSummary[] = [];
  filesDisplayedColumns: string[] = ["select", 'file_type','file_name','creation_time','expiration_time','content_type','is_permanent','target_id' ];

  filesSearchTextBox: FormControl = new FormControl();
  filesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) filesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) filesSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public filePersist: FilePersist,
              public dialogRef: MatDialogRef<FilePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("files");
    //delay subsequent keyup events
    this.filesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.filePersist.fileSearchText = value;
      this.searchFiles();
    });
  }

  ngOnInit() {

    this.filesSort.sortChange.subscribe(() => {
      this.filesPaginator.pageIndex = 0;
      this.searchFiles();
    });

    this.filesPaginator.page.subscribe(() => {
      this.searchFiles();
    });
    //start by loading items
    this.searchFiles();
  }

  searchFiles(): void {
    this.filesIsLoading = true;
    this.filesSelection = [];

    this.filePersist.searchFile(this.filesPaginator.pageSize,
        this.filesPaginator.pageIndex,
        this.filesSort.active,
        this.filesSort.direction).subscribe((partialList: FileSummaryPartialList) => {
      this.filesData = partialList.data;
      if (partialList.total != -1) {
        this.filesTotalCount = partialList.total;
      }
      this.filesIsLoading = false;
    }, error => {
      this.filesIsLoading = false;
    });

  }

  markOneItem(item: FileSummary) {
    this.filesSelection = [];
    this.filesSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.filesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
