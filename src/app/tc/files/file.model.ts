import {TCId} from "../../tc/models";

export class FileSummary extends TCId {
  file_type: number;
  file_name: string;
  creation_time: number;
  expiration_time: number;
  content_type: number;
  is_permanent: boolean;
  target_id: string;
  download_key: string;
}

export class FileSummaryPartialList {
  data: FileSummary[];
  total: number;
}

export class FileDetail extends FileSummary {
  is_downloaded: boolean;
}

export class FileDashboard {
  total: number;
}
