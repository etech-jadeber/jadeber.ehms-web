import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {FileEditComponent} from "./file-edit/file-edit.component";
import {FilePickComponent} from "./file-pick/file-pick.component";


@Injectable({
  providedIn: 'root'
})

export class FileNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  filesUrl(): string {
    return "/files";
  }

  fileUrl(id: string): string {
    return "/files/" + id;
  }

  viewFiles(): void {
    this.router.navigateByUrl(this.filesUrl());
  }

  viewFile(id: string): void {
    this.router.navigateByUrl(this.fileUrl(id));
  }

  editFile(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(FileEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addFile(): MatDialogRef<unknown,any> {
    return this.editFile(null);
  }
  
   pickFiles(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(FilePickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
