import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {FileDashboard, FileDetail, FileSummary, FileSummaryPartialList} from "./file.model";


@Injectable({
  providedIn: 'root'
})
export class FilePersist {

  allowedDocumentTypes: string[] = [".txt", ".doc", ".docx", ".pdf", ".jpg", ".jpeg", ".png", ".tif", ".xls", ".xlsx"];
  allowedDocumentFilter: string = "image/*, .doc, .docx, .xls, .xlsx, .pdf";

  fileSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchFile(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<FileSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/files", this.fileSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<FileSummaryPartialList>(url);

  }

  fileDashboard(): Observable<FileDashboard> {
    return this.http.get<FileDashboard>(environment.tcApiBaseUri + "tc/files/dashboard");
  }

  getFile(id: string): Observable<FileDetail> {
    return this.http.get<FileDetail>(environment.tcApiBaseUri + "tc/files/" + id);
  }

  addFile(item: FileDetail): Observable<TCId> {
    return this.http.post<FileDetail>(environment.tcApiBaseUri + "tc/files/", item);
  }

  updateFile(item: FileDetail): Observable<FileDetail> {
    return this.http.patch<FileDetail>(environment.tcApiBaseUri + "tc/files/" + item.id, item);
  }

  deleteFile(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "tc/files/" + id);
  }

  filesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/files/do", new TCDoParam(method, payload));
  }

  fileDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/files/" + id + "/do", new TCDoParam(method, payload));
  }

  downloadLink(download_key:string):string{
    return environment.tcApiBaseUri + "tc/files/download?fk=" + download_key;
  }

  getDownloadKey(id:string):Observable<string>{
    return this.http.get<string>(environment.tcApiBaseUri + `tc/files/download_key?id=${id}`)
  }

  isAllowedDocumentType(fileName: string): boolean {
    for (let fileType of this.allowedDocumentTypes) {

      if (fileName.toLowerCase().endsWith(fileType.toLocaleLowerCase())) {
        return true;
      }
    }
    return false;
  }



}
