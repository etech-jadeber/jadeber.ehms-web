import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {TCUtilsAngular} from "../../utils-angular";
import {TCNotification} from "../../notification";
import {TCUtilsArray} from "../../utils-array";

import {GroupDetail} from "../group.model";
import {GroupPersist} from "../group.persist";
import {GroupNavigator} from "../group.navigator";
import {UserDetail} from "../../users/user.model";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {FormControl} from "@angular/forms";
import {debounceTime} from "rxjs/operators";
import {UserNavigator} from "../../users/user.navigator";
import {TCNavigator} from "../../navigator";


export enum GroupTabs {
  overview,
  users
}

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css']
})
export class GroupDetailComponent implements OnInit {

  //basics
  groupDetail: GroupDetail;
  groupTabs: typeof GroupTabs = GroupTabs;
  activeTab: GroupTabs = GroupTabs.overview;

  //members
  usersColumns: string[] = ['select', 'username', 'name', 'email'];
  usersDs: MatTableDataSource<UserDetail>;
  usersSelected: UserDetail[] = [];
  usersFilter: FormControl = new FormControl();

  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public groupNavigator: GroupNavigator,
              public userNavigator: UserNavigator,
              public  persist: GroupPersist) {
    this.groupDetail = new GroupDetail();
    this.groupDetail.members = [];
    //members filter
    this.usersFilter.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.usersDs.filter = value.trim().toLowerCase();
      if (this.usersDs.paginator) {
        this.usersDs.paginator.firstPage();
      }
    });
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getGroup(id.toString()).subscribe(groupDetail => {
      this.groupDetail = groupDetail;
      //members data source
      this.bindUsersData();
    }, error => {
    });
  }

  editGroup(): void {
    let modalRef = this.groupNavigator.editGroup(this.groupDetail.id);
    modalRef.afterClosed().subscribe(modifiedGroupDetail => {
      TCUtilsAngular.assign(this.groupDetail, modifiedGroupDetail);
    }, error => {
      console.error(error);
    });
  }

  bindUsersData(): void {
    this.usersDs = new MatTableDataSource(this.groupDetail.members);
    //caution: use proper index for paginator & sorter if not the first table added to component
    this.usersDs.sort = this.sorters.toArray()[0];
    this.usersDs.paginator = this.paginators.toArray()[0];
  }

  addMembers(): void {
    let dialogRef = this.userNavigator.pickUsers(false);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let pickedUsers: UserDetail[] = result;
        let params = {user_ids: []};
        pickedUsers.forEach(user => {
          params.user_ids.push(user.id);
        });

        this.persist.groupDo(this.groupDetail, "members_add", params).subscribe(result => {
          this.tcNotification.success(pickedUsers.length.toString() + " Members added");
          //update the display
          pickedUsers.forEach(user => {
            if (!this.tcUtilsArray.contains(this.groupDetail.members, user)) {
              this.groupDetail.members.push(user)
            }
          });
          this.bindUsersData();
        });

      }
    });

  }

  removeSelectedMembers(): void {

    let params = {user_ids: []};
    this.usersSelected.forEach(user => {
      params.user_ids.push(user.id);
    });
    this.persist.groupDo(this.groupDetail, "members_remove", params).subscribe(result => {
      this.tcNotification.success(this.usersSelected.length.toString() + " Members removed");
      //update the display
      this.usersSelected.forEach(user => this.tcUtilsArray.removeItem(this.groupDetail.members, user));
      this.usersSelected = [];
      this.bindUsersData();
    });
  }

}
