import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../notification";


import {GroupDetail} from "../group.model";
import {GroupPersist} from "../group.persist";


@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.css']
})
export class GroupEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: GroupDetail = new GroupDetail();

  constructor(public tcNotification: TCNotification,
              public dialogRef: MatDialogRef<GroupEditComponent>,
              public  persist: GroupPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
      this.title = "New Group";
      this.isNew = true;
      //set necessary defaults
    } else {
      this.title = "Edit Group";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getGroup(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addGroup(this.item).subscribe(value => {
      this.tcNotification.success("Group added");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateGroup(this.item).subscribe(value => {
      this.tcNotification.success("Group updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
