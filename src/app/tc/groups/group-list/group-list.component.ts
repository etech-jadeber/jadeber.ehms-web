import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCUtilsAngular} from "../../utils-angular";
import {TCUtilsArray} from "../../utils-array";
import {TCUtilsDate} from "../../utils-date";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";

import {GroupPersist} from "../group.persist";
import {GroupNavigator} from "../group.navigator";
import {GroupDetail, GroupSummary, GroupSummaryPartialList} from "../group.model";
import {AppTranslation} from "../../../app.translation";

export enum GroupListTabs {
  overview
}

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
  activeTab: GroupListTabs = GroupListTabs.overview;
  tabs: typeof GroupListTabs = GroupListTabs;

  groupsData: GroupSummary[] = [];
  groupsTotalCount: number = 0;
  groupsSelection: GroupDetail[] = [];
  groupsDisplayedColumns: string[] = ["select", "action","name","members"];

  groupsSearchTextBox: FormControl = new FormControl();
  groupsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) groupsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) groupsSort: MatSort;

  constructor(private router: Router,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public groupPersist: GroupPersist,
                public  appTranslation:AppTranslation,
                public groupNavigator: GroupNavigator
    ) {

      //delay subsequent keyup events
      this.groupsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.groupPersist.groupSearchText = value;
        this.searchGroups();
      });
    }

    ngOnInit() {

      this.groupsSort.sortChange.subscribe(() => {
        this.groupsPaginator.pageIndex = 0;
        this.searchGroups();
      });

      this.groupsPaginator.page.subscribe(() => {
        this.searchGroups();
      });
      //start by loading items
      this.searchGroups();
    }

  searchGroups(): void {
    this.groupsIsLoading = true;
    this.groupsSelection = [];

    this.groupPersist.searchGroup(this.groupsPaginator.pageSize, this.groupsPaginator.pageIndex, this.groupsSort.active, this.groupsSort.direction).subscribe((partialList: GroupSummaryPartialList) => {
      this.groupsData = partialList.data;
      if (partialList.total != -1) {
        this.groupsTotalCount = partialList.total;
      }
      this.groupsIsLoading = false;
    }, error => {
      this.groupsIsLoading = false;
    });

  }

  downloadGroups(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addGroup(): void {
    let dialogRef = this.groupNavigator.addGroup();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchGroups();
      }
    });
  }

  editGroup(item: GroupDetail) {
    let dialogRef = this.groupNavigator.editGroup(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteGroup(item: GroupDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Group");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.groupPersist.deleteGroup(item.id).subscribe(response => {
          this.tcNotification.success("Group deleted");
          this.searchGroups();
        }, error => {
        });
      }

    });
  }

}
