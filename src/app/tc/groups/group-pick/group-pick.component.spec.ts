import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GroupPickComponent } from './group-pick.component';

describe('GroupPickComponent', () => {
  let component: GroupPickComponent;
  let fixture: ComponentFixture<GroupPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
