import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {GroupSummary, GroupSummaryPartialList} from "../group.model";
import {GroupPersist} from "../group.persist";


@Component({
  selector: 'app-group-pick',
  templateUrl: './group-pick.component.html',
  styleUrls: ['./group-pick.component.css']
})
export class GroupPickComponent implements OnInit {

  groupsData: GroupSummary[] = [];
  groupsTotalCount: number = 0;
  groupsSelection: GroupSummary[] = [];
  groupsDisplayedColumns: string[] = ["select", 'name'];

  groupsSearchTextBox: FormControl = new FormControl();
  groupsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) groupsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) groupsSort: MatSort;

  constructor(public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public groupPersist: GroupPersist,
              public dialogRef: MatDialogRef<GroupPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("groups");
    //delay subsequent keyup events
    this.groupsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.groupPersist.groupSearchText = value;
      this.searchGroups();
    });
  }

  ngOnInit() {

    this.groupsSort.sortChange.subscribe(() => {
      this.groupsPaginator.pageIndex = 0;
      this.searchGroups();
    });

    this.groupsPaginator.page.subscribe(() => {
      this.searchGroups();
    });
    //start by loading items
    this.searchGroups();
  }

  searchGroups(): void {
    this.groupsIsLoading = true;
    this.groupsSelection = [];

    this.groupPersist.searchGroup(this.groupsPaginator.pageSize,
      this.groupsPaginator.pageIndex,
      this.groupsSort.active,
      this.groupsSort.direction).subscribe((partialList: GroupSummaryPartialList) => {
      this.groupsData = partialList.data;
      if (partialList.total != -1) {
        this.groupsTotalCount = partialList.total;
      }
      this.groupsIsLoading = false;
    }, error => {
      this.groupsIsLoading = false;
    });

  }

  markOneItem(item: GroupSummary) {
    if (!this.tcUtilsArray.containsId(this.groupsSelection, item.id)) {
      this.groupsSelection = [];
      this.groupsSelection.push(item);
    } else {
      this.groupsSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.groupsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
