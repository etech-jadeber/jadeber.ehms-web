import {TCCollection} from "../models";
import {UserDetail} from "../users/user.model";
import {TCUtilsString} from "../utils-string";

export class GroupSummary extends TCCollection {
}

export class GroupSummaryPartialList {
  data: GroupSummary[];
  total: number;
}

export class GroupDetail extends GroupSummary {
  name: string;
  members: UserDetail[];

  static InvalidGroup(): GroupDetail {
    let ret = new GroupDetail();
    ret.id = TCUtilsString.getInvalidId();
    return ret;
  }
}

export class GroupDashboard {
  total: number;
}
