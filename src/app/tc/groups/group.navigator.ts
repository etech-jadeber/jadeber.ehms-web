import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../utils-angular";

import {GroupEditComponent} from "./group-edit/group-edit.component";
import {GroupPickComponent} from "./group-pick/group-pick.component";


@Injectable({
  providedIn: 'root'
})

export class GroupNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  groupsUrl(): string {
    return "/groups";
  }

  groupUrl(id: string): string {
    return "/groups/" + id;
  }

  viewGroups(): void {
    this.router.navigateByUrl(this.groupsUrl());
  }

  viewGroup(id: string): void {
    this.router.navigateByUrl(this.groupUrl(id));
  }

  editGroup(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(GroupEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addGroup(): MatDialogRef<unknown,any> {
    return this.editGroup(null);
  }

  pickGroups(selectOne: boolean=false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(GroupPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
