import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../utils-http";
import {GroupDashboard, GroupDetail, GroupSummaryPartialList} from "./group.model";


@Injectable({
  providedIn: 'root'
})
export class GroupPersist {

  groupSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchGroup(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<GroupSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("groups", this.groupSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<GroupSummaryPartialList>(url);

  }

  groupDashboard(): Observable<GroupDashboard> {
    return this.http.get<GroupDashboard>(environment.tcApiBaseUri + "groups/dashboard");
  }

  getGroup(id: string): Observable<GroupDetail> {
    return this.http.get<GroupDetail>(environment.tcApiBaseUri + "groups/" + id);
  }

  deleteGroup(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "groups/" + id);
  }

  addGroup(item: GroupDetail): Observable<{}> {
    return this.http.post<GroupDetail>(environment.tcApiBaseUri + "groups/", item);
  }

  updateGroup(item: GroupDetail): Observable<GroupDetail> {
    return this.http.patch<GroupDetail>(environment.tcApiBaseUri + "groups/" + item.id, item);
  }

  groupsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "groups/do", new TCDoParam(method, payload));
  }

  groupDo(item: GroupDetail, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "groups/" + item.id + "/do", new TCDoParam(method, payload));
  }


}
