import {TCNotification} from "./notification";
import {HTTP_INTERCEPTORS, HttpHandler, HttpInterceptor, HttpRequest, HttpClient, HttpResponse, HttpStatusCode} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable, of, throwError} from "rxjs";
import {catchError, switchMap} from "rxjs/operators";

import {TCNavigator} from "./navigator";
import {TCAppInit} from "./app-init";
import {TCUserInfo, Tokens } from "./authentication";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root',
})

export class TCHttpInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  constructor(
    private tcNavigator: TCNavigator, private http:HttpClient,
    private tcNotification: TCNotification) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {


    //insert authorization token if user is already logged in
    const authReq = this.addToken(req);


    return next.handle(authReq).pipe(
      this.handleError(req, next)
    );
  }

  private addToken(req: HttpRequest<any>){
    return TCAppInit.token ? req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + TCAppInit.token)
    }) : req;
  }

  private handleError(req: HttpRequest<any>, next: HttpHandler) {
    return catchError((error) => {
      switch (error.status) {
                case 302:
                  this.tcNotification.info(error.error.msgs, true)
                  return of(new HttpResponse({body: {}, status: 0}))
                case 400:
                  this.tcNotification.validationErrors(error.error.msgs);
                  break;
                case 401:
                  return this.handle401Error(req, next);
                case 403:
                  //this.tcNavigator.accessDenied();
                  break;
                case 404:
                  //this.tcNavigator.notFound();
                  break;
                case 503:
                  this.tcNotification.error("Unable to contact remote server. Make sure your connection is working and try again");
                  break;
                default:
                  //handle remaining 4xx or 5xx responses
                  if (error.status / 100 > 3) {
                    this.tcNotification.error(error.error.description);
                  }
                  break;
              }
    })
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    const url = environment.tcApiBaseUri + "refreshtoken";
    if(!localStorage.getItem("refresh_token")){
      this.logout(); 
      return;
    } 

    if(request.url == url){
      this.logout(); 
      return;
    }
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      return this.http.post(url, {"token":localStorage.getItem("refresh_token")}).pipe(
          switchMap((res: Tokens) => {
            this.isRefreshing = false;
            localStorage.setItem("refresh_token", res.refresh_token);
            localStorage.setItem(environment.tcApiBaseUri + ".authorizationData", JSON.stringify({"token": res.access_token}));
            TCAppInit.token = res.access_token
            return next.handle(this.addToken(request)).pipe(this.handleError(request, next));
          })
        );
    }

    return next.handle(this.addToken(request)).pipe(this.handleError(request, next));
  }

  logout(): void {
  //   TCAppInit.userInfo = new TCUserInfo();
  //   const url = environment.tcApiBaseUri + "logout";
  //   this.http.post(url, {}).subscribe((value) => {
  //     localStorage.removeItem(environment.tcApiBaseUri + ".authorizationData");
  //     localStorage.removeItem("refresh_token");
      
  //     TCAppInit.token = null;
  //     this.tcNavigator.login();
  //     TCAppInit.isLoggedIn = false;
  //     TCAppInit.appInitialized$.next(true);
  //   })
  // }
    TCAppInit.userInfo = new TCUserInfo();
    localStorage.removeItem(environment.tcApiBaseUri + ".authorizationData");
    localStorage.removeItem("refresh_token");
    
    TCAppInit.token = null;
    this.tcNavigator.login();
    TCAppInit.isLoggedIn = false;
    TCAppInit.appInitialized$.next(true);
  }

}

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: TCHttpInterceptor, multi: true},
];

