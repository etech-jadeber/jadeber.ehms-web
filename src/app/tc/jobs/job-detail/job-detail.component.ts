import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {JobDetail} from "../job.model";
import {JobPersist} from "../job.persist";
import {JobNavigator} from "../job.navigator";


export enum JobTabs {
  overview,
}

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {

  //basics
  jobDetail: JobDetail;
  jobTabs: typeof JobTabs = JobTabs;
  activeTab: JobTabs = JobTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public jobNavigator: JobNavigator,
              public  persist: JobPersist) {
    this.tcAuthorization.requireRead("jobs");
    this.jobDetail = new JobDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getJob(id.toString()).subscribe(jobDetail => {
      this.jobDetail = jobDetail;
    }, error => {
    });
  }

  editJob(): void {
    let modalRef = this.jobNavigator.editJob(this.jobDetail.id);
    modalRef.afterClosed().subscribe(modifiedJobDetail => {
      TCUtilsAngular.assign(this.jobDetail, modifiedJobDetail);
    }, error => {
      console.error(error);
    });
  }


}
