import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";


import {JobDetail} from "../job.model";
import {JobPersist} from "../job.persist";
import { TCUtilsDate } from '../../utils-date';


@Component({
  selector: 'app-job-edit',
  templateUrl: './job-edit.component.html',
  styleUrls: ['./job-edit.component.css']
})
export class JobEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: JobDetail = new JobDetail();
  creation_time: Date = new Date();
  update_time: Date = new Date();

  constructor(public tcAuthorization: TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsDate: TCUtilsDate,
              public appTranslation: AppTranslation,
              public dialogRef: MatDialogRef<JobEditComponent>,
              public  persist: JobPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
      this.tcAuthorization.requireCreate("jobs");
      this.title = this.appTranslation.getText("general", "new") + " Job";
      this.isNew = true;
      //set necessary defaults
    } else {
      this.tcAuthorization.requireUpdate("jobs");
      this.title = this.appTranslation.getText("general", "edit") + " Job";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getJob(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.addJob(this.item).subscribe(value => {
      this.tcNotification.success("Job added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.updateJob(this.item).subscribe(value => {
      this.tcNotification.success("Job updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
  transformDates(todate:boolean){
    if(todate){
      this.item.creation_time = this.tcUtilsDate.toTimeStamp(this.creation_time);
      this.item.update_time = this.tcUtilsDate.toTimeStamp(this.update_time);
    }
    else{
      this.creation_time = this.tcUtilsDate.toDate(this.item.creation_time);
      this.update_time = this.tcUtilsDate.toDate(this.item.update_time);
    }
  }
}
