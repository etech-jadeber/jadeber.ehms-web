import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {JobPersist} from "../job.persist";
import {JobNavigator} from "../job.navigator";
import {JobSummary, JobSummaryPartialList} from "../job.model";
import {TCAsyncJob} from "../../asyncjob";

export enum JobListTabs {
  overview
}

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  activeTab: JobListTabs = JobListTabs.overview;
  tabs: typeof JobListTabs = JobListTabs;

  jobsData: JobSummary[] = [];
  jobsTotalCount: number = 0;
  jobsSelection: JobSummary[] = [];
  jobsDisplayedColumns: string[] = ["select", "action", "link", "job_type", "target_id", "job_state", "creation_time", "update_time",];

  jobsSearchTextBox: FormControl = new FormControl();
  jobsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) jobsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) jobsSort: MatSort;

  constructor(private router: Router,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcAsyncJob: TCAsyncJob,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public jobPersist: JobPersist,
              public jobNavigator: JobNavigator
  ) {

    this.tcAuthorization.requireRead("jobs");
    //delay subsequent keyup events
    this.jobsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.jobPersist.jobSearchText = value;
      this.searchJobs();
    });
  }

  ngOnInit() {

    this.jobsSort.sortChange.subscribe(() => {
      this.jobsPaginator.pageIndex = 0;
      this.searchJobs();
    });

    this.jobsPaginator.page.subscribe(() => {
      this.searchJobs();
    });
    //start by loading items
    this.searchJobs();
  }

  searchJobs(): void {
    this.jobsIsLoading = true;
    this.jobsSelection = [];

    this.jobPersist.searchJob(this.jobsPaginator.pageSize, this.jobsPaginator.pageIndex, this.jobsSort.active, this.jobsSort.direction).subscribe((partialList: JobSummaryPartialList) => {
      this.jobsData = partialList.data;
      if (partialList.total != -1) {
        this.jobsTotalCount = partialList.total;
      }
      this.jobsIsLoading = false;
    }, error => {
      this.jobsIsLoading = false;
    });

  }

  downloadJobs(): void {
    this.tcNotification.info("Download selected items . . . .");
  }


  addJob(): void {
    let dialogRef = this.jobNavigator.addJob();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchJobs();
      }
    });
  }

  editJob(item: JobSummary) {
    let dialogRef = this.jobNavigator.editJob(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteJob(item: JobSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Job");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.jobPersist.deleteJob(item.id).subscribe(response => {
          this.tcNotification.success("Job deleted");
          this.searchJobs();
        }, error => {
        });
      }

    });
  }



}
