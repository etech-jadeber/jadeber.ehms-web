import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { JobPickComponent } from './job-pick.component';

describe('JobPickComponent', () => {
  let component: JobPickComponent;
  let fixture: ComponentFixture<JobPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
