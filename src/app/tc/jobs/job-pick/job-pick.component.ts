import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {JobDetail, JobSummary, JobSummaryPartialList} from "../job.model";
import {JobPersist} from "../job.persist";


@Component({
  selector: 'app-job-pick',
  templateUrl: './job-pick.component.html',
  styleUrls: ['./job-pick.component.css']
})
export class JobPickComponent implements OnInit {

  jobsData: JobSummary[] = [];
  jobsTotalCount: number = 0;
  jobsSelection: JobSummary[] = [];
  jobsDisplayedColumns: string[] = ["select", 'job_type','target_id','job_state','creation_time','update_time' ];

  jobsSearchTextBox: FormControl = new FormControl();
  jobsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) jobsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) jobsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public jobPersist: JobPersist,
              public dialogRef: MatDialogRef<JobPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("jobs");
    //delay subsequent keyup events
    this.jobsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.jobPersist.jobSearchText = value;
      this.searchJobs();
    });
  }

  ngOnInit() {

    this.jobsSort.sortChange.subscribe(() => {
      this.jobsPaginator.pageIndex = 0;
      this.searchJobs();
    });

    this.jobsPaginator.page.subscribe(() => {
      this.searchJobs();
    });
    //start by loading items
    this.searchJobs();
  }

  searchJobs(): void {
    this.jobsIsLoading = true;
    this.jobsSelection = [];

    this.jobPersist.searchJob(this.jobsPaginator.pageSize,
        this.jobsPaginator.pageIndex,
        this.jobsSort.active,
        this.jobsSort.direction).subscribe((partialList: JobSummaryPartialList) => {
      this.jobsData = partialList.data;
      if (partialList.total != -1) {
        this.jobsTotalCount = partialList.total;
      }
      this.jobsIsLoading = false;
    }, error => {
      this.jobsIsLoading = false;
    });

  }

  markOneItem(item: JobSummary) {
    this.jobsSelection = [];
    this.jobsSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.jobsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
