import {TCId} from "../../tc/models";

import {Subscription} from 'rxjs'
import {Injectable} from "@angular/core";

export class JobSummary extends TCId {
  job_type: number;
  target_id: string;
  job_state: number;
  creation_time: number;
  update_time: number;
}

export class JobSummaryPartialList {
  data: JobSummary[];
  total: number;
}

export class JobDetail extends JobSummary {
  job_type: number;
  target_id: string;
  job_state: number;
  creation_time: number;
  update_time: number;
}

export class JobDashboard {
  total: number;
}

export class JobState {
  job_state: number;
  message:string;
}

export class JobFile {
  download_key: string;
}

export class AsyncJob extends TCId {
  title: string;
  is_terminated: boolean;
  is_download_job: boolean;
  subscription: Subscription;
}


@Injectable({
  providedIn: 'root'
})
export class JobData {
  unfinishedJobs: AsyncJob[] = [];
}
