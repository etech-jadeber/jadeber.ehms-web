import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {JobEditComponent} from "./job-edit/job-edit.component";
import {JobPickComponent} from "./job-pick/job-pick.component";


@Injectable({
  providedIn: 'root'
})

export class JobNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  jobsUrl(): string {
    return "/jobs";
  }

  jobUrl(id: string): string {
    return "/jobs/" + id;
  }

  viewJobs(): void {
    this.router.navigateByUrl(this.jobsUrl());
  }

  viewJob(id: string): void {
    this.router.navigateByUrl(this.jobUrl(id));
  }

  editJob(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(JobEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addJob(): MatDialogRef<unknown,any> {
    return this.editJob(null);
  }
  
   pickJobs(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(JobPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
