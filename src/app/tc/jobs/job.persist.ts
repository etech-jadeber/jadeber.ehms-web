import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {interval, Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCEnum, TCId} from "../../tc/models";
import {AsyncJob, JobDashboard, JobData, JobDetail, JobFile, JobState, JobSummaryPartialList} from "./job.model";
import {TCUtilsString} from "../utils-string";
import {TCUtilsArray} from "../utils-array";
import {FilePersist} from "../files/file.persist";
import {TCNavigator} from "../navigator";
import {TCAsyncJob} from "../asyncjob";
import { TCNotification } from '../notification';


@Injectable({
  providedIn: 'root'
})
export class JobPersist {

  unknown_state = -1;
  queued_state = 1;
  running_state = 2;
  success_state = 3;
  failed_state = 4;
  aborted_state = 5;


  JobStates: TCEnum[] = [
    new TCEnum(this.unknown_state, ''),
    new TCEnum(this.queued_state, 'Queued'),
    new TCEnum(this.running_state, 'Running'),
    new TCEnum(this.success_state, 'Success'),
    new TCEnum(this.failed_state, 'Failed'),
    new TCEnum(this.aborted_state, 'Aborted'),
  ];

  jobSearchText: string;
  jobStateFilter: string = "";

  constructor(private http: HttpClient,
              public tcUtilsArray: TCUtilsArray,
              public filePersist: FilePersist,
              public jobData: JobData,
              public tcAsyncJob: TCAsyncJob,
              public tcNavigator: TCNavigator,
              public tcNotification: TCNotification) {

  }

  searchJob(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<JobSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/jobs", this.jobSearchText, pageSize, pageIndex, sort, order);
    if (this.jobStateFilter) {
      url = TCUtilsString.appendUrlParameter(url, "job_state", this.jobStateFilter);
    }
    return this.http.get<JobSummaryPartialList>(url);

  }

  jobDashboard(): Observable<JobDashboard> {
    return this.http.get<JobDashboard>(environment.tcApiBaseUri + "tc/jobs/dashboard");
  }

  getJob(id: string): Observable<JobDetail> {
    return this.http.get<JobDetail>(environment.tcApiBaseUri + "tc/jobs/" + id);
  }

  addJob(item: JobDetail): Observable<TCId> {
    return this.http.post<JobDetail>(environment.tcApiBaseUri + "tc/jobs/", item);
  }

  updateJob(item: JobDetail): Observable<JobDetail> {
    return this.http.patch<JobDetail>(environment.tcApiBaseUri + "tc/jobs/" + item.id, item);
  }

  deleteJob(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "tc/jobs/" + id);
  }

  jobsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/jobs/do", new TCDoParam(method, payload));
  }

  jobDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/jobs/" + id + "/do", new TCDoParam(method, payload));
  }

  jobState(id: string): Observable<JobState> {
    return this.http.get<JobState>(environment.tcApiBaseUri + "tc/jobs/" + id + "/state");
  }

  downloadFileKey(job_id: string): Observable<JobFile> {
    return this.http.get<JobFile>(environment.tcApiBaseUri + "tc/jobs/" + job_id + "/file");
  }

  unfollowJob(id: string, state: JobState): void {
    let async_job: AsyncJob = this.tcUtilsArray.getById(this.jobData.unfinishedJobs, id);
    async_job.is_terminated = true;
    async_job.subscription.unsubscribe();
    this.tcUtilsArray.removeItem(this.jobData.unfinishedJobs, async_job);
    //console.log("job terminated. unfollowing : " + id);
  }

  abortJob(id: string): void {
    let abortedState: JobState = new JobState();
    abortedState.job_state = this.aborted_state;
    this.unfollowJob(id, abortedState);
  }


  followJob(id: string, descrption: string, is_download_job: boolean = true, onSuccess: Function = null, onFailure: Function = null): AsyncJob {
    if (this.tcUtilsArray.containsId(this.jobData.unfinishedJobs, id)) {
      return;
    }


    let jb: AsyncJob = new AsyncJob();
    jb.id = id;
    jb.is_terminated = false;
    jb.is_download_job = is_download_job;
    jb.title = descrption;

    this.jobData.unfinishedJobs.push(jb);

    if (this.jobData.unfinishedJobs.length > 0) {
      this.tcAsyncJob.show();
    }

    const attemptsCounter = interval(3000); //every 3 second
    jb.subscription = attemptsCounter.subscribe(n => {
      //console.log("checking: " + id);
      this.jobState(id).subscribe((jobstate: JobState) => {

        if (jobstate.job_state == this.success_state) {
          this.unfollowJob(id, jobstate);
          //handle download
          if (is_download_job) {
            this.downloadFileKey(id).subscribe(jobFile => {
              this.tcNavigator.openUrl(this.filePersist.downloadLink(jobFile.download_key), true)
            });
          } else {
            onSuccess();
          }
        } else if (jobstate.job_state == this.failed_state) {
          this.unfollowJob(id, jobstate);
          this.tcNotification.error(jobstate.message);
          onFailure();
        }
      });
    });

    return jb;

  }

}
