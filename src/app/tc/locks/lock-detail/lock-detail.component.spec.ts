import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LockDetailComponent } from './lock-detail.component';

describe('LockDetailComponent', () => {
  let component: LockDetailComponent;
  let fixture: ComponentFixture<LockDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LockDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
