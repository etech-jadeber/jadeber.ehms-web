import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {LockDetail} from "../lock.model";
import {LockPersist} from "../lock.persist";
import {LockNavigator} from "../lock.navigator";


export enum LockTabs {
  overview,
}

@Component({
  selector: 'app-lock-detail',
  templateUrl: './lock-detail.component.html',
  styleUrls: ['./lock-detail.component.css']
})
export class LockDetailComponent implements OnInit {

  //basics
  lockDetail: LockDetail;
  lockTabs: typeof LockTabs = LockTabs;
  activeTab: LockTabs = LockTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public lockNavigator: LockNavigator,
              public  persist: LockPersist) {
    this.tcAuthorization.requireRead("locks");
    this.lockDetail = new LockDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getLock(id.toString()).subscribe(lockDetail => {
      this.lockDetail = lockDetail;
    }, error => {
    });
  }

  editLock(): void {
    let modalRef = this.lockNavigator.editLock(this.lockDetail.id);
    modalRef.afterClosed().subscribe(modifiedLockDetail => {
      TCUtilsAngular.assign(this.lockDetail, modifiedLockDetail);
    }, error => {
      console.error(error);
    });
  }


}
