import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LockEditComponent } from './lock-edit.component';

describe('LockEditComponent', () => {
  let component: LockEditComponent;
  let fixture: ComponentFixture<LockEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LockEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
