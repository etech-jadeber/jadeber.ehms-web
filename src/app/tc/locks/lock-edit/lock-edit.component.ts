import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";


import {LockDetail} from "../lock.model";
import {LockPersist} from "../lock.persist";


@Component({
  selector: 'app-lock-edit',
  templateUrl: './lock-edit.component.html',
  styleUrls: ['./lock-edit.component.css']
})
export class LockEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: LockDetail = new LockDetail();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<LockEditComponent>,
              public  persist: LockPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
     this.tcAuthorization.requireCreate("locks");
      this.title = this.appTranslation.getText("general","new") +  " Lock";
      this.isNew = true;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("locks");
      this.title = this.appTranslation.getText("general","edit") +  " Lock";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getLock(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addLock(this.item).subscribe(value => {
      this.tcNotification.success("Lock added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateLock(this.item).subscribe(value => {
      this.tcNotification.success("Lock updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
