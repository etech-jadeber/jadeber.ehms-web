import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {LockPersist} from "../lock.persist";
import {LockNavigator} from "../lock.navigator";
import {LockDetail, LockSummary, LockSummaryPartialList} from "../lock.model";

export enum LockListTabs {
  overview
}

@Component({
  selector: 'app-lock-list',
  templateUrl: './lock-list.component.html',
  styleUrls: ['./lock-list.component.css']
})
export class LockListComponent implements OnInit {
  activeTab: LockListTabs = LockListTabs.overview;
  tabs: typeof LockListTabs = LockListTabs;

  locksData: LockSummary[] = [];
  locksTotalCount: number = 0;
  locksSelection: LockSummary[] = [];
  locksDisplayedColumns: string[] = ["select", "action","link", "lock_type_id","target_id","lock_value","lock_time"];

  locksSearchTextBox: FormControl = new FormControl();
  locksIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) locksPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) locksSort: MatSort;

  constructor(private router: Router,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public lockPersist: LockPersist,
                public lockNavigator: LockNavigator
    ) {

        this.tcAuthorization.requireRead("locks");
      //delay subsequent keyup events
      this.locksSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.lockPersist.lockSearchText = value;
        this.searchLocks();
      });
    }

    ngOnInit() {

      this.locksSort.sortChange.subscribe(() => {
        this.locksPaginator.pageIndex = 0;
        this.searchLocks();
      });

      this.locksPaginator.page.subscribe(() => {
        this.searchLocks();
      });
      //start by loading items
      this.searchLocks();
    }

  searchLocks(): void {
    this.locksIsLoading = true;
    this.locksSelection = [];

    this.lockPersist.searchLock(this.locksPaginator.pageSize, this.locksPaginator.pageIndex, this.locksSort.active, this.locksSort.direction).subscribe((partialList: LockSummaryPartialList) => {
      this.locksData = partialList.data;
      if (partialList.total != -1) {
        this.locksTotalCount = partialList.total;
      }
      this.locksIsLoading = false;
    }, error => {
      this.locksIsLoading = false;
    });

  }

  downloadLocks(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addLock(): void {
    let dialogRef = this.lockNavigator.addLock();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchLocks();
      }
    });
  }

  editLock(item: LockSummary) {
    let dialogRef = this.lockNavigator.editLock(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteLock(item: LockSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Lock");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.lockPersist.deleteLock(item.id).subscribe(response => {
          this.tcNotification.success("Lock deleted");
          this.searchLocks();
        }, error => {
        });
      }

    });
  }

}
