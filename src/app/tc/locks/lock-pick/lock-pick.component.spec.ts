import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LockPickComponent } from './lock-pick.component';

describe('LockPickComponent', () => {
  let component: LockPickComponent;
  let fixture: ComponentFixture<LockPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ LockPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LockPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
