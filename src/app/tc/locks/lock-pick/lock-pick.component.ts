import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {LockDetail, LockSummary, LockSummaryPartialList} from "../lock.model";
import {LockPersist} from "../lock.persist";


@Component({
  selector: 'app-lock-pick',
  templateUrl: './lock-pick.component.html',
  styleUrls: ['./lock-pick.component.css']
})
export class LockPickComponent implements OnInit {

  locksData: LockSummary[] = [];
  locksTotalCount: number = 0;
  locksSelection: LockSummary[] = [];
  locksDisplayedColumns: string[] = ["select", 'lock_type_id','target_id','lock_value','lock_time' ];

  locksSearchTextBox: FormControl = new FormControl();
  locksIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) locksPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) locksSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public lockPersist: LockPersist,
              public dialogRef: MatDialogRef<LockPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("locks");
    //delay subsequent keyup events
    this.locksSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.lockPersist.lockSearchText = value;
      this.searchLocks();
    });
  }

  ngOnInit() {

    this.locksSort.sortChange.subscribe(() => {
      this.locksPaginator.pageIndex = 0;
      this.searchLocks();
    });

    this.locksPaginator.page.subscribe(() => {
      this.searchLocks();
    });
    //start by loading items
    this.searchLocks();
  }

  searchLocks(): void {
    this.locksIsLoading = true;
    this.locksSelection = [];

    this.lockPersist.searchLock(this.locksPaginator.pageSize,
        this.locksPaginator.pageIndex,
        this.locksSort.active,
        this.locksSort.direction).subscribe((partialList: LockSummaryPartialList) => {
      this.locksData = partialList.data;
      if (partialList.total != -1) {
        this.locksTotalCount = partialList.total;
      }
      this.locksIsLoading = false;
    }, error => {
      this.locksIsLoading = false;
    });

  }

  markOneItem(item: LockSummary) {
    this.locksSelection = [];
    this.locksSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.locksSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
