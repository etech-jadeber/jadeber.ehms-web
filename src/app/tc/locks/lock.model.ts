import {TCId} from "../../tc/models";

export class LockSummary extends TCId {
  lock_type_id : number;
target_id : string;
lock_value : string;
lock_time : number;
}

export class LockSummaryPartialList {
  data: LockSummary[];
  total: number;
}

export class LockDetail extends LockSummary {
  lock_type_id : number;
target_id : string;
lock_value : string;
lock_time : number;
}

export class LockDashboard {
  total: number;
}
