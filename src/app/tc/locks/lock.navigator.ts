import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {LockEditComponent} from "./lock-edit/lock-edit.component";
import {LockPickComponent} from "./lock-pick/lock-pick.component";


@Injectable({
  providedIn: 'root'
})

export class LockNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  locksUrl(): string {
    return "/locks";
  }

  lockUrl(id: string): string {
    return "/locks/" + id;
  }

  viewLocks(): void {
    this.router.navigateByUrl(this.locksUrl());
  }

  viewLock(id: string): void {
    this.router.navigateByUrl(this.lockUrl(id));
  }

  editLock(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(LockEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addLock(): MatDialogRef<unknown,any> {
    return this.editLock(null);
  }
  
   pickLocks(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(LockPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
