import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {LockDashboard, LockDetail, LockSummaryPartialList} from "./lock.model";


@Injectable({
  providedIn: 'root'
})
export class LockPersist {

  lockSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchLock(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<LockSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/locks", this.lockSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<LockSummaryPartialList>(url);

  }

  lockDashboard(): Observable<LockDashboard> {
    return this.http.get<LockDashboard>(environment.tcApiBaseUri + "tc/locks/dashboard");
  }

  getLock(id: string): Observable<LockDetail> {
    return this.http.get<LockDetail>(environment.tcApiBaseUri + "tc/locks/" + id);
  }

  addLock(item: LockDetail): Observable<TCId> {
    return this.http.post<LockDetail>(environment.tcApiBaseUri + "tc/locks/", item);
  }

  updateLock(item: LockDetail): Observable<LockDetail> {
    return this.http.patch<LockDetail>(environment.tcApiBaseUri + "tc/locks/" + item.id, item);
  }

  deleteLock(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/locks/" + id);
  }

  locksDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/locks/do", new TCDoParam(method, payload));
  }

  lockDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/locks/" + id + "/do", new TCDoParam(method, payload));
  }


}
