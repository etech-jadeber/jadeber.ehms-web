import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {MailDetail} from "../mail.model";
import {MailPersist} from "../mail.persist";
import {MailNavigator} from "../mail.navigator";


export enum MailTabs {
  overview,
}

@Component({
  selector: 'app-mail-detail',
  templateUrl: './mail-detail.component.html',
  styleUrls: ['./mail-detail.component.css']
})
export class MailDetailComponent implements OnInit {

  //basics
  mailDetail: MailDetail;
  mailTabs: typeof MailTabs = MailTabs;
  activeTab: MailTabs = MailTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public mailNavigator: MailNavigator,
              public  persist: MailPersist) {
    this.tcAuthorization.requireRead("mails");
    this.mailDetail = new MailDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getMail(id.toString()).subscribe(mailDetail => {
      this.mailDetail = mailDetail;
    }, error => {
    });
  }

  editMail(): void {
    let modalRef = this.mailNavigator.editMail(this.mailDetail.id);
    modalRef.afterClosed().subscribe(modifiedMailDetail => {
      TCUtilsAngular.assign(this.mailDetail, modifiedMailDetail);
    }, error => {
      console.error(error);
    });
  }


}
