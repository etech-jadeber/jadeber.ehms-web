import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";


import {MailDetail} from "../mail.model";
import {MailPersist} from "../mail.persist";


@Component({
  selector: 'app-mail-edit',
  templateUrl: './mail-edit.component.html',
  styleUrls: ['./mail-edit.component.css']
})
export class MailEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: MailDetail = new MailDetail();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<MailEditComponent>,
              public  persist: MailPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
     this.tcAuthorization.requireCreate("mails");
      this.title = this.appTranslation.getText("general","new") +  " Mail";
      this.isNew = true;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("mails");
      this.title = this.appTranslation.getText("general","edit") +  " Mail";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getMail(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addMail(this.item).subscribe(value => {
      this.tcNotification.success("Mail added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateMail(this.item).subscribe(value => {
      this.tcNotification.success("Mail updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
