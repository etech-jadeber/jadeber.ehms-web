import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {MailPersist} from "../mail.persist";
import {MailNavigator} from "../mail.navigator";
import {MailDetail, MailSummary, MailSummaryPartialList} from "../mail.model";

export enum MailListTabs {
  overview
}

@Component({
  selector: 'app-mail-list',
  templateUrl: './mail-list.component.html',
  styleUrls: ['./mail-list.component.css']
})
export class MailListComponent implements OnInit {
  activeTab: MailListTabs = MailListTabs.overview;
  tabs: typeof MailListTabs = MailListTabs;

  mailsData: MailSummary[] = [];
  mailsTotalCount: number = 0;
  mailsSelection: MailSummary[] = [];
  mailsDisplayedColumns: string[] = ["select", "action","link", "subject","sender_id","sender_email","recipient_id","recipient_email","is_html","has_attachments","submission_time","sending_time","is_sent","is_failed"];

  mailsSearchTextBox: FormControl = new FormControl();
  mailsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) mailsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) mailsSort: MatSort;

  constructor(private router: Router,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public mailPersist: MailPersist,
                public mailNavigator: MailNavigator
    ) {

        this.tcAuthorization.requireRead("mails");
      //delay subsequent keyup events
      this.mailsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.mailPersist.mailSearchText = value;
        this.searchMails();
      });
    }

    ngOnInit() {

      this.mailsSort.sortChange.subscribe(() => {
        this.mailsPaginator.pageIndex = 0;
        this.searchMails();
      });

      this.mailsPaginator.page.subscribe(() => {
        this.searchMails();
      });
      //start by loading items
      this.searchMails();
    }

  searchMails(): void {
    this.mailsIsLoading = true;
    this.mailsSelection = [];

    this.mailPersist.searchMail(this.mailsPaginator.pageSize, this.mailsPaginator.pageIndex, this.mailsSort.active, this.mailsSort.direction).subscribe((partialList: MailSummaryPartialList) => {
      this.mailsData = partialList.data;
      if (partialList.total != -1) {
        this.mailsTotalCount = partialList.total;
      }
      this.mailsIsLoading = false;
    }, error => {
      this.mailsIsLoading = false;
    });

  }

  downloadMails(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addMail(): void {
    let dialogRef = this.mailNavigator.addMail();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchMails();
      }
    });
  }

  editMail(item: MailSummary) {
    let dialogRef = this.mailNavigator.editMail(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteMail(item: MailSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Mail");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.mailPersist.deleteMail(item.id).subscribe(response => {
          this.tcNotification.success("Mail deleted");
          this.searchMails();
        }, error => {
        });
      }

    });
  }

}
