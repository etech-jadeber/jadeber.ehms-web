import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MailPickComponent } from './mail-pick.component';

describe('MailPickComponent', () => {
  let component: MailPickComponent;
  let fixture: ComponentFixture<MailPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MailPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
