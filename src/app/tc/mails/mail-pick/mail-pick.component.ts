import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {MailDetail, MailSummary, MailSummaryPartialList} from "../mail.model";
import {MailPersist} from "../mail.persist";


@Component({
  selector: 'app-mail-pick',
  templateUrl: './mail-pick.component.html',
  styleUrls: ['./mail-pick.component.css']
})
export class MailPickComponent implements OnInit {

  mailsData: MailSummary[] = [];
  mailsTotalCount: number = 0;
  mailsSelection: MailSummary[] = [];
  mailsDisplayedColumns: string[] = ["select", 'subject','sender_id','sender_email','recipient_id','recipient_email','is_html','has_attachments','submission_time','sending_time','is_sent','is_failed' ];

  mailsSearchTextBox: FormControl = new FormControl();
  mailsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) mailsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) mailsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public mailPersist: MailPersist,
              public dialogRef: MatDialogRef<MailPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("mails");
    //delay subsequent keyup events
    this.mailsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.mailPersist.mailSearchText = value;
      this.searchMails();
    });
  }

  ngOnInit() {

    this.mailsSort.sortChange.subscribe(() => {
      this.mailsPaginator.pageIndex = 0;
      this.searchMails();
    });

    this.mailsPaginator.page.subscribe(() => {
      this.searchMails();
    });
    //start by loading items
    this.searchMails();
  }

  searchMails(): void {
    this.mailsIsLoading = true;
    this.mailsSelection = [];

    this.mailPersist.searchMail(this.mailsPaginator.pageSize,
        this.mailsPaginator.pageIndex,
        this.mailsSort.active,
        this.mailsSort.direction).subscribe((partialList: MailSummaryPartialList) => {
      this.mailsData = partialList.data;
      if (partialList.total != -1) {
        this.mailsTotalCount = partialList.total;
      }
      this.mailsIsLoading = false;
    }, error => {
      this.mailsIsLoading = false;
    });

  }

  markOneItem(item: MailSummary) {
    this.mailsSelection = [];
    this.mailsSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.mailsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
