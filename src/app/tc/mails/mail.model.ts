import {TCId} from "../../tc/models";

export class MailSummary extends TCId {
  subject : string;
sender_id : string;
sender_email : string;
recipient_id : string;
recipient_email : string;
is_html : boolean;
has_attachments : boolean;
submission_time : number;
sending_time : number;
is_sent : boolean;
is_failed : boolean;
}

export class MailSummaryPartialList {
  data: MailSummary[];
  total: number;
}

export class MailDetail extends MailSummary {
  subject : string;
sender_id : string;
sender_email : string;
recipient_id : string;
recipient_email : string;
is_html : boolean;
has_attachments : boolean;
submission_time : number;
sending_time : number;
is_sent : boolean;
is_failed : boolean;
}

export class MailDashboard {
  total: number;
}
