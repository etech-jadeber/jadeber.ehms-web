import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {MailEditComponent} from "./mail-edit/mail-edit.component";
import {MailPickComponent} from "./mail-pick/mail-pick.component";


@Injectable({
  providedIn: 'root'
})

export class MailNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  mailsUrl(): string {
    return "/mails";
  }

  mailUrl(id: string): string {
    return "/mails/" + id;
  }

  viewMails(): void {
    this.router.navigateByUrl(this.mailsUrl());
  }

  viewMail(id: string): void {
    this.router.navigateByUrl(this.mailUrl(id));
  }

  editMail(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(MailEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addMail(): MatDialogRef<unknown,any> {
    return this.editMail(null);
  }
  
   pickMails(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(MailPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
