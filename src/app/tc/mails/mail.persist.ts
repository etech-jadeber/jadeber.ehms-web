import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {MailDashboard, MailDetail, MailSummaryPartialList} from "./mail.model";


@Injectable({
  providedIn: 'root'
})
export class MailPersist {

  mailSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchMail(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<MailSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/mails", this.mailSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<MailSummaryPartialList>(url);

  }

  mailDashboard(): Observable<MailDashboard> {
    return this.http.get<MailDashboard>(environment.tcApiBaseUri + "tc/mails/dashboard");
  }

  getMail(id: string): Observable<MailDetail> {
    return this.http.get<MailDetail>(environment.tcApiBaseUri + "tc/mails/" + id);
  }

  addMail(item: MailDetail): Observable<TCId> {
    return this.http.post<MailDetail>(environment.tcApiBaseUri + "tc/mails/", item);
  }

  updateMail(item: MailDetail): Observable<MailDetail> {
    return this.http.patch<MailDetail>(environment.tcApiBaseUri + "tc/mails/" + item.id, item);
  }

  deleteMail(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/mails/" + id);
  }

  mailsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/mails/do", new TCDoParam(method, payload));
  }

  mailDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/mails/" + id + "/do", new TCDoParam(method, payload));
  }


}
