import { Component, Input } from '@angular/core';
import { MenuParentComponent } from '../menu-parent/menu-parent.component';
import { slideUpDownAnimation } from './menu-child.animation';

@Component({
  selector: 'app-menu-child',
  templateUrl: 'menu-child.component.html',
  animations: [slideUpDownAnimation]
})
export class MenuChildComponent {
  
  @Input() parent?: MenuParentComponent;

}
