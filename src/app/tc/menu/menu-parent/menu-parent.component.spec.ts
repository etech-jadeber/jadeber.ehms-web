import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MenuParentComponent } from './menu-parent.component';

describe('MenuParentComponent', () => {
  let component: MenuParentComponent;
  let fixture: ComponentFixture<MenuParentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
