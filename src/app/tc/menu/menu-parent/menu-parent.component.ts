import { Component, HostListener } from '@angular/core';
import { indicatorRotateAnimation } from './menu-parent.animation';

@Component({
  selector: 'app-menu-parent',
  templateUrl: 'menu-parent.component.html',
  animations: [indicatorRotateAnimation]
})
export class MenuParentComponent {
  
  expanded: boolean = false;

  @HostListener('click')
  onClick() {
    this.expanded = !this.expanded;
  }

}
