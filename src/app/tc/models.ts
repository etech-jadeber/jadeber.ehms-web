//tc generic models


export class TCEnum {
  id: number;
  name: string;

  constructor(idVal?: number, nameVal?: string) {
    if (idVal) {
      this.id = idVal;
    }
    if (nameVal) {
      this.name = nameVal;
    }
  }

}

export class TCString {
  id: string;
  name: string;

  constructor(idVal?: string, nameVal?: string) {
    if (idVal) {
      this.id = idVal;
    }
    if (nameVal) {
      this.name = nameVal;
    }
  }

}

export class TCEnumTranslation extends TCEnum {
}

export class TCId {
  id: string;

  constructor(idVal?: string) {
    if (idVal) {
      this.id = idVal;
    }
  }
}

export class TCIdPartialList {
  data: TCId[];
  total: number;
}

export class TCIdName extends TCId {
  name: string;
}

export class TCIdNameValue extends TCIdName {
  value: number;
}

export class TCUrl {
  url: string;
}


export class TCIdWithContext {
  constructor(public id: string, public context: TcDictionary<any>) {

  }
}

export class TCIdMode {
  id: string;
  mode: string;

  constructor(idVal?: string, modeVal?: string) {
    if (idVal) {
      this.id = idVal;
    }
    if (modeVal) {
      this.mode = modeVal;
    }
  }

}

export class TCObjMode {
  obj: any;
  mode: string;

  constructor(objVal?: any, modeVal?: string) {
    if (objVal) {
      this.obj = objVal;
    }
    if (modeVal) {
      this.mode = modeVal;
    }
  }

}

export class TCModalModes {
  static NEW: string = "new";
  static EDIT: string = "edit";
  static CASE: string = "case";
  static WIZARD:string = "wizard";
}

export class TCWizardProgressModes {
  static NEXT: string = "next";
  static SKIP: string = "skip";
  static FINISH: string = "finish";
}

export class TCMandatoryTypes {
  static tos_firsttime: number = 1;
  static tos_updated: number = 2;
  static force_pwd_change: number = 3;
}

export class TCWizardProgress {
  constructor(public progressMode: string, public item: any) {

  }
}

export class TCIdNameDescription extends TCIdName {
  description: string;
}

export class TCCollection extends TCIdName {
  items_count: number;
}

export class TCParentChildIds {
  constructor(public parentId: string, public childId: string, public context: TcDictionary<any> = null,
    // public bloodExchangeId:string
    ) {

  }
}

export class TcDictionary<T> {
  [K: string]: T;

  static buildContext(obj: Object): TcDictionary<any> {
    let ret = new TcDictionary<any>();
    for (let attribute in obj) {
      ret[attribute.toString()] = obj[attribute];
    }
    return ret;

  }

}


export class CommentSummary extends TCId {
  comment_type: number;
  target_id: string;
  comment_text: string;
  commented_by: string;
  is_archived: boolean;
  comment_time: number;
}

export class CommentSummaryPartialList {
  data: CommentSummary[];
  total: number;
}

export class CommentDetail extends CommentSummary {
}

export class CountByType {
  type: number;
  count: number;
}

export class NoticeDetail extends TCId {
  notice_type: number;
  notice_text: string;
  submission_time: number;
  user_id: string;
  is_read: boolean;
  is_archived: boolean;
  target_id: string;
}

export class TelegramUserStates {
  static registered: number = 1;
  static active: number = 2;
  static disabled: number = 3;
  static filled: number = 4;
}

