import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {TCAuthentication} from "./authentication";

import {ConfirmComponent} from "./confirm/confirm.component";
import {TCModalWidths} from "./utils-angular";
import {TextinputComponent} from "./textinput/textinput.component";
import {PasswordChangeComponent} from "../profile/password-change/password-change.component";
import {AppTranslation} from "../app.translation";
import {DateinputComponent} from "./dateinput/dateinput.component";
import {TCUtilsDate} from "./utils-date";
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TextDateComponent } from './text-date/text-date.component';
import { NumberInputComponent } from './numberinput/numberinput.component';

@Injectable({
  providedIn: 'root',
})

export class TCNavigator {

  constructor(private router: Router,
              private tcAuthentication: TCAuthentication,
              public appTranslation: AppTranslation,
              public tcUtilsDate: TCUtilsDate,
              public dialog: MatDialog) {

  }

  home(): void {
    this.router.navigateByUrl('');
  }

  login(): void {
    let loginUrl: string = environment.tcApiBaseUri + "authorize?response_type=token&client_id=" + environment.clientId + "&redirect_uri=" + environment.loginCallback + "&scope=user";
    window.location.href = loginUrl;
  }

  openUrl(url: string, new_window: boolean = false): void {
    if (new_window) {
      window.open(url, "_blank");
    } else {
      window.location.href = url;
    }
  }

  logout(): void {
    this.tcAuthentication.logout();
    this.home();
  }

  about(): void {
    this.router.navigateByUrl('/about');
  }

  terms(): void {
    this.router.navigateByUrl('/tos');
  }

  mandatory(): void {
    this.router.navigateByUrl('/mandatory');
  }

  profile(): void {
    this.router.navigateByUrl('/profile');
  }


  dashboard(): void {
    this.router.navigateByUrl('/dashboard');
  }

  notFound(): void {
    this.router.navigateByUrl('/not-found');
  }

  accessDenied(): void {
    this.router.navigateByUrl('/access-denied');
  }

  invalidOperation(): void {
    this.router.navigateByUrl('/invalid');
  }

  passwordChange(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(PasswordChangeComponent, {
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  confirmAction(action: string, itemType: string, description: string, cancelLabel: string, wdt = TCModalWidths.small, actionReturn: any = true, cancelReturn :any = null ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: {action: action, itemType: itemType, description: description, cancelLabel: cancelLabel, actionReturn, cancelReturn},
      width: wdt
    });

    return dialogRef;
  }

  confirmDeletion(itemType: string): MatDialogRef<unknown,any> {
    return this.confirmAction("Delete", itemType, "The " + itemType + " will be permanently removed. It is not possible to recover after deletion.", "Cancel");
  }

  textInput(prompt: string,
            currentValue: string = null,
            multiLine: boolean = false,
            actionLabel: string = "save",
            cancelLabel: string = "Cancel",
            guidelines: string[] = [],
            validationPattern: string = "",
            editor: boolean = false,width:TCModalWidths = null, minHeight: string = null, panelClass = null, rows: number = 10): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(TextinputComponent, {
      data: {
        prompt: prompt,
        currentValue: currentValue,
        multiLine: multiLine,
        action: actionLabel,
        cancelLabel: cancelLabel,
        guidelines: guidelines,
        validationPattern: validationPattern,
        editor,
        minHeight,
        rows
      },
      minHeight: minHeight || 'auto',
      width: width ? width : (multiLine ? TCModalWidths.large : TCModalWidths.medium),
      panelClass: panelClass
    });

    return dialogRef;
  }

  numberInput(prompt: string,
    currentValue: number = null,
    actionLabel: string = "save",
    cancelLabel: string = "Cancel",
    guidelines: string[] = [],
    validationPattern: string = "",
    title: string = "",
    ): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(NumberInputComponent, {
    data: {
    prompt: prompt,
    currentValue: currentValue,
    action: actionLabel,
    cancelLabel: cancelLabel,
    guidelines: guidelines,
    title: title,
    validationPattern: validationPattern
    },
    width:TCModalWidths.medium
  });

return dialogRef;
}

  dateTextInput(
    prompt: string,
    current_text_value: string = null,
    multiLine: boolean= false,
    current_date_value: number  =  this.tcUtilsDate.toTimeStamp(this.tcUtilsDate.now()),
    actionLabel: string = "save",
    cancelLabel: string = "Cancel",
    guidelines: string[] = [],
    validationPattern: string = ""): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(TextDateComponent, {
    data: {
      prompt: prompt,
      current_text_value: current_text_value,
      multiLine: multiLine,
      current_date_value: current_date_value,
      cancelLabel: cancelLabel,
      action: actionLabel,
      guidelines: guidelines,
      validationPattern: validationPattern
    },
    width: multiLine ? TCModalWidths.large : TCModalWidths.medium
    });

    return dialogRef;
    }

  dateInput(prompt: string,
            currentValue: number = this.tcUtilsDate.toTimeStamp(this.tcUtilsDate.now()),
            actionLabel: string = "save",
            cancelLabel: string = "Cancel", is_range: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(DateinputComponent, {
      data: {
        prompt: prompt,
        currentValue: currentValue,
        action: actionLabel,
        cancelLabel: cancelLabel,
        is_range
      },
      width: TCModalWidths.medium
    });

    return dialogRef;
  }
}
