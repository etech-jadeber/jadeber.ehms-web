import {Injectable} from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';


import {
  ValidationMessagesComponent,
  ValidationMessagesService
} from "./validation-messages/validation-messages.component";
import { QueueBarService } from 'ngx-queue-bar';

@Injectable({
  providedIn: 'root',
})

export class TCNotification {

  constructor(public snackBar: MatSnackBar, public validationMessagesService: ValidationMessagesService, public queueBar: QueueBarService) {

  }

  message(msg: string, ackLabel: string, panelClass: string, duration: number) {
    let config: MatSnackBarConfig = new MatSnackBarConfig();
    // if (panelClass != "") {
    //   config.panelClass = panelClass;
    // }
    if (duration > 0) {
      config.duration = duration;
    }else {
      config.duration = 0;
    }
    const ref = this.queueBar.open(msg, ackLabel, config);
    panelClass && ref.container.location.nativeElement.classList.add(panelClass)
  }

  info(msg: string, requireConfirmation: boolean = false) {
    this.message(msg, "ok", "", requireConfirmation ? -1 : 3000);
  }

  infos(msg: string[], requireConfirmation: boolean = false) {
    for (let m of msg){
      this.info(m, requireConfirmation);
    }
  }

  success(msg: string, requireConfirmation: boolean = false) {
    this.message(msg, requireConfirmation ? "ok" : "", "tc-panel-success", requireConfirmation ? -1 : 3000);
  }

  error(msg: string) {
    this.message(msg, "ok", "tc-panel-danger", -1);
  }

  validationErrors(messages: string[]) {
    for (let message of messages){
      this.error(message)
    }
    // this.validationMessagesService.msgs = messages;
    // this.snackBar.openFromComponent(ValidationMessagesComponent, {panelClass: "tc-panel-danger"});
  }

}


