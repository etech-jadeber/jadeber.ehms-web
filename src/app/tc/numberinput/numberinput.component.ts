import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export class NumberInputParameter {
  currentValue: number;
  prompt: string;
  cancelLabel: string;
  action: string;
  guidelines: string[] = [];
  validationPattern: string = "";
}

@Component({
  selector: 'app-numberinput',
  templateUrl: './numberinput.component.html',
  styleUrls: ['./numberinput.component.scss']
})
export class NumberInputComponent implements OnInit {


  num: number;

  constructor(public dialogRef: MatDialogRef<NumberInputParameter>,
              @Inject(MAT_DIALOG_DATA) public data: NumberInputParameter) {
    this.num = data.currentValue ;
  }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onOk(): void {
    this.dialogRef.close(this.num);
  }

  canSubmit(): boolean {
    if (this.num == null || this.num < 0) {
      return false;
    } 
    // else if (this.data.validationPattern != "") {
    //   return RegExp(this.data.validationPattern).test(this.num);
    // }
    return true;
  }

}
