import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";


import {ResourceDetail, TcaclDetail} from "../resource.model";
import {ResourcePersist} from "../resource.persist";
import {ResourceNavigator} from "../resource.navigator";
import {TCUtilsArray} from "../../utils-array";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {TCUtilsAngular} from "../../utils-angular";
import {AppTranslation} from "../../../app.translation";
import {FormControl} from "@angular/forms";
import {UserPersist} from "../../users/user.persist";
import {GroupPersist} from "../../groups/group.persist";
import {UserDetail} from "../../users/user.model";
import {GroupDetail} from "../../groups/group.model";
import {TCUtilsString} from "../../utils-string";
import {UserNavigator} from "../../users/user.navigator";
import {GroupNavigator} from "../../groups/group.navigator";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';


export enum ResourceTabs {
  overview, tcacls
}

@Component({
  selector: 'app-resource-detail',
  templateUrl: './resource-detail.component.html',
  styleUrls: ['./resource-detail.component.css']
})
export class ResourceDetailComponent implements OnInit {

  //basics
  resourceDetail: ResourceDetail;
  resourceTabs: typeof ResourceTabs = ResourceTabs;
  activeTab: ResourceTabs = ResourceTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  //tcacls
  tcaclsColumns: string[] = ['select', 'action', 'uri', 'target', 'acl'];
  tcaclsDs: MatTableDataSource<TcaclDetail>;
  tcaclsSelected: TcaclDetail[] = [];
  tcaclsFilter: FormControl = new FormControl();
  users: UserDetail[] = [UserDetail.InvalidUser()];
  groups: GroupDetail[] = [GroupDetail.InvalidGroup()];

  constructor(private route: ActivatedRoute,
              public tcUtilsString: TCUtilsString,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public userPersist: UserPersist,
              public userNavigator: UserNavigator,
              public groupPersist: GroupPersist,
              public groupNavigator: GroupNavigator,
              public appTranslation: AppTranslation,
              public resourceNavigator: ResourceNavigator,
              public  persist: ResourcePersist) {
    this.resourceDetail = new ResourceDetail();
    this.resourceDetail.tcacls = [];

//tcacls filter
    this.tcaclsFilter.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.tcaclsDs.filter = value.trim().toLowerCase();
      if (this.tcaclsDs.paginator) {
        this.tcaclsDs.paginator.firstPage();
      }
    });
  }

  reload() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getResource(id.toString()).subscribe(resourceDetail => {
      this.resourceDetail = resourceDetail;
      //tcacls data source
      this.bindTcaclsData();
      //fetch users and groups
      for (let acl of this.resourceDetail.tcacls) {
        if (!this.tcUtilsArray.containsId(this.users, acl.user_id)) {
          this.userPersist.getUser(acl.user_id).subscribe(user => {
            this.users.push(user);
          });
        }
        if (!this.tcUtilsArray.containsId(this.groups, acl.group_id)) {
          this.groupPersist.getGroup(acl.group_id).subscribe(group => {
            this.groups.push(group);
          });
        }
      }

    }, error => {
    });
  }

  ngOnInit() {
    this.reload();
  }

  editResource(): void {
    let modalRef = this.resourceNavigator.editResource(this.resourceDetail.id);
    modalRef.afterClosed().subscribe(modifiedResourceDetail => {
      this.reload();
    }, error => {
      console.error(error);
    });
  }

  editTcacl(item: TcaclDetail): void {
    let dialogRef = this.resourceNavigator.editTcacl(this.resourceDetail.id, item.id);
    dialogRef.afterClosed().subscribe(updatedTcacl => {
      if (updatedTcacl) {
        this.reload();
      }
    });
  }

  addUserAcl(): void {
    let dialogRef = this.userNavigator.pickUsers(true);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let pickedUsers: UserDetail[] = result;
        let dialogRef = this.resourceNavigator.addTcacl(this.resourceDetail.id, pickedUsers[0].id, TCUtilsString.getInvalidId());
        dialogRef.afterClosed().subscribe(newTcacl => {
          if (newTcacl) {
            this.reload();
          }
        });
      }
    });
  }

  addGroupAcl(): void {
    let dialogRef = this.groupNavigator.pickGroups(true);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let pickedGroups: GroupDetail[] = result;
        let dialogRef = this.resourceNavigator.addTcacl(this.resourceDetail.id, TCUtilsString.getInvalidId(), pickedGroups[0].id);
        dialogRef.afterClosed().subscribe(newTcacl => {
          if (newTcacl) {
            this.reload();
          }
        });
      }
    });
  }


  deleteTcacl(item: TcaclDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Tcacl");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.persist.deleteTcacl(this.resourceDetail.id, item.id).subscribe(response => {
          this.tcNotification.success("tcacl deleted");
          this.reload();
        }, error => {
        });
      }

    });
  }

  downloadTcacls(): void {
    this.tcNotification.info("Download tcacls : " + this.tcaclsSelected.length);
  }

  bindTcaclsData(): void {
    this.tcaclsDs = new MatTableDataSource(this.resourceDetail.tcacls);
    //caution: use proper index for paginator & sorter if not the first table added to component
    this.tcaclsDs.sort = this.sorters.toArray()[0];
    this.tcaclsDs.paginator = this.paginators.toArray()[0];
  }

  permissions(tcaclDetail: TcaclDetail): string {
    let ret: string = "";
    ret += tcaclDetail.can_create ? "C" : "-";
    ret += tcaclDetail.can_read ? "R" : "-";
    ret += tcaclDetail.can_update ? "U" : "-";
    ret += tcaclDetail.can_delete ? "D" : "-";
    return ret;
  }

  isInvalidId(id: string): boolean {
    return TCUtilsString.isInvalidId(id);
  }


}
