import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";




import {ResourceDetail} from "../resource.model";
import {ResourcePersist} from "../resource.persist";
import {TCNotification} from "../../notification";
import {AppTranslation} from "../../../app.translation";


@Component({
  selector: 'app-resource-edit',
  templateUrl: './resource-edit.component.html',
  styleUrls: ['./resource-edit.component.css']
})
export class ResourceEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: ResourceDetail = new ResourceDetail();

  constructor(public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<ResourceEditComponent>,
              public  persist: ResourcePersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
      this.title = this.appTranslation.getText("general","new") +  " Resource";
      this.isNew = true;
      //set necessary defaults
    } else {
      this.title = this.appTranslation.getText("general","edit") +  " Resource";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getResource(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addResource(this.item).subscribe(value => {
      this.tcNotification.success("Resource added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateResource(this.item).subscribe(value => {
      this.tcNotification.success("Resource updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
