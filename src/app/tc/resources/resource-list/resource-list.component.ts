import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";



import {ResourcePersist} from "../resource.persist";
import {ResourceNavigator} from "../resource.navigator";
import {ResourceDetail, ResourceSummary, ResourceSummaryPartialList} from "../resource.model";
import {TCUtilsAngular} from "../../utils-angular";
import {TCUtilsDate} from "../../utils-date";
import {TCUtilsArray} from "../../utils-array";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {AppTranslation} from "../../../app.translation";

export enum ResourceListTabs {
  overview
}

@Component({
  selector: 'app-resource-list',
  templateUrl: './resource-list.component.html',
  styleUrls: ['./resource-list.component.css']
})
export class ResourceListComponent implements OnInit {
  activeTab: ResourceListTabs = ResourceListTabs.overview;
  tabs: typeof ResourceListTabs = ResourceListTabs;

  resourcesData: ResourceSummary[] = [];
  resourcesTotalCount: number = 0;
  resourcesSelection: ResourceSummary[] = [];
  resourcesDisplayedColumns: string[] = ["select", "action", "uri","users_count","groups_count"];

  resourcesSearchTextBox: FormControl = new FormControl();
  resourcesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) resourcesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) resourcesSort: MatSort;

  constructor(private router: Router,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public resourcePersist: ResourcePersist,
                public resourceNavigator: ResourceNavigator
    ) {

      //delay subsequent keyup events
      this.resourcesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.resourcePersist.resourceSearchText = value;
        this.searchResources();
      });
    }

    ngOnInit() {

      this.resourcesSort.sortChange.subscribe(() => {
        this.resourcesPaginator.pageIndex = 0;
        this.searchResources();
      });

      this.resourcesPaginator.page.subscribe(() => {
        this.searchResources();
      });
      //start by loading items
      this.searchResources();
    }

  searchResources(): void {
    this.resourcesIsLoading = true;
    this.resourcesSelection = [];

    this.resourcePersist.searchResource(this.resourcesPaginator.pageSize, this.resourcesPaginator.pageIndex, this.resourcesSort.active, this.resourcesSort.direction).subscribe((partialList: ResourceSummaryPartialList) => {
      this.resourcesData = partialList.data;
      if (partialList.total != -1) {
        this.resourcesTotalCount = partialList.total;
      }
      this.resourcesIsLoading = false;
    }, error => {
      this.resourcesIsLoading = false;
    });

  }

  downloadResources(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addResource(): void {
    let dialogRef = this.resourceNavigator.addResource();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchResources();
      }
    });
  }

  editResource(item: ResourceSummary) {
    let dialogRef = this.resourceNavigator.editResource(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteResource(item: ResourceSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Resource");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.resourcePersist.deleteResource(item.id).subscribe(response => {
          this.tcNotification.success("Resource deleted");
          this.searchResources();
        }, error => {
        });
      }

    });
  }

}
