import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ResourcePickComponent } from './resource-pick.component';

describe('ResourcePickComponent', () => {
  let component: ResourcePickComponent;
  let fixture: ComponentFixture<ResourcePickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourcePickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcePickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
