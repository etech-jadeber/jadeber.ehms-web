import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCUtilsAngular} from "../../utils-angular";
import {TCUtilsArray} from "../../utils-array";
import {TCUtilsDate} from "../../utils-date";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {AppTranslation} from "../../../app.translation";

import {ResourceDetail, ResourceSummary, ResourceSummaryPartialList} from "../resource.model";
import {ResourcePersist} from "../resource.persist";


@Component({
  selector: 'app-resource-pick',
  templateUrl: './resource-pick.component.html',
  styleUrls: ['./resource-pick.component.css']
})
export class ResourcePickComponent implements OnInit {

  resourcesData: ResourceSummary[] = [];
  resourcesTotalCount: number = 0;
  resourcesSelection: ResourceSummary[] = [];
  resourcesDisplayedColumns: string[] = ["select", 'uri', 'description'];

  resourcesSearchTextBox: FormControl = new FormControl();
  resourcesIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) resourcesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) resourcesSort: MatSort;

  constructor(public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public resourcePersist: ResourcePersist,
              public dialogRef: MatDialogRef<ResourcePickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    //delay subsequent keyup events
    this.resourcesSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.resourcePersist.resourceSearchText = value;
      this.searchResources();
    });
  }

  ngOnInit() {

    this.resourcesSort.sortChange.subscribe(() => {
      this.resourcesPaginator.pageIndex = 0;
      this.searchResources();
    });

    this.resourcesPaginator.page.subscribe(() => {
      this.searchResources();
    });
    //start by loading items
    this.searchResources();
  }

  searchResources(): void {
    this.resourcesIsLoading = true;
    this.resourcesSelection = [];

    this.resourcePersist.searchResource(this.resourcesPaginator.pageSize,
      this.resourcesPaginator.pageIndex,
      this.resourcesSort.active,
      this.resourcesSort.direction).subscribe((partialList: ResourceSummaryPartialList) => {
      this.resourcesData = partialList.data;
      if (partialList.total != -1) {
        this.resourcesTotalCount = partialList.total;
      }
      this.resourcesIsLoading = false;
    }, error => {
      this.resourcesIsLoading = false;
    });

  }

  markOneItem(item: ResourceSummary) {
    this.resourcesSelection = [];
    this.resourcesSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.resourcesSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
