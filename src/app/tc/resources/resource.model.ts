import {TCId} from "../models";

export class TcaclDetail extends TCId {

  uri: string;
  user_id: string;
  group_id: string;
  can_create: boolean;
  can_read: boolean;
  can_update: boolean;
  can_delete: boolean;

}

export class ResourceSummary extends TCId {
  uri: string;
  description: string;
  users_count: number;
  groups_count: number;
}

export class ResourceSummaryPartialList {
  data: ResourceSummary[];
  total: number;
}

export class ResourceDetail extends ResourceSummary {
  uri: string;
  description: string;
  tcacls: TcaclDetail[];
}

export class ResourceDashboard {
  total: number;
}


export class AclEditParams {
  constructor(public resource_id: string,
              public acl_id: string,
              public user_id: string,
              public group_id: string) {
  }
}
