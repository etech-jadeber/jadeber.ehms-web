import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../utils-angular";

import {ResourceEditComponent} from "./resource-edit/resource-edit.component";
import {ResourcePickComponent} from "./resource-pick/resource-pick.component";
import {TcaclEditComponent} from "./tcacl-edit/tcacl-edit.component";
import {AclEditParams} from "./resource.model";


@Injectable({
  providedIn: 'root'
})

export class ResourceNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  resourcesUrl(): string {
    return "/resources";
  }

  resourceUrl(id: string): string {
    return "/resources/" + id;
  }

  viewResources(): void {
    this.router.navigateByUrl(this.resourcesUrl());
  }

  viewResource(id: string): void {
    this.router.navigateByUrl(this.resourceUrl(id));
  }

  editResource(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ResourceEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addResource(): MatDialogRef<unknown,any> {
    return this.editResource(null);
  }

  pickResources(selectOne: boolean = false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(ResourcePickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }

  //tcacls
  editTcacl(resourceId: string, aclId: string, user_id: string="", group_id: string=""): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(TcaclEditComponent, {
      data: new AclEditParams(resourceId, aclId, user_id, group_id),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addTcacl(resourceId: string, user_id: string, group_id): MatDialogRef<unknown,any> {
    return this.editTcacl(resourceId, null, user_id, group_id);
  }
}
