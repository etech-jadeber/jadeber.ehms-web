import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';


import {TCDoParam, TCUtilsHttp} from "../utils-http";
import {TCId} from "../models";
import {ResourceDashboard, ResourceDetail, ResourceSummaryPartialList, TcaclDetail} from "./resource.model";
import {environment} from "../../../environments/environment";


@Injectable({
  providedIn: 'root'
})
export class ResourcePersist {

  resourceSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchResource(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<ResourceSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("resources", this.resourceSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<ResourceSummaryPartialList>(url);

  }

  resourceDashboard(): Observable<ResourceDashboard> {
    return this.http.get<ResourceDashboard>(environment.tcApiBaseUri + "resources/dashboard");
  }

  getResource(id: string): Observable<ResourceDetail> {
    return this.http.get<ResourceDetail>(environment.tcApiBaseUri + "resources/" + id);
  }

  addResource(item: ResourceDetail): Observable<TCId> {
    return this.http.post<ResourceDetail>(environment.tcApiBaseUri + "resources/", item);
  }

  updateResource(item: ResourceDetail): Observable<ResourceDetail> {
    return this.http.patch<ResourceDetail>(environment.tcApiBaseUri + "resources/" + item.id, item);
  }

  deleteResource(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "resources/" + id);
  }

  resourcesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "resources/do", new TCDoParam(method, payload));
  }

  resourceDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "resources/" + id + "/do", new TCDoParam(method, payload));
  }

  //tcacls
  getTcacl(parentId: string, id: string): Observable<TcaclDetail> {
    return this.http.get<TcaclDetail>(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/" + id);
  }


  addTcacl(parentId: string, item: TcaclDetail): Observable<TCId> {
    return this.http.post<TcaclDetail>(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/", item);
  }

  updateTcacl(parentId: string, item: TcaclDetail): Observable<TcaclDetail> {
    return this.http.patch<TcaclDetail>(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/" + item.id, item);
  }

  deleteTcacl(parentId: string, id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/" + id);
  }

  tcaclsDo(parentId: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/do", new TCDoParam(method, payload));
  }

  tcaclDo(parentId: string, id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "resources/" + parentId + "/tcacls/" + id + "/do", new TCDoParam(method, payload));
  }


}
