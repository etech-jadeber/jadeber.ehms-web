import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TcaclEditComponent } from './tcacl-edit.component';

describe('TcaclEditComponent', () => {
  let component: TcaclEditComponent;
  let fixture: ComponentFixture<TcaclEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TcaclEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TcaclEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
