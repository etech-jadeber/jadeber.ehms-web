import {Component, Inject, OnInit} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";

import {AclEditParams, TcaclDetail} from "../resource.model";
import {ResourcePersist} from "../resource.persist";
import {TCUtilsString} from "../../utils-string";
import {GroupPersist} from "../../groups/group.persist";
import {UserPersist} from "../../users/user.persist";


@Component({
  selector: 'app-tcacl-edit',
  templateUrl: './tcacl-edit.component.html',
  styleUrls: ['./tcacl-edit.component.css']
})
export class TcaclEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  tcaclDetail: TcaclDetail = new TcaclDetail();
  name: string = "";


  constructor(public tcNotification: TCNotification,
              public dialogRef: MatDialogRef<TcaclEditComponent>,
              public appTranslation: AppTranslation,
              public userPersist: UserPersist,
              public groupPersist: GroupPersist,
              public  resourcePersist: ResourcePersist,
              @Inject(MAT_DIALOG_DATA) public aclEditParams: AclEditParams) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  showName(): void {
    if (!TCUtilsString.isInvalidId(this.tcaclDetail.user_id)) {
      this.userPersist.getUser(this.tcaclDetail.user_id).subscribe(user => {
        this.name = user.name;
      });
    } else {
      this.groupPersist.getGroup(this.tcaclDetail.group_id).subscribe(group => {
        this.name = group.name;
      });
    }
  }

  ngOnInit() {
    if (this.aclEditParams.acl_id === null) {
      this.title = "New ACL";
      this.isNew = true;
      //set necessary defaults
      this.tcaclDetail.user_id = this.aclEditParams.user_id;
      this.tcaclDetail.group_id = this.aclEditParams.group_id;

      this.tcaclDetail.can_create = false;
      this.tcaclDetail.can_read = false;
      this.tcaclDetail.can_update = false;
      this.tcaclDetail.can_delete = false;
      this.showName();

    } else {
      this.title = "Edit ACL";
      this.isNew = false;

      this.isLoadingResults = true;
      this.resourcePersist.getTcacl(this.aclEditParams.resource_id, this.aclEditParams.acl_id).subscribe(item => {
        this.tcaclDetail = item;
        this.isLoadingResults = false;
        this.showName();

      }, error => {
        this.isLoadingResults = false;
        console.log(error);
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.resourcePersist.addTcacl(this.aclEditParams.resource_id, this.tcaclDetail).subscribe(value => {
      this.tcNotification.success("ACL added");
      this.tcaclDetail.id = value.id;
      this.dialogRef.close(this.tcaclDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.resourcePersist.updateTcacl(this.aclEditParams.resource_id, this.tcaclDetail).subscribe(value => {
      this.tcNotification.success("ACL updated");
      this.dialogRef.close(this.tcaclDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
