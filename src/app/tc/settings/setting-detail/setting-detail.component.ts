import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {SettingDetail} from "../setting.model";
import {SettingPersist} from "../setting.persist";
import {SettingNavigator} from "../setting.navigator";


export enum SettingTabs {
  overview,
}

@Component({
  selector: 'app-setting-detail',
  templateUrl: './setting-detail.component.html',
  styleUrls: ['./setting-detail.component.css']
})
export class SettingDetailComponent implements OnInit {

  //basics
  settingDetail: SettingDetail;
  settingTabs: typeof SettingTabs = SettingTabs;
  activeTab: SettingTabs = SettingTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public settingNavigator: SettingNavigator,
              public  persist: SettingPersist) {
    this.tcAuthorization.requireRead("settings");
    this.settingDetail = new SettingDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getSetting(id.toString()).subscribe(settingDetail => {
      this.settingDetail = settingDetail;
    }, error => {
    });
  }

  editSetting(): void {
    let modalRef = this.settingNavigator.editSetting(this.settingDetail.id);
    modalRef.afterClosed().subscribe(modifiedSettingDetail => {
      TCUtilsAngular.assign(this.settingDetail, modifiedSettingDetail);
    }, error => {
      console.error(error);
    });
  }


}
