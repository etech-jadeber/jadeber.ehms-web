import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";


import {SettingDetail} from "../setting.model";
import {SettingPersist} from "../setting.persist";


@Component({
  selector: 'app-setting-edit',
  templateUrl: './setting-edit.component.html',
  styleUrls: ['./setting-edit.component.css']
})
export class SettingEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: SettingDetail = new SettingDetail();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<SettingEditComponent>,
              public  persist: SettingPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
     this.tcAuthorization.requireCreate("settings");
      this.title = this.appTranslation.getText("general","new") +  " Setting";
      this.isNew = true;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("settings");
      this.title = this.appTranslation.getText("general","edit") +  " Setting";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getSetting(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addSetting(this.item).subscribe(value => {
      this.tcNotification.success("Setting added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateSetting(this.item).subscribe(value => {
      this.tcNotification.success("Setting updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
