import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {SettingPersist} from "../setting.persist";
import {SettingNavigator} from "../setting.navigator";
import {SettingDetail, SettingSummary, SettingSummaryPartialList} from "../setting.model";

export enum SettingListTabs {
  overview
}

@Component({
  selector: 'app-setting-list',
  templateUrl: './setting-list.component.html',
  styleUrls: ['./setting-list.component.css']
})
export class SettingListComponent implements OnInit {
  activeTab: SettingListTabs = SettingListTabs.overview;
  tabs: typeof SettingListTabs = SettingListTabs;

  settingsData: SettingSummary[] = [];
  settingsTotalCount: number = 0;
  settingsSelection: SettingSummary[] = [];
  settingsDisplayedColumns: string[] = ["select", "action","link", "setting_type","target_id","setting_value","update_time"];

  settingsSearchTextBox: FormControl = new FormControl();
  settingsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) settingsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) settingsSort: MatSort;

  constructor(private router: Router,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public settingPersist: SettingPersist,
                public settingNavigator: SettingNavigator
    ) {

        this.tcAuthorization.requireRead("settings");
      //delay subsequent keyup events
      this.settingsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.settingPersist.settingSearchText = value;
        this.searchSettings();
      });
    }

    ngOnInit() {

      this.settingsSort.sortChange.subscribe(() => {
        this.settingsPaginator.pageIndex = 0;
        this.searchSettings();
      });

      this.settingsPaginator.page.subscribe(() => {
        this.searchSettings();
      });
      //start by loading items
      this.searchSettings();
    }

  searchSettings(): void {
    this.settingsIsLoading = true;
    this.settingsSelection = [];

    this.settingPersist.searchSetting(this.settingsPaginator.pageSize, this.settingsPaginator.pageIndex, this.settingsSort.active, this.settingsSort.direction).subscribe((partialList: SettingSummaryPartialList) => {
      this.settingsData = partialList.data;
      if (partialList.total != -1) {
        this.settingsTotalCount = partialList.total;
      }
      this.settingsIsLoading = false;
    }, error => {
      this.settingsIsLoading = false;
    });

  }

  downloadSettings(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addSetting(): void {
    let dialogRef = this.settingNavigator.addSetting();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchSettings();
      }
    });
  }

  editSetting(item: SettingSummary) {
    let dialogRef = this.settingNavigator.editSetting(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteSetting(item: SettingSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Setting");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.settingPersist.deleteSetting(item.id).subscribe(response => {
          this.tcNotification.success("Setting deleted");
          this.searchSettings();
        }, error => {
        });
      }

    });
  }

}
