import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SettingPickComponent } from './setting-pick.component';

describe('SettingPickComponent', () => {
  let component: SettingPickComponent;
  let fixture: ComponentFixture<SettingPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
