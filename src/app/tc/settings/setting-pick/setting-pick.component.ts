import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {SettingDetail, SettingSummary, SettingSummaryPartialList} from "../setting.model";
import {SettingPersist} from "../setting.persist";


@Component({
  selector: 'app-setting-pick',
  templateUrl: './setting-pick.component.html',
  styleUrls: ['./setting-pick.component.css']
})
export class SettingPickComponent implements OnInit {

  settingsData: SettingSummary[] = [];
  settingsTotalCount: number = 0;
  settingsSelection: SettingSummary[] = [];
  settingsDisplayedColumns: string[] = ["select", 'setting_type','target_id','setting_value','update_time' ];

  settingsSearchTextBox: FormControl = new FormControl();
  settingsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) settingsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) settingsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public settingPersist: SettingPersist,
              public dialogRef: MatDialogRef<SettingPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("settings");
    //delay subsequent keyup events
    this.settingsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.settingPersist.settingSearchText = value;
      this.searchSettings();
    });
  }

  ngOnInit() {

    this.settingsSort.sortChange.subscribe(() => {
      this.settingsPaginator.pageIndex = 0;
      this.searchSettings();
    });

    this.settingsPaginator.page.subscribe(() => {
      this.searchSettings();
    });
    //start by loading items
    this.searchSettings();
  }

  searchSettings(): void {
    this.settingsIsLoading = true;
    this.settingsSelection = [];

    this.settingPersist.searchSetting(this.settingsPaginator.pageSize,
        this.settingsPaginator.pageIndex,
        this.settingsSort.active,
        this.settingsSort.direction).subscribe((partialList: SettingSummaryPartialList) => {
      this.settingsData = partialList.data;
      if (partialList.total != -1) {
        this.settingsTotalCount = partialList.total;
      }
      this.settingsIsLoading = false;
    }, error => {
      this.settingsIsLoading = false;
    });

  }

  markOneItem(item: SettingSummary) {
    this.settingsSelection = [];
    this.settingsSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.settingsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
