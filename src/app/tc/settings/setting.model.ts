import {TCId} from "../../tc/models";

export class SettingSummary extends TCId {
  setting_type : number;
target_id : string;
setting_value : string;
update_time : number;
}

export class SettingSummaryPartialList {
  data: SettingSummary[];
  total: number;
}

export class SettingDetail extends SettingSummary {
  setting_type : number;
target_id : string;
setting_value : string;
update_time : number;
}

export class SettingDashboard {
  total: number;
}
