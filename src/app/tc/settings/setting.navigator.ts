import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {SettingEditComponent} from "./setting-edit/setting-edit.component";
import {SettingPickComponent} from "./setting-pick/setting-pick.component";


@Injectable({
  providedIn: 'root'
})

export class SettingNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  settingsUrl(): string {
    return "/settings";
  }

  settingUrl(id: string): string {
    return "/settings/" + id;
  }

  viewSettings(): void {
    this.router.navigateByUrl(this.settingsUrl());
  }

  viewSetting(id: string): void {
    this.router.navigateByUrl(this.settingUrl(id));
  }

  editSetting(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(SettingEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addSetting(): MatDialogRef<unknown,any> {
    return this.editSetting(null);
  }
  
   pickSettings(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(SettingPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
