import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {SettingDashboard, SettingDetail, SettingSummaryPartialList} from "./setting.model";


@Injectable({
  providedIn: 'root'
})
export class SettingPersist {

  settingSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchSetting(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<SettingSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/settings", this.settingSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<SettingSummaryPartialList>(url);

  }

  settingDashboard(): Observable<SettingDashboard> {
    return this.http.get<SettingDashboard>(environment.tcApiBaseUri + "tc/settings/dashboard");
  }

  getSetting(id: string): Observable<SettingDetail> {
    return this.http.get<SettingDetail>(environment.tcApiBaseUri + "tc/settings/" + id);
  }

  addSetting(item: SettingDetail): Observable<TCId> {
    return this.http.post<SettingDetail>(environment.tcApiBaseUri + "tc/settings/", item);
  }

  updateSetting(item: SettingDetail): Observable<SettingDetail> {
    return this.http.patch<SettingDetail>(environment.tcApiBaseUri + "tc/settings/" + item.id, item);
  }

  deleteSetting(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/settings/" + id);
  }

  settingsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/settings/do", new TCDoParam(method, payload));
  }

  settingDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/settings/" + id + "/do", new TCDoParam(method, payload));
  }


}
