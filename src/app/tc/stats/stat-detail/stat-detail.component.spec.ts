import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StatDetailComponent } from './stat-detail.component';

describe('StatDetailComponent', () => {
  let component: StatDetailComponent;
  let fixture: ComponentFixture<StatDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StatDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
