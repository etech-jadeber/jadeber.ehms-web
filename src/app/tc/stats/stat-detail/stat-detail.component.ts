import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCNotification} from "../../../tc/notification";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {StatDetail} from "../stat.model";
import {StatPersist} from "../stat.persist";
import {StatNavigator} from "../stat.navigator";


export enum StatTabs {
  overview,
}

@Component({
  selector: 'app-stat-detail',
  templateUrl: './stat-detail.component.html',
  styleUrls: ['./stat-detail.component.css']
})
export class StatDetailComponent implements OnInit {

  //basics
  statDetail: StatDetail;
  statTabs: typeof StatTabs = StatTabs;
  activeTab: StatTabs = StatTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public appTranslation:AppTranslation,
              public statNavigator: StatNavigator,
              public  persist: StatPersist) {
    this.tcAuthorization.requireRead("stats");
    this.statDetail = new StatDetail();
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.persist.getStat(id.toString()).subscribe(statDetail => {
      this.statDetail = statDetail;
    }, error => {
    });
  }

  editStat(): void {
    let modalRef = this.statNavigator.editStat(this.statDetail.id);
    modalRef.afterClosed().subscribe(modifiedStatDetail => {
      TCUtilsAngular.assign(this.statDetail, modifiedStatDetail);
    }, error => {
      console.error(error);
    });
  }


}
