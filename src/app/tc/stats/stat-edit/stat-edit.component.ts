import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification} from "../../../tc/notification";
import {AppTranslation} from "../../../app.translation";


import {StatDetail} from "../stat.model";
import {StatPersist} from "../stat.persist";


@Component({
  selector: 'app-stat-edit',
  templateUrl: './stat-edit.component.html',
  styleUrls: ['./stat-edit.component.css']
})
export class StatEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: StatDetail = new StatDetail();

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<StatEditComponent>,
              public  persist: StatPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
     this.tcAuthorization.requireCreate("stats");
      this.title = this.appTranslation.getText("general","new") +  " Stat";
      this.isNew = true;
      //set necessary defaults
    } else {
     this.tcAuthorization.requireUpdate("stats");
      this.title = this.appTranslation.getText("general","edit") +  " Stat";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getStat(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addStat(this.item).subscribe(value => {
      this.tcNotification.success("Stat added");
      this.item.id = value.id;
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateStat(this.item).subscribe(value => {
      this.tcNotification.success("Stat updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
