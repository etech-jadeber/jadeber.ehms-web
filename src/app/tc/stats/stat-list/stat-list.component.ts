import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {StatPersist} from "../stat.persist";
import {StatNavigator} from "../stat.navigator";
import {StatDetail, StatSummary, StatSummaryPartialList} from "../stat.model";

export enum StatListTabs {
  overview
}

@Component({
  selector: 'app-stat-list',
  templateUrl: './stat-list.component.html',
  styleUrls: ['./stat-list.component.css']
})
export class StatListComponent implements OnInit {
  activeTab: StatListTabs = StatListTabs.overview;
  tabs: typeof StatListTabs = StatListTabs;

  statsData: StatSummary[] = [];
  statsTotalCount: number = 0;
  statsSelection: StatSummary[] = [];
  statsDisplayedColumns: string[] = ["select", "action","link", "stat_type","target_id","stat_value","stat_time"];

  statsSearchTextBox: FormControl = new FormControl();
  statsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) statsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) statsSort: MatSort;

  constructor(private router: Router,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public statPersist: StatPersist,
                public statNavigator: StatNavigator
    ) {

        this.tcAuthorization.requireRead("stats");
      //delay subsequent keyup events
      this.statsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.statPersist.statSearchText = value;
        this.searchStats();
      });
    }

    ngOnInit() {

      this.statsSort.sortChange.subscribe(() => {
        this.statsPaginator.pageIndex = 0;
        this.searchStats();
      });

      this.statsPaginator.page.subscribe(() => {
        this.searchStats();
      });
      //start by loading items
      this.searchStats();
    }

  searchStats(): void {
    this.statsIsLoading = true;
    this.statsSelection = [];

    this.statPersist.searchStat(this.statsPaginator.pageSize, this.statsPaginator.pageIndex, this.statsSort.active, this.statsSort.direction).subscribe((partialList: StatSummaryPartialList) => {
      this.statsData = partialList.data;
      if (partialList.total != -1) {
        this.statsTotalCount = partialList.total;
      }
      this.statsIsLoading = false;
    }, error => {
      this.statsIsLoading = false;
    });

  }

  downloadStats(): void {
    this.tcNotification.info("Download selected items . . . .");
  }



  addStat(): void {
    let dialogRef = this.statNavigator.addStat();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchStats();
      }
    });
  }

  editStat(item: StatSummary) {
    let dialogRef = this.statNavigator.editStat(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteStat(item: StatSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Stat");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.statPersist.deleteStat(item.id).subscribe(response => {
          this.tcNotification.success("Stat deleted");
          this.searchStats();
        }, error => {
        });
      }

    });
  }

}
