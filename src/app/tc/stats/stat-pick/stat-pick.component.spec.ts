import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StatPickComponent } from './stat-pick.component';

describe('StatPickComponent', () => {
  let component: StatPickComponent;
  let fixture: ComponentFixture<StatPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StatPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
