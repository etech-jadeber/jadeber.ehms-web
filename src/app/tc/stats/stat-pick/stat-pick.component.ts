import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {StatDetail, StatSummary, StatSummaryPartialList} from "../stat.model";
import {StatPersist} from "../stat.persist";


@Component({
  selector: 'app-stat-pick',
  templateUrl: './stat-pick.component.html',
  styleUrls: ['./stat-pick.component.css']
})
export class StatPickComponent implements OnInit {

  statsData: StatSummary[] = [];
  statsTotalCount: number = 0;
  statsSelection: StatSummary[] = [];
  statsDisplayedColumns: string[] = ["select", 'stat_type','target_id','stat_value','stat_time' ];

  statsSearchTextBox: FormControl = new FormControl();
  statsIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) statsPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) statsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public statPersist: StatPersist,
              public dialogRef: MatDialogRef<StatPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("stats");
    //delay subsequent keyup events
    this.statsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.statPersist.statSearchText = value;
      this.searchStats();
    });
  }

  ngOnInit() {

    this.statsSort.sortChange.subscribe(() => {
      this.statsPaginator.pageIndex = 0;
      this.searchStats();
    });

    this.statsPaginator.page.subscribe(() => {
      this.searchStats();
    });
    //start by loading items
    this.searchStats();
  }

  searchStats(): void {
    this.statsIsLoading = true;
    this.statsSelection = [];

    this.statPersist.searchStat(this.statsPaginator.pageSize,
        this.statsPaginator.pageIndex,
        this.statsSort.active,
        this.statsSort.direction).subscribe((partialList: StatSummaryPartialList) => {
      this.statsData = partialList.data;
      if (partialList.total != -1) {
        this.statsTotalCount = partialList.total;
      }
      this.statsIsLoading = false;
    }, error => {
      this.statsIsLoading = false;
    });

  }

  markOneItem(item: StatSummary) {
    this.statsSelection = [];
    this.statsSelection.push(item);
    //this.returnSelected();
  }

  returnSelected(): void {
    this.dialogRef.close(this.statsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
