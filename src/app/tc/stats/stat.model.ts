import {TCId} from "../../tc/models";

export class StatSummary extends TCId {
  stat_type : number;
target_id : string;
stat_value : number;
stat_time : number;
}

export class StatSummaryPartialList {
  data: StatSummary[];
  total: number;
}

export class StatDetail extends StatSummary {
  stat_type : number;
target_id : string;
stat_value : number;
stat_time : number;
}

export class StatDashboard {
  total: number;
}
