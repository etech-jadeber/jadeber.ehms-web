import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef,MatDialog, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";

import {StatEditComponent} from "./stat-edit/stat-edit.component";
import {StatPickComponent} from "./stat-pick/stat-pick.component";


@Injectable({
  providedIn: 'root'
})

export class StatNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  statsUrl(): string {
    return "/stats";
  }

  statUrl(id: string): string {
    return "/stats/" + id;
  }

  viewStats(): void {
    this.router.navigateByUrl(this.statsUrl());
  }

  viewStat(id: string): void {
    this.router.navigateByUrl(this.statUrl(id));
  }

  editStat(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(StatEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addStat(): MatDialogRef<unknown,any> {
    return this.editStat(null);
  }
  
   pickStats(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(StatPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
