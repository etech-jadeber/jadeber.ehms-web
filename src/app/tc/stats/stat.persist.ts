import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../../tc/utils-http";
import {TCId} from "../../tc/models";
import {StatDashboard, StatDetail, StatSummaryPartialList} from "./stat.model";


@Injectable({
  providedIn: 'root'
})
export class StatPersist {

  statSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchStat(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<StatSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/stats", this.statSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<StatSummaryPartialList>(url);

  }

  statDashboard(): Observable<StatDashboard> {
    return this.http.get<StatDashboard>(environment.tcApiBaseUri + "tc/stats/dashboard");
  }

  getStat(id: string): Observable<StatDetail> {
    return this.http.get<StatDetail>(environment.tcApiBaseUri + "tc/stats/" + id);
  }

  addStat(item: StatDetail): Observable<TCId> {
    return this.http.post<StatDetail>(environment.tcApiBaseUri + "tc/stats/", item);
  }

  updateStat(item: StatDetail): Observable<StatDetail> {
    return this.http.patch<StatDetail>(environment.tcApiBaseUri + "tc/stats/" + item.id, item);
  }

  deleteStat(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "tc/stats/" + id);
  }

  statsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/stats/do", new TCDoParam(method, payload));
  }

  statDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/stats/" + id + "/do", new TCDoParam(method, payload));
  }


}
