import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessagesDetailComponent } from '../messagess/messages-detail/messages-detail.component';
import { MessagesListComponent } from '../messagess/messages-list/messages-list.component';
import { CaseDetailComponent } from './cases/case-detail/case-detail.component';
import { CaseListComponent } from './cases/case-list/case-list.component';
import { EventlogDetailComponent } from './eventlogs/eventlog-detail/eventlog-detail.component';
import { EventlogListComponent } from './eventlogs/eventlog-list/eventlog-list.component';
import { FileDetailComponent } from './files/file-detail/file-detail.component';
import { FileListComponent } from './files/file-list/file-list.component';
import { GroupDetailComponent } from './groups/group-detail/group-detail.component';
import { GroupListComponent } from './groups/group-list/group-list.component';
import { JobDetailComponent } from './jobs/job-detail/job-detail.component';
import { JobListComponent } from './jobs/job-list/job-list.component';
import { LockDetailComponent } from './locks/lock-detail/lock-detail.component';
import { LockListComponent } from './locks/lock-list/lock-list.component';
import { MailDetailComponent } from './mails/mail-detail/mail-detail.component';
import { MailListComponent } from './mails/mail-list/mail-list.component';
import { ResourceDetailComponent } from './resources/resource-detail/resource-detail.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { SettingDetailComponent } from './settings/setting-detail/setting-detail.component';
import { SettingListComponent } from './settings/setting-list/setting-list.component';
import { StatDetailComponent } from './stats/stat-detail/stat-detail.component';
import { StatListComponent } from './stats/stat-list/stat-list.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { UserListComponent } from './users/user-list/user-list.component';

const routes: Routes = [
  {path: 'users/:id', component: UserDetailComponent},
  {path: 'users', component: UserListComponent},
  {path: 'groups/:id', component: GroupDetailComponent},
  {path: 'groups', component: GroupListComponent},
  {path: 'resources/:id', component: ResourceDetailComponent},
  {path: 'resources', component: ResourceListComponent},
  {path: 'eventlogs/:id', component: EventlogDetailComponent},
  {path: 'eventlogs', component: EventlogListComponent},
  {path: 'settings/:id', component: SettingDetailComponent},
  {path: 'settings', component: SettingListComponent},
  {path: 'files/:id', component: FileDetailComponent},
  {path: 'files', component: FileListComponent},
  {path: 'mails/:id', component: MailDetailComponent},
  {path: 'mails', component: MailListComponent},
  {path: 'locks/:id', component: LockDetailComponent},
  {path: 'locks', component: LockListComponent},
  {path: 'cases/:id', component: CaseDetailComponent},
  {path: 'cases', component: CaseListComponent},



  {path: 'stats/:id', component: StatDetailComponent},
  {path: 'stats', component: StatListComponent},
  {path: 'jobs/:id', component: JobDetailComponent},
  {path: 'jobs', component: JobListComponent},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TcRoutingModule { }
