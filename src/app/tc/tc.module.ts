import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TcRoutingModule } from './tc-routing.module';
import { FullCalendarModule } from '@fullcalendar/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCommonModule, MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { UserListComponent } from './users/user-list/user-list.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';
import { UserEditComponent } from './users/user-edit/user-edit.component';
import { AsyncJobsComponent } from './async-jobs/async-jobs.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { UserPickComponent } from './users/user-pick/user-pick.component';
import { GroupListComponent } from './groups/group-list/group-list.component';
import { RadDetailComponent } from '../orderss/rad-detail/rad-detail.component';
import { GroupDetailComponent } from './groups/group-detail/group-detail.component';
import { GroupEditComponent } from './groups/group-edit/group-edit.component';
import { GroupPickComponent } from './groups/group-pick/group-pick.component';
import { ResourceListComponent } from './resources/resource-list/resource-list.component';
import { ResourceDetailComponent } from './resources/resource-detail/resource-detail.component';
import { ResourceEditComponent } from './resources/resource-edit/resource-edit.component';
import { ResourcePickComponent } from './resources/resource-pick/resource-pick.component';
import { TcaclEditComponent } from './resources/tcacl-edit/tcacl-edit.component';
import { EventlogListComponent } from './eventlogs/eventlog-list/eventlog-list.component';
import { EventlogDetailComponent } from './eventlogs/eventlog-detail/eventlog-detail.component';
import { EventlogEditComponent } from './eventlogs/eventlog-edit/eventlog-edit.component';
import { EventlogPickComponent } from './eventlogs/eventlog-pick/eventlog-pick.component';
import { SettingListComponent } from './settings/setting-list/setting-list.component';
import { SettingDetailComponent } from './settings/setting-detail/setting-detail.component';
import { SettingEditComponent } from './settings/setting-edit/setting-edit.component';
import { SettingPickComponent } from './settings/setting-pick/setting-pick.component';
import { FileDetailComponent } from './files/file-detail/file-detail.component';
import { FileEditComponent } from './files/file-edit/file-edit.component';
import { FilePickComponent } from './files/file-pick/file-pick.component';
import { LockPickComponent } from './locks/lock-pick/lock-pick.component';
import { MailListComponent } from './mails/mail-list/mail-list.component';
import { MailDetailComponent } from './mails/mail-detail/mail-detail.component';
import { MailEditComponent } from './mails/mail-edit/mail-edit.component';
import { JobPickComponent } from './jobs/job-pick/job-pick.component';
import { JobEditComponent } from './jobs/job-edit/job-edit.component';
import { JobDetailComponent } from './jobs/job-detail/job-detail.component';
import { MailPickComponent } from './mails/mail-pick/mail-pick.component';
import { LockListComponent } from './locks/lock-list/lock-list.component';
import { LockDetailComponent } from './locks/lock-detail/lock-detail.component';
import { LockEditComponent } from './locks/lock-edit/lock-edit.component';
import { CaseListComponent } from './cases/case-list/case-list.component';
import { CaseDetailComponent } from './cases/case-detail/case-detail.component';
import { CaseEditComponent } from './cases/case-edit/case-edit.component';
import { CasePickComponent } from './cases/case-pick/case-pick.component';
import { StatListComponent } from './stats/stat-list/stat-list.component';
import { StatDetailComponent } from './stats/stat-detail/stat-detail.component';
import { StatEditComponent } from './stats/stat-edit/stat-edit.component';
import { StatPickComponent } from './stats/stat-pick/stat-pick.component';
import { JobListComponent } from './jobs/job-list/job-list.component';
import { TextDateComponent } from './text-date/text-date.component';
import { NumberInputComponent } from './numberinput/numberinput.component';


@NgModule({
  declarations: [   
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
  
    UserPickComponent,
    GroupListComponent,
    
    GroupDetailComponent,
    GroupEditComponent,
    GroupPickComponent,
    ResourceListComponent,
    ResourceDetailComponent,
    ResourceEditComponent,
    ResourcePickComponent,
    TcaclEditComponent,
    EventlogListComponent,
    EventlogDetailComponent,
    EventlogEditComponent,
  
    EventlogPickComponent,
    
    FileDetailComponent,
    FileEditComponent,
    FilePickComponent,
  
    LockPickComponent,

    MailListComponent,
    MailDetailComponent,
    MailEditComponent,
    MailPickComponent,
    LockListComponent,
    LockDetailComponent,
    LockEditComponent,
    CaseListComponent,
    CaseDetailComponent,

    CaseEditComponent,
    CasePickComponent,
    StatListComponent,
    StatDetailComponent,
    StatEditComponent,
    StatPickComponent,
    JobListComponent,
    JobDetailComponent,
    JobEditComponent,
    JobPickComponent,
    TextDateComponent,
    NumberInputComponent,],
  imports: [
    TcRoutingModule,
    CommonModule,
    FullCalendarModule,
    FormsModule,
    MatBadgeModule,
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatListModule,
    MatRippleModule,
    MatTableModule,
    MatCommonModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatRadioModule,
    MatExpansionModule,
    MatSelectModule,
    MatTabsModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatChipsModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatSortModule,
    MatAutocompleteModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    NgxChartsModule,
  ]
})
export class TcModule { }
