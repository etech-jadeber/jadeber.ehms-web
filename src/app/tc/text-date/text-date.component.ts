
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {TCUtilsDate} from "../utils-date";
import { TextinputParameter } from '../textinput/textinput.component';


export class TextDateParameter {
  prompt: string;
  current_text_value: string;
  multiLine: boolean;
  current_date_value: number;
  cancelLabel: string;
  action: string;
  guidelines: string[] = [];
  validationPattern: string = "";
}
export class TextDate {
  date: number;
  text: string;
}

@Component({
  selector: 'app-text-date',
  templateUrl: './text-date.component.html',
  styleUrls: ['./text-date.component.scss']
})
export class TextDateComponent implements OnInit {
  
  date: string = "";
  textDate: TextDate = new TextDate();
  constructor(public dialogRef: MatDialogRef<TextDateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TextDateParameter,
    public tcUtilsDate: TCUtilsDate) {
}


  ngOnInit(): void {
    this.date = this.getStartTime(this.tcUtilsDate.toDate(this.data.current_date_value));
    this.textDate.text = this.data.current_text_value;
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onOk(): void {
    this.textDate.date =  new Date(this.date).getTime() / 1000;;
    this.dialogRef.close(this.textDate);
  }

  getStartTime(date: Date) {
    let now = date;
    let month = ('0' + (now.getMonth() + 1)).slice(-2);
    let day = ('0' + now.getDate()).slice(-2);
    let min = ('0' + now.getMinutes()).slice(-2);
    let hour = ('0' + now.getHours()).slice(-2);
    return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }

  canSubmit(): boolean{
    if(this.textDate == null){
      return false;
    }
    else if(this.date == null){
      return false;
    }
    else if(this.textDate.text == null || this.textDate.text == ""){
      return false;
    }

    return true;
  }
  
}
