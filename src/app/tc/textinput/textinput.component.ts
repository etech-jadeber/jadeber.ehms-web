import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';


export class TextinputParameter {
  currentValue: string;
  multiLine: boolean;
  prompt: string;
  cancelLabel: string;
  action: string;
  guidelines: string[] = [];
  validationPattern: string = "";
  editor: boolean;
}


@Component({
  selector: 'app-textinput',
  templateUrl: './textinput.component.html',
  styleUrls: ['./textinput.component.css']
})
export class TextinputComponent implements OnInit {


  Editor = ClassicEditor;
  txt: string = '';
  config = {};

  constructor(public dialogRef: MatDialogRef<TextinputComponent>,
    public radiologyResultSettingPersist: RadiologyResultSettingsPersist,
              @Inject(MAT_DIALOG_DATA) public data: TextinputParameter) {
    this.txt = data.currentValue == null ? '' : data.currentValue;
  }

  ngOnInit() {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onOk(): void {
    this.dialogRef.close(this.txt);
  }

  canSubmit(): boolean {
    if (this.txt == "") {
      return false;
    } else if (this.data.validationPattern != "") {
      return RegExp(this.data.validationPattern).test(this.txt);
    }
    return true;
  }

}
