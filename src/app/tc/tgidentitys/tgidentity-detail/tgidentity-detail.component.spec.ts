import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TgidentityDetailComponent } from './tgidentity-detail.component';

describe('TgidentityDetailComponent', () => {
  let component: TgidentityDetailComponent;
  let fixture: ComponentFixture<TgidentityDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TgidentityDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgidentityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
