import { Component, OnInit, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime } from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { Location } from '@angular/common';

import { Subscription } from "rxjs";

import { TCAuthorization } from "../../../tc/authorization";
import { TCUtilsAngular } from "../../../tc/utils-angular";
import { TCNotification } from "../../../tc/notification";
import { TCUtilsArray } from "../../../tc/utils-array";
import { TCNavigator } from "../../../tc/navigator";
import { JobPersist } from "../../../tc/jobs/job.persist";
import { AppTranslation } from "../../../app.translation";


import { TgidentityDetail } from "../tgidentity.model";
import { TgidentityPersist } from "../tgidentity.persist";
import { TgidentityNavigator } from "../tgidentity.navigator";


export enum TgidentityTabs {
  overview,
}

@Component({
  selector: 'app-tgidentity-detail',
  templateUrl: './tgidentity-detail.component.html',
  styleUrls: ['./tgidentity-detail.component.css']
})
export class TgidentityDetailComponent implements OnInit, OnDestroy {

  paramsSubscription: Subscription;
  tgidentityLoaded: boolean = false;
  //basics
  tgidentityDetail: TgidentityDetail;
  tgidentityTabs: typeof TgidentityTabs = TgidentityTabs;
  activeTab: TgidentityTabs = TgidentityTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public tgidentityNavigator: TgidentityNavigator,
    public persist: TgidentityPersist) {
    this.tcAuthorization.requireRead("tgidentitys");
    this.tgidentityDetail = new TgidentityDetail();
  }

  ngOnInit() {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

  loadDetails(id: string): void {
    this.persist.getTgidentity(id).subscribe(tgidentityDetail => {
      this.tgidentityDetail = tgidentityDetail;
      this.tgidentityLoaded = true;
    }, error => {
      console.error(error);
    });
  }

  editTgidentity(): void {
    let modalRef = this.tgidentityNavigator.editTgidentity(this.tgidentityDetail.id);
    modalRef.afterClosed().subscribe(modifiedTgidentityDetail => {
      TCUtilsAngular.assign(this.tgidentityDetail, modifiedTgidentityDetail);
    }, error => {
      console.error(error);
    });
  }

  printTgidentity(): void {
    this.persist.print(this.tgidentityDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print tgidentity", true);
    });
  }

  back(): void {
    this.location.back();
  }

  reload() {
    this.loadDetails(this.tgidentityDetail.id);
  }

}
