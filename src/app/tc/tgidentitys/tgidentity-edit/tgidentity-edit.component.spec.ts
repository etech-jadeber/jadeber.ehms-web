import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TgidentityEditComponent } from './tgidentity-edit.component';

describe('TgidentityEditComponent', () => {
  let component: TgidentityEditComponent;
  let fixture: ComponentFixture<TgidentityEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TgidentityEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgidentityEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
