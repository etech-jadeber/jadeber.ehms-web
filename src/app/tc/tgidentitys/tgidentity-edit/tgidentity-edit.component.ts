import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../../tc/authorization";
import {TCNotification}  from "../../../tc/notification";
import {TCUtilsString}   from "../../../tc/utils-string";
import {AppTranslation} from "../../../app.translation";

import {TCIdMode, TCModalModes} from "../../../tc/models";


import {TgidentityDetail} from "../tgidentity.model";
import {TgidentityPersist} from "../tgidentity.persist";


@Component({
  selector: 'app-tgidentity-edit',
  templateUrl: './tgidentity-edit.component.html',
  styleUrls: ['./tgidentity-edit.component.css']
})
export class TgidentityEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  tgidentityDetail: TgidentityDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<TgidentityEditComponent>,
              public  persist: TgidentityPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("tgidentitys");
      this.title = this.appTranslation.getText("general","new") +  " Tgidentity";
      this.tgidentityDetail = new TgidentityDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("tgidentitys");
      this.title = this.appTranslation.getText("general","edit") +  " Tgidentity";
      this.isLoadingResults = true;
      this.persist.getTgidentity(this.idMode.id).subscribe(tgidentityDetail => {
        this.tgidentityDetail = tgidentityDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addTgidentity(this.tgidentityDetail).subscribe(value => {
      this.tcNotification.success("Tgidentity added");
      this.tgidentityDetail.id = value.id;
      this.dialogRef.close(this.tgidentityDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateTgidentity(this.tgidentityDetail).subscribe(value => {
      this.tcNotification.success("Tgidentity updated");
      this.dialogRef.close(this.tgidentityDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.tgidentityDetail == null){
            return false;
          }

        if (this.tgidentityDetail.telephone_number == null || this.tgidentityDetail.telephone_number  == "") {
                      return false;
                    }

if (this.tgidentityDetail.username == null || this.tgidentityDetail.username  == "") {
                      return false;
                    }

if (this.tgidentityDetail.first_name == null || this.tgidentityDetail.first_name  == "") {
                      return false;
                    }

if (this.tgidentityDetail.last_name == null || this.tgidentityDetail.last_name  == "") {
                      return false;
                    }

if (this.tgidentityDetail.photo_url == null || this.tgidentityDetail.photo_url  == "") {
                      return false;
                    }


        return true;
      }


}
