import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TgidentityListComponent } from './tgidentity-list.component';

describe('TgidentityListComponent', () => {
  let component: TgidentityListComponent;
  let fixture: ComponentFixture<TgidentityListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TgidentityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgidentityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
