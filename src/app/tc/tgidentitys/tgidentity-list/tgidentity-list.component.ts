import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {JobPersist} from "../../../tc/jobs/job.persist";

import {AppTranslation} from "../../../app.translation";

import {TgidentityPersist} from "../tgidentity.persist";
import {TgidentityNavigator} from "../tgidentity.navigator";
import {TgidentitySummary, TgidentitySummaryPartialList} from "../tgidentity.model";
import {TCUtilsString} from "../../utils-string";
import {TelegramUserStates} from "../../models";
import {UserDetail} from "../../users/user.model";
import {UserNavigator} from "../../users/user.navigator";
import {UserPersist} from "../../users/user.persist";

export enum TgidentityListTabs {
  overview
}

@Component({
  selector: 'app-tgidentity-list',
  templateUrl: './tgidentity-list.component.html',
  styleUrls: ['./tgidentity-list.component.css']
})
export class TgidentityListComponent implements OnInit {
  activeTab: TgidentityListTabs = TgidentityListTabs.overview;
  tabs: typeof TgidentityListTabs = TgidentityListTabs;

  tgidentitysData: TgidentitySummary[] = [];
  tgidentitysTotalCount: number = 0;
  tgidentitysSelectAll: boolean = false;
  tgidentitysSelection: TgidentitySummary[] = [];
  tgidentitysDisplayedColumns: string[] = ["select", "action", "photo_url", "name", "telephone_number", "reg_date", "contactable", "state"];

  tgidentitysSearchTextBox: FormControl = new FormControl();
  tgidentitysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) tgidentitysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) tgidentitysSort: MatSort;

  constructor(private router: Router,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcUtilsString: TCUtilsString,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation: AppTranslation,
              public tgidentityPersist: TgidentityPersist,
              public tgidentityNavigator: TgidentityNavigator,
              public userNavigator: UserNavigator,
              public userPersist:UserPersist,
              public jobPersist: JobPersist,
  ) {

    this.tcAuthorization.requireRead("tgidentitys");
    this.tgidentitysSearchTextBox.setValue(tgidentityPersist.tgidentitySearchText);
    //delay subsequent keyup events
    this.tgidentitysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.tgidentityPersist.tgidentitySearchText = value;
      this.searchTgidentitys();
    });
  }

  ngOnInit() {

    this.tgidentitysSort.sortChange.subscribe(() => {
      this.tgidentitysPaginator.pageIndex = 0;
      this.searchTgidentitys();
    });

    this.tgidentitysPaginator.page.subscribe(() => {
      this.searchTgidentitys();
    });
    //start by loading items
    this.searchTgidentitys();
  }

  searchTgidentitys(): void {
    this.tgidentitysIsLoading = true;
    this.tgidentitysSelection = [];

    this.tgidentityPersist.searchTgidentity(this.tgidentitysPaginator.pageSize, this.tgidentitysPaginator.pageIndex, this.tgidentitysSort.active, this.tgidentitysSort.direction).subscribe((partialList: TgidentitySummaryPartialList) => {
      this.tgidentitysData = partialList.data;
      if (partialList.total != -1) {
        this.tgidentitysTotalCount = partialList.total;
      }
      this.tgidentitysIsLoading = false;
    }, error => {
      this.tgidentitysIsLoading = false;
    });

  }

  downloadTgidentitys(): void {
    if (this.tgidentitysSelectAll) {
      this.tgidentityPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download tgidentitys", true);
      });
    } else {
      this.tgidentityPersist.download(this.tcUtilsArray.idsList(this.tgidentitysSelection)).subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download tgidentitys", true);
      });
    }
  }


  addTgidentity(): void {
    let dialogRef = this.tgidentityNavigator.addTgidentity();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchTgidentitys();
      }
    });
  }

  editTgidentity(item: TgidentitySummary) {
    let dialogRef = this.tgidentityNavigator.editTgidentity(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteTgidentity(item: TgidentitySummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Tgidentity");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.tgidentityPersist.deleteTgidentity(item.id).subscribe(response => {
          this.tcNotification.success("Tgidentity deleted");
          this.searchTgidentitys();
        }, error => {
        });
      }

    });
  }

  back(): void {
    this.location.back();
  }

  hasUser(item: TgidentitySummary): boolean {
    return !TCUtilsString.isInvalidId(item.user_id);
  }

  createNewUser(item: TgidentitySummary): void {
    this.tgidentityPersist.createUser(item.id).subscribe(response => {
      this.tcNotification.success("user created");
      this.searchTgidentitys();
    });
  }

  assignUser(item: TgidentitySummary): void {
    let dialogRef = this.userNavigator.pickUsers(true);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let userDetail: UserDetail = (<UserDetail[]>result)[0];
        this.tgidentityPersist.assignUser(item.id, userDetail.id).subscribe(response => {
          this.tcNotification.success("user assigned");
          this.searchTgidentitys();
        });
      }
    });
  }

  sendMessage(item: TgidentitySummary) {
    let mdl = this.tcNavigator.textInput(this.appTranslation.getText("general", "message"), "", true, this.appTranslation.getText("general", "send_message"));
    mdl.afterClosed().subscribe(value => {
      if (value) {
        this.tgidentityPersist.sendMessage(item.id, value).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("general", "message_sent"));
        });
      }
    });
  }

  canActivate(item: TgidentitySummary): boolean {
    return this.hasUser(item) && item.state == TelegramUserStates.disabled;
  }

  canDeactivate(item: TgidentitySummary): boolean {
    return this.hasUser(item) && item.state == TelegramUserStates.active;
  }

  activate(item: TgidentitySummary): void {

  }

  deactivate(item: TgidentitySummary): void {

  }


}
