import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TgidentityPickComponent } from './tgidentity-pick.component';

describe('TgidentityPickComponent', () => {
  let component: TgidentityPickComponent;
  let fixture: ComponentFixture<TgidentityPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TgidentityPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TgidentityPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
