import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../../tc/authorization";
import {TCUtilsAngular} from "../../../tc/utils-angular";
import {TCUtilsArray} from "../../../tc/utils-array";
import {TCUtilsDate} from "../../../tc/utils-date";
import {TCNotification} from "../../../tc/notification";
import {TCNavigator} from "../../../tc/navigator";
import {AppTranslation} from "../../../app.translation";

import {TgidentityDetail, TgidentitySummary, TgidentitySummaryPartialList} from "../tgidentity.model";
import {TgidentityPersist} from "../tgidentity.persist";
import {TCUtilsString} from "../../utils-string";


@Component({
  selector: 'app-tgidentity-pick',
  templateUrl: './tgidentity-pick.component.html',
  styleUrls: ['./tgidentity-pick.component.css']
})
export class TgidentityPickComponent implements OnInit {

  tgidentitysData: TgidentitySummary[] = [];
  tgidentitysTotalCount: number = 0;
  tgidentitysSelection: TgidentitySummary[] = [];
  tgidentitysDisplayedColumns: string[] = ["select", "photo_url", "name", "telephone_number", "reg_date", "contactable", "state"];

  tgidentitysSearchTextBox: FormControl = new FormControl();
  tgidentitysIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) tgidentitysPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) tgidentitysSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public tgidentityPersist: TgidentityPersist,
              public dialogRef: MatDialogRef<TgidentityPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("tgidentitys");
    this.tgidentitysSearchTextBox.setValue(tgidentityPersist.tgidentitySearchText);
    //delay subsequent keyup events
    this.tgidentitysSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.tgidentityPersist.tgidentitySearchText = value;
      this.searchTgidentitys();
    });
  }

  ngOnInit() {

    this.tgidentitysSort.sortChange.subscribe(() => {
      this.tgidentitysPaginator.pageIndex = 0;
      this.searchTgidentitys();
    });

    this.tgidentitysPaginator.page.subscribe(() => {
      this.searchTgidentitys();
    });
    //start by loading items
    this.searchTgidentitys();
  }

  searchTgidentitys(): void {
    this.tgidentitysIsLoading = true;
    this.tgidentitysSelection = [];

    this.tgidentityPersist.searchTgidentity(this.tgidentitysPaginator.pageSize,
        this.tgidentitysPaginator.pageIndex,
        this.tgidentitysSort.active,
        this.tgidentitysSort.direction).subscribe((partialList: TgidentitySummaryPartialList) => {
      this.tgidentitysData = partialList.data;
      if (partialList.total != -1) {
        this.tgidentitysTotalCount = partialList.total;
      }
      this.tgidentitysIsLoading = false;
    }, error => {
      this.tgidentitysIsLoading = false;
    });

  }

  markOneItem(item: TgidentitySummary) {
    if(!this.tcUtilsArray.containsId(this.tgidentitysSelection,item.id)){
          this.tgidentitysSelection = [];
          this.tgidentitysSelection.push(item);
        }
        else{
          this.tgidentitysSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.tgidentitysSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  hasUser(item: TgidentitySummary): boolean {
    return !TCUtilsString.isInvalidId(item.user_id);
  }

}
