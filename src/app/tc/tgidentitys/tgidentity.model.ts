import {TCId} from "../../tc/models";

export class TgidentitySummary extends TCId {
  telegram_id: number;
  telephone_number: string;
  username: string;
  first_name: string;
  last_name: string;
  photo_url: string;
  reg_date: number;
  contactable: boolean;
  state: number;
  user_id: string;
}

export class TgidentitySummaryPartialList {
  data: TgidentitySummary[];
  total: number;
}

export class TgidentityDetail extends TgidentitySummary {
  telegram_id: number;
  telephone_number: string;
  username: string;
  first_name: string;
  last_name: string;
  photo_url: string;
  reg_date: number;
  contactable: boolean;
  state: number;
  user_id: string;
}

export class TgidentityDashboard {
  total: number;
}
