import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../../tc/models";

import {TgidentityEditComponent} from "./tgidentity-edit/tgidentity-edit.component";
import {TgidentityPickComponent} from "./tgidentity-pick/tgidentity-pick.component";


@Injectable({
  providedIn: 'root'
})

export class TgidentityNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  tgidentitysUrl(): string {
    return "/tgidentitys";
  }

  tgidentityUrl(id: string): string {
    return "/tgidentitys/" + id;
  }

  viewTgidentitys(): void {
    this.router.navigateByUrl(this.tgidentitysUrl());
  }

  viewTgidentity(id: string): void {
    this.router.navigateByUrl(this.tgidentityUrl(id));
  }

  editTgidentity(id: string): MatDialogRef<TgidentityEditComponent> {
    const dialogRef = this.dialog.open(TgidentityEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addTgidentity(): MatDialogRef<TgidentityEditComponent> {
    const dialogRef = this.dialog.open(TgidentityEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickTgidentitys(selectOne: boolean=false): MatDialogRef<TgidentityPickComponent> {
      const dialogRef = this.dialog.open(TgidentityPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
