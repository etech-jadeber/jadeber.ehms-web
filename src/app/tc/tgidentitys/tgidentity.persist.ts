import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUrlParams, TCUtilsHttp} from "../../tc/utils-http";
import {TCEnum, TCId, TelegramUserStates} from "../../tc/models";
import {TgidentityDashboard, TgidentityDetail, TgidentitySummaryPartialList} from "./tgidentity.model";


@Injectable({
  providedIn: 'root'
})
export class TgidentityPersist {

  tgidentitySearchText: string = "";

  TelegramUserStates: TCEnum[] = [
    new TCEnum(TelegramUserStates.registered, 'registered'),
    new TCEnum(TelegramUserStates.active, 'active'),
    new TCEnum(TelegramUserStates.disabled, 'disabled'),
    new TCEnum(TelegramUserStates.filled, 'basic data filled'),
  ];

  constructor(private http: HttpClient) {
  }

  searchTgidentity(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<TgidentitySummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("tc/tgidentitys", this.tgidentitySearchText, pageSize, pageIndex, sort, order);
    return this.http.get<TgidentitySummaryPartialList>(url);

  }

  filters(): any {
    let fltrs = {};
    fltrs[TCUrlParams.searchText] = this.tgidentitySearchText;
    //add custom filters
    return fltrs;
  }

  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "tgidentitys/do", new TCDoParam("download_all", this.filters()));
  }

  tgidentityDashboard(): Observable<TgidentityDashboard> {
    return this.http.get<TgidentityDashboard>(environment.tcApiBaseUri + "tc/tgidentitys/dashboard");
  }

  getTgidentity(id: string): Observable<TgidentityDetail> {
    return this.http.get<TgidentityDetail>(environment.tcApiBaseUri + "tc/tgidentitys/" + id);
  }

  addTgidentity(item: TgidentityDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "tc/tgidentitys/", item);
  }

  updateTgidentity(item: TgidentityDetail): Observable<TgidentityDetail> {
    return this.http.patch<TgidentityDetail>(environment.tcApiBaseUri + "tc/tgidentitys/" + item.id, item);
  }

  deleteTgidentity(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "tc/tgidentitys/" + id);
  }

  tgidentitysDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/tgidentitys/do", new TCDoParam(method, payload));
  }

  tgidentityDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "tc/tgidentitys/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "tc/tgidentitys/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "tc/tgidentitys/" + id + "/do", new TCDoParam("print", {}));
  }

  createUser(tgIdentityId: string): Observable<{}> {
    return this.tgidentityDo(tgIdentityId, "user_create", {});
  }

  assignUser(tgIdentityId: string, userId: string): Observable<{}> {
    return this.tgidentityDo(tgIdentityId, "user_assign", {"user_id": userId});
  }

  sendMessage(id: string, message: string): Observable<{}> {
    return this.tgidentityDo(id, "send_message", message);
  }

}
