import {Component, OnDestroy, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../authorization";
import {TCUtilsAngular} from "../../utils-angular";
import {TCNotification} from "../../notification";
import {TCUtilsArray} from "../../utils-array";
import {TCNavigator} from "../../navigator";
import {JobPersist} from "../../jobs/job.persist";
import {AppTranslation} from "../../../app.translation";


import {UserDetail} from "../user.model";
import {UserPersist} from "../user.persist";
import {UserNavigator} from "../user.navigator";
import {TCUtilsDate} from "../../utils-date";
import {TCId} from "../../models";


export enum UserTabs {
  overview,
}

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  paramsSubscription: Subscription;
  userLoaded: boolean = false;
  //basics
  userDetail: UserDetail;
  userTabs: typeof UserTabs = UserTabs;
  activeTab: UserTabs = UserTabs.overview;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization: TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public tcUtilsDate: TCUtilsDate,
              public jobPersist: JobPersist,
              public appTranslation: AppTranslation,
              public userNavigator: UserNavigator,
              public  persist: UserPersist) {
    this.tcAuthorization.requireRead("users");
    this.userDetail = new UserDetail();
  }

  ngOnInit() {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

  loadDetails(id: string): void {
    this.persist.getUser(id).subscribe(userDetail => {
      this.userDetail = userDetail;
      this.userLoaded = true;
    }, error => {
      console.error(error);
    });
  }

  editUser(): void {
    let modalRef = this.userNavigator.editUser(this.userDetail.id);
    modalRef.afterClosed().subscribe(modifiedUserDetail => {
      TCUtilsAngular.assign(this.userDetail, modifiedUserDetail);
    }, error => {
      console.error(error);
    });
  }

  printUser(): void {
    this.persist.print(this.userDetail.id).subscribe(printJob => {
      this.jobPersist.followJob(printJob.id, "print user", true);
    });
  }

  back(): void {
    this.location.back();
  }

  otpEnable(): void {
    this.persist.otpEnable(this.userDetail.id).subscribe(response => {
      if (response) {
        this.userDetail.otp = (<TCId>response).id;
      }
    });
  }

  otpReset(): void {
    this.persist.otpReset(this.userDetail.id).subscribe(response => {
      if (response) {
        this.userDetail.otp = (<TCId>response).id;
      }
    });
  }

  otpDisable(): void {
    this.persist.otpDisable(this.userDetail.id).subscribe(response => {
      this.userDetail.otp = "";
    });
  }

}
