import {Component, OnInit, Inject} from '@angular/core';



import {TCNotification} from "../../notification";


import {UserDetail} from "../user.model";
import {UserPersist} from "../user.persist";
import {TCUtilsString} from "../../utils-string";
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  isNew: boolean = false;
  title: string;
  item: UserDetail = new UserDetail();

  constructor(public tcNotification: TCNotification,
              public tcUtilsString: TCUtilsString,
              public dialogRef: MatDialogRef<UserEditComponent>,
              public persist: UserPersist,
              @Inject(MAT_DIALOG_DATA) public id: string) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    if (this.id === null) {
      this.title = "New User";
      this.isNew = true;
      //set necessary defaults
      this.item.sysadmin = false;
      this.item.disabled = false;
    } else {
      this.title = "Edit User";
      this.isNew = false;
      this.isLoadingResults = true;
      this.persist.getUser(this.id).subscribe(item => {
        this.item = item;
        this.isLoadingResults = false;
      }, error => {
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addUser(this.item).subscribe(value => {
      this.tcNotification.success("User added");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateUser(this.item).subscribe(value => {
      this.tcNotification.success("User updated");
      this.dialogRef.close(this.item);
    }, error => {
      this.isLoadingResults = false;
    })

  }
}
