import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";

import {TCUtilsAngular} from "../../utils-angular";
import {TCUtilsArray} from "../../utils-array";
import {TCUtilsDate} from "../../utils-date";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";

import {UserPersist} from "../user.persist";
import {UserNavigator} from "../user.navigator";
import {UserDetail, UserSummary, UserSummaryPartialList} from "../user.model";
import {JobPersist} from "../../jobs/job.persist";
import {TCAsyncJob} from "../../asyncjob";
import {FilePersist} from "../../files/file.persist";
import {AppTranslation} from "../../../app.translation";
import {AsyncJob} from "../../jobs/job.model";
import {interval} from "rxjs";
import {TCAuthorization} from "../../authorization";


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  usersData: UserSummary[] = [];
  usersTotalCount: number = 0;
  usersSelection: UserDetail[] = [];
  usersDisplayedColumns: string[] = ["select", "action", "name", "username", "reg_date", "disabled", "sysadmin", "report"];

  usersSearchTextBox: FormControl = new FormControl();
  usersIsLoading: boolean = false;
  usersIsDownloading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) usersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) usersSort: MatSort;

  constructor(private router: Router,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public tcAuthorization:TCAuthorization,
              public userPersist: UserPersist,
              public userNavigator: UserNavigator,
              public filePersist: FilePersist,
              public appTranslation: AppTranslation,
              public jobPersist: JobPersist,
              public tcAsyncJob: TCAsyncJob,
  ) {

    this.tcAuthorization.requireRead("users");
    this.usersSearchTextBox.setValue(userPersist.userSearchText);
    //delay subsequent keyup events
    this.usersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.userPersist.userSearchText = value;
      this.searchUsers();
    });
  }

  ngOnInit() {

    this.usersSort.sortChange.subscribe(() => {
      this.usersPaginator.pageIndex = 0;
      this.searchUsers();
    });

    this.usersPaginator.page.subscribe(() => {
      this.searchUsers();
    });
    //start by loading items
    this.searchUsers();
  }

  searchUsers(): void {
    this.usersIsLoading = true;
    this.usersSelection = [];

    this.userPersist.searchUser(this.usersPaginator.pageSize, this.usersPaginator.pageIndex, this.usersSort.active, this.usersSort.direction).subscribe((partialList: UserSummaryPartialList) => {
      this.usersData = partialList.data;
      if (partialList.total != -1) {
        this.usersTotalCount = partialList.total;
      }
      this.usersIsLoading = false;
    }, error => {
      this.usersIsLoading = false;
    });

  }

  downloadUsers(): void {
    this.userPersist.download(this.tcUtilsArray.idsList(this.usersSelection)).subscribe(downloadJob => {
      this.usersIsDownloading = true;
      let downloadPoll: AsyncJob = this.jobPersist.followJob(downloadJob.id, "download users",true);
      const attemptsCounter = interval(5000); //every five second
      let ss = attemptsCounter.subscribe(n => {
        if (downloadPoll.is_terminated) {
          ss.unsubscribe();
          this.usersIsDownloading = false;
        }
      });
    });

  }


  addUser(): void {
    let dialogRef = this.userNavigator.addUser();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchUsers();
      }
    });
  }

  editUser(item: UserDetail): void {
    let dialogRef = this.userNavigator.editUser(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }


  deleteUser(item: UserDetail): void {
    let dialogRef = this.tcNavigator.confirmDeletion("User");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.userPersist.deleteUser(item.id).subscribe(response => {
          this.tcNotification.success("User deleted");
          this.searchUsers();
        }, error => {
        });
      }

    });
  }

  sendMessage(item: UserDetail) {
    let mdl = this.tcNavigator.textInput(this.appTranslation.getText("general", "message"), "", true, this.appTranslation.getText("general", "send_message"));
    mdl.afterClosed().subscribe(value => {
      if (value) {
        this.userPersist.sendMessage(item.id, value).subscribe(response => {
          this.tcNotification.success(this.appTranslation.getText("general", "message_sent"));
        });
      }
    });
  }

}
