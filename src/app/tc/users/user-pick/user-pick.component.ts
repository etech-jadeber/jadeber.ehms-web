import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {UserDetail, UserSummary, UserSummaryPartialList} from "../user.model";
import {FormControl} from "@angular/forms";

import {TCUtilsAngular} from "../../utils-angular";
import {TCUtilsArray} from "../../utils-array";
import {TCUtilsDate} from "../../utils-date";
import {TCNotification} from "../../notification";
import {TCNavigator} from "../../navigator";
import {UserPersist} from "../user.persist";
import {debounceTime} from "rxjs/operators";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
@Component({
  selector: 'app-user-pick',
  templateUrl: './user-pick.component.html',
  styleUrls: ['./user-pick.component.css']
})
export class UserPickComponent implements OnInit {

  usersData: UserSummary[] = [];
  usersTotalCount: number = 0;
  usersSelection: UserDetail[] = [];
  usersDisplayedColumns: string[] = ["select", "name", "username", "reg_date", "disabled", "sysadmin"];

  usersSearchTextBox: FormControl = new FormControl();
  usersIsLoading: boolean = false;

  @ViewChild(MatPaginator, { static: true }) usersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) usersSort: MatSort;

  constructor(public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public userPersist: UserPersist,
              public dialogRef: MatDialogRef<UserPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {

    //delay subsequent keyup events
    this.usersSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.userPersist.userSearchText = value;
      this.searchUsers();
    });
  }

  ngOnInit() {

    this.usersSort.sortChange.subscribe(() => {
      this.usersPaginator.pageIndex = 0;
      this.searchUsers();
    });

    this.usersPaginator.page.subscribe(() => {
      this.searchUsers();
    });
    //start by loading items
    this.searchUsers();
  }

  searchUsers(): void {
    this.usersIsLoading = true;
    this.usersSelection = [];

    this.userPersist.searchUser(this.usersPaginator.pageSize,
      this.usersPaginator.pageIndex,
      this.usersSort.active,
      this.usersSort.direction).subscribe((partialList: UserSummaryPartialList) => {
      this.usersData = partialList.data;
      if (partialList.total != -1) {
        this.usersTotalCount = partialList.total;
      }
      this.usersIsLoading = false;
    }, error => {
      this.usersIsLoading = false;
    });

  }

  markOneItem(item: UserDetail) {
    if(!this.tcUtilsArray.containsId(this.usersSelection,item.id)){
      this.usersSelection = [];
      this.usersSelection.push(item);
    }
    else{
      this.usersSelection = [];
    }

  }

  returnSelected(): void {
    this.dialogRef.close(this.usersSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
