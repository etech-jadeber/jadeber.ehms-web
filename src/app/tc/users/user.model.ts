import {TCId} from "../models";
import {TCUtilsString} from "../utils-string";

export class UserDisplay extends TCId {
  username: string;
  name: string;
  email: string;
}

export type Prefixed<T> = {
  [K in keyof T as `et_user_${string & K}`]: T[K];
};

export type PrefixedUser = Prefixed<UserDetail>;

export class UserSummary extends TCId {
  username: string;
  name: string;
  email: string;
  disabled: boolean;
  reg_date: number;
  sysadmin: boolean;
  phone_no:string
  report_key: string;
}

export class UserSummaryPartialList {
  data: UserSummary[];
  total: number;
}

export class UserDetail extends UserSummary {
  identity_source: number;
  username: string;
  name: string;
  email: string;
  disabled: boolean;
  reg_date: number;
  sysadmin: boolean;
  last_login: number;
  otp: string;

  static InvalidUser(): UserDetail {
    let ret = new UserDetail();
    ret.id = TCUtilsString.getInvalidId();
    return ret;
  }
}

export class UserDashboard {
  total: number;
}

export class Passwords {
  pwd_current: string;
  pwd_new: string;
  pwd_repeat: string;
}

export function getPrefixedUserName(user: PrefixedUser){
  return user.et_user_name
}

export function getUserName(user: UserDetail){
  return user.name
}