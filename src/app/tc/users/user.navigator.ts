import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialog,MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../utils-angular";

import {UserEditComponent} from "./user-edit/user-edit.component";
import {UserPersist} from "./user.persist";
import {UserPickComponent} from "./user-pick/user-pick.component";


@Injectable({
  providedIn: 'root'
})

export class UserNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  usersUrl(): string {
    return "/users";
  }

  userUrl(id: string): string {
    return "/users/" + id;
  }

  viewUsers(): void {
    this.router.navigateByUrl(this.usersUrl());
  }

  viewUser(id: string): void {
    this.router.navigateByUrl(this.userUrl(id));
  }

  editUser(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(UserEditComponent, {
      data: id,
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addUser(): MatDialogRef<unknown,any> {
    return this.editUser(null);
  }

  pickUsers(selectOne: boolean=false): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(UserPickComponent, {
      data: selectOne,
      width: TCModalWidths.large
    });
    return dialogRef;
  }
}
