import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../../environments/environment";


import {TCDoParam, TCUtilsHttp} from "../utils-http";
import {Passwords, UserDashboard, UserDetail, UserDisplay, UserSummaryPartialList} from "./user.model";
import {CommentDetail, TCId} from "../models";
import {TCUtilsArray} from "../utils-array";
import {EventlogDetail} from "../eventlogs/eventlog.model";
import {TCUserInfo} from "../authentication";
import {TCUtilsString} from "../utils-string";


@Injectable({
  providedIn: 'root'
})
export class UserPersist {

  userSearchText: string = "";
  userDisplays: UserDisplay[] = [];
  userIds: TCId[] = [];

  constructor(private http: HttpClient,
              public tcUtilsString: TCUtilsString,
              public tcUtilsArray: TCUtilsArray) {
    this.userIds.push(new TCId(this.tcUtilsString.invalid_id));

    let unknownUser = new UserDisplay();
    unknownUser.id = this.tcUtilsString.invalid_id;
    unknownUser.name = "Unknown User";
    this.userDisplays.push(unknownUser);
  }

  searchUser(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<UserSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("users", this.userSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<UserSummaryPartialList>(url);

  }

  userDashboard(): Observable<UserDashboard> {
    return this.http.get<UserDashboard>(environment.tcApiBaseUri + "users/dashboard");
  }

  getUser(id: string): Observable<UserDetail> {
    return this.http.get<UserDetail>(environment.tcApiBaseUri + "users/" + id);
  }

  deleteUser(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "users/" + id);
  }

  addUser(item: UserDetail): Observable<{}> {
    return this.http.post<UserDetail>(environment.tcApiBaseUri + "users/", item);
  }

  updateUser(item: UserDetail): Observable<UserDetail> {
    return this.http.patch<UserDetail>(environment.tcApiBaseUri + "users/" + item.id, item);
  }

  updateLanguage(langCode: string): Observable<{}> {
    return this.http.patch<{}>(environment.tcApiBaseUri + "me", {"lang": langCode});
  }

  meDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "me/do", new TCDoParam(method, payload));
  }

  pwdChange(passwords: Passwords): Observable<{}> {
    return this.meDo("pwd_change", passwords);
  }

  usersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "users/do", new TCDoParam(method, payload));
  }

  userDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "users/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "users/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "users/" + id + "/do", new TCDoParam("print", {}));
  }

  otpEnable(user_id): Observable<{}> {
    return this.userDo(user_id, "otp_enable", {});
  }

  otpReset(user_id): Observable<{}> {
    return this.userDo(user_id, "otp_reset", {});
  }

  otpDisable(user_id): Observable<{}> {
    return this.userDo(user_id, "otp_disable", {});
  }

  loadUsersOfEvents(logs: EventlogDetail[]) {
    for (let log of logs) {
      this.loadUserDisplay(log.operator_id);
    }
  }

  loadUsersOfComments(comments: CommentDetail[]) {
    for (let comment of comments) {
      this.loadUserDisplay(comment.commented_by);
    }
  }

  loadUserDisplay(user_id): void {
    if (!user_id) {
      return;
    }
    if (this.tcUtilsArray.containsId(this.userIds, user_id)) {
      return;
    }

    this.userIds.push(new TCId(user_id));

    this.http.get<UserDisplay>(environment.tcApiBaseUri + "users/" + user_id + "/display").subscribe(userDisplay => {
      if (!this.tcUtilsArray.containsId(this.userDisplays, user_id)) {
        this.userDisplays.push(userDisplay);
      }
    });
  }

  reloadUserInfo(): Observable<TCUserInfo> {
    console.log("here");
    
    
    let url = environment.tcApiBaseUri + "user-info";
    return this.http.get<TCUserInfo>(url);
  }

  sendMessage(id: string, message: string): Observable<{}> {
    return this.userDo(id, "send_message", message);
  }

}
