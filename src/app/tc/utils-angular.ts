import {MediaObserver} from "@angular/flex-layout";
import {Injectable} from '@angular/core';

export enum TCModalWidths {
  small = "250px",
  medium = "550px",
  large = "900px",
  xtra_large = "80vw"
}

@Injectable({
  providedIn: 'root',
})

export class TCUtilsAngular {
  smallDevice: boolean = false;
  sideNavMode: string = 'side';

  constructor(private media: MediaObserver) {
    this.media.asObservable().subscribe((mediaChang) => {
      this.smallDevice = !this.media.isActive('gt-sm');
      this.sideNavMode = this.smallDevice ? 'over' : 'side';
    });
  }

  static clone(original: Object): Object {
    return JSON.parse(JSON.stringify(original));
  }

  static assign(original: Object, updated: Object) {
    for (var attribute in updated) {
      original[attribute] = updated[attribute];
    }
  }

  static findDiff = function (original, edited): Object {
    let diff = {}
    for (var key in original) {
      if (original[key] !== edited[key]) {
        diff[key] = edited[key];
      }
    }
    return diff;
  }
}


