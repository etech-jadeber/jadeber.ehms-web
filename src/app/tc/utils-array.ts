import {TCEnum, TCId} from "./models";

import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root',
})

export class TCUtilsArray {
  pageSizeOptions: number[] = [5, 10, 25, 50];
  defaultPageSize: number = 10;
  defaultPageSizeSmall: number = 5;

  getEnum(list: TCEnum[], idVal: number): TCEnum {
    return list.find(x => x.id == idVal);
  }

  getById(list: any[], idVal: string): any {
    return list.find(x => x.id == idVal);
  }

  getByKey(list: any[], key: string, idVal: string): any {
    return list.find(x => x[key] == idVal);
  }

  removeItemById(list: any[], id: string, key: string = "id") {
    let index = list.findIndex(x => x[key] == id);
    while (index >= 0) {
      list.splice(index, 1);
      index = list.findIndex(x => x[key] == id);
    }
  }

  removeItem(list: any[], item: any, key:string="id") {
    this.removeItemById(list, item[key], key);
  }

  getByProperty(list: Array<any>, fn: Function): Array<any> {
    if (!list) {
      return [];
    }
    return list.filter(x => fn(x));
  }

  getMax(list: Array<any>, fn: Function) {
    return Math.max.apply(Math, list.map(function (o) {
      return fn(o);
    }));
  }

  getMin(list: Array<any>, fn: Function) {
    return Math.min.apply(Math, list.map(function (o) {
      return fn(o);
    }));
  }

  contains(list: any[], item: any, key: string = "id"): boolean {
    return this.containsId(list, item[key], key);
  }

  containsId(list: any[], id: string, key: string = "id"): boolean {
    return list.length > 0 ? list.some(x => x[key] === id) : false;
  }

  containsByKey(list: any[], key: string, value: string): boolean {
    return list.length > 0 ? list.some(x => x[key] === value) : false;
  }

  isAllSelected(data: any[], selection: any[]): boolean {
    if (!data || !selection) {
      return false;
    }
    return data.length == selection.length && data.length != 0;
  }

  toggleSelectAll(data: any[], selection: any[], key: string = "id"): void {
    this.isAllSelected(data, selection) ? selection.splice(0, selection.length) : data.forEach(row => {
      if (!this.contains(selection, row, key)) {
        selection.push(row)
      }
    });
  }

  toggleSelection(selection: any[], item: any, key: string = "id") {
    this.contains(selection, item, key) ? this.removeItem(selection, item, key) : selection.push(item);
  }

  idsList(items: any[]): string[] {
    let ids = [];
    items.forEach(item => {
      ids.push(item.id);
    });
    return ids;
  }
}
