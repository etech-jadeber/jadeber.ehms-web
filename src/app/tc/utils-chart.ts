import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root',
})

export class TCUtilsChart {

  chartColorScheme = {domain: [ '#E64C3C', '#F39C13', '#3497DA']};

  constructor() {
  }
}
