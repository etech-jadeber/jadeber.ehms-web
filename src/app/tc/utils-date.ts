import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {Moment} from "moment";

interface TimeMinutes {time: string, minutes: number}


@Injectable({
  providedIn: 'root',
})

export class TCUtilsDate {

  toDate(timestamp: number): Date {
    return timestamp != undefined ? new Date(timestamp * 1000) : new Date();
  }

  toTimestamp() {
    return Math.round(new Date().getTime() / 1000);
  }

  toFormDate(timestamp: number) {
    return timestamp != undefined ? new Date(timestamp * 1000) : '';
  }

  toMomentDate(epoch: number): Moment {
    return moment(this.toFormDate(epoch));
  }

  formatDateFromEpoch(timestamp: number) {
    if(timestamp == null){
      return ''
    }
    const date = new Date(timestamp * 1000); // Convert milliseconds to seconds
    const day = ('0' + date.getDate()).slice(-2); // Ensure two digits for day
    const month = date.toLocaleString('default', { month: 'short' }); // Get short month name
    const year = date.getFullYear(); // Get full year
    return `${day} ${month} ${year}`;
  }


  toNativeFormDateAndTime(timestamp: number) {
    if(timestamp == undefined)
      return ''
    const tzOffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(timestamp * 1000 - tzOffset)).toISOString().slice(0, 16)
  }

  formatDate(timestamp: number): string {
    if(timestamp == null){
      return ''
    }
    const date = new Date(timestamp * 1000);

    // Get date parts
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // Months are zero-indexed
    const year = date.getFullYear();

    // Get time parts
    let hours = date.getHours();
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const ampm = hours >= 12 ? 'PM' : 'AM';

    // Convert to 12-hour format
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    const strHours = hours.toString().padStart(2, '0');

    return `${month}/${day}/${year}, ${strHours}:${minutes} ${ampm}`;
  }

  toNativeFormDate(timestamp: number) {
    if(timestamp == undefined)
      return ''
    const tzOffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    return (new Date(timestamp * 1000 - tzOffset)).toISOString().split('T')[0]
  }

  toDateString(timestamp: number): string {
    return timestamp ? new Date(timestamp * 1000).toDateString() : "";
  }

  toTimeStamp(date: Date): number {
    if(date == null){
      return null;
    }
    return Math.round(date.valueOf() / 1000);
  }

  static toTimeStamp(date: Date): number {
    if(date == null){
      return null;
    }
    return Math.round(date.valueOf() / 1000);
  }

  toTimeStampFromString(date: string): number {
    if(date == null){
      return null;
    }
    return new Date(date).getTime() / 1000;
  }

  toTimestampFromHourMin(hourMin: string): number {
    if(hourMin == null){
      return null;
    }
    let time= hourMin.split(':');
    return parseInt(time[0])*3600+ parseInt(time[1])*60
  }

  toHourMinFromTimestamp(timestamp: number): string {
    if(timestamp == null){
      return null;
    }
    return this.format(Math.floor(timestamp/3600))+":"+this.format((timestamp/60)%60);
  }

  hourMinutesDuration(from_time: string, to_time: string): string {
    if(from_time == null || to_time == null){
      return "";
    }
    const fromTimestamp = this.toTimestampFromHourMin(from_time);
    const toTimestamp = this.toTimestampFromHourMin(to_time);
    const duration = toTimestamp - fromTimestamp;
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor((duration % 3600) / 60)
    if(hours == 0){
      return `${minutes} Minute${minutes > 1 ? 's':''}`
    }
    if(minutes == 0){
      return `${hours} Hour${hours > 1 ? 's':''}`
    }
    return `${hours} Hour${hours > 1 ? 's':''} and ${minutes} Minute${minutes > 1 ? 's':''}`
  }

  format(num: number):string{
    let value=num.toString();
    if(value.length==1){
      value='0'+value;
    }
    return value;
  }

  toFormDateTime(dateTimeString?: string) {
      let now = dateTimeString ? new Date(dateTimeString) : new Date();
      let month = ('0' + (now.getMonth() + 1)).slice(-2);
      let day = ('0' + now.getDate()).slice(-2);
      let min = ('0' + now.getMinutes()).slice(-2);
      let hour = ('0' + now.getHours()).slice(-2);
      return `${now.getFullYear()}-${month}-${day}T${hour}:${min}`;
  }

  FromFormToTimeStamp(value: any): number {
    if(value && value._d){
      return this.toTimeStamp(value._d)
    } else if(value instanceof Date){
      return this.toTimeStamp(value)
    }
    return null;
  }

  toDateTimeStamp(date: Date): number {
    date.setUTCHours(0, 0, 0);
    return Math.round(date.valueOf() / 1000);
  }

  getAgeFromTime(dateOfBirth: Date | string | number, fromTime: Date | string | number, max:number = 5) {
    const fromDate = new Date(typeof fromTime === 'number' ? fromTime * 1000 : fromTime);
    const birthDate = new Date(typeof dateOfBirth === 'number' ? dateOfBirth * 1000 : dateOfBirth);
    let age = fromDate.getFullYear() - birthDate.getFullYear();
    const monthDifference = fromDate.getMonth() - birthDate.getMonth();

    if (monthDifference < 0 || (monthDifference === 0 && fromDate.getDate() < birthDate.getDate())) {
      age--;
    }

    let months = fromDate.getMonth() - birthDate.getMonth();
    if (months < 0) {
      months = 12 + months;
    }

    const ageString = age > 0 ? age + " Year" + (age > 1 ? "s" : "") : "";
    const monthsString = months > 0 ? months + " Month" + (months > 1 ? "s" : "") : "";

    if(age > max){
      return ageString;
    }
    if (ageString && monthsString) {
      return ageString + " and " + monthsString;
    } else {
      return ageString + monthsString;
    }
  }

  getAgeString(dateOfBirth: Date | string | number, max: number = 5) {
    return this.getAgeFromTime(dateOfBirth, new Date(), max);
  }

  generateIntervals(): TimeMinutes[] {
    const intervals: TimeMinutes[] = [];
    for (let hour = 0; hour < 24; hour++) {
      for (let minute = 0; minute < 60; minute += 15) {
        const totalMinutes = hour * 60 + minute;
        intervals.push({
          time: `${String(hour).padStart(2, '0')}:${String(minute).padStart(2, '0')}`,
          minutes: totalMinutes
        });
      }
    }
    return intervals;
  }

  getAvailableIntervals(unavailableMinutes: number[]): number[] {
    const allIntervals = this.generateIntervals();
    const unavailableSet = new Set(unavailableMinutes);
    return allIntervals.filter(interval => !unavailableSet.has(interval.minutes)).map(value => value.minutes);
  }

  calculateWeeksAndDays(startDate: number, endDate: number): { weeks: number, days: number } {
    const millisecondsPerDay = 60 * 60 * 24;

    const timeDiff = endDate - startDate;
    const daysDiff = Math.floor(timeDiff / millisecondsPerDay);

    const weeks = Math.floor(daysDiff / 7);
    const remainingDays = daysDiff % 7;

    return { weeks, days: remainingDays };
}

  calculateDaysDifference(startDate: number, endDate?: number) {
    if(!endDate){
      endDate = this.toTimestamp()
    }

    // Calculate the difference in milliseconds
    const timeDifference = endDate - startDate;

    // Convert milliseconds to days (1 day = 24 * 60 * 60 seconds)
    return Math.round(timeDifference / (60 * 60 * 24));
  }

calculateWeeks(startDate: number, endDate: number): number {
  const millisecondsPerDay = 60 * 60 * 24;

  const timeDiff = endDate - startDate;
  const daysDiff = Math.floor(timeDiff / millisecondsPerDay);

  const weeks = daysDiff / 7;

  return weeks;
}

getWeeksAndDays(week: number): { weeks: number, days: number } {
  const weeks = Math.floor(week)
  const remainingDays = (week % weeks || 0)

  return { weeks, days: Math.round(remainingDays * 7) };
}

getWeek(weeks: number, day: number) {
  const days = day / 7
  return weeks + days
}

  daysToYearsMonthsDays(days: number): { years: number, months: number, days: number } {
    const years = Math.floor(days / 365);
    const months = Math.floor((days % 365) / 30);
    const remainingDays = days - (years * 365 + months * 30);
    return { years, months, days: remainingDays };
  }

  convertDays(days: number): string {
    if (days % 365 === 0) {
      const years = days / 365;
      return `${years} year${years > 1 ? 's' : ''}`;
    } else if (days % 30 === 0) {
      const months = days / 30;
      return `${months} month${months > 1 ? 's' : ''}`;
    } else {
      return `${days} day${days > 1 ? 's' : ''}`;
    }
  }

  calculateAge(dateOfBirth) {
    var today = new Date();
    var birthDate = new Date(dateOfBirth);
    var age = today.getFullYear() - birthDate.getFullYear();
    var monthDifference = today.getMonth() - birthDate.getMonth();
    var dayDifference = today.getDate() - birthDate.getDate();
    if (dayDifference < 0 || (dayDifference === 0 && today.getDate() < birthDate.getDate())) {
      monthDifference--;
    }

    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }

    if (monthDifference < 0) {
      monthDifference = 12 + monthDifference;
    }

    if (dayDifference < 0) {
      dayDifference = 30 + dayDifference;
    }

    return { year: age, months: monthDifference , day: dayDifference};
  }

  fromTcDate(tcdt: TCDate): Date {
    //TCDate: uses month 1-12
    return new Date(tcdt.year, tcdt.month-1, tcdt.date);
  }

  toTcDate(dt: Date): TCDate {
    //TCDate: uses month 1-12
    return new TCDate(dt.getFullYear(), dt.getMonth()+1, dt.getDate());
  }

  now(): Date {
    return new Date();
  }

  /**
   * Checks if given time is in the past.
   * @param selectedDate the date we want to check
   * @note Compares two dates based on only YY/MM/DD
   */
   isPast(selectedDate: Date): boolean {
    var now = this.now();
    now.setHours(0, 0, 0, 0);  //to make sure comparison is only done with YY/MM/DD
    if (selectedDate < now) {
      return true;
    }
    return false;
  }

  /**
   * converts mysql datetime object to js date
   * @param mySqlDateObject
   */
  mySqlDateTimeToJsDate(mySqlDateObject: any) {
    return new Date(mySqlDateObject.year, mySqlDateObject.monthValue, mySqlDateObject.dayOfMonth);
  }
}

export class TCDate {

  constructor(public year: number,
              public month: number,
              public date: number) {
  }
}


export const TC_DATE_FORMATS = {
  parse: {
    dateInput: ['DD MMM YYYY']
  },
  display: {
    dateInput: 'DD MMM YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
