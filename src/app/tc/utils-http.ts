import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';

import {environment} from "../../environments/environment";

import {TCUtilsString} from "./utils-string";

export const TCUrlParams = {
  searchText: "st",
  sortColumn: "sc",
  sortOrder: "so",
  pageSize: "ps",
  pageIndex: "pi",
  searchColumn: "search_column",
};

export class TCDoParam {
  constructor(public method: string, public payload: any) {

  }
}

@Injectable({
  providedIn: 'root',
})

export class TCUtilsHttp {

  static defaultPageSize: number = 10;

  constructor(httpClient: HttpClient) {

  }

  static buildSearchUrl(resourceType: string, searchText: string, pageSize: number = this.defaultPageSize, pageIndex: number = 0, sort: string = '', order: string = '') {
    let url: string = environment.tcApiBaseUri + resourceType;
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.searchText + "=" + encodeURIComponent(searchText), "?");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageSize + "=" + encodeURIComponent(pageSize.toString()), "&");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageIndex + "=" + encodeURIComponent(pageIndex.toString()), "&");
    if (sort) {
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortColumn + "=" + encodeURIComponent(sort), "&");
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortOrder + "=" + encodeURIComponent(order), "&");
    }

    return encodeURI(url);
  }

  static buildSearchWithColumnUrl(resourceType: string, searchText: string, pageSize: number = this.defaultPageSize, pageIndex: number = 0, sort: string = '', order: string = '', searchColumn: string = "") {
    let url: string = environment.tcApiBaseUri + resourceType;
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.searchText + "=" + encodeURIComponent(searchText), "?");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.searchColumn + "=" + encodeURIComponent(searchColumn), "&");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageSize + "=" + encodeURIComponent(pageSize.toString()), "&");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageIndex + "=" + encodeURIComponent(pageIndex.toString()), "&");
    if (sort) {
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortColumn + "=" + encodeURIComponent(sort), "&");
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortOrder + "=" + encodeURIComponent(order), "&");
    }

    return encodeURI(url);
  }

  static buildSocketSearchUrl(resourceType: string, searchText: string, pageSize: number = this.defaultPageSize, pageIndex: number = 0, sort: string = '', order: string = '') {
    let url: string = environment.socketUrl + resourceType;
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.searchText + "=" + searchText, "?");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageSize + "=" + pageSize.toString(), "&");
    url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.pageIndex + "=" + pageIndex.toString(), "&");
    if (sort) {
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortColumn + "=" + sort, "&");
      url = TCUtilsString.appendWithDelimiter(url, TCUrlParams.sortOrder + "=" + order, "&");
    }

    return encodeURI(url);
  }


}
