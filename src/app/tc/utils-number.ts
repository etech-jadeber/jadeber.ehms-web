import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root',
})

export class TCUtilsNumber {

  convertToDecimalPlaces(number: number, place: number = 2): number {
    const convertedNumber = parseFloat(number.toFixed(place));
    return convertedNumber;
  }

  constructor() {
  }
}
