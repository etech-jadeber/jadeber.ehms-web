import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root',
})

export class TCUtilsString {

  emailPattern: string = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,3}$";
  invalid_id: string = "b792fd55-43cd-4e8d-9e14-b3952520e2e8";
  idPattern: string = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"
  phonePattern: string = "^(9|7)[0-9]{8}$"


  constructor(private router: Router) {

  }

  isValidEmail(email: string): boolean {
    return RegExp(this.emailPattern).test(email);
  }

  valueIfNot<T>(value: T, comp: T){
    if(value == comp){
      return null
    }else {
      return value
    }
  }

  isValidId(id: string): boolean {
    return id != this.invalid_id && RegExp(this.idPattern).test(id)
  }

  isValidPhone(phone: string): boolean {
    return RegExp(this.phonePattern).test(phone);
  }

  isNumber(value) {
    return !isNaN(value);
  }


  static appendUrlParameter(url: string, key: string, value: string): string {
    return this.appendWithDelimiter(url, encodeURIComponent(key) + "=" + encodeURIComponent(value), url.includes("?") ? "&" : "?");
  }

  static applyFilterParameter(filter: {  }, values: any): { } {
    for (let value in values){
      if(values[value]){
        filter[value] = values[value];
      }
    }
    return filter
  }

  static appendUrlParameters(url: string, values: any): string {
    for (let value in values){
      if(values[value] != undefined && values[value] !== ""){
        url = this.appendUrlParameter(url, value, values[value])
      }
    }
    return url
  }

  static appendWithDelimiter(src: string, toAdd: string, delimiter: string): string {
    return (src == null || src == "") ? toAdd : src + delimiter + toAdd;
  }

  static isInvalidId(id): boolean {
    return id === this.getInvalidId();
  }

  static getInvalidId(): string {
    // @ts-ignore
    return new TCUtilsString().invalid_id;
  }

  static getRandomString(length: number = 10) {
    return Math.random().toString(36).substring(length);
  }

  static convertToEthiopiaCurrency(birr: number): string {
    return birr.toLocaleString('en-ET') + " ETB";
  }

  pictureFullUri(relativeUri: string): string {
    if (relativeUri == undefined) {
      return "";
    }

    if (relativeUri.startsWith("http")) {
      return relativeUri;
    }
    let fullPath = environment.tcApiBaseUri + "/" + relativeUri;
    return fullPath.replace("///", "/");
  }

  isActiveTab(routeStart: string): boolean {
    let replaceEndSlashes = (value: string, replace: string = '') => value.replace(/(\/)*$/, replace);
    const url = replaceEndSlashes(this.router.url, '/');
    routeStart = replaceEndSlashes(routeStart, '/');
    return url.startsWith(routeStart);
  }

  valueOrDefaultString(value: string, prefix: string = "", suffix: string=""): string{
    if(value){
      return this.valueOrDefault(prefix, "") + value + this.valueOrDefault(suffix, "")
    }
    return ""
  }

  valueOrDefault(value: string, def: string): string{
    if(value){
      return value
    }
    return def
  }

  getReferenceRange(inputString: string): { min_range: number, max_range: number } {
    if (!inputString){
      return null;
    }
    const rangePatternWithBrackets = /\(.*?(\d+\.?\d*)\s*-\s*(\d+\.?\d*).*?\)/; // Match format (start - end)
    const rangePatternGreaterThan = />(\d+\.?\d*)/; // Match format >number
    const rangePatternLessThan = /<(\d+\.?\d*)/; // Match format <number

    const matchBrackets = inputString.match(rangePatternWithBrackets);
    const matchGreaterThan = inputString.match(rangePatternGreaterThan);
    const matchLessThan = inputString.match(rangePatternLessThan);

    if (matchBrackets) {
      const start = parseFloat(matchBrackets[1]);
      const end = parseFloat(matchBrackets[2]);
      return { min_range: start, max_range: end };
    } else if (matchGreaterThan) {
      const value = parseFloat(matchGreaterThan[1]);
      return { min_range: value, max_range: undefined };
    } else if (matchLessThan) {
      const value = parseFloat(matchLessThan[1]);
      return { max_range: value, min_range: undefined };
    } else {
      return null; // No valid range format found in the string
    }
  }

}
