import {Component, Injectable, OnInit} from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';



@Injectable({
  providedIn: 'root',
})
export class ValidationMessagesService {
  msgs: string[] = [];
}


@Component({
  selector: 'app-validation-messages',
  templateUrl: './validation-messages.component.html',
  styleUrls: ['./validation-messages.component.css']
})
export class ValidationMessagesComponent implements OnInit {

  constructor(public validationMessagesService: ValidationMessagesService,
              public snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

  onOk(): void {
    this.validationMessagesService.msgs = [];
    this.snackBar.dismiss();
  }

}
