import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRadTemplatesDetailComponent } from './test-rad-templates-detail.component';

describe('TestRadTemplatesDetailComponent', () => {
  let component: TestRadTemplatesDetailComponent;
  let fixture: ComponentFixture<TestRadTemplatesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestRadTemplatesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRadTemplatesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
