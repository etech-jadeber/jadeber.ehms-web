import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { TCAuthorization } from 'src/app/tc/authorization';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TestRadTemplateDetail } from '../test-rad-templates.model';
import { TestRadTemplateNavigator } from '../test-rad-templates.navigator';
import { TestRadTemplatePersist } from '../test-rad-templates.persist';
import { Location } from '@angular/common';

export enum TestRadTemplateTabs {
  overview,
}

export enum PaginatorIndexes {}

@Component({
  selector: 'app-test-rad-templates-detail',
  templateUrl: './test-rad-templates-detail.component.html',
  styleUrls: ['./test-rad-templates-detail.component.scss']
})
export class TestRadTemplatesDetailComponent implements OnInit {

  paramsSubscription: Subscription;
  testradtemplateIsLoading: boolean = false;

  TestRadTemplateTabs: typeof TestRadTemplateTabs = TestRadTemplateTabs;
  activeTab: TestRadTemplateTabs = TestRadTemplateTabs.overview;
  //basics
  testradtemplateDetail: TestRadTemplateDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public jobPersist: JobPersist,
    public appTranslation: AppTranslation,
    public testradtemplateNavigator: TestRadTemplateNavigator,
    public testradtemplatePersist: TestRadTemplatePersist
  ) {
    this.tcAuthorization.requireRead('test-rad-templates');
    this.testradtemplateDetail = new TestRadTemplateDetail();
  }
  ngOnInit() {
    this.tcAuthorization.requireRead('test-rad-templates');
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe((param) => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
  }

  loadDetails(id: string): void {
    this.testradtemplateIsLoading = true;
    this.testradtemplatePersist.getTestRadTemplate(id).subscribe(
      (testradtemplateDetail) => {
        this.testradtemplateDetail = testradtemplateDetail;
        this.testradtemplateIsLoading = false;
      },
      (error) => {
        console.error(error);
        this.testradtemplateIsLoading = false;
      }
    );
  }

  reload() {
    this.loadDetails(this.testradtemplateDetail.id);
  }
  editTestRadTemplate(): void {
    let modalRef = this.testradtemplateNavigator.edittestRadTemplate(
      this.testradtemplateDetail.id
    );
    modalRef.afterClosed().subscribe(
      (testradtemplateDetail) => {
        TCUtilsAngular.assign(this.testradtemplateDetail, testradtemplateDetail);
      },
      (error) => {
        console.error(error);
      }
    );
  }

  printBirth(): void {
    this.testradtemplatePersist
      .print(this.testradtemplateDetail.id)
      .subscribe((printJob) => {
        this.jobPersist.followJob(printJob.id, 'print testradtemplate', true);
      });
  }

  back(): void {
    this.location.back();
  }

}
