import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRadTemplatesEditComponent } from './test-rad-templates-edit.component';

describe('TestRadTemplatesEditComponent', () => {
  let component: TestRadTemplatesEditComponent;
  let fixture: ComponentFixture<TestRadTemplatesEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestRadTemplatesEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRadTemplatesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
