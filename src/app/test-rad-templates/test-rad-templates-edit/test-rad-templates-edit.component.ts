import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppTranslation } from 'src/app/app.translation';
import { RadiologyResultSettingsDetail } from 'src/app/radiology_result_settings/radiology_result_settings.model';
import { RadiologyResultSettingsNavigator } from 'src/app/radiology_result_settings/radiology_result_settings.navigator';
import { TCAuthorization } from 'src/app/tc/authorization';
import { TCIdMode, TCModalModes } from 'src/app/tc/models';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { TestRadTemplateDetail } from '../test-rad-templates.model';
import { TestRadTemplateNavigator } from '../test-rad-templates.navigator';
import { TestRadTemplatePersist } from '../test-rad-templates.persist';

@Component({
  selector: 'app-test-rad-templates-edit',
  templateUrl: './test-rad-templates-edit.component.html',
  styleUrls: ['./test-rad-templates-edit.component.scss']
})
export class TestRadTemplatesEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  templateName: string;
  testradtemplateDetail: TestRadTemplateDetail;
  constructor(
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcUtilsString: TCUtilsString,
    public appTranslation: AppTranslation,
    public dialogRef: MatDialogRef<TestRadTemplatesEditComponent>,
    public persist: TestRadTemplatePersist,
    public templateNavigator: RadiologyResultSettingsNavigator,
    public tcUtilsDate: TCUtilsDate,

    @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode
  ) {}

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }
  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
      this.tcAuthorization.requireCreate('testradtemplates');
      this.title =
        this.appTranslation.getText('general', 'new') + ' ' + 'testradtemplate';
      this.testradtemplateDetail = new TestRadTemplateDetail();
      //set necessary defaults
      this.testradtemplateDetail.test_id = this.idMode.id;
    } else {
      this.tcAuthorization.requireUpdate('testradtemplates');
      this.title =
        this.appTranslation.getText('general', 'edit') +
        ' ' +
        ' ' +
        'testradtemplate';
      this.isLoadingResults = true;
      this.persist.getTestRadTemplate(this.idMode.id).subscribe(
        (testradtemplateDetail) => {
          this.testradtemplateDetail = testradtemplateDetail;
          this.isLoadingResults = false;
        },
        (error) => {
          console.log(error);
          this.isLoadingResults = false;
        }
      );
    }
  }
  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addTestRadTemplate(this.testradtemplateDetail).subscribe(
      (value) => {
        this.tcNotification.success('testradtemplate added');
        this.testradtemplateDetail.id = value.id;
        this.dialogRef.close(this.testradtemplateDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  searchTemplates() {
    let dialoRef = this.templateNavigator.pickRadiologyResultSettingss(true, this.testradtemplateDetail.test_id)
    dialoRef.afterClosed().subscribe(
      (template: RadiologyResultSettingsDetail[]) => {
        if (template.length){
          this.templateName = template[0].template_name
          this.testradtemplateDetail.template_id = template[0].id
        }
      }
    )

  }

  onUpdate(): void {
    this.isLoadingResults = true;

    this.persist.updateTestRadTemplate(this.testradtemplateDetail).subscribe(
      (value) => {
        this.tcNotification.success('testradtemplate updated');
        this.dialogRef.close(this.testradtemplateDetail);
      },
      (error) => {
        this.isLoadingResults = false;
      }
    );
  }

  canSubmit(): boolean {
    if (this.testradtemplateDetail == null) {
      return false;
    }

    if (this.testradtemplateDetail.test_id == null) {
      return false;
    }
    if (this.testradtemplateDetail.template_id== null) {
      return false;
    }
    return true;
  }

}
