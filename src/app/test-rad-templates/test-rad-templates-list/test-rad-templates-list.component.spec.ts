import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRadTemplatesListComponent } from './test-rad-templates-list.component';

describe('TestRadTemplatesListComponent', () => {
  let component: TestRadTemplatesListComponent;
  let fixture: ComponentFixture<TestRadTemplatesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestRadTemplatesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRadTemplatesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
