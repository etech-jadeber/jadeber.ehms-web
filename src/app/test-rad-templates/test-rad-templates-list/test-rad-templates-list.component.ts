import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TestRadTemplateSummary, TestRadTemplateSummaryPartialList } from '../test-rad-templates.model';
import { TestRadTemplateNavigator } from '../test-rad-templates.navigator';
import { TestRadTemplatePersist } from '../test-rad-templates.persist';
import { Location } from '@angular/common';
import { RadiologyResultSettingsNavigator } from 'src/app/radiology_result_settings/radiology_result_settings.navigator';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';

@Component({
  selector: 'app-test-rad-templates-list',
  templateUrl: './test-rad-templates-list.component.html',
  styleUrls: ['./test-rad-templates-list.component.scss']
})
export class TestRadTemplatesListComponent implements OnInit {

  testradtemplatesData: TestRadTemplateSummary[] = [];
  testradtemplatesTotalCount: number = 0;
  testradtemplateSelectAll: boolean = false;
  testradtemplateSelection: TestRadTemplateSummary[] = [];

  testradtemplatesDisplayedColumns: string[] = [
    'select',
    'action',
    'template_id',
  ];
  testradtemplateSearchTextBox: FormControl = new FormControl();
  testradtemplateIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  testradtemplatesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) testradtemplatesSort: MatSort;
  @Input() lab_test_id: string;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public testradtemplatePersist: TestRadTemplatePersist,
    public testradtemplateNavigator: TestRadTemplateNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public templateNavigator: RadiologyResultSettingsNavigator,
    public templatePersist: RadiologyResultSettingsPersist,
  ) {
    this.tcAuthorization.requireRead('test_rad_templates');
    this.testradtemplateSearchTextBox.setValue(
      testradtemplatePersist.testradtemplateSearchText
    );
    //delay subsequent keyup events
    this.testradtemplateSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.testradtemplatePersist.testradtemplateSearchText = value;
        this.searchTestRadTemplates();
      });
  }
  ngOnInit() {
    this.testradtemplatePersist.lab_test_id = this.lab_test_id
    this.testradtemplatesSort.sortChange.subscribe(() => {
      this.testradtemplatesPaginator.pageIndex = 0;
      this.searchTestRadTemplates(true);
    });

    this.testradtemplatesPaginator.page.subscribe(() => {
      this.searchTestRadTemplates(true);
    });
    //start by loading items
    this.searchTestRadTemplates();
  }

  searchTestRadTemplates(isPagination: boolean = false): void {
    let paginator = this.testradtemplatesPaginator;
    let sorter = this.testradtemplatesSort;

    this.testradtemplateIsLoading = true;
    this.testradtemplateSelection = [];

    this.testradtemplatePersist
      .searchTestRadTemplate(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: TestRadTemplateSummaryPartialList) => {
          this.testradtemplatesData = partialList.data;
          this.testradtemplatesData.forEach(
            testTemplate => {
              this.templatePersist.getRadiologyResultSettings(testTemplate.template_id).subscribe(
                template => {
                  testTemplate.template_name = template.template_name
                }
              )
            }
          )
          if (partialList.total != -1) {
            this.testradtemplatesTotalCount = partialList.total;
          }
          this.testradtemplateIsLoading = false;
        },
        (error) => {
          this.testradtemplateIsLoading = false;
        }
      );
  }
  downloadTestRadTemplates(): void {
    if (this.testradtemplateSelectAll) {
      this.testradtemplatePersist.downloadAll().subscribe((downloadJob) => {
        this.jobPersist.followJob(
          downloadJob.id,
          'download testradtemplate',
          true
        );
      });
    } else {
      this.testradtemplatePersist
        .download(this.tcUtilsArray.idsList(this.testradtemplateSelection))
        .subscribe((downloadJob) => {
          this.jobPersist.followJob(
            downloadJob.id,
            'download testradtemplate',
            true
          );
        });
    }
  }
  addTestRadTemplate(): void {
    let dialogRef = this.testradtemplateNavigator.addtestRadTemplate(this.lab_test_id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.searchTestRadTemplates();
      }
    });
  }

  editTestRadTemplate(item: TestRadTemplateSummary) {
    let dialogRef = this.testradtemplateNavigator.edittestRadTemplate(item.id);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }
    });
  }

  deleteTestRadTemplate(item: TestRadTemplateSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion('testradtemplate');
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        //do the deletion
        this.testradtemplatePersist.deleteTestRadTemplate(item.id).subscribe(
          (response) => {
            this.tcNotification.success('testradtemplate deleted');
            this.searchTestRadTemplates();
          },
          (error) => {}
        );
      }
    });
  }
  back(): void {
    this.location.back();
  }

}
