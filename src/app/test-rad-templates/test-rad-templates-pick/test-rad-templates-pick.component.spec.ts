import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestRadTemplatesPickComponent } from './test-rad-templates-pick.component';

describe('TestRadTemplatesPickComponent', () => {
  let component: TestRadTemplatesPickComponent;
  let fixture: ComponentFixture<TestRadTemplatesPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestRadTemplatesPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestRadTemplatesPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
