import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs';
import { AppTranslation } from 'src/app/app.translation';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { TCAuthorization } from 'src/app/tc/authorization';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { JobData } from 'src/app/tc/jobs/job.model';
import { JobPersist } from 'src/app/tc/jobs/job.persist';
import { TCNavigator } from 'src/app/tc/navigator';
import { TCNotification } from 'src/app/tc/notification';
import { TCUtilsAngular } from 'src/app/tc/utils-angular';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TestRadTemplateSummary, TestRadTemplateSummaryPartialList } from '../test-rad-templates.model';
import { TestRadTemplateNavigator } from '../test-rad-templates.navigator';
import { TestRadTemplatePersist } from '../test-rad-templates.persist';
import { Location } from '@angular/common';
import { RadiologyResultSettingsPersist } from 'src/app/radiology_result_settings/radiology_result_settings.persist';

@Component({
  selector: 'app-test-rad-templates-pick',
  templateUrl: './test-rad-templates-pick.component.html',
  styleUrls: ['./test-rad-templates-pick.component.scss']
})
export class TestRadTemplatesPickComponent implements OnInit {

  testradtemplatesData: TestRadTemplateSummary[] = [];
  testradtemplatesTotalCount: number = 0;
  testradtemplateSelectAll: boolean = false;
  testradtemplateSelection: TestRadTemplateSummary[] = [];

  testradtemplatesDisplayedColumns: string[] = [
    'select',
    'template_id',
  ];
  testradtemplateSearchTextBox: FormControl = new FormControl();
  testradtemplateIsLoading: boolean = false;
  @ViewChild(MatPaginator, { static: true })
  testradtemplatesPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) testradtemplatesSort: MatSort;

  constructor(
    private router: Router,
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcUtilsAngular: TCUtilsAngular,
    public tcUtilsArray: TCUtilsArray,
    public tcUtilsDate: TCUtilsDate,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public appTranslation: AppTranslation,
    public testradtemplatePersist: TestRadTemplatePersist,
    public testradtemplateNavigator: TestRadTemplateNavigator,
    public jobPersist: JobPersist,
    public jobData: JobData,
    public filePersist: FilePersist,
    public tcAsyncJob: TCAsyncJob,
    public dialogRef: MatDialogRef<TestRadTemplatesPickComponent>,
    public templatePersist: RadiologyResultSettingsPersist,
    @Inject(MAT_DIALOG_DATA) public data: {selectOne: boolean, test_id: string}
  ) {
    this.testradtemplatePersist.lab_test_id = this.data.test_id
    this.tcAuthorization.requireRead('testradtemplates');
    this.testradtemplateSearchTextBox.setValue(
      testradtemplatePersist.testradtemplateSearchText
    );
    //delay subsequent keyup events
    this.testradtemplateSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.testradtemplatePersist.testradtemplateSearchText = value;
        this.searchTestRadTemplates();
      });
  }
  ngOnInit() {
    this.testradtemplatesSort.sortChange.subscribe(() => {
      this.testradtemplatesPaginator.pageIndex = 0;
      this.searchTestRadTemplates(true);
    });

    this.testradtemplatesPaginator.page.subscribe(() => {
      this.searchTestRadTemplates(true);
    });
    //start by loading items
    this.searchTestRadTemplates();
  }

  searchTestRadTemplates(isPagination: boolean = false): void {
    let paginator = this.testradtemplatesPaginator;
    let sorter = this.testradtemplatesSort;

    this.testradtemplateIsLoading = true;
    this.testradtemplateSelection = [];

    this.testradtemplatePersist
      .searchTestRadTemplate(
        paginator.pageSize,
        isPagination ? paginator.pageIndex : 0,
        sorter.active,
        sorter.direction
      )
      .subscribe(
        (partialList: TestRadTemplateSummaryPartialList) => {
          this.testradtemplatesData = partialList.data;
          this.testradtemplatesData.forEach(
            radTemplate => {
              this.templatePersist.getRadiologyResultSettings(radTemplate.template_id).subscribe(
                template => {
                  radTemplate.template = template;
                  radTemplate.template_name = template.template_name;
                }
              )
            }
          )
          if (partialList.total != -1) {
            this.testradtemplatesTotalCount = partialList.total;
          }
          this.testradtemplateIsLoading = false;
        },
        (error) => {
          this.testradtemplateIsLoading = false;
        }
      );
  }
  markOneItem(item: TestRadTemplateSummary) {
    if (!this.tcUtilsArray.containsId(this.testradtemplateSelection, item.id)) {
      this.testradtemplateSelection = [];
      this.testradtemplateSelection.push(item);
    } else {
      this.testradtemplateSelection = [];
    }
  }

  returnSelected(): void {
    this.dialogRef.close(this.testradtemplateSelection.map(radTemplate => radTemplate.template));
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
