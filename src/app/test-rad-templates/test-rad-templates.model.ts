import { RadiologyResultSettingsDetail } from '../radiology_result_settings/radiology_result_settings.model';
import { TCId } from '../tc/models';
export class TestRadTemplateSummary extends TCId {
  test_id: string;
  template_id: string;
  template_name: string;
  template: RadiologyResultSettingsDetail;
}
export class TestRadTemplateSummaryPartialList {
  data: TestRadTemplateSummary[];
  total: number;
}
export class TestRadTemplateDetail extends TestRadTemplateSummary {}

export class TestRadTemplateDashboard {
  total: number;
}
