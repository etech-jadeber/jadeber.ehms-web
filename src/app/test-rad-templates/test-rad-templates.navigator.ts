import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { TCModalWidths } from '../tc/utils-angular';
import { TCIdMode, TCModalModes } from '../tc/models';

import { TestRadTemplatesEditComponent } from './test-rad-templates-edit/test-rad-templates-edit.component';
import { TestRadTemplatesPickComponent } from './test-rad-templates-pick/test-rad-templates-pick.component';

@Injectable({
  providedIn: 'root',
})
export class TestRadTemplateNavigator {
  constructor(private router: Router, public dialog: MatDialog) {}
  testRadTemplatesUrl(): string {
    return '/test_rad_templates';
  }

  testRadTemplateUrl(id: string): string {
    return '/test_rad_templates/' + id;
  }

  viewtestRadTemplates(): void {
    this.router.navigateByUrl(this.testRadTemplatesUrl());
  }

  viewtestRadTemplate(id: string): void {
    this.router.navigateByUrl(this.testRadTemplateUrl(id));
  }

  edittestRadTemplate(id: string): MatDialogRef<TestRadTemplatesEditComponent> {
    const dialogRef = this.dialog.open(TestRadTemplatesEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  addtestRadTemplate(lab_test_id: string): MatDialogRef<TestRadTemplatesEditComponent> {
    const dialogRef = this.dialog.open(TestRadTemplatesEditComponent, {
      data: new TCIdMode(lab_test_id, TCModalModes.NEW),
      width: TCModalWidths.medium,
    });
    return dialogRef;
  }

  picktestRadTemplates(
    selectOne: boolean = false, test_id: string = null
  ): MatDialogRef<TestRadTemplatesPickComponent> {
    const dialogRef = this.dialog.open(TestRadTemplatesPickComponent, {
      data: {selectOne, test_id},
      width: TCModalWidths.large,
    });
    return dialogRef;
  }
}
