import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

import { TCDoParam, TCUrlParams, TCUtilsHttp } from '../tc/utils-http';
import { TCId, TcDictionary, TCEnum } from '../tc/models';
import {
  TestRadTemplateDashboard,
  TestRadTemplateDetail,
  TestRadTemplateSummaryPartialList,
} from './test-rad-templates.model';
import { store_type } from '../app.enums';
import { TCUtilsString } from '../tc/utils-string';

@Injectable({
  providedIn: 'root',
})
export class TestRadTemplatePersist {
  testradtemplateSearchText: string = '';
  lab_test_id: string;
  store_type: TCEnum[] = [
    new TCEnum(store_type.outpatient, 'outpatient'),
    new TCEnum(store_type.inpatient, 'inpatient'),
    new TCEnum(store_type.emergency, 'emergency'),
  ];

  constructor(private http: HttpClient) {}
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.testradtemplateSearchText;
    //add custom filters
    return fltrs;
  }

  searchTestRadTemplate(
    pageSize: number,
    pageIndex: number,
    sort: string,
    order: string
  ): Observable<TestRadTemplateSummaryPartialList> {
    let url = TCUtilsHttp.buildSearchUrl(
      'test_rad_templates',
      this.testradtemplateSearchText,
      pageSize,
      pageIndex,
      sort,
      order
    );
    if (this.lab_test_id){
      url = TCUtilsString.appendUrlParameter(url, 'test_id', this.lab_test_id)
    }
    return this.http.get<TestRadTemplateSummaryPartialList>(url);
  }
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'test_rad_templates/do',
      new TCDoParam('download_all', this.filters())
    );
  }

  testradtemplateDashboard(): Observable<TestRadTemplateDashboard> {
    return this.http.get<TestRadTemplateDashboard>(
      environment.tcApiBaseUri + 'test_rad_templates/dashboard'
    );
  }

  getTestRadTemplate(id: string): Observable<TestRadTemplateDetail> {
    return this.http.get<TestRadTemplateDetail>(
      environment.tcApiBaseUri + 'test_rad_templates/' + id
    );
  }
  addTestRadTemplate(item: TestRadTemplateDetail): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'test_rad_templates/',
      item
    );
  }

  updateTestRadTemplate(item: TestRadTemplateDetail): Observable<TestRadTemplateDetail> {
    return this.http.patch<TestRadTemplateDetail>(
      environment.tcApiBaseUri + 'test_rad_templates/' + item.id,
      item
    );
  }

  deleteTestRadTemplate(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + 'test_rad_templates/' + id);
  }
  testradtemplatesDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'test_rad_templates/do',
      new TCDoParam(method, payload)
    );
  }

  testradtemplateDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(
      environment.tcApiBaseUri + 'test_rad_templates/' + id + '/do',
      new TCDoParam(method, payload)
    );
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'test_rad_templates/do',
      new TCDoParam('download', ids)
    );
  }

  print(id: string): Observable<TCId> {
    return this.http.post<TCId>(
      environment.tcApiBaseUri + 'test_rad_templates/' + id + '/do',
      new TCDoParam('print', {})
    );
  }
}
