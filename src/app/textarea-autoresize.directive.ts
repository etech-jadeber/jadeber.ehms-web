import {Directive, HostListener, ElementRef, OnInit, SimpleChanges, OnChanges, Input} from '@angular/core';

@Directive({
  selector: '[appTextareaAutoresize]'
})
export class TextareaAutoresizeDirective implements OnInit, OnChanges {

  @Input() ngModel;
  @Input() rows = 1;
  constructor(private elementRef: ElementRef) { }

  @HostListener(':input')
  onInput() {
    this.resize();
  }

  ngOnChanges(changes: SimpleChanges) {
    setTimeout(() => this.resize())

  }

  ngOnInit() {
    if (this.elementRef.nativeElement.scrollHeight) {
      this.elementRef.nativeElement.rows = this.rows
      setTimeout(() => this.resize());
    }
  }

  resize() {
    if(this.ngModel) {
      this.elementRef.nativeElement.style.height = '0';
      const lineHeight = (parseFloat(getComputedStyle(this.elementRef.nativeElement).lineHeight) || 20) * this.rows;
      const scrollHeight = this.elementRef.nativeElement.scrollHeight;
      if(scrollHeight < lineHeight){
        this.elementRef.nativeElement.style.height = lineHeight + 'px';
      } else {
        this.elementRef.nativeElement.style.height = scrollHeight + 'px';
      }
    }
  }
}
