import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopTenDiseasListComponent } from './top-ten-diseas-list.component';

describe('TopTenDiseasListComponent', () => {
  let component: TopTenDiseasListComponent;
  let fixture: ComponentFixture<TopTenDiseasListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopTenDiseasListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopTenDiseasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
