import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { AppTranslation } from '../app.translation';
import { ICD10_report } from '../patients/patients.model';
import { PatientPersist } from '../patients/patients.persist';
import { TCAuthorization } from '../tc/authorization';
import { TCNavigator } from '../tc/navigator';
import { TCNotification } from '../tc/notification';
import { TCUtilsArray } from '../tc/utils-array';
import { TCUtilsDate } from '../tc/utils-date';
import {Location} from '@angular/common';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { JobPersist } from '../tc/jobs/job.persist';
import { TCId } from '../tc/models';

@Component({
  selector: 'app-top-ten-diseas-list',
  templateUrl: './top-ten-diseas-list.component.html',
  styleUrls: ['./top-ten-diseas-list.component.scss']
})
export class TopTenDiseasListComponent implements OnInit {
  ICD10_reportDisplayedColumns: String[] = ["disease","infant", "child", "young", "old", "total"];
  ICD10_reportData: ICD10_report[] = [];
  ICD10_reportTotalCount: number = 0;
  diagnosisReportIsLoading :boolean = false;  
  @Input() isDashboard = false;
  Gender = [
    {name:"Male",id:1},
    {name:"Female",id:0}
  ]
  start_date: FormControl = new FormControl({disabled: true, value: new Date(new Date().getFullYear(),new Date().getMonth()-1,new Date().getDate())});
  end_date: FormControl = new FormControl({disabled: true, value: new Date()});
  diagnosisReportSearchTextBox: FormControl = new FormControl();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sorter: MatSort;
  constructor(
    private location: Location,
    public tcAuthorization: TCAuthorization,
    public tcNotification: TCNotification,
    public tcNavigator: TCNavigator,
    public tcUtilsDate: TCUtilsDate,
    public jobPersist: JobPersist,
    public tcUtilsArray: TCUtilsArray,
    public appTranslation: AppTranslation,
    public patientPersisit: PatientPersist,

  ) { 
    
    this.diagnosisReportSearchTextBox.valueChanges
      .pipe(debounceTime(500))
      .subscribe((value) => {
        this.patientPersisit.diagnosisReportSearchHistory.sex = value;
        this.loadICD10_report();
      });

      this.patientPersisit.diagnosisReportSearchHistory.start_date = Math.floor(new Date(this.start_date.value).getTime() / 1000)
      this.patientPersisit.diagnosisReportSearchHistory.end_date = Math.floor(new Date(this.end_date.value).getTime() / 1000)
      this.start_date.valueChanges.pipe(debounceTime(500))
      .subscribe((value)=>{
        this.patientPersisit.diagnosisReportSearchHistory.start_date =new Date(value._d).getTime()/1000;
        this.loadICD10_report();
      });
      this.end_date.valueChanges.pipe(debounceTime(500))
      .subscribe((value)=>{
        this.patientPersisit.diagnosisReportSearchHistory.end_date =new Date(value._d).getTime()/1000;
        this.loadICD10_report();
      });
  }

  ngOnInit(): void {
    this.tcAuthorization.requireRead("disease_reports")
  }

  ngAfterViewInit(): void {
    this.isDashboard ? this.paginator.pageSize = 5 : '';
    this.sorter.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.loadICD10_report(true);
    });
  // medical_certificates paginator
  this.paginator.page.subscribe(() => {
      this.loadICD10_report(true);
    });
    if(this.tcAuthorization.canRead('patients')){
      this.loadICD10_report();
    }
  }
  loadICD10_report(isPagination:boolean=false){
    let paginator=this.paginator;
    this.diagnosisReportIsLoading = true;
    this.patientPersisit.diagnosisReportDo("diagnosis_reports", [],paginator.pageSize,isPagination? paginator.pageIndex:0,"total","desc").subscribe(
      (result: any) => {
        if (result){
        this.ICD10_reportData = result.data;
        if (result.total != -1) {
          this.ICD10_reportTotalCount = result.total;
        }
      }
      this.diagnosisReportIsLoading = false;
    }, error => {
        this.diagnosisReportIsLoading = false;
        console.error(error)
      }
    );
  }

  generateReport():void{
    const payload  ={start_date:this.patientPersisit.diagnosisReportSearchHistory.start_date ? this.patientPersisit.diagnosisReportSearchHistory.start_date.toString(): '',end_date:this.patientPersisit.diagnosisReportSearchHistory.end_date ? this.patientPersisit.diagnosisReportSearchHistory.end_date.toString(): '',total: this.ICD10_reportTotalCount}
    this.patientPersisit.patientsDo("generate_report",payload).subscribe((downloadJob: TCId) => {
      this.jobPersist.followJob(downloadJob.id, 'Exporting to excel', true);
      let success_state = 3;
      this.jobPersist.getJob(downloadJob.id).subscribe((job) => {
        if (job.job_state == success_state) {
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.patientPersisit.diagnosisReportSearchHistory.sex = null;
  }

  back():void{
    this.location.back();
  }
}
