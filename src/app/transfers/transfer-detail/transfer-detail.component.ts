import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatSort} from "@angular/material/sort";
import { MatPaginator } from '@angular/material/paginator';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {TransferDetail} from "../transfer.model";
import {TransferPersist} from "../transfer.persist";
import {TransferNavigator} from "../transfer.navigator";

export enum TransferTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-transfer-detail',
  templateUrl: './transfer-detail.component.html',
  styleUrls: ['./transfer-detail.component.css']
})
export class TransferDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  transferIsLoading:boolean = false;
  
  TransferTabs: typeof TransferTabs = TransferTabs;
  activeTab: TransferTabs = TransferTabs.overview;
  //basics
  transferDetail: TransferDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public transferNavigator: TransferNavigator,
              public  transferPersist: TransferPersist) {
    this.tcAuthorization.requireRead("transfers");
    this.transferDetail = new TransferDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("transfers");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.transferIsLoading = true;
    this.transferPersist.getTransfer(id).subscribe(transferDetail => {
          this.transferDetail = transferDetail;
          this.transferIsLoading = false;
        }, error => {
          console.error(error);
          this.transferIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.transferDetail.id);
  }
  editTransfer(): void {
    let modalRef = this.transferNavigator.editTransfer(this.transferDetail.id);
    modalRef.afterClosed().subscribe(transferDetail => {
      TCUtilsAngular.assign(this.transferDetail, transferDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.transferPersist.print(this.transferDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print transfer", true);
      });
    }

  back():void{
      this.location.back();
    }

}