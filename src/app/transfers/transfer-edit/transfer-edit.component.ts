import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { TransferDetail } from '../transfer.model';import { TransferPersist } from '../transfer.persist';import { ItemReceiveNavigator } from 'src/app/item_receive/item_receive.navigator';
import { ItemReceiveDetail } from 'src/app/item_receive/item_receive.model';
import { RequestPersist } from 'src/app/requests/request.persist';
import { RequestDetail } from 'src/app/requests/request.model';
import { ReceiveStatus, units } from 'src/app/app.enums';
import { TCUtilsArray } from 'src/app/tc/utils-array';
import { ItemPersist } from 'src/app/items/item.persist';
@Component({
  selector: 'app-transfer-edit',
  templateUrl: './transfer-edit.component.html',
  styleUrls: ['./transfer-edit.component.css']
})export class TransferEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  remaining :number;
  totalQuantity:number=0;
  transferDetail: TransferDetail;
  transferRequest: RequestDetail; 
  item_name: string;
  transfersDisplayedColumns: string[] = ["edit","action","item_id","batch_no","quantity","unit"];
  transferDetailList:TransferDetail[]=[];
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<TransferEditComponent>,
              public  persist: TransferPersist,
              public itemReceivedNavigator: ItemReceiveNavigator,
              public tcUtilsDate: TCUtilsDate,
              public requestPersist: RequestPersist,
              public tcUtilsArray: TCUtilsArray,
              public itemPersist: ItemPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("transfers");
      this.title = this.appTranslation.getText("general","new") +  " " + "transfer";
      this.transferDetail = new TransferDetail();
      //set necessary defaults
      this.transferDetail.transfer_request_id = this.idMode.id;
      this.transferDetail.transfer_unit = units.piece;
      this.isLoadingResults = true;
      this.requestPersist.getRequest(this.idMode.id).subscribe(
        (request) => {
          this.transferRequest = request;
          this.itemPersist.getItem(request.item_id).subscribe((item)=>{
            this.item_name = item.name;
          })
          this.persist.transfersDo("total_transfer",{"transfer_request_id":request.id}).subscribe((result:any)=>{
            if(result){
              this.remaining=+this.transferRequest.quantity-(+result.total_transfer)
            }
          })
          
          this.isLoadingResults = false
        },
        error => {
          this.isLoadingResults = false;
          console.log(error)
        }
      )

    } else {
     this.tcAuthorization.requireUpdate("transfers");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "transfer";
      this.isLoadingResults = true;
      this.persist.getTransfer(this.idMode.id).subscribe(transferDetail => {
        this.transferDetail = transferDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  
  
  AddToList():void{
    this.totalQuantity+=+this.transferDetail.quantity;
    this.remaining-=+this.transferDetail.quantity
    this.transferDetailList=[this.transferDetail,...this.transferDetailList];
    this.transferDetail=new TransferDetail();
    this.transferDetail.transfer_request_id=this.idMode.id;
    this.transferDetail.transfer_unit = units.piece;
  }

  removeFromList(item:TransferDetail):void{
    this.totalQuantity-=+item.quantity;
    this.remaining+=+item.quantity;
    this.transferDetailList=this.transferDetailList.filter((value)=> value != item);
  }
  editFromList(item:TransferDetail){
    this.removeFromList(item);
    this.transferDetail=item;
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.addTransfer(this.transferDetailList).subscribe(value => {
      this.tcNotification.success("transfer added");
      // this.transferDetail.id = value.id;
      this.dialogRef.close(this.transferDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.updateTransfer(this.transferDetail).subscribe(value => {
      this.tcNotification.success("transfer updated");
      this.dialogRef.close(this.transferDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.transferDetail == null){
            return false;
          }

if (this.transferDetail.transfer_request_id == null || this.transferDetail.transfer_request_id  == "") {
            return false;
        }
if (this.transferDetail.item_receive_id == null || this.transferDetail.item_receive_id  == "") {
            return false;
        }
if (this.transferDetail.quantity == null||this.transferDetail.quantity>this.remaining) {
            return false;
        }
if (this.transferDetail.transfer_unit == null) {
            return false;
        }
if(this.transferDetail.transfer_of_remaining<this.transferDetail.quantity){
  return false;
}
 return true;

 }
 SearchItemReceived(){
  let dialogRef = this.itemReceivedNavigator.pickItemReceives(this.transferRequest.item_id, ReceiveStatus.received, true)
    dialogRef.afterClosed().subscribe((result: ItemReceiveDetail[]) => {
      if(result){
        if(this.transferDetailList.some(value=>value.item_receive_id==result[0].id)){
          this.tcNotification.error("You have already selected this item please edit if you want")
        }
        else{
          this.transferDetail.item_batch_no = result[0].batch_no
          // this.transferDetail.item_name = 
          this.transferDetail.item_receive_id = result[0].id
          this.transferDetail.transfer_of_remaining=result[0].remaining;
        }
      }
    });
 }
 canSave():boolean{
  if (this.totalQuantity<=0){
    return false
  }
  return true;
 }
 }