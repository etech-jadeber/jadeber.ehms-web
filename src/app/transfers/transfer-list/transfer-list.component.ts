import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import {MatSort} from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TransferSummary, TransferSummaryPartialList } from '../transfer.model';
import { TransferPersist } from '../transfer.persist';
import { TransferNavigator } from '../transfer.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemReceivePersist } from 'src/app/item_receive/item_receive.persist';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ItemPersist } from 'src/app/items/item.persist';
import { ReceiveStatus } from 'src/app/app.enums';
import { LostDamageNavigator } from 'src/app/lostDamages/lostDamage.navigator';
import { RequestNavigator } from 'src/app/requests/request.navigator';
import { ItemReturnNavigator } from 'src/app/item_return/item_return.navigator';
import { TCUtilsString } from 'src/app/tc/utils-string';
import { ItemCategoryNavigator } from 'src/app/item_category/item_category.navigator';
import { ItemCategoryDetail } from 'src/app/item_category/item_category.model';
import { Item_In_StorePersist } from 'src/app/item_in_stores/item_in_store.persist';
import { ItemNavigator } from 'src/app/items/item.navigator';
import { ItemCategoryPersist } from 'src/app/item_category/item_category.persist';
import { StoreNavigator } from 'src/app/stores/store.navigator';
import { StoreSummary } from 'src/app/stores/store.model';
@Component({
  selector: 'app-transfer-list',
  templateUrl: './transfer-list.component.html',
  styleUrls: ['./transfer-list.component.css']
})export class TransferListComponent implements OnInit {
  transfersData: TransferSummary[] = [];
  transfersTotalCount: number = 0;
  transferSelectAll:boolean = false;
  transferSelection: TransferSummary[] = [];
  receiveStatus = ReceiveStatus;
  categoryName: string;
  subCategoryName: string;
  categoryId: string;
 transfersDisplayedColumns: string[] = ["select","action","item_id","batch_no",'ref_no',"quantity","unit", "unit_price","selling_price" ,"transfer_date","status", 'expire_date',"remaining","remark"];
  transferSearchTextBox: FormControl = new FormControl();
  refNoBox: FormControl = new FormControl();
  transferIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) transfersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) transfersSort: MatSort;
  @Input() requestId: string;
  @Input() item_id: string;
  store_name: any;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public transferPersist: TransferPersist,
                public transferNavigator: TransferNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemReceivePersist: ItemReceivePersist,
                public itemNavigator: ItemNavigator,
                public requestPersist: RequestPersist,
                public itemPersist: ItemPersist,
                public lostDamageNavigator: LostDamageNavigator,
                public requestNavigator: RequestNavigator,
                public itemReturnNavigator: ItemReturnNavigator,
                public tcUtilsString: TCUtilsString,
                public categoryNavigator: ItemCategoryNavigator,
                public categoryPersist: ItemCategoryPersist,
                public storeNavigator : StoreNavigator,
                public item_in_storePersist: Item_In_StorePersist,

    ) {

        this.tcAuthorization.requireRead("transfers");
       this.transferSearchTextBox.setValue(transferPersist.transferSearchText);
      //delay subsequent keyup events
      this.transferSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.transferPersist.transferSearchText = value;
        this.searchTransfers();
      });
      this.transferPersist.start_date.valueChanges.subscribe((value)=>{

        this.searchTransfers()
    });
    this.transferPersist.end_date.valueChanges.subscribe((value)=>{
        this.searchTransfers()
    });
    this.refNoBox.valueChanges
    .pipe(debounceTime(500))
    .subscribe((value) => {
      this.transferPersist.ref_no = value;
      this.searchTransfers();
    });

    } ngOnInit() {
   
      this.transfersSort.sortChange.subscribe(() => {
        this.transfersPaginator.pageIndex = 0;
        this.searchTransfers(true);
      });

      this.transfersPaginator.page.subscribe(() => {
        this.searchTransfers(true);
      });
      //start by loading items
      this.searchTransfers();
    }

  searchTransfers(isPagination:boolean = false): void {


    let paginator = this.transfersPaginator;
    let sorter = this.transfersSort;

    this.transferIsLoading = true;
    this.transferSelection = [];

    this.transferPersist.searchTransfer(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction,this.item_id).subscribe((partialList: TransferSummaryPartialList) => {
      this.transfersData = partialList.data;
      if (partialList.total != -1) {
        this.transfersTotalCount = partialList.total;
      }
      this.transferIsLoading = false;
    }, error => {
      this.transferIsLoading = false;
    });

  } downloadTransfers(): void {
    if(this.transferSelectAll){
         this.transferPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download transfer", true);
      });
    }
    else{
        this.transferPersist.download(this.tcUtilsArray.idsList(this.transferSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download transfer",true);
            });
        }
  }

  acceptAll():void{
    this.transferSelection = this.transferSelection.filter(transfer => transfer.status == this.receiveStatus.sent);
    for(let transfer of  this.transferSelection){
      this.acceptTransfer(transfer);
    }
  }

  acceptTransfer(item: TransferSummary){
    this.transferPersist.transferDo(item.id, "accept", {}).subscribe(
      result => this.searchTransfers()
    )
  }

  searchStore() {
    let dialogRef = this.storeNavigator.pickStores(true);
    dialogRef.afterClosed().subscribe((result: StoreSummary[]) => {
      if (result) {
        this.store_name = result[0].name;
        this.transferPersist.store_id = result[0].id;
        this.searchTransfers();
      }
    });
  }

  rejectTransfer(item: TransferSummary){
    let dialogRef = this.requestNavigator.addReason();
    dialogRef.afterClosed().subscribe(
      result => {
        if (result){
          this.transferPersist.transferDo(item.id, "reject", {reject_description: result.reason}).subscribe(
      result => this.searchTransfers()
    )
        }
      }
    )
  }

addTransfer(): void {
    let dialogRef = this.transferNavigator.addTransfer(this.requestId);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchTransfers();
      }
    });
  }

  editTransfer(item: TransferSummary) {
    let dialogRef = this.transferNavigator.editTransfer(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }
  lostDamage(item: TransferSummary){
      let dialogRef = this.lostDamageNavigator.addLostDamage(item.id);
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.searchTransfers();
        }
      });
  }

  returnItem(item: TransferSummary){
    let dialogRef = this.itemReturnNavigator.addItemReturn(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchTransfers()
      }
    })
  }

  searchCategory(isSub: boolean = false){
    let dialogRef = this.categoryNavigator.pickItemCategorys(this.transferPersist.category?.toString() , true)
    dialogRef.afterClosed().subscribe((result: ItemCategoryDetail[]) => {
      if(result){
        this.subCategoryName = result[0].name
        this.transferPersist.categoryId = result[0].id;
      }
      this.searchTransfers()
    })
  }

  deleteTransfer(item: TransferSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("transfer");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.transferPersist.deleteTransfer(item.id).subscribe(response => {
          this.tcNotification.success("transfer deleted");
          this.searchTransfers();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }
}