import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransferPickComponent } from './transfer-pick.component';

describe('TransferPickComponent', () => {
  let component: TransferPickComponent;
  let fixture: ComponentFixture<TransferPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
