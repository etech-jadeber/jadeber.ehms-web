import {Component, OnInit, Inject,ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";
import { TransferSummary, TransferSummaryPartialList } from '../transfer.model';
import { TransferPersist } from '../transfer.persist';
import { TransferNavigator } from '../transfer.navigator';
import { JobData } from 'src/app/tc/jobs/job.model';
import { FilePersist } from 'src/app/tc/files/file.persist';
import { TCAsyncJob } from 'src/app/tc/asyncjob';
import { ItemReceivePersist } from 'src/app/item_receive/item_receive.persist';
import { RequestPersist } from 'src/app/requests/request.persist';
import { ReceiveStatus } from 'src/app/app.enums';
import { ItemNavigator } from 'src/app/items/item.navigator';
@Component({
  selector: 'app-transfer-pick',
  templateUrl: './transfer-pick.component.html',
  styleUrls: ['./transfer-pick.component.css']
})export class TransferPickComponent implements OnInit {
  transfersData: TransferSummary[] = [];
  transfersTotalCount: number = 0;
  transferSelectAll:boolean = false;
  transferSelection: TransferSummary[] = [];

 transfersDisplayedColumns: string[] = ["select",'item_name',"batch_no","remaining","unit","unit_price","selling_price","expire_date" ];
  transferSearchTextBox: FormControl = new FormControl();
  transferIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) transfersPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) transfersSort: MatSort;
 
constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public itemNav: ItemNavigator,
                public transferPersist: TransferPersist,
                public transferNavigator: TransferNavigator,
                public jobPersist: JobPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,
                public itemReceivePersist: ItemReceivePersist,
                public requestPersist: RequestPersist,
 public dialogRef: MatDialogRef<TransferPickComponent>,@Inject(MAT_DIALOG_DATA) public data :{selectOne: boolean, item_id: string}

    ) {

        this.tcAuthorization.requireRead("transfers");
       this.transferSearchTextBox.setValue(transferPersist.transferSearchText);
      this.transferPersist.status = ReceiveStatus.received
      this.transferPersist.remaining = true;
      this.transferPersist.not_expired = true;
      //delay subsequent keyup events
      this.transferSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.transferPersist.transferSearchText = value;
        this.searchTransfers();
      });
    } ngOnInit() {
   
      this.transfersSort.sortChange.subscribe(() => {
        this.transfersPaginator.pageIndex = 0;
        this.searchTransfers(true);
      });

      this.transfersPaginator.page.subscribe(() => {
        this.searchTransfers(true);
      });
      //start by loading items
      this.searchTransfers();
    }

  searchTransfers(isPagination:boolean = false): void {


    let paginator = this.transfersPaginator;
    let sorter = this.transfersSort;

    this.transferIsLoading = true;
    this.transferSelection = [];
    this.transferPersist.searchTransfer(paginator.pageSize, isPagination? paginator.pageIndex:0, "expire_date", "asc", this.data.item_id).subscribe((partialList: TransferSummaryPartialList) => {
      this.transfersData = partialList.data;
      if (partialList.total != -1) {
        this.transfersTotalCount = partialList.total;
      }
      this.transferIsLoading = false;
    }, error => {
      this.transferIsLoading = false;
    });

  }
  markOneItem(item: TransferSummary) {
    if(!this.tcUtilsArray.containsId(this.transferSelection,item.id)){
          this.transferSelection = [];
          this.transferSelection.push(item);
        }
        else{
          this.transferSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.transferSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  ngOnDestroy():void {
    this.transferPersist.status=null;
    this.transferPersist.not_expired = false;
  }
  }