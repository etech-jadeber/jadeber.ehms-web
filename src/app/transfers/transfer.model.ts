import { ItemDetail } from "../items/item.model";
import {TCId} from "../tc/models";
export class TransferSummary extends TCId {
  transfer_request_id:string;
  item_receive_id:string;
  quantity:number;
  transfer_unit:number;
  transfer_date:number;
  reject_description: string;
  status:number;
  receiver_id:string;
  sender_id:string;
  item_batch_no: string;
  item_id: string;
  item_name: string;
  batch_no: string;
  remaining: number;
  transfer_of_remaining:number;
  unit_price: number;
  item: ItemDetail;
  }
  export class TransferSummaryPartialList {
    data: TransferSummary[];
    total: number;
  }
  export class TransferDetail extends TransferSummary {
  }
  
  export class TransferDashboard {
    total: number;
  }