import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {TransferEditComponent} from "./transfer-edit/transfer-edit.component";
import {TransferPickComponent} from "./transfer-pick/transfer-pick.component";


@Injectable({
  providedIn: 'root'
})

export class TransferNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  transfersUrl(): string {
    return "/transfers";
  }

  transferUrl(id: string): string {
    return "/transfers/" + id;
  }

  viewTransfers(): void {
    this.router.navigateByUrl(this.transfersUrl());
  }

  viewTransfer(id: string): void {
    this.router.navigateByUrl(this.transferUrl(id));
  }

  editTransfer(id: string): MatDialogRef<TransferEditComponent> {
    const dialogRef = this.dialog.open(TransferEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addTransfer(parentId: string): MatDialogRef<TransferEditComponent> {
    const dialogRef = this.dialog.open(TransferEditComponent, {
          data: new TCIdMode(parentId, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickTransfers(item_id: string, selectOne: boolean=false): MatDialogRef<TransferPickComponent> {
      const dialogRef = this.dialog.open(TransferPickComponent, {
        data: {selectOne, item_id},
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}