import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {TransferDashboard, TransferDetail, TransferSummaryPartialList} from "./transfer.model";
import { TCUtilsString } from '../tc/utils-string';
import { FormControl } from "@angular/forms";


@Injectable({
  providedIn: 'root'
})
export class TransferPersist {
  start_date: FormControl = new FormControl({disabled: true, value: ""});
  end_date: FormControl = new FormControl({disabled: true, value: ""});
 transferSearchText: string = "";
 status: number
 categoryId: string;
 category: number;
 remaining :boolean = true;
 not_expired :boolean = false;
  store_id: string;
  ref_no: string;
 constructor(private http: HttpClient) {
  }
  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.transferSearchText;
    if (this.status){
      fltrs["status"] = this.status.toString();
    }
    if (this.start_date.value){
      fltrs["start_date"] = (new Date(this.start_date.value).getTime()/1000).toString();
    }
    if (this.not_expired){
      fltrs["not_expired"] = this.not_expired? "1":"0";
    }
    if (this.end_date.value){
      fltrs["end_date"] = (new Date(this.end_date.value).getTime()/1000).toString();
    }
    if (this.categoryId){
      fltrs["category_id"] = this.categoryId;
    }
    if (this.ref_no){
      fltrs["ref_no"] = this.ref_no;
    }
    if (this.store_id){
      fltrs["store_id"] = this.store_id;
    }
    if (this.category){
      fltrs["category"] = this.category.toString();
    }
    fltrs["remaining"] = this.remaining ?'1':'0'
    //add custom filters
    return fltrs;
  }
 
  searchTransfer(pageSize: number, pageIndex: number, sort: string, order: string, item_id: string = null): Observable<TransferSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("transfers", this.transferSearchText, pageSize, pageIndex, sort, order);
    if (item_id){
      url = TCUtilsString.appendUrlParameter(url, "item_id", item_id)
    }
    if (this.status){
      url = TCUtilsString.appendUrlParameter(url, "status", this.status.toString())
    }
    if (this.start_date.value){
      url = TCUtilsString.appendUrlParameter(url, "start_date", (new Date(this.start_date.value).getTime()/1000).toString())
    }
    if (this.not_expired){
      url = TCUtilsString.appendUrlParameter(url, "not_expired", this.not_expired? "1":"0")
    }
    if (this.end_date.value){
      url = TCUtilsString.appendUrlParameter(url, "end_date", (new Date(this.end_date.value).getTime()/1000).toString())
    }
    if(this.categoryId){
      url = TCUtilsString.appendUrlParameter(url, "category_id", this.categoryId)
    }
    if(this.ref_no){
      url = TCUtilsString.appendUrlParameter(url, "ref_no", this.ref_no);
    }
    if(this.store_id){
      url = TCUtilsString.appendUrlParameter(url, "store_id", this.store_id)
    }
    if(this.category){
      url = TCUtilsString.appendUrlParameter(url, "category", this.category.toString())
    }
      url = TCUtilsString.appendUrlParameter(url, "remaining", this.remaining ?'1':'0')
    return this.http.get<TransferSummaryPartialList>(url);

  }

  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "transfers/do", new TCDoParam("download_all", this.filters()));
  }

  transferDashboard(): Observable<TransferDashboard> {
    return this.http.get<TransferDashboard>(environment.tcApiBaseUri + "transfers/dashboard");
  }

  getTransfer(id: string): Observable<TransferDetail> {
    return this.http.get<TransferDetail>(environment.tcApiBaseUri + "transfers/" + id);
  }

  addTransfer(item: TransferDetail[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "transfers/", {"data":item});
  }

  updateTransfer(item: TransferDetail): Observable<TransferDetail> {
    return this.http.patch<TransferDetail>(environment.tcApiBaseUri + "transfers/" + item.id, item);
  }

  deleteTransfer(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "transfers/" + id);
  }

 transfersDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "transfers/do", new TCDoParam(method, payload));
  }

  transferDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "transfers/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "transfers/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "transfers/" + id + "/do", new TCDoParam("print", {}));
  }


}