import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VaccinatedDetailComponent } from './vaccinated-detail.component';

describe('VaccinatedDetailComponent', () => {
  let component: VaccinatedDetailComponent;
  let fixture: ComponentFixture<VaccinatedDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinatedDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinatedDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
