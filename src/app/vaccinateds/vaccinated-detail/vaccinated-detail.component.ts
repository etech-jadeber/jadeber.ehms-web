import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {VaccinatedDetail} from "../vaccinated.model";
import {VaccinatedPersist} from "../vaccinated.persist";
import {VaccinatedNavigator} from "../vaccinated.navigator";

export enum VaccinatedTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-vaccinated-detail',
  templateUrl: './vaccinated-detail.component.html',
  styleUrls: ['./vaccinated-detail.component.css']
})
export class VaccinatedDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  vaccinatedLoading:boolean = false;
  
  VaccinatedTabs: typeof VaccinatedTabs = VaccinatedTabs;
  activeTab: VaccinatedTabs = VaccinatedTabs.overview;
  //basics
  vaccinatedDetail: VaccinatedDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public vaccinatedNavigator: VaccinatedNavigator,
              public  vaccinatedPersist: VaccinatedPersist) {
    this.tcAuthorization.requireRead("vaccinateds");
    this.vaccinatedDetail = new VaccinatedDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("vaccinateds");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.vaccinatedLoading = true;
    this.vaccinatedPersist.getVaccinated(id).subscribe(vaccinatedDetail => {
          this.vaccinatedDetail = vaccinatedDetail;
          this.vaccinatedLoading = false;
        }, error => {
          console.error(error);
          this.vaccinatedLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.vaccinatedDetail.id);
  }

  editVaccinated(): void {
    let modalRef = this.vaccinatedNavigator.editVaccinated(this.vaccinatedDetail.id);
    modalRef.afterClosed().subscribe(modifiedVaccinatedDetail => {
      TCUtilsAngular.assign(this.vaccinatedDetail, modifiedVaccinatedDetail);
    }, error => {
      console.error(error);
    });
  }

   printVaccinated():void{
      this.vaccinatedPersist.print(this.vaccinatedDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print vaccinated", true);
      });
    }

  back():void{
      this.location.back();
    }

}
