import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VaccinatedEditComponent } from './vaccinated-edit.component';

describe('VaccinatedEditComponent', () => {
  let component: VaccinatedEditComponent;
  let fixture: ComponentFixture<VaccinatedEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinatedEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinatedEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
