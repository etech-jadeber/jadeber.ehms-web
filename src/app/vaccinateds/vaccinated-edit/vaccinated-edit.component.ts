import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {VaccinatedDetail} from "../vaccinated.model";
import {VaccinatedPersist} from "../vaccinated.persist";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { PatientNavigator } from 'src/app/patients/patients.navigator';
import { PatientSummary } from 'src/app/patients/patients.model';
import { VaccinationNavigator } from 'src/app/vaccinations/vaccination.navigator';
import { VaccinationSummary } from 'src/app/vaccinations/vaccination.model';


@Component({
  selector: 'app-vaccinated-edit',
  templateUrl: './vaccinated-edit.component.html',
  styleUrls: ['./vaccinated-edit.component.css']
})
export class VaccinatedEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  vaccinatedDetail: VaccinatedDetail;
  vac_date: Date = new Date();
  appointment_date: Date = new Date();
  vaccineName: string = "";

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<VaccinatedEditComponent>,
              public  persist: VaccinatedPersist,
              public tCUtilsDate: TCUtilsDate,
              public vaccinationNavigator: VaccinationNavigator,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("vaccinateds");
      this.title = this.appTranslation.getText("general","new") +  " " + this.appTranslation.getText("patient","vaccinated");
      this.vaccinatedDetail = new VaccinatedDetail();
      this.vaccinatedDetail.patient_type = -1;

      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("vaccinateds");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient","vaccinated");
      this.isLoadingResults = true;
      this.persist.getVaccinated(this.idMode.id).subscribe(vaccinatedDetail => {
        this.vaccinatedDetail = vaccinatedDetail;
        this.transformDates(false);
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.addVaccinated(this.vaccinatedDetail).subscribe(value => {
      this.tcNotification.success("Vaccinated added");
      this.vaccinatedDetail.id = value.id;
      this.dialogRef.close(this.vaccinatedDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.transformDates(true);
    this.persist.updateVaccinated(this.vaccinatedDetail).subscribe(value => {
      this.tcNotification.success("Vaccinated updated");
      this.dialogRef.close(this.vaccinatedDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }

  transformDates(todate: boolean = true) {
    if (todate) {
      this.vaccinatedDetail.vac_date = this.tCUtilsDate.toTimeStamp(this.vac_date);
      this.vaccinatedDetail.appointment_date = this.tCUtilsDate.toTimeStamp(this.appointment_date);


    } else {
      this.vac_date = this.tCUtilsDate.toDate(this.vaccinatedDetail.vac_date);
      this.appointment_date = this.tCUtilsDate.toDate(this.vaccinatedDetail.appointment_date);


    }
  }

  searchVaccine() {
    let dialogRef = this.vaccinationNavigator.pickVaccinations(true);
    dialogRef.afterClosed().subscribe((result: VaccinationSummary[]) => {
      if (result) {
        this.vaccineName = result[0].name
        this.vaccinatedDetail.vac_id = result[0].id;
      }
    });
  }


  canSubmit():boolean{
        if (this.vaccinatedDetail == null){
            return false;
          }
          if (this.vaccinatedDetail.patient_type == -1) {
            return false;
          }
        


        return true;
      }


}
