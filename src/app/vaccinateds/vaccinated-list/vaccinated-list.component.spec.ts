import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VaccinatedListComponent } from './vaccinated-list.component';

describe('VaccinatedListComponent', () => {
  let component: VaccinatedListComponent;
  let fixture: ComponentFixture<VaccinatedListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinatedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinatedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
