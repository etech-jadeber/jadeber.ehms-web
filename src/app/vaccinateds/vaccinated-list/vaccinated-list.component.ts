import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {VaccinatedPersist} from "../vaccinated.persist";
import {VaccinatedNavigator} from "../vaccinated.navigator";
import {VaccinatedDetail, VaccinatedSummary, VaccinatedSummaryPartialList} from "../vaccinated.model";
import { VaccinationSummary, VaccinationSummaryPartialList } from 'src/app/vaccinations/vaccination.model';
import { VaccinationPersist } from 'src/app/vaccinations/vaccination.persist';


@Component({
  selector: 'app-vaccinated-list',
  templateUrl: './vaccinated-list.component.html',
  styleUrls: ['./vaccinated-list.component.css']
})
export class VaccinatedListComponent implements OnInit {

  vaccinatedsData: VaccinatedSummary[] = [];
  vaccinatedsTotalCount: number = 0;
  vaccinatedsSelectAll:boolean = false;
  vaccinatedsSelection: VaccinatedSummary[] = [];

  vaccinatedsDisplayedColumns: string[] = ["select","action", "patient_type","target_id","vac_date","vac_id","appointment_date", ];
  vaccinatedsSearchTextBox: FormControl = new FormControl();
  vaccinatedsIsLoading: boolean = false;

  vaccinations: VaccinationSummary[] = [];

  @ViewChild(MatPaginator, {static: true}) vaccinatedsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) vaccinatedsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public vaccinatedPersist: VaccinatedPersist,
                public vaccinatedNavigator: VaccinatedNavigator,
                public jobPersist: JobPersist,
                public vaccinationPersist: VaccinationPersist,
    ) {

        this.tcAuthorization.requireRead("vaccinateds");
       this.vaccinatedsSearchTextBox.setValue(vaccinatedPersist.vaccinatedSearchText);
      //delay subsequent keyup events
      this.vaccinatedsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.vaccinatedPersist.vaccinatedSearchText = value;
        this.searchVaccinateds();
      });
    }

    ngOnInit() {
      this. loadVaccinations();
      this.vaccinatedsSort.sortChange.subscribe(() => {
        this.vaccinatedsPaginator.pageIndex = 0;
        this.searchVaccinateds(true);
      });

      this.vaccinatedsPaginator.page.subscribe(() => {
        this.searchVaccinateds(true);
      });
      //start by loading items
      this.searchVaccinateds();
    }

  searchVaccinateds(isPagination:boolean = false): void {


    let paginator = this.vaccinatedsPaginator;
    let sorter = this.vaccinatedsSort;

    this.vaccinatedsIsLoading = true;
    this.vaccinatedsSelection = [];

    this.vaccinatedPersist.searchVaccinated(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: VaccinatedSummaryPartialList) => {
      this.vaccinatedsData = partialList.data;
      if (partialList.total != -1) {
        this.vaccinatedsTotalCount = partialList.total;
      }
      this.vaccinatedsIsLoading = false;
    }, error => {
      this.vaccinatedsIsLoading = false;
    });

  }

  downloadVaccinateds(): void {
    if(this.vaccinatedsSelectAll){
         this.vaccinatedPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download vaccinateds", true);
      });
    }
    else{
        this.vaccinatedPersist.download(this.tcUtilsArray.idsList(this.vaccinatedsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download vaccinateds",true);
            });
        }
  }

  addVaccinated(): void {
    let dialogRef = this.vaccinatedNavigator.addVaccinated();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchVaccinateds();
      }
    });
  }

  editVaccinated(item: VaccinatedSummary) {
    let dialogRef = this.vaccinatedNavigator.editVaccinated(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteVaccinated(item: VaccinatedSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Vaccinated");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.vaccinatedPersist.deleteVaccinated(item.id).subscribe(response => {
          this.tcNotification.success("Vaccinated deleted");
          this.searchVaccinateds();
        }, error => {
        });
      }

    });
  }
  loadVaccinations(): void {
      this.vaccinationPersist.searchVaccination(50, 0, "", "").subscribe((result: VaccinationSummaryPartialList) => {
          this.vaccinations = result.data;
      })
  }
  

  back():void{
      this.location.back();
    }

}
