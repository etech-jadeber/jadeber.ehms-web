import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VaccinatedPickComponent } from './vaccinated-pick.component';

describe('VaccinatedPickComponent', () => {
  let component: VaccinatedPickComponent;
  let fixture: ComponentFixture<VaccinatedPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinatedPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinatedPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
