import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {VaccinatedDetail, VaccinatedSummary, VaccinatedSummaryPartialList} from "../vaccinated.model";
import {VaccinatedPersist} from "../vaccinated.persist";


@Component({
  selector: 'app-vaccinated-pick',
  templateUrl: './vaccinated-pick.component.html',
  styleUrls: ['./vaccinated-pick.component.css']
})
export class VaccinatedPickComponent implements OnInit {

  vaccinatedsData: VaccinatedSummary[] = [];
  vaccinatedsTotalCount: number = 0;
  vaccinatedsSelection: VaccinatedSummary[] = [];
  vaccinatedsDisplayedColumns: string[] = ["select", 'patient_type','target_id','vac_date','vac_id','appointment_date' ];

  vaccinatedsSearchTextBox: FormControl = new FormControl();
  vaccinatedsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) vaccinatedsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) vaccinatedsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public vaccinatedPersist: VaccinatedPersist,
              public dialogRef: MatDialogRef<VaccinatedPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("vaccinateds");
    this.vaccinatedsSearchTextBox.setValue(vaccinatedPersist.vaccinatedSearchText);
    //delay subsequent keyup events
    this.vaccinatedsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.vaccinatedPersist.vaccinatedSearchText = value;
      this.searchVaccinateds();
    });
  }

  ngOnInit() {

    this.vaccinatedsSort.sortChange.subscribe(() => {
      this.vaccinatedsPaginator.pageIndex = 0;
      this.searchVaccinateds();
    });

    this.vaccinatedsPaginator.page.subscribe(() => {
      this.searchVaccinateds();
    });

    //set initial picker list to 5
    this.vaccinatedsPaginator.pageSize = 5;

    //start by loading items
    this.searchVaccinateds();
  }

  searchVaccinateds(): void {
    this.vaccinatedsIsLoading = true;
    this.vaccinatedsSelection = [];

    this.vaccinatedPersist.searchVaccinated(this.vaccinatedsPaginator.pageSize,
        this.vaccinatedsPaginator.pageIndex,
        this.vaccinatedsSort.active,
        this.vaccinatedsSort.direction).subscribe((partialList: VaccinatedSummaryPartialList) => {
      this.vaccinatedsData = partialList.data;
      if (partialList.total != -1) {
        this.vaccinatedsTotalCount = partialList.total;
      }
      this.vaccinatedsIsLoading = false;
    }, error => {
      this.vaccinatedsIsLoading = false;
    });

  }

  markOneItem(item: VaccinatedSummary) {
    if(!this.tcUtilsArray.containsId(this.vaccinatedsSelection,item.id)){
          this.vaccinatedsSelection = [];
          this.vaccinatedsSelection.push(item);
        }
        else{
          this.vaccinatedsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.vaccinatedsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
