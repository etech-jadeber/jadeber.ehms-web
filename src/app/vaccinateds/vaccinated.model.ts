import {TCId} from "../tc/models";

export class VaccinatedSummary extends TCId {
  patient_type : number;
target_id : string;
vac_date : number;
vac_id : string;
appointment_date : number;
}

export class VaccinatedSummaryPartialList {
  data: VaccinatedSummary[];
  total: number;
}

export class VaccinatedDetail extends VaccinatedSummary {
  patient_type : number;
target_id : string;
vac_date : number;
vac_id : string;
appointment_date : number;
}

export class VaccinatedDashboard {
  total: number;
}
