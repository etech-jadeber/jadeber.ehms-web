import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {VaccinatedEditComponent} from "./vaccinated-edit/vaccinated-edit.component";
import {VaccinatedPickComponent} from "./vaccinated-pick/vaccinated-pick.component";


@Injectable({
  providedIn: 'root'
})

export class VaccinatedNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  vaccinatedsUrl(): string {
    return "/vaccinateds";
  }

  vaccinatedUrl(id: string): string {
    return "/vaccinateds/" + id;
  }

  viewVaccinateds(): void {
    this.router.navigateByUrl(this.vaccinatedsUrl());
  }

  viewVaccinated(id: string): void {
    this.router.navigateByUrl(this.vaccinatedUrl(id));
  }

  editVaccinated(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(VaccinatedEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addVaccinated(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(VaccinatedEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickVaccinateds(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(VaccinatedPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
