import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary, TCEnum} from "../tc/models";
import {VaccinatedDashboard, VaccinatedDetail, VaccinatedSummaryPartialList} from "./vaccinated.model";
import { vaccinated_patient_type } from '../app.enums';
import { AppTranslation } from '../app.translation';


@Injectable({
  providedIn: 'root'
})
export class VaccinatedPersist {

  vaccinatedSearchText: string = "";

  vaccinated_patient_typeId: number ;
    
  vaccinated_patient_type: TCEnum[] = [
  new TCEnum(vaccinated_patient_type.child, this.appTranslation.getKey('patient', 'child')),
  new TCEnum(vaccinated_patient_type.patient, this.appTranslation.getKey('patient', 'patient')),
];

  constructor(
      private http: HttpClient,
      private appTranslation: AppTranslation,
      ) {
  }

  searchVaccinated(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<VaccinatedSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("vaccinateds", this.vaccinatedSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<VaccinatedSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.vaccinatedSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinateds/do", new TCDoParam("download_all", this.filters()));
  }

  vaccinatedDashboard(): Observable<VaccinatedDashboard> {
    return this.http.get<VaccinatedDashboard>(environment.tcApiBaseUri + "vaccinateds/dashboard");
  }

  getVaccinated(id: string): Observable<VaccinatedDetail> {
    return this.http.get<VaccinatedDetail>(environment.tcApiBaseUri + "vaccinateds/" + id);
  }

  addVaccinated(item: VaccinatedDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinateds/", item);
  }

  updateVaccinated(item: VaccinatedDetail): Observable<VaccinatedDetail> {
    return this.http.patch<VaccinatedDetail>(environment.tcApiBaseUri + "vaccinateds/" + item.id, item);
  }

  deleteVaccinated(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "vaccinateds/" + id);
  }

  vaccinatedsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vaccinateds/do", new TCDoParam(method, payload));
  }

  vaccinatedDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vaccinateds/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinateds/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinateds/" + id + "/do", new TCDoParam("print", {}));
  }


}
