import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {VaccinationDetail} from "../vaccination.model";
import {VaccinationPersist} from "../vaccination.persist";
import {VaccinationNavigator} from "../vaccination.navigator";

export enum VaccinationTabs {
  overview,
}

export enum PaginatorIndexes {

}


@Component({
  selector: 'app-vaccination-detail',
  templateUrl: './vaccination-detail.component.html',
  styleUrls: ['./vaccination-detail.component.css']
})
export class VaccinationDetailComponent implements OnInit , OnDestroy, AfterViewInit {

  paramsSubscription: Subscription;
  vaccinationLoading:boolean = false;
  
  VaccinationTabs: typeof VaccinationTabs = VaccinationTabs;
  activeTab: VaccinationTabs = VaccinationTabs.overview;
  //basics
  vaccinationDetail: VaccinationDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public vaccinationNavigator: VaccinationNavigator,
              public  vaccinationPersist: VaccinationPersist) {
    this.tcAuthorization.requireRead("vaccinations");
    this.vaccinationDetail = new VaccinationDetail();
  }

  ngOnInit() {
    this.tcAuthorization.requireRead("vaccinations");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.vaccinationLoading = true;
    this.vaccinationPersist.getVaccination(id).subscribe(vaccinationDetail => {
          this.vaccinationDetail = vaccinationDetail;
          this.vaccinationLoading = false;
        }, error => {
          console.error(error);
          this.vaccinationLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.vaccinationDetail.id);
  }

  editVaccination(): void {
    let modalRef = this.vaccinationNavigator.editVaccination(this.vaccinationDetail.id);
    modalRef.afterClosed().subscribe(modifiedVaccinationDetail => {
      TCUtilsAngular.assign(this.vaccinationDetail, modifiedVaccinationDetail);
    }, error => {
      console.error(error);
    });
  }

   printVaccination():void{
      this.vaccinationPersist.print(this.vaccinationDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print vaccination", true);
      });
    }

  back():void{
      this.location.back();
    }

}
