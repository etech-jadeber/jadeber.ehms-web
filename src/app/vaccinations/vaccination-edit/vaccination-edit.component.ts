import {Component, OnInit, Inject} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";


import {VaccinationDetail} from "../vaccination.model";
import {VaccinationPersist} from "../vaccination.persist";


@Component({
  selector: 'app-vaccination-edit',
  templateUrl: './vaccination-edit.component.html',
  styleUrls: ['./vaccination-edit.component.css']
})
export class VaccinationEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  vaccinationDetail: VaccinationDetail;

  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<VaccinationEditComponent>,
              public  persist: VaccinationPersist,
              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }

  ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("vaccinations");
      this.title = this.appTranslation.getText("general","new") + " " + this.appTranslation.getText("patient","vaccination");
      this.vaccinationDetail = new VaccinationDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("vaccinations");
      this.title = this.appTranslation.getText("general","edit") +  " " + this.appTranslation.getText("patient","vaccination");
      this.isLoadingResults = true;
      this.persist.getVaccination(this.idMode.id).subscribe(vaccinationDetail => {
        this.vaccinationDetail = vaccinationDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }

  onAdd(): void {
    this.isLoadingResults = true;
    this.persist.addVaccination(this.vaccinationDetail).subscribe(value => {
      this.tcNotification.success("Vaccination added");
      this.vaccinationDetail.id = value.id;
      this.dialogRef.close(this.vaccinationDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
    this.persist.updateVaccination(this.vaccinationDetail).subscribe(value => {
      this.tcNotification.success("Vaccination updated");
      this.dialogRef.close(this.vaccinationDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }


  canSubmit():boolean{
        if (this.vaccinationDetail == null){
            return false;
          }

        if (this.vaccinationDetail.name == null || this.vaccinationDetail.name  == "") {
                      return false;
                    }


        return true;
      }


}
