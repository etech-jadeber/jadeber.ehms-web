import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import {AppTranslation} from "../../app.translation";

import {VaccinationPersist} from "../vaccination.persist";
import {VaccinationNavigator} from "../vaccination.navigator";
import {VaccinationDetail, VaccinationSummary, VaccinationSummaryPartialList} from "../vaccination.model";


@Component({
  selector: 'app-vaccination-list',
  templateUrl: './vaccination-list.component.html',
  styleUrls: ['./vaccination-list.component.css']
})
export class VaccinationListComponent implements OnInit {

  vaccinationsData: VaccinationSummary[] = [];
  vaccinationsTotalCount: number = 0;
  vaccinationsSelectAll:boolean = false;
  vaccinationsSelection: VaccinationSummary[] = [];

  vaccinationsDisplayedColumns: string[] = ["select","action", "name","duration_in_days", ];
  vaccinationsSearchTextBox: FormControl = new FormControl();
  vaccinationsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) vaccinationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) vaccinationsSort: MatSort;

  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public vaccinationPersist: VaccinationPersist,
                public vaccinationNavigator: VaccinationNavigator,
                public jobPersist: JobPersist,
    ) {

        this.tcAuthorization.requireRead("vaccinations");
       this.vaccinationsSearchTextBox.setValue(vaccinationPersist.vaccinationSearchText);
      //delay subsequent keyup events
      this.vaccinationsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.vaccinationPersist.vaccinationSearchText = value;
        this.searchVaccinations();
      });
    }

    ngOnInit() {

      this.vaccinationsSort.sortChange.subscribe(() => {
        this.vaccinationsPaginator.pageIndex = 0;
        this.searchVaccinations(true);
      });

      this.vaccinationsPaginator.page.subscribe(() => {
        this.searchVaccinations(true);
      });
      //start by loading items
      this.searchVaccinations();
    }

  searchVaccinations(isPagination:boolean = false): void {


    let paginator = this.vaccinationsPaginator;
    let sorter = this.vaccinationsSort;

    this.vaccinationsIsLoading = true;
    this.vaccinationsSelection = [];

    this.vaccinationPersist.searchVaccination(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: VaccinationSummaryPartialList) => {
      this.vaccinationsData = partialList.data;
      if (partialList.total != -1) {
        this.vaccinationsTotalCount = partialList.total;
      }
      this.vaccinationsIsLoading = false;
    }, error => {
      this.vaccinationsIsLoading = false;
    });

  }

  downloadVaccinations(): void {
    if(this.vaccinationsSelectAll){
         this.vaccinationPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download vaccinations", true);
      });
    }
    else{
        this.vaccinationPersist.download(this.tcUtilsArray.idsList(this.vaccinationsSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download vaccinations",true);
            });
        }
  }



  addVaccination(): void {
    let dialogRef = this.vaccinationNavigator.addVaccination();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchVaccinations();
      }
    });
  }

  editVaccination(item: VaccinationSummary) {
    let dialogRef = this.vaccinationNavigator.editVaccination(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteVaccination(item: VaccinationSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("Vaccination");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.vaccinationPersist.deleteVaccination(item.id).subscribe(response => {
          this.tcNotification.success("Vaccination deleted");
          this.searchVaccinations();
        }, error => {
        });
      }

    });
  }

  back():void{
      this.location.back();
    }

}
