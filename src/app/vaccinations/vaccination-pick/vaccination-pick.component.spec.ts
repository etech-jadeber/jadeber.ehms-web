import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VaccinationPickComponent } from './vaccination-pick.component';

describe('VaccinationPickComponent', () => {
  let component: VaccinationPickComponent;
  let fixture: ComponentFixture<VaccinationPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VaccinationPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaccinationPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
