import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {debounceTime} from "rxjs/operators";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {AppTranslation} from "../../app.translation";

import {VaccinationDetail, VaccinationSummary, VaccinationSummaryPartialList} from "../vaccination.model";
import {VaccinationPersist} from "../vaccination.persist";


@Component({
  selector: 'app-vaccination-pick',
  templateUrl: './vaccination-pick.component.html',
  styleUrls: ['./vaccination-pick.component.css']
})
export class VaccinationPickComponent implements OnInit {

  vaccinationsData: VaccinationSummary[] = [];
  vaccinationsTotalCount: number = 0;
  vaccinationsSelection: VaccinationSummary[] = [];
  vaccinationsDisplayedColumns: string[] = ["select", 'name','duration_in_days' ];

  vaccinationsSearchTextBox: FormControl = new FormControl();
  vaccinationsIsLoading: boolean = false;

  @ViewChild(MatPaginator, {static: true}) vaccinationsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) vaccinationsSort: MatSort;

  constructor(public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcUtilsDate: TCUtilsDate,
              public tcNotification: TCNotification,
              public tcNavigator: TCNavigator,
              public appTranslation:AppTranslation,
              public vaccinationPersist: VaccinationPersist,
              public dialogRef: MatDialogRef<VaccinationPickComponent>,
              @Inject(MAT_DIALOG_DATA) public selectOne: boolean
  ) {
    this.tcAuthorization.requireRead("vaccinations");
    this.vaccinationsSearchTextBox.setValue(vaccinationPersist.vaccinationSearchText);
    //delay subsequent keyup events
    this.vaccinationsSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
      this.vaccinationPersist.vaccinationSearchText = value;
      this.searchVaccinations();
    });
  }

  ngOnInit() {

    this.vaccinationsSort.sortChange.subscribe(() => {
      this.vaccinationsPaginator.pageIndex = 0;
      this.searchVaccinations();
    });

    this.vaccinationsPaginator.page.subscribe(() => {
      this.searchVaccinations();
    });

    //set initial picker list to 5
    this.vaccinationsPaginator.pageSize = 5;

    //start by loading items
    this.searchVaccinations();
  }

  searchVaccinations(): void {
    this.vaccinationsIsLoading = true;
    this.vaccinationsSelection = [];

    this.vaccinationPersist.searchVaccination(this.vaccinationsPaginator.pageSize,
        this.vaccinationsPaginator.pageIndex,
        this.vaccinationsSort.active,
        this.vaccinationsSort.direction).subscribe((partialList: VaccinationSummaryPartialList) => {
      this.vaccinationsData = partialList.data;
      if (partialList.total != -1) {
        this.vaccinationsTotalCount = partialList.total;
      }
      this.vaccinationsIsLoading = false;
    }, error => {
      this.vaccinationsIsLoading = false;
    });

  }

  markOneItem(item: VaccinationSummary) {
    if(!this.tcUtilsArray.containsId(this.vaccinationsSelection,item.id)){
          this.vaccinationsSelection = [];
          this.vaccinationsSelection.push(item);
        }
        else{
          this.vaccinationsSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.vaccinationsSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

}
