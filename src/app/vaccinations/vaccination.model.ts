import {TCId} from "../tc/models";

export class VaccinationSummary extends TCId {
  name : string;
duration_in_days : number;
}

export class VaccinationSummaryPartialList {
  data: VaccinationSummary[];
  total: number;
}

export class VaccinationDetail extends VaccinationSummary {
  name : string;
duration_in_days : number;
}

export class VaccinationDashboard {
  total: number;
}
