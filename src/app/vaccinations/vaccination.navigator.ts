import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {VaccinationEditComponent} from "./vaccination-edit/vaccination-edit.component";
import {VaccinationPickComponent} from "./vaccination-pick/vaccination-pick.component";


@Injectable({
  providedIn: 'root'
})

export class VaccinationNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  vaccinationsUrl(): string {
    return "/vaccinations";
  }

  vaccinationUrl(id: string): string {
    return "/vaccinations/" + id;
  }

  viewVaccinations(): void {
    this.router.navigateByUrl(this.vaccinationsUrl());
  }

  viewVaccination(id: string): void {
    this.router.navigateByUrl(this.vaccinationUrl(id));
  }

  editVaccination(id: string): MatDialogRef<VaccinationEditComponent> {
    const dialogRef = this.dialog.open(VaccinationEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addVaccination(): MatDialogRef<VaccinationEditComponent> {
    const dialogRef = this.dialog.open(VaccinationEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickVaccinations(selectOne: boolean=false): MatDialogRef<VaccinationPickComponent> {
      const dialogRef = this.dialog.open(VaccinationPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
