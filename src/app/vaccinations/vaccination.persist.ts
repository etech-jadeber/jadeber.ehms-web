import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {VaccinationDashboard, VaccinationDetail, VaccinationSummaryPartialList} from "./vaccination.model";


@Injectable({
  providedIn: 'root'
})
export class VaccinationPersist {

  vaccinationSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchVaccination(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<VaccinationSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("vaccinations", this.vaccinationSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<VaccinationSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.vaccinationSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinations/do", new TCDoParam("download_all", this.filters()));
  }

  vaccinationDashboard(): Observable<VaccinationDashboard> {
    return this.http.get<VaccinationDashboard>(environment.tcApiBaseUri + "vaccinations/dashboard");
  }

  getVaccination(id: string): Observable<VaccinationDetail> {
    return this.http.get<VaccinationDetail>(environment.tcApiBaseUri + "vaccinations/" + id);
  }

  addVaccination(item: VaccinationDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinations/", item);
  }

  updateVaccination(item: VaccinationDetail): Observable<VaccinationDetail> {
    return this.http.patch<VaccinationDetail>(environment.tcApiBaseUri + "vaccinations/" + item.id, item);
  }

  deleteVaccination(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "vaccinations/" + id);
  }

  vaccinationsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vaccinations/do", new TCDoParam(method, payload));
  }

  vaccinationDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vaccinations/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinations/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "vaccinations/" + id + "/do", new TCDoParam("print", {}));
  }


}
