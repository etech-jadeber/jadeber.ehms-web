import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VitalDetailComponent } from './vital-detail.component';

describe('VitalDetailComponent', () => {
  let component: VitalDetailComponent;
  let fixture: ComponentFixture<VitalDetailComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
