import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VitalEditComponent } from './vital-edit.component';

describe('VitalEditComponent', () => {
  let component: VitalEditComponent;
  let fixture: ComponentFixture<VitalEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
