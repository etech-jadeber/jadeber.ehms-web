import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VitalListComponent } from './vital-list.component';

describe('VitalListComponent', () => {
  let component: VitalListComponent;
  let fixture: ComponentFixture<VitalListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
