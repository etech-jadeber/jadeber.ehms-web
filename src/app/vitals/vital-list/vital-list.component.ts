import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app',
  templateUrl: './vital-list.component.html',
  styleUrls: ['./vital-list.component.css']
})
export class VitalListComponent implements OnInit {
  @Input() encounterId: any;
  @Input() id: string;
  @Output() onResult = new EventEmitter<number>();
  constructor() { }
  ngOnInit() {
  }

}
