import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { VitalPickComponent } from './vital-pick.component';

describe('VitalPickComponent', () => {
  let component: VitalPickComponent;
  let fixture: ComponentFixture<VitalPickComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalPickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
