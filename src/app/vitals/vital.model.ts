import {TCId} from "../tc/models";

export class VitalSummary extends TCId {
  pid : number;
weight : number;
height : number;
temperature : number;
temp_method : number;
pulse : number;
BMI : number;
oxygen_saturation : number;
output: number;
input: number;
gcs: string;
rbs: string;
urine_ketone: string;
}

export class VitalSummaryPartialList {
  data: VitalSummary[];
  total: number;
}

export class VitalDetail extends VitalSummary {
  pid : number;
weight : number;
height : number;
temperature : number;
temp_method : number;
pulse : number;
BMI : number;
oxygen_saturation : number;
}

export class VitalDashboard {
  total: number;
}
