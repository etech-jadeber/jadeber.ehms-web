import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import {MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {VitalEditComponent} from "./vital-edit/vital-edit.component";
import {VitalPickComponent} from "./vital-pick/vital-pick.component";


@Injectable({
  providedIn: 'root'
})

export class VitalNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }

  vitalsUrl(): string {
    return "/vitals";
  }

  

  viewVitals(): void {
    this.router.navigateByUrl(this.vitalsUrl());
  }
  




  vitalUrl(id: string): string {
    return "/vitals/" + id;
  }
  viewVital(id: string): void {
    this.router.navigateByUrl(this.vitalUrl(id));
  }



  viewProcedures(id: string): string {
    return "/procedures/" + id;
  }


  vitalUrlNurse(id: string): string {
    return "/vitals_nurse/" + id;
  }
  viewVitalNurse(id: string): void {
    this.router.navigateByUrl(this.vitalUrlNurse(id));
  }


  editVital(id: string): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(VitalEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addVital(): MatDialogRef<unknown,any> {
    const dialogRef = this.dialog.open(VitalEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickVitals(selectOne: boolean=false): MatDialogRef<unknown,any> {
      const dialogRef = this.dialog.open(VitalPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}
