import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {VitalDashboard, VitalDetail, VitalSummaryPartialList} from "./vital.model";


@Injectable({
  providedIn: 'root'
})
export class VitalPersist {

  vitalSearchText: string = "";

  constructor(private http: HttpClient) {
  }

  searchVital(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<VitalSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("vitals", this.vitalSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<VitalSummaryPartialList>(url);

  }

  filters(): any {
    let fltrs : TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.vitalSearchText;
    //add custom filters
    return fltrs;
  }
  
  downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vitals/do", new TCDoParam("download_all", this.filters()));
  }

  vitalDashboard(): Observable<VitalDashboard> {
    return this.http.get<VitalDashboard>(environment.tcApiBaseUri + "vitals/dashboard");
  }

  getVital(id: string): Observable<VitalDetail> {
    return this.http.get<VitalDetail>(environment.tcApiBaseUri + "vitals/" + id);
  }

  addVital(item: VitalDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "vitals/", item);
  }

  updateVital(item: VitalDetail): Observable<VitalDetail> {
    return this.http.patch<VitalDetail>(environment.tcApiBaseUri + "vitals/" + item.id, item);
  }

  deleteVital(id: string): Observable<{}> {
      return this.http.delete(environment.tcApiBaseUri + "vitals/" + id);
  }

  vitalsDo(method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vitals/do", new TCDoParam(method, payload));
  }

  vitalDo(id: string, method: string, payload: any): Observable<{}> {
    return this.http.post<{}>(environment.tcApiBaseUri + "vitals/" + id + "/do", new TCDoParam(method, payload));
  }

  download(ids: string[]): Observable<TCId> {
      return this.http.post<TCId>(environment.tcApiBaseUri + "vitals/do", new TCDoParam("download", ids));
  }

  print(id: string): Observable<TCId> {
       return this.http.post<TCId>(environment.tcApiBaseUri + "vitals/" + id + "/do", new TCDoParam("print", {}));
  }


}
