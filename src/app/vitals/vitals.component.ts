import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-vitals',
  templateUrl: './vitals.component.html',
  styleUrls: ['./vitals.component.scss']
})
export class VitalsComponent implements OnInit {
  form_encounter_id:string;
  paramsSubscription: Subscription;
  form_vitalssTotalCount: number = 0;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.paramsSubscription = this.route.params.subscribe(param => {
      this.encounterChange(this.route.snapshot.paramMap.get('id'));
    });
  }


  ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    // this.paramsSubscription = this.route.params.subscribe(param => {
    //   this.encounterChange(this.route.snapshot.paramMap.get('id'));
    // });
   }

   ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }

  OnVitalResult(count: number) {
    this.form_vitalssTotalCount = count;
  }

  encounterChange(id: string){
    this.form_encounter_id = id;
  }
}
