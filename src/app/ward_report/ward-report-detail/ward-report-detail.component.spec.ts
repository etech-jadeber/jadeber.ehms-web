import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WardReportDetailComponent } from './ward-report-detail.component';

describe('WardReportDetailComponent', () => {
  let component: WardReportDetailComponent;
  let fixture: ComponentFixture<WardReportDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WardReportDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WardReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
