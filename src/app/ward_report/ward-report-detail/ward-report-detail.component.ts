import {AfterViewInit, Component, OnInit, OnDestroy, QueryList, ViewChildren} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {debounceTime} from "rxjs/operators";
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {Location} from '@angular/common';

import {Subscription} from "rxjs";

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCNotification} from "../../tc/notification";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {AppTranslation} from "../../app.translation";


import {WardReportDetail} from "../ward_report.model";
import {WardReportPersist} from "../ward_report.persist";
import {WardReportNavigator} from "../ward_reprort.navigator";

export enum WardReportTabs {
  overview,
}

export enum PaginatorIndexes {

}@Component({
  selector: 'app-ward_report-detail',
  templateUrl: './ward-report-detail.component.html',
  styleUrls: ['./ward-report-detail.component.css']
})
export class WardReportDetailComponent implements OnInit , OnDestroy, AfterViewInit {
  paramsSubscription: Subscription;
  wardReportIsLoading:boolean = false;
  
  WardReportTabs: typeof WardReportTabs = WardReportTabs;
  activeTab: WardReportTabs = WardReportTabs.overview;
  //basics
  wardReportDetail: WardReportDetail;
  @ViewChildren(MatPaginator) paginators = new QueryList<MatPaginator>();
  @ViewChildren(MatSort) sorters = new QueryList<MatSort>();

  constructor(private route: ActivatedRoute,
              private location: Location,
              public tcAuthorization:TCAuthorization,
              public tcUtilsAngular: TCUtilsAngular,
              public tcUtilsArray: TCUtilsArray,
              public tcNotification: TCNotification,
              public tcNavigator:TCNavigator,
              public jobPersist:JobPersist,
              public appTranslation:AppTranslation,
              public wardReportNavigator: WardReportNavigator,
              public  wardReportPersist: WardReportPersist) {
    this.tcAuthorization.requireRead("ward_report");
    this.wardReportDetail = new WardReportDetail();
  } ngOnInit() {
    this.tcAuthorization.requireRead("ward_report");
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
  }


   ngAfterViewInit(): void {
    //subscribe to route to get id changes -> even on routing to the same component with different id
    this.paramsSubscription = this.route.params.subscribe(param => {
      let id = this.route.snapshot.paramMap.get('id');
      this.loadDetails(id);
    });
   }

  loadDetails(id: string): void {
  this.wardReportIsLoading = true;
    this.wardReportPersist.getWardReport(id).subscribe(wardReportDetail => {
          this.wardReportDetail = wardReportDetail;
          this.wardReportIsLoading = false;
        }, error => {
          console.error(error);
          this.wardReportIsLoading = false;
        });
  }
  
  reload(){
    this.loadDetails(this.wardReportDetail.id);
  }
  editWardReport(): void {
    let modalRef = this.wardReportNavigator.editWardReport(this.wardReportDetail.id);
    modalRef.afterClosed().subscribe(wardReportDetail => {
      TCUtilsAngular.assign(this.wardReportDetail, wardReportDetail);
    }, error => {
      console.error(error);
    });
  }

   printBirth():void{
      this.wardReportPersist.print(this.wardReportDetail.id).subscribe(printJob => {
        this.jobPersist.followJob(printJob.id, "print ward_report", true);
      });
    }

  back():void{
      this.location.back();
    }

}