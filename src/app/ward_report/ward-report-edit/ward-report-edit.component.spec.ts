import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WardReportEditComponent } from './ward-report-edit.component';

describe('WardReportEditComponent', () => {
  let component: WardReportEditComponent;
  let fixture: ComponentFixture<WardReportEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WardReportEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WardReportEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
