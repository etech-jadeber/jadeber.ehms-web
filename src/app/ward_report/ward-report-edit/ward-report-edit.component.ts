import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {TCAuthorization} from "../../tc/authorization";
import {TCNotification}  from "../../tc/notification";
import {TCUtilsString}   from "../../tc/utils-string";
import {AppTranslation} from "../../app.translation";

import {TCIdMode, TCModalModes} from "../../tc/models";
import { TCUtilsDate } from 'src/app/tc/utils-date';
import { WardReportDetail } from '../ward_report.model';
import { DoctorSummary } from '../../doctors/doctor.model';
import { BedroomtypeSummary } from '../../bed/bedroomtypes/bedroomtype.model';
import { WardReportPersist } from '../ward_report.persist';
import { DoctorNavigator } from '../../doctors/doctor.navigator';
import { BedroomtypeNavigator } from '../../bed/bedroomtypes/bedroomtype.navigator';


@Component({
  selector: 'app-ward_report-edit',
  templateUrl: './ward-report-edit.component.html',
  styleUrls: ['./ward-report-edit.component.css']
})

export class WardReportEditComponent implements OnInit {

  isLoadingResults: boolean = false;
  title: string;
  wardReportDetail: WardReportDetail;
  Ward: string;
 

  
  constructor(public tcAuthorization:TCAuthorization,
              public tcNotification: TCNotification,
              public tcUtilsString:TCUtilsString,
              public appTranslation:AppTranslation,
              public dialogRef: MatDialogRef<WardReportEditComponent>,
              public  persist: WardReportPersist,
              private physcianNavigator: DoctorNavigator,
              private wardNavigator: BedroomtypeNavigator, 
              public tcUtilsDate: TCUtilsDate,

              @Inject(MAT_DIALOG_DATA) public idMode: TCIdMode) {

  }

  onCancel(): void {
    this.dialogRef.close();
  }

  isNew(): boolean {
    return this.idMode.mode === TCModalModes.NEW;
  }

  isEdit(): boolean {
    return this.idMode.mode === TCModalModes.EDIT;
  }

  isCase(): boolean {
    return this.idMode.mode === TCModalModes.CASE;
  }ngOnInit() {
    if (this.idMode.mode == TCModalModes.NEW) {
     this.tcAuthorization.requireCreate("ward_reports");
      this.title = this.appTranslation.getText("general","new") +  " " + "ward_report";
      this.wardReportDetail = new WardReportDetail();
      //set necessary defaults

    } else {
     this.tcAuthorization.requireUpdate("ward_report");
      this.title = this.appTranslation.getText("general","edit") +  " " + " " + "ward_report";
      this.isLoadingResults = true;
      this.persist.getWardReport(this.idMode.id).subscribe(wardReportDetail => {
        this.wardReportDetail = wardReportDetail;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        this.isLoadingResults = false;
      })

    }
  }  onAdd(): void {
    this.isLoadingResults = true;
   
    this.persist.add_ward_report(this.wardReportDetail).subscribe(value => {
      this.tcNotification.success("wardReport added");
      this.wardReportDetail.id = value.id;
      this.dialogRef.close(this.wardReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })
  }

  onUpdate(): void {
    this.isLoadingResults = true;
   

    this.persist.update_ward_report(this.wardReportDetail).subscribe(value => {
      this.tcNotification.success("ward_report updated");
      this.dialogRef.close(this.wardReportDetail);
    }, error => {
      this.isLoadingResults = false;
    })

  }  canSubmit():boolean{
        if (this.wardReportDetail.report == null){
            return false;
          }
          return true;

 }


//  searchPhyscian()
//  {
//   let dialogRef = this.physcianNavigator.pickDoctors(true);
//   dialogRef.afterClosed().subscribe((result: DoctorSummary[]) => {
//       if (result) {
//         // this.department_id = result[0].id;
//         this.Physcian = result[0].first_name;
//       }
//     });
//  }


  searchWard()
 {
  let dialogRef = this.wardNavigator.pickBedroomtypes(true)
  dialogRef.afterClosed().subscribe((result: BedroomtypeSummary[]) => {
      if (result) {
        // this.department_id = result[0].id;
        this.Ward = result[0].name;
        this.wardReportDetail.ward = result[0].id;
      }
    });
 }
 }