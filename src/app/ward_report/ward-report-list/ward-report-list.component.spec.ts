import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WardReportListComponent } from './ward-report-list.component';

describe('WardReportListComponent', () => {
  let component: WardReportListComponent;
  let fixture: ComponentFixture<WardReportListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WardReportListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WardReportListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
