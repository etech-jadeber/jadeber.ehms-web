import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";
import {TCAsyncJob} from "../../tc/asyncjob";
import {JobData} from "../../tc/jobs/job.model";
import {FilePersist} from "../../tc/files/file.persist";
import { UserDetail } from 'src/app/tc/users/user.model';
import { UserPersist } from 'src/app/tc/users/user.persist';
import { BedroomtypePersist } from 'src/app/bed/bedroomtypes/bedroomtype.persist';
import { BedroomtypeDetail } from 'src/app/bed/bedroomtypes/bedroomtype.model';

import {AppTranslation} from "../../app.translation";

import { WardReportPersist } from '../ward_report.persist';
import { WardReportNavigator } from '../ward_reprort.navigator';
import {
  WardReportDetail,
  WardReportSummary,
  WardReportSummaryPartialList,
} from '../ward_report.model';

@Component({
  selector: 'app-ward-report-list',
  templateUrl: './ward-report-list.component.html',
  styleUrls: ['./ward-report-list.component.css']
})
export class WardReportListComponent implements OnInit {

  wardReportsData: WardReportSummary[] = [];
  wardReportsTotalCount: number = 0;
  wardReportSelectAll:boolean = false;
  wardReportSelection: WardReportSummary[] = [];

  Report_date: string;
  doctors: {[id: string]: UserDetail} = {}
  wards : {[id: string] : BedroomtypeDetail} = {}

 wardReportsDisplayedColumns: string[] = ["select","action" ,"Date", "Ward", "Report", "Physcian" ];
  wardReportSearchTextBox: FormControl = new FormControl();
  wardReportIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) wardReportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) wardReportsSort: MatSort;  constructor(private router: Router,
                private location: Location,
                public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public wardReportPersist: WardReportPersist,
                public wardPersist: BedroomtypePersist,
                public wardReportNavigator: WardReportNavigator,
                public jobPersist: JobPersist,
                public userPersist: UserPersist,
                public  jobData: JobData,
                public filePersist: FilePersist,
                public tcAsyncJob: TCAsyncJob,

    ) {

        this.tcAuthorization.requireRead("ward_report");
       this.wardReportSearchTextBox.setValue(wardReportPersist.wardReportSearchText);
      //delay subsequent keyup events
      this.wardReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.wardReportPersist.wardReportSearchText = value;
        this.searchWard_reports();
      });
    } ngOnInit() {
   
      this.wardReportsSort.sortChange.subscribe(() => {
        this.wardReportsPaginator.pageIndex = 0;
        this.searchWard_reports(true);
      });

      this.wardReportsPaginator.page.subscribe(() => {
        this.searchWard_reports(true);
      });
      //start by loading items
      this.searchWard_reports();
    }

  searchWard_reports(isPagination:boolean = false): void {


    let paginator = this.wardReportsPaginator;
    let sorter = this.wardReportsSort;

    this.wardReportIsLoading = true;
    this.wardReportSelection = [];

    this.wardReportPersist.searchWardReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: WardReportSummaryPartialList) => {
      this.wardReportsData = partialList.data;
      this.loadAdditional(this.wardReportsData)
      if (partialList.total != -1) {
        this.wardReportsTotalCount = partialList.total;
      }
      this.wardReportIsLoading = false;
    }, error => {
      this.wardReportIsLoading = false;
    });

  } 
  
  loadAdditional(data: WardReportDetail[], isCalendar = false) {
    data.forEach((d, idx) => {
      if (!this.doctors[d.physcian]){
        this.doctors[d.physcian] = new UserDetail()
        this.userPersist.getUser(d.physcian).subscribe(
          doctor => this.doctors[d.physcian] = doctor
        )                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
      }

      if (!this.wards[d.ward]){
            this.wards[d.ward] = new BedroomtypeDetail()
            this.wardPersist.getBedroomtype(d.ward).subscribe(
              ward => {
                this.wards[d.ward] = ward;
              }
            )
      }


    })
  }

    getDoctor(id: string){
    if(this.doctors[id]){
      const {name} = this.doctors[id]
    return name ? `Dr.${name}` : ""
    }
  }

  
  
  
  downloadWardReports(): void {
    if(this.wardReportSelectAll){
         this.wardReportPersist.downloadAll().subscribe(downloadJob => {
        this.jobPersist.followJob(downloadJob.id, "download ward_report", true);
      });
    }
    else{
        this.wardReportPersist.download(this.tcUtilsArray.idsList(this.wardReportSelection)).subscribe(downloadJob => {
              this.jobPersist.followJob(downloadJob.id, "download ward_report",true);
            });
        }
  }
addWard_report(): void {
    let dialogRef = this.wardReportNavigator.addWardReport();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.searchWard_reports();
      }
    });
  }

  editWardReport(item: WardReportSummary) {
    let dialogRef = this.wardReportNavigator.editWardReport(item.id);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        TCUtilsAngular.assign(item, result);
      }

    });
  }

  deleteWardReport(item: WardReportSummary): void {
    let dialogRef = this.tcNavigator.confirmDeletion("ward_report");
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //do the deletion
        this.wardReportPersist.deleteWardReport(item.id).subscribe(response => {
          this.tcNotification.success("ward_report deleted");
          this.searchWard_reports();
        }, error => {
        });
      }

    });
  }  back():void{
      this.location.back();
    }


      getWard(ward_id: string): string {
      console.log(ward_id)
      if (!this.wards[ward_id]){
        return ''
      }
      return this.wards[ward_id].name
    }

}
