import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WardReportPickComponent } from './ward-report-pick.component';

describe('WardReportPickComponent', () => {
  let component: WardReportPickComponent;
  let fixture: ComponentFixture<WardReportPickComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WardReportPickComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WardReportPickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
