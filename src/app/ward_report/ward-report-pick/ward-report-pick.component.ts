import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Router} from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {debounceTime} from "rxjs/operators";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'
import {Location} from '@angular/common';

import {TCAuthorization} from "../../tc/authorization";
import {TCUtilsAngular} from "../../tc/utils-angular";
import {TCUtilsArray} from "../../tc/utils-array";
import {TCUtilsDate} from "../../tc/utils-date";
import {TCNotification} from "../../tc/notification";
import {TCNavigator} from "../../tc/navigator";
import {JobPersist} from "../../tc/jobs/job.persist";

import { WardReportPersist } from '../ward_report.persist';
import { WardReportNavigator } from '../ward_reprort.navigator';
import {
  WardReportDetail,
  WardReportSummary,
  WardReportSummaryPartialList,
} from '../ward_report.model';

import {AppTranslation} from "../../app.translation";
@Component({
  selector: 'app-ward_report-list',
  templateUrl: './ward-report-pick.component.html',
  styleUrls: ['./ward-report-pick.component.css']
})


export class WardReportPickComponent implements OnInit {

  wardReportsData: WardReportSummary[] = [];
  wardReportsTotalCount: number = 0;
  wardReportSelectAll:boolean = false;
  wardReportSelection: WardReportSummary[] = [];

 wardReportsDisplayedColumns: string[] = ["select","action" ,"report" ];
  wardReportSearchTextBox: FormControl = new FormControl();
  wardReportIsLoading: boolean = false;  @ViewChild(MatPaginator, {static: true}) wardReportsPaginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) wardReportsSort: MatSort;  
  
  constructor(  public tcAuthorization:TCAuthorization,
                public tcUtilsAngular: TCUtilsAngular,
                public tcUtilsArray: TCUtilsArray,
                public tcUtilsDate:TCUtilsDate,
                public tcNotification: TCNotification,
                public tcNavigator: TCNavigator,
                public appTranslation:AppTranslation,
                public wardReportPersist: WardReportPersist,
                public dialogRef: MatDialogRef<WardReportPickComponent>,
                @Inject(MAT_DIALOG_DATA) public selectOne: boolean | string,
    ) {

        this.tcAuthorization.requireRead("ward_report");
       this.wardReportSearchTextBox.setValue(wardReportPersist.wardReportSearchText);
      //delay subsequent keyup events
      this.wardReportSearchTextBox.valueChanges.pipe(debounceTime(500)).subscribe(value => {
        this.wardReportPersist.wardReportSearchText = value;
        this.searchWard_reports();
      });
    } ngOnInit() {
   
      this.wardReportsSort.sortChange.subscribe(() => {
        this.wardReportsPaginator.pageIndex = 0;
        this.searchWard_reports(true);
      });

      this.wardReportsPaginator.page.subscribe(() => {
        this.searchWard_reports(true);
      });
      //start by loading items
      this.searchWard_reports();
    }

  searchWard_reports(isPagination:boolean = false): void {


    let paginator = this.wardReportsPaginator;
    let sorter = this.wardReportsSort;

    this.wardReportIsLoading = true;
    this.wardReportSelection = [];

    this.wardReportPersist.searchWardReport(paginator.pageSize, isPagination? paginator.pageIndex:0, sorter.active, sorter.direction).subscribe((partialList: WardReportSummaryPartialList) => {
      this.wardReportsData = partialList.data;
      if (partialList.total != -1) {
        this.wardReportsTotalCount = partialList.total;
      }
      this.wardReportIsLoading = false;
    }, error => {
      this.wardReportIsLoading = false;
    });

  }
  markOneItem(item: WardReportSummary) {
    if(!this.tcUtilsArray.containsId(this.wardReportSelection, item.id)){
          this.wardReportSelection = [];
          this.wardReportSelection.push(item);
        }
        else{
          this.wardReportSelection = [];
        }
  }

  returnSelected(): void {
    this.dialogRef.close(this.wardReportSelection);
  }

  onCancel(): void {
    this.dialogRef.close();
  }
  }




  