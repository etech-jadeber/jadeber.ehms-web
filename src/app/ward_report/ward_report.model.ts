import { ExecFileSyncOptionsWithBufferEncoding } from "child_process";
import {TCId} from "../tc/models";

export class WardReportSummary extends TCId {
    Date: string;
    ward: string;
    physcian:string
    report:string;  
    }
    export class WardReportSummaryPartialList {
      data: WardReportSummary[];
      total: number;
    }

    export class WardReportDetail extends WardReportSummary {
       ward: string;
       report:string;  
    }
    
    export class WardReportDashboard {
      total: number;
    }