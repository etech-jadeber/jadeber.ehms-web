import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from 'rxjs';
import {environment} from "../../environments/environment";


import {TCDoParam,TCUrlParams,  TCUtilsHttp} from "../tc/utils-http";
import {TCId, TcDictionary} from "../tc/models";
import {WardReportDashboard, WardReportDetail, WardReportSummaryPartialList} from "./ward_report.model";


@Injectable({
  providedIn: 'root'
})
export class WardReportPersist {
 wardReportSearchText: string = "";
 
 constructor(private http: HttpClient) {
  }

  add_ward_report(item: WardReportDetail): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ward_report/", item);
  }

  update_ward_report(item: WardReportDetail): Observable<WardReportDetail> {
    return this.http.patch<WardReportDetail>(environment.tcApiBaseUri + "ward_report/" + item.id, item);
  }

 print(id: string): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ward_report/" + id + "/do", new TCDoParam("print", {}));
  }  filters(): any {
    let fltrs: TcDictionary<string> = new TcDictionary<string>();
    fltrs[TCUrlParams.searchText] = this.wardReportSearchText;
    //add custom filters
    return fltrs;
  }
 
  searchWardReport(pageSize: number, pageIndex: number, sort: string, order: string,): Observable<WardReportSummaryPartialList> {

    let url = TCUtilsHttp.buildSearchUrl("ward_report", this.wardReportSearchText, pageSize, pageIndex, sort, order);
    return this.http.get<WardReportSummaryPartialList>(url);

  } downloadAll(): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ward_report/do", new TCDoParam("download_all", this.filters()));
  }

  wardReportDashboard(): Observable<WardReportDashboard> {
    return this.http.get<WardReportDashboard>(environment.tcApiBaseUri + "ward_report/dashboard");
  }

  getWardReport(id: string): Observable<WardReportDetail> {
    return this.http.get<WardReportDetail>(environment.tcApiBaseUri + "ward_report/" + id);
  }

  download(ids: string[]): Observable<TCId> {
    return this.http.post<TCId>(environment.tcApiBaseUri + "ward_report/do", new TCDoParam("download", ids));
  }

  deleteWardReport(id: string): Observable<{}> {
    return this.http.delete(environment.tcApiBaseUri + "ward_report/" + id);
  }
 }