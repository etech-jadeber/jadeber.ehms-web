import {Router} from "@angular/router";
import {Injectable} from "@angular/core";
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

import {TCModalWidths} from "../tc/utils-angular";
import {TCIdMode, TCModalModes} from "../tc/models";

import {WardReportEditComponent} from "./ward-report-edit/ward-report-edit.component";
import {WardReportPickComponent} from "./ward-report-pick/ward-report-pick.component";


@Injectable({
  providedIn: 'root'
})

export class WardReportNavigator {

  constructor(private router: Router,
              public dialog: MatDialog) {
  }
  ward_reportsUrl(): string {
    return "/ward_reports";
  }

  ward_reportUrl(id: string): string {
    return "/ward_reports/" + id;
  }

  viewWardReports(): void {
    this.router.navigateByUrl(this.ward_reportsUrl());
  }

  viewWardReport(id: string): void {
    this.router.navigateByUrl(this.ward_reportUrl(id));
  }

  editWardReport(id: string): MatDialogRef<WardReportEditComponent> {
    const dialogRef = this.dialog.open(WardReportEditComponent, {
      data: new TCIdMode(id, TCModalModes.EDIT),
      width: TCModalWidths.medium
    });
    return dialogRef;
  }

  addWardReport(): MatDialogRef<WardReportEditComponent> {
    const dialogRef = this.dialog.open(WardReportEditComponent, {
          data: new TCIdMode(null, TCModalModes.NEW),
          width: TCModalWidths.medium
    });
    return dialogRef;
  }
  
   pickWardReports(selectOne: boolean=false): MatDialogRef<WardReportPickComponent> {
      const dialogRef = this.dialog.open(WardReportPickComponent, {
        data: selectOne,
        width: TCModalWidths.large
      });
      return dialogRef;
    }
}