import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { TCAppInit } from '../tc/app-init';
import { TCAuthentication } from '../tc/authentication';
import { TCNavigator } from '../tc/navigator';
import { TCUtilsDate } from '../tc/utils-date';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  companyName: string = environment.companyName;
  appName: string = environment.appName;

  constructor(public router: Router,public tcNavigator: TCNavigator,
              public tcAuthorization: TCAuthentication,
              public tcUtilsDate: TCUtilsDate) {
  }

  ngOnInit() {
    if (TCAppInit.isLoggedIn) {
      // this.tcNavigator.dashboard();
      this.router.navigateByUrl('/home')
    }
  }

}
