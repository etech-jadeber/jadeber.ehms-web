
export const environment = {
    production: true,
    companyName: 'Jadeber',
    appName: 'ehms',
    tcApiBaseUri: "http://192.168.1.65/jadeber-ehms-api/",
    loginCallback: 'http%3A%2F%2F192.168.1.65%2Fjadeber%2F%23%2Flogin',
    clientId: 'xyz',
    nationalLibraryOfMedicineAPIUri: "https://clinicaltables.nlm.nih.gov/",
    version: '0.0.29',
    core_emr_url :'http://192.168.1.65/core-emr/',
    dicom_url:"http://192.168.1.65/dicom/",
    dicom_viewer: "http://192.168.1.65:3000/viewer/",
    socketUrl: "http://192.168.1.65/jadeber-socket/",
  };
