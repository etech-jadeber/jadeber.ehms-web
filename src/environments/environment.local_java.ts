export const environment = {
    production: true,
    companyName: 'eTech',
    appName: 'eHealth',
    tcApiBaseUri: "http://localhost/jadeber-api-java/",
    nationalLibraryOfMedicineAPIUri: "https://clinicaltables.nlm.nih.gov/",
    loginCallback: 'http%3A%2F%2Flocalhost%2Fjadeber%2F%23%2Flogin',
    core_emr_url :'http://localhost/core-emr',
    clientId: 'xyz',
    version: '0.0.29',
    lastDays:15,
    dicom_url:"http://localhost/dicom/",
    dicom_viewer: "http://localhost:3000/viewer/",
     socketUrl:"http://localhost/jadeber-socket/"

  };
