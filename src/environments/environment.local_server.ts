
export const environment = {
  production: true,
  companyName: 'eTech',
  appName: 'eHealth',
  tcApiBaseUri: "https://et1.etechsc.com/jadeber-api/",
  loginCallback: 'https%3A%2F%2Fet1.etechsc.com%2Fjadeber%2F%23%2Flogin',
  clientId: 'xyz',
  nationalLibraryOfMedicineAPIUri: "https://clinicaltables.nlm.nih.gov/",
  version: '0.0.29',
  core_emr_url :'https://et1.etechsc.com/core-emr/',
  dicom_url:"https://et1.etechsc.com/dicom",
  dicom_viewer:"",
  socketUrl:"https://et1.etechsc.com/jadeber-socket/"

};
