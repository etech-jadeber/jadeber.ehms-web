export const environment = {
  production: false,
  companyName: 'eTech',
  appName: 'eHealth',
  tcApiBaseUri: "http://localhost/jadeber-api-java/",
  nationalLibraryOfMedicineAPIUri: "https://clinicaltables.nlm.nih.gov/",
  loginCallback: 'http%3A%2F%2Flocalhost%2Fehms%2F%23%2Flogin',
  clientId: 'def',
  version: '0.0.29',
  shareUnit: 100,
  serviceChargeNoraml: 0.05,
  serviceChargeForex: 0.01,
  evdModule:true,
  shareModule:true,
  coopModule:true,
  core_emr_url :'',
  coopShareUnit:1000,
  coopServiceCharge:1000,
  dicom_url:"http://localhost/dicom/",
  dicom_viewer: "http://localhost:3000/viewer/",
  socketUrl: ""

};
